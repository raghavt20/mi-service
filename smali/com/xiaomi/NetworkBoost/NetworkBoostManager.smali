.class public Lcom/xiaomi/NetworkBoost/NetworkBoostManager;
.super Ljava/lang/Object;
.source "NetworkBoostManager.java"


# instance fields
.field public a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

.field public b:Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;

.field public final c:Landroid/content/Context;

.field public d:Ljava/lang/Object;

.field public e:Lcom/xiaomi/NetworkBoost/ServiceCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/xiaomi/NetworkBoost/ServiceCallback;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->d:Ljava/lang/Object;

    .line 14
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->c:Landroid/content/Context;

    .line 15
    iput-object p2, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->e:Lcom/xiaomi/NetworkBoost/ServiceCallback;

    .line 16
    new-instance p1, Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;

    invoke-direct {p1, p0}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;-><init>(Lcom/xiaomi/NetworkBoost/NetworkBoostManager;)V

    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->b:Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;

    return-void
.end method

.method public static a(Ljava/io/FileDescriptor;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    :try_start_0
    invoke-virtual {p0}, Ljava/io/FileDescriptor;->valid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8
    const-string v0, "fd"

    .line 9
    const-class v1, Ljava/io/FileDescriptor;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 10
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 11
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result p0

    int-to-long v1, p0

    .line 12
    const/4 p0, 0x0

    invoke-virtual {v0, p0}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    long-to-int p0, v1

    return p0

    :cond_0
    const/4 p0, -0x1

    return p0

    :catch_0
    move-exception p0

    .line 13
    new-instance v0, Ljava/io/IOException;

    const-string v1, "FileDescriptor in this JVM lacks handle/fd fields"

    invoke-direct {v0, v1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_1
    move-exception p0

    .line 14
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "unable to access handle/fd fields in FileDescriptor"

    invoke-direct {v0, v1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static getSDKVersion()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public static getServiceVersion()I
    .locals 1

    .line 1
    invoke-static {}, Lcom/xiaomi/NetworkBoost/Version;->getServiceVersion()I

    move-result v0

    return v0
.end method


# virtual methods
.method public abortScan()Z
    .locals 2

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->abortScan()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 10
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public activeScan([I)Z
    .locals 2

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->activeScan([I)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 10
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public bindService()Z
    .locals 7

    .line 1
    const-string/jumbo v0, "xiaomi.NetworkBoostServiceManager"

    .line 4
    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_0
    const-string v3, "android.os.ServiceManager"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const-string v4, "getService"

    new-array v5, v2, [Ljava/lang/Class;

    const-class v6, Ljava/lang/String;

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 5
    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    .line 6
    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 11
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_0

    .line 14
    :try_start_1
    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->getServiceVersion()I

    move-result v0

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->setServiceVersion(I)V

    .line 15
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->e:Lcom/xiaomi/NetworkBoost/ServiceCallback;

    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/ServiceCallback;->onServiceConnected()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 17
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 18
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->e:Lcom/xiaomi/NetworkBoost/ServiceCallback;

    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/ServiceCallback;->onServiceDisconnected()V

    goto :goto_1

    .line 21
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 22
    const-string v3, "com.xiaomi.NetworkBoost"

    const-string v4, "com.xiaomi.NetworkBoost.NetworkBoostService"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 23
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->c:Landroid/content/Context;

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->b:Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;

    invoke-virtual {v3, v0, v4, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 28
    :goto_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    move v1, v2

    :cond_1
    return v1
.end method

.method public connectSlaveWifi(I)Z
    .locals 2

    .line 1
    const v0, 0x7fffffff

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->connectSlaveWifi(I)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 10
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public disableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;)Z
    .locals 2

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->disableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 10
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 11
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    return v1
.end method

.method public disableWifiSelectionOptByUid(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;I)Z
    .locals 2

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1, p2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->disableWifiSelectionOptByUid(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;I)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 10
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 11
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    return v1
.end method

.method public disconnectSlaveWifi()Z
    .locals 2

    .line 1
    const v0, 0x7fffffff

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->disconnectSlaveWifi()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 10
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public enableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;I)Z
    .locals 2

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1, p2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->enableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;I)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 10
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 11
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    return v1
.end method

.method public enableWifiSelectionOptByUid(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;II)Z
    .locals 2

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1, p2, p3}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->enableWifiSelectionOptByUid(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;II)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 10
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 11
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    return v1
.end method

.method public getAvailableIfaces()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->getAvailableIfaces()Ljava/util/Map;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 10
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 11
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 14
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method public getQoEByAvailableIfaceName(Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    const v0, 0x7fffffff

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    return-object p1

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 7
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->getQoEByAvailableIfaceName(Ljava/lang/String;)Ljava/util/Map;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 9
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 10
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 13
    :cond_1
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    return-object p1
.end method

.method public getQoEByAvailableIfaceNameV1(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .locals 1

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    new-instance p1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-direct {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;-><init>()V

    return-object p1

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 7
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->getQoEByAvailableIfaceNameV1(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 9
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 10
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 13
    :cond_1
    new-instance p1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-direct {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;-><init>()V

    return-object p1
.end method

.method public isCelluarDSDAState()Z
    .locals 2

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->isCelluarDSDAState()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 10
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public isSlaveWifiEnabledAndOthersOpt(I)I
    .locals 2

    .line 1
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, -0x1

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->isSlaveWifiEnabledAndOthersOpt(I)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 10
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public isSlaveWifiEnabledAndOthersOptByUid(II)I
    .locals 2

    .line 1
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, -0x1

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1, p2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->isSlaveWifiEnabledAndOthersOptByUid(II)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 10
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public isSupportDualCelluarData()Z
    .locals 2

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->isSupportDualCelluarData()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 10
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public isSupportDualWifi()Z
    .locals 2

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->isSupportDualWifi()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 10
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public isSupportMediaPlayerPolicy()Z
    .locals 2

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->isSupportMediaPlayerPolicy()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 10
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public registerCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
    .locals 2

    .line 1
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->registerCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 10
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public registerNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;I)Z
    .locals 2

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 10
    :try_start_0
    invoke-interface {v0, p1, p2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->registerNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;I)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 12
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 13
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    return v1
.end method

.method public registerNetLinkCallbackByUid(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;II)Z
    .locals 2

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 10
    :try_start_0
    invoke-interface {v0, p1, p2, p3}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->registerNetLinkCallbackByUid(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;II)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 12
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 13
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    return v1
.end method

.method public registerWifiLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback;)Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 10
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->registerWifiLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 12
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public reportBssidScore(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->reportBssidScore(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 10
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 11
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    :goto_0
    return-void
.end method

.method public requestAppTrafficStatistics(IJJ)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJJ)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    return-object p1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    :try_start_0
    invoke-interface/range {v0 .. v5}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->requestAppTrafficStatistics(IJJ)Ljava/util/Map;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 10
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 11
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 14
    :cond_1
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    return-object p1
.end method

.method public requestAppTrafficStatisticsByUid(IJJI)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJJI)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    return-object p1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move v6, p6

    :try_start_0
    invoke-interface/range {v0 .. v6}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->requestAppTrafficStatisticsByUid(IJJI)Ljava/util/Map;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 10
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 11
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 14
    :cond_1
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    return-object p1
.end method

.method public resumeBackgroundScan()Z
    .locals 2

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->resumeBackgroundScan()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 10
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public resumeWifiPowerSave()Z
    .locals 2

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->resumeWifiPowerSave()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 10
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public setDualCelluarDataEnable(Z)Z
    .locals 2

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->setDualCelluarDataEnable(Z)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 10
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public setSlaveWifiEnable(Z)Z
    .locals 2

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->setSlaveWifiEnable(Z)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 10
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public setSockPrio(II)Z
    .locals 2

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 7
    :try_start_0
    invoke-interface {v0, p1, p2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->setSockPrio(II)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 9
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public setSockPrio(Ljava/io/FileDescriptor;I)Z
    .locals 2

    .line 10
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 15
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a(Ljava/io/FileDescriptor;)I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->setSockPrio(II)Z

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 17
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    return v1
.end method

.method public setSockPrio(Ljava/net/Socket;I)Z
    .locals 2

    .line 18
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 23
    :cond_0
    :try_start_0
    invoke-static {p1}, Landroid/os/ParcelFileDescriptor;->fromSocket(Ljava/net/Socket;)Landroid/os/ParcelFileDescriptor;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a(Ljava/io/FileDescriptor;)I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->setSockPrio(II)Z

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 25
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    return v1
.end method

.method public setTCPCongestion(ILjava/lang/String;)Z
    .locals 2

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 7
    :try_start_0
    invoke-interface {v0, p1, p2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->setTCPCongestion(ILjava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 9
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public setTCPCongestion(Ljava/io/FileDescriptor;Ljava/lang/String;)Z
    .locals 2

    .line 10
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 15
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a(Ljava/io/FileDescriptor;)I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->setTCPCongestion(ILjava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 17
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    return v1
.end method

.method public setTCPCongestion(Ljava/net/Socket;Ljava/lang/String;)Z
    .locals 2

    .line 18
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 23
    :cond_0
    :try_start_0
    invoke-static {p1}, Landroid/os/ParcelFileDescriptor;->fromSocket(Ljava/net/Socket;)Landroid/os/ParcelFileDescriptor;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a(Ljava/io/FileDescriptor;)I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->setTCPCongestion(ILjava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 25
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    return v1
.end method

.method public setTrafficTransInterface(ILjava/lang/String;)Z
    .locals 2

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1, p2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->setTrafficTransInterface(ILjava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 10
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public setTrafficTransInterface(Ljava/io/FileDescriptor;Ljava/lang/String;)Z
    .locals 2

    .line 11
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 16
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a(Ljava/io/FileDescriptor;)I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->setTrafficTransInterface(ILjava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 18
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    return v1
.end method

.method public setTrafficTransInterface(Ljava/net/Socket;Ljava/lang/String;)Z
    .locals 2

    .line 19
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 24
    :cond_0
    :try_start_0
    invoke-static {p1}, Landroid/os/ParcelFileDescriptor;->fromSocket(Ljava/net/Socket;)Landroid/os/ParcelFileDescriptor;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a(Ljava/io/FileDescriptor;)I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->setTrafficTransInterface(ILjava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 26
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    return v1
.end method

.method public suspendBackgroundScan()Z
    .locals 2

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->suspendBackgroundScan()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 10
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public suspendWifiPowerSave()Z
    .locals 2

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->suspendWifiPowerSave()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 10
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public triggerWifiSelection()V
    .locals 1

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->triggerWifiSelection()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 10
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 11
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    :goto_0
    return-void
.end method

.method public unbindService()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->c:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 3
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->b:Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 9
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->d:Ljava/lang/Object;

    monitor-enter v0

    .line 10
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    .line 11
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->d:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 12
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
    .locals 2

    .line 1
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 10
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method

.method public unregisterNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;)Z
    .locals 2

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->unregisterNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 10
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 11
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    return v1
.end method

.method public unregisterNetLinkCallbackByUid(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;I)Z
    .locals 2

    .line 1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1, p2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->unregisterNetLinkCallbackByUid(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;I)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 10
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 11
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    return v1
.end method

.method public unregisterWifiLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback;)Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/Version;->isSupport(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    if-eqz v0, :cond_1

    .line 8
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->unregisterWifiLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 10
    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    :cond_1
    return v1
.end method
