.class public final Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;
.super Ljava/lang/Object;
.source "NetworkBoostManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/NetworkBoostManager;-><init>(Landroid/content/Context;Lcom/xiaomi/NetworkBoost/ServiceCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/xiaomi/NetworkBoost/NetworkBoostManager;


# direct methods
.method public constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkBoostManager;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;->a:Lcom/xiaomi/NetworkBoost/NetworkBoostManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;->a:Lcom/xiaomi/NetworkBoost/NetworkBoostManager;

    .line 2
    iget-object p1, p1, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->d:Ljava/lang/Object;

    .line 3
    monitor-enter p1

    .line 4
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;->a:Lcom/xiaomi/NetworkBoost/NetworkBoostManager;

    invoke-static {p2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    move-result-object p2

    .line 5
    iput-object p2, v0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :try_start_1
    iget-object p2, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;->a:Lcom/xiaomi/NetworkBoost/NetworkBoostManager;

    .line 7
    iget-object p2, p2, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    .line 8
    invoke-interface {p2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->getServiceVersion()I

    move-result p2

    .line 10
    invoke-static {p2}, Lcom/xiaomi/NetworkBoost/Version;->setServiceVersion(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 12
    :try_start_2
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 14
    :goto_0
    iget-object p2, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;->a:Lcom/xiaomi/NetworkBoost/NetworkBoostManager;

    .line 15
    iget-object p2, p2, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->d:Ljava/lang/Object;

    .line 16
    invoke-virtual {p2}, Ljava/lang/Object;->notifyAll()V

    .line 17
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 19
    iget-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;->a:Lcom/xiaomi/NetworkBoost/NetworkBoostManager;

    .line 20
    iget-object p1, p1, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->e:Lcom/xiaomi/NetworkBoost/ServiceCallback;

    .line 21
    invoke-interface {p1}, Lcom/xiaomi/NetworkBoost/ServiceCallback;->onServiceConnected()V

    return-void

    :catchall_0
    move-exception p2

    .line 22
    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p2
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;->a:Lcom/xiaomi/NetworkBoost/NetworkBoostManager;

    .line 2
    iget-object p1, p1, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->d:Ljava/lang/Object;

    .line 3
    monitor-enter p1

    .line 4
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;->a:Lcom/xiaomi/NetworkBoost/NetworkBoostManager;

    .line 5
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->a:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    .line 6
    iget-object v0, v0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->d:Ljava/lang/Object;

    .line 7
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 8
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    iget-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;->a:Lcom/xiaomi/NetworkBoost/NetworkBoostManager;

    .line 11
    iget-object p1, p1, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->e:Lcom/xiaomi/NetworkBoost/ServiceCallback;

    .line 12
    invoke-interface {p1}, Lcom/xiaomi/NetworkBoost/ServiceCallback;->onServiceDisconnected()V

    return-void

    :catchall_0
    move-exception v0

    .line 13
    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
