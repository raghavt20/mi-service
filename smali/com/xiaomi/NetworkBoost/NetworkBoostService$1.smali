.class Lcom/xiaomi/NetworkBoost/NetworkBoostService$1;
.super Lvendor/xiaomi/hidl/minet/V1_0/IMiNetCallback$Stub;
.source "NetworkBoostService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/NetworkBoostService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    .line 602
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-direct {p0}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public notifyCommon(ILjava/lang/String;)V
    .locals 3
    .param p1, "cmd"    # I
    .param p2, "attr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 607
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NetworkBoostService"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    packed-switch p1, :pswitch_data_0

    .line 613
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknow cmd:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 610
    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostService$1;->onFlowStatGet(Ljava/lang/String;)V

    .line 611
    nop

    .line 615
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x3ed
        :pswitch_0
    .end packed-switch
.end method

.method public onFlowStatGet(Ljava/lang/String;)V
    .locals 2
    .param p1, "attr"    # Ljava/lang/String;

    .line 618
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onFlowStatGet"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NetworkBoostService"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkAccelerateSwitchService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 620
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkAccelerateSwitchService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->notifyCollectIpAddress(Ljava/lang/String;)Z

    .line 622
    :cond_0
    return-void
.end method
