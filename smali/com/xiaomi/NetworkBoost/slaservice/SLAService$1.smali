.class Lcom/xiaomi/NetworkBoost/slaservice/SLAService$1;
.super Landroid/database/ContentObserver;
.source "SLAService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->initCloudObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 624
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .line 627
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$mcheckMiwillCloudController(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Z

    move-result v0

    const-string v1, "SLM-SRV-SLAService"

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmMiWillManager(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 628
    const-string v0, "cloud controller enable miwill."

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fputmMiWillManager(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)V

    goto :goto_0

    .line 631
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$mcheckMiwillCloudController(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmMiWillManager(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 632
    const-string v0, "cloud controller disable miwill."

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->destroyInstance()V

    .line 634
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fputmMiWillManager(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)V

    .line 637
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$misEnableDWMonitor(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V

    .line 638
    return-void
.end method
