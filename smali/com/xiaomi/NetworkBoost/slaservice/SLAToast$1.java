class com.xiaomi.NetworkBoost.slaservice.SLAToast$1 implements com.xiaomi.NetworkBoost.StatusManager$IAppStatusListener {
	 /* .source "SLAToast.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/SLAToast; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.SLAToast this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.slaservice.SLAToast$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/slaservice/SLAToast; */
/* .line 40 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAudioChanged ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mode" # I */
/* .line 61 */
return;
} // .end method
public void onForegroundInfoChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 4 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .line 43 */
com.xiaomi.NetworkBoost.slaservice.SLAToast .-$$Nest$sfgetTAG ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "foreground uid:"; // const-string v2, "foreground uid:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
java.lang.Integer .toString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", isColdStart:"; // const-string v2, ", isColdStart:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 44 */
v2 = (( miui.process.ForegroundInfo ) p1 ).isColdStart ( ); // invoke-virtual {p1}, Lmiui/process/ForegroundInfo;->isColdStart()Z
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", isLinkTurboEnable:"; // const-string v2, ", isLinkTurboEnable:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.xiaomi.NetworkBoost.slaservice.SLAToast .-$$Nest$fgetisLinkTurboEnable ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 43 */
android.util.Log .i ( v0,v1 );
/* .line 46 */
v0 = (( miui.process.ForegroundInfo ) p1 ).isColdStart ( ); // invoke-virtual {p1}, Lmiui/process/ForegroundInfo;->isColdStart()Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 v0 = this.this$0;
	 v0 = 	 com.xiaomi.NetworkBoost.slaservice.SLAToast .-$$Nest$fgetisLinkTurboEnable ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 47 */
		 com.xiaomi.NetworkBoost.slaservice.SLAToast .-$$Nest$sfgetmApps ( );
		 /* iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
		 java.lang.Integer .toString ( v1 );
		 v0 = 		 (( java.util.HashSet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 48 */
			 android.os.Message .obtain ( );
			 /* .line 49 */
			 /* .local v0, "msg":Landroid/os/Message; */
			 int v1 = 1; // const/4 v1, 0x1
			 /* iput v1, v0, Landroid/os/Message;->what:I */
			 /* .line 50 */
			 v1 = this.this$0;
			 com.xiaomi.NetworkBoost.slaservice.SLAToast .-$$Nest$fgetmHandler ( v1 );
			 /* const-wide/16 v2, 0x64 */
			 (( android.os.Handler ) v1 ).sendMessageAtTime ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z
			 /* .line 53 */
		 } // .end local v0 # "msg":Landroid/os/Message;
	 } // :cond_0
	 return;
} // .end method
public void onUidGone ( Integer p0, Boolean p1 ) {
	 /* .locals 0 */
	 /* .param p1, "uid" # I */
	 /* .param p2, "disabled" # Z */
	 /* .line 57 */
	 return;
} // .end method
