.class public Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;
.super Ljava/lang/Object;
.source "MiWillManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;,
        Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$MiWillHandler;
    }
.end annotation


# static fields
.field private static final ACTION_ENABLE_MIWILL:Ljava/lang/String; = "miui.wifi.ENABLE_MIWILL"

.field private static final COOKIE_UPBOUND:J = 0x64L

.field private static final DBG:Z = true

.field private static final DEBUG_COMMAND:Z = true

.field private static final INTERFACE_NAME:Ljava/lang/String; = "miw_oem0"

.field private static final MIWILL_DISABLED:I = 0x0

.field private static final MIWILL_ENABLED:I = 0x1

.field private static final MIWILL_TYPE:[B

.field private static final RECHECK_MAX_TIMES:I = 0x2

.field private static final REDUDANCY_MODE:Ljava/lang/String; = "redudancy"

.field public static final REDUDANCY_MODE_KEY:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MIWILL-MiWillManager"

.field private static final WAIT_RECHECK_DELAY_MILLIS:I = 0x2710

.field private static final WIFI_MIWILL_ENABLE:Ljava/lang/String; = "wifi_miwill_enable"

.field private static final WPS_DEVICE_XIAOMI:Ljava/lang/String; = "xiaomi"

.field private static final XIAOMI_OUI:[B

.field private static final mModeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sInstance:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;


# instance fields
.field private isDualWifiReady:Z

.field private mContext:Landroid/content/Context;

.field private mCookie:J

.field private mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

.field private mHandler:Landroid/os/Handler;

.field private mMiWillHal:Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

.field private mModeKey:I

.field private final mNotification:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;

.field private mSettingsObserver:Landroid/database/ContentObserver;

.field private mThread:Landroid/os/HandlerThread;

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmCookie(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mCookie:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmMiWillHal(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mMiWillHal:Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

    return-void
.end method

.method static bridge synthetic -$$Nest$minitMiWillHal(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->initMiWillHal()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetMiwillStatus(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->setMiwillStatus(ZI)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateStatus(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->updateStatus()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 88
    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->XIAOMI_OUI:[B

    .line 89
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->MIWILL_TYPE:[B

    .line 94
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mModeMap:Ljava/util/HashMap;

    .line 96
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "redudancy"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    return-void

    :array_0
    .array-data 1
        -0x74t
        -0x42t
        -0x42t
    .end array-data

    :array_1
    .array-data 1
        0x10t
        0x34t
    .end array-data
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 101
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 102
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mHandler:Landroid/os/Handler;

    .line 103
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mThread:Landroid/os/HandlerThread;

    .line 104
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mMiWillHal:Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

    .line 105
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mContext:Landroid/content/Context;

    .line 106
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mCookie:J

    .line 107
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->isDualWifiReady:Z

    .line 108
    const/4 v1, 0x1

    iput v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mModeKey:I

    .line 110
    new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$1;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$1;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)V

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    .line 120
    new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;

    invoke-direct {v1, p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification-IA;)V

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mNotification:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;

    .line 183
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mContext:Landroid/content/Context;

    .line 184
    return-void
.end method

.method private checkGateway(Ljava/lang/String;Ljava/lang/String;)Landroid/net/MacAddress;
    .locals 20
    .param p1, "wifi_gateway"    # Ljava/lang/String;
    .param p2, "slave_wifi_gateway"    # Ljava/lang/String;

    .line 621
    move-object/from16 v1, p1

    const-string v2, ""

    const-string v3, "MIWILL-MiWillManager"

    invoke-virtual/range {p1 .. p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v4, 0x0

    if-nez v0, :cond_0

    .line 622
    return-object v4

    .line 624
    :cond_0
    const/4 v5, 0x0

    .line 626
    .local v5, "fd":Ljava/io/FileDescriptor;
    :try_start_0
    sget v0, Landroid/system/OsConstants;->NETLINK_ROUTE:I

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkSocket;->forProto(I)Ljava/io/FileDescriptor;

    move-result-object v0

    move-object v5, v0

    .line 628
    invoke-static {v5}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkSocket;->connectToKernel(Ljava/io/FileDescriptor;)V

    .line 630
    invoke-static {v5}, Landroid/system/Os;->getsockname(Ljava/io/FileDescriptor;)Ljava/net/SocketAddress;

    move-result-object v0

    check-cast v0, Landroid/system/NetlinkSocketAddress;

    .line 632
    .local v0, "localAddr":Landroid/system/NetlinkSocketAddress;
    const/4 v12, 0x1

    .line 633
    .local v12, "TEST_SEQNO":I
    const/4 v6, 0x1

    invoke-static {v6}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->newGetNeighborsRequest(I)[B

    move-result-object v6

    move-object v13, v6

    .line 635
    .local v13, "req":[B
    const-wide/16 v14, 0x1f4

    .line 636
    .local v14, "TIMEOUT":J
    const/4 v8, 0x0

    array-length v9, v13

    const-wide/16 v10, 0x1f4

    move-object v6, v5

    move-object v7, v13

    invoke-static/range {v6 .. v11}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkSocket;->sendMessage(Ljava/io/FileDescriptor;[BIIJ)I

    move-result v6

    .line 637
    .local v6, "len":I
    array-length v7, v13

    if-ne v7, v6, :cond_1

    .line 638
    const-string v7, "req.length == req.length"

    invoke-static {v3, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    :cond_1
    const/4 v7, 0x0

    .line 644
    .local v7, "doneMessageCount":I
    const/4 v8, 0x0

    .line 645
    .local v8, "wifi_gateway_mac":Landroid/net/MacAddress;
    const/4 v9, 0x0

    .line 647
    .local v9, "slave_wifi_gateway_mac":Landroid/net/MacAddress;
    :goto_0
    if-nez v7, :cond_8

    .line 648
    const/16 v10, 0x2000

    move v11, v6

    move/from16 v16, v7

    .end local v6    # "len":I
    .end local v7    # "doneMessageCount":I
    .local v11, "len":I
    .local v16, "doneMessageCount":I
    const-wide/16 v6, 0x1f4

    invoke-static {v5, v10, v6, v7}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkSocket;->recvMessage(Ljava/io/FileDescriptor;IJ)Ljava/nio/ByteBuffer;

    move-result-object v6

    move/from16 v7, v16

    .line 651
    .end local v16    # "doneMessageCount":I
    .local v6, "response":Ljava/nio/ByteBuffer;
    .restart local v7    # "doneMessageCount":I
    :goto_1
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v10

    if-lez v10, :cond_7

    .line 652
    sget v10, Landroid/system/OsConstants;->NETLINK_ROUTE:I

    invoke-static {v6, v10}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;->parse(Ljava/nio/ByteBuffer;I)Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;

    move-result-object v10

    .line 653
    .local v10, "nlMsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;
    invoke-virtual {v10}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;->getHeader()Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;

    move-result-object v16

    move-object/from16 v17, v16

    .line 655
    .local v17, "hdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
    move-object/from16 v4, v17

    move-object/from16 v17, v0

    .end local v0    # "localAddr":Landroid/system/NetlinkSocketAddress;
    .local v4, "hdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
    .local v17, "localAddr":Landroid/system/NetlinkSocketAddress;
    iget-short v0, v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S

    move-object/from16 v18, v6

    .end local v6    # "response":Ljava/nio/ByteBuffer;
    .local v18, "response":Ljava/nio/ByteBuffer;
    const/4 v6, 0x3

    if-ne v0, v6, :cond_2

    .line 656
    add-int/lit8 v7, v7, 0x1

    .line 657
    move-object/from16 v0, v17

    move-object/from16 v6, v18

    const/4 v4, 0x0

    goto :goto_1

    .line 660
    :cond_2
    iget-short v0, v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S

    const/16 v6, 0x1c

    if-ne v6, v0, :cond_5

    .line 661
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NetlinkConstants.RTM_NEWNEIGH == hdr.nlmsg_type"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v10}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    move-object v0, v10

    check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;

    .line 663
    .local v0, "neighMsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v16, v4

    .end local v4    # "hdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
    .local v16, "hdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
    const-string v4, "macaddress "

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->getLinkLayerAddress()[B

    move-result-object v6

    invoke-static {v6}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->getMacAddress([B)Landroid/net/MacAddress;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->getDestination()Ljava/net/InetAddress;

    move-result-object v4

    if-nez v4, :cond_3

    move-object v4, v2

    goto :goto_2

    :cond_3
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->getDestination()Ljava/net/InetAddress;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v4

    .line 665
    .local v4, "ipLiteral":Ljava/lang/String;
    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v19, v7

    .end local v7    # "doneMessageCount":I
    .local v19, "doneMessageCount":I
    const-string v7, "ip address "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 667
    if-nez v8, :cond_4

    .line 668
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->getLinkLayerAddress()[B

    move-result-object v6

    invoke-static {v6}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->getMacAddress([B)Landroid/net/MacAddress;

    move-result-object v6

    move-object v8, v6

    goto :goto_3

    .line 670
    :cond_4
    if-nez v9, :cond_6

    .line 671
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->getLinkLayerAddress()[B

    move-result-object v6

    invoke-static {v6}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->getMacAddress([B)Landroid/net/MacAddress;

    move-result-object v6

    move-object v9, v6

    goto :goto_3

    .line 660
    .end local v0    # "neighMsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;
    .end local v16    # "hdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
    .end local v19    # "doneMessageCount":I
    .local v4, "hdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
    .restart local v7    # "doneMessageCount":I
    :cond_5
    move-object/from16 v16, v4

    move/from16 v19, v7

    .line 675
    .end local v4    # "hdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
    .end local v7    # "doneMessageCount":I
    .end local v10    # "nlMsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;
    .restart local v19    # "doneMessageCount":I
    :cond_6
    :goto_3
    move-object/from16 v0, v17

    move-object/from16 v6, v18

    move/from16 v7, v19

    const/4 v4, 0x0

    goto/16 :goto_1

    .line 651
    .end local v17    # "localAddr":Landroid/system/NetlinkSocketAddress;
    .end local v18    # "response":Ljava/nio/ByteBuffer;
    .end local v19    # "doneMessageCount":I
    .local v0, "localAddr":Landroid/system/NetlinkSocketAddress;
    .restart local v6    # "response":Ljava/nio/ByteBuffer;
    .restart local v7    # "doneMessageCount":I
    :cond_7
    move-object/from16 v17, v0

    move-object/from16 v18, v6

    move/from16 v19, v7

    .line 676
    .end local v0    # "localAddr":Landroid/system/NetlinkSocketAddress;
    .end local v6    # "response":Ljava/nio/ByteBuffer;
    .end local v7    # "doneMessageCount":I
    .restart local v17    # "localAddr":Landroid/system/NetlinkSocketAddress;
    .restart local v19    # "doneMessageCount":I
    move v6, v11

    const/4 v4, 0x0

    goto/16 :goto_0

    .line 677
    .end local v11    # "len":I
    .end local v17    # "localAddr":Landroid/system/NetlinkSocketAddress;
    .end local v19    # "doneMessageCount":I
    .restart local v0    # "localAddr":Landroid/system/NetlinkSocketAddress;
    .local v6, "len":I
    .restart local v7    # "doneMessageCount":I
    :cond_8
    move-object/from16 v17, v0

    move v11, v6

    move/from16 v16, v7

    .end local v0    # "localAddr":Landroid/system/NetlinkSocketAddress;
    .end local v6    # "len":I
    .end local v7    # "doneMessageCount":I
    .restart local v11    # "len":I
    .local v16, "doneMessageCount":I
    .restart local v17    # "localAddr":Landroid/system/NetlinkSocketAddress;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "wifi_gateway_mac:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " slave_wifi_gateway_mac:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    if-eqz v8, :cond_9

    invoke-virtual {v8, v9}, Landroid/net/MacAddress;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_9

    .line 679
    nop

    .line 686
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    .line 679
    return-object v8

    .line 686
    .end local v8    # "wifi_gateway_mac":Landroid/net/MacAddress;
    .end local v9    # "slave_wifi_gateway_mac":Landroid/net/MacAddress;
    .end local v11    # "len":I
    .end local v12    # "TEST_SEQNO":I
    .end local v13    # "req":[B
    .end local v14    # "TIMEOUT":J
    .end local v16    # "doneMessageCount":I
    .end local v17    # "localAddr":Landroid/system/NetlinkSocketAddress;
    :cond_9
    goto :goto_4

    :catchall_0
    move-exception v0

    move-object/from16 v2, p2

    goto :goto_5

    .line 682
    :catch_0
    move-exception v0

    .line 683
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 686
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    :goto_4
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    .line 687
    nop

    .line 688
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "wifi_gateway:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " slave_wifi_gateway:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v2, p2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    const/4 v3, 0x0

    return-object v3

    .line 686
    :goto_5
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    .line 687
    throw v0
.end method

.method private checkMiWillIp()Z
    .locals 8

    .line 493
    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v1

    .line 494
    .local v1, "net":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 495
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/net/NetworkInterface;

    .line 496
    .local v2, "networkInterface":Ljava/net/NetworkInterface;
    invoke-virtual {v2}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v3

    .line 497
    .local v3, "add":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :goto_1
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 498
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/InetAddress;

    .line 499
    .local v4, "a":Ljava/net/InetAddress;
    invoke-virtual {v4}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    const-string v6, "192.168."

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 500
    invoke-virtual {v2}, Ljava/net/NetworkInterface;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    .line 501
    .local v5, "netInterface":Ljava/lang/String;
    if-eqz v5, :cond_0

    const-string v6, "miw_oem0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    .line 502
    invoke-static {v5}, Lcom/xiaomi/NetworkBoost/StatusManager;->isTetheredIfaces(Ljava/lang/String;)Z

    move-result v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v6, :cond_0

    .line 503
    const/4 v0, 0x1

    return v0

    .line 506
    .end local v4    # "a":Ljava/net/InetAddress;
    .end local v5    # "netInterface":Ljava/lang/String;
    :cond_0
    goto :goto_1

    .line 507
    .end local v2    # "networkInterface":Ljava/net/NetworkInterface;
    .end local v3    # "add":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :cond_1
    goto :goto_0

    .line 511
    .end local v1    # "net":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_2
    nop

    .line 512
    return v0

    .line 508
    :catch_0
    move-exception v1

    .line 509
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkMiWillIp Exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MIWILL-MiWillManager"

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    return v0
.end method

.method private deinitMiWillHal()V
    .locals 2

    .line 264
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mMiWillHal:Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

    if-eqz v0, :cond_0

    .line 266
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    invoke-interface {v0, v1}, Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;->unlinkToDeath(Landroid/os/IHwBinder$DeathRecipient;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    goto :goto_0

    .line 268
    :catch_0
    move-exception v0

    .line 271
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mMiWillHal:Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

    .line 273
    :cond_0
    return-void
.end method

.method public static destroyInstance()V
    .locals 4

    .line 167
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    if-eqz v0, :cond_1

    .line 168
    const-class v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    monitor-enter v0

    .line 169
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 171
    :try_start_1
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->onDestroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175
    goto :goto_0

    .line 173
    :catch_0
    move-exception v1

    .line 174
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "MIWILL-MiWillManager"

    const-string v3, "destroyInstance onDestroy catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 176
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    .line 178
    :cond_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 180
    :cond_1
    :goto_1
    return-void
.end method

.method private disableMiwill(ZI)V
    .locals 4
    .param p1, "ready"    # Z
    .param p2, "retry_cnt"    # I

    .line 383
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mMiWillHal:Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

    const-string/jumbo v1, "set miw_oem0 0"

    invoke-interface {v0, v1}, Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;->setMiwillParameter(Ljava/lang/String;)Z

    .line 384
    if-eqz p1, :cond_0

    if-lez p2, :cond_0

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 385
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x67

    invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 390
    :cond_0
    goto :goto_0

    .line 388
    :catch_0
    move-exception v0

    .line 389
    .local v0, "e1":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "disableMiwill catch e = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MIWILL-MiWillManager"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    .end local v0    # "e1":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 150
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    if-nez v0, :cond_1

    .line 151
    const-class v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    monitor-enter v0

    .line 152
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    if-nez v1, :cond_0

    .line 153
    new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :try_start_1
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->onCreate()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    goto :goto_0

    .line 157
    :catch_0
    move-exception v1

    .line 158
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "MIWILL-MiWillManager"

    const-string v3, "getInstance onCreate catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 161
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 163
    :cond_1
    :goto_1
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    return-object v0
.end method

.method private static getMacAddress([B)Landroid/net/MacAddress;
    .locals 3
    .param p0, "linkLayerAddress"    # [B

    .line 609
    if-eqz p0, :cond_0

    .line 611
    :try_start_0
    invoke-static {p0}, Landroid/net/MacAddress;->fromBytes([B)Landroid/net/MacAddress;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 612
    :catch_0
    move-exception v0

    .line 613
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to parse link-layer address: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MIWILL-MiWillManager"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private initMiWillHal()V
    .locals 6

    .line 241
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mMiWillHal:Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

    const-string v1, "MIWILL-MiWillManager"

    if-eqz v0, :cond_0

    .line 242
    const-string v0, "initMiWillHal mMiWillHal has already been inited."

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    return-void

    .line 247
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;->getService(Z)Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mMiWillHal:Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

    .line 248
    if-nez v0, :cond_1

    .line 249
    const-string v0, "MiWill hal get failed"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 252
    :cond_1
    const-string v0, "MiWill hal get success"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    iget-wide v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mCookie:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    const-wide/16 v4, 0x64

    rem-long/2addr v2, v4

    iput-wide v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mCookie:J

    .line 254
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mMiWillHal:Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    invoke-interface {v0, v4, v2, v3}, Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;->linkToDeath(Landroid/os/IHwBinder$DeathRecipient;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 260
    :goto_0
    goto :goto_1

    .line 257
    :catch_0
    move-exception v0

    .line 258
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "onCreate catch"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 259
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->deinitMiWillHal()V

    .line 261
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method public static is24GHz(I)Z
    .locals 1
    .param p0, "freq"    # I

    .line 561
    const/16 v0, 0x960

    if-le p0, v0, :cond_0

    const/16 v0, 0x9c4

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static is24GHz(Landroid/net/wifi/ScanResult;)Z
    .locals 1
    .param p0, "scanResult"    # Landroid/net/wifi/ScanResult;

    .line 553
    iget v0, p0, Landroid/net/wifi/ScanResult;->frequency:I

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->is24GHz(I)Z

    move-result v0

    return v0
.end method

.method public static is5GHz(I)Z
    .locals 1
    .param p0, "freq"    # I

    .line 565
    const/16 v0, 0x1324

    if-le p0, v0, :cond_0

    const/16 v0, 0x170c

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static is5GHz(Landroid/net/wifi/ScanResult;)Z
    .locals 1
    .param p0, "scanResult"    # Landroid/net/wifi/ScanResult;

    .line 557
    iget v0, p0, Landroid/net/wifi/ScanResult;->frequency:I

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->is5GHz(I)Z

    move-result v0

    return v0
.end method

.method private isMiWillRouter(Landroid/net/wifi/ScanResult;)Z
    .locals 11
    .param p1, "scanResult"    # Landroid/net/wifi/ScanResult;

    .line 327
    const-string v0, "MIWILL-MiWillManager"

    const/4 v1, 0x0

    if-nez p1, :cond_0

    .line 328
    const-string v2, "isMiWillRouter: null scan result"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    return v1

    .line 331
    :cond_0
    invoke-virtual {p1}, Landroid/net/wifi/ScanResult;->getInformationElements()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/net/wifi/ScanResult$InformationElement;

    .line 332
    .local v2, "ies":[Landroid/net/wifi/ScanResult$InformationElement;
    if-eqz v2, :cond_9

    array-length v3, v2

    if-lez v3, :cond_9

    .line 333
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_9

    .line 334
    aget-object v4, v2, v3

    invoke-virtual {v4}, Landroid/net/wifi/ScanResult$InformationElement;->getId()I

    move-result v4

    const/16 v5, 0xdd

    if-ne v4, v5, :cond_8

    .line 335
    aget-object v4, v2, v3

    invoke-virtual {v4}, Landroid/net/wifi/ScanResult$InformationElement;->getBytes()Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 336
    .local v4, "byteBuffers":Ljava/nio/ByteBuffer;
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I

    move-result v5

    new-array v5, v5, [B

    .line 337
    .local v5, "value":[B
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I

    move-result v6

    sget-object v7, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->XIAOMI_OUI:[B

    array-length v7, v7

    sget-object v8, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->MIWILL_TYPE:[B

    array-length v8, v8

    add-int/2addr v7, v8

    if-eq v6, v7, :cond_1

    .line 338
    goto :goto_5

    .line 341
    :cond_1
    :try_start_0
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I

    move-result v6

    invoke-virtual {v4, v5, v1, v6}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 342
    const/4 v6, 0x0

    .line 343
    .local v6, "j":I
    const/4 v7, 0x0

    .local v7, "k":I
    :goto_1
    sget-object v8, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->XIAOMI_OUI:[B

    array-length v9, v8

    if-ge v7, v9, :cond_3

    .line 344
    aget-byte v9, v8, v7

    aget-byte v10, v5, v6

    if-eq v9, v10, :cond_2

    .line 345
    goto :goto_2

    .line 347
    :cond_2
    add-int/lit8 v6, v6, 0x1

    .line 343
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 349
    .end local v7    # "k":I
    :cond_3
    :goto_2
    array-length v7, v8

    if-eq v6, v7, :cond_4

    .line 350
    goto :goto_5

    .line 352
    :cond_4
    const/4 v7, 0x0

    .restart local v7    # "k":I
    :goto_3
    sget-object v8, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->MIWILL_TYPE:[B

    array-length v9, v8

    if-ge v7, v9, :cond_6

    .line 353
    aget-byte v8, v8, v7

    aget-byte v9, v5, v6

    if-eq v8, v9, :cond_5

    .line 354
    goto :goto_4

    .line 356
    :cond_5
    add-int/lit8 v6, v6, 0x1

    .line 352
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 358
    .end local v7    # "k":I
    :cond_6
    :goto_4
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I

    move-result v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v6, v7, :cond_7

    .line 359
    goto :goto_5

    .line 361
    :cond_7
    const/4 v0, 0x1

    return v0

    .line 363
    .end local v6    # "j":I
    :catch_0
    move-exception v6

    .line 364
    .local v6, "e":Ljava/lang/Exception;
    const-string v7, "isMiWillRouter catch:"

    invoke-static {v0, v7, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 333
    .end local v4    # "byteBuffers":Ljava/nio/ByteBuffer;
    .end local v5    # "value":[B
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_8
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 369
    .end local v3    # "i":I
    :cond_9
    return v1
.end method

.method private registerSwitchObserver()V
    .locals 4

    .line 276
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mSettingsObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 277
    const-string v0, "MIWILL-MiWillManager"

    const-string v1, "registerSwitchObserver observer already registered! check your code."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    return-void

    .line 281
    :cond_0
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$2;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 292
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 293
    const-string/jumbo v1, "wifi_miwill_enable"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 292
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 296
    return-void
.end method

.method private setMiwillStatus(ZI)V
    .locals 19
    .param p1, "ready"    # Z
    .param p2, "retry_cnt"    # I

    .line 394
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mMiWillHal:Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

    const-string v2, "MIWILL-MiWillManager"

    if-eqz v0, :cond_17

    .line 395
    const/4 v0, 0x0

    .line 396
    .local v0, "wifi_support_miwill":Z
    const/4 v3, 0x0

    .line 397
    .local v3, "slave_wifi_support_miwill":Z
    const/4 v4, 0x0

    .line 398
    .local v4, "wifiBSSID":Ljava/lang/String;
    const/4 v5, 0x0

    .line 399
    .local v5, "slavewifiBSSID":Ljava/lang/String;
    const/4 v6, 0x0

    .line 400
    .local v6, "wifi_srs":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    const/4 v7, 0x0

    .line 401
    .local v7, "dhcpInfo":Landroid/net/DhcpInfo;
    const/4 v8, 0x0

    .line 402
    .local v8, "slaveDhcpInfo":Landroid/net/DhcpInfo;
    const/4 v9, 0x0

    .line 403
    .local v9, "wifiInfo":Landroid/net/wifi/WifiInfo;
    const/4 v10, 0x0

    .line 406
    .local v10, "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    iget-object v11, v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v12, 0x0

    if-eqz v11, :cond_1

    .line 407
    invoke-virtual {v11}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v9

    .line 408
    if-nez v9, :cond_0

    move-object v11, v12

    goto :goto_0

    :cond_0
    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v11

    :goto_0
    move-object v4, v11

    .line 410
    iget-object v11, v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v11}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v6

    .line 411
    iget-object v11, v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v11}, Landroid/net/wifi/WifiManager;->getDhcpInfo()Landroid/net/DhcpInfo;

    move-result-object v7

    .line 413
    :cond_1
    iget-object v11, v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mContext:Landroid/content/Context;

    const-string v13, "SlaveWifiService"

    invoke-virtual {v11, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/net/wifi/SlaveWifiManager;

    .line 414
    .local v11, "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    if-eqz v11, :cond_3

    .line 415
    invoke-virtual {v11}, Landroid/net/wifi/SlaveWifiManager;->getWifiSlaveConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v10

    .line 416
    if-nez v10, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v10}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v12

    :goto_1
    move-object v5, v12

    .line 417
    invoke-virtual {v11}, Landroid/net/wifi/SlaveWifiManager;->getSlaveDhcpInfo()Landroid/net/DhcpInfo;

    move-result-object v8

    .line 421
    :cond_3
    if-eqz v4, :cond_11

    if-eqz v5, :cond_11

    if-eqz v6, :cond_11

    if-eqz v7, :cond_11

    if-nez v8, :cond_4

    move-object/from16 v16, v10

    move-object/from16 v17, v11

    goto/16 :goto_7

    .line 428
    :cond_4
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v13, v3

    move v3, v0

    .end local v0    # "wifi_support_miwill":Z
    .local v3, "wifi_support_miwill":Z
    .local v13, "slave_wifi_support_miwill":Z
    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    .line 429
    .local v0, "wifi_sr":Landroid/net/wifi/ScanResult;
    iget-object v14, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-direct {v1, v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->isMiWillRouter(Landroid/net/wifi/ScanResult;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 430
    const/4 v3, 0x1

    .line 432
    :cond_5
    iget-object v14, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    invoke-direct {v1, v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->isMiWillRouter(Landroid/net/wifi/ScanResult;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 433
    const/4 v13, 0x1

    .line 435
    .end local v0    # "wifi_sr":Landroid/net/wifi/ScanResult;
    :cond_6
    goto :goto_2

    .line 437
    :cond_7
    const-string v0, " "

    if-eqz v3, :cond_10

    if-nez v13, :cond_8

    move-object/from16 v16, v10

    move-object/from16 v17, v11

    goto/16 :goto_6

    .line 443
    :cond_8
    iget v12, v7, Landroid/net/DhcpInfo;->gateway:I

    invoke-static {v12}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    move-result-object v12

    invoke-virtual {v12}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v12

    iget v14, v8, Landroid/net/DhcpInfo;->gateway:I

    .line 444
    invoke-static {v14}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    move-result-object v14

    invoke-virtual {v14}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v14

    .line 443
    invoke-direct {v1, v12, v14}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->checkGateway(Ljava/lang/String;Ljava/lang/String;)Landroid/net/MacAddress;

    move-result-object v12

    .line 445
    .local v12, "gateway_mac":Landroid/net/MacAddress;
    if-nez v12, :cond_9

    .line 446
    const-string/jumbo v0, "setDualWifiReady turn off miwill gateway_mac is null"

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    invoke-direct/range {p0 .. p2}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->disableMiwill(ZI)V

    .line 448
    return-void

    .line 454
    :cond_9
    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getFrequency()I

    move-result v14

    invoke-static {v14}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->is24GHz(I)Z

    move-result v14

    if-eqz v14, :cond_a

    invoke-virtual {v10}, Landroid/net/wifi/WifiInfo;->getFrequency()I

    move-result v14

    invoke-static {v14}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->is5GHz(I)Z

    move-result v14

    if-nez v14, :cond_b

    .line 455
    :cond_a
    invoke-virtual {v10}, Landroid/net/wifi/WifiInfo;->getFrequency()I

    move-result v14

    invoke-static {v14}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->is24GHz(I)Z

    move-result v14

    if-eqz v14, :cond_f

    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getFrequency()I

    move-result v14

    invoke-static {v14}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->is5GHz(I)Z

    move-result v14

    if-eqz v14, :cond_f

    .line 456
    :cond_b
    const-string/jumbo v14, "setDualWifiReady all check ok!"

    invoke-static {v2, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 466
    .local v14, "cmd":Ljava/lang/StringBuilder;
    const-string/jumbo v15, "set"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 467
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 468
    move-object/from16 v16, v10

    .end local v10    # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    .local v16, "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    const-string v10, "miw_oem0"

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 469
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 470
    if-eqz p1, :cond_c

    const-string v15, "1"

    goto :goto_3

    :cond_c
    const-string v15, "0"

    :goto_3
    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 471
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v15, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mModeMap:Ljava/util/HashMap;

    move-object/from16 v17, v11

    .end local v11    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    .local v17, "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    iget v11, v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mModeKey:I

    .line 472
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v15, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 473
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 474
    invoke-virtual {v12}, Landroid/net/MacAddress;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 475
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 476
    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getFrequency()I

    move-result v11

    invoke-static {v11}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->is24GHz(I)Z

    move-result v11

    const-string/jumbo v15, "wlan1"

    const-string/jumbo v18, "wlan0"

    if-eqz v11, :cond_d

    move-object v11, v15

    goto :goto_4

    :cond_d
    move-object/from16 v11, v18

    :goto_4
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 477
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 478
    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getFrequency()I

    move-result v10

    invoke-static {v10}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->is24GHz(I)Z

    move-result v10

    if-eqz v10, :cond_e

    move-object/from16 v15, v18

    :cond_e
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 480
    :try_start_0
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mMiWillHal:Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v0, v10}, Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;->setMiwillParameter(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 484
    goto :goto_5

    .line 482
    :catch_0
    move-exception v0

    .line 483
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "catch ex:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v3    # "wifi_support_miwill":Z
    .end local v4    # "wifiBSSID":Ljava/lang/String;
    .end local v5    # "slavewifiBSSID":Ljava/lang/String;
    .end local v6    # "wifi_srs":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    .end local v7    # "dhcpInfo":Landroid/net/DhcpInfo;
    .end local v8    # "slaveDhcpInfo":Landroid/net/DhcpInfo;
    .end local v9    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v12    # "gateway_mac":Landroid/net/MacAddress;
    .end local v13    # "slave_wifi_support_miwill":Z
    .end local v14    # "cmd":Ljava/lang/StringBuilder;
    .end local v16    # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v17    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    :goto_5
    goto/16 :goto_d

    .line 455
    .restart local v3    # "wifi_support_miwill":Z
    .restart local v4    # "wifiBSSID":Ljava/lang/String;
    .restart local v5    # "slavewifiBSSID":Ljava/lang/String;
    .restart local v6    # "wifi_srs":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    .restart local v7    # "dhcpInfo":Landroid/net/DhcpInfo;
    .restart local v8    # "slaveDhcpInfo":Landroid/net/DhcpInfo;
    .restart local v9    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .restart local v10    # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    .restart local v11    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    .restart local v12    # "gateway_mac":Landroid/net/MacAddress;
    .restart local v13    # "slave_wifi_support_miwill":Z
    :cond_f
    move-object/from16 v16, v10

    move-object/from16 v17, v11

    .line 459
    .end local v10    # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v11    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    .restart local v16    # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    .restart local v17    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    const-string/jumbo v0, "setDualWifiReady turn off miwill found 6G!?"

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    invoke-direct/range {p0 .. p2}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->disableMiwill(ZI)V

    .line 461
    return-void

    .line 437
    .end local v12    # "gateway_mac":Landroid/net/MacAddress;
    .end local v16    # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v17    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    .restart local v10    # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    .restart local v11    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    :cond_10
    move-object/from16 v16, v10

    move-object/from16 v17, v11

    .line 438
    .end local v10    # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v11    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    .restart local v16    # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    .restart local v17    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    :goto_6
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "setDualWifiReady turn off miwill: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    invoke-direct/range {p0 .. p2}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->disableMiwill(ZI)V

    .line 440
    return-void

    .line 421
    .end local v13    # "slave_wifi_support_miwill":Z
    .end local v16    # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v17    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    .local v0, "wifi_support_miwill":Z
    .local v3, "slave_wifi_support_miwill":Z
    .restart local v10    # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    .restart local v11    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    :cond_11
    move-object/from16 v16, v10

    move-object/from16 v17, v11

    .line 422
    .end local v10    # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v11    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    .restart local v16    # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    .restart local v17    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    :goto_7
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "setDualWifiReady turn off miwill1: wifiBSSID:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x1

    const/4 v12, 0x0

    if-nez v4, :cond_12

    move v13, v11

    goto :goto_8

    :cond_12
    move v13, v12

    :goto_8
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v13, " slavewifiBSSID:"

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-nez v5, :cond_13

    move v13, v11

    goto :goto_9

    :cond_13
    move v13, v12

    :goto_9
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v13, " wifi_srs:"

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-nez v6, :cond_14

    move v13, v11

    goto :goto_a

    :cond_14
    move v13, v12

    :goto_a
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v13, " dhcpInfo:"

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-nez v7, :cond_15

    move v13, v11

    goto :goto_b

    :cond_15
    move v13, v12

    :goto_b
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v13, " slaveDhcpInfo:"

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-nez v8, :cond_16

    goto :goto_c

    :cond_16
    move v11, v12

    :goto_c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    invoke-direct/range {p0 .. p2}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->disableMiwill(ZI)V

    .line 425
    return-void

    .line 487
    .end local v0    # "wifi_support_miwill":Z
    .end local v3    # "slave_wifi_support_miwill":Z
    .end local v4    # "wifiBSSID":Ljava/lang/String;
    .end local v5    # "slavewifiBSSID":Ljava/lang/String;
    .end local v6    # "wifi_srs":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    .end local v7    # "dhcpInfo":Landroid/net/DhcpInfo;
    .end local v8    # "slaveDhcpInfo":Landroid/net/DhcpInfo;
    .end local v9    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v16    # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v17    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    :cond_17
    const-string/jumbo v0, "setDualWifiReady miwill hal service lost"

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    :goto_d
    return-void
.end method

.method private unregisterSwitchObserver()V
    .locals 2

    .line 300
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mSettingsObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 302
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 304
    :cond_0
    return-void
.end method

.method private updateStatus()V
    .locals 4

    .line 312
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mMiWillHal:Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

    const-string v1, "MIWILL-MiWillManager"

    if-eqz v0, :cond_0

    .line 314
    :try_start_0
    invoke-interface {v0}, Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;->startMiwild()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 316
    :catch_0
    move-exception v0

    .line 317
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateStatus catch:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    goto :goto_1

    .line 321
    :cond_0
    const-string/jumbo v0, "updateStatus mMiWillHal is null"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    :goto_1
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 5

    .line 187
    const-string v0, "onCreate"

    const-string v1, "MIWILL-MiWillManager"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "wifi"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 192
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "MiWillWorkHandler"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mThread:Landroid/os/HandlerThread;

    .line 193
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 194
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$MiWillHandler;

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$MiWillHandler;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mHandler:Landroid/os/Handler;

    .line 197
    :try_start_0
    invoke-static {}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager;->getService()Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager;

    move-result-object v0

    const-string/jumbo v2, "vendor.xiaomi.hidl.miwill@1.0::IMiwillService"

    const-string v3, "default"

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mNotification:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;

    invoke-interface {v0, v2, v3, v4}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager;->registerForNotifications(Ljava/lang/String;Ljava/lang/String;Lmiui/android/services/internal/hidl/manager/V1_0/IServiceNotification;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    goto :goto_0

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "onCreate catch"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 218
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 221
    const-string v0, "onDestroy"

    const-string v1, "MIWILL-MiWillManager"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    const-string v0, "disable miwill because miwill manager destroying."

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->disableMiwill(ZI)V

    .line 232
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mThread:Landroid/os/HandlerThread;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 234
    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mThread:Landroid/os/HandlerThread;

    .line 236
    :cond_0
    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mHandler:Landroid/os/Handler;

    .line 237
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->deinitMiWillHal()V

    .line 238
    return-void
.end method

.method public setDualWifiReady(Z)V
    .locals 3
    .param p1, "ready"    # Z

    .line 373
    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->isDualWifiReady:Z

    .line 374
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 375
    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 376
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x67

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 377
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mHandler:Landroid/os/Handler;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 379
    :cond_0
    return-void
.end method

.method public setMiWillGameStart(Ljava/lang/String;)Z
    .locals 5
    .param p1, "uid"    # Ljava/lang/String;

    .line 516
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->checkMiWillIp()Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "MIWILL-MiWillManager"

    if-nez v0, :cond_0

    .line 517
    const-string/jumbo v0, "setMiWillGameStart miwill ip not found."

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    return v1

    .line 520
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mMiWillHal:Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

    if-eqz v0, :cond_1

    .line 522
    :try_start_0
    invoke-interface {v0, p1}, Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;->setMiwillAppUidList(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 527
    nop

    .line 528
    const/4 v0, 0x1

    return v0

    .line 524
    :catch_0
    move-exception v0

    .line 525
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setMiWillGameStart catch:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    return v1

    .line 531
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const-string/jumbo v0, "setMiWillGameStart miwill hal service lost"

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    return v1
.end method

.method public setMiWillGameStop(Ljava/lang/String;)Z
    .locals 4
    .param p1, "uid"    # Ljava/lang/String;

    .line 537
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mMiWillHal:Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;

    const-string v1, "MIWILL-MiWillManager"

    if-eqz v0, :cond_0

    .line 539
    :try_start_0
    const-string v2, "0"

    invoke-interface {v0, v2}, Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;->setMiwillAppUidList(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 543
    goto :goto_0

    .line 541
    :catch_0
    move-exception v0

    .line 542
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setMiWillGameStop catch:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 547
    :cond_0
    const-string/jumbo v0, "setMiWillGameStop miwill hal service lost"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    const/4 v0, 0x0

    return v0
.end method
