class com.xiaomi.NetworkBoost.slaservice.SLAAppLib$3 extends android.database.ContentObserver {
	 /* .source "SLAAppLib.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLACloudObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.SLAAppLib this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.slaservice.SLAAppLib$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 1052 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 7 */
/* .param p1, "selfChange" # Z */
/* .line 1056 */
try { // :try_start_0
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmContext ( v0 );
	 (( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
	 /* .line 1057 */
	 /* .local v0, "pm":Landroid/content/pm/PackageManager; */
	 int v1 = 1; // const/4 v1, 0x1
	 (( android.content.pm.PackageManager ) v0 ).getInstalledPackages ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;
	 /* .line 1059 */
	 /* .local v1, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
	 v2 = android.os.Build.DEVICE;
	 (( java.lang.String ) v2 ).toLowerCase ( ); // invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
	 /* .line 1060 */
	 /* .local v2, "device":Ljava/lang/String; */
	 v3 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLAAppDefaultPN ( v3 );
	 /* .line 1062 */
	 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$sfgetmAppLists ( );
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 /* .line 1063 */
		 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$sfgetmSLAAppLibLock ( );
		 /* monitor-enter v3 */
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 1064 */
		 int v4 = 0; // const/4 v4, 0x0
		 /* .local v4, "i":I */
	 } // :goto_0
	 try { // :try_start_1
		 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$sfgetmAppLists ( );
		 v5 = 		 (( java.util.ArrayList ) v5 ).size ( ); // invoke-virtual {v5}, Ljava/util/ArrayList;->size()I
		 /* if-ge v4, v5, :cond_0 */
		 /* .line 1065 */
		 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$sfgetmAppLists ( );
		 (( java.util.ArrayList ) v5 ).get ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
		 /* check-cast v5, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
		 int v6 = 0; // const/4 v6, 0x0
		 (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v5 ).setState ( v6 ); // invoke-virtual {v5, v6}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setState(Z)V
		 /* .line 1066 */
		 v5 = this.this$0;
		 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$sfgetmAppLists ( );
		 (( java.util.ArrayList ) v6 ).get ( v4 ); // invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
		 /* check-cast v6, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
		 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$mdbUpdateSLAApp ( v5,v6 );
		 /* .line 1064 */
		 /* add-int/lit8 v4, v4, 0x1 */
		 /* .line 1068 */
	 } // .end local v4 # "i":I
} // :cond_0
/* monitor-exit v3 */
/* :catchall_0 */
/* move-exception v4 */
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3;
} // .end local p1 # "selfChange":Z
try { // :try_start_2
/* throw v4 */
/* .line 1071 */
/* .restart local p0 # "this":Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3; */
/* .restart local p1 # "selfChange":Z */
} // :cond_1
} // :goto_1
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$minitSLAAppDefault ( v3 );
/* .line 1073 */
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Landroid/content/pm/PackageInfo; */
/* .line 1074 */
/* .local v4, "app":Landroid/content/pm/PackageInfo; */
v5 = this.packageName;
if ( v5 != null) { // if-eqz v5, :cond_2
v5 = this.applicationInfo;
if ( v5 != null) { // if-eqz v5, :cond_2
v5 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLAAppDefaultPN ( v5 );
v6 = this.packageName;
v5 = /* .line 1075 */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1076 */
v5 = this.applicationInfo;
/* iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 1077 */
/* .local v5, "uid":I */
v6 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$maddSLAAppDefault ( v6,v5 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 1079 */
} // .end local v4 # "app":Landroid/content/pm/PackageInfo;
} // .end local v5 # "uid":I
} // :cond_2
/* .line 1082 */
} // .end local v0 # "pm":Landroid/content/pm/PackageManager;
} // .end local v1 # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
} // .end local v2 # "device":Ljava/lang/String;
} // :cond_3
/* .line 1080 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1081 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "initSLACloudObserver onChange error "; // const-string v3, "initSLACloudObserver onChange error "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 1083 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_3
return;
} // .end method
