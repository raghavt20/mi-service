class com.xiaomi.NetworkBoost.slaservice.SLAService$7 implements com.xiaomi.NetworkBoost.StatusManager$IModemSignalStrengthListener {
	 /* .source "SLAService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.SLAService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.slaservice.SLAService$7 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* .line 1357 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void isModemSingnalStrengthStatus ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "status" # I */
/* .line 1364 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "isModemSingnalStrengthStatus = "; // const-string v1, "isModemSingnalStrengthStatus = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLAService"; // const-string v1, "SLM-SRV-SLAService"
android.util.Log .d ( v1,v0 );
/* .line 1365 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_0 */
v2 = this.this$0;
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmSingnalPoor ( v2 );
/* if-nez v2, :cond_0 */
/* .line 1367 */
final String v2 = "isModemSingnalStrengthStatus setMobileDataAlwaysOff"; // const-string v2, "isModemSingnalStrengthStatus setMobileDataAlwaysOff"
android.util.Log .i ( v1,v2 );
/* .line 1368 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fputmSingnalPoor ( v1,v0 );
/* .line 1369 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$msetMobileDataAlwaysOff ( v0 );
/* .line 1370 */
} // :cond_0
/* if-nez p1, :cond_1 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmLinkTurboSwitch ( );
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetdisableSLMCnt ( v0 );
/* if-nez v0, :cond_1 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmDataReady ( );
/* if-nez v0, :cond_1 */
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmSingnalPoor ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 1373 */
	 final String v0 = "isModemSingnalStrengthStatus setMobileDataAlwaysOn"; // const-string v0, "isModemSingnalStrengthStatus setMobileDataAlwaysOn"
	 android.util.Log .i ( v1,v0 );
	 /* .line 1374 */
	 v0 = this.this$0;
	 int v1 = 0; // const/4 v1, 0x0
	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fputmSingnalPoor ( v0,v1 );
	 /* .line 1375 */
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$msetMobileDataAlwaysOn ( v0 );
	 /* .line 1377 */
} // :cond_1
} // :goto_0
return;
} // .end method
