public class com.xiaomi.NetworkBoost.slaservice.MiWillManager {
	 /* .source "MiWillManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;, */
	 /* Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$MiWillHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_ENABLE_MIWILL;
private static final Long COOKIE_UPBOUND;
private static final Boolean DBG;
private static final Boolean DEBUG_COMMAND;
private static final java.lang.String INTERFACE_NAME;
private static final Integer MIWILL_DISABLED;
private static final Integer MIWILL_ENABLED;
private static final MIWILL_TYPE;
private static final Integer RECHECK_MAX_TIMES;
private static final java.lang.String REDUDANCY_MODE;
public static final Integer REDUDANCY_MODE_KEY;
private static final java.lang.String TAG;
private static final Integer WAIT_RECHECK_DELAY_MILLIS;
private static final java.lang.String WIFI_MIWILL_ENABLE;
private static final java.lang.String WPS_DEVICE_XIAOMI;
private static final XIAOMI_OUI;
private static final java.util.HashMap mModeMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static volatile com.xiaomi.NetworkBoost.slaservice.MiWillManager sInstance;
/* # instance fields */
private Boolean isDualWifiReady;
private android.content.Context mContext;
private Long mCookie;
private android.os.IHwBinder$DeathRecipient mDeathRecipient;
private android.os.Handler mHandler;
private vendor.xiaomi.hidl.miwill.V1_0.IMiwillService mMiWillHal;
private Integer mModeKey;
private final com.xiaomi.NetworkBoost.slaservice.MiWillManager$Notification mNotification;
private android.database.ContentObserver mSettingsObserver;
private android.os.HandlerThread mThread;
private android.net.wifi.WifiManager mWifiManager;
/* # direct methods */
static Long -$$Nest$fgetmCookie ( com.xiaomi.NetworkBoost.slaservice.MiWillManager p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mCookie:J */
/* return-wide v0 */
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.xiaomi.NetworkBoost.slaservice.MiWillManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static void -$$Nest$fputmMiWillHal ( com.xiaomi.NetworkBoost.slaservice.MiWillManager p0, vendor.xiaomi.hidl.miwill.V1_0.IMiwillService p1 ) { //bridge//synthethic
/* .locals 0 */
this.mMiWillHal = p1;
return;
} // .end method
static void -$$Nest$minitMiWillHal ( com.xiaomi.NetworkBoost.slaservice.MiWillManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->initMiWillHal()V */
return;
} // .end method
static void -$$Nest$msetMiwillStatus ( com.xiaomi.NetworkBoost.slaservice.MiWillManager p0, Boolean p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->setMiwillStatus(ZI)V */
return;
} // .end method
static void -$$Nest$mupdateStatus ( com.xiaomi.NetworkBoost.slaservice.MiWillManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->updateStatus()V */
return;
} // .end method
static com.xiaomi.NetworkBoost.slaservice.MiWillManager ( ) {
/* .locals 3 */
/* .line 88 */
int v0 = 3; // const/4 v0, 0x3
/* new-array v0, v0, [B */
/* fill-array-data v0, :array_0 */
/* .line 89 */
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [B */
/* fill-array-data v0, :array_1 */
/* .line 94 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 96 */
int v1 = 1; // const/4 v1, 0x1
java.lang.Integer .valueOf ( v1 );
final String v2 = "redudancy"; // const-string v2, "redudancy"
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 97 */
return;
/* :array_0 */
/* .array-data 1 */
/* -0x74t */
/* -0x42t */
/* -0x42t */
} // .end array-data
/* :array_1 */
/* .array-data 1 */
/* 0x10t */
/* 0x34t */
} // .end array-data
} // .end method
private com.xiaomi.NetworkBoost.slaservice.MiWillManager ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 182 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 100 */
int v0 = 0; // const/4 v0, 0x0
this.mSettingsObserver = v0;
/* .line 101 */
this.mWifiManager = v0;
/* .line 102 */
this.mHandler = v0;
/* .line 103 */
this.mThread = v0;
/* .line 104 */
this.mMiWillHal = v0;
/* .line 105 */
this.mContext = v0;
/* .line 106 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mCookie:J */
/* .line 107 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->isDualWifiReady:Z */
/* .line 108 */
int v1 = 1; // const/4 v1, 0x1
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mModeKey:I */
/* .line 110 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$1; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$1;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)V */
this.mDeathRecipient = v1;
/* .line 120 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification; */
/* invoke-direct {v1, p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification-IA;)V */
this.mNotification = v1;
/* .line 183 */
(( android.content.Context ) p1 ).getApplicationContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
this.mContext = v0;
/* .line 184 */
return;
} // .end method
private android.net.MacAddress checkGateway ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 20 */
/* .param p1, "wifi_gateway" # Ljava/lang/String; */
/* .param p2, "slave_wifi_gateway" # Ljava/lang/String; */
/* .line 621 */
/* move-object/from16 v1, p1 */
final String v2 = ""; // const-string v2, ""
final String v3 = "MIWILL-MiWillManager"; // const-string v3, "MIWILL-MiWillManager"
v0 = /* invoke-virtual/range {p1 ..p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z */
int v4 = 0; // const/4 v4, 0x0
/* if-nez v0, :cond_0 */
/* .line 622 */
/* .line 624 */
} // :cond_0
int v5 = 0; // const/4 v5, 0x0
/* .line 626 */
/* .local v5, "fd":Ljava/io/FileDescriptor; */
try { // :try_start_0
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkSocket .forProto ( v0 );
/* move-object v5, v0 */
/* .line 628 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkSocket .connectToKernel ( v5 );
/* .line 630 */
android.system.Os .getsockname ( v5 );
/* check-cast v0, Landroid/system/NetlinkSocketAddress; */
/* .line 632 */
/* .local v0, "localAddr":Landroid/system/NetlinkSocketAddress; */
int v12 = 1; // const/4 v12, 0x1
/* .line 633 */
/* .local v12, "TEST_SEQNO":I */
int v6 = 1; // const/4 v6, 0x1
com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage .newGetNeighborsRequest ( v6 );
/* move-object v13, v6 */
/* .line 635 */
/* .local v13, "req":[B */
/* const-wide/16 v14, 0x1f4 */
/* .line 636 */
/* .local v14, "TIMEOUT":J */
int v8 = 0; // const/4 v8, 0x0
/* array-length v9, v13 */
/* const-wide/16 v10, 0x1f4 */
/* move-object v6, v5 */
/* move-object v7, v13 */
v6 = /* invoke-static/range {v6 ..v11}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkSocket;->sendMessage(Ljava/io/FileDescriptor;[BIIJ)I */
/* .line 637 */
/* .local v6, "len":I */
/* array-length v7, v13 */
/* if-ne v7, v6, :cond_1 */
/* .line 638 */
final String v7 = "req.length == req.length"; // const-string v7, "req.length == req.length"
android.util.Log .i ( v3,v7 );
/* .line 642 */
} // :cond_1
int v7 = 0; // const/4 v7, 0x0
/* .line 644 */
/* .local v7, "doneMessageCount":I */
int v8 = 0; // const/4 v8, 0x0
/* .line 645 */
/* .local v8, "wifi_gateway_mac":Landroid/net/MacAddress; */
int v9 = 0; // const/4 v9, 0x0
/* .line 647 */
/* .local v9, "slave_wifi_gateway_mac":Landroid/net/MacAddress; */
} // :goto_0
/* if-nez v7, :cond_8 */
/* .line 648 */
/* const/16 v10, 0x2000 */
/* move v11, v6 */
/* move/from16 v16, v7 */
} // .end local v6 # "len":I
} // .end local v7 # "doneMessageCount":I
/* .local v11, "len":I */
/* .local v16, "doneMessageCount":I */
/* const-wide/16 v6, 0x1f4 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkSocket .recvMessage ( v5,v10,v6,v7 );
/* move/from16 v7, v16 */
/* .line 651 */
} // .end local v16 # "doneMessageCount":I
/* .local v6, "response":Ljava/nio/ByteBuffer; */
/* .restart local v7 # "doneMessageCount":I */
} // :goto_1
v10 = (( java.nio.ByteBuffer ) v6 ).remaining ( ); // invoke-virtual {v6}, Ljava/nio/ByteBuffer;->remaining()I
/* if-lez v10, :cond_7 */
/* .line 652 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage .parse ( v6,v10 );
/* .line 653 */
/* .local v10, "nlMsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage; */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage ) v10 ).getHeader ( ); // invoke-virtual {v10}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;->getHeader()Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
/* move-object/from16 v17, v16 */
/* .line 655 */
/* .local v17, "hdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
/* move-object/from16 v4, v17 */
/* move-object/from16 v17, v0 */
} // .end local v0 # "localAddr":Landroid/system/NetlinkSocketAddress;
/* .local v4, "hdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
/* .local v17, "localAddr":Landroid/system/NetlinkSocketAddress; */
/* iget-short v0, v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
/* move-object/from16 v18, v6 */
} // .end local v6 # "response":Ljava/nio/ByteBuffer;
/* .local v18, "response":Ljava/nio/ByteBuffer; */
int v6 = 3; // const/4 v6, 0x3
/* if-ne v0, v6, :cond_2 */
/* .line 656 */
/* add-int/lit8 v7, v7, 0x1 */
/* .line 657 */
/* move-object/from16 v0, v17 */
/* move-object/from16 v6, v18 */
int v4 = 0; // const/4 v4, 0x0
/* .line 660 */
} // :cond_2
/* iget-short v0, v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
/* const/16 v6, 0x1c */
/* if-ne v6, v0, :cond_5 */
/* .line 661 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "NetlinkConstants.RTM_NEWNEIGH == hdr.nlmsg_type"; // const-string v6, "NetlinkConstants.RTM_NEWNEIGH == hdr.nlmsg_type"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage ) v10 ).toString ( ); // invoke-virtual {v10}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v0 );
/* .line 662 */
/* move-object v0, v10 */
/* check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage; */
/* .line 663 */
/* .local v0, "neighMsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v16, v4 */
} // .end local v4 # "hdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
/* .local v16, "hdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
final String v4 = "macaddress "; // const-string v4, "macaddress "
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage ) v0 ).getLinkLayerAddress ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->getLinkLayerAddress()[B
com.xiaomi.NetworkBoost.slaservice.MiWillManager .getMacAddress ( v6 );
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v4 );
/* .line 664 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage ) v0 ).getDestination ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->getDestination()Ljava/net/InetAddress;
/* if-nez v4, :cond_3 */
/* move-object v4, v2 */
} // :cond_3
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage ) v0 ).getDestination ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->getDestination()Ljava/net/InetAddress;
(( java.net.InetAddress ) v4 ).getHostAddress ( ); // invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
/* .line 665 */
/* .local v4, "ipLiteral":Ljava/lang/String; */
} // :goto_2
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* move/from16 v19, v7 */
} // .end local v7 # "doneMessageCount":I
/* .local v19, "doneMessageCount":I */
final String v7 = "ip address "; // const-string v7, "ip address "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v6 );
/* .line 666 */
v6 = (( java.lang.String ) v4 ).equals ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 667 */
/* if-nez v8, :cond_4 */
/* .line 668 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage ) v0 ).getLinkLayerAddress ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->getLinkLayerAddress()[B
com.xiaomi.NetworkBoost.slaservice.MiWillManager .getMacAddress ( v6 );
/* move-object v8, v6 */
/* .line 670 */
} // :cond_4
/* if-nez v9, :cond_6 */
/* .line 671 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage ) v0 ).getLinkLayerAddress ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->getLinkLayerAddress()[B
com.xiaomi.NetworkBoost.slaservice.MiWillManager .getMacAddress ( v6 );
/* move-object v9, v6 */
/* .line 660 */
} // .end local v0 # "neighMsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;
} // .end local v16 # "hdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
} // .end local v19 # "doneMessageCount":I
/* .local v4, "hdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
/* .restart local v7 # "doneMessageCount":I */
} // :cond_5
/* move-object/from16 v16, v4 */
/* move/from16 v19, v7 */
/* .line 675 */
} // .end local v4 # "hdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
} // .end local v7 # "doneMessageCount":I
} // .end local v10 # "nlMsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;
/* .restart local v19 # "doneMessageCount":I */
} // :cond_6
} // :goto_3
/* move-object/from16 v0, v17 */
/* move-object/from16 v6, v18 */
/* move/from16 v7, v19 */
int v4 = 0; // const/4 v4, 0x0
/* goto/16 :goto_1 */
/* .line 651 */
} // .end local v17 # "localAddr":Landroid/system/NetlinkSocketAddress;
} // .end local v18 # "response":Ljava/nio/ByteBuffer;
} // .end local v19 # "doneMessageCount":I
/* .local v0, "localAddr":Landroid/system/NetlinkSocketAddress; */
/* .restart local v6 # "response":Ljava/nio/ByteBuffer; */
/* .restart local v7 # "doneMessageCount":I */
} // :cond_7
/* move-object/from16 v17, v0 */
/* move-object/from16 v18, v6 */
/* move/from16 v19, v7 */
/* .line 676 */
} // .end local v0 # "localAddr":Landroid/system/NetlinkSocketAddress;
} // .end local v6 # "response":Ljava/nio/ByteBuffer;
} // .end local v7 # "doneMessageCount":I
/* .restart local v17 # "localAddr":Landroid/system/NetlinkSocketAddress; */
/* .restart local v19 # "doneMessageCount":I */
/* move v6, v11 */
int v4 = 0; // const/4 v4, 0x0
/* goto/16 :goto_0 */
/* .line 677 */
} // .end local v11 # "len":I
} // .end local v17 # "localAddr":Landroid/system/NetlinkSocketAddress;
} // .end local v19 # "doneMessageCount":I
/* .restart local v0 # "localAddr":Landroid/system/NetlinkSocketAddress; */
/* .local v6, "len":I */
/* .restart local v7 # "doneMessageCount":I */
} // :cond_8
/* move-object/from16 v17, v0 */
/* move v11, v6 */
/* move/from16 v16, v7 */
} // .end local v0 # "localAddr":Landroid/system/NetlinkSocketAddress;
} // .end local v6 # "len":I
} // .end local v7 # "doneMessageCount":I
/* .restart local v11 # "len":I */
/* .local v16, "doneMessageCount":I */
/* .restart local v17 # "localAddr":Landroid/system/NetlinkSocketAddress; */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "wifi_gateway_mac:" */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " slave_wifi_gateway_mac:"; // const-string v4, " slave_wifi_gateway_mac:"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v0 );
/* .line 678 */
if ( v8 != null) { // if-eqz v8, :cond_9
v0 = (( android.net.MacAddress ) v8 ).equals ( v9 ); // invoke-virtual {v8, v9}, Landroid/net/MacAddress;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 679 */
/* nop */
/* .line 686 */
libcore.io.IoUtils .closeQuietly ( v5 );
/* .line 679 */
/* .line 686 */
} // .end local v8 # "wifi_gateway_mac":Landroid/net/MacAddress;
} // .end local v9 # "slave_wifi_gateway_mac":Landroid/net/MacAddress;
} // .end local v11 # "len":I
} // .end local v12 # "TEST_SEQNO":I
} // .end local v13 # "req":[B
} // .end local v14 # "TIMEOUT":J
} // .end local v16 # "doneMessageCount":I
} // .end local v17 # "localAddr":Landroid/system/NetlinkSocketAddress;
} // :cond_9
/* :catchall_0 */
/* move-exception v0 */
/* move-object/from16 v2, p2 */
/* .line 682 */
/* :catch_0 */
/* move-exception v0 */
/* .line 683 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
android.util.Log .e ( v3,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 686 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_4
libcore.io.IoUtils .closeQuietly ( v5 );
/* .line 687 */
/* nop */
/* .line 688 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "wifi_gateway:" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " slave_wifi_gateway:"; // const-string v2, " slave_wifi_gateway:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v2, p2 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v0 );
/* .line 689 */
int v3 = 0; // const/4 v3, 0x0
/* .line 686 */
} // :goto_5
libcore.io.IoUtils .closeQuietly ( v5 );
/* .line 687 */
/* throw v0 */
} // .end method
private Boolean checkMiWillIp ( ) {
/* .locals 8 */
/* .line 493 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
java.net.NetworkInterface .getNetworkInterfaces ( );
/* .line 494 */
/* .local v1, "net":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 495 */
/* check-cast v2, Ljava/net/NetworkInterface; */
/* .line 496 */
/* .local v2, "networkInterface":Ljava/net/NetworkInterface; */
(( java.net.NetworkInterface ) v2 ).getInetAddresses ( ); // invoke-virtual {v2}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;
/* .line 497 */
/* .local v3, "add":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;" */
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 498 */
/* check-cast v4, Ljava/net/InetAddress; */
/* .line 499 */
/* .local v4, "a":Ljava/net/InetAddress; */
v5 = (( java.net.InetAddress ) v4 ).isLoopbackAddress ( ); // invoke-virtual {v4}, Ljava/net/InetAddress;->isLoopbackAddress()Z
/* if-nez v5, :cond_0 */
(( java.net.InetAddress ) v4 ).getHostAddress ( ); // invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
final String v6 = ":"; // const-string v6, ":"
v5 = (( java.lang.String ) v5 ).contains ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v5, :cond_0 */
(( java.net.InetAddress ) v4 ).getHostAddress ( ); // invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
final String v6 = "192.168."; // const-string v6, "192.168."
v5 = (( java.lang.String ) v5 ).contains ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 500 */
(( java.net.NetworkInterface ) v2 ).getDisplayName ( ); // invoke-virtual {v2}, Ljava/net/NetworkInterface;->getDisplayName()Ljava/lang/String;
/* .line 501 */
/* .local v5, "netInterface":Ljava/lang/String; */
if ( v5 != null) { // if-eqz v5, :cond_0
final String v6 = "miw_oem0"; // const-string v6, "miw_oem0"
v6 = (( java.lang.String ) v5 ).indexOf ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
int v7 = -1; // const/4 v7, -0x1
/* if-eq v6, v7, :cond_0 */
/* .line 502 */
v6 = com.xiaomi.NetworkBoost.StatusManager .isTetheredIfaces ( v5 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-nez v6, :cond_0 */
/* .line 503 */
int v0 = 1; // const/4 v0, 0x1
/* .line 506 */
} // .end local v4 # "a":Ljava/net/InetAddress;
} // .end local v5 # "netInterface":Ljava/lang/String;
} // :cond_0
/* .line 507 */
} // .end local v2 # "networkInterface":Ljava/net/NetworkInterface;
} // .end local v3 # "add":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
} // :cond_1
/* .line 511 */
} // .end local v1 # "net":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
} // :cond_2
/* nop */
/* .line 512 */
/* .line 508 */
/* :catch_0 */
/* move-exception v1 */
/* .line 509 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "checkMiWillIp Exception:"; // const-string v3, "checkMiWillIp Exception:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MIWILL-MiWillManager"; // const-string v3, "MIWILL-MiWillManager"
android.util.Log .e ( v3,v2 );
/* .line 510 */
} // .end method
private void deinitMiWillHal ( ) {
/* .locals 2 */
/* .line 264 */
v0 = this.mMiWillHal;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 266 */
try { // :try_start_0
v1 = this.mDeathRecipient;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 270 */
/* .line 268 */
/* :catch_0 */
/* move-exception v0 */
/* .line 271 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
this.mMiWillHal = v0;
/* .line 273 */
} // :cond_0
return;
} // .end method
public static void destroyInstance ( ) {
/* .locals 4 */
/* .line 167 */
v0 = com.xiaomi.NetworkBoost.slaservice.MiWillManager.sInstance;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 168 */
/* const-class v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager; */
/* monitor-enter v0 */
/* .line 169 */
try { // :try_start_0
v1 = com.xiaomi.NetworkBoost.slaservice.MiWillManager.sInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 171 */
try { // :try_start_1
v1 = com.xiaomi.NetworkBoost.slaservice.MiWillManager.sInstance;
(( com.xiaomi.NetworkBoost.slaservice.MiWillManager ) v1 ).onDestroy ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->onDestroy()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 175 */
/* .line 173 */
/* :catch_0 */
/* move-exception v1 */
/* .line 174 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v2 = "MIWILL-MiWillManager"; // const-string v2, "MIWILL-MiWillManager"
final String v3 = "destroyInstance onDestroy catch:"; // const-string v3, "destroyInstance onDestroy catch:"
android.util.Log .e ( v2,v3,v1 );
/* .line 176 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
/* .line 178 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 180 */
} // :cond_1
} // :goto_1
return;
} // .end method
private void disableMiwill ( Boolean p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "ready" # Z */
/* .param p2, "retry_cnt" # I */
/* .line 383 */
try { // :try_start_0
v0 = this.mMiWillHal;
/* const-string/jumbo v1, "set miw_oem0 0" */
/* .line 384 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* if-lez p2, :cond_0 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 385 */
java.lang.Integer .valueOf ( p2 );
/* const/16 v2, 0x67 */
(( android.os.Handler ) v0 ).obtainMessage ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* const-wide/16 v2, 0x2710 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 390 */
} // :cond_0
/* .line 388 */
/* :catch_0 */
/* move-exception v0 */
/* .line 389 */
/* .local v0, "e1":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "disableMiwill catch e = "; // const-string v2, "disableMiwill catch e = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MIWILL-MiWillManager"; // const-string v2, "MIWILL-MiWillManager"
android.util.Log .e ( v2,v1 );
/* .line 391 */
} // .end local v0 # "e1":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.MiWillManager getInstance ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 150 */
v0 = com.xiaomi.NetworkBoost.slaservice.MiWillManager.sInstance;
/* if-nez v0, :cond_1 */
/* .line 151 */
/* const-class v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager; */
/* monitor-enter v0 */
/* .line 152 */
try { // :try_start_0
v1 = com.xiaomi.NetworkBoost.slaservice.MiWillManager.sInstance;
/* if-nez v1, :cond_0 */
/* .line 153 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;-><init>(Landroid/content/Context;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 155 */
try { // :try_start_1
v1 = com.xiaomi.NetworkBoost.slaservice.MiWillManager.sInstance;
(( com.xiaomi.NetworkBoost.slaservice.MiWillManager ) v1 ).onCreate ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->onCreate()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 159 */
/* .line 157 */
/* :catch_0 */
/* move-exception v1 */
/* .line 158 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v2 = "MIWILL-MiWillManager"; // const-string v2, "MIWILL-MiWillManager"
final String v3 = "getInstance onCreate catch:"; // const-string v3, "getInstance onCreate catch:"
android.util.Log .e ( v2,v3,v1 );
/* .line 161 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 163 */
} // :cond_1
} // :goto_1
v0 = com.xiaomi.NetworkBoost.slaservice.MiWillManager.sInstance;
} // .end method
private static android.net.MacAddress getMacAddress ( Object[] p0 ) {
/* .locals 3 */
/* .param p0, "linkLayerAddress" # [B */
/* .line 609 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 611 */
try { // :try_start_0
android.net.MacAddress .fromBytes ( p0 );
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 612 */
/* :catch_0 */
/* move-exception v0 */
/* .line 613 */
/* .local v0, "e":Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to parse link-layer address: "; // const-string v2, "Failed to parse link-layer address: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MIWILL-MiWillManager"; // const-string v2, "MIWILL-MiWillManager"
android.util.Log .e ( v2,v1 );
/* .line 617 */
} // .end local v0 # "e":Ljava/lang/IllegalArgumentException;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void initMiWillHal ( ) {
/* .locals 6 */
/* .line 241 */
v0 = this.mMiWillHal;
final String v1 = "MIWILL-MiWillManager"; // const-string v1, "MIWILL-MiWillManager"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 242 */
final String v0 = "initMiWillHal mMiWillHal has already been inited."; // const-string v0, "initMiWillHal mMiWillHal has already been inited."
android.util.Log .w ( v1,v0 );
/* .line 243 */
return;
/* .line 247 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
vendor.xiaomi.hidl.miwill.V1_0.IMiwillService .getService ( v0 );
this.mMiWillHal = v0;
/* .line 248 */
/* if-nez v0, :cond_1 */
/* .line 249 */
final String v0 = "MiWill hal get failed"; // const-string v0, "MiWill hal get failed"
android.util.Log .e ( v1,v0 );
/* .line 252 */
} // :cond_1
final String v0 = "MiWill hal get success"; // const-string v0, "MiWill hal get success"
android.util.Log .d ( v1,v0 );
/* .line 253 */
/* iget-wide v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mCookie:J */
/* const-wide/16 v4, 0x1 */
/* add-long/2addr v2, v4 */
/* const-wide/16 v4, 0x64 */
/* rem-long/2addr v2, v4 */
/* iput-wide v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mCookie:J */
/* .line 254 */
v0 = this.mMiWillHal;
v4 = this.mDeathRecipient;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 260 */
} // :goto_0
/* .line 257 */
/* :catch_0 */
/* move-exception v0 */
/* .line 258 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v2 = "onCreate catch"; // const-string v2, "onCreate catch"
android.util.Log .e ( v1,v2,v0 );
/* .line 259 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->deinitMiWillHal()V */
/* .line 261 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
public static Boolean is24GHz ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "freq" # I */
/* .line 561 */
/* const/16 v0, 0x960 */
/* if-le p0, v0, :cond_0 */
/* const/16 v0, 0x9c4 */
/* if-ge p0, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static Boolean is24GHz ( android.net.wifi.ScanResult p0 ) {
/* .locals 1 */
/* .param p0, "scanResult" # Landroid/net/wifi/ScanResult; */
/* .line 553 */
/* iget v0, p0, Landroid/net/wifi/ScanResult;->frequency:I */
v0 = com.xiaomi.NetworkBoost.slaservice.MiWillManager .is24GHz ( v0 );
} // .end method
public static Boolean is5GHz ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "freq" # I */
/* .line 565 */
/* const/16 v0, 0x1324 */
/* if-le p0, v0, :cond_0 */
/* const/16 v0, 0x170c */
/* if-ge p0, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static Boolean is5GHz ( android.net.wifi.ScanResult p0 ) {
/* .locals 1 */
/* .param p0, "scanResult" # Landroid/net/wifi/ScanResult; */
/* .line 557 */
/* iget v0, p0, Landroid/net/wifi/ScanResult;->frequency:I */
v0 = com.xiaomi.NetworkBoost.slaservice.MiWillManager .is5GHz ( v0 );
} // .end method
private Boolean isMiWillRouter ( android.net.wifi.ScanResult p0 ) {
/* .locals 11 */
/* .param p1, "scanResult" # Landroid/net/wifi/ScanResult; */
/* .line 327 */
final String v0 = "MIWILL-MiWillManager"; // const-string v0, "MIWILL-MiWillManager"
int v1 = 0; // const/4 v1, 0x0
/* if-nez p1, :cond_0 */
/* .line 328 */
final String v2 = "isMiWillRouter: null scan result"; // const-string v2, "isMiWillRouter: null scan result"
android.util.Log .e ( v0,v2 );
/* .line 329 */
/* .line 331 */
} // :cond_0
(( android.net.wifi.ScanResult ) p1 ).getInformationElements ( ); // invoke-virtual {p1}, Landroid/net/wifi/ScanResult;->getInformationElements()Ljava/util/List;
/* check-cast v2, [Landroid/net/wifi/ScanResult$InformationElement; */
/* .line 332 */
/* .local v2, "ies":[Landroid/net/wifi/ScanResult$InformationElement; */
if ( v2 != null) { // if-eqz v2, :cond_9
/* array-length v3, v2 */
/* if-lez v3, :cond_9 */
/* .line 333 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* array-length v4, v2 */
/* if-ge v3, v4, :cond_9 */
/* .line 334 */
/* aget-object v4, v2, v3 */
v4 = (( android.net.wifi.ScanResult$InformationElement ) v4 ).getId ( ); // invoke-virtual {v4}, Landroid/net/wifi/ScanResult$InformationElement;->getId()I
/* const/16 v5, 0xdd */
/* if-ne v4, v5, :cond_8 */
/* .line 335 */
/* aget-object v4, v2, v3 */
(( android.net.wifi.ScanResult$InformationElement ) v4 ).getBytes ( ); // invoke-virtual {v4}, Landroid/net/wifi/ScanResult$InformationElement;->getBytes()Ljava/nio/ByteBuffer;
/* .line 336 */
/* .local v4, "byteBuffers":Ljava/nio/ByteBuffer; */
v5 = (( java.nio.ByteBuffer ) v4 ).limit ( ); // invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I
/* new-array v5, v5, [B */
/* .line 337 */
/* .local v5, "value":[B */
v6 = (( java.nio.ByteBuffer ) v4 ).limit ( ); // invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I
v7 = com.xiaomi.NetworkBoost.slaservice.MiWillManager.XIAOMI_OUI;
/* array-length v7, v7 */
v8 = com.xiaomi.NetworkBoost.slaservice.MiWillManager.MIWILL_TYPE;
/* array-length v8, v8 */
/* add-int/2addr v7, v8 */
/* if-eq v6, v7, :cond_1 */
/* .line 338 */
/* .line 341 */
} // :cond_1
try { // :try_start_0
v6 = (( java.nio.ByteBuffer ) v4 ).limit ( ); // invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I
(( java.nio.ByteBuffer ) v4 ).get ( v5, v1, v6 ); // invoke-virtual {v4, v5, v1, v6}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;
/* .line 342 */
int v6 = 0; // const/4 v6, 0x0
/* .line 343 */
/* .local v6, "j":I */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "k":I */
} // :goto_1
v8 = com.xiaomi.NetworkBoost.slaservice.MiWillManager.XIAOMI_OUI;
/* array-length v9, v8 */
/* if-ge v7, v9, :cond_3 */
/* .line 344 */
/* aget-byte v9, v8, v7 */
/* aget-byte v10, v5, v6 */
/* if-eq v9, v10, :cond_2 */
/* .line 345 */
/* .line 347 */
} // :cond_2
/* add-int/lit8 v6, v6, 0x1 */
/* .line 343 */
/* add-int/lit8 v7, v7, 0x1 */
/* .line 349 */
} // .end local v7 # "k":I
} // :cond_3
} // :goto_2
/* array-length v7, v8 */
/* if-eq v6, v7, :cond_4 */
/* .line 350 */
/* .line 352 */
} // :cond_4
int v7 = 0; // const/4 v7, 0x0
/* .restart local v7 # "k":I */
} // :goto_3
v8 = com.xiaomi.NetworkBoost.slaservice.MiWillManager.MIWILL_TYPE;
/* array-length v9, v8 */
/* if-ge v7, v9, :cond_6 */
/* .line 353 */
/* aget-byte v8, v8, v7 */
/* aget-byte v9, v5, v6 */
/* if-eq v8, v9, :cond_5 */
/* .line 354 */
/* .line 356 */
} // :cond_5
/* add-int/lit8 v6, v6, 0x1 */
/* .line 352 */
/* add-int/lit8 v7, v7, 0x1 */
/* .line 358 */
} // .end local v7 # "k":I
} // :cond_6
} // :goto_4
v7 = (( java.nio.ByteBuffer ) v4 ).limit ( ); // invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-eq v6, v7, :cond_7 */
/* .line 359 */
/* .line 361 */
} // :cond_7
int v0 = 1; // const/4 v0, 0x1
/* .line 363 */
} // .end local v6 # "j":I
/* :catch_0 */
/* move-exception v6 */
/* .line 364 */
/* .local v6, "e":Ljava/lang/Exception; */
final String v7 = "isMiWillRouter catch:"; // const-string v7, "isMiWillRouter catch:"
android.util.Log .e ( v0,v7,v6 );
/* .line 333 */
} // .end local v4 # "byteBuffers":Ljava/nio/ByteBuffer;
} // .end local v5 # "value":[B
} // .end local v6 # "e":Ljava/lang/Exception;
} // :cond_8
} // :goto_5
/* add-int/lit8 v3, v3, 0x1 */
/* .line 369 */
} // .end local v3 # "i":I
} // :cond_9
} // .end method
private void registerSwitchObserver ( ) {
/* .locals 4 */
/* .line 276 */
v0 = this.mSettingsObserver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 277 */
final String v0 = "MIWILL-MiWillManager"; // const-string v0, "MIWILL-MiWillManager"
final String v1 = "registerSwitchObserver observer already registered! check your code."; // const-string v1, "registerSwitchObserver observer already registered! check your code."
android.util.Log .e ( v0,v1 );
/* .line 279 */
return;
/* .line 281 */
} // :cond_0
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$2; */
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$2;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 292 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 293 */
/* const-string/jumbo v1, "wifi_miwill_enable" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 292 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 296 */
return;
} // .end method
private void setMiwillStatus ( Boolean p0, Integer p1 ) {
/* .locals 19 */
/* .param p1, "ready" # Z */
/* .param p2, "retry_cnt" # I */
/* .line 394 */
/* move-object/from16 v1, p0 */
v0 = this.mMiWillHal;
final String v2 = "MIWILL-MiWillManager"; // const-string v2, "MIWILL-MiWillManager"
if ( v0 != null) { // if-eqz v0, :cond_17
/* .line 395 */
int v0 = 0; // const/4 v0, 0x0
/* .line 396 */
/* .local v0, "wifi_support_miwill":Z */
int v3 = 0; // const/4 v3, 0x0
/* .line 397 */
/* .local v3, "slave_wifi_support_miwill":Z */
int v4 = 0; // const/4 v4, 0x0
/* .line 398 */
/* .local v4, "wifiBSSID":Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* .line 399 */
/* .local v5, "slavewifiBSSID":Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* .line 400 */
/* .local v6, "wifi_srs":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;" */
int v7 = 0; // const/4 v7, 0x0
/* .line 401 */
/* .local v7, "dhcpInfo":Landroid/net/DhcpInfo; */
int v8 = 0; // const/4 v8, 0x0
/* .line 402 */
/* .local v8, "slaveDhcpInfo":Landroid/net/DhcpInfo; */
int v9 = 0; // const/4 v9, 0x0
/* .line 403 */
/* .local v9, "wifiInfo":Landroid/net/wifi/WifiInfo; */
int v10 = 0; // const/4 v10, 0x0
/* .line 406 */
/* .local v10, "slaveWifiInfo":Landroid/net/wifi/WifiInfo; */
v11 = this.mWifiManager;
int v12 = 0; // const/4 v12, 0x0
if ( v11 != null) { // if-eqz v11, :cond_1
/* .line 407 */
(( android.net.wifi.WifiManager ) v11 ).getConnectionInfo ( ); // invoke-virtual {v11}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;
/* .line 408 */
/* if-nez v9, :cond_0 */
/* move-object v11, v12 */
} // :cond_0
(( android.net.wifi.WifiInfo ) v9 ).getBSSID ( ); // invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;
} // :goto_0
/* move-object v4, v11 */
/* .line 410 */
v11 = this.mWifiManager;
(( android.net.wifi.WifiManager ) v11 ).getScanResults ( ); // invoke-virtual {v11}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;
/* .line 411 */
v11 = this.mWifiManager;
(( android.net.wifi.WifiManager ) v11 ).getDhcpInfo ( ); // invoke-virtual {v11}, Landroid/net/wifi/WifiManager;->getDhcpInfo()Landroid/net/DhcpInfo;
/* .line 413 */
} // :cond_1
v11 = this.mContext;
final String v13 = "SlaveWifiService"; // const-string v13, "SlaveWifiService"
(( android.content.Context ) v11 ).getSystemService ( v13 ); // invoke-virtual {v11, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v11, Landroid/net/wifi/SlaveWifiManager; */
/* .line 414 */
/* .local v11, "slavewifimanager":Landroid/net/wifi/SlaveWifiManager; */
if ( v11 != null) { // if-eqz v11, :cond_3
/* .line 415 */
(( android.net.wifi.SlaveWifiManager ) v11 ).getWifiSlaveConnectionInfo ( ); // invoke-virtual {v11}, Landroid/net/wifi/SlaveWifiManager;->getWifiSlaveConnectionInfo()Landroid/net/wifi/WifiInfo;
/* .line 416 */
/* if-nez v10, :cond_2 */
} // :cond_2
(( android.net.wifi.WifiInfo ) v10 ).getBSSID ( ); // invoke-virtual {v10}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;
} // :goto_1
/* move-object v5, v12 */
/* .line 417 */
(( android.net.wifi.SlaveWifiManager ) v11 ).getSlaveDhcpInfo ( ); // invoke-virtual {v11}, Landroid/net/wifi/SlaveWifiManager;->getSlaveDhcpInfo()Landroid/net/DhcpInfo;
/* .line 421 */
} // :cond_3
if ( v4 != null) { // if-eqz v4, :cond_11
if ( v5 != null) { // if-eqz v5, :cond_11
if ( v6 != null) { // if-eqz v6, :cond_11
if ( v7 != null) { // if-eqz v7, :cond_11
/* if-nez v8, :cond_4 */
/* move-object/from16 v16, v10 */
/* move-object/from16 v17, v11 */
/* goto/16 :goto_7 */
/* .line 428 */
} // :cond_4
/* move v13, v3 */
/* move v3, v0 */
} // .end local v0 # "wifi_support_miwill":Z
/* .local v3, "wifi_support_miwill":Z */
/* .local v13, "slave_wifi_support_miwill":Z */
v0 = } // :goto_2
if ( v0 != null) { // if-eqz v0, :cond_7
/* check-cast v0, Landroid/net/wifi/ScanResult; */
/* .line 429 */
/* .local v0, "wifi_sr":Landroid/net/wifi/ScanResult; */
v14 = this.BSSID;
v14 = (( java.lang.String ) v4 ).equals ( v14 ); // invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v14 != null) { // if-eqz v14, :cond_5
v14 = /* invoke-direct {v1, v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->isMiWillRouter(Landroid/net/wifi/ScanResult;)Z */
if ( v14 != null) { // if-eqz v14, :cond_5
/* .line 430 */
int v3 = 1; // const/4 v3, 0x1
/* .line 432 */
} // :cond_5
v14 = this.BSSID;
v14 = (( java.lang.String ) v5 ).equals ( v14 ); // invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v14 != null) { // if-eqz v14, :cond_6
v14 = /* invoke-direct {v1, v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->isMiWillRouter(Landroid/net/wifi/ScanResult;)Z */
if ( v14 != null) { // if-eqz v14, :cond_6
/* .line 433 */
int v13 = 1; // const/4 v13, 0x1
/* .line 435 */
} // .end local v0 # "wifi_sr":Landroid/net/wifi/ScanResult;
} // :cond_6
/* .line 437 */
} // :cond_7
final String v0 = " "; // const-string v0, " "
if ( v3 != null) { // if-eqz v3, :cond_10
/* if-nez v13, :cond_8 */
/* move-object/from16 v16, v10 */
/* move-object/from16 v17, v11 */
/* goto/16 :goto_6 */
/* .line 443 */
} // :cond_8
/* iget v12, v7, Landroid/net/DhcpInfo;->gateway:I */
android.net.NetworkUtils .intToInetAddress ( v12 );
(( java.net.InetAddress ) v12 ).getHostAddress ( ); // invoke-virtual {v12}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
/* iget v14, v8, Landroid/net/DhcpInfo;->gateway:I */
/* .line 444 */
android.net.NetworkUtils .intToInetAddress ( v14 );
(( java.net.InetAddress ) v14 ).getHostAddress ( ); // invoke-virtual {v14}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
/* .line 443 */
/* invoke-direct {v1, v12, v14}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->checkGateway(Ljava/lang/String;Ljava/lang/String;)Landroid/net/MacAddress; */
/* .line 445 */
/* .local v12, "gateway_mac":Landroid/net/MacAddress; */
/* if-nez v12, :cond_9 */
/* .line 446 */
/* const-string/jumbo v0, "setDualWifiReady turn off miwill gateway_mac is null" */
android.util.Log .i ( v2,v0 );
/* .line 447 */
/* invoke-direct/range {p0 ..p2}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->disableMiwill(ZI)V */
/* .line 448 */
return;
/* .line 454 */
} // :cond_9
v14 = (( android.net.wifi.WifiInfo ) v9 ).getFrequency ( ); // invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getFrequency()I
v14 = com.xiaomi.NetworkBoost.slaservice.MiWillManager .is24GHz ( v14 );
if ( v14 != null) { // if-eqz v14, :cond_a
v14 = (( android.net.wifi.WifiInfo ) v10 ).getFrequency ( ); // invoke-virtual {v10}, Landroid/net/wifi/WifiInfo;->getFrequency()I
v14 = com.xiaomi.NetworkBoost.slaservice.MiWillManager .is5GHz ( v14 );
/* if-nez v14, :cond_b */
/* .line 455 */
} // :cond_a
v14 = (( android.net.wifi.WifiInfo ) v10 ).getFrequency ( ); // invoke-virtual {v10}, Landroid/net/wifi/WifiInfo;->getFrequency()I
v14 = com.xiaomi.NetworkBoost.slaservice.MiWillManager .is24GHz ( v14 );
if ( v14 != null) { // if-eqz v14, :cond_f
v14 = (( android.net.wifi.WifiInfo ) v9 ).getFrequency ( ); // invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getFrequency()I
v14 = com.xiaomi.NetworkBoost.slaservice.MiWillManager .is5GHz ( v14 );
if ( v14 != null) { // if-eqz v14, :cond_f
/* .line 456 */
} // :cond_b
/* const-string/jumbo v14, "setDualWifiReady all check ok!" */
android.util.Log .i ( v2,v14 );
/* .line 465 */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 466 */
/* .local v14, "cmd":Ljava/lang/StringBuilder; */
/* const-string/jumbo v15, "set" */
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 467 */
(( java.lang.StringBuilder ) v15 ).append ( v0 ); // invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 468 */
/* move-object/from16 v16, v10 */
} // .end local v10 # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
/* .local v16, "slaveWifiInfo":Landroid/net/wifi/WifiInfo; */
final String v10 = "miw_oem0"; // const-string v10, "miw_oem0"
(( java.lang.StringBuilder ) v15 ).append ( v10 ); // invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 469 */
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 470 */
if ( p1 != null) { // if-eqz p1, :cond_c
final String v15 = "1"; // const-string v15, "1"
} // :cond_c
final String v15 = "0"; // const-string v15, "0"
} // :goto_3
(( java.lang.StringBuilder ) v10 ).append ( v15 ); // invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 471 */
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v15 = com.xiaomi.NetworkBoost.slaservice.MiWillManager.mModeMap;
/* move-object/from16 v17, v11 */
} // .end local v11 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
/* .local v17, "slavewifimanager":Landroid/net/wifi/SlaveWifiManager; */
/* iget v11, v1, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->mModeKey:I */
/* .line 472 */
java.lang.Integer .valueOf ( v11 );
(( java.util.HashMap ) v15 ).get ( v11 ); // invoke-virtual {v15, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v11, Ljava/lang/String; */
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 473 */
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 474 */
(( android.net.MacAddress ) v12 ).toString ( ); // invoke-virtual {v12}, Landroid/net/MacAddress;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 475 */
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 476 */
v11 = (( android.net.wifi.WifiInfo ) v9 ).getFrequency ( ); // invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getFrequency()I
v11 = com.xiaomi.NetworkBoost.slaservice.MiWillManager .is24GHz ( v11 );
/* const-string/jumbo v15, "wlan1" */
/* const-string/jumbo v18, "wlan0" */
if ( v11 != null) { // if-eqz v11, :cond_d
/* move-object v11, v15 */
} // :cond_d
/* move-object/from16 v11, v18 */
} // :goto_4
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 477 */
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 478 */
v10 = (( android.net.wifi.WifiInfo ) v9 ).getFrequency ( ); // invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getFrequency()I
v10 = com.xiaomi.NetworkBoost.slaservice.MiWillManager .is24GHz ( v10 );
if ( v10 != null) { // if-eqz v10, :cond_e
/* move-object/from16 v15, v18 */
} // :cond_e
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 480 */
try { // :try_start_0
v0 = this.mMiWillHal;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 484 */
/* .line 482 */
/* :catch_0 */
/* move-exception v0 */
/* .line 483 */
/* .local v0, "ex":Ljava/lang/Exception; */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "catch ex:"; // const-string v11, "catch ex:"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v10 );
/* .line 485 */
} // .end local v0 # "ex":Ljava/lang/Exception;
} // .end local v3 # "wifi_support_miwill":Z
} // .end local v4 # "wifiBSSID":Ljava/lang/String;
} // .end local v5 # "slavewifiBSSID":Ljava/lang/String;
} // .end local v6 # "wifi_srs":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
} // .end local v7 # "dhcpInfo":Landroid/net/DhcpInfo;
} // .end local v8 # "slaveDhcpInfo":Landroid/net/DhcpInfo;
} // .end local v9 # "wifiInfo":Landroid/net/wifi/WifiInfo;
} // .end local v12 # "gateway_mac":Landroid/net/MacAddress;
} // .end local v13 # "slave_wifi_support_miwill":Z
} // .end local v14 # "cmd":Ljava/lang/StringBuilder;
} // .end local v16 # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
} // .end local v17 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
} // :goto_5
/* goto/16 :goto_d */
/* .line 455 */
/* .restart local v3 # "wifi_support_miwill":Z */
/* .restart local v4 # "wifiBSSID":Ljava/lang/String; */
/* .restart local v5 # "slavewifiBSSID":Ljava/lang/String; */
/* .restart local v6 # "wifi_srs":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;" */
/* .restart local v7 # "dhcpInfo":Landroid/net/DhcpInfo; */
/* .restart local v8 # "slaveDhcpInfo":Landroid/net/DhcpInfo; */
/* .restart local v9 # "wifiInfo":Landroid/net/wifi/WifiInfo; */
/* .restart local v10 # "slaveWifiInfo":Landroid/net/wifi/WifiInfo; */
/* .restart local v11 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager; */
/* .restart local v12 # "gateway_mac":Landroid/net/MacAddress; */
/* .restart local v13 # "slave_wifi_support_miwill":Z */
} // :cond_f
/* move-object/from16 v16, v10 */
/* move-object/from16 v17, v11 */
/* .line 459 */
} // .end local v10 # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
} // .end local v11 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
/* .restart local v16 # "slaveWifiInfo":Landroid/net/wifi/WifiInfo; */
/* .restart local v17 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager; */
/* const-string/jumbo v0, "setDualWifiReady turn off miwill found 6G!?" */
android.util.Log .i ( v2,v0 );
/* .line 460 */
/* invoke-direct/range {p0 ..p2}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->disableMiwill(ZI)V */
/* .line 461 */
return;
/* .line 437 */
} // .end local v12 # "gateway_mac":Landroid/net/MacAddress;
} // .end local v16 # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
} // .end local v17 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
/* .restart local v10 # "slaveWifiInfo":Landroid/net/wifi/WifiInfo; */
/* .restart local v11 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager; */
} // :cond_10
/* move-object/from16 v16, v10 */
/* move-object/from16 v17, v11 */
/* .line 438 */
} // .end local v10 # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
} // .end local v11 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
/* .restart local v16 # "slaveWifiInfo":Landroid/net/wifi/WifiInfo; */
/* .restart local v17 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager; */
} // :goto_6
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v11, "setDualWifiReady turn off miwill: " */
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v3 ); // invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v2,v0 );
/* .line 439 */
/* invoke-direct/range {p0 ..p2}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->disableMiwill(ZI)V */
/* .line 440 */
return;
/* .line 421 */
} // .end local v13 # "slave_wifi_support_miwill":Z
} // .end local v16 # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
} // .end local v17 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
/* .local v0, "wifi_support_miwill":Z */
/* .local v3, "slave_wifi_support_miwill":Z */
/* .restart local v10 # "slaveWifiInfo":Landroid/net/wifi/WifiInfo; */
/* .restart local v11 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager; */
} // :cond_11
/* move-object/from16 v16, v10 */
/* move-object/from16 v17, v11 */
/* .line 422 */
} // .end local v10 # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
} // .end local v11 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
/* .restart local v16 # "slaveWifiInfo":Landroid/net/wifi/WifiInfo; */
/* .restart local v17 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager; */
} // :goto_7
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v11, "setDualWifiReady turn off miwill1: wifiBSSID:" */
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v11 = 1; // const/4 v11, 0x1
int v12 = 0; // const/4 v12, 0x0
/* if-nez v4, :cond_12 */
/* move v13, v11 */
} // :cond_12
/* move v13, v12 */
} // :goto_8
(( java.lang.StringBuilder ) v10 ).append ( v13 ); // invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v13 = " slavewifiBSSID:"; // const-string v13, " slavewifiBSSID:"
(( java.lang.StringBuilder ) v10 ).append ( v13 ); // invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* if-nez v5, :cond_13 */
/* move v13, v11 */
} // :cond_13
/* move v13, v12 */
} // :goto_9
(( java.lang.StringBuilder ) v10 ).append ( v13 ); // invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v13 = " wifi_srs:"; // const-string v13, " wifi_srs:"
(( java.lang.StringBuilder ) v10 ).append ( v13 ); // invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* if-nez v6, :cond_14 */
/* move v13, v11 */
} // :cond_14
/* move v13, v12 */
} // :goto_a
(( java.lang.StringBuilder ) v10 ).append ( v13 ); // invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v13 = " dhcpInfo:"; // const-string v13, " dhcpInfo:"
(( java.lang.StringBuilder ) v10 ).append ( v13 ); // invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* if-nez v7, :cond_15 */
/* move v13, v11 */
} // :cond_15
/* move v13, v12 */
} // :goto_b
(( java.lang.StringBuilder ) v10 ).append ( v13 ); // invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v13 = " slaveDhcpInfo:"; // const-string v13, " slaveDhcpInfo:"
(( java.lang.StringBuilder ) v10 ).append ( v13 ); // invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* if-nez v8, :cond_16 */
} // :cond_16
/* move v11, v12 */
} // :goto_c
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v2,v10 );
/* .line 424 */
/* invoke-direct/range {p0 ..p2}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->disableMiwill(ZI)V */
/* .line 425 */
return;
/* .line 487 */
} // .end local v0 # "wifi_support_miwill":Z
} // .end local v3 # "slave_wifi_support_miwill":Z
} // .end local v4 # "wifiBSSID":Ljava/lang/String;
} // .end local v5 # "slavewifiBSSID":Ljava/lang/String;
} // .end local v6 # "wifi_srs":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
} // .end local v7 # "dhcpInfo":Landroid/net/DhcpInfo;
} // .end local v8 # "slaveDhcpInfo":Landroid/net/DhcpInfo;
} // .end local v9 # "wifiInfo":Landroid/net/wifi/WifiInfo;
} // .end local v16 # "slaveWifiInfo":Landroid/net/wifi/WifiInfo;
} // .end local v17 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
} // :cond_17
/* const-string/jumbo v0, "setDualWifiReady miwill hal service lost" */
android.util.Log .e ( v2,v0 );
/* .line 489 */
} // :goto_d
return;
} // .end method
private void unregisterSwitchObserver ( ) {
/* .locals 2 */
/* .line 300 */
v0 = this.mSettingsObserver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 301 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).unregisterContentObserver ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
/* .line 302 */
int v0 = 0; // const/4 v0, 0x0
this.mSettingsObserver = v0;
/* .line 304 */
} // :cond_0
return;
} // .end method
private void updateStatus ( ) {
/* .locals 4 */
/* .line 312 */
v0 = this.mMiWillHal;
final String v1 = "MIWILL-MiWillManager"; // const-string v1, "MIWILL-MiWillManager"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 314 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 316 */
/* :catch_0 */
/* move-exception v0 */
/* .line 317 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateStatus catch:" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v2 );
/* .line 318 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* .line 321 */
} // :cond_0
/* const-string/jumbo v0, "updateStatus mMiWillHal is null" */
android.util.Log .e ( v1,v0 );
/* .line 324 */
} // :goto_1
return;
} // .end method
/* # virtual methods */
public void onCreate ( ) {
/* .locals 5 */
/* .line 187 */
final String v0 = "onCreate"; // const-string v0, "onCreate"
final String v1 = "MIWILL-MiWillManager"; // const-string v1, "MIWILL-MiWillManager"
android.util.Log .i ( v1,v0 );
/* .line 189 */
v0 = this.mContext;
/* const-string/jumbo v2, "wifi" */
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/WifiManager; */
this.mWifiManager = v0;
/* .line 192 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v2 = "MiWillWorkHandler"; // const-string v2, "MiWillWorkHandler"
/* invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mThread = v0;
/* .line 193 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 194 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$MiWillHandler; */
v2 = this.mThread;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$MiWillHandler;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 197 */
try { // :try_start_0
miui.android.services.internal.hidl.manager.V1_0.IServiceManager .getService ( );
/* const-string/jumbo v2, "vendor.xiaomi.hidl.miwill@1.0::IMiwillService" */
final String v3 = "default"; // const-string v3, "default"
v4 = this.mNotification;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 201 */
/* .line 199 */
/* :catch_0 */
/* move-exception v0 */
/* .line 200 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v2 = "onCreate catch"; // const-string v2, "onCreate catch"
android.util.Log .e ( v1,v2,v0 );
/* .line 218 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void onDestroy ( ) {
/* .locals 2 */
/* .line 221 */
final String v0 = "onDestroy"; // const-string v0, "onDestroy"
final String v1 = "MIWILL-MiWillManager"; // const-string v1, "MIWILL-MiWillManager"
android.util.Log .i ( v1,v0 );
/* .line 223 */
final String v0 = "disable miwill because miwill manager destroying."; // const-string v0, "disable miwill because miwill manager destroying."
android.util.Log .i ( v1,v0 );
/* .line 224 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->disableMiwill(ZI)V */
/* .line 232 */
v0 = this.mThread;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 233 */
(( android.os.HandlerThread ) v0 ).quitSafely ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z
/* .line 234 */
this.mThread = v1;
/* .line 236 */
} // :cond_0
this.mHandler = v1;
/* .line 237 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->deinitMiWillHal()V */
/* .line 238 */
return;
} // .end method
public void setDualWifiReady ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "ready" # Z */
/* .line 373 */
/* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->isDualWifiReady:Z */
/* .line 374 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 375 */
/* const/16 v1, 0x66 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 376 */
v0 = this.mHandler;
/* const/16 v2, 0x67 */
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 377 */
v0 = this.mHandler;
java.lang.Boolean .valueOf ( p1 );
(( android.os.Handler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 379 */
} // :cond_0
return;
} // .end method
public Boolean setMiWillGameStart ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 516 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->checkMiWillIp()Z */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MIWILL-MiWillManager"; // const-string v2, "MIWILL-MiWillManager"
/* if-nez v0, :cond_0 */
/* .line 517 */
/* const-string/jumbo v0, "setMiWillGameStart miwill ip not found." */
android.util.Log .w ( v2,v0 );
/* .line 518 */
/* .line 520 */
} // :cond_0
v0 = this.mMiWillHal;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 522 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 527 */
/* nop */
/* .line 528 */
int v0 = 1; // const/4 v0, 0x1
/* .line 524 */
/* :catch_0 */
/* move-exception v0 */
/* .line 525 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "setMiWillGameStart catch:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 526 */
/* .line 531 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_1
/* const-string/jumbo v0, "setMiWillGameStart miwill hal service lost" */
android.util.Log .e ( v2,v0 );
/* .line 532 */
} // .end method
public Boolean setMiWillGameStop ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 537 */
v0 = this.mMiWillHal;
final String v1 = "MIWILL-MiWillManager"; // const-string v1, "MIWILL-MiWillManager"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 539 */
try { // :try_start_0
final String v2 = "0"; // const-string v2, "0"
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 543 */
/* .line 541 */
/* :catch_0 */
/* move-exception v0 */
/* .line 542 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setMiWillGameStop catch:" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 544 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 547 */
} // :cond_0
/* const-string/jumbo v0, "setMiWillGameStop miwill hal service lost" */
android.util.Log .e ( v1,v0 );
/* .line 548 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
