.class public Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
.super Ljava/lang/Object;
.source "SLAApp.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mCurTraffic:J

.field private mDay:I

.field private mDayTraffic:J

.field private mMonth:I

.field private mMonthTraffic:J

.field private mState:Z

.field private mUid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 86
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp$1;

    invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp$1;-><init>()V

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mUid:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDayTraffic:J

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonthTraffic:J

    .line 74
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "uid"    # Ljava/lang/String;

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 78
    .local v0, "c":Ljava/util/Calendar;
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mUid:Ljava/lang/String;

    .line 79
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDayTraffic:J

    .line 80
    iput-wide v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonthTraffic:J

    .line 81
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mState:Z

    .line 82
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iput v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDay:I

    .line 83
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/2addr v2, v1

    iput v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonth:I

    .line 84
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method public getDay()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDay:I

    return v0
.end method

.method public getDayTraffic()J
    .locals 2

    .line 44
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDayTraffic:J

    return-wide v0
.end method

.method public getMonth()I
    .locals 1

    .line 36
    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonth:I

    return v0
.end method

.method public getMonthTraffic()J
    .locals 2

    .line 52
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonthTraffic:J

    return-wide v0
.end method

.method public getState()Z
    .locals 1

    .line 60
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mState:Z

    return v0
.end method

.method public getUid()Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mUid:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mUid:Ljava/lang/String;

    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDayTraffic:J

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonthTraffic:J

    .line 114
    return-void
.end method

.method public setDay(I)V
    .locals 0
    .param p1, "day"    # I

    .line 32
    iput p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDay:I

    .line 33
    return-void
.end method

.method public setDayTraffic(J)V
    .locals 0
    .param p1, "traffic"    # J

    .line 48
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDayTraffic:J

    .line 49
    return-void
.end method

.method public setMonth(I)V
    .locals 0
    .param p1, "month"    # I

    .line 40
    iput p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonth:I

    .line 41
    return-void
.end method

.method public setMonthTraffic(J)V
    .locals 0
    .param p1, "traffic"    # J

    .line 56
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonthTraffic:J

    .line 57
    return-void
.end method

.method public setState(Z)V
    .locals 0
    .param p1, "state"    # Z

    .line 64
    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mState:Z

    .line 65
    return-void
.end method

.method public setUid(Ljava/lang/String;)V
    .locals 0
    .param p1, "uid"    # Ljava/lang/String;

    .line 24
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mUid:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .line 105
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mUid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 106
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDayTraffic:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 107
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonthTraffic:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 108
    return-void
.end method
