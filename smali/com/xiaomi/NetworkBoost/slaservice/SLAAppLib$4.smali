.class Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;
.super Landroid/database/ContentObserver;
.source "SLAAppLib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLMCloudObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 1090
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 19
    .param p1, "selfChange"    # Z

    .line 1094
    move-object/from16 v1, p0

    const-string v2, "SLM-SRV-SLAAppLib"

    :try_start_0
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1095
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v4

    .line 1097
    .local v4, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    sget-object v5, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 1098
    .local v5, "device":Ljava/lang/String;
    iget-object v6, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v6}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->clear()V

    .line 1099
    iget-object v6, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v6}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleWifiAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->clear()V

    .line 1100
    iget-object v6, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    .line 1101
    iget-object v6, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v6, v7}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    .line 1104
    iget-object v6, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v6}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Landroid/content/Context;

    move-result-object v6

    .line 1105
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "cloud_sls_whitelist"

    .line 1104
    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1106
    .local v6, "whiteString_sls":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Cloud SLSApp: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1107
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v8, ","

    if-nez v7, :cond_1

    .line 1108
    :try_start_1
    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 1109
    .local v7, "packages":[Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 1110
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    array-length v10, v7

    if-ge v9, v10, :cond_1

    .line 1111
    aget-object v10, v7, v9

    const-string v11, "miwill"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 1112
    iget-object v10, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v10

    aget-object v11, v7, v9

    invoke-interface {v10, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1110
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 1119
    .end local v7    # "packages":[Ljava/lang/String;
    .end local v9    # "i":I
    :cond_1
    iget-object v7, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v7}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Landroid/content/Context;

    move-result-object v7

    .line 1120
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v9, "cloud_double_wifi_uidlist"

    .line 1119
    invoke-static {v7, v9}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1121
    .local v7, "whiteString_doubleWifi":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Cloud DWApp: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1122
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 1123
    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 1124
    .local v9, "packages":[Ljava/lang/String;
    if-eqz v9, :cond_2

    .line 1125
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    array-length v11, v9

    if-ge v10, v11, :cond_2

    .line 1126
    iget-object v11, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleWifiAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v11

    aget-object v12, v9, v10

    invoke-interface {v11, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1125
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 1132
    .end local v9    # "packages":[Ljava/lang/String;
    .end local v10    # "i":I
    :cond_2
    iget-object v9, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->isEmpty()Z

    move-result v9
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v10, "ro.boot.hwc"

    const-string v11, "CN"

    const-string v12, "com.tencent.mm"

    const/4 v14, 0x2

    const/4 v15, 0x4

    const/4 v13, 0x0

    if-eqz v9, :cond_4

    .line 1134
    :try_start_2
    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v11, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1135
    new-array v9, v15, [Ljava/lang/String;

    const-string v17, "com.tencent.tmgp.pubgmhd"

    aput-object v17, v9, v13

    const-string v17, "com.tencent.tmgp.sgame"

    aput-object v17, v9, v3

    const-string v17, "com.tencent.tmgp.speedmobile"

    aput-object v17, v9, v14

    const/16 v16, 0x3

    aput-object v12, v9, v16

    .restart local v9    # "packages":[Ljava/lang/String;
    goto :goto_2

    .line 1138
    .end local v9    # "packages":[Ljava/lang/String;
    :cond_3
    new-array v9, v3, [Ljava/lang/String;

    const-string v17, "com.test"

    aput-object v17, v9, v13

    .line 1140
    .restart local v9    # "packages":[Ljava/lang/String;
    :goto_2
    nop

    .line 1141
    const/16 v17, 0x0

    move/from16 v15, v17

    .local v15, "i":I
    :goto_3
    array-length v14, v9

    if-ge v15, v14, :cond_4

    .line 1142
    iget-object v14, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v14}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v14

    aget-object v3, v9, v15

    invoke-interface {v14, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1141
    add-int/lit8 v15, v15, 0x1

    const/4 v3, 0x1

    const/4 v14, 0x2

    goto :goto_3

    .line 1146
    .end local v9    # "packages":[Ljava/lang/String;
    .end local v15    # "i":I
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "set SLSApp: "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v9, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1147
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/pm/PackageInfo;

    .line 1149
    .local v9, "app":Landroid/content/pm/PackageInfo;
    iget-object v14, v9, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v14, :cond_7

    iget-object v14, v9, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v14, :cond_7

    iget-object v14, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v14}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v14

    iget-object v15, v9, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 1150
    invoke-interface {v14, v15}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1151
    iget-object v14, v9, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v14, v14, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 1152
    .local v14, "uid":I
    iget-object v15, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v15, v14}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$maddSLAAppDefault(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;I)V

    .line 1153
    iget-object v15, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v15}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v15

    if-nez v15, :cond_5

    .line 1154
    iget-object v15, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v18, v0

    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    .local v18, "pm":Landroid/content/pm/PackageManager;
    invoke-static {v14}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    goto :goto_5

    .line 1156
    .end local v18    # "pm":Landroid/content/pm/PackageManager;
    .restart local v0    # "pm":Landroid/content/pm/PackageManager;
    :cond_5
    move-object/from16 v18, v0

    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    .restart local v18    # "pm":Landroid/content/pm/PackageManager;
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v15, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v15}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v14}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v0, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    goto :goto_5

    .line 1150
    .end local v14    # "uid":I
    .end local v18    # "pm":Landroid/content/pm/PackageManager;
    .restart local v0    # "pm":Landroid/content/pm/PackageManager;
    :cond_6
    move-object/from16 v18, v0

    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    .restart local v18    # "pm":Landroid/content/pm/PackageManager;
    goto :goto_5

    .line 1149
    .end local v18    # "pm":Landroid/content/pm/PackageManager;
    .restart local v0    # "pm":Landroid/content/pm/PackageManager;
    :cond_7
    move-object/from16 v18, v0

    .line 1161
    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    .restart local v18    # "pm":Landroid/content/pm/PackageManager;
    :goto_5
    iget-object v0, v9, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, v9, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 1162
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1163
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1164
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    iget-object v13, v9, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v13, v13, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v0, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;I)V

    goto :goto_6

    .line 1166
    :cond_8
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    const/4 v13, 0x0

    invoke-static {v0, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;I)V

    .line 1169
    .end local v9    # "app":Landroid/content/pm/PackageInfo;
    :cond_9
    :goto_6
    move-object/from16 v0, v18

    const/4 v13, 0x0

    goto/16 :goto_4

    .line 1170
    .end local v18    # "pm":Landroid/content/pm/PackageManager;
    .restart local v0    # "pm":Landroid/content/pm/PackageManager;
    :cond_a
    move-object/from16 v18, v0

    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    .restart local v18    # "pm":Landroid/content/pm/PackageManager;
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)I

    move-result v0

    if-eqz v0, :cond_b

    .line 1171
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v3

    iget-object v9, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    const-string v12, ""

    invoke-virtual {v3, v9, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    .line 1172
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v3

    const-string v9, ",,"

    invoke-virtual {v3, v9, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    .line 1174
    :cond_b
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSGameUidList(Ljava/lang/String;)V

    .line 1175
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)I

    move-result v0

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSVoIPUid(I)V

    .line 1178
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->setNetworkAccelerateSwitchUidList(Ljava/lang/String;)V

    .line 1179
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)I

    move-result v0

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->setNetworkAccelerateSwitchVoIPUid(I)V

    .line 1182
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleWifiAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1184
    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    const/4 v3, 0x5

    const/4 v9, 0x6

    if-eqz v0, :cond_c

    .line 1185
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v10, "air.tv.douyu.android"

    const/4 v11, 0x0

    aput-object v10, v0, v11

    const-string v10, "com.duowan.kiwi"

    const/4 v11, 0x1

    aput-object v10, v0, v11

    const-string v10, "com.ss.android.ugc.aweme"

    const/4 v11, 0x2

    aput-object v10, v0, v11

    const-string v10, "com.taobao.taobao"

    const/4 v11, 0x3

    aput-object v10, v0, v11

    const-string v10, "com.tmall.wireless"

    const/4 v11, 0x4

    aput-object v10, v0, v11

    const-string v10, "com.jingdong.app.mall"

    aput-object v10, v0, v3

    const-string v3, "com.youku.phone"

    aput-object v3, v0, v9

    const-string v3, "com.qiyi.video"

    const/4 v9, 0x7

    aput-object v3, v0, v9

    const-string v3, "com.android.providers.downloads.ui"

    const/16 v9, 0x8

    aput-object v3, v0, v9

    const-string v3, "com.android.providers.downloads"

    const/16 v9, 0x9

    aput-object v3, v0, v9

    const-string v3, "com.xiaomi.market"

    const/16 v9, 0xa

    aput-object v3, v0, v9

    const-string/jumbo v3, "tv.danmaku.bili"

    const/16 v9, 0xb

    aput-object v3, v0, v9

    const-string v3, "org.zwanoo.android.speedtest"

    const/16 v9, 0xc

    aput-object v3, v0, v9

    .local v0, "packages":[Ljava/lang/String;
    goto :goto_7

    .line 1190
    .end local v0    # "packages":[Ljava/lang/String;
    :cond_c
    new-array v0, v9, [Ljava/lang/String;

    const-string v9, "com.spotify.music"

    const/4 v10, 0x0

    aput-object v9, v0, v10

    const-string v9, "com.ebay.mobile"

    const/4 v10, 0x1

    aput-object v9, v0, v10

    const-string v9, "com.amazon.kindle"

    const/4 v10, 0x2

    aput-object v9, v0, v10

    const-string v9, "com.instagram.android"

    const/4 v10, 0x3

    aput-object v9, v0, v10

    const-string v9, "com.melodis.midomiMusicIdentifier.freemium"

    const/4 v10, 0x4

    aput-object v9, v0, v10

    const-string v9, "com.google.android.youtube"

    aput-object v9, v0, v3

    .line 1195
    .restart local v0    # "packages":[Ljava/lang/String;
    :goto_7
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_8
    array-length v9, v0

    if-ge v3, v9, :cond_d

    .line 1196
    iget-object v9, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleWifiAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v9

    aget-object v10, v0, v3

    invoke-interface {v9, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1195
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 1199
    .end local v0    # "packages":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "set DWApp: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleWifiAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1200
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageInfo;

    .line 1201
    .local v3, "app":Landroid/content/pm/PackageInfo;
    iget-object v9, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v9, :cond_f

    iget-object v9, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v9, :cond_f

    iget-object v9, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleWifiAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v9

    iget-object v10, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 1202
    invoke-interface {v9, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 1203
    iget-object v9, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 1204
    .local v9, "uid":I
    iget-object v10, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_e

    .line 1205
    iget-object v10, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    goto :goto_a

    .line 1207
    :cond_e
    iget-object v10, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v12}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1210
    .end local v3    # "app":Landroid/content/pm/PackageInfo;
    .end local v9    # "uid":I
    :cond_f
    :goto_a
    goto :goto_9

    .line 1213
    .end local v4    # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v5    # "device":Ljava/lang/String;
    .end local v6    # "whiteString_sls":Ljava/lang/String;
    .end local v7    # "whiteString_doubleWifi":Ljava/lang/String;
    .end local v18    # "pm":Landroid/content/pm/PackageManager;
    :cond_10
    goto :goto_b

    .line 1211
    :catch_0
    move-exception v0

    .line 1212
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initSLMCloudObserver onChange error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1214
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_b
    return-void
.end method
