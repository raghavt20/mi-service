public class com.xiaomi.NetworkBoost.slaservice.SLSAppLib {
	 /* .source "SLSAppLib.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$InternalHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer EVENT_START_VOIP;
private static final Integer EVENT_STOP_VOIP;
static final java.lang.String TAG;
private static final Integer VOIP_START_DELAY_TIME;
private static final Integer VOIP_STOP_DELAY_TIME;
private static Integer mSLSUid;
private static Integer mSLSVoIPUid;
private static java.util.HashSet mSlaAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static vendor.qti.sla.service.V1_0.ISlaService mSlaService;
private static java.util.HashSet mSlsAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private Boolean isSLSEnable;
private Boolean isSLSGameRunning;
private Boolean isSLSVoIPRunning;
private android.app.IActivityManager mActivityManager;
private com.xiaomi.NetworkBoost.StatusManager$IAppStatusListener mAppStatusListener;
private android.content.Context mContext;
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
private com.xiaomi.NetworkBoost.slaservice.SLATrack mSLATrack;
private java.lang.ref.WeakReference mSLMService;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ref/WeakReference<", */
/* "Lcom/xiaomi/NetworkBoost/slaservice/SLAService;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.xiaomi.NetworkBoost.StatusManager mStatusManager;
private Boolean powerMgrGameStatus;
private Integer powerMgrGameUid;
private Boolean powerMgrSLMStatus;
private Boolean powerMgrScreenOn;
private Boolean powerMgrVoIPStatus;
/* # direct methods */
static android.os.Handler -$$Nest$fgetmHandler ( com.xiaomi.NetworkBoost.slaservice.SLSAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Integer -$$Nest$fgetpowerMgrGameUid ( com.xiaomi.NetworkBoost.slaservice.SLSAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameUid:I */
} // .end method
static void -$$Nest$msetPowerMgrGameInfo ( com.xiaomi.NetworkBoost.slaservice.SLSAppLib p0, Boolean p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setPowerMgrGameInfo(ZI)V */
return;
} // .end method
static void -$$Nest$msetPowerMgrVoIPInfo ( com.xiaomi.NetworkBoost.slaservice.SLSAppLib p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setPowerMgrVoIPInfo(Z)V */
return;
} // .end method
static Boolean -$$Nest$msetSLSGameStart ( com.xiaomi.NetworkBoost.slaservice.SLSAppLib p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSGameStart(Ljava/lang/String;)Z */
} // .end method
static Boolean -$$Nest$msetSLSGameStop ( com.xiaomi.NetworkBoost.slaservice.SLSAppLib p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSGameStop(Ljava/lang/String;)Z */
} // .end method
static void -$$Nest$msetSLSVoIPStart ( com.xiaomi.NetworkBoost.slaservice.SLSAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSVoIPStart()V */
return;
} // .end method
static void -$$Nest$msetSLSVoIPStop ( com.xiaomi.NetworkBoost.slaservice.SLSAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSVoIPStop()V */
return;
} // .end method
static Integer -$$Nest$sfgetmSLSUid ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static java.util.HashSet -$$Nest$sfgetmSlaAppList ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib.mSlaAppList;
} // .end method
static java.util.HashSet -$$Nest$sfgetmSlsAppList ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib.mSlsAppList;
} // .end method
static com.xiaomi.NetworkBoost.slaservice.SLSAppLib ( ) {
/* .locals 1 */
/* .line 46 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 47 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 48 */
int v0 = 0; // const/4 v0, 0x0
/* .line 51 */
return;
} // .end method
public com.xiaomi.NetworkBoost.slaservice.SLSAppLib ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "service" # Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* .line 78 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 52 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSGameRunning:Z */
/* .line 53 */
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSVoIPRunning:Z */
/* .line 56 */
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameStatus:Z */
/* .line 57 */
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameUid:I */
/* .line 58 */
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrVoIPStatus:Z */
/* .line 59 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrSLMStatus:Z */
/* .line 60 */
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrScreenOn:Z */
/* .line 68 */
int v0 = 0; // const/4 v0, 0x0
this.mStatusManager = v0;
/* .line 350 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)V */
this.mAppStatusListener = v0;
/* .line 79 */
this.mContext = p1;
/* .line 80 */
/* new-instance v0, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
this.mSLMService = v0;
/* .line 82 */
com.xiaomi.NetworkBoost.slaservice.SLATrack .getSLATrack ( p1 );
this.mSLATrack = v0;
/* .line 84 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "SLSAppLibHandler"; // const-string v1, "SLSAppLibHandler"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 85 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 86 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$InternalHandler; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$InternalHandler;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 88 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->registerAppStatusListener()V */
/* .line 89 */
return;
} // .end method
private Boolean checkHAL ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "func" # Ljava/lang/String; */
/* .line 224 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib.mSlaService;
/* if-nez v0, :cond_0 */
/* .line 225 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " checkHAL null"; // const-string v1, " checkHAL null"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLSAppLib"; // const-string v1, "SLM-SRV-SLSAppLib"
android.util.Log .e ( v1,v0 );
/* .line 226 */
int v0 = 0; // const/4 v0, 0x0
/* .line 228 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void checkSLSStatus ( ) {
/* .locals 2 */
/* .line 92 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameStatus:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrVoIPStatus:Z */
/* if-nez v1, :cond_1 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrScreenOn:Z */
/* if-nez v1, :cond_2 */
/* .line 93 */
} // :cond_1
return;
/* .line 96 */
} // :cond_2
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 97 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameUid:I */
java.lang.Integer .toString ( v0 );
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSGameStart(Ljava/lang/String;)Z */
/* .line 98 */
} // :cond_3
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrVoIPStatus:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 99 */
v0 = this.mHandler;
/* const/16 v1, 0x64 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 100 */
v0 = this.mHandler;
/* const/16 v1, 0x65 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 101 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSVoIPStart()V */
/* .line 103 */
} // :cond_4
final String v0 = "SLM-SRV-SLSAppLib"; // const-string v0, "SLM-SRV-SLSAppLib"
final String v1 = "checkSLSStatus false."; // const-string v1, "checkSLSStatus false."
android.util.Log .d ( v0,v1 );
/* .line 105 */
} // :goto_0
return;
} // .end method
private void disableSLM ( ) {
/* .locals 3 */
/* .line 162 */
v0 = this.mSLMService;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* .line 163 */
/* .local v0, "service":Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* if-nez v0, :cond_0 */
/* .line 164 */
final String v1 = "SLM-SRV-SLSAppLib"; // const-string v1, "SLM-SRV-SLSAppLib"
final String v2 = "enableSLM get SLAService null!"; // const-string v2, "enableSLM get SLAService null!"
android.util.Log .e ( v1,v2 );
/* .line 165 */
return;
/* .line 168 */
} // :cond_0
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v0 ).disableSLM ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLM()V
/* .line 169 */
return;
} // .end method
private void enableSLM ( ) {
/* .locals 3 */
/* .line 152 */
v0 = this.mSLMService;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* .line 153 */
/* .local v0, "service":Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* if-nez v0, :cond_0 */
/* .line 154 */
final String v1 = "SLM-SRV-SLSAppLib"; // const-string v1, "SLM-SRV-SLSAppLib"
final String v2 = "enableSLM get SLAService null!"; // const-string v2, "enableSLM get SLAService null!"
android.util.Log .e ( v1,v2 );
/* .line 155 */
return;
/* .line 158 */
} // :cond_0
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v0 ).enableSLM ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->enableSLM()V
/* .line 159 */
return;
} // .end method
private void powerMgrCheck ( ) {
/* .locals 2 */
/* .line 108 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameStatus:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrVoIPStatus:Z */
/* if-nez v1, :cond_1 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrScreenOn:Z */
/* if-nez v0, :cond_2 */
/* .line 109 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrSLMStatus:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 110 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrSLMStatus:Z */
/* .line 111 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->disableSLM()V */
/* .line 114 */
} // :cond_2
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrSLMStatus:Z */
/* if-nez v0, :cond_5 */
/* .line 115 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrSLMStatus:Z */
/* .line 116 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->enableSLM()V */
/* .line 118 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameStatus:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 119 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameUid:I */
java.lang.Integer .toString ( v0 );
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSGameStart(Ljava/lang/String;)Z */
/* .line 120 */
} // :cond_3
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrVoIPStatus:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 121 */
v0 = this.mHandler;
/* const/16 v1, 0x64 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 122 */
v0 = this.mHandler;
/* const/16 v1, 0x65 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 123 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSVoIPStart()V */
/* .line 125 */
} // :cond_4
final String v0 = "SLM-SRV-SLSAppLib"; // const-string v0, "SLM-SRV-SLSAppLib"
final String v1 = "powerMgrCheck error."; // const-string v1, "powerMgrCheck error."
android.util.Log .e ( v0,v1 );
/* .line 129 */
} // :cond_5
} // :goto_0
return;
} // .end method
private void registerAppStatusListener ( ) {
/* .locals 3 */
/* .line 343 */
try { // :try_start_0
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 344 */
v1 = this.mAppStatusListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerAppStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerAppStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 347 */
/* .line 345 */
/* :catch_0 */
/* move-exception v0 */
/* .line 346 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLSAppLib"; // const-string v2, "SLM-SRV-SLSAppLib"
android.util.Log .e ( v2,v1 );
/* .line 348 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private Boolean setMiWillGameStart ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 267 */
v0 = this.mSLMService;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* .line 268 */
/* .local v0, "service":Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* if-nez v0, :cond_0 */
/* .line 269 */
final String v1 = "SLM-SRV-SLSAppLib"; // const-string v1, "SLM-SRV-SLSAppLib"
/* const-string/jumbo v2, "setMiWillGameStart get SLAService null!" */
android.util.Log .e ( v1,v2 );
/* .line 270 */
int v1 = 0; // const/4 v1, 0x0
/* .line 273 */
} // :cond_0
v1 = (( com.xiaomi.NetworkBoost.slaservice.SLAService ) v0 ).setMiWillGameStart ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMiWillGameStart(Ljava/lang/String;)Z
} // .end method
private Boolean setMiWillGameStop ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 277 */
v0 = this.mSLMService;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* .line 278 */
/* .local v0, "service":Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* if-nez v0, :cond_0 */
/* .line 279 */
final String v1 = "SLM-SRV-SLSAppLib"; // const-string v1, "SLM-SRV-SLSAppLib"
/* const-string/jumbo v2, "setMiWillGameStop get SLAService null!" */
android.util.Log .e ( v1,v2 );
/* .line 280 */
int v1 = 0; // const/4 v1, 0x0
/* .line 283 */
} // :cond_0
v1 = (( com.xiaomi.NetworkBoost.slaservice.SLAService ) v0 ).setMiWillGameStop ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMiWillGameStop(Ljava/lang/String;)Z
} // .end method
private void setPowerMgrGameInfo ( Boolean p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "status" # Z */
/* .param p2, "uid" # I */
/* .line 132 */
/* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameStatus:Z */
/* .line 133 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 134 */
/* iput p2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameUid:I */
/* .line 136 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameUid:I */
/* .line 138 */
} // :goto_0
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrCheck()V */
/* .line 139 */
return;
} // .end method
private void setPowerMgrVoIPInfo ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "status" # Z */
/* .line 142 */
/* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrVoIPStatus:Z */
/* .line 143 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrCheck()V */
/* .line 144 */
return;
} // .end method
public static void setSLAAppWhiteList ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "uidList" # Ljava/lang/String; */
/* .line 193 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setSLAAppWhiteList:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLSAppLib"; // const-string v1, "SLM-SRV-SLSAppLib"
android.util.Log .i ( v1,v0 );
/* .line 194 */
/* if-nez p0, :cond_0 */
/* .line 195 */
return;
/* .line 197 */
} // :cond_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib.mSlaAppList;
(( java.util.HashSet ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
/* .line 199 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p0 ).split ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 200 */
/* .local v0, "temp":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_1 */
/* .line 201 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib.mSlaAppList;
/* aget-object v3, v0, v1 */
(( java.util.HashSet ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 200 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 203 */
} // .end local v1 # "i":I
} // :cond_1
return;
} // .end method
private Boolean setSLSGameStart ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 288 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSEnable:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_5
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSGameRunning:Z */
/* if-nez v0, :cond_5 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSVoIPRunning:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 292 */
} // :cond_0
final String v0 = "SLM-SRV-SLSAppLib"; // const-string v0, "SLM-SRV-SLSAppLib"
/* if-nez p1, :cond_1 */
/* .line 293 */
/* const-string/jumbo v2, "setSLSGameStart null" */
android.util.Log .e ( v0,v2 );
/* .line 294 */
/* .line 297 */
} // :cond_1
/* const-string/jumbo v2, "setSLSGameStart" */
v2 = /* invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->checkHAL(Ljava/lang/String;)Z */
/* if-nez v2, :cond_2 */
/* .line 298 */
/* .line 300 */
} // :cond_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setSLSGameStart:" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v0,v2 );
/* .line 301 */
v2 = (( com.xiaomi.NetworkBoost.slaservice.SLSAppLib ) p0 ).setWifiBSSID ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setWifiBSSID()Z
/* if-nez v2, :cond_3 */
/* .line 302 */
} // :cond_3
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSGameRunning:Z */
/* .line 303 */
java.lang.Integer .valueOf ( p1 );
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 304 */
com.xiaomi.NetworkBoost.slaservice.SLATrack .sendMsgSlsStart ( );
/* .line 307 */
try { // :try_start_0
v3 = /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setMiWillGameStart(Ljava/lang/String;)Z */
/* if-nez v3, :cond_4 */
/* .line 308 */
v3 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib.mSlaService;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 313 */
} // :cond_4
/* nop */
/* .line 315 */
/* .line 310 */
/* :catch_0 */
/* move-exception v2 */
/* .line 311 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Exception:"; // const-string v4, "Exception:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v3 );
/* .line 312 */
/* .line 289 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_5
} // :goto_0
} // .end method
private Boolean setSLSGameStop ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 319 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSGameRunning:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 320 */
/* .line 323 */
} // :cond_0
/* const-string/jumbo v0, "setSLSGameStop" */
v0 = /* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->checkHAL(Ljava/lang/String;)Z */
/* if-nez v0, :cond_1 */
/* .line 324 */
/* .line 326 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setSLSGameStop:" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLSAppLib"; // const-string v2, "SLM-SRV-SLSAppLib"
android.util.Log .i ( v2,v0 );
/* .line 327 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSGameRunning:Z */
/* .line 328 */
/* .line 329 */
com.xiaomi.NetworkBoost.slaservice.SLATrack .sendMsgSlsStop ( );
/* .line 331 */
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setMiWillGameStop(Ljava/lang/String;)Z */
/* .line 332 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib.mSlaService;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 336 */
/* nop */
/* .line 338 */
int v0 = 1; // const/4 v0, 0x1
/* .line 333 */
/* :catch_0 */
/* move-exception v0 */
/* .line 334 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Exception:"; // const-string v4, "Exception:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 335 */
} // .end method
public static void setSLSGameUidList ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "uidList" # Ljava/lang/String; */
/* .line 206 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setSLSGameUidList:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLSAppLib"; // const-string v1, "SLM-SRV-SLSAppLib"
android.util.Log .i ( v1,v0 );
/* .line 207 */
/* if-nez p0, :cond_0 */
/* .line 208 */
return;
/* .line 210 */
} // :cond_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib.mSlsAppList;
(( java.util.HashSet ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
/* .line 212 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p0 ).split ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 213 */
/* .local v0, "temp":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_1 */
/* .line 214 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib.mSlsAppList;
/* aget-object v3, v0, v1 */
(( java.util.HashSet ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 213 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 216 */
} // .end local v1 # "i":I
} // :cond_1
return;
} // .end method
private void setSLSVoIPStart ( ) {
/* .locals 4 */
/* .line 438 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSGameRunning:Z */
/* if-nez v0, :cond_4 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSVoIPRunning:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 442 */
} // :cond_0
/* const-string/jumbo v1, "setSLSVoIPStart:" */
final String v2 = "SLM-SRV-SLSAppLib"; // const-string v2, "SLM-SRV-SLSAppLib"
/* if-gtz v0, :cond_1 */
/* .line 443 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v0 );
/* .line 444 */
return;
/* .line 447 */
} // :cond_1
/* const-string/jumbo v0, "setSLSVoIPStart" */
v0 = /* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->checkHAL(Ljava/lang/String;)Z */
/* if-nez v0, :cond_2 */
/* .line 448 */
return;
/* .line 450 */
} // :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v2,v0 );
/* .line 451 */
v0 = (( com.xiaomi.NetworkBoost.slaservice.SLSAppLib ) p0 ).setWifiBSSID ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setWifiBSSID()Z
/* if-nez v0, :cond_3 */
return;
/* .line 452 */
} // :cond_3
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSVoIPRunning:Z */
/* .line 454 */
try { // :try_start_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib.mSlaService;
java.lang.Integer .toString ( v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 457 */
/* .line 455 */
/* :catch_0 */
/* move-exception v0 */
/* .line 456 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Exception:"; // const-string v3, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v1 );
/* .line 458 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 439 */
} // :cond_4
} // :goto_1
return;
} // .end method
private void setSLSVoIPStop ( ) {
/* .locals 4 */
/* .line 461 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSVoIPRunning:Z */
/* if-nez v0, :cond_0 */
/* .line 462 */
return;
/* .line 465 */
} // :cond_0
/* const-string/jumbo v0, "setSLSVoIPStop" */
v0 = /* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->checkHAL(Ljava/lang/String;)Z */
/* if-nez v0, :cond_1 */
/* .line 466 */
return;
/* .line 468 */
} // :cond_1
/* const-string/jumbo v0, "setSLSVoIPStop:" */
final String v1 = "SLM-SRV-SLSAppLib"; // const-string v1, "SLM-SRV-SLSAppLib"
android.util.Log .i ( v1,v0 );
/* .line 469 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSVoIPRunning:Z */
/* .line 471 */
try { // :try_start_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib.mSlaService;
final String v2 = "0"; // const-string v2, "0"
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 474 */
/* .line 472 */
/* :catch_0 */
/* move-exception v0 */
/* .line 473 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Exception:"; // const-string v3, "Exception:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 475 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public static void setSLSVoIPUid ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "uid" # I */
/* .line 219 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setSLSVoIPUid:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLSAppLib"; // const-string v1, "SLM-SRV-SLSAppLib"
android.util.Log .i ( v1,v0 );
/* .line 220 */
/* .line 221 */
return;
} // .end method
/* # virtual methods */
public void setPowerMgrScreenInfo ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "status" # Z */
/* .line 147 */
/* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrScreenOn:Z */
/* .line 148 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrCheck()V */
/* .line 149 */
return;
} // .end method
public void setSLSEnableStatus ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 176 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setSLSEnableStatus:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLSAppLib"; // const-string v1, "SLM-SRV-SLSAppLib"
android.util.Log .i ( v1,v0 );
/* .line 177 */
/* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSEnable:Z */
/* .line 178 */
/* if-nez p1, :cond_1 */
/* .line 179 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSGameRunning:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 180 */
java.lang.Integer .toString ( v0 );
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSGameStop(Ljava/lang/String;)Z */
/* .line 182 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSVoIPRunning:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 183 */
v0 = this.mHandler;
/* const/16 v1, 0x64 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 184 */
v0 = this.mHandler;
/* const/16 v1, 0x65 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 185 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSVoIPStop()V */
/* .line 188 */
} // :cond_1
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->checkSLSStatus()V */
/* .line 190 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void setSlaService ( vendor.qti.sla.service.V1_0.ISlaService p0 ) {
/* .locals 0 */
/* .param p1, "service" # Lvendor/qti/sla/service/V1_0/ISlaService; */
/* .line 172 */
/* .line 173 */
return;
} // .end method
public Boolean setWifiBSSID ( ) {
/* .locals 9 */
/* .line 232 */
final String v0 = "SLM-SRV-SLSAppLib"; // const-string v0, "SLM-SRV-SLSAppLib"
/* const-string/jumbo v1, "setWifiBSSID" */
v1 = /* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->checkHAL(Ljava/lang/String;)Z */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 233 */
/* .line 236 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 237 */
/* .local v1, "BSSID":Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .line 238 */
/* .local v3, "wifiBSSID":Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .line 240 */
/* .local v4, "slavewifiBSSID":Ljava/lang/String; */
try { // :try_start_0
v5 = this.mContext;
/* const-string/jumbo v6, "wifi" */
(( android.content.Context ) v5 ).getSystemService ( v6 ); // invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v5, Landroid/net/wifi/WifiManager; */
/* .line 241 */
/* .local v5, "wifimanager":Landroid/net/wifi/WifiManager; */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 242 */
(( android.net.wifi.WifiManager ) v5 ).getConnectionInfo ( ); // invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;
(( android.net.wifi.WifiInfo ) v6 ).getBSSID ( ); // invoke-virtual {v6}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;
/* move-object v3, v6 */
/* .line 245 */
} // :cond_1
v6 = this.mContext;
final String v7 = "SlaveWifiService"; // const-string v7, "SlaveWifiService"
(( android.content.Context ) v6 ).getSystemService ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v6, Landroid/net/wifi/SlaveWifiManager; */
/* .line 246 */
/* .local v6, "slavewifimanager":Landroid/net/wifi/SlaveWifiManager; */
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 247 */
(( android.net.wifi.SlaveWifiManager ) v6 ).getWifiSlaveConnectionInfo ( ); // invoke-virtual {v6}, Landroid/net/wifi/SlaveWifiManager;->getWifiSlaveConnectionInfo()Landroid/net/wifi/WifiInfo;
(( android.net.wifi.WifiInfo ) v7 ).getBSSID ( ); // invoke-virtual {v7}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;
/* move-object v4, v7 */
/* .line 250 */
} // :cond_2
/* if-nez v3, :cond_3 */
/* if-nez v4, :cond_3 */
/* .line 251 */
/* const-string/jumbo v7, "setWifiBSSID:null" */
android.util.Log .e ( v0,v7 );
/* .line 252 */
/* .line 255 */
} // :cond_3
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ","; // const-string v8, ","
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v1, v7 */
/* .line 256 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "setWifiBSSID:" */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v0,v7 );
/* .line 257 */
v7 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib.mSlaService;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 261 */
} // .end local v1 # "BSSID":Ljava/lang/String;
} // .end local v3 # "wifiBSSID":Ljava/lang/String;
} // .end local v4 # "slavewifiBSSID":Ljava/lang/String;
} // .end local v5 # "wifimanager":Landroid/net/wifi/WifiManager;
} // .end local v6 # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
/* nop */
/* .line 263 */
int v0 = 1; // const/4 v0, 0x1
/* .line 258 */
/* :catch_0 */
/* move-exception v1 */
/* .line 259 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Exception:"; // const-string v4, "Exception:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v3 );
/* .line 260 */
} // .end method
