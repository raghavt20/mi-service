.class Lcom/xiaomi/NetworkBoost/slaservice/SLAService$3;
.super Landroid/content/BroadcastReceiver;
.source "SLAService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 984
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$3;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 987
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 988
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 989
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmWifiReady()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSlaveWifiReady()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$3;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmMiWillManager(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 990
    const-string v1, "SLM-SRV-SLAService"

    const-string v2, "Dual wifi roaming,check miwill status"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 991
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$3;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmMiWillManager(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    move-result-object v1

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmWifiReady()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSlaveWifiReady()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v1, v2}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->setDualWifiReady(Z)V

    .line 994
    :cond_1
    return-void
.end method
