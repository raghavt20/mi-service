.class Lcom/xiaomi/NetworkBoost/slaservice/SLAService$5;
.super Ljava/lang/Object;
.source "SLAService.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 1085
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$5;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVPNStatusChange(Z)V
    .locals 2
    .param p1, "vpnEnable"    # Z

    .line 1088
    const-string v0, "SLM-SRV-SLAService"

    if-eqz p1, :cond_0

    .line 1089
    const-string v1, "VPN CONNECTED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1090
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$5;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLM()V

    goto :goto_0

    .line 1092
    :cond_0
    const-string v1, "VPN DISCONNECTED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1093
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$5;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->enableSLM()V

    .line 1095
    :goto_0
    return-void
.end method
