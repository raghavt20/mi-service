.class Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;
.super Landroid/os/Handler;
.source "SLAService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;


# direct methods
.method public constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 847
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 848
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 849
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .line 853
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "Exception:"

    const/4 v2, 0x1

    const-wide/16 v3, 0x2710

    const-string v5, "SLM-SRV-SLAService"

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_2

    .line 921
    :pswitch_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    const-string v1, "/sys/class/thermal/thermal_message/board_sensor_temp"

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$mchecktemp(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Ljava/lang/String;)V

    .line 922
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2

    .line 910
    :pswitch_2
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$misSlaRemind(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$mgetLinkTurboAppsTotalDayTraffic(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)J

    move-result-wide v6

    const-wide/16 v8, 0x12c

    cmp-long v0, v6, v8

    if-lez v0, :cond_0

    .line 912
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$mpostTrafficNotification(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 915
    goto :goto_0

    .line 913
    :catch_0
    move-exception v0

    .line 914
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 917
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x6c

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 918
    goto/16 :goto_2

    .line 901
    :pswitch_3
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSLSAppLib()Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 902
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSLSAppLib()Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setPowerMgrScreenInfo(Z)V

    goto/16 :goto_2

    .line 905
    :cond_1
    const-string v0, "EVENT_SCREEN_OFF mSLSAppLib is null"

    invoke-static {v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    goto/16 :goto_2

    .line 892
    :pswitch_4
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSLSAppLib()Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 893
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSLSAppLib()Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setPowerMgrScreenInfo(Z)V

    goto/16 :goto_2

    .line 896
    :cond_2
    const-string v0, "EVENT_SCREEN_ON mSLSAppLib is null"

    invoke-static {v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    goto/16 :goto_2

    .line 888
    :pswitch_5
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$mhandleDisableSLM(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V

    .line 889
    goto/16 :goto_2

    .line 884
    :pswitch_6
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$mhandleEnableSLM(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V

    .line 885
    goto/16 :goto_2

    .line 859
    :pswitch_7
    :try_start_1
    invoke-static {}, Lvendor/qti/sla/service/V1_0/ISlaService;->getService()Lvendor/qti/sla/service/V1_0/ISlaService;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfputmSlaService(Lvendor/qti/sla/service/V1_0/ISlaService;)V

    .line 861
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSlaService()Lvendor/qti/sla/service/V1_0/ISlaService;

    move-result-object v0

    if-nez v0, :cond_3

    .line 862
    const-string v0, "HAL service is null"

    invoke-static {v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 863
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    const/16 v6, 0x67

    invoke-virtual {v3, v6, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v6, 0xfa0

    invoke-virtual {v0, v3, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    .line 866
    :cond_3
    const-string v0, "HAL service get success"

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 867
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSlaService()Lvendor/qti/sla/service/V1_0/ISlaService;

    move-result-object v0

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmDeathRecipient(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Landroid/os/IHwBinder$DeathRecipient;

    move-result-object v3

    const-wide/16 v6, 0x0

    invoke-interface {v0, v3, v6, v7}, Lvendor/qti/sla/service/V1_0/ISlaService;->linkToDeath(Landroid/os/IHwBinder$DeathRecipient;J)Z

    .line 870
    :goto_1
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSlaService()Lvendor/qti/sla/service/V1_0/ISlaService;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSLAAppLib()Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 871
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSLAAppLib()Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSlaService()Lvendor/qti/sla/service/V1_0/ISlaService;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setSlaService(Lvendor/qti/sla/service/V1_0/ISlaService;)V

    .line 873
    :cond_4
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSlaService()Lvendor/qti/sla/service/V1_0/ISlaService;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSLSAppLib()Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 874
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSLSAppLib()Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSlaService()Lvendor/qti/sla/service/V1_0/ISlaService;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSlaService(Lvendor/qti/sla/service/V1_0/ISlaService;)V

    .line 876
    :cond_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_6

    .line 877
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 881
    :cond_6
    goto :goto_2

    .line 879
    :catch_1
    move-exception v0

    .line 880
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_2

    .line 855
    :pswitch_8
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    const-string v1, "ON_USER"

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$mprocessNetworkCallback(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Ljava/lang/String;)V

    .line 856
    nop

    .line 926
    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
