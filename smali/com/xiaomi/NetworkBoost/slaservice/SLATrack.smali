.class public Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;
.super Ljava/lang/Object;
.source "SLATrack.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final METRICS_DIR:Ljava/lang/String; = "sla"

.field private static final METRICS_PREFS_FILE:Ljava/lang/String; = "sla_track"

.field private static final MILLIS_ONE_DAY:J = 0x5265bffL

.field private static final MSG_DOUBLEWIFI_SLA_START:I = 0x7

.field private static final MSG_DOUBLEWIFI_SLA_STOP:I = 0x8

.field private static final MSG_SLA_START:I = 0x3

.field private static final MSG_SLA_STOP:I = 0x4

.field private static final MSG_SLS_START:I = 0x5

.field private static final MSG_SLS_STOP:I = 0x6

.field private static final MSG_START_MONITOR:I = 0x2

.field private static final MSG_START_REPORT:I = 0x1

.field private static final ON_AVAILABLE:I = 0x1

.field private static final PREFERENCE_NAME:Ljava/lang/String; = "sla_track"

.field private static final STR_DBSLA_DURATION:Ljava/lang/String; = "dbsla_duration"

.field private static final STR_DBSLA_STATE:Ljava/lang/String; = "dbsla"

.field private static final STR_SLA_DURATION:Ljava/lang/String; = "sla_duration"

.field private static final STR_SLA_STATE:Ljava/lang/String; = "sla"

.field private static final STR_SLS_DURATION:Ljava/lang/String; = "sls_duration"

.field private static final STR_SLS_STATE:Ljava/lang/String; = "sls"

.field private static final TAG:Ljava/lang/String; = "SLM-SRV-SLATrack"

.field private static mSLATrack:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

.field private static mWorkHandler:Landroid/os/Handler;


# instance fields
.field private DoubleWifiAndSLAStartTime:J

.field private SLAStartTime:J

.field private SLSStartTime:J

.field private mContext:Landroid/content/Context;

.field private mMetricsPrefsFile:Ljava/io/File;

.field private mWorkThread:Landroid/os/HandlerThread;


# direct methods
.method static bridge synthetic -$$Nest$fgetDoubleWifiAndSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->DoubleWifiAndSLAStartTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->SLAStartTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetSLSStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->SLSStartTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMetricsPrefsFile(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)Ljava/io/File;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mMetricsPrefsFile:Ljava/io/File;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputDoubleWifiAndSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;J)V
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->DoubleWifiAndSLAStartTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;J)V
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->SLAStartTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputSLSStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;J)V
    .locals 0

    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->SLSStartTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMetricsPrefsFile(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mMetricsPrefsFile:Ljava/io/File;

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->getTime()J

    move-result-wide v0

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$sfgetmWorkHandler()Landroid/os/Handler;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mWorkHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mSLATrack:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->SLAStartTime:J

    .line 49
    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->SLSStartTime:J

    .line 50
    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->DoubleWifiAndSLAStartTime:J

    .line 64
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mContext:Landroid/content/Context;

    .line 65
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SLM-SRV-SLATrack"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mWorkThread:Landroid/os/HandlerThread;

    .line 66
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 67
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mWorkThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;Landroid/os/Looper;)V

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mWorkHandler:Landroid/os/Handler;

    .line 68
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/32 v2, 0x5265bff

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 70
    return-void
.end method

.method public static getSLATrack(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 57
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mSLATrack:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mSLATrack:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    .line 60
    :cond_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mSLATrack:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    return-object v0
.end method

.method private getTime()J
    .locals 4

    .line 165
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 166
    .local v0, "totalMilliSeconds":J
    const-wide/32 v2, 0xea60

    div-long v2, v0, v2

    return-wide v2
.end method

.method public static sendMsgDoubleWifiAndSLAStart()V
    .locals 4

    .line 180
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mWorkHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 182
    return-void
.end method

.method public static sendMsgDoubleWifiAndSLAStop()V
    .locals 4

    .line 195
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mWorkHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 197
    return-void
.end method

.method public static sendMsgSlaStart()V
    .locals 4

    .line 170
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mWorkHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 172
    return-void
.end method

.method public static sendMsgSlaStop()V
    .locals 4

    .line 185
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mWorkHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 187
    return-void
.end method

.method public static sendMsgSlsStart()V
    .locals 4

    .line 175
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mWorkHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 177
    return-void
.end method

.method public static sendMsgSlsStop()V
    .locals 4

    .line 190
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mWorkHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 192
    return-void
.end method


# virtual methods
.method public reportMQSEvent(Landroid/content/SharedPreferences;)V
    .locals 5
    .param p1, "prefs"    # Landroid/content/SharedPreferences;

    .line 200
    const-string v0, "SLM-SRV-SLATrack"

    const-string v1, "reportSLATrackEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onetrack.action.TRACK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 203
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 204
    const-string v1, "APP_ID"

    const-string v2, "31000000060"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 205
    const-string v1, "EVENT_NAME"

    const-string v2, "SLM"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    const-string v1, "PACKAGE"

    const-string v2, "com.qti.slaservice"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 207
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 208
    .local v1, "params":Landroid/os/Bundle;
    const-string/jumbo v2, "sla"

    const/4 v3, 0x0

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 209
    const-string/jumbo v2, "sls"

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 210
    const-string v2, "dbsla"

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 211
    const-string/jumbo v2, "sla_duration"

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 212
    const-string/jumbo v2, "sls_duration"

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 213
    const-string v2, "dbsla_duration"

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 214
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 215
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 216
    return-void
.end method
