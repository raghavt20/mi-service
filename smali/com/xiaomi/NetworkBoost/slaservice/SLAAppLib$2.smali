.class Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$2;
.super Landroid/os/Handler;
.source "SLAAppLib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 212
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 214
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    .line 215
    .local v0, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 232
    :pswitch_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$msetDoubleDataUidToSlad(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V

    .line 233
    goto :goto_0

    .line 230
    :pswitch_1
    goto :goto_0

    .line 227
    :pswitch_2
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$msetDoubleWifiUidToSlad(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V

    .line 228
    goto :goto_0

    .line 224
    :pswitch_3
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v1, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$mdbUpdateSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V

    .line 225
    goto :goto_0

    .line 221
    :pswitch_4
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v1, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$mdbUpdateSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V

    .line 222
    goto :goto_0

    .line 217
    :pswitch_5
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v1, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$mdbAddSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V

    .line 218
    nop

    .line 238
    :goto_0
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$sfputmIsUidChangeReboot(Z)V

    .line 239
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$msetUidWhiteListToSlad(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V

    .line 240
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->setLinkTurboUidList(Ljava/lang/String;)V

    .line 241
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLAAppWhiteList(Ljava/lang/String;)V

    .line 242
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$sfputmIsUidChangeReboot(Z)V

    .line 243
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
