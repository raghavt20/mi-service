.class Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;
.super Landroid/content/BroadcastReceiver;
.source "SLAAppLib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initBroadcastReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    .line 1250
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1253
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1254
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    .line 1255
    .local v1, "packageName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1256
    return-void

    .line 1259
    :cond_0
    const-string v2, "android.intent.extra.UID"

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1260
    .local v2, "uid":I
    if-ne v2, v3, :cond_1

    .line 1261
    return-void

    .line 1264
    :cond_1
    const-string v3, "SLM-SRV-SLAAppLib"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initBroadcastReceiver"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    const-string v3, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_a

    .line 1266
    const-string v3, "SLM-SRV-SLAAppLib"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACTION_PACKAGE_ADDED uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1267
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1268
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$maddSLAAppDefault(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;I)V

    .line 1269
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    .line 1270
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    goto :goto_0

    .line 1272
    :cond_2
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v6}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    goto :goto_0

    .line 1274
    :cond_3
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLAAppDefaultPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1275
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$maddSLAAppDefault(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;I)V

    .line 1277
    :cond_4
    :goto_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleWifiAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1278
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_5

    .line 1279
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    goto :goto_1

    .line 1281
    :cond_5
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v6}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    .line 1284
    :cond_6
    :goto_1
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleDataAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1285
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleDataUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_7

    .line 1286
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmDoubleDataUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    goto :goto_2

    .line 1288
    :cond_7
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v6}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleDataUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmDoubleDataUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    .line 1292
    :cond_8
    :goto_2
    const-string v3, "com.tencent.mm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 1293
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v3

    const-string v5, "com.tencent.mm"

    invoke-interface {v3, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1294
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;I)V

    goto/16 :goto_3

    .line 1296
    :cond_9
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;I)V

    goto/16 :goto_3

    .line 1300
    :cond_a
    const-string v3, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 1301
    const-string v3, "SLM-SRV-SLAAppLib"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACTION_PACKAGE_REMOVED uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1302
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$sfgetmSLAAppLibLock()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 1303
    :try_start_0
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$mgetSLAAppByUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    move-result-object v5

    if-eqz v5, :cond_b

    .line 1304
    const-string v5, "SLM-SRV-SLAAppLib"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "package upgrade store, uid = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1305
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v5, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$mstoreSLAAppUpgrade(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    .line 1307
    :cond_b
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1308
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$mclearSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    .line 1309
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$mdeleteSLAAppDefault(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;I)V

    .line 1310
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_c

    .line 1311
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    .line 1313
    :cond_c
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_d

    .line 1314
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    .line 1316
    :cond_d
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleDataUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_e

    .line 1317
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleDataUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmDoubleDataUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    .line 1320
    :cond_e
    const-string v3, "com.tencent.mm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 1321
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;I)V

    goto :goto_3

    .line 1307
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 1323
    :cond_f
    const-string v3, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 1324
    const-string v3, "SLM-SRV-SLAAppLib"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_PACKAGE_REPLACED uid = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1325
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$mrestoreSLAAppUpgrade(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 1326
    const-string v3, "SLM-SRV-SLAAppLib"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "package upgrade restore, uid = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1327
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgAddSLAUid(Ljava/lang/String;)Z

    goto :goto_3

    .line 1329
    :cond_10
    const-string v3, "com.android.phone.intent.action.DUAL_DATA_CONCURRENT_MODE_WHITE_LIST_DONE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    const-string v3, "com.android.phone.intent.action.DUAL_DATA_CONCURRENT_LIMITED_WHITE_LIST_DONE"

    .line 1330
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 1331
    :cond_11
    const-string v3, "SLM-SRV-SLAAppLib"

    const-string v4, "DUAL_DATA_COCURRENT_WHITE_LIST CHANGE"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1332
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$mfetchDoubleDataWhiteListApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V

    .line 1335
    :cond_12
    :goto_3
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$msendMsgSetSLAAppList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Z

    .line 1336
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)I

    move-result v3

    if-eqz v3, :cond_13

    .line 1337
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    .line 1338
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ",,"

    const-string v6, ","

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fputmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V

    .line 1340
    :cond_13
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSGameUidList(Ljava/lang/String;)V

    .line 1341
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)I

    move-result v3

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSVoIPUid(I)V

    .line 1344
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->setNetworkAccelerateSwitchUidList(Ljava/lang/String;)V

    .line 1345
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)I

    move-result v3

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->setNetworkAccelerateSwitchVoIPUid(I)V

    .line 1346
    const-string v3, "SLM-SRV-SLAAppLib"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mDoubleWifiUidList = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1347
    const-string v3, "SLM-SRV-SLAAppLib"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mDoubleDataUidList = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmDoubleDataUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1348
    return-void
.end method
