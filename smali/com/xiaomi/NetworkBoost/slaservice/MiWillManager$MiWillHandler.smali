.class Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$MiWillHandler;
.super Landroid/os/Handler;
.source "MiWillManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MiWillHandler"
.end annotation


# static fields
.field public static final GET_MIWILL_HAL:I = 0x65

.field public static final RECHECK_MIWILL_STATUS:I = 0x67

.field public static final SET_MIWILL_STATUS:I = 0x66

.field public static final UPDATE_STATUS:I = 0x64


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;


# direct methods
.method public constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 575
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$MiWillHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    .line 576
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 577
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 580
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handle: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MIWILL-MiWillManager"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 602
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MiWillHandler handle unexpected code:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 596
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 597
    .local v0, "retry_cnt":I
    add-int/lit8 v0, v0, -0x1

    .line 598
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$MiWillHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    const/4 v2, 0x1

    invoke-static {v1, v2, v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->-$$Nest$msetMiwillStatus(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;ZI)V

    .line 599
    goto :goto_0

    .line 592
    .end local v0    # "retry_cnt":I
    :pswitch_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$MiWillHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->-$$Nest$msetMiwillStatus(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;ZI)V

    .line 593
    goto :goto_0

    .line 588
    :pswitch_2
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$MiWillHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->-$$Nest$minitMiWillHal(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)V

    .line 589
    goto :goto_0

    .line 584
    :pswitch_3
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$MiWillHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->-$$Nest$mupdateStatus(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)V

    .line 585
    nop

    .line 605
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
