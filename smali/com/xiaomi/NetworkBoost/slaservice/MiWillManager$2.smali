.class Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$2;
.super Landroid/database/ContentObserver;
.source "MiWillManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->registerSwitchObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 281
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 285
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onChange selfChange = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " uri = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MIWILL-MiWillManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 290
    :cond_0
    return-void
.end method
