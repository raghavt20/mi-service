class com.xiaomi.NetworkBoost.slaservice.SLAService$SlaServiceDeathRecipient implements android.os.IHwBinder$DeathRecipient {
	 /* .source "SLAService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "SlaServiceDeathRecipient" */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.SLAService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.slaservice.SLAService$SlaServiceDeathRecipient ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* .line 478 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void serviceDied ( Long p0 ) {
/* .locals 4 */
/* .param p1, "cookie" # J */
/* .line 481 */
final String v0 = "SLM-SRV-SLAService"; // const-string v0, "SLM-SRV-SLAService"
final String v1 = "HAL service died"; // const-string v1, "HAL service died"
android.util.Log .e ( v0,v1 );
/* .line 482 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmHandler ( v1 );
int v2 = 1; // const/4 v2, 0x1
java.lang.Integer .valueOf ( v2 );
/* const/16 v3, 0x67 */
(( android.os.Handler ) v1 ).obtainMessage ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* const-wide/16 v2, 0xfa0 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 484 */
return;
} // .end method
