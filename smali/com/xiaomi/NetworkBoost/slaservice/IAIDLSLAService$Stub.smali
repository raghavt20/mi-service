.class public abstract Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub;
.super Landroid/os/Binder;
.source "IAIDLSLAService.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub$Proxy;
    }
.end annotation


# static fields
.field static final TRANSACTION_addUidToLinkTurboWhiteList:I = 0x1

.field static final TRANSACTION_getLinkTurboAppsTraffic:I = 0x4

.field static final TRANSACTION_getLinkTurboDefaultPn:I = 0x6

.field static final TRANSACTION_getLinkTurboWhiteList:I = 0x3

.field static final TRANSACTION_removeUidInLinkTurboWhiteList:I = 0x2

.field static final TRANSACTION_setLinkTurboEnable:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 44
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 45
    const-string v0, "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"

    invoke-virtual {p0, p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .line 53
    if-nez p0, :cond_0

    .line 54
    const/4 v0, 0x0

    return-object v0

    .line 56
    :cond_0
    const-string v0, "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 57
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService;

    if-eqz v1, :cond_1

    .line 58
    move-object v1, v0

    check-cast v1, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService;

    return-object v1

    .line 60
    :cond_1
    new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub$Proxy;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    return-object v1
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .line 64
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 68
    const-string v0, "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"

    .line 69
    .local v0, "descriptor":Ljava/lang/String;
    const/4 v1, 0x1

    if-lt p1, v1, :cond_0

    const v2, 0xffffff

    if-gt p1, v2, :cond_0

    .line 70
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 72
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 80
    packed-switch p1, :pswitch_data_1

    .line 135
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    return v1

    .line 76
    :pswitch_0
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 77
    return v1

    .line 128
    :pswitch_1
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub;->getLinkTurboDefaultPn()Ljava/util/List;

    move-result-object v2

    .line 129
    .local v2, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 130
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 131
    goto :goto_0

    .line 119
    .end local v2    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :pswitch_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v2

    .line 120
    .local v2, "_arg0":Z
    invoke-virtual {p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 121
    invoke-virtual {p0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub;->setLinkTurboEnable(Z)Z

    move-result v3

    .line 122
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 123
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 124
    goto :goto_0

    .line 111
    .end local v2    # "_arg0":Z
    .end local v3    # "_result":Z
    :pswitch_3
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub;->getLinkTurboAppsTraffic()Ljava/util/List;

    move-result-object v2

    .line 112
    .local v2, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 113
    invoke-virtual {p3, v2, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;I)V

    .line 114
    goto :goto_0

    .line 104
    .end local v2    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;"
    :pswitch_4
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub;->getLinkTurboWhiteList()Ljava/lang/String;

    move-result-object v2

    .line 105
    .local v2, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 106
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 107
    goto :goto_0

    .line 95
    .end local v2    # "_result":Ljava/lang/String;
    :pswitch_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 97
    invoke-virtual {p0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub;->removeUidInLinkTurboWhiteList(Ljava/lang/String;)Z

    move-result v3

    .line 98
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 99
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 100
    goto :goto_0

    .line 85
    .end local v2    # "_arg0":Ljava/lang/String;
    .end local v3    # "_result":Z
    :pswitch_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 86
    .restart local v2    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 87
    invoke-virtual {p0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub;->addUidToLinkTurboWhiteList(Ljava/lang/String;)Z

    move-result v3

    .line 88
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 89
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 90
    nop

    .line 138
    .end local v2    # "_arg0":Ljava/lang/String;
    .end local v3    # "_result":Z
    :goto_0
    return v1

    :pswitch_data_0
    .packed-switch 0x5f4e5446
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
