public class com.xiaomi.NetworkBoost.slaservice.SLATrack {
	 /* .source "SLATrack.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
private static final java.lang.String METRICS_DIR;
private static final java.lang.String METRICS_PREFS_FILE;
private static final Long MILLIS_ONE_DAY;
private static final Integer MSG_DOUBLEWIFI_SLA_START;
private static final Integer MSG_DOUBLEWIFI_SLA_STOP;
private static final Integer MSG_SLA_START;
private static final Integer MSG_SLA_STOP;
private static final Integer MSG_SLS_START;
private static final Integer MSG_SLS_STOP;
private static final Integer MSG_START_MONITOR;
private static final Integer MSG_START_REPORT;
private static final Integer ON_AVAILABLE;
private static final java.lang.String PREFERENCE_NAME;
private static final java.lang.String STR_DBSLA_DURATION;
private static final java.lang.String STR_DBSLA_STATE;
private static final java.lang.String STR_SLA_DURATION;
private static final java.lang.String STR_SLA_STATE;
private static final java.lang.String STR_SLS_DURATION;
private static final java.lang.String STR_SLS_STATE;
private static final java.lang.String TAG;
private static com.xiaomi.NetworkBoost.slaservice.SLATrack mSLATrack;
private static android.os.Handler mWorkHandler;
/* # instance fields */
private Long DoubleWifiAndSLAStartTime;
private Long SLAStartTime;
private Long SLSStartTime;
private android.content.Context mContext;
private java.io.File mMetricsPrefsFile;
private android.os.HandlerThread mWorkThread;
/* # direct methods */
static Long -$$Nest$fgetDoubleWifiAndSLAStartTime ( com.xiaomi.NetworkBoost.slaservice.SLATrack p0 ) { //bridge//synthethic
	 /* .locals 2 */
	 /* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->DoubleWifiAndSLAStartTime:J */
	 /* return-wide v0 */
} // .end method
static Long -$$Nest$fgetSLAStartTime ( com.xiaomi.NetworkBoost.slaservice.SLATrack p0 ) { //bridge//synthethic
	 /* .locals 2 */
	 /* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->SLAStartTime:J */
	 /* return-wide v0 */
} // .end method
static Long -$$Nest$fgetSLSStartTime ( com.xiaomi.NetworkBoost.slaservice.SLATrack p0 ) { //bridge//synthethic
	 /* .locals 2 */
	 /* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->SLSStartTime:J */
	 /* return-wide v0 */
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.xiaomi.NetworkBoost.slaservice.SLATrack p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static java.io.File -$$Nest$fgetmMetricsPrefsFile ( com.xiaomi.NetworkBoost.slaservice.SLATrack p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mMetricsPrefsFile;
} // .end method
static void -$$Nest$fputDoubleWifiAndSLAStartTime ( com.xiaomi.NetworkBoost.slaservice.SLATrack p0, Long p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->DoubleWifiAndSLAStartTime:J */
	 return;
} // .end method
static void -$$Nest$fputSLAStartTime ( com.xiaomi.NetworkBoost.slaservice.SLATrack p0, Long p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->SLAStartTime:J */
	 return;
} // .end method
static void -$$Nest$fputSLSStartTime ( com.xiaomi.NetworkBoost.slaservice.SLATrack p0, Long p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->SLSStartTime:J */
	 return;
} // .end method
static void -$$Nest$fputmMetricsPrefsFile ( com.xiaomi.NetworkBoost.slaservice.SLATrack p0, java.io.File p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mMetricsPrefsFile = p1;
	 return;
} // .end method
static Long -$$Nest$mgetTime ( com.xiaomi.NetworkBoost.slaservice.SLATrack p0 ) { //bridge//synthethic
	 /* .locals 2 */
	 /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->getTime()J */
	 /* move-result-wide v0 */
	 /* return-wide v0 */
} // .end method
static android.os.Handler -$$Nest$sfgetmWorkHandler ( ) { //bridge//synthethic
	 /* .locals 1 */
	 v0 = com.xiaomi.NetworkBoost.slaservice.SLATrack.mWorkHandler;
} // .end method
static com.xiaomi.NetworkBoost.slaservice.SLATrack ( ) {
	 /* .locals 1 */
	 /* .line 44 */
	 int v0 = 0; // const/4 v0, 0x0
	 return;
} // .end method
private com.xiaomi.NetworkBoost.slaservice.SLATrack ( ) {
	 /* .locals 4 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 63 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 48 */
	 /* const-wide/16 v0, 0x0 */
	 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->SLAStartTime:J */
	 /* .line 49 */
	 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->SLSStartTime:J */
	 /* .line 50 */
	 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->DoubleWifiAndSLAStartTime:J */
	 /* .line 64 */
	 this.mContext = p1;
	 /* .line 65 */
	 /* new-instance v0, Landroid/os/HandlerThread; */
	 final String v1 = "SLM-SRV-SLATrack"; // const-string v1, "SLM-SRV-SLATrack"
	 /* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
	 this.mWorkThread = v0;
	 /* .line 66 */
	 (( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
	 /* .line 67 */
	 /* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler; */
	 v1 = this.mWorkThread;
	 (( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;Landroid/os/Looper;)V */
	 /* .line 68 */
	 int v1 = 2; // const/4 v1, 0x2
	 (( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
	 /* const-wide/32 v2, 0x5265bff */
	 (( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
	 /* .line 70 */
	 return;
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.SLATrack getSLATrack ( android.content.Context p0 ) {
	 /* .locals 1 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .line 57 */
	 v0 = com.xiaomi.NetworkBoost.slaservice.SLATrack.mSLATrack;
	 /* if-nez v0, :cond_0 */
	 /* .line 58 */
	 /* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack; */
	 /* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;-><init>(Landroid/content/Context;)V */
	 /* .line 60 */
} // :cond_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLATrack.mSLATrack;
} // .end method
private Long getTime ( ) {
/* .locals 4 */
/* .line 165 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 166 */
/* .local v0, "totalMilliSeconds":J */
/* const-wide/32 v2, 0xea60 */
/* div-long v2, v0, v2 */
/* return-wide v2 */
} // .end method
public static void sendMsgDoubleWifiAndSLAStart ( ) {
/* .locals 4 */
/* .line 180 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLATrack.mWorkHandler;
int v1 = 7; // const/4 v1, 0x7
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* const-wide/16 v2, 0x1 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 182 */
return;
} // .end method
public static void sendMsgDoubleWifiAndSLAStop ( ) {
/* .locals 4 */
/* .line 195 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLATrack.mWorkHandler;
/* const/16 v1, 0x8 */
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* const-wide/16 v2, 0x1 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 197 */
return;
} // .end method
public static void sendMsgSlaStart ( ) {
/* .locals 4 */
/* .line 170 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLATrack.mWorkHandler;
int v1 = 3; // const/4 v1, 0x3
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* const-wide/16 v2, 0x1 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 172 */
return;
} // .end method
public static void sendMsgSlaStop ( ) {
/* .locals 4 */
/* .line 185 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLATrack.mWorkHandler;
int v1 = 4; // const/4 v1, 0x4
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* const-wide/16 v2, 0x1 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 187 */
return;
} // .end method
public static void sendMsgSlsStart ( ) {
/* .locals 4 */
/* .line 175 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLATrack.mWorkHandler;
int v1 = 5; // const/4 v1, 0x5
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* const-wide/16 v2, 0x1 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 177 */
return;
} // .end method
public static void sendMsgSlsStop ( ) {
/* .locals 4 */
/* .line 190 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLATrack.mWorkHandler;
int v1 = 6; // const/4 v1, 0x6
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* const-wide/16 v2, 0x1 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 192 */
return;
} // .end method
/* # virtual methods */
public void reportMQSEvent ( android.content.SharedPreferences p0 ) {
/* .locals 5 */
/* .param p1, "prefs" # Landroid/content/SharedPreferences; */
/* .line 200 */
final String v0 = "SLM-SRV-SLATrack"; // const-string v0, "SLM-SRV-SLATrack"
final String v1 = "reportSLATrackEvent"; // const-string v1, "reportSLATrackEvent"
android.util.Log .i ( v0,v1 );
/* .line 202 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "onetrack.action.TRACK_EVENT"; // const-string v1, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 203 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 204 */
final String v1 = "APP_ID"; // const-string v1, "APP_ID"
final String v2 = "31000000060"; // const-string v2, "31000000060"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 205 */
final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
final String v2 = "SLM"; // const-string v2, "SLM"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 206 */
final String v1 = "PACKAGE"; // const-string v1, "PACKAGE"
final String v2 = "com.qti.slaservice"; // const-string v2, "com.qti.slaservice"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 207 */
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 208 */
/* .local v1, "params":Landroid/os/Bundle; */
/* const-string/jumbo v2, "sla" */
v4 = int v3 = 0; // const/4 v3, 0x0
(( android.content.Intent ) v0 ).putExtra ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 209 */
v4 = /* const-string/jumbo v2, "sls" */
(( android.content.Intent ) v0 ).putExtra ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 210 */
v4 = final String v2 = "dbsla"; // const-string v2, "dbsla"
(( android.content.Intent ) v0 ).putExtra ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 211 */
v4 = /* const-string/jumbo v2, "sla_duration" */
(( android.content.Intent ) v0 ).putExtra ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 212 */
v4 = /* const-string/jumbo v2, "sls_duration" */
(( android.content.Intent ) v0 ).putExtra ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 213 */
v3 = final String v2 = "dbsla_duration"; // const-string v2, "dbsla_duration"
(( android.content.Intent ) v0 ).putExtra ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 214 */
(( android.content.Intent ) v0 ).putExtras ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 215 */
v2 = this.mContext;
(( android.content.Context ) v2 ).startService ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* .line 216 */
return;
} // .end method
