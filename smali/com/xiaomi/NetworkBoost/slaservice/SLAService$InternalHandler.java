class com.xiaomi.NetworkBoost.slaservice.SLAService$InternalHandler extends android.os.Handler {
	 /* .source "SLAService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "InternalHandler" */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.SLAService this$0; //synthetic
/* # direct methods */
public com.xiaomi.NetworkBoost.slaservice.SLAService$InternalHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 847 */
this.this$0 = p1;
/* .line 848 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 849 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 10 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 853 */
/* iget v0, p1, Landroid/os/Message;->what:I */
final String v1 = "Exception:"; // const-string v1, "Exception:"
int v2 = 1; // const/4 v2, 0x1
/* const-wide/16 v3, 0x2710 */
final String v5 = "SLM-SRV-SLAService"; // const-string v5, "SLM-SRV-SLAService"
/* packed-switch v0, :pswitch_data_0 */
/* :pswitch_0 */
/* goto/16 :goto_2 */
/* .line 921 */
/* :pswitch_1 */
v0 = this.this$0;
final String v1 = "/sys/class/thermal/thermal_message/board_sensor_temp"; // const-string v1, "/sys/class/thermal/thermal_message/board_sensor_temp"
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$mchecktemp ( v0,v1 );
/* .line 922 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmHandler ( v0 );
/* const/16 v1, 0x6e */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v3, v4 ); // invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* goto/16 :goto_2 */
/* .line 910 */
/* :pswitch_2 */
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$misSlaRemind ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$mgetLinkTurboAppsTotalDayTraffic ( v0 );
	 /* move-result-wide v6 */
	 /* const-wide/16 v8, 0x12c */
	 /* cmp-long v0, v6, v8 */
	 /* if-lez v0, :cond_0 */
	 /* .line 912 */
	 try { // :try_start_0
		 v0 = this.this$0;
		 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$mpostTrafficNotification ( v0 );
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 915 */
		 /* .line 913 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 914 */
		 /* .local v0, "e":Ljava/lang/Exception; */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .e ( v5,v1 );
		 /* .line 917 */
	 } // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmHandler ( v1 );
/* const/16 v2, 0x6c */
(( android.os.Handler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v3, v4 ); // invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 918 */
/* goto/16 :goto_2 */
/* .line 901 */
/* :pswitch_3 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSLSAppLib ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 902 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSLSAppLib ( );
int v1 = 0; // const/4 v1, 0x0
(( com.xiaomi.NetworkBoost.slaservice.SLSAppLib ) v0 ).setPowerMgrScreenInfo ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setPowerMgrScreenInfo(Z)V
/* goto/16 :goto_2 */
/* .line 905 */
} // :cond_1
final String v0 = "EVENT_SCREEN_OFF mSLSAppLib is null"; // const-string v0, "EVENT_SCREEN_OFF mSLSAppLib is null"
android.util.Log .e ( v5,v0 );
/* .line 907 */
/* goto/16 :goto_2 */
/* .line 892 */
/* :pswitch_4 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSLSAppLib ( );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 893 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSLSAppLib ( );
(( com.xiaomi.NetworkBoost.slaservice.SLSAppLib ) v0 ).setPowerMgrScreenInfo ( v2 ); // invoke-virtual {v0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setPowerMgrScreenInfo(Z)V
/* goto/16 :goto_2 */
/* .line 896 */
} // :cond_2
final String v0 = "EVENT_SCREEN_ON mSLSAppLib is null"; // const-string v0, "EVENT_SCREEN_ON mSLSAppLib is null"
android.util.Log .e ( v5,v0 );
/* .line 898 */
/* goto/16 :goto_2 */
/* .line 888 */
/* :pswitch_5 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$mhandleDisableSLM ( v0 );
/* .line 889 */
/* goto/16 :goto_2 */
/* .line 884 */
/* :pswitch_6 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$mhandleEnableSLM ( v0 );
/* .line 885 */
/* goto/16 :goto_2 */
/* .line 859 */
/* :pswitch_7 */
try { // :try_start_1
vendor.qti.sla.service.V1_0.ISlaService .getService ( );
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfputmSlaService ( v0 );
/* .line 861 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSlaService ( );
/* if-nez v0, :cond_3 */
/* .line 862 */
final String v0 = "HAL service is null"; // const-string v0, "HAL service is null"
android.util.Log .e ( v5,v0 );
/* .line 863 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmHandler ( v0 );
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmHandler ( v3 );
v4 = this.obj;
/* const/16 v6, 0x67 */
(( android.os.Handler ) v3 ).obtainMessage ( v6, v4 ); // invoke-virtual {v3, v6, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* const-wide/16 v6, 0xfa0 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v3, v6, v7 ); // invoke-virtual {v0, v3, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 866 */
} // :cond_3
final String v0 = "HAL service get success"; // const-string v0, "HAL service get success"
android.util.Log .d ( v5,v0 );
/* .line 867 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSlaService ( );
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmDeathRecipient ( v3 );
/* const-wide/16 v6, 0x0 */
/* .line 870 */
} // :goto_1
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSlaService ( );
if ( v0 != null) { // if-eqz v0, :cond_4
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSLAAppLib ( );
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 871 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSLAAppLib ( );
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSlaService ( );
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v0 ).setSlaService ( v3 ); // invoke-virtual {v0, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setSlaService(Lvendor/qti/sla/service/V1_0/ISlaService;)V
/* .line 873 */
} // :cond_4
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSlaService ( );
if ( v0 != null) { // if-eqz v0, :cond_5
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSLSAppLib ( );
if ( v0 != null) { // if-eqz v0, :cond_5
	 /* .line 874 */
	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSLSAppLib ( );
	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSlaService ( );
	 (( com.xiaomi.NetworkBoost.slaservice.SLSAppLib ) v0 ).setSlaService ( v3 ); // invoke-virtual {v0, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSlaService(Lvendor/qti/sla/service/V1_0/ISlaService;)V
	 /* .line 876 */
} // :cond_5
v0 = this.obj;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* if-ne v0, v2, :cond_6 */
/* .line 877 */
/* const/16 v0, 0x64 */
(( com.xiaomi.NetworkBoost.slaservice.SLAService$InternalHandler ) p0 ).obtainMessage ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->obtainMessage(I)Landroid/os/Message;
(( com.xiaomi.NetworkBoost.slaservice.SLAService$InternalHandler ) p0 ).sendMessage ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;->sendMessage(Landroid/os/Message;)Z
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 881 */
} // :cond_6
/* .line 879 */
/* :catch_1 */
/* move-exception v0 */
/* .line 880 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v5,v1 );
/* .line 882 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 855 */
/* :pswitch_8 */
v0 = this.this$0;
final String v1 = "ON_USER"; // const-string v1, "ON_USER"
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$mprocessNetworkCallback ( v0,v1 );
/* .line 856 */
/* nop */
/* .line 926 */
} // :goto_2
return;
/* :pswitch_data_0 */
/* .packed-switch 0x64 */
/* :pswitch_8 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
