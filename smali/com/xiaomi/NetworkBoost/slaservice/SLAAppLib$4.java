class com.xiaomi.NetworkBoost.slaservice.SLAAppLib$4 extends android.database.ContentObserver {
	 /* .source "SLAAppLib.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLMCloudObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.SLAAppLib this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.slaservice.SLAAppLib$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 1090 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 19 */
/* .param p1, "selfChange" # Z */
/* .line 1094 */
/* move-object/from16 v1, p0 */
final String v2 = "SLM-SRV-SLAAppLib"; // const-string v2, "SLM-SRV-SLAAppLib"
try { // :try_start_0
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmContext ( v0 );
	 (( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
	 /* .line 1095 */
	 /* .local v0, "pm":Landroid/content/pm/PackageManager; */
	 int v3 = 1; // const/4 v3, 0x1
	 (( android.content.pm.PackageManager ) v0 ).getInstalledPackages ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;
	 /* .line 1097 */
	 /* .local v4, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
	 v5 = android.os.Build.DEVICE;
	 (( java.lang.String ) v5 ).toLowerCase ( ); // invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
	 /* .line 1098 */
	 /* .local v5, "device":Ljava/lang/String; */
	 v6 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameAppPN ( v6 );
	 /* .line 1099 */
	 v6 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleWifiAppPN ( v6 );
	 /* .line 1100 */
	 v6 = this.this$0;
	 int v7 = 0; // const/4 v7, 0x0
	 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSGameUidList ( v6,v7 );
	 /* .line 1101 */
	 v6 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmDoubleWifiUidList ( v6,v7 );
	 /* .line 1104 */
	 v6 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmContext ( v6 );
	 /* .line 1105 */
	 (( android.content.Context ) v6 ).getContentResolver ( ); // invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 final String v7 = "cloud_sls_whitelist"; // const-string v7, "cloud_sls_whitelist"
	 /* .line 1104 */
	 android.provider.Settings$System .getString ( v6,v7 );
	 /* .line 1106 */
	 /* .local v6, "whiteString_sls":Ljava/lang/String; */
	 /* new-instance v7, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v8 = "Cloud SLSApp: "; // const-string v8, "Cloud SLSApp: "
	 (( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .d ( v2,v7 );
	 /* .line 1107 */
	 v7 = 	 android.text.TextUtils .isEmpty ( v6 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 final String v8 = ","; // const-string v8, ","
	 /* if-nez v7, :cond_1 */
	 /* .line 1108 */
	 try { // :try_start_1
		 (( java.lang.String ) v6 ).split ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
		 /* .line 1109 */
		 /* .local v7, "packages":[Ljava/lang/String; */
		 if ( v7 != null) { // if-eqz v7, :cond_1
			 /* .line 1110 */
			 int v9 = 0; // const/4 v9, 0x0
			 /* .local v9, "i":I */
		 } // :goto_0
		 /* array-length v10, v7 */
		 /* if-ge v9, v10, :cond_1 */
		 /* .line 1111 */
		 /* aget-object v10, v7, v9 */
		 final String v11 = "miwill"; // const-string v11, "miwill"
		 v10 = 		 (( java.lang.String ) v10 ).startsWith ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v10, :cond_0 */
		 /* .line 1112 */
		 v10 = this.this$0;
		 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameAppPN ( v10 );
		 /* aget-object v11, v7, v9 */
		 /* .line 1110 */
	 } // :cond_0
	 /* add-int/lit8 v9, v9, 0x1 */
	 /* .line 1119 */
} // .end local v7 # "packages":[Ljava/lang/String;
} // .end local v9 # "i":I
} // :cond_1
v7 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmContext ( v7 );
/* .line 1120 */
(( android.content.Context ) v7 ).getContentResolver ( ); // invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v9 = "cloud_double_wifi_uidlist"; // const-string v9, "cloud_double_wifi_uidlist"
/* .line 1119 */
android.provider.Settings$System .getString ( v7,v9 );
/* .line 1121 */
/* .local v7, "whiteString_doubleWifi":Ljava/lang/String; */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Cloud DWApp: "; // const-string v10, "Cloud DWApp: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v9 );
/* .line 1122 */
v9 = android.text.TextUtils .isEmpty ( v7 );
/* if-nez v9, :cond_2 */
/* .line 1123 */
(( java.lang.String ) v7 ).split ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1124 */
/* .local v9, "packages":[Ljava/lang/String; */
if ( v9 != null) { // if-eqz v9, :cond_2
/* .line 1125 */
int v10 = 0; // const/4 v10, 0x0
/* .local v10, "i":I */
} // :goto_1
/* array-length v11, v9 */
/* if-ge v10, v11, :cond_2 */
/* .line 1126 */
v11 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleWifiAppPN ( v11 );
/* aget-object v12, v9, v10 */
/* .line 1125 */
/* add-int/lit8 v10, v10, 0x1 */
/* .line 1132 */
} // .end local v9 # "packages":[Ljava/lang/String;
} // .end local v10 # "i":I
} // :cond_2
v9 = this.this$0;
v9 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameAppPN ( v9 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
final String v10 = "ro.boot.hwc"; // const-string v10, "ro.boot.hwc"
final String v11 = "CN"; // const-string v11, "CN"
final String v12 = "com.tencent.mm"; // const-string v12, "com.tencent.mm"
int v14 = 2; // const/4 v14, 0x2
int v15 = 4; // const/4 v15, 0x4
int v13 = 0; // const/4 v13, 0x0
if ( v9 != null) { // if-eqz v9, :cond_4
/* .line 1134 */
try { // :try_start_2
android.os.SystemProperties .get ( v10 );
v9 = (( java.lang.String ) v11 ).equalsIgnoreCase ( v9 ); // invoke-virtual {v11, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v9 != null) { // if-eqz v9, :cond_3
/* .line 1135 */
/* new-array v9, v15, [Ljava/lang/String; */
final String v17 = "com.tencent.tmgp.pubgmhd"; // const-string v17, "com.tencent.tmgp.pubgmhd"
/* aput-object v17, v9, v13 */
final String v17 = "com.tencent.tmgp.sgame"; // const-string v17, "com.tencent.tmgp.sgame"
/* aput-object v17, v9, v3 */
final String v17 = "com.tencent.tmgp.speedmobile"; // const-string v17, "com.tencent.tmgp.speedmobile"
/* aput-object v17, v9, v14 */
/* const/16 v16, 0x3 */
/* aput-object v12, v9, v16 */
/* .restart local v9 # "packages":[Ljava/lang/String; */
/* .line 1138 */
} // .end local v9 # "packages":[Ljava/lang/String;
} // :cond_3
/* new-array v9, v3, [Ljava/lang/String; */
final String v17 = "com.test"; // const-string v17, "com.test"
/* aput-object v17, v9, v13 */
/* .line 1140 */
/* .restart local v9 # "packages":[Ljava/lang/String; */
} // :goto_2
/* nop */
/* .line 1141 */
/* const/16 v17, 0x0 */
/* move/from16 v15, v17 */
/* .local v15, "i":I */
} // :goto_3
/* array-length v14, v9 */
/* if-ge v15, v14, :cond_4 */
/* .line 1142 */
v14 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameAppPN ( v14 );
/* aget-object v3, v9, v15 */
/* .line 1141 */
/* add-int/lit8 v15, v15, 0x1 */
int v3 = 1; // const/4 v3, 0x1
int v14 = 2; // const/4 v14, 0x2
/* .line 1146 */
} // .end local v9 # "packages":[Ljava/lang/String;
} // .end local v15 # "i":I
} // :cond_4
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "set SLSApp: " */
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameAppPN ( v9 );
(( java.lang.Object ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 1147 */
v9 = } // :goto_4
if ( v9 != null) { // if-eqz v9, :cond_a
/* check-cast v9, Landroid/content/pm/PackageInfo; */
/* .line 1149 */
/* .local v9, "app":Landroid/content/pm/PackageInfo; */
v14 = this.packageName;
if ( v14 != null) { // if-eqz v14, :cond_7
v14 = this.applicationInfo;
if ( v14 != null) { // if-eqz v14, :cond_7
v14 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameAppPN ( v14 );
v15 = this.packageName;
v14 = /* .line 1150 */
if ( v14 != null) { // if-eqz v14, :cond_6
/* .line 1151 */
v14 = this.applicationInfo;
/* iget v14, v14, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 1152 */
/* .local v14, "uid":I */
v15 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$maddSLAAppDefault ( v15,v14 );
/* .line 1153 */
v15 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameUidList ( v15 );
/* if-nez v15, :cond_5 */
/* .line 1154 */
v15 = this.this$0;
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v18, v0 */
} // .end local v0 # "pm":Landroid/content/pm/PackageManager;
/* .local v18, "pm":Landroid/content/pm/PackageManager; */
java.lang.Integer .toString ( v14 );
(( java.lang.StringBuilder ) v13 ).append ( v0 ); // invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSGameUidList ( v15,v0 );
/* .line 1156 */
} // .end local v18 # "pm":Landroid/content/pm/PackageManager;
/* .restart local v0 # "pm":Landroid/content/pm/PackageManager; */
} // :cond_5
/* move-object/from16 v18, v0 */
} // .end local v0 # "pm":Landroid/content/pm/PackageManager;
/* .restart local v18 # "pm":Landroid/content/pm/PackageManager; */
v0 = this.this$0;
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
v15 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameUidList ( v15 );
(( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toString ( v14 );
(( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v8 ); // invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSGameUidList ( v0,v13 );
/* .line 1150 */
} // .end local v14 # "uid":I
} // .end local v18 # "pm":Landroid/content/pm/PackageManager;
/* .restart local v0 # "pm":Landroid/content/pm/PackageManager; */
} // :cond_6
/* move-object/from16 v18, v0 */
} // .end local v0 # "pm":Landroid/content/pm/PackageManager;
/* .restart local v18 # "pm":Landroid/content/pm/PackageManager; */
/* .line 1149 */
} // .end local v18 # "pm":Landroid/content/pm/PackageManager;
/* .restart local v0 # "pm":Landroid/content/pm/PackageManager; */
} // :cond_7
/* move-object/from16 v18, v0 */
/* .line 1161 */
} // .end local v0 # "pm":Landroid/content/pm/PackageManager;
/* .restart local v18 # "pm":Landroid/content/pm/PackageManager; */
} // :goto_5
v0 = this.packageName;
if ( v0 != null) { // if-eqz v0, :cond_9
v0 = this.packageName;
/* .line 1162 */
v0 = (( java.lang.String ) v0 ).equals ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 1163 */
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameAppPN ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 1164 */
v0 = this.this$0;
v13 = this.applicationInfo;
/* iget v13, v13, Landroid/content/pm/ApplicationInfo;->uid:I */
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSVoIPUid ( v0,v13 );
/* .line 1166 */
} // :cond_8
v0 = this.this$0;
int v13 = 0; // const/4 v13, 0x0
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSVoIPUid ( v0,v13 );
/* .line 1169 */
} // .end local v9 # "app":Landroid/content/pm/PackageInfo;
} // :cond_9
} // :goto_6
/* move-object/from16 v0, v18 */
int v13 = 0; // const/4 v13, 0x0
/* goto/16 :goto_4 */
/* .line 1170 */
} // .end local v18 # "pm":Landroid/content/pm/PackageManager;
/* .restart local v0 # "pm":Landroid/content/pm/PackageManager; */
} // :cond_a
/* move-object/from16 v18, v0 */
} // .end local v0 # "pm":Landroid/content/pm/PackageManager;
/* .restart local v18 # "pm":Landroid/content/pm/PackageManager; */
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSVoIPUid ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 1171 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameUidList ( v0 );
v9 = this.this$0;
v9 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSVoIPUid ( v9 );
java.lang.Integer .toString ( v9 );
final String v12 = ""; // const-string v12, ""
(( java.lang.String ) v3 ).replace ( v9, v12 ); // invoke-virtual {v3, v9, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSGameUidList ( v0,v3 );
/* .line 1172 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameUidList ( v0 );
final String v9 = ",,"; // const-string v9, ",,"
(( java.lang.String ) v3 ).replace ( v9, v8 ); // invoke-virtual {v3, v9, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSGameUidList ( v0,v3 );
/* .line 1174 */
} // :cond_b
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameUidList ( v0 );
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .setSLSGameUidList ( v0 );
/* .line 1175 */
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSVoIPUid ( v0 );
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .setSLSVoIPUid ( v0 );
/* .line 1178 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameUidList ( v0 );
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .setNetworkAccelerateSwitchUidList ( v0 );
/* .line 1179 */
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSVoIPUid ( v0 );
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .setNetworkAccelerateSwitchVoIPUid ( v0 );
/* .line 1182 */
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleWifiAppPN ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_d
/* .line 1184 */
android.os.SystemProperties .get ( v10 );
v0 = (( java.lang.String ) v11 ).equalsIgnoreCase ( v0 ); // invoke-virtual {v11, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
int v3 = 5; // const/4 v3, 0x5
int v9 = 6; // const/4 v9, 0x6
if ( v0 != null) { // if-eqz v0, :cond_c
/* .line 1185 */
/* const/16 v0, 0xd */
/* new-array v0, v0, [Ljava/lang/String; */
final String v10 = "air.tv.douyu.android"; // const-string v10, "air.tv.douyu.android"
int v11 = 0; // const/4 v11, 0x0
/* aput-object v10, v0, v11 */
final String v10 = "com.duowan.kiwi"; // const-string v10, "com.duowan.kiwi"
int v11 = 1; // const/4 v11, 0x1
/* aput-object v10, v0, v11 */
final String v10 = "com.ss.android.ugc.aweme"; // const-string v10, "com.ss.android.ugc.aweme"
int v11 = 2; // const/4 v11, 0x2
/* aput-object v10, v0, v11 */
final String v10 = "com.taobao.taobao"; // const-string v10, "com.taobao.taobao"
int v11 = 3; // const/4 v11, 0x3
/* aput-object v10, v0, v11 */
final String v10 = "com.tmall.wireless"; // const-string v10, "com.tmall.wireless"
int v11 = 4; // const/4 v11, 0x4
/* aput-object v10, v0, v11 */
final String v10 = "com.jingdong.app.mall"; // const-string v10, "com.jingdong.app.mall"
/* aput-object v10, v0, v3 */
final String v3 = "com.youku.phone"; // const-string v3, "com.youku.phone"
/* aput-object v3, v0, v9 */
final String v3 = "com.qiyi.video"; // const-string v3, "com.qiyi.video"
int v9 = 7; // const/4 v9, 0x7
/* aput-object v3, v0, v9 */
final String v3 = "com.android.providers.downloads.ui"; // const-string v3, "com.android.providers.downloads.ui"
/* const/16 v9, 0x8 */
/* aput-object v3, v0, v9 */
final String v3 = "com.android.providers.downloads"; // const-string v3, "com.android.providers.downloads"
/* const/16 v9, 0x9 */
/* aput-object v3, v0, v9 */
final String v3 = "com.xiaomi.market"; // const-string v3, "com.xiaomi.market"
/* const/16 v9, 0xa */
/* aput-object v3, v0, v9 */
/* const-string/jumbo v3, "tv.danmaku.bili" */
/* const/16 v9, 0xb */
/* aput-object v3, v0, v9 */
final String v3 = "org.zwanoo.android.speedtest"; // const-string v3, "org.zwanoo.android.speedtest"
/* const/16 v9, 0xc */
/* aput-object v3, v0, v9 */
/* .local v0, "packages":[Ljava/lang/String; */
/* .line 1190 */
} // .end local v0 # "packages":[Ljava/lang/String;
} // :cond_c
/* new-array v0, v9, [Ljava/lang/String; */
final String v9 = "com.spotify.music"; // const-string v9, "com.spotify.music"
int v10 = 0; // const/4 v10, 0x0
/* aput-object v9, v0, v10 */
final String v9 = "com.ebay.mobile"; // const-string v9, "com.ebay.mobile"
int v10 = 1; // const/4 v10, 0x1
/* aput-object v9, v0, v10 */
final String v9 = "com.amazon.kindle"; // const-string v9, "com.amazon.kindle"
int v10 = 2; // const/4 v10, 0x2
/* aput-object v9, v0, v10 */
final String v9 = "com.instagram.android"; // const-string v9, "com.instagram.android"
int v10 = 3; // const/4 v10, 0x3
/* aput-object v9, v0, v10 */
final String v9 = "com.melodis.midomiMusicIdentifier.freemium"; // const-string v9, "com.melodis.midomiMusicIdentifier.freemium"
int v10 = 4; // const/4 v10, 0x4
/* aput-object v9, v0, v10 */
final String v9 = "com.google.android.youtube"; // const-string v9, "com.google.android.youtube"
/* aput-object v9, v0, v3 */
/* .line 1195 */
/* .restart local v0 # "packages":[Ljava/lang/String; */
} // :goto_7
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_8
/* array-length v9, v0 */
/* if-ge v3, v9, :cond_d */
/* .line 1196 */
v9 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleWifiAppPN ( v9 );
/* aget-object v10, v0, v3 */
/* .line 1195 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1199 */
} // .end local v0 # "packages":[Ljava/lang/String;
} // .end local v3 # "i":I
} // :cond_d
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "set DWApp: " */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleWifiAppPN ( v3 );
(( java.lang.Object ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v0 );
/* .line 1200 */
v3 = } // :goto_9
if ( v3 != null) { // if-eqz v3, :cond_10
/* check-cast v3, Landroid/content/pm/PackageInfo; */
/* .line 1201 */
/* .local v3, "app":Landroid/content/pm/PackageInfo; */
v9 = this.packageName;
if ( v9 != null) { // if-eqz v9, :cond_f
v9 = this.applicationInfo;
if ( v9 != null) { // if-eqz v9, :cond_f
v9 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleWifiAppPN ( v9 );
v10 = this.packageName;
v9 = /* .line 1202 */
if ( v9 != null) { // if-eqz v9, :cond_f
/* .line 1203 */
v9 = this.applicationInfo;
/* iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 1204 */
/* .local v9, "uid":I */
v10 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleWifiUidList ( v10 );
/* if-nez v10, :cond_e */
/* .line 1205 */
v10 = this.this$0;
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.Integer .toString ( v9 );
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v8 ); // invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmDoubleWifiUidList ( v10,v11 );
/* .line 1207 */
} // :cond_e
v10 = this.this$0;
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
v12 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleWifiUidList ( v12 );
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toString ( v9 );
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v8 ); // invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmDoubleWifiUidList ( v10,v11 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 1210 */
} // .end local v3 # "app":Landroid/content/pm/PackageInfo;
} // .end local v9 # "uid":I
} // :cond_f
} // :goto_a
/* .line 1213 */
} // .end local v4 # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
} // .end local v5 # "device":Ljava/lang/String;
} // .end local v6 # "whiteString_sls":Ljava/lang/String;
} // .end local v7 # "whiteString_doubleWifi":Ljava/lang/String;
} // .end local v18 # "pm":Landroid/content/pm/PackageManager;
} // :cond_10
/* .line 1211 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1212 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "initSLMCloudObserver onChange error "; // const-string v4, "initSLMCloudObserver onChange error "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 1214 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_b
return;
} // .end method
