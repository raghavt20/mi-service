.class Lcom/xiaomi/NetworkBoost/slaservice/SLAService$7;
.super Ljava/lang/Object;
.source "SLAService.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 1357
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$7;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isModemSingnalStrengthStatus(I)V
    .locals 3
    .param p1, "status"    # I

    .line 1364
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isModemSingnalStrengthStatus = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLAService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1365
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$7;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmSingnalPoor(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1367
    const-string v2, "isModemSingnalStrengthStatus setMobileDataAlwaysOff"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1368
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$7;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v1, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fputmSingnalPoor(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Z)V

    .line 1369
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$7;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$msetMobileDataAlwaysOff(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V

    goto :goto_0

    .line 1370
    :cond_0
    if-nez p1, :cond_1

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmLinkTurboSwitch()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$7;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetdisableSLMCnt(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)I

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmDataReady()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$7;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmSingnalPoor(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1373
    const-string v0, "isModemSingnalStrengthStatus setMobileDataAlwaysOn"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$7;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fputmSingnalPoor(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Z)V

    .line 1375
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$7;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$msetMobileDataAlwaysOn(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V

    .line 1377
    :cond_1
    :goto_0
    return-void
.end method
