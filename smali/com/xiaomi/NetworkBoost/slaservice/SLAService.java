public class com.xiaomi.NetworkBoost.slaservice.SLAService {
	 /* .source "SLAService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/NetworkBoost/slaservice/SLAService$SlaServiceDeathRecipient;, */
	 /* Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;, */
	 /* Lcom/xiaomi/NetworkBoost/slaservice/SLAService$WatchdogHandler; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String ACTION_START_SERVICE;
private static final java.lang.String CHANNEL_ID;
private static final java.lang.String CLOUD_DOUBLE_WIFI_MONITOR;
private static final java.lang.String CLOUD_MIWILL_CONTROLLER_LIST;
private static final java.lang.String CLOUD_MIWILL_DUAL_WIFI_OFF;
private static final java.lang.String CLOUD_MIWILL_DUAL_WIFI_ON;
private static final java.lang.String CLOUD_MIWILL_OFF;
private static final java.lang.String CLOUD_MIWILL_ON;
private static final java.lang.String COEXISTENSE;
private static final Integer EVENT_DISABLE_SLM;
private static final Integer EVENT_ENABLE_SLM;
private static final Integer EVENT_GET_HAL;
private static final Integer EVENT_GET_THERMAL;
private static final Integer EVENT_SCREEN_OFF;
private static final Integer EVENT_SCREEN_ON;
private static final Integer EVENT_TRAFFIC_CHECK;
private static final Integer EVENT_UPDATE_SLAD_STATE;
private static final Integer EVENT_WATCH_DOG_TIMEOUT;
private static final Integer GET_SERVICE_DELAY_MILLIS;
private static final Integer GET_THERMAL_DELAY_MILIS;
private static final java.lang.String NOTIFACATION_RECEIVER_PACKAGE;
private static final Integer NOTIFICATION_ID_1;
private static final Integer NOTIFICATION_ID_2;
private static final java.lang.String ON_AVAILABLE;
private static final java.lang.String ON_DISABLE_EVENT;
private static final java.lang.String ON_LOST;
private static final java.lang.String ON_USER;
private static final java.lang.String PROP_MIWILL_DUAL_WIFI_ENABLE;
public static final Integer REMIND_OFF;
public static final Integer REMIND_ON;
private static final Integer SCREEN_OFF_DELAY_MIILLIS;
private static final Integer SETTING_VALUE_OFF;
private static final Integer SETTING_VALUE_ON;
private static final Integer SIGNAL_STRENGTH_GOOD;
private static final Integer SIGNAL_STRENGTH_POOR;
private static final java.lang.String SLAD_STOP;
private static final java.lang.String SLA_NOTIFICATION_STATE;
private static final java.lang.String SLM_FEATURE_STATUS;
private static final java.lang.String TAG;
private static final Integer THERMAL_CNT;
private static final Long THERMAL_DISABLE;
private static final Long THERMAL_ENABLE;
private static final java.lang.String THERMAL_MODE_FILE;
private static final Integer THERMAL_PERFORMANCE_MODE;
private static final java.lang.String THERMAL_STATE_FILE;
private static final Integer TRAFFIC_LIMIT;
private static final Long TRAFFIC_TIMER;
private static final Integer WATCH_DOG_TIMEOUT;
private static android.content.Context mContext;
private static Boolean mDataReady;
private static Integer mIfaceNumber;
private static java.lang.String mInterface;
private static Boolean mLinkTurboSwitch;
private static com.xiaomi.NetworkBoost.slaservice.SLAAppLib mSLAAppLib;
private static com.xiaomi.NetworkBoost.slaservice.SLATrack mSLATrack;
private static com.xiaomi.NetworkBoost.slaservice.SLSAppLib mSLSAppLib;
private static final java.util.concurrent.Semaphore mSemaphore;
public static final java.lang.Object mSlaLock;
private static vendor.qti.sla.service.V1_0.ISlaService mSlaService;
private static com.xiaomi.NetworkBoost.slaservice.SLAToast mSlaToast;
private static Boolean mSlaveDataReady;
private static Boolean mSlaveWifiReady;
private static Boolean mWifiReady;
private static volatile com.xiaomi.NetworkBoost.slaservice.SLAService sInstance;
private static Boolean sladEnabled;
/* # instance fields */
private final java.lang.String LINK_TURBO_ENABLE_PREF;
private Integer disableSLMCnt;
private android.database.ContentObserver mCloudObserver;
private android.content.BroadcastReceiver mConnectivityChangedReceiver;
private android.os.IHwBinder$DeathRecipient mDeathRecipient;
private com.xiaomi.NetworkBoost.StatusManager$IDefaultNetworkInterfaceListener mDefaultNetworkInterfaceListener;
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
private com.xiaomi.NetworkBoost.slaservice.MiWillManager mMiWillManager;
private com.xiaomi.NetworkBoost.StatusManager$IModemSignalStrengthListener mModemSignalStrengthListener;
private com.xiaomi.NetworkBoost.StatusManager$INetworkInterfaceListener mNetworkInterfaceListener;
public android.app.NotificationManager mNotificationManager;
private java.util.List mSLAApp;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.xiaomi.NetworkBoost.StatusManager$IScreenStatusListener mScreenStatusListener;
private Boolean mSingnalPoor;
private android.net.wifi.SlaveWifiManager mSlaveWifiManager;
private com.xiaomi.NetworkBoost.StatusManager mStatusManager;
private java.util.ArrayList mThermalList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.xiaomi.NetworkBoost.StatusManager$IVPNListener mVPNListener;
private android.os.Handler mWacthdogHandler;
private android.os.HandlerThread mWacthdogHandlerThread;
private android.net.wifi.WifiManager mWifiManager;
private final android.content.BroadcastReceiver mWifiStateReceiver;
private Boolean misEnableDWMonitor;
private Integer mlastWlanWwanCoexistanceStatus;
private Boolean thermal_enable_slm;
/* # direct methods */
static Integer -$$Nest$fgetdisableSLMCnt ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I */
} // .end method
static android.os.IHwBinder$DeathRecipient -$$Nest$fgetmDeathRecipient ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDeathRecipient;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static com.xiaomi.NetworkBoost.slaservice.MiWillManager -$$Nest$fgetmMiWillManager ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiWillManager;
} // .end method
static Boolean -$$Nest$fgetmSingnalPoor ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSingnalPoor:Z */
} // .end method
static void -$$Nest$fputmMiWillManager ( com.xiaomi.NetworkBoost.slaservice.SLAService p0, com.xiaomi.NetworkBoost.slaservice.MiWillManager p1 ) { //bridge//synthethic
/* .locals 0 */
this.mMiWillManager = p1;
return;
} // .end method
static void -$$Nest$fputmSingnalPoor ( com.xiaomi.NetworkBoost.slaservice.SLAService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSingnalPoor:Z */
return;
} // .end method
static Boolean -$$Nest$mcheckMiwillCloudController ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->checkMiwillCloudController()Z */
} // .end method
static void -$$Nest$mchecktemp ( com.xiaomi.NetworkBoost.slaservice.SLAService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->checktemp(Ljava/lang/String;)V */
return;
} // .end method
static Long -$$Nest$mgetLinkTurboAppsTotalDayTraffic ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 2 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->getLinkTurboAppsTotalDayTraffic()J */
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
static void -$$Nest$mhandleDisableSLM ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->handleDisableSLM()V */
return;
} // .end method
static void -$$Nest$mhandleEnableSLM ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->handleEnableSLM()V */
return;
} // .end method
static void -$$Nest$mhandleScreenOff ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->handleScreenOff()V */
return;
} // .end method
static void -$$Nest$mhandleScreenOn ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->handleScreenOn()V */
return;
} // .end method
static void -$$Nest$misEnableDWMonitor ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isEnableDWMonitor()V */
return;
} // .end method
static Boolean -$$Nest$misSlaRemind ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSlaRemind()Z */
} // .end method
static void -$$Nest$mpostTrafficNotification ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->postTrafficNotification()V */
return;
} // .end method
static void -$$Nest$mprocessNetworkCallback ( com.xiaomi.NetworkBoost.slaservice.SLAService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->processNetworkCallback(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$msetMobileDataAlwaysOff ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMobileDataAlwaysOff()V */
return;
} // .end method
static void -$$Nest$msetMobileDataAlwaysOn ( com.xiaomi.NetworkBoost.slaservice.SLAService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMobileDataAlwaysOn()V */
return;
} // .end method
static android.content.Context -$$Nest$sfgetmContext ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
} // .end method
static Boolean -$$Nest$sfgetmDataReady ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDataReady:Z */
} // .end method
static Integer -$$Nest$sfgetmIfaceNumber ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static java.lang.String -$$Nest$sfgetmInterface ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mInterface;
} // .end method
static Boolean -$$Nest$sfgetmLinkTurboSwitch ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mLinkTurboSwitch:Z */
} // .end method
static com.xiaomi.NetworkBoost.slaservice.SLAAppLib -$$Nest$sfgetmSLAAppLib ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
} // .end method
static com.xiaomi.NetworkBoost.slaservice.SLSAppLib -$$Nest$sfgetmSLSAppLib ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLSAppLib;
} // .end method
static vendor.qti.sla.service.V1_0.ISlaService -$$Nest$sfgetmSlaService ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaService;
} // .end method
static Boolean -$$Nest$sfgetmSlaveDataReady ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveDataReady:Z */
} // .end method
static Boolean -$$Nest$sfgetmSlaveWifiReady ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiReady:Z */
} // .end method
static Boolean -$$Nest$sfgetmWifiReady ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiReady:Z */
} // .end method
static void -$$Nest$sfputmDataReady ( Boolean p0 ) { //bridge//synthethic
/* .locals 0 */
com.xiaomi.NetworkBoost.slaservice.SLAService.mDataReady = (p0!= 0);
return;
} // .end method
static void -$$Nest$sfputmIfaceNumber ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static void -$$Nest$sfputmInterface ( java.lang.String p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static void -$$Nest$sfputmSlaService ( vendor.qti.sla.service.V1_0.ISlaService p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static void -$$Nest$sfputmSlaveDataReady ( Boolean p0 ) { //bridge//synthethic
/* .locals 0 */
com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaveDataReady = (p0!= 0);
return;
} // .end method
static void -$$Nest$sfputmSlaveWifiReady ( Boolean p0 ) { //bridge//synthethic
/* .locals 0 */
com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaveWifiReady = (p0!= 0);
return;
} // .end method
static void -$$Nest$sfputmWifiReady ( Boolean p0 ) { //bridge//synthethic
/* .locals 0 */
com.xiaomi.NetworkBoost.slaservice.SLAService.mWifiReady = (p0!= 0);
return;
} // .end method
static com.xiaomi.NetworkBoost.slaservice.SLAService ( ) {
/* .locals 3 */
/* .line 147 */
int v0 = 0; // const/4 v0, 0x0
com.xiaomi.NetworkBoost.slaservice.SLAService.sladEnabled = (v0!= 0);
/* .line 148 */
/* new-instance v1, Ljava/lang/Object; */
/* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
/* .line 149 */
com.xiaomi.NetworkBoost.slaservice.SLAService.mLinkTurboSwitch = (v0!= 0);
/* .line 161 */
/* new-instance v1, Ljava/util/concurrent/Semaphore; */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {v1, v2}, Ljava/util/concurrent/Semaphore;-><init>(I)V */
/* .line 195 */
com.xiaomi.NetworkBoost.slaservice.SLAService.mWifiReady = (v0!= 0);
/* .line 196 */
com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaveWifiReady = (v0!= 0);
/* .line 197 */
com.xiaomi.NetworkBoost.slaservice.SLAService.mDataReady = (v0!= 0);
/* .line 198 */
com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaveDataReady = (v0!= 0);
/* .line 199 */
final String v1 = ""; // const-string v1, ""
/* .line 200 */
return;
} // .end method
private com.xiaomi.NetworkBoost.slaservice.SLAService ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 369 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 150 */
final String v0 = "linkturbo_is_enable"; // const-string v0, "linkturbo_is_enable"
this.LINK_TURBO_ENABLE_PREF = v0;
/* .line 151 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mSLAApp = v0;
/* .line 186 */
int v0 = 0; // const/4 v0, 0x0
this.mMiWillManager = v0;
/* .line 187 */
this.mCloudObserver = v0;
/* .line 209 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I */
/* .line 217 */
int v2 = -1; // const/4 v2, -0x1
/* iput v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mlastWlanWwanCoexistanceStatus:I */
/* .line 220 */
this.mStatusManager = v0;
/* .line 222 */
this.mWifiManager = v0;
/* .line 223 */
this.mSlaveWifiManager = v0;
/* .line 224 */
this.mNotificationManager = v0;
/* .line 227 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->misEnableDWMonitor:Z */
/* .line 486 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$SlaServiceDeathRecipient; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$SlaServiceDeathRecipient;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V */
this.mDeathRecipient = v0;
/* .line 950 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$2; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$2;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V */
this.mNetworkInterfaceListener = v0;
/* .line 984 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$3; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$3;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V */
this.mWifiStateReceiver = v0;
/* .line 1038 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$4; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$4;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V */
this.mDefaultNetworkInterfaceListener = v0;
/* .line 1085 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$5; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$5;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V */
this.mVPNListener = v0;
/* .line 1119 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$6; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$6;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V */
this.mScreenStatusListener = v0;
/* .line 1250 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->thermal_enable_slm:Z */
/* .line 1252 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mThermalList = v0;
/* .line 1355 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSingnalPoor:Z */
/* .line 1357 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$7; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$7;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V */
this.mModemSignalStrengthListener = v0;
/* .line 370 */
(( android.content.Context ) p1 ).getApplicationContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
/* .line 371 */
return;
} // .end method
private void acquireSlaServiceLock ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "func" # Ljava/lang/String; */
/* .line 806 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " Lock"; // const-string v1, " Lock"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLAService"; // const-string v1, "SLM-SRV-SLAService"
android.util.Log .d ( v1,v0 );
/* .line 808 */
try { // :try_start_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSemaphore;
(( java.util.concurrent.Semaphore ) v0 ).acquire ( ); // invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V
/* .line 809 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " Lock success"; // const-string v2, " Lock success"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 813 */
/* .line 810 */
/* :catch_0 */
/* move-exception v0 */
/* .line 811 */
/* .local v0, "e":Ljava/lang/InterruptedException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "InterruptedException:"; // const-string v3, "InterruptedException:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 812 */
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v1 ).interrupt ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
/* .line 814 */
} // .end local v0 # "e":Ljava/lang/InterruptedException;
} // :goto_0
return;
} // .end method
private Boolean checkMiwillCloudController ( ) {
/* .locals 9 */
/* .line 582 */
final String v0 = "SLM-SRV-SLAService"; // const-string v0, "SLM-SRV-SLAService"
int v1 = 1; // const/4 v1, 0x1
/* .line 584 */
/* .local v1, "isMiwillOn":Z */
try { // :try_start_0
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
/* .line 585 */
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "cloud_miwill_controller_list"; // const-string v3, "cloud_miwill_controller_list"
/* .line 584 */
android.provider.Settings$System .getString ( v2,v3 );
/* .line 586 */
/* .local v2, "miwillControllerList":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v3, :cond_4 */
/* .line 587 */
final String v3 = ","; // const-string v3, ","
(( java.lang.String ) v2 ).split ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 588 */
/* .local v3, "controllers":[Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 589 */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* array-length v5, v3 */
/* if-ge v4, v5, :cond_4 */
/* .line 590 */
/* aget-object v5, v3, v4 */
final String v6 = "miwill_on"; // const-string v6, "miwill_on"
v5 = (( java.lang.String ) v5 ).equals ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 591 */
int v1 = 1; // const/4 v1, 0x1
/* .line 593 */
} // :cond_0
/* aget-object v5, v3, v4 */
final String v6 = "miwill_off"; // const-string v6, "miwill_off"
v5 = (( java.lang.String ) v5 ).equals ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
final String v6 = "false"; // const-string v6, "false"
final String v7 = "persist.log.tag.miwill_dual_wifi_enable"; // const-string v7, "persist.log.tag.miwill_dual_wifi_enable"
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 594 */
try { // :try_start_1
final String v5 = "checkMiwillCloudController miwill dual wifi off because miwill off"; // const-string v5, "checkMiwillCloudController miwill dual wifi off because miwill off"
android.util.Log .i ( v0,v5 );
/* .line 595 */
android.os.SystemProperties .set ( v7,v6 );
/* .line 596 */
int v1 = 0; // const/4 v1, 0x0
/* .line 599 */
} // :cond_1
/* aget-object v5, v3, v4 */
final String v8 = "miwill_dual_wifi_on"; // const-string v8, "miwill_dual_wifi_on"
v5 = (( java.lang.String ) v5 ).equals ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 600 */
final String v5 = "checkMiwillCloudController miwill dual wifi on"; // const-string v5, "checkMiwillCloudController miwill dual wifi on"
android.util.Log .i ( v0,v5 );
/* .line 601 */
/* const-string/jumbo v5, "true" */
android.os.SystemProperties .set ( v7,v5 );
/* .line 603 */
} // :cond_2
/* aget-object v5, v3, v4 */
final String v8 = "miwill_dual_wifi_off"; // const-string v8, "miwill_dual_wifi_off"
v5 = (( java.lang.String ) v5 ).equals ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 604 */
final String v5 = "checkMiwillCloudController miwill dual wifi off"; // const-string v5, "checkMiwillCloudController miwill dual wifi off"
android.util.Log .i ( v0,v5 );
/* .line 605 */
android.os.SystemProperties .set ( v7,v6 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 589 */
} // :cond_3
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 614 */
} // .end local v2 # "miwillControllerList":Ljava/lang/String;
} // .end local v3 # "controllers":[Ljava/lang/String;
} // .end local v4 # "i":I
} // :cond_4
/* nop */
/* .line 616 */
/* .line 611 */
/* :catch_0 */
/* move-exception v2 */
/* .line 612 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "cloud observer catch:"; // const-string v3, "cloud observer catch:"
android.util.Log .e ( v0,v3,v2 );
/* .line 613 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void checkWlanWwanCoexistense ( ) {
/* .locals 2 */
/* .line 1152 */
int v0 = -1; // const/4 v0, -0x1
/* .line 1153 */
/* .local v0, "wlanwwanCoexistanceStatus":I */
v1 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1154 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1156 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1158 */
} // :goto_0
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mlastWlanWwanCoexistanceStatus:I */
/* if-eq v0, v1, :cond_1 */
/* .line 1159 */
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mlastWlanWwanCoexistanceStatus:I */
/* .line 1160 */
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->notifyWlanWwanCoexistenseStatus(I)V */
/* .line 1162 */
} // :cond_1
return;
} // .end method
private void checktemp ( java.lang.String p0 ) {
/* .locals 12 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 1299 */
final String v0 = "SLM-SRV-SLAService"; // const-string v0, "SLM-SRV-SLAService"
int v1 = 0; // const/4 v1, 0x0
/* .line 1301 */
/* .local v1, "reader":Ljava/io/BufferedReader; */
int v2 = 0; // const/4 v2, 0x0
/* .line 1302 */
/* .local v2, "line":Ljava/lang/String; */
try { // :try_start_0
final String v3 = "/sys/class/thermal/thermal_message/board_sensor_temp"; // const-string v3, "/sys/class/thermal/thermal_message/board_sensor_temp"
/* invoke-direct {p0, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->readLine(Ljava/lang/String;)Ljava/lang/String; */
/* move-object v2, v3 */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1303 */
(( java.lang.String ) v2 ).trim ( ); // invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
/* .line 1304 */
/* .local v3, "str_temp":Ljava/lang/String; */
java.lang.Long .parseLong ( v3 );
/* move-result-wide v4 */
/* .line 1305 */
/* .local v4, "temperature":J */
v6 = this.mThermalList;
java.lang.Long .valueOf ( v4,v5 );
(( java.util.ArrayList ) v6 ).add ( v7 ); // invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1306 */
v6 = this.mThermalList;
v6 = (( java.util.ArrayList ) v6 ).size ( ); // invoke-virtual {v6}, Ljava/util/ArrayList;->size()I
int v7 = 6; // const/4 v7, 0x6
int v8 = 0; // const/4 v8, 0x0
/* if-le v6, v7, :cond_0 */
/* .line 1307 */
v6 = this.mThermalList;
(( java.util.ArrayList ) v6 ).remove ( v8 ); // invoke-virtual {v6, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
/* .line 1309 */
} // :cond_0
/* const-wide/16 v6, 0x0 */
/* .line 1310 */
/* .local v6, "temperature_average":J */
v9 = this.mThermalList;
(( java.util.ArrayList ) v9 ).iterator ( ); // invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v10 = } // :goto_0
if ( v10 != null) { // if-eqz v10, :cond_1
/* check-cast v10, Ljava/lang/Long; */
(( java.lang.Long ) v10 ).longValue ( ); // invoke-virtual {v10}, Ljava/lang/Long;->longValue()J
/* move-result-wide v10 */
/* .line 1311 */
/* .local v10, "tmp":J */
/* add-long/2addr v6, v10 */
/* .line 1312 */
} // .end local v10 # "tmp":J
/* .line 1313 */
} // :cond_1
v9 = this.mThermalList;
v9 = (( java.util.ArrayList ) v9 ).size ( ); // invoke-virtual {v9}, Ljava/util/ArrayList;->size()I
/* int-to-long v9, v9 */
/* div-long v9, v6, v9 */
/* move-wide v6, v9 */
/* .line 1314 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "checktemp temperature = "; // const-string v10, "checktemp temperature = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v4, v5 ); // invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v10 = " temperature_average = "; // const-string v10, " temperature_average = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6, v7 ); // invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v10 = " isPerformanceMode = "; // const-string v10, " isPerformanceMode = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1315 */
v10 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isPerformanceMode()Z */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v10 = " thermal_enable_slm = "; // const-string v10, " thermal_enable_slm = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v10, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->thermal_enable_slm:Z */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1314 */
android.util.Log .d ( v0,v9 );
/* .line 1316 */
/* iget-boolean v9, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->thermal_enable_slm:Z */
if ( v9 != null) { // if-eqz v9, :cond_2
v9 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isPerformanceMode()Z */
/* if-nez v9, :cond_2 */
/* const-wide/32 v9, 0xb1bc */
/* cmp-long v9, v6, v9 */
/* if-lez v9, :cond_2 */
/* .line 1317 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Thermal management disableSLM - "; // const-string v10, "Thermal management disableSLM - "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6, v7 ); // invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v0,v9 );
/* .line 1318 */
/* iput-boolean v8, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->thermal_enable_slm:Z */
/* .line 1319 */
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) p0 ).disableSLM ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLM()V
/* .line 1320 */
} // :cond_2
/* iget-boolean v8, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->thermal_enable_slm:Z */
/* if-nez v8, :cond_4 */
/* const-wide/32 v8, 0xa028 */
/* cmp-long v8, v6, v8 */
/* if-ltz v8, :cond_3 */
v8 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isPerformanceMode()Z */
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 1321 */
} // :cond_3
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Thermal management enableSLM - "; // const-string v9, "Thermal management enableSLM - "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6, v7 ); // invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v0,v8 );
/* .line 1322 */
int v8 = 1; // const/4 v8, 0x1
/* iput-boolean v8, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->thermal_enable_slm:Z */
/* .line 1323 */
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) p0 ).enableSLM ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->enableSLM()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1330 */
} // .end local v2 # "line":Ljava/lang/String;
} // .end local v3 # "str_temp":Ljava/lang/String;
} // .end local v4 # "temperature":J
} // .end local v6 # "temperature_average":J
} // :cond_4
} // :goto_1
/* .line 1328 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1329 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "checktemp:"; // const-string v3, "checktemp:"
android.util.Log .e ( v0,v3,v2 );
/* .line 1331 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
private void createSLANotificationChannel ( ) {
/* .locals 6 */
/* .line 1460 */
/* const-string/jumbo v0, "xiaomi.NetworkBoost" */
/* .line 1461 */
/* .local v0, "name":Ljava/lang/CharSequence; */
/* const-string/jumbo v1, "xiaomi.NetworkBoost.channel" */
/* .line 1462 */
/* .local v1, "description":Ljava/lang/String; */
int v2 = 4; // const/4 v2, 0x4
/* .line 1463 */
/* .local v2, "importance":I */
/* new-instance v3, Landroid/app/NotificationChannel; */
final String v4 = "notification_channel"; // const-string v4, "notification_channel"
/* invoke-direct {v3, v4, v0, v2}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V */
/* .line 1464 */
/* .local v3, "channel":Landroid/app/NotificationChannel; */
(( android.app.NotificationChannel ) v3 ).setDescription ( v1 ); // invoke-virtual {v3, v1}, Landroid/app/NotificationChannel;->setDescription(Ljava/lang/String;)V
/* .line 1465 */
v4 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
/* const-class v5, Landroid/app/NotificationManager; */
(( android.content.Context ) v4 ).getSystemService ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v4, Landroid/app/NotificationManager; */
this.mNotificationManager = v4;
/* .line 1466 */
(( android.app.NotificationManager ) v4 ).createNotificationChannel ( v3 ); // invoke-virtual {v4, v3}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V
/* .line 1467 */
return;
} // .end method
private void deinitCloudObserver ( ) {
/* .locals 2 */
/* .line 651 */
v0 = this.mCloudObserver;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 652 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.mCloudObserver;
(( android.content.ContentResolver ) v0 ).unregisterContentObserver ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
/* .line 653 */
int v0 = 0; // const/4 v0, 0x0
this.mCloudObserver = v0;
/* .line 656 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "deinitCloudObserver ignore because: observer: "; // const-string v1, "deinitCloudObserver ignore because: observer: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCloudObserver;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = " context: "; // const-string v1, " context: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLAService"; // const-string v1, "SLM-SRV-SLAService"
android.util.Log .w ( v1,v0 );
/* .line 658 */
} // :goto_0
return;
} // .end method
public static void destroyInstance ( ) {
/* .locals 4 */
/* .line 354 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.sInstance;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 355 */
/* const-class v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager; */
/* monitor-enter v0 */
/* .line 356 */
try { // :try_start_0
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService.sInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 358 */
try { // :try_start_1
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService.sInstance;
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v1 ).onDestroy ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->onDestroy()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 362 */
/* .line 360 */
/* :catch_0 */
/* move-exception v1 */
/* .line 361 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
final String v3 = "destroyInstance onDestroy catch:"; // const-string v3, "destroyInstance onDestroy catch:"
android.util.Log .e ( v2,v3,v1 );
/* .line 363 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
/* .line 365 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 367 */
} // :cond_1
} // :goto_1
return;
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.SLAService getInstance ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 337 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.sInstance;
/* if-nez v0, :cond_1 */
/* .line 338 */
/* const-class v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* monitor-enter v0 */
/* .line 339 */
try { // :try_start_0
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService.sInstance;
/* if-nez v1, :cond_0 */
/* .line 340 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;-><init>(Landroid/content/Context;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 342 */
try { // :try_start_1
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService.sInstance;
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v1 ).onCreate ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->onCreate()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 346 */
/* .line 344 */
/* :catch_0 */
/* move-exception v1 */
/* .line 345 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
final String v3 = "getInstance onCreate catch:"; // const-string v3, "getInstance onCreate catch:"
android.util.Log .e ( v2,v3,v1 );
/* .line 348 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 350 */
} // :cond_1
} // :goto_1
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.sInstance;
} // .end method
private Long getLinkTurboAppsTotalDayTraffic ( ) {
/* .locals 6 */
/* .line 1523 */
/* const-wide/16 v0, 0x0 */
/* .line 1524 */
/* .local v0, "traffic":J */
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) p0 ).getLinkTurboAppsTraffic ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->getLinkTurboAppsTraffic()Ljava/util/List;
this.mSLAApp = v2;
/* .line 1525 */
v2 = if ( v2 != null) { // if-eqz v2, :cond_2
int v3 = 1; // const/4 v3, 0x1
/* if-ge v2, v3, :cond_0 */
/* .line 1528 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = v3 = this.mSLAApp;
/* if-ge v2, v3, :cond_1 */
/* .line 1529 */
v3 = this.mSLAApp;
/* check-cast v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v3 ).getDayTraffic ( ); // invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDayTraffic()J
/* move-result-wide v3 */
/* add-long/2addr v0, v3 */
/* .line 1528 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1531 */
} // .end local v2 # "i":I
} // :cond_1
/* const-wide/32 v2, 0x100000 */
/* .line 1532 */
/* .local v2, "MB":J */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "TotalDayTraffic: "; // const-string v5, "TotalDayTraffic: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0, v1 ); // invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "SLM-SRV-SLAService"; // const-string v5, "SLM-SRV-SLAService"
android.util.Log .d ( v5,v4 );
/* .line 1533 */
/* div-long v4, v0, v2 */
/* return-wide v4 */
/* .line 1526 */
} // .end local v2 # "MB":J
} // :cond_2
} // :goto_1
/* const-wide/16 v2, 0x0 */
/* return-wide v2 */
} // .end method
private static android.net.MacAddress getMacAddress ( Object[] p0 ) {
/* .locals 3 */
/* .param p0, "linkLayerAddress" # [B */
/* .line 489 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 491 */
try { // :try_start_0
android.net.MacAddress .fromBytes ( p0 );
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 492 */
/* :catch_0 */
/* move-exception v0 */
/* .line 493 */
/* .local v0, "e":Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to parse link-layer address: "; // const-string v2, "Failed to parse link-layer address: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
android.util.Log .e ( v2,v1 );
/* .line 497 */
} // .end local v0 # "e":Ljava/lang/IllegalArgumentException;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void handleDisableSLM ( ) {
/* .locals 2 */
/* .line 550 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I */
int v1 = 1; // const/4 v1, 0x1
/* add-int/2addr v0, v1 */
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I */
/* .line 551 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLMSwitchON()Z */
/* if-nez v0, :cond_0 */
/* .line 552 */
return;
/* .line 554 */
} // :cond_0
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I */
/* if-ne v0, v1, :cond_1 */
/* .line 555 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->stopSLAD()V */
/* .line 556 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->checkWlanWwanCoexistense()V */
/* .line 557 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMobileDataAlwaysOff()V */
/* .line 559 */
} // :cond_1
return;
} // .end method
private void handleEnableSLM ( ) {
/* .locals 1 */
/* .line 528 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I */
/* if-lez v0, :cond_0 */
/* .line 529 */
/* add-int/lit8 v0, v0, -0x1 */
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I */
/* .line 531 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLMSwitchON()Z */
/* if-nez v0, :cond_1 */
/* .line 532 */
return;
/* .line 534 */
} // :cond_1
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I */
/* if-nez v0, :cond_2 */
/* .line 535 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMobileDataAlwaysOn()V */
/* .line 536 */
final String v0 = "ON_DISABLE_EVENT"; // const-string v0, "ON_DISABLE_EVENT"
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->processNetworkCallback(Ljava/lang/String;)V */
/* .line 538 */
} // :cond_2
return;
} // .end method
private void handleScreenOff ( ) {
/* .locals 4 */
/* .line 1142 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1143 */
/* const/16 v1, 0x6b */
/* const-wide/32 v2, 0x2bf20 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 1146 */
} // :cond_0
final String v0 = "SLM-SRV-SLAService"; // const-string v0, "SLM-SRV-SLAService"
final String v1 = "handleScreenOff handler is null"; // const-string v1, "handleScreenOff handler is null"
android.util.Log .e ( v0,v1 );
/* .line 1148 */
} // :goto_0
return;
} // .end method
private void handleScreenOn ( ) {
/* .locals 2 */
/* .line 1132 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1133 */
/* const/16 v1, 0x6b */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1134 */
v0 = this.mHandler;
/* const/16 v1, 0x6a */
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 1137 */
} // :cond_0
final String v0 = "SLM-SRV-SLAService"; // const-string v0, "SLM-SRV-SLAService"
final String v1 = "handleScreenOn handler is null"; // const-string v1, "handleScreenOn handler is null"
android.util.Log .e ( v0,v1 );
/* .line 1139 */
} // :goto_0
return;
} // .end method
private void initCloudObserver ( ) {
/* .locals 4 */
/* .line 620 */
v0 = this.mCloudObserver;
final String v1 = "SLM-SRV-SLAService"; // const-string v1, "SLM-SRV-SLAService"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 621 */
final String v0 = "initCloudObserver rejected! observer has already registered! check your code."; // const-string v0, "initCloudObserver rejected! observer has already registered! check your code."
android.util.Log .e ( v1,v0 );
/* .line 623 */
} // :cond_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 624 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$1; */
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$1;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Landroid/os/Handler;)V */
this.mCloudObserver = v0;
/* .line 640 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 641 */
final String v1 = "cloud_miwill_controller_list"; // const-string v1, "cloud_miwill_controller_list"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mCloudObserver;
/* .line 640 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 642 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 643 */
final String v1 = "cloud_double_wifi_monitor_controll"; // const-string v1, "cloud_double_wifi_monitor_controll"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mCloudObserver;
/* .line 642 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 646 */
} // :cond_1
final String v0 = "initCloudObserver failed! mContext should be initialized in onCreate"; // const-string v0, "initCloudObserver failed! mContext should be initialized in onCreate"
android.util.Log .e ( v1,v0 );
/* .line 648 */
} // :goto_0
return;
} // .end method
private Boolean isDDReady ( ) {
/* .locals 2 */
/* .line 680 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDataReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveDataReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiReady:Z */
/* sget-boolean v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiReady:Z */
/* if-ne v0, v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
/* if-le v0, v1, :cond_0 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I */
/* if-nez v0, :cond_0 */
/* .line 682 */
/* .line 684 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isDWReady ( ) {
/* .locals 2 */
/* .line 673 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v1 = 1; // const/4 v1, 0x1
/* if-le v0, v1, :cond_0 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I */
/* if-nez v0, :cond_0 */
/* .line 674 */
/* .line 676 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void isEnableDWMonitor ( ) {
/* .locals 3 */
/* .line 1214 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
/* .line 1215 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1214 */
final String v1 = "cloud_double_wifi_monitor_controll"; // const-string v1, "cloud_double_wifi_monitor_controll"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 1216 */
/* .local v0, "cvalue":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "isEnableDWMonitor ="; // const-string v2, "isEnableDWMonitor ="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
android.util.Log .d ( v2,v1 );
/* .line 1217 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* const-string/jumbo v1, "v1" */
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->misEnableDWMonitor:Z */
/* .line 1218 */
return;
} // .end method
private Boolean isPerformanceMode ( ) {
/* .locals 4 */
/* .line 1282 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1284 */
/* .local v0, "line":Ljava/lang/String; */
try { // :try_start_0
final String v1 = "/sys/class/thermal/thermal_message/balance_mode"; // const-string v1, "/sys/class/thermal/thermal_message/balance_mode"
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->readLine(Ljava/lang/String;)Ljava/lang/String; */
/* move-object v0, v1 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1285 */
(( java.lang.String ) v0 ).trim ( ); // invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
/* .line 1286 */
/* .local v1, "str_temp":Ljava/lang/String; */
v2 = java.lang.Integer .parseInt ( v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1287 */
/* .local v2, "mode":I */
int v3 = 1; // const/4 v3, 0x1
/* if-ne v2, v3, :cond_0 */
/* .line 1288 */
/* .line 1294 */
} // .end local v1 # "str_temp":Ljava/lang/String;
} // .end local v2 # "mode":I
} // :cond_0
/* .line 1292 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1295 */
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private Boolean isSLAReady ( ) {
/* .locals 2 */
/* .line 661 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mLinkTurboSwitch:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDataReady:Z */
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveDataReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* if-le v0, v1, :cond_1 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I */
/* if-nez v0, :cond_1 */
/* .line 663 */
/* .line 665 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isSLMSwitchON ( ) {
/* .locals 1 */
/* .line 688 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiReady:Z */
/* if-nez v0, :cond_1 */
} // :cond_0
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mLinkTurboSwitch:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 689 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* .line 691 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isSLSReady ( ) {
/* .locals 1 */
/* .line 669 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z */
/* if-nez v0, :cond_1 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean isSlaRemind ( ) {
/* .locals 3 */
/* .line 1494 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "sla_notification_state" */
int v2 = 1; // const/4 v2, 0x1
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
/* if-ne v2, v0, :cond_0 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
} // .end method
private void notifyWlanWwanCoexistenseStatus ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "status" # I */
/* .line 1165 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "notifyWlanWwanCoexistenseStatus "; // const-string v1, "notifyWlanWwanCoexistenseStatus "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLAService"; // const-string v1, "SLM-SRV-SLAService"
android.util.Log .d ( v1,v0 );
/* .line 1166 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1167 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.android.phone.intent.action.SLM_STATUS"; // const-string v1, "com.android.phone.intent.action.SLM_STATUS"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1168 */
final String v1 = "com.android.phone"; // const-string v1, "com.android.phone"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1169 */
final String v1 = "coexistense"; // const-string v1, "coexistense"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1171 */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 1172 */
return;
} // .end method
private void postNotification ( ) {
/* .locals 0 */
/* .line 1492 */
return;
} // .end method
private void postTrafficNotification ( ) {
/* .locals 0 */
/* .line 1520 */
return;
} // .end method
private void processNetworkCallback ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "status" # Ljava/lang/String; */
/* .line 841 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "processNetworkCallback "; // const-string v1, "processNetworkCallback "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLAService"; // const-string v1, "SLM-SRV-SLAService"
android.util.Log .i ( v1,v0 );
/* .line 842 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->checkWlanWwanCoexistense()V */
/* .line 843 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->updateSLAD()V */
/* .line 844 */
return;
} // .end method
private java.lang.String readLine ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 1255 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1256 */
/* .local v0, "reader":Ljava/io/BufferedReader; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1259 */
/* .local v1, "line":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/FileReader; */
/* invoke-direct {v3, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* const/16 v4, 0x400 */
/* invoke-direct {v2, v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V */
/* move-object v0, v2 */
/* .line 1260 */
(( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v1, v2 */
/* .line 1269 */
/* nop */
/* .line 1271 */
try { // :try_start_1
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 1275 */
} // :goto_0
/* .line 1273 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1269 */
/* :catchall_0 */
/* move-exception v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1271 */
try { // :try_start_2
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 1275 */
/* .line 1273 */
/* :catch_1 */
/* move-exception v3 */
/* .line 1277 */
} // :cond_0
} // :goto_1
/* throw v2 */
/* .line 1265 */
/* :catch_2 */
/* move-exception v2 */
/* .line 1269 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1271 */
try { // :try_start_3
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* .line 1262 */
/* :catch_3 */
/* move-exception v2 */
/* .line 1269 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1271 */
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 1278 */
} // :cond_1
} // :goto_2
} // .end method
private void registerDefaultNetworkCallback ( ) {
/* .locals 3 */
/* .line 1020 */
try { // :try_start_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 1021 */
v1 = this.mDefaultNetworkInterfaceListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerDefaultNetworkInterfaceListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerDefaultNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1024 */
/* .line 1022 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1023 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
android.util.Log .e ( v2,v1 );
/* .line 1025 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void registerModemSignalStrengthListener ( ) {
/* .locals 3 */
/* .line 1337 */
try { // :try_start_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 1338 */
v1 = this.mModemSignalStrengthListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerModemSignalStrengthListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerModemSignalStrengthListener(Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1341 */
/* .line 1339 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1340 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
android.util.Log .e ( v2,v1 );
/* .line 1342 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void registerNetworkBoostWatchdog ( ) {
/* .locals 3 */
/* .line 1391 */
try { // :try_start_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 1392 */
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).getNetworkBoostWatchdogHandler ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getNetworkBoostWatchdogHandler()Landroid/os/HandlerThread;
this.mWacthdogHandlerThread = v0;
/* .line 1393 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$WatchdogHandler; */
v1 = this.mWacthdogHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$WatchdogHandler;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Landroid/os/Looper;)V */
this.mWacthdogHandler = v0;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1396 */
/* .line 1394 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1395 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
android.util.Log .e ( v2,v1 );
/* .line 1397 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void registerNetworkCallback ( ) {
/* .locals 3 */
/* .line 932 */
try { // :try_start_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 933 */
v1 = this.mNetworkInterfaceListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerNetworkInterfaceListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 936 */
/* .line 934 */
/* :catch_0 */
/* move-exception v0 */
/* .line 935 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
android.util.Log .e ( v2,v1 );
/* .line 937 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void registerNetworkStateBroadcastReceiver ( ) {
/* .locals 3 */
/* .line 975 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 976 */
/* .local v0, "wifiStateFilter":Landroid/content/IntentFilter; */
final String v1 = "android.net.wifi.STATE_CHANGE"; // const-string v1, "android.net.wifi.STATE_CHANGE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 977 */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
v2 = this.mWifiStateReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 978 */
return;
} // .end method
private void registerScreenStatusListener ( ) {
/* .locals 3 */
/* .line 1101 */
try { // :try_start_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 1102 */
v1 = this.mScreenStatusListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerScreenStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1105 */
/* .line 1103 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1104 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
android.util.Log .e ( v2,v1 );
/* .line 1106 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void registerVPNChangedCallback ( ) {
/* .locals 3 */
/* .line 1066 */
try { // :try_start_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 1067 */
v1 = this.mVPNListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerVPNListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerVPNListener(Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1070 */
/* .line 1068 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1069 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
android.util.Log .e ( v2,v1 );
/* .line 1072 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void releaseSlaServiceLock ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "func" # Ljava/lang/String; */
/* .line 834 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSemaphore;
(( java.util.concurrent.Semaphore ) v0 ).drainPermits ( ); // invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->drainPermits()I
/* .line 835 */
(( java.util.concurrent.Semaphore ) v0 ).release ( ); // invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V
/* .line 836 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " Unlock"; // const-string v1, " Unlock"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLAService"; // const-string v1, "SLM-SRV-SLAService"
android.util.Log .d ( v1,v0 );
/* .line 837 */
return;
} // .end method
private void setDWMonitorEnable ( ) {
/* .locals 4 */
/* .line 1221 */
/* const-string/jumbo v0, "setDWMonitorEnable enter" */
final String v1 = "SLM-SRV-SLAService"; // const-string v1, "SLM-SRV-SLAService"
android.util.Log .d ( v1,v0 );
/* .line 1223 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->misEnableDWMonitor:Z */
/* if-nez v0, :cond_0 */
/* .line 1224 */
return;
/* .line 1227 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z */
/* const-string/jumbo v2, "setDWMonitorEnable catch ex:" */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z */
/* if-nez v0, :cond_1 */
/* .line 1229 */
try { // :try_start_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaService;
final String v3 = "1"; // const-string v3, "1"
(( java.lang.String ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1230 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1231 */
/* .local v0, "ex":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 1232 */
} // .end local v0 # "ex":Ljava/lang/Exception;
} // :goto_0
/* .line 1235 */
} // :cond_1
try { // :try_start_1
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaService;
final String v3 = "0"; // const-string v3, "0"
(( java.lang.String ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 1238 */
/* .line 1236 */
/* :catch_1 */
/* move-exception v0 */
/* .line 1237 */
/* .restart local v0 # "ex":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 1240 */
} // .end local v0 # "ex":Ljava/lang/Exception;
} // :goto_1
/* const-string/jumbo v0, "setDWMonitorEnable exit" */
android.util.Log .d ( v1,v0 );
/* .line 1241 */
return;
} // .end method
private void setInterface ( ) {
/* .locals 4 */
/* .line 999 */
/* const-string/jumbo v0, "setInterface" */
final String v1 = "SLM-SRV-SLAService"; // const-string v1, "SLM-SRV-SLAService"
android.util.Log .i ( v1,v0 );
/* .line 1000 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1003 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setInterface:" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mInterface;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v0 );
/* .line 1004 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaService;
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mInterface;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1005 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1006 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Exception:"; // const-string v3, "Exception:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 1007 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* .line 1010 */
} // :cond_0
final String v0 = "mSlaService is null"; // const-string v0, "mSlaService is null"
android.util.Log .d ( v1,v0 );
/* .line 1012 */
} // :goto_1
return;
} // .end method
private void setMobileDataAlwaysOff ( ) {
/* .locals 4 */
/* .line 510 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "mobile_data_always_on"; // const-string v1, "mobile_data_always_on"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
/* .line 512 */
/* .local v0, "mobileDataAlwaysOnMode":I */
int v3 = 1; // const/4 v3, 0x1
/* if-ne v0, v3, :cond_0 */
/* .line 513 */
v3 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Global .putInt ( v3,v1,v2 );
/* .line 516 */
} // :cond_0
return;
} // .end method
private void setMobileDataAlwaysOn ( ) {
/* .locals 4 */
/* .line 501 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = 0; // const/4 v1, 0x0
final String v2 = "mobile_data_always_on"; // const-string v2, "mobile_data_always_on"
v0 = android.provider.Settings$Global .getInt ( v0,v2,v1 );
/* .line 503 */
/* .local v0, "mobileDataAlwaysOnMode":I */
/* if-nez v0, :cond_0 */
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSingnalPoor:Z */
/* if-nez v1, :cond_0 */
/* .line 504 */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v3 = 1; // const/4 v3, 0x1
android.provider.Settings$Global .putInt ( v1,v2,v3 );
/* .line 507 */
} // :cond_0
return;
} // .end method
private void setSLAWifiGateway ( ) {
/* .locals 8 */
/* .line 1176 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1177 */
/* .local v0, "dhcpInfo":Landroid/net/DhcpInfo; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1178 */
/* .local v1, "slaveDhcpInfo":Landroid/net/DhcpInfo; */
/* new-instance v2, Ljava/lang/StringBuffer; */
/* invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V */
/* .line 1180 */
/* .local v2, "gateway":Ljava/lang/StringBuffer; */
v3 = this.mWifiManager;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1181 */
(( android.net.wifi.WifiManager ) v3 ).getDhcpInfo ( ); // invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getDhcpInfo()Landroid/net/DhcpInfo;
/* .line 1184 */
} // :cond_0
v3 = this.mSlaveWifiManager;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1185 */
(( android.net.wifi.SlaveWifiManager ) v3 ).getSlaveDhcpInfo ( ); // invoke-virtual {v3}, Landroid/net/wifi/SlaveWifiManager;->getSlaveDhcpInfo()Landroid/net/DhcpInfo;
/* .line 1188 */
} // :cond_1
final String v3 = ","; // const-string v3, ","
final String v4 = "SLM-SRV-SLAService"; // const-string v4, "SLM-SRV-SLAService"
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1189 */
/* iget v5, v0, Landroid/net/DhcpInfo;->gateway:I */
android.net.NetworkUtils .intToInetAddress ( v5 );
(( java.net.InetAddress ) v5 ).getHostAddress ( ); // invoke-virtual {v5}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
/* .line 1190 */
/* .local v5, "address":Ljava/lang/String; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Gateway address:"; // const-string v7, "Gateway address:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v4,v6 );
/* .line 1192 */
(( java.lang.StringBuffer ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 1193 */
(( java.lang.StringBuffer ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 1196 */
} // .end local v5 # "address":Ljava/lang/String;
} // :cond_2
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1197 */
/* iget v5, v1, Landroid/net/DhcpInfo;->gateway:I */
android.net.NetworkUtils .intToInetAddress ( v5 );
(( java.net.InetAddress ) v5 ).getHostAddress ( ); // invoke-virtual {v5}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
/* .line 1198 */
/* .restart local v5 # "address":Ljava/lang/String; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Slave Gateway address:"; // const-string v7, "Slave Gateway address:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v4,v6 );
/* .line 1199 */
(( java.lang.StringBuffer ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 1200 */
(( java.lang.StringBuffer ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 1203 */
} // .end local v5 # "address":Ljava/lang/String;
} // :cond_3
v3 = (( java.lang.StringBuffer ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I
/* if-lez v3, :cond_4 */
/* .line 1205 */
try { // :try_start_0
v3 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaService;
(( java.lang.StringBuffer ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1208 */
/* .line 1206 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1207 */
/* .local v3, "ex":Ljava/lang/Exception; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "setSLAWifiGateway catch ex:" */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v4,v5 );
/* .line 1210 */
} // .end local v3 # "ex":Ljava/lang/Exception;
} // :cond_4
} // :goto_0
return;
} // .end method
private void stopSLAD ( ) {
/* .locals 5 */
/* .line 784 */
final String v0 = "SLM-SRV-SLAService"; // const-string v0, "SLM-SRV-SLAService"
/* const-string/jumbo v1, "stopSLAD" */
android.util.Log .i ( v0,v1 );
/* .line 786 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaLock;
/* monitor-enter v0 */
/* .line 788 */
try { // :try_start_0
/* sget-boolean v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sladEnabled:Z */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
/* .line 789 */
final String v1 = "SLM-SRV-SLAService"; // const-string v1, "SLM-SRV-SLAService"
/* const-string/jumbo v2, "stopSlad" */
android.util.Log .i ( v1,v2 );
/* .line 790 */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaService;
/* .line 791 */
int v1 = 0; // const/4 v1, 0x0
com.xiaomi.NetworkBoost.slaservice.SLAService.sladEnabled = (v1!= 0);
/* .line 792 */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v1 ).stopSladHandle ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->stopSladHandle()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 796 */
} // :cond_0
/* .line 797 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 794 */
/* :catch_0 */
/* move-exception v1 */
/* .line 795 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_1
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Exception:"; // const-string v4, "Exception:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 797 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 799 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLSAppLib;
v1 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLSReady()Z */
(( com.xiaomi.NetworkBoost.slaservice.SLSAppLib ) v0 ).setSLSEnableStatus ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSEnableStatus(Z)V
/* .line 800 */
com.xiaomi.NetworkBoost.slaservice.SLATrack .sendMsgSlsStop ( );
/* .line 801 */
com.xiaomi.NetworkBoost.slaservice.SLATrack .sendMsgDoubleWifiAndSLAStop ( );
/* .line 802 */
com.xiaomi.NetworkBoost.slaservice.SLATrack .sendMsgSlaStop ( );
/* .line 803 */
return;
/* .line 797 */
} // :goto_1
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
private void unregisterDefaultNetworkCallback ( ) {
/* .locals 3 */
/* .line 1029 */
try { // :try_start_0
v0 = this.mDefaultNetworkInterfaceListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1030 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 1031 */
v1 = this.mDefaultNetworkInterfaceListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterDefaultNetworkInterfaceListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterDefaultNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1035 */
} // :cond_0
/* .line 1033 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1034 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
android.util.Log .e ( v2,v1 );
/* .line 1036 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unregisterModemSignalStrengthListener ( ) {
/* .locals 3 */
/* .line 1346 */
try { // :try_start_0
v0 = this.mModemSignalStrengthListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1347 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 1348 */
v1 = this.mModemSignalStrengthListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterModemSignalStrengthListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterModemSignalStrengthListener(Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1352 */
} // :cond_0
/* .line 1350 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1351 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
android.util.Log .e ( v2,v1 );
/* .line 1353 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unregisterNetworkCallback ( ) {
/* .locals 3 */
/* .line 941 */
try { // :try_start_0
v0 = this.mNetworkInterfaceListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 942 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 943 */
v1 = this.mNetworkInterfaceListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterNetworkInterfaceListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 947 */
} // :cond_0
/* .line 945 */
/* :catch_0 */
/* move-exception v0 */
/* .line 946 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
android.util.Log .e ( v2,v1 );
/* .line 948 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unregisterNetworkStateBroadcastReceiver ( ) {
/* .locals 2 */
/* .line 981 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
v1 = this.mWifiStateReceiver;
(( android.content.Context ) v0 ).unregisterReceiver ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 982 */
return;
} // .end method
private void unregisterScreenStatusListener ( ) {
/* .locals 3 */
/* .line 1110 */
try { // :try_start_0
v0 = this.mScreenStatusListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1111 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 1112 */
v1 = this.mScreenStatusListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterScreenStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1116 */
} // :cond_0
/* .line 1114 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1115 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
android.util.Log .e ( v2,v1 );
/* .line 1117 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unregisterVPNChangedCallback ( ) {
/* .locals 3 */
/* .line 1076 */
try { // :try_start_0
v0 = this.mVPNListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1077 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 1078 */
v1 = this.mVPNListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterVPNListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterVPNListener(Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1082 */
} // :cond_0
/* .line 1080 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1081 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
android.util.Log .e ( v2,v1 );
/* .line 1083 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void updateSLAD ( ) {
/* .locals 6 */
/* .line 695 */
final String v0 = "SLM-SRV-SLAService"; // const-string v0, "SLM-SRV-SLAService"
/* const-string/jumbo v1, "updateSLAD" */
android.util.Log .i ( v0,v1 );
/* .line 696 */
v0 = this.mWacthdogHandler;
/* const/16 v1, 0x64 */
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* const-wide/32 v3, 0x1d4c0 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 697 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaLock;
/* monitor-enter v0 */
/* .line 699 */
try { // :try_start_0
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaService;
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_0 */
/* .line 700 */
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
final String v4 = "HAL service is null, get"; // const-string v4, "HAL service is null, get"
android.util.Log .e ( v2,v4 );
/* .line 701 */
v2 = this.mHandler;
java.lang.Integer .valueOf ( v3 );
/* const/16 v4, 0x67 */
(( android.os.Handler ) v2 ).obtainMessage ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Handler ) v2 ).sendMessage ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* goto/16 :goto_3 */
/* .line 703 */
} // :cond_0
v4 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v4 ).setSlaService ( v2 ); // invoke-virtual {v4, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setSlaService(Lvendor/qti/sla/service/V1_0/ISlaService;)V
/* .line 704 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLSAppLib;
v4 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaService;
(( com.xiaomi.NetworkBoost.slaservice.SLSAppLib ) v2 ).setSlaService ( v4 ); // invoke-virtual {v2, v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSlaService(Lvendor/qti/sla/service/V1_0/ISlaService;)V
/* .line 705 */
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "isSLAReady:"; // const-string v5, "isSLAReady:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v5 = ", isDWReady:"; // const-string v5, ", isDWReady:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 706 */
v5 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v5 = ", isDDReady:"; // const-string v5, ", isDDReady:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 707 */
v5 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDDReady()Z */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 705 */
android.util.Log .i ( v2,v4 );
/* .line 709 */
v2 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z */
int v4 = 1; // const/4 v4, 0x1
/* if-nez v2, :cond_3 */
v2 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z */
/* if-nez v2, :cond_3 */
v2 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDDReady()Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 746 */
} // :cond_1
/* sget-boolean v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sladEnabled:Z */
/* if-ne v2, v4, :cond_2 */
/* .line 747 */
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
/* const-string/jumbo v4, "stopSlad" */
android.util.Log .i ( v2,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 750 */
try { // :try_start_1
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaService;
/* .line 751 */
com.xiaomi.NetworkBoost.slaservice.SLAService.sladEnabled = (v3!= 0);
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 754 */
/* .line 752 */
/* :catch_0 */
/* move-exception v2 */
/* .line 753 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v3 = "SLM-SRV-SLAService"; // const-string v3, "SLM-SRV-SLAService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "updateSLAD SlaService stopSlad failed " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v4 );
/* .line 757 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v2 ).stopSladHandle ( ); // invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->stopSladHandle()V
/* goto/16 :goto_3 */
/* .line 759 */
} // :cond_2
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
/* const-string/jumbo v3, "slad is already disabled" */
android.util.Log .d ( v2,v3 );
/* .line 710 */
} // :cond_3
} // :goto_1
/* sget-boolean v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sladEnabled:Z */
/* if-nez v2, :cond_4 */
/* .line 711 */
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
/* const-string/jumbo v3, "startSlad " */
android.util.Log .i ( v2,v3 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 714 */
try { // :try_start_3
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaService;
/* .line 715 */
com.xiaomi.NetworkBoost.slaservice.SLAService.sladEnabled = (v4!= 0);
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 718 */
/* .line 716 */
/* :catch_1 */
/* move-exception v2 */
/* .line 717 */
/* .restart local v2 # "e":Ljava/lang/Exception; */
try { // :try_start_4
final String v3 = "SLM-SRV-SLAService"; // const-string v3, "SLM-SRV-SLAService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "updateSLAD SlaService startSlad failed " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v4 );
/* .line 718 */
/* nop */
} // .end local v2 # "e":Ljava/lang/Exception;
/* .line 721 */
} // :cond_4
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
/* const-string/jumbo v3, "slad is already enabled" */
android.util.Log .d ( v2,v3 );
/* .line 725 */
} // :goto_2
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setInterface()V */
/* .line 727 */
v2 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 728 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v2 ).startSladHandle ( ); // invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->startSladHandle()V
/* .line 731 */
} // :cond_5
v2 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLSReady()Z */
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 732 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLSAppLib;
(( com.xiaomi.NetworkBoost.slaservice.SLSAppLib ) v2 ).setWifiBSSID ( ); // invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setWifiBSSID()Z
/* .line 735 */
} // :cond_6
v2 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z */
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 736 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v2 ).sendMsgDoubleWifiUid ( ); // invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgDoubleWifiUid()Z
/* .line 739 */
} // :cond_7
v2 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDDReady()Z */
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 740 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v2 ).sendMsgDoubleDataUid ( ); // invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgDoubleDataUid()Z
/* .line 741 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v2 ).setDDSLAMode ( ); // invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setDDSLAMode()V
/* .line 743 */
} // :cond_8
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setSLAWifiGateway()V */
/* .line 744 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDWMonitorEnable()V */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_2 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 765 */
} // :goto_3
/* .line 779 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 763 */
/* :catch_2 */
/* move-exception v2 */
/* .line 764 */
/* .restart local v2 # "e":Ljava/lang/Exception; */
try { // :try_start_5
final String v3 = "SLM-SRV-SLAService"; // const-string v3, "SLM-SRV-SLAService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Exception:"; // const-string v5, "Exception:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v4 );
/* .line 767 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_4
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaToast;
v3 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z */
(( com.xiaomi.NetworkBoost.slaservice.SLAToast ) v2 ).setLinkTurboStatus ( v3 ); // invoke-virtual {v2, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->setLinkTurboStatus(Z)V
/* .line 768 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLSAppLib;
v3 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLSReady()Z */
(( com.xiaomi.NetworkBoost.slaservice.SLSAppLib ) v2 ).setSLSEnableStatus ( v3 ); // invoke-virtual {v2, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSEnableStatus(Z)V
/* .line 769 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v2 ).getLinkTurboWhiteList ( ); // invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAToast .setLinkTurboUidList ( v2 );
/* .line 770 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v2 ).getLinkTurboWhiteList ( ); // invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .setSLAAppWhiteList ( v2 );
/* .line 772 */
v2 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z */
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 773 */
v2 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z */
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 774 */
com.xiaomi.NetworkBoost.slaservice.SLATrack .sendMsgDoubleWifiAndSLAStart ( );
/* .line 776 */
} // :cond_9
com.xiaomi.NetworkBoost.slaservice.SLATrack .sendMsgSlaStart ( );
/* .line 779 */
} // :cond_a
} // :goto_5
/* monitor-exit v0 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
/* .line 780 */
v0 = this.mWacthdogHandler;
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 781 */
return;
/* .line 779 */
} // :goto_6
try { // :try_start_6
/* monitor-exit v0 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public Boolean addUidToLinkTurboWhiteList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 287 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
v0 = (( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v0 ).sendMsgAddSLAUid ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgAddSLAUid(Ljava/lang/String;)Z
} // .end method
public void disableSLM ( ) {
/* .locals 2 */
/* .line 541 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 542 */
/* const/16 v1, 0x69 */
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 545 */
} // :cond_0
final String v0 = "SLM-SRV-SLAService"; // const-string v0, "SLM-SRV-SLAService"
final String v1 = "disableSLM handler is null"; // const-string v1, "disableSLM handler is null"
android.util.Log .e ( v0,v1 );
/* .line 547 */
} // :goto_0
return;
} // .end method
public void dumpModule ( java.io.PrintWriter p0 ) {
/* .locals 8 */
/* .param p1, "writer" # Ljava/io/PrintWriter; */
/* .line 1421 */
final String v0 = "SLAService end.\n"; // const-string v0, "SLAService end.\n"
final String v1 = " "; // const-string v1, " "
/* .line 1423 */
/* .local v1, "spacePrefix":Ljava/lang/String; */
try { // :try_start_0
final String v2 = "SLAService begin:"; // const-string v2, "SLAService begin:"
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1424 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "WifiReady:"; // const-string v3, "WifiReady:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiReady:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1425 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "SlaveWifiReady:"; // const-string v3, "SlaveWifiReady:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiReady:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1426 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "DataReady:"; // const-string v3, "DataReady:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDataReady:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1427 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "Interface:"; // const-string v3, "Interface:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = com.xiaomi.NetworkBoost.slaservice.SLAService.mInterface;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1428 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "IfaceNumber:"; // const-string v3, "IfaceNumber:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1429 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "disableSLMCnt:"; // const-string v3, "disableSLMCnt:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1430 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mlastWlanWwanCoexistanceStatus:"; // const-string v3, "mlastWlanWwanCoexistanceStatus:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mlastWlanWwanCoexistanceStatus:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1431 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "thermal_enable_slm:" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->thermal_enable_slm:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1432 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mThermalList:"; // const-string v3, "mThermalList:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mThermalList;
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " {"; // const-string v3, " {"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1433 */
final String v2 = ""; // const-string v2, ""
/* .line 1434 */
/* .local v2, "temperatureStr":Ljava/lang/String; */
int v3 = 1; // const/4 v3, 0x1
/* .line 1435 */
/* .local v3, "firstItem":Z */
v4 = this.mThermalList;
(( java.util.ArrayList ) v4 ).iterator ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_1
/* check-cast v5, Ljava/lang/Long; */
/* .line 1436 */
/* .local v5, "temperature":Ljava/lang/Long; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1437 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v2, v6 */
/* .line 1440 */
} // :cond_0
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ", "; // const-string v7, ", "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v2, v6 */
/* .line 1442 */
} // .end local v5 # "temperature":Ljava/lang/Long;
} // :goto_1
/* .line 1443 */
} // :cond_1
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1444 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v5, "}" */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1445 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1456 */
} // .end local v2 # "temperatureStr":Ljava/lang/String;
} // .end local v3 # "firstItem":Z
/* .line 1447 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1448 */
/* .local v2, "ex":Ljava/lang/Exception; */
final String v3 = "dump failed!"; // const-string v3, "dump failed!"
final String v4 = "SLM-SRV-SLAService"; // const-string v4, "SLM-SRV-SLAService"
android.util.Log .e ( v4,v3,v2 );
/* .line 1450 */
try { // :try_start_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Dump of SLAService failed:"; // const-string v5, "Dump of SLAService failed:"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1451 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 1455 */
/* .line 1453 */
/* :catch_1 */
/* move-exception v0 */
/* .line 1454 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "dump failure failed:"; // const-string v5, "dump failure failed:"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v4,v3 );
/* .line 1457 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end local v2 # "ex":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
public void enableSLM ( ) {
/* .locals 2 */
/* .line 519 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 520 */
/* const/16 v1, 0x68 */
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 523 */
} // :cond_0
final String v0 = "SLM-SRV-SLAService"; // const-string v0, "SLM-SRV-SLAService"
final String v1 = "enableSLM handler is null"; // const-string v1, "enableSLM handler is null"
android.util.Log .e ( v0,v1 );
/* .line 525 */
} // :goto_0
return;
} // .end method
public java.util.List getLinkTurboAppsTraffic ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 301 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v0 ).getLinkTurboAppsTraffic ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboAppsTraffic()Ljava/util/List;
} // .end method
public java.util.List getLinkTurboDefaultPn ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 298 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v0 ).getLinkTurboDefaultPn ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboDefaultPn()Ljava/util/List;
} // .end method
public java.lang.String getLinkTurboWhiteList ( ) {
/* .locals 1 */
/* .line 295 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v0 ).getLinkTurboWhiteList ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;
} // .end method
public void onCreate ( ) {
/* .locals 4 */
/* .line 384 */
final String v0 = "SLM-SRV-SLAService"; // const-string v0, "SLM-SRV-SLAService"
final String v1 = "onCreate"; // const-string v1, "onCreate"
android.util.Log .i ( v0,v1 );
/* .line 386 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "SLAServiceHandler"; // const-string v1, "SLAServiceHandler"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 387 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 388 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 389 */
int v1 = 0; // const/4 v1, 0x0
java.lang.Integer .valueOf ( v1 );
/* const/16 v3, 0x67 */
(( android.os.Handler ) v0 ).obtainMessage ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 391 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->registerNetworkBoostWatchdog()V */
/* .line 394 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast; */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
/* invoke-direct {v0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;-><init>(Landroid/content/Context;)V */
/* .line 395 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .get ( v0 );
/* .line 396 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib; */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
/* invoke-direct {v0, v2, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;-><init>(Landroid/content/Context;Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V */
/* .line 397 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
com.xiaomi.NetworkBoost.slaservice.SLATrack .getSLATrack ( v0 );
/* .line 399 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
/* const-string/jumbo v2, "wifi" */
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/WifiManager; */
this.mWifiManager = v0;
/* .line 400 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
final String v2 = "SlaveWifiService"; // const-string v2, "SlaveWifiService"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/SlaveWifiManager; */
this.mSlaveWifiManager = v0;
/* .line 403 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->initCloudObserver()V */
/* .line 405 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->checkMiwillCloudController()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 406 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
com.xiaomi.NetworkBoost.slaservice.MiWillManager .getInstance ( v0 );
this.mMiWillManager = v0;
/* .line 410 */
} // :cond_0
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isEnableDWMonitor()V */
/* .line 412 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->createSLANotificationChannel()V */
/* .line 413 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->registerVPNChangedCallback()V */
/* .line 414 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->registerScreenStatusListener()V */
/* .line 415 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->registerModemSignalStrengthListener()V */
/* .line 417 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "linkturbo_is_enable"; // const-string v2, "linkturbo_is_enable"
v0 = android.provider.Settings$System .getInt ( v0,v2,v1 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_1 */
/* move v1, v2 */
} // :cond_1
com.xiaomi.NetworkBoost.slaservice.SLAService.mLinkTurboSwitch = (v1!= 0);
/* .line 419 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->registerNetworkCallback()V */
/* .line 420 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->registerDefaultNetworkCallback()V */
/* .line 421 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->registerNetworkStateBroadcastReceiver()V */
/* .line 430 */
v0 = this.mHandler;
/* const/16 v1, 0x6e */
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 431 */
return;
} // .end method
public void onDestroy ( ) {
/* .locals 2 */
/* .line 446 */
final String v0 = "SLM-SRV-SLAService"; // const-string v0, "SLM-SRV-SLAService"
final String v1 = "onDestroy"; // const-string v1, "onDestroy"
android.util.Log .i ( v0,v1 );
/* .line 447 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 449 */
try { // :try_start_0
v1 = this.mDeathRecipient;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 452 */
/* .line 450 */
/* :catch_0 */
/* move-exception v0 */
/* .line 455 */
} // :cond_0
} // :goto_0
v0 = this.mHandlerThread;
(( android.os.HandlerThread ) v0 ).quit ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
/* .line 456 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSlaToast;
(( com.xiaomi.NetworkBoost.slaservice.SLAToast ) v0 ).getSLAToastHandlerThread ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->getSLAToastHandlerThread()Landroid/os/HandlerThread;
(( android.os.HandlerThread ) v0 ).quit ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
/* .line 457 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->unregisterVPNChangedCallback()V */
/* .line 458 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->unregisterScreenStatusListener()V */
/* .line 459 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->unregisterModemSignalStrengthListener()V */
/* .line 460 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->unregisterNetworkCallback()V */
/* .line 461 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->unregisterDefaultNetworkCallback()V */
/* .line 462 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->unregisterNetworkStateBroadcastReceiver()V */
/* .line 463 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->deinitCloudObserver()V */
/* .line 464 */
v0 = this.mMiWillManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 465 */
com.xiaomi.NetworkBoost.slaservice.MiWillManager .destroyInstance ( );
/* .line 466 */
int v0 = 0; // const/4 v0, 0x0
this.mMiWillManager = v0;
/* .line 476 */
} // :cond_1
return;
} // .end method
public Boolean removeUidInLinkTurboWhiteList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 291 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
v0 = (( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v0 ).sendMsgDelSLAUid ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgDelSLAUid(Ljava/lang/String;)Z
} // .end method
public void setDWUidToSlad ( ) {
/* .locals 2 */
/* .line 825 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 826 */
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v0 ).sendMsgDoubleWifiUid ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgDoubleWifiUid()Z
/* .line 828 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setDWUidToSlad isDWReady = " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
android.util.Log .d ( v1,v0 );
/* .line 830 */
} // :goto_0
return;
} // .end method
public Boolean setDoubleWifiWhiteList ( Integer p0, java.lang.String p1, java.lang.String p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "uid" # Ljava/lang/String; */
/* .param p4, "isOperateBlacklist" # Z */
/* .line 818 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mSLAAppLib;
/* if-nez v0, :cond_0 */
/* .line 819 */
int v0 = 0; // const/4 v0, 0x0
/* .line 821 */
} // :cond_0
v0 = (( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v0 ).setDoubleWifiWhiteList ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z
} // .end method
public Boolean setLinkTurboEnable ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "enable" # Z */
/* .line 305 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mLinkTurboSwitch:Z */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, p1, :cond_0 */
/* .line 306 */
/* .line 309 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 310 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMobileDataAlwaysOn()V */
/* .line 312 */
} // :cond_1
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMobileDataAlwaysOff()V */
/* .line 315 */
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "SLM Enable:"; // const-string v2, "SLM Enable:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAService"; // const-string v2, "SLM-SRV-SLAService"
android.util.Log .i ( v2,v0 );
/* .line 316 */
/* const/16 v0, 0x6c */
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 317 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v3, "sla_notification_state" */
android.provider.Settings$System .putInt ( v2,v3,v1 );
/* .line 318 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).obtainMessage ( v0 ); // invoke-virtual {v2, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 319 */
/* .local v0, "message":Landroid/os/Message; */
v2 = this.mHandler;
/* const-wide/16 v3, 0x2710 */
(( android.os.Handler ) v2 ).sendMessageDelayed ( v0, v3, v4 ); // invoke-virtual {v2, v0, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 320 */
} // .end local v0 # "message":Landroid/os/Message;
/* .line 321 */
} // :cond_2
v2 = this.mHandler;
(( android.os.Handler ) v2 ).removeMessages ( v0 ); // invoke-virtual {v2, v0}, Landroid/os/Handler;->removeMessages(I)V
/* .line 323 */
} // :goto_1
com.xiaomi.NetworkBoost.slaservice.SLAService.mLinkTurboSwitch = (p1!= 0);
/* .line 324 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "linkturbo_is_enable"; // const-string v2, "linkturbo_is_enable"
android.provider.Settings$System .putInt ( v0,v2,p1 );
/* .line 326 */
android.os.Message .obtain ( );
/* .line 327 */
/* .local v0, "msg":Landroid/os/Message; */
/* const/16 v2, 0x64 */
/* iput v2, v0, Landroid/os/Message;->what:I */
/* .line 328 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).sendMessage ( v0 ); // invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 329 */
} // .end method
public Boolean setMiWillGameStart ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 562 */
v0 = this.mMiWillManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 563 */
v0 = (( com.xiaomi.NetworkBoost.slaservice.MiWillManager ) v0 ).setMiWillGameStart ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->setMiWillGameStart(Ljava/lang/String;)Z
/* .line 566 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setMiWillGameStart manager is null: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLAService"; // const-string v1, "SLM-SRV-SLAService"
android.util.Log .e ( v1,v0 );
/* .line 567 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setMiWillGameStop ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 572 */
v0 = this.mMiWillManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 573 */
v0 = (( com.xiaomi.NetworkBoost.slaservice.MiWillManager ) v0 ).setMiWillGameStop ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->setMiWillGameStop(Ljava/lang/String;)Z
/* .line 576 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setMiWillGameStop manager is null: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLAService"; // const-string v1, "SLM-SRV-SLAService"
android.util.Log .e ( v1,v0 );
/* .line 577 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
