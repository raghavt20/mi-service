class com.xiaomi.NetworkBoost.slaservice.MiWillManager$Notification extends miui.android.services.internal.hidl.manager.V1_0.IServiceNotification$Stub {
	 /* .source "MiWillManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "Notification" */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.MiWillManager this$0; //synthetic
/* # direct methods */
private com.xiaomi.NetworkBoost.slaservice.MiWillManager$Notification ( ) {
/* .locals 0 */
/* .line 121 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceNotification$Stub;-><init>()V */
return;
} // .end method
 com.xiaomi.NetworkBoost.slaservice.MiWillManager$Notification ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)V */
return;
} // .end method
/* # virtual methods */
public final void onRegistration ( java.lang.String p0, java.lang.String p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "interfaceName" # Ljava/lang/String; */
/* .param p2, "instanceName" # Ljava/lang/String; */
/* .param p3, "preexisting" # Z */
/* .line 125 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onRegistration interfaceName = "; // const-string v1, "onRegistration interfaceName = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " instanceName = "; // const-string v1, " instanceName = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " preexisting = "; // const-string v1, " preexisting = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MIWILL-MiWillManager"; // const-string v1, "MIWILL-MiWillManager"
android.util.Log .i ( v1,v0 );
/* .line 126 */
/* const-string/jumbo v0, "vendor.xiaomi.hidl.miwill@1.0::IMiwillService" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
return;
/* .line 127 */
} // :cond_0
final String v0 = "default"; // const-string v0, "default"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
return;
/* .line 129 */
} // :cond_1
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.MiWillManager .-$$Nest$fgetmHandler ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 130 */
final String v0 = "prepare to get new hal service."; // const-string v0, "prepare to get new hal service."
android.util.Log .w ( v1,v0 );
/* .line 131 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.MiWillManager .-$$Nest$fgetmHandler ( v0 );
/* const/16 v1, 0x65 */
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 133 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.MiWillManager .-$$Nest$fgetmHandler ( v0 );
/* const/16 v1, 0x64 */
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 135 */
} // :cond_2
return;
} // .end method
