.class Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$5;
.super Landroid/database/ContentObserver;
.source "SLAAppLib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLAUIObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 1226
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$5;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .line 1228
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$5;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "link_turbo_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1230
    .local v0, "linkTurboMode":I
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$sfgetmLinkTurboMode()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 1231
    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$sfputmLinkTurboMode(I)V

    .line 1232
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$smsetSLAMode()V

    .line 1234
    :cond_0
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 1235
    return-void
.end method
