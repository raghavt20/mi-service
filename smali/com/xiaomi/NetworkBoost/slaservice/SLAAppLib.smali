.class public Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;
.super Ljava/lang/Object;
.source "SLAAppLib.java"


# static fields
.field private static final ACTION_DUAL_DATA_CONCURRENT_LIMITED_WHITE_LIST_DONE:Ljava/lang/String; = "com.android.phone.intent.action.DUAL_DATA_CONCURRENT_LIMITED_WHITE_LIST_DONE"

.field private static final ACTION_DUAL_DATA_CONCURRENT_MODE_WHITE_LIST_DONE:Ljava/lang/String; = "com.android.phone.intent.action.DUAL_DATA_CONCURRENT_MODE_WHITE_LIST_DONE"

.field public static final ADD_TO_WHITELIST:I = 0x1

.field private static final CLOUD_DOUBLEDATA_LIMITED_WHITELIST:Ljava/lang/String; = "dual_data_concurrent_limited_white_list_pkg_name"

.field private static final CLOUD_DOUBLEDATA_MODE_WHITELIST:Ljava/lang/String; = "dual_data_concurrent_mode_white_list_pkg_name"

.field private static final CLOUD_DOUBLEWIFI_WHITELIST:Ljava/lang/String; = "cloud_double_wifi_uidlist"

.field private static final CLOUD_SLA_WHITELIST:Ljava/lang/String; = "cloud_sla_whitelist"

.field private static final CLOUD_SLS_GAMING_WHITELIST:Ljava/lang/String; = "cloud_sls_whitelist"

.field private static final DEBUG:Z = false

.field private static final IS_DAY_CHANGE:I = 0x1

.field public static final IS_IN_WHITELIST:I = 0x4

.field private static final IS_MONTH_CHANGE:I = 0x2

.field private static final IS_TODAY:I = 0x0

.field private static final LINKTURBO_UID_TO_SLA:Ljava/lang/String; = "update_uidlist_to_sla"

.field public static final LINK_TURBO_MODE:Ljava/lang/String; = "link_turbo_mode"

.field private static final LINK_TURBO_OPTION:Ljava/lang/String; = "link_turbo_option"

.field private static final MSG_DB_SLAAPP_ADD:I = 0x64

.field private static final MSG_DB_SLAAPP_DEL:I = 0x65

.field private static final MSG_DB_SLAAPP_UPDATE:I = 0x66

.field private static final MSG_DELAY_TIME:I = 0x12c

.field private static final MSG_SET_DDAPP_LIST:I = 0x69

.field private static final MSG_SET_DWAPP_LIST:I = 0x67

.field private static final MSG_SET_SLAAPP_LIST:I = 0x68

.field public static final REMOVE_FROM_WHITELIST:I = 0x2

.field public static final RESET_WHITELIST:I = 0x3

.field public static final RESTORE_RESET_WHITELIST:I = 0x5

.field private static final SLA_MODE_CONCURRENT:Ljava/lang/String; = "0"

.field private static final SLA_MODE_SLAVE:Ljava/lang/String; = "1"

.field private static final START_CALC_TRAFFIC_STAT:I = 0xc9

.field private static final STOP_CALC_TRAFFIC_STAT:I = 0xcb

.field static final TAG:Ljava/lang/String; = "SLM-SRV-SLAAppLib"

.field public static final TYPE_MOBILE:I = 0x0

.field private static final UPDATE_CALC_TRAFFIC_STAT:I = 0xca

.field private static final UPDATE_DB_TRAFFIC_STAT:I = 0xcc

.field private static defaultSlaEnable:Z = false

.field private static mAppLists:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;",
            ">;"
        }
    .end annotation
.end field

.field private static mIsSladRunning:Z = false

.field private static mIsUidChangeReboot:Z = false

.field private static mLinkTurboMode:I = 0x0

.field private static mNetworkStatsManager:Landroid/app/usage/NetworkStatsManager; = null

.field private static final mSLAAppLibLock:Ljava/lang/Object;

.field private static mSLAAppUpgradeList:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final mSLSVoIPAppPN:Ljava/lang/String; = "com.tencent.mm"

.field private static mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

.field private static mTraffic:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static sSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;


# instance fields
.field private mCalcTrafficInterval:I

.field private mContext:Landroid/content/Context;

.field private mCount:I

.field private mDataBaseHandler:Landroid/os/Handler;

.field private mDataBaseThread:Landroid/os/HandlerThread;

.field private mDatabase:Landroid/database/sqlite/SQLiteDatabase;

.field private mDay:I

.field private mDoubleDataAppPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDoubleDataUidList:Ljava/lang/String;

.field private mDoubleWifiAppPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDoubleWifiUidList:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mMonth:I

.field private mSDKDoubleWifiAppPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSDKDoubleWifiBlackUidSets:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSDKDoubleWifiUidList:Ljava/lang/String;

.field private mSLAAppDefaultPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSLSGameAppPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSLSGameUidList:Ljava/lang/String;

.field private mSLSVoIPUid:I

.field private final mSemaphore:Ljava/util/concurrent/Semaphore;

.field private slaBaseHelper:Ldatabase/SlaDbSchema/SlaBaseHelper;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDoubleDataAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataAppPN:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDoubleDataUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataUidList:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDoubleWifiAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleWifiAppPN:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleWifiUidList:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSLAAppDefaultPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppDefaultPN:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSLSGameAppPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLSGameAppPN:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLSGameUidList:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLSVoIPUid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmDoubleDataUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataUidList:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDoubleWifiUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleWifiUidList:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSLSGameUidList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLSGameUidList:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSLSVoIPUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLSVoIPUid:I

    return-void
.end method

.method static bridge synthetic -$$Nest$maddSLAAppDefault(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->addSLAAppDefault(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcalcAppsTrafficStat(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->calcAppsTrafficStat()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mclearSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->clearSLAApp(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdbAddSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbAddSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdbUpdateSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbUpdateSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdeleteSLAAppDefault(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->deleteSLAAppDefault(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mfetchDoubleDataWhiteListApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->fetchDoubleDataWhiteListApp()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetSLAAppByUid(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$minitSLAAppDefault(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLAAppDefault()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mrestoreAppsTrafficStat(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->restoreAppsTrafficStat()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mrestoreSLAAppUpgrade(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->restoreSLAAppUpgrade(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msendMsgSetSLAAppList(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Z
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgSetSLAAppList()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msetDoubleDataUidToSlad(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setDoubleDataUidToSlad()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetDoubleWifiUidToSlad(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setDoubleWifiUidToSlad()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetUidWhiteListToSlad(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setUidWhiteListToSlad()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstoreSLAAppUpgrade(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->storeSLAAppUpgrade(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetmAppLists()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmLinkTurboMode()I
    .locals 1

    sget v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mLinkTurboMode:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmSLAAppLibLock()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfputmIsUidChangeReboot(Z)V
    .locals 0

    sput-boolean p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mIsUidChangeReboot:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmLinkTurboMode(I)V
    .locals 0

    sput p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mLinkTurboMode:I

    return-void
.end method

.method static bridge synthetic -$$Nest$smsetSLAMode()V
    .locals 0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setSLAMode()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mTraffic:Ljava/util/HashMap;

    .line 101
    const/4 v0, -0x2

    sput v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mLinkTurboMode:I

    .line 105
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    .line 144
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppUpgradeList:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    const/16 v0, 0xfa0

    iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCalcTrafficInterval:I

    .line 94
    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I

    .line 95
    const/4 v0, 0x1

    iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mMonth:I

    .line 99
    const/4 v1, 0x0

    iput v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCount:I

    .line 106
    new-instance v2, Ljava/util/concurrent/Semaphore;

    invoke-direct {v2, v0}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSemaphore:Ljava/util/concurrent/Semaphore;

    .line 112
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleWifiUidList:Ljava/lang/String;

    .line 116
    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    .line 120
    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataUidList:Ljava/lang/String;

    .line 124
    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLSGameUidList:Ljava/lang/String;

    .line 127
    iput v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLSVoIPUid:I

    .line 155
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    .line 156
    new-instance v2, Ldatabase/SlaDbSchema/SlaBaseHelper;

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Ldatabase/SlaDbSchema/SlaBaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->slaBaseHelper:Ldatabase/SlaDbSchema/SlaBaseHelper;

    .line 157
    invoke-virtual {v2}, Ldatabase/SlaDbSchema/SlaBaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    .line 158
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLSGameAppPN:Ljava/util/Set;

    .line 159
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppDefaultPN:Ljava/util/Set;

    .line 160
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleWifiAppPN:Ljava/util/Set;

    .line 161
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataAppPN:Ljava/util/Set;

    .line 162
    sput-boolean v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mIsUidChangeReboot:Z

    .line 163
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->syncSLAAppFromDB()V

    .line 164
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLAUIObserver()V

    .line 165
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initBroadcastReceiver()V

    .line 167
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "link_turbo_mode"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v0, v2, :cond_0

    .line 168
    sput v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mLinkTurboMode:I

    goto :goto_0

    .line 170
    :cond_0
    sput v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mLinkTurboMode:I

    .line 173
    :goto_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 174
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 175
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mTraffic:Ljava/util/HashMap;

    sget-object v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 178
    .end local v0    # "i":I
    :cond_1
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 179
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDay()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I

    .line 180
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDay()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mMonth:I

    .line 184
    :cond_2
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    .line 185
    const-string v1, "netstats"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/usage/NetworkStatsManager;

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mNetworkStatsManager:Landroid/app/usage/NetworkStatsManager;

    .line 187
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SlaUidHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mHandlerThread:Landroid/os/HandlerThread;

    .line 188
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 189
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$1;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$1;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mHandler:Landroid/os/Handler;

    .line 210
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "DatabaseHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDataBaseThread:Landroid/os/HandlerThread;

    .line 211
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 212
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$2;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDataBaseThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$2;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDataBaseHandler:Landroid/os/Handler;

    .line 245
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLMCloudObserver()V

    .line 246
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLACloudObserver()V

    .line 247
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLAAppDefault()V

    .line 248
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initDoubleDataCloudObserver()V

    .line 249
    return-void
.end method

.method private UpdateSLAAppList(Ljava/util/List;Z)V
    .locals 6
    .param p2, "change"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;",
            ">;Z)V"
        }
    .end annotation

    .line 624
    .local p1, "src":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;"
    if-nez p1, :cond_0

    .line 625
    return-void

    .line 627
    :cond_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    monitor-enter v0

    .line 628
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 629
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    .line 630
    .local v2, "tmpapp":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    move-result-object v3

    .line 631
    .local v3, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    if-eqz p2, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-virtual {v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getState()Z

    move-result v4

    if-nez v4, :cond_1

    .line 632
    sget-object v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 633
    invoke-direct {p0, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbDeleteSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V

    .line 634
    goto :goto_1

    .line 636
    :cond_1
    if-eqz v3, :cond_2

    .line 637
    iget v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I

    invoke-virtual {v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDay(I)V

    .line 638
    iget v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mMonth:I

    invoke-virtual {v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonth(I)V

    .line 639
    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDayTraffic(J)V

    .line 640
    invoke-virtual {v3, v4, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonthTraffic(J)V

    .line 628
    .end local v2    # "tmpapp":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    .end local v3    # "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 643
    .end local v1    # "i":I
    :cond_3
    monitor-exit v0

    .line 644
    return-void

    .line 643
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addOrRemoveWhiteList(Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .param p2, "remove"    # Z
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "uidList"    # Ljava/lang/String;
    .param p5, "isOperateBlacklist"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .line 745
    .local p1, "whiteUidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    monitor-enter v0

    .line 746
    if-eqz p1, :cond_a

    .line 747
    :try_start_0
    const-string v1, ","

    invoke-virtual {p3, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 749
    .local v1, "packages":[Ljava/lang/String;
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiBlackUidSets:Ljava/util/Set;

    if-nez v2, :cond_0

    .line 750
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiBlackUidSets:Ljava/util/Set;

    .line 752
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_3

    .line 753
    if-eqz p2, :cond_1

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiAppPN:Ljava/util/Set;

    aget-object v4, v1, v2

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 754
    if-nez p5, :cond_2

    .line 755
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiAppPN:Ljava/util/Set;

    aget-object v4, v1, v2

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 756
    :cond_1
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiAppPN:Ljava/util/Set;

    aget-object v4, v1, v2

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 757
    if-nez p5, :cond_2

    .line 758
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiAppPN:Ljava/util/Set;

    aget-object v4, v1, v2

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 752
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 762
    .end local v2    # "i":I
    :cond_3
    const-string v2, ","

    invoke-virtual {p4, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 763
    .local v2, "uids":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    array-length v4, v2

    if-ge v3, v4, :cond_8

    .line 764
    if-eqz p2, :cond_5

    .line 765
    if-eqz p5, :cond_4

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiBlackUidSets:Ljava/util/Set;

    aget-object v5, v2, v3

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 766
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiBlackUidSets:Ljava/util/Set;

    aget-object v5, v2, v3

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 767
    :cond_4
    if-nez p5, :cond_7

    aget-object v4, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 768
    aget-object v4, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_3

    .line 770
    :cond_5
    if-eqz p5, :cond_6

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiBlackUidSets:Ljava/util/Set;

    aget-object v5, v2, v3

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 771
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiBlackUidSets:Ljava/util/Set;

    aget-object v5, v2, v3

    invoke-interface {v4, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 772
    :cond_6
    if-nez p5, :cond_7

    aget-object v4, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 773
    aget-object v4, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 763
    :cond_7
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 777
    .end local v3    # "i":I
    :cond_8
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_9

    .line 778
    const-string v3, ","

    invoke-static {v3, p1}, Ljava/lang/String;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    .line 779
    .local v3, "curWhites":Ljava/lang/String;
    iput-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    .line 780
    .end local v3    # "curWhites":Ljava/lang/String;
    goto :goto_4

    .line 781
    :cond_9
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    .line 784
    .end local v1    # "packages":[Ljava/lang/String;
    .end local v2    # "uids":[Ljava/lang/String;
    :cond_a
    :goto_4
    monitor-exit v0

    .line 785
    return-void

    .line 784
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addSLAApp(Ljava/lang/String;)I
    .locals 6
    .param p1, "uid"    # Ljava/lang/String;

    .line 1427
    const/4 v0, 0x0

    .line 1428
    .local v0, "ret":I
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1429
    :try_start_0
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    move-result-object v2

    .line 1430
    .local v2, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    if-nez v2, :cond_0

    .line 1431
    new-instance v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-direct {v3, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;-><init>(Ljava/lang/String;)V

    move-object v2, v3

    .line 1432
    sget-object v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1433
    sget-object v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mTraffic:Ljava/util/HashMap;

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1434
    const/4 v0, 0x1

    goto :goto_0

    .line 1436
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setState(Z)V

    .line 1437
    const/4 v0, 0x0

    .line 1439
    .end local v2    # "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    :goto_0
    monitor-exit v1

    .line 1440
    return v0

    .line 1439
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private addSLAAppDefault(I)V
    .locals 5
    .param p1, "uid"    # I

    .line 1398
    const/4 v0, 0x0

    .line 1399
    .local v0, "slaDefUid":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    move-result-object v0

    .line 1400
    if-nez v0, :cond_0

    .line 1401
    new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 1402
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1403
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mTraffic:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1404
    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbAddSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V

    goto :goto_0

    .line 1406
    :cond_0
    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbUpdateSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V

    .line 1408
    :goto_0
    return-void
.end method

.method private calcAppsTrafficStat()V
    .locals 16

    .line 282
    move-object/from16 v0, p0

    const/4 v1, 0x0

    .line 283
    .local v1, "uid":I
    const-wide/16 v2, 0x0

    .line 284
    .local v2, "traffic":J
    const-wide/16 v4, 0x0

    .line 285
    .local v4, "curtraffic":J
    const/4 v6, 0x0

    .line 286
    .local v6, "state":Z
    invoke-direct/range {p0 .. p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->isDateChanged()I

    move-result v7

    .line 287
    .local v7, "change":I
    const/4 v8, 0x0

    .line 288
    .local v8, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;"
    invoke-direct/range {p0 .. p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->cloneSLAApp()Ljava/util/List;

    move-result-object v8

    .line 289
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x1

    if-ge v9, v10, :cond_0

    .line 290
    iget v9, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCalcTrafficInterval:I

    int-to-long v9, v9

    invoke-direct {v0, v9, v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->postCalcAppsTrafficStat(J)V

    .line 291
    return-void

    .line 294
    :cond_0
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v11

    if-ge v9, v11, :cond_8

    .line 295
    const/4 v6, 0x0

    .line 296
    const-wide/16 v2, 0x0

    .line 297
    const-wide/16 v4, 0x0

    .line 299
    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    .line 300
    .local v11, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    invoke-virtual {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 301
    invoke-virtual {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getState()Z

    move-result v6

    .line 302
    const-string v13, "SLM-SRV-SLAAppLib"

    const-wide/16 v14, 0x0

    if-nez v6, :cond_2

    .line 303
    if-nez v7, :cond_1

    .line 304
    goto/16 :goto_2

    .line 305
    :cond_1
    if-ne v7, v10, :cond_2

    .line 306
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "IS_DAY_CHANGE state = "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v13, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    invoke-virtual {v11, v14, v15}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDayTraffic(J)V

    .line 308
    iget v10, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I

    invoke-virtual {v11, v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDay(I)V

    .line 312
    :cond_2
    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getDataConsumedForUid(I)J

    move-result-wide v2

    .line 314
    const/4 v10, 0x0

    .line 315
    .local v10, "lCurTrafficLong":Ljava/lang/Long;
    sget-object v12, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mTraffic:Ljava/util/HashMap;

    invoke-virtual {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    move-object v10, v12

    check-cast v10, Ljava/lang/Long;

    .line 317
    if-eqz v10, :cond_3

    .line 318
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 322
    :cond_3
    const-wide/16 v14, 0x0

    cmp-long v12, v4, v14

    if-nez v12, :cond_4

    .line 323
    move-wide v4, v2

    .line 327
    :cond_4
    cmp-long v12, v4, v2

    if-lez v12, :cond_5

    .line 329
    move-wide v2, v4

    .line 332
    :cond_5
    if-nez v7, :cond_6

    .line 334
    invoke-virtual {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDayTraffic()J

    move-result-wide v12

    sub-long v14, v2, v4

    add-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDayTraffic(J)V

    .line 335
    invoke-virtual {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getMonthTraffic()J

    move-result-wide v12

    sub-long v14, v2, v4

    add-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonthTraffic(J)V

    goto :goto_1

    .line 336
    :cond_6
    const/4 v12, 0x2

    if-ne v7, v12, :cond_7

    .line 337
    const-string v12, "IS_MONTH_CHANGE"

    invoke-static {v13, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    const-wide/16 v12, 0x0

    invoke-virtual {v11, v12, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDayTraffic(J)V

    .line 339
    invoke-virtual {v11, v12, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonthTraffic(J)V

    .line 340
    iget v12, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I

    invoke-virtual {v11, v12}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDay(I)V

    .line 341
    iget v12, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mMonth:I

    invoke-virtual {v11, v12}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonth(I)V

    goto :goto_1

    .line 343
    :cond_7
    const-string v12, "IS_DAY_CHANGE"

    invoke-static {v13, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    const-wide/16 v12, 0x0

    invoke-virtual {v11, v12, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDayTraffic(J)V

    .line 345
    invoke-virtual {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getMonthTraffic()J

    move-result-wide v12

    sub-long v14, v2, v4

    add-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonthTraffic(J)V

    .line 346
    iget v12, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I

    invoke-virtual {v11, v12}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDay(I)V

    .line 349
    :goto_1
    move-wide v4, v2

    .line 350
    sget-object v12, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mTraffic:Ljava/util/HashMap;

    invoke-virtual {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v13

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    iput-wide v4, v11, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mCurTraffic:J

    .line 294
    .end local v10    # "lCurTrafficLong":Ljava/lang/Long;
    .end local v11    # "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    :goto_2
    add-int/lit8 v9, v9, 0x1

    const/4 v10, 0x1

    goto/16 :goto_0

    .line 356
    .end local v9    # "i":I
    :cond_8
    const/4 v9, 0x2

    if-ne v7, v9, :cond_9

    .line 357
    invoke-direct/range {p0 .. p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->cloneSLAApp()Ljava/util/List;

    move-result-object v9

    const/4 v10, 0x1

    invoke-direct {v0, v9, v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->UpdateSLAAppList(Ljava/util/List;Z)V

    .line 361
    :cond_9
    sget-boolean v9, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mIsSladRunning:Z

    if-eqz v9, :cond_a

    .line 362
    iget v9, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCalcTrafficInterval:I

    int-to-long v9, v9

    invoke-direct {v0, v9, v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->postCalcAppsTrafficStat(J)V

    .line 364
    :cond_a
    return-void
.end method

.method private clearSLAApp(Ljava/lang/String;)V
    .locals 4
    .param p1, "uid"    # Ljava/lang/String;

    .line 1462
    const/4 v0, 0x0

    .line 1464
    .local v0, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    const-string v1, "SLM-SRV-SLAAppLib"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clearSLAApp uid = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1465
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1466
    :try_start_0
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    move-result-object v2

    move-object v0, v2

    .line 1467
    if-eqz v0, :cond_0

    .line 1468
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1470
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1471
    if-eqz v0, :cond_1

    .line 1472
    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbDeleteSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V

    .line 1473
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "update_uidlist_to_sla"

    .line 1474
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;

    move-result-object v3

    .line 1473
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1476
    :cond_1
    return-void

    .line 1470
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private cloneSLAApp()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;",
            ">;"
        }
    .end annotation

    .line 616
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 617
    .local v0, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;"
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    monitor-enter v1

    .line 618
    :try_start_0
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 619
    monitor-exit v1

    .line 620
    return-object v0

    .line 619
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private dbAcquirePermit(Ljava/lang/String;)I
    .locals 4
    .param p1, "action"    # Ljava/lang/String;

    .line 949
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "action:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " acquire a permit"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "SLM-SRV-SLAAppLib"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 951
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 952
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " acquire a permit success"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 957
    nop

    .line 958
    const/4 v0, 0x0

    return v0

    .line 953
    :catch_0
    move-exception v0

    .line 954
    .local v0, "e":Ljava/lang/InterruptedException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "an InterruptedException hanppened! action:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 955
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 956
    const/4 v1, -0x1

    return v1
.end method

.method private dbAddSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V
    .locals 5
    .param p1, "app"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    .line 968
    if-nez p1, :cond_0

    .line 969
    return-void

    .line 971
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dbAddSLAApp uid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLAAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 972
    invoke-static {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getContentValues(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)Landroid/content/ContentValues;

    move-result-object v0

    .line 973
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "add"

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbAcquirePermit(Ljava/lang/String;)I

    .line 974
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "SlaUid"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 975
    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbReleasePermit(Ljava/lang/String;)V

    .line 976
    return-void
.end method

.method private dbDeleteSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V
    .locals 6
    .param p1, "app"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    .line 992
    if-nez p1, :cond_0

    .line 993
    return-void

    .line 996
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dbDeleteSLAApp uid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLAAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    invoke-static {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getContentValues(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)Landroid/content/ContentValues;

    move-result-object v0

    .line 998
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "del"

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbAcquirePermit(Ljava/lang/String;)I

    .line 999
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v3

    const-string v4, "SlaUid"

    const-string/jumbo v5, "uid = ?"

    invoke-virtual {v2, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1000
    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbReleasePermit(Ljava/lang/String;)V

    .line 1001
    return-void
.end method

.method private dbGetCursorSLAApp(Ljava/lang/String;[Ljava/lang/String;)Ldatabase/SlaDbSchema/SlaCursorWrapper;
    .locals 8
    .param p1, "whereClaue"    # Ljava/lang/String;
    .param p2, "whereArgs"    # [Ljava/lang/String;

    .line 1004
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SlaUid"

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1013
    .local v0, "cursor":Landroid/database/Cursor;
    new-instance v1, Ldatabase/SlaDbSchema/SlaCursorWrapper;

    invoke-direct {v1, v0}, Ldatabase/SlaDbSchema/SlaCursorWrapper;-><init>(Landroid/database/Cursor;)V

    return-object v1
.end method

.method private dbReleasePermit(Ljava/lang/String;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/String;

    .line 962
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->drainPermits()I

    .line 963
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 964
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "action:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " releases a permit"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLAAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    return-void
.end method

.method private dbUpdateSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V
    .locals 6
    .param p1, "app"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    .line 979
    if-nez p1, :cond_0

    .line 980
    return-void

    .line 983
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dbUpdateSLAApp uid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLAAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 984
    invoke-static {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getContentValues(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)Landroid/content/ContentValues;

    move-result-object v0

    .line 985
    .local v0, "values":Landroid/content/ContentValues;
    const-string/jumbo v1, "update"

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbAcquirePermit(Ljava/lang/String;)I

    .line 986
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    .line 987
    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v3

    .line 986
    const-string v4, "SlaUid"

    const-string/jumbo v5, "uid = ?"

    invoke-virtual {v2, v4, v0, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 988
    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbReleasePermit(Ljava/lang/String;)V

    .line 989
    return-void
.end method

.method private deleteSLAApp(Ljava/lang/String;)I
    .locals 4
    .param p1, "uid"    # Ljava/lang/String;

    .line 1450
    const/4 v0, 0x0

    .line 1451
    .local v0, "ret":I
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1452
    :try_start_0
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    move-result-object v2

    .line 1453
    .local v2, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    if-eqz v2, :cond_0

    .line 1454
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setState(Z)V

    .line 1455
    const/4 v0, 0x1

    .line 1457
    .end local v2    # "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    :cond_0
    monitor-exit v1

    .line 1458
    return v0

    .line 1457
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private deleteSLAAppDefault(I)V
    .locals 2
    .param p1, "uid"    # I

    .line 1411
    const/4 v0, 0x0

    .line 1412
    .local v0, "slaDefUid":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    move-result-object v0

    .line 1413
    if-eqz v0, :cond_0

    .line 1414
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1415
    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbDeleteSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V

    .line 1417
    :cond_0
    return-void
.end method

.method private fetchDoubleDataWhiteListApp()V
    .locals 12

    .line 1552
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fetchDoubleDataWhiteListApp: mDoubleDataUidList = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataUidList:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mDoubleDataAppPN size = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataAppPN:Ljava/util/Set;

    .line 1553
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1552
    const-string v1, "SLM-SRV-SLAAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1555
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataUidList:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataAppPN:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1556
    :cond_0
    const-string v0, "reset mDoubleDataAppPN and mDoubleDataUidList"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1557
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataAppPN:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1558
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataUidList:Ljava/lang/String;

    .line 1562
    :cond_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "dual_data_concurrent_mode_white_list_pkg_name"

    invoke-static {v0, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1564
    .local v0, "tput_data":Ljava/lang/String;
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "dual_data_concurrent_limited_white_list_pkg_name"

    invoke-static {v2, v3}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1566
    .local v2, "limited_data":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1567
    .local v3, "whiteString_doubleData":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cloud DDApp: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1568
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1569
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1570
    .local v5, "packages":[Ljava/lang/String;
    if-eqz v5, :cond_2

    .line 1571
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    array-length v7, v5

    if-ge v6, v7, :cond_2

    .line 1572
    iget-object v7, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataAppPN:Ljava/util/Set;

    aget-object v8, v5, v6

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1571
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1578
    .end local v5    # "packages":[Ljava/lang/String;
    .end local v6    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 1579
    .local v5, "pm":Landroid/content/pm/PackageManager;
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v6

    .line 1580
    .local v6, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/PackageInfo;

    .line 1581
    .local v8, "app":Landroid/content/pm/PackageInfo;
    iget-object v9, v8, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v9, :cond_4

    iget-object v9, v8, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataAppPN:Ljava/util/Set;

    iget-object v10, v8, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 1582
    invoke-interface {v9, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1583
    iget-object v9, v8, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 1584
    .local v9, "uid":I
    iget-object v10, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataUidList:Ljava/lang/String;

    if-nez v10, :cond_3

    .line 1585
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataUidList:Ljava/lang/String;

    goto :goto_2

    .line 1587
    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataUidList:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataUidList:Ljava/lang/String;

    .line 1590
    .end local v8    # "app":Landroid/content/pm/PackageInfo;
    .end local v9    # "uid":I
    :cond_4
    :goto_2
    goto :goto_1

    .line 1591
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mDoubleDataUidList = "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v7, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataUidList:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1592
    return-void
.end method

.method private generateFinalWhiteList(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 912
    .local p1, "uidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 913
    const-string v0, ""

    return-object v0

    .line 915
    :cond_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    monitor-enter v0

    .line 916
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiBlackUidSets:Ljava/util/Set;

    if-eqz v1, :cond_2

    .line 917
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 918
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiBlackUidSets:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 919
    .local v2, "blackUid":Ljava/lang/String;
    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 920
    invoke-interface {p1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 921
    .end local v2    # "blackUid":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 923
    :cond_2
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 925
    const-string v0, ","

    invoke-static {v0, p1}, Ljava/lang/String;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 927
    .local v0, "finalUids":Ljava/lang/String;
    return-object v0

    .line 923
    .end local v0    # "finalUids":Ljava/lang/String;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static get(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 147
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    if-nez v0, :cond_0

    .line 148
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    .line 151
    :cond_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    return-object v0
.end method

.method private static getContentValues(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)Landroid/content/ContentValues;
    .locals 3
    .param p0, "app"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    .line 1040
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1041
    .local v0, "values":Landroid/content/ContentValues;
    const-string/jumbo v1, "uid"

    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1042
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDayTraffic()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "dayTraffic"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1043
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getMonthTraffic()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "monthTraffic"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1044
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getState()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "state"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1045
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDay()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "day"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1046
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getMonth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "month"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1048
    return-object v0
.end method

.method private static getDataConsumedForUid(I)J
    .locals 14
    .param p0, "uid"    # I

    .line 378
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mNetworkStatsManager:Landroid/app/usage/NetworkStatsManager;

    if-eqz v0, :cond_4

    .line 379
    const/4 v0, 0x0

    .line 380
    .local v0, "result":Landroid/app/usage/NetworkStats;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 381
    .local v8, "currentTime":J
    const-wide/16 v10, 0x0

    .line 382
    .local v10, "totalTxBytes":J
    const-wide/16 v12, 0x0

    .line 384
    .local v12, "totalRxBytes":J
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mNetworkStatsManager:Landroid/app/usage/NetworkStatsManager;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-wide v6, v8

    invoke-virtual/range {v1 .. v7}, Landroid/app/usage/NetworkStatsManager;->querySummary(ILjava/lang/String;JJ)Landroid/app/usage/NetworkStats;

    move-result-object v1

    move-object v0, v1

    .line 390
    new-instance v1, Landroid/app/usage/NetworkStats$Bucket;

    invoke-direct {v1}, Landroid/app/usage/NetworkStats$Bucket;-><init>()V

    .line 391
    .local v1, "bucket":Landroid/app/usage/NetworkStats$Bucket;
    :goto_0
    invoke-virtual {v0}, Landroid/app/usage/NetworkStats;->hasNextBucket()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 392
    invoke-virtual {v0, v1}, Landroid/app/usage/NetworkStats;->getNextBucket(Landroid/app/usage/NetworkStats$Bucket;)Z

    .line 393
    invoke-virtual {v1}, Landroid/app/usage/NetworkStats$Bucket;->getUid()I

    move-result v2

    .line 395
    .local v2, "summaryUid":I
    if-ne v2, p0, :cond_0

    .line 396
    invoke-virtual {v1}, Landroid/app/usage/NetworkStats$Bucket;->getTxBytes()J

    move-result-wide v3

    add-long/2addr v10, v3

    .line 397
    invoke-virtual {v1}, Landroid/app/usage/NetworkStats$Bucket;->getRxBytes()J

    move-result-wide v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-long/2addr v12, v3

    .line 399
    .end local v2    # "summaryUid":I
    :cond_0
    goto :goto_0

    .line 402
    .end local v1    # "bucket":Landroid/app/usage/NetworkStats$Bucket;
    :cond_1
    if-eqz v0, :cond_3

    .line 403
    :goto_1
    invoke-virtual {v0}, Landroid/app/usage/NetworkStats;->close()V

    goto :goto_2

    .line 402
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_2

    .line 403
    invoke-virtual {v0}, Landroid/app/usage/NetworkStats;->close()V

    .line 405
    :cond_2
    throw v1

    .line 400
    :catch_0
    move-exception v1

    .line 402
    if-eqz v0, :cond_3

    .line 403
    goto :goto_1

    .line 406
    :cond_3
    :goto_2
    add-long v1, v10, v12

    return-wide v1

    .line 408
    .end local v0    # "result":Landroid/app/usage/NetworkStats;
    .end local v8    # "currentTime":J
    .end local v10    # "totalTxBytes":J
    .end local v12    # "totalRxBytes":J
    :cond_4
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method private getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    .locals 2
    .param p1, "uid"    # Ljava/lang/String;

    .line 648
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 649
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 650
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    return-object v1

    .line 648
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 654
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private initBroadcastReceiver()V
    .locals 3

    .line 1243
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1244
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1245
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1246
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1247
    const-string v1, "com.android.phone.intent.action.DUAL_DATA_CONCURRENT_MODE_WHITE_LIST_DONE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1248
    const-string v1, "com.android.phone.intent.action.DUAL_DATA_CONCURRENT_LIMITED_WHITE_LIST_DONE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1249
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1250
    new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V

    .line 1350
    .local v1, "receiver":Landroid/content/BroadcastReceiver;
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1351
    return-void
.end method

.method private initDoubleDataCloudObserver()V
    .locals 4

    .line 1532
    const-string v0, "SLM-SRV-SLAAppLib"

    const-string v1, "initDoubleDataWhiteListApp"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1533
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->fetchDoubleDataWhiteListApp()V

    .line 1534
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$7;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$7;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Handler;)V

    .line 1544
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1545
    const-string v2, "dual_data_concurrent_mode_white_list_pkg_name"

    invoke-static {v2}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1544
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1546
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1547
    const-string v2, "dual_data_concurrent_limited_white_list_pkg_name"

    invoke-static {v2}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1546
    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1548
    return-void
.end method

.method private initSLAAppDefault()V
    .locals 23

    .line 1354
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppDefaultPN:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1355
    return-void

    .line 1358
    :cond_0
    iget-object v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    .line 1359
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1358
    const-string v2, "cloud_sla_whitelist"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1360
    .local v1, "whiteString_sla":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cloud SLAAppDefault: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SLM-SRV-SLAAppLib"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1361
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1362
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1363
    .local v2, "packages":[Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 1364
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, v2

    if-ge v4, v5, :cond_1

    .line 1365
    iget-object v5, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppDefaultPN:Ljava/util/Set;

    aget-object v6, v2, v4

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1364
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1371
    .end local v2    # "packages":[Ljava/lang/String;
    .end local v4    # "i":I
    :cond_1
    iget-object v2, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppDefaultPN:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1373
    const-string v2, "ro.boot.hwc"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "CN"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1374
    const-string v4, "com.tencent.mm"

    const-string v5, "com.tencent.mobileqq"

    const-string v6, "com.eg.android.AlipayGphone"

    const-string v7, "com.taobao.taobao"

    const-string v8, "com.tmall.wireless"

    const-string v9, "com.jingdong.app.mall"

    const-string v10, "com.xiaomi.youpin"

    const-string v11, "com.tencent.mtt"

    const-string v12, "com.hupu.games"

    const-string v13, "com.zhihu.android"

    const-string v14, "com.dianping.v1"

    const-string v15, "com.tencent.qqmusic"

    const-string v16, "com.netease.cloudmusic"

    const-string v17, "com.UCMobile"

    const-string v18, "com.ss.android.article.news"

    const-string v19, "com.smile.gifmaker"

    const-string v20, "com.ss.android.ugc.aweme"

    const-string/jumbo v21, "tv.danmaku.bili"

    const-string v22, "org.zwanoo.android.speedtest"

    filled-new-array/range {v4 .. v22}, [Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "packages":[Ljava/lang/String;
    goto :goto_1

    .line 1383
    .end local v2    # "packages":[Ljava/lang/String;
    :cond_2
    const-string v4, "com.spotify.music"

    const-string v5, "com.ebay.mobile"

    const-string v6, "com.amazon.kindle"

    const-string v7, "com.instagram.android"

    const-string v8, "com.melodis.midomiMusicIdentifier.freemium"

    const-string v9, "com.google.android.youtube"

    filled-new-array/range {v4 .. v9}, [Ljava/lang/String;

    move-result-object v2

    .line 1388
    .restart local v2    # "packages":[Ljava/lang/String;
    :goto_1
    nop

    .line 1389
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_2
    array-length v5, v2

    if-ge v4, v5, :cond_3

    .line 1390
    iget-object v5, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppDefaultPN:Ljava/util/Set;

    aget-object v6, v2, v4

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1389
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1394
    .end local v2    # "packages":[Ljava/lang/String;
    .end local v4    # "i":I
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "set SLAAppDefault: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppDefaultPN:Ljava/util/Set;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1395
    return-void
.end method

.method private initSLACloudObserver()V
    .locals 4

    .line 1052
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Handler;)V

    .line 1085
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1086
    const-string v2, "cloud_sla_whitelist"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1085
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1087
    return-void
.end method

.method private initSLAUIObserver()V
    .locals 4

    .line 1226
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$5;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$5;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Handler;)V

    .line 1238
    .local v0, "mContentObserver":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1239
    const-string v2, "link_turbo_mode"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1238
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1240
    return-void
.end method

.method private initSLMCloudObserver()V
    .locals 4

    .line 1090
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Handler;)V

    .line 1216
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1217
    const-string v2, "cloud_sls_whitelist"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1216
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1218
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1219
    const-string v2, "cloud_double_wifi_uidlist"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1218
    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1220
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda2;

    invoke-direct {v2, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda2;-><init>(Landroid/database/ContentObserver;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1222
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1223
    return-void
.end method

.method private isDateChanged()I
    .locals 10

    .line 417
    const/4 v0, 0x0

    .line 418
    .local v0, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    const/4 v1, 0x0

    .line 419
    .local v1, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;"
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->cloneSLAApp()Ljava/util/List;

    move-result-object v1

    .line 420
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    monitor-enter v2

    .line 421
    :try_start_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-ge v3, v5, :cond_0

    .line 422
    monitor-exit v2

    return v4

    .line 424
    :cond_0
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    move-object v0, v3

    .line 426
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 427
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDay()I

    move-result v2

    .line 428
    .local v2, "day":I
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getMonth()I

    move-result v3

    .line 430
    .local v3, "month":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 431
    .local v6, "c":Ljava/util/Calendar;
    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v8

    add-int/2addr v8, v5

    .line 432
    .local v8, "localmonth":I
    const/4 v9, 0x5

    invoke-virtual {v6, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    .line 434
    .local v9, "localday":I
    if-eq v3, v8, :cond_1

    if-lez v3, :cond_1

    .line 435
    iput v9, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I

    .line 436
    iput v8, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mMonth:I

    .line 437
    return v7

    .line 438
    :cond_1
    if-eq v2, v9, :cond_2

    if-lez v2, :cond_2

    .line 439
    iput v9, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I

    .line 440
    return v5

    .line 442
    :cond_2
    return v4

    .line 426
    .end local v2    # "day":I
    .end local v3    # "month":I
    .end local v6    # "c":Ljava/util/Calendar;
    .end local v8    # "localmonth":I
    .end local v9    # "localday":I
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method static synthetic lambda$initSLMCloudObserver$0(Landroid/database/ContentObserver;)V
    .locals 1
    .param p0, "observer"    # Landroid/database/ContentObserver;

    .line 1221
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 1222
    return-void
.end method

.method private postCalcAppsTrafficStat(J)V
    .locals 2
    .param p1, "delay"    # J

    .line 271
    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCount:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_0

    .line 272
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->restoreAppsTrafficStat()V

    .line 274
    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCount:I

    .line 276
    :cond_0
    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCount:I

    .line 277
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xc9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 278
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 279
    return-void
.end method

.method private static printTraffic(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V
    .locals 4
    .param p0, "app"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    .line 368
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+++++++++++++++++++app uid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLAAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "app day traffic: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDayTraffic()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->formatBytes(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "app month traffic: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getMonthTraffic()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->formatBytes(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current traffic: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mCurTraffic:J

    invoke-static {v2, v3}, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->formatBytes(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "app state: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getState()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "app day: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDay()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "app month: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getMonth()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    return-void
.end method

.method private printUidLists()V
    .locals 8

    .line 659
    const-string v0, ""

    .line 660
    .local v0, "activeuidlist":Ljava/lang/String;
    const-string v1, ""

    .line 662
    .local v1, "deactiveuidlist":Ljava/lang/String;
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->cloneSLAApp()Ljava/util/List;

    move-result-object v2

    .line 664
    .local v2, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 665
    return-void

    .line 668
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_2

    .line 669
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-virtual {v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getState()Z

    move-result v5

    const-string v6, ","

    if-ne v5, v4, :cond_1

    .line 670
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-virtual {v7}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 672
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-virtual {v7}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 668
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 676
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "activeuidlist:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "SLM-SRV-SLAAppLib"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "deactiveuidlist:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    return-void
.end method

.method private refreshSLAAppUpgradeList()V
    .locals 8

    .line 1493
    const-wide/32 v0, 0x493e0

    .line 1494
    .local v0, "refreshInterval":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 1495
    .local v2, "now":J
    sget-object v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppUpgradeList:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 1498
    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1499
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 1500
    .local v5, "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v6, v0

    cmp-long v6, v6, v2

    if-gez v6, :cond_0

    .line 1501
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1504
    .end local v5    # "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    :cond_1
    return-void
.end method

.method private restoreAppsTrafficStat()V
    .locals 3

    .line 447
    const/4 v0, 0x0

    .line 448
    .local v0, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;"
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    monitor-enter v1

    .line 449
    :try_start_0
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->cloneSLAApp()Ljava/util/List;

    move-result-object v2

    move-object v0, v2

    .line 450
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 452
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 453
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbUpdateSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V

    .line 452
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 455
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 450
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private restoreSLAAppUpgrade(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;

    .line 1520
    const/4 v0, 0x0

    .line 1521
    .local v0, "ret":Z
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->refreshSLAAppUpgradeList()V

    .line 1523
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppUpgradeList:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1524
    const/4 v0, 0x1

    .line 1525
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppUpgradeList:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1528
    :cond_0
    return v0
.end method

.method private sendMsgSetSLAAppList()Z
    .locals 3

    .line 608
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 609
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x68

    iput v1, v0, Landroid/os/Message;->what:I

    .line 610
    const-string v1, "SLM-SRV-SLAAppLib"

    const-string/jumbo v2, "sendMsgSetSLAAppList"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDataBaseHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 612
    const/4 v1, 0x1

    return v1
.end method

.method private sendMsgStartCalc()V
    .locals 2

    .line 257
    const-string v0, "SLM-SRV-SLAAppLib"

    const-string/jumbo v1, "sendMsgStartCalc"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 259
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0xc9

    iput v1, v0, Landroid/os/Message;->what:I

    .line 260
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 261
    return-void
.end method

.method private sendMsgStopCalc()V
    .locals 2

    .line 264
    const-string v0, "SLM-SRV-SLAAppLib"

    const-string/jumbo v1, "sendMsgStopCalc"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 266
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0xcb

    iput v1, v0, Landroid/os/Message;->what:I

    .line 267
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 268
    return-void
.end method

.method private sendMsgUpdateTrafficStat()V
    .locals 2

    .line 1606
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1607
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0xcb

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1608
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1609
    return-void
.end method

.method private setDoubleDataUidToSlad()V
    .locals 4

    .line 931
    const-string/jumbo v0, "setDoubleDataUidToSlad"

    const-string v1, "SLM-SRV-SLAAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 932
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    if-eqz v0, :cond_1

    .line 934
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setDDUidList:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataUidList:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleDataUidList:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 936
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-interface {v2, v0}, Lvendor/qti/sla/service/V1_0/ISlaService;->setDDUidList(Ljava/lang/String;)V

    goto :goto_0

    .line 938
    :cond_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    const-string v2, "NULL"

    invoke-interface {v0, v2}, Lvendor/qti/sla/service/V1_0/ISlaService;->setDDUidList(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 940
    :catch_0
    move-exception v0

    .line 941
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 942
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    goto :goto_1

    .line 944
    :cond_1
    const-string v0, "mSlaService is null"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 946
    :goto_1
    return-void
.end method

.method private setDoubleWifiUidToSlad()V
    .locals 9

    .line 845
    const-string/jumbo v0, "setDoubleWifiUidToSlad"

    const-string v1, "SLM-SRV-SLAAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 846
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    if-eqz v0, :cond_8

    .line 848
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    .line 849
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "is_opened_dual_wifi"

    .line 848
    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    move v0, v2

    .line 851
    .local v0, "isSDKOpended":Z
    const/4 v2, 0x0

    .line 852
    .local v2, "uidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v4, " mSDKDoubleWifiBlackUidSets = "

    const-string v5, ","

    const-string v6, "NULL"

    if-eqz v0, :cond_3

    .line 853
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "setDWUidList isSDKOpend = true DoubleWifiUidList = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v7, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiBlackUidSets:Ljava/util/Set;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 857
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 858
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->stream([Ljava/lang/Object;)Ljava/util/stream/Stream;

    move-result-object v3

    new-instance v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0;

    invoke-direct {v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0;-><init>()V

    .line 859
    invoke-interface {v3, v4}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v3

    new-instance v4, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;

    invoke-direct {v4}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;-><init>()V

    invoke-static {v4}, Ljava/util/stream/Collectors;->toCollection(Ljava/util/function/Supplier;)Ljava/util/stream/Collector;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    move-object v2, v3

    .line 861
    invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->generateFinalWhiteList(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    .line 862
    .local v3, "finalUids":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setDWUidList isSDKOpend= true final DoubleWifiUidList = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 865
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 866
    sget-object v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-interface {v4, v3}, Lvendor/qti/sla/service/V1_0/ISlaService;->setDWUidList(Ljava/lang/String;)V

    goto :goto_1

    .line 868
    :cond_1
    sget-object v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-interface {v4, v6}, Lvendor/qti/sla/service/V1_0/ISlaService;->setDWUidList(Ljava/lang/String;)V

    .line 870
    .end local v3    # "finalUids":Ljava/lang/String;
    :goto_1
    goto/16 :goto_4

    .line 871
    :cond_2
    sget-object v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-interface {v3, v6}, Lvendor/qti/sla/service/V1_0/ISlaService;->setDWUidList(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 874
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "setDWUidList isSDKOpend= false mDoubleWifiUidList = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleWifiUidList:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mSDKDoubleWifiUidList = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v7, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiBlackUidSets:Ljava/util/Set;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleWifiUidList:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 879
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDoubleWifiUidList:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->stream([Ljava/lang/Object;)Ljava/util/stream/Stream;

    move-result-object v4

    new-instance v7, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0;

    invoke-direct {v7}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0;-><init>()V

    .line 880
    invoke-interface {v4, v7}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v4

    new-instance v7, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;

    invoke-direct {v7}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;-><init>()V

    invoke-static {v7}, Ljava/util/stream/Collectors;->toCollection(Ljava/util/function/Supplier;)Ljava/util/stream/Collector;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    move-object v2, v4

    goto :goto_2

    .line 882
    :cond_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object v2, v4

    .line 885
    :goto_2
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    if-eqz v4, :cond_6

    .line 886
    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 887
    .local v4, "sdkUids":[Ljava/lang/String;
    array-length v5, v4

    :goto_3
    if-ge v3, v5, :cond_6

    aget-object v7, v4, v3

    .line 888
    .local v7, "sdkUid":Ljava/lang/String;
    invoke-interface {v2, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 889
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 887
    .end local v7    # "sdkUid":Ljava/lang/String;
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 893
    .end local v4    # "sdkUids":[Ljava/lang/String;
    :cond_6
    invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->generateFinalWhiteList(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    .line 894
    .restart local v3    # "finalUids":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setDWUidList isSDKOpend= false final DoubleWifiUidList = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 898
    sget-object v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-interface {v4, v3}, Lvendor/qti/sla/service/V1_0/ISlaService;->setDWUidList(Ljava/lang/String;)V

    goto :goto_4

    .line 900
    :cond_7
    sget-object v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-interface {v4, v6}, Lvendor/qti/sla/service/V1_0/ISlaService;->setDWUidList(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 903
    .end local v0    # "isSDKOpended":Z
    .end local v2    # "uidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "finalUids":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 904
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_4
    goto :goto_5

    .line 907
    :cond_8
    const-string v0, "mSlaService is null"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 909
    :goto_5
    return-void
.end method

.method private static setSLAMode()V
    .locals 4

    .line 789
    const-string/jumbo v0, "setSLAMode"

    const-string v1, "SLM-SRV-SLAAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    if-eqz v0, :cond_1

    .line 792
    :try_start_0
    sget v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mLinkTurboMode:I

    if-nez v0, :cond_0

    .line 793
    const-string/jumbo v0, "setSLAMode:1"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    const-string v2, "1"

    invoke-interface {v0, v2}, Lvendor/qti/sla/service/V1_0/ISlaService;->setSLAMode(Ljava/lang/String;)V

    goto :goto_0

    .line 796
    :cond_0
    const-string/jumbo v0, "setSLAMode:0"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    const-string v2, "0"

    invoke-interface {v0, v2}, Lvendor/qti/sla/service/V1_0/ISlaService;->setSLAMode(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 799
    :catch_0
    move-exception v0

    .line 800
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 801
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    goto :goto_1

    .line 803
    :cond_1
    const-string v0, "mSlaService is null"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    :goto_1
    return-void
.end method

.method private setUidWhiteListToSlad()V
    .locals 9

    .line 808
    const-string/jumbo v0, "setUidWhiteListToSlad"

    const-string v1, "SLM-SRV-SLAAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;

    move-result-object v0

    .line 810
    .local v0, "uidlist":Ljava/lang/String;
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    if-eqz v2, :cond_4

    .line 812
    const-string v3, "NULL"

    if-eqz v0, :cond_3

    .line 814
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 815
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const-string v4, "com.android.providers.downloads.ui"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 816
    .local v4, "downloadui_uid":I
    const-string v6, "com.android.providers.downloads"

    invoke-virtual {v2, v6, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 817
    .local v5, "download_uid":I
    const-string v6, ","

    const/4 v7, -0x1

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    .line 818
    :try_start_1
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-ne v8, v7, :cond_0

    .line 819
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-eq v8, v7, :cond_0

    .line 820
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v0, v6

    goto :goto_0

    .line 822
    :cond_0
    if-eqz v4, :cond_1

    if-eqz v5, :cond_1

    .line 823
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-eq v8, v7, :cond_1

    .line 824
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-ne v8, v7, :cond_1

    .line 825
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v0, v6

    .line 827
    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_2

    .line 828
    sget-object v6, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-interface {v6, v3}, Lvendor/qti/sla/service/V1_0/ISlaService;->setSLAUidList(Ljava/lang/String;)V

    .line 829
    return-void

    .line 831
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "setSLAUidList:"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    sget-object v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-interface {v3, v0}, Lvendor/qti/sla/service/V1_0/ISlaService;->setSLAUidList(Ljava/lang/String;)V

    .line 833
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    .end local v4    # "downloadui_uid":I
    .end local v5    # "download_uid":I
    goto :goto_2

    .line 836
    :catch_0
    move-exception v2

    goto :goto_1

    .line 834
    :cond_3
    invoke-interface {v2, v3}, Lvendor/qti/sla/service/V1_0/ISlaService;->setSLAUidList(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 837
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 838
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    goto :goto_3

    .line 840
    :cond_4
    const-string v2, "mSlaService is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    :goto_3
    return-void
.end method

.method private storeSLAAppUpgrade(Ljava/lang/String;)V
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;

    .line 1507
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 1509
    .local v0, "now":J
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->refreshSLAAppUpgradeList()V

    .line 1511
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppUpgradeList:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1512
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppUpgradeList:Ljava/util/HashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1514
    :cond_0
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppUpgradeList:Ljava/util/HashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1517
    :goto_0
    return-void
.end method

.method private syncSLAAppFromDB()V
    .locals 5

    .line 1019
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1020
    .local v0, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;"
    const/4 v1, 0x0

    invoke-direct {p0, v1, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbGetCursorSLAApp(Ljava/lang/String;[Ljava/lang/String;)Ldatabase/SlaDbSchema/SlaCursorWrapper;

    move-result-object v1

    .line 1022
    .local v1, "cursor":Ldatabase/SlaDbSchema/SlaCursorWrapper;
    :try_start_0
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1023
    invoke-virtual {v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->moveToFirst()Z

    .line 1024
    :goto_0
    invoke-virtual {v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1025
    invoke-virtual {v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getSLAApp()Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1026
    invoke-virtual {v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 1029
    :cond_0
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    monitor-enter v2

    .line 1030
    :try_start_1
    sget-object v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1031
    sget-object v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1032
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1033
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->printUidLists()V

    .line 1034
    invoke-virtual {v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->close()V

    .line 1035
    nop

    .line 1036
    return-void

    .line 1032
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 1029
    :catchall_1
    move-exception v2

    sget-object v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    monitor-enter v3

    .line 1030
    :try_start_3
    sget-object v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1031
    sget-object v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mAppLists:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1032
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1033
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->printUidLists()V

    .line 1034
    invoke-virtual {v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->close()V

    .line 1035
    throw v2

    .line 1032
    :catchall_2
    move-exception v2

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v2
.end method


# virtual methods
.method public getLinkTurboAppsTraffic()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;",
            ">;"
        }
    .end annotation

    .line 504
    const/4 v0, 0x0

    .line 505
    .local v0, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;"
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->cloneSLAApp()Ljava/util/List;

    move-result-object v0

    .line 507
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 508
    const/4 v1, 0x0

    return-object v1

    .line 511
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 512
    .local v1, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 514
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 512
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 517
    .end local v2    # "i":I
    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgUpdateTrafficStat()V

    .line 518
    return-object v1
.end method

.method getLinkTurboDefaultPn()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 489
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 490
    .local v0, "defaultPn":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppDefaultPN:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 491
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppDefaultPN:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 493
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLSGameAppPN:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 494
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLSGameAppPN:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 496
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 497
    const-string v1, "SLM-SRV-SLAAppLib"

    const-string v2, "LinkTurboDefaultPn is empty"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    :cond_2
    return-object v0
.end method

.method public getLinkTurboWhiteList()Ljava/lang/String;
    .locals 7

    .line 459
    const-string v0, ""

    .line 460
    .local v0, "uidlist":Ljava/lang/String;
    const/4 v1, 0x0

    .line 462
    .local v1, "i":I
    const/4 v2, 0x0

    .line 463
    .local v2, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;"
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->cloneSLAApp()Ljava/util/List;

    move-result-object v2

    .line 464
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x0

    if-nez v3, :cond_0

    .line 465
    return-object v4

    .line 468
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 469
    .local v3, "uids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 470
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-virtual {v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getState()Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 471
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-virtual {v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 469
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 475
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_3

    .line 476
    return-object v4

    .line 479
    :cond_3
    const/4 v1, 0x0

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 480
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 479
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 483
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getLinkTurboWhiteList:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "SLM-SRV-SLAAppLib"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    return-object v0
.end method

.method public sendMsgAddSLAUid(Ljava/lang/String;)Z
    .locals 7
    .param p1, "uid"    # Ljava/lang/String;

    .line 536
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 539
    .local v0, "msg":Landroid/os/Message;
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->addSLAApp(Ljava/lang/String;)I

    move-result v1

    .line 540
    .local v1, "ret":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 541
    const/16 v3, 0x64

    iput v3, v0, Landroid/os/Message;->what:I

    .line 542
    const-string v3, "SLM-SRV-SLAAppLib"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MSG_DB_SLAAPP_ADD:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 543
    :cond_0
    if-nez v1, :cond_1

    .line 544
    const/16 v3, 0x66

    iput v3, v0, Landroid/os/Message;->what:I

    .line 545
    const-string v3, "SLM-SRV-SLAAppLib"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MSG_DB_SLAAPP_UPDATE:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    :cond_1
    :goto_0
    const/4 v3, 0x0

    .line 550
    .local v3, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    sget-object v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    monitor-enter v4

    .line 551
    :try_start_0
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    move-result-object v5

    move-object v3, v5

    .line 552
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 553
    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 554
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDataBaseHandler:Landroid/os/Handler;

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 557
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "update_uidlist_to_sla"

    .line 558
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;

    move-result-object v6

    .line 557
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 559
    return v2

    .line 552
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public sendMsgDelSLAUid(Ljava/lang/String;)Z
    .locals 7
    .param p1, "uid"    # Ljava/lang/String;

    .line 564
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 567
    .local v0, "msg":Landroid/os/Message;
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->deleteSLAApp(Ljava/lang/String;)I

    move-result v1

    .line 568
    .local v1, "ret":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 569
    const/16 v3, 0x65

    iput v3, v0, Landroid/os/Message;->what:I

    .line 570
    const-string v3, "SLM-SRV-SLAAppLib"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MSG_DB_SLAAPP_DEL:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    const/4 v3, 0x0

    .line 577
    .local v3, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    sget-object v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    monitor-enter v4

    .line 578
    :try_start_0
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    move-result-object v5

    move-object v3, v5

    .line 579
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 580
    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 581
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDataBaseHandler:Landroid/os/Handler;

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 584
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "update_uidlist_to_sla"

    .line 585
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;

    move-result-object v6

    .line 584
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 586
    return v2

    .line 579
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 572
    .end local v3    # "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public sendMsgDoubleDataUid()Z
    .locals 4

    .line 599
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 600
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x69

    iput v1, v0, Landroid/os/Message;->what:I

    .line 601
    const-string v1, "SLM-SRV-SLAAppLib"

    const-string/jumbo v2, "sendMsgDoubleDataUid"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDataBaseHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    .line 603
    const/4 v1, 0x1

    return v1
.end method

.method public sendMsgDoubleWifiUid()Z
    .locals 4

    .line 591
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 592
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x67

    iput v1, v0, Landroid/os/Message;->what:I

    .line 593
    const-string v1, "SLM-SRV-SLAAppLib"

    const-string/jumbo v2, "sendMsgDoubleWifiUid"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDataBaseHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    .line 595
    const/4 v1, 0x1

    return v1
.end method

.method public setDDSLAMode()V
    .locals 4

    .line 1597
    const-string v0, "SLM-SRV-SLAAppLib"

    :try_start_0
    const-string/jumbo v1, "setSLAMode: SLA_MODE_CONCURRENT"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1598
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    const-string v2, "0"

    invoke-interface {v1, v2}, Lvendor/qti/sla/service/V1_0/ISlaService;->setSLAMode(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1601
    goto :goto_0

    .line 1599
    :catch_0
    move-exception v1

    .line 1600
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1602
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z
    .locals 11
    .param p1, "type"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "uidList"    # Ljava/lang/String;
    .param p4, "isOperateBlacklist"    # Z

    .line 682
    const-string v0, "SLM-SRV-SLAAppLib"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setDoubleWifiWhiteList type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " packageName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " uidList = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    const-string v0, "SLM-SRV-SLAAppLib"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pre setDoubleWifiWhiteList mSDKDoubleWifiUidList = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mSDKDoubleWifiAppPN = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiAppPN:Ljava/util/Set;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mSDKDoubleWifiBlackUidSets = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiBlackUidSets:Ljava/util/Set;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    const/4 v0, 0x0

    if-eqz p2, :cond_6

    if-nez p3, :cond_0

    goto/16 :goto_3

    .line 690
    :cond_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLAAppLibLock:Ljava/lang/Object;

    monitor-enter v1

    .line 691
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 692
    const-string v2, ""

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    .line 694
    :cond_1
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiAppPN:Ljava/util/Set;

    if-nez v2, :cond_2

    .line 695
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiAppPN:Ljava/util/Set;

    .line 697
    :cond_2
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->stream([Ljava/lang/Object;)Ljava/util/stream/Stream;

    move-result-object v2

    new-instance v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0;

    invoke-direct {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0;-><init>()V

    .line 698
    invoke-interface {v2, v3}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v2

    new-instance v3, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;

    invoke-direct {v3}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;-><init>()V

    invoke-static {v3}, Ljava/util/stream/Collectors;->toCollection(Ljava/util/function/Supplier;)Ljava/util/stream/Collector;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Ljava/util/List;

    .line 700
    .local v4, "whiteUidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x1

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_2

    .line 726
    :pswitch_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    .line 727
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiAppPN:Ljava/util/Set;

    .line 728
    goto/16 :goto_2

    .line 716
    :pswitch_1
    const-string v3, ","

    invoke-virtual {p3, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 717
    .local v3, "uids":[Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v6, v3

    if-ge v5, v6, :cond_5

    .line 718
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiBlackUidSets:Ljava/util/Set;

    if-eqz v6, :cond_3

    aget-object v7, v3, v5

    .line 719
    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v6, v2

    goto :goto_1

    :cond_3
    move v6, v0

    .line 720
    .local v6, "isInBlackSet":Z
    :goto_1
    if-eqz v6, :cond_4

    .line 721
    monitor-exit v1

    return v0

    .line 717
    .end local v6    # "isInBlackSet":Z
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 724
    .end local v5    # "i":I
    :cond_5
    monitor-exit v1

    return v2

    .line 708
    .end local v3    # "uids":[Ljava/lang/String;
    :pswitch_2
    const-string v0, ","

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->stream([Ljava/lang/Object;)Ljava/util/stream/Stream;

    move-result-object v0

    new-instance v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0;

    invoke-direct {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0;-><init>()V

    .line 709
    invoke-interface {v0, v3}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v0

    new-instance v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda1;

    invoke-direct {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda1;-><init>()V

    invoke-static {v3}, Ljava/util/stream/Collectors;->toCollection(Ljava/util/function/Supplier;)Ljava/util/stream/Collector;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 711
    .local v0, "packagesSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiAppPN:Ljava/util/Set;

    .line 713
    iput-object p3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    .line 714
    goto :goto_2

    .line 702
    .end local v0    # "packagesSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :pswitch_3
    const/4 v5, 0x1

    move-object v3, p0

    move-object v6, p2

    move-object v7, p3

    move v8, p4

    invoke-direct/range {v3 .. v8}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->addOrRemoveWhiteList(Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;Z)V

    .line 703
    goto :goto_2

    .line 705
    :pswitch_4
    const/4 v7, 0x0

    move-object v5, p0

    move-object v6, v4

    move-object v8, p2

    move-object v9, p3

    move v10, p4

    invoke-direct/range {v5 .. v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->addOrRemoveWhiteList(Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;Z)V

    .line 706
    nop

    .line 732
    .end local v4    # "whiteUidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 734
    const-string v0, "SLM-SRV-SLAAppLib"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "after setDoubleWifiWhiteList mSDKDoubleWifiUidList = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiUidList:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " mSDKDoubleWifiAppPN = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiAppPN:Ljava/util/Set;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " mSDKDoubleWifiBlackUidSets = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSDKDoubleWifiBlackUidSets:Ljava/util/Set;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    return v2

    .line 732
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 688
    :cond_6
    :goto_3
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setSlaService(Lvendor/qti/sla/service/V1_0/ISlaService;)V
    .locals 0
    .param p1, "service"    # Lvendor/qti/sla/service/V1_0/ISlaService;

    .line 252
    sput-object p1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    .line 253
    return-void
.end method

.method public startSladHandle()V
    .locals 1

    .line 522
    const/4 v0, 0x1

    sput-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mIsSladRunning:Z

    .line 523
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgStartCalc()V

    .line 525
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setUidWhiteListToSlad()V

    .line 526
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setSLAMode()V

    .line 527
    return-void
.end method

.method public stopSladHandle()V
    .locals 1

    .line 530
    const/4 v0, 0x0

    sput-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mIsSladRunning:Z

    .line 531
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgStopCalc()V

    .line 532
    return-void
.end method
