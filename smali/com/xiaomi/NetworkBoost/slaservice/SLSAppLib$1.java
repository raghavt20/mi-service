class com.xiaomi.NetworkBoost.slaservice.SLSAppLib$1 implements com.xiaomi.NetworkBoost.StatusManager$IAppStatusListener {
	 /* .source "SLSAppLib.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.SLSAppLib this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.slaservice.SLSAppLib$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib; */
/* .line 350 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAudioChanged ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "mode" # I */
/* .line 393 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "initBroadcastReceiver mode:"; // const-string v1, "initBroadcastReceiver mode:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLSAppLib"; // const-string v1, "SLM-SRV-SLSAppLib"
android.util.Log .d ( v1,v0 );
/* .line 394 */
int v0 = 3; // const/4 v0, 0x3
/* const/16 v1, 0x65 */
/* const/16 v2, 0x64 */
/* if-ne p1, v0, :cond_0 */
/* .line 397 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$fgetmHandler ( v0 );
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 398 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$fgetmHandler ( v0 );
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 399 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$fgetmHandler ( v0 );
/* const-wide/16 v3, 0x2710 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 401 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 404 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$fgetmHandler ( v0 );
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 405 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$fgetmHandler ( v0 );
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 406 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$fgetmHandler ( v0 );
/* const-wide/16 v2, 0x1388 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 409 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void onForegroundInfoChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 3 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .line 354 */
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$sfgetmSlaAppList ( );
/* iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
java.lang.Integer .toString ( v1 );
v0 = (( java.util.HashSet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 355 */
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$sfgetmSlsAppList ( );
/* iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
java.lang.Integer .toString ( v1 );
v0 = (( java.util.HashSet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 356 */
v0 = this.this$0;
/* iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
java.lang.Integer .toString ( v1 );
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$msetSLSGameStart ( v0,v1 );
/* .line 357 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
/* iget v2, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$msetPowerMgrGameInfo ( v0,v1,v2 );
/* goto/16 :goto_0 */
/* .line 360 */
} // :cond_0
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$sfgetmSlaAppList ( );
/* iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
java.lang.Integer .toString ( v1 );
v0 = (( java.util.HashSet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$sfgetmSlsAppList ( );
/* iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
/* .line 361 */
java.lang.Integer .toString ( v1 );
v0 = (( java.util.HashSet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_3 */
/* .line 362 */
} // :cond_1
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$sfgetmSlaAppList ( );
/* iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I */
java.lang.Integer .toString ( v1 );
v0 = (( java.util.HashSet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$sfgetmSlsAppList ( );
/* iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I */
/* .line 363 */
java.lang.Integer .toString ( v1 );
v0 = (( java.util.HashSet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 364 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$sfgetmSLSUid ( );
/* iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I */
final String v2 = "SLM-SRV-SLSAppLib"; // const-string v2, "SLM-SRV-SLSAppLib"
/* if-ne v0, v1, :cond_2 */
/* .line 365 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onForegroundInfoChanged stop game uid:"; // const-string v1, "onForegroundInfoChanged stop game uid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v0 );
/* .line 366 */
v0 = this.this$0;
/* iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I */
java.lang.Integer .toString ( v1 );
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$msetSLSGameStop ( v0,v1 );
/* .line 369 */
} // :cond_2
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$fgetpowerMgrGameUid ( v0 );
/* iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I */
/* if-ne v0, v1, :cond_3 */
/* .line 370 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onForegroundInfoChanged reset gameinfo uid:"; // const-string v1, "onForegroundInfoChanged reset gameinfo uid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v0 );
/* .line 371 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
/* iget v2, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I */
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$msetPowerMgrGameInfo ( v0,v1,v2 );
/* .line 375 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void onUidGone ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "disabled" # Z */
/* .line 380 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$sfgetmSLSUid ( );
/* if-ne v0, p1, :cond_0 */
/* .line 381 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onUidGone uid:"; // const-string v1, "onUidGone uid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLSAppLib"; // const-string v1, "SLM-SRV-SLSAppLib"
android.util.Log .d ( v1,v0 );
/* .line 382 */
v0 = this.this$0;
java.lang.Integer .toString ( p1 );
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$msetSLSGameStop ( v0,v1 );
/* .line 385 */
} // :cond_0
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$fgetpowerMgrGameUid ( v0 );
/* if-ne v0, p1, :cond_1 */
/* .line 386 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$msetPowerMgrGameInfo ( v0,v1,p1 );
/* .line 388 */
} // :cond_1
return;
} // .end method
