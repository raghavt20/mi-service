class com.xiaomi.NetworkBoost.slaservice.SLAService$2 implements com.xiaomi.NetworkBoost.StatusManager$INetworkInterfaceListener {
	 /* .source "SLAService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.SLAService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.slaservice.SLAService$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* .line 950 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onNetwrokInterfaceChange ( java.lang.String p0, Integer p1, Boolean p2, Boolean p3, Boolean p4, Boolean p5, java.lang.String p6 ) {
/* .locals 2 */
/* .param p1, "ifacename" # Ljava/lang/String; */
/* .param p2, "ifacenum" # I */
/* .param p3, "wifiready" # Z */
/* .param p4, "slavewifiready" # Z */
/* .param p5, "dataready" # Z */
/* .param p6, "slaveDataReady" # Z */
/* .param p7, "status" # Ljava/lang/String; */
/* .line 955 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfputmInterface ( p1 );
/* .line 956 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfputmIfaceNumber ( p2 );
/* .line 957 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfputmWifiReady ( p3 );
/* .line 958 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfputmSlaveWifiReady ( p4 );
/* .line 959 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfputmDataReady ( p5 );
/* .line 960 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfputmSlaveDataReady ( p6 );
/* .line 962 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "interface name:"; // const-string v1, "interface name:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmInterface ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " interface number:"; // const-string v1, " interface number:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmIfaceNumber ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " WifiReady:"; // const-string v1, " WifiReady:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmWifiReady ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " SlaveWifiReady:"; // const-string v1, " SlaveWifiReady:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSlaveWifiReady ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " DataReady:"; // const-string v1, " DataReady:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmDataReady ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " SlaveDataReady:"; // const-string v1, " SlaveDataReady:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSlaveDataReady ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLAService"; // const-string v1, "SLM-SRV-SLAService"
android.util.Log .i ( v1,v0 );
/* .line 967 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmMiWillManager ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 968 */
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmMiWillManager ( v0 );
	 v1 = 	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmWifiReady ( );
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 v1 = 		 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSlaveWifiReady ( );
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 int v1 = 1; // const/4 v1, 0x1
		 } // :cond_0
		 int v1 = 0; // const/4 v1, 0x0
	 } // :goto_0
	 (( com.xiaomi.NetworkBoost.slaservice.MiWillManager ) v0 ).setDualWifiReady ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->setDualWifiReady(Z)V
	 /* .line 970 */
} // :cond_1
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$mprocessNetworkCallback ( v0,p7 );
/* .line 971 */
return;
} // .end method
