.class Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$InternalHandler;
.super Landroid/os/Handler;
.source "SLSAppLib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;


# direct methods
.method public constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 414
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    .line 415
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 416
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 419
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "SLM-SRV-SLSAppLib"

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 426
    :pswitch_0
    const-string v0, "EVENT_STOP_VOIP"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$msetSLSVoIPStop(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)V

    .line 428
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$msetPowerMgrVoIPInfo(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;Z)V

    .line 429
    goto :goto_0

    .line 421
    :pswitch_1
    const-string v0, "EVENT_START_VOIP"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$msetSLSVoIPStart(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)V

    .line 423
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$msetPowerMgrVoIPInfo(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;Z)V

    .line 424
    nop

    .line 433
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
