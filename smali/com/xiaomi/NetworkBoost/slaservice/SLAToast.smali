.class public Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;
.super Ljava/lang/Object;
.source "SLAToast.java"


# static fields
.field private static final DELAY_TIME:J = 0x64L

.field private static TAG:Ljava/lang/String; = null

.field private static final TOAST_LOOPER:I = 0x1

.field private static final TOAST_TIME:I = 0x7d0

.field private static mApps:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;


# instance fields
.field private isLinkTurboEnable:Z

.field private mAppStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetisLinkTurboEnable(Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->isLinkTurboEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmApps()Ljava/util/HashSet;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mApps:Ljava/util/HashSet;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 26
    const-string v0, "SLM-SRV-SLAToast"

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 40
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$1;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$1;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mAppStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;

    .line 65
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->TAG:Ljava/lang/String;

    const-string v1, "SLAToast"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mContext:Landroid/content/Context;

    .line 67
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mApps:Ljava/util/HashSet;

    .line 68
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->initSlaToastHandler()V

    .line 69
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->registerAppStatusListener()V

    .line 70
    return-void
.end method

.method private initSlaToastHandler()V
    .locals 2

    .line 82
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SlaToastHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mHandlerThread:Landroid/os/HandlerThread;

    .line 83
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 84
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$2;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$2;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mHandler:Landroid/os/Handler;

    .line 100
    return-void
.end method

.method private registerAppStatusListener()V
    .locals 4

    .line 74
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 75
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mAppStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerAppStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public static setLinkTurboUidList(Ljava/lang/String;)V
    .locals 4
    .param p0, "uidList"    # Ljava/lang/String;

    .line 107
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mApps:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 108
    if-nez p0, :cond_0

    .line 109
    return-void

    .line 112
    :cond_0
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "temp":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 114
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mApps:Ljava/util/HashSet;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 113
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 116
    .end local v1    # "i":I
    :cond_1
    return-void
.end method


# virtual methods
.method public getSLAToastHandlerThread()Landroid/os/HandlerThread;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->mHandlerThread:Landroid/os/HandlerThread;

    return-object v0
.end method

.method public setLinkTurboStatus(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 103
    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->isLinkTurboEnable:Z

    .line 104
    return-void
.end method
