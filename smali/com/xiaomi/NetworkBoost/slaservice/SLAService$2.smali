.class Lcom/xiaomi/NetworkBoost/slaservice/SLAService$2;
.super Ljava/lang/Object;
.source "SLAService.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 950
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNetwrokInterfaceChange(Ljava/lang/String;IZZZZLjava/lang/String;)V
    .locals 2
    .param p1, "ifacename"    # Ljava/lang/String;
    .param p2, "ifacenum"    # I
    .param p3, "wifiready"    # Z
    .param p4, "slavewifiready"    # Z
    .param p5, "dataready"    # Z
    .param p6, "slaveDataReady"    # Z
    .param p7, "status"    # Ljava/lang/String;

    .line 955
    invoke-static {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfputmInterface(Ljava/lang/String;)V

    .line 956
    invoke-static {p2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfputmIfaceNumber(I)V

    .line 957
    invoke-static {p3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfputmWifiReady(Z)V

    .line 958
    invoke-static {p4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfputmSlaveWifiReady(Z)V

    .line 959
    invoke-static {p5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfputmDataReady(Z)V

    .line 960
    invoke-static {p6}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfputmSlaveDataReady(Z)V

    .line 962
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "interface name:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmInterface()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " interface number:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmIfaceNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WifiReady:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmWifiReady()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " SlaveWifiReady:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSlaveWifiReady()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DataReady:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmDataReady()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " SlaveDataReady:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSlaveDataReady()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLAService"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 967
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmMiWillManager(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 968
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmMiWillManager(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmWifiReady()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSlaveWifiReady()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->setDualWifiReady(Z)V

    .line 970
    :cond_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0, p7}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$mprocessNetworkCallback(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Ljava/lang/String;)V

    .line 971
    return-void
.end method
