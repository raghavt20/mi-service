.class public Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;
.super Ljava/lang/Object;
.source "SLSAppLib.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$InternalHandler;
    }
.end annotation


# static fields
.field private static final EVENT_START_VOIP:I = 0x64

.field private static final EVENT_STOP_VOIP:I = 0x65

.field static final TAG:Ljava/lang/String; = "SLM-SRV-SLSAppLib"

.field private static final VOIP_START_DELAY_TIME:I = 0x2710

.field private static final VOIP_STOP_DELAY_TIME:I = 0x1388

.field private static mSLSUid:I

.field private static mSLSVoIPUid:I

.field private static mSlaAppList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

.field private static mSlsAppList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private isSLSEnable:Z

.field private isSLSGameRunning:Z

.field private isSLSVoIPRunning:Z

.field private mActivityManager:Landroid/app/IActivityManager;

.field private mAppStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mSLATrack:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

.field private mSLMService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/xiaomi/NetworkBoost/slaservice/SLAService;",
            ">;"
        }
    .end annotation
.end field

.field private mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

.field private powerMgrGameStatus:Z

.field private powerMgrGameUid:I

.field private powerMgrSLMStatus:Z

.field private powerMgrScreenOn:Z

.field private powerMgrVoIPStatus:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetpowerMgrGameUid(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameUid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$msetPowerMgrGameInfo(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setPowerMgrGameInfo(ZI)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetPowerMgrVoIPInfo(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setPowerMgrVoIPInfo(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetSLSGameStart(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSGameStart(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msetSLSGameStop(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSGameStop(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msetSLSVoIPStart(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSVoIPStart()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetSLSVoIPStop(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSVoIPStop()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetmSLSUid()I
    .locals 1

    sget v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLSUid:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmSlaAppList()Ljava/util/HashSet;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlaAppList:Ljava/util/HashSet;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmSlsAppList()Ljava/util/HashSet;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlsAppList:Ljava/util/HashSet;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 46
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlaAppList:Ljava/util/HashSet;

    .line 47
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlsAppList:Ljava/util/HashSet;

    .line 48
    const/4 v0, 0x0

    sput v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLSVoIPUid:I

    .line 51
    sput v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLSUid:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSGameRunning:Z

    .line 53
    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSVoIPRunning:Z

    .line 56
    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameStatus:Z

    .line 57
    iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameUid:I

    .line 58
    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrVoIPStatus:Z

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrSLMStatus:Z

    .line 60
    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrScreenOn:Z

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 350
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mAppStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;

    .line 79
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mContext:Landroid/content/Context;

    .line 80
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLMService:Ljava/lang/ref/WeakReference;

    .line 82
    invoke-static {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->getSLATrack(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLATrack:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    .line 84
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SLSAppLibHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mHandlerThread:Landroid/os/HandlerThread;

    .line 85
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 86
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$InternalHandler;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$InternalHandler;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mHandler:Landroid/os/Handler;

    .line 88
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->registerAppStatusListener()V

    .line 89
    return-void
.end method

.method private checkHAL(Ljava/lang/String;)Z
    .locals 2
    .param p1, "func"    # Ljava/lang/String;

    .line 224
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    if-nez v0, :cond_0

    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " checkHAL null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLSAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    const/4 v0, 0x0

    return v0

    .line 228
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private checkSLSStatus()V
    .locals 2

    .line 92
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameStatus:Z

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrVoIPStatus:Z

    if-nez v1, :cond_1

    :cond_0
    if-eqz v0, :cond_2

    iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrScreenOn:Z

    if-nez v1, :cond_2

    .line 93
    :cond_1
    return-void

    .line 96
    :cond_2
    if-eqz v0, :cond_3

    .line 97
    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameUid:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSGameStart(Ljava/lang/String;)Z

    goto :goto_0

    .line 98
    :cond_3
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrVoIPStatus:Z

    if-eqz v0, :cond_4

    .line 99
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 100
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 101
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSVoIPStart()V

    goto :goto_0

    .line 103
    :cond_4
    const-string v0, "SLM-SRV-SLSAppLib"

    const-string v1, "checkSLSStatus false."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :goto_0
    return-void
.end method

.method private disableSLM()V
    .locals 3

    .line 162
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLMService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 163
    .local v0, "service":Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
    if-nez v0, :cond_0

    .line 164
    const-string v1, "SLM-SRV-SLSAppLib"

    const-string v2, "enableSLM get SLAService null!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    return-void

    .line 168
    :cond_0
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLM()V

    .line 169
    return-void
.end method

.method private enableSLM()V
    .locals 3

    .line 152
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLMService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 153
    .local v0, "service":Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
    if-nez v0, :cond_0

    .line 154
    const-string v1, "SLM-SRV-SLSAppLib"

    const-string v2, "enableSLM get SLAService null!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    return-void

    .line 158
    :cond_0
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->enableSLM()V

    .line 159
    return-void
.end method

.method private powerMgrCheck()V
    .locals 2

    .line 108
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameStatus:Z

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrVoIPStatus:Z

    if-nez v1, :cond_1

    :cond_0
    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrScreenOn:Z

    if-nez v0, :cond_2

    .line 109
    :cond_1
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrSLMStatus:Z

    if-eqz v0, :cond_5

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrSLMStatus:Z

    .line 111
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->disableSLM()V

    goto :goto_0

    .line 114
    :cond_2
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrSLMStatus:Z

    if-nez v0, :cond_5

    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrSLMStatus:Z

    .line 116
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->enableSLM()V

    .line 118
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameStatus:Z

    if-eqz v0, :cond_3

    .line 119
    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameUid:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSGameStart(Ljava/lang/String;)Z

    goto :goto_0

    .line 120
    :cond_3
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrVoIPStatus:Z

    if-eqz v0, :cond_4

    .line 121
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 122
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 123
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSVoIPStart()V

    goto :goto_0

    .line 125
    :cond_4
    const-string v0, "SLM-SRV-SLSAppLib"

    const-string v1, "powerMgrCheck error."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    :cond_5
    :goto_0
    return-void
.end method

.method private registerAppStatusListener()V
    .locals 3

    .line 343
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 344
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mAppStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerAppStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    goto :goto_0

    .line 345
    :catch_0
    move-exception v0

    .line 346
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SLM-SRV-SLSAppLib"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private setMiWillGameStart(Ljava/lang/String;)Z
    .locals 3
    .param p1, "uid"    # Ljava/lang/String;

    .line 267
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLMService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 268
    .local v0, "service":Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
    if-nez v0, :cond_0

    .line 269
    const-string v1, "SLM-SRV-SLSAppLib"

    const-string/jumbo v2, "setMiWillGameStart get SLAService null!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    const/4 v1, 0x0

    return v1

    .line 273
    :cond_0
    invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMiWillGameStart(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private setMiWillGameStop(Ljava/lang/String;)Z
    .locals 3
    .param p1, "uid"    # Ljava/lang/String;

    .line 277
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLMService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 278
    .local v0, "service":Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
    if-nez v0, :cond_0

    .line 279
    const-string v1, "SLM-SRV-SLSAppLib"

    const-string/jumbo v2, "setMiWillGameStop get SLAService null!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    const/4 v1, 0x0

    return v1

    .line 283
    :cond_0
    invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMiWillGameStop(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private setPowerMgrGameInfo(ZI)V
    .locals 1
    .param p1, "status"    # Z
    .param p2, "uid"    # I

    .line 132
    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameStatus:Z

    .line 133
    if-eqz p1, :cond_0

    .line 134
    iput p2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameUid:I

    goto :goto_0

    .line 136
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrGameUid:I

    .line 138
    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrCheck()V

    .line 139
    return-void
.end method

.method private setPowerMgrVoIPInfo(Z)V
    .locals 0
    .param p1, "status"    # Z

    .line 142
    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrVoIPStatus:Z

    .line 143
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrCheck()V

    .line 144
    return-void
.end method

.method public static setSLAAppWhiteList(Ljava/lang/String;)V
    .locals 4
    .param p0, "uidList"    # Ljava/lang/String;

    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setSLAAppWhiteList:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLSAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    if-nez p0, :cond_0

    .line 195
    return-void

    .line 197
    :cond_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlaAppList:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 199
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 200
    .local v0, "temp":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 201
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlaAppList:Ljava/util/HashSet;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 200
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 203
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method private setSLSGameStart(Ljava/lang/String;)Z
    .locals 5
    .param p1, "uid"    # Ljava/lang/String;

    .line 288
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSEnable:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSGameRunning:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSVoIPRunning:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 292
    :cond_0
    const-string v0, "SLM-SRV-SLSAppLib"

    if-nez p1, :cond_1

    .line 293
    const-string/jumbo v2, "setSLSGameStart null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    return v1

    .line 297
    :cond_1
    const-string/jumbo v2, "setSLSGameStart"

    invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->checkHAL(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 298
    return v1

    .line 300
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setSLSGameStart:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setWifiBSSID()Z

    move-result v2

    if-nez v2, :cond_3

    return v1

    .line 302
    :cond_3
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSGameRunning:Z

    .line 303
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sput v3, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLSUid:I

    .line 304
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->sendMsgSlsStart()V

    .line 307
    :try_start_0
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setMiWillGameStart(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 308
    sget-object v3, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-interface {v3, p1}, Lvendor/qti/sla/service/V1_0/ISlaService;->setSLSGameStart(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    :cond_4
    nop

    .line 315
    return v2

    .line 310
    :catch_0
    move-exception v2

    .line 311
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    return v1

    .line 289
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_0
    return v1
.end method

.method private setSLSGameStop(Ljava/lang/String;)Z
    .locals 5
    .param p1, "uid"    # Ljava/lang/String;

    .line 319
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSGameRunning:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 320
    return v1

    .line 323
    :cond_0
    const-string/jumbo v0, "setSLSGameStop"

    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->checkHAL(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 324
    return v1

    .line 326
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setSLSGameStop:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "SLM-SRV-SLSAppLib"

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSGameRunning:Z

    .line 328
    sput v1, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLSUid:I

    .line 329
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->sendMsgSlsStop()V

    .line 331
    :try_start_0
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setMiWillGameStop(Ljava/lang/String;)Z

    .line 332
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-interface {v0, p1}, Lvendor/qti/sla/service/V1_0/ISlaService;->setSLSGameStop(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    nop

    .line 338
    const/4 v0, 0x1

    return v0

    .line 333
    :catch_0
    move-exception v0

    .line 334
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    return v1
.end method

.method public static setSLSGameUidList(Ljava/lang/String;)V
    .locals 4
    .param p0, "uidList"    # Ljava/lang/String;

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setSLSGameUidList:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLSAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    if-nez p0, :cond_0

    .line 208
    return-void

    .line 210
    :cond_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlsAppList:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 212
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 213
    .local v0, "temp":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 214
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlsAppList:Ljava/util/HashSet;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 213
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 216
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method private setSLSVoIPStart()V
    .locals 4

    .line 438
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSEnable:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSGameRunning:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSVoIPRunning:Z

    if-eqz v0, :cond_0

    goto :goto_1

    .line 442
    :cond_0
    sget v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLSVoIPUid:I

    const-string/jumbo v1, "setSLSVoIPStart:"

    const-string v2, "SLM-SRV-SLSAppLib"

    if-gtz v0, :cond_1

    .line 443
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLSVoIPUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    return-void

    .line 447
    :cond_1
    const-string/jumbo v0, "setSLSVoIPStart"

    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->checkHAL(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 448
    return-void

    .line 450
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLSVoIPUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setWifiBSSID()Z

    move-result v0

    if-nez v0, :cond_3

    return-void

    .line 452
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSVoIPRunning:Z

    .line 454
    :try_start_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    sget v1, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLSVoIPUid:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lvendor/qti/sla/service/V1_0/ISlaService;->setSLSVoIPStart(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 457
    goto :goto_0

    .line 455
    :catch_0
    move-exception v0

    .line 456
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 439
    :cond_4
    :goto_1
    return-void
.end method

.method private setSLSVoIPStop()V
    .locals 4

    .line 461
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSVoIPRunning:Z

    if-nez v0, :cond_0

    .line 462
    return-void

    .line 465
    :cond_0
    const-string/jumbo v0, "setSLSVoIPStop"

    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->checkHAL(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 466
    return-void

    .line 468
    :cond_1
    const-string/jumbo v0, "setSLSVoIPStop:"

    const-string v1, "SLM-SRV-SLSAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSVoIPRunning:Z

    .line 471
    :try_start_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    const-string v2, "0"

    invoke-interface {v0, v2}, Lvendor/qti/sla/service/V1_0/ISlaService;->setSLSVoIPStart(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 474
    goto :goto_0

    .line 472
    :catch_0
    move-exception v0

    .line 473
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public static setSLSVoIPUid(I)V
    .locals 2
    .param p0, "uid"    # I

    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setSLSVoIPUid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLSAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    sput p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLSVoIPUid:I

    .line 221
    return-void
.end method


# virtual methods
.method public setPowerMgrScreenInfo(Z)V
    .locals 0
    .param p1, "status"    # Z

    .line 147
    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrScreenOn:Z

    .line 148
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->powerMgrCheck()V

    .line 149
    return-void
.end method

.method public setSLSEnableStatus(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setSLSEnableStatus:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLSAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSEnable:Z

    .line 178
    if-nez p1, :cond_1

    .line 179
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSGameRunning:Z

    if-eqz v0, :cond_0

    .line 180
    sget v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSLSUid:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSGameStop(Ljava/lang/String;)Z

    .line 182
    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->isSLSVoIPRunning:Z

    if-eqz v0, :cond_2

    .line 183
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 184
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 185
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSVoIPStop()V

    goto :goto_0

    .line 188
    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->checkSLSStatus()V

    .line 190
    :cond_2
    :goto_0
    return-void
.end method

.method public setSlaService(Lvendor/qti/sla/service/V1_0/ISlaService;)V
    .locals 0
    .param p1, "service"    # Lvendor/qti/sla/service/V1_0/ISlaService;

    .line 172
    sput-object p1, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    .line 173
    return-void
.end method

.method public setWifiBSSID()Z
    .locals 9

    .line 232
    const-string v0, "SLM-SRV-SLSAppLib"

    const-string/jumbo v1, "setWifiBSSID"

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->checkHAL(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 233
    return v2

    .line 236
    :cond_0
    const/4 v1, 0x0

    .line 237
    .local v1, "BSSID":Ljava/lang/String;
    const/4 v3, 0x0

    .line 238
    .local v3, "wifiBSSID":Ljava/lang/String;
    const/4 v4, 0x0

    .line 240
    .local v4, "slavewifiBSSID":Ljava/lang/String;
    :try_start_0
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mContext:Landroid/content/Context;

    const-string/jumbo v6, "wifi"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/wifi/WifiManager;

    .line 241
    .local v5, "wifimanager":Landroid/net/wifi/WifiManager;
    if-eqz v5, :cond_1

    .line 242
    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v6

    move-object v3, v6

    .line 245
    :cond_1
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mContext:Landroid/content/Context;

    const-string v7, "SlaveWifiService"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/SlaveWifiManager;

    .line 246
    .local v6, "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    if-eqz v6, :cond_2

    .line 247
    invoke-virtual {v6}, Landroid/net/wifi/SlaveWifiManager;->getWifiSlaveConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v7

    move-object v4, v7

    .line 250
    :cond_2
    if-nez v3, :cond_3

    if-nez v4, :cond_3

    .line 251
    const-string/jumbo v7, "setWifiBSSID:null"

    invoke-static {v0, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    return v2

    .line 255
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v1, v7

    .line 256
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "setWifiBSSID:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    sget-object v7, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-interface {v7, v1}, Lvendor/qti/sla/service/V1_0/ISlaService;->setSLSBSSID(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    .end local v1    # "BSSID":Ljava/lang/String;
    .end local v3    # "wifiBSSID":Ljava/lang/String;
    .end local v4    # "slavewifiBSSID":Ljava/lang/String;
    .end local v5    # "wifimanager":Landroid/net/wifi/WifiManager;
    .end local v6    # "slavewifimanager":Landroid/net/wifi/SlaveWifiManager;
    nop

    .line 263
    const/4 v0, 0x1

    return v0

    .line 258
    :catch_0
    move-exception v1

    .line 259
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    return v2
.end method
