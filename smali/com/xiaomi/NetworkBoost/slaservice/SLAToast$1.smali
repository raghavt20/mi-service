.class Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$1;
.super Ljava/lang/Object;
.source "SLAToast.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;

    .line 40
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioChanged(I)V
    .locals 0
    .param p1, "mode"    # I

    .line 61
    return-void
.end method

.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 4
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;

    .line 43
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "foreground uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isColdStart:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 44
    invoke-virtual {p1}, Lmiui/process/ForegroundInfo;->isColdStart()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isLinkTurboEnable:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->-$$Nest$fgetisLinkTurboEnable(Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 43
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-virtual {p1}, Lmiui/process/ForegroundInfo;->isColdStart()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->-$$Nest$fgetisLinkTurboEnable(Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->-$$Nest$sfgetmApps()Ljava/util/HashSet;

    move-result-object v0

    iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 49
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 50
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    .line 53
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public onUidGone(IZ)V
    .locals 0
    .param p1, "uid"    # I
    .param p2, "disabled"    # Z

    .line 57
    return-void
.end method
