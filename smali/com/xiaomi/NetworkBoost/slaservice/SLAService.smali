.class public Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
.super Ljava/lang/Object;
.source "SLAService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/NetworkBoost/slaservice/SLAService$SlaServiceDeathRecipient;,
        Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;,
        Lcom/xiaomi/NetworkBoost/slaservice/SLAService$WatchdogHandler;
    }
.end annotation


# static fields
.field public static final ACTION_START_SERVICE:Ljava/lang/String; = "ACTION_SLA_JAVA_SERVICE_START"

.field private static final CHANNEL_ID:Ljava/lang/String; = "notification_channel"

.field private static final CLOUD_DOUBLE_WIFI_MONITOR:Ljava/lang/String; = "cloud_double_wifi_monitor_controll"

.field private static final CLOUD_MIWILL_CONTROLLER_LIST:Ljava/lang/String; = "cloud_miwill_controller_list"

.field private static final CLOUD_MIWILL_DUAL_WIFI_OFF:Ljava/lang/String; = "miwill_dual_wifi_off"

.field private static final CLOUD_MIWILL_DUAL_WIFI_ON:Ljava/lang/String; = "miwill_dual_wifi_on"

.field private static final CLOUD_MIWILL_OFF:Ljava/lang/String; = "miwill_off"

.field private static final CLOUD_MIWILL_ON:Ljava/lang/String; = "miwill_on"

.field private static final COEXISTENSE:Ljava/lang/String; = "coexistense"

.field private static final EVENT_DISABLE_SLM:I = 0x69

.field private static final EVENT_ENABLE_SLM:I = 0x68

.field private static final EVENT_GET_HAL:I = 0x67

.field private static final EVENT_GET_THERMAL:I = 0x6e

.field private static final EVENT_SCREEN_OFF:I = 0x6b

.field private static final EVENT_SCREEN_ON:I = 0x6a

.field private static final EVENT_TRAFFIC_CHECK:I = 0x6c

.field private static final EVENT_UPDATE_SLAD_STATE:I = 0x64

.field private static final EVENT_WATCH_DOG_TIMEOUT:I = 0x64

.field private static final GET_SERVICE_DELAY_MILLIS:I = 0xfa0

.field private static final GET_THERMAL_DELAY_MILIS:I = 0x2710

.field private static final NOTIFACATION_RECEIVER_PACKAGE:Ljava/lang/String; = "com.android.phone"

.field private static final NOTIFICATION_ID_1:I = 0x1

.field private static final NOTIFICATION_ID_2:I = 0x2

.field private static final ON_AVAILABLE:Ljava/lang/String; = "ON_AVAILABLE"

.field private static final ON_DISABLE_EVENT:Ljava/lang/String; = "ON_DISABLE_EVENT"

.field private static final ON_LOST:Ljava/lang/String; = "ON_LOST"

.field private static final ON_USER:Ljava/lang/String; = "ON_USER"

.field private static final PROP_MIWILL_DUAL_WIFI_ENABLE:Ljava/lang/String; = "persist.log.tag.miwill_dual_wifi_enable"

.field public static final REMIND_OFF:I = 0x0

.field public static final REMIND_ON:I = 0x1

.field private static final SCREEN_OFF_DELAY_MIILLIS:I = 0x2bf20

.field private static final SETTING_VALUE_OFF:I = 0x0

.field private static final SETTING_VALUE_ON:I = 0x1

.field private static final SIGNAL_STRENGTH_GOOD:I = 0x0

.field private static final SIGNAL_STRENGTH_POOR:I = 0x1

.field private static final SLAD_STOP:Ljava/lang/String; = "0"

.field private static final SLA_NOTIFICATION_STATE:Ljava/lang/String; = "sla_notification_state"

.field private static final SLM_FEATURE_STATUS:Ljava/lang/String; = "com.android.phone.intent.action.SLM_STATUS"

.field private static final TAG:Ljava/lang/String; = "SLM-SRV-SLAService"

.field private static final THERMAL_CNT:I = 0x6

.field private static final THERMAL_DISABLE:J = 0xb1bcL

.field private static final THERMAL_ENABLE:J = 0xa028L

.field private static final THERMAL_MODE_FILE:Ljava/lang/String; = "/sys/class/thermal/thermal_message/balance_mode"

.field private static final THERMAL_PERFORMANCE_MODE:I = 0x1

.field private static final THERMAL_STATE_FILE:Ljava/lang/String; = "/sys/class/thermal/thermal_message/board_sensor_temp"

.field private static final TRAFFIC_LIMIT:I = 0x12c

.field private static final TRAFFIC_TIMER:J = 0x2710L

.field private static final WATCH_DOG_TIMEOUT:I = 0x1d4c0

.field private static mContext:Landroid/content/Context;

.field private static mDataReady:Z

.field private static mIfaceNumber:I

.field private static mInterface:Ljava/lang/String;

.field private static mLinkTurboSwitch:Z

.field private static mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

.field private static mSLATrack:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

.field private static mSLSAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

.field private static final mSemaphore:Ljava/util/concurrent/Semaphore;

.field public static final mSlaLock:Ljava/lang/Object;

.field private static mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

.field private static mSlaToast:Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;

.field private static mSlaveDataReady:Z

.field private static mSlaveWifiReady:Z

.field private static mWifiReady:Z

.field private static volatile sInstance:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

.field private static sladEnabled:Z


# instance fields
.field private final LINK_TURBO_ENABLE_PREF:Ljava/lang/String;

.field private disableSLMCnt:I

.field private mCloudObserver:Landroid/database/ContentObserver;

.field private mConnectivityChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

.field private mDefaultNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mMiWillManager:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

.field private mModemSignalStrengthListener:Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;

.field private mNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

.field public mNotificationManager:Landroid/app/NotificationManager;

.field private mSLAApp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;",
            ">;"
        }
    .end annotation
.end field

.field private mScreenStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

.field private mSingnalPoor:Z

.field private mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

.field private mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

.field private mThermalList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mVPNListener:Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;

.field private mWacthdogHandler:Landroid/os/Handler;

.field private mWacthdogHandlerThread:Landroid/os/HandlerThread;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private final mWifiStateReceiver:Landroid/content/BroadcastReceiver;

.field private misEnableDWMonitor:Z

.field private mlastWlanWwanCoexistanceStatus:I

.field private thermal_enable_slm:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetdisableSLMCnt(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmDeathRecipient(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Landroid/os/IHwBinder$DeathRecipient;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiWillManager(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mMiWillManager:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSingnalPoor(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSingnalPoor:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmMiWillManager(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mMiWillManager:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSingnalPoor(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSingnalPoor:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckMiwillCloudController(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->checkMiwillCloudController()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mchecktemp(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->checktemp(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetLinkTurboAppsTotalDayTraffic(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)J
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->getLinkTurboAppsTotalDayTraffic()J

    move-result-wide v0

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$mhandleDisableSLM(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->handleDisableSLM()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleEnableSLM(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->handleEnableSLM()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleScreenOff(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->handleScreenOff()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleScreenOn(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->handleScreenOn()V

    return-void
.end method

.method static bridge synthetic -$$Nest$misEnableDWMonitor(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isEnableDWMonitor()V

    return-void
.end method

.method static bridge synthetic -$$Nest$misSlaRemind(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSlaRemind()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mpostTrafficNotification(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->postTrafficNotification()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mprocessNetworkCallback(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->processNetworkCallback(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetMobileDataAlwaysOff(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMobileDataAlwaysOff()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetMobileDataAlwaysOn(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMobileDataAlwaysOn()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetmContext()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmDataReady()Z
    .locals 1

    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDataReady:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmIfaceNumber()I
    .locals 1

    sget v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mIfaceNumber:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmInterface()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mInterface:Ljava/lang/String;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmLinkTurboSwitch()Z
    .locals 1

    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mLinkTurboSwitch:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmSLAAppLib()Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmSLSAppLib()Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLSAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmSlaService()Lvendor/qti/sla/service/V1_0/ISlaService;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmSlaveDataReady()Z
    .locals 1

    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveDataReady:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmSlaveWifiReady()Z
    .locals 1

    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiReady:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmWifiReady()Z
    .locals 1

    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiReady:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfputmDataReady(Z)V
    .locals 0

    sput-boolean p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDataReady:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmIfaceNumber(I)V
    .locals 0

    sput p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mIfaceNumber:I

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmInterface(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mInterface:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmSlaService(Lvendor/qti/sla/service/V1_0/ISlaService;)V
    .locals 0

    sput-object p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmSlaveDataReady(Z)V
    .locals 0

    sput-boolean p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveDataReady:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmSlaveWifiReady(Z)V
    .locals 0

    sput-boolean p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiReady:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmWifiReady(Z)V
    .locals 0

    sput-boolean p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiReady:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 147
    const/4 v0, 0x0

    sput-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sladEnabled:Z

    .line 148
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    sput-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaLock:Ljava/lang/Object;

    .line 149
    sput-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mLinkTurboSwitch:Z

    .line 161
    new-instance v1, Ljava/util/concurrent/Semaphore;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    sput-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSemaphore:Ljava/util/concurrent/Semaphore;

    .line 195
    sput-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiReady:Z

    .line 196
    sput-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiReady:Z

    .line 197
    sput-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDataReady:Z

    .line 198
    sput-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveDataReady:Z

    .line 199
    const-string v1, ""

    sput-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mInterface:Ljava/lang/String;

    .line 200
    sput v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mIfaceNumber:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    const-string v0, "linkturbo_is_enable"

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->LINK_TURBO_ENABLE_PREF:Ljava/lang/String;

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAApp:Ljava/util/List;

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mMiWillManager:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    .line 187
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mCloudObserver:Landroid/database/ContentObserver;

    .line 209
    const/4 v1, 0x0

    iput v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I

    .line 217
    const/4 v2, -0x1

    iput v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mlastWlanWwanCoexistanceStatus:I

    .line 220
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 222
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 223
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    .line 224
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 227
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->misEnableDWMonitor:Z

    .line 486
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$SlaServiceDeathRecipient;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$SlaServiceDeathRecipient;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    .line 950
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$2;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$2;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    .line 984
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$3;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$3;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    .line 1038
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$4;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$4;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDefaultNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;

    .line 1085
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$5;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$5;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mVPNListener:Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;

    .line 1119
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$6;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$6;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mScreenStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    .line 1250
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->thermal_enable_slm:Z

    .line 1252
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mThermalList:Ljava/util/ArrayList;

    .line 1355
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSingnalPoor:Z

    .line 1357
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$7;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$7;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mModemSignalStrengthListener:Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;

    .line 370
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    .line 371
    return-void
.end method

.method private acquireSlaServiceLock(Ljava/lang/String;)V
    .locals 4
    .param p1, "func"    # Ljava/lang/String;

    .line 806
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Lock"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLAService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    :try_start_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 809
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " Lock success"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 813
    goto :goto_0

    .line 810
    :catch_0
    move-exception v0

    .line 811
    .local v0, "e":Ljava/lang/InterruptedException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "InterruptedException:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 812
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 814
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_0
    return-void
.end method

.method private checkMiwillCloudController()Z
    .locals 9

    .line 582
    const-string v0, "SLM-SRV-SLAService"

    const/4 v1, 0x1

    .line 584
    .local v1, "isMiwillOn":Z
    :try_start_0
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    .line 585
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "cloud_miwill_controller_list"

    .line 584
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 586
    .local v2, "miwillControllerList":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 587
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 588
    .local v3, "controllers":[Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 589
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, v3

    if-ge v4, v5, :cond_4

    .line 590
    aget-object v5, v3, v4

    const-string v6, "miwill_on"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 591
    const/4 v1, 0x1

    goto :goto_1

    .line 593
    :cond_0
    aget-object v5, v3, v4

    const-string v6, "miwill_off"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v6, "false"

    const-string v7, "persist.log.tag.miwill_dual_wifi_enable"

    if-eqz v5, :cond_1

    .line 594
    :try_start_1
    const-string v5, "checkMiwillCloudController miwill dual wifi off because miwill off"

    invoke-static {v0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    invoke-static {v7, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    const/4 v1, 0x0

    goto :goto_1

    .line 599
    :cond_1
    aget-object v5, v3, v4

    const-string v8, "miwill_dual_wifi_on"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 600
    const-string v5, "checkMiwillCloudController miwill dual wifi on"

    invoke-static {v0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    const-string/jumbo v5, "true"

    invoke-static {v7, v5}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 603
    :cond_2
    aget-object v5, v3, v4

    const-string v8, "miwill_dual_wifi_off"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 604
    const-string v5, "checkMiwillCloudController miwill dual wifi off"

    invoke-static {v0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    invoke-static {v7, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 589
    :cond_3
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 614
    .end local v2    # "miwillControllerList":Ljava/lang/String;
    .end local v3    # "controllers":[Ljava/lang/String;
    .end local v4    # "i":I
    :cond_4
    nop

    .line 616
    return v1

    .line 611
    :catch_0
    move-exception v2

    .line 612
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "cloud observer catch:"

    invoke-static {v0, v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 613
    const/4 v0, 0x0

    return v0
.end method

.method private checkWlanWwanCoexistense()V
    .locals 2

    .line 1152
    const/4 v0, -0x1

    .line 1153
    .local v0, "wlanwwanCoexistanceStatus":I
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1154
    const/4 v0, 0x1

    goto :goto_0

    .line 1156
    :cond_0
    const/4 v0, 0x0

    .line 1158
    :goto_0
    iget v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mlastWlanWwanCoexistanceStatus:I

    if-eq v0, v1, :cond_1

    .line 1159
    iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mlastWlanWwanCoexistanceStatus:I

    .line 1160
    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->notifyWlanWwanCoexistenseStatus(I)V

    .line 1162
    :cond_1
    return-void
.end method

.method private checktemp(Ljava/lang/String;)V
    .locals 12
    .param p1, "path"    # Ljava/lang/String;

    .line 1299
    const-string v0, "SLM-SRV-SLAService"

    const/4 v1, 0x0

    .line 1301
    .local v1, "reader":Ljava/io/BufferedReader;
    const/4 v2, 0x0

    .line 1302
    .local v2, "line":Ljava/lang/String;
    :try_start_0
    const-string v3, "/sys/class/thermal/thermal_message/board_sensor_temp"

    invoke-direct {p0, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->readLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    if-eqz v3, :cond_4

    .line 1303
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 1304
    .local v3, "str_temp":Ljava/lang/String;
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1305
    .local v4, "temperature":J
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mThermalList:Ljava/util/ArrayList;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1306
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mThermalList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v7, 0x6

    const/4 v8, 0x0

    if-le v6, v7, :cond_0

    .line 1307
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mThermalList:Ljava/util/ArrayList;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1309
    :cond_0
    const-wide/16 v6, 0x0

    .line 1310
    .local v6, "temperature_average":J
    iget-object v9, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mThermalList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 1311
    .local v10, "tmp":J
    add-long/2addr v6, v10

    .line 1312
    .end local v10    # "tmp":J
    goto :goto_0

    .line 1313
    :cond_1
    iget-object v9, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mThermalList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    int-to-long v9, v9

    div-long v9, v6, v9

    move-wide v6, v9

    .line 1314
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checktemp temperature = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " temperature_average = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " isPerformanceMode = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1315
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isPerformanceMode()Z

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " thermal_enable_slm = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v10, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->thermal_enable_slm:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1314
    invoke-static {v0, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1316
    iget-boolean v9, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->thermal_enable_slm:Z

    if-eqz v9, :cond_2

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isPerformanceMode()Z

    move-result v9

    if-nez v9, :cond_2

    const-wide/32 v9, 0xb1bc

    cmp-long v9, v6, v9

    if-lez v9, :cond_2

    .line 1317
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Thermal management disableSLM - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1318
    iput-boolean v8, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->thermal_enable_slm:Z

    .line 1319
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLM()V

    goto :goto_1

    .line 1320
    :cond_2
    iget-boolean v8, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->thermal_enable_slm:Z

    if-nez v8, :cond_4

    const-wide/32 v8, 0xa028

    cmp-long v8, v6, v8

    if-ltz v8, :cond_3

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isPerformanceMode()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1321
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Thermal management enableSLM - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1322
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->thermal_enable_slm:Z

    .line 1323
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->enableSLM()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1330
    .end local v2    # "line":Ljava/lang/String;
    .end local v3    # "str_temp":Ljava/lang/String;
    .end local v4    # "temperature":J
    .end local v6    # "temperature_average":J
    :cond_4
    :goto_1
    goto :goto_2

    .line 1328
    :catch_0
    move-exception v2

    .line 1329
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "checktemp:"

    invoke-static {v0, v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1331
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method

.method private createSLANotificationChannel()V
    .locals 6

    .line 1460
    const-string/jumbo v0, "xiaomi.NetworkBoost"

    .line 1461
    .local v0, "name":Ljava/lang/CharSequence;
    const-string/jumbo v1, "xiaomi.NetworkBoost.channel"

    .line 1462
    .local v1, "description":Ljava/lang/String;
    const/4 v2, 0x4

    .line 1463
    .local v2, "importance":I
    new-instance v3, Landroid/app/NotificationChannel;

    const-string v4, "notification_channel"

    invoke-direct {v3, v4, v0, v2}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 1464
    .local v3, "channel":Landroid/app/NotificationChannel;
    invoke-virtual {v3, v1}, Landroid/app/NotificationChannel;->setDescription(Ljava/lang/String;)V

    .line 1465
    sget-object v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    const-class v5, Landroid/app/NotificationManager;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    iput-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 1466
    invoke-virtual {v4, v3}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 1467
    return-void
.end method

.method private deinitCloudObserver()V
    .locals 2

    .line 651
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mCloudObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 652
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mCloudObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 653
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mCloudObserver:Landroid/database/ContentObserver;

    goto :goto_0

    .line 656
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "deinitCloudObserver ignore because: observer: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mCloudObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " context: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLAService"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    :goto_0
    return-void
.end method

.method public static destroyInstance()V
    .locals 4

    .line 354
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    if-eqz v0, :cond_1

    .line 355
    const-class v0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    monitor-enter v0

    .line 356
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 358
    :try_start_1
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->onDestroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 362
    goto :goto_0

    .line 360
    :catch_0
    move-exception v1

    .line 361
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "SLM-SRV-SLAService"

    const-string v3, "destroyInstance onDestroy catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 363
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 365
    :cond_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 367
    :cond_1
    :goto_1
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 337
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    if-nez v0, :cond_1

    .line 338
    const-class v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    monitor-enter v0

    .line 339
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    if-nez v1, :cond_0

    .line 340
    new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342
    :try_start_1
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->onCreate()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346
    goto :goto_0

    .line 344
    :catch_0
    move-exception v1

    .line 345
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "SLM-SRV-SLAService"

    const-string v3, "getInstance onCreate catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 348
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 350
    :cond_1
    :goto_1
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sInstance:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    return-object v0
.end method

.method private getLinkTurboAppsTotalDayTraffic()J
    .locals 6

    .line 1523
    const-wide/16 v0, 0x0

    .line 1524
    .local v0, "traffic":J
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->getLinkTurboAppsTraffic()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAApp:Ljava/util/List;

    .line 1525
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_0

    goto :goto_1

    .line 1528
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAApp:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 1529
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAApp:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDayTraffic()J

    move-result-wide v3

    add-long/2addr v0, v3

    .line 1528
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1531
    .end local v2    # "i":I
    :cond_1
    const-wide/32 v2, 0x100000

    .line 1532
    .local v2, "MB":J
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TotalDayTraffic: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "SLM-SRV-SLAService"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1533
    div-long v4, v0, v2

    return-wide v4

    .line 1526
    .end local v2    # "MB":J
    :cond_2
    :goto_1
    const-wide/16 v2, 0x0

    return-wide v2
.end method

.method private static getMacAddress([B)Landroid/net/MacAddress;
    .locals 3
    .param p0, "linkLayerAddress"    # [B

    .line 489
    if-eqz p0, :cond_0

    .line 491
    :try_start_0
    invoke-static {p0}, Landroid/net/MacAddress;->fromBytes([B)Landroid/net/MacAddress;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 492
    :catch_0
    move-exception v0

    .line 493
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to parse link-layer address: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SLM-SRV-SLAService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private handleDisableSLM()V
    .locals 2

    .line 550
    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I

    .line 551
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLMSwitchON()Z

    move-result v0

    if-nez v0, :cond_0

    .line 552
    return-void

    .line 554
    :cond_0
    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I

    if-ne v0, v1, :cond_1

    .line 555
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->stopSLAD()V

    .line 556
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->checkWlanWwanCoexistense()V

    .line 557
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMobileDataAlwaysOff()V

    .line 559
    :cond_1
    return-void
.end method

.method private handleEnableSLM()V
    .locals 1

    .line 528
    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I

    if-lez v0, :cond_0

    .line 529
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I

    .line 531
    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLMSwitchON()Z

    move-result v0

    if-nez v0, :cond_1

    .line 532
    return-void

    .line 534
    :cond_1
    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I

    if-nez v0, :cond_2

    .line 535
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMobileDataAlwaysOn()V

    .line 536
    const-string v0, "ON_DISABLE_EVENT"

    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->processNetworkCallback(Ljava/lang/String;)V

    .line 538
    :cond_2
    return-void
.end method

.method private handleScreenOff()V
    .locals 4

    .line 1142
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1143
    const/16 v1, 0x6b

    const-wide/32 v2, 0x2bf20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1146
    :cond_0
    const-string v0, "SLM-SRV-SLAService"

    const-string v1, "handleScreenOff handler is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1148
    :goto_0
    return-void
.end method

.method private handleScreenOn()V
    .locals 2

    .line 1132
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1133
    const/16 v1, 0x6b

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1134
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1137
    :cond_0
    const-string v0, "SLM-SRV-SLAService"

    const-string v1, "handleScreenOn handler is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1139
    :goto_0
    return-void
.end method

.method private initCloudObserver()V
    .locals 4

    .line 620
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mCloudObserver:Landroid/database/ContentObserver;

    const-string v1, "SLM-SRV-SLAService"

    if-eqz v0, :cond_0

    .line 621
    const-string v0, "initCloudObserver rejected! observer has already registered! check your code."

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 623
    :cond_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 624
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$1;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mCloudObserver:Landroid/database/ContentObserver;

    .line 640
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 641
    const-string v1, "cloud_miwill_controller_list"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mCloudObserver:Landroid/database/ContentObserver;

    .line 640
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 642
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 643
    const-string v1, "cloud_double_wifi_monitor_controll"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mCloudObserver:Landroid/database/ContentObserver;

    .line 642
    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_0

    .line 646
    :cond_1
    const-string v0, "initCloudObserver failed! mContext should be initialized in onCreate"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    :goto_0
    return-void
.end method

.method private isDDReady()Z
    .locals 2

    .line 680
    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDataReady:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveDataReady:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiReady:Z

    sget-boolean v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiReady:Z

    if-ne v0, v1, :cond_0

    sget v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mIfaceNumber:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I

    if-nez v0, :cond_0

    .line 682
    return v1

    .line 684
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isDWReady()Z
    .locals 2

    .line 673
    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiReady:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiReady:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mIfaceNumber:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I

    if-nez v0, :cond_0

    .line 674
    return v1

    .line 676
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isEnableDWMonitor()V
    .locals 3

    .line 1214
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    .line 1215
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1214
    const-string v1, "cloud_double_wifi_monitor_controll"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1216
    .local v0, "cvalue":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isEnableDWMonitor ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SLM-SRV-SLAService"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1217
    if-eqz v0, :cond_0

    const-string/jumbo v1, "v1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->misEnableDWMonitor:Z

    .line 1218
    return-void
.end method

.method private isPerformanceMode()Z
    .locals 4

    .line 1282
    const/4 v0, 0x0

    .line 1284
    .local v0, "line":Ljava/lang/String;
    :try_start_0
    const-string v1, "/sys/class/thermal/thermal_message/balance_mode"

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->readLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    if-eqz v1, :cond_0

    .line 1285
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1286
    .local v1, "str_temp":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1287
    .local v2, "mode":I
    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1288
    return v3

    .line 1294
    .end local v1    # "str_temp":Ljava/lang/String;
    .end local v2    # "mode":I
    :cond_0
    goto :goto_0

    .line 1292
    :catch_0
    move-exception v1

    .line 1295
    :goto_0
    const/4 v1, 0x0

    return v1
.end method

.method private isSLAReady()Z
    .locals 2

    .line 661
    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mLinkTurboSwitch:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDataReady:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveDataReady:Z

    if-eqz v0, :cond_1

    :cond_0
    sget v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mIfaceNumber:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I

    if-nez v0, :cond_1

    .line 663
    return v1

    .line 665
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private isSLMSwitchON()Z
    .locals 1

    .line 688
    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiReady:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiReady:Z

    if-nez v0, :cond_1

    :cond_0
    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mLinkTurboSwitch:Z

    if-eqz v0, :cond_2

    .line 689
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 691
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private isSLSReady()Z
    .locals 1

    .line 669
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isSlaRemind()Z
    .locals 3

    .line 1494
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "sla_notification_state"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v2, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method private notifyWlanWwanCoexistenseStatus(I)V
    .locals 3
    .param p1, "status"    # I

    .line 1165
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notifyWlanWwanCoexistenseStatus "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLAService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1166
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1167
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.phone.intent.action.SLM_STATUS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1168
    const-string v1, "com.android.phone"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1169
    const-string v1, "coexistense"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1171
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1172
    return-void
.end method

.method private postNotification()V
    .locals 0

    .line 1492
    return-void
.end method

.method private postTrafficNotification()V
    .locals 0

    .line 1520
    return-void
.end method

.method private processNetworkCallback(Ljava/lang/String;)V
    .locals 2
    .param p1, "status"    # Ljava/lang/String;

    .line 841
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "processNetworkCallback "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLAService"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->checkWlanWwanCoexistense()V

    .line 843
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->updateSLAD()V

    .line 844
    return-void
.end method

.method private readLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .line 1255
    const/4 v0, 0x0

    .line 1256
    .local v0, "reader":Ljava/io/BufferedReader;
    const/4 v1, 0x0

    .line 1259
    .local v1, "line":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v4, 0x400

    invoke-direct {v2, v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    move-object v0, v2

    .line 1260
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, v2

    .line 1269
    nop

    .line 1271
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1275
    :goto_0
    goto :goto_2

    .line 1273
    :catch_0
    move-exception v2

    goto :goto_0

    .line 1269
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_0

    .line 1271
    :try_start_2
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1275
    goto :goto_1

    .line 1273
    :catch_1
    move-exception v3

    .line 1277
    :cond_0
    :goto_1
    throw v2

    .line 1265
    :catch_2
    move-exception v2

    .line 1269
    if-eqz v0, :cond_1

    .line 1271
    :try_start_3
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    goto :goto_0

    .line 1262
    :catch_3
    move-exception v2

    .line 1269
    if-eqz v0, :cond_1

    .line 1271
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 1278
    :cond_1
    :goto_2
    return-object v1
.end method

.method private registerDefaultNetworkCallback()V
    .locals 3

    .line 1020
    :try_start_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 1021
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDefaultNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerDefaultNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1024
    goto :goto_0

    .line 1022
    :catch_0
    move-exception v0

    .line 1023
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SLM-SRV-SLAService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1025
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private registerModemSignalStrengthListener()V
    .locals 3

    .line 1337
    :try_start_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 1338
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mModemSignalStrengthListener:Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerModemSignalStrengthListener(Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1341
    goto :goto_0

    .line 1339
    :catch_0
    move-exception v0

    .line 1340
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SLM-SRV-SLAService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1342
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private registerNetworkBoostWatchdog()V
    .locals 3

    .line 1391
    :try_start_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 1392
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getNetworkBoostWatchdogHandler()Landroid/os/HandlerThread;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWacthdogHandlerThread:Landroid/os/HandlerThread;

    .line 1393
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$WatchdogHandler;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWacthdogHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$WatchdogHandler;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWacthdogHandler:Landroid/os/Handler;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1396
    goto :goto_0

    .line 1394
    :catch_0
    move-exception v0

    .line 1395
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SLM-SRV-SLAService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1397
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private registerNetworkCallback()V
    .locals 3

    .line 932
    :try_start_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 933
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 936
    goto :goto_0

    .line 934
    :catch_0
    move-exception v0

    .line 935
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SLM-SRV-SLAService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 937
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private registerNetworkStateBroadcastReceiver()V
    .locals 3

    .line 975
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 976
    .local v0, "wifiStateFilter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 977
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 978
    return-void
.end method

.method private registerScreenStatusListener()V
    .locals 3

    .line 1101
    :try_start_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 1102
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mScreenStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1105
    goto :goto_0

    .line 1103
    :catch_0
    move-exception v0

    .line 1104
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SLM-SRV-SLAService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1106
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private registerVPNChangedCallback()V
    .locals 3

    .line 1066
    :try_start_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 1067
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mVPNListener:Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerVPNListener(Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1070
    goto :goto_0

    .line 1068
    :catch_0
    move-exception v0

    .line 1069
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SLM-SRV-SLAService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1072
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private releaseSlaServiceLock(Ljava/lang/String;)V
    .locals 2
    .param p1, "func"    # Ljava/lang/String;

    .line 834
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->drainPermits()I

    .line 835
    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 836
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Unlock"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLAService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    return-void
.end method

.method private setDWMonitorEnable()V
    .locals 4

    .line 1221
    const-string/jumbo v0, "setDWMonitorEnable enter"

    const-string v1, "SLM-SRV-SLAService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1223
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->misEnableDWMonitor:Z

    if-nez v0, :cond_0

    .line 1224
    return-void

    .line 1227
    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z

    move-result v0

    const-string/jumbo v2, "setDWMonitorEnable catch ex:"

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1229
    :try_start_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    const-string v3, "1"

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lvendor/qti/sla/service/V1_0/ISlaService;->setDWMonitorEnable(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1230
    :catch_0
    move-exception v0

    .line 1231
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1232
    .end local v0    # "ex":Ljava/lang/Exception;
    :goto_0
    goto :goto_1

    .line 1235
    :cond_1
    :try_start_1
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    const-string v3, "0"

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lvendor/qti/sla/service/V1_0/ISlaService;->setDWMonitorEnable(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1238
    goto :goto_1

    .line 1236
    :catch_1
    move-exception v0

    .line 1237
    .restart local v0    # "ex":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1240
    .end local v0    # "ex":Ljava/lang/Exception;
    :goto_1
    const-string/jumbo v0, "setDWMonitorEnable exit"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1241
    return-void
.end method

.method private setInterface()V
    .locals 4

    .line 999
    const-string/jumbo v0, "setInterface"

    const-string v1, "SLM-SRV-SLAService"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1000
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    if-eqz v0, :cond_0

    .line 1003
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setInterface:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mInterface:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1004
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mInterface:Ljava/lang/String;

    invoke-interface {v0, v2}, Lvendor/qti/sla/service/V1_0/ISlaService;->setInterface(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1005
    :catch_0
    move-exception v0

    .line 1006
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1007
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    goto :goto_1

    .line 1010
    :cond_0
    const-string v0, "mSlaService is null"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1012
    :goto_1
    return-void
.end method

.method private setMobileDataAlwaysOff()V
    .locals 4

    .line 510
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mobile_data_always_on"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 512
    .local v0, "mobileDataAlwaysOnMode":I
    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 513
    sget-object v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 516
    :cond_0
    return-void
.end method

.method private setMobileDataAlwaysOn()V
    .locals 4

    .line 501
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "mobile_data_always_on"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 503
    .local v0, "mobileDataAlwaysOnMode":I
    if-nez v0, :cond_0

    iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSingnalPoor:Z

    if-nez v1, :cond_0

    .line 504
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 507
    :cond_0
    return-void
.end method

.method private setSLAWifiGateway()V
    .locals 8

    .line 1176
    const/4 v0, 0x0

    .line 1177
    .local v0, "dhcpInfo":Landroid/net/DhcpInfo;
    const/4 v1, 0x0

    .line 1178
    .local v1, "slaveDhcpInfo":Landroid/net/DhcpInfo;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 1180
    .local v2, "gateway":Ljava/lang/StringBuffer;
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v3, :cond_0

    .line 1181
    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getDhcpInfo()Landroid/net/DhcpInfo;

    move-result-object v0

    .line 1184
    :cond_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    if-eqz v3, :cond_1

    .line 1185
    invoke-virtual {v3}, Landroid/net/wifi/SlaveWifiManager;->getSlaveDhcpInfo()Landroid/net/DhcpInfo;

    move-result-object v1

    .line 1188
    :cond_1
    const-string v3, ","

    const-string v4, "SLM-SRV-SLAService"

    if-eqz v0, :cond_2

    .line 1189
    iget v5, v0, Landroid/net/DhcpInfo;->gateway:I

    invoke-static {v5}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    .line 1190
    .local v5, "address":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Gateway address:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1192
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1193
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1196
    .end local v5    # "address":Ljava/lang/String;
    :cond_2
    if-eqz v1, :cond_3

    .line 1197
    iget v5, v1, Landroid/net/DhcpInfo;->gateway:I

    invoke-static {v5}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    .line 1198
    .restart local v5    # "address":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Slave Gateway address:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1199
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1200
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1203
    .end local v5    # "address":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-lez v3, :cond_4

    .line 1205
    :try_start_0
    sget-object v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Lvendor/qti/sla/service/V1_0/ISlaService;->setWifiGateway(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1208
    goto :goto_0

    .line 1206
    :catch_0
    move-exception v3

    .line 1207
    .local v3, "ex":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "setSLAWifiGateway catch ex:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1210
    .end local v3    # "ex":Ljava/lang/Exception;
    :cond_4
    :goto_0
    return-void
.end method

.method private stopSLAD()V
    .locals 5

    .line 784
    const-string v0, "SLM-SRV-SLAService"

    const-string/jumbo v1, "stopSLAD"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaLock:Ljava/lang/Object;

    monitor-enter v0

    .line 788
    :try_start_0
    sget-boolean v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sladEnabled:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 789
    const-string v1, "SLM-SRV-SLAService"

    const-string/jumbo v2, "stopSlad"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-interface {v1}, Lvendor/qti/sla/service/V1_0/ISlaService;->stopSlad()V

    .line 791
    const/4 v1, 0x0

    sput-boolean v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sladEnabled:Z

    .line 792
    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->stopSladHandle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796
    :cond_0
    goto :goto_0

    .line 797
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 794
    :catch_0
    move-exception v1

    .line 795
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v2, "SLM-SRV-SLAService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 799
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLSAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLSReady()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSEnableStatus(Z)V

    .line 800
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->sendMsgSlsStop()V

    .line 801
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->sendMsgDoubleWifiAndSLAStop()V

    .line 802
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->sendMsgSlaStop()V

    .line 803
    return-void

    .line 797
    :goto_1
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private unregisterDefaultNetworkCallback()V
    .locals 3

    .line 1029
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDefaultNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;

    if-eqz v0, :cond_0

    .line 1030
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 1031
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDefaultNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterDefaultNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1035
    :cond_0
    goto :goto_0

    .line 1033
    :catch_0
    move-exception v0

    .line 1034
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SLM-SRV-SLAService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1036
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private unregisterModemSignalStrengthListener()V
    .locals 3

    .line 1346
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mModemSignalStrengthListener:Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;

    if-eqz v0, :cond_0

    .line 1347
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 1348
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mModemSignalStrengthListener:Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterModemSignalStrengthListener(Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1352
    :cond_0
    goto :goto_0

    .line 1350
    :catch_0
    move-exception v0

    .line 1351
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SLM-SRV-SLAService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1353
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private unregisterNetworkCallback()V
    .locals 3

    .line 941
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    if-eqz v0, :cond_0

    .line 942
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 943
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 947
    :cond_0
    goto :goto_0

    .line 945
    :catch_0
    move-exception v0

    .line 946
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SLM-SRV-SLAService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 948
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private unregisterNetworkStateBroadcastReceiver()V
    .locals 2

    .line 981
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 982
    return-void
.end method

.method private unregisterScreenStatusListener()V
    .locals 3

    .line 1110
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mScreenStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    if-eqz v0, :cond_0

    .line 1111
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 1112
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mScreenStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1116
    :cond_0
    goto :goto_0

    .line 1114
    :catch_0
    move-exception v0

    .line 1115
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SLM-SRV-SLAService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1117
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private unregisterVPNChangedCallback()V
    .locals 3

    .line 1076
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mVPNListener:Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;

    if-eqz v0, :cond_0

    .line 1077
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 1078
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mVPNListener:Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterVPNListener(Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1082
    :cond_0
    goto :goto_0

    .line 1080
    :catch_0
    move-exception v0

    .line 1081
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SLM-SRV-SLAService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1083
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private updateSLAD()V
    .locals 6

    .line 695
    const-string v0, "SLM-SRV-SLAService"

    const-string/jumbo v1, "updateSLAD"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 696
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWacthdogHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/32 v3, 0x1d4c0

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 697
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaLock:Ljava/lang/Object;

    monitor-enter v0

    .line 699
    :try_start_0
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    const/4 v3, 0x0

    if-nez v2, :cond_0

    .line 700
    const-string v2, "SLM-SRV-SLAService"

    const-string v4, "HAL service is null, get"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandler:Landroid/os/Handler;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v4, 0x67

    invoke-virtual {v2, v4, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_3

    .line 703
    :cond_0
    sget-object v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v4, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setSlaService(Lvendor/qti/sla/service/V1_0/ISlaService;)V

    .line 704
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLSAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    sget-object v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-virtual {v2, v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSlaService(Lvendor/qti/sla/service/V1_0/ISlaService;)V

    .line 705
    const-string v2, "SLM-SRV-SLAService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isSLAReady:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", isDWReady:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 706
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", isDDReady:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 707
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDDReady()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 705
    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z

    move-result v2

    const/4 v4, 0x1

    if-nez v2, :cond_3

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDDReady()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 746
    :cond_1
    sget-boolean v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sladEnabled:Z

    if-ne v2, v4, :cond_2

    .line 747
    const-string v2, "SLM-SRV-SLAService"

    const-string/jumbo v4, "stopSlad"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 750
    :try_start_1
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-interface {v2}, Lvendor/qti/sla/service/V1_0/ISlaService;->stopSlad()V

    .line 751
    sput-boolean v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sladEnabled:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 754
    goto :goto_0

    .line 752
    :catch_0
    move-exception v2

    .line 753
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v3, "SLM-SRV-SLAService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateSLAD SlaService stopSlad failed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 757
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->stopSladHandle()V

    goto/16 :goto_3

    .line 759
    :cond_2
    const-string v2, "SLM-SRV-SLAService"

    const-string/jumbo v3, "slad is already disabled"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 710
    :cond_3
    :goto_1
    sget-boolean v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sladEnabled:Z

    if-nez v2, :cond_4

    .line 711
    const-string v2, "SLM-SRV-SLAService"

    const-string/jumbo v3, "startSlad "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 714
    :try_start_3
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    invoke-interface {v2}, Lvendor/qti/sla/service/V1_0/ISlaService;->startSlad()V

    .line 715
    sput-boolean v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->sladEnabled:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 718
    goto :goto_2

    .line 716
    :catch_1
    move-exception v2

    .line 717
    .restart local v2    # "e":Ljava/lang/Exception;
    :try_start_4
    const-string v3, "SLM-SRV-SLAService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateSLAD SlaService startSlad failed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    nop

    .end local v2    # "e":Ljava/lang/Exception;
    goto :goto_2

    .line 721
    :cond_4
    const-string v2, "SLM-SRV-SLAService"

    const-string/jumbo v3, "slad is already enabled"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    :goto_2
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setInterface()V

    .line 727
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 728
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->startSladHandle()V

    .line 731
    :cond_5
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLSReady()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 732
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLSAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setWifiBSSID()Z

    .line 735
    :cond_6
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 736
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgDoubleWifiUid()Z

    .line 739
    :cond_7
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDDReady()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 740
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgDoubleDataUid()Z

    .line 741
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setDDSLAMode()V

    .line 743
    :cond_8
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setSLAWifiGateway()V

    .line 744
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDWMonitorEnable()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 765
    :goto_3
    goto :goto_4

    .line 779
    :catchall_0
    move-exception v1

    goto :goto_6

    .line 763
    :catch_2
    move-exception v2

    .line 764
    .restart local v2    # "e":Ljava/lang/Exception;
    :try_start_5
    const-string v3, "SLM-SRV-SLAService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_4
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaToast:Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->setLinkTurboStatus(Z)V

    .line 768
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLSAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLSReady()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLSEnableStatus(Z)V

    .line 769
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->setLinkTurboUidList(Ljava/lang/String;)V

    .line 770
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->setSLAAppWhiteList(Ljava/lang/String;)V

    .line 772
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isSLAReady()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 773
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 774
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->sendMsgDoubleWifiAndSLAStart()V

    goto :goto_5

    .line 776
    :cond_9
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->sendMsgSlaStart()V

    .line 779
    :cond_a
    :goto_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 780
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWacthdogHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 781
    return-void

    .line 779
    :goto_6
    :try_start_6
    monitor-exit v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v1
.end method


# virtual methods
.method public addUidToLinkTurboWhiteList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "uid"    # Ljava/lang/String;

    .line 287
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgAddSLAUid(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public disableSLM()V
    .locals 2

    .line 541
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 542
    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 545
    :cond_0
    const-string v0, "SLM-SRV-SLAService"

    const-string v1, "disableSLM handler is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    :goto_0
    return-void
.end method

.method public dumpModule(Ljava/io/PrintWriter;)V
    .locals 8
    .param p1, "writer"    # Ljava/io/PrintWriter;

    .line 1421
    const-string v0, "SLAService end.\n"

    const-string v1, "    "

    .line 1423
    .local v1, "spacePrefix":Ljava/lang/String;
    :try_start_0
    const-string v2, "SLAService begin:"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1424
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "WifiReady:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiReady:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1425
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SlaveWifiReady:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiReady:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1426
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "DataReady:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDataReady:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1427
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Interface:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mInterface:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1428
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "IfaceNumber:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mIfaceNumber:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1429
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "disableSLMCnt:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLMCnt:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1430
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mlastWlanWwanCoexistanceStatus:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mlastWlanWwanCoexistanceStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1431
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "thermal_enable_slm:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->thermal_enable_slm:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1432
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mThermalList:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mThermalList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " {"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1433
    const-string v2, ""

    .line 1434
    .local v2, "temperatureStr":Ljava/lang/String;
    const/4 v3, 0x1

    .line 1435
    .local v3, "firstItem":Z
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mThermalList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    .line 1436
    .local v5, "temperature":Ljava/lang/Long;
    if-eqz v3, :cond_0

    .line 1437
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v2, v6

    goto :goto_1

    .line 1440
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v2, v6

    .line 1442
    .end local v5    # "temperature":Ljava/lang/Long;
    :goto_1
    goto :goto_0

    .line 1443
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1444
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "}"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1445
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1456
    .end local v2    # "temperatureStr":Ljava/lang/String;
    .end local v3    # "firstItem":Z
    goto :goto_2

    .line 1447
    :catch_0
    move-exception v2

    .line 1448
    .local v2, "ex":Ljava/lang/Exception;
    const-string v3, "dump failed!"

    const-string v4, "SLM-SRV-SLAService"

    invoke-static {v4, v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1450
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Dump of SLAService failed:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1451
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1455
    goto :goto_2

    .line 1453
    :catch_1
    move-exception v0

    .line 1454
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "dump failure failed:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1457
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "ex":Ljava/lang/Exception;
    :goto_2
    return-void
.end method

.method public enableSLM()V
    .locals 2

    .line 519
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 520
    const/16 v1, 0x68

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 523
    :cond_0
    const-string v0, "SLM-SRV-SLAService"

    const-string v1, "enableSLM handler is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    :goto_0
    return-void
.end method

.method public getLinkTurboAppsTraffic()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;",
            ">;"
        }
    .end annotation

    .line 301
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboAppsTraffic()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLinkTurboDefaultPn()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 298
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboDefaultPn()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLinkTurboWhiteList()Ljava/lang/String;
    .locals 1

    .line 295
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .line 384
    const-string v0, "SLM-SRV-SLAService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SLAServiceHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 387
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 388
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$InternalHandler;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandler:Landroid/os/Handler;

    .line 389
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x67

    invoke-virtual {v0, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 391
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->registerNetworkBoostWatchdog()V

    .line 394
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;

    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaToast:Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;

    .line 395
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->get(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    .line 396
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;-><init>(Landroid/content/Context;Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLSAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    .line 397
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->getSLATrack(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLATrack:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    .line 399
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "wifi"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 400
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    const-string v2, "SlaveWifiService"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/SlaveWifiManager;

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    .line 403
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->initCloudObserver()V

    .line 405
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->checkMiwillCloudController()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mMiWillManager:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    .line 410
    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isEnableDWMonitor()V

    .line 412
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->createSLANotificationChannel()V

    .line 413
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->registerVPNChangedCallback()V

    .line 414
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->registerScreenStatusListener()V

    .line 415
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->registerModemSignalStrengthListener()V

    .line 417
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "linkturbo_is_enable"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    move v1, v2

    :cond_1
    sput-boolean v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mLinkTurboSwitch:Z

    .line 419
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->registerNetworkCallback()V

    .line 420
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->registerDefaultNetworkCallback()V

    .line 421
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->registerNetworkStateBroadcastReceiver()V

    .line 430
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x6e

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 431
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 446
    const-string v0, "SLM-SRV-SLAService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaService:Lvendor/qti/sla/service/V1_0/ISlaService;

    if-eqz v0, :cond_0

    .line 449
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    invoke-interface {v0, v1}, Lvendor/qti/sla/service/V1_0/ISlaService;->unlinkToDeath(Landroid/os/IHwBinder$DeathRecipient;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 452
    goto :goto_0

    .line 450
    :catch_0
    move-exception v0

    .line 455
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 456
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSlaToast:Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->getSLAToastHandlerThread()Landroid/os/HandlerThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 457
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->unregisterVPNChangedCallback()V

    .line 458
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->unregisterScreenStatusListener()V

    .line 459
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->unregisterModemSignalStrengthListener()V

    .line 460
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->unregisterNetworkCallback()V

    .line 461
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->unregisterDefaultNetworkCallback()V

    .line 462
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->unregisterNetworkStateBroadcastReceiver()V

    .line 463
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->deinitCloudObserver()V

    .line 464
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mMiWillManager:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    if-eqz v0, :cond_1

    .line 465
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->destroyInstance()V

    .line 466
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mMiWillManager:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    .line 476
    :cond_1
    return-void
.end method

.method public removeUidInLinkTurboWhiteList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "uid"    # Ljava/lang/String;

    .line 291
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgDelSLAUid(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setDWUidToSlad()V
    .locals 2

    .line 825
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    if-eqz v0, :cond_0

    .line 826
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgDoubleWifiUid()Z

    goto :goto_0

    .line 828
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setDWUidToSlad isDWReady = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->isDWReady()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NetworkSDKService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    :goto_0
    return-void
.end method

.method public setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "type"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "uid"    # Ljava/lang/String;
    .param p4, "isOperateBlacklist"    # Z

    .line 818
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mSLAAppLib:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    if-nez v0, :cond_0

    .line 819
    const/4 v0, 0x0

    return v0

    .line 821
    :cond_0
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public setLinkTurboEnable(Z)Z
    .locals 5
    .param p1, "enable"    # Z

    .line 305
    sget-boolean v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mLinkTurboSwitch:Z

    const/4 v1, 0x1

    if-ne v0, p1, :cond_0

    .line 306
    return v1

    .line 309
    :cond_0
    if-eqz p1, :cond_1

    .line 310
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMobileDataAlwaysOn()V

    goto :goto_0

    .line 312
    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setMobileDataAlwaysOff()V

    .line 315
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SLM Enable:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "SLM-SRV-SLAService"

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    const/16 v0, 0x6c

    if-eqz p1, :cond_2

    .line 317
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "sla_notification_state"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 318
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 319
    .local v0, "message":Landroid/os/Message;
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandler:Landroid/os/Handler;

    const-wide/16 v3, 0x2710

    invoke-virtual {v2, v0, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 320
    .end local v0    # "message":Landroid/os/Message;
    goto :goto_1

    .line 321
    :cond_2
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 323
    :goto_1
    sput-boolean p1, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mLinkTurboSwitch:Z

    .line 324
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "linkturbo_is_enable"

    invoke-static {v0, v2, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 326
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 327
    .local v0, "msg":Landroid/os/Message;
    const/16 v2, 0x64

    iput v2, v0, Landroid/os/Message;->what:I

    .line 328
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 329
    return v1
.end method

.method public setMiWillGameStart(Ljava/lang/String;)Z
    .locals 2
    .param p1, "uid"    # Ljava/lang/String;

    .line 562
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mMiWillManager:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    if-eqz v0, :cond_0

    .line 563
    invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->setMiWillGameStart(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 566
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setMiWillGameStart manager is null: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLAService"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    const/4 v0, 0x0

    return v0
.end method

.method public setMiWillGameStop(Ljava/lang/String;)Z
    .locals 2
    .param p1, "uid"    # Ljava/lang/String;

    .line 572
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->mMiWillManager:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    if-eqz v0, :cond_0

    .line 573
    invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->setMiWillGameStop(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 576
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setMiWillGameStop manager is null: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLAService"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    const/4 v0, 0x0

    return v0
.end method
