class com.xiaomi.NetworkBoost.slaservice.MiWillManager$MiWillHandler extends android.os.Handler {
	 /* .source "MiWillManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MiWillHandler" */
} // .end annotation
/* # static fields */
public static final Integer GET_MIWILL_HAL;
public static final Integer RECHECK_MIWILL_STATUS;
public static final Integer SET_MIWILL_STATUS;
public static final Integer UPDATE_STATUS;
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.MiWillManager this$0; //synthetic
/* # direct methods */
public com.xiaomi.NetworkBoost.slaservice.MiWillManager$MiWillHandler ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 575 */
this.this$0 = p1;
/* .line 576 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 577 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 580 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handle: "; // const-string v1, "handle: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Landroid/os/Message;->what:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MIWILL-MiWillManager"; // const-string v1, "MIWILL-MiWillManager"
android.util.Log .i ( v1,v0 );
/* .line 582 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 602 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "MiWillHandler handle unexpected code:"; // const-string v2, "MiWillHandler handle unexpected code:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p1, Landroid/os/Message;->what:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v0 );
/* .line 596 */
/* :pswitch_0 */
v0 = this.obj;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 597 */
/* .local v0, "retry_cnt":I */
/* add-int/lit8 v0, v0, -0x1 */
/* .line 598 */
v1 = this.this$0;
int v2 = 1; // const/4 v2, 0x1
com.xiaomi.NetworkBoost.slaservice.MiWillManager .-$$Nest$msetMiwillStatus ( v1,v2,v0 );
/* .line 599 */
/* .line 592 */
} // .end local v0 # "retry_cnt":I
/* :pswitch_1 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
int v2 = 2; // const/4 v2, 0x2
com.xiaomi.NetworkBoost.slaservice.MiWillManager .-$$Nest$msetMiwillStatus ( v0,v1,v2 );
/* .line 593 */
/* .line 588 */
/* :pswitch_2 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.MiWillManager .-$$Nest$minitMiWillHal ( v0 );
/* .line 589 */
/* .line 584 */
/* :pswitch_3 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.MiWillManager .-$$Nest$mupdateStatus ( v0 );
/* .line 585 */
/* nop */
/* .line 605 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x64 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
