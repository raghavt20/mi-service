class com.xiaomi.NetworkBoost.slaservice.SLAAppLib$6 extends android.content.BroadcastReceiver {
	 /* .source "SLAAppLib.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initBroadcastReceiver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.SLAAppLib this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.slaservice.SLAAppLib$6 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib; */
/* .line 1250 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 1253 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 1254 */
/* .local v0, "action":Ljava/lang/String; */
(( android.content.Intent ) p2 ).getData ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;
(( android.net.Uri ) v1 ).getSchemeSpecificPart ( ); // invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;
/* .line 1255 */
/* .local v1, "packageName":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 1256 */
	 return;
	 /* .line 1259 */
} // :cond_0
final String v2 = "android.intent.extra.UID"; // const-string v2, "android.intent.extra.UID"
int v3 = -1; // const/4 v3, -0x1
v2 = (( android.content.Intent ) p2 ).getIntExtra ( v2, v3 ); // invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 1260 */
/* .local v2, "uid":I */
/* if-ne v2, v3, :cond_1 */
/* .line 1261 */
return;
/* .line 1264 */
} // :cond_1
final String v3 = "SLM-SRV-SLAAppLib"; // const-string v3, "SLM-SRV-SLAAppLib"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "initBroadcastReceiver"; // const-string v5, "initBroadcastReceiver"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v4 );
/* .line 1265 */
final String v3 = "android.intent.action.PACKAGE_ADDED"; // const-string v3, "android.intent.action.PACKAGE_ADDED"
v3 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v4 = 0; // const/4 v4, 0x0
if ( v3 != null) { // if-eqz v3, :cond_a
/* .line 1266 */
final String v3 = "SLM-SRV-SLAAppLib"; // const-string v3, "SLM-SRV-SLAAppLib"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "ACTION_PACKAGE_ADDED uid = "; // const-string v6, "ACTION_PACKAGE_ADDED uid = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v5 );
/* .line 1267 */
v3 = this.this$0;
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameAppPN ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_3
	 /* .line 1268 */
	 v3 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$maddSLAAppDefault ( v3,v2 );
	 /* .line 1269 */
	 v3 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameUidList ( v3 );
	 /* if-nez v3, :cond_2 */
	 /* .line 1270 */
	 v3 = this.this$0;
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 java.lang.Integer .toString ( v2 );
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v6 = ","; // const-string v6, ","
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSGameUidList ( v3,v5 );
	 /* .line 1272 */
} // :cond_2
v3 = this.this$0;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
v6 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameUidList ( v6 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toString ( v2 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ","; // const-string v6, ","
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSGameUidList ( v3,v5 );
/* .line 1274 */
} // :cond_3
v3 = this.this$0;
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLAAppDefaultPN ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1275 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$maddSLAAppDefault ( v3,v2 );
/* .line 1277 */
} // :cond_4
} // :goto_0
v3 = this.this$0;
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleWifiAppPN ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 1278 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleWifiUidList ( v3 );
/* if-nez v3, :cond_5 */
/* .line 1279 */
v3 = this.this$0;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.Integer .toString ( v2 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ","; // const-string v6, ","
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmDoubleWifiUidList ( v3,v5 );
/* .line 1281 */
} // :cond_5
v3 = this.this$0;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
v6 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleWifiUidList ( v6 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toString ( v2 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ","; // const-string v6, ","
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmDoubleWifiUidList ( v3,v5 );
/* .line 1284 */
} // :cond_6
} // :goto_1
v3 = this.this$0;
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleDataAppPN ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 1285 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleDataUidList ( v3 );
/* if-nez v3, :cond_7 */
/* .line 1286 */
v3 = this.this$0;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.Integer .toString ( v2 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ","; // const-string v6, ","
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmDoubleDataUidList ( v3,v5 );
/* .line 1288 */
} // :cond_7
v3 = this.this$0;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
v6 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleDataUidList ( v6 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toString ( v2 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ","; // const-string v6, ","
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmDoubleDataUidList ( v3,v5 );
/* .line 1292 */
} // :cond_8
} // :goto_2
final String v3 = "com.tencent.mm"; // const-string v3, "com.tencent.mm"
v3 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_12
/* .line 1293 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameAppPN ( v3 );
v3 = final String v5 = "com.tencent.mm"; // const-string v5, "com.tencent.mm"
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 1294 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSVoIPUid ( v3,v2 );
/* goto/16 :goto_3 */
/* .line 1296 */
} // :cond_9
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSVoIPUid ( v3,v4 );
/* goto/16 :goto_3 */
/* .line 1300 */
} // :cond_a
final String v3 = "android.intent.action.PACKAGE_REMOVED"; // const-string v3, "android.intent.action.PACKAGE_REMOVED"
v3 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_f
/* .line 1301 */
final String v3 = "SLM-SRV-SLAAppLib"; // const-string v3, "SLM-SRV-SLAAppLib"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "ACTION_PACKAGE_REMOVED uid = "; // const-string v6, "ACTION_PACKAGE_REMOVED uid = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v5 );
/* .line 1302 */
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$sfgetmSLAAppLibLock ( );
/* monitor-enter v3 */
/* .line 1303 */
try { // :try_start_0
v5 = this.this$0;
java.lang.Integer .toString ( v2 );
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$mgetSLAAppByUid ( v5,v6 );
if ( v5 != null) { // if-eqz v5, :cond_b
/* .line 1304 */
final String v5 = "SLM-SRV-SLAAppLib"; // const-string v5, "SLM-SRV-SLAAppLib"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "package upgrade store, uid = "; // const-string v7, "package upgrade store, uid = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v6 );
/* .line 1305 */
v5 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$mstoreSLAAppUpgrade ( v5,v1 );
/* .line 1307 */
} // :cond_b
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1308 */
v3 = this.this$0;
java.lang.Integer .toString ( v2 );
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$mclearSLAApp ( v3,v5 );
/* .line 1309 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$mdeleteSLAAppDefault ( v3,v2 );
/* .line 1310 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameUidList ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_c
/* .line 1311 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameUidList ( v3 );
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.Integer .toString ( v2 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ","; // const-string v7, ","
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v7 = ""; // const-string v7, ""
(( java.lang.String ) v5 ).replaceAll ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSGameUidList ( v3,v5 );
/* .line 1313 */
} // :cond_c
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleWifiUidList ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_d
/* .line 1314 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleWifiUidList ( v3 );
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.Integer .toString ( v2 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ","; // const-string v7, ","
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v7 = ""; // const-string v7, ""
(( java.lang.String ) v5 ).replaceAll ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmDoubleWifiUidList ( v3,v5 );
/* .line 1316 */
} // :cond_d
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleDataUidList ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_e
/* .line 1317 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleDataUidList ( v3 );
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.Integer .toString ( v2 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ","; // const-string v7, ","
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v7 = ""; // const-string v7, ""
(( java.lang.String ) v5 ).replaceAll ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmDoubleDataUidList ( v3,v5 );
/* .line 1320 */
} // :cond_e
final String v3 = "com.tencent.mm"; // const-string v3, "com.tencent.mm"
v3 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_12
/* .line 1321 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSVoIPUid ( v3,v4 );
/* .line 1307 */
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_1
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v4 */
/* .line 1323 */
} // :cond_f
final String v3 = "android.intent.action.PACKAGE_REPLACED"; // const-string v3, "android.intent.action.PACKAGE_REPLACED"
v3 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_10
/* .line 1324 */
final String v3 = "SLM-SRV-SLAAppLib"; // const-string v3, "SLM-SRV-SLAAppLib"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "ACTION_PACKAGE_REPLACED uid = "; // const-string v5, "ACTION_PACKAGE_REPLACED uid = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v4 );
/* .line 1325 */
v3 = this.this$0;
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$mrestoreSLAAppUpgrade ( v3,v1 );
if ( v3 != null) { // if-eqz v3, :cond_12
/* .line 1326 */
final String v3 = "SLM-SRV-SLAAppLib"; // const-string v3, "SLM-SRV-SLAAppLib"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "package upgrade restore, uid = "; // const-string v5, "package upgrade restore, uid = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v4 );
/* .line 1327 */
v3 = this.this$0;
java.lang.Integer .toString ( v2 );
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) v3 ).sendMsgAddSLAUid ( v4 ); // invoke-virtual {v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgAddSLAUid(Ljava/lang/String;)Z
/* .line 1329 */
} // :cond_10
final String v3 = "com.android.phone.intent.action.DUAL_DATA_CONCURRENT_MODE_WHITE_LIST_DONE"; // const-string v3, "com.android.phone.intent.action.DUAL_DATA_CONCURRENT_MODE_WHITE_LIST_DONE"
v3 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_11 */
final String v3 = "com.android.phone.intent.action.DUAL_DATA_CONCURRENT_LIMITED_WHITE_LIST_DONE"; // const-string v3, "com.android.phone.intent.action.DUAL_DATA_CONCURRENT_LIMITED_WHITE_LIST_DONE"
/* .line 1330 */
v3 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_12
/* .line 1331 */
} // :cond_11
final String v3 = "SLM-SRV-SLAAppLib"; // const-string v3, "SLM-SRV-SLAAppLib"
final String v4 = "DUAL_DATA_COCURRENT_WHITE_LIST CHANGE"; // const-string v4, "DUAL_DATA_COCURRENT_WHITE_LIST CHANGE"
android.util.Log .i ( v3,v4 );
/* .line 1332 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$mfetchDoubleDataWhiteListApp ( v3 );
/* .line 1335 */
} // :cond_12
} // :goto_3
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$msendMsgSetSLAAppList ( v3 );
/* .line 1336 */
v3 = this.this$0;
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSVoIPUid ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_13
/* .line 1337 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameUidList ( v3 );
v5 = this.this$0;
v5 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSVoIPUid ( v5 );
java.lang.Integer .toString ( v5 );
final String v6 = ""; // const-string v6, ""
(( java.lang.String ) v4 ).replace ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSGameUidList ( v3,v4 );
/* .line 1338 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameUidList ( v3 );
final String v5 = ",,"; // const-string v5, ",,"
final String v6 = ","; // const-string v6, ","
(( java.lang.String ) v4 ).replace ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fputmSLSGameUidList ( v3,v4 );
/* .line 1340 */
} // :cond_13
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameUidList ( v3 );
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .setSLSGameUidList ( v3 );
/* .line 1341 */
v3 = this.this$0;
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSVoIPUid ( v3 );
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .setSLSVoIPUid ( v3 );
/* .line 1344 */
v3 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSGameUidList ( v3 );
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .setNetworkAccelerateSwitchUidList ( v3 );
/* .line 1345 */
v3 = this.this$0;
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmSLSVoIPUid ( v3 );
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .setNetworkAccelerateSwitchVoIPUid ( v3 );
/* .line 1346 */
final String v3 = "SLM-SRV-SLAAppLib"; // const-string v3, "SLM-SRV-SLAAppLib"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "mDoubleWifiUidList = "; // const-string v5, "mDoubleWifiUidList = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleWifiUidList ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v4 );
/* .line 1347 */
final String v3 = "SLM-SRV-SLAAppLib"; // const-string v3, "SLM-SRV-SLAAppLib"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "mDoubleDataUidList = "; // const-string v5, "mDoubleDataUidList = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .-$$Nest$fgetmDoubleDataUidList ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v4 );
/* .line 1348 */
return;
} // .end method
