public class com.xiaomi.NetworkBoost.slaservice.SLAAppLib {
	 /* .source "SLAAppLib.java" */
	 /* # static fields */
	 private static final java.lang.String ACTION_DUAL_DATA_CONCURRENT_LIMITED_WHITE_LIST_DONE;
	 private static final java.lang.String ACTION_DUAL_DATA_CONCURRENT_MODE_WHITE_LIST_DONE;
	 public static final Integer ADD_TO_WHITELIST;
	 private static final java.lang.String CLOUD_DOUBLEDATA_LIMITED_WHITELIST;
	 private static final java.lang.String CLOUD_DOUBLEDATA_MODE_WHITELIST;
	 private static final java.lang.String CLOUD_DOUBLEWIFI_WHITELIST;
	 private static final java.lang.String CLOUD_SLA_WHITELIST;
	 private static final java.lang.String CLOUD_SLS_GAMING_WHITELIST;
	 private static final Boolean DEBUG;
	 private static final Integer IS_DAY_CHANGE;
	 public static final Integer IS_IN_WHITELIST;
	 private static final Integer IS_MONTH_CHANGE;
	 private static final Integer IS_TODAY;
	 private static final java.lang.String LINKTURBO_UID_TO_SLA;
	 public static final java.lang.String LINK_TURBO_MODE;
	 private static final java.lang.String LINK_TURBO_OPTION;
	 private static final Integer MSG_DB_SLAAPP_ADD;
	 private static final Integer MSG_DB_SLAAPP_DEL;
	 private static final Integer MSG_DB_SLAAPP_UPDATE;
	 private static final Integer MSG_DELAY_TIME;
	 private static final Integer MSG_SET_DDAPP_LIST;
	 private static final Integer MSG_SET_DWAPP_LIST;
	 private static final Integer MSG_SET_SLAAPP_LIST;
	 public static final Integer REMOVE_FROM_WHITELIST;
	 public static final Integer RESET_WHITELIST;
	 public static final Integer RESTORE_RESET_WHITELIST;
	 private static final java.lang.String SLA_MODE_CONCURRENT;
	 private static final java.lang.String SLA_MODE_SLAVE;
	 private static final Integer START_CALC_TRAFFIC_STAT;
	 private static final Integer STOP_CALC_TRAFFIC_STAT;
	 static final java.lang.String TAG;
	 public static final Integer TYPE_MOBILE;
	 private static final Integer UPDATE_CALC_TRAFFIC_STAT;
	 private static final Integer UPDATE_DB_TRAFFIC_STAT;
	 private static Boolean defaultSlaEnable;
	 private static java.util.ArrayList mAppLists;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static Boolean mIsSladRunning;
private static Boolean mIsUidChangeReboot;
private static Integer mLinkTurboMode;
private static android.app.usage.NetworkStatsManager mNetworkStatsManager;
private static final java.lang.Object mSLAAppLibLock;
private static java.util.HashMap mSLAAppUpgradeList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String mSLSVoIPAppPN;
private static vendor.qti.sla.service.V1_0.ISlaService mSlaService;
private static java.util.HashMap mTraffic;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static com.xiaomi.NetworkBoost.slaservice.SLAAppLib sSLAAppLib;
/* # instance fields */
private Integer mCalcTrafficInterval;
private android.content.Context mContext;
private Integer mCount;
private android.os.Handler mDataBaseHandler;
private android.os.HandlerThread mDataBaseThread;
private android.database.sqlite.SQLiteDatabase mDatabase;
private Integer mDay;
private java.util.Set mDoubleDataAppPN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String mDoubleDataUidList;
private java.util.Set mDoubleWifiAppPN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String mDoubleWifiUidList;
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
private Integer mMonth;
private java.util.Set mSDKDoubleWifiAppPN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Set mSDKDoubleWifiBlackUidSets;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String mSDKDoubleWifiUidList;
private java.util.Set mSLAAppDefaultPN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Set mSLSGameAppPN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String mSLSGameUidList;
private Integer mSLSVoIPUid;
private final java.util.concurrent.Semaphore mSemaphore;
private database.SlaDbSchema.SlaBaseHelper slaBaseHelper;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static java.util.Set -$$Nest$fgetmDoubleDataAppPN ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDoubleDataAppPN;
} // .end method
static java.lang.String -$$Nest$fgetmDoubleDataUidList ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDoubleDataUidList;
} // .end method
static java.util.Set -$$Nest$fgetmDoubleWifiAppPN ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDoubleWifiAppPN;
} // .end method
static java.lang.String -$$Nest$fgetmDoubleWifiUidList ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDoubleWifiUidList;
} // .end method
static java.util.Set -$$Nest$fgetmSLAAppDefaultPN ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSLAAppDefaultPN;
} // .end method
static java.util.Set -$$Nest$fgetmSLSGameAppPN ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSLSGameAppPN;
} // .end method
static java.lang.String -$$Nest$fgetmSLSGameUidList ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSLSGameUidList;
} // .end method
static Integer -$$Nest$fgetmSLSVoIPUid ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLSVoIPUid:I */
} // .end method
static void -$$Nest$fputmDoubleDataUidList ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mDoubleDataUidList = p1;
return;
} // .end method
static void -$$Nest$fputmDoubleWifiUidList ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mDoubleWifiUidList = p1;
return;
} // .end method
static void -$$Nest$fputmSLSGameUidList ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mSLSGameUidList = p1;
return;
} // .end method
static void -$$Nest$fputmSLSVoIPUid ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLSVoIPUid:I */
return;
} // .end method
static void -$$Nest$maddSLAAppDefault ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->addSLAAppDefault(I)V */
return;
} // .end method
static void -$$Nest$mcalcAppsTrafficStat ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->calcAppsTrafficStat()V */
return;
} // .end method
static void -$$Nest$mclearSLAApp ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->clearSLAApp(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mdbAddSLAApp ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0, com.xiaomi.NetworkBoost.slaservice.SLAApp p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbAddSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V */
return;
} // .end method
static void -$$Nest$mdbUpdateSLAApp ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0, com.xiaomi.NetworkBoost.slaservice.SLAApp p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbUpdateSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V */
return;
} // .end method
static void -$$Nest$mdeleteSLAAppDefault ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->deleteSLAAppDefault(I)V */
return;
} // .end method
static void -$$Nest$mfetchDoubleDataWhiteListApp ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->fetchDoubleDataWhiteListApp()V */
return;
} // .end method
static com.xiaomi.NetworkBoost.slaservice.SLAApp -$$Nest$mgetSLAAppByUid ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
} // .end method
static void -$$Nest$minitSLAAppDefault ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLAAppDefault()V */
return;
} // .end method
static void -$$Nest$mrestoreAppsTrafficStat ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->restoreAppsTrafficStat()V */
return;
} // .end method
static Boolean -$$Nest$mrestoreSLAAppUpgrade ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->restoreSLAAppUpgrade(Ljava/lang/String;)Z */
} // .end method
static Boolean -$$Nest$msendMsgSetSLAAppList ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgSetSLAAppList()Z */
} // .end method
static void -$$Nest$msetDoubleDataUidToSlad ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setDoubleDataUidToSlad()V */
return;
} // .end method
static void -$$Nest$msetDoubleWifiUidToSlad ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setDoubleWifiUidToSlad()V */
return;
} // .end method
static void -$$Nest$msetUidWhiteListToSlad ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setUidWhiteListToSlad()V */
return;
} // .end method
static void -$$Nest$mstoreSLAAppUpgrade ( com.xiaomi.NetworkBoost.slaservice.SLAAppLib p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->storeSLAAppUpgrade(Ljava/lang/String;)V */
return;
} // .end method
static java.util.ArrayList -$$Nest$sfgetmAppLists ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
} // .end method
static Integer -$$Nest$sfgetmLinkTurboMode ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static java.lang.Object -$$Nest$sfgetmSLAAppLibLock ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
} // .end method
static void -$$Nest$sfputmIsUidChangeReboot ( Boolean p0 ) { //bridge//synthethic
/* .locals 0 */
com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mIsUidChangeReboot = (p0!= 0);
return;
} // .end method
static void -$$Nest$sfputmLinkTurboMode ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static void -$$Nest$smsetSLAMode ( ) { //bridge//synthethic
/* .locals 0 */
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .setSLAMode ( );
return;
} // .end method
static com.xiaomi.NetworkBoost.slaservice.SLAAppLib ( ) {
/* .locals 1 */
/* .line 68 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 69 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 101 */
int v0 = -2; // const/4 v0, -0x2
/* .line 105 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
/* .line 144 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
return;
} // .end method
private com.xiaomi.NetworkBoost.slaservice.SLAAppLib ( ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 154 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 93 */
/* const/16 v0, 0xfa0 */
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCalcTrafficInterval:I */
/* .line 94 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I */
/* .line 95 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mMonth:I */
/* .line 99 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCount:I */
/* .line 106 */
/* new-instance v2, Ljava/util/concurrent/Semaphore; */
/* invoke-direct {v2, v0}, Ljava/util/concurrent/Semaphore;-><init>(I)V */
this.mSemaphore = v2;
/* .line 112 */
int v2 = 0; // const/4 v2, 0x0
this.mDoubleWifiUidList = v2;
/* .line 116 */
this.mSDKDoubleWifiUidList = v2;
/* .line 120 */
this.mDoubleDataUidList = v2;
/* .line 124 */
this.mSLSGameUidList = v2;
/* .line 127 */
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mSLSVoIPUid:I */
/* .line 155 */
(( android.content.Context ) p1 ).getApplicationContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
this.mContext = v2;
/* .line 156 */
/* new-instance v2, Ldatabase/SlaDbSchema/SlaBaseHelper; */
v3 = this.mContext;
/* invoke-direct {v2, v3}, Ldatabase/SlaDbSchema/SlaBaseHelper;-><init>(Landroid/content/Context;)V */
this.slaBaseHelper = v2;
/* .line 157 */
(( database.SlaDbSchema.SlaBaseHelper ) v2 ).getWritableDatabase ( ); // invoke-virtual {v2}, Ldatabase/SlaDbSchema/SlaBaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
this.mDatabase = v2;
/* .line 158 */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
this.mSLSGameAppPN = v2;
/* .line 159 */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
this.mSLAAppDefaultPN = v2;
/* .line 160 */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
this.mDoubleWifiAppPN = v2;
/* .line 161 */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
this.mDoubleDataAppPN = v2;
/* .line 162 */
com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mIsUidChangeReboot = (v1!= 0);
/* .line 163 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->syncSLAAppFromDB()V */
/* .line 164 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLAUIObserver()V */
/* .line 165 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initBroadcastReceiver()V */
/* .line 167 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "link_turbo_mode"; // const-string v3, "link_turbo_mode"
v2 = android.provider.Settings$System .getInt ( v2,v3,v1 );
/* if-ne v0, v2, :cond_0 */
/* .line 168 */
/* .line 170 */
} // :cond_0
/* .line 173 */
} // :goto_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-lez v0, :cond_1 */
/* .line 174 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_1
v2 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
v2 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* if-ge v0, v2, :cond_1 */
/* .line 175 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mTraffic;
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
(( java.util.ArrayList ) v3 ).get ( v0 ); // invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v3 ).getUid ( ); // invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
/* const-wide/16 v4, 0x0 */
java.lang.Long .valueOf ( v4,v5 );
(( java.util.HashMap ) v2 ).put ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 174 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 178 */
} // .end local v0 # "i":I
} // :cond_1
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-lez v0, :cond_2 */
/* .line 179 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
(( java.util.ArrayList ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
v0 = (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v0 ).getDay ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDay()I
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I */
/* .line 180 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
(( java.util.ArrayList ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
v0 = (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v0 ).getDay ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDay()I
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mMonth:I */
/* .line 184 */
} // :cond_2
v0 = this.mContext;
/* .line 185 */
final String v1 = "netstats"; // const-string v1, "netstats"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/usage/NetworkStatsManager; */
/* .line 187 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "SlaUidHandler"; // const-string v1, "SlaUidHandler"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 188 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 189 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$1; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$1;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 210 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "DatabaseHandler"; // const-string v1, "DatabaseHandler"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mDataBaseThread = v0;
/* .line 211 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 212 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$2; */
v1 = this.mDataBaseThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$2;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Looper;)V */
this.mDataBaseHandler = v0;
/* .line 245 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLMCloudObserver()V */
/* .line 246 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLACloudObserver()V */
/* .line 247 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLAAppDefault()V */
/* .line 248 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initDoubleDataCloudObserver()V */
/* .line 249 */
return;
} // .end method
private void UpdateSLAAppList ( java.util.List p0, Boolean p1 ) {
/* .locals 6 */
/* .param p2, "change" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 624 */
/* .local p1, "src":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;" */
/* if-nez p1, :cond_0 */
/* .line 625 */
return;
/* .line 627 */
} // :cond_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
/* monitor-enter v0 */
/* .line 628 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = try { // :try_start_0
/* if-ge v1, v2, :cond_3 */
/* .line 629 */
/* check-cast v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* .line 630 */
/* .local v2, "tmpapp":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v2 ).getUid ( ); // invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
/* invoke-direct {p0, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* .line 631 */
/* .local v3, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
if ( p2 != null) { // if-eqz p2, :cond_1
/* check-cast v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
v4 = (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v4 ).getState ( ); // invoke-virtual {v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getState()Z
/* if-nez v4, :cond_1 */
/* .line 632 */
v4 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
(( java.util.ArrayList ) v4 ).remove ( v3 ); // invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 633 */
/* invoke-direct {p0, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbDeleteSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V */
/* .line 634 */
/* .line 636 */
} // :cond_1
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 637 */
/* iget v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v3 ).setDay ( v4 ); // invoke-virtual {v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDay(I)V
/* .line 638 */
/* iget v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mMonth:I */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v3 ).setMonth ( v4 ); // invoke-virtual {v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonth(I)V
/* .line 639 */
/* const-wide/16 v4, 0x0 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v3 ).setDayTraffic ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDayTraffic(J)V
/* .line 640 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v3 ).setMonthTraffic ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonthTraffic(J)V
/* .line 628 */
} // .end local v2 # "tmpapp":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
} // .end local v3 # "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
} // :cond_2
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 643 */
} // .end local v1 # "i":I
} // :cond_3
/* monitor-exit v0 */
/* .line 644 */
return;
/* .line 643 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void addOrRemoveWhiteList ( java.util.List p0, Boolean p1, java.lang.String p2, java.lang.String p3, Boolean p4 ) {
/* .locals 6 */
/* .param p2, "remove" # Z */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "uidList" # Ljava/lang/String; */
/* .param p5, "isOperateBlacklist" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;Z", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Z)V" */
/* } */
} // .end annotation
/* .line 745 */
/* .local p1, "whiteUidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
/* monitor-enter v0 */
/* .line 746 */
if ( p1 != null) { // if-eqz p1, :cond_a
/* .line 747 */
try { // :try_start_0
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) p3 ).split ( v1 ); // invoke-virtual {p3, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 749 */
/* .local v1, "packages":[Ljava/lang/String; */
v2 = this.mSDKDoubleWifiBlackUidSets;
/* if-nez v2, :cond_0 */
/* .line 750 */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
this.mSDKDoubleWifiBlackUidSets = v2;
/* .line 752 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, v1 */
/* if-ge v2, v3, :cond_3 */
/* .line 753 */
if ( p2 != null) { // if-eqz p2, :cond_1
v3 = this.mSDKDoubleWifiAppPN;
v3 = /* aget-object v4, v1, v2 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 754 */
/* if-nez p5, :cond_2 */
/* .line 755 */
v3 = this.mSDKDoubleWifiAppPN;
/* aget-object v4, v1, v2 */
/* .line 756 */
} // :cond_1
v3 = this.mSDKDoubleWifiAppPN;
v3 = /* aget-object v4, v1, v2 */
/* if-nez v3, :cond_2 */
/* .line 757 */
/* if-nez p5, :cond_2 */
/* .line 758 */
v3 = this.mSDKDoubleWifiAppPN;
/* aget-object v4, v1, v2 */
/* .line 752 */
} // :cond_2
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 762 */
} // .end local v2 # "i":I
} // :cond_3
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) p4 ).split ( v2 ); // invoke-virtual {p4, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 763 */
/* .local v2, "uids":[Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_2
/* array-length v4, v2 */
/* if-ge v3, v4, :cond_8 */
/* .line 764 */
if ( p2 != null) { // if-eqz p2, :cond_5
/* .line 765 */
if ( p5 != null) { // if-eqz p5, :cond_4
v4 = this.mSDKDoubleWifiBlackUidSets;
v4 = /* aget-object v5, v2, v3 */
/* if-nez v4, :cond_4 */
/* .line 766 */
v4 = this.mSDKDoubleWifiBlackUidSets;
/* aget-object v5, v2, v3 */
/* .line 767 */
} // :cond_4
/* if-nez p5, :cond_7 */
v4 = /* aget-object v4, v2, v3 */
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 768 */
/* aget-object v4, v2, v3 */
/* .line 770 */
} // :cond_5
if ( p5 != null) { // if-eqz p5, :cond_6
v4 = this.mSDKDoubleWifiBlackUidSets;
v4 = /* aget-object v5, v2, v3 */
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 771 */
v4 = this.mSDKDoubleWifiBlackUidSets;
/* aget-object v5, v2, v3 */
/* .line 772 */
} // :cond_6
/* if-nez p5, :cond_7 */
v4 = /* aget-object v4, v2, v3 */
/* if-nez v4, :cond_7 */
/* .line 773 */
/* aget-object v4, v2, v3 */
/* .line 763 */
} // :cond_7
} // :goto_3
/* add-int/lit8 v3, v3, 0x1 */
/* .line 777 */
} // .end local v3 # "i":I
v3 = } // :cond_8
/* if-lez v3, :cond_9 */
/* .line 778 */
final String v3 = ","; // const-string v3, ","
java.lang.String .join ( v3,p1 );
/* .line 779 */
/* .local v3, "curWhites":Ljava/lang/String; */
this.mSDKDoubleWifiUidList = v3;
/* .line 780 */
} // .end local v3 # "curWhites":Ljava/lang/String;
/* .line 781 */
} // :cond_9
int v3 = 0; // const/4 v3, 0x0
this.mSDKDoubleWifiUidList = v3;
/* .line 784 */
} // .end local v1 # "packages":[Ljava/lang/String;
} // .end local v2 # "uids":[Ljava/lang/String;
} // :cond_a
} // :goto_4
/* monitor-exit v0 */
/* .line 785 */
return;
/* .line 784 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Integer addSLAApp ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 1427 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1428 */
/* .local v0, "ret":I */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
/* monitor-enter v1 */
/* .line 1429 */
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* .line 1430 */
/* .local v2, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* if-nez v2, :cond_0 */
/* .line 1431 */
/* new-instance v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* invoke-direct {v3, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;-><init>(Ljava/lang/String;)V */
/* move-object v2, v3 */
/* .line 1432 */
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
(( java.util.ArrayList ) v3 ).add ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1433 */
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mTraffic;
/* const-wide/16 v4, 0x0 */
java.lang.Long .valueOf ( v4,v5 );
(( java.util.HashMap ) v3 ).put ( p1, v4 ); // invoke-virtual {v3, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1434 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1436 */
} // :cond_0
int v3 = 1; // const/4 v3, 0x1
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v2 ).setState ( v3 ); // invoke-virtual {v2, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setState(Z)V
/* .line 1437 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1439 */
} // .end local v2 # "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
} // :goto_0
/* monitor-exit v1 */
/* .line 1440 */
/* .line 1439 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private void addSLAAppDefault ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .line 1398 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1399 */
/* .local v0, "slaDefUid":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
java.lang.Integer .toString ( p1 );
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* .line 1400 */
/* if-nez v0, :cond_0 */
/* .line 1401 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
java.lang.Integer .toString ( p1 );
/* invoke-direct {v1, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;-><init>(Ljava/lang/String;)V */
/* move-object v0, v1 */
/* .line 1402 */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1403 */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mTraffic;
java.lang.Integer .toString ( p1 );
/* const-wide/16 v3, 0x0 */
java.lang.Long .valueOf ( v3,v4 );
(( java.util.HashMap ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1404 */
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbAddSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V */
/* .line 1406 */
} // :cond_0
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbUpdateSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V */
/* .line 1408 */
} // :goto_0
return;
} // .end method
private void calcAppsTrafficStat ( ) {
/* .locals 16 */
/* .line 282 */
/* move-object/from16 v0, p0 */
int v1 = 0; // const/4 v1, 0x0
/* .line 283 */
/* .local v1, "uid":I */
/* const-wide/16 v2, 0x0 */
/* .line 284 */
/* .local v2, "traffic":J */
/* const-wide/16 v4, 0x0 */
/* .line 285 */
/* .local v4, "curtraffic":J */
int v6 = 0; // const/4 v6, 0x0
/* .line 286 */
/* .local v6, "state":Z */
v7 = /* invoke-direct/range {p0 ..p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->isDateChanged()I */
/* .line 287 */
/* .local v7, "change":I */
int v8 = 0; // const/4 v8, 0x0
/* .line 288 */
/* .local v8, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;" */
/* invoke-direct/range {p0 ..p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->cloneSLAApp()Ljava/util/List; */
v9 = /* .line 289 */
int v10 = 1; // const/4 v10, 0x1
/* if-ge v9, v10, :cond_0 */
/* .line 290 */
/* iget v9, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCalcTrafficInterval:I */
/* int-to-long v9, v9 */
/* invoke-direct {v0, v9, v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->postCalcAppsTrafficStat(J)V */
/* .line 291 */
return;
/* .line 294 */
} // :cond_0
int v9 = 0; // const/4 v9, 0x0
/* .local v9, "i":I */
v11 = } // :goto_0
/* if-ge v9, v11, :cond_8 */
/* .line 295 */
int v6 = 0; // const/4 v6, 0x0
/* .line 296 */
/* const-wide/16 v2, 0x0 */
/* .line 297 */
/* const-wide/16 v4, 0x0 */
/* .line 299 */
/* check-cast v11, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* .line 300 */
/* .local v11, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).getUid ( ); // invoke-virtual {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
v1 = java.lang.Integer .parseInt ( v13 );
/* .line 301 */
v6 = (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).getState ( ); // invoke-virtual {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getState()Z
/* .line 302 */
final String v13 = "SLM-SRV-SLAAppLib"; // const-string v13, "SLM-SRV-SLAAppLib"
/* const-wide/16 v14, 0x0 */
/* if-nez v6, :cond_2 */
/* .line 303 */
/* if-nez v7, :cond_1 */
/* .line 304 */
/* goto/16 :goto_2 */
/* .line 305 */
} // :cond_1
/* if-ne v7, v10, :cond_2 */
/* .line 306 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "IS_DAY_CHANGE state = "; // const-string v12, "IS_DAY_CHANGE state = "
(( java.lang.StringBuilder ) v10 ).append ( v12 ); // invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v6 ); // invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v13,v10 );
/* .line 307 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).setDayTraffic ( v14, v15 ); // invoke-virtual {v11, v14, v15}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDayTraffic(J)V
/* .line 308 */
/* iget v10, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).setDay ( v10 ); // invoke-virtual {v11, v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDay(I)V
/* .line 312 */
} // :cond_2
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .getDataConsumedForUid ( v1 );
/* move-result-wide v2 */
/* .line 314 */
int v10 = 0; // const/4 v10, 0x0
/* .line 315 */
/* .local v10, "lCurTrafficLong":Ljava/lang/Long; */
v12 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mTraffic;
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).getUid ( ); // invoke-virtual {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
(( java.util.HashMap ) v12 ).get ( v14 ); // invoke-virtual {v12, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* move-object v10, v12 */
/* check-cast v10, Ljava/lang/Long; */
/* .line 317 */
if ( v10 != null) { // if-eqz v10, :cond_3
/* .line 318 */
(( java.lang.Long ) v10 ).longValue ( ); // invoke-virtual {v10}, Ljava/lang/Long;->longValue()J
/* move-result-wide v4 */
/* .line 322 */
} // :cond_3
/* const-wide/16 v14, 0x0 */
/* cmp-long v12, v4, v14 */
/* if-nez v12, :cond_4 */
/* .line 323 */
/* move-wide v4, v2 */
/* .line 327 */
} // :cond_4
/* cmp-long v12, v4, v2 */
/* if-lez v12, :cond_5 */
/* .line 329 */
/* move-wide v2, v4 */
/* .line 332 */
} // :cond_5
/* if-nez v7, :cond_6 */
/* .line 334 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).getDayTraffic ( ); // invoke-virtual {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDayTraffic()J
/* move-result-wide v12 */
/* sub-long v14, v2, v4 */
/* add-long/2addr v12, v14 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).setDayTraffic ( v12, v13 ); // invoke-virtual {v11, v12, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDayTraffic(J)V
/* .line 335 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).getMonthTraffic ( ); // invoke-virtual {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getMonthTraffic()J
/* move-result-wide v12 */
/* sub-long v14, v2, v4 */
/* add-long/2addr v12, v14 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).setMonthTraffic ( v12, v13 ); // invoke-virtual {v11, v12, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonthTraffic(J)V
/* .line 336 */
} // :cond_6
int v12 = 2; // const/4 v12, 0x2
/* if-ne v7, v12, :cond_7 */
/* .line 337 */
final String v12 = "IS_MONTH_CHANGE"; // const-string v12, "IS_MONTH_CHANGE"
android.util.Log .d ( v13,v12 );
/* .line 338 */
/* const-wide/16 v12, 0x0 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).setDayTraffic ( v12, v13 ); // invoke-virtual {v11, v12, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDayTraffic(J)V
/* .line 339 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).setMonthTraffic ( v12, v13 ); // invoke-virtual {v11, v12, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonthTraffic(J)V
/* .line 340 */
/* iget v12, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).setDay ( v12 ); // invoke-virtual {v11, v12}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDay(I)V
/* .line 341 */
/* iget v12, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mMonth:I */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).setMonth ( v12 ); // invoke-virtual {v11, v12}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonth(I)V
/* .line 343 */
} // :cond_7
final String v12 = "IS_DAY_CHANGE"; // const-string v12, "IS_DAY_CHANGE"
android.util.Log .d ( v13,v12 );
/* .line 344 */
/* const-wide/16 v12, 0x0 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).setDayTraffic ( v12, v13 ); // invoke-virtual {v11, v12, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDayTraffic(J)V
/* .line 345 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).getMonthTraffic ( ); // invoke-virtual {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getMonthTraffic()J
/* move-result-wide v12 */
/* sub-long v14, v2, v4 */
/* add-long/2addr v12, v14 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).setMonthTraffic ( v12, v13 ); // invoke-virtual {v11, v12, v13}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonthTraffic(J)V
/* .line 346 */
/* iget v12, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).setDay ( v12 ); // invoke-virtual {v11, v12}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDay(I)V
/* .line 349 */
} // :goto_1
/* move-wide v4, v2 */
/* .line 350 */
v12 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mTraffic;
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v11 ).getUid ( ); // invoke-virtual {v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
java.lang.Long .valueOf ( v4,v5 );
(( java.util.HashMap ) v12 ).put ( v13, v14 ); // invoke-virtual {v12, v13, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 351 */
/* iput-wide v4, v11, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mCurTraffic:J */
/* .line 294 */
} // .end local v10 # "lCurTrafficLong":Ljava/lang/Long;
} // .end local v11 # "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
} // :goto_2
/* add-int/lit8 v9, v9, 0x1 */
int v10 = 1; // const/4 v10, 0x1
/* goto/16 :goto_0 */
/* .line 356 */
} // .end local v9 # "i":I
} // :cond_8
int v9 = 2; // const/4 v9, 0x2
/* if-ne v7, v9, :cond_9 */
/* .line 357 */
/* invoke-direct/range {p0 ..p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->cloneSLAApp()Ljava/util/List; */
int v10 = 1; // const/4 v10, 0x1
/* invoke-direct {v0, v9, v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->UpdateSLAAppList(Ljava/util/List;Z)V */
/* .line 361 */
} // :cond_9
/* sget-boolean v9, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mIsSladRunning:Z */
if ( v9 != null) { // if-eqz v9, :cond_a
/* .line 362 */
/* iget v9, v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCalcTrafficInterval:I */
/* int-to-long v9, v9 */
/* invoke-direct {v0, v9, v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->postCalcAppsTrafficStat(J)V */
/* .line 364 */
} // :cond_a
return;
} // .end method
private void clearSLAApp ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 1462 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1464 */
/* .local v0, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "clearSLAApp uid = "; // const-string v3, "clearSLAApp uid = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v2 );
/* .line 1465 */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
/* monitor-enter v1 */
/* .line 1466 */
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* move-object v0, v2 */
/* .line 1467 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1468 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
(( java.util.ArrayList ) v2 ).remove ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 1470 */
} // :cond_0
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1471 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1472 */
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbDeleteSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V */
/* .line 1473 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "update_uidlist_to_sla" */
/* .line 1474 */
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) p0 ).getLinkTurboWhiteList ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;
/* .line 1473 */
android.provider.Settings$System .putString ( v1,v2,v3 );
/* .line 1476 */
} // :cond_1
return;
/* .line 1470 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
private java.util.List cloneSLAApp ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 616 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 617 */
/* .local v0, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;" */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
/* monitor-enter v1 */
/* .line 618 */
try { // :try_start_0
v2 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
/* .line 619 */
/* monitor-exit v1 */
/* .line 620 */
/* .line 619 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private Integer dbAcquirePermit ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 949 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "action:"; // const-string v1, "action:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " acquire a permit"; // const-string v2, " acquire a permit"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SLM-SRV-SLAAppLib"; // const-string v2, "SLM-SRV-SLAAppLib"
android.util.Log .d ( v2,v0 );
/* .line 951 */
try { // :try_start_0
v0 = this.mSemaphore;
(( java.util.concurrent.Semaphore ) v0 ).acquire ( ); // invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V
/* .line 952 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " acquire a permit success"; // const-string v1, " acquire a permit success"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v0 );
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 957 */
/* nop */
/* .line 958 */
int v0 = 0; // const/4 v0, 0x0
/* .line 953 */
/* :catch_0 */
/* move-exception v0 */
/* .line 954 */
/* .local v0, "e":Ljava/lang/InterruptedException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "an InterruptedException hanppened! action:"; // const-string v3, "an InterruptedException hanppened! action:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v1 );
/* .line 955 */
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v1 ).interrupt ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
/* .line 956 */
int v1 = -1; // const/4 v1, -0x1
} // .end method
private void dbAddSLAApp ( com.xiaomi.NetworkBoost.slaservice.SLAApp p0 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* .line 968 */
/* if-nez p1, :cond_0 */
/* .line 969 */
return;
/* .line 971 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "dbAddSLAApp uid = "; // const-string v1, "dbAddSLAApp uid = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
android.util.Log .d ( v1,v0 );
/* .line 972 */
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .getContentValues ( p1 );
/* .line 973 */
/* .local v0, "values":Landroid/content/ContentValues; */
final String v1 = "add"; // const-string v1, "add"
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbAcquirePermit(Ljava/lang/String;)I */
/* .line 974 */
v2 = this.mDatabase;
final String v3 = "SlaUid"; // const-string v3, "SlaUid"
int v4 = 0; // const/4 v4, 0x0
(( android.database.sqlite.SQLiteDatabase ) v2 ).insert ( v3, v4, v0 ); // invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
/* .line 975 */
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbReleasePermit(Ljava/lang/String;)V */
/* .line 976 */
return;
} // .end method
private void dbDeleteSLAApp ( com.xiaomi.NetworkBoost.slaservice.SLAApp p0 ) {
/* .locals 6 */
/* .param p1, "app" # Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* .line 992 */
/* if-nez p1, :cond_0 */
/* .line 993 */
return;
/* .line 996 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "dbDeleteSLAApp uid = "; // const-string v1, "dbDeleteSLAApp uid = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
android.util.Log .d ( v1,v0 );
/* .line 997 */
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .getContentValues ( p1 );
/* .line 998 */
/* .local v0, "values":Landroid/content/ContentValues; */
final String v1 = "del"; // const-string v1, "del"
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbAcquirePermit(Ljava/lang/String;)I */
/* .line 999 */
v2 = this.mDatabase;
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
/* filled-new-array {v3}, [Ljava/lang/String; */
final String v4 = "SlaUid"; // const-string v4, "SlaUid"
/* const-string/jumbo v5, "uid = ?" */
(( android.database.sqlite.SQLiteDatabase ) v2 ).delete ( v4, v5, v3 ); // invoke-virtual {v2, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
/* .line 1000 */
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbReleasePermit(Ljava/lang/String;)V */
/* .line 1001 */
return;
} // .end method
private database.SlaDbSchema.SlaCursorWrapper dbGetCursorSLAApp ( java.lang.String p0, java.lang.String[] p1 ) {
/* .locals 8 */
/* .param p1, "whereClaue" # Ljava/lang/String; */
/* .param p2, "whereArgs" # [Ljava/lang/String; */
/* .line 1004 */
v0 = this.mDatabase;
final String v1 = "SlaUid"; // const-string v1, "SlaUid"
int v2 = 0; // const/4 v2, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* move-object v3, p1 */
/* move-object v4, p2 */
/* invoke-virtual/range {v0 ..v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 1013 */
/* .local v0, "cursor":Landroid/database/Cursor; */
/* new-instance v1, Ldatabase/SlaDbSchema/SlaCursorWrapper; */
/* invoke-direct {v1, v0}, Ldatabase/SlaDbSchema/SlaCursorWrapper;-><init>(Landroid/database/Cursor;)V */
} // .end method
private void dbReleasePermit ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 962 */
v0 = this.mSemaphore;
(( java.util.concurrent.Semaphore ) v0 ).drainPermits ( ); // invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->drainPermits()I
/* .line 963 */
v0 = this.mSemaphore;
(( java.util.concurrent.Semaphore ) v0 ).release ( ); // invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V
/* .line 964 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "action:"; // const-string v1, "action:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " releases a permit"; // const-string v1, " releases a permit"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
android.util.Log .d ( v1,v0 );
/* .line 965 */
return;
} // .end method
private void dbUpdateSLAApp ( com.xiaomi.NetworkBoost.slaservice.SLAApp p0 ) {
/* .locals 6 */
/* .param p1, "app" # Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* .line 979 */
/* if-nez p1, :cond_0 */
/* .line 980 */
return;
/* .line 983 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "dbUpdateSLAApp uid = "; // const-string v1, "dbUpdateSLAApp uid = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
android.util.Log .d ( v1,v0 );
/* .line 984 */
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .getContentValues ( p1 );
/* .line 985 */
/* .local v0, "values":Landroid/content/ContentValues; */
/* const-string/jumbo v1, "update" */
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbAcquirePermit(Ljava/lang/String;)I */
/* .line 986 */
v2 = this.mDatabase;
/* .line 987 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
/* filled-new-array {v3}, [Ljava/lang/String; */
/* .line 986 */
final String v4 = "SlaUid"; // const-string v4, "SlaUid"
/* const-string/jumbo v5, "uid = ?" */
(( android.database.sqlite.SQLiteDatabase ) v2 ).update ( v4, v0, v5, v3 ); // invoke-virtual {v2, v4, v0, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
/* .line 988 */
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbReleasePermit(Ljava/lang/String;)V */
/* .line 989 */
return;
} // .end method
private Integer deleteSLAApp ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 1450 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1451 */
/* .local v0, "ret":I */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
/* monitor-enter v1 */
/* .line 1452 */
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* .line 1453 */
/* .local v2, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1454 */
int v3 = 0; // const/4 v3, 0x0
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v2 ).setState ( v3 ); // invoke-virtual {v2, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setState(Z)V
/* .line 1455 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1457 */
} // .end local v2 # "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
} // :cond_0
/* monitor-exit v1 */
/* .line 1458 */
/* .line 1457 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private void deleteSLAAppDefault ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 1411 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1412 */
/* .local v0, "slaDefUid":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
java.lang.Integer .toString ( p1 );
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* .line 1413 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1414 */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
(( java.util.ArrayList ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 1415 */
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbDeleteSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V */
/* .line 1417 */
} // :cond_0
return;
} // .end method
private void fetchDoubleDataWhiteListApp ( ) {
/* .locals 12 */
/* .line 1552 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "fetchDoubleDataWhiteListApp: mDoubleDataUidList = "; // const-string v1, "fetchDoubleDataWhiteListApp: mDoubleDataUidList = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mDoubleDataUidList;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "mDoubleDataAppPN size = "; // const-string v1, "mDoubleDataAppPN size = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mDoubleDataAppPN;
v1 = /* .line 1553 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1552 */
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
android.util.Log .d ( v1,v0 );
/* .line 1555 */
v0 = this.mDoubleDataUidList;
/* if-nez v0, :cond_0 */
v0 = v0 = this.mDoubleDataAppPN;
/* if-lez v0, :cond_1 */
/* .line 1556 */
} // :cond_0
final String v0 = "reset mDoubleDataAppPN and mDoubleDataUidList"; // const-string v0, "reset mDoubleDataAppPN and mDoubleDataUidList"
android.util.Log .d ( v1,v0 );
/* .line 1557 */
v0 = this.mDoubleDataAppPN;
/* .line 1558 */
int v0 = 0; // const/4 v0, 0x0
this.mDoubleDataUidList = v0;
/* .line 1562 */
} // :cond_1
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "dual_data_concurrent_mode_white_list_pkg_name"; // const-string v2, "dual_data_concurrent_mode_white_list_pkg_name"
android.provider.Settings$Global .getString ( v0,v2 );
/* .line 1564 */
/* .local v0, "tput_data":Ljava/lang/String; */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "dual_data_concurrent_limited_white_list_pkg_name"; // const-string v3, "dual_data_concurrent_limited_white_list_pkg_name"
android.provider.Settings$Global .getString ( v2,v3 );
/* .line 1566 */
/* .local v2, "limited_data":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ","; // const-string v4, ","
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1567 */
/* .local v3, "whiteString_doubleData":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Cloud DDApp: "; // const-string v6, "Cloud DDApp: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v5 );
/* .line 1568 */
v5 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v5, :cond_2 */
/* .line 1569 */
(( java.lang.String ) v3 ).split ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1570 */
/* .local v5, "packages":[Ljava/lang/String; */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1571 */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_0
/* array-length v7, v5 */
/* if-ge v6, v7, :cond_2 */
/* .line 1572 */
v7 = this.mDoubleDataAppPN;
/* aget-object v8, v5, v6 */
/* .line 1571 */
/* add-int/lit8 v6, v6, 0x1 */
/* .line 1578 */
} // .end local v5 # "packages":[Ljava/lang/String;
} // .end local v6 # "i":I
} // :cond_2
v5 = this.mContext;
(( android.content.Context ) v5 ).getPackageManager ( ); // invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 1579 */
/* .local v5, "pm":Landroid/content/pm/PackageManager; */
int v6 = 1; // const/4 v6, 0x1
(( android.content.pm.PackageManager ) v5 ).getInstalledPackages ( v6 ); // invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;
/* .line 1580 */
/* .local v6, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
v8 = } // :goto_1
if ( v8 != null) { // if-eqz v8, :cond_5
/* check-cast v8, Landroid/content/pm/PackageInfo; */
/* .line 1581 */
/* .local v8, "app":Landroid/content/pm/PackageInfo; */
v9 = this.packageName;
if ( v9 != null) { // if-eqz v9, :cond_4
v9 = this.applicationInfo;
if ( v9 != null) { // if-eqz v9, :cond_4
v9 = this.mDoubleDataAppPN;
v10 = this.packageName;
v9 = /* .line 1582 */
if ( v9 != null) { // if-eqz v9, :cond_4
/* .line 1583 */
v9 = this.applicationInfo;
/* iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 1584 */
/* .local v9, "uid":I */
v10 = this.mDoubleDataUidList;
/* if-nez v10, :cond_3 */
/* .line 1585 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.Integer .toString ( v9 );
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v4 ); // invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
this.mDoubleDataUidList = v10;
/* .line 1587 */
} // :cond_3
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
v11 = this.mDoubleDataUidList;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toString ( v9 );
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v4 ); // invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
this.mDoubleDataUidList = v10;
/* .line 1590 */
} // .end local v8 # "app":Landroid/content/pm/PackageInfo;
} // .end local v9 # "uid":I
} // :cond_4
} // :goto_2
/* .line 1591 */
} // :cond_5
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "mDoubleDataUidList = "; // const-string v7, "mDoubleDataUidList = "
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mDoubleDataUidList;
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v4 );
/* .line 1592 */
return;
} // .end method
private java.lang.String generateFinalWhiteList ( java.util.List p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)", */
/* "Ljava/lang/String;" */
/* } */
} // .end annotation
/* .line 912 */
/* .local p1, "uidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez p1, :cond_0 */
/* .line 913 */
final String v0 = ""; // const-string v0, ""
/* .line 915 */
} // :cond_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
/* monitor-enter v0 */
/* .line 916 */
try { // :try_start_0
v1 = this.mSDKDoubleWifiBlackUidSets;
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = /* .line 917 */
/* if-lez v1, :cond_2 */
/* .line 918 */
v1 = this.mSDKDoubleWifiBlackUidSets;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/lang/String; */
/* .line 919 */
v3 = /* .local v2, "blackUid":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 920 */
/* .line 921 */
} // .end local v2 # "blackUid":Ljava/lang/String;
} // :cond_1
/* .line 923 */
} // :cond_2
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 925 */
final String v0 = ","; // const-string v0, ","
java.lang.String .join ( v0,p1 );
/* .line 927 */
/* .local v0, "finalUids":Ljava/lang/String; */
/* .line 923 */
} // .end local v0 # "finalUids":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.SLAAppLib get ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 147 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.sSLAAppLib;
/* if-nez v0, :cond_0 */
/* .line 148 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;-><init>(Landroid/content/Context;)V */
/* .line 151 */
} // :cond_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.sSLAAppLib;
} // .end method
private static android.content.ContentValues getContentValues ( com.xiaomi.NetworkBoost.slaservice.SLAApp p0 ) {
/* .locals 3 */
/* .param p0, "app" # Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* .line 1040 */
/* new-instance v0, Landroid/content/ContentValues; */
/* invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V */
/* .line 1041 */
/* .local v0, "values":Landroid/content/ContentValues; */
/* const-string/jumbo v1, "uid" */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p0 ).getUid ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
(( android.content.ContentValues ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1042 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p0 ).getDayTraffic ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDayTraffic()J
/* move-result-wide v1 */
java.lang.Long .valueOf ( v1,v2 );
final String v2 = "dayTraffic"; // const-string v2, "dayTraffic"
(( android.content.ContentValues ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
/* .line 1043 */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p0 ).getMonthTraffic ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getMonthTraffic()J
/* move-result-wide v1 */
java.lang.Long .valueOf ( v1,v2 );
final String v2 = "monthTraffic"; // const-string v2, "monthTraffic"
(( android.content.ContentValues ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
/* .line 1044 */
v1 = (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p0 ).getState ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getState()Z
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "state" */
(( android.content.ContentValues ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 1045 */
v1 = (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p0 ).getDay ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDay()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "day"; // const-string v2, "day"
(( android.content.ContentValues ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 1046 */
v1 = (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p0 ).getMonth ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getMonth()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "month"; // const-string v2, "month"
(( android.content.ContentValues ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 1048 */
} // .end method
private static Long getDataConsumedForUid ( Integer p0 ) {
/* .locals 14 */
/* .param p0, "uid" # I */
/* .line 378 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mNetworkStatsManager;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 379 */
int v0 = 0; // const/4 v0, 0x0
/* .line 380 */
/* .local v0, "result":Landroid/app/usage/NetworkStats; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v8 */
/* .line 381 */
/* .local v8, "currentTime":J */
/* const-wide/16 v10, 0x0 */
/* .line 382 */
/* .local v10, "totalTxBytes":J */
/* const-wide/16 v12, 0x0 */
/* .line 384 */
/* .local v12, "totalRxBytes":J */
try { // :try_start_0
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mNetworkStatsManager;
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
/* const-wide/16 v4, 0x0 */
/* move-wide v6, v8 */
/* invoke-virtual/range {v1 ..v7}, Landroid/app/usage/NetworkStatsManager;->querySummary(ILjava/lang/String;JJ)Landroid/app/usage/NetworkStats; */
/* move-object v0, v1 */
/* .line 390 */
/* new-instance v1, Landroid/app/usage/NetworkStats$Bucket; */
/* invoke-direct {v1}, Landroid/app/usage/NetworkStats$Bucket;-><init>()V */
/* .line 391 */
/* .local v1, "bucket":Landroid/app/usage/NetworkStats$Bucket; */
} // :goto_0
v2 = (( android.app.usage.NetworkStats ) v0 ).hasNextBucket ( ); // invoke-virtual {v0}, Landroid/app/usage/NetworkStats;->hasNextBucket()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 392 */
(( android.app.usage.NetworkStats ) v0 ).getNextBucket ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/usage/NetworkStats;->getNextBucket(Landroid/app/usage/NetworkStats$Bucket;)Z
/* .line 393 */
v2 = (( android.app.usage.NetworkStats$Bucket ) v1 ).getUid ( ); // invoke-virtual {v1}, Landroid/app/usage/NetworkStats$Bucket;->getUid()I
/* .line 395 */
/* .local v2, "summaryUid":I */
/* if-ne v2, p0, :cond_0 */
/* .line 396 */
(( android.app.usage.NetworkStats$Bucket ) v1 ).getTxBytes ( ); // invoke-virtual {v1}, Landroid/app/usage/NetworkStats$Bucket;->getTxBytes()J
/* move-result-wide v3 */
/* add-long/2addr v10, v3 */
/* .line 397 */
(( android.app.usage.NetworkStats$Bucket ) v1 ).getRxBytes ( ); // invoke-virtual {v1}, Landroid/app/usage/NetworkStats$Bucket;->getRxBytes()J
/* move-result-wide v3 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* add-long/2addr v12, v3 */
/* .line 399 */
} // .end local v2 # "summaryUid":I
} // :cond_0
/* .line 402 */
} // .end local v1 # "bucket":Landroid/app/usage/NetworkStats$Bucket;
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 403 */
} // :goto_1
(( android.app.usage.NetworkStats ) v0 ).close ( ); // invoke-virtual {v0}, Landroid/app/usage/NetworkStats;->close()V
/* .line 402 */
/* :catchall_0 */
/* move-exception v1 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 403 */
(( android.app.usage.NetworkStats ) v0 ).close ( ); // invoke-virtual {v0}, Landroid/app/usage/NetworkStats;->close()V
/* .line 405 */
} // :cond_2
/* throw v1 */
/* .line 400 */
/* :catch_0 */
/* move-exception v1 */
/* .line 402 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 403 */
/* .line 406 */
} // :cond_3
} // :goto_2
/* add-long v1, v10, v12 */
/* return-wide v1 */
/* .line 408 */
} // .end local v0 # "result":Landroid/app/usage/NetworkStats;
} // .end local v8 # "currentTime":J
} // .end local v10 # "totalTxBytes":J
} // .end local v12 # "totalRxBytes":J
} // :cond_4
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
} // .end method
private com.xiaomi.NetworkBoost.slaservice.SLAApp getSLAAppByUid ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 648 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-ge v0, v1, :cond_1 */
/* .line 649 */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v1 ).getUid ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 650 */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* .line 648 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 654 */
} // .end local v0 # "i":I
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void initBroadcastReceiver ( ) {
/* .locals 3 */
/* .line 1243 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 1244 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.PACKAGE_ADDED"; // const-string v1, "android.intent.action.PACKAGE_ADDED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1245 */
final String v1 = "android.intent.action.PACKAGE_REMOVED"; // const-string v1, "android.intent.action.PACKAGE_REMOVED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1246 */
final String v1 = "android.intent.action.PACKAGE_REPLACED"; // const-string v1, "android.intent.action.PACKAGE_REPLACED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1247 */
final String v1 = "com.android.phone.intent.action.DUAL_DATA_CONCURRENT_MODE_WHITE_LIST_DONE"; // const-string v1, "com.android.phone.intent.action.DUAL_DATA_CONCURRENT_MODE_WHITE_LIST_DONE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1248 */
final String v1 = "com.android.phone.intent.action.DUAL_DATA_CONCURRENT_LIMITED_WHITE_LIST_DONE"; // const-string v1, "com.android.phone.intent.action.DUAL_DATA_CONCURRENT_LIMITED_WHITE_LIST_DONE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1249 */
final String v1 = "package"; // const-string v1, "package"
(( android.content.IntentFilter ) v0 ).addDataScheme ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V
/* .line 1250 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$6;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V */
/* .line 1350 */
/* .local v1, "receiver":Landroid/content/BroadcastReceiver; */
v2 = this.mContext;
(( android.content.Context ) v2 ).registerReceiver ( v1, v0 ); // invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 1351 */
return;
} // .end method
private void initDoubleDataCloudObserver ( ) {
/* .locals 4 */
/* .line 1532 */
final String v0 = "SLM-SRV-SLAAppLib"; // const-string v0, "SLM-SRV-SLAAppLib"
final String v1 = "initDoubleDataWhiteListApp"; // const-string v1, "initDoubleDataWhiteListApp"
android.util.Log .d ( v0,v1 );
/* .line 1533 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->fetchDoubleDataWhiteListApp()V */
/* .line 1534 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$7; */
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$7;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Handler;)V */
/* .line 1544 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1545 */
final String v2 = "dual_data_concurrent_mode_white_list_pkg_name"; // const-string v2, "dual_data_concurrent_mode_white_list_pkg_name"
android.provider.Settings$Global .getUriFor ( v2 );
/* .line 1544 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 1546 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1547 */
final String v2 = "dual_data_concurrent_limited_white_list_pkg_name"; // const-string v2, "dual_data_concurrent_limited_white_list_pkg_name"
android.provider.Settings$Global .getUriFor ( v2 );
/* .line 1546 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 1548 */
return;
} // .end method
private void initSLAAppDefault ( ) {
/* .locals 23 */
/* .line 1354 */
/* move-object/from16 v0, p0 */
v1 = v1 = this.mSLAAppDefaultPN;
/* if-nez v1, :cond_0 */
/* .line 1355 */
return;
/* .line 1358 */
} // :cond_0
v1 = this.mContext;
/* .line 1359 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1358 */
final String v2 = "cloud_sla_whitelist"; // const-string v2, "cloud_sla_whitelist"
android.provider.Settings$System .getString ( v1,v2 );
/* .line 1360 */
/* .local v1, "whiteString_sla":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Cloud SLAAppDefault: "; // const-string v3, "Cloud SLAAppDefault: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "SLM-SRV-SLAAppLib"; // const-string v3, "SLM-SRV-SLAAppLib"
android.util.Log .d ( v3,v2 );
/* .line 1361 */
v2 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v2, :cond_1 */
/* .line 1362 */
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1363 */
/* .local v2, "packages":[Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1364 */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* array-length v5, v2 */
/* if-ge v4, v5, :cond_1 */
/* .line 1365 */
v5 = this.mSLAAppDefaultPN;
/* aget-object v6, v2, v4 */
/* .line 1364 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1371 */
} // .end local v2 # "packages":[Ljava/lang/String;
} // .end local v4 # "i":I
} // :cond_1
v2 = v2 = this.mSLAAppDefaultPN;
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1373 */
final String v2 = "ro.boot.hwc"; // const-string v2, "ro.boot.hwc"
android.os.SystemProperties .get ( v2 );
final String v4 = "CN"; // const-string v4, "CN"
v2 = (( java.lang.String ) v4 ).equalsIgnoreCase ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1374 */
final String v4 = "com.tencent.mm"; // const-string v4, "com.tencent.mm"
final String v5 = "com.tencent.mobileqq"; // const-string v5, "com.tencent.mobileqq"
final String v6 = "com.eg.android.AlipayGphone"; // const-string v6, "com.eg.android.AlipayGphone"
final String v7 = "com.taobao.taobao"; // const-string v7, "com.taobao.taobao"
final String v8 = "com.tmall.wireless"; // const-string v8, "com.tmall.wireless"
final String v9 = "com.jingdong.app.mall"; // const-string v9, "com.jingdong.app.mall"
final String v10 = "com.xiaomi.youpin"; // const-string v10, "com.xiaomi.youpin"
final String v11 = "com.tencent.mtt"; // const-string v11, "com.tencent.mtt"
final String v12 = "com.hupu.games"; // const-string v12, "com.hupu.games"
final String v13 = "com.zhihu.android"; // const-string v13, "com.zhihu.android"
final String v14 = "com.dianping.v1"; // const-string v14, "com.dianping.v1"
final String v15 = "com.tencent.qqmusic"; // const-string v15, "com.tencent.qqmusic"
final String v16 = "com.netease.cloudmusic"; // const-string v16, "com.netease.cloudmusic"
final String v17 = "com.UCMobile"; // const-string v17, "com.UCMobile"
final String v18 = "com.ss.android.article.news"; // const-string v18, "com.ss.android.article.news"
final String v19 = "com.smile.gifmaker"; // const-string v19, "com.smile.gifmaker"
final String v20 = "com.ss.android.ugc.aweme"; // const-string v20, "com.ss.android.ugc.aweme"
/* const-string/jumbo v21, "tv.danmaku.bili" */
final String v22 = "org.zwanoo.android.speedtest"; // const-string v22, "org.zwanoo.android.speedtest"
/* filled-new-array/range {v4 ..v22}, [Ljava/lang/String; */
/* .restart local v2 # "packages":[Ljava/lang/String; */
/* .line 1383 */
} // .end local v2 # "packages":[Ljava/lang/String;
} // :cond_2
final String v4 = "com.spotify.music"; // const-string v4, "com.spotify.music"
final String v5 = "com.ebay.mobile"; // const-string v5, "com.ebay.mobile"
final String v6 = "com.amazon.kindle"; // const-string v6, "com.amazon.kindle"
final String v7 = "com.instagram.android"; // const-string v7, "com.instagram.android"
final String v8 = "com.melodis.midomiMusicIdentifier.freemium"; // const-string v8, "com.melodis.midomiMusicIdentifier.freemium"
final String v9 = "com.google.android.youtube"; // const-string v9, "com.google.android.youtube"
/* filled-new-array/range {v4 ..v9}, [Ljava/lang/String; */
/* .line 1388 */
/* .restart local v2 # "packages":[Ljava/lang/String; */
} // :goto_1
/* nop */
/* .line 1389 */
int v4 = 0; // const/4 v4, 0x0
/* .restart local v4 # "i":I */
} // :goto_2
/* array-length v5, v2 */
/* if-ge v4, v5, :cond_3 */
/* .line 1390 */
v5 = this.mSLAAppDefaultPN;
/* aget-object v6, v2, v4 */
/* .line 1389 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1394 */
} // .end local v2 # "packages":[Ljava/lang/String;
} // .end local v4 # "i":I
} // :cond_3
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "set SLAAppDefault: " */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mSLAAppDefaultPN;
(( java.lang.Object ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v2 );
/* .line 1395 */
return;
} // .end method
private void initSLACloudObserver ( ) {
/* .locals 4 */
/* .line 1052 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3; */
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Handler;)V */
/* .line 1085 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1086 */
final String v2 = "cloud_sla_whitelist"; // const-string v2, "cloud_sla_whitelist"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1085 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 1087 */
return;
} // .end method
private void initSLAUIObserver ( ) {
/* .locals 4 */
/* .line 1226 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$5; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$5;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Handler;)V */
/* .line 1238 */
/* .local v0, "mContentObserver":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1239 */
final String v2 = "link_turbo_mode"; // const-string v2, "link_turbo_mode"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1238 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 1240 */
return;
} // .end method
private void initSLMCloudObserver ( ) {
/* .locals 4 */
/* .line 1090 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4; */
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$4;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Handler;)V */
/* .line 1216 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1217 */
final String v2 = "cloud_sls_whitelist"; // const-string v2, "cloud_sls_whitelist"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1216 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 1218 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1219 */
final String v2 = "cloud_double_wifi_uidlist"; // const-string v2, "cloud_double_wifi_uidlist"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1218 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 1220 */
/* new-instance v1, Ljava/lang/Thread; */
/* new-instance v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda2; */
/* invoke-direct {v2, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda2;-><init>(Landroid/database/ContentObserver;)V */
/* invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 1222 */
(( java.lang.Thread ) v1 ).start ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->start()V
/* .line 1223 */
return;
} // .end method
private Integer isDateChanged ( ) {
/* .locals 10 */
/* .line 417 */
int v0 = 0; // const/4 v0, 0x0
/* .line 418 */
/* .local v0, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
int v1 = 0; // const/4 v1, 0x0
/* .line 419 */
/* .local v1, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;" */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->cloneSLAApp()Ljava/util/List; */
/* .line 420 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
/* monitor-enter v2 */
/* .line 421 */
v3 = try { // :try_start_0
int v4 = 0; // const/4 v4, 0x0
int v5 = 1; // const/4 v5, 0x1
/* if-ge v3, v5, :cond_0 */
/* .line 422 */
/* monitor-exit v2 */
/* .line 424 */
} // :cond_0
/* check-cast v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* move-object v0, v3 */
/* .line 426 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 427 */
v2 = (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v0 ).getDay ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDay()I
/* .line 428 */
/* .local v2, "day":I */
v3 = (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v0 ).getMonth ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getMonth()I
/* .line 430 */
/* .local v3, "month":I */
java.util.Calendar .getInstance ( );
/* .line 431 */
/* .local v6, "c":Ljava/util/Calendar; */
int v7 = 2; // const/4 v7, 0x2
v8 = (( java.util.Calendar ) v6 ).get ( v7 ); // invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I
/* add-int/2addr v8, v5 */
/* .line 432 */
/* .local v8, "localmonth":I */
int v9 = 5; // const/4 v9, 0x5
v9 = (( java.util.Calendar ) v6 ).get ( v9 ); // invoke-virtual {v6, v9}, Ljava/util/Calendar;->get(I)I
/* .line 434 */
/* .local v9, "localday":I */
/* if-eq v3, v8, :cond_1 */
/* if-lez v3, :cond_1 */
/* .line 435 */
/* iput v9, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I */
/* .line 436 */
/* iput v8, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mMonth:I */
/* .line 437 */
/* .line 438 */
} // :cond_1
/* if-eq v2, v9, :cond_2 */
/* if-lez v2, :cond_2 */
/* .line 439 */
/* iput v9, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mDay:I */
/* .line 440 */
/* .line 442 */
} // :cond_2
/* .line 426 */
} // .end local v2 # "day":I
} // .end local v3 # "month":I
} // .end local v6 # "c":Ljava/util/Calendar;
} // .end local v8 # "localmonth":I
} // .end local v9 # "localday":I
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v3 */
} // .end method
static void lambda$initSLMCloudObserver$0 ( android.database.ContentObserver p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "observer" # Landroid/database/ContentObserver; */
/* .line 1221 */
int v0 = 0; // const/4 v0, 0x0
(( android.database.ContentObserver ) p0 ).onChange ( v0 ); // invoke-virtual {p0, v0}, Landroid/database/ContentObserver;->onChange(Z)V
/* .line 1222 */
return;
} // .end method
private void postCalcAppsTrafficStat ( Long p0 ) {
/* .locals 2 */
/* .param p1, "delay" # J */
/* .line 271 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCount:I */
/* const/16 v1, 0xf */
/* if-ne v0, v1, :cond_0 */
/* .line 272 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->restoreAppsTrafficStat()V */
/* .line 274 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCount:I */
/* .line 276 */
} // :cond_0
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCount:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->mCount:I */
/* .line 277 */
v0 = this.mHandler;
/* const/16 v1, 0xc9 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 278 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, p1, p2 ); // invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 279 */
return;
} // .end method
private static void printTraffic ( com.xiaomi.NetworkBoost.slaservice.SLAApp p0 ) {
/* .locals 4 */
/* .param p0, "app" # Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* .line 368 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "+++++++++++++++++++app uid: "; // const-string v1, "+++++++++++++++++++app uid: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p0 ).getUid ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
android.util.Log .d ( v1,v0 );
/* .line 369 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "app day traffic: "; // const-string v2, "app day traffic: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p0 ).getDayTraffic ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDayTraffic()J
/* move-result-wide v2 */
com.xiaomi.NetworkBoost.slaservice.FormatBytesUtil .formatBytes ( v2,v3 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 370 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "app month traffic: "; // const-string v2, "app month traffic: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p0 ).getMonthTraffic ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getMonthTraffic()J
/* move-result-wide v2 */
com.xiaomi.NetworkBoost.slaservice.FormatBytesUtil .formatBytes ( v2,v3 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 371 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "current traffic: "; // const-string v2, "current traffic: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mCurTraffic:J */
com.xiaomi.NetworkBoost.slaservice.FormatBytesUtil .formatBytes ( v2,v3 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 372 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "app state: "; // const-string v2, "app state: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p0 ).getState ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getState()Z
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 373 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "app day: "; // const-string v2, "app day: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p0 ).getDay ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getDay()I
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 374 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "app month: "; // const-string v2, "app month: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) p0 ).getMonth ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getMonth()I
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 375 */
return;
} // .end method
private void printUidLists ( ) {
/* .locals 8 */
/* .line 659 */
final String v0 = ""; // const-string v0, ""
/* .line 660 */
/* .local v0, "activeuidlist":Ljava/lang/String; */
final String v1 = ""; // const-string v1, ""
/* .line 662 */
/* .local v1, "deactiveuidlist":Ljava/lang/String; */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->cloneSLAApp()Ljava/util/List; */
/* .line 664 */
v3 = /* .local v2, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;" */
int v4 = 1; // const/4 v4, 0x1
/* if-ge v3, v4, :cond_0 */
/* .line 665 */
return;
/* .line 668 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
v5 = } // :goto_0
/* if-ge v3, v5, :cond_2 */
/* .line 669 */
/* check-cast v5, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
v5 = (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v5 ).getState ( ); // invoke-virtual {v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getState()Z
final String v6 = ","; // const-string v6, ","
/* if-ne v5, v4, :cond_1 */
/* .line 670 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v7, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v7 ).getUid ( ); // invoke-virtual {v7}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 672 */
} // :cond_1
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v7, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v7 ).getUid ( ); // invoke-virtual {v7}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 668 */
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 676 */
} // :cond_2
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "activeuidlist:"; // const-string v5, "activeuidlist:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "SLM-SRV-SLAAppLib"; // const-string v5, "SLM-SRV-SLAAppLib"
android.util.Log .d ( v5,v4 );
/* .line 677 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "deactiveuidlist:"; // const-string v6, "deactiveuidlist:"
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v5,v4 );
/* .line 678 */
return;
} // .end method
private void refreshSLAAppUpgradeList ( ) {
/* .locals 8 */
/* .line 1493 */
/* const-wide/32 v0, 0x493e0 */
/* .line 1494 */
/* .local v0, "refreshInterval":J */
java.util.Calendar .getInstance ( );
(( java.util.Calendar ) v2 ).getTimeInMillis ( ); // invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v2 */
/* .line 1495 */
/* .local v2, "now":J */
v4 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppUpgradeList;
(( java.util.HashMap ) v4 ).entrySet ( ); // invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
/* .line 1498 */
/* .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;>;" */
} // :cond_0
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 1499 */
/* check-cast v5, Ljava/util/Map$Entry; */
/* .line 1500 */
/* .local v5, "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;" */
/* check-cast v6, Ljava/lang/Long; */
(( java.lang.Long ) v6 ).longValue ( ); // invoke-virtual {v6}, Ljava/lang/Long;->longValue()J
/* move-result-wide v6 */
/* add-long/2addr v6, v0 */
/* cmp-long v6, v6, v2 */
/* if-gez v6, :cond_0 */
/* .line 1501 */
/* .line 1504 */
} // .end local v5 # "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
} // :cond_1
return;
} // .end method
private void restoreAppsTrafficStat ( ) {
/* .locals 3 */
/* .line 447 */
int v0 = 0; // const/4 v0, 0x0
/* .line 448 */
/* .local v0, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;" */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
/* monitor-enter v1 */
/* .line 449 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->cloneSLAApp()Ljava/util/List; */
/* move-object v0, v2 */
/* .line 450 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 452 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v2 = } // :goto_0
/* if-ge v1, v2, :cond_0 */
/* .line 453 */
/* check-cast v2, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbUpdateSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V */
/* .line 452 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 455 */
} // .end local v1 # "i":I
} // :cond_0
return;
/* .line 450 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
private Boolean restoreSLAAppUpgrade ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 1520 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1521 */
/* .local v0, "ret":Z */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->refreshSLAAppUpgradeList()V */
/* .line 1523 */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppUpgradeList;
v1 = (( java.util.HashMap ) v1 ).containsKey ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1524 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1525 */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppUpgradeList;
(( java.util.HashMap ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1528 */
} // :cond_0
} // .end method
private Boolean sendMsgSetSLAAppList ( ) {
/* .locals 3 */
/* .line 608 */
android.os.Message .obtain ( );
/* .line 609 */
/* .local v0, "msg":Landroid/os/Message; */
/* const/16 v1, 0x68 */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 610 */
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
/* const-string/jumbo v2, "sendMsgSetSLAAppList" */
android.util.Log .d ( v1,v2 );
/* .line 611 */
v1 = this.mDataBaseHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 612 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
private void sendMsgStartCalc ( ) {
/* .locals 2 */
/* .line 257 */
final String v0 = "SLM-SRV-SLAAppLib"; // const-string v0, "SLM-SRV-SLAAppLib"
/* const-string/jumbo v1, "sendMsgStartCalc" */
android.util.Log .d ( v0,v1 );
/* .line 258 */
android.os.Message .obtain ( );
/* .line 259 */
/* .local v0, "msg":Landroid/os/Message; */
/* const/16 v1, 0xc9 */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 260 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 261 */
return;
} // .end method
private void sendMsgStopCalc ( ) {
/* .locals 2 */
/* .line 264 */
final String v0 = "SLM-SRV-SLAAppLib"; // const-string v0, "SLM-SRV-SLAAppLib"
/* const-string/jumbo v1, "sendMsgStopCalc" */
android.util.Log .d ( v0,v1 );
/* .line 265 */
android.os.Message .obtain ( );
/* .line 266 */
/* .local v0, "msg":Landroid/os/Message; */
/* const/16 v1, 0xcb */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 267 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 268 */
return;
} // .end method
private void sendMsgUpdateTrafficStat ( ) {
/* .locals 2 */
/* .line 1606 */
android.os.Message .obtain ( );
/* .line 1607 */
/* .local v0, "msg":Landroid/os/Message; */
/* const/16 v1, 0xcb */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 1608 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1609 */
return;
} // .end method
private void setDoubleDataUidToSlad ( ) {
/* .locals 4 */
/* .line 931 */
/* const-string/jumbo v0, "setDoubleDataUidToSlad" */
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
android.util.Log .i ( v1,v0 );
/* .line 932 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 934 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setDDUidList:" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mDoubleDataUidList;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v0 );
/* .line 935 */
v0 = this.mDoubleDataUidList;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 936 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
/* .line 938 */
} // :cond_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
final String v2 = "NULL"; // const-string v2, "NULL"
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 940 */
/* :catch_0 */
/* move-exception v0 */
/* .line 941 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Exception:"; // const-string v3, "Exception:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 942 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* .line 944 */
} // :cond_1
final String v0 = "mSlaService is null"; // const-string v0, "mSlaService is null"
android.util.Log .d ( v1,v0 );
/* .line 946 */
} // :goto_1
return;
} // .end method
private void setDoubleWifiUidToSlad ( ) {
/* .locals 9 */
/* .line 845 */
/* const-string/jumbo v0, "setDoubleWifiUidToSlad" */
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
android.util.Log .i ( v1,v0 );
/* .line 846 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 848 */
try { // :try_start_0
v0 = this.mContext;
/* .line 849 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "is_opened_dual_wifi"; // const-string v2, "is_opened_dual_wifi"
/* .line 848 */
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v2,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_0 */
} // :cond_0
/* move v2, v3 */
} // :goto_0
/* move v0, v2 */
/* .line 851 */
/* .local v0, "isSDKOpended":Z */
int v2 = 0; // const/4 v2, 0x0
/* .line 852 */
/* .local v2, "uidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v4 = " mSDKDoubleWifiBlackUidSets = "; // const-string v4, " mSDKDoubleWifiBlackUidSets = "
final String v5 = ","; // const-string v5, ","
final String v6 = "NULL"; // const-string v6, "NULL"
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 853 */
try { // :try_start_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v7, "setDWUidList isSDKOpend = true DoubleWifiUidList = " */
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mSDKDoubleWifiUidList;
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mSDKDoubleWifiBlackUidSets;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v3 );
/* .line 857 */
v3 = this.mSDKDoubleWifiUidList;
v3 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v3, :cond_2 */
/* .line 858 */
v3 = this.mSDKDoubleWifiUidList;
(( java.lang.String ) v3 ).split ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .stream ( v3 );
/* new-instance v4, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0; */
/* invoke-direct {v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0;-><init>()V */
/* .line 859 */
/* new-instance v4, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3; */
/* invoke-direct {v4}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;-><init>()V */
java.util.stream.Collectors .toCollection ( v4 );
/* check-cast v3, Ljava/util/List; */
/* move-object v2, v3 */
/* .line 861 */
/* invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->generateFinalWhiteList(Ljava/util/List;)Ljava/lang/String; */
/* .line 862 */
/* .local v3, "finalUids":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "setDWUidList isSDKOpend= true final DoubleWifiUidList = " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v4 );
/* .line 865 */
v4 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v4, :cond_1 */
/* .line 866 */
v4 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
/* .line 868 */
} // :cond_1
v4 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
/* .line 870 */
} // .end local v3 # "finalUids":Ljava/lang/String;
} // :goto_1
/* goto/16 :goto_4 */
/* .line 871 */
} // :cond_2
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
/* goto/16 :goto_4 */
/* .line 874 */
} // :cond_3
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "setDWUidList isSDKOpend= false mDoubleWifiUidList = " */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mDoubleWifiUidList;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " mSDKDoubleWifiUidList = "; // const-string v8, " mSDKDoubleWifiUidList = "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mSDKDoubleWifiUidList;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mSDKDoubleWifiBlackUidSets;
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v4 );
/* .line 878 */
v4 = this.mDoubleWifiUidList;
v4 = android.text.TextUtils .isEmpty ( v4 );
/* if-nez v4, :cond_4 */
/* .line 879 */
v4 = this.mDoubleWifiUidList;
(( java.lang.String ) v4 ).split ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .stream ( v4 );
/* new-instance v7, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0; */
/* invoke-direct {v7}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0;-><init>()V */
/* .line 880 */
/* new-instance v7, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3; */
/* invoke-direct {v7}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;-><init>()V */
java.util.stream.Collectors .toCollection ( v7 );
/* check-cast v4, Ljava/util/List; */
/* move-object v2, v4 */
/* .line 882 */
} // :cond_4
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
/* move-object v2, v4 */
/* .line 885 */
} // :goto_2
v4 = this.mSDKDoubleWifiUidList;
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 886 */
(( java.lang.String ) v4 ).split ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 887 */
/* .local v4, "sdkUids":[Ljava/lang/String; */
/* array-length v5, v4 */
} // :goto_3
/* if-ge v3, v5, :cond_6 */
/* aget-object v7, v4, v3 */
/* .line 888 */
v8 = /* .local v7, "sdkUid":Ljava/lang/String; */
/* if-nez v8, :cond_5 */
/* .line 889 */
/* .line 887 */
} // .end local v7 # "sdkUid":Ljava/lang/String;
} // :cond_5
/* add-int/lit8 v3, v3, 0x1 */
/* .line 893 */
} // .end local v4 # "sdkUids":[Ljava/lang/String;
} // :cond_6
/* invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->generateFinalWhiteList(Ljava/util/List;)Ljava/lang/String; */
/* .line 894 */
/* .restart local v3 # "finalUids":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "setDWUidList isSDKOpend= false final DoubleWifiUidList = " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v4 );
/* .line 897 */
v4 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v4, :cond_7 */
/* .line 898 */
v4 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
/* .line 900 */
} // :cond_7
v4 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 903 */
} // .end local v0 # "isSDKOpended":Z
} // .end local v2 # "uidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v3 # "finalUids":Ljava/lang/String;
/* :catch_0 */
/* move-exception v0 */
/* .line 904 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Exception:"; // const-string v3, "Exception:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 905 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_4
/* .line 907 */
} // :cond_8
final String v0 = "mSlaService is null"; // const-string v0, "mSlaService is null"
android.util.Log .d ( v1,v0 );
/* .line 909 */
} // :goto_5
return;
} // .end method
private static void setSLAMode ( ) {
/* .locals 4 */
/* .line 789 */
/* const-string/jumbo v0, "setSLAMode" */
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
android.util.Log .i ( v1,v0 );
/* .line 790 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 792 */
try { // :try_start_0
/* if-nez v0, :cond_0 */
/* .line 793 */
/* const-string/jumbo v0, "setSLAMode:1" */
android.util.Log .i ( v1,v0 );
/* .line 794 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
final String v2 = "1"; // const-string v2, "1"
/* .line 796 */
} // :cond_0
/* const-string/jumbo v0, "setSLAMode:0" */
android.util.Log .i ( v1,v0 );
/* .line 797 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
final String v2 = "0"; // const-string v2, "0"
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 799 */
/* :catch_0 */
/* move-exception v0 */
/* .line 800 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Exception:"; // const-string v3, "Exception:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 801 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* .line 803 */
} // :cond_1
final String v0 = "mSlaService is null"; // const-string v0, "mSlaService is null"
android.util.Log .d ( v1,v0 );
/* .line 805 */
} // :goto_1
return;
} // .end method
private void setUidWhiteListToSlad ( ) {
/* .locals 9 */
/* .line 808 */
/* const-string/jumbo v0, "setUidWhiteListToSlad" */
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
android.util.Log .i ( v1,v0 );
/* .line 809 */
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) p0 ).getLinkTurboWhiteList ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;
/* .line 810 */
/* .local v0, "uidlist":Ljava/lang/String; */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 812 */
final String v3 = "NULL"; // const-string v3, "NULL"
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 814 */
try { // :try_start_0
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 815 */
/* .local v2, "pm":Landroid/content/pm/PackageManager; */
final String v4 = "com.android.providers.downloads.ui"; // const-string v4, "com.android.providers.downloads.ui"
int v5 = 0; // const/4 v5, 0x0
(( android.content.pm.PackageManager ) v2 ).getApplicationInfo ( v4, v5 ); // invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 816 */
/* .local v4, "downloadui_uid":I */
final String v6 = "com.android.providers.downloads"; // const-string v6, "com.android.providers.downloads"
(( android.content.pm.PackageManager ) v2 ).getApplicationInfo ( v6, v5 ); // invoke-virtual {v2, v6, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 817 */
/* .local v5, "download_uid":I */
final String v6 = ","; // const-string v6, ","
int v7 = -1; // const/4 v7, -0x1
if ( v4 != null) { // if-eqz v4, :cond_0
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 818 */
try { // :try_start_1
java.lang.Integer .toString ( v5 );
v8 = (( java.lang.String ) v0 ).indexOf ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* if-ne v8, v7, :cond_0 */
/* .line 819 */
java.lang.Integer .toString ( v4 );
v8 = (( java.lang.String ) v0 ).indexOf ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* if-eq v8, v7, :cond_0 */
/* .line 820 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toString ( v5 );
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v0, v6 */
/* .line 822 */
} // :cond_0
if ( v4 != null) { // if-eqz v4, :cond_1
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 823 */
java.lang.Integer .toString ( v5 );
v8 = (( java.lang.String ) v0 ).indexOf ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* if-eq v8, v7, :cond_1 */
/* .line 824 */
java.lang.Integer .toString ( v4 );
v8 = (( java.lang.String ) v0 ).indexOf ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* if-ne v8, v7, :cond_1 */
/* .line 825 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.Integer .toString ( v5 );
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v7 = ""; // const-string v7, ""
(( java.lang.String ) v0 ).replaceFirst ( v6, v7 ); // invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* move-object v0, v6 */
/* .line 827 */
} // :cond_1
} // :goto_0
v6 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* if-nez v6, :cond_2 */
/* .line 828 */
v6 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
/* .line 829 */
return;
/* .line 831 */
} // :cond_2
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "setSLAUidList:" */
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v3 );
/* .line 832 */
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
/* .line 833 */
} // .end local v2 # "pm":Landroid/content/pm/PackageManager;
} // .end local v4 # "downloadui_uid":I
} // .end local v5 # "download_uid":I
/* .line 836 */
/* :catch_0 */
/* move-exception v2 */
/* .line 834 */
} // :cond_3
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 837 */
/* .local v2, "e":Ljava/lang/Exception; */
} // :goto_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Exception:"; // const-string v4, "Exception:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v3 );
/* .line 838 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_2
/* .line 840 */
} // :cond_4
final String v2 = "mSlaService is null"; // const-string v2, "mSlaService is null"
android.util.Log .d ( v1,v2 );
/* .line 842 */
} // :goto_3
return;
} // .end method
private void storeSLAAppUpgrade ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 1507 */
java.util.Calendar .getInstance ( );
(( java.util.Calendar ) v0 ).getTimeInMillis ( ); // invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v0 */
/* .line 1509 */
/* .local v0, "now":J */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->refreshSLAAppUpgradeList()V */
/* .line 1511 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppUpgradeList;
v2 = (( java.util.HashMap ) v2 ).containsKey ( p1 ); // invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1512 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppUpgradeList;
java.lang.Long .valueOf ( v0,v1 );
(( java.util.HashMap ) v2 ).replace ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1514 */
} // :cond_0
v2 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppUpgradeList;
java.lang.Long .valueOf ( v0,v1 );
(( java.util.HashMap ) v2 ).put ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1517 */
} // :goto_0
return;
} // .end method
private void syncSLAAppFromDB ( ) {
/* .locals 5 */
/* .line 1019 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1020 */
/* .local v0, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;" */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v1, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->dbGetCursorSLAApp(Ljava/lang/String;[Ljava/lang/String;)Ldatabase/SlaDbSchema/SlaCursorWrapper; */
/* .line 1022 */
/* .local v1, "cursor":Ldatabase/SlaDbSchema/SlaCursorWrapper; */
try { // :try_start_0
/* .line 1023 */
(( database.SlaDbSchema.SlaCursorWrapper ) v1 ).moveToFirst ( ); // invoke-virtual {v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->moveToFirst()Z
/* .line 1024 */
} // :goto_0
v2 = (( database.SlaDbSchema.SlaCursorWrapper ) v1 ).isAfterLast ( ); // invoke-virtual {v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->isAfterLast()Z
/* if-nez v2, :cond_0 */
/* .line 1025 */
(( database.SlaDbSchema.SlaCursorWrapper ) v1 ).getSLAApp ( ); // invoke-virtual {v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getSLAApp()Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
/* .line 1026 */
(( database.SlaDbSchema.SlaCursorWrapper ) v1 ).moveToNext ( ); // invoke-virtual {v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->moveToNext()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1029 */
} // :cond_0
v2 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
/* monitor-enter v2 */
/* .line 1030 */
try { // :try_start_1
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
(( java.util.ArrayList ) v3 ).clear ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V
/* .line 1031 */
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
(( java.util.ArrayList ) v3 ).addAll ( v0 ); // invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 1032 */
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1033 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->printUidLists()V */
/* .line 1034 */
(( database.SlaDbSchema.SlaCursorWrapper ) v1 ).close ( ); // invoke-virtual {v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->close()V
/* .line 1035 */
/* nop */
/* .line 1036 */
return;
/* .line 1032 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_2
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v3 */
/* .line 1029 */
/* :catchall_1 */
/* move-exception v2 */
v3 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
/* monitor-enter v3 */
/* .line 1030 */
try { // :try_start_3
v4 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
(( java.util.ArrayList ) v4 ).clear ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V
/* .line 1031 */
v4 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mAppLists;
(( java.util.ArrayList ) v4 ).addAll ( v0 ); // invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 1032 */
/* monitor-exit v3 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
/* .line 1033 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->printUidLists()V */
/* .line 1034 */
(( database.SlaDbSchema.SlaCursorWrapper ) v1 ).close ( ); // invoke-virtual {v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->close()V
/* .line 1035 */
/* throw v2 */
/* .line 1032 */
/* :catchall_2 */
/* move-exception v2 */
try { // :try_start_4
/* monitor-exit v3 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* throw v2 */
} // .end method
/* # virtual methods */
public java.util.List getLinkTurboAppsTraffic ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 504 */
int v0 = 0; // const/4 v0, 0x0
/* .line 505 */
/* .local v0, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;" */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->cloneSLAApp()Ljava/util/List; */
v1 = /* .line 507 */
/* if-nez v1, :cond_0 */
/* .line 508 */
int v1 = 0; // const/4 v1, 0x0
/* .line 511 */
} // :cond_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 512 */
/* .local v1, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = } // :goto_0
/* if-ge v2, v3, :cond_1 */
/* .line 514 */
/* check-cast v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* .line 512 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 517 */
} // .end local v2 # "i":I
} // :cond_1
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgUpdateTrafficStat()V */
/* .line 518 */
} // .end method
java.util.List getLinkTurboDefaultPn ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 489 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 490 */
/* .local v0, "defaultPn":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = v1 = this.mSLAAppDefaultPN;
/* if-nez v1, :cond_0 */
/* .line 491 */
v1 = this.mSLAAppDefaultPN;
/* .line 493 */
} // :cond_0
v1 = v1 = this.mSLSGameAppPN;
/* if-nez v1, :cond_1 */
/* .line 494 */
v1 = this.mSLSGameAppPN;
/* .line 496 */
v1 = } // :cond_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 497 */
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
final String v2 = "LinkTurboDefaultPn is empty"; // const-string v2, "LinkTurboDefaultPn is empty"
android.util.Log .e ( v1,v2 );
/* .line 499 */
} // :cond_2
} // .end method
public java.lang.String getLinkTurboWhiteList ( ) {
/* .locals 7 */
/* .line 459 */
final String v0 = ""; // const-string v0, ""
/* .line 460 */
/* .local v0, "uidlist":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .line 462 */
/* .local v1, "i":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 463 */
/* .local v2, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;" */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->cloneSLAApp()Ljava/util/List; */
v3 = /* .line 464 */
int v4 = 0; // const/4 v4, 0x0
/* if-nez v3, :cond_0 */
/* .line 465 */
/* .line 468 */
} // :cond_0
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 469 */
/* .local v3, "uids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v1 = 0; // const/4 v1, 0x0
v5 = } // :goto_0
/* if-ge v1, v5, :cond_2 */
/* .line 470 */
/* check-cast v5, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
v5 = (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v5 ).getState ( ); // invoke-virtual {v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getState()Z
int v6 = 1; // const/4 v6, 0x1
/* if-ne v5, v6, :cond_1 */
/* .line 471 */
/* check-cast v5, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
(( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v5 ).getUid ( ); // invoke-virtual {v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->getUid()Ljava/lang/String;
/* .line 469 */
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 475 */
v5 = } // :cond_2
/* if-nez v5, :cond_3 */
/* .line 476 */
/* .line 479 */
} // :cond_3
int v1 = 0; // const/4 v1, 0x0
v4 = } // :goto_1
/* if-ge v1, v4, :cond_4 */
/* .line 480 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v5, Ljava/lang/String; */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ","; // const-string v5, ","
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 479 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 483 */
} // :cond_4
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "getLinkTurboWhiteList:"; // const-string v5, "getLinkTurboWhiteList:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "SLM-SRV-SLAAppLib"; // const-string v5, "SLM-SRV-SLAAppLib"
android.util.Log .d ( v5,v4 );
/* .line 484 */
} // .end method
public Boolean sendMsgAddSLAUid ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 536 */
android.os.Message .obtain ( );
/* .line 539 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->addSLAApp(Ljava/lang/String;)I */
/* .line 540 */
/* .local v1, "ret":I */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
/* .line 541 */
/* const/16 v3, 0x64 */
/* iput v3, v0, Landroid/os/Message;->what:I */
/* .line 542 */
final String v3 = "SLM-SRV-SLAAppLib"; // const-string v3, "SLM-SRV-SLAAppLib"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "MSG_DB_SLAAPP_ADD:"; // const-string v5, "MSG_DB_SLAAPP_ADD:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 543 */
} // :cond_0
/* if-nez v1, :cond_1 */
/* .line 544 */
/* const/16 v3, 0x66 */
/* iput v3, v0, Landroid/os/Message;->what:I */
/* .line 545 */
final String v3 = "SLM-SRV-SLAAppLib"; // const-string v3, "SLM-SRV-SLAAppLib"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "MSG_DB_SLAAPP_UPDATE:"; // const-string v5, "MSG_DB_SLAAPP_UPDATE:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 549 */
} // :cond_1
} // :goto_0
int v3 = 0; // const/4 v3, 0x0
/* .line 550 */
/* .local v3, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
v4 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
/* monitor-enter v4 */
/* .line 551 */
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* move-object v3, v5 */
/* .line 552 */
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 553 */
this.obj = v3;
/* .line 554 */
v4 = this.mDataBaseHandler;
(( android.os.Handler ) v4 ).sendMessage ( v0 ); // invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 557 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v5, "update_uidlist_to_sla" */
/* .line 558 */
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) p0 ).getLinkTurboWhiteList ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;
/* .line 557 */
android.provider.Settings$System .putString ( v4,v5,v6 );
/* .line 559 */
/* .line 552 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
public Boolean sendMsgDelSLAUid ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 564 */
android.os.Message .obtain ( );
/* .line 567 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->deleteSLAApp(Ljava/lang/String;)I */
/* .line 568 */
/* .local v1, "ret":I */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
/* .line 569 */
/* const/16 v3, 0x65 */
/* iput v3, v0, Landroid/os/Message;->what:I */
/* .line 570 */
final String v3 = "SLM-SRV-SLAAppLib"; // const-string v3, "SLM-SRV-SLAAppLib"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "MSG_DB_SLAAPP_DEL:"; // const-string v5, "MSG_DB_SLAAPP_DEL:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 576 */
int v3 = 0; // const/4 v3, 0x0
/* .line 577 */
/* .local v3, "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
v4 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
/* monitor-enter v4 */
/* .line 578 */
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getSLAAppByUid(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
/* move-object v3, v5 */
/* .line 579 */
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 580 */
this.obj = v3;
/* .line 581 */
v4 = this.mDataBaseHandler;
(( android.os.Handler ) v4 ).sendMessage ( v0 ); // invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 584 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v5, "update_uidlist_to_sla" */
/* .line 585 */
(( com.xiaomi.NetworkBoost.slaservice.SLAAppLib ) p0 ).getLinkTurboWhiteList ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->getLinkTurboWhiteList()Ljava/lang/String;
/* .line 584 */
android.provider.Settings$System .putString ( v4,v5,v6 );
/* .line 586 */
/* .line 579 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
/* .line 572 */
} // .end local v3 # "app":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // .end method
public Boolean sendMsgDoubleDataUid ( ) {
/* .locals 4 */
/* .line 599 */
android.os.Message .obtain ( );
/* .line 600 */
/* .local v0, "msg":Landroid/os/Message; */
/* const/16 v1, 0x69 */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 601 */
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
/* const-string/jumbo v2, "sendMsgDoubleDataUid" */
android.util.Log .d ( v1,v2 );
/* .line 602 */
v1 = this.mDataBaseHandler;
/* const-wide/16 v2, 0x12c */
(( android.os.Handler ) v1 ).sendMessageAtTime ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z
/* .line 603 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
public Boolean sendMsgDoubleWifiUid ( ) {
/* .locals 4 */
/* .line 591 */
android.os.Message .obtain ( );
/* .line 592 */
/* .local v0, "msg":Landroid/os/Message; */
/* const/16 v1, 0x67 */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 593 */
final String v1 = "SLM-SRV-SLAAppLib"; // const-string v1, "SLM-SRV-SLAAppLib"
/* const-string/jumbo v2, "sendMsgDoubleWifiUid" */
android.util.Log .d ( v1,v2 );
/* .line 594 */
v1 = this.mDataBaseHandler;
/* const-wide/16 v2, 0x12c */
(( android.os.Handler ) v1 ).sendMessageAtTime ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z
/* .line 595 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
public void setDDSLAMode ( ) {
/* .locals 4 */
/* .line 1597 */
final String v0 = "SLM-SRV-SLAAppLib"; // const-string v0, "SLM-SRV-SLAAppLib"
try { // :try_start_0
/* const-string/jumbo v1, "setSLAMode: SLA_MODE_CONCURRENT" */
android.util.Log .i ( v0,v1 );
/* .line 1598 */
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSlaService;
final String v2 = "0"; // const-string v2, "0"
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1601 */
/* .line 1599 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1600 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Exception:"; // const-string v3, "Exception:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 1602 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public Boolean setDoubleWifiWhiteList ( Integer p0, java.lang.String p1, java.lang.String p2, Boolean p3 ) {
/* .locals 11 */
/* .param p1, "type" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "uidList" # Ljava/lang/String; */
/* .param p4, "isOperateBlacklist" # Z */
/* .line 682 */
final String v0 = "SLM-SRV-SLAAppLib"; // const-string v0, "SLM-SRV-SLAAppLib"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setDoubleWifiWhiteList type = " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " packageName = "; // const-string v2, " packageName = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " uidList = "; // const-string v2, " uidList = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 683 */
final String v0 = "SLM-SRV-SLAAppLib"; // const-string v0, "SLM-SRV-SLAAppLib"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "pre setDoubleWifiWhiteList mSDKDoubleWifiUidList = "; // const-string v2, "pre setDoubleWifiWhiteList mSDKDoubleWifiUidList = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mSDKDoubleWifiUidList;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " mSDKDoubleWifiAppPN = "; // const-string v2, " mSDKDoubleWifiAppPN = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mSDKDoubleWifiAppPN;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " mSDKDoubleWifiBlackUidSets = "; // const-string v2, " mSDKDoubleWifiBlackUidSets = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mSDKDoubleWifiBlackUidSets;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 687 */
int v0 = 0; // const/4 v0, 0x0
if ( p2 != null) { // if-eqz p2, :cond_6
/* if-nez p3, :cond_0 */
/* goto/16 :goto_3 */
/* .line 690 */
} // :cond_0
v1 = com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mSLAAppLibLock;
/* monitor-enter v1 */
/* .line 691 */
try { // :try_start_0
v2 = this.mSDKDoubleWifiUidList;
/* if-nez v2, :cond_1 */
/* .line 692 */
final String v2 = ""; // const-string v2, ""
this.mSDKDoubleWifiUidList = v2;
/* .line 694 */
} // :cond_1
v2 = this.mSDKDoubleWifiAppPN;
/* if-nez v2, :cond_2 */
/* .line 695 */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
this.mSDKDoubleWifiAppPN = v2;
/* .line 697 */
} // :cond_2
v2 = this.mSDKDoubleWifiUidList;
final String v3 = ","; // const-string v3, ","
(( java.lang.String ) v2 ).split ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .stream ( v2 );
/* new-instance v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0; */
/* invoke-direct {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0;-><init>()V */
/* .line 698 */
/* new-instance v3, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3; */
/* invoke-direct {v3}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;-><init>()V */
java.util.stream.Collectors .toCollection ( v3 );
/* move-object v4, v2 */
/* check-cast v4, Ljava/util/List; */
/* .line 700 */
/* .local v4, "whiteUidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v2 = 1; // const/4 v2, 0x1
/* packed-switch p1, :pswitch_data_0 */
/* goto/16 :goto_2 */
/* .line 726 */
/* :pswitch_0 */
int v0 = 0; // const/4 v0, 0x0
this.mSDKDoubleWifiUidList = v0;
/* .line 727 */
this.mSDKDoubleWifiAppPN = v0;
/* .line 728 */
/* goto/16 :goto_2 */
/* .line 716 */
/* :pswitch_1 */
final String v3 = ","; // const-string v3, ","
(( java.lang.String ) p3 ).split ( v3 ); // invoke-virtual {p3, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 717 */
/* .local v3, "uids":[Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* array-length v6, v3 */
/* if-ge v5, v6, :cond_5 */
/* .line 718 */
v6 = this.mSDKDoubleWifiBlackUidSets;
if ( v6 != null) { // if-eqz v6, :cond_3
/* aget-object v7, v3, v5 */
v6 = /* .line 719 */
if ( v6 != null) { // if-eqz v6, :cond_3
/* move v6, v2 */
} // :cond_3
/* move v6, v0 */
/* .line 720 */
/* .local v6, "isInBlackSet":Z */
} // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 721 */
/* monitor-exit v1 */
/* .line 717 */
} // .end local v6 # "isInBlackSet":Z
} // :cond_4
/* add-int/lit8 v5, v5, 0x1 */
/* .line 724 */
} // .end local v5 # "i":I
} // :cond_5
/* monitor-exit v1 */
/* .line 708 */
} // .end local v3 # "uids":[Ljava/lang/String;
/* :pswitch_2 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p2 ).split ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .stream ( v0 );
/* new-instance v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0; */
/* invoke-direct {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda0;-><init>()V */
/* .line 709 */
/* new-instance v3, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda1; */
/* invoke-direct {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$$ExternalSyntheticLambda1;-><init>()V */
java.util.stream.Collectors .toCollection ( v3 );
/* check-cast v0, Ljava/util/Set; */
/* .line 711 */
/* .local v0, "packagesSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
this.mSDKDoubleWifiAppPN = v0;
/* .line 713 */
this.mSDKDoubleWifiUidList = p3;
/* .line 714 */
/* .line 702 */
} // .end local v0 # "packagesSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* :pswitch_3 */
int v5 = 1; // const/4 v5, 0x1
/* move-object v3, p0 */
/* move-object v6, p2 */
/* move-object v7, p3 */
/* move v8, p4 */
/* invoke-direct/range {v3 ..v8}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->addOrRemoveWhiteList(Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;Z)V */
/* .line 703 */
/* .line 705 */
/* :pswitch_4 */
int v7 = 0; // const/4 v7, 0x0
/* move-object v5, p0 */
/* move-object v6, v4 */
/* move-object v8, p2 */
/* move-object v9, p3 */
/* move v10, p4 */
/* invoke-direct/range {v5 ..v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->addOrRemoveWhiteList(Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;Z)V */
/* .line 706 */
/* nop */
/* .line 732 */
} // .end local v4 # "whiteUidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :goto_2
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 734 */
final String v0 = "SLM-SRV-SLAAppLib"; // const-string v0, "SLM-SRV-SLAAppLib"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "after setDoubleWifiWhiteList mSDKDoubleWifiUidList = "; // const-string v3, "after setDoubleWifiWhiteList mSDKDoubleWifiUidList = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mSDKDoubleWifiUidList;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " mSDKDoubleWifiAppPN = "; // const-string v3, " mSDKDoubleWifiAppPN = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mSDKDoubleWifiAppPN;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " mSDKDoubleWifiBlackUidSets = "; // const-string v3, " mSDKDoubleWifiBlackUidSets = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mSDKDoubleWifiBlackUidSets;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 738 */
/* .line 732 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
/* .line 688 */
} // :cond_6
} // :goto_3
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void setSlaService ( vendor.qti.sla.service.V1_0.ISlaService p0 ) {
/* .locals 0 */
/* .param p1, "service" # Lvendor/qti/sla/service/V1_0/ISlaService; */
/* .line 252 */
/* .line 253 */
return;
} // .end method
public void startSladHandle ( ) {
/* .locals 1 */
/* .line 522 */
int v0 = 1; // const/4 v0, 0x1
com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mIsSladRunning = (v0!= 0);
/* .line 523 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgStartCalc()V */
/* .line 525 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->setUidWhiteListToSlad()V */
/* .line 526 */
com.xiaomi.NetworkBoost.slaservice.SLAAppLib .setSLAMode ( );
/* .line 527 */
return;
} // .end method
public void stopSladHandle ( ) {
/* .locals 1 */
/* .line 530 */
int v0 = 0; // const/4 v0, 0x0
com.xiaomi.NetworkBoost.slaservice.SLAAppLib.mIsSladRunning = (v0!= 0);
/* .line 531 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->sendMsgStopCalc()V */
/* .line 532 */
return;
} // .end method
