class com.xiaomi.NetworkBoost.slaservice.SLSAppLib$InternalHandler extends android.os.Handler {
	 /* .source "SLSAppLib.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "InternalHandler" */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.SLSAppLib this$0; //synthetic
/* # direct methods */
public com.xiaomi.NetworkBoost.slaservice.SLSAppLib$InternalHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 414 */
this.this$0 = p1;
/* .line 415 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 416 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 419 */
/* iget v0, p1, Landroid/os/Message;->what:I */
final String v1 = "SLM-SRV-SLSAppLib"; // const-string v1, "SLM-SRV-SLSAppLib"
/* packed-switch v0, :pswitch_data_0 */
/* .line 426 */
/* :pswitch_0 */
final String v0 = "EVENT_STOP_VOIP"; // const-string v0, "EVENT_STOP_VOIP"
android.util.Log .i ( v1,v0 );
/* .line 427 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$msetSLSVoIPStop ( v0 );
/* .line 428 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$msetPowerMgrVoIPInfo ( v0,v1 );
/* .line 429 */
/* .line 421 */
/* :pswitch_1 */
final String v0 = "EVENT_START_VOIP"; // const-string v0, "EVENT_START_VOIP"
android.util.Log .i ( v1,v0 );
/* .line 422 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$msetSLSVoIPStart ( v0 );
/* .line 423 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.xiaomi.NetworkBoost.slaservice.SLSAppLib .-$$Nest$msetPowerMgrVoIPInfo ( v0,v1 );
/* .line 424 */
/* nop */
/* .line 433 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x64 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
