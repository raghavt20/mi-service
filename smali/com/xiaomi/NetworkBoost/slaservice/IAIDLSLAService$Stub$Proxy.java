class com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService$Stub$Proxy implements com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService {
	 /* .source "IAIDLSLAService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private android.os.IBinder mRemote;
/* # direct methods */
 com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService$Stub$Proxy ( ) {
/* .locals 0 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 144 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 145 */
this.mRemote = p1;
/* .line 146 */
return;
} // .end method
/* # virtual methods */
public Boolean addUidToLinkTurboWhiteList ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 157 */
(( com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 158 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 161 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"; // const-string v2, "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 162 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 163 */
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 164 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 165 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 168 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 169 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 170 */
/* nop */
/* .line 171 */
/* .line 168 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 169 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 170 */
/* throw v2 */
} // .end method
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 149 */
v0 = this.mRemote;
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 153 */
final String v0 = "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"; // const-string v0, "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"
} // .end method
public java.util.List getLinkTurboAppsTraffic ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 210 */
(( com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 211 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 214 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"; // const-string v2, "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 215 */
v2 = this.mRemote;
int v3 = 4; // const/4 v3, 0x4
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 216 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 217 */
v3 = com.xiaomi.NetworkBoost.slaservice.SLAApp.CREATOR;
(( android.os.Parcel ) v1 ).createTypedArrayList ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v2, v3 */
/* .line 220 */
/* .local v2, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;" */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 221 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 222 */
/* nop */
/* .line 223 */
/* .line 220 */
} // .end local v2 # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;"
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 221 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 222 */
/* throw v2 */
} // .end method
public java.util.List getLinkTurboDefaultPn ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 245 */
(( com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 246 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 249 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"; // const-string v2, "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 250 */
v2 = this.mRemote;
int v3 = 6; // const/4 v3, 0x6
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 251 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 252 */
(( android.os.Parcel ) v1 ).createStringArrayList ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v2, v3 */
/* .line 255 */
/* .local v2, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 256 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 257 */
/* nop */
/* .line 258 */
/* .line 255 */
} // .end local v2 # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 256 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 257 */
/* throw v2 */
} // .end method
public java.lang.String getLinkTurboWhiteList ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 193 */
(( com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 194 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 197 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"; // const-string v2, "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 198 */
v2 = this.mRemote;
int v3 = 3; // const/4 v3, 0x3
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 199 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 200 */
(( android.os.Parcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v2, v3 */
/* .line 203 */
/* .local v2, "_result":Ljava/lang/String; */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 204 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 205 */
/* nop */
/* .line 206 */
/* .line 203 */
} // .end local v2 # "_result":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 204 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 205 */
/* throw v2 */
} // .end method
public Boolean removeUidInLinkTurboWhiteList ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 175 */
(( com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 176 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 179 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"; // const-string v2, "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 180 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 181 */
v2 = this.mRemote;
int v3 = 2; // const/4 v3, 0x2
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 182 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 183 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 186 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 187 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 188 */
/* nop */
/* .line 189 */
/* .line 186 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 187 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 188 */
/* throw v2 */
} // .end method
public Boolean setLinkTurboEnable ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "enable" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 227 */
(( com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 228 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 231 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"; // const-string v2, "com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 232 */
(( android.os.Parcel ) v0 ).writeBoolean ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 233 */
v2 = this.mRemote;
int v3 = 5; // const/4 v3, 0x5
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 234 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 235 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 238 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 239 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 240 */
/* nop */
/* .line 241 */
/* .line 238 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 239 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 240 */
/* throw v2 */
} // .end method
