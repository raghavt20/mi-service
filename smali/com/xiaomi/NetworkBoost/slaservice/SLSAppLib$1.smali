.class Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;
.super Ljava/lang/Object;
.source "SLSAppLib.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    .line 350
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioChanged(I)V
    .locals 5
    .param p1, "mode"    # I

    .line 393
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initBroadcastReceiver mode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLSAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    const/4 v0, 0x3

    const/16 v1, 0x65

    const/16 v2, 0x64

    if-ne p1, v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 398
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 399
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v3, 0x2710

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 401
    :cond_0
    if-nez p1, :cond_1

    .line 404
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 405
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 406
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 409
    :cond_1
    :goto_0
    return-void
.end method

.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 3
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;

    .line 354
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$sfgetmSlaAppList()Ljava/util/HashSet;

    move-result-object v0

    iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$sfgetmSlsAppList()Ljava/util/HashSet;

    move-result-object v0

    iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 356
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$msetSLSGameStart(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;Ljava/lang/String;)Z

    .line 357
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    const/4 v1, 0x1

    iget v2, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-static {v0, v1, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$msetPowerMgrGameInfo(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;ZI)V

    goto/16 :goto_0

    .line 360
    :cond_0
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$sfgetmSlaAppList()Ljava/util/HashSet;

    move-result-object v0

    iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$sfgetmSlsAppList()Ljava/util/HashSet;

    move-result-object v0

    iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    .line 361
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 362
    :cond_1
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$sfgetmSlaAppList()Ljava/util/HashSet;

    move-result-object v0

    iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$sfgetmSlsAppList()Ljava/util/HashSet;

    move-result-object v0

    iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I

    .line 363
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 364
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$sfgetmSLSUid()I

    move-result v0

    iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I

    const-string v2, "SLM-SRV-SLSAppLib"

    if-ne v0, v1, :cond_2

    .line 365
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onForegroundInfoChanged stop game uid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$msetSLSGameStop(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;Ljava/lang/String;)Z

    .line 369
    :cond_2
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$fgetpowerMgrGameUid(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)I

    move-result v0

    iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I

    if-ne v0, v1, :cond_3

    .line 370
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onForegroundInfoChanged reset gameinfo uid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    const/4 v1, 0x0

    iget v2, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I

    invoke-static {v0, v1, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$msetPowerMgrGameInfo(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;ZI)V

    .line 375
    :cond_3
    :goto_0
    return-void
.end method

.method public onUidGone(IZ)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "disabled"    # Z

    .line 380
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$sfgetmSLSUid()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 381
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onUidGone uid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SLM-SRV-SLSAppLib"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$msetSLSGameStop(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;Ljava/lang/String;)Z

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$fgetpowerMgrGameUid(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;)I

    move-result v0

    if-ne v0, p1, :cond_1

    .line 386
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;->-$$Nest$msetPowerMgrGameInfo(Lcom/xiaomi/NetworkBoost/slaservice/SLSAppLib;ZI)V

    .line 388
    :cond_1
    return-void
.end method
