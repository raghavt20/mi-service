public class com.xiaomi.NetworkBoost.slaservice.SLAToast {
	 /* .source "SLAToast.java" */
	 /* # static fields */
	 private static final Long DELAY_TIME;
	 private static java.lang.String TAG;
	 private static final Integer TOAST_LOOPER;
	 private static final Integer TOAST_TIME;
	 private static java.util.HashSet mApps;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashSet<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static com.xiaomi.NetworkBoost.slaservice.SLAAppLib mSLAAppLib;
/* # instance fields */
private Boolean isLinkTurboEnable;
private com.xiaomi.NetworkBoost.StatusManager$IAppStatusListener mAppStatusListener;
private android.content.Context mContext;
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
private com.xiaomi.NetworkBoost.StatusManager mStatusManager;
/* # direct methods */
static Boolean -$$Nest$fgetisLinkTurboEnable ( com.xiaomi.NetworkBoost.slaservice.SLAToast p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->isLinkTurboEnable:Z */
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.xiaomi.NetworkBoost.slaservice.SLAToast p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.xiaomi.NetworkBoost.slaservice.SLAToast p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAToast.TAG;
} // .end method
static java.util.HashSet -$$Nest$sfgetmApps ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAToast.mApps;
} // .end method
static com.xiaomi.NetworkBoost.slaservice.SLAToast ( ) {
/* .locals 1 */
/* .line 26 */
final String v0 = "SLM-SRV-SLAToast"; // const-string v0, "SLM-SRV-SLAToast"
return;
} // .end method
public com.xiaomi.NetworkBoost.slaservice.SLAToast ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 64 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 38 */
int v0 = 0; // const/4 v0, 0x0
this.mStatusManager = v0;
/* .line 40 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$1; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$1;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;)V */
this.mAppStatusListener = v0;
/* .line 65 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAToast.TAG;
final String v1 = "SLAToast"; // const-string v1, "SLAToast"
android.util.Log .i ( v0,v1 );
/* .line 66 */
this.mContext = p1;
/* .line 67 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 68 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->initSlaToastHandler()V */
/* .line 69 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->registerAppStatusListener()V */
/* .line 70 */
return;
} // .end method
private void initSlaToastHandler ( ) {
/* .locals 2 */
/* .line 82 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "SlaToastHandler"; // const-string v1, "SlaToastHandler"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 83 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 84 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$2; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$2;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 100 */
return;
} // .end method
private void registerAppStatusListener ( ) {
/* .locals 4 */
/* .line 74 */
try { // :try_start_0
	 v0 = this.mContext;
	 com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
	 this.mStatusManager = v0;
	 /* .line 75 */
	 v1 = this.mAppStatusListener;
	 (( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerAppStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerAppStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;)V
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 78 */
	 /* .line 76 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 77 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 v1 = com.xiaomi.NetworkBoost.slaservice.SLAToast.TAG;
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "Exception:"; // const-string v3, "Exception:"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .e ( v1,v2 );
	 /* .line 79 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public static void setLinkTurboUidList ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "uidList" # Ljava/lang/String; */
/* .line 107 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAToast.mApps;
(( java.util.HashSet ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
/* .line 108 */
/* if-nez p0, :cond_0 */
/* .line 109 */
return;
/* .line 112 */
} // :cond_0
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p0 ).split ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 113 */
/* .local v0, "temp":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_1 */
/* .line 114 */
v2 = com.xiaomi.NetworkBoost.slaservice.SLAToast.mApps;
/* aget-object v3, v0, v1 */
(( java.util.HashSet ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 113 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 116 */
} // .end local v1 # "i":I
} // :cond_1
return;
} // .end method
/* # virtual methods */
public android.os.HandlerThread getSLAToastHandlerThread ( ) {
/* .locals 1 */
/* .line 119 */
v0 = this.mHandlerThread;
} // .end method
public void setLinkTurboStatus ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 103 */
/* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->isLinkTurboEnable:Z */
/* .line 104 */
return;
} // .end method
