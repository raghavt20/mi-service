.class Lcom/xiaomi/NetworkBoost/slaservice/SLAService$SlaServiceDeathRecipient;
.super Ljava/lang/Object;
.source "SLAService.java"

# interfaces
.implements Landroid/os/IHwBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SlaServiceDeathRecipient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 478
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$SlaServiceDeathRecipient;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public serviceDied(J)V
    .locals 4
    .param p1, "cookie"    # J

    .line 481
    const-string v0, "SLM-SRV-SLAService"

    const-string v1, "HAL service died"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$SlaServiceDeathRecipient;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$SlaServiceDeathRecipient;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x67

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 484
    return-void
.end method
