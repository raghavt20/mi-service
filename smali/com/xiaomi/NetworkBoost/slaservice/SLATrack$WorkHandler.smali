.class Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;
.super Landroid/os/Handler;
.source "SLATrack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WorkHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;


# direct methods
.method public constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 74
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    .line 75
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 76
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 18
    .param p1, "msg"    # Landroid/os/Message;

    .line 80
    move-object/from16 v1, p0

    const-string v2, "SLM-SRV-SLATrack"

    new-instance v0, Ljava/io/File;

    const/4 v3, 0x0

    invoke-static {v3}, Landroid/os/Environment;->getDataSystemCeDirectory(I)Ljava/io/File;

    move-result-object v4

    const-string/jumbo v5, "sla"

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v4, v0

    .line 82
    .local v4, "prefsDir":Ljava/io/File;
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    new-instance v6, Ljava/io/File;

    const-string/jumbo v7, "sla_track"

    invoke-direct {v6, v4, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0, v6}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fputmMetricsPrefsFile(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;Ljava/io/File;)V

    .line 86
    :try_start_0
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)Landroid/content/Context;

    move-result-object v0

    iget-object v6, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v6}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetmMetricsPrefsFile(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v0, v6, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 87
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    .local v6, "editor":Landroid/content/SharedPreferences$Editor;
    nop

    .line 92
    move-object/from16 v7, p1

    iget v8, v7, Landroid/os/Message;->what:I

    const-string v9, "dbsla"

    const-string/jumbo v10, "sls"

    const-string v11, "dbsla_duration"

    const-string/jumbo v12, "sls_duration"

    const-string/jumbo v13, "sla_duration"

    const-wide/16 v14, 0x0

    packed-switch v8, :pswitch_data_0

    goto/16 :goto_0

    .line 149
    :pswitch_0
    iget-object v5, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetDoubleWifiAndSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v8

    cmp-long v5, v8, v14

    if-eqz v5, :cond_4

    .line 150
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MSG_DOUBLEWIFI_SLA_STOP time = "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    .line 151
    invoke-static {v8}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$mgetTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v8

    iget-object v10, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetDoubleWifiAndSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v12

    sub-long/2addr v8, v12

    invoke-interface {v0, v11, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v10

    int-to-long v12, v10

    add-long/2addr v8, v12

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 150
    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$mgetTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v8

    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetDoubleWifiAndSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v12

    sub-long/2addr v8, v12

    long-to-int v2, v8

    .line 153
    invoke-interface {v0, v11, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    add-int/2addr v2, v3

    .line 152
    invoke-interface {v6, v11, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 154
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2, v14, v15}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fputDoubleWifiAndSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;J)V

    goto/16 :goto_0

    .line 142
    :pswitch_1
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v2

    cmp-long v2, v2, v14

    if-eqz v2, :cond_0

    .line 143
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->sendMsgSlaStop()V

    .line 145
    :cond_0
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$mgetTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v10

    invoke-static {v2, v10, v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fputDoubleWifiAndSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;J)V

    .line 146
    const/4 v2, 0x1

    invoke-interface {v6, v9, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 147
    goto/16 :goto_0

    .line 133
    :pswitch_2
    iget-object v5, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetSLSStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v8

    cmp-long v5, v8, v14

    if-eqz v5, :cond_4

    .line 134
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MSG_SLS_STOP time = "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v8}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$mgetTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v8

    iget-object v10, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetSLSStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v10

    sub-long/2addr v8, v10

    .line 135
    invoke-interface {v0, v12, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v10

    int-to-long v10, v10

    add-long/2addr v8, v10

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 134
    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$mgetTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v8

    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetSLSStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v10

    sub-long/2addr v8, v10

    long-to-int v2, v8

    .line 137
    invoke-interface {v0, v12, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    add-int/2addr v2, v3

    .line 136
    invoke-interface {v6, v12, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 138
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2, v14, v15}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fputSLSStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;J)V

    goto/16 :goto_0

    .line 129
    :pswitch_3
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$mgetTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v8

    invoke-static {v2, v8, v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fputSLSStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;J)V

    .line 130
    const/4 v2, 0x1

    invoke-interface {v6, v10, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 131
    goto/16 :goto_0

    .line 120
    :pswitch_4
    iget-object v5, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v8

    cmp-long v5, v8, v14

    if-eqz v5, :cond_4

    .line 121
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MSG_SLA_STOP time = "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v8}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$mgetTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v8

    iget-object v10, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v10}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v10

    sub-long/2addr v8, v10

    .line 122
    invoke-interface {v0, v13, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v10

    int-to-long v10, v10

    add-long/2addr v8, v10

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 121
    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$mgetTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v8

    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v10

    sub-long/2addr v8, v10

    long-to-int v2, v8

    .line 124
    invoke-interface {v0, v13, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    add-int/2addr v2, v3

    .line 123
    invoke-interface {v6, v13, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 125
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2, v14, v15}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fputSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;J)V

    goto :goto_0

    .line 116
    :pswitch_5
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$mgetTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v8

    invoke-static {v2, v8, v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fputSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;J)V

    .line 117
    const/4 v2, 0x1

    invoke-interface {v6, v5, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 118
    goto :goto_0

    .line 112
    :pswitch_6
    const/4 v2, 0x1

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$sfgetmWorkHandler()Landroid/os/Handler;

    move-result-object v3

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$sfgetmWorkHandler()Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v8, 0x1

    invoke-virtual {v3, v2, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 114
    goto :goto_0

    .line 94
    :pswitch_7
    const-string v8, "handleMessage: MSG_START_REPORT"

    invoke-static {v2, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-virtual {v2, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->reportMQSEvent(Landroid/content/SharedPreferences;)V

    .line 96
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v16

    cmp-long v2, v16, v14

    if-nez v2, :cond_1

    .line 97
    invoke-interface {v6, v5, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 99
    :cond_1
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetSLSStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v16

    cmp-long v2, v16, v14

    if-nez v2, :cond_2

    .line 100
    invoke-interface {v6, v10, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 102
    :cond_2
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack$WorkHandler;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$fgetDoubleWifiAndSLAStartTime(Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;)J

    move-result-wide v16

    cmp-long v2, v16, v14

    if-nez v2, :cond_3

    .line 103
    invoke-interface {v6, v9, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 105
    :cond_3
    invoke-interface {v6, v13, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 106
    invoke-interface {v6, v12, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 107
    invoke-interface {v6, v11, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 108
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$sfgetmWorkHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->-$$Nest$sfgetmWorkHandler()Landroid/os/Handler;

    move-result-object v3

    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    const-wide/32 v8, 0x5265bff

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 110
    nop

    .line 160
    :cond_4
    :goto_0
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 161
    return-void

    .line 88
    .end local v0    # "prefs":Landroid/content/SharedPreferences;
    .end local v6    # "editor":Landroid/content/SharedPreferences$Editor;
    :catch_0
    move-exception v0

    move-object/from16 v7, p1

    .line 89
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v3, "IllegalStateException"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 90
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
