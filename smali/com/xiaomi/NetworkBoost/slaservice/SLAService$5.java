class com.xiaomi.NetworkBoost.slaservice.SLAService$5 implements com.xiaomi.NetworkBoost.StatusManager$IVPNListener {
	 /* .source "SLAService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.SLAService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.slaservice.SLAService$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* .line 1085 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onVPNStatusChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "vpnEnable" # Z */
/* .line 1088 */
final String v0 = "SLM-SRV-SLAService"; // const-string v0, "SLM-SRV-SLAService"
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 1089 */
	 final String v1 = "VPN CONNECTED"; // const-string v1, "VPN CONNECTED"
	 android.util.Log .d ( v0,v1 );
	 /* .line 1090 */
	 v0 = this.this$0;
	 (( com.xiaomi.NetworkBoost.slaservice.SLAService ) v0 ).disableSLM ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->disableSLM()V
	 /* .line 1092 */
} // :cond_0
final String v1 = "VPN DISCONNECTED"; // const-string v1, "VPN DISCONNECTED"
android.util.Log .d ( v0,v1 );
/* .line 1093 */
v0 = this.this$0;
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v0 ).enableSLM ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->enableSLM()V
/* .line 1095 */
} // :goto_0
return;
} // .end method
