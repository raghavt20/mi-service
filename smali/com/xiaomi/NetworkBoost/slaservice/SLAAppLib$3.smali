.class Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3;
.super Landroid/database/ContentObserver;
.source "SLAAppLib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->initSLACloudObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 1052
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 7
    .param p1, "selfChange"    # Z

    .line 1056
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1057
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v1

    .line 1059
    .local v1, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 1060
    .local v2, "device":Ljava/lang/String;
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLAAppDefaultPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 1062
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$sfgetmAppLists()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1063
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$sfgetmSLAAppLibLock()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1064
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    :try_start_1
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$sfgetmAppLists()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 1065
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$sfgetmAppLists()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setState(Z)V

    .line 1066
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$sfgetmAppLists()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-static {v5, v6}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$mdbUpdateSLAApp(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;)V

    .line 1064
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1068
    .end local v4    # "i":I
    :cond_0
    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3;
    .end local p1    # "selfChange":Z
    :try_start_2
    throw v4

    .line 1071
    .restart local p0    # "this":Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3;
    .restart local p1    # "selfChange":Z
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$minitSLAAppDefault(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)V

    .line 1073
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageInfo;

    .line 1074
    .local v4, "app":Landroid/content/pm/PackageInfo;
    iget-object v5, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$fgetmSLAAppDefaultPN(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;)Ljava/util/Set;

    move-result-object v5

    iget-object v6, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 1075
    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1076
    iget-object v5, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 1077
    .local v5, "uid":I
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib$3;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;

    invoke-static {v6, v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;->-$$Nest$maddSLAAppDefault(Lcom/xiaomi/NetworkBoost/slaservice/SLAAppLib;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1079
    .end local v4    # "app":Landroid/content/pm/PackageInfo;
    .end local v5    # "uid":I
    :cond_2
    goto :goto_2

    .line 1082
    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    .end local v1    # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v2    # "device":Ljava/lang/String;
    :cond_3
    goto :goto_3

    .line 1080
    :catch_0
    move-exception v0

    .line 1081
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SLM-SRV-SLAAppLib"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initSLACloudObserver onChange error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1083
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_3
    return-void
.end method
