public class com.xiaomi.NetworkBoost.slaservice.SLAApp implements android.os.Parcelable {
	 /* .source "SLAApp.java" */
	 /* # interfaces */
	 /* # static fields */
	 public static final android.os.Parcelable$Creator CREATOR;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/os/Parcelable$Creator<", */
	 /* "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # instance fields */
public Long mCurTraffic;
private Integer mDay;
private Long mDayTraffic;
private Integer mMonth;
private Long mMonthTraffic;
private Boolean mState;
private java.lang.String mUid;
/* # direct methods */
static com.xiaomi.NetworkBoost.slaservice.SLAApp ( ) {
/* .locals 1 */
/* .line 86 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp$1; */
/* invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp$1;-><init>()V */
return;
} // .end method
public com.xiaomi.NetworkBoost.slaservice.SLAApp ( ) {
/* .locals 0 */
/* .line 67 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 68 */
return;
} // .end method
protected com.xiaomi.NetworkBoost.slaservice.SLAApp ( ) {
/* .locals 2 */
/* .param p1, "in" # Landroid/os/Parcel; */
/* .line 70 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 71 */
(( android.os.Parcel ) p1 ).readString ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
this.mUid = v0;
/* .line 72 */
(( android.os.Parcel ) p1 ).readLong ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDayTraffic:J */
/* .line 73 */
(( android.os.Parcel ) p1 ).readLong ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonthTraffic:J */
/* .line 74 */
return;
} // .end method
public com.xiaomi.NetworkBoost.slaservice.SLAApp ( ) {
/* .locals 3 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 76 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 77 */
java.util.Calendar .getInstance ( );
/* .line 78 */
/* .local v0, "c":Ljava/util/Calendar; */
this.mUid = p1;
/* .line 79 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDayTraffic:J */
/* .line 80 */
/* iput-wide v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonthTraffic:J */
/* .line 81 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mState:Z */
/* .line 82 */
int v2 = 5; // const/4 v2, 0x5
v2 = (( java.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I
/* iput v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDay:I */
/* .line 83 */
int v2 = 2; // const/4 v2, 0x2
v2 = (( java.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I
/* add-int/2addr v2, v1 */
/* iput v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonth:I */
/* .line 84 */
return;
} // .end method
/* # virtual methods */
public Integer describeContents ( ) {
/* .locals 1 */
/* .line 100 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer getDay ( ) {
/* .locals 1 */
/* .line 28 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDay:I */
} // .end method
public Long getDayTraffic ( ) {
/* .locals 2 */
/* .line 44 */
/* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDayTraffic:J */
/* return-wide v0 */
} // .end method
public Integer getMonth ( ) {
/* .locals 1 */
/* .line 36 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonth:I */
} // .end method
public Long getMonthTraffic ( ) {
/* .locals 2 */
/* .line 52 */
/* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonthTraffic:J */
/* return-wide v0 */
} // .end method
public Boolean getState ( ) {
/* .locals 1 */
/* .line 60 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mState:Z */
} // .end method
public java.lang.String getUid ( ) {
/* .locals 1 */
/* .line 20 */
v0 = this.mUid;
} // .end method
public void readFromParcel ( android.os.Parcel p0 ) {
/* .locals 2 */
/* .param p1, "source" # Landroid/os/Parcel; */
/* .line 111 */
(( android.os.Parcel ) p1 ).readString ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
this.mUid = v0;
/* .line 112 */
(( android.os.Parcel ) p1 ).readLong ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDayTraffic:J */
/* .line 113 */
(( android.os.Parcel ) p1 ).readLong ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonthTraffic:J */
/* .line 114 */
return;
} // .end method
public void setDay ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "day" # I */
/* .line 32 */
/* iput p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDay:I */
/* .line 33 */
return;
} // .end method
public void setDayTraffic ( Long p0 ) {
/* .locals 0 */
/* .param p1, "traffic" # J */
/* .line 48 */
/* iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDayTraffic:J */
/* .line 49 */
return;
} // .end method
public void setMonth ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "month" # I */
/* .line 40 */
/* iput p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonth:I */
/* .line 41 */
return;
} // .end method
public void setMonthTraffic ( Long p0 ) {
/* .locals 0 */
/* .param p1, "traffic" # J */
/* .line 56 */
/* iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonthTraffic:J */
/* .line 57 */
return;
} // .end method
public void setState ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "state" # Z */
/* .line 64 */
/* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mState:Z */
/* .line 65 */
return;
} // .end method
public void setUid ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 24 */
this.mUid = p1;
/* .line 25 */
return;
} // .end method
public void writeToParcel ( android.os.Parcel p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "dest" # Landroid/os/Parcel; */
/* .param p2, "flags" # I */
/* .line 105 */
v0 = this.mUid;
(( android.os.Parcel ) p1 ).writeString ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 106 */
/* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mDayTraffic:J */
(( android.os.Parcel ) p1 ).writeLong ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V
/* .line 107 */
/* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->mMonthTraffic:J */
(( android.os.Parcel ) p1 ).writeLong ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V
/* .line 108 */
return;
} // .end method
