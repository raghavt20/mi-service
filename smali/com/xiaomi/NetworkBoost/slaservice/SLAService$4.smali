.class Lcom/xiaomi/NetworkBoost/slaservice/SLAService$4;
.super Ljava/lang/Object;
.source "SLAService.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 1038
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDefaultNetwrokChange(Ljava/lang/String;I)V
    .locals 5
    .param p1, "ifacename"    # Ljava/lang/String;
    .param p2, "ifacestatus"    # I

    .line 1041
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmWifiReady()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    if-eq p2, v2, :cond_0

    .line 1042
    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfputmWifiReady(Z)V

    .line 1043
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmInterface()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "wlan0,"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfputmInterface(Ljava/lang/String;)V

    .line 1044
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmIfaceNumber()I

    move-result v0

    sub-int/2addr v0, v2

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfputmIfaceNumber(I)V

    goto :goto_0

    .line 1045
    :cond_0
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmWifiReady()Z

    move-result v0

    if-nez v0, :cond_3

    if-ne p2, v2, :cond_3

    .line 1046
    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfputmWifiReady(Z)V

    .line 1047
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmInterface()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfputmInterface(Ljava/lang/String;)V

    .line 1048
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmIfaceNumber()I

    move-result v0

    add-int/2addr v0, v2

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfputmIfaceNumber(I)V

    .line 1052
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Default Netwrok Change mInterface:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmInterface()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " mWifiReady:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmWifiReady()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "SLM-SRV-SLAService"

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1055
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmMiWillManager(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1056
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$fgetmMiWillManager(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;)Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmWifiReady()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$sfgetmSlaveWifiReady()Z

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    :cond_1
    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->setDualWifiReady(Z)V

    .line 1058
    :cond_2
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAService$4;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    const-string v1, "Default Netwrok Change"

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->-$$Nest$mprocessNetworkCallback(Lcom/xiaomi/NetworkBoost/slaservice/SLAService;Ljava/lang/String;)V

    .line 1059
    return-void

    .line 1050
    :cond_3
    return-void
.end method
