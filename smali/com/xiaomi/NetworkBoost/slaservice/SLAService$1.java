class com.xiaomi.NetworkBoost.slaservice.SLAService$1 extends android.database.ContentObserver {
	 /* .source "SLAService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->initCloudObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.SLAService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.slaservice.SLAService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 624 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .line 627 */
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$mcheckMiwillCloudController ( v0 );
final String v1 = "SLM-SRV-SLAService"; // const-string v1, "SLM-SRV-SLAService"
if ( v0 != null) { // if-eqz v0, :cond_0
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmMiWillManager ( v0 );
	 /* if-nez v0, :cond_0 */
	 /* .line 628 */
	 final String v0 = "cloud controller enable miwill."; // const-string v0, "cloud controller enable miwill."
	 android.util.Log .i ( v1,v0 );
	 /* .line 629 */
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmContext ( );
	 com.xiaomi.NetworkBoost.slaservice.MiWillManager .getInstance ( v1 );
	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fputmMiWillManager ( v0,v1 );
	 /* .line 631 */
} // :cond_0
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$mcheckMiwillCloudController ( v0 );
/* if-nez v0, :cond_1 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmMiWillManager ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 632 */
	 final String v0 = "cloud controller disable miwill."; // const-string v0, "cloud controller disable miwill."
	 android.util.Log .i ( v1,v0 );
	 /* .line 633 */
	 com.xiaomi.NetworkBoost.slaservice.MiWillManager .destroyInstance ( );
	 /* .line 634 */
	 v0 = this.this$0;
	 int v1 = 0; // const/4 v1, 0x0
	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fputmMiWillManager ( v0,v1 );
	 /* .line 637 */
} // :cond_1
} // :goto_0
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$misEnableDWMonitor ( v0 );
/* .line 638 */
return;
} // .end method
