.class Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$2;
.super Landroid/os/Handler;
.source "SLAToast.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->initSlaToastHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 84
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 86
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 89
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast$2;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->-$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x110f0361

    const/16 v2, 0x7d0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    goto :goto_0

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAToast;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "show slatoast"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    .end local v0    # "e":Ljava/lang/Exception;
    nop

    .line 98
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
