public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructInetDiagMsg {
	 /* .source "StructInetDiagMsg.java" */
	 /* # static fields */
	 private static final Integer IDIAG_UID_OFFSET;
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public Integer idiag_uid;
	 /* # direct methods */
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructInetDiagMsg ( ) {
		 /* .locals 0 */
		 /* .line 41 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructInetDiagMsg parse ( java.nio.ByteBuffer p0 ) {
		 /* .locals 2 */
		 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .line 48 */
		 /* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagMsg; */
		 /* invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagMsg;-><init>()V */
		 /* .line 49 */
		 /* .local v0, "struct":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagMsg; */
		 /* const/16 v1, 0x50 */
		 v1 = 		 (( java.nio.ByteBuffer ) p0 ).getInt ( v1 ); // invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I
		 /* iput v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagMsg;->idiag_uid:I */
		 /* .line 50 */
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String toString ( ) {
		 /* .locals 2 */
		 /* .line 55 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "StructInetDiagMsg{ idiag_uid{"; // const-string v1, "StructInetDiagMsg{ idiag_uid{"
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagMsg;->idiag_uid:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 /* const-string/jumbo v1, "}, }" */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
