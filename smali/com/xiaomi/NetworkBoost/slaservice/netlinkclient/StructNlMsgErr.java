public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgErr {
	 /* .source "StructNlMsgErr.java" */
	 /* # static fields */
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public Integer error;
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr msg;
	 /* # direct methods */
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgErr ( ) {
		 /* .locals 0 */
		 /* .line 29 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Boolean hasAvailableSpace ( java.nio.ByteBuffer p0 ) {
		 /* .locals 2 */
		 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .line 33 */
		 if ( p0 != null) { // if-eqz p0, :cond_0
			 v0 = 			 (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
			 /* const/16 v1, 0x14 */
			 /* if-lt v0, v1, :cond_0 */
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :goto_0
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgErr parse ( java.nio.ByteBuffer p0 ) {
	 /* .locals 2 */
	 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
	 /* .line 37 */
	 v0 = 	 com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgErr .hasAvailableSpace ( p0 );
	 /* if-nez v0, :cond_0 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 42 */
} // :cond_0
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr; */
/* invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;-><init>()V */
/* .line 43 */
/* .local v0, "struct":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr; */
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;->error:I */
/* .line 44 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr .parse ( p0 );
this.msg = v1;
/* .line 45 */
} // .end method
/* # virtual methods */
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 1 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 55 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;->error:I */
(( java.nio.ByteBuffer ) p1 ).putInt ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
/* .line 56 */
v0 = this.msg;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 57 */
	 (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V
	 /* .line 59 */
} // :cond_0
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 63 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "StructNlMsgErr{ error{"; // const-string v1, "StructNlMsgErr{ error{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;->error:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, msg{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 65 */
v1 = this.msg;
/* if-nez v1, :cond_0 */
final String v1 = ""; // const-string v1, ""
} // :cond_0
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->toString()Ljava/lang/String;
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "} }" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 63 */
} // .end method
