public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage extends com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage {
	 /* .source "RtNetlinkNeighborMessage.java" */
	 /* # static fields */
	 public static final Object NDA_CACHEINFO;
	 public static final Object NDA_DST;
	 public static final Object NDA_IFINDEX;
	 public static final Object NDA_LLADDR;
	 public static final Object NDA_MASTER;
	 public static final Object NDA_PORT;
	 public static final Object NDA_PROBES;
	 public static final Object NDA_UNSPEC;
	 public static final Object NDA_VLAN;
	 public static final Object NDA_VNI;
	 /* # instance fields */
	 private com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo mCacheInfo;
	 private java.net.InetAddress mDestination;
	 private mLinkLayerAddr;
	 private com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg mNdmsg;
	 private Integer mNumProbes;
	 /* # direct methods */
	 private com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage ( ) {
		 /* .locals 2 */
		 /* .param p1, "header" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
		 /* .line 153 */
		 /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V */
		 /* .line 154 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mNdmsg = v0;
		 /* .line 155 */
		 this.mDestination = v0;
		 /* .line 156 */
		 this.mLinkLayerAddr = v0;
		 /* .line 157 */
		 int v1 = 0; // const/4 v1, 0x0
		 /* iput v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNumProbes:I */
		 /* .line 158 */
		 this.mCacheInfo = v0;
		 /* .line 159 */
		 return;
	 } // .end method
	 public static newGetNeighborsRequest ( Integer p0 ) {
		 /* .locals 5 */
		 /* .param p0, "seqNo" # I */
		 /* .line 100 */
		 /* const/16 v0, 0x1c */
		 /* .line 101 */
		 /* .local v0, "length":I */
		 /* const/16 v1, 0x1c */
		 /* new-array v2, v1, [B */
		 /* .line 102 */
		 /* .local v2, "bytes":[B */
		 java.nio.ByteBuffer .wrap ( v2 );
		 /* .line 103 */
		 /* .local v3, "byteBuffer":Ljava/nio/ByteBuffer; */
		 java.nio.ByteOrder .nativeOrder ( );
		 (( java.nio.ByteBuffer ) v3 ).order ( v4 ); // invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
		 /* .line 105 */
		 /* new-instance v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
		 /* invoke-direct {v4}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;-><init>()V */
		 /* .line 106 */
		 /* .local v4, "nlmsghdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
		 /* iput v1, v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_len:I */
		 /* .line 107 */
		 /* const/16 v1, 0x1e */
		 /* iput-short v1, v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
		 /* .line 108 */
		 /* const/16 v1, 0x301 */
		 /* iput-short v1, v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_flags:S */
		 /* .line 109 */
		 /* iput p0, v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_seq:I */
		 /* .line 110 */
		 (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr ) v4 ).pack ( v3 ); // invoke-virtual {v4, v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V
		 /* .line 112 */
		 /* new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg; */
		 /* invoke-direct {v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;-><init>()V */
		 /* .line 113 */
		 /* .local v1, "ndmsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg; */
		 (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg ) v1 ).pack ( v3 ); // invoke-virtual {v1, v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->pack(Ljava/nio/ByteBuffer;)V
		 /* .line 115 */
	 } // .end method
	 public static newNewNeighborMessage ( Integer p0, java.net.InetAddress p1, Object p2, Integer p3, Object[] p4 ) {
		 /* .locals 5 */
		 /* .param p0, "seqNo" # I */
		 /* .param p1, "ip" # Ljava/net/InetAddress; */
		 /* .param p2, "nudState" # S */
		 /* .param p3, "ifIndex" # I */
		 /* .param p4, "llAddr" # [B */
		 /* .line 124 */
		 /* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
		 /* invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;-><init>()V */
		 /* .line 125 */
		 /* .local v0, "nlmsghdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
		 /* const/16 v1, 0x1c */
		 /* iput-short v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
		 /* .line 126 */
		 /* const/16 v1, 0x105 */
		 /* iput-short v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_flags:S */
		 /* .line 127 */
		 /* iput p0, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_seq:I */
		 /* .line 129 */
		 /* new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage; */
		 /* invoke-direct {v1, v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V */
		 /* .line 130 */
		 /* .local v1, "msg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage; */
		 /* new-instance v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg; */
		 /* invoke-direct {v2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;-><init>()V */
		 this.mNdmsg = v2;
		 /* .line 131 */
		 /* nop */
		 /* .line 132 */
		 /* instance-of v3, p1, Ljava/net/Inet6Address; */
		 if ( v3 != null) { // if-eqz v3, :cond_0
		 } // :cond_0
	 } // :goto_0
	 /* int-to-byte v3, v3 */
	 /* iput-byte v3, v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_family:B */
	 /* .line 133 */
	 v2 = this.mNdmsg;
	 /* iput p3, v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_ifindex:I */
	 /* .line 134 */
	 v2 = this.mNdmsg;
	 /* iput-short p2, v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_state:S */
	 /* .line 135 */
	 this.mDestination = p1;
	 /* .line 136 */
	 this.mLinkLayerAddr = p4;
	 /* .line 138 */
	 v2 = 	 (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage ) v1 ).getRequiredSpace ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->getRequiredSpace()I
	 /* new-array v2, v2, [B */
	 /* .line 139 */
	 /* .local v2, "bytes":[B */
	 /* array-length v3, v2 */
	 /* iput v3, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_len:I */
	 /* .line 140 */
	 java.nio.ByteBuffer .wrap ( v2 );
	 /* .line 141 */
	 /* .local v3, "byteBuffer":Ljava/nio/ByteBuffer; */
	 java.nio.ByteOrder .nativeOrder ( );
	 (( java.nio.ByteBuffer ) v3 ).order ( v4 ); // invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
	 /* .line 142 */
	 (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage ) v1 ).pack ( v3 ); // invoke-virtual {v1, v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->pack(Ljava/nio/ByteBuffer;)V
	 /* .line 143 */
} // .end method
private static void packNlAttr ( Object p0, Object[] p1, java.nio.ByteBuffer p2 ) {
	 /* .locals 2 */
	 /* .param p0, "nlType" # S */
	 /* .param p1, "nlValue" # [B */
	 /* .param p2, "byteBuffer" # Ljava/nio/ByteBuffer; */
	 /* .line 197 */
	 /* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
	 /* invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>()V */
	 /* .line 198 */
	 /* .local v0, "nlAttr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
	 /* iput-short p0, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_type:S */
	 /* .line 199 */
	 this.nla_value = p1;
	 /* .line 200 */
	 v1 = this.nla_value;
	 /* array-length v1, v1 */
	 /* add-int/lit8 v1, v1, 0x4 */
	 /* int-to-short v1, v1 */
	 /* iput-short v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_len:S */
	 /* .line 201 */
	 (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v0 ).pack ( p2 ); // invoke-virtual {v0, p2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
	 /* .line 202 */
	 return;
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage parse ( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
	 /* .locals 6 */
	 /* .param p0, "header" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
	 /* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
	 /* .line 52 */
	 /* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage; */
	 /* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V */
	 /* .line 54 */
	 /* .local v0, "neighMsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage; */
	 com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg .parse ( p1 );
	 this.mNdmsg = v1;
	 /* .line 55 */
	 /* if-nez v1, :cond_0 */
	 /* .line 56 */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 60 */
} // :cond_0
v1 = (( java.nio.ByteBuffer ) p1 ).position ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I
/* .line 61 */
/* .local v1, "baseOffset":I */
int v2 = 1; // const/4 v2, 0x1
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v2,p1 );
/* .line 62 */
/* .local v2, "nlAttr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* .line 63 */
	 (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v2 ).getValueAsInetAddress ( ); // invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;
	 this.mDestination = v3;
	 /* .line 66 */
} // :cond_1
(( java.nio.ByteBuffer ) p1 ).position ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 67 */
int v3 = 2; // const/4 v3, 0x2
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v3,p1 );
/* .line 68 */
if ( v2 != null) { // if-eqz v2, :cond_2
	 /* .line 69 */
	 v3 = this.nla_value;
	 this.mLinkLayerAddr = v3;
	 /* .line 72 */
} // :cond_2
(( java.nio.ByteBuffer ) p1 ).position ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 73 */
int v3 = 4; // const/4 v3, 0x4
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v3,p1 );
/* .line 74 */
if ( v2 != null) { // if-eqz v2, :cond_3
	 /* .line 75 */
	 int v3 = 0; // const/4 v3, 0x0
	 v3 = 	 (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v2 ).getValueAsInt ( v3 ); // invoke-virtual {v2, v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsInt(I)I
	 /* iput v3, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNumProbes:I */
	 /* .line 78 */
} // :cond_3
(( java.nio.ByteBuffer ) p1 ).position ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 79 */
int v3 = 3; // const/4 v3, 0x3
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v3,p1 );
/* .line 80 */
if ( v2 != null) { // if-eqz v2, :cond_4
	 /* .line 81 */
	 (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v2 ).getValueAsByteBuffer ( ); // invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
	 com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo .parse ( v3 );
	 this.mCacheInfo = v3;
	 /* .line 84 */
} // :cond_4
/* const/16 v3, 0x1c */
/* .line 85 */
/* .local v3, "kMinConsumed":I */
v4 = this.mHeader;
/* iget v4, v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_len:I */
/* add-int/lit8 v4, v4, -0x1c */
v4 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .alignedLengthOf ( v4 );
/* .line 87 */
/* .local v4, "kAdditionalSpace":I */
v5 = (( java.nio.ByteBuffer ) p1 ).remaining ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I
/* if-ge v5, v4, :cond_5 */
/* .line 88 */
v5 = (( java.nio.ByteBuffer ) p1 ).limit ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I
(( java.nio.ByteBuffer ) p1 ).position ( v5 ); // invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 90 */
} // :cond_5
/* add-int v5, v1, v4 */
(( java.nio.ByteBuffer ) p1 ).position ( v5 ); // invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 93 */
} // :goto_0
} // .end method
/* # virtual methods */
public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo getCacheInfo ( ) {
/* .locals 1 */
/* .line 178 */
v0 = this.mCacheInfo;
} // .end method
public java.net.InetAddress getDestination ( ) {
/* .locals 1 */
/* .line 166 */
v0 = this.mDestination;
} // .end method
public getLinkLayerAddress ( ) {
/* .locals 1 */
/* .line 170 */
v0 = this.mLinkLayerAddr;
} // .end method
public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg getNdHeader ( ) {
/* .locals 1 */
/* .line 162 */
v0 = this.mNdmsg;
} // .end method
public Integer getProbes ( ) {
/* .locals 1 */
/* .line 174 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNumProbes:I */
} // .end method
public Integer getRequiredSpace ( ) {
/* .locals 2 */
/* .line 182 */
/* const/16 v0, 0x1c */
/* .line 183 */
/* .local v0, "spaceRequired":I */
v1 = this.mDestination;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 184 */
/* nop */
/* .line 185 */
(( java.net.InetAddress ) v1 ).getAddress ( ); // invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B
/* array-length v1, v1 */
/* add-int/lit8 v1, v1, 0x4 */
/* .line 184 */
v1 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .alignedLengthOf ( v1 );
/* add-int/2addr v0, v1 */
/* .line 187 */
} // :cond_0
v1 = this.mLinkLayerAddr;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 188 */
/* array-length v1, v1 */
/* add-int/lit8 v1, v1, 0x4 */
v1 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .alignedLengthOf ( v1 );
/* add-int/2addr v0, v1 */
/* .line 193 */
} // :cond_1
} // .end method
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 2 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 205 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage ) p0 ).getHeader ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->getHeader()Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 206 */
v0 = this.mNdmsg;
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->pack(Ljava/nio/ByteBuffer;)V
/* .line 208 */
v0 = this.mDestination;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 209 */
int v1 = 1; // const/4 v1, 0x1
(( java.net.InetAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B
com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage .packNlAttr ( v1,v0,p1 );
/* .line 211 */
} // :cond_0
v0 = this.mLinkLayerAddr;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 212 */
int v1 = 2; // const/4 v1, 0x2
com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage .packNlAttr ( v1,v0,p1 );
/* .line 214 */
} // :cond_1
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 5 */
/* .line 218 */
v0 = this.mDestination;
final String v1 = ""; // const-string v1, ""
/* if-nez v0, :cond_0 */
/* move-object v0, v1 */
} // :cond_0
(( java.net.InetAddress ) v0 ).getHostAddress ( ); // invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
/* .line 219 */
/* .local v0, "ipLiteral":Ljava/lang/String; */
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "RtNetlinkNeighborMessage{ nlmsghdr{"; // const-string v3, "RtNetlinkNeighborMessage{ nlmsghdr{"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 221 */
v3 = this.mHeader;
/* if-nez v3, :cond_1 */
/* move-object v3, v1 */
} // :cond_1
v3 = this.mHeader;
java.lang.Integer .valueOf ( v4 );
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr ) v3 ).toString ( v4 ); // invoke-virtual {v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;
} // :goto_1
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, ndmsg{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 222 */
v3 = this.mNdmsg;
/* if-nez v3, :cond_2 */
/* move-object v3, v1 */
} // :cond_2
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->toString()Ljava/lang/String;
} // :goto_2
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, destination{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "} linklayeraddr{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mLinkLayerAddr;
/* .line 224 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .hexify ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "} probes{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNumProbes:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "} cacheinfo{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 226 */
v3 = this.mCacheInfo;
/* if-nez v3, :cond_3 */
} // :cond_3
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->toString()Ljava/lang/String;
} // :goto_3
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "} }" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 219 */
} // .end method
