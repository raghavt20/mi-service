public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructInetDiagSockId {
	 /* .source "StructInetDiagSockId.java" */
	 /* # static fields */
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 private final INET_DIAG_NOCOOKIE;
	 private final IPV4_PADDING;
	 private final java.net.InetSocketAddress mLocSocketAddress;
	 private final java.net.InetSocketAddress mRemSocketAddress;
	 /* # direct methods */
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructInetDiagSockId ( ) {
		 /* .locals 1 */
		 /* .param p1, "loc" # Ljava/net/InetSocketAddress; */
		 /* .param p2, "rem" # Ljava/net/InetSocketAddress; */
		 /* .line 53 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 48 */
		 /* const/16 v0, 0x8 */
		 /* new-array v0, v0, [B */
		 /* fill-array-data v0, :array_0 */
		 this.INET_DIAG_NOCOOKIE = v0;
		 /* .line 51 */
		 /* const/16 v0, 0xc */
		 /* new-array v0, v0, [B */
		 /* fill-array-data v0, :array_1 */
		 this.IPV4_PADDING = v0;
		 /* .line 54 */
		 this.mLocSocketAddress = p1;
		 /* .line 55 */
		 this.mRemSocketAddress = p2;
		 /* .line 56 */
		 return;
		 /* :array_0 */
		 /* .array-data 1 */
		 /* -0x1t */
		 /* -0x1t */
		 /* -0x1t */
		 /* -0x1t */
		 /* -0x1t */
		 /* -0x1t */
		 /* -0x1t */
		 /* -0x1t */
	 } // .end array-data
	 /* :array_1 */
	 /* .array-data 1 */
	 /* 0x0t */
	 /* 0x0t */
	 /* 0x0t */
	 /* 0x0t */
	 /* 0x0t */
	 /* 0x0t */
	 /* 0x0t */
	 /* 0x0t */
	 /* 0x0t */
	 /* 0x0t */
	 /* 0x0t */
	 /* 0x0t */
} // .end array-data
} // .end method
/* # virtual methods */
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 1 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 59 */
v0 = java.nio.ByteOrder.BIG_ENDIAN;
(( java.nio.ByteBuffer ) p1 ).order ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 60 */
v0 = this.mLocSocketAddress;
v0 = (( java.net.InetSocketAddress ) v0 ).getPort ( ); // invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I
/* int-to-short v0, v0 */
(( java.nio.ByteBuffer ) p1 ).putShort ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 61 */
v0 = this.mRemSocketAddress;
v0 = (( java.net.InetSocketAddress ) v0 ).getPort ( ); // invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I
/* int-to-short v0, v0 */
(( java.nio.ByteBuffer ) p1 ).putShort ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 62 */
v0 = this.mLocSocketAddress;
(( java.net.InetSocketAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;
(( java.net.InetAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 63 */
v0 = this.mLocSocketAddress;
(( java.net.InetSocketAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;
/* instance-of v0, v0, Ljava/net/Inet4Address; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 64 */
	 v0 = this.IPV4_PADDING;
	 (( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
	 /* .line 66 */
} // :cond_0
v0 = this.mRemSocketAddress;
(( java.net.InetSocketAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;
(( java.net.InetAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 67 */
v0 = this.mRemSocketAddress;
(( java.net.InetSocketAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;
/* instance-of v0, v0, Ljava/net/Inet4Address; */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 68 */
	 v0 = this.IPV4_PADDING;
	 (( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
	 /* .line 70 */
} // :cond_1
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) p1 ).order ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 71 */
int v0 = 0; // const/4 v0, 0x0
(( java.nio.ByteBuffer ) p1 ).putInt ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
/* .line 72 */
v0 = this.INET_DIAG_NOCOOKIE;
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 73 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 77 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "StructInetDiagSockId{ idiag_sport{"; // const-string v1, "StructInetDiagSockId{ idiag_sport{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mLocSocketAddress;
/* .line 78 */
v1 = (( java.net.InetSocketAddress ) v1 ).getPort ( ); // invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getPort()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_dport{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRemSocketAddress;
/* .line 79 */
v1 = (( java.net.InetSocketAddress ) v1 ).getPort ( ); // invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getPort()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_src{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mLocSocketAddress;
/* .line 80 */
(( java.net.InetSocketAddress ) v1 ).getAddress ( ); // invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;
(( java.net.InetAddress ) v1 ).getHostAddress ( ); // invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_dst{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRemSocketAddress;
/* .line 81 */
(( java.net.InetSocketAddress ) v1 ).getAddress ( ); // invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;
(( java.net.InetAddress ) v1 ).getHostAddress ( ); // invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_if{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v1 = 0; // const/4 v1, 0x0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "} idiag_cookie{INET_DIAG_NOCOOKIE}}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 77 */
} // .end method
