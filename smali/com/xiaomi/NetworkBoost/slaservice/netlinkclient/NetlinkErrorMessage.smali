.class public Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage;
.super Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;
.source "NetlinkErrorMessage.java"


# instance fields
.field private mNlMsgErr:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V
    .locals 1
    .param p1, "header"    # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;

    .line 43
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage;->mNlMsgErr:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;

    .line 45
    return-void
.end method

.method public static parse(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage;
    .locals 2
    .param p0, "header"    # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 30
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V

    .line 32
    .local v0, "errorMsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage;
    invoke-static {p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;->parse(Ljava/nio/ByteBuffer;)Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage;->mNlMsgErr:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;

    .line 33
    if-nez v1, :cond_0

    .line 34
    const/4 v1, 0x0

    return-object v1

    .line 37
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getNlMsgError()Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage;->mNlMsgErr:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NetlinkErrorMessage{ nlmsghdr{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage;->mHeader:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;

    const-string v2, ""

    if-nez v1, :cond_0

    move-object v1, v2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage;->mHeader:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, nlmsgerr{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage;->mNlMsgErr:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "} }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 53
    return-object v0
.end method
