.class public Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;
.super Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;
.source "RtNetlinkNeighborMessage.java"


# static fields
.field public static final NDA_CACHEINFO:S = 0x3s

.field public static final NDA_DST:S = 0x1s

.field public static final NDA_IFINDEX:S = 0x8s

.field public static final NDA_LLADDR:S = 0x2s

.field public static final NDA_MASTER:S = 0x9s

.field public static final NDA_PORT:S = 0x6s

.field public static final NDA_PROBES:S = 0x4s

.field public static final NDA_UNSPEC:S = 0x0s

.field public static final NDA_VLAN:S = 0x5s

.field public static final NDA_VNI:S = 0x7s


# instance fields
.field private mCacheInfo:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;

.field private mDestination:Ljava/net/InetAddress;

.field private mLinkLayerAddr:[B

.field private mNdmsg:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;

.field private mNumProbes:I


# direct methods
.method private constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V
    .locals 2
    .param p1, "header"    # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;

    .line 153
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V

    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNdmsg:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;

    .line 155
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mDestination:Ljava/net/InetAddress;

    .line 156
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mLinkLayerAddr:[B

    .line 157
    const/4 v1, 0x0

    iput v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNumProbes:I

    .line 158
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mCacheInfo:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;

    .line 159
    return-void
.end method

.method public static newGetNeighborsRequest(I)[B
    .locals 5
    .param p0, "seqNo"    # I

    .line 100
    const/16 v0, 0x1c

    .line 101
    .local v0, "length":I
    const/16 v1, 0x1c

    new-array v2, v1, [B

    .line 102
    .local v2, "bytes":[B
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 103
    .local v3, "byteBuffer":Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 105
    new-instance v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;

    invoke-direct {v4}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;-><init>()V

    .line 106
    .local v4, "nlmsghdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
    iput v1, v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_len:I

    .line 107
    const/16 v1, 0x1e

    iput-short v1, v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S

    .line 108
    const/16 v1, 0x301

    iput-short v1, v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_flags:S

    .line 109
    iput p0, v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_seq:I

    .line 110
    invoke-virtual {v4, v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V

    .line 112
    new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;

    invoke-direct {v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;-><init>()V

    .line 113
    .local v1, "ndmsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;
    invoke-virtual {v1, v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->pack(Ljava/nio/ByteBuffer;)V

    .line 115
    return-object v2
.end method

.method public static newNewNeighborMessage(ILjava/net/InetAddress;SI[B)[B
    .locals 5
    .param p0, "seqNo"    # I
    .param p1, "ip"    # Ljava/net/InetAddress;
    .param p2, "nudState"    # S
    .param p3, "ifIndex"    # I
    .param p4, "llAddr"    # [B

    .line 124
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;

    invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;-><init>()V

    .line 125
    .local v0, "nlmsghdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
    const/16 v1, 0x1c

    iput-short v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S

    .line 126
    const/16 v1, 0x105

    iput-short v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_flags:S

    .line 127
    iput p0, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_seq:I

    .line 129
    new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;

    invoke-direct {v1, v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V

    .line 130
    .local v1, "msg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;
    new-instance v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;

    invoke-direct {v2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;-><init>()V

    iput-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNdmsg:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;

    .line 131
    nop

    .line 132
    instance-of v3, p1, Ljava/net/Inet6Address;

    if-eqz v3, :cond_0

    sget v3, Landroid/system/OsConstants;->AF_INET6:I

    goto :goto_0

    :cond_0
    sget v3, Landroid/system/OsConstants;->AF_INET:I

    :goto_0
    int-to-byte v3, v3

    iput-byte v3, v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_family:B

    .line 133
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNdmsg:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;

    iput p3, v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_ifindex:I

    .line 134
    iget-object v2, v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNdmsg:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;

    iput-short p2, v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_state:S

    .line 135
    iput-object p1, v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mDestination:Ljava/net/InetAddress;

    .line 136
    iput-object p4, v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mLinkLayerAddr:[B

    .line 138
    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->getRequiredSpace()I

    move-result v2

    new-array v2, v2, [B

    .line 139
    .local v2, "bytes":[B
    array-length v3, v2

    iput v3, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_len:I

    .line 140
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 141
    .local v3, "byteBuffer":Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 142
    invoke-virtual {v1, v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->pack(Ljava/nio/ByteBuffer;)V

    .line 143
    return-object v2
.end method

.method private static packNlAttr(S[BLjava/nio/ByteBuffer;)V
    .locals 2
    .param p0, "nlType"    # S
    .param p1, "nlValue"    # [B
    .param p2, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 197
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;

    invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>()V

    .line 198
    .local v0, "nlAttr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;
    iput-short p0, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_type:S

    .line 199
    iput-object p1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_value:[B

    .line 200
    iget-object v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_value:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x4

    int-to-short v1, v1

    iput-short v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_len:S

    .line 201
    invoke-virtual {v0, p2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V

    .line 202
    return-void
.end method

.method public static parse(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;
    .locals 6
    .param p0, "header"    # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 52
    new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V

    .line 54
    .local v0, "neighMsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;
    invoke-static {p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->parse(Ljava/nio/ByteBuffer;)Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNdmsg:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;

    .line 55
    if-nez v1, :cond_0

    .line 56
    const/4 v1, 0x0

    return-object v1

    .line 60
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 61
    .local v1, "baseOffset":I
    const/4 v2, 0x1

    invoke-static {v2, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;

    move-result-object v2

    .line 62
    .local v2, "nlAttr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;
    if-eqz v2, :cond_1

    .line 63
    invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;

    move-result-object v3

    iput-object v3, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mDestination:Ljava/net/InetAddress;

    .line 66
    :cond_1
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 67
    const/4 v3, 0x2

    invoke-static {v3, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;

    move-result-object v2

    .line 68
    if-eqz v2, :cond_2

    .line 69
    iget-object v3, v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_value:[B

    iput-object v3, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mLinkLayerAddr:[B

    .line 72
    :cond_2
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 73
    const/4 v3, 0x4

    invoke-static {v3, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;

    move-result-object v2

    .line 74
    if-eqz v2, :cond_3

    .line 75
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsInt(I)I

    move-result v3

    iput v3, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNumProbes:I

    .line 78
    :cond_3
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 79
    const/4 v3, 0x3

    invoke-static {v3, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;

    move-result-object v2

    .line 80
    if-eqz v2, :cond_4

    .line 81
    invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->parse(Ljava/nio/ByteBuffer;)Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;

    move-result-object v3

    iput-object v3, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mCacheInfo:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;

    .line 84
    :cond_4
    const/16 v3, 0x1c

    .line 85
    .local v3, "kMinConsumed":I
    iget-object v4, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mHeader:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;

    iget v4, v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_len:I

    add-int/lit8 v4, v4, -0x1c

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkConstants;->alignedLengthOf(I)I

    move-result v4

    .line 87
    .local v4, "kAdditionalSpace":I
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    if-ge v5, v4, :cond_5

    .line 88
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v5

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_0

    .line 90
    :cond_5
    add-int v5, v1, v4

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 93
    :goto_0
    return-object v0
.end method


# virtual methods
.method public getCacheInfo()Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mCacheInfo:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;

    return-object v0
.end method

.method public getDestination()Ljava/net/InetAddress;
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mDestination:Ljava/net/InetAddress;

    return-object v0
.end method

.method public getLinkLayerAddress()[B
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mLinkLayerAddr:[B

    return-object v0
.end method

.method public getNdHeader()Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNdmsg:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;

    return-object v0
.end method

.method public getProbes()I
    .locals 1

    .line 174
    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNumProbes:I

    return v0
.end method

.method public getRequiredSpace()I
    .locals 2

    .line 182
    const/16 v0, 0x1c

    .line 183
    .local v0, "spaceRequired":I
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mDestination:Ljava/net/InetAddress;

    if-eqz v1, :cond_0

    .line 184
    nop

    .line 185
    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, 0x4

    .line 184
    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkConstants;->alignedLengthOf(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mLinkLayerAddr:[B

    if-eqz v1, :cond_1

    .line 188
    array-length v1, v1

    add-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkConstants;->alignedLengthOf(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 193
    :cond_1
    return v0
.end method

.method public pack(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 205
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->getHeader()Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V

    .line 206
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNdmsg:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;

    invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->pack(Ljava/nio/ByteBuffer;)V

    .line 208
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mDestination:Ljava/net/InetAddress;

    if-eqz v0, :cond_0

    .line 209
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    invoke-static {v1, v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->packNlAttr(S[BLjava/nio/ByteBuffer;)V

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mLinkLayerAddr:[B

    if-eqz v0, :cond_1

    .line 212
    const/4 v1, 0x2

    invoke-static {v1, v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->packNlAttr(S[BLjava/nio/ByteBuffer;)V

    .line 214
    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 218
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mDestination:Ljava/net/InetAddress;

    const-string v1, ""

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    .line 219
    .local v0, "ipLiteral":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RtNetlinkNeighborMessage{ nlmsghdr{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 221
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mHeader:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;

    if-nez v3, :cond_1

    move-object v3, v1

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mHeader:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;

    sget v4, Landroid/system/OsConstants;->NETLINK_ROUTE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "}, ndmsg{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 222
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNdmsg:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;

    if-nez v3, :cond_2

    move-object v3, v1

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "}, destination{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "} linklayeraddr{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mLinkLayerAddr:[B

    .line 224
    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkConstants;->hexify([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "} probes{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mNumProbes:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "} cacheinfo{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 226
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/RtNetlinkNeighborMessage;->mCacheInfo:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;

    if-nez v3, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "} }"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 219
    return-object v1
.end method
