public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage extends com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage {
	 /* .source "ConntrackMessage.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$Tuple;, */
	 /* Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$TupleIpv4;, */
	 /* Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$TupleProto; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Object CTA_IP_V4_DST;
public static final Object CTA_IP_V4_SRC;
public static final Object CTA_PROTO_DST_PORT;
public static final Object CTA_PROTO_NUM;
public static final Object CTA_PROTO_SRC_PORT;
public static final Object CTA_STATUS;
public static final Object CTA_TIMEOUT;
public static final Object CTA_TUPLE_IP;
public static final Object CTA_TUPLE_ORIG;
public static final Object CTA_TUPLE_PROTO;
public static final Object CTA_TUPLE_REPLY;
public static final Integer DYING_MASK;
public static final Integer ESTABLISHED_MASK;
public static final Integer IPS_ASSURED;
public static final Integer IPS_CONFIRMED;
public static final Integer IPS_DST_NAT;
public static final Integer IPS_DST_NAT_DONE;
public static final Integer IPS_DYING;
public static final Integer IPS_EXPECTED;
public static final Integer IPS_FIXED_TIMEOUT;
public static final Integer IPS_HELPER;
public static final Integer IPS_HW_OFFLOAD;
public static final Integer IPS_OFFLOAD;
public static final Integer IPS_SEEN_REPLY;
public static final Integer IPS_SEQ_ADJUST;
public static final Integer IPS_SRC_NAT;
public static final Integer IPS_SRC_NAT_DONE;
public static final Integer IPS_TEMPLATE;
public static final Integer IPS_UNTRACKED;
public static final Integer STRUCT_SIZE;
/* # instance fields */
public final com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNfGenMsg nfGenMsg;
public final Integer status;
public final Integer timeoutSec;
public final com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage$Tuple tupleOrig;
public final com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage$Tuple tupleReply;
/* # direct methods */
private com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage ( ) {
	 /* .locals 2 */
	 /* .line 437 */
	 /* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
	 /* invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;-><init>()V */
	 /* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V */
	 /* .line 438 */
	 /* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg; */
	 /* int-to-byte v1, v1 */
	 /* invoke-direct {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;-><init>(B)V */
	 this.nfGenMsg = v0;
	 /* .line 442 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.tupleOrig = v0;
	 /* .line 443 */
	 this.tupleReply = v0;
	 /* .line 444 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage;->status:I */
	 /* .line 445 */
	 /* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage;->timeoutSec:I */
	 /* .line 446 */
	 return;
} // .end method
private com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage ( ) {
	 /* .locals 0 */
	 /* .param p1, "header" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
	 /* .param p2, "nfGenMsg" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg; */
	 /* .param p3, "tupleOrig" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$Tuple; */
	 /* .param p4, "tupleReply" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$Tuple; */
	 /* .param p5, "status" # I */
	 /* .param p6, "timeoutSec" # I */
	 /* .line 450 */
	 /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V */
	 /* .line 451 */
	 this.nfGenMsg = p2;
	 /* .line 452 */
	 this.tupleOrig = p3;
	 /* .line 453 */
	 this.tupleReply = p4;
	 /* .line 454 */
	 /* iput p5, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage;->status:I */
	 /* .line 455 */
	 /* iput p6, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage;->timeoutSec:I */
	 /* .line 456 */
	 return;
} // .end method
private static java.net.Inet4Address castToInet4Address ( java.net.InetAddress p0 ) {
	 /* .locals 1 */
	 /* .param p0, "address" # Ljava/net/InetAddress; */
	 /* .line 337 */
	 if ( p0 != null) { // if-eqz p0, :cond_1
		 /* instance-of v0, p0, Ljava/net/Inet4Address; */
		 /* if-nez v0, :cond_0 */
		 /* .line 338 */
	 } // :cond_0
	 /* move-object v0, p0 */
	 /* check-cast v0, Ljava/net/Inet4Address; */
	 /* .line 337 */
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static newIPv4TimeoutUpdateRequest ( Integer p0, java.net.Inet4Address p1, Integer p2, java.net.Inet4Address p3, Integer p4, Integer p5 ) {
/* .locals 16 */
/* .param p0, "proto" # I */
/* .param p1, "src" # Ljava/net/Inet4Address; */
/* .param p2, "sport" # I */
/* .param p3, "dst" # Ljava/net/Inet4Address; */
/* .param p4, "dport" # I */
/* .param p5, "timeoutSec" # I */
/* .line 190 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
/* new-instance v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
int v3 = 1; // const/4 v3, 0x1
/* move-object/from16 v4, p1 */
/* invoke-direct {v2, v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>(SLjava/net/InetAddress;)V */
/* new-instance v5, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
int v6 = 2; // const/4 v6, 0x2
/* move-object/from16 v7, p3 */
/* invoke-direct {v5, v6, v7}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>(SLjava/net/InetAddress;)V */
/* filled-new-array {v2, v5}, [Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
/* invoke-direct {v1, v3, v2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>(S[Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;)V */
/* new-instance v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
/* new-instance v5, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
/* move/from16 v8, p0 */
/* int-to-byte v9, v8 */
/* invoke-direct {v5, v3, v9}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>(SB)V */
/* new-instance v9, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
/* move/from16 v10, p2 */
/* int-to-short v11, v10 */
v12 = java.nio.ByteOrder.BIG_ENDIAN;
/* invoke-direct {v9, v6, v11, v12}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>(SSLjava/nio/ByteOrder;)V */
/* new-instance v11, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
/* move/from16 v12, p4 */
/* int-to-short v13, v12 */
v14 = java.nio.ByteOrder.BIG_ENDIAN;
int v15 = 3; // const/4 v15, 0x3
/* invoke-direct {v11, v15, v13, v14}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>(SSLjava/nio/ByteOrder;)V */
/* filled-new-array {v5, v9, v11}, [Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
/* invoke-direct {v2, v6, v5}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>(S[Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;)V */
/* filled-new-array {v1, v2}, [Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
/* invoke-direct {v0, v3, v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>(S[Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;)V */
/* .line 199 */
/* .local v0, "ctaTupleOrig":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
int v2 = 7; // const/4 v2, 0x7
v5 = java.nio.ByteOrder.BIG_ENDIAN;
/* move/from16 v6, p5 */
/* invoke-direct {v1, v2, v6, v5}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>(SILjava/nio/ByteOrder;)V */
/* .line 201 */
/* .local v1, "ctaTimeout":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
v2 = (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v0 ).getAlignedLength ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getAlignedLength()I
v5 = (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v1 ).getAlignedLength ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getAlignedLength()I
/* add-int/2addr v2, v5 */
/* .line 202 */
/* .local v2, "payloadLength":I */
/* add-int/lit8 v5, v2, 0x14 */
/* new-array v5, v5, [B */
/* .line 203 */
/* .local v5, "bytes":[B */
java.nio.ByteBuffer .wrap ( v5 );
/* .line 204 */
/* .local v9, "byteBuffer":Ljava/nio/ByteBuffer; */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) v9 ).order ( v11 ); // invoke-virtual {v9, v11}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 206 */
/* new-instance v11, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage; */
/* invoke-direct {v11}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage;-><init>()V */
/* .line 207 */
/* .local v11, "ctmsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage; */
v13 = this.mHeader;
/* array-length v14, v5 */
/* iput v14, v13, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_len:I */
/* .line 208 */
v13 = this.mHeader;
/* const/16 v14, 0x100 */
/* iput-short v14, v13, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
/* .line 210 */
v13 = this.mHeader;
/* const/16 v14, 0x105 */
/* iput-short v14, v13, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_flags:S */
/* .line 211 */
v13 = this.mHeader;
/* iput v3, v13, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_seq:I */
/* .line 212 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage ) v11 ).pack ( v9 ); // invoke-virtual {v11, v9}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage;->pack(Ljava/nio/ByteBuffer;)V
/* .line 214 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v0 ).pack ( v9 ); // invoke-virtual {v0, v9}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 215 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v1 ).pack ( v9 ); // invoke-virtual {v1, v9}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 217 */
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage parse ( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
/* .locals 18 */
/* .param p0, "header" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 232 */
/* move-object/from16 v0, p1 */
/* invoke-static/range {p1 ..p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;->parse(Ljava/nio/ByteBuffer;)Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg; */
/* .line 233 */
/* .local v8, "nfGenMsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v8, :cond_0 */
/* .line 234 */
/* .line 237 */
} // :cond_0
v9 = /* invoke-virtual/range {p1 ..p1}, Ljava/nio/ByteBuffer;->position()I */
/* .line 238 */
/* .local v9, "baseOffset":I */
int v2 = 3; // const/4 v2, 0x3
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v2,v0 );
/* .line 239 */
/* .local v2, "nlAttr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
int v3 = 0; // const/4 v3, 0x0
/* .line 240 */
/* .local v3, "status":I */
int v4 = 0; // const/4 v4, 0x0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 241 */
v3 = (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v2 ).getValueAsBe32 ( v4 ); // invoke-virtual {v2, v4}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsBe32(I)I
/* move v10, v3 */
/* .line 240 */
} // :cond_1
/* move v10, v3 */
/* .line 244 */
} // .end local v3 # "status":I
/* .local v10, "status":I */
} // :goto_0
(( java.nio.ByteBuffer ) v0 ).position ( v9 ); // invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 245 */
int v3 = 7; // const/4 v3, 0x7
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v3,v0 );
/* .line 246 */
int v3 = 0; // const/4 v3, 0x0
/* .line 247 */
/* .local v3, "timeoutSec":I */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 248 */
v3 = (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v2 ).getValueAsBe32 ( v4 ); // invoke-virtual {v2, v4}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsBe32(I)I
/* move v11, v3 */
/* .line 247 */
} // :cond_2
/* move v11, v3 */
/* .line 251 */
} // .end local v3 # "timeoutSec":I
/* .local v11, "timeoutSec":I */
} // :goto_1
(( java.nio.ByteBuffer ) v0 ).position ( v9 ); // invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 252 */
int v3 = 1; // const/4 v3, 0x1
v3 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .makeNestedType ( v3 );
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v3,v0 );
/* .line 253 */
int v3 = 0; // const/4 v3, 0x0
/* .line 254 */
/* .local v3, "tupleOrig":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$Tuple; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 255 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v2 ).getValueAsByteBuffer ( ); // invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage .parseTuple ( v4 );
/* move-object v12, v3 */
/* .line 254 */
} // :cond_3
/* move-object v12, v3 */
/* .line 258 */
} // .end local v3 # "tupleOrig":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$Tuple;
/* .local v12, "tupleOrig":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$Tuple; */
} // :goto_2
(( java.nio.ByteBuffer ) v0 ).position ( v9 ); // invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 259 */
int v3 = 2; // const/4 v3, 0x2
v3 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .makeNestedType ( v3 );
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v3,v0 );
/* .line 260 */
} // .end local v2 # "nlAttr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;
/* .local v13, "nlAttr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
int v2 = 0; // const/4 v2, 0x0
/* .line 261 */
/* .local v2, "tupleReply":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$Tuple; */
if ( v13 != null) { // if-eqz v13, :cond_4
/* .line 262 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v13 ).getValueAsByteBuffer ( ); // invoke-virtual {v13}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage .parseTuple ( v3 );
/* move-object v14, v2 */
/* .line 261 */
} // :cond_4
/* move-object v14, v2 */
/* .line 266 */
} // .end local v2 # "tupleReply":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$Tuple;
/* .local v14, "tupleReply":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$Tuple; */
} // :goto_3
(( java.nio.ByteBuffer ) v0 ).position ( v9 ); // invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 267 */
/* const/16 v15, 0x14 */
/* .line 268 */
/* .local v15, "kMinConsumed":I */
/* move-object/from16 v7, p0 */
/* iget v2, v7, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_len:I */
/* add-int/lit8 v2, v2, -0x14 */
v6 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .alignedLengthOf ( v2 );
/* .line 270 */
/* .local v6, "kAdditionalSpace":I */
v2 = /* invoke-virtual/range {p1 ..p1}, Ljava/nio/ByteBuffer;->remaining()I */
/* if-ge v2, v6, :cond_5 */
/* .line 271 */
/* .line 273 */
} // :cond_5
/* add-int v1, v9, v6 */
(( java.nio.ByteBuffer ) v0 ).position ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 275 */
/* new-instance v16, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage; */
/* move-object/from16 v1, v16 */
/* move-object/from16 v2, p0 */
/* move-object v3, v8 */
/* move-object v4, v12 */
/* move-object v5, v14 */
/* move/from16 v17, v6 */
} // .end local v6 # "kAdditionalSpace":I
/* .local v17, "kAdditionalSpace":I */
/* move v6, v10 */
/* move v7, v11 */
/* invoke-direct/range {v1 ..v7}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$Tuple;Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$Tuple;II)V */
} // .end method
private static com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage$Tuple parseTuple ( java.nio.ByteBuffer p0 ) {
/* .locals 6 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 313 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 315 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 316 */
/* .local v1, "tupleIpv4":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$TupleIpv4; */
int v2 = 0; // const/4 v2, 0x0
/* .line 318 */
/* .local v2, "tupleProto":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$TupleProto; */
v3 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
/* .line 319 */
/* .local v3, "baseOffset":I */
int v4 = 1; // const/4 v4, 0x1
v4 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .makeNestedType ( v4 );
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v4,p0 );
/* .line 320 */
/* .local v4, "nlAttr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 321 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v4 ).getValueAsByteBuffer ( ); // invoke-virtual {v4}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage .parseTupleIpv4 ( v5 );
/* .line 323 */
} // :cond_1
/* if-nez v1, :cond_2 */
/* .line 325 */
} // :cond_2
(( java.nio.ByteBuffer ) p0 ).position ( v3 ); // invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 326 */
int v5 = 2; // const/4 v5, 0x2
v5 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .makeNestedType ( v5 );
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v5,p0 );
/* .line 327 */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 328 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v4 ).getValueAsByteBuffer ( ); // invoke-virtual {v4}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage .parseTupleProto ( v5 );
/* .line 330 */
} // :cond_3
/* if-nez v2, :cond_4 */
/* .line 332 */
} // :cond_4
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$Tuple; */
/* invoke-direct {v0, v1, v2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$Tuple;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$TupleIpv4;Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$TupleProto;)V */
} // .end method
private static com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage$TupleIpv4 parseTupleIpv4 ( java.nio.ByteBuffer p0 ) {
/* .locals 6 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 343 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 345 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 346 */
/* .local v1, "src":Ljava/net/Inet4Address; */
int v2 = 0; // const/4 v2, 0x0
/* .line 348 */
/* .local v2, "dst":Ljava/net/Inet4Address; */
v3 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
/* .line 349 */
/* .local v3, "baseOffset":I */
int v4 = 1; // const/4 v4, 0x1
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v4,p0 );
/* .line 350 */
/* .local v4, "nlAttr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 351 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v4 ).getValueAsInetAddress ( ); // invoke-virtual {v4}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;
com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage .castToInet4Address ( v5 );
/* .line 353 */
} // :cond_1
/* if-nez v1, :cond_2 */
/* .line 355 */
} // :cond_2
(( java.nio.ByteBuffer ) p0 ).position ( v3 ); // invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 356 */
int v5 = 2; // const/4 v5, 0x2
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v5,p0 );
/* .line 357 */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 358 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v4 ).getValueAsInetAddress ( ); // invoke-virtual {v4}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;
com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage .castToInet4Address ( v5 );
/* .line 360 */
} // :cond_3
/* if-nez v2, :cond_4 */
/* .line 362 */
} // :cond_4
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$TupleIpv4; */
/* invoke-direct {v0, v1, v2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$TupleIpv4;-><init>(Ljava/net/Inet4Address;Ljava/net/Inet4Address;)V */
} // .end method
private static com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage$TupleProto parseTupleProto ( java.nio.ByteBuffer p0 ) {
/* .locals 8 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 367 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 369 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 370 */
/* .local v1, "protoNum":B */
int v2 = 0; // const/4 v2, 0x0
/* .line 371 */
/* .local v2, "srcPort":S */
int v3 = 0; // const/4 v3, 0x0
/* .line 373 */
/* .local v3, "dstPort":S */
v4 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
/* .line 374 */
/* .local v4, "baseOffset":I */
int v5 = 1; // const/4 v5, 0x1
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v5,p0 );
/* .line 375 */
/* .local v5, "nlAttr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
int v6 = 0; // const/4 v6, 0x0
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 376 */
v1 = (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v5 ).getValueAsByte ( v6 ); // invoke-virtual {v5, v6}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByte(B)B
/* .line 378 */
} // :cond_1
/* if-eq v1, v7, :cond_2 */
/* if-eq v1, v7, :cond_2 */
/* .line 380 */
} // :cond_2
(( java.nio.ByteBuffer ) p0 ).position ( v4 ); // invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 381 */
int v7 = 2; // const/4 v7, 0x2
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v7,p0 );
/* .line 382 */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 383 */
v2 = (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v5 ).getValueAsBe16 ( v6 ); // invoke-virtual {v5, v6}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsBe16(S)S
/* .line 385 */
} // :cond_3
/* if-nez v2, :cond_4 */
/* .line 387 */
} // :cond_4
(( java.nio.ByteBuffer ) p0 ).position ( v4 ); // invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 388 */
int v7 = 3; // const/4 v7, 0x3
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .findNextAttrOfType ( v7,p0 );
/* .line 389 */
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 390 */
v3 = (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v5 ).getValueAsBe16 ( v6 ); // invoke-virtual {v5, v6}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsBe16(S)S
/* .line 392 */
} // :cond_5
/* if-nez v3, :cond_6 */
/* .line 394 */
} // :cond_6
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$TupleProto; */
/* invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage$TupleProto;-><init>(BSS)V */
} // .end method
public static java.lang.String stringForIpConntrackStatus ( Integer p0 ) {
/* .locals 3 */
/* .param p0, "flags" # I */
/* .line 471 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 473 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* and-int/lit8 v1, p0, 0x1 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 474 */
final String v1 = "IPS_EXPECTED"; // const-string v1, "IPS_EXPECTED"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 476 */
} // :cond_0
/* and-int/lit8 v1, p0, 0x2 */
/* const-string/jumbo v2, "|" */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 477 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_1 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 478 */
} // :cond_1
final String v1 = "IPS_SEEN_REPLY"; // const-string v1, "IPS_SEEN_REPLY"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 480 */
} // :cond_2
/* and-int/lit8 v1, p0, 0x4 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 481 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_3 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 482 */
} // :cond_3
final String v1 = "IPS_ASSURED"; // const-string v1, "IPS_ASSURED"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 484 */
} // :cond_4
/* and-int/lit8 v1, p0, 0x8 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 485 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_5 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 486 */
} // :cond_5
final String v1 = "IPS_CONFIRMED"; // const-string v1, "IPS_CONFIRMED"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 488 */
} // :cond_6
/* and-int/lit8 v1, p0, 0x10 */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 489 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_7 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 490 */
} // :cond_7
final String v1 = "IPS_SRC_NAT"; // const-string v1, "IPS_SRC_NAT"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 492 */
} // :cond_8
/* and-int/lit8 v1, p0, 0x20 */
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 493 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_9 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 494 */
} // :cond_9
final String v1 = "IPS_DST_NAT"; // const-string v1, "IPS_DST_NAT"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 496 */
} // :cond_a
/* and-int/lit8 v1, p0, 0x40 */
if ( v1 != null) { // if-eqz v1, :cond_c
/* .line 497 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_b */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 498 */
} // :cond_b
final String v1 = "IPS_SEQ_ADJUST"; // const-string v1, "IPS_SEQ_ADJUST"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 500 */
} // :cond_c
/* and-int/lit16 v1, p0, 0x80 */
if ( v1 != null) { // if-eqz v1, :cond_e
/* .line 501 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_d */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 502 */
} // :cond_d
final String v1 = "IPS_SRC_NAT_DONE"; // const-string v1, "IPS_SRC_NAT_DONE"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 504 */
} // :cond_e
/* and-int/lit16 v1, p0, 0x100 */
if ( v1 != null) { // if-eqz v1, :cond_10
/* .line 505 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_f */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 506 */
} // :cond_f
final String v1 = "IPS_DST_NAT_DONE"; // const-string v1, "IPS_DST_NAT_DONE"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 508 */
} // :cond_10
/* and-int/lit16 v1, p0, 0x200 */
if ( v1 != null) { // if-eqz v1, :cond_12
/* .line 509 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_11 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 510 */
} // :cond_11
final String v1 = "IPS_DYING"; // const-string v1, "IPS_DYING"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 512 */
} // :cond_12
/* and-int/lit16 v1, p0, 0x400 */
if ( v1 != null) { // if-eqz v1, :cond_14
/* .line 513 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_13 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 514 */
} // :cond_13
final String v1 = "IPS_FIXED_TIMEOUT"; // const-string v1, "IPS_FIXED_TIMEOUT"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 516 */
} // :cond_14
/* and-int/lit16 v1, p0, 0x800 */
if ( v1 != null) { // if-eqz v1, :cond_16
/* .line 517 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_15 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 518 */
} // :cond_15
final String v1 = "IPS_TEMPLATE"; // const-string v1, "IPS_TEMPLATE"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 520 */
} // :cond_16
/* and-int/lit16 v1, p0, 0x1000 */
if ( v1 != null) { // if-eqz v1, :cond_18
/* .line 521 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_17 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 522 */
} // :cond_17
final String v1 = "IPS_UNTRACKED"; // const-string v1, "IPS_UNTRACKED"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 524 */
} // :cond_18
/* and-int/lit16 v1, p0, 0x2000 */
if ( v1 != null) { // if-eqz v1, :cond_1a
/* .line 525 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_19 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 526 */
} // :cond_19
final String v1 = "IPS_HELPER"; // const-string v1, "IPS_HELPER"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 528 */
} // :cond_1a
/* and-int/lit16 v1, p0, 0x4000 */
if ( v1 != null) { // if-eqz v1, :cond_1c
/* .line 529 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_1b */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 530 */
} // :cond_1b
final String v1 = "IPS_OFFLOAD"; // const-string v1, "IPS_OFFLOAD"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 532 */
} // :cond_1c
/* const v1, 0x8000 */
/* and-int/2addr v1, p0 */
if ( v1 != null) { // if-eqz v1, :cond_1e
/* .line 533 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_1d */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 534 */
} // :cond_1d
final String v1 = "IPS_HW_OFFLOAD"; // const-string v1, "IPS_HW_OFFLOAD"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 536 */
} // :cond_1e
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
/* # virtual methods */
public Object getMessageType ( ) {
/* .locals 1 */
/* .line 464 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage ) p0 ).getHeader ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage;->getHeader()Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
/* iget-short v0, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
/* and-int/lit16 v0, v0, -0x101 */
/* int-to-short v0, v0 */
} // .end method
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 1 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 459 */
v0 = this.mHeader;
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 460 */
v0 = this.nfGenMsg;
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNfGenMsg ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;->pack(Ljava/nio/ByteBuffer;)V
/* .line 461 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 541 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ConntrackMessage{nlmsghdr{"; // const-string v1, "ConntrackMessage{nlmsghdr{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 543 */
v1 = this.mHeader;
/* if-nez v1, :cond_0 */
final String v1 = ""; // const-string v1, ""
} // :cond_0
v1 = this.mHeader;
java.lang.Integer .valueOf ( v2 );
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr ) v1 ).toString ( v2 ); // invoke-virtual {v1, v2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, nfgenmsg{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.nfGenMsg;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, tuple_orig{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.tupleOrig;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, tuple_reply{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.tupleReply;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, status{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage;->status:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "("; // const-string v1, "("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage;->status:I */
/* .line 548 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage .stringForIpConntrackStatus ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ")}, timeout_sec{"; // const-string v1, ")}, timeout_sec{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/ConntrackMessage;->timeoutSec:I */
/* .line 549 */
java.lang.Integer .toUnsignedLong ( v1 );
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 541 */
} // .end method
