public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg {
	 /* .source "StructNdMsg.java" */
	 /* # static fields */
	 public static Object NTF_MASTER;
	 public static Object NTF_PROXY;
	 public static Object NTF_ROUTER;
	 public static Object NTF_SELF;
	 public static Object NTF_USE;
	 public static final Object NUD_DELAY;
	 public static final Object NUD_FAILED;
	 public static final Object NUD_INCOMPLETE;
	 public static final Object NUD_NOARP;
	 public static final Object NUD_NONE;
	 public static final Object NUD_PERMANENT;
	 public static final Object NUD_PROBE;
	 public static final Object NUD_REACHABLE;
	 public static final Object NUD_STALE;
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public Object ndm_family;
	 public Object ndm_flags;
	 public Integer ndm_ifindex;
	 public Object ndm_state;
	 public Object ndm_type;
	 /* # direct methods */
	 static com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg ( ) {
		 /* .locals 1 */
		 /* .line 71 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* sput-byte v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->NTF_USE:B */
		 /* .line 72 */
		 int v0 = 2; // const/4 v0, 0x2
		 /* sput-byte v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->NTF_SELF:B */
		 /* .line 73 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* sput-byte v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->NTF_MASTER:B */
		 /* .line 74 */
		 /* const/16 v0, 0x8 */
		 /* sput-byte v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->NTF_PROXY:B */
		 /* .line 75 */
		 /* const/16 v0, -0x80 */
		 /* sput-byte v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->NTF_ROUTER:B */
		 return;
	 } // .end method
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg ( ) {
		 /* .locals 1 */
		 /* .line 128 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 129 */
		 /* int-to-byte v0, v0 */
		 /* iput-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_family:B */
		 /* .line 130 */
		 return;
	 } // .end method
	 private static Boolean hasAvailableSpace ( java.nio.ByteBuffer p0 ) {
		 /* .locals 2 */
		 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .line 102 */
		 if ( p0 != null) { // if-eqz p0, :cond_0
			 v0 = 			 (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
			 /* const/16 v1, 0xc */
			 /* if-lt v0, v1, :cond_0 */
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :goto_0
} // .end method
public static Boolean isNudStateConnected ( Object p0 ) {
	 /* .locals 1 */
	 /* .param p0, "nudState" # S */
	 /* .line 62 */
	 /* and-int/lit16 v0, p0, 0xc2 */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 int v0 = 1; // const/4 v0, 0x1
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static Boolean isNudStateValid ( Object p0 ) {
/* .locals 1 */
/* .param p0, "nudState" # S */
/* .line 66 */
v0 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg .isNudStateConnected ( p0 );
/* if-nez v0, :cond_1 */
/* and-int/lit8 v0, p0, 0x1c */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg parse ( java.nio.ByteBuffer p0 ) {
/* .locals 4 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 106 */
v0 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg .hasAvailableSpace ( p0 );
/* if-nez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 111 */
} // :cond_0
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg; */
/* invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;-><init>()V */
/* .line 112 */
/* .local v0, "struct":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg; */
v1 = (( java.nio.ByteBuffer ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B
/* iput-byte v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_family:B */
/* .line 113 */
v1 = (( java.nio.ByteBuffer ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B
/* .line 114 */
/* .local v1, "pad1":B */
v2 = (( java.nio.ByteBuffer ) p0 ).getShort ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S
/* .line 115 */
/* .local v2, "pad2":S */
v3 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v3, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_ifindex:I */
/* .line 116 */
v3 = (( java.nio.ByteBuffer ) p0 ).getShort ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S
/* iput-short v3, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_state:S */
/* .line 117 */
v3 = (( java.nio.ByteBuffer ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B
/* iput-byte v3, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_flags:B */
/* .line 118 */
v3 = (( java.nio.ByteBuffer ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B
/* iput-byte v3, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_type:B */
/* .line 119 */
} // .end method
public static java.lang.String stringForNudFlags ( Object p0 ) {
/* .locals 3 */
/* .param p0, "flags" # B */
/* .line 78 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 79 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* sget-byte v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->NTF_USE:B */
/* and-int/2addr v1, p0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 80 */
final String v1 = "NTF_USE"; // const-string v1, "NTF_USE"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 82 */
} // :cond_0
/* sget-byte v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->NTF_SELF:B */
/* and-int/2addr v1, p0 */
/* const-string/jumbo v2, "|" */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 83 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_1 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 84 */
} // :cond_1
final String v1 = "NTF_SELF"; // const-string v1, "NTF_SELF"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 86 */
} // :cond_2
/* sget-byte v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->NTF_MASTER:B */
/* and-int/2addr v1, p0 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 87 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_3 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 88 */
} // :cond_3
final String v1 = "NTF_MASTER"; // const-string v1, "NTF_MASTER"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 90 */
} // :cond_4
/* sget-byte v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->NTF_PROXY:B */
/* and-int/2addr v1, p0 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 91 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_5 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 93 */
} // :cond_5
final String v1 = "NTF_PROXY"; // const-string v1, "NTF_PROXY"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 94 */
} // :cond_6
/* sget-byte v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->NTF_ROUTER:B */
/* and-int/2addr v1, p0 */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 95 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_7 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 96 */
} // :cond_7
final String v1 = "NTF_ROUTER"; // const-string v1, "NTF_ROUTER"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 98 */
} // :cond_8
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static java.lang.String stringForNudState ( Object p0 ) {
/* .locals 2 */
/* .param p0, "nudState" # S */
/* .line 46 */
/* sparse-switch p0, :sswitch_data_0 */
/* .line 57 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "unknown NUD state: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 55 */
/* :sswitch_0 */
final String v0 = "NUD_PERMANENT"; // const-string v0, "NUD_PERMANENT"
/* .line 54 */
/* :sswitch_1 */
final String v0 = "NUD_NOARP"; // const-string v0, "NUD_NOARP"
/* .line 53 */
/* :sswitch_2 */
final String v0 = "NUD_FAILED"; // const-string v0, "NUD_FAILED"
/* .line 52 */
/* :sswitch_3 */
final String v0 = "NUD_PROBE"; // const-string v0, "NUD_PROBE"
/* .line 51 */
/* :sswitch_4 */
final String v0 = "NUD_DELAY"; // const-string v0, "NUD_DELAY"
/* .line 50 */
/* :sswitch_5 */
final String v0 = "NUD_STALE"; // const-string v0, "NUD_STALE"
/* .line 49 */
/* :sswitch_6 */
final String v0 = "NUD_REACHABLE"; // const-string v0, "NUD_REACHABLE"
/* .line 48 */
/* :sswitch_7 */
final String v0 = "NUD_INCOMPLETE"; // const-string v0, "NUD_INCOMPLETE"
/* .line 47 */
/* :sswitch_8 */
final String v0 = "NUD_NONE"; // const-string v0, "NUD_NONE"
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x0 -> :sswitch_8 */
/* 0x1 -> :sswitch_7 */
/* 0x2 -> :sswitch_6 */
/* 0x4 -> :sswitch_5 */
/* 0x8 -> :sswitch_4 */
/* 0x10 -> :sswitch_3 */
/* 0x20 -> :sswitch_2 */
/* 0x40 -> :sswitch_1 */
/* 0x80 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
/* # virtual methods */
public Boolean nudConnected ( ) {
/* .locals 1 */
/* .line 146 */
/* iget-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_state:S */
v0 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg .isNudStateConnected ( v0 );
} // .end method
public Boolean nudValid ( ) {
/* .locals 1 */
/* .line 150 */
/* iget-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_state:S */
v0 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg .isNudStateValid ( v0 );
} // .end method
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 1 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 136 */
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_family:B */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 137 */
int v0 = 0; // const/4 v0, 0x0
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 138 */
(( java.nio.ByteBuffer ) p1 ).putShort ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 139 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_ifindex:I */
(( java.nio.ByteBuffer ) p1 ).putInt ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
/* .line 140 */
/* iget-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_state:S */
(( java.nio.ByteBuffer ) p1 ).putShort ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 141 */
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_flags:B */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 142 */
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_type:B */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 143 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 5 */
/* .line 155 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = ""; // const-string v1, ""
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_state:S */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " ("; // const-string v2, " ("
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_state:S */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg .stringForNudState ( v3 );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ")"; // const-string v3, ")"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 156 */
/* .local v0, "stateStr":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_flags:B */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_flags:B */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdMsg .stringForNudFlags ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 157 */
/* .local v1, "flagsStr":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "StructNdMsg{ family{"; // const-string v3, "StructNdMsg{ family{"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_family:B */
/* .line 158 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .stringForAddressFamily ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, ifindex{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_ifindex:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, state{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, flags{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, type{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdMsg;->ndm_type:B */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "} }" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 157 */
} // .end method
