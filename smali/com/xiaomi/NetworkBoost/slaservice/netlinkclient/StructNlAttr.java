public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr {
	 /* .source "StructNlAttr.java" */
	 /* # static fields */
	 public static final Integer NLA_F_NESTED;
	 public static final Integer NLA_HEADERLEN;
	 /* # instance fields */
	 public Object nla_len;
	 public Object nla_type;
	 public nla_value;
	 /* # direct methods */
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ( ) {
		 /* .locals 1 */
		 /* .line 121 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 117 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* iput-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_len:S */
		 /* .line 121 */
		 return;
	 } // .end method
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ( ) {
		 /* .locals 2 */
		 /* .param p1, "type" # S */
		 /* .param p2, "value" # B */
		 /* .line 123 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 117 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* iput-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_len:S */
		 /* .line 124 */
		 /* iput-short p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_type:S */
		 /* .line 125 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* new-array v0, v0, [B */
		 /* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->setValue([B)V */
		 /* .line 126 */
		 v0 = this.nla_value;
		 int v1 = 0; // const/4 v1, 0x0
		 /* aput-byte p2, v0, v1 */
		 /* .line 127 */
		 return;
	 } // .end method
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ( ) {
		 /* .locals 1 */
		 /* .param p1, "type" # S */
		 /* .param p2, "value" # I */
		 /* .line 147 */
		 java.nio.ByteOrder .nativeOrder ( );
		 /* invoke-direct {p0, p1, p2, v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>(SILjava/nio/ByteOrder;)V */
		 /* .line 148 */
		 return;
	 } // .end method
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ( ) {
		 /* .locals 3 */
		 /* .param p1, "type" # S */
		 /* .param p2, "value" # I */
		 /* .param p3, "order" # Ljava/nio/ByteOrder; */
		 /* .line 150 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 117 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* iput-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_len:S */
		 /* .line 151 */
		 /* iput-short p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_type:S */
		 /* .line 152 */
		 /* new-array v0, v0, [B */
		 /* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->setValue([B)V */
		 /* .line 153 */
		 (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
		 /* .line 154 */
		 /* .local v0, "buf":Ljava/nio/ByteBuffer; */
		 (( java.nio.ByteBuffer ) v0 ).order ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
		 /* .line 156 */
		 /* .local v1, "originalOrder":Ljava/nio/ByteOrder; */
		 try { // :try_start_0
			 (( java.nio.ByteBuffer ) v0 ).order ( p3 ); // invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
			 /* .line 157 */
			 (( java.nio.ByteBuffer ) v0 ).putInt ( p2 ); // invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* .line 159 */
			 (( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
			 /* .line 160 */
			 /* nop */
			 /* .line 161 */
			 return;
			 /* .line 159 */
			 /* :catchall_0 */
			 /* move-exception v2 */
			 (( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
			 /* .line 160 */
			 /* throw v2 */
		 } // .end method
		 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ( ) {
			 /* .locals 1 */
			 /* .param p1, "type" # S */
			 /* .param p2, "ip" # Ljava/net/InetAddress; */
			 /* .line 163 */
			 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
			 /* .line 117 */
			 int v0 = 4; // const/4 v0, 0x4
			 /* iput-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_len:S */
			 /* .line 164 */
			 /* iput-short p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_type:S */
			 /* .line 165 */
			 (( java.net.InetAddress ) p2 ).getAddress ( ); // invoke-virtual {p2}, Ljava/net/InetAddress;->getAddress()[B
			 /* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->setValue([B)V */
			 /* .line 166 */
			 return;
		 } // .end method
		 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ( ) {
			 /* .locals 1 */
			 /* .param p1, "type" # S */
			 /* .param p2, "value" # S */
			 /* .line 130 */
			 java.nio.ByteOrder .nativeOrder ( );
			 /* invoke-direct {p0, p1, p2, v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>(SSLjava/nio/ByteOrder;)V */
			 /* .line 131 */
			 return;
		 } // .end method
		 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ( ) {
			 /* .locals 3 */
			 /* .param p1, "type" # S */
			 /* .param p2, "value" # S */
			 /* .param p3, "order" # Ljava/nio/ByteOrder; */
			 /* .line 133 */
			 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
			 /* .line 117 */
			 int v0 = 4; // const/4 v0, 0x4
			 /* iput-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_len:S */
			 /* .line 134 */
			 /* iput-short p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_type:S */
			 /* .line 135 */
			 int v0 = 2; // const/4 v0, 0x2
			 /* new-array v0, v0, [B */
			 /* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->setValue([B)V */
			 /* .line 136 */
			 (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
			 /* .line 137 */
			 /* .local v0, "buf":Ljava/nio/ByteBuffer; */
			 (( java.nio.ByteBuffer ) v0 ).order ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
			 /* .line 139 */
			 /* .local v1, "originalOrder":Ljava/nio/ByteOrder; */
			 try { // :try_start_0
				 (( java.nio.ByteBuffer ) v0 ).order ( p3 ); // invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
				 /* .line 140 */
				 (( java.nio.ByteBuffer ) v0 ).putShort ( p2 ); // invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
				 /* :try_end_0 */
				 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
				 /* .line 142 */
				 (( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
				 /* .line 143 */
				 /* nop */
				 /* .line 144 */
				 return;
				 /* .line 142 */
				 /* :catchall_0 */
				 /* move-exception v2 */
				 (( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
				 /* .line 143 */
				 /* throw v2 */
			 } // .end method
			 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ( ) {
				 /* .locals 6 */
				 /* .param p1, "type" # S */
				 /* .param p2, "nested" # [Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
				 /* .line 169 */
				 /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>()V */
				 /* .line 170 */
				 v0 = 				 com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .makeNestedType ( p1 );
				 /* iput-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_type:S */
				 /* .line 172 */
				 int v0 = 0; // const/4 v0, 0x0
				 /* .line 173 */
				 /* .local v0, "payloadLength":I */
				 /* array-length v1, p2 */
				 int v2 = 0; // const/4 v2, 0x0
				 /* move v3, v2 */
			 } // :goto_0
			 /* if-ge v3, v1, :cond_0 */
			 /* aget-object v4, p2, v3 */
			 /* .local v4, "nla":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
			 v5 = 			 (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v4 ).getAlignedLength ( ); // invoke-virtual {v4}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getAlignedLength()I
			 /* add-int/2addr v0, v5 */
		 } // .end local v4 # "nla":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;
		 /* add-int/lit8 v3, v3, 0x1 */
		 /* .line 174 */
	 } // :cond_0
	 /* new-array v1, v0, [B */
	 /* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->setValue([B)V */
	 /* .line 176 */
	 (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
	 /* .line 177 */
	 /* .local v1, "buf":Ljava/nio/ByteBuffer; */
	 /* array-length v3, p2 */
} // :goto_1
/* if-ge v2, v3, :cond_1 */
/* aget-object v4, p2, v2 */
/* .line 178 */
/* .restart local v4 # "nla":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v4 ).pack ( v1 ); // invoke-virtual {v4, v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 177 */
} // .end local v4 # "nla":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 180 */
} // :cond_1
return;
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr findNextAttrOfType ( Object p0, java.nio.ByteBuffer p1 ) {
/* .locals 3 */
/* .param p0, "attrType" # S */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 101 */
/* nop */
} // :goto_0
if ( p1 != null) { // if-eqz p1, :cond_3
v0 = (( java.nio.ByteBuffer ) p1 ).remaining ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I
/* if-lez v0, :cond_3 */
/* .line 102 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .peek ( p1 );
/* .line 103 */
/* .local v0, "nlAttr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
/* if-nez v0, :cond_0 */
/* .line 104 */
/* .line 106 */
} // :cond_0
/* iget-short v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_type:S */
/* if-ne v1, p0, :cond_1 */
/* .line 107 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .parse ( p1 );
/* .line 109 */
} // :cond_1
v1 = (( java.nio.ByteBuffer ) p1 ).remaining ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I
v2 = (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v0 ).getAlignedLength ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getAlignedLength()I
/* if-ge v1, v2, :cond_2 */
/* .line 110 */
/* .line 112 */
} // :cond_2
v1 = (( java.nio.ByteBuffer ) p1 ).position ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I
v2 = (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v0 ).getAlignedLength ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getAlignedLength()I
/* add-int/2addr v1, v2 */
(( java.nio.ByteBuffer ) p1 ).position ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 113 */
} // .end local v0 # "nlAttr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;
/* .line 114 */
} // :cond_3
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Object makeNestedType ( Object p0 ) {
/* .locals 1 */
/* .param p0, "type" # S */
/* .line 40 */
/* const v0, 0x8000 */
/* or-int/2addr v0, p0 */
/* int-to-short v0, v0 */
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr parse ( java.nio.ByteBuffer p0 ) {
/* .locals 5 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 72 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .peek ( p0 );
/* .line 73 */
/* .local v0, "struct":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
if ( v0 != null) { // if-eqz v0, :cond_2
v1 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
v2 = (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v0 ).getAlignedLength ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getAlignedLength()I
/* if-ge v1, v2, :cond_0 */
/* .line 77 */
} // :cond_0
v1 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
/* .line 78 */
/* .local v1, "baseOffset":I */
/* add-int/lit8 v2, v1, 0x4 */
(( java.nio.ByteBuffer ) p0 ).position ( v2 ); // invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 80 */
/* iget-short v2, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_len:S */
/* const v3, 0xffff */
/* and-int/2addr v2, v3 */
/* .line 81 */
/* .local v2, "valueLen":I */
/* add-int/lit8 v2, v2, -0x4 */
/* .line 82 */
/* if-lez v2, :cond_1 */
/* .line 83 */
/* new-array v3, v2, [B */
this.nla_value = v3;
/* .line 84 */
int v4 = 0; // const/4 v4, 0x0
(( java.nio.ByteBuffer ) p0 ).get ( v3, v4, v2 ); // invoke-virtual {p0, v3, v4, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;
/* .line 85 */
v3 = (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) v0 ).getAlignedLength ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getAlignedLength()I
/* add-int/2addr v3, v1 */
(( java.nio.ByteBuffer ) p0 ).position ( v3 ); // invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 87 */
} // :cond_1
/* .line 74 */
} // .end local v1 # "baseOffset":I
} // .end local v2 # "valueLen":I
} // :cond_2
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr peek ( java.nio.ByteBuffer p0 ) {
/* .locals 6 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 48 */
int v0 = 0; // const/4 v0, 0x0
if ( p0 != null) { // if-eqz p0, :cond_2
v1 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
int v2 = 4; // const/4 v2, 0x4
/* if-ge v1, v2, :cond_0 */
/* .line 51 */
} // :cond_0
v1 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
/* .line 53 */
/* .local v1, "baseOffset":I */
/* new-instance v3, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
/* invoke-direct {v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;-><init>()V */
/* .line 54 */
/* .local v3, "struct":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
(( java.nio.ByteBuffer ) p0 ).order ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
/* .line 55 */
/* .local v4, "originalOrder":Ljava/nio/ByteOrder; */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) p0 ).order ( v5 ); // invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 57 */
try { // :try_start_0
v5 = (( java.nio.ByteBuffer ) p0 ).getShort ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S
/* iput-short v5, v3, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_len:S */
/* .line 58 */
v5 = (( java.nio.ByteBuffer ) p0 ).getShort ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S
/* iput-short v5, v3, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_type:S */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 60 */
(( java.nio.ByteBuffer ) p0 ).order ( v4 ); // invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 61 */
/* nop */
/* .line 63 */
(( java.nio.ByteBuffer ) p0 ).position ( v1 ); // invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 64 */
/* iget-short v5, v3, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_len:S */
/* if-ge v5, v2, :cond_1 */
/* .line 66 */
/* .line 68 */
} // :cond_1
/* .line 60 */
/* :catchall_0 */
/* move-exception v0 */
(( java.nio.ByteBuffer ) p0 ).order ( v4 ); // invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 61 */
/* throw v0 */
/* .line 49 */
} // .end local v1 # "baseOffset":I
} // .end local v3 # "struct":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;
} // .end local v4 # "originalOrder":Ljava/nio/ByteOrder;
} // :cond_2
} // :goto_0
} // .end method
private void setValue ( Object[] p0 ) {
/* .locals 1 */
/* .param p1, "value" # [B */
/* .line 272 */
this.nla_value = p1;
/* .line 273 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* array-length v0, p1 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* add-int/lit8 v0, v0, 0x4 */
/* int-to-short v0, v0 */
/* iput-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_len:S */
/* .line 274 */
return;
} // .end method
/* # virtual methods */
public Integer getAlignedLength ( ) {
/* .locals 1 */
/* .line 183 */
/* iget-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_len:S */
v0 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .alignedLengthOf ( v0 );
} // .end method
public Object getValueAsBe16 ( Object p0 ) {
/* .locals 3 */
/* .param p1, "defaultValue" # S */
/* .line 190 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
/* .line 191 */
/* .local v0, "byteBuffer":Ljava/nio/ByteBuffer; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.nio.ByteBuffer ) v0 ).remaining ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I
int v2 = 2; // const/4 v2, 0x2
/* if-eq v1, v2, :cond_0 */
/* .line 194 */
} // :cond_0
(( java.nio.ByteBuffer ) v0 ).order ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
/* .line 196 */
/* .local v1, "originalOrder":Ljava/nio/ByteOrder; */
try { // :try_start_0
v2 = java.nio.ByteOrder.BIG_ENDIAN;
(( java.nio.ByteBuffer ) v0 ).order ( v2 ); // invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 197 */
v2 = (( java.nio.ByteBuffer ) v0 ).getShort ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 199 */
(( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 197 */
/* .line 199 */
/* :catchall_0 */
/* move-exception v2 */
(( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 200 */
/* throw v2 */
/* .line 192 */
} // .end local v1 # "originalOrder":Ljava/nio/ByteOrder;
} // :cond_1
} // :goto_0
} // .end method
public Integer getValueAsBe32 ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "defaultValue" # I */
/* .line 204 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
/* .line 205 */
/* .local v0, "byteBuffer":Ljava/nio/ByteBuffer; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.nio.ByteBuffer ) v0 ).remaining ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I
int v2 = 4; // const/4 v2, 0x4
/* if-eq v1, v2, :cond_0 */
/* .line 208 */
} // :cond_0
(( java.nio.ByteBuffer ) v0 ).order ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
/* .line 210 */
/* .local v1, "originalOrder":Ljava/nio/ByteOrder; */
try { // :try_start_0
v2 = java.nio.ByteOrder.BIG_ENDIAN;
(( java.nio.ByteBuffer ) v0 ).order ( v2 ); // invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 211 */
v2 = (( java.nio.ByteBuffer ) v0 ).getInt ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 213 */
(( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 211 */
/* .line 213 */
/* :catchall_0 */
/* move-exception v2 */
(( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 214 */
/* throw v2 */
/* .line 206 */
} // .end local v1 # "originalOrder":Ljava/nio/ByteOrder;
} // :cond_1
} // :goto_0
} // .end method
public Object getValueAsByte ( Object p0 ) {
/* .locals 3 */
/* .param p1, "defaultValue" # B */
/* .line 231 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
/* .line 232 */
/* .local v0, "byteBuffer":Ljava/nio/ByteBuffer; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.nio.ByteBuffer ) v0 ).remaining ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I
int v2 = 1; // const/4 v2, 0x1
/* if-eq v1, v2, :cond_0 */
/* .line 235 */
} // :cond_0
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
v1 = (( java.nio.ByteBuffer ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/nio/ByteBuffer;->get()B
/* .line 233 */
} // :cond_1
} // :goto_0
} // .end method
public java.nio.ByteBuffer getValueAsByteBuffer ( ) {
/* .locals 2 */
/* .line 218 */
v0 = this.nla_value;
/* if-nez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 219 */
} // :cond_0
java.nio.ByteBuffer .wrap ( v0 );
/* .line 223 */
/* .local v0, "byteBuffer":Ljava/nio/ByteBuffer; */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 224 */
} // .end method
public java.net.InetAddress getValueAsInetAddress ( ) {
/* .locals 2 */
/* .line 247 */
v0 = this.nla_value;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 250 */
} // :cond_0
try { // :try_start_0
java.net.InetAddress .getByAddress ( v0 );
/* :try_end_0 */
/* .catch Ljava/net/UnknownHostException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 251 */
/* :catch_0 */
/* move-exception v0 */
/* .line 252 */
/* .local v0, "ignored":Ljava/net/UnknownHostException; */
} // .end method
public Integer getValueAsInt ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "defaultValue" # I */
/* .line 239 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
/* .line 240 */
/* .local v0, "byteBuffer":Ljava/nio/ByteBuffer; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.nio.ByteBuffer ) v0 ).remaining ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I
int v2 = 4; // const/4 v2, 0x4
/* if-eq v1, v2, :cond_0 */
/* .line 243 */
} // :cond_0
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
v1 = (( java.nio.ByteBuffer ) v1 ).getInt ( ); // invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I
/* .line 241 */
} // :cond_1
} // :goto_0
} // .end method
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 3 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 257 */
(( java.nio.ByteBuffer ) p1 ).order ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
/* .line 258 */
/* .local v0, "originalOrder":Ljava/nio/ByteOrder; */
v1 = (( java.nio.ByteBuffer ) p1 ).position ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I
/* .line 260 */
/* .local v1, "originalPosition":I */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) p1 ).order ( v2 ); // invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 262 */
try { // :try_start_0
/* iget-short v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_len:S */
(( java.nio.ByteBuffer ) p1 ).putShort ( v2 ); // invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 263 */
/* iget-short v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_type:S */
(( java.nio.ByteBuffer ) p1 ).putShort ( v2 ); // invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 264 */
v2 = this.nla_value;
if ( v2 != null) { // if-eqz v2, :cond_0
(( java.nio.ByteBuffer ) p1 ).put ( v2 ); // invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 266 */
} // :cond_0
(( java.nio.ByteBuffer ) p1 ).order ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 267 */
/* nop */
/* .line 268 */
v2 = (( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr ) p0 ).getAlignedLength ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->getAlignedLength()I
/* add-int/2addr v2, v1 */
(( java.nio.ByteBuffer ) p1 ).position ( v2 ); // invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 269 */
return;
/* .line 266 */
/* :catchall_0 */
/* move-exception v2 */
(( java.nio.ByteBuffer ) p1 ).order ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 267 */
/* throw v2 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 278 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "StructNlAttr{ nla_len{"; // const-string v1, "StructNlAttr{ nla_len{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_len:S */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, nla_type{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_type:S */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, nla_value{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.nla_value;
/* .line 281 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .hexify ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, }" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 278 */
} // .end method
