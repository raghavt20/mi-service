public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo {
	 /* .source "StructNdaCacheInfo.java" */
	 /* # static fields */
	 private static final Long CLOCK_TICKS_PER_SECOND;
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public Integer ndm_confirmed;
	 public Integer ndm_refcnt;
	 public Integer ndm_updated;
	 public Integer ndm_used;
	 /* # direct methods */
	 static com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo ( ) {
		 /* .locals 2 */
		 /* .line 56 */
		 android.system.Os .sysconf ( v0 );
		 /* move-result-wide v0 */
		 /* sput-wide v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->CLOCK_TICKS_PER_SECOND:J */
		 return;
	 } // .end method
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo ( ) {
		 /* .locals 0 */
		 /* .line 94 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static Boolean hasAvailableSpace ( java.nio.ByteBuffer p0 ) {
		 /* .locals 2 */
		 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .line 37 */
		 if ( p0 != null) { // if-eqz p0, :cond_0
			 v0 = 			 (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
			 /* const/16 v1, 0x10 */
			 /* if-lt v0, v1, :cond_0 */
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :goto_0
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo parse ( java.nio.ByteBuffer p0 ) {
	 /* .locals 2 */
	 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
	 /* .line 41 */
	 v0 = 	 com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo .hasAvailableSpace ( p0 );
	 /* if-nez v0, :cond_0 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 46 */
} // :cond_0
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo; */
/* invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;-><init>()V */
/* .line 47 */
/* .local v0, "struct":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo; */
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->ndm_used:I */
/* .line 48 */
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->ndm_confirmed:I */
/* .line 49 */
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->ndm_updated:I */
/* .line 50 */
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v1, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->ndm_refcnt:I */
/* .line 51 */
} // .end method
private static Long ticksToMilliSeconds ( Integer p0 ) {
/* .locals 6 */
/* .param p0, "intClockTicks" # I */
/* .line 59 */
/* int-to-long v0, p0 */
/* const-wide/16 v2, -0x1 */
/* and-long/2addr v0, v2 */
/* .line 60 */
/* .local v0, "longClockTicks":J */
/* const-wide/16 v2, 0x3e8 */
/* mul-long/2addr v2, v0 */
/* sget-wide v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->CLOCK_TICKS_PER_SECOND:J */
/* div-long/2addr v2, v4 */
/* return-wide v2 */
} // .end method
/* # virtual methods */
public Long lastConfirmed ( ) {
/* .locals 2 */
/* .line 101 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->ndm_confirmed:I */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo .ticksToMilliSeconds ( v0 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Long lastUpdated ( ) {
/* .locals 2 */
/* .line 105 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->ndm_updated:I */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo .ticksToMilliSeconds ( v0 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Long lastUsed ( ) {
/* .locals 2 */
/* .line 97 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->ndm_used:I */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo .ticksToMilliSeconds ( v0 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 110 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "NdaCacheInfo{ ndm_used{"; // const-string v1, "NdaCacheInfo{ ndm_used{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 111 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo ) p0 ).lastUsed ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->lastUsed()J
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, ndm_confirmed{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 112 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo ) p0 ).lastConfirmed ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->lastConfirmed()J
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, ndm_updated{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 113 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdaCacheInfo ) p0 ).lastUpdated ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->lastUpdated()J
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, ndm_refcnt{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNdaCacheInfo;->ndm_refcnt:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "} }" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 110 */
} // .end method
