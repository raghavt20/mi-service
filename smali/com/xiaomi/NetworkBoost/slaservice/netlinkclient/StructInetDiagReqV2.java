public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructInetDiagReqV2 {
	 /* .source "StructInetDiagReqV2.java" */
	 /* # static fields */
	 public static final Integer INET_DIAG_REQ_V2_ALL_STATES;
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 private final com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructInetDiagSockId mId;
	 private final Object mIdiagExt;
	 private final Object mPad;
	 private final Object mSdiagFamily;
	 private final Object mSdiagProtocol;
	 private final Integer mState;
	 /* # direct methods */
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructInetDiagReqV2 ( ) {
		 /* .locals 8 */
		 /* .param p1, "protocol" # I */
		 /* .param p2, "local" # Ljava/net/InetSocketAddress; */
		 /* .param p3, "remote" # Ljava/net/InetSocketAddress; */
		 /* .param p4, "family" # I */
		 /* .line 53 */
		 int v5 = 0; // const/4 v5, 0x0
		 int v6 = 0; // const/4 v6, 0x0
		 int v7 = -1; // const/4 v7, -0x1
		 /* move-object v0, p0 */
		 /* move v1, p1 */
		 /* move-object v2, p2 */
		 /* move-object v3, p3 */
		 /* move v4, p4 */
		 /* invoke-direct/range {v0 ..v7}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;-><init>(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;IIII)V */
		 /* .line 55 */
		 return;
	 } // .end method
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructInetDiagReqV2 ( ) {
		 /* .locals 3 */
		 /* .param p1, "protocol" # I */
		 /* .param p2, "local" # Ljava/net/InetSocketAddress; */
		 /* .param p3, "remote" # Ljava/net/InetSocketAddress; */
		 /* .param p4, "family" # I */
		 /* .param p5, "pad" # I */
		 /* .param p6, "extension" # I */
		 /* .param p7, "state" # I */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Ljava/lang/NullPointerException; */
		 /* } */
	 } // .end annotation
	 /* .line 59 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 60 */
	 /* int-to-byte v0, p4 */
	 /* iput-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mSdiagFamily:B */
	 /* .line 61 */
	 /* int-to-byte v0, p1 */
	 /* iput-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mSdiagProtocol:B */
	 /* .line 64 */
	 int v0 = 1; // const/4 v0, 0x1
	 int v1 = 0; // const/4 v1, 0x0
	 /* if-nez p2, :cond_0 */
	 /* move v2, v0 */
} // :cond_0
/* move v2, v1 */
} // :goto_0
/* if-nez p3, :cond_1 */
} // :cond_1
/* move v0, v1 */
} // :goto_1
/* if-ne v2, v0, :cond_3 */
/* .line 67 */
if ( p2 != null) { // if-eqz p2, :cond_2
if ( p3 != null) { // if-eqz p3, :cond_2
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagSockId; */
/* invoke-direct {v0, p2, p3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagSockId;-><init>(Ljava/net/InetSocketAddress;Ljava/net/InetSocketAddress;)V */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // :goto_2
this.mId = v0;
/* .line 68 */
/* int-to-byte v0, p5 */
/* iput-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mPad:B */
/* .line 69 */
/* int-to-byte v0, p6 */
/* iput-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mIdiagExt:B */
/* .line 70 */
/* iput p7, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mState:I */
/* .line 71 */
return;
/* .line 65 */
} // :cond_3
/* new-instance v0, Ljava/lang/NullPointerException; */
final String v1 = "Local and remote must be both null or both non-null"; // const-string v1, "Local and remote must be both null or both non-null"
/* invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
/* # virtual methods */
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 1 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 75 */
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mSdiagFamily:B */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 76 */
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mSdiagProtocol:B */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 77 */
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mIdiagExt:B */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 78 */
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mPad:B */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 79 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mState:I */
(( java.nio.ByteBuffer ) p1 ).putInt ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
/* .line 80 */
v0 = this.mId;
if ( v0 != null) { // if-eqz v0, :cond_0
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructInetDiagSockId ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagSockId;->pack(Ljava/nio/ByteBuffer;)V
/* .line 81 */
} // :cond_0
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 4 */
/* .line 85 */
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mSdiagFamily:B */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .stringForAddressFamily ( v0 );
/* .line 86 */
/* .local v0, "familyStr":Ljava/lang/String; */
/* iget-byte v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mSdiagProtocol:B */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .stringForAddressFamily ( v1 );
/* .line 88 */
/* .local v1, "protocolStr":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "StructInetDiagReqV2{ sdiag_family{"; // const-string v3, "StructInetDiagReqV2{ sdiag_family{"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, sdiag_protocol{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, idiag_ext{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mIdiagExt:B */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ")}, pad{"; // const-string v3, ")}, pad{"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mPad:B */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, idiag_states{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagReqV2;->mState:I */
/* .line 93 */
java.lang.Integer .toHexString ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 94 */
v3 = this.mId;
if ( v3 != null) { // if-eqz v3, :cond_0
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructInetDiagSockId ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructInetDiagSockId;->toString()Ljava/lang/String;
} // :cond_0
final String v3 = "inet_diag_sockid=null"; // const-string v3, "inet_diag_sockid=null"
} // :goto_0
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 88 */
} // .end method
