public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.NdOption {
	 /* .source "NdOption.java" */
	 /* # static fields */
	 public static final Integer STRUCT_SIZE;
	 public static final com.xiaomi.NetworkBoost.slaservice.netlinkclient.NdOption UNKNOWN;
	 /* # instance fields */
	 public final Integer length;
	 public final Object type;
	 /* # direct methods */
	 static com.xiaomi.NetworkBoost.slaservice.netlinkclient.NdOption ( ) {
		 /* .locals 2 */
		 /* .line 82 */
		 /* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NdOption; */
		 int v1 = 0; // const/4 v1, 0x0
		 /* invoke-direct {v0, v1, v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NdOption;-><init>(BI)V */
		 return;
	 } // .end method
	 com.xiaomi.NetworkBoost.slaservice.netlinkclient.NdOption ( ) {
		 /* .locals 0 */
		 /* .param p1, "type" # B */
		 /* .param p2, "length" # I */
		 /* .line 33 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 34 */
		 /* iput-byte p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NdOption;->type:B */
		 /* .line 35 */
		 /* iput p2, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NdOption;->length:I */
		 /* .line 36 */
		 return;
	 } // .end method
	 public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.NdOption parse ( java.nio.ByteBuffer p0 ) {
		 /* .locals 5 */
		 /* .param p0, "buf" # Ljava/nio/ByteBuffer; */
		 /* .line 54 */
		 int v0 = 0; // const/4 v0, 0x0
		 if ( p0 != null) { // if-eqz p0, :cond_2
			 v1 = 			 (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
			 int v2 = 2; // const/4 v2, 0x2
			 /* if-ge v1, v2, :cond_0 */
			 /* .line 57 */
		 } // :cond_0
		 v1 = 		 (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
		 v1 = 		 (( java.nio.ByteBuffer ) p0 ).get ( v1 ); // invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->get(I)B
		 /* .line 58 */
		 /* .local v1, "type":B */
		 v2 = 		 (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
		 /* add-int/lit8 v2, v2, 0x1 */
		 v2 = 		 (( java.nio.ByteBuffer ) p0 ).get ( v2 ); // invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->get(I)B
		 v2 = 		 java.lang.Byte .toUnsignedInt ( v2 );
		 /* .line 59 */
		 /* .local v2, "length":I */
		 /* if-nez v2, :cond_1 */
		 /* .line 61 */
	 } // :cond_1
	 /* packed-switch v1, :pswitch_data_0 */
	 /* .line 66 */
	 v0 = 	 (( java.nio.ByteBuffer ) p0 ).limit ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I
	 v3 = 	 (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
	 /* mul-int/lit8 v4, v2, 0x8 */
	 /* add-int/2addr v3, v4 */
	 v0 = 	 java.lang.Math .min ( v0,v3 );
	 /* .line 67 */
	 /* .local v0, "newPosition":I */
	 (( java.nio.ByteBuffer ) p0 ).position ( v0 ); // invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
	 /* .line 68 */
	 v3 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.NdOption.UNKNOWN;
	 /* .line 63 */
} // .end local v0 # "newPosition":I
/* :pswitch_0 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNdOptPref64 .parse ( p0 );
/* .line 54 */
} // .end local v1 # "type":B
} // .end local v2 # "length":I
} // :cond_2
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x26 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 79 */
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NdOption;->type:B */
v0 = java.lang.Byte .toUnsignedInt ( v0 );
java.lang.Integer .valueOf ( v0 );
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NdOption;->length:I */
java.lang.Integer .valueOf ( v1 );
/* filled-new-array {v0, v1}, [Ljava/lang/Object; */
final String v1 = "NdOption(%d, %d)"; // const-string v1, "NdOption(%d, %d)"
java.lang.String .format ( v1,v0 );
} // .end method
void writeToByteBuffer ( java.nio.ByteBuffer p0 ) {
/* .locals 1 */
/* .param p1, "buf" # Ljava/nio/ByteBuffer; */
/* .line 73 */
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NdOption;->type:B */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 74 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NdOption;->length:I */
/* int-to-byte v0, v0 */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 75 */
return;
} // .end method
