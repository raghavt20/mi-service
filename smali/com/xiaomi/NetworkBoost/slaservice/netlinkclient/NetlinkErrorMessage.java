public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkErrorMessage extends com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage {
	 /* .source "NetlinkErrorMessage.java" */
	 /* # instance fields */
	 private com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgErr mNlMsgErr;
	 /* # direct methods */
	 com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkErrorMessage ( ) {
		 /* .locals 1 */
		 /* .param p1, "header" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
		 /* .line 43 */
		 /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V */
		 /* .line 44 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mNlMsgErr = v0;
		 /* .line 45 */
		 return;
	 } // .end method
	 public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkErrorMessage parse ( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
		 /* .locals 2 */
		 /* .param p0, "header" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
		 /* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .line 30 */
		 /* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage; */
		 /* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V */
		 /* .line 32 */
		 /* .local v0, "errorMsg":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage; */
		 com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgErr .parse ( p1 );
		 this.mNlMsgErr = v1;
		 /* .line 33 */
		 /* if-nez v1, :cond_0 */
		 /* .line 34 */
		 int v1 = 0; // const/4 v1, 0x0
		 /* .line 37 */
	 } // :cond_0
} // .end method
/* # virtual methods */
public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgErr getNlMsgError ( ) {
	 /* .locals 1 */
	 /* .line 48 */
	 v0 = this.mNlMsgErr;
} // .end method
public java.lang.String toString ( ) {
	 /* .locals 3 */
	 /* .line 53 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "NetlinkErrorMessage{ nlmsghdr{"; // const-string v1, "NetlinkErrorMessage{ nlmsghdr{"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 54 */
	 v1 = this.mHeader;
	 final String v2 = ""; // const-string v2, ""
	 /* if-nez v1, :cond_0 */
	 /* move-object v1, v2 */
} // :cond_0
v1 = this.mHeader;
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->toString()Ljava/lang/String;
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, nlmsgerr{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 55 */
v1 = this.mNlMsgErr;
/* if-nez v1, :cond_1 */
} // :cond_1
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgErr ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;->toString()Ljava/lang/String;
} // :goto_1
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "} }" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 53 */
} // .end method
