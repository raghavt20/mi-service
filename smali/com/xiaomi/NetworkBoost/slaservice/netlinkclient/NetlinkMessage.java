public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage {
	 /* .source "NetlinkMessage.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 protected com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr mHeader;
	 /* # direct methods */
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage ( ) {
		 /* .locals 0 */
		 /* .param p1, "nlmsghdr" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
		 /* .line 84 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 85 */
		 this.mHeader = p1;
		 /* .line 86 */
		 return;
	 } // .end method
	 public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage parse ( java.nio.ByteBuffer p0, Integer p1 ) {
		 /* .locals 6 */
		 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .param p1, "nlFamily" # I */
		 /* .line 48 */
		 if ( p0 != null) { // if-eqz p0, :cond_0
			 v0 = 			 (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
		 } // :cond_0
		 int v0 = -1; // const/4 v0, -0x1
		 /* .line 49 */
		 /* .local v0, "startPosition":I */
	 } // :goto_0
	 com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr .parse ( p0 );
	 /* .line 50 */
	 /* .local v1, "nlmsghdr":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
	 int v2 = 0; // const/4 v2, 0x0
	 /* if-nez v1, :cond_1 */
	 /* .line 51 */
	 /* .line 54 */
} // :cond_1
/* iget v3, v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_len:I */
v3 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .alignedLengthOf ( v3 );
/* .line 55 */
/* .local v3, "payloadLength":I */
/* add-int/lit8 v3, v3, -0x10 */
/* .line 56 */
/* if-ltz v3, :cond_7 */
v4 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
/* if-le v3, v4, :cond_2 */
/* .line 64 */
} // :cond_2
/* iget-short v4, v1, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
/* const/16 v5, 0xf */
/* if-gt v4, v5, :cond_3 */
/* .line 65 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage .parseCtlMessage ( v1,p0,v3 );
/* .line 71 */
} // :cond_3
/* if-ne p1, v4, :cond_4 */
/* .line 72 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage .parseRtMessage ( v1,p0 );
/* .line 73 */
} // :cond_4
/* if-ne p1, v4, :cond_5 */
/* .line 74 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage .parseInetDiagMessage ( v1,p0 );
/* .line 75 */
} // :cond_5
/* if-ne p1, v4, :cond_6 */
/* .line 76 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage .parseNfMessage ( v1,p0 );
/* .line 79 */
} // :cond_6
/* .line 58 */
} // :cond_7
} // :goto_1
v4 = (( java.nio.ByteBuffer ) p0 ).limit ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I
(( java.nio.ByteBuffer ) p0 ).position ( v4 ); // invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 59 */
} // .end method
private static com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage parseCtlMessage ( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr p0, java.nio.ByteBuffer p1, Integer p2 ) {
/* .locals 1 */
/* .param p0, "nlmsghdr" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .param p2, "payloadLength" # I */
/* .line 105 */
/* iget-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
/* packed-switch v0, :pswitch_data_0 */
/* .line 111 */
v0 = (( java.nio.ByteBuffer ) p1 ).position ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I
/* add-int/2addr v0, p2 */
(( java.nio.ByteBuffer ) p1 ).position ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 112 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V */
/* .line 107 */
/* :pswitch_0 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkErrorMessage .parse ( p0,p1 );
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage parseInetDiagMessage ( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
/* .locals 1 */
/* .param p0, "nlmsghdr" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 134 */
/* iget-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
/* packed-switch v0, :pswitch_data_0 */
/* .line 137 */
int v0 = 0; // const/4 v0, 0x0
/* .line 136 */
/* :pswitch_0 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.InetDiagMessage .parse ( p0,p1 );
/* :pswitch_data_0 */
/* .packed-switch 0x14 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage parseNfMessage ( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
/* .locals 1 */
/* .param p0, "nlmsghdr" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 144 */
/* iget-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
/* packed-switch v0, :pswitch_data_0 */
/* .line 150 */
/* :pswitch_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 149 */
/* :pswitch_1 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.ConntrackMessage .parse ( p0,p1 );
/* :pswitch_data_0 */
/* .packed-switch 0x100 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private static com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage parseRtMessage ( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
/* .locals 1 */
/* .param p0, "nlmsghdr" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 120 */
/* iget-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
/* sparse-switch v0, :sswitch_data_0 */
/* .line 127 */
int v0 = 0; // const/4 v0, 0x0
/* .line 126 */
/* :sswitch_0 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NduseroptMessage .parse ( p0,p1 );
/* .line 124 */
/* :sswitch_1 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.RtNetlinkNeighborMessage .parse ( p0,p1 );
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1c -> :sswitch_1 */
/* 0x1d -> :sswitch_1 */
/* 0x1e -> :sswitch_1 */
/* 0x44 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
/* # virtual methods */
public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr getHeader ( ) {
/* .locals 1 */
/* .line 89 */
v0 = this.mHeader;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 99 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "NetlinkMessage{"; // const-string v1, "NetlinkMessage{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mHeader;
/* if-nez v1, :cond_0 */
final String v1 = ""; // const-string v1, ""
} // :cond_0
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->toString()Ljava/lang/String;
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
