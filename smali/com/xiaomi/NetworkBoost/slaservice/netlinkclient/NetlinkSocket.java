public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkSocket {
	 /* .source "NetlinkSocket.java" */
	 /* # static fields */
	 public static final Integer DEFAULT_RECV_BUFSIZE;
	 public static final Integer SOCKET_RECV_BUFSIZE;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkSocket ( ) {
		 /* .locals 0 */
		 /* .line 51 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static void checkTimeout ( Long p0 ) {
		 /* .locals 2 */
		 /* .param p0, "timeoutMs" # J */
		 /* .line 117 */
		 /* const-wide/16 v0, 0x0 */
		 /* cmp-long v0, p0, v0 */
		 /* if-ltz v0, :cond_0 */
		 /* .line 120 */
		 return;
		 /* .line 118 */
	 } // :cond_0
	 /* new-instance v0, Ljava/lang/IllegalArgumentException; */
	 final String v1 = "Negative timeouts not permitted"; // const-string v1, "Negative timeouts not permitted"
	 /* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
public static void connectToKernel ( java.io.FileDescriptor p0 ) {
	 /* .locals 1 */
	 /* .param p0, "fd" # Ljava/io/FileDescriptor; */
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/system/ErrnoException;, */
	 /* Ljava/net/SocketException; */
	 /* } */
} // .end annotation
/* .line 113 */
int v0 = 0; // const/4 v0, 0x0
android.net.util.SocketUtils .makeNetlinkSocketAddress ( v0,v0 );
android.system.Os .connect ( p0,v0 );
/* .line 114 */
return;
} // .end method
public static java.io.FileDescriptor forProto ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "nlProto" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 107 */
android.system.Os .socket ( v0,v1,p0 );
/* .line 108 */
/* .local v0, "fd":Ljava/io/FileDescriptor; */
/* const/high16 v3, 0x10000 */
android.system.Os .setsockoptInt ( v0,v1,v2,v3 );
/* .line 109 */
} // .end method
public static java.nio.ByteBuffer recvMessage ( java.io.FileDescriptor p0, Integer p1, Long p2 ) {
/* .locals 4 */
/* .param p0, "fd" # Ljava/io/FileDescriptor; */
/* .param p1, "bufsize" # I */
/* .param p2, "timeoutMs" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/io/InterruptedIOException; */
/* } */
} // .end annotation
/* .line 130 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkSocket .checkTimeout ( p2,p3 );
/* .line 132 */
android.system.StructTimeval .fromMillis ( p2,p3 );
android.system.Os .setsockoptTimeval ( p0,v0,v1,v2 );
/* .line 134 */
java.nio.ByteBuffer .allocate ( p1 );
/* .line 135 */
/* .local v0, "byteBuffer":Ljava/nio/ByteBuffer; */
v1 = android.system.Os .read ( p0,v0 );
/* .line 136 */
/* .local v1, "length":I */
/* if-ne v1, p1, :cond_0 */
/* .line 137 */
final String v2 = "NetlinkSocket"; // const-string v2, "NetlinkSocket"
final String v3 = "maximum read"; // const-string v3, "maximum read"
android.util.Log .w ( v2,v3 );
/* .line 139 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
(( java.nio.ByteBuffer ) v0 ).position ( v2 ); // invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 140 */
(( java.nio.ByteBuffer ) v0 ).limit ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;
/* .line 141 */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) v0 ).order ( v2 ); // invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 142 */
} // .end method
public static Integer sendMessage ( java.io.FileDescriptor p0, Object[] p1, Integer p2, Integer p3, Long p4 ) {
/* .locals 3 */
/* .param p0, "fd" # Ljava/io/FileDescriptor; */
/* .param p1, "bytes" # [B */
/* .param p2, "offset" # I */
/* .param p3, "count" # I */
/* .param p4, "timeoutMs" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/io/InterruptedIOException; */
/* } */
} // .end annotation
/* .line 154 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkSocket .checkTimeout ( p4,p5 );
/* .line 155 */
android.system.StructTimeval .fromMillis ( p4,p5 );
android.system.Os .setsockoptTimeval ( p0,v0,v1,v2 );
/* .line 156 */
v0 = android.system.Os .write ( p0,p1,p2,p3 );
} // .end method
public static void sendOneShotKernelMessage ( Integer p0, Object[] p1 ) {
/* .locals 12 */
/* .param p0, "nlProto" # I */
/* .param p1, "msg" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 58 */
final String v0 = "Error in NetlinkSocket.sendOneShotKernelMessage"; // const-string v0, "Error in NetlinkSocket.sendOneShotKernelMessage"
final String v1 = "NetlinkSocket"; // const-string v1, "NetlinkSocket"
final String v2 = "Error in NetlinkSocket.sendOneShotKernelMessage"; // const-string v2, "Error in NetlinkSocket.sendOneShotKernelMessage"
/* .line 59 */
/* .local v2, "errPrefix":Ljava/lang/String; */
/* const-wide/16 v3, 0x12c */
/* .line 61 */
/* .local v3, "IO_TIMEOUT":J */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkSocket .forProto ( p0 );
/* .line 64 */
/* .local v11, "fd":Ljava/io/FileDescriptor; */
try { // :try_start_0
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkSocket .connectToKernel ( v11 );
/* .line 65 */
int v7 = 0; // const/4 v7, 0x0
/* array-length v8, p1 */
/* const-wide/16 v9, 0x12c */
/* move-object v5, v11 */
/* move-object v6, p1 */
/* invoke-static/range {v5 ..v10}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkSocket;->sendMessage(Ljava/io/FileDescriptor;[BIIJ)I */
/* .line 66 */
/* const/16 v5, 0x2000 */
/* const-wide/16 v6, 0x12c */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkSocket .recvMessage ( v11,v5,v6,v7 );
/* .line 68 */
/* .local v5, "bytes":Ljava/nio/ByteBuffer; */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage .parse ( v5,p0 );
/* :try_end_0 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/net/SocketException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 69 */
/* .local v6, "response":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage; */
final String v7 = "Error in NetlinkSocket.sendOneShotKernelMessage, errmsg="; // const-string v7, "Error in NetlinkSocket.sendOneShotKernelMessage, errmsg="
if ( v6 != null) { // if-eqz v6, :cond_1
try { // :try_start_1
/* instance-of v8, v6, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage; */
if ( v8 != null) { // if-eqz v8, :cond_1
/* move-object v8, v6 */
/* check-cast v8, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage; */
/* .line 70 */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkErrorMessage ) v8 ).getNlMsgError ( ); // invoke-virtual {v8}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage;->getNlMsgError()Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 71 */
/* move-object v8, v6 */
/* check-cast v8, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage; */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkErrorMessage ) v8 ).getNlMsgError ( ); // invoke-virtual {v8}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkErrorMessage;->getNlMsgError()Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;
/* iget v8, v8, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgErr;->error:I */
/* :try_end_1 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_1 ..:try_end_1} :catch_2 */
/* .catch Ljava/net/SocketException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 72 */
/* .local v8, "errno":I */
/* if-nez v8, :cond_0 */
/* .line 80 */
} // .end local v8 # "errno":I
/* nop */
/* .line 99 */
} // .end local v5 # "bytes":Ljava/nio/ByteBuffer;
} // .end local v6 # "response":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;
try { // :try_start_2
android.net.util.SocketUtils .closeSocket ( v11 );
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 102 */
/* .line 100 */
/* :catch_0 */
/* move-exception v0 */
/* .line 103 */
/* nop */
/* .line 104 */
} // :goto_0
return;
/* .line 76 */
/* .restart local v5 # "bytes":Ljava/nio/ByteBuffer; */
/* .restart local v6 # "response":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage; */
/* .restart local v8 # "errno":I */
} // :cond_0
try { // :try_start_3
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage ) v6 ).toString ( ); // invoke-virtual {v6}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v7 );
/* .line 78 */
/* new-instance v7, Landroid/system/ErrnoException; */
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage ) v6 ).toString ( ); // invoke-virtual {v6}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;->toString()Ljava/lang/String;
v10 = java.lang.Math .abs ( v8 );
/* invoke-direct {v7, v9, v10}, Landroid/system/ErrnoException;-><init>(Ljava/lang/String;I)V */
} // .end local v2 # "errPrefix":Ljava/lang/String;
} // .end local v3 # "IO_TIMEOUT":J
} // .end local v11 # "fd":Ljava/io/FileDescriptor;
} // .end local p0 # "nlProto":I
} // .end local p1 # "msg":[B
/* throw v7 */
/* .line 82 */
} // .end local v8 # "errno":I
/* .restart local v2 # "errPrefix":Ljava/lang/String; */
/* .restart local v3 # "IO_TIMEOUT":J */
/* .restart local v11 # "fd":Ljava/io/FileDescriptor; */
/* .restart local p0 # "nlProto":I */
/* .restart local p1 # "msg":[B */
} // :cond_1
/* if-nez v6, :cond_2 */
/* .line 83 */
int v8 = 0; // const/4 v8, 0x0
(( java.nio.ByteBuffer ) v5 ).position ( v8 ); // invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 84 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "raw bytes: "; // const-string v9, "raw bytes: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .hexify ( v5 );
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .local v8, "errmsg":Ljava/lang/String; */
/* .line 86 */
} // .end local v8 # "errmsg":Ljava/lang/String;
} // :cond_2
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage ) v6 ).toString ( ); // invoke-virtual {v6}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;->toString()Ljava/lang/String;
/* .line 88 */
/* .restart local v8 # "errmsg":Ljava/lang/String; */
} // :goto_1
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v7 );
/* .line 89 */
/* new-instance v7, Landroid/system/ErrnoException; */
/* invoke-direct {v7, v8, v9}, Landroid/system/ErrnoException;-><init>(Ljava/lang/String;I)V */
} // .end local v2 # "errPrefix":Ljava/lang/String;
} // .end local v3 # "IO_TIMEOUT":J
} // .end local v11 # "fd":Ljava/io/FileDescriptor;
} // .end local p0 # "nlProto":I
} // .end local p1 # "msg":[B
/* throw v7 */
/* :try_end_3 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .catch Ljava/net/SocketException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 98 */
} // .end local v5 # "bytes":Ljava/nio/ByteBuffer;
} // .end local v6 # "response":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;
} // .end local v8 # "errmsg":Ljava/lang/String;
/* .restart local v2 # "errPrefix":Ljava/lang/String; */
/* .restart local v3 # "IO_TIMEOUT":J */
/* .restart local v11 # "fd":Ljava/io/FileDescriptor; */
/* .restart local p0 # "nlProto":I */
/* .restart local p1 # "msg":[B */
/* :catchall_0 */
/* move-exception v0 */
/* .line 94 */
/* :catch_1 */
/* move-exception v5 */
/* .line 95 */
/* .local v5, "e":Ljava/net/SocketException; */
try { // :try_start_4
android.util.Log .e ( v1,v0,v5 );
/* .line 96 */
/* new-instance v1, Landroid/system/ErrnoException; */
/* invoke-direct {v1, v0, v6, v5}, Landroid/system/ErrnoException;-><init>(Ljava/lang/String;ILjava/lang/Throwable;)V */
} // .end local v2 # "errPrefix":Ljava/lang/String;
} // .end local v3 # "IO_TIMEOUT":J
} // .end local v11 # "fd":Ljava/io/FileDescriptor;
} // .end local p0 # "nlProto":I
} // .end local p1 # "msg":[B
/* throw v1 */
/* .line 91 */
} // .end local v5 # "e":Ljava/net/SocketException;
/* .restart local v2 # "errPrefix":Ljava/lang/String; */
/* .restart local v3 # "IO_TIMEOUT":J */
/* .restart local v11 # "fd":Ljava/io/FileDescriptor; */
/* .restart local p0 # "nlProto":I */
/* .restart local p1 # "msg":[B */
/* :catch_2 */
/* move-exception v5 */
/* .line 92 */
/* .local v5, "e":Ljava/io/InterruptedIOException; */
android.util.Log .e ( v1,v0,v5 );
/* .line 93 */
/* new-instance v1, Landroid/system/ErrnoException; */
/* invoke-direct {v1, v0, v6, v5}, Landroid/system/ErrnoException;-><init>(Ljava/lang/String;ILjava/lang/Throwable;)V */
} // .end local v2 # "errPrefix":Ljava/lang/String;
} // .end local v3 # "IO_TIMEOUT":J
} // .end local v11 # "fd":Ljava/io/FileDescriptor;
} // .end local p0 # "nlProto":I
} // .end local p1 # "msg":[B
/* throw v1 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 99 */
} // .end local v5 # "e":Ljava/io/InterruptedIOException;
/* .restart local v2 # "errPrefix":Ljava/lang/String; */
/* .restart local v3 # "IO_TIMEOUT":J */
/* .restart local v11 # "fd":Ljava/io/FileDescriptor; */
/* .restart local p0 # "nlProto":I */
/* .restart local p1 # "msg":[B */
} // :goto_2
try { // :try_start_5
android.net.util.SocketUtils .closeSocket ( v11 );
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_3 */
/* .line 102 */
/* .line 100 */
/* :catch_3 */
/* move-exception v1 */
/* .line 103 */
} // :goto_3
/* throw v0 */
} // .end method
