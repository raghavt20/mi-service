public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.NduseroptMessage extends com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkMessage {
	 /* .source "NduseroptMessage.java" */
	 /* # static fields */
	 static final Integer NDUSEROPT_SRCADDR;
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public final Object family;
	 public final Object icmp_code;
	 public final Object icmp_type;
	 public final Integer ifindex;
	 public final com.xiaomi.NetworkBoost.slaservice.netlinkclient.NdOption option;
	 public final Integer opts_len;
	 public final java.net.InetAddress srcaddr;
	 /* # direct methods */
	 com.xiaomi.NetworkBoost.slaservice.netlinkclient.NduseroptMessage ( ) {
		 /* .locals 8 */
		 /* .param p1, "header" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
		 /* .param p2, "buf" # Ljava/nio/ByteBuffer; */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Ljava/net/UnknownHostException; */
		 /* } */
	 } // .end annotation
	 /* .line 66 */
	 /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V */
	 /* .line 69 */
	 java.nio.ByteOrder .nativeOrder ( );
	 (( java.nio.ByteBuffer ) p2 ).order ( v0 ); // invoke-virtual {p2, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
	 /* .line 70 */
	 v0 = 	 (( java.nio.ByteBuffer ) p2 ).position ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I
	 /* .line 71 */
	 /* .local v0, "start":I */
	 v1 = 	 (( java.nio.ByteBuffer ) p2 ).get ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B
	 /* iput-byte v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->family:B */
	 /* .line 72 */
	 (( java.nio.ByteBuffer ) p2 ).get ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B
	 /* .line 73 */
	 v2 = 	 (( java.nio.ByteBuffer ) p2 ).getShort ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->getShort()S
	 v2 = 	 java.lang.Short .toUnsignedInt ( v2 );
	 /* iput v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->opts_len:I */
	 /* .line 74 */
	 v3 = 	 (( java.nio.ByteBuffer ) p2 ).getInt ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->getInt()I
	 /* iput v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->ifindex:I */
	 /* .line 75 */
	 v4 = 	 (( java.nio.ByteBuffer ) p2 ).get ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B
	 /* iput-byte v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->icmp_type:B */
	 /* .line 76 */
	 v4 = 	 (( java.nio.ByteBuffer ) p2 ).get ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B
	 /* iput-byte v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->icmp_code:B */
	 /* .line 77 */
	 v4 = 	 (( java.nio.ByteBuffer ) p2 ).position ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I
	 /* add-int/lit8 v4, v4, 0x6 */
	 (( java.nio.ByteBuffer ) p2 ).position ( v4 ); // invoke-virtual {p2, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
	 /* .line 83 */
	 v4 = java.nio.ByteOrder.BIG_ENDIAN;
	 (( java.nio.ByteBuffer ) p2 ).order ( v4 ); // invoke-virtual {p2, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
	 /* .line 84 */
	 v4 = 	 (( java.nio.ByteBuffer ) p2 ).limit ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->limit()I
	 /* .line 85 */
	 /* .local v4, "oldLimit":I */
	 /* add-int/lit8 v5, v0, 0x10 */
	 /* add-int/2addr v5, v2 */
	 (( java.nio.ByteBuffer ) p2 ).limit ( v5 ); // invoke-virtual {p2, v5}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;
	 /* .line 87 */
	 try { // :try_start_0
		 com.xiaomi.NetworkBoost.slaservice.netlinkclient.NdOption .parse ( p2 );
		 this.option = v5;
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* .line 89 */
		 (( java.nio.ByteBuffer ) p2 ).limit ( v4 ); // invoke-virtual {p2, v4}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;
		 /* .line 90 */
		 /* nop */
		 /* .line 93 */
		 /* add-int/lit8 v5, v0, 0x10 */
		 /* add-int/2addr v5, v2 */
		 /* .line 94 */
		 /* .local v5, "newPosition":I */
		 v2 = 		 (( java.nio.ByteBuffer ) p2 ).limit ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->limit()I
		 /* if-ge v5, v2, :cond_2 */
		 /* .line 97 */
		 (( java.nio.ByteBuffer ) p2 ).position ( v5 ); // invoke-virtual {p2, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
		 /* .line 99 */
		 com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlAttr .parse ( p2 );
		 /* .line 100 */
		 /* .local v2, "nla":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr; */
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 /* iget-short v6, v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_type:S */
			 int v7 = 1; // const/4 v7, 0x1
			 /* if-ne v6, v7, :cond_1 */
			 v6 = this.nla_value;
			 if ( v6 != null) { // if-eqz v6, :cond_1
				 /* .line 103 */
				 /* if-ne v1, v6, :cond_0 */
				 /* .line 105 */
				 int v1 = 0; // const/4 v1, 0x0
				 v6 = this.nla_value;
				 java.net.Inet6Address .getByAddress ( v1,v6,v3 );
				 this.srcaddr = v1;
				 /* .line 107 */
			 } // :cond_0
			 v1 = this.nla_value;
			 java.net.InetAddress .getByAddress ( v1 );
			 this.srcaddr = v1;
			 /* .line 109 */
		 } // :goto_0
		 return;
		 /* .line 101 */
	 } // :cond_1
	 /* new-instance v1, Ljava/lang/IllegalArgumentException; */
	 final String v3 = "Invalid source address in ND useropt"; // const-string v3, "Invalid source address in ND useropt"
	 /* invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
	 /* throw v1 */
	 /* .line 95 */
} // .end local v2 # "nla":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;
} // :cond_2
/* new-instance v1, Ljava/lang/IllegalArgumentException; */
final String v2 = "ND options extend past end of buffer"; // const-string v2, "ND options extend past end of buffer"
/* invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 89 */
} // .end local v5 # "newPosition":I
/* :catchall_0 */
/* move-exception v1 */
(( java.nio.ByteBuffer ) p2 ).limit ( v4 ); // invoke-virtual {p2, v4}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;
/* .line 90 */
/* throw v1 */
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.NduseroptMessage parse ( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
/* .locals 3 */
/* .param p0, "header" # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
/* .param p1, "buf" # Ljava/nio/ByteBuffer; */
/* .line 121 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_1
v1 = (( java.nio.ByteBuffer ) p1 ).remaining ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I
/* const/16 v2, 0x10 */
/* if-ge v1, v2, :cond_0 */
/* .line 122 */
} // :cond_0
(( java.nio.ByteBuffer ) p1 ).order ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
/* .line 124 */
/* .local v1, "oldOrder":Ljava/nio/ByteOrder; */
try { // :try_start_0
/* new-instance v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage; */
/* invoke-direct {v2, p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;Ljava/nio/ByteBuffer;)V */
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/net/UnknownHostException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/nio/BufferUnderflowException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 131 */
(( java.nio.ByteBuffer ) p1 ).order ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 124 */
/* .line 131 */
/* :catchall_0 */
/* move-exception v0 */
(( java.nio.ByteBuffer ) p1 ).order ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 132 */
/* throw v0 */
/* .line 125 */
/* :catch_0 */
/* move-exception v2 */
/* .line 129 */
/* .local v2, "e":Ljava/lang/Exception; */
/* nop */
/* .line 131 */
(( java.nio.ByteBuffer ) p1 ).order ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 129 */
/* .line 121 */
} // .end local v1 # "oldOrder":Ljava/nio/ByteOrder;
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_0
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 7 */
/* .line 137 */
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->family:B */
/* .line 138 */
java.lang.Byte .valueOf ( v0 );
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->opts_len:I */
java.lang.Integer .valueOf ( v0 );
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->ifindex:I */
java.lang.Integer .valueOf ( v0 );
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->icmp_type:B */
v0 = java.lang.Byte .toUnsignedInt ( v0 );
java.lang.Integer .valueOf ( v0 );
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->icmp_code:B */
/* .line 139 */
v0 = java.lang.Byte .toUnsignedInt ( v0 );
java.lang.Integer .valueOf ( v0 );
v0 = this.srcaddr;
(( java.net.InetAddress ) v0 ).getHostAddress ( ); // invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
/* filled-new-array/range {v1 ..v6}, [Ljava/lang/Object; */
/* .line 137 */
final String v1 = "Nduseroptmsg(%d, %d, %d, %d, %d, %s)"; // const-string v1, "Nduseroptmsg(%d, %d, %d, %d, %d, %s)"
java.lang.String .format ( v1,v0 );
} // .end method
