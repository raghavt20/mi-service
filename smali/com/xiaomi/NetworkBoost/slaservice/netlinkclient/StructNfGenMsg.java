public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNfGenMsg {
	 /* .source "StructNfGenMsg.java" */
	 /* # static fields */
	 public static final Integer NFNETLINK_V0;
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public final Object nfgen_family;
	 public final Object res_id;
	 public final Object version;
	 /* # direct methods */
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNfGenMsg ( ) {
		 /* .locals 1 */
		 /* .param p1, "family" # B */
		 /* .line 73 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 74 */
		 /* iput-byte p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;->nfgen_family:B */
		 /* .line 75 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;->version:B */
		 /* .line 76 */
		 /* iput-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;->res_id:S */
		 /* .line 77 */
		 return;
	 } // .end method
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNfGenMsg ( ) {
		 /* .locals 0 */
		 /* .param p1, "family" # B */
		 /* .param p2, "ver" # B */
		 /* .param p3, "id" # S */
		 /* .line 67 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 68 */
		 /* iput-byte p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;->nfgen_family:B */
		 /* .line 69 */
		 /* iput-byte p2, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;->version:B */
		 /* .line 70 */
		 /* iput-short p3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;->res_id:S */
		 /* .line 71 */
		 return;
	 } // .end method
	 private static Boolean hasAvailableSpace ( java.nio.ByteBuffer p0 ) {
		 /* .locals 2 */
		 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .line 90 */
		 v0 = 		 (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
		 int v1 = 4; // const/4 v1, 0x4
		 /* if-lt v0, v1, :cond_0 */
		 int v0 = 1; // const/4 v0, 0x1
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNfGenMsg parse ( java.nio.ByteBuffer p0 ) {
/* .locals 5 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 52 */
java.util.Objects .requireNonNull ( p0 );
/* .line 54 */
v0 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNfGenMsg .hasAvailableSpace ( p0 );
/* if-nez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 56 */
} // :cond_0
v0 = (( java.nio.ByteBuffer ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B
/* .line 57 */
/* .local v0, "nfgen_family":B */
v1 = (( java.nio.ByteBuffer ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B
/* .line 59 */
/* .local v1, "version":B */
(( java.nio.ByteBuffer ) p0 ).order ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
/* .line 60 */
/* .local v2, "originalOrder":Ljava/nio/ByteOrder; */
v3 = java.nio.ByteOrder.BIG_ENDIAN;
(( java.nio.ByteBuffer ) p0 ).order ( v3 ); // invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 61 */
v3 = (( java.nio.ByteBuffer ) p0 ).getShort ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S
/* .line 62 */
/* .local v3, "res_id":S */
(( java.nio.ByteBuffer ) p0 ).order ( v2 ); // invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 64 */
/* new-instance v4, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg; */
/* invoke-direct {v4, v0, v1, v3}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;-><init>(BBS)V */
} // .end method
/* # virtual methods */
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 2 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 80 */
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;->nfgen_family:B */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 81 */
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;->version:B */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 83 */
(( java.nio.ByteBuffer ) p1 ).order ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
/* .line 84 */
/* .local v0, "originalOrder":Ljava/nio/ByteOrder; */
v1 = java.nio.ByteOrder.BIG_ENDIAN;
(( java.nio.ByteBuffer ) p1 ).order ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 85 */
/* iget-short v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;->res_id:S */
(( java.nio.ByteBuffer ) p1 ).putShort ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 86 */
(( java.nio.ByteBuffer ) p1 ).order ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 87 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 95 */
/* iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;->nfgen_family:B */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .stringForAddressFamily ( v0 );
/* .line 97 */
/* .local v0, "familyStr":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "NfGenMsg{ nfgen_family{"; // const-string v2, "NfGenMsg{ nfgen_family{"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "}, version{" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;->version:B */
/* .line 99 */
v2 = java.lang.Byte .toUnsignedInt ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "}, res_id{" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNfGenMsg;->res_id:S */
/* .line 100 */
v2 = java.lang.Short .toUnsignedInt ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "} }" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 97 */
} // .end method
