.class public Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;
.super Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;
.source "NduseroptMessage.java"


# static fields
.field static final NDUSEROPT_SRCADDR:I = 0x1

.field public static final STRUCT_SIZE:I = 0x10


# instance fields
.field public final family:B

.field public final icmp_code:B

.field public final icmp_type:B

.field public final ifindex:I

.field public final option:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NdOption;

.field public final opts_len:I

.field public final srcaddr:Ljava/net/InetAddress;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;Ljava/nio/ByteBuffer;)V
    .locals 8
    .param p1, "header"    # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
    .param p2, "buf"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    .line 66
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NetlinkMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;)V

    .line 69
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 70
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    .line 71
    .local v0, "start":I
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    iput-byte v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->family:B

    .line 72
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B

    .line 73
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v2

    invoke-static {v2}, Ljava/lang/Short;->toUnsignedInt(S)I

    move-result v2

    iput v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->opts_len:I

    .line 74
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->ifindex:I

    .line 75
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    iput-byte v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->icmp_type:B

    .line 76
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    iput-byte v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->icmp_code:B

    .line 77
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/lit8 v4, v4, 0x6

    invoke-virtual {p2, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 83
    sget-object v4, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p2, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 84
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v4

    .line 85
    .local v4, "oldLimit":I
    add-int/lit8 v5, v0, 0x10

    add-int/2addr v5, v2

    invoke-virtual {p2, v5}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 87
    :try_start_0
    invoke-static {p2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NdOption;->parse(Ljava/nio/ByteBuffer;)Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NdOption;

    move-result-object v5

    iput-object v5, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->option:Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NdOption;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    invoke-virtual {p2, v4}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 90
    nop

    .line 93
    add-int/lit8 v5, v0, 0x10

    add-int/2addr v5, v2

    .line 94
    .local v5, "newPosition":I
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    if-ge v5, v2, :cond_2

    .line 97
    invoke-virtual {p2, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 99
    invoke-static {p2}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->parse(Ljava/nio/ByteBuffer;)Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;

    move-result-object v2

    .line 100
    .local v2, "nla":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;
    if-eqz v2, :cond_1

    iget-short v6, v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_type:S

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    iget-object v6, v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_value:[B

    if-eqz v6, :cond_1

    .line 103
    sget v6, Landroid/system/OsConstants;->AF_INET6:I

    if-ne v1, v6, :cond_0

    .line 105
    const/4 v1, 0x0

    iget-object v6, v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_value:[B

    invoke-static {v1, v6, v3}, Ljava/net/Inet6Address;->getByAddress(Ljava/lang/String;[BI)Ljava/net/Inet6Address;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->srcaddr:Ljava/net/InetAddress;

    goto :goto_0

    .line 107
    :cond_0
    iget-object v1, v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;->nla_value:[B

    invoke-static {v1}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->srcaddr:Ljava/net/InetAddress;

    .line 109
    :goto_0
    return-void

    .line 101
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v3, "Invalid source address in ND useropt"

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 95
    .end local v2    # "nla":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlAttr;
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "ND options extend past end of buffer"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 89
    .end local v5    # "newPosition":I
    :catchall_0
    move-exception v1

    invoke-virtual {p2, v4}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 90
    throw v1
.end method

.method public static parse(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;
    .locals 3
    .param p0, "header"    # Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;
    .param p1, "buf"    # Ljava/nio/ByteBuffer;

    .line 121
    const/4 v0, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    goto :goto_0

    .line 122
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;

    move-result-object v1

    .line 124
    .local v1, "oldOrder":Ljava/nio/ByteOrder;
    :try_start_0
    new-instance v2, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;

    invoke-direct {v2, p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 124
    return-object v2

    .line 131
    :catchall_0
    move-exception v0

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 132
    throw v0

    .line 125
    :catch_0
    move-exception v2

    .line 129
    .local v2, "e":Ljava/lang/Exception;
    nop

    .line 131
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 129
    return-object v0

    .line 121
    .end local v1    # "oldOrder":Ljava/nio/ByteOrder;
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 7

    .line 137
    iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->family:B

    .line 138
    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->opts_len:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->ifindex:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->icmp_type:B

    invoke-static {v0}, Ljava/lang/Byte;->toUnsignedInt(B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-byte v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->icmp_code:B

    .line 139
    invoke-static {v0}, Ljava/lang/Byte;->toUnsignedInt(B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/NduseroptMessage;->srcaddr:Ljava/net/InetAddress;

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    filled-new-array/range {v1 .. v6}, [Ljava/lang/Object;

    move-result-object v0

    .line 137
    const-string v1, "Nduseroptmsg(%d, %d, %d, %d, %d, %s)"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
