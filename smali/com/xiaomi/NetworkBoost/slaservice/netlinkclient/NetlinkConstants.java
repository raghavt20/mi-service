public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants {
	 /* .source "NetlinkConstants.java" */
	 /* # static fields */
	 private static final HEX_DIGITS;
	 public static final Integer INET_DIAG_MEMINFO;
	 public static final Object IPCTNL_MSG_CT_DELETE;
	 public static final Object IPCTNL_MSG_CT_GET;
	 public static final Object IPCTNL_MSG_CT_GET_CTRZERO;
	 public static final Object IPCTNL_MSG_CT_GET_DYING;
	 public static final Object IPCTNL_MSG_CT_GET_STATS;
	 public static final Object IPCTNL_MSG_CT_GET_STATS_CPU;
	 public static final Object IPCTNL_MSG_CT_GET_UNCONFIRMED;
	 public static final Object IPCTNL_MSG_CT_NEW;
	 public static final Object NFNL_SUBSYS_CTNETLINK;
	 public static final Integer NLA_ALIGNTO;
	 public static final Object NLMSG_DONE;
	 public static final Object NLMSG_ERROR;
	 public static final Object NLMSG_MAX_RESERVED;
	 public static final Object NLMSG_NOOP;
	 public static final Object NLMSG_OVERRUN;
	 public static final Integer RTMGRP_ND_USEROPT;
	 public static final Object RTM_DELADDR;
	 public static final Object RTM_DELLINK;
	 public static final Object RTM_DELNEIGH;
	 public static final Object RTM_DELROUTE;
	 public static final Object RTM_DELRULE;
	 public static final Object RTM_GETADDR;
	 public static final Object RTM_GETLINK;
	 public static final Object RTM_GETNEIGH;
	 public static final Object RTM_GETROUTE;
	 public static final Object RTM_GETRULE;
	 public static final Object RTM_NEWADDR;
	 public static final Object RTM_NEWLINK;
	 public static final Object RTM_NEWNDUSEROPT;
	 public static final Object RTM_NEWNEIGH;
	 public static final Object RTM_NEWROUTE;
	 public static final Object RTM_NEWRULE;
	 public static final Object RTM_SETLINK;
	 public static final Integer RTNLGRP_ND_USEROPT;
	 public static final Integer SOCKDIAG_MSG_HEADER_SIZE;
	 public static final Object SOCK_DIAG_BY_FAMILY;
	 /* # direct methods */
	 static com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants ( ) {
		 /* .locals 1 */
		 /* .line 225 */
		 /* const/16 v0, 0x10 */
		 /* new-array v0, v0, [C */
		 /* fill-array-data v0, :array_0 */
		 return;
		 /* :array_0 */
		 /* .array-data 2 */
		 /* 0x30s */
		 /* 0x31s */
		 /* 0x32s */
		 /* 0x33s */
		 /* 0x34s */
		 /* 0x35s */
		 /* 0x36s */
		 /* 0x37s */
		 /* 0x38s */
		 /* 0x39s */
		 /* 0x41s */
		 /* 0x42s */
		 /* 0x43s */
		 /* 0x44s */
		 /* 0x45s */
		 /* 0x46s */
	 } // .end array-data
} // .end method
private com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants ( ) {
	 /* .locals 0 */
	 /* .line 38 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
public static final Integer alignedLengthOf ( Integer p0 ) {
	 /* .locals 1 */
	 /* .param p0, "length" # I */
	 /* .line 56 */
	 /* if-gtz p0, :cond_0 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 57 */
} // :cond_0
/* add-int/lit8 v0, p0, 0x4 */
/* add-int/lit8 v0, v0, -0x1 */
/* div-int/lit8 v0, v0, 0x4 */
/* mul-int/lit8 v0, v0, 0x4 */
} // .end method
public static final Integer alignedLengthOf ( Object p0 ) {
/* .locals 2 */
/* .param p0, "length" # S */
/* .line 51 */
/* const v0, 0xffff */
/* and-int/2addr v0, p0 */
/* .line 52 */
/* .local v0, "intLength":I */
v1 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .alignedLengthOf ( v0 );
} // .end method
public static java.lang.String hexify ( java.nio.ByteBuffer p0 ) {
/* .locals 3 */
/* .param p0, "buffer" # Ljava/nio/ByteBuffer; */
/* .line 79 */
/* if-nez p0, :cond_0 */
final String v0 = "(null)"; // const-string v0, "(null)"
/* .line 80 */
} // :cond_0
/* nop */
/* .line 81 */
(( java.nio.ByteBuffer ) p0 ).array ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B
v1 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
v2 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
/* .line 80 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .toHexString ( v0,v1,v2 );
} // .end method
public static java.lang.String hexify ( Object[] p0 ) {
/* .locals 2 */
/* .param p0, "bytes" # [B */
/* .line 74 */
/* if-nez p0, :cond_0 */
final String v0 = "(null)"; // const-string v0, "(null)"
/* .line 75 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* array-length v1, p0 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .toHexString ( p0,v0,v1 );
} // .end method
public static java.lang.String stringForAddressFamily ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "family" # I */
/* .line 61 */
/* if-ne p0, v0, :cond_0 */
final String v0 = "AF_INET"; // const-string v0, "AF_INET"
/* .line 62 */
} // :cond_0
/* if-ne p0, v0, :cond_1 */
final String v0 = "AF_INET6"; // const-string v0, "AF_INET6"
/* .line 63 */
} // :cond_1
/* if-ne p0, v0, :cond_2 */
final String v0 = "AF_NETLINK"; // const-string v0, "AF_NETLINK"
/* .line 64 */
} // :cond_2
java.lang.String .valueOf ( p0 );
} // .end method
private static java.lang.String stringForCtlMsgType ( Object p0 ) {
/* .locals 2 */
/* .param p0, "nlmType" # S */
/* .line 135 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 140 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "unknown control message type: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 139 */
/* :pswitch_0 */
final String v0 = "NLMSG_OVERRUN"; // const-string v0, "NLMSG_OVERRUN"
/* .line 138 */
/* :pswitch_1 */
final String v0 = "NLMSG_DONE"; // const-string v0, "NLMSG_DONE"
/* .line 137 */
/* :pswitch_2 */
final String v0 = "NLMSG_ERROR"; // const-string v0, "NLMSG_ERROR"
/* .line 136 */
/* :pswitch_3 */
final String v0 = "NLMSG_NOOP"; // const-string v0, "NLMSG_NOOP"
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static java.lang.String stringForInetDiagMsgType ( Object p0 ) {
/* .locals 2 */
/* .param p0, "nlmType" # S */
/* .line 176 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 178 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "unknown SOCK_DIAG type: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 177 */
/* :pswitch_0 */
final String v0 = "SOCK_DIAG_BY_FAMILY"; // const-string v0, "SOCK_DIAG_BY_FAMILY"
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x14 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static java.lang.String stringForNfMsgType ( Object p0 ) {
/* .locals 4 */
/* .param p0, "nlmType" # S */
/* .line 187 */
/* shr-int/lit8 v0, p0, 0x8 */
/* int-to-byte v0, v0 */
/* .line 188 */
/* .local v0, "subsysId":B */
/* int-to-byte v1, p0 */
/* .line 189 */
/* .local v1, "msgType":B */
/* packed-switch v0, :pswitch_data_0 */
/* .line 191 */
/* :pswitch_0 */
/* packed-switch v1, :pswitch_data_1 */
/* .line 199 */
/* :pswitch_1 */
final String v2 = "IPCTNL_MSG_CT_GET_UNCONFIRMED"; // const-string v2, "IPCTNL_MSG_CT_GET_UNCONFIRMED"
/* .line 198 */
/* :pswitch_2 */
final String v2 = "IPCTNL_MSG_CT_GET_DYING"; // const-string v2, "IPCTNL_MSG_CT_GET_DYING"
/* .line 197 */
/* :pswitch_3 */
final String v2 = "IPCTNL_MSG_CT_GET_STATS"; // const-string v2, "IPCTNL_MSG_CT_GET_STATS"
/* .line 196 */
/* :pswitch_4 */
final String v2 = "IPCTNL_MSG_CT_GET_STATS_CPU"; // const-string v2, "IPCTNL_MSG_CT_GET_STATS_CPU"
/* .line 195 */
/* :pswitch_5 */
final String v2 = "IPCTNL_MSG_CT_GET_CTRZERO"; // const-string v2, "IPCTNL_MSG_CT_GET_CTRZERO"
/* .line 194 */
/* :pswitch_6 */
final String v2 = "IPCTNL_MSG_CT_DELETE"; // const-string v2, "IPCTNL_MSG_CT_DELETE"
/* .line 193 */
/* :pswitch_7 */
final String v2 = "IPCTNL_MSG_CT_GET"; // const-string v2, "IPCTNL_MSG_CT_GET"
/* .line 192 */
/* :pswitch_8 */
final String v2 = "IPCTNL_MSG_CT_NEW"; // const-string v2, "IPCTNL_MSG_CT_NEW"
/* .line 203 */
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "unknown NETFILTER type: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public static java.lang.String stringForNlMsgType ( Object p0, Integer p1 ) {
/* .locals 2 */
/* .param p0, "nlmType" # S */
/* .param p1, "nlFamily" # I */
/* .line 213 */
/* const/16 v0, 0xf */
/* if-gt p0, v0, :cond_0 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .stringForCtlMsgType ( p0 );
/* .line 218 */
} // :cond_0
/* if-ne p1, v0, :cond_1 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .stringForRtMsgType ( p0 );
/* .line 219 */
} // :cond_1
/* if-ne p1, v0, :cond_2 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .stringForInetDiagMsgType ( p0 );
/* .line 220 */
} // :cond_2
/* if-ne p1, v0, :cond_3 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .stringForNfMsgType ( p0 );
/* .line 222 */
} // :cond_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "unknown type: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static java.lang.String stringForProtocol ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "protocol" # I */
/* .line 68 */
/* if-ne p0, v0, :cond_0 */
final String v0 = "IPPROTO_TCP"; // const-string v0, "IPPROTO_TCP"
/* .line 69 */
} // :cond_0
/* if-ne p0, v0, :cond_1 */
final String v0 = "IPPROTO_UDP"; // const-string v0, "IPPROTO_UDP"
/* .line 70 */
} // :cond_1
java.lang.String .valueOf ( p0 );
} // .end method
private static java.lang.String stringForRtMsgType ( Object p0 ) {
/* .locals 2 */
/* .param p0, "nlmType" # S */
/* .line 149 */
/* sparse-switch p0, :sswitch_data_0 */
/* .line 167 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "unknown RTM type: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 166 */
/* :sswitch_0 */
final String v0 = "RTM_NEWNDUSEROPT"; // const-string v0, "RTM_NEWNDUSEROPT"
/* .line 165 */
/* :sswitch_1 */
final String v0 = "RTM_GETRULE"; // const-string v0, "RTM_GETRULE"
/* .line 164 */
/* :sswitch_2 */
final String v0 = "RTM_DELRULE"; // const-string v0, "RTM_DELRULE"
/* .line 163 */
/* :sswitch_3 */
final String v0 = "RTM_NEWRULE"; // const-string v0, "RTM_NEWRULE"
/* .line 162 */
/* :sswitch_4 */
final String v0 = "RTM_GETNEIGH"; // const-string v0, "RTM_GETNEIGH"
/* .line 161 */
/* :sswitch_5 */
final String v0 = "RTM_DELNEIGH"; // const-string v0, "RTM_DELNEIGH"
/* .line 160 */
/* :sswitch_6 */
final String v0 = "RTM_NEWNEIGH"; // const-string v0, "RTM_NEWNEIGH"
/* .line 159 */
/* :sswitch_7 */
final String v0 = "RTM_GETROUTE"; // const-string v0, "RTM_GETROUTE"
/* .line 158 */
/* :sswitch_8 */
final String v0 = "RTM_DELROUTE"; // const-string v0, "RTM_DELROUTE"
/* .line 157 */
/* :sswitch_9 */
final String v0 = "RTM_NEWROUTE"; // const-string v0, "RTM_NEWROUTE"
/* .line 156 */
/* :sswitch_a */
final String v0 = "RTM_GETADDR"; // const-string v0, "RTM_GETADDR"
/* .line 155 */
/* :sswitch_b */
final String v0 = "RTM_DELADDR"; // const-string v0, "RTM_DELADDR"
/* .line 154 */
/* :sswitch_c */
final String v0 = "RTM_NEWADDR"; // const-string v0, "RTM_NEWADDR"
/* .line 153 */
/* :sswitch_d */
final String v0 = "RTM_SETLINK"; // const-string v0, "RTM_SETLINK"
/* .line 152 */
/* :sswitch_e */
final String v0 = "RTM_GETLINK"; // const-string v0, "RTM_GETLINK"
/* .line 151 */
/* :sswitch_f */
final String v0 = "RTM_DELLINK"; // const-string v0, "RTM_DELLINK"
/* .line 150 */
/* :sswitch_10 */
final String v0 = "RTM_NEWLINK"; // const-string v0, "RTM_NEWLINK"
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x10 -> :sswitch_10 */
/* 0x11 -> :sswitch_f */
/* 0x12 -> :sswitch_e */
/* 0x13 -> :sswitch_d */
/* 0x14 -> :sswitch_c */
/* 0x15 -> :sswitch_b */
/* 0x16 -> :sswitch_a */
/* 0x18 -> :sswitch_9 */
/* 0x19 -> :sswitch_8 */
/* 0x1a -> :sswitch_7 */
/* 0x1c -> :sswitch_6 */
/* 0x1d -> :sswitch_5 */
/* 0x1e -> :sswitch_4 */
/* 0x20 -> :sswitch_3 */
/* 0x21 -> :sswitch_2 */
/* 0x22 -> :sswitch_1 */
/* 0x44 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public static java.lang.String toHexString ( Object[] p0, Integer p1, Integer p2 ) {
/* .locals 7 */
/* .param p0, "array" # [B */
/* .param p1, "offset" # I */
/* .param p2, "length" # I */
/* .line 231 */
/* mul-int/lit8 v0, p2, 0x2 */
/* new-array v0, v0, [C */
/* .line 233 */
/* .local v0, "buf":[C */
int v1 = 0; // const/4 v1, 0x0
/* .line 234 */
/* .local v1, "bufIndex":I */
/* move v2, p1 */
/* .local v2, "i":I */
} // :goto_0
/* add-int v3, p1, p2 */
/* if-ge v2, v3, :cond_0 */
/* .line 235 */
/* aget-byte v3, p0, v2 */
/* .line 236 */
/* .local v3, "b":B */
/* add-int/lit8 v4, v1, 0x1 */
} // .end local v1 # "bufIndex":I
/* .local v4, "bufIndex":I */
v5 = com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants.HEX_DIGITS;
/* ushr-int/lit8 v6, v3, 0x4 */
/* and-int/lit8 v6, v6, 0xf */
/* aget-char v6, v5, v6 */
/* aput-char v6, v0, v1 */
/* .line 237 */
/* add-int/lit8 v1, v4, 0x1 */
} // .end local v4 # "bufIndex":I
/* .restart local v1 # "bufIndex":I */
/* and-int/lit8 v6, v3, 0xf */
/* aget-char v5, v5, v6 */
/* aput-char v5, v0, v4 */
/* .line 234 */
} // .end local v3 # "b":B
/* add-int/lit8 v2, v2, 0x1 */
/* .line 240 */
} // .end local v2 # "i":I
} // :cond_0
/* new-instance v2, Ljava/lang/String; */
/* invoke-direct {v2, v0}, Ljava/lang/String;-><init>([C)V */
} // .end method
