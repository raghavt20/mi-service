public class com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr {
	 /* .source "StructNlMsgHdr.java" */
	 /* # static fields */
	 public static final Object NLM_F_ACK;
	 public static final Object NLM_F_APPEND;
	 public static final Object NLM_F_CREATE;
	 public static final Object NLM_F_DUMP;
	 public static final Object NLM_F_ECHO;
	 public static final Object NLM_F_EXCL;
	 public static final Object NLM_F_MATCH;
	 public static final Object NLM_F_MULTI;
	 public static final Object NLM_F_REPLACE;
	 public static final Object NLM_F_REQUEST;
	 public static final Object NLM_F_ROOT;
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public Object nlmsg_flags;
	 public Integer nlmsg_len;
	 public Integer nlmsg_pid;
	 public Integer nlmsg_seq;
	 public Object nlmsg_type;
	 /* # direct methods */
	 public com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr ( ) {
		 /* .locals 1 */
		 /* .line 110 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 111 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_len:I */
		 /* .line 112 */
		 /* iput-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
		 /* .line 113 */
		 /* iput-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_flags:S */
		 /* .line 114 */
		 /* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_seq:I */
		 /* .line 115 */
		 /* iput v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_pid:I */
		 /* .line 116 */
		 return;
	 } // .end method
	 public static Boolean hasAvailableSpace ( java.nio.ByteBuffer p0 ) {
		 /* .locals 2 */
		 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .line 81 */
		 if ( p0 != null) { // if-eqz p0, :cond_0
			 v0 = 			 (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
			 /* const/16 v1, 0x10 */
			 /* if-lt v0, v1, :cond_0 */
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :goto_0
} // .end method
public static com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr parse ( java.nio.ByteBuffer p0 ) {
	 /* .locals 4 */
	 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
	 /* .line 85 */
	 v0 = 	 com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr .hasAvailableSpace ( p0 );
	 int v1 = 0; // const/4 v1, 0x0
	 /* if-nez v0, :cond_0 */
	 /* .line 90 */
} // :cond_0
/* new-instance v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
/* invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;-><init>()V */
/* .line 91 */
/* .local v0, "struct":Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr; */
v2 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v2, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_len:I */
/* .line 92 */
v2 = (( java.nio.ByteBuffer ) p0 ).getShort ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S
/* iput-short v2, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
/* .line 93 */
v2 = (( java.nio.ByteBuffer ) p0 ).getShort ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S
/* iput-short v2, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_flags:S */
/* .line 94 */
v2 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v2, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_seq:I */
/* .line 95 */
v2 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v2, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_pid:I */
/* .line 97 */
/* iget v2, v0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_len:I */
/* const/16 v3, 0x10 */
/* if-ge v2, v3, :cond_1 */
/* .line 99 */
/* .line 101 */
} // :cond_1
} // .end method
public static java.lang.String stringForNlMsgFlags ( Object p0 ) {
/* .locals 3 */
/* .param p0, "flags" # S */
/* .line 53 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 54 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* and-int/lit8 v1, p0, 0x1 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 55 */
final String v1 = "NLM_F_REQUEST"; // const-string v1, "NLM_F_REQUEST"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 57 */
} // :cond_0
/* and-int/lit8 v1, p0, 0x2 */
/* const-string/jumbo v2, "|" */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 58 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_1 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 59 */
} // :cond_1
final String v1 = "NLM_F_MULTI"; // const-string v1, "NLM_F_MULTI"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 61 */
} // :cond_2
/* and-int/lit8 v1, p0, 0x4 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 62 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_3 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 63 */
} // :cond_3
final String v1 = "NLM_F_ACK"; // const-string v1, "NLM_F_ACK"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 65 */
} // :cond_4
/* and-int/lit8 v1, p0, 0x8 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 66 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_5 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 67 */
} // :cond_5
final String v1 = "NLM_F_ECHO"; // const-string v1, "NLM_F_ECHO"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 69 */
} // :cond_6
/* and-int/lit16 v1, p0, 0x100 */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 70 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_7 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 71 */
} // :cond_7
final String v1 = "NLM_F_ROOT"; // const-string v1, "NLM_F_ROOT"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 73 */
} // :cond_8
/* and-int/lit16 v1, p0, 0x200 */
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 74 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_9 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 75 */
} // :cond_9
final String v1 = "NLM_F_MATCH"; // const-string v1, "NLM_F_MATCH"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 77 */
} // :cond_a
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
/* # virtual methods */
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 1 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 122 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_len:I */
(( java.nio.ByteBuffer ) p1 ).putInt ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
/* .line 123 */
/* iget-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
(( java.nio.ByteBuffer ) p1 ).putShort ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 124 */
/* iget-short v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_flags:S */
(( java.nio.ByteBuffer ) p1 ).putShort ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 125 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_seq:I */
(( java.nio.ByteBuffer ) p1 ).putInt ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
/* .line 126 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_pid:I */
(( java.nio.ByteBuffer ) p1 ).putInt ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
/* .line 127 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 1 */
/* .line 131 */
int v0 = 0; // const/4 v0, 0x0
(( com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr ) p0 ).toString ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;
} // .end method
public java.lang.String toString ( java.lang.Integer p0 ) {
/* .locals 5 */
/* .param p1, "nlFamily" # Ljava/lang/Integer; */
/* .line 144 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = ""; // const-string v1, ""
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "("; // const-string v2, "("
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 145 */
/* if-nez p1, :cond_0 */
/* .line 146 */
/* move-object v3, v1 */
} // :cond_0
/* iget-short v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_type:S */
v4 = (( java.lang.Integer ) p1 ).intValue ( ); // invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I
com.xiaomi.NetworkBoost.slaservice.netlinkclient.NetlinkConstants .stringForNlMsgType ( v3,v4 );
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ")"; // const-string v3, ")"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 148 */
/* .local v0, "typeStr":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v4, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_flags:S */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v2, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_flags:S */
/* .line 149 */
com.xiaomi.NetworkBoost.slaservice.netlinkclient.StructNlMsgHdr .stringForNlMsgFlags ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 150 */
/* .local v1, "flagsStr":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "StructNlMsgHdr{ nlmsg_len{"; // const-string v3, "StructNlMsgHdr{ nlmsg_len{"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_len:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, nlmsg_type{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, nlmsg_flags{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ")}, nlmsg_seq{"; // const-string v3, ")}, nlmsg_seq{"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_seq:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, nlmsg_pid{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/slaservice/netlinkclient/StructNlMsgHdr;->nlmsg_pid:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "} }" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
