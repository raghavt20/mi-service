.class Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;
.super Lmiui/android/services/internal/hidl/manager/V1_0/IServiceNotification$Stub;
.source "MiWillManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Notification"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;


# direct methods
.method private constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)V
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-direct {p0}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceNotification$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;-><init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)V

    return-void
.end method


# virtual methods
.method public final onRegistration(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "interfaceName"    # Ljava/lang/String;
    .param p2, "instanceName"    # Ljava/lang/String;
    .param p3, "preexisting"    # Z

    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onRegistration interfaceName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " instanceName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " preexisting = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MIWILL-MiWillManager"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const-string/jumbo v0, "vendor.xiaomi.hidl.miwill@1.0::IMiwillService"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 127
    :cond_0
    const-string v0, "default"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 130
    const-string v0, "prepare to get new hal service."

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 133
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$Notification;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 135
    :cond_2
    return-void
.end method
