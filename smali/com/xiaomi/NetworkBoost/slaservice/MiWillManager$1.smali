.class Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$1;
.super Ljava/lang/Object;
.source "MiWillManager.java"

# interfaces
.implements Landroid/os/IHwBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    .line 110
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public serviceDied(J)V
    .locals 3
    .param p1, "cookie"    # J

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HAL service died cookie = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mCookie = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->-$$Nest$fgetmCookie(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MIWILL-MiWillManager"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->-$$Nest$fgetmCookie(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;)J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager$1;->this$0:Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->-$$Nest$fputmMiWillHal(Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;Lvendor/xiaomi/hidl/miwill/V1_0/IMiwillService;)V

    .line 117
    :cond_0
    return-void
.end method
