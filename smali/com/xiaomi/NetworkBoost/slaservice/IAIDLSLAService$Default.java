public class com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService$Default implements com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService {
	 /* .source "IAIDLSLAService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "Default" */
} // .end annotation
/* # direct methods */
public com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService$Default ( ) {
/* .locals 0 */
/* .line 8 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Boolean addUidToLinkTurboWhiteList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 12 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 36 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.util.List getLinkTurboAppsTraffic ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 24 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.util.List getLinkTurboDefaultPn ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 32 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String getLinkTurboWhiteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 20 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean removeUidInLinkTurboWhiteList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 16 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setLinkTurboEnable ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 28 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
