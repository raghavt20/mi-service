class com.xiaomi.NetworkBoost.slaservice.SLAService$4 implements com.xiaomi.NetworkBoost.StatusManager$IDefaultNetworkInterfaceListener {
	 /* .source "SLAService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.SLAService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.slaservice.SLAService$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/slaservice/SLAService; */
/* .line 1038 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onDefaultNetwrokChange ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "ifacename" # Ljava/lang/String; */
/* .param p2, "ifacestatus" # I */
/* .line 1041 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmWifiReady ( );
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* if-eq p2, v2, :cond_0 */
	 /* .line 1042 */
	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfputmWifiReady ( v1 );
	 /* .line 1043 */
	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmInterface ( );
	 /* const-string/jumbo v3, "wlan0," */
	 final String v4 = ""; // const-string v4, ""
	 (( java.lang.String ) v0 ).replaceAll ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfputmInterface ( v0 );
	 /* .line 1044 */
	 v0 = 	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmIfaceNumber ( );
	 /* sub-int/2addr v0, v2 */
	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfputmIfaceNumber ( v0 );
	 /* .line 1045 */
} // :cond_0
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmWifiReady ( );
/* if-nez v0, :cond_3 */
/* if-ne p2, v2, :cond_3 */
/* .line 1046 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfputmWifiReady ( v2 );
/* .line 1047 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmInterface ( );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ","; // const-string v3, ","
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfputmInterface ( v0 );
/* .line 1048 */
v0 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmIfaceNumber ( );
/* add-int/2addr v0, v2 */
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfputmIfaceNumber ( v0 );
/* .line 1052 */
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Default Netwrok Change mInterface:"; // const-string v3, "Default Netwrok Change mInterface:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmInterface ( );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " mWifiReady:"; // const-string v3, " mWifiReady:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmWifiReady ( );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "SLM-SRV-SLAService"; // const-string v3, "SLM-SRV-SLAService"
android.util.Log .i ( v3,v0 );
/* .line 1055 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmMiWillManager ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1056 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$fgetmMiWillManager ( v0 );
v3 = com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmWifiReady ( );
if ( v3 != null) { // if-eqz v3, :cond_1
	 v3 = 	 com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$sfgetmSlaveWifiReady ( );
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 /* move v1, v2 */
	 } // :cond_1
	 (( com.xiaomi.NetworkBoost.slaservice.MiWillManager ) v0 ).setDualWifiReady ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/MiWillManager;->setDualWifiReady(Z)V
	 /* .line 1058 */
} // :cond_2
v0 = this.this$0;
final String v1 = "Default Netwrok Change"; // const-string v1, "Default Netwrok Change"
com.xiaomi.NetworkBoost.slaservice.SLAService .-$$Nest$mprocessNetworkCallback ( v0,v1 );
/* .line 1059 */
return;
/* .line 1050 */
} // :cond_3
return;
} // .end method
