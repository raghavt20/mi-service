class com.xiaomi.NetworkBoost.slaservice.SLATrack$WorkHandler extends android.os.Handler {
	 /* .source "SLATrack.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/slaservice/SLATrack; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "WorkHandler" */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.slaservice.SLATrack this$0; //synthetic
/* # direct methods */
public com.xiaomi.NetworkBoost.slaservice.SLATrack$WorkHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 74 */
this.this$0 = p1;
/* .line 75 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 76 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 18 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 80 */
/* move-object/from16 v1, p0 */
final String v2 = "SLM-SRV-SLATrack"; // const-string v2, "SLM-SRV-SLATrack"
/* new-instance v0, Ljava/io/File; */
int v3 = 0; // const/4 v3, 0x0
android.os.Environment .getDataSystemCeDirectory ( v3 );
/* const-string/jumbo v5, "sla" */
/* invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* move-object v4, v0 */
/* .line 82 */
/* .local v4, "prefsDir":Ljava/io/File; */
v0 = this.this$0;
/* new-instance v6, Ljava/io/File; */
/* const-string/jumbo v7, "sla_track" */
/* invoke-direct {v6, v4, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fputmMetricsPrefsFile ( v0,v6 );
/* .line 86 */
try { // :try_start_0
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetmContext ( v0 );
	 v6 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetmMetricsPrefsFile ( v6 );
	 (( android.content.Context ) v0 ).getSharedPreferences ( v6, v3 ); // invoke-virtual {v0, v6, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
	 /* .line 87 */
	 /* .local v0, "prefs":Landroid/content/SharedPreferences; */
	 /* :try_end_0 */
	 /* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 91 */
	 /* .local v6, "editor":Landroid/content/SharedPreferences$Editor; */
	 /* nop */
	 /* .line 92 */
	 /* move-object/from16 v7, p1 */
	 /* iget v8, v7, Landroid/os/Message;->what:I */
	 final String v9 = "dbsla"; // const-string v9, "dbsla"
	 /* const-string/jumbo v10, "sls" */
	 final String v11 = "dbsla_duration"; // const-string v11, "dbsla_duration"
	 /* const-string/jumbo v12, "sls_duration" */
	 /* const-string/jumbo v13, "sla_duration" */
	 /* const-wide/16 v14, 0x0 */
	 /* packed-switch v8, :pswitch_data_0 */
	 /* goto/16 :goto_0 */
	 /* .line 149 */
	 /* :pswitch_0 */
	 v5 = this.this$0;
	 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetDoubleWifiAndSLAStartTime ( v5 );
	 /* move-result-wide v8 */
	 /* cmp-long v5, v8, v14 */
	 if ( v5 != null) { // if-eqz v5, :cond_4
		 /* .line 150 */
		 /* new-instance v5, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v8 = "MSG_DOUBLEWIFI_SLA_STOP time = "; // const-string v8, "MSG_DOUBLEWIFI_SLA_STOP time = "
		 (( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v8 = this.this$0;
		 /* .line 151 */
		 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$mgetTime ( v8 );
		 /* move-result-wide v8 */
		 v10 = this.this$0;
		 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetDoubleWifiAndSLAStartTime ( v10 );
		 /* move-result-wide v12 */
		 v10 = 		 /* sub-long/2addr v8, v12 */
		 /* int-to-long v12, v10 */
		 /* add-long/2addr v8, v12 */
		 (( java.lang.StringBuilder ) v5 ).append ( v8, v9 ); // invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* .line 150 */
		 android.util.Log .i ( v2,v5 );
		 /* .line 152 */
		 v2 = this.this$0;
		 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$mgetTime ( v2 );
		 /* move-result-wide v8 */
		 v2 = this.this$0;
		 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetDoubleWifiAndSLAStartTime ( v2 );
		 /* move-result-wide v12 */
		 /* sub-long/2addr v8, v12 */
		 /* long-to-int v2, v8 */
		 v3 = 		 /* .line 153 */
		 /* add-int/2addr v2, v3 */
		 /* .line 152 */
		 /* .line 154 */
		 v2 = this.this$0;
		 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fputDoubleWifiAndSLAStartTime ( v2,v14,v15 );
		 /* goto/16 :goto_0 */
		 /* .line 142 */
		 /* :pswitch_1 */
		 v2 = this.this$0;
		 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetSLAStartTime ( v2 );
		 /* move-result-wide v2 */
		 /* cmp-long v2, v2, v14 */
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* .line 143 */
			 com.xiaomi.NetworkBoost.slaservice.SLATrack .sendMsgSlaStop ( );
			 /* .line 145 */
		 } // :cond_0
		 v2 = this.this$0;
		 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$mgetTime ( v2 );
		 /* move-result-wide v10 */
		 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fputDoubleWifiAndSLAStartTime ( v2,v10,v11 );
		 /* .line 146 */
		 int v2 = 1; // const/4 v2, 0x1
		 /* .line 147 */
		 /* goto/16 :goto_0 */
		 /* .line 133 */
		 /* :pswitch_2 */
		 v5 = this.this$0;
		 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetSLSStartTime ( v5 );
		 /* move-result-wide v8 */
		 /* cmp-long v5, v8, v14 */
		 if ( v5 != null) { // if-eqz v5, :cond_4
			 /* .line 134 */
			 /* new-instance v5, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v8 = "MSG_SLS_STOP time = "; // const-string v8, "MSG_SLS_STOP time = "
			 (( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 v8 = this.this$0;
			 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$mgetTime ( v8 );
			 /* move-result-wide v8 */
			 v10 = this.this$0;
			 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetSLSStartTime ( v10 );
			 /* move-result-wide v10 */
			 /* sub-long/2addr v8, v10 */
			 v10 = 			 /* .line 135 */
			 /* int-to-long v10, v10 */
			 /* add-long/2addr v8, v10 */
			 (( java.lang.StringBuilder ) v5 ).append ( v8, v9 ); // invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 /* .line 134 */
			 android.util.Log .i ( v2,v5 );
			 /* .line 136 */
			 v2 = this.this$0;
			 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$mgetTime ( v2 );
			 /* move-result-wide v8 */
			 v2 = this.this$0;
			 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetSLSStartTime ( v2 );
			 /* move-result-wide v10 */
			 /* sub-long/2addr v8, v10 */
			 /* long-to-int v2, v8 */
			 v3 = 			 /* .line 137 */
			 /* add-int/2addr v2, v3 */
			 /* .line 136 */
			 /* .line 138 */
			 v2 = this.this$0;
			 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fputSLSStartTime ( v2,v14,v15 );
			 /* goto/16 :goto_0 */
			 /* .line 129 */
			 /* :pswitch_3 */
			 v2 = this.this$0;
			 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$mgetTime ( v2 );
			 /* move-result-wide v8 */
			 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fputSLSStartTime ( v2,v8,v9 );
			 /* .line 130 */
			 int v2 = 1; // const/4 v2, 0x1
			 /* .line 131 */
			 /* goto/16 :goto_0 */
			 /* .line 120 */
			 /* :pswitch_4 */
			 v5 = this.this$0;
			 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetSLAStartTime ( v5 );
			 /* move-result-wide v8 */
			 /* cmp-long v5, v8, v14 */
			 if ( v5 != null) { // if-eqz v5, :cond_4
				 /* .line 121 */
				 /* new-instance v5, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v8 = "MSG_SLA_STOP time = "; // const-string v8, "MSG_SLA_STOP time = "
				 (( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 v8 = this.this$0;
				 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$mgetTime ( v8 );
				 /* move-result-wide v8 */
				 v10 = this.this$0;
				 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetSLAStartTime ( v10 );
				 /* move-result-wide v10 */
				 /* sub-long/2addr v8, v10 */
				 v10 = 				 /* .line 122 */
				 /* int-to-long v10, v10 */
				 /* add-long/2addr v8, v10 */
				 (( java.lang.StringBuilder ) v5 ).append ( v8, v9 ); // invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 /* .line 121 */
				 android.util.Log .i ( v2,v5 );
				 /* .line 123 */
				 v2 = this.this$0;
				 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$mgetTime ( v2 );
				 /* move-result-wide v8 */
				 v2 = this.this$0;
				 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetSLAStartTime ( v2 );
				 /* move-result-wide v10 */
				 /* sub-long/2addr v8, v10 */
				 /* long-to-int v2, v8 */
				 v3 = 				 /* .line 124 */
				 /* add-int/2addr v2, v3 */
				 /* .line 123 */
				 /* .line 125 */
				 v2 = this.this$0;
				 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fputSLAStartTime ( v2,v14,v15 );
				 /* .line 116 */
				 /* :pswitch_5 */
				 v2 = this.this$0;
				 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$mgetTime ( v2 );
				 /* move-result-wide v8 */
				 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fputSLAStartTime ( v2,v8,v9 );
				 /* .line 117 */
				 int v2 = 1; // const/4 v2, 0x1
				 /* .line 118 */
				 /* .line 112 */
				 /* :pswitch_6 */
				 int v2 = 1; // const/4 v2, 0x1
				 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$sfgetmWorkHandler ( );
				 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$sfgetmWorkHandler ( );
				 (( android.os.Handler ) v5 ).obtainMessage ( v2 ); // invoke-virtual {v5, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
				 /* const-wide/16 v8, 0x1 */
				 (( android.os.Handler ) v3 ).sendMessageDelayed ( v2, v8, v9 ); // invoke-virtual {v3, v2, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
				 /* .line 114 */
				 /* .line 94 */
				 /* :pswitch_7 */
				 final String v8 = "handleMessage: MSG_START_REPORT"; // const-string v8, "handleMessage: MSG_START_REPORT"
				 android.util.Log .i ( v2,v8 );
				 /* .line 95 */
				 v2 = this.this$0;
				 (( com.xiaomi.NetworkBoost.slaservice.SLATrack ) v2 ).reportMQSEvent ( v0 ); // invoke-virtual {v2, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLATrack;->reportMQSEvent(Landroid/content/SharedPreferences;)V
				 /* .line 96 */
				 v2 = this.this$0;
				 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetSLAStartTime ( v2 );
				 /* move-result-wide v16 */
				 /* cmp-long v2, v16, v14 */
				 /* if-nez v2, :cond_1 */
				 /* .line 97 */
				 /* .line 99 */
			 } // :cond_1
			 v2 = this.this$0;
			 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetSLSStartTime ( v2 );
			 /* move-result-wide v16 */
			 /* cmp-long v2, v16, v14 */
			 /* if-nez v2, :cond_2 */
			 /* .line 100 */
			 /* .line 102 */
		 } // :cond_2
		 v2 = this.this$0;
		 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$fgetDoubleWifiAndSLAStartTime ( v2 );
		 /* move-result-wide v16 */
		 /* cmp-long v2, v16, v14 */
		 /* if-nez v2, :cond_3 */
		 /* .line 103 */
		 /* .line 105 */
	 } // :cond_3
	 /* .line 106 */
	 /* .line 107 */
	 /* .line 108 */
	 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$sfgetmWorkHandler ( );
	 com.xiaomi.NetworkBoost.slaservice.SLATrack .-$$Nest$sfgetmWorkHandler ( );
	 int v5 = 2; // const/4 v5, 0x2
	 (( android.os.Handler ) v3 ).obtainMessage ( v5 ); // invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
	 /* const-wide/32 v8, 0x5265bff */
	 (( android.os.Handler ) v2 ).sendMessageDelayed ( v3, v8, v9 ); // invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
	 /* .line 110 */
	 /* nop */
	 /* .line 160 */
} // :cond_4
} // :goto_0
/* .line 161 */
return;
/* .line 88 */
} // .end local v0 # "prefs":Landroid/content/SharedPreferences;
} // .end local v6 # "editor":Landroid/content/SharedPreferences$Editor;
/* :catch_0 */
/* move-exception v0 */
/* move-object/from16 v7, p1 */
/* .line 89 */
/* .local v0, "e":Ljava/lang/IllegalStateException; */
final String v3 = "IllegalStateException"; // const-string v3, "IllegalStateException"
android.util.Log .e ( v2,v3,v0 );
/* .line 90 */
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
