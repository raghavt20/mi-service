.class public Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;
.super Ljava/lang/Object;
.source "FormatBytesUtil.java"


# static fields
.field public static BString:Ljava/lang/String; = null

.field public static final GB:J = 0x40000000L

.field public static GBString:Ljava/lang/String; = null

.field public static final KB:J = 0x400L

.field public static KBString:Ljava/lang/String; = null

.field public static final MB:J = 0x100000L

.field public static MBString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    const-string v0, "GB"

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->GBString:Ljava/lang/String;

    .line 8
    const-string v0, "MB"

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->MBString:Ljava/lang/String;

    .line 9
    const-string v0, "KB"

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->KBString:Ljava/lang/String;

    .line 10
    const-string v0, "B"

    sput-object v0, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->BString:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static formatBytes(J)Ljava/lang/String;
    .locals 7
    .param p0, "bytes"    # J

    .line 32
    const/4 v0, 0x0

    .line 34
    .local v0, "uint":Ljava/lang/String;
    const/4 v1, 0x1

    .line 35
    .local v1, "numOfPoint":I
    const-wide/32 v2, 0x40000000

    cmp-long v2, p0, v2

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    if-ltz v2, :cond_0

    .line 36
    long-to-double v5, p0

    mul-double/2addr v5, v3

    const-wide/high16 v2, 0x41d0000000000000L    # 1.073741824E9

    div-double/2addr v5, v2

    .line 37
    .local v5, "f":D
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->GBString:Ljava/lang/String;

    .line 38
    const/4 v1, 0x2

    goto :goto_0

    .line 39
    .end local v5    # "f":D
    :cond_0
    const-wide/32 v5, 0x100000

    cmp-long v2, p0, v5

    if-ltz v2, :cond_1

    .line 40
    long-to-double v5, p0

    mul-double/2addr v5, v3

    const-wide/high16 v2, 0x4130000000000000L    # 1048576.0

    div-double/2addr v5, v2

    .line 41
    .restart local v5    # "f":D
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->MBString:Ljava/lang/String;

    goto :goto_0

    .line 42
    .end local v5    # "f":D
    :cond_1
    const-wide/16 v5, 0x400

    cmp-long v2, p0, v5

    if-ltz v2, :cond_2

    .line 43
    long-to-double v5, p0

    mul-double/2addr v5, v3

    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    div-double/2addr v5, v2

    .line 44
    .restart local v5    # "f":D
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->KBString:Ljava/lang/String;

    goto :goto_0

    .line 46
    .end local v5    # "f":D
    :cond_2
    long-to-double v5, p0

    mul-double/2addr v5, v3

    .line 47
    .restart local v5    # "f":D
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->BString:Ljava/lang/String;

    .line 49
    :goto_0
    invoke-static {v5, v6, v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->textFormat(DLjava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static formatBytes(JI)Ljava/lang/String;
    .locals 6
    .param p0, "bytes"    # J
    .param p2, "numOfPoint"    # I

    .line 13
    const/4 v0, 0x0

    .line 15
    .local v0, "uint":Ljava/lang/String;
    const-wide/32 v1, 0x40000000

    cmp-long v1, p0, v1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    if-ltz v1, :cond_0

    .line 16
    long-to-double v4, p0

    mul-double/2addr v4, v2

    const-wide/high16 v1, 0x41d0000000000000L    # 1.073741824E9

    div-double/2addr v4, v1

    .line 17
    .local v4, "f":D
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->GBString:Ljava/lang/String;

    goto :goto_0

    .line 18
    .end local v4    # "f":D
    :cond_0
    const-wide/32 v4, 0x100000

    cmp-long v1, p0, v4

    if-ltz v1, :cond_1

    .line 19
    long-to-double v4, p0

    mul-double/2addr v4, v2

    const-wide/high16 v1, 0x4130000000000000L    # 1048576.0

    div-double/2addr v4, v1

    .line 20
    .restart local v4    # "f":D
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->MBString:Ljava/lang/String;

    goto :goto_0

    .line 21
    .end local v4    # "f":D
    :cond_1
    const-wide/16 v4, 0x400

    cmp-long v1, p0, v4

    if-ltz v1, :cond_2

    .line 22
    long-to-double v4, p0

    mul-double/2addr v4, v2

    const-wide/high16 v1, 0x4090000000000000L    # 1024.0

    div-double/2addr v4, v1

    .line 23
    .restart local v4    # "f":D
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->KBString:Ljava/lang/String;

    goto :goto_0

    .line 25
    .end local v4    # "f":D
    :cond_2
    long-to-double v4, p0

    mul-double/2addr v4, v2

    .line 26
    .restart local v4    # "f":D
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->BString:Ljava/lang/String;

    .line 28
    :goto_0
    invoke-static {v4, v5, v0, p2}, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->textFormat(DLjava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static formatBytesByMB(J)Ljava/lang/String;
    .locals 5
    .param p0, "bytes"    # J

    .line 53
    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->MBString:Ljava/lang/String;

    .line 54
    .local v0, "uint":Ljava/lang/String;
    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    long-to-double v3, p0

    mul-double/2addr v3, v1

    const-wide/high16 v1, 0x4130000000000000L    # 1048576.0

    div-double/2addr v3, v1

    .line 55
    .local v3, "f":D
    const/4 v1, 0x0

    invoke-static {v3, v4, v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->textFormat(DLjava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static formatBytesNoUint(J)Ljava/lang/String;
    .locals 5
    .param p0, "bytes"    # J

    .line 99
    const-wide/32 v0, 0x40000000

    cmp-long v0, p0, v0

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    if-ltz v0, :cond_0

    .line 100
    long-to-double v3, p0

    mul-double/2addr v3, v1

    const-wide/high16 v0, 0x41d0000000000000L    # 1.073741824E9

    div-double/2addr v3, v0

    .local v3, "f":D
    goto :goto_0

    .line 101
    .end local v3    # "f":D
    :cond_0
    const-wide/32 v3, 0x100000

    cmp-long v0, p0, v3

    if-lez v0, :cond_1

    .line 102
    long-to-double v3, p0

    mul-double/2addr v3, v1

    const-wide/high16 v0, 0x4130000000000000L    # 1048576.0

    div-double/2addr v3, v0

    .restart local v3    # "f":D
    goto :goto_0

    .line 103
    .end local v3    # "f":D
    :cond_1
    const-wide/16 v3, 0x400

    cmp-long v0, p0, v3

    if-lez v0, :cond_2

    .line 104
    long-to-double v3, p0

    mul-double/2addr v3, v1

    const-wide/high16 v0, 0x4090000000000000L    # 1024.0

    div-double/2addr v3, v0

    .restart local v3    # "f":D
    goto :goto_0

    .line 106
    .end local v3    # "f":D
    :cond_2
    long-to-double v3, p0

    mul-double/2addr v3, v1

    .line 108
    .restart local v3    # "f":D
    :goto_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v3, v4, v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->textFormat(DLjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formatBytesSplited(J)[Ljava/lang/String;
    .locals 8
    .param p0, "bytes"    # J

    .line 59
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 61
    .local v0, "result":[Ljava/lang/String;
    const/4 v1, 0x2

    .line 62
    .local v1, "point":I
    const-wide/32 v2, 0x40000000

    cmp-long v2, p0, v2

    const/4 v3, 0x1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    if-ltz v2, :cond_0

    .line 63
    long-to-double v6, p0

    mul-double/2addr v6, v4

    const-wide/high16 v4, 0x41d0000000000000L    # 1.073741824E9

    div-double/2addr v6, v4

    .line 64
    .local v6, "f":D
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->GBString:Ljava/lang/String;

    aput-object v2, v0, v3

    goto :goto_0

    .line 65
    .end local v6    # "f":D
    :cond_0
    const-wide/32 v6, 0x100000

    cmp-long v2, p0, v6

    if-ltz v2, :cond_1

    .line 66
    long-to-double v6, p0

    mul-double/2addr v6, v4

    const-wide/high16 v4, 0x4130000000000000L    # 1048576.0

    div-double/2addr v6, v4

    .line 67
    .restart local v6    # "f":D
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->MBString:Ljava/lang/String;

    aput-object v2, v0, v3

    goto :goto_0

    .line 68
    .end local v6    # "f":D
    :cond_1
    const-wide/16 v6, 0x400

    cmp-long v2, p0, v6

    if-ltz v2, :cond_2

    .line 69
    long-to-double v6, p0

    mul-double/2addr v6, v4

    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    div-double/2addr v6, v4

    .line 70
    .restart local v6    # "f":D
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->KBString:Ljava/lang/String;

    aput-object v2, v0, v3

    goto :goto_0

    .line 72
    .end local v6    # "f":D
    :cond_2
    long-to-double v6, p0

    mul-double/2addr v6, v4

    .line 73
    .restart local v6    # "f":D
    sget-object v2, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->BString:Ljava/lang/String;

    aput-object v2, v0, v3

    .line 74
    const/4 v1, 0x0

    .line 76
    :goto_0
    const/4 v2, 0x0

    invoke-static {v6, v7, v2, v1}, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->textFormat(DLjava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v0, v3

    .line 77
    return-object v0
.end method

.method public static formatBytesWithUintLong(J)Ljava/lang/String;
    .locals 3
    .param p0, "bytes"    # J

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-wide/32 v1, 0x100000

    div-long v1, p0, v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->MBString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formatMaxBytes(J)J
    .locals 3
    .param p0, "bytes"    # J

    .line 86
    const-wide/32 v0, 0x40000000

    cmp-long v2, p0, v0

    if-ltz v2, :cond_0

    .line 87
    return-wide v0

    .line 88
    :cond_0
    const-wide/32 v0, 0x100000

    cmp-long v2, p0, v0

    if-lez v2, :cond_1

    .line 89
    return-wide v0

    .line 90
    :cond_1
    const-wide/16 v0, 0x400

    cmp-long v2, p0, v0

    if-lez v2, :cond_2

    .line 91
    return-wide v0

    .line 93
    :cond_2
    const-wide/16 v0, 0x1

    return-wide v0
.end method

.method public static formatUniteUnit(JJ)Ljava/lang/String;
    .locals 4
    .param p0, "bytes"    # J
    .param p2, "maxUnit"    # J

    .line 81
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    long-to-double v2, p0

    mul-double/2addr v2, v0

    long-to-double v0, p2

    div-double/2addr v2, v0

    .line 82
    .local v2, "f":D
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v2, v3, v0, v1}, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->textFormat(DLjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getBytesByUnit(FLjava/lang/String;)J
    .locals 2
    .param p0, "value"    # F
    .param p1, "unit"    # Ljava/lang/String;

    .line 137
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 138
    const-string v0, "k"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    const/high16 v0, 0x44800000    # 1024.0f

    mul-float/2addr v0, p0

    float-to-long v0, v0

    return-wide v0

    .line 140
    :cond_0
    const-string v0, "m"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    const/high16 v0, 0x49800000    # 1048576.0f

    mul-float/2addr v0, p0

    float-to-long v0, v0

    return-wide v0

    .line 142
    :cond_1
    const-string v0, "g"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 143
    const/high16 v0, 0x4e800000

    mul-float/2addr v0, p0

    float-to-long v0, v0

    return-wide v0

    .line 145
    :cond_2
    float-to-long v0, p0

    return-wide v0
.end method

.method private static textFormat(DLjava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p0, "f"    # D
    .param p2, "uint"    # Ljava/lang/String;
    .param p3, "numOfPoint"    # I

    .line 117
    const-wide v0, 0x408f3c0000000000L    # 999.5

    cmpl-double v0, p0, v0

    if-gtz v0, :cond_2

    sget-object v0, Lcom/xiaomi/NetworkBoost/slaservice/FormatBytesUtil;->BString:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 119
    :cond_0
    const-wide v0, 0x4058e00000000000L    # 99.5

    cmpl-double v0, p0, v0

    if-lez v0, :cond_1

    .line 120
    invoke-static {p0, p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "%.01f"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .local v0, "text":Ljava/lang/String;
    goto :goto_1

    .line 122
    .end local v0    # "text":Ljava/lang/String;
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 123
    .local v0, "format":Ljava/lang/StringBuilder;
    const-string v1, "%.0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 125
    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 126
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .end local v0    # "format":Ljava/lang/StringBuilder;
    invoke-static {p0, p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "text":Ljava/lang/String;
    goto :goto_1

    .line 118
    .end local v0    # "text":Ljava/lang/String;
    :cond_2
    :goto_0
    double-to-int v0, p0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "%d"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 129
    .restart local v0    # "text":Ljava/lang/String;
    :goto_1
    if-eqz p2, :cond_3

    .line 130
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 132
    :cond_3
    return-object v0
.end method
