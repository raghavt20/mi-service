class com.xiaomi.NetworkBoost.StatusManager$8 extends android.content.BroadcastReceiver {
	 /* .source "StatusManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/xiaomi/NetworkBoost/StatusManager;->registerScreenStatusReceiver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.StatusManager this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.StatusManager$8 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/StatusManager; */
/* .line 1139 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 1142 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 1143 */
/* .local v0, "action":Ljava/lang/String; */
v1 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmScreenStatusListenerList ( v1 );
/* monitor-enter v1 */
/* .line 1144 */
try { // :try_start_0
	 final String v2 = "android.intent.action.SCREEN_ON"; // const-string v2, "android.intent.action.SCREEN_ON"
	 v2 = 	 (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* .line 1145 */
		 final String v2 = "NetworkBoostStatusManager"; // const-string v2, "NetworkBoostStatusManager"
		 final String v3 = "ACTION_SCREEN_ON"; // const-string v3, "ACTION_SCREEN_ON"
		 android.util.Log .d ( v2,v3 );
		 /* .line 1146 */
		 v2 = this.this$0;
		 com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmScreenStatusListenerList ( v2 );
	 v3 = 	 } // :goto_0
	 if ( v3 != null) { // if-eqz v3, :cond_0
		 /* check-cast v3, Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener; */
		 /* .line 1147 */
		 /* .local v3, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener; */
		 /* .line 1148 */
	 } // .end local v3 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;
} // :cond_0
/* .line 1149 */
} // :cond_1
final String v2 = "android.intent.action.SCREEN_OFF"; // const-string v2, "android.intent.action.SCREEN_OFF"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1150 */
final String v2 = "NetworkBoostStatusManager"; // const-string v2, "NetworkBoostStatusManager"
final String v3 = "ACTION_SCREEN_OFF"; // const-string v3, "ACTION_SCREEN_OFF"
android.util.Log .d ( v2,v3 );
/* .line 1151 */
v2 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmScreenStatusListenerList ( v2 );
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener; */
/* .line 1152 */
/* .restart local v3 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener; */
/* .line 1153 */
} // .end local v3 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;
/* .line 1155 */
} // :cond_2
} // :goto_2
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1157 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$mcheckSensorStatus ( v1 );
/* .line 1158 */
return;
/* .line 1155 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
