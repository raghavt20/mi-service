public class com.xiaomi.NetworkBoost.NetworkBoostService$NetworkBoostServiceManager extends com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost$Stub {
	 /* .source "NetworkBoostService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkBoostService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "NetworkBoostServiceManager" */
} // .end annotation
/* # static fields */
public static final java.lang.String SERVICE_NAME;
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkBoostService this$0; //synthetic
/* # direct methods */
public com.xiaomi.NetworkBoost.NetworkBoostService$NetworkBoostServiceManager ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkBoostService; */
/* .line 87 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Boolean abortScan ( ) {
/* .locals 2 */
/* .line 183 */
int v0 = 0; // const/4 v0, 0x0
/* .line 184 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 185 */
	 v1 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
	 v0 = 	 (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).abortScan ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->abortScan()Z
	 /* .line 187 */
} // :cond_0
} // .end method
public Boolean activeScan ( Integer[] p0 ) {
/* .locals 2 */
/* .param p1, "channelList" # [I */
/* .line 173 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "activeScan:"; // const-string v1, "activeScan:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkBoostService"; // const-string v1, "NetworkBoostService"
android.util.Log .i ( v1,v0 );
/* .line 174 */
int v0 = 0; // const/4 v0, 0x0
/* .line 175 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 176 */
	 v1 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
	 v0 = 	 (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).activeScan ( p1 ); // invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->activeScan([I)Z
	 /* .line 178 */
} // :cond_0
} // .end method
public Boolean connectSlaveWifi ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "networkId" # I */
/* .line 131 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean disableWifiSelectionOpt ( com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback p0 ) {
/* .locals 4 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
/* .line 443 */
int v0 = 0; // const/4 v0, 0x0
/* .line 444 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 446 */
	 try { // :try_start_0
		 v1 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
		 v1 = 		 (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).disableWifiSelectionOpt ( p1 ); // invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->disableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;)Z
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* move v0, v1 */
		 /* .line 450 */
		 /* .line 447 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 448 */
		 /* .local v1, "e":Ljava/lang/Exception; */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = " disableWifiSelectionOpt:"; // const-string v3, " disableWifiSelectionOpt:"
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v3 = "NetworkBoostService"; // const-string v3, "NetworkBoostService"
		 android.util.Log .d ( v3,v2 );
		 /* .line 449 */
		 (( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
		 /* .line 452 */
	 } // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
} // .end method
public Boolean disableWifiSelectionOptByUid ( com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
/* .param p2, "uid" # I */
/* .line 386 */
int v0 = 0; // const/4 v0, 0x0
/* .line 387 */
/* .local v0, "ret":Z */
v1 = this.this$0;
v1 = com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$misSystemProcess ( v1 );
/* if-nez v1, :cond_0 */
/* .line 388 */
/* .line 390 */
} // :cond_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 392 */
try { // :try_start_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).disableWifiSelectionOpt ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->disableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;I)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 396 */
/* .line 393 */
/* :catch_0 */
/* move-exception v1 */
/* .line 394 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " disableWifiSelectionOpt:"; // const-string v3, " disableWifiSelectionOpt:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "NetworkBoostService"; // const-string v3, "NetworkBoostService"
android.util.Log .d ( v3,v2 );
/* .line 395 */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 398 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_0
} // .end method
public Boolean disconnectSlaveWifi ( ) {
/* .locals 1 */
/* .line 136 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 2 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "writer" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 524 */
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$sfgetmContext ( );
final String v1 = "android.permission.DUMP"; // const-string v1, "android.permission.DUMP"
v0 = (( android.content.Context ) v0 ).checkCallingOrSelfPermission ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 526 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: can\'t dump xiaomi.NetworkBoostServiceManager from pid="; // const-string v1, "Permission Denial: can\'t dump xiaomi.NetworkBoostServiceManager from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 527 */
v1 = com.xiaomi.NetworkBoost.NetworkBoostService$NetworkBoostServiceManager .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.xiaomi.NetworkBoost.NetworkBoostService$NetworkBoostServiceManager .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " due to missing android.permission.DUMP permission"; // const-string v1, " due to missing android.permission.DUMP permission"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 526 */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 529 */
return;
/* .line 531 */
} // :cond_0
com.xiaomi.NetworkBoost.NetworkBoostService .dumpModule ( p1,p2,p3 );
/* .line 532 */
return;
} // .end method
public Boolean enableWifiSelectionOpt ( com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
/* .param p2, "type" # I */
/* .line 403 */
int v0 = 0; // const/4 v0, 0x0
/* .line 405 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 407 */
try { // :try_start_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).enableWifiSelectionOpt ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->enableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;I)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 411 */
/* .line 408 */
/* :catch_0 */
/* move-exception v1 */
/* .line 409 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " enableWifiSelectionOpt:"; // const-string v3, " enableWifiSelectionOpt:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "NetworkBoostService"; // const-string v3, "NetworkBoostService"
android.util.Log .d ( v3,v2 );
/* .line 410 */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 414 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
} // .end method
public Boolean enableWifiSelectionOptByUid ( com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
/* .param p2, "type" # I */
/* .param p3, "uid" # I */
/* .line 368 */
int v0 = 0; // const/4 v0, 0x0
/* .line 369 */
/* .local v0, "ret":Z */
v1 = this.this$0;
v1 = com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$misSystemProcess ( v1 );
/* if-nez v1, :cond_0 */
/* .line 370 */
/* .line 372 */
} // :cond_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 374 */
try { // :try_start_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).enableWifiSelectionOpt ( p1, p2, p3 ); // invoke-virtual {v1, p1, p2, p3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->enableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;II)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 378 */
/* .line 375 */
/* :catch_0 */
/* move-exception v1 */
/* .line 376 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " enableWifiSelectionOpt:"; // const-string v3, " enableWifiSelectionOpt:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "NetworkBoostService"; // const-string v3, "NetworkBoostService"
android.util.Log .d ( v3,v2 );
/* .line 377 */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 381 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_0
} // .end method
public java.util.Map getAvailableIfaces ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 238 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 240 */
try { // :try_start_0
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v0 );
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v0 ).getAvailableIfaces ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->getAvailableIfaces()Ljava/util/Map;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 241 */
/* :catch_0 */
/* move-exception v0 */
/* .line 242 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " getAvailableIfaces:"; // const-string v2, " getAvailableIfaces:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkBoostService"; // const-string v2, "NetworkBoostService"
android.util.Log .d ( v2,v1 );
/* .line 243 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 247 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
} // .end method
public java.util.Map getQoEByAvailableIfaceName ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "ifaceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 159 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public com.xiaomi.NetworkBoost.NetLinkLayerQoE getQoEByAvailableIfaceNameV1 ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "ifaceName" # Ljava/lang/String; */
/* .line 275 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 277 */
try { // :try_start_0
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v0 );
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v0 ).getQoEByAvailableIfaceNameV1 ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->getQoEByAvailableIfaceNameV1(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 278 */
/* :catch_0 */
/* move-exception v0 */
/* .line 279 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " getQoEByAvailableIfaceNameV1:"; // const-string v2, " getQoEByAvailableIfaceNameV1:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkBoostService"; // const-string v2, "NetworkBoostService"
android.util.Log .d ( v2,v1 );
/* .line 280 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 284 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
/* invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;-><init>()V */
} // .end method
public Integer getServiceVersion ( ) {
/* .locals 2 */
/* .line 266 */
final String v0 = "NetworkBoostService"; // const-string v0, "NetworkBoostService"
final String v1 = "Service Version:6"; // const-string v1, "Service Version:6"
android.util.Log .d ( v0,v1 );
/* .line 267 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 268 */
int v0 = 6; // const/4 v0, 0x6
/* .line 270 */
} // :cond_0
/* const/16 v0, 0x64 */
} // .end method
public Boolean isCelluarDSDAState ( ) {
/* .locals 2 */
/* .line 475 */
int v0 = 0; // const/4 v0, 0x0
/* .line 476 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 477 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).isCelluarDSDAState ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isCelluarDSDAState()Z
/* .line 479 */
} // :cond_0
} // .end method
public Integer isSlaveWifiEnabledAndOthersOpt ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "type" # I */
/* .line 493 */
int v0 = -1; // const/4 v0, -0x1
/* .line 494 */
/* .local v0, "ret":I */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 496 */
try { // :try_start_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).isSlaveWifiEnabledAndOthersOpt ( p1 ); // invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSlaveWifiEnabledAndOthersOpt(I)I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 500 */
/* .line 497 */
/* :catch_0 */
/* move-exception v1 */
/* .line 498 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " isSlaveWifiEnabledAndOthersOpt:"; // const-string v3, " isSlaveWifiEnabledAndOthersOpt:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "NetworkBoostService"; // const-string v3, "NetworkBoostService"
android.util.Log .d ( v3,v2 );
/* .line 499 */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 502 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
} // .end method
public Integer isSlaveWifiEnabledAndOthersOptByUid ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "type" # I */
/* .param p2, "uid" # I */
/* .line 507 */
int v0 = -1; // const/4 v0, -0x1
/* .line 508 */
/* .local v0, "ret":I */
v1 = this.this$0;
v1 = com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$misSystemProcess ( v1 );
/* if-nez v1, :cond_0 */
/* .line 509 */
int v1 = -1; // const/4 v1, -0x1
/* .line 511 */
} // :cond_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 513 */
try { // :try_start_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).isSlaveWifiEnabledAndOthersOpt ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSlaveWifiEnabledAndOthersOpt(II)I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 517 */
/* .line 514 */
/* :catch_0 */
/* move-exception v1 */
/* .line 515 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " isSlaveWifiEnabledAndOthersOptByUid:"; // const-string v3, " isSlaveWifiEnabledAndOthersOptByUid:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "NetworkBoostService"; // const-string v3, "NetworkBoostService"
android.util.Log .d ( v3,v2 );
/* .line 516 */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 519 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_0
} // .end method
public Boolean isSupportDualCelluarData ( ) {
/* .locals 2 */
/* .line 457 */
int v0 = 0; // const/4 v0, 0x0
/* .line 458 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 459 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).isSupportDualCelluarData ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSupportDualCelluarData()Z
/* .line 461 */
} // :cond_0
} // .end method
public Boolean isSupportDualWifi ( ) {
/* .locals 2 */
/* .line 113 */
int v0 = 0; // const/4 v0, 0x0
/* .line 114 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 115 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).isSupportDualWifi ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSupportDualWifi()Z
/* .line 117 */
} // :cond_0
} // .end method
public Boolean isSupportMediaPlayerPolicy ( ) {
/* .locals 2 */
/* .line 466 */
int v0 = 0; // const/4 v0, 0x0
/* .line 467 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 468 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).isSupportMediaPlayerPolicy ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSupportMediaPlayerPolicy()Z
/* .line 470 */
} // :cond_0
} // .end method
public Boolean registerCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiNetworkCallback p0 ) {
/* .locals 2 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* .line 141 */
int v0 = 0; // const/4 v0, 0x0
/* .line 142 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 143 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).registerCallback ( p1 ); // invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registerCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
/* .line 145 */
} // :cond_0
} // .end method
public Boolean registerNetLinkCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .param p2, "interval" # I */
/* .line 289 */
int v0 = 0; // const/4 v0, 0x0
/* .line 290 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 292 */
try { // :try_start_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).registerNetLinkCallback ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registerNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;I)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 296 */
/* .line 293 */
/* :catch_0 */
/* move-exception v1 */
/* .line 294 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " registerNetLinkCallback:"; // const-string v3, " registerNetLinkCallback:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "NetworkBoostService"; // const-string v3, "NetworkBoostService"
android.util.Log .d ( v3,v2 );
/* .line 295 */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 298 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
} // .end method
public Boolean registerNetLinkCallbackByUid ( com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .param p2, "interval" # I */
/* .param p3, "uid" # I */
/* .line 317 */
int v0 = 0; // const/4 v0, 0x0
/* .line 318 */
/* .local v0, "ret":Z */
v1 = this.this$0;
v1 = com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$misSystemProcess ( v1 );
/* if-nez v1, :cond_0 */
/* .line 319 */
/* .line 321 */
} // :cond_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 323 */
try { // :try_start_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).registerNetLinkCallback ( p1, p2, p3 ); // invoke-virtual {v1, p1, p2, p3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registerNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;II)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 327 */
/* .line 324 */
/* :catch_0 */
/* move-exception v1 */
/* .line 325 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " registerNetLinkCallback:"; // const-string v3, " registerNetLinkCallback:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "NetworkBoostService"; // const-string v3, "NetworkBoostService"
android.util.Log .d ( v3,v2 );
/* .line 326 */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 329 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_0
} // .end method
public Boolean registerWifiLinkCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback p0 ) {
/* .locals 1 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback; */
/* .line 192 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void reportBssidScore ( java.util.Map p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 431 */
/* .local p1, "bssidScores":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 433 */
try { // :try_start_0
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v0 );
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v0 ).reportBssidScore ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->reportBssidScore(Ljava/util/Map;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 437 */
/* .line 434 */
/* :catch_0 */
/* move-exception v0 */
/* .line 435 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " reportBssidScore:"; // const-string v2, " reportBssidScore:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkBoostService"; // const-string v2, "NetworkBoostService"
android.util.Log .d ( v2,v1 );
/* .line 436 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 439 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
public java.util.Map requestAppTrafficStatistics ( Integer p0, Long p1, Long p2 ) {
/* .locals 7 */
/* .param p1, "type" # I */
/* .param p2, "startTime" # J */
/* .param p4, "endTime" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(IJJ)", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 252 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 254 */
try { // :try_start_0
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v0 );
/* move v2, p1 */
/* move-wide v3, p2 */
/* move-wide v5, p4 */
/* invoke-virtual/range {v1 ..v6}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->requestAppTrafficStatistics(IJJ)Ljava/util/Map; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 255 */
/* :catch_0 */
/* move-exception v0 */
/* .line 256 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " requestAppTrafficStatistics:"; // const-string v2, " requestAppTrafficStatistics:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkBoostService"; // const-string v2, "NetworkBoostService"
android.util.Log .d ( v2,v1 );
/* .line 257 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 261 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
} // .end method
public java.util.Map requestAppTrafficStatisticsByUid ( Integer p0, Long p1, Long p2, Integer p3 ) {
/* .locals 8 */
/* .param p1, "type" # I */
/* .param p2, "startTime" # J */
/* .param p4, "endTime" # J */
/* .param p6, "uid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(IJJI)", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 351 */
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$misSystemProcess ( v0 );
/* if-nez v0, :cond_0 */
/* .line 352 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 354 */
} // :cond_0
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 356 */
try { // :try_start_0
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v0 );
/* move v2, p1 */
/* move-wide v3, p2 */
/* move-wide v5, p4 */
/* move v7, p6 */
/* invoke-virtual/range {v1 ..v7}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->requestAppTrafficStatistics(IJJI)Ljava/util/Map; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 357 */
/* :catch_0 */
/* move-exception v0 */
/* .line 358 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " requestAppTrafficStatistics:"; // const-string v2, " requestAppTrafficStatistics:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkBoostService"; // const-string v2, "NetworkBoostService"
android.util.Log .d ( v2,v1 );
/* .line 359 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 363 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_1
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
} // .end method
public Boolean resumeBackgroundScan ( ) {
/* .locals 2 */
/* .line 211 */
int v0 = 0; // const/4 v0, 0x0
/* .line 212 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 213 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).resumeBackgroundScan ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->resumeBackgroundScan()Z
/* .line 215 */
} // :cond_0
} // .end method
public Boolean resumeWifiPowerSave ( ) {
/* .locals 2 */
/* .line 229 */
int v0 = 0; // const/4 v0, 0x0
/* .line 230 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 231 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).resumeWifiPowerSave ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->resumeWifiPowerSave()Z
/* .line 233 */
} // :cond_0
} // .end method
public Boolean setDualCelluarDataEnable ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 484 */
int v0 = 0; // const/4 v0, 0x0
/* .line 485 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 486 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).setDualCelluarDataEnable ( p1 ); // invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setDualCelluarDataEnable(Z)Z
/* .line 488 */
} // :cond_0
} // .end method
public Boolean setSlaveWifiEnable ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 122 */
int v0 = 0; // const/4 v0, 0x0
/* .line 123 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 124 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).setSlaveWifiEnabled ( p1 ); // invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setSlaveWifiEnabled(Z)Z
/* .line 126 */
} // :cond_0
} // .end method
public Boolean setSockPrio ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "fd" # I */
/* .param p2, "prio" # I */
/* .line 95 */
int v0 = 0; // const/4 v0, 0x0
/* .line 96 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 97 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).setSockPrio ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setSockPrio(II)Z
/* .line 99 */
} // :cond_0
} // .end method
public Boolean setTCPCongestion ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "fd" # I */
/* .param p2, "cc" # Ljava/lang/String; */
/* .line 104 */
int v0 = 0; // const/4 v0, 0x0
/* .line 105 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 106 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).setTCPCongestion ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setTCPCongestion(ILjava/lang/String;)Z
/* .line 108 */
} // :cond_0
} // .end method
public Boolean setTrafficTransInterface ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "fd" # I */
/* .param p2, "bindInterface" # Ljava/lang/String; */
/* .line 164 */
int v0 = 0; // const/4 v0, 0x0
/* .line 165 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 166 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).setTrafficTransInterface ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setTrafficTransInterface(ILjava/lang/String;)Z
/* .line 168 */
} // :cond_0
} // .end method
public Boolean suspendBackgroundScan ( ) {
/* .locals 2 */
/* .line 197 */
int v0 = 0; // const/4 v0, 0x0
/* .line 198 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 199 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).suspendBackgroundScan ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->suspendBackgroundScan()Z
/* .line 201 */
} // :cond_0
} // .end method
public Boolean suspendWifiPowerSave ( ) {
/* .locals 2 */
/* .line 220 */
int v0 = 0; // const/4 v0, 0x0
/* .line 221 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 222 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).suspendWifiPowerSave ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->suspendWifiPowerSave()Z
/* .line 224 */
} // :cond_0
} // .end method
public void triggerWifiSelection ( ) {
/* .locals 3 */
/* .line 419 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 421 */
try { // :try_start_0
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v0 );
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v0 ).triggerWifiSelection ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->triggerWifiSelection()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 425 */
/* .line 422 */
/* :catch_0 */
/* move-exception v0 */
/* .line 423 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " triggerWifiSelection:"; // const-string v2, " triggerWifiSelection:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkBoostService"; // const-string v2, "NetworkBoostService"
android.util.Log .d ( v2,v1 );
/* .line 424 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 427 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
public Boolean unregisterCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiNetworkCallback p0 ) {
/* .locals 2 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* .line 150 */
int v0 = 0; // const/4 v0, 0x0
/* .line 151 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 152 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).unregisterCallback ( p1 ); // invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
/* .line 154 */
} // :cond_0
} // .end method
public Boolean unregisterNetLinkCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback p0 ) {
/* .locals 4 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .line 303 */
int v0 = 0; // const/4 v0, 0x0
/* .line 304 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 306 */
try { // :try_start_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).unregisterNetLinkCallback ( p1 ); // invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 310 */
/* .line 307 */
/* :catch_0 */
/* move-exception v1 */
/* .line 308 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " unregisterNetLinkCallback:"; // const-string v3, " unregisterNetLinkCallback:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "NetworkBoostService"; // const-string v3, "NetworkBoostService"
android.util.Log .d ( v3,v2 );
/* .line 309 */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 312 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
} // .end method
public Boolean unregisterNetLinkCallbackByUid ( com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .param p2, "uid" # I */
/* .line 334 */
int v0 = 0; // const/4 v0, 0x0
/* .line 335 */
/* .local v0, "ret":Z */
v1 = this.this$0;
v1 = com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$misSystemProcess ( v1 );
/* if-nez v1, :cond_0 */
/* .line 336 */
/* .line 338 */
} // :cond_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 340 */
try { // :try_start_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkSDKService ( v1 );
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).unregisterNetLinkCallback ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;I)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 344 */
/* .line 341 */
/* :catch_0 */
/* move-exception v1 */
/* .line 342 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " unregisterNetLinkCallback:"; // const-string v3, " unregisterNetLinkCallback:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "NetworkBoostService"; // const-string v3, "NetworkBoostService"
android.util.Log .d ( v3,v2 );
/* .line 343 */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 346 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_0
} // .end method
public Boolean unregisterWifiLinkCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback p0 ) {
/* .locals 1 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback; */
/* .line 206 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
