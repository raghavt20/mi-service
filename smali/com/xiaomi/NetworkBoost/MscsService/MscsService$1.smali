.class Lcom/xiaomi/NetworkBoost/MscsService/MscsService$1;
.super Landroid/database/ContentObserver;
.source "MscsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->registerMscsChangeObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/MscsService/MscsService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 147
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$1;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 6
    .param p1, "selfChange"    # Z

    .line 150
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$1;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "cloud_mscs_enable"

    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fputmMSCSEnable(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V

    .line 152
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$1;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v5, "cloud_wifi7_mscs_enable"

    invoke-static {v1, v5, v3, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-ne v1, v2, :cond_1

    move v3, v2

    :cond_1
    invoke-static {v0, v3}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fputmWifi7MSCSEnable(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MSCS cloud change, mMSCSEnable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$1;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmMSCSEnable(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MscsService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WIFI7 MSCS cloud change, mWifi7MSCSEnable: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$1;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmWifi7MSCSEnable(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    return-void
.end method
