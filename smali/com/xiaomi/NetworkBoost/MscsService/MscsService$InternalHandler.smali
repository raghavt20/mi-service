.class Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;
.super Landroid/os/Handler;
.source "MscsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/MscsService/MscsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;


# direct methods
.method public constructor <init>(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 236
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    .line 237
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 238
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .line 241
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "MscsService"

    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_2

    .line 261
    :pswitch_0
    const-string v0, "removeMessages"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 263
    goto/16 :goto_2

    .line 243
    :pswitch_1
    invoke-static {}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$sfgetRESULT_CODE_NO_EFFECT()I

    move-result v0

    .line 244
    .local v0, "ret0":I
    invoke-static {}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$sfgetRESULT_CODE_NO_EFFECT()I

    move-result v3

    .line 245
    .local v3, "ret1":I
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmMasterWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmMasterWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 246
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    const-string/jumbo v5, "wlan0"

    invoke-static {v4, v5}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$menableMSCS(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Ljava/lang/String;)I

    move-result v0

    .line 248
    :cond_0
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmSlaveWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmSlaveWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 249
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    const-string/jumbo v5, "wlan1"

    invoke-static {v4, v5}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$menableMSCS(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Ljava/lang/String;)I

    move-result v3

    .line 251
    :cond_1
    invoke-static {}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$sfgetRESULT_CODE_SUCCESS()I

    move-result v4

    if-eq v0, v4, :cond_2

    invoke-static {}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$sfgetRESULT_CODE_SUCCESS()I

    move-result v4

    if-ne v3, v4, :cond_3

    .line 252
    :cond_2
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/os/Handler;

    move-result-object v4

    const-wide/32 v5, 0xc350

    invoke-virtual {v4, v2, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 254
    :cond_3
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$sfgetRESULT_CODE_SUCCESS()I

    move-result v5

    const/4 v6, 0x0

    if-ne v0, v5, :cond_4

    move v5, v2

    goto :goto_0

    :cond_4
    move v5, v6

    :goto_0
    invoke-static {v4, v5}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fputmMasterWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V

    .line 255
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$sfgetRESULT_CODE_SUCCESS()I

    move-result v5

    if-ne v3, v5, :cond_5

    goto :goto_1

    :cond_5
    move v2, v6

    :goto_1
    invoke-static {v4, v2}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fputmSlaveWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V

    .line 256
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mMasterWifiUsed:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmMasterWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " mMasterWifiSupport:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmMasterWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " mSlaveWifiUsed:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmSlaveWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " mSlaveWifiSupport:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmSlaveWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ret0:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ret1:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    nop

    .line 267
    .end local v0    # "ret0":I
    .end local v3    # "ret1":I
    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
