class com.xiaomi.NetworkBoost.MscsService.MscsService$1 extends android.database.ContentObserver {
	 /* .source "MscsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->registerMscsChangeObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.MscsService.MscsService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.MscsService.MscsService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/MscsService/MscsService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 147 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .line 150 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "cloud_mscs_enable"; // const-string v2, "cloud_mscs_enable"
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
v1 = android.provider.Settings$System .getIntForUser ( v1,v2,v3,v4 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
/* move v1, v2 */
} // :cond_0
/* move v1, v3 */
} // :goto_0
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fputmMSCSEnable ( v0,v1 );
/* .line 152 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v5 = "cloud_wifi7_mscs_enable"; // const-string v5, "cloud_wifi7_mscs_enable"
v1 = android.provider.Settings$System .getIntForUser ( v1,v5,v3,v4 );
/* if-ne v1, v2, :cond_1 */
/* move v3, v2 */
} // :cond_1
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fputmWifi7MSCSEnable ( v0,v3 );
/* .line 154 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "MSCS cloud change, mMSCSEnable: "; // const-string v1, "MSCS cloud change, mMSCSEnable: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmMSCSEnable ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MscsService"; // const-string v1, "MscsService"
android.util.Log .d ( v1,v0 );
/* .line 155 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "WIFI7 MSCS cloud change, mWifi7MSCSEnable: "; // const-string v2, "WIFI7 MSCS cloud change, mWifi7MSCSEnable: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmWifi7MSCSEnable ( v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 156 */
return;
} // .end method
