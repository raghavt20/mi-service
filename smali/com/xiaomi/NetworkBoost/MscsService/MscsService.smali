.class public Lcom/xiaomi/NetworkBoost/MscsService/MscsService;
.super Ljava/lang/Object;
.source "MscsService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;
    }
.end annotation


# static fields
.field private static final CLOUD_MSCS_ENABLE:Ljava/lang/String; = "cloud_mscs_enable"

.field private static final CLOUD_WIFI7_MSCS_ENABLE:Ljava/lang/String; = "cloud_wifi7_mscs_enable"

.field private static final MSCS_FRAME_CLASSIFIER:Ljava/lang/String; = "0401040000000000000000000000000000"

.field private static final MSCS_STREAM_TIMEOUT:I = 0xea60

.field private static final MSCS_UP_BITMAP:Ljava/lang/String; = "f0"

.field private static final MSCS_UP_LIMIT:Ljava/lang/String; = "7"

.field private static final MSG_REMOVE_REQ:I = 0x2

.field private static final MSG_SEND_REQ:I = 0x1

.field private static final REMOVE_DELAY:I = 0xea60

.field private static RESULT_CODE_NO_EFFECT:I = 0x0

.field private static RESULT_CODE_SUCCESS:I = 0x0

.field private static final SEND_DELAY:I = 0xc350

.field private static final TAG:Ljava/lang/String; = "MscsService"

.field private static final WIFI_INTERFACE_0:Ljava/lang/String; = "wlan0"

.field private static final WIFI_INTERFACE_1:Ljava/lang/String; = "wlan1"

.field private static volatile sInstance:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;


# instance fields
.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mIsScreenOn:Z

.field private mMSCSEnable:Z

.field private mMasterWifiSupport:Z

.field private mMasterWifiUsed:Z

.field private mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

.field private mNetwork:Landroid/net/Network;

.field private mNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

.field private mScreenStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

.field private mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

.field private mSlaveWifiSupport:Z

.field private mSlaveWifiUsed:Z

.field private mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

.field private mWifi7MSCSEnable:Z

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsScreenOn(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mIsScreenOn:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMSCSEnable(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMSCSEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMasterWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMasterWifiSupport:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMasterWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMasterWifiUsed:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSlaveWifiManager(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/net/wifi/SlaveWifiManager;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSlaveWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiSupport:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSlaveWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiUsed:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmWifi7MSCSEnable(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mWifi7MSCSEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmWifiManager(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/net/wifi/WifiManager;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIsScreenOn(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mIsScreenOn:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMSCSEnable(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMSCSEnable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMasterWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMasterWifiSupport:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMasterWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMasterWifiUsed:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSlaveWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiSupport:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSlaveWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiUsed:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmWifi7MSCSEnable(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mWifi7MSCSEnable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$menableMSCS(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Ljava/lang/String;)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->enableMSCS(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misWifi7MLO(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Landroid/net/wifi/WifiInfo;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->isWifi7MLO(Landroid/net/wifi/WifiInfo;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$sfgetRESULT_CODE_NO_EFFECT()I
    .locals 1

    sget v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->RESULT_CODE_NO_EFFECT:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetRESULT_CODE_SUCCESS()I
    .locals 1

    sget v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->RESULT_CODE_SUCCESS:I

    return v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->sInstance:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    .line 43
    const/4 v0, -0x1

    sput v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->RESULT_CODE_NO_EFFECT:I

    .line 44
    const/4 v0, 0x0

    sput v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->RESULT_CODE_SUCCESS:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 35
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    .line 36
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    .line 37
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 38
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mNetwork:Landroid/net/Network;

    .line 53
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMasterWifiSupport:Z

    .line 54
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiSupport:Z

    .line 56
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMasterWifiUsed:Z

    .line 57
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiUsed:Z

    .line 59
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mIsScreenOn:Z

    .line 61
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMSCSEnable:Z

    .line 62
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mWifi7MSCSEnable:Z

    .line 64
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 65
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    .line 270
    new-instance v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;-><init>(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    .line 335
    new-instance v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;-><init>(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mScreenStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    .line 81
    return-void
.end method

.method public static destroyInstance()V
    .locals 4

    .line 101
    sget-object v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->sInstance:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    if-eqz v0, :cond_1

    .line 102
    const-class v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    monitor-enter v0

    .line 103
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->sInstance:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 105
    :try_start_1
    sget-object v1, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->sInstance:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->onDestroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109
    goto :goto_0

    .line 107
    :catch_0
    move-exception v1

    .line 108
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "MscsService"

    const-string v3, "destroyInstance onDestroy catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 110
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->sInstance:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    .line 112
    :cond_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 114
    :cond_1
    :goto_1
    return-void
.end method

.method private disableMSCS()I
    .locals 2

    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "disableMSCS mMSCSEnable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMSCSEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MscsService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    sget v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->RESULT_CODE_NO_EFFECT:I

    .line 206
    .local v0, "ret":I
    nop

    .line 207
    return v0
.end method

.method private enableMSCS(Ljava/lang/String;)I
    .locals 12
    .param p1, "interfaceName"    # Ljava/lang/String;

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enableMSCS mMSCSEnable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMSCSEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MscsService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    sget v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->RESULT_CODE_NO_EFFECT:I

    .line 183
    .local v0, "ret":I
    iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMSCSEnable:Z

    if-nez v2, :cond_0

    .line 184
    return v0

    .line 186
    :cond_0
    const-string v2, "f0"

    .line 187
    .local v2, "up_bitmap":Ljava/lang/String;
    const-string v9, "7"

    .line 188
    .local v9, "up_limit":Ljava/lang/String;
    const v3, 0xea60

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    .line 189
    .local v10, "stream_timeout":Ljava/lang/String;
    const-string v11, "0401040000000000000000000000000000"

    .line 190
    .local v11, "frame_classifier":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableMSCS interfaceName:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " up_bitmap:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " up_limit:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " stream_timeout:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " frame_classifier:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v3, :cond_2

    .line 193
    move-object v4, p1

    move-object v5, v2

    move-object v6, v9

    move-object v7, v10

    move-object v8, v11

    invoke-virtual/range {v3 .. v8}, Landroid/net/wifi/MiuiWifiManager;->changeMSCS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 194
    sget v1, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->RESULT_CODE_SUCCESS:I

    if-ne v0, v1, :cond_1

    .line 195
    return v0

    .line 196
    :cond_1
    sget v1, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->RESULT_CODE_NO_EFFECT:I

    if-ne v0, v1, :cond_2

    .line 197
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    move-object v4, p1

    move-object v5, v2

    move-object v6, v9

    move-object v7, v10

    move-object v8, v11

    invoke-virtual/range {v3 .. v8}, Landroid/net/wifi/MiuiWifiManager;->addMSCS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 200
    :cond_2
    return v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/MscsService/MscsService;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 84
    sget-object v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->sInstance:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    if-nez v0, :cond_1

    .line 85
    const-class v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    monitor-enter v0

    .line 86
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->sInstance:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    if-nez v1, :cond_0

    .line 87
    new-instance v1, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->sInstance:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :try_start_1
    sget-object v1, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->sInstance:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->onCreate()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    goto :goto_0

    .line 91
    :catch_0
    move-exception v1

    .line 92
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "MscsService"

    const-string v3, "getInstance onCreate catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 95
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 97
    :cond_1
    :goto_1
    sget-object v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->sInstance:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    return-object v0
.end method

.method private isWifi7MLO(Landroid/net/wifi/WifiInfo;)Z
    .locals 2
    .param p1, "wifiInfo"    # Landroid/net/wifi/WifiInfo;

    .line 304
    if-eqz p1, :cond_0

    .line 305
    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getApMldMacAddress()Landroid/net/MacAddress;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 306
    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getApMldMacAddress()Landroid/net/MacAddress;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/MacAddress;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "00:00:00:00:00:00"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0

    .line 309
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private registerMscsChangeObserver()V
    .locals 5

    .line 146
    const-string v0, "MscsService"

    const-string v1, "MSCS cloud observer register"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    new-instance v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$1;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$1;-><init>(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Landroid/os/Handler;)V

    .line 159
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 161
    const-string v2, "cloud_mscs_enable"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 160
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 165
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 167
    const-string v2, "cloud_wifi7_mscs_enable"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 166
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 172
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$2;

    invoke-direct {v2, p0, v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$2;-><init>(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Landroid/database/ContentObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 178
    return-void
.end method

.method private registerNetworkCallback()V
    .locals 3

    .line 217
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 218
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    goto :goto_0

    .line 219
    :catch_0
    move-exception v0

    .line 220
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MscsService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private registerScreenStatusListener()V
    .locals 3

    .line 317
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 318
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mScreenStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    goto :goto_0

    .line 319
    :catch_0
    move-exception v0

    .line 320
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MscsService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private unregisterNetworkCallback()V
    .locals 3

    .line 226
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 228
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mNetworkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    :cond_0
    goto :goto_0

    .line 230
    :catch_0
    move-exception v0

    .line 231
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MscsService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private unregisterScreenStatusListener()V
    .locals 3

    .line 326
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mScreenStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 328
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mScreenStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    :cond_0
    goto :goto_0

    .line 330
    :catch_0
    move-exception v0

    .line 331
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MscsService"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 5

    .line 117
    const-string v0, "onCreate "

    const-string v1, "MscsService"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    const-string v2, "MiuiWifiService"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/MiuiWifiManager;

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    .line 120
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 121
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "wifi"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 122
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    const-string v2, "SlaveWifiService"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/SlaveWifiManager;

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSystemService Exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    new-instance v0, Landroid/os/HandlerThread;

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 128
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 129
    new-instance v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;-><init>(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mHandler:Landroid/os/Handler;

    .line 130
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->registerMscsChangeObserver()V

    .line 131
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_mscs_enable"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_1

    :cond_0
    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMSCSEnable:Z

    .line 133
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v4, "cloud_wifi7_mscs_enable"

    invoke-static {v0, v4, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v1, :cond_1

    move v2, v1

    :cond_1
    iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mWifi7MSCSEnable:Z

    .line 135
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->registerNetworkCallback()V

    .line 136
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->registerScreenStatusListener()V

    .line 137
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 140
    const-string v0, "MscsService"

    const-string v1, "onDestroy "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->unregisterNetworkCallback()V

    .line 142
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->unregisterScreenStatusListener()V

    .line 143
    return-void
.end method
