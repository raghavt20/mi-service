class com.xiaomi.NetworkBoost.MscsService.MscsService$InternalHandler extends android.os.Handler {
	 /* .source "MscsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/MscsService/MscsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "InternalHandler" */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.MscsService.MscsService this$0; //synthetic
/* # direct methods */
public com.xiaomi.NetworkBoost.MscsService.MscsService$InternalHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 236 */
this.this$0 = p1;
/* .line 237 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 238 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 7 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 241 */
/* iget v0, p1, Landroid/os/Message;->what:I */
final String v1 = "MscsService"; // const-string v1, "MscsService"
int v2 = 1; // const/4 v2, 0x1
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_2 */
/* .line 261 */
/* :pswitch_0 */
final String v0 = "removeMessages"; // const-string v0, "removeMessages"
android.util.Log .d ( v1,v0 );
/* .line 262 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmHandler ( v0 );
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 263 */
/* goto/16 :goto_2 */
/* .line 243 */
/* :pswitch_1 */
v0 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$sfgetRESULT_CODE_NO_EFFECT ( );
/* .line 244 */
/* .local v0, "ret0":I */
v3 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$sfgetRESULT_CODE_NO_EFFECT ( );
/* .line 245 */
/* .local v3, "ret1":I */
v4 = this.this$0;
v4 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmMasterWifiUsed ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_0
	 v4 = this.this$0;
	 v4 = 	 com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmMasterWifiSupport ( v4 );
	 if ( v4 != null) { // if-eqz v4, :cond_0
		 /* .line 246 */
		 v4 = this.this$0;
		 /* const-string/jumbo v5, "wlan0" */
		 v0 = 		 com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$menableMSCS ( v4,v5 );
		 /* .line 248 */
	 } // :cond_0
	 v4 = this.this$0;
	 v4 = 	 com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmSlaveWifiUsed ( v4 );
	 if ( v4 != null) { // if-eqz v4, :cond_1
		 v4 = this.this$0;
		 v4 = 		 com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmSlaveWifiSupport ( v4 );
		 if ( v4 != null) { // if-eqz v4, :cond_1
			 /* .line 249 */
			 v4 = this.this$0;
			 /* const-string/jumbo v5, "wlan1" */
			 v3 = 			 com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$menableMSCS ( v4,v5 );
			 /* .line 251 */
		 } // :cond_1
		 v4 = 		 com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$sfgetRESULT_CODE_SUCCESS ( );
		 /* if-eq v0, v4, :cond_2 */
		 v4 = 		 com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$sfgetRESULT_CODE_SUCCESS ( );
		 /* if-ne v3, v4, :cond_3 */
		 /* .line 252 */
	 } // :cond_2
	 v4 = this.this$0;
	 com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmHandler ( v4 );
	 /* const-wide/32 v5, 0xc350 */
	 (( android.os.Handler ) v4 ).sendEmptyMessageDelayed ( v2, v5, v6 ); // invoke-virtual {v4, v2, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
	 /* .line 254 */
} // :cond_3
v4 = this.this$0;
v5 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$sfgetRESULT_CODE_SUCCESS ( );
int v6 = 0; // const/4 v6, 0x0
/* if-ne v0, v5, :cond_4 */
/* move v5, v2 */
} // :cond_4
/* move v5, v6 */
} // :goto_0
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fputmMasterWifiSupport ( v4,v5 );
/* .line 255 */
v4 = this.this$0;
v5 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$sfgetRESULT_CODE_SUCCESS ( );
/* if-ne v3, v5, :cond_5 */
} // :cond_5
/* move v2, v6 */
} // :goto_1
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fputmSlaveWifiSupport ( v4,v2 );
/* .line 256 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "mMasterWifiUsed:"; // const-string v4, "mMasterWifiUsed:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
v4 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmMasterWifiUsed ( v4 );
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = " mMasterWifiSupport:"; // const-string v4, " mMasterWifiSupport:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
v4 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmMasterWifiSupport ( v4 );
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = " mSlaveWifiUsed:"; // const-string v4, " mSlaveWifiUsed:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
v4 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmSlaveWifiUsed ( v4 );
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = " mSlaveWifiSupport:"; // const-string v4, " mSlaveWifiSupport:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
v4 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmSlaveWifiSupport ( v4 );
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 258 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "ret0:"; // const-string v4, "ret0:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " ret1:"; // const-string v4, " ret1:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 259 */
/* nop */
/* .line 267 */
} // .end local v0 # "ret0":I
} // .end local v3 # "ret1":I
} // :goto_2
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
