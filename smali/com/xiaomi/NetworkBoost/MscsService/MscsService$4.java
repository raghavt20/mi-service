class com.xiaomi.NetworkBoost.MscsService.MscsService$4 implements com.xiaomi.NetworkBoost.StatusManager$IScreenStatusListener {
	 /* .source "MscsService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/MscsService/MscsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.MscsService.MscsService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.MscsService.MscsService$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/MscsService/MscsService; */
/* .line 335 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onScreenOFF ( ) {
/* .locals 4 */
/* .line 349 */
final String v0 = "MscsService"; // const-string v0, "MscsService"
final String v1 = "onScreenOFF"; // const-string v1, "onScreenOFF"
android.util.Log .d ( v0,v1 );
/* .line 350 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fputmIsScreenOn ( v0,v1 );
/* .line 351 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmHandler ( v0 );
int v1 = 2; // const/4 v1, 0x2
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 352 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmHandler ( v0 );
/* const-wide/32 v2, 0xea60 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 353 */
return;
} // .end method
public void onScreenON ( ) {
/* .locals 3 */
/* .line 338 */
final String v0 = "MscsService"; // const-string v0, "MscsService"
final String v1 = "onScreenON"; // const-string v1, "onScreenON"
android.util.Log .d ( v0,v1 );
/* .line 339 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fputmIsScreenOn ( v0,v1 );
/* .line 340 */
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmMasterWifiUsed ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 v0 = this.this$0;
	 v0 = 	 com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmMasterWifiSupport ( v0 );
	 /* if-nez v0, :cond_1 */
} // :cond_0
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmSlaveWifiUsed ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
	 v0 = this.this$0;
	 v0 = 	 com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmSlaveWifiSupport ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 341 */
	 } // :cond_1
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmHandler ( v0 );
	 int v2 = 2; // const/4 v2, 0x2
	 (( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
	 /* .line 342 */
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmHandler ( v0 );
	 (( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
	 /* .line 343 */
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmHandler ( v0 );
	 (( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
	 /* .line 345 */
} // :cond_2
return;
} // .end method
