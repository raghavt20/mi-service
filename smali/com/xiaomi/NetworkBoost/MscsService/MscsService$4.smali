.class Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;
.super Ljava/lang/Object;
.source "MscsService.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/MscsService/MscsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    .line 335
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScreenOFF()V
    .locals 4

    .line 349
    const-string v0, "MscsService"

    const-string v1, "onScreenOFF"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fputmIsScreenOn(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V

    .line 351
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 352
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/os/Handler;

    move-result-object v0

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 353
    return-void
.end method

.method public onScreenON()V
    .locals 3

    .line 338
    const-string v0, "MscsService"

    const-string v1, "onScreenON"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fputmIsScreenOn(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V

    .line 340
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmMasterWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmMasterWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmSlaveWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmSlaveWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 341
    :cond_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 342
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 343
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 345
    :cond_2
    return-void
.end method
