class com.xiaomi.NetworkBoost.MscsService.MscsService$3 implements com.xiaomi.NetworkBoost.StatusManager$INetworkInterfaceListener {
	 /* .source "MscsService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/MscsService/MscsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.MscsService.MscsService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.MscsService.MscsService$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/MscsService/MscsService; */
/* .line 270 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onNetwrokInterfaceChange ( java.lang.String p0, Integer p1, Boolean p2, Boolean p3, Boolean p4, Boolean p5, java.lang.String p6 ) {
/* .locals 6 */
/* .param p1, "ifacename" # Ljava/lang/String; */
/* .param p2, "ifacenum" # I */
/* .param p3, "wifiready" # Z */
/* .param p4, "slavewifiready" # Z */
/* .param p5, "dataready" # Z */
/* .param p6, "slaveDataReady" # Z */
/* .param p7, "status" # Ljava/lang/String; */
/* .line 274 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_7
	 /* .line 275 */
	 /* const-string/jumbo v1, "wlan" */
	 v1 = 	 (( java.lang.String ) p1 ).indexOf ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
	 int v2 = -1; // const/4 v2, -0x1
	 /* if-eq v1, v2, :cond_6 */
	 /* .line 276 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "onNetwrokInterfaceChange ifacename:"; // const-string v3, "onNetwrokInterfaceChange ifacename:"
	 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v3 = "MscsService"; // const-string v3, "MscsService"
	 android.util.Log .d ( v3,v1 );
	 /* .line 277 */
	 v1 = this.this$0;
	 /* const-string/jumbo v4, "wlan0" */
	 v4 = 	 (( java.lang.String ) p1 ).indexOf ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
	 int v5 = 0; // const/4 v5, 0x0
	 /* if-eq v4, v2, :cond_0 */
	 /* move v4, v0 */
} // :cond_0
/* move v4, v5 */
} // :goto_0
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fputmMasterWifiUsed ( v1,v4 );
/* .line 278 */
v1 = this.this$0;
v4 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmMasterWifiUsed ( v1 );
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fputmMasterWifiSupport ( v1,v4 );
/* .line 279 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmWifiManager ( v1 );
(( android.net.wifi.WifiManager ) v4 ).getConnectionInfo ( ); // invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;
v1 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$misWifi7MLO ( v1,v4 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 280 */
v1 = this.this$0;
v4 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmMasterWifiSupport ( v1 );
if ( v4 != null) { // if-eqz v4, :cond_1
	 v4 = this.this$0;
	 v4 = 	 com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmWifi7MSCSEnable ( v4 );
	 if ( v4 != null) { // if-eqz v4, :cond_1
		 /* move v4, v0 */
	 } // :cond_1
	 /* move v4, v5 */
} // :goto_1
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fputmMasterWifiSupport ( v1,v4 );
/* .line 282 */
} // :cond_2
v1 = this.this$0;
/* const-string/jumbo v4, "wlan1" */
v4 = (( java.lang.String ) p1 ).indexOf ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* if-eq v4, v2, :cond_3 */
/* move v2, v0 */
} // :cond_3
/* move v2, v5 */
} // :goto_2
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fputmSlaveWifiUsed ( v1,v2 );
/* .line 283 */
v1 = this.this$0;
v2 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmSlaveWifiUsed ( v1 );
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fputmSlaveWifiSupport ( v1,v2 );
/* .line 284 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmSlaveWifiManager ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_5
v1 = this.this$0;
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmSlaveWifiManager ( v1 );
(( android.net.wifi.SlaveWifiManager ) v2 ).getWifiSlaveConnectionInfo ( ); // invoke-virtual {v2}, Landroid/net/wifi/SlaveWifiManager;->getWifiSlaveConnectionInfo()Landroid/net/wifi/WifiInfo;
v1 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$misWifi7MLO ( v1,v2 );
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 285 */
v1 = this.this$0;
v2 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmSlaveWifiSupport ( v1 );
if ( v2 != null) { // if-eqz v2, :cond_4
v2 = this.this$0;
v2 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmWifi7MSCSEnable ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_4
	 /* move v5, v0 */
} // :cond_4
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fputmSlaveWifiSupport ( v1,v5 );
/* .line 287 */
} // :cond_5
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onNetwrokInterfaceChange mMasterWifiUsed:"; // const-string v2, "onNetwrokInterfaceChange mMasterWifiUsed:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmMasterWifiUsed ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " mSlaveWifiUsed:"; // const-string v2, " mSlaveWifiUsed:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmSlaveWifiUsed ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " mMasterWifiSupport:"; // const-string v2, " mMasterWifiSupport:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmMasterWifiSupport ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " mSlaveWifiSupport"; // const-string v2, " mSlaveWifiSupport"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmSlaveWifiSupport ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " mIsScreenOn:"; // const-string v2, " mIsScreenOn:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmIsScreenOn ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v1 );
/* .line 290 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmHandler ( v1 );
(( android.os.Handler ) v1 ).removeMessages ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V
/* .line 291 */
v1 = this.this$0;
v1 = com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmIsScreenOn ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 292 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmHandler ( v1 );
(( android.os.Handler ) v1 ).sendEmptyMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 295 */
} // :cond_6
v1 = this.this$0;
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmHandler ( v1 );
(( android.os.Handler ) v1 ).removeMessages ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V
/* .line 298 */
} // :cond_7
v1 = this.this$0;
com.xiaomi.NetworkBoost.MscsService.MscsService .-$$Nest$fgetmHandler ( v1 );
(( android.os.Handler ) v1 ).removeMessages ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V
/* .line 300 */
} // :cond_8
} // :goto_3
return;
} // .end method
