.class Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;
.super Ljava/lang/Object;
.source "MscsService.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/MscsService/MscsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    .line 270
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNetwrokInterfaceChange(Ljava/lang/String;IZZZZLjava/lang/String;)V
    .locals 6
    .param p1, "ifacename"    # Ljava/lang/String;
    .param p2, "ifacenum"    # I
    .param p3, "wifiready"    # Z
    .param p4, "slavewifiready"    # Z
    .param p5, "dataready"    # Z
    .param p6, "slaveDataReady"    # Z
    .param p7, "status"    # Ljava/lang/String;

    .line 274
    const/4 v0, 0x1

    if-eqz p1, :cond_7

    .line 275
    const-string/jumbo v1, "wlan"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_6

    .line 276
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNetwrokInterfaceChange ifacename:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "MscsService"

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    const-string/jumbo v4, "wlan0"

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x0

    if-eq v4, v2, :cond_0

    move v4, v0

    goto :goto_0

    :cond_0
    move v4, v5

    :goto_0
    invoke-static {v1, v4}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fputmMasterWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V

    .line 278
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmMasterWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v4

    invoke-static {v1, v4}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fputmMasterWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V

    .line 279
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmWifiManager(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/net/wifi/WifiManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$misWifi7MLO(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Landroid/net/wifi/WifiInfo;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 280
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmMasterWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmWifi7MSCSEnable(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v0

    goto :goto_1

    :cond_1
    move v4, v5

    :goto_1
    invoke-static {v1, v4}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fputmMasterWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V

    .line 282
    :cond_2
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    const-string/jumbo v4, "wlan1"

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v2, :cond_3

    move v2, v0

    goto :goto_2

    :cond_3
    move v2, v5

    :goto_2
    invoke-static {v1, v2}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fputmSlaveWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V

    .line 283
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmSlaveWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fputmSlaveWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V

    .line 284
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmSlaveWifiManager(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/net/wifi/SlaveWifiManager;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmSlaveWifiManager(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/net/wifi/SlaveWifiManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/wifi/SlaveWifiManager;->getWifiSlaveConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$misWifi7MLO(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Landroid/net/wifi/WifiInfo;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 285
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmSlaveWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmWifi7MSCSEnable(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v5, v0

    :cond_4
    invoke-static {v1, v5}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fputmSlaveWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Z)V

    .line 287
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNetwrokInterfaceChange mMasterWifiUsed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmMasterWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mSlaveWifiUsed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmSlaveWifiUsed(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mMasterWifiSupport:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmMasterWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mSlaveWifiSupport"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmSlaveWifiSupport(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mIsScreenOn:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmIsScreenOn(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 291
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmIsScreenOn(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 292
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_3

    .line 295
    :cond_6
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_3

    .line 298
    :cond_7
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;->this$0:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 300
    :cond_8
    :goto_3
    return-void
.end method
