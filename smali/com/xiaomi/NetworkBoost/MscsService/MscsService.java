public class com.xiaomi.NetworkBoost.MscsService.MscsService {
	 /* .source "MscsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CLOUD_MSCS_ENABLE;
private static final java.lang.String CLOUD_WIFI7_MSCS_ENABLE;
private static final java.lang.String MSCS_FRAME_CLASSIFIER;
private static final Integer MSCS_STREAM_TIMEOUT;
private static final java.lang.String MSCS_UP_BITMAP;
private static final java.lang.String MSCS_UP_LIMIT;
private static final Integer MSG_REMOVE_REQ;
private static final Integer MSG_SEND_REQ;
private static final Integer REMOVE_DELAY;
private static Integer RESULT_CODE_NO_EFFECT;
private static Integer RESULT_CODE_SUCCESS;
private static final Integer SEND_DELAY;
private static final java.lang.String TAG;
private static final java.lang.String WIFI_INTERFACE_0;
private static final java.lang.String WIFI_INTERFACE_1;
private static volatile com.xiaomi.NetworkBoost.MscsService.MscsService sInstance;
/* # instance fields */
private android.net.ConnectivityManager mConnectivityManager;
private android.content.Context mContext;
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
private Boolean mIsScreenOn;
private Boolean mMSCSEnable;
private Boolean mMasterWifiSupport;
private Boolean mMasterWifiUsed;
private android.net.wifi.MiuiWifiManager mMiuiWifiManager;
private android.net.Network mNetwork;
private com.xiaomi.NetworkBoost.StatusManager$INetworkInterfaceListener mNetworkInterfaceListener;
private com.xiaomi.NetworkBoost.StatusManager$IScreenStatusListener mScreenStatusListener;
private android.net.wifi.SlaveWifiManager mSlaveWifiManager;
private Boolean mSlaveWifiSupport;
private Boolean mSlaveWifiUsed;
private com.xiaomi.NetworkBoost.StatusManager mStatusManager;
private Boolean mWifi7MSCSEnable;
private android.net.wifi.WifiManager mWifiManager;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.xiaomi.NetworkBoost.MscsService.MscsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.xiaomi.NetworkBoost.MscsService.MscsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmIsScreenOn ( com.xiaomi.NetworkBoost.MscsService.MscsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mIsScreenOn:Z */
} // .end method
static Boolean -$$Nest$fgetmMSCSEnable ( com.xiaomi.NetworkBoost.MscsService.MscsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMSCSEnable:Z */
} // .end method
static Boolean -$$Nest$fgetmMasterWifiSupport ( com.xiaomi.NetworkBoost.MscsService.MscsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMasterWifiSupport:Z */
} // .end method
static Boolean -$$Nest$fgetmMasterWifiUsed ( com.xiaomi.NetworkBoost.MscsService.MscsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMasterWifiUsed:Z */
} // .end method
static android.net.wifi.SlaveWifiManager -$$Nest$fgetmSlaveWifiManager ( com.xiaomi.NetworkBoost.MscsService.MscsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mSlaveWifiManager;
} // .end method
static Boolean -$$Nest$fgetmSlaveWifiSupport ( com.xiaomi.NetworkBoost.MscsService.MscsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiSupport:Z */
} // .end method
static Boolean -$$Nest$fgetmSlaveWifiUsed ( com.xiaomi.NetworkBoost.MscsService.MscsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiUsed:Z */
} // .end method
static Boolean -$$Nest$fgetmWifi7MSCSEnable ( com.xiaomi.NetworkBoost.MscsService.MscsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mWifi7MSCSEnable:Z */
} // .end method
static android.net.wifi.WifiManager -$$Nest$fgetmWifiManager ( com.xiaomi.NetworkBoost.MscsService.MscsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mWifiManager;
} // .end method
static void -$$Nest$fputmIsScreenOn ( com.xiaomi.NetworkBoost.MscsService.MscsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mIsScreenOn:Z */
	 return;
} // .end method
static void -$$Nest$fputmMSCSEnable ( com.xiaomi.NetworkBoost.MscsService.MscsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMSCSEnable:Z */
	 return;
} // .end method
static void -$$Nest$fputmMasterWifiSupport ( com.xiaomi.NetworkBoost.MscsService.MscsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMasterWifiSupport:Z */
	 return;
} // .end method
static void -$$Nest$fputmMasterWifiUsed ( com.xiaomi.NetworkBoost.MscsService.MscsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMasterWifiUsed:Z */
	 return;
} // .end method
static void -$$Nest$fputmSlaveWifiSupport ( com.xiaomi.NetworkBoost.MscsService.MscsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiSupport:Z */
	 return;
} // .end method
static void -$$Nest$fputmSlaveWifiUsed ( com.xiaomi.NetworkBoost.MscsService.MscsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiUsed:Z */
	 return;
} // .end method
static void -$$Nest$fputmWifi7MSCSEnable ( com.xiaomi.NetworkBoost.MscsService.MscsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mWifi7MSCSEnable:Z */
	 return;
} // .end method
static Integer -$$Nest$menableMSCS ( com.xiaomi.NetworkBoost.MscsService.MscsService p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->enableMSCS(Ljava/lang/String;)I */
} // .end method
static Boolean -$$Nest$misWifi7MLO ( com.xiaomi.NetworkBoost.MscsService.MscsService p0, android.net.wifi.WifiInfo p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->isWifi7MLO(Landroid/net/wifi/WifiInfo;)Z */
} // .end method
static Integer -$$Nest$sfgetRESULT_CODE_NO_EFFECT ( ) { //bridge//synthethic
	 /* .locals 1 */
} // .end method
static Integer -$$Nest$sfgetRESULT_CODE_SUCCESS ( ) { //bridge//synthethic
	 /* .locals 1 */
} // .end method
static com.xiaomi.NetworkBoost.MscsService.MscsService ( ) {
	 /* .locals 1 */
	 /* .line 31 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 43 */
	 int v0 = -1; // const/4 v0, -0x1
	 /* .line 44 */
	 int v0 = 0; // const/4 v0, 0x0
	 return;
} // .end method
private com.xiaomi.NetworkBoost.MscsService.MscsService ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 79 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 33 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mStatusManager = v0;
	 /* .line 35 */
	 this.mContext = v0;
	 /* .line 36 */
	 this.mMiuiWifiManager = v0;
	 /* .line 37 */
	 this.mConnectivityManager = v0;
	 /* .line 38 */
	 this.mNetwork = v0;
	 /* .line 53 */
	 int v1 = 1; // const/4 v1, 0x1
	 /* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMasterWifiSupport:Z */
	 /* .line 54 */
	 /* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiSupport:Z */
	 /* .line 56 */
	 int v1 = 0; // const/4 v1, 0x0
	 /* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMasterWifiUsed:Z */
	 /* .line 57 */
	 /* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mSlaveWifiUsed:Z */
	 /* .line 59 */
	 /* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mIsScreenOn:Z */
	 /* .line 61 */
	 /* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMSCSEnable:Z */
	 /* .line 62 */
	 /* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mWifi7MSCSEnable:Z */
	 /* .line 64 */
	 this.mWifiManager = v0;
	 /* .line 65 */
	 this.mSlaveWifiManager = v0;
	 /* .line 270 */
	 /* new-instance v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3; */
	 /* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$3;-><init>(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)V */
	 this.mNetworkInterfaceListener = v0;
	 /* .line 335 */
	 /* new-instance v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4; */
	 /* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$4;-><init>(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;)V */
	 this.mScreenStatusListener = v0;
	 /* .line 80 */
	 (( android.content.Context ) p1 ).getApplicationContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
	 this.mContext = v0;
	 /* .line 81 */
	 return;
} // .end method
public static void destroyInstance ( ) {
	 /* .locals 4 */
	 /* .line 101 */
	 v0 = com.xiaomi.NetworkBoost.MscsService.MscsService.sInstance;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 102 */
		 /* const-class v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService; */
		 /* monitor-enter v0 */
		 /* .line 103 */
		 try { // :try_start_0
			 v1 = com.xiaomi.NetworkBoost.MscsService.MscsService.sInstance;
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 /* .line 105 */
				 try { // :try_start_1
					 v1 = com.xiaomi.NetworkBoost.MscsService.MscsService.sInstance;
					 (( com.xiaomi.NetworkBoost.MscsService.MscsService ) v1 ).onDestroy ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->onDestroy()V
					 /* :try_end_1 */
					 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
					 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
					 /* .line 109 */
					 /* .line 107 */
					 /* :catch_0 */
					 /* move-exception v1 */
					 /* .line 108 */
					 /* .local v1, "e":Ljava/lang/Exception; */
					 try { // :try_start_2
						 final String v2 = "MscsService"; // const-string v2, "MscsService"
						 final String v3 = "destroyInstance onDestroy catch:"; // const-string v3, "destroyInstance onDestroy catch:"
						 android.util.Log .e ( v2,v3,v1 );
						 /* .line 110 */
					 } // .end local v1 # "e":Ljava/lang/Exception;
				 } // :goto_0
				 int v1 = 0; // const/4 v1, 0x0
				 /* .line 112 */
			 } // :cond_0
			 /* monitor-exit v0 */
			 /* :catchall_0 */
			 /* move-exception v1 */
			 /* monitor-exit v0 */
			 /* :try_end_2 */
			 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
			 /* throw v1 */
			 /* .line 114 */
		 } // :cond_1
	 } // :goto_1
	 return;
} // .end method
private Integer disableMSCS ( ) {
	 /* .locals 2 */
	 /* .line 204 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "disableMSCS mMSCSEnable: "; // const-string v1, "disableMSCS mMSCSEnable: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMSCSEnable:Z */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "MscsService"; // const-string v1, "MscsService"
	 android.util.Log .d ( v1,v0 );
	 /* .line 205 */
	 /* .line 206 */
	 /* .local v0, "ret":I */
	 /* nop */
	 /* .line 207 */
} // .end method
private Integer enableMSCS ( java.lang.String p0 ) {
	 /* .locals 12 */
	 /* .param p1, "interfaceName" # Ljava/lang/String; */
	 /* .line 181 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "enableMSCS mMSCSEnable: "; // const-string v1, "enableMSCS mMSCSEnable: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMSCSEnable:Z */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "MscsService"; // const-string v1, "MscsService"
	 android.util.Log .d ( v1,v0 );
	 /* .line 182 */
	 /* .line 183 */
	 /* .local v0, "ret":I */
	 /* iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMSCSEnable:Z */
	 /* if-nez v2, :cond_0 */
	 /* .line 184 */
	 /* .line 186 */
} // :cond_0
final String v2 = "f0"; // const-string v2, "f0"
/* .line 187 */
/* .local v2, "up_bitmap":Ljava/lang/String; */
final String v9 = "7"; // const-string v9, "7"
/* .line 188 */
/* .local v9, "up_limit":Ljava/lang/String; */
/* const v3, 0xea60 */
java.lang.String .valueOf ( v3 );
/* .line 189 */
/* .local v10, "stream_timeout":Ljava/lang/String; */
final String v11 = "0401040000000000000000000000000000"; // const-string v11, "0401040000000000000000000000000000"
/* .line 190 */
/* .local v11, "frame_classifier":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "enableMSCS interfaceName:"; // const-string v4, "enableMSCS interfaceName:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " up_bitmap:"; // const-string v4, " up_bitmap:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " up_limit:"; // const-string v4, " up_limit:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " stream_timeout:"; // const-string v4, " stream_timeout:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " frame_classifier:"; // const-string v4, " frame_classifier:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v11 ); // invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v3 );
/* .line 192 */
v3 = this.mMiuiWifiManager;
if ( v3 != null) { // if-eqz v3, :cond_2
	 /* .line 193 */
	 /* move-object v4, p1 */
	 /* move-object v5, v2 */
	 /* move-object v6, v9 */
	 /* move-object v7, v10 */
	 /* move-object v8, v11 */
	 v0 = 	 /* invoke-virtual/range {v3 ..v8}, Landroid/net/wifi/MiuiWifiManager;->changeMSCS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I */
	 /* .line 194 */
	 /* if-ne v0, v1, :cond_1 */
	 /* .line 195 */
	 /* .line 196 */
} // :cond_1
/* if-ne v0, v1, :cond_2 */
/* .line 197 */
v3 = this.mMiuiWifiManager;
/* move-object v4, p1 */
/* move-object v5, v2 */
/* move-object v6, v9 */
/* move-object v7, v10 */
/* move-object v8, v11 */
v0 = /* invoke-virtual/range {v3 ..v8}, Landroid/net/wifi/MiuiWifiManager;->addMSCS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I */
/* .line 200 */
} // :cond_2
} // .end method
public static com.xiaomi.NetworkBoost.MscsService.MscsService getInstance ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 84 */
v0 = com.xiaomi.NetworkBoost.MscsService.MscsService.sInstance;
/* if-nez v0, :cond_1 */
/* .line 85 */
/* const-class v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService; */
/* monitor-enter v0 */
/* .line 86 */
try { // :try_start_0
v1 = com.xiaomi.NetworkBoost.MscsService.MscsService.sInstance;
/* if-nez v1, :cond_0 */
/* .line 87 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/MscsService/MscsService; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;-><init>(Landroid/content/Context;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 89 */
try { // :try_start_1
	 v1 = com.xiaomi.NetworkBoost.MscsService.MscsService.sInstance;
	 (( com.xiaomi.NetworkBoost.MscsService.MscsService ) v1 ).onCreate ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->onCreate()V
	 /* :try_end_1 */
	 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* .line 93 */
	 /* .line 91 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 92 */
	 /* .local v1, "e":Ljava/lang/Exception; */
	 try { // :try_start_2
		 final String v2 = "MscsService"; // const-string v2, "MscsService"
		 final String v3 = "getInstance onCreate catch:"; // const-string v3, "getInstance onCreate catch:"
		 android.util.Log .e ( v2,v3,v1 );
		 /* .line 95 */
	 } // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 97 */
} // :cond_1
} // :goto_1
v0 = com.xiaomi.NetworkBoost.MscsService.MscsService.sInstance;
} // .end method
private Boolean isWifi7MLO ( android.net.wifi.WifiInfo p0 ) {
/* .locals 2 */
/* .param p1, "wifiInfo" # Landroid/net/wifi/WifiInfo; */
/* .line 304 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 305 */
(( android.net.wifi.WifiInfo ) p1 ).getApMldMacAddress ( ); // invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getApMldMacAddress()Landroid/net/MacAddress;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 306 */
(( android.net.wifi.WifiInfo ) p1 ).getApMldMacAddress ( ); // invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getApMldMacAddress()Landroid/net/MacAddress;
(( android.net.MacAddress ) v0 ).toString ( ); // invoke-virtual {v0}, Landroid/net/MacAddress;->toString()Ljava/lang/String;
final String v1 = "00:00:00:00:00:00"; // const-string v1, "00:00:00:00:00:00"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* xor-int/lit8 v0, v0, 0x1 */
/* .line 309 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void registerMscsChangeObserver ( ) {
/* .locals 5 */
/* .line 146 */
final String v0 = "MscsService"; // const-string v0, "MscsService"
final String v1 = "MSCS cloud observer register"; // const-string v1, "MSCS cloud observer register"
android.util.Log .d ( v0,v1 );
/* .line 147 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$1; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$1;-><init>(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Landroid/os/Handler;)V */
/* .line 159 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 161 */
final String v2 = "cloud_mscs_enable"; // const-string v2, "cloud_mscs_enable"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 160 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 165 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 167 */
final String v2 = "cloud_wifi7_mscs_enable"; // const-string v2, "cloud_wifi7_mscs_enable"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 166 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 172 */
v1 = this.mHandler;
/* new-instance v2, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$2; */
/* invoke-direct {v2, p0, v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$2;-><init>(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Landroid/database/ContentObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 178 */
return;
} // .end method
private void registerNetworkCallback ( ) {
/* .locals 3 */
/* .line 217 */
try { // :try_start_0
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 218 */
v1 = this.mNetworkInterfaceListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerNetworkInterfaceListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 221 */
/* .line 219 */
/* :catch_0 */
/* move-exception v0 */
/* .line 220 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MscsService"; // const-string v2, "MscsService"
android.util.Log .e ( v2,v1 );
/* .line 222 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void registerScreenStatusListener ( ) {
/* .locals 3 */
/* .line 317 */
try { // :try_start_0
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 318 */
v1 = this.mScreenStatusListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerScreenStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 321 */
/* .line 319 */
/* :catch_0 */
/* move-exception v0 */
/* .line 320 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MscsService"; // const-string v2, "MscsService"
android.util.Log .e ( v2,v1 );
/* .line 322 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unregisterNetworkCallback ( ) {
/* .locals 3 */
/* .line 226 */
try { // :try_start_0
v0 = this.mNetworkInterfaceListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 227 */
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 228 */
v1 = this.mNetworkInterfaceListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterNetworkInterfaceListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 232 */
} // :cond_0
/* .line 230 */
/* :catch_0 */
/* move-exception v0 */
/* .line 231 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MscsService"; // const-string v2, "MscsService"
android.util.Log .e ( v2,v1 );
/* .line 233 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unregisterScreenStatusListener ( ) {
/* .locals 3 */
/* .line 326 */
try { // :try_start_0
v0 = this.mScreenStatusListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 327 */
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 328 */
v1 = this.mScreenStatusListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterScreenStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 332 */
} // :cond_0
/* .line 330 */
/* :catch_0 */
/* move-exception v0 */
/* .line 331 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MscsService"; // const-string v2, "MscsService"
android.util.Log .e ( v2,v1 );
/* .line 333 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void onCreate ( ) {
/* .locals 5 */
/* .line 117 */
final String v0 = "onCreate "; // const-string v0, "onCreate "
final String v1 = "MscsService"; // const-string v1, "MscsService"
android.util.Log .i ( v1,v0 );
/* .line 119 */
try { // :try_start_0
v0 = this.mContext;
final String v2 = "MiuiWifiService"; // const-string v2, "MiuiWifiService"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/MiuiWifiManager; */
this.mMiuiWifiManager = v0;
/* .line 120 */
v0 = this.mContext;
final String v2 = "connectivity"; // const-string v2, "connectivity"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/ConnectivityManager; */
this.mConnectivityManager = v0;
/* .line 121 */
v0 = this.mContext;
/* const-string/jumbo v2, "wifi" */
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/WifiManager; */
this.mWifiManager = v0;
/* .line 122 */
v0 = this.mContext;
final String v2 = "SlaveWifiService"; // const-string v2, "SlaveWifiService"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/SlaveWifiManager; */
this.mSlaveWifiManager = v0;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 125 */
/* .line 123 */
/* :catch_0 */
/* move-exception v0 */
/* .line 124 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getSystemService Exception:"; // const-string v3, "getSystemService Exception:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 127 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* new-instance v0, Landroid/os/HandlerThread; */
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 128 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 129 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService$InternalHandler;-><init>(Lcom/xiaomi/NetworkBoost/MscsService/MscsService;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 130 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->registerMscsChangeObserver()V */
/* .line 131 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_mscs_enable"; // const-string v1, "cloud_mscs_enable"
int v2 = 0; // const/4 v2, 0x0
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$System .getIntForUser ( v0,v1,v2,v3 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
} // :goto_1
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mMSCSEnable:Z */
/* .line 133 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v4 = "cloud_wifi7_mscs_enable"; // const-string v4, "cloud_wifi7_mscs_enable"
v0 = android.provider.Settings$System .getIntForUser ( v0,v4,v2,v3 );
/* if-ne v0, v1, :cond_1 */
/* move v2, v1 */
} // :cond_1
/* iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->mWifi7MSCSEnable:Z */
/* .line 135 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->registerNetworkCallback()V */
/* .line 136 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->registerScreenStatusListener()V */
/* .line 137 */
return;
} // .end method
public void onDestroy ( ) {
/* .locals 2 */
/* .line 140 */
final String v0 = "MscsService"; // const-string v0, "MscsService"
final String v1 = "onDestroy "; // const-string v1, "onDestroy "
android.util.Log .i ( v0,v1 );
/* .line 141 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->unregisterNetworkCallback()V */
/* .line 142 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->unregisterScreenStatusListener()V */
/* .line 143 */
return;
} // .end method
