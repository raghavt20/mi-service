public abstract class com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost$Stub extends android.os.Binder implements com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost {
	 /* .source "IAIDLMiuiNetworkBoost.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub$a; */
/* } */
} // .end annotation
/* # direct methods */
public com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost$Stub ( ) {
/* .locals 1 */
/* .line 1 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 2 */
final String v0 = "com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost"; // const-string v0, "com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost"
(( android.os.Binder ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Landroid/os/Binder;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return;
} // .end method
public static void a ( android.os.Parcel p0, java.lang.String p1, java.lang.String p2 ) { //synthethic
/* .locals 0 */
/* .line 1 */
(( android.os.Parcel ) p0 ).writeString ( p1 ); // invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 2 */
(( android.os.Parcel ) p0 ).writeString ( p2 ); // invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
return;
} // .end method
public static void a ( android.os.Parcel p0, java.util.Map p1, Integer p2 ) { //synthethic
/* .locals 0 */
/* .line 3 */
(( android.os.Parcel ) p0 ).readString ( ); // invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 5 */
(( android.os.Parcel ) p0 ).readString ( ); // invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 6 */
return;
} // .end method
public static com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* if-nez p0, :cond_0 */
int p0 = 0; // const/4 p0, 0x0
/* .line 1 */
} // :cond_0
final String v0 = "com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost"; // const-string v0, "com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost"
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2 */
/* instance-of v1, v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 3 */
/* check-cast v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost; */
/* .line 5 */
} // :cond_1
/* new-instance v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub$a; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub$a;-><init>(Landroid/os/IBinder;)V */
} // .end method
public static void b ( android.os.Parcel p0, java.lang.String p1, java.lang.String p2 ) { //synthethic
/* .locals 0 */
/* .line 1 */
(( android.os.Parcel ) p0 ).writeString ( p1 ); // invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 2 */
(( android.os.Parcel ) p0 ).writeString ( p2 ); // invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
return;
} // .end method
public static void c ( android.os.Parcel p0, java.lang.String p1, java.lang.String p2 ) { //synthethic
/* .locals 0 */
/* .line 1 */
(( android.os.Parcel ) p0 ).writeString ( p1 ); // invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 2 */
(( android.os.Parcel ) p0 ).writeString ( p2 ); // invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
return;
} // .end method
public static void d ( android.os.Parcel p0, java.lang.String p1, java.lang.String p2 ) { //synthethic
/* .locals 0 */
/* .line 1 */
(( android.os.Parcel ) p0 ).writeString ( p1 ); // invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 2 */
(( android.os.Parcel ) p0 ).writeString ( p2 ); // invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
return;
} // .end method
public static com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost getDefaultImpl ( ) {
/* .locals 1 */
/* .line 1 */
v0 = com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost$Stub$a.b;
} // .end method
public static Boolean setDefaultImpl ( com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost p0 ) {
/* .locals 1 */
/* .line 1 */
v0 = com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost$Stub$a.b;
/* if-nez v0, :cond_1 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 5 */
int p0 = 1; // const/4 p0, 0x1
} // :cond_0
int p0 = 0; // const/4 p0, 0x0
/* .line 6 */
} // :cond_1
/* new-instance p0, Ljava/lang/IllegalStateException; */
/* const-string/jumbo v0, "setDefaultImpl() called twice" */
/* invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw p0 */
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1 */
/* const v0, 0x5f4e5446 */
int v1 = 1; // const/4 v1, 0x1
final String v2 = "com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost"; // const-string v2, "com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost"
/* if-eq p1, v0, :cond_8 */
int v0 = -1; // const/4 v0, -0x1
int v3 = 0; // const/4 v3, 0x0
/* packed-switch p1, :pswitch_data_0 */
/* .line 454 */
p1 = /* invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 455 */
/* :pswitch_0 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 457 */
p1 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 459 */
p2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
p1 = /* .line 460 */
/* .line 461 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 462 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 463 */
/* :pswitch_1 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 465 */
p1 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
p1 = /* .line 466 */
/* .line 467 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 468 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 469 */
/* :pswitch_2 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
p1 = /* .line 470 */
/* .line 471 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 472 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 473 */
/* :pswitch_3 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 475 */
p1 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
if ( p1 != null) { // if-eqz p1, :cond_0
/* move v3, v1 */
/* .line 476 */
p1 = } // :cond_0
/* .line 477 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 478 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 479 */
/* :pswitch_4 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
p1 = /* .line 480 */
/* .line 481 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 482 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 483 */
/* :pswitch_5 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
p1 = /* .line 484 */
/* .line 485 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 486 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 487 */
/* :pswitch_6 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 489 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback$Stub .asInterface ( p1 );
p1 = /* .line 490 */
/* .line 491 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 492 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 493 */
/* :pswitch_7 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 496 */
p1 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* if-gez p1, :cond_1 */
int p4 = 0; // const/4 p4, 0x0
/* .line 497 */
} // :cond_1
/* new-instance p4, Ljava/util/HashMap; */
/* invoke-direct {p4}, Ljava/util/HashMap;-><init>()V */
} // :goto_0
/* nop */
/* .line 498 */
java.util.stream.IntStream .range ( v3,p1 );
/* new-instance v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p2, p4}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub$$ExternalSyntheticLambda0;-><init>(Landroid/os/Parcel;Ljava/util/Map;)V */
/* .line 505 */
/* .line 506 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 507 */
/* :pswitch_8 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 508 */
/* .line 509 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 510 */
/* :pswitch_9 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 512 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback$Stub .asInterface ( p1 );
/* .line 514 */
p2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
p1 = /* .line 515 */
/* .line 516 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 517 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 518 */
/* :pswitch_a */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 520 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback$Stub .asInterface ( p1 );
/* .line 522 */
p2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
p1 = /* .line 523 */
/* .line 524 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 525 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 526 */
/* :pswitch_b */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 528 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback$Stub .asInterface ( p1 );
/* .line 530 */
p4 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 532 */
p2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
p1 = /* .line 533 */
/* .line 534 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 535 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 536 */
/* :pswitch_c */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 538 */
v3 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 540 */
(( android.os.Parcel ) p2 ).readLong ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v4 */
/* .line 542 */
(( android.os.Parcel ) p2 ).readLong ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v6 */
/* .line 544 */
v8 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 545 */
/* move-object v2, p0 */
/* invoke-interface/range {v2 ..v8}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->requestAppTrafficStatisticsByUid(IJJI)Ljava/util/Map; */
/* .line 546 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* if-nez p1, :cond_2 */
/* .line 548 */
(( android.os.Parcel ) p3 ).writeInt ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 550 */
p2 = } // :cond_2
(( android.os.Parcel ) p3 ).writeInt ( p2 ); // invoke-virtual {p3, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 551 */
/* new-instance p2, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub$$ExternalSyntheticLambda1; */
/* invoke-direct {p2, p3}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub$$ExternalSyntheticLambda1;-><init>(Landroid/os/Parcel;)V */
} // :goto_1
/* .line 552 */
/* :pswitch_d */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 554 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback$Stub .asInterface ( p1 );
/* .line 556 */
p2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
p1 = /* .line 557 */
/* .line 558 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 559 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 560 */
/* :pswitch_e */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 562 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback$Stub .asInterface ( p1 );
/* .line 564 */
p4 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 566 */
p2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
p1 = /* .line 567 */
/* .line 568 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 569 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 570 */
/* :pswitch_f */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 572 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback$Stub .asInterface ( p1 );
p1 = /* .line 573 */
/* .line 574 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 575 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 576 */
/* :pswitch_10 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 578 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback$Stub .asInterface ( p1 );
/* .line 580 */
p2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
p1 = /* .line 581 */
/* .line 582 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 583 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 584 */
/* :pswitch_11 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 586 */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 587 */
/* .line 588 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 590 */
(( android.os.Parcel ) p3 ).writeInt ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 591 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).writeToParcel ( p3, v1 ); // invoke-virtual {p1, p3, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->writeToParcel(Landroid/os/Parcel;I)V
} // :cond_3
/* nop */
/* .line 594 */
(( android.os.Parcel ) p3 ).writeInt ( v3 ); // invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V
} // :goto_2
/* .line 595 */
/* :pswitch_12 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
p1 = /* .line 596 */
/* .line 597 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 598 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 599 */
/* :pswitch_13 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 601 */
p1 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 603 */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
p1 = /* .line 604 */
/* .line 605 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 606 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 607 */
/* :pswitch_14 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 609 */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 610 */
/* .line 611 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* if-nez p1, :cond_4 */
/* .line 613 */
(( android.os.Parcel ) p3 ).writeInt ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 615 */
p2 = } // :cond_4
(( android.os.Parcel ) p3 ).writeInt ( p2 ); // invoke-virtual {p3, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 616 */
/* new-instance p2, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub$$ExternalSyntheticLambda2; */
/* invoke-direct {p2, p3}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub$$ExternalSyntheticLambda2;-><init>(Landroid/os/Parcel;)V */
} // :goto_3
/* .line 617 */
/* :pswitch_15 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 619 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback$Stub .asInterface ( p1 );
p1 = /* .line 620 */
/* .line 621 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 622 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 623 */
/* :pswitch_16 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 625 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback$Stub .asInterface ( p1 );
p1 = /* .line 626 */
/* .line 627 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 628 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 629 */
/* :pswitch_17 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
p1 = /* .line 630 */
/* .line 631 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 632 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 633 */
/* :pswitch_18 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
p1 = /* .line 634 */
/* .line 635 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 636 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 637 */
/* :pswitch_19 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
p1 = /* .line 638 */
/* .line 639 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 640 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 641 */
/* :pswitch_1a */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
p1 = /* .line 642 */
/* .line 643 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 644 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 645 */
/* :pswitch_1b */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
p1 = /* .line 646 */
/* .line 647 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 648 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 649 */
/* :pswitch_1c */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 651 */
(( android.os.Parcel ) p2 ).createIntArray ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I
p1 = /* .line 652 */
/* .line 653 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 654 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 655 */
/* :pswitch_1d */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 657 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
com.xiaomi.NetworkBoost.IAIDLMiuiNetworkCallback$Stub .asInterface ( p1 );
p1 = /* .line 658 */
/* .line 659 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 660 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 661 */
/* :pswitch_1e */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 663 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
com.xiaomi.NetworkBoost.IAIDLMiuiNetworkCallback$Stub .asInterface ( p1 );
p1 = /* .line 664 */
/* .line 665 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 666 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 667 */
/* :pswitch_1f */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 669 */
v3 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 671 */
(( android.os.Parcel ) p2 ).readLong ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v4 */
/* .line 673 */
(( android.os.Parcel ) p2 ).readLong ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v6 */
/* .line 674 */
/* move-object v2, p0 */
/* invoke-interface/range {v2 ..v7}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->requestAppTrafficStatistics(IJJ)Ljava/util/Map; */
/* .line 675 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* if-nez p1, :cond_5 */
/* .line 677 */
(( android.os.Parcel ) p3 ).writeInt ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 679 */
p2 = } // :cond_5
(( android.os.Parcel ) p3 ).writeInt ( p2 ); // invoke-virtual {p3, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 680 */
/* new-instance p2, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub$$ExternalSyntheticLambda3; */
/* invoke-direct {p2, p3}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub$$ExternalSyntheticLambda3;-><init>(Landroid/os/Parcel;)V */
} // :goto_4
/* .line 681 */
/* :pswitch_20 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 682 */
/* .line 683 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* if-nez p1, :cond_6 */
/* .line 685 */
(( android.os.Parcel ) p3 ).writeInt ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 687 */
p2 = } // :cond_6
(( android.os.Parcel ) p3 ).writeInt ( p2 ); // invoke-virtual {p3, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 688 */
/* new-instance p2, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub$$ExternalSyntheticLambda4; */
/* invoke-direct {p2, p3}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub$$ExternalSyntheticLambda4;-><init>(Landroid/os/Parcel;)V */
} // :goto_5
/* .line 689 */
/* :pswitch_21 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
p1 = /* .line 690 */
/* .line 691 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 692 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 693 */
/* :pswitch_22 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 695 */
p1 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
p1 = /* .line 696 */
/* .line 697 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 698 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 699 */
/* :pswitch_23 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 701 */
p1 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
if ( p1 != null) { // if-eqz p1, :cond_7
/* move v3, v1 */
/* .line 702 */
p1 = } // :cond_7
/* .line 703 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 704 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 705 */
/* :pswitch_24 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
p1 = /* .line 706 */
/* .line 707 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 708 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 709 */
/* :pswitch_25 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 711 */
p1 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 713 */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
p1 = /* .line 714 */
/* .line 715 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 716 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 717 */
/* :pswitch_26 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 719 */
p1 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 721 */
p2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
p1 = /* .line 722 */
/* .line 723 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 724 */
(( android.os.Parcel ) p3 ).writeInt ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 725 */
} // :cond_8
(( android.os.Parcel ) p3 ).writeString ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_26 */
/* :pswitch_25 */
/* :pswitch_24 */
/* :pswitch_23 */
/* :pswitch_22 */
/* :pswitch_21 */
/* :pswitch_20 */
/* :pswitch_1f */
/* :pswitch_1e */
/* :pswitch_1d */
/* :pswitch_1c */
/* :pswitch_1b */
/* :pswitch_1a */
/* :pswitch_19 */
/* :pswitch_18 */
/* :pswitch_17 */
/* :pswitch_16 */
/* :pswitch_15 */
/* :pswitch_14 */
/* :pswitch_13 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
