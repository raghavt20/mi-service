public class com.xiaomi.NetworkBoost.NetworkBoostProcessMonitor {
	 /* .source "NetworkBoostProcessMonitor.java" */
	 /* # static fields */
	 private static java.lang.String TAG;
	 /* # instance fields */
	 private java.lang.reflect.Method method_getForegroundInfo;
	 private java.lang.reflect.Method method_registerForegroundInfoListener;
	 private java.lang.reflect.Method method_unregisterForegroundInfoListener;
	 private java.lang.Class processManager;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/lang/Class<", */
	 /* "*>;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.xiaomi.NetworkBoost.NetworkBoostProcessMonitor ( ) {
/* .locals 1 */
/* .line 13 */
final String v0 = "NetworkBoostProcessMonitor"; // const-string v0, "NetworkBoostProcessMonitor"
return;
} // .end method
public com.xiaomi.NetworkBoost.NetworkBoostProcessMonitor ( ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 19 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 20 */
v0 = com.xiaomi.NetworkBoost.NetworkBoostProcessMonitor.TAG;
final String v1 = "NetworkBoostProcessMonitor"; // const-string v1, "NetworkBoostProcessMonitor"
android.util.Log .i ( v0,v1 );
/* .line 21 */
java.lang.ClassLoader .getSystemClassLoader ( );
/* .line 23 */
/* .local v0, "classLoader":Ljava/lang/ClassLoader; */
try { // :try_start_0
	 final String v1 = "miui.process.ProcessManager"; // const-string v1, "miui.process.ProcessManager"
	 (( java.lang.ClassLoader ) v0 ).loadClass ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
	 this.processManager = v1;
	 /* .line 24 */
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 25 */
		 final String v2 = "getForegroundInfo"; // const-string v2, "getForegroundInfo"
		 int v3 = 0; // const/4 v3, 0x0
		 /* move-object v4, v3 */
		 /* check-cast v4, [Ljava/lang/Class; */
		 (( java.lang.Class ) v1 ).getMethod ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
		 this.method_getForegroundInfo = v1;
		 /* .line 26 */
		 int v2 = 1; // const/4 v2, 0x1
		 (( java.lang.reflect.Method ) v1 ).setAccessible ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
		 /* .line 27 */
		 v1 = this.processManager;
		 final String v3 = "registerForegroundInfoListener"; // const-string v3, "registerForegroundInfoListener"
		 /* new-array v4, v2, [Ljava/lang/Class; */
		 /* const-class v5, Lmiui/process/IForegroundInfoListener; */
		 int v6 = 0; // const/4 v6, 0x0
		 /* aput-object v5, v4, v6 */
		 (( java.lang.Class ) v1 ).getMethod ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
		 this.method_registerForegroundInfoListener = v1;
		 /* .line 29 */
		 v1 = this.processManager;
		 /* const-string/jumbo v3, "unregisterForegroundInfoListener" */
		 /* new-array v4, v2, [Ljava/lang/Class; */
		 /* const-class v5, Lmiui/process/IForegroundInfoListener; */
		 /* aput-object v5, v4, v6 */
		 (( java.lang.Class ) v1 ).getMethod ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
		 this.method_unregisterForegroundInfoListener = v1;
		 /* .line 31 */
		 v1 = this.method_registerForegroundInfoListener;
		 (( java.lang.reflect.Method ) v1 ).setAccessible ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
		 /* .line 33 */
	 } // :cond_0
	 v1 = com.xiaomi.NetworkBoost.NetworkBoostProcessMonitor.TAG;
	 final String v2 = "processManager is null"; // const-string v2, "processManager is null"
	 android.util.Log .e ( v1,v2 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 37 */
} // :goto_0
/* .line 35 */
/* :catch_0 */
/* move-exception v1 */
/* .line 36 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.xiaomi.NetworkBoost.NetworkBoostProcessMonitor.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "NetworkBoostProcessMonitor don\'t support ProcessManager"; // const-string v4, "NetworkBoostProcessMonitor don\'t support ProcessManager"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 39 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
/* # virtual methods */
public miui.process.ForegroundInfo getForegroundInfo ( ) {
/* .locals 5 */
/* .line 42 */
int v0 = 0; // const/4 v0, 0x0
/* .line 44 */
/* .local v0, "foregroundInfo":Lmiui/process/ForegroundInfo; */
try { // :try_start_0
v1 = this.method_getForegroundInfo;
int v2 = 0; // const/4 v2, 0x0
/* move-object v3, v2 */
/* check-cast v3, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v1 ).invoke ( v2, v2 ); // invoke-virtual {v1, v2, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lmiui/process/ForegroundInfo; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v1 */
/* .line 47 */
/* .line 45 */
/* :catch_0 */
/* move-exception v1 */
/* .line 46 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.xiaomi.NetworkBoost.NetworkBoostProcessMonitor.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getForegroundInfo Exception"; // const-string v4, "getForegroundInfo Exception"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 48 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
public void registerForegroundInfoListener ( miui.process.IForegroundInfoListener p0 ) {
/* .locals 4 */
/* .param p1, "iForegroundInfoListener" # Lmiui/process/IForegroundInfoListener; */
/* .line 53 */
try { // :try_start_0
v0 = this.method_registerForegroundInfoListener;
/* filled-new-array {p1}, [Ljava/lang/Object; */
int v2 = 0; // const/4 v2, 0x0
(( java.lang.reflect.Method ) v0 ).invoke ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 56 */
/* .line 54 */
/* :catch_0 */
/* move-exception v0 */
/* .line 55 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.xiaomi.NetworkBoost.NetworkBoostProcessMonitor.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "registerForegroundInfoListener Exception"; // const-string v3, "registerForegroundInfoListener Exception"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 57 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void unregisterForegroundInfoListener ( miui.process.IForegroundInfoListener p0 ) {
/* .locals 4 */
/* .param p1, "iForegroundInfoListener" # Lmiui/process/IForegroundInfoListener; */
/* .line 61 */
try { // :try_start_0
v0 = this.method_unregisterForegroundInfoListener;
/* filled-new-array {p1}, [Ljava/lang/Object; */
int v2 = 0; // const/4 v2, 0x0
(( java.lang.reflect.Method ) v0 ).invoke ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 64 */
/* .line 62 */
/* :catch_0 */
/* move-exception v0 */
/* .line 63 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.xiaomi.NetworkBoost.NetworkBoostProcessMonitor.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "unregisterForegroundInfoListener Exception" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 65 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
