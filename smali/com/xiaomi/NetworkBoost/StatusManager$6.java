class com.xiaomi.NetworkBoost.StatusManager$6 extends android.net.ConnectivityManager$NetworkCallback {
	 /* .source "StatusManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/StatusManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.StatusManager this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.StatusManager$6 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/StatusManager; */
/* .line 1017 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAvailable ( android.net.Network p0 ) {
/* .locals 3 */
/* .param p1, "network" # Landroid/net/Network; */
/* .line 1021 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
final String v1 = "Default Network Callback onAvailable"; // const-string v1, "Default Network Callback onAvailable"
android.util.Log .i ( v0,v1 );
/* .line 1022 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmHandler ( v1 );
/* const/16 v2, 0x66 */
(( android.os.Handler ) v1 ).obtainMessage ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1023 */
return;
} // .end method
public void onCapabilitiesChanged ( android.net.Network p0, android.net.NetworkCapabilities p1 ) {
/* .locals 0 */
/* .param p1, "network" # Landroid/net/Network; */
/* .param p2, "networkCapabilities" # Landroid/net/NetworkCapabilities; */
/* .line 1039 */
return;
} // .end method
public void onLinkPropertiesChanged ( android.net.Network p0, android.net.LinkProperties p1 ) {
/* .locals 0 */
/* .param p1, "network" # Landroid/net/Network; */
/* .param p2, "linkProperties" # Landroid/net/LinkProperties; */
/* .line 1033 */
return;
} // .end method
public void onLost ( android.net.Network p0 ) {
/* .locals 0 */
/* .param p1, "network" # Landroid/net/Network; */
/* .line 1028 */
return;
} // .end method
