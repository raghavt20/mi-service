public abstract class com.xiaomi.NetworkBoost.MiuiNetworkCallback extends com.xiaomi.NetworkBoost.IAIDLMiuiNetworkCallback$Stub implements com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback implements com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback implements com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback {
	 /* .source "MiuiNetworkCallback.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public com.xiaomi.NetworkBoost.MiuiNetworkCallback ( ) {
		 /* .locals 0 */
		 /* .line 1 */
		 /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback$Stub;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void avaliableBssidCb ( java.util.List p0 ) {
		 /* .locals 0 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/List<", */
		 /* "Ljava/lang/String;", */
		 /* ">;)V" */
		 /* } */
	 } // .end annotation
	 return;
} // .end method
public void connectionStatusCb ( Integer p0 ) {
	 /* .locals 0 */
	 return;
} // .end method
public void dsdaStateChanged ( Boolean p0 ) {
	 /* .locals 0 */
	 return;
} // .end method
public void ifaceAdded ( java.util.List p0 ) {
	 /* .locals 0 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;)V" */
	 /* } */
} // .end annotation
return;
} // .end method
public void ifaceRemoved ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
return;
} // .end method
public void masterQoECallBack ( com.xiaomi.NetworkBoost.NetLinkLayerQoE p0 ) {
/* .locals 0 */
return;
} // .end method
public void mediaPlayerPolicyNotify ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 0 */
return;
} // .end method
public void onNetworkPriorityChanged ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 0 */
return;
} // .end method
public void onScanSuccussed ( Integer p0 ) {
/* .locals 0 */
return;
} // .end method
public void onSetSlaveWifiResult ( Boolean p0 ) {
/* .locals 0 */
return;
} // .end method
public void onSlaveWifiConnected ( Boolean p0 ) {
/* .locals 0 */
return;
} // .end method
public void onSlaveWifiDisconnected ( Boolean p0 ) {
/* .locals 0 */
return;
} // .end method
public void onSlaveWifiEnable ( Boolean p0 ) {
/* .locals 0 */
return;
} // .end method
public void onSlaveWifiEnableV1 ( Integer p0 ) {
/* .locals 0 */
return;
} // .end method
public void slaveQoECallBack ( com.xiaomi.NetworkBoost.NetLinkLayerQoE p0 ) {
/* .locals 0 */
return;
} // .end method
public void wlanQoEReportUpdateMaster ( java.util.Map p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
return;
} // .end method
public void wlanQoEReportUpdateSlave ( java.util.Map p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
return;
} // .end method
