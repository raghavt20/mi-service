public class com.xiaomi.NetworkBoost.NetworkBoostService extends android.app.Service {
	 /* .source "NetworkBoostService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;, */
	 /* Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String ACTION_START_SERVICE;
public static final Integer BASE;
public static final Integer MINETD_CMD_FLOWSTATGET;
public static final Integer MINETD_CMD_FLOWSTATSTART;
public static final Integer MINETD_CMD_FLOWSTATSTOP;
public static final Integer MINETD_CMD_SETSOCKPRIO;
public static final Integer MINETD_CMD_SETTCPCONGESTION;
public static final Integer SERVICE_VERSION;
private static final java.lang.String TAG;
private static android.content.Context mContext;
private static Boolean mJniLoaded;
private static volatile com.xiaomi.NetworkBoost.NetworkBoostService sInstance;
/* # instance fields */
private final com.xiaomi.NetworkBoost.NetworkBoostService$NetworkBoostServiceManager mAIDLMiuiNetworkBoost;
private final com.xiaomi.NetworkBoost.NetworkBoostService$SLAServiceBinder mAIDLSLAServiceBinder;
private final vendor.xiaomi.hidl.minet.V1_0.IMiNetCallback$Stub mMiNetCallback;
private com.xiaomi.NetworkBoost.MscsService.MscsService mMscsService;
private com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService mNetworkAccelerateSwitchService;
private com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService mNetworkSDKService;
private com.xiaomi.NetworkBoost.slaservice.SLAService mSLAService;
private com.xiaomi.NetworkBoost.StatusManager mStatusManager;
/* # direct methods */
static com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService -$$Nest$fgetmNetworkAccelerateSwitchService ( com.xiaomi.NetworkBoost.NetworkBoostService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mNetworkAccelerateSwitchService;
} // .end method
static com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService -$$Nest$fgetmNetworkSDKService ( com.xiaomi.NetworkBoost.NetworkBoostService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mNetworkSDKService;
} // .end method
static com.xiaomi.NetworkBoost.slaservice.SLAService -$$Nest$fgetmSLAService ( com.xiaomi.NetworkBoost.NetworkBoostService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mSLAService;
} // .end method
static Boolean -$$Nest$misSystemProcess ( com.xiaomi.NetworkBoost.NetworkBoostService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->isSystemProcess()Z */
} // .end method
static android.content.Context -$$Nest$sfgetmContext ( ) { //bridge//synthethic
	 /* .locals 1 */
	 v0 = com.xiaomi.NetworkBoost.NetworkBoostService.mContext;
} // .end method
static com.xiaomi.NetworkBoost.NetworkBoostService ( ) {
	 /* .locals 1 */
	 /* .line 50 */
	 int v0 = 0; // const/4 v0, 0x0
	 com.xiaomi.NetworkBoost.NetworkBoostService.mJniLoaded = (v0!= 0);
	 /* .line 57 */
	 int v0 = 0; // const/4 v0, 0x0
	 return;
} // .end method
private com.xiaomi.NetworkBoost.NetworkBoostService ( ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 679 */
	 /* invoke-direct {p0}, Landroid/app/Service;-><init>()V */
	 /* .line 60 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mSLAService = v0;
	 /* .line 63 */
	 this.mNetworkSDKService = v0;
	 /* .line 66 */
	 this.mStatusManager = v0;
	 /* .line 69 */
	 this.mNetworkAccelerateSwitchService = v0;
	 /* .line 72 */
	 this.mMscsService = v0;
	 /* .line 86 */
	 /* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager; */
	 /* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;-><init>(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)V */
	 this.mAIDLMiuiNetworkBoost = v0;
	 /* .line 541 */
	 /* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder; */
	 /* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;-><init>(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)V */
	 this.mAIDLSLAServiceBinder = v0;
	 /* .line 601 */
	 /* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$1; */
	 /* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService$1;-><init>(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)V */
	 this.mMiNetCallback = v0;
	 /* .line 680 */
	 /* .line 681 */
	 return;
} // .end method
public static void destroyInstance ( ) {
	 /* .locals 4 */
	 /* .line 643 */
	 v0 = com.xiaomi.NetworkBoost.NetworkBoostService.sInstance;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 644 */
		 /* const-class v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService; */
		 /* monitor-enter v0 */
		 /* .line 645 */
		 try { // :try_start_0
			 v1 = com.xiaomi.NetworkBoost.NetworkBoostService.sInstance;
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 /* .line 647 */
				 try { // :try_start_1
					 v1 = com.xiaomi.NetworkBoost.NetworkBoostService.sInstance;
					 (( com.xiaomi.NetworkBoost.NetworkBoostService ) v1 ).onDestroy ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->onDestroy()V
					 /* :try_end_1 */
					 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
					 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
					 /* .line 651 */
					 /* .line 649 */
					 /* :catch_0 */
					 /* move-exception v1 */
					 /* .line 650 */
					 /* .local v1, "e":Ljava/lang/Exception; */
					 try { // :try_start_2
						 final String v2 = "NetworkBoostService"; // const-string v2, "NetworkBoostService"
						 final String v3 = "destroyInstance onDestroy catch:"; // const-string v3, "destroyInstance onDestroy catch:"
						 android.util.Log .e ( v2,v3,v1 );
						 /* .line 652 */
					 } // .end local v1 # "e":Ljava/lang/Exception;
				 } // :goto_0
				 int v1 = 0; // const/4 v1, 0x0
				 /* .line 654 */
			 } // :cond_0
			 /* monitor-exit v0 */
			 /* :catchall_0 */
			 /* move-exception v1 */
			 /* monitor-exit v0 */
			 /* :try_end_2 */
			 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
			 /* throw v1 */
			 /* .line 656 */
		 } // :cond_1
	 } // :goto_1
	 return;
} // .end method
public static void dumpModule ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
	 /* .locals 4 */
	 /* .param p0, "fd" # Ljava/io/FileDescriptor; */
	 /* .param p1, "writer" # Ljava/io/PrintWriter; */
	 /* .param p2, "args" # [Ljava/lang/String; */
	 /* .line 659 */
	 v0 = com.xiaomi.NetworkBoost.NetworkBoostService.sInstance;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 660 */
		 /* const-class v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService; */
		 /* monitor-enter v0 */
		 /* .line 661 */
		 try { // :try_start_0
			 v1 = com.xiaomi.NetworkBoost.NetworkBoostService.sInstance;
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 /* .line 663 */
				 try { // :try_start_1
					 v1 = com.xiaomi.NetworkBoost.NetworkBoostService.sInstance;
					 (( com.xiaomi.NetworkBoost.NetworkBoostService ) v1 ).dump ( p0, p1, p2 ); // invoke-virtual {v1, p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
					 /* :try_end_1 */
					 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
					 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
					 /* .line 667 */
					 /* .line 665 */
					 /* :catch_0 */
					 /* move-exception v1 */
					 /* .line 666 */
					 /* .local v1, "e":Ljava/lang/Exception; */
					 try { // :try_start_2
						 final String v2 = "NetworkBoostService"; // const-string v2, "NetworkBoostService"
						 final String v3 = "dumpModule dump catch:"; // const-string v3, "dumpModule dump catch:"
						 android.util.Log .e ( v2,v3,v1 );
						 /* .line 667 */
						 /* nop */
					 } // .end local v1 # "e":Ljava/lang/Exception;
					 /* .line 670 */
				 } // :cond_0
				 final String v1 = "NetworkBoostService"; // const-string v1, "NetworkBoostService"
				 final String v2 = "dumpModule: instance is uninitialized."; // const-string v2, "dumpModule: instance is uninitialized."
				 android.util.Log .w ( v1,v2 );
				 /* .line 672 */
			 } // :goto_0
			 /* monitor-exit v0 */
			 /* :catchall_0 */
			 /* move-exception v1 */
			 /* monitor-exit v0 */
			 /* :try_end_2 */
			 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
			 /* throw v1 */
			 /* .line 675 */
		 } // :cond_1
		 final String v0 = "NetworkBoostService"; // const-string v0, "NetworkBoostService"
		 final String v1 = "dumpModule: instance is uninitialized."; // const-string v1, "dumpModule: instance is uninitialized."
		 android.util.Log .w ( v0,v1 );
		 /* .line 677 */
	 } // :goto_1
	 return;
} // .end method
public static com.xiaomi.NetworkBoost.NetworkBoostService getInstance ( android.content.Context p0 ) {
	 /* .locals 4 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .line 626 */
	 v0 = com.xiaomi.NetworkBoost.NetworkBoostService.sInstance;
	 /* if-nez v0, :cond_1 */
	 /* .line 627 */
	 /* const-class v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService; */
	 /* monitor-enter v0 */
	 /* .line 628 */
	 try { // :try_start_0
		 v1 = com.xiaomi.NetworkBoost.NetworkBoostService.sInstance;
		 /* if-nez v1, :cond_0 */
		 /* .line 629 */
		 /* new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkBoostService; */
		 /* invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;-><init>(Landroid/content/Context;)V */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* .line 631 */
		 try { // :try_start_1
			 v1 = com.xiaomi.NetworkBoost.NetworkBoostService.sInstance;
			 (( com.xiaomi.NetworkBoost.NetworkBoostService ) v1 ).onCreate ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->onCreate()V
			 /* :try_end_1 */
			 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
			 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
			 /* .line 635 */
			 /* .line 633 */
			 /* :catch_0 */
			 /* move-exception v1 */
			 /* .line 634 */
			 /* .local v1, "e":Ljava/lang/Exception; */
			 try { // :try_start_2
				 final String v2 = "NetworkBoostService"; // const-string v2, "NetworkBoostService"
				 final String v3 = "getInstance onCreate catch:"; // const-string v3, "getInstance onCreate catch:"
				 android.util.Log .e ( v2,v3,v1 );
				 /* .line 637 */
			 } // .end local v1 # "e":Ljava/lang/Exception;
		 } // :cond_0
	 } // :goto_0
	 /* monitor-exit v0 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_2 */
	 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
	 /* throw v1 */
	 /* .line 639 */
} // :cond_1
} // :goto_1
v0 = com.xiaomi.NetworkBoost.NetworkBoostService.sInstance;
} // .end method
private Boolean isSystemProcess ( ) {
/* .locals 2 */
/* .line 536 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 537 */
/* .local v0, "uid":I */
/* const/16 v1, 0x2710 */
/* if-ge v0, v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
/* # virtual methods */
protected void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 5 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "writer" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 826 */
final String v0 = "Dump of NetworkBoostService end"; // const-string v0, "Dump of NetworkBoostService end"
try { // :try_start_0
final String v1 = "Dump of NetworkBoostService begin:"; // const-string v1, "Dump of NetworkBoostService begin:"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 828 */
v1 = this.mStatusManager;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 829 */
(( com.xiaomi.NetworkBoost.StatusManager ) v1 ).dumpModule ( p2 ); // invoke-virtual {v1, p2}, Lcom/xiaomi/NetworkBoost/StatusManager;->dumpModule(Ljava/io/PrintWriter;)V
/* .line 832 */
} // :cond_0
final String v1 = "mStatusManager is null"; // const-string v1, "mStatusManager is null"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 836 */
} // :goto_0
v1 = this.mSLAService;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 837 */
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v1 ).dumpModule ( p2 ); // invoke-virtual {v1, p2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->dumpModule(Ljava/io/PrintWriter;)V
/* .line 840 */
} // :cond_1
final String v1 = "mSLAService is null"; // const-string v1, "mSLAService is null"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 844 */
} // :goto_1
v1 = this.mNetworkSDKService;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 845 */
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).dumpModule ( p2 ); // invoke-virtual {v1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->dumpModule(Ljava/io/PrintWriter;)V
/* .line 848 */
} // :cond_2
final String v1 = "mNetworkSDKService is null"; // const-string v1, "mNetworkSDKService is null"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 851 */
} // :goto_2
v1 = this.mNetworkAccelerateSwitchService;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 852 */
(( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService ) v1 ).dumpModule ( p2 ); // invoke-virtual {v1, p2}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->dumpModule(Ljava/io/PrintWriter;)V
/* .line 855 */
} // :cond_3
final String v1 = "mNetworkAccelerateSwitchService is null"; // const-string v1, "mNetworkAccelerateSwitchService is null"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 858 */
} // :goto_3
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 869 */
/* .line 860 */
/* :catch_0 */
/* move-exception v1 */
/* .line 861 */
/* .local v1, "ex":Ljava/lang/Exception; */
final String v2 = "dump failed!"; // const-string v2, "dump failed!"
final String v3 = "NetworkBoostService"; // const-string v3, "NetworkBoostService"
android.util.Log .e ( v3,v2,v1 );
/* .line 863 */
try { // :try_start_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Dump of NetworkBoostService failed:"; // const-string v4, "Dump of NetworkBoostService failed:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 864 */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 868 */
/* .line 866 */
/* :catch_1 */
/* move-exception v0 */
/* .line 867 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "dump failure failed:"; // const-string v4, "dump failure failed:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v2 );
/* .line 870 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end local v1 # "ex":Ljava/lang/Exception;
} // :goto_4
return;
} // .end method
public android.os.IBinder onBind ( android.content.Intent p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 768 */
final String v0 = "NetworkBoostService"; // const-string v0, "NetworkBoostService"
final String v1 = " onBind "; // const-string v1, " onBind "
android.util.Log .i ( v0,v1 );
/* .line 770 */
/* const-class v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost; */
(( java.lang.Class ) v0 ).getName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
(( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 771 */
v0 = this.mAIDLMiuiNetworkBoost;
/* .line 774 */
} // :cond_0
/* const-class v0, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService; */
(( java.lang.Class ) v0 ).getName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
(( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 775 */
v0 = this.mAIDLSLAServiceBinder;
/* .line 777 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void onCreate ( ) {
/* .locals 4 */
/* .line 691 */
final String v0 = " onCreate "; // const-string v0, " onCreate "
final String v1 = "NetworkBoostService"; // const-string v1, "NetworkBoostService"
android.util.Log .i ( v1,v0 );
/* .line 694 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mJniLoaded:Z */
/* if-nez v0, :cond_0 */
/* .line 696 */
try { // :try_start_0
final String v0 = "networkboostjni"; // const-string v0, "networkboostjni"
java.lang.System .loadLibrary ( v0 );
/* .line 697 */
int v0 = 1; // const/4 v0, 0x1
com.xiaomi.NetworkBoost.NetworkBoostService.mJniLoaded = (v0!= 0);
/* .line 698 */
final String v0 = "load libnetworkboostjni.so"; // const-string v0, "load libnetworkboostjni.so"
android.util.Log .i ( v1,v0 );
/* :try_end_0 */
/* .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 703 */
/* :catch_0 */
/* move-exception v0 */
/* .line 704 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "libnetworkboostjni.so load failed:"; // const-string v3, "libnetworkboostjni.so load failed:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 700 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v0 */
/* .line 701 */
/* .local v0, "e":Ljava/lang/UnsatisfiedLinkError; */
final String v2 = "load libnetworkboostjni.so failed"; // const-string v2, "load libnetworkboostjni.so failed"
android.util.Log .e ( v1,v2,v0 );
/* .line 705 */
} // .end local v0 # "e":Ljava/lang/UnsatisfiedLinkError;
} // :goto_0
/* nop */
/* .line 709 */
} // :cond_0
} // :goto_1
v0 = com.xiaomi.NetworkBoost.NetworkBoostService.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 712 */
v0 = com.xiaomi.NetworkBoost.NetworkBoostService.mContext;
com.xiaomi.NetworkBoost.slaservice.SLAService .getInstance ( v0 );
this.mSLAService = v0;
/* .line 715 */
v0 = com.xiaomi.NetworkBoost.NetworkBoostService.mContext;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .getInstance ( v0 );
this.mNetworkSDKService = v0;
/* .line 718 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mJniLoaded:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 720 */
v0 = com.xiaomi.NetworkBoost.NetworkBoostService.mContext;
v2 = this.mMiNetCallback;
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .getInstance ( v0,v2 );
this.mNetworkAccelerateSwitchService = v0;
/* .line 723 */
} // :cond_1
final String v0 = "WlanRamdumpCollector required libnetworkboostjni.so"; // const-string v0, "WlanRamdumpCollector required libnetworkboostjni.so"
android.util.Log .e ( v1,v0 );
/* .line 727 */
} // :goto_2
/* sget-boolean v0, Lmiui/util/DeviceLevel;->IS_MIUI_MIDDLE_VERSION:Z */
/* if-nez v0, :cond_2 */
/* .line 728 */
final String v0 = "is not MIUI Middle or is not port MIUI Middle, start MscsService"; // const-string v0, "is not MIUI Middle or is not port MIUI Middle, start MscsService"
android.util.Log .d ( v1,v0 );
/* .line 729 */
v0 = com.xiaomi.NetworkBoost.NetworkBoostService.mContext;
com.xiaomi.NetworkBoost.MscsService.MscsService .getInstance ( v0 );
this.mMscsService = v0;
/* .line 732 */
} // :cond_2
final String v0 = "addService >>>"; // const-string v0, "addService >>>"
android.util.Log .d ( v1,v0 );
/* .line 736 */
try { // :try_start_1
v0 = this.mAIDLMiuiNetworkBoost;
/* const-string/jumbo v2, "xiaomi.NetworkBoostServiceManager" */
android.os.ServiceManager .addService ( v2,v0 );
/* .line 737 */
final String v0 = "addService NetworkBoost success"; // const-string v0, "addService NetworkBoost success"
android.util.Log .d ( v1,v0 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_2 */
/* .line 741 */
/* .line 739 */
/* :catch_2 */
/* move-exception v0 */
/* .line 740 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "addService NetworkBoost failed:"; // const-string v3, "addService NetworkBoost failed:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 743 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_3
try { // :try_start_2
v0 = this.mAIDLSLAServiceBinder;
/* const-string/jumbo v2, "xiaomi.SLAService" */
android.os.ServiceManager .addService ( v2,v0 );
/* .line 744 */
final String v0 = "addService SLAService success"; // const-string v0, "addService SLAService success"
android.util.Log .d ( v1,v0 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_3 */
/* .line 748 */
/* .line 746 */
/* :catch_3 */
/* move-exception v0 */
/* .line 747 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "addService SLAService failed:"; // const-string v3, "addService SLAService failed:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 749 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_4
final String v0 = "addService <<<"; // const-string v0, "addService <<<"
android.util.Log .d ( v1,v0 );
/* .line 751 */
/* invoke-super {p0}, Landroid/app/Service;->onCreate()V */
/* .line 752 */
return;
} // .end method
public void onDestroy ( ) {
/* .locals 2 */
/* .line 788 */
final String v0 = "NetworkBoostService"; // const-string v0, "NetworkBoostService"
final String v1 = " onDestroy "; // const-string v1, " onDestroy "
android.util.Log .i ( v0,v1 );
/* .line 791 */
v0 = this.mStatusManager;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 792 */
com.xiaomi.NetworkBoost.StatusManager .destroyInstance ( );
/* .line 793 */
this.mStatusManager = v1;
/* .line 797 */
} // :cond_0
v0 = this.mSLAService;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 798 */
com.xiaomi.NetworkBoost.slaservice.SLAService .destroyInstance ( );
/* .line 799 */
this.mSLAService = v1;
/* .line 803 */
} // :cond_1
v0 = this.mNetworkSDKService;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 804 */
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .destroyInstance ( );
/* .line 805 */
this.mNetworkSDKService = v1;
/* .line 809 */
} // :cond_2
v0 = this.mNetworkAccelerateSwitchService;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 810 */
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .destroyInstance ( );
/* .line 811 */
this.mNetworkAccelerateSwitchService = v1;
/* .line 814 */
} // :cond_3
v0 = this.mMscsService;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 815 */
com.xiaomi.NetworkBoost.MscsService.MscsService .destroyInstance ( );
/* .line 816 */
this.mMscsService = v1;
/* .line 819 */
} // :cond_4
/* invoke-super {p0}, Landroid/app/Service;->onDestroy()V */
/* .line 820 */
return;
} // .end method
public void onNetworkPriorityChanged ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "priorityMode" # I */
/* .param p2, "trafficPolicy" # I */
/* .param p3, "thermalLevel" # I */
/* .line 684 */
v0 = this.mStatusManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 685 */
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).onNetworkPriorityChanged ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/xiaomi/NetworkBoost/StatusManager;->onNetworkPriorityChanged(III)V
/* .line 687 */
} // :cond_0
return;
} // .end method
public void onStart ( android.content.Intent p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "startId" # I */
/* .line 756 */
final String v0 = " onStart "; // const-string v0, " onStart "
final String v1 = "NetworkBoostService"; // const-string v1, "NetworkBoostService"
android.util.Log .i ( v1,v0 );
/* .line 758 */
if ( p1 != null) { // if-eqz p1, :cond_0
(( android.content.Intent ) p1 ).getExtras ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 759 */
final String v0 = "onStart, get intent:"; // const-string v0, "onStart, get intent:"
android.util.Log .d ( v1,v0 );
/* .line 762 */
} // :cond_0
/* invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V */
/* .line 763 */
return;
} // .end method
public Boolean onUnbind ( android.content.Intent p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 782 */
final String v0 = "NetworkBoostService"; // const-string v0, "NetworkBoostService"
final String v1 = " onUnbind "; // const-string v1, " onUnbind "
android.util.Log .i ( v0,v1 );
/* .line 783 */
v0 = /* invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z */
} // .end method
