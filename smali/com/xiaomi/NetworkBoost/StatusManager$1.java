class com.xiaomi.NetworkBoost.StatusManager$1 extends miui.process.IForegroundInfoListener$Stub {
	 /* .source "StatusManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/StatusManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.StatusManager this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.StatusManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/StatusManager; */
/* .line 297 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundInfoChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 3 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 300 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onForegroundInfoChanged uid:"; // const-string v2, "onForegroundInfoChanged uid:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
java.lang.Integer .toString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", isColdStart:"; // const-string v2, ", isColdStart:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 301 */
v2 = (( miui.process.ForegroundInfo ) p1 ).isColdStart ( ); // invoke-virtual {p1}, Lmiui/process/ForegroundInfo;->isColdStart()Z
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 300 */
android.util.Log .i ( v0,v1 );
/* .line 302 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmAppStatusListenerList ( v0 );
/* monitor-enter v0 */
/* .line 303 */
try { // :try_start_0
v1 = this.this$0;
/* iget v2, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fputmFUid ( v1,v2 );
/* .line 304 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmAppStatusListenerList ( v1 );
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener; */
/* .line 305 */
/* .local v2, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener; */
/* .line 306 */
} // .end local v2 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;
/* .line 307 */
} // :cond_0
/* monitor-exit v0 */
/* .line 308 */
return;
/* .line 307 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
