class com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService$InternalHandler extends android.os.Handler {
	 /* .source "NetworkAccelerateSwitchService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "InternalHandler" */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService this$0; //synthetic
/* # direct methods */
public com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService$InternalHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 723 */
this.this$0 = p1;
/* .line 724 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 725 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 5 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 728 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* const/16 v1, 0x67 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
final String v4 = "NetworkAccelerateSwitchService"; // const-string v4, "NetworkAccelerateSwitchService"
/* packed-switch v0, :pswitch_data_0 */
/* :pswitch_0 */
/* goto/16 :goto_2 */
/* .line 792 */
/* :pswitch_1 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fgetmMiuiWifiManager ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 793 */
	 final String v0 = "EVENT_STATIONARY not movement set Poll Rssi Interval 3000ms"; // const-string v0, "EVENT_STATIONARY not movement set Poll Rssi Interval 3000ms"
	 android.util.Log .i ( v4,v0 );
	 /* .line 794 */
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fgetmHandler ( v0 );
	 /* const/16 v1, 0x6e */
	 (( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
	 /* .line 795 */
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fgetmMiuiWifiManager ( v0 );
	 /* const/16 v1, 0xbb8 */
	 (( android.net.wifi.MiuiWifiManager ) v0 ).setPollRssiIntervalMillis ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/wifi/MiuiWifiManager;->setPollRssiIntervalMillis(I)V
	 /* .line 796 */
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fputmMovement ( v0,v2 );
	 /* goto/16 :goto_2 */
	 /* .line 784 */
	 /* :pswitch_2 */
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fgetmMiuiWifiManager ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 785 */
		 final String v0 = "EVENT_MOVEMENT movement set Poll Rssi Interval 1000ms"; // const-string v0, "EVENT_MOVEMENT movement set Poll Rssi Interval 1000ms"
		 android.util.Log .i ( v4,v0 );
		 /* .line 786 */
		 v0 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fgetmHandler ( v0 );
		 /* const/16 v1, 0x6d */
		 (( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
		 /* .line 787 */
		 v0 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fgetmMiuiWifiManager ( v0 );
		 /* const/16 v1, 0x3e8 */
		 (( android.net.wifi.MiuiWifiManager ) v0 ).setPollRssiIntervalMillis ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/wifi/MiuiWifiManager;->setPollRssiIntervalMillis(I)V
		 /* .line 788 */
		 v0 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fputmMovement ( v0,v3 );
		 /* goto/16 :goto_2 */
		 /* .line 778 */
		 /* :pswitch_3 */
		 final String v0 = "EVENT_SCREEN_OFF"; // const-string v0, "EVENT_SCREEN_OFF"
		 android.util.Log .i ( v4,v0 );
		 /* .line 779 */
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfputmScreenon ( v2 );
		 /* .line 780 */
		 v0 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mcheckRssiPollStartConditions ( v0 );
		 /* .line 781 */
		 v0 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mcheckRttPollStartConditions ( v0 );
		 /* .line 782 */
		 /* goto/16 :goto_2 */
		 /* .line 772 */
		 /* :pswitch_4 */
		 final String v0 = "EVENT_SCREEN_ON"; // const-string v0, "EVENT_SCREEN_ON"
		 android.util.Log .i ( v4,v0 );
		 /* .line 773 */
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfputmScreenon ( v3 );
		 /* .line 774 */
		 v0 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mcheckRssiPollStartConditions ( v0 );
		 /* .line 775 */
		 v0 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mcheckRttPollStartConditions ( v0 );
		 /* .line 776 */
		 /* goto/16 :goto_2 */
		 /* .line 768 */
		 /* :pswitch_5 */
		 final String v0 = "EVENT_EFFECT_APP_STOP"; // const-string v0, "EVENT_EFFECT_APP_STOP"
		 android.util.Log .i ( v4,v0 );
		 /* .line 769 */
		 v0 = this.this$0;
		 v1 = this.obj;
		 /* check-cast v1, Ljava/lang/Integer; */
		 v1 = 		 (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mcheckCollectIpAddress ( v0,v1,v2 );
		 /* .line 770 */
		 /* goto/16 :goto_2 */
		 /* .line 764 */
		 /* :pswitch_6 */
		 final String v0 = "EVENT_EFFECT_APP_START"; // const-string v0, "EVENT_EFFECT_APP_START"
		 android.util.Log .i ( v4,v0 );
		 /* .line 765 */
		 v0 = this.this$0;
		 v1 = this.obj;
		 /* check-cast v1, Ljava/lang/Integer; */
		 v1 = 		 (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mcheckCollectIpAddress ( v0,v1,v3 );
		 /* .line 766 */
		 /* goto/16 :goto_2 */
		 /* .line 750 */
		 /* :pswitch_7 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "EVENT_RSSI_POLL.mRssiPoll="; // const-string v2, "EVENT_RSSI_POLL.mRssiPoll="
		 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v2 = 		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfgetmRssiPoll ( );
		 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .i ( v4,v0 );
		 /* .line 751 */
		 v0 = this.this$0;
		 v0 = 		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mgetAverageWifiRssi ( v0 );
		 /* .line 752 */
		 /* .local v0, "rssi":I */
		 v2 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mcheckRssiInRange ( v2,v0 );
		 /* .line 753 */
		 v2 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mcheckRssiChange ( v2 );
		 /* .line 754 */
		 v2 = 		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfgetmRssiPoll ( );
		 if ( v2 != null) { // if-eqz v2, :cond_2
			 /* .line 755 */
			 v2 = this.this$0;
			 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fgetmHandler ( v2 );
			 /* .line 756 */
			 v3 = this.this$0;
			 v3 = 			 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fgetmMovement ( v3 );
			 if ( v3 != null) { // if-eqz v3, :cond_0
				 /* const-wide/16 v3, 0x3e8 */
			 } // :cond_0
			 /* const-wide/16 v3, 0xbb8 */
			 /* .line 755 */
		 } // :goto_0
		 (( android.os.Handler ) v2 ).sendEmptyMessageDelayed ( v1, v3, v4 ); // invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
		 /* .line 760 */
	 } // .end local v0 # "rssi":I
	 /* :pswitch_8 */
	 final String v0 = "EVENT_STOP_RSSI_POLL"; // const-string v0, "EVENT_STOP_RSSI_POLL"
	 android.util.Log .i ( v4,v0 );
	 /* .line 761 */
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mrestoreMovementState ( v0 );
	 /* .line 762 */
	 /* .line 746 */
	 /* :pswitch_9 */
	 final String v0 = "EVENT_START_RSSI_POLL"; // const-string v0, "EVENT_START_RSSI_POLL"
	 android.util.Log .i ( v4,v0 );
	 /* .line 747 */
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fgetmHandler ( v0 );
	 (( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
	 /* .line 748 */
	 /* .line 731 */
	 /* :pswitch_a */
	 try { // :try_start_0
		 vendor.xiaomi.hidl.minet.V1_0.IMiNetService .getService ( );
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfputmMiNetService ( v0 );
		 /* .line 732 */
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfgetmMiNetService ( );
		 /* if-nez v0, :cond_1 */
		 /* .line 733 */
		 final String v0 = "HAL service is null"; // const-string v0, "HAL service is null"
		 android.util.Log .e ( v4,v0 );
		 /* .line 734 */
		 v0 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fgetmHandler ( v0 );
		 /* const/16 v1, 0x64 */
		 /* const-wide/16 v2, 0xfa0 */
		 (( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
		 /* .line 737 */
	 } // :cond_1
	 final String v0 = "HAL service get success"; // const-string v0, "HAL service get success"
	 android.util.Log .d ( v4,v0 );
	 /* .line 738 */
	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfgetmMiNetService ( );
	 v1 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fgetmDeathRecipient ( v1 );
	 /* const-wide/16 v2, 0x0 */
	 /* .line 739 */
	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfgetmMiNetService ( );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 743 */
} // :goto_1
/* .line 741 */
/* :catch_0 */
/* move-exception v0 */
/* .line 742 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v4,v1 );
/* .line 744 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* nop */
/* .line 802 */
} // :cond_2
} // :goto_2
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x64 */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_0 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
