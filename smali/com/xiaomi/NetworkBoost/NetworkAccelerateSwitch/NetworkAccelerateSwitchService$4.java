class com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService$4 implements com.xiaomi.NetworkBoost.StatusManager$IDefaultNetworkInterfaceListener {
	 /* .source "NetworkAccelerateSwitchService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService; */
/* .line 658 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onDefaultNetwrokChange ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "ifacename" # Ljava/lang/String; */
/* .param p2, "ifacestatus" # I */
/* .line 661 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfgetmWifiReady ( );
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* if-eq p2, v1, :cond_0 */
	 /* .line 662 */
	 int v0 = 0; // const/4 v0, 0x0
	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfputmWifiReady ( v0 );
	 /* .line 663 */
	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfgetmInterface ( );
	 /* const-string/jumbo v2, "wlan0," */
	 final String v3 = ""; // const-string v3, ""
	 (( java.lang.String ) v0 ).replaceAll ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfputmInterface ( v0 );
	 /* .line 664 */
	 v0 = 	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfgetmIfaceNumber ( );
	 /* sub-int/2addr v0, v1 */
	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfputmIfaceNumber ( v0 );
	 /* .line 665 */
} // :cond_0
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfgetmWifiReady ( );
/* if-nez v0, :cond_1 */
/* if-ne p2, v1, :cond_1 */
/* .line 666 */
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfputmWifiReady ( v1 );
/* .line 667 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfgetmInterface ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ","; // const-string v2, ","
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfputmInterface ( v0 );
/* .line 668 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfgetmIfaceNumber ( );
/* add-int/2addr v0, v1 */
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfputmIfaceNumber ( v0 );
/* .line 672 */
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Default Netwrok Change mInterface:"; // const-string v1, "Default Netwrok Change mInterface:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfgetmInterface ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " mWifiReady:"; // const-string v1, " mWifiReady:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfgetmWifiReady ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkAccelerateSwitchService"; // const-string v1, "NetworkAccelerateSwitchService"
android.util.Log .i ( v1,v0 );
/* .line 673 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mcheckRssiPollStartConditions ( v0 );
/* .line 674 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mcheckRttPollStartConditions ( v0 );
/* .line 675 */
return;
/* .line 670 */
} // :cond_1
return;
} // .end method
