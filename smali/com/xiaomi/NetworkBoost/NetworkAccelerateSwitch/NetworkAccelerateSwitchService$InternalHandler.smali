.class Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;
.super Landroid/os/Handler;
.source "NetworkAccelerateSwitchService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;


# direct methods
.method public constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 723
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    .line 724
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 725
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .line 728
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x67

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, "NetworkAccelerateSwitchService"

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_2

    .line 792
    :pswitch_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fgetmMiuiWifiManager(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Landroid/net/wifi/MiuiWifiManager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 793
    const-string v0, "EVENT_STATIONARY not movement set Poll Rssi Interval 3000ms"

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 795
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fgetmMiuiWifiManager(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Landroid/net/wifi/MiuiWifiManager;

    move-result-object v0

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Landroid/net/wifi/MiuiWifiManager;->setPollRssiIntervalMillis(I)V

    .line 796
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0, v2}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fputmMovement(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;Z)V

    goto/16 :goto_2

    .line 784
    :pswitch_2
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fgetmMiuiWifiManager(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Landroid/net/wifi/MiuiWifiManager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 785
    const-string v0, "EVENT_MOVEMENT movement set Poll Rssi Interval 1000ms"

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x6d

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 787
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fgetmMiuiWifiManager(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Landroid/net/wifi/MiuiWifiManager;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/net/wifi/MiuiWifiManager;->setPollRssiIntervalMillis(I)V

    .line 788
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0, v3}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fputmMovement(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;Z)V

    goto/16 :goto_2

    .line 778
    :pswitch_3
    const-string v0, "EVENT_SCREEN_OFF"

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfputmScreenon(Z)V

    .line 780
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mcheckRssiPollStartConditions(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V

    .line 781
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mcheckRttPollStartConditions(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V

    .line 782
    goto/16 :goto_2

    .line 772
    :pswitch_4
    const-string v0, "EVENT_SCREEN_ON"

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfputmScreenon(Z)V

    .line 774
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mcheckRssiPollStartConditions(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V

    .line 775
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mcheckRttPollStartConditions(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V

    .line 776
    goto/16 :goto_2

    .line 768
    :pswitch_5
    const-string v0, "EVENT_EFFECT_APP_STOP"

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1, v2}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mcheckCollectIpAddress(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;IZ)V

    .line 770
    goto/16 :goto_2

    .line 764
    :pswitch_6
    const-string v0, "EVENT_EFFECT_APP_START"

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1, v3}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mcheckCollectIpAddress(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;IZ)V

    .line 766
    goto/16 :goto_2

    .line 750
    :pswitch_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EVENT_RSSI_POLL. mRssiPoll="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmRssiPoll()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 751
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mgetAverageWifiRssi(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)I

    move-result v0

    .line 752
    .local v0, "rssi":I
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v2, v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mcheckRssiInRange(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;I)V

    .line 753
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mcheckRssiChange(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V

    .line 754
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmRssiPoll()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 755
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Landroid/os/Handler;

    move-result-object v2

    .line 756
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fgetmMovement(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-wide/16 v3, 0x3e8

    goto :goto_0

    :cond_0
    const-wide/16 v3, 0xbb8

    .line 755
    :goto_0
    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 760
    .end local v0    # "rssi":I
    :pswitch_8
    const-string v0, "EVENT_STOP_RSSI_POLL"

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mrestoreMovementState(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V

    .line 762
    goto :goto_2

    .line 746
    :pswitch_9
    const-string v0, "EVENT_START_RSSI_POLL"

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 748
    goto :goto_2

    .line 731
    :pswitch_a
    :try_start_0
    invoke-static {}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;->getService()Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfputmMiNetService(Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;)V

    .line 732
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmMiNetService()Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    move-result-object v0

    if-nez v0, :cond_1

    .line 733
    const-string v0, "HAL service is null"

    invoke-static {v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x64

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 737
    :cond_1
    const-string v0, "HAL service get success"

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmMiNetService()Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fgetmDeathRecipient(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Landroid/os/IHwBinder$DeathRecipient;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;->linkToDeath(Landroid/os/IHwBinder$DeathRecipient;J)Z

    .line 739
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmMiNetService()Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    move-result-object v0

    invoke-interface {v0}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;->startMiNetd()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 743
    :goto_1
    goto :goto_2

    .line 741
    :catch_0
    move-exception v0

    .line 742
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 744
    .end local v0    # "e":Ljava/lang/Exception;
    nop

    .line 802
    :cond_2
    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
