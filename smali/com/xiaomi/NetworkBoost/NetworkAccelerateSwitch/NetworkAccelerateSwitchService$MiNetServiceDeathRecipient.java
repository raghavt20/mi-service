class com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService$MiNetServiceDeathRecipient implements android.os.IHwBinder$DeathRecipient {
	 /* .source "NetworkAccelerateSwitchService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MiNetServiceDeathRecipient" */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService$MiNetServiceDeathRecipient ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService; */
/* .line 307 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void serviceDied ( Long p0 ) {
/* .locals 4 */
/* .param p1, "cookie" # J */
/* .line 310 */
final String v0 = "NetworkAccelerateSwitchService"; // const-string v0, "NetworkAccelerateSwitchService"
final String v1 = "HAL service died"; // const-string v1, "HAL service died"
android.util.Log .e ( v0,v1 );
/* .line 311 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fgetmHandler ( v0 );
/* const/16 v1, 0x64 */
/* const-wide/16 v2, 0xfa0 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 313 */
return;
} // .end method
