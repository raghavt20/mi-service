.class Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$2;
.super Ljava/lang/Object;
.source "NetworkAccelerateSwitchService.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    .line 523
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$2;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioChanged(I)V
    .locals 5
    .param p1, "mode"    # I

    .line 551
    const/4 v0, 0x3

    const-string v1, " mEffectForegroundUid "

    const-string v2, "mEffectVoIPUid:"

    const-string v3, "NetworkAccelerateSwitchService"

    if-ne p1, v0, :cond_0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectVoIPUid()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectForegroundUid()I

    move-result v0

    if-nez v0, :cond_0

    .line 553
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectVoIPUid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectForegroundUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$2;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectVoIPUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$msetNetworkAccelerateSwitchAppStart(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;I)V

    goto :goto_0

    .line 555
    :cond_0
    if-nez p1, :cond_1

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectVoIPUid()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectForegroundUid()I

    move-result v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectVoIPUid()I

    move-result v4

    if-ne v0, v4, :cond_1

    .line 556
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectVoIPUid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectForegroundUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$2;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectVoIPUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$msetNetworkAccelerateSwitchAppStop(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;I)V

    .line 559
    :cond_1
    :goto_0
    return-void
.end method

.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 2
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;

    .line 528
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectForegroundUid()I

    move-result v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectVoIPUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectForegroundUid()I

    move-result v0

    iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I

    if-ne v0, v1, :cond_0

    .line 529
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$2;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    iget v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$msetNetworkAccelerateSwitchAppStop(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;I)V

    .line 532
    :cond_0
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectAppUidList()Ljava/util/HashSet;

    move-result-object v0

    iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 533
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$2;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$msetNetworkAccelerateSwitchAppStart(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;I)V

    .line 536
    :cond_1
    return-void
.end method

.method public onUidGone(IZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "disabled"    # Z

    .line 541
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmEffectForegroundUid()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 542
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$2;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0, p1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$msetNetworkAccelerateSwitchAppStop(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;I)V

    .line 545
    :cond_0
    return-void
.end method
