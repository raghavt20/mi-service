.class Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7;
.super Landroid/content/BroadcastReceiver;
.source "NetworkAccelerateSwitchService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->registerElevatorStatusReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    .line 1129
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1132
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1133
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$misEnableAccelerationSwitchHighSpeedMode(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    .line 1134
    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$misEnableAccelerationSwitch(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1135
    const-string v1, "com.android.phone.intent.action.ELEVATOR_STATE_SCENE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfgetmWifiReady()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    .line 1136
    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mcheckScreenStatus(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1137
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 1138
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v3, "elevatorState"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1139
    .local v3, "status":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "elevator status:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "NetworkAccelerateSwitchService"

    invoke-static {v5, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1140
    const/4 v4, 0x2

    const/4 v5, 0x1

    if-ne v3, v4, :cond_0

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fgetmisEvevator(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1141
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v2, v5}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fputmisEvevator(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;Z)V

    .line 1142
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$menabelNetworkSwitch(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V

    goto :goto_0

    .line 1143
    :cond_0
    if-ne v3, v5, :cond_1

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fgetmisEvevator(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)Z

    move-result v4

    if-ne v4, v5, :cond_1

    .line 1144
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v4, v2}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fputmisEvevator(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;Z)V

    .line 1145
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mdisabelNetworkSwitch(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V

    .line 1147
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v3    # "status":I
    :cond_1
    :goto_0
    goto :goto_1

    .line 1148
    :cond_2
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v1, v2}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$fputmisEvevator(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;Z)V

    .line 1149
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mdisabelNetworkSwitch(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V

    .line 1151
    :goto_1
    return-void
.end method
