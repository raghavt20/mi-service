class com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService$3 implements com.xiaomi.NetworkBoost.StatusManager$INetworkInterfaceListener {
	 /* .source "NetworkAccelerateSwitchService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService; */
/* .line 606 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onNetwrokInterfaceChange ( java.lang.String p0, Integer p1, Boolean p2, Boolean p3, Boolean p4, Boolean p5, java.lang.String p6 ) {
/* .locals 7 */
/* .param p1, "ifacename" # Ljava/lang/String; */
/* .param p2, "ifacenum" # I */
/* .param p3, "wifiready" # Z */
/* .param p4, "slavewifiready" # Z */
/* .param p5, "dataready" # Z */
/* .param p6, "slaveDataReady" # Z */
/* .param p7, "status" # Ljava/lang/String; */
/* .line 610 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0, p2 ); // invoke-virtual {p1, v0, p2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;
/* .line 611 */
/* .local v1, "arr":[Ljava/lang/String; */
final String v2 = ""; // const-string v2, ""
/* .line 612 */
/* .local v2, "newstring":Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .line 613 */
/* .local v3, "newcnt":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* array-length v5, v1 */
/* if-ge v4, v5, :cond_2 */
/* .line 614 */
/* aget-object v5, v1, v4 */
final String v6 = "rmnet_data"; // const-string v6, "rmnet_data"
v5 = (( java.lang.String ) v5 ).indexOf ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* if-gez v5, :cond_1 */
/* aget-object v5, v1, v4 */
/* .line 615 */
final String v6 = "ccmni"; // const-string v6, "ccmni"
v5 = (( java.lang.String ) v5 ).indexOf ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* if-ltz v5, :cond_0 */
/* .line 616 */
/* .line 618 */
} // :cond_0
/* aget-object v5, v1, v4 */
/* const-string/jumbo v6, "wlan" */
v5 = (( java.lang.String ) v5 ).indexOf ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* if-ltz v5, :cond_1 */
/* .line 619 */
/* aget-object v5, v1, v4 */
final String v6 = ""; // const-string v6, ""
(( java.lang.String ) v5 ).replaceAll ( v0, v6 ); // invoke-virtual {v5, v0, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* aput-object v5, v1, v4 */
/* .line 620 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v6, v1, v4 */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 621 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 613 */
} // :cond_1
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 624 */
} // .end local v4 # "i":I
} // :cond_2
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfputmInterface ( v2 );
/* .line 625 */
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfputmIfaceNumber ( v3 );
/* .line 626 */
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfputmWifiReady ( p3 );
/* .line 627 */
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfputmSlaveWifiReady ( p4 );
/* .line 628 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mcheckRssiPollStartConditions ( v0 );
/* .line 629 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mcheckRttPollStartConditions ( v0 );
/* .line 630 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mcheckWifiBand ( v0 );
/* .line 631 */
return;
} // .end method
