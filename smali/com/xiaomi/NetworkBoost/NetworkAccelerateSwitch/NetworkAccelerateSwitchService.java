public class com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService {
	 /* .source "NetworkAccelerateSwitchService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$MiNetServiceDeathRecipient;, */
	 /* Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;, */
	 /* Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$RttResult; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer BAND_24_GHZ_END_FREQ_MHZ;
private static final Integer BAND_24_GHZ_START_FREQ_MHZ;
public static final Integer BASE;
private static final java.lang.String CLOUD_WEAK_NETWORK_SWITCH_ENABLED;
private static final java.lang.String CLOUD_WEAK_NETWORK_SWITCH_HIGH_SPEED_MODE;
private static final Integer DEFAULT_FOREGROUND_UID;
private static final Integer DISABLE_NETWORK_SWITCH;
private static final Integer DUMP_MAX_COLUMN;
private static final Integer DUMP_MAX_ROW;
private static final Integer DUMP_SIZE_UPBOUND;
private static final Integer EFFECT_APP_STATUS_CHANGE_DELAY_MIILLIS;
private static final java.lang.String ELEVATOR_STATE;
private static final java.lang.String ELEVATOR_STATE_SCENE;
private static final Integer ENABLE_NETWORK_SWITCH;
private static final Integer ENTER_ELECATOR;
private static final Integer EVENT_CHECK_INTERFACE;
private static final Integer EVENT_EFFECT_APP_START;
private static final Integer EVENT_EFFECT_APP_STOP;
private static final Integer EVENT_GET_HAL;
private static final Integer EVENT_MOVEMENT;
private static final Integer EVENT_RSSI_POLL;
private static final Integer EVENT_SCREEN_OFF;
private static final Integer EVENT_SCREEN_ON;
private static final Integer EVENT_START_RSSI_POLL;
private static final Integer EVENT_STATIONARY;
private static final Integer EVENT_STOP_RSSI_POLL;
private static final Integer EXIT_ELECATOR;
private static final java.lang.String FEATURE_STATUS;
private static final Integer GET_SERVICE_DELAY_MILLIS;
private static final Integer GET_WIFI_RSSI_DELAY_MILLIS;
private static final java.lang.String LINKTURBO_IS_ENABLE;
private static final Integer LONG_RSSI_QUEUE_SIZE;
private static final Integer LONG_RTT_QUEUE_SIZE;
public static final Integer MINETD_CMD_FLOWSTATGET;
public static final Integer MINETD_CMD_FLOWSTATSTART;
public static final Integer MINETD_CMD_FLOWSTATSTOP;
public static final Integer MINETD_CMD_SETSOCKPRIO;
public static final Integer MINETD_CMD_SETTCPCONGESTION;
private static final Integer MOVEMENT_CHECK_TIME_DELAY;
private static final Integer MOVEMENT_EXIT_TIME_DELAY;
private static final Integer MOVEMENT_THRESHOLD_24GHZ;
private static final Integer MOVEMENT_THRESHOLD_5GHZ;
private static final java.lang.String NETWORKBOOST_ACCELERATE_RSSI_INCREASE;
private static final java.lang.String NETWORKBOOST_ACCELERATE_SCORE;
private static final Integer NETWORK_CACL_RTT_START;
private static final Integer NETWORK_CACL_RTT_STOP;
private static final java.lang.String NETWORK_SWITCH;
private static final Integer QOE_LOST_RATIO_QUEUE_SIZE;
private static final Integer SCREEN_OFF_DELAY_MIILLIS;
private static final Integer SCREEN_ON_DELAY_MIILLIS;
private static final Integer SETTING_VALUE_OFF;
private static final Integer SETTING_VALUE_ON;
private static final Integer SHOART_RSSI_QUEUE_SIZE;
private static final Integer SHOART_RTT_QUEUE_SIZE;
private static final Boolean START_COLLECT_IPADDRESS;
private static final Integer START_NETWORK_SWITCH_CONDITIONS;
private static final Boolean STOP_COLLECT_IPADDRESS;
private static final Integer STOP_NETWORK_SWITCH_CONDITIONS;
private static final java.lang.String TAG;
private static final java.lang.String TCP_BLACK_LIST;
private static final Integer TCP_TARTGET_INDEX;
private static final java.lang.String UDP_BLACK_LIST;
private static final Integer UDP_TARTGET_INDEX;
private static final Integer VOIP_RSSI_DEFAULT;
private static final Integer VOIP_RSSI_INCREASE;
private static final java.lang.String WIFI_ASSISTANT;
private static final Integer WIFI_ASSISTANT_DEFAULT;
private static java.util.HashSet mEffectAppUidList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static Integer mEffectForegroundUid;
private static Integer mEffectVoIPUid;
private static Integer mIfaceNumber;
private static java.lang.String mInterface;
private static vendor.xiaomi.hidl.minet.V1_0.IMiNetCallback mMiNetCallback;
private static vendor.xiaomi.hidl.minet.V1_0.IMiNetService mMiNetService;
private static Boolean mRssiInRange;
private static Boolean mRssiPoll;
private static Boolean mScreenon;
private static Boolean mSlaveWifiReady;
private static Boolean mWifiReady;
public static volatile com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService sInstance;
/* # instance fields */
private com.xiaomi.NetworkBoost.StatusManager$IAppStatusListener mAppStatusListener;
private Integer mAverageRssi;
private android.content.Context mContext;
private android.os.IHwBinder$DeathRecipient mDeathRecipient;
private com.xiaomi.NetworkBoost.StatusManager$IDefaultNetworkInterfaceListener mDefaultNetworkInterfaceListener;
private android.content.BroadcastReceiver mElevatorStatusReceiver;
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
java.util.Queue mLongRssiQueue;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Queue<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
java.util.Queue mLongUnmeteredRttQueue;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Queue<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.net.wifi.MiuiWifiManager mMiuiWifiManager;
private volatile Boolean mMobileDataAlwaysonEnable;
private Boolean mMovement;
private Integer mMovement24GHzThreshold;
private Integer mMovement5GHzThreshold;
private android.database.ContentObserver mMovementObserver;
private com.xiaomi.NetworkBoost.StatusManager$IMovementSensorStatusListener mMovementSensorStatusListener;
private Integer mMovementState;
private com.xiaomi.NetworkBoost.StatusManager$INetworkInterfaceListener mNetworkInterfaceListener;
private com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService mNetworkSDKService;
private android.database.ContentObserver mObserver;
java.util.Queue mQoeLostRatioQueue;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Queue<", */
/* "Ljava/lang/Double;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.xiaomi.NetworkBoost.StatusManager$IScreenStatusListener mScreenStatusListener;
private Boolean mSendBroadcast;
java.util.Queue mShortRssiQueue;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Queue<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
java.util.Queue mShortUnmeteredRttQueue;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Queue<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.xiaomi.NetworkBoost.StatusManager mStatusManager;
private volatile Boolean mSwithInWeakNet;
private android.net.wifi.WifiManager mWifiManager;
private Boolean mis24GHz;
private Boolean misEvevator;
private Boolean misHighRtt;
/* # direct methods */
static android.os.IHwBinder$DeathRecipient -$$Nest$fgetmDeathRecipient ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDeathRecipient;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static android.net.wifi.MiuiWifiManager -$$Nest$fgetmMiuiWifiManager ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiuiWifiManager;
} // .end method
static Boolean -$$Nest$fgetmMovement ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement:Z */
} // .end method
static Integer -$$Nest$fgetmMovementState ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovementState:I */
} // .end method
static Boolean -$$Nest$fgetmisEvevator ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misEvevator:Z */
} // .end method
static void -$$Nest$fputmMovement ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement:Z */
return;
} // .end method
static void -$$Nest$fputmMovementState ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovementState:I */
return;
} // .end method
static void -$$Nest$fputmisEvevator ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misEvevator:Z */
return;
} // .end method
static void -$$Nest$mcheckCollectIpAddress ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0, Integer p1, Boolean p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkCollectIpAddress(IZ)V */
return;
} // .end method
static void -$$Nest$mcheckHighSpeedModeCloudController ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkHighSpeedModeCloudController()V */
return;
} // .end method
static void -$$Nest$mcheckRssiChange ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkRssiChange()V */
return;
} // .end method
static void -$$Nest$mcheckRssiInRange ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkRssiInRange(I)V */
return;
} // .end method
static void -$$Nest$mcheckRssiPollStartConditions ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkRssiPollStartConditions()V */
return;
} // .end method
static void -$$Nest$mcheckRttPollStartConditions ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkRttPollStartConditions()V */
return;
} // .end method
static Boolean -$$Nest$mcheckScreenStatus ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkScreenStatus()Z */
} // .end method
static void -$$Nest$mcheckWifiBand ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkWifiBand()V */
return;
} // .end method
static void -$$Nest$mdisabelNetworkSwitch ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->disabelNetworkSwitch()V */
return;
} // .end method
static void -$$Nest$menabelNetworkSwitch ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->enabelNetworkSwitch()V */
return;
} // .end method
static Integer -$$Nest$mgetAverageWifiRssi ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->getAverageWifiRssi()I */
} // .end method
static Boolean -$$Nest$misEnableAccelerationSwitch ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->isEnableAccelerationSwitch()Z */
} // .end method
static Boolean -$$Nest$misEnableAccelerationSwitchHighSpeedMode ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->isEnableAccelerationSwitchHighSpeedMode()Z */
} // .end method
static void -$$Nest$mrestoreMovementState ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->restoreMovementState()V */
return;
} // .end method
static void -$$Nest$msetNetworkAccelerateSwitchAppStart ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->setNetworkAccelerateSwitchAppStart(I)V */
return;
} // .end method
static void -$$Nest$msetNetworkAccelerateSwitchAppStop ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->setNetworkAccelerateSwitchAppStop(I)V */
return;
} // .end method
static java.util.HashSet -$$Nest$sfgetmEffectAppUidList ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mEffectAppUidList;
} // .end method
static Integer -$$Nest$sfgetmEffectForegroundUid ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static Integer -$$Nest$sfgetmEffectVoIPUid ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static Integer -$$Nest$sfgetmIfaceNumber ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static java.lang.String -$$Nest$sfgetmInterface ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mInterface;
} // .end method
static vendor.xiaomi.hidl.minet.V1_0.IMiNetService -$$Nest$sfgetmMiNetService ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mMiNetService;
} // .end method
static Boolean -$$Nest$sfgetmRssiPoll ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mRssiPoll:Z */
} // .end method
static Boolean -$$Nest$sfgetmWifiReady ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mWifiReady:Z */
} // .end method
static void -$$Nest$sfputmIfaceNumber ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static void -$$Nest$sfputmInterface ( java.lang.String p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static void -$$Nest$sfputmMiNetService ( vendor.xiaomi.hidl.minet.V1_0.IMiNetService p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static void -$$Nest$sfputmScreenon ( Boolean p0 ) { //bridge//synthethic
/* .locals 0 */
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mScreenon = (p0!= 0);
return;
} // .end method
static void -$$Nest$sfputmSlaveWifiReady ( Boolean p0 ) { //bridge//synthethic
/* .locals 0 */
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mSlaveWifiReady = (p0!= 0);
return;
} // .end method
static void -$$Nest$sfputmWifiReady ( Boolean p0 ) { //bridge//synthethic
/* .locals 0 */
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mWifiReady = (p0!= 0);
return;
} // .end method
static com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService ( ) {
/* .locals 2 */
/* .line 117 */
int v0 = 0; // const/4 v0, 0x0
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mWifiReady = (v0!= 0);
/* .line 118 */
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mSlaveWifiReady = (v0!= 0);
/* .line 119 */
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mRssiInRange = (v0!= 0);
/* .line 120 */
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mRssiPoll = (v0!= 0);
/* .line 121 */
final String v1 = ""; // const-string v1, ""
/* .line 122 */
/* .line 125 */
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mScreenon = (v0!= 0);
/* .line 136 */
int v1 = 0; // const/4 v1, 0x0
/* .line 137 */
/* .line 141 */
/* .line 142 */
/* .line 143 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
return;
} // .end method
private com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "minetcallback" # Lvendor/xiaomi/hidl/minet/V1_0/IMiNetCallback; */
/* .line 246 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 53 */
int v0 = 0; // const/4 v0, 0x0
this.mContext = v0;
/* .line 57 */
this.mWifiManager = v0;
/* .line 58 */
this.mMiuiWifiManager = v0;
/* .line 61 */
this.mObserver = v0;
/* .line 69 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mSwithInWeakNet:Z */
/* .line 78 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mSendBroadcast:Z */
/* .line 83 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMobileDataAlwaysonEnable:Z */
/* .line 86 */
this.mStatusManager = v0;
/* .line 89 */
this.mNetworkSDKService = v0;
/* .line 150 */
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mAverageRssi:I */
/* .line 153 */
/* new-instance v2, Ljava/util/LinkedList; */
/* invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V */
this.mShortRssiQueue = v2;
/* .line 154 */
/* new-instance v2, Ljava/util/LinkedList; */
/* invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V */
this.mLongRssiQueue = v2;
/* .line 159 */
/* new-instance v2, Ljava/util/LinkedList; */
/* invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V */
this.mShortUnmeteredRttQueue = v2;
/* .line 160 */
/* new-instance v2, Ljava/util/LinkedList; */
/* invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V */
this.mLongUnmeteredRttQueue = v2;
/* .line 172 */
/* new-instance v2, Ljava/util/LinkedList; */
/* invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V */
this.mQoeLostRatioQueue = v2;
/* .line 196 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misEvevator:Z */
/* .line 197 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misHighRtt:Z */
/* .line 207 */
this.mMovementObserver = v0;
/* .line 208 */
int v0 = 6; // const/4 v0, 0x6
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement24GHzThreshold:I */
/* .line 209 */
/* const/16 v0, 0xa */
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement5GHzThreshold:I */
/* .line 211 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mis24GHz:Z */
/* .line 315 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$MiNetServiceDeathRecipient; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$MiNetServiceDeathRecipient;-><init>(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V */
this.mDeathRecipient = v0;
/* .line 523 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$2; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$2;-><init>(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V */
this.mAppStatusListener = v0;
/* .line 606 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$3; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$3;-><init>(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V */
this.mNetworkInterfaceListener = v0;
/* .line 658 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$4; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$4;-><init>(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V */
this.mDefaultNetworkInterfaceListener = v0;
/* .line 702 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$5; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$5;-><init>(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V */
this.mScreenStatusListener = v0;
/* .line 1203 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement:Z */
/* .line 1227 */
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovementState:I */
/* .line 1229 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$8; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$8;-><init>(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V */
this.mMovementSensorStatusListener = v0;
/* .line 247 */
(( android.content.Context ) p1 ).getApplicationContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
this.mContext = v0;
/* .line 248 */
/* .line 249 */
return;
} // .end method
private void checkCollectIpAddress ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "opt" # Z */
/* .line 878 */
int v0 = 0; // const/4 v0, 0x0
/* .line 880 */
/* .local v0, "chagne":Z */
int v1 = 1; // const/4 v1, 0x1
/* if-ne p2, v1, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* if-eq p1, v1, :cond_0 */
/* .line 881 */
v1 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->isEnableAccelerationSwitch()Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 882 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->startCollectIpAddress(I)Z */
/* .line 883 */
/* .line 884 */
int v0 = 1; // const/4 v0, 0x1
/* .line 885 */
} // :cond_0
/* if-nez p2, :cond_1 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 886 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->stopCollectIpAddress(I)Z */
/* .line 887 */
int v1 = 0; // const/4 v1, 0x0
/* .line 888 */
int v0 = 1; // const/4 v0, 0x1
/* .line 891 */
} // :cond_1
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 892 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkRttPollStartConditions()V */
/* .line 894 */
} // :cond_2
return;
} // .end method
private void checkHighSpeedModeCloudController ( ) {
/* .locals 4 */
/* .line 1100 */
int v0 = 6; // const/4 v0, 0x6
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement24GHzThreshold:I */
/* .line 1101 */
/* const/16 v0, 0xa */
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement5GHzThreshold:I */
/* .line 1103 */
v0 = this.mContext;
/* .line 1104 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1103 */
final String v1 = "cloud_weak_network_switch_high_speed_mode"; // const-string v1, "cloud_weak_network_switch_high_speed_mode"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 1105 */
/* .local v0, "cvalue":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* const-string/jumbo v1, "v1" */
v1 = (( java.lang.String ) v0 ).indexOf ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
int v2 = -1; // const/4 v2, -0x1
/* if-ne v1, v2, :cond_0 */
/* .line 1110 */
} // :cond_0
try { // :try_start_0
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_1 */
/* .line 1111 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1112 */
/* .local v1, "controllers":[Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* array-length v2, v1 */
int v3 = 3; // const/4 v3, 0x3
/* if-ne v2, v3, :cond_1 */
/* .line 1113 */
int v2 = 1; // const/4 v2, 0x1
/* aget-object v2, v1, v2 */
v2 = java.lang.Integer .parseInt ( v2 );
/* iput v2, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement24GHzThreshold:I */
/* .line 1114 */
int v2 = 2; // const/4 v2, 0x2
/* aget-object v2, v1, v2 */
v2 = java.lang.Integer .parseInt ( v2 );
/* iput v2, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement5GHzThreshold:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1119 */
} // .end local v1 # "controllers":[Ljava/lang/String;
} // :cond_1
/* .line 1117 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1118 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "NetworkAccelerateSwitchService"; // const-string v2, "NetworkAccelerateSwitchService"
final String v3 = "cloud observer catch:"; // const-string v3, "cloud observer catch:"
android.util.Log .e ( v2,v3,v1 );
/* .line 1120 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 1106 */
} // :cond_2
} // :goto_1
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->restoreMovementState()V */
/* .line 1107 */
return;
} // .end method
private void checkRssiChange ( ) {
/* .locals 4 */
/* .line 1247 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->isEnableAccelerationSwitch()Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1248 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->isEnableAccelerationSwitchHighSpeedMode()Z */
/* if-nez v0, :cond_0 */
/* .line 1251 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "checkRssiChange mMovementState:"; // const-string v1, "checkRssiChange mMovementState:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovementState:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " mMovement:"; // const-string v1, " mMovement:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkAccelerateSwitchService"; // const-string v1, "NetworkAccelerateSwitchService"
android.util.Log .i ( v1,v0 );
/* .line 1252 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovementState:I */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement:Z */
/* if-nez v1, :cond_1 */
/* .line 1253 */
v0 = this.mShortRssiQueue;
v0 = (( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService ) p0 ).checkMovement ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkMovement(Ljava/util/Queue;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1254 */
v0 = this.mHandler;
/* const/16 v1, 0x6d */
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 1256 */
} // :cond_1
/* const/16 v1, 0x6e */
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1257 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1258 */
} // :cond_2
/* if-nez v0, :cond_3 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1259 */
v0 = this.mHandler;
/* const-wide/16 v2, 0x7530 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 1261 */
} // :cond_3
} // :goto_0
return;
/* .line 1249 */
} // :cond_4
} // :goto_1
return;
} // .end method
private void checkRssiInRange ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "rssi" # I */
/* .line 865 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mRssiInRange:Z */
/* .line 866 */
/* .local v0, "rssiinrange":Z */
/* const/16 v1, 0x47 */
/* if-le p1, v1, :cond_0 */
/* .line 867 */
int v1 = 1; // const/4 v1, 0x1
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mRssiInRange = (v1!= 0);
/* .line 868 */
} // :cond_0
/* const/16 v1, 0x43 */
/* if-ge p1, v1, :cond_1 */
/* .line 869 */
int v1 = 0; // const/4 v1, 0x0
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mRssiInRange = (v1!= 0);
/* .line 872 */
} // :cond_1
} // :goto_0
/* sget-boolean v1, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mRssiInRange:Z */
/* if-eq v0, v1, :cond_2 */
/* .line 873 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkRttPollStartConditions()V */
/* .line 875 */
} // :cond_2
return;
} // .end method
private void checkRssiPollStartConditions ( ) {
/* .locals 2 */
/* .line 843 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "checkRssiPollStartConditions mWifiReady:"; // const-string v1, "checkRssiPollStartConditions mWifiReady:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mWifiReady:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " mRssiPoll:"; // const-string v1, " mRssiPoll:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mRssiPoll:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " checkScreenStatus():"; // const-string v1, " checkScreenStatus():"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 845 */
v1 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkScreenStatus()Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " isEnableAccelerationSwitch():"; // const-string v1, " isEnableAccelerationSwitch():"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 846 */
v1 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->isEnableAccelerationSwitch()Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 843 */
final String v1 = "NetworkAccelerateSwitchService"; // const-string v1, "NetworkAccelerateSwitchService"
android.util.Log .i ( v1,v0 );
/* .line 847 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 848 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mRssiPoll:Z */
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mWifiReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkScreenStatus()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 849 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->isEnableAccelerationSwitch()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 850 */
v0 = this.mHandler;
/* const/16 v1, 0x65 */
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 851 */
int v0 = 1; // const/4 v0, 0x1
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mRssiPoll = (v0!= 0);
/* .line 852 */
} // :cond_0
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mRssiPoll:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mWifiReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkScreenStatus()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 853 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->isEnableAccelerationSwitch()Z */
/* if-nez v0, :cond_3 */
/* .line 854 */
} // :cond_1
v0 = this.mHandler;
/* const/16 v1, 0x67 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 855 */
v0 = this.mHandler;
/* const/16 v1, 0x66 */
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 856 */
int v0 = 0; // const/4 v0, 0x0
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mRssiPoll = (v0!= 0);
/* .line 860 */
} // :cond_2
final String v0 = "checkRssiPollStartConditions handler is null"; // const-string v0, "checkRssiPollStartConditions handler is null"
android.util.Log .e ( v1,v0 );
/* .line 862 */
} // :cond_3
} // :goto_0
return;
} // .end method
private void checkRttPollStartConditions ( ) {
/* .locals 2 */
/* .line 923 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "checkRttPollStartConditions mWifiReady:"; // const-string v1, "checkRttPollStartConditions mWifiReady:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mWifiReady:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " mRssiPoll:"; // const-string v1, " mRssiPoll:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mRssiPoll:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " mRssiInRange:"; // const-string v1, " mRssiInRange:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mRssiInRange:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " mEffectForegroundUid:"; // const-string v1, " mEffectForegroundUid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " checkScreenStatus():"; // const-string v1, " checkScreenStatus():"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 927 */
v1 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkScreenStatus()Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " isEnableAccelerationSwitch():"; // const-string v1, " isEnableAccelerationSwitch():"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 928 */
v1 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->isEnableAccelerationSwitch()Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 923 */
final String v1 = "NetworkAccelerateSwitchService"; // const-string v1, "NetworkAccelerateSwitchService"
android.util.Log .i ( v1,v0 );
/* .line 929 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->isEnableAccelerationSwitch()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mWifiReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mRssiPoll:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mRssiInRange:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 931 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->checkScreenStatus()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 932 */
final String v0 = "checkRttPollStartConditions start"; // const-string v0, "checkRttPollStartConditions start"
android.util.Log .i ( v1,v0 );
/* .line 933 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mInterface;
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->startRttCollection(Ljava/lang/String;)I */
/* .line 935 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misHighRtt:Z */
/* .line 936 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->stopRttCollection()I */
/* .line 937 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->disabelNetworkSwitch()V */
/* .line 938 */
final String v0 = "checkRttPollStartConditions stop"; // const-string v0, "checkRttPollStartConditions stop"
android.util.Log .i ( v1,v0 );
/* .line 940 */
} // :goto_0
return;
} // .end method
private Boolean checkScreenStatus ( ) {
/* .locals 2 */
/* .line 897 */
/* if-ne v0, v1, :cond_0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 899 */
int v0 = 1; // const/4 v0, 0x1
/* .line 901 */
} // :cond_0
/* sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mScreenon:Z */
} // .end method
private void checkWifiBand ( ) {
/* .locals 2 */
/* .line 905 */
v0 = this.mWifiManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 906 */
(( android.net.wifi.WifiManager ) v0 ).getConnectionInfo ( ); // invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;
/* .line 907 */
/* .local v0, "wifiInfo":Landroid/net/wifi/WifiInfo; */
v1 = (( android.net.wifi.WifiInfo ) v0 ).getFrequency ( ); // invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getFrequency()I
v1 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .is24GHz ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 908 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mis24GHz:Z */
/* .line 910 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mis24GHz:Z */
/* .line 913 */
} // .end local v0 # "wifiInfo":Landroid/net/wifi/WifiInfo;
} // :cond_1
} // :goto_0
return;
} // .end method
private native Integer deinitNetworkAccelerateSwitch ( ) {
} // .end method
public static void destroyInstance ( ) {
/* .locals 4 */
/* .line 231 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.sInstance;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 232 */
/* const-class v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService; */
/* monitor-enter v0 */
/* .line 233 */
try { // :try_start_0
v1 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.sInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 235 */
try { // :try_start_1
v1 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.sInstance;
(( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService ) v1 ).onDestroy ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->onDestroy()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 239 */
/* .line 237 */
/* :catch_0 */
/* move-exception v1 */
/* .line 238 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v2 = "NetworkAccelerateSwitchService"; // const-string v2, "NetworkAccelerateSwitchService"
final String v3 = "destroyInstance onDestroy catch:"; // const-string v3, "destroyInstance onDestroy catch:"
android.util.Log .e ( v2,v3,v1 );
/* .line 240 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
/* .line 242 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 244 */
} // :cond_1
} // :goto_1
return;
} // .end method
private void disabelNetworkSwitch ( ) {
/* .locals 3 */
/* .line 1184 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "disabelNetworkSwitch mSendBroadcast:"; // const-string v1, "disabelNetworkSwitch mSendBroadcast:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mSendBroadcast:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " misEvevator:"; // const-string v1, " misEvevator:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misEvevator:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " misHighRtt:"; // const-string v1, " misHighRtt:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misHighRtt:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkAccelerateSwitchService"; // const-string v1, "NetworkAccelerateSwitchService"
android.util.Log .d ( v1,v0 );
/* .line 1188 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mSendBroadcast:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misEvevator:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misHighRtt:Z */
/* if-nez v0, :cond_0 */
/* .line 1189 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1190 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.xiaomi.NetworkBoost.NetworkAccelerateSwitchService.action.enableNetworkSwitch"; // const-string v1, "com.xiaomi.NetworkBoost.NetworkAccelerateSwitchService.action.enableNetworkSwitch"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1191 */
final String v1 = "networkboost_sorce"; // const-string v1, "networkboost_sorce"
int v2 = 0; // const/4 v2, 0x0
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1192 */
final String v1 = "networkboot_rssi_increase"; // const-string v1, "networkboot_rssi_increase"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1193 */
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1194 */
v1 = this.mContext;
(( android.content.Context ) v1 ).sendBroadcast ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 1195 */
/* iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mSendBroadcast:Z */
/* .line 1197 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_0
return;
} // .end method
private void dumpIntegerQueue ( java.io.PrintWriter p0, java.lang.String p1, java.lang.String p2, java.util.Queue p3 ) {
/* .locals 5 */
/* .param p1, "writer" # Ljava/io/PrintWriter; */
/* .param p2, "spacePrefix" # Ljava/lang/String; */
/* .param p3, "name" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/io/PrintWriter;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Queue<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1309 */
/* .local p4, "dumpQueue":Ljava/util/Queue;, "Ljava/util/Queue<Ljava/lang/Integer;>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 1310 */
/* .local v0, "dump_cnt":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ":"; // const-string v2, ":"
v2 = (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1311 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1312 */
v2 = } // :goto_0
final String v3 = ""; // const-string v3, ""
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 1313 */
/* .local v2, "item":I */
/* rem-int/lit8 v4, v0, 0x5 */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1314 */
final String v4 = ", "; // const-string v4, ", "
(( java.io.PrintWriter ) p1 ).print ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1316 */
} // :cond_0
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1317 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1318 */
/* const/16 v4, 0x32 */
/* if-lt v0, v4, :cond_1 */
/* .line 1319 */
/* .line 1321 */
} // :cond_1
/* rem-int/lit8 v3, v0, 0x5 */
/* if-nez v3, :cond_2 */
/* .line 1322 */
final String v3 = ","; // const-string v3, ","
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1323 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1325 */
} // .end local v2 # "item":I
} // :cond_2
/* .line 1326 */
} // :cond_3
} // :goto_1
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1327 */
return;
} // .end method
private void enabelNetworkSwitch ( ) {
/* .locals 4 */
/* .line 1168 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "enabelNetworkSwitch mSendBroadcast:"; // const-string v1, "enabelNetworkSwitch mSendBroadcast:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mSendBroadcast:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " misEvevator:"; // const-string v1, " misEvevator:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misEvevator:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " misHighRtt:"; // const-string v1, " misHighRtt:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misHighRtt:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkAccelerateSwitchService"; // const-string v1, "NetworkAccelerateSwitchService"
android.util.Log .d ( v1,v0 );
/* .line 1172 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mSendBroadcast:Z */
/* if-nez v0, :cond_2 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misEvevator:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misHighRtt:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1173 */
} // :cond_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1174 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.xiaomi.NetworkBoost.NetworkAccelerateSwitchService.action.enableNetworkSwitch"; // const-string v1, "com.xiaomi.NetworkBoost.NetworkAccelerateSwitchService.action.enableNetworkSwitch"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1175 */
final String v1 = "networkboost_sorce"; // const-string v1, "networkboost_sorce"
int v2 = 1; // const/4 v2, 0x1
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1176 */
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misHighRtt:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 3; // const/4 v1, 0x3
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
final String v3 = "networkboot_rssi_increase"; // const-string v3, "networkboot_rssi_increase"
(( android.content.Intent ) v0 ).putExtra ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1177 */
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1178 */
v1 = this.mContext;
(( android.content.Context ) v1 ).sendBroadcast ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 1179 */
/* iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mSendBroadcast:Z */
/* .line 1181 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_2
return;
} // .end method
private Integer getAverageWifiRssi ( ) {
/* .locals 7 */
/* .line 806 */
int v0 = 0; // const/4 v0, 0x0
/* .line 807 */
/* .local v0, "wifiInfo":Landroid/net/wifi/WifiInfo; */
int v1 = 0; // const/4 v1, 0x0
/* .line 808 */
/* .local v1, "rssi":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 809 */
/* .local v2, "longSum":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 810 */
/* .local v3, "sortSum":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 811 */
/* .local v4, "averageRssi":I */
v5 = this.mWifiManager;
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 812 */
(( android.net.wifi.WifiManager ) v5 ).getConnectionInfo ( ); // invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;
/* .line 813 */
v5 = (( android.net.wifi.WifiInfo ) v0 ).getRssi ( ); // invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getRssi()I
v1 = java.lang.Math .abs ( v5 );
/* .line 816 */
} // :cond_0
v5 = v5 = this.mShortRssiQueue;
int v6 = 3; // const/4 v6, 0x3
/* if-lt v5, v6, :cond_1 */
/* .line 817 */
v5 = this.mShortRssiQueue;
/* .line 819 */
} // :cond_1
v5 = this.mShortRssiQueue;
java.lang.Integer .valueOf ( v1 );
/* .line 821 */
v5 = v5 = this.mLongRssiQueue;
int v6 = 7; // const/4 v6, 0x7
/* if-lt v5, v6, :cond_2 */
/* .line 822 */
v5 = this.mLongRssiQueue;
/* .line 824 */
} // :cond_2
v5 = this.mLongRssiQueue;
java.lang.Integer .valueOf ( v1 );
/* .line 826 */
v5 = this.mShortRssiQueue;
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_3
/* check-cast v6, Ljava/lang/Integer; */
v6 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
/* .line 827 */
/* .local v6, "a":I */
/* add-int/2addr v3, v6 */
/* .line 828 */
} // .end local v6 # "a":I
/* .line 830 */
} // :cond_3
v5 = this.mLongRssiQueue;
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_4
/* check-cast v6, Ljava/lang/Integer; */
v6 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
/* .line 831 */
/* .restart local v6 # "a":I */
/* add-int/2addr v2, v6 */
/* .line 832 */
} // .end local v6 # "a":I
/* .line 834 */
} // :cond_4
v5 = v5 = this.mShortRssiQueue;
/* div-int v5, v3, v5 */
/* mul-int/lit8 v5, v5, 0x4b */
v6 = v6 = this.mLongRssiQueue;
/* div-int v6, v2, v6 */
/* mul-int/lit8 v6, v6, 0x19 */
/* add-int/2addr v5, v6 */
/* .line 835 */
} // .end local v4 # "averageRssi":I
/* .local v5, "averageRssi":I */
/* div-int/lit8 v5, v5, 0x64 */
/* .line 837 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "getAverageWifiRssi:"; // const-string v6, "getAverageWifiRssi:"
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "NetworkAccelerateSwitchService"; // const-string v6, "NetworkAccelerateSwitchService"
android.util.Log .i ( v6,v4 );
/* .line 838 */
/* iput v5, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mAverageRssi:I */
/* .line 839 */
} // .end method
public static com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService getInstance ( android.content.Context p0, vendor.xiaomi.hidl.minet.V1_0.IMiNetCallback p1 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "minetcallback" # Lvendor/xiaomi/hidl/minet/V1_0/IMiNetCallback; */
/* .line 214 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.sInstance;
/* if-nez v0, :cond_1 */
/* .line 215 */
/* const-class v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService; */
/* monitor-enter v0 */
/* .line 216 */
try { // :try_start_0
v1 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.sInstance;
/* if-nez v1, :cond_0 */
/* .line 217 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService; */
/* invoke-direct {v1, p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;-><init>(Landroid/content/Context;Lvendor/xiaomi/hidl/minet/V1_0/IMiNetCallback;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 219 */
try { // :try_start_1
v1 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.sInstance;
(( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService ) v1 ).onCreate ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->onCreate()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 223 */
/* .line 221 */
/* :catch_0 */
/* move-exception v1 */
/* .line 222 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v2 = "NetworkAccelerateSwitchService"; // const-string v2, "NetworkAccelerateSwitchService"
final String v3 = "getInstance onCreate catch:"; // const-string v3, "getInstance onCreate catch:"
android.util.Log .e ( v2,v3,v1 );
/* .line 225 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 227 */
} // :cond_1
} // :goto_1
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.sInstance;
} // .end method
private Integer getMovementThreshold ( ) {
/* .locals 1 */
/* .line 1298 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mis24GHz:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1299 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement24GHzThreshold:I */
/* .line 1301 */
} // :cond_0
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement5GHzThreshold:I */
} // .end method
private native Integer initNetworkAccelerateSwitch ( ) {
} // .end method
private static Boolean is24GHz ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "freqMhz" # I */
/* .line 916 */
/* const/16 v0, 0x96c */
/* if-lt p0, v0, :cond_0 */
/* const/16 v0, 0x9b4 */
/* if-gt p0, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isEnableAccelerationSwitch ( ) {
/* .locals 7 */
/* .line 437 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "wifi_assistant" */
int v2 = 1; // const/4 v2, 0x1
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
int v1 = 0; // const/4 v1, 0x0
/* if-ne v0, v2, :cond_0 */
/* move v0, v2 */
} // :cond_0
/* move v0, v1 */
/* .line 439 */
/* .local v0, "isWifiAssistEnable":Z */
} // :goto_0
/* if-nez v0, :cond_1 */
/* .line 440 */
/* .line 443 */
} // :cond_1
v3 = this.mContext;
/* .line 444 */
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 443 */
final String v4 = "cloud_weak_network_switch_enabled"; // const-string v4, "cloud_weak_network_switch_enabled"
int v5 = -2; // const/4 v5, -0x2
android.provider.Settings$System .getStringForUser ( v3,v4,v5 );
/* .line 445 */
/* .local v3, "cvalue":Ljava/lang/String; */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "linkturbo_is_enable"; // const-string v6, "linkturbo_is_enable"
v4 = android.provider.Settings$System .getIntForUser ( v4,v6,v1,v5 );
/* if-ne v4, v2, :cond_2 */
/* move v4, v2 */
} // :cond_2
/* move v4, v1 */
/* .line 447 */
/* .local v4, "enable":Z */
} // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_4
/* const-string/jumbo v5, "v2" */
v5 = (( java.lang.String ) v5 ).equals ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_4
/* if-nez v4, :cond_4 */
/* sget-boolean v5, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mSlaveWifiReady:Z */
/* if-nez v5, :cond_3 */
/* sget-boolean v6, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mWifiReady:Z */
/* if-nez v6, :cond_5 */
} // :cond_3
if ( v5 != null) { // if-eqz v5, :cond_4
/* sget-boolean v5, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mWifiReady:Z */
/* if-nez v5, :cond_4 */
} // :cond_4
/* move v2, v1 */
} // :cond_5
} // :goto_2
} // .end method
private Boolean isEnableAccelerationSwitchHighSpeedMode ( ) {
/* .locals 3 */
/* .line 1123 */
v0 = this.mContext;
/* .line 1124 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1123 */
final String v1 = "cloud_weak_network_switch_high_speed_mode"; // const-string v1, "cloud_weak_network_switch_high_speed_mode"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 1125 */
/* .local v0, "cvalue":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* const-string/jumbo v1, "v1" */
v1 = (( java.lang.String ) v0 ).indexOf ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
int v2 = -1; // const/4 v2, -0x1
/* if-eq v1, v2, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private native java.lang.String nativeDump ( ) {
} // .end method
private void registerAppStatusListener ( ) {
/* .locals 3 */
/* .line 565 */
try { // :try_start_0
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 566 */
v1 = this.mAppStatusListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerAppStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerAppStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 569 */
/* .line 567 */
/* :catch_0 */
/* move-exception v0 */
/* .line 568 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkAccelerateSwitchService"; // const-string v2, "NetworkAccelerateSwitchService"
android.util.Log .e ( v2,v1 );
/* .line 570 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void registerDefaultNetworkCallback ( ) {
/* .locals 3 */
/* .line 640 */
try { // :try_start_0
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 641 */
v1 = this.mDefaultNetworkInterfaceListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerDefaultNetworkInterfaceListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerDefaultNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 644 */
/* .line 642 */
/* :catch_0 */
/* move-exception v0 */
/* .line 643 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkAccelerateSwitchService"; // const-string v2, "NetworkAccelerateSwitchService"
android.util.Log .e ( v2,v1 );
/* .line 645 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void registerElevatorStatusReceiver ( ) {
/* .locals 3 */
/* .line 1129 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$7;-><init>(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V */
this.mElevatorStatusReceiver = v0;
/* .line 1153 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 1154 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "com.android.phone.intent.action.ELEVATOR_STATE_SCENE"; // const-string v1, "com.android.phone.intent.action.ELEVATOR_STATE_SCENE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1155 */
v1 = this.mContext;
v2 = this.mElevatorStatusReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 1156 */
return;
} // .end method
private void registerHighSpeedModeChangedObserver ( ) {
/* .locals 5 */
/* .line 1074 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$6; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$6;-><init>(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;Landroid/os/Handler;)V */
this.mMovementObserver = v0;
/* .line 1080 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1081 */
final String v1 = "cloud_weak_network_switch_high_speed_mode"; // const-string v1, "cloud_weak_network_switch_high_speed_mode"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mMovementObserver;
/* .line 1080 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1084 */
v0 = this.mMovementObserver;
(( android.database.ContentObserver ) v0 ).onChange ( v3 ); // invoke-virtual {v0, v3}, Landroid/database/ContentObserver;->onChange(Z)V
/* .line 1085 */
return;
} // .end method
private void registerMovementSensorStatusListener ( ) {
/* .locals 3 */
/* .line 1206 */
try { // :try_start_0
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 1207 */
v1 = this.mMovementSensorStatusListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerMovementSensorStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerMovementSensorStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;)V
/* .line 1209 */
v0 = this.mContext;
final String v1 = "MiuiWifiService"; // const-string v1, "MiuiWifiService"
/* .line 1210 */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/MiuiWifiManager; */
this.mMiuiWifiManager = v0;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1213 */
/* .line 1211 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1212 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkAccelerateSwitchService"; // const-string v2, "NetworkAccelerateSwitchService"
android.util.Log .e ( v2,v1 );
/* .line 1214 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void registerNetworkCallback ( ) {
/* .locals 3 */
/* .line 588 */
try { // :try_start_0
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 589 */
v1 = this.mNetworkInterfaceListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerNetworkInterfaceListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 592 */
/* .line 590 */
/* :catch_0 */
/* move-exception v0 */
/* .line 591 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkAccelerateSwitchService"; // const-string v2, "NetworkAccelerateSwitchService"
android.util.Log .e ( v2,v1 );
/* .line 593 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void registerNetworkSDK ( ) {
/* .locals 3 */
/* .line 324 */
try { // :try_start_0
v0 = this.mContext;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .getInstance ( v0 );
this.mNetworkSDKService = v0;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 327 */
/* .line 325 */
/* :catch_0 */
/* move-exception v0 */
/* .line 326 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkAccelerateSwitchService"; // const-string v2, "NetworkAccelerateSwitchService"
android.util.Log .e ( v2,v1 );
/* .line 328 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void registerNetworkSwitchModeChangedObserver ( ) {
/* .locals 5 */
/* .line 455 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$1; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$1;-><init>(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;Landroid/os/Handler;)V */
this.mObserver = v0;
/* .line 464 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 465 */
final String v1 = "cloud_weak_network_switch_enabled"; // const-string v1, "cloud_weak_network_switch_enabled"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mObserver;
/* .line 464 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 467 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 468 */
/* const-string/jumbo v1, "wifi_assistant" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mObserver;
/* .line 467 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 470 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 471 */
final String v1 = "linkturbo_is_enable"; // const-string v1, "linkturbo_is_enable"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mObserver;
/* .line 470 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 474 */
v0 = this.mObserver;
(( android.database.ContentObserver ) v0 ).onChange ( v3 ); // invoke-virtual {v0, v3}, Landroid/database/ContentObserver;->onChange(Z)V
/* .line 475 */
return;
} // .end method
private void registerScreenStatusListener ( ) {
/* .locals 3 */
/* .line 684 */
try { // :try_start_0
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 685 */
v1 = this.mScreenStatusListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerScreenStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 688 */
/* .line 686 */
/* :catch_0 */
/* move-exception v0 */
/* .line 687 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkAccelerateSwitchService"; // const-string v2, "NetworkAccelerateSwitchService"
android.util.Log .e ( v2,v1 );
/* .line 689 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void restoreMovementState ( ) {
/* .locals 2 */
/* .line 1291 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1292 */
v0 = this.mHandler;
/* const/16 v1, 0x6e */
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 1293 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mMovement:Z */
/* .line 1295 */
} // :cond_0
return;
} // .end method
private void setNetworkAccelerateSwitchAppStart ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 510 */
v0 = this.mHandler;
/* const/16 v1, 0x69 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 511 */
v0 = this.mHandler;
/* const/16 v2, 0x6a */
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 512 */
v0 = this.mHandler;
/* .line 513 */
java.lang.Integer .valueOf ( p1 );
/* .line 512 */
(( android.os.Handler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* const-wide/16 v2, 0x1388 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 514 */
return;
} // .end method
private void setNetworkAccelerateSwitchAppStop ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 517 */
v0 = this.mHandler;
/* const/16 v1, 0x69 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 518 */
v0 = this.mHandler;
/* const/16 v1, 0x6a */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 519 */
v0 = this.mHandler;
/* .line 520 */
java.lang.Integer .valueOf ( p1 );
/* .line 519 */
(( android.os.Handler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* const-wide/16 v2, 0x1388 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 521 */
return;
} // .end method
public static void setNetworkAccelerateSwitchUidList ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "uidList" # Ljava/lang/String; */
/* .line 492 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setNetworkAccelerateSwitchUidList:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkAccelerateSwitchService"; // const-string v1, "NetworkAccelerateSwitchService"
android.util.Log .i ( v1,v0 );
/* .line 493 */
/* if-nez p0, :cond_0 */
/* .line 494 */
return;
/* .line 496 */
} // :cond_0
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mEffectAppUidList;
(( java.util.HashSet ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
/* .line 498 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p0 ).split ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 499 */
/* .local v0, "temp":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_1 */
/* .line 500 */
v2 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mEffectAppUidList;
/* aget-object v3, v0, v1 */
(( java.util.HashSet ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 499 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 502 */
} // .end local v1 # "i":I
} // :cond_1
return;
} // .end method
public static void setNetworkAccelerateSwitchVoIPUid ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "uid" # I */
/* .line 505 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setNetworkAccelerateSwitchVoIPUid:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkAccelerateSwitchService"; // const-string v1, "NetworkAccelerateSwitchService"
android.util.Log .i ( v1,v0 );
/* .line 506 */
/* .line 507 */
return;
} // .end method
private native Integer setRttTarget ( java.lang.String p0, Integer p1, java.lang.String p2, Integer p3, Integer p4 ) {
} // .end method
private Boolean startCollectIpAddress ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 334 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "startCollectIpAddress " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkAccelerateSwitchService"; // const-string v1, "NetworkAccelerateSwitchService"
android.util.Log .i ( v1,v0 );
/* .line 335 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mMiNetService;
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mMiNetCallback;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 337 */
try { // :try_start_0
java.lang.Integer .toString ( p1 );
/* const/16 v3, 0x3eb */
/* .line 338 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mMiNetService;
java.lang.Integer .toString ( p1 );
/* const/16 v3, 0x3ed */
/* .line 339 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mMiNetService;
v2 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mMiNetCallback;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 342 */
/* .line 340 */
/* :catch_0 */
/* move-exception v0 */
/* .line 341 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Exception:"; // const-string v3, "Exception:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 344 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private native Integer startRttCollection ( java.lang.String p0 ) {
} // .end method
private Boolean stopCollectIpAddress ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 348 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "stopCollectIpAddress " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkAccelerateSwitchService"; // const-string v1, "NetworkAccelerateSwitchService"
android.util.Log .i ( v1,v0 );
/* .line 349 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mMiNetService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 351 */
try { // :try_start_0
java.lang.Integer .toString ( p1 );
/* const/16 v3, 0x3ec */
/* .line 352 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mMiNetService;
v2 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mMiNetCallback;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 355 */
/* .line 353 */
/* :catch_0 */
/* move-exception v0 */
/* .line 354 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Exception:"; // const-string v3, "Exception:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 357 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private native Integer stopRttCollection ( ) {
} // .end method
private void unregisterAppStatusListener ( ) {
/* .locals 3 */
/* .line 574 */
try { // :try_start_0
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 575 */
v1 = this.mAppStatusListener;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 576 */
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterAppStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterAppStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 580 */
} // :cond_0
/* .line 578 */
/* :catch_0 */
/* move-exception v0 */
/* .line 579 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkAccelerateSwitchService"; // const-string v2, "NetworkAccelerateSwitchService"
android.util.Log .e ( v2,v1 );
/* .line 581 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unregisterDefaultNetworkCallback ( ) {
/* .locals 3 */
/* .line 649 */
try { // :try_start_0
v0 = this.mDefaultNetworkInterfaceListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 650 */
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 651 */
v1 = this.mDefaultNetworkInterfaceListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterDefaultNetworkInterfaceListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterDefaultNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 655 */
} // :cond_0
/* .line 653 */
/* :catch_0 */
/* move-exception v0 */
/* .line 654 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkAccelerateSwitchService"; // const-string v2, "NetworkAccelerateSwitchService"
android.util.Log .e ( v2,v1 );
/* .line 656 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unregisterElevatorStatusReceiver ( ) {
/* .locals 2 */
/* .line 1159 */
v0 = this.mElevatorStatusReceiver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1160 */
v1 = this.mContext;
(( android.content.Context ) v1 ).unregisterReceiver ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 1161 */
final String v0 = "NetworkAccelerateSwitchService"; // const-string v0, "NetworkAccelerateSwitchService"
/* const-string/jumbo v1, "unregisterElevatorStatusReceiver" */
android.util.Log .d ( v0,v1 );
/* .line 1163 */
} // :cond_0
return;
} // .end method
private void unregisterHighSpeedModeChangedObserver ( ) {
/* .locals 2 */
/* .line 1088 */
v0 = this.mMovementObserver;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mContext;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1089 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.mMovementObserver;
(( android.content.ContentResolver ) v0 ).unregisterContentObserver ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
/* .line 1090 */
int v0 = 0; // const/4 v0, 0x0
this.mMovementObserver = v0;
/* .line 1093 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "deinitCloudObserver ignore because: observer: "; // const-string v1, "deinitCloudObserver ignore because: observer: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mMovementObserver;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = " context: "; // const-string v1, " context: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mContext;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkAccelerateSwitchService"; // const-string v1, "NetworkAccelerateSwitchService"
android.util.Log .w ( v1,v0 );
/* .line 1095 */
} // :goto_0
return;
} // .end method
private void unregisterMovementSensorStatusListener ( ) {
/* .locals 3 */
/* .line 1218 */
try { // :try_start_0
v0 = this.mMovementSensorStatusListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1219 */
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 1220 */
v1 = this.mMovementSensorStatusListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterMovementSensorStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterMovementSensorStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1224 */
} // :cond_0
/* .line 1222 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1223 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkAccelerateSwitchService"; // const-string v2, "NetworkAccelerateSwitchService"
android.util.Log .e ( v2,v1 );
/* .line 1225 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unregisterNetworkCallback ( ) {
/* .locals 3 */
/* .line 597 */
try { // :try_start_0
v0 = this.mNetworkInterfaceListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 598 */
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 599 */
v1 = this.mNetworkInterfaceListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterNetworkInterfaceListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 603 */
} // :cond_0
/* .line 601 */
/* :catch_0 */
/* move-exception v0 */
/* .line 602 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkAccelerateSwitchService"; // const-string v2, "NetworkAccelerateSwitchService"
android.util.Log .e ( v2,v1 );
/* .line 604 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unregisterNetworkSwitchModeChangedObserver ( ) {
/* .locals 2 */
/* .line 478 */
v0 = this.mObserver;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mContext;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 479 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.mObserver;
(( android.content.ContentResolver ) v0 ).unregisterContentObserver ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
/* .line 480 */
int v0 = 0; // const/4 v0, 0x0
this.mObserver = v0;
/* .line 483 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "deinitCloudObserver ignore because: observer: "; // const-string v1, "deinitCloudObserver ignore because: observer: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mObserver;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = " context: "; // const-string v1, " context: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mContext;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkAccelerateSwitchService"; // const-string v1, "NetworkAccelerateSwitchService"
android.util.Log .w ( v1,v0 );
/* .line 485 */
} // :goto_0
return;
} // .end method
private void unregisterScreenStatusListener ( ) {
/* .locals 3 */
/* .line 693 */
try { // :try_start_0
v0 = this.mScreenStatusListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 694 */
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 695 */
v1 = this.mScreenStatusListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterScreenStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 699 */
} // :cond_0
/* .line 697 */
/* :catch_0 */
/* move-exception v0 */
/* .line 698 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkAccelerateSwitchService"; // const-string v2, "NetworkAccelerateSwitchService"
android.util.Log .e ( v2,v1 );
/* .line 700 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean checkMovement ( java.util.Queue p0 ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Queue<", */
/* "Ljava/lang/Integer;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 1264 */
/* .local p1, "rssiQueue":Ljava/util/Queue;, "Ljava/util/Queue<Ljava/lang/Integer;>;" */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "rssiChange":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 1265 */
/* .local v1, "rssi":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 1266 */
v3 = /* .local v2, "movement":Z */
int v4 = 3; // const/4 v4, 0x3
/* if-ge v3, v4, :cond_0 */
/* .line 1267 */
int v3 = 0; // const/4 v3, 0x0
/* .line 1270 */
} // :cond_0
v4 = } // :goto_0
final String v5 = "NetworkAccelerateSwitchService"; // const-string v5, "NetworkAccelerateSwitchService"
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 1271 */
/* .local v4, "a":I */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "checkMovement:"; // const-string v7, "checkMovement:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v5,v6 );
/* .line 1272 */
/* if-nez v1, :cond_1 */
/* .line 1273 */
/* move v1, v4 */
/* .line 1275 */
} // :cond_1
/* if-ge v4, v1, :cond_2 */
/* .line 1276 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1277 */
/* .line 1279 */
} // :cond_2
/* sub-int v5, v4, v1 */
/* add-int/2addr v5, v0 */
/* .line 1280 */
} // .end local v0 # "rssiChange":I
/* .local v5, "rssiChange":I */
/* move v0, v4 */
/* move v1, v0 */
/* move v0, v5 */
/* .line 1282 */
} // .end local v4 # "a":I
} // .end local v5 # "rssiChange":I
/* .restart local v0 # "rssiChange":I */
} // :goto_1
/* .line 1283 */
} // :cond_3
} // :goto_2
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "checkMovement rssiChange:"; // const-string v4, "checkMovement rssiChange:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " getMovementThreshold:"; // const-string v4, " getMovementThreshold:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->getMovementThreshold()I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v3 );
/* .line 1284 */
v3 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->getMovementThreshold()I */
/* if-le v0, v3, :cond_4 */
/* .line 1285 */
int v2 = 1; // const/4 v2, 0x1
/* .line 1287 */
} // :cond_4
} // .end method
public void dumpModule ( java.io.PrintWriter p0 ) {
/* .locals 7 */
/* .param p1, "writer" # Ljava/io/PrintWriter; */
/* .line 1330 */
final String v0 = "NetworkAccelerateSwitchService end.\n"; // const-string v0, "NetworkAccelerateSwitchService end.\n"
final String v1 = " "; // const-string v1, " "
/* .line 1332 */
/* .local v1, "spacePrefix":Ljava/lang/String; */
try { // :try_start_0
final String v2 = "NetworkAccelerateSwitchService begin:"; // const-string v2, "NetworkAccelerateSwitchService begin:"
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1333 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mWifiReady:"; // const-string v3, "mWifiReady:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mWifiReady:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1334 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mSlaveWifiReady:"; // const-string v3, "mSlaveWifiReady:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mSlaveWifiReady:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1335 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mRssiInRange:"; // const-string v3, "mRssiInRange:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mRssiInRange:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1336 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mInterface:"; // const-string v3, "mInterface:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mInterface;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1337 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mIfaceNumber:"; // const-string v3, "mIfaceNumber:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1338 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mScreenon:"; // const-string v3, "mScreenon:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mScreenon:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1339 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "DUMP_SIZE_UPBOUND:"; // const-string v3, "DUMP_SIZE_UPBOUND:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v3, 0x32 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1340 */
int v2 = 0; // const/4 v2, 0x0
/* .line 1341 */
/* .local v2, "dump_cnt":I */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "mEffectAppUidList:"; // const-string v5, "mEffectAppUidList:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mEffectAppUidList;
v5 = (( java.util.HashSet ) v5 ).size ( ); // invoke-virtual {v5}, Ljava/util/HashSet;->size()I
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1342 */
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1343 */
v4 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mEffectAppUidList;
(( java.util.HashSet ) v4 ).iterator ( ); // invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_3
/* check-cast v5, Ljava/lang/String; */
/* .line 1344 */
/* .local v5, "str_uid":Ljava/lang/String; */
/* rem-int/lit8 v6, v2, 0x5 */
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 1345 */
final String v6 = ", "; // const-string v6, ", "
(( java.io.PrintWriter ) p1 ).print ( v6 ); // invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1347 */
} // :cond_0
(( java.io.PrintWriter ) p1 ).print ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1348 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1349 */
/* if-lt v2, v3, :cond_1 */
/* .line 1350 */
/* .line 1352 */
} // :cond_1
/* rem-int/lit8 v6, v2, 0x5 */
/* if-nez v6, :cond_2 */
/* .line 1353 */
final String v6 = ","; // const-string v6, ","
(( java.io.PrintWriter ) p1 ).println ( v6 ); // invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1354 */
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1356 */
} // .end local v5 # "str_uid":Ljava/lang/String;
} // :cond_2
/* .line 1357 */
} // :cond_3
} // :goto_1
final String v3 = ""; // const-string v3, ""
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1358 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "mEffectVoIPUid:"; // const-string v4, "mEffectVoIPUid:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1359 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "mEffectForegroundUid:"; // const-string v4, "mEffectForegroundUid:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1360 */
final String v3 = "mShortRssiQueue"; // const-string v3, "mShortRssiQueue"
v4 = this.mShortRssiQueue;
/* invoke-direct {p0, p1, v1, v3, v4}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->dumpIntegerQueue(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/Queue;)V */
/* .line 1361 */
final String v3 = "mLongRssiQueue"; // const-string v3, "mLongRssiQueue"
v4 = this.mLongRssiQueue;
/* invoke-direct {p0, p1, v1, v3, v4}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->dumpIntegerQueue(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/Queue;)V */
/* .line 1362 */
final String v3 = "mShortUnmeteredRttQueue"; // const-string v3, "mShortUnmeteredRttQueue"
v4 = this.mShortUnmeteredRttQueue;
/* invoke-direct {p0, p1, v1, v3, v4}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->dumpIntegerQueue(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/Queue;)V */
/* .line 1363 */
final String v3 = "mLongUnmeteredRttQueue"; // const-string v3, "mLongUnmeteredRttQueue"
v4 = this.mLongUnmeteredRttQueue;
/* invoke-direct {p0, p1, v1, v3, v4}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->dumpIntegerQueue(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/Queue;)V */
/* .line 1364 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "mAverageRssi:"; // const-string v4, "mAverageRssi:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mAverageRssi:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1365 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "UDP_BLACK_LIST:"; // const-string v4, "UDP_BLACK_LIST:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "53"; // const-string v4, "53"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1366 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "TCP_BLACK_LIST:"; // const-string v4, "TCP_BLACK_LIST:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "8081"; // const-string v4, "8081"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1367 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->nativeDump()Ljava/lang/String; */
/* .line 1368 */
/* .local v3, "nativeDumpStr":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_4
/* move-object v4, v3 */
} // :cond_4
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "native dump is null"; // const-string v5, "native dump is null"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :goto_2
(( java.io.PrintWriter ) p1 ).println ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1369 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1380 */
} // .end local v2 # "dump_cnt":I
} // .end local v3 # "nativeDumpStr":Ljava/lang/String;
/* .line 1371 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1372 */
/* .local v2, "ex":Ljava/lang/Exception; */
final String v3 = "dump failed!"; // const-string v3, "dump failed!"
final String v4 = "NetworkAccelerateSwitchService"; // const-string v4, "NetworkAccelerateSwitchService"
android.util.Log .e ( v4,v3,v2 );
/* .line 1374 */
try { // :try_start_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Dump of NetworkAccelerateSwitchService failed:"; // const-string v5, "Dump of NetworkAccelerateSwitchService failed:"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1375 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 1379 */
/* .line 1377 */
/* :catch_1 */
/* move-exception v0 */
/* .line 1378 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "dump failure failed:"; // const-string v5, "dump failure failed:"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v4,v3 );
/* .line 1381 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end local v2 # "ex":Ljava/lang/Exception;
} // :goto_3
return;
} // .end method
public Boolean notifyCollectIpAddress ( java.lang.String p0 ) {
/* .locals 16 */
/* .param p1, "attr" # Ljava/lang/String; */
/* .line 362 */
int v0 = 0; // const/4 v0, 0x0
/* .line 363 */
/* .local v0, "uid":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .line 364 */
/* .local v1, "tcp_ipaddress":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 365 */
/* .local v2, "tcp_port":Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .line 366 */
/* .local v3, "udp_ipaddress":Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .line 367 */
/* .local v4, "udp_port":Ljava/lang/String; */
final String v5 = "\\/"; // const-string v5, "\\/"
/* move-object/from16 v6, p1 */
(( java.lang.String ) v6 ).split ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 368 */
/* .local v5, "buff":[Ljava/lang/String; */
int v7 = 0; // const/4 v7, 0x0
/* move-object v13, v4 */
/* move-object v4, v3 */
/* move-object v3, v2 */
/* move-object v2, v1 */
/* move-object v1, v0 */
} // .end local v0 # "uid":Ljava/lang/String;
/* .local v1, "uid":Ljava/lang/String; */
/* .local v2, "tcp_ipaddress":Ljava/lang/String; */
/* .local v3, "tcp_port":Ljava/lang/String; */
/* .local v4, "udp_ipaddress":Ljava/lang/String; */
/* .local v7, "i":I */
/* .local v13, "udp_port":Ljava/lang/String; */
} // :goto_0
/* array-length v0, v5 */
int v8 = 0; // const/4 v8, 0x0
int v14 = 1; // const/4 v14, 0x1
/* if-ge v7, v0, :cond_2 */
/* .line 369 */
/* if-nez v7, :cond_0 */
/* .line 370 */
/* aget-object v0, v5, v8 */
/* move-object v1, v0 */
} // .end local v1 # "uid":Ljava/lang/String;
/* .restart local v0 # "uid":Ljava/lang/String; */
/* .line 372 */
} // .end local v0 # "uid":Ljava/lang/String;
/* .restart local v1 # "uid":Ljava/lang/String; */
} // :cond_0
/* aget-object v0, v5, v7 */
final String v9 = "\\,"; // const-string v9, "\\,"
(( java.lang.String ) v0 ).split ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 373 */
/* .local v0, "tmp":[Ljava/lang/String; */
/* aget-object v8, v0, v8 */
final String v9 = "6"; // const-string v9, "6"
v8 = (( java.lang.String ) v8 ).equals ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v9 = 2; // const/4 v9, 0x2
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 374 */
/* aget-object v2, v0, v14 */
/* .line 375 */
/* aget-object v3, v0, v9 */
/* .line 377 */
} // :cond_1
/* aget-object v4, v0, v14 */
/* .line 378 */
/* aget-object v8, v0, v9 */
/* move-object v13, v8 */
/* .line 368 */
} // .end local v0 # "tmp":[Ljava/lang/String;
} // :goto_1
/* add-int/lit8 v7, v7, 0x1 */
/* .line 386 */
} // .end local v7 # "i":I
} // :cond_2
if ( v1 != null) { // if-eqz v1, :cond_8
java.lang.Integer .toString ( v0 );
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_3 */
/* goto/16 :goto_5 */
/* .line 401 */
} // :cond_3
int v7 = -1; // const/4 v7, -0x1
/* .line 402 */
/* .local v7, "tcp_port_value":I */
int v8 = -1; // const/4 v8, -0x1
/* .line 403 */
/* .local v8, "udp_port_value":I */
final String v9 = "notifyCollectIpAddress tcp_port_value catch:"; // const-string v9, "notifyCollectIpAddress tcp_port_value catch:"
final String v10 = "NetworkAccelerateSwitchService"; // const-string v10, "NetworkAccelerateSwitchService"
if ( v3 != null) { // if-eqz v3, :cond_4
final String v0 = "8081"; // const-string v0, "8081"
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_4 */
/* .line 405 */
try { // :try_start_0
v0 = java.lang.Integer .parseInt ( v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v7, v0 */
/* .line 409 */
/* move v15, v7 */
/* .line 407 */
/* :catch_0 */
/* move-exception v0 */
/* move-object v11, v0 */
/* move-object v0, v11 */
/* .line 408 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v11 ).append ( v9 ); // invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v0 ); // invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v10,v11 );
/* .line 411 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_4
/* move v15, v7 */
} // .end local v7 # "tcp_port_value":I
/* .local v15, "tcp_port_value":I */
} // :goto_2
if ( v13 != null) { // if-eqz v13, :cond_5
final String v0 = "53"; // const-string v0, "53"
v0 = (( java.lang.String ) v0 ).equals ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_5 */
/* .line 413 */
try { // :try_start_1
v0 = java.lang.Integer .parseInt ( v13 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* move v8, v0 */
/* .line 417 */
/* .line 415 */
/* :catch_1 */
/* move-exception v0 */
/* move-object v7, v0 */
/* move-object v0, v7 */
/* .line 416 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v10,v7 );
/* .line 420 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_5
/* move v0, v8 */
} // .end local v8 # "udp_port_value":I
/* .local v0, "udp_port_value":I */
} // :goto_3
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "tcp_ipaddress:" */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " tcp_port_value:"; // const-string v8, " tcp_port_value:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v15 ); // invoke-virtual {v7, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " udp_ipaddress:"; // const-string v8, " udp_ipaddress:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " udp_port_value:"; // const-string v8, " udp_port_value:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v10,v7 );
/* .line 422 */
java.lang.Integer .toString ( v7 );
v7 = (( java.lang.String ) v1 ).equals ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v8 = -1; // const/4 v8, -0x1
if ( v7 != null) { // if-eqz v7, :cond_6
if ( v4 != null) { // if-eqz v4, :cond_6
/* if-eq v0, v8, :cond_6 */
/* .line 423 */
int v12 = 1; // const/4 v12, 0x1
/* move-object/from16 v7, p0 */
/* move-object v8, v2 */
/* move v9, v15 */
/* move-object v10, v4 */
/* move v11, v0 */
/* invoke-direct/range {v7 ..v12}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->setRttTarget(Ljava/lang/String;ILjava/lang/String;II)I */
/* .line 426 */
} // :cond_6
if ( v2 != null) { // if-eqz v2, :cond_7
/* if-eq v15, v8, :cond_7 */
/* .line 427 */
int v12 = 0; // const/4 v12, 0x0
/* move-object/from16 v7, p0 */
/* move-object v8, v2 */
/* move v9, v15 */
/* move-object v10, v4 */
/* move v11, v0 */
/* invoke-direct/range {v7 ..v12}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->setRttTarget(Ljava/lang/String;ILjava/lang/String;II)I */
/* .line 430 */
} // :cond_7
} // :goto_4
/* .line 387 */
} // .end local v0 # "udp_port_value":I
} // .end local v15 # "tcp_port_value":I
} // :cond_8
} // :goto_5
} // .end method
public Integer notifyRttInfo ( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService$RttResult[] p0 ) {
/* .locals 17 */
/* .param p1, "results" # [Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$RttResult; */
/* .line 965 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
int v2 = 0; // const/4 v2, 0x0
/* .line 966 */
/* .local v2, "shortunmeterdrtt":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 969 */
/* .local v3, "longunmeterdrtt":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 970 */
/* .local v4, "tmp_rtt":I */
int v5 = 0; // const/4 v5, 0x0
/* .line 971 */
/* .local v5, "tmp_rtt_count":I */
/* array-length v6, v1 */
int v7 = 0; // const/4 v7, 0x0
/* move v8, v7 */
} // :goto_0
/* if-ge v8, v6, :cond_0 */
/* aget-object v9, v1, v8 */
/* .line 972 */
/* .local v9, "ret":Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$RttResult; */
/* iget v10, v9, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$RttResult;->rtt:I */
/* add-int/2addr v4, v10 */
/* .line 973 */
/* nop */
} // .end local v9 # "ret":Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$RttResult;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 971 */
/* add-int/lit8 v8, v8, 0x1 */
/* .line 976 */
} // :cond_0
final String v6 = "NetworkAccelerateSwitchService"; // const-string v6, "NetworkAccelerateSwitchService"
/* if-gtz v5, :cond_1 */
/* .line 977 */
/* const-string/jumbo v7, "tmp_rtt /= tmp_rtt_count error!" */
android.util.Log .e ( v6,v7 );
/* .line 978 */
int v6 = -1; // const/4 v6, -0x1
/* .line 981 */
} // :cond_1
/* div-int/2addr v4, v5 */
/* .line 983 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "tmp_rtt: " */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v6,v8 );
/* .line 985 */
/* const/16 v8, 0x1f4 */
/* if-gt v4, v8, :cond_5 */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 986 */
/* array-length v8, v1 */
/* move v9, v7 */
} // :goto_1
/* if-ge v9, v8, :cond_5 */
/* aget-object v10, v1, v9 */
/* .line 987 */
/* .local v10, "ret":Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$RttResult; */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "ret: "; // const-string v12, "ret: "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v10 ); // invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v6,v11 );
/* .line 988 */
v11 = this.interfaceName;
/* const-string/jumbo v12, "wlan" */
v11 = (( java.lang.String ) v11 ).startsWith ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v11 != null) { // if-eqz v11, :cond_4
/* .line 989 */
v11 = v11 = this.mShortUnmeteredRttQueue;
int v12 = 3; // const/4 v12, 0x3
/* if-lt v11, v12, :cond_2 */
/* .line 990 */
v11 = this.mShortUnmeteredRttQueue;
/* .line 992 */
} // :cond_2
v11 = this.mShortUnmeteredRttQueue;
/* iget v12, v10, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$RttResult;->rtt:I */
java.lang.Integer .valueOf ( v12 );
/* .line 994 */
v11 = v11 = this.mLongUnmeteredRttQueue;
int v12 = 7; // const/4 v12, 0x7
/* if-lt v11, v12, :cond_3 */
/* .line 995 */
v11 = this.mLongUnmeteredRttQueue;
/* .line 997 */
} // :cond_3
v11 = this.mLongUnmeteredRttQueue;
/* iget v12, v10, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$RttResult;->rtt:I */
java.lang.Integer .valueOf ( v12 );
/* .line 986 */
} // .end local v10 # "ret":Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$RttResult;
} // :cond_4
/* add-int/lit8 v9, v9, 0x1 */
/* .line 1006 */
} // :cond_5
v8 = this.mNetworkSDKService;
int v9 = 1; // const/4 v9, 0x1
if ( v8 != null) { // if-eqz v8, :cond_8
/* .line 1007 */
/* const-string/jumbo v10, "wlan0" */
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v8 ).getQoEByAvailableIfaceNameV1 ( v10 ); // invoke-virtual {v8, v10}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->getQoEByAvailableIfaceNameV1(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
/* .line 1008 */
/* .local v8, "resultQOE":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
if ( v8 != null) { // if-eqz v8, :cond_8
/* .line 1009 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) v8 ).getMpduLostRatio ( ); // invoke-virtual {v8}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getMpduLostRatio()D
/* move-result-wide v10 */
/* const-wide/high16 v12, 0x4059000000000000L # 100.0 */
/* mul-double/2addr v10, v12 */
/* .line 1010 */
/* .local v10, "lostRatio":D */
v12 = v12 = this.mQoeLostRatioQueue;
int v13 = 4; // const/4 v13, 0x4
/* if-lt v12, v13, :cond_6 */
/* .line 1011 */
v12 = this.mQoeLostRatioQueue;
/* .line 1013 */
} // :cond_6
v12 = this.mQoeLostRatioQueue;
java.lang.Double .valueOf ( v10,v11 );
/* .line 1015 */
v12 = v12 = this.mQoeLostRatioQueue;
/* if-ne v12, v13, :cond_8 */
/* .line 1016 */
/* const-wide/16 v12, 0x0 */
/* .line 1017 */
/* .local v12, "averageLostRatio":D */
v14 = this.mQoeLostRatioQueue;
v15 = } // :goto_2
if ( v15 != null) { // if-eqz v15, :cond_7
/* check-cast v15, Ljava/lang/Double; */
(( java.lang.Double ) v15 ).doubleValue ( ); // invoke-virtual {v15}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v15 */
/* .line 1018 */
/* .local v15, "a":D */
/* add-double/2addr v12, v15 */
/* .line 1019 */
} // .end local v15 # "a":D
/* .line 1020 */
} // :cond_7
v14 = v14 = this.mQoeLostRatioQueue;
/* int-to-double v14, v14 */
/* div-double/2addr v12, v14 */
/* .line 1021 */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "averageLostRatio: "; // const-string v15, "averageLostRatio: "
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v12, v13 ); // invoke-virtual {v14, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v6,v14 );
/* .line 1022 */
/* const-wide/high16 v14, 0x402e000000000000L # 15.0 */
/* cmpl-double v14, v12, v14 */
/* if-ltz v14, :cond_8 */
/* iget-boolean v14, v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misHighRtt:Z */
/* if-nez v14, :cond_8 */
/* .line 1023 */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "enable net weak! averageLostRatio: "; // const-string v15, "enable net weak! averageLostRatio: "
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v12, v13 ); // invoke-virtual {v14, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v6,v14 );
/* .line 1024 */
/* iput-boolean v9, v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misHighRtt:Z */
/* .line 1025 */
/* invoke-direct/range {p0 ..p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->enabelNetworkSwitch()V */
/* .line 1026 */
/* .line 1033 */
} // .end local v8 # "resultQOE":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
} // .end local v10 # "lostRatio":D
} // .end local v12 # "averageLostRatio":D
} // :cond_8
v8 = v8 = this.mShortUnmeteredRttQueue;
if ( v8 != null) { // if-eqz v8, :cond_c
v8 = v8 = this.mLongUnmeteredRttQueue;
if ( v8 != null) { // if-eqz v8, :cond_c
/* .line 1034 */
v8 = this.mShortUnmeteredRttQueue;
v10 = } // :goto_3
if ( v10 != null) { // if-eqz v10, :cond_9
/* check-cast v10, Ljava/lang/Integer; */
v10 = (( java.lang.Integer ) v10 ).intValue ( ); // invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I
/* .line 1035 */
/* .local v10, "a":I */
/* add-int/2addr v2, v10 */
/* .line 1036 */
} // .end local v10 # "a":I
/* .line 1038 */
} // :cond_9
v8 = this.mLongUnmeteredRttQueue;
v10 = } // :goto_4
if ( v10 != null) { // if-eqz v10, :cond_a
/* check-cast v10, Ljava/lang/Integer; */
v10 = (( java.lang.Integer ) v10 ).intValue ( ); // invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I
/* .line 1039 */
/* .restart local v10 # "a":I */
/* add-int/2addr v3, v10 */
/* .line 1040 */
} // .end local v10 # "a":I
/* .line 1042 */
} // :cond_a
v8 = v8 = this.mShortUnmeteredRttQueue;
/* div-int v8, v2, v8 */
v10 = this.mLongUnmeteredRttQueue;
v10 = /* .line 1043 */
/* div-int v10, v3, v10 */
/* add-int/2addr v8, v10 */
/* div-int/lit8 v8, v8, 0x2 */
/* .line 1045 */
/* .local v8, "unmeterdaveragertt":I */
/* mul-int/lit8 v10, v8, 0x64 */
/* iget v11, v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->mAverageRssi:I */
/* mul-int/2addr v11, v8 */
/* add-int/2addr v10, v11 */
/* .line 1048 */
/* .local v10, "calcunmeterdaverage":I */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "results:end unmeterdaveragertt:"; // const-string v12, "results:end unmeterdaveragertt:"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v8 ); // invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v6,v11 );
/* .line 1049 */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "results:end calcunmeterdaverage:"; // const-string v12, "results:end calcunmeterdaverage:"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* div-int/lit8 v12, v10, 0x64 */
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v6,v11 );
/* .line 1051 */
/* const/16 v11, 0x2328 */
/* if-le v10, v11, :cond_b */
/* iget-boolean v11, v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misHighRtt:Z */
/* if-nez v11, :cond_b */
/* .line 1052 */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "enable net weak! averagertt:"; // const-string v12, "enable net weak! averagertt:"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* div-int/lit8 v12, v10, 0x64 */
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v6,v11 );
/* .line 1053 */
/* iput-boolean v9, v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misHighRtt:Z */
/* .line 1054 */
/* invoke-direct/range {p0 ..p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->enabelNetworkSwitch()V */
/* .line 1055 */
/* .line 1056 */
} // :cond_b
/* const/16 v11, 0x1388 */
/* if-ge v10, v11, :cond_c */
/* iget-boolean v11, v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misHighRtt:Z */
/* if-ne v11, v9, :cond_c */
/* .line 1057 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "disable net weak! averagertt:"; // const-string v11, "disable net weak! averagertt:"
(( java.lang.StringBuilder ) v9 ).append ( v11 ); // invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* div-int/lit8 v11, v10, 0x64 */
(( java.lang.StringBuilder ) v9 ).append ( v11 ); // invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v6,v9 );
/* .line 1058 */
/* iput-boolean v7, v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->misHighRtt:Z */
/* .line 1059 */
/* invoke-direct/range {p0 ..p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->disabelNetworkSwitch()V */
/* .line 1060 */
/* .line 1064 */
} // .end local v8 # "unmeterdaveragertt":I
} // .end local v10 # "calcunmeterdaverage":I
} // :cond_c
} // .end method
public void onCreate ( ) {
/* .locals 2 */
/* .line 252 */
final String v0 = "NetworkAccelerateSwitchService"; // const-string v0, "NetworkAccelerateSwitchService"
final String v1 = "onCreate "; // const-string v1, "onCreate "
android.util.Log .i ( v0,v1 );
/* .line 254 */
v0 = this.mContext;
/* const-string/jumbo v1, "wifi" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/WifiManager; */
this.mWifiManager = v0;
/* .line 256 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "NetworkAccelerateSwitchHandler"; // const-string v1, "NetworkAccelerateSwitchHandler"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 257 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 258 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$InternalHandler;-><init>(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 260 */
/* const/16 v1, 0x64 */
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 262 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->initNetworkAccelerateSwitch()I */
/* .line 264 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->registerNetworkSDK()V */
/* .line 265 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->registerNetworkSwitchModeChangedObserver()V */
/* .line 266 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->registerAppStatusListener()V */
/* .line 267 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->registerNetworkCallback()V */
/* .line 268 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->registerDefaultNetworkCallback()V */
/* .line 269 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->registerScreenStatusListener()V */
/* .line 270 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->registerHighSpeedModeChangedObserver()V */
/* .line 271 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->registerElevatorStatusReceiver()V */
/* .line 272 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->registerMovementSensorStatusListener()V */
/* .line 274 */
return;
} // .end method
public void onDestroy ( ) {
/* .locals 2 */
/* .line 277 */
final String v0 = "NetworkAccelerateSwitchService"; // const-string v0, "NetworkAccelerateSwitchService"
final String v1 = "onDestroy "; // const-string v1, "onDestroy "
android.util.Log .i ( v0,v1 );
/* .line 279 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->unregisterNetworkSwitchModeChangedObserver()V */
/* .line 280 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->unregisterAppStatusListener()V */
/* .line 281 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->unregisterNetworkCallback()V */
/* .line 282 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->unregisterDefaultNetworkCallback()V */
/* .line 283 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->unregisterScreenStatusListener()V */
/* .line 284 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->unregisterHighSpeedModeChangedObserver()V */
/* .line 285 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->unregisterElevatorStatusReceiver()V */
/* .line 286 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->unregisterMovementSensorStatusListener()V */
/* .line 288 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mMiNetService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 290 */
try { // :try_start_0
v1 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mMiNetCallback;
/* .line 291 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mMiNetService;
/* .line 292 */
v0 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService.mMiNetService;
v1 = this.mDeathRecipient;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 295 */
/* .line 293 */
/* :catch_0 */
/* move-exception v0 */
/* .line 298 */
} // :cond_0
} // :goto_0
v0 = this.mHandlerThread;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 299 */
(( android.os.HandlerThread ) v0 ).quitSafely ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z
/* .line 300 */
this.mHandlerThread = v1;
/* .line 302 */
} // :cond_1
this.mHandler = v1;
/* .line 303 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->deinitNetworkAccelerateSwitch()I */
/* .line 305 */
return;
} // .end method
