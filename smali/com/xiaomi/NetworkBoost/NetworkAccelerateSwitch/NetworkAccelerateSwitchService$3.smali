.class Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$3;
.super Ljava/lang/Object;
.source "NetworkAccelerateSwitchService.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    .line 606
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$3;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNetwrokInterfaceChange(Ljava/lang/String;IZZZZLjava/lang/String;)V
    .locals 7
    .param p1, "ifacename"    # Ljava/lang/String;
    .param p2, "ifacenum"    # I
    .param p3, "wifiready"    # Z
    .param p4, "slavewifiready"    # Z
    .param p5, "dataready"    # Z
    .param p6, "slaveDataReady"    # Z
    .param p7, "status"    # Ljava/lang/String;

    .line 610
    const-string v0, ","

    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 611
    .local v1, "arr":[Ljava/lang/String;
    const-string v2, ""

    .line 612
    .local v2, "newstring":Ljava/lang/String;
    const/4 v3, 0x0

    .line 613
    .local v3, "newcnt":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, v1

    if-ge v4, v5, :cond_2

    .line 614
    aget-object v5, v1, v4

    const-string v6, "rmnet_data"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-gez v5, :cond_1

    aget-object v5, v1, v4

    .line 615
    const-string v6, "ccmni"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ltz v5, :cond_0

    .line 616
    goto :goto_1

    .line 618
    :cond_0
    aget-object v5, v1, v4

    const-string/jumbo v6, "wlan"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ltz v5, :cond_1

    .line 619
    aget-object v5, v1, v4

    const-string v6, ""

    invoke-virtual {v5, v0, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    .line 620
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v1, v4

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 621
    add-int/lit8 v3, v3, 0x1

    .line 613
    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 624
    .end local v4    # "i":I
    :cond_2
    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfputmInterface(Ljava/lang/String;)V

    .line 625
    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfputmIfaceNumber(I)V

    .line 626
    invoke-static {p3}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfputmWifiReady(Z)V

    .line 627
    invoke-static {p4}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$sfputmSlaveWifiReady(Z)V

    .line 628
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$3;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mcheckRssiPollStartConditions(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V

    .line 629
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$3;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mcheckRttPollStartConditions(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V

    .line 630
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService$3;->this$0:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->-$$Nest$mcheckWifiBand(Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;)V

    .line 631
    return-void
.end method
