class com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService$7 extends android.content.BroadcastReceiver {
	 /* .source "NetworkAccelerateSwitchService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->registerElevatorStatusReceiver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService$7 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService; */
/* .line 1129 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 1132 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 1133 */
/* .local v0, "action":Ljava/lang/String; */
v1 = this.this$0;
v1 = com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$misEnableAccelerationSwitchHighSpeedMode ( v1 );
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
	 v1 = this.this$0;
	 /* .line 1134 */
	 v1 = 	 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$misEnableAccelerationSwitch ( v1 );
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 1135 */
		 final String v1 = "com.android.phone.intent.action.ELEVATOR_STATE_SCENE"; // const-string v1, "com.android.phone.intent.action.ELEVATOR_STATE_SCENE"
		 v1 = 		 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v1 != null) { // if-eqz v1, :cond_2
			 v1 = 			 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$sfgetmWifiReady ( );
			 if ( v1 != null) { // if-eqz v1, :cond_2
				 v1 = this.this$0;
				 /* .line 1136 */
				 v1 = 				 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mcheckScreenStatus ( v1 );
				 if ( v1 != null) { // if-eqz v1, :cond_2
					 /* .line 1137 */
					 (( android.content.Intent ) p2 ).getExtras ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
					 /* .line 1138 */
					 /* .local v1, "bundle":Landroid/os/Bundle; */
					 final String v3 = "elevatorState"; // const-string v3, "elevatorState"
					 v3 = 					 (( android.os.Bundle ) v1 ).getInt ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
					 /* .line 1139 */
					 /* .local v3, "status":I */
					 /* new-instance v4, Ljava/lang/StringBuilder; */
					 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
					 final String v5 = "elevator status:"; // const-string v5, "elevator status:"
					 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
					 final String v5 = "NetworkAccelerateSwitchService"; // const-string v5, "NetworkAccelerateSwitchService"
					 android.util.Log .i ( v5,v4 );
					 /* .line 1140 */
					 int v4 = 2; // const/4 v4, 0x2
					 int v5 = 1; // const/4 v5, 0x1
					 /* if-ne v3, v4, :cond_0 */
					 v4 = this.this$0;
					 v4 = 					 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fgetmisEvevator ( v4 );
					 /* if-nez v4, :cond_0 */
					 /* .line 1141 */
					 v2 = this.this$0;
					 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fputmisEvevator ( v2,v5 );
					 /* .line 1142 */
					 v2 = this.this$0;
					 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$menabelNetworkSwitch ( v2 );
					 /* .line 1143 */
				 } // :cond_0
				 /* if-ne v3, v5, :cond_1 */
				 v4 = this.this$0;
				 v4 = 				 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fgetmisEvevator ( v4 );
				 /* if-ne v4, v5, :cond_1 */
				 /* .line 1144 */
				 v4 = this.this$0;
				 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fputmisEvevator ( v4,v2 );
				 /* .line 1145 */
				 v2 = this.this$0;
				 com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mdisabelNetworkSwitch ( v2 );
				 /* .line 1147 */
			 } // .end local v1 # "bundle":Landroid/os/Bundle;
		 } // .end local v3 # "status":I
	 } // :cond_1
} // :goto_0
/* .line 1148 */
} // :cond_2
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$fputmisEvevator ( v1,v2 );
/* .line 1149 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService .-$$Nest$mdisabelNetworkSwitch ( v1 );
/* .line 1151 */
} // :goto_1
return;
} // .end method
