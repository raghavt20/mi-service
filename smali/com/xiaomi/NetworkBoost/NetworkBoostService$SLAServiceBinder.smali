.class public Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;
.super Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub;
.source "NetworkBoostService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/NetworkBoostService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SLAServiceBinder"
.end annotation


# static fields
.field public static final SERVICE_NAME:Ljava/lang/String; = "xiaomi.SLAService"


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;


# direct methods
.method public constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    .line 542
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public addUidToLinkTurboWhiteList(Ljava/lang/String;)Z
    .locals 2
    .param p1, "uid"    # Ljava/lang/String;

    .line 547
    const/4 v0, 0x0

    .line 548
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmSLAService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 549
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmSLAService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->addUidToLinkTurboWhiteList(Ljava/lang/String;)Z

    move-result v0

    .line 551
    :cond_0
    return v0
.end method

.method public getLinkTurboAppsTraffic()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;",
            ">;"
        }
    .end annotation

    .line 574
    const/4 v0, 0x0

    .line 575
    .local v0, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;"
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmSLAService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 576
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmSLAService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->getLinkTurboAppsTraffic()Ljava/util/List;

    move-result-object v0

    .line 578
    :cond_0
    return-object v0
.end method

.method public getLinkTurboDefaultPn()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 592
    const/4 v0, 0x0

    .line 593
    .local v0, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmSLAService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 594
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmSLAService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->getLinkTurboDefaultPn()Ljava/util/List;

    move-result-object v0

    .line 596
    :cond_0
    return-object v0
.end method

.method public getLinkTurboWhiteList()Ljava/lang/String;
    .locals 2

    .line 565
    const/4 v0, 0x0

    .line 566
    .local v0, "tmpLists":Ljava/lang/String;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmSLAService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 567
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmSLAService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->getLinkTurboWhiteList()Ljava/lang/String;

    move-result-object v0

    .line 569
    :cond_0
    return-object v0
.end method

.method public removeUidInLinkTurboWhiteList(Ljava/lang/String;)Z
    .locals 2
    .param p1, "uid"    # Ljava/lang/String;

    .line 556
    const/4 v0, 0x0

    .line 557
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmSLAService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 558
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmSLAService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->removeUidInLinkTurboWhiteList(Ljava/lang/String;)Z

    move-result v0

    .line 560
    :cond_0
    return v0
.end method

.method public setLinkTurboEnable(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .line 583
    const/4 v0, 0x0

    .line 584
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmSLAService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 585
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmSLAService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setLinkTurboEnable(Z)Z

    move-result v0

    .line 587
    :cond_0
    return v0
.end method
