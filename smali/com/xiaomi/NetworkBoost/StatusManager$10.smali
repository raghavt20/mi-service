.class Lcom/xiaomi/NetworkBoost/StatusManager$10;
.super Ljava/lang/Object;
.source "StatusManager.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/StatusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/StatusManager;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 1592
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$10;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .line 1595
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 1599
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$10;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmMovementSensorStatusListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v0

    monitor-enter v0

    .line 1600
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$10;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fputmCurrentSensorevent(Lcom/xiaomi/NetworkBoost/StatusManager;Landroid/hardware/SensorEvent;)V

    .line 1601
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$10;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmMovementSensorStatusListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;

    .line 1602
    .local v2, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;
    invoke-interface {v2, p1}, Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;->onSensorChanged(Landroid/hardware/SensorEvent;)V

    .line 1603
    .end local v2    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;
    goto :goto_0

    .line 1604
    :cond_0
    monitor-exit v0

    .line 1605
    return-void

    .line 1604
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
