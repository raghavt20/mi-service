class com.xiaomi.NetworkBoost.NetworkBoostService$1 extends vendor.xiaomi.hidl.minet.V1_0.IMiNetCallback$Stub {
	 /* .source "NetworkBoostService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkBoostService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkBoostService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.NetworkBoostService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkBoostService; */
/* .line 602 */
this.this$0 = p1;
/* invoke-direct {p0}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetCallback$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void notifyCommon ( Integer p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "cmd" # I */
/* .param p2, "attr" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 607 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ","; // const-string v1, ","
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkBoostService"; // const-string v1, "NetworkBoostService"
android.util.Log .i ( v1,v0 );
/* .line 608 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 613 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "unknow cmd:" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v0 );
/* .line 610 */
/* :pswitch_0 */
(( com.xiaomi.NetworkBoost.NetworkBoostService$1 ) p0 ).onFlowStatGet ( p2 ); // invoke-virtual {p0, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostService$1;->onFlowStatGet(Ljava/lang/String;)V
/* .line 611 */
/* nop */
/* .line 615 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x3ed */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void onFlowStatGet ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "attr" # Ljava/lang/String; */
/* .line 618 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onFlowStatGet"; // const-string v1, "onFlowStatGet"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkBoostService"; // const-string v1, "NetworkBoostService"
android.util.Log .i ( v1,v0 );
/* .line 619 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkAccelerateSwitchService ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 620 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmNetworkAccelerateSwitchService ( v0 );
(( com.xiaomi.NetworkBoost.NetworkAccelerateSwitch.NetworkAccelerateSwitchService ) v0 ).notifyCollectIpAddress ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->notifyCollectIpAddress(Ljava/lang/String;)Z
/* .line 622 */
} // :cond_0
return;
} // .end method
