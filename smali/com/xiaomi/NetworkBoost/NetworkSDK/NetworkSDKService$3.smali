.class Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$3;
.super Ljava/lang/Object;
.source "NetworkSDKService.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    .line 290
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$3;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNetworkPriorityChanged(III)V
    .locals 4
    .param p1, "priorityMode"    # I
    .param p2, "trafficPolicy"    # I
    .param p3, "thermalLevel"    # I

    .line 293
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$3;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbackHandler(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$3;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbackHandler(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/Handler;

    move-result-object v1

    .line 294
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x3f7

    invoke-virtual {v1, v3, p1, p2, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 293
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 296
    return-void
.end method
