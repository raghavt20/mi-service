.class public Lcom/xiaomi/NetworkBoost/NetworkSDK/ResultInfoConstants;
.super Ljava/lang/Object;
.source "ResultInfoConstants.java"


# static fields
.field public static final CODE:Ljava/lang/String; = "code"

.field public static final ERROR_LONG_CODE:J = 0x3e9L

.field public static final ERROR_LONG_PERMISSION_CODE:J = 0x3eaL

.field public static final ERROR_STR_CODE:Ljava/lang/String; = "1001"

.field public static final ERROR_STR_PERMISSION_CODE:Ljava/lang/String; = "1002"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final PERMISSION_ERROR:Ljava/lang/String; = "No permission"

.field public static final RESULT:Ljava/lang/String; = "result"

.field public static final SUCCESS_LONG_CODE:J = 0x3e8L

.field public static final SUCCESS_STR_CODE:Ljava/lang/String; = "1000"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
