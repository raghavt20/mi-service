class com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$10 extends android.content.BroadcastReceiver {
	 /* .source "NetworkSDKService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initBroadcastReceiver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$10 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* .line 2356 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 2359 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 2360 */
/* .local v0, "action":Ljava/lang/String; */
(( android.content.Intent ) p2 ).getData ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;
(( android.net.Uri ) v1 ).getSchemeSpecificPart ( ); // invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;
/* .line 2361 */
/* .local v1, "packageName":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 2362 */
	 return;
	 /* .line 2365 */
} // :cond_0
final String v2 = "android.intent.extra.UID"; // const-string v2, "android.intent.extra.UID"
int v3 = -1; // const/4 v3, -0x1
v2 = (( android.content.Intent ) p2 ).getIntExtra ( v2, v3 ); // invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 2366 */
/* .local v2, "uid":I */
/* if-ne v2, v3, :cond_1 */
/* .line 2367 */
return;
/* .line 2371 */
} // :cond_1
final String v4 = "android.intent.action.PACKAGE_ADDED"; // const-string v4, "android.intent.action.PACKAGE_ADDED"
v4 = (( java.lang.String ) v4 ).equals ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 2372 */
v4 = this.this$0;
v4 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmMarketUid ( v4 );
/* if-eq v4, v3, :cond_2 */
final String v4 = "com.xiaomi.market"; // const-string v4, "com.xiaomi.market"
v4 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
	 /* .line 2373 */
	 v4 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fputmMarketUid ( v4,v2 );
	 /* .line 2375 */
} // :cond_2
v4 = this.this$0;
v4 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmDownloadsUid ( v4 );
/* if-ne v4, v3, :cond_3 */
final String v3 = "com.android.providers.downloads"; // const-string v3, "com.android.providers.downloads"
v3 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
	 /* .line 2376 */
	 v3 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fputmDownloadsUid ( v3,v2 );
	 /* .line 2379 */
} // :cond_3
v3 = this.this$0;
v3 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmNetworkSDKPN ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_5
	 /* .line 2380 */
	 final String v3 = "android.intent.action.PACKAGE_ADDED"; // const-string v3, "android.intent.action.PACKAGE_ADDED"
	 v3 = 	 (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v3 != null) { // if-eqz v3, :cond_4
		 /* .line 2381 */
		 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$sfgetmListLock ( );
		 /* monitor-enter v3 */
		 /* .line 2382 */
		 try { // :try_start_0
			 v4 = this.this$0;
			 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmNetworkSDKUid ( v4 );
			 java.lang.Integer .toString ( v2 );
			 /* .line 2383 */
			 v4 = this.this$0;
			 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmAppUidToPackName ( v4 );
			 java.lang.Integer .valueOf ( v2 );
			 /* .line 2384 */
			 /* monitor-exit v3 */
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* .line 2385 */
			 final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
			 /* new-instance v4, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v5 = "ACTION_PACKAGE_ADDED uid = "; // const-string v5, "ACTION_PACKAGE_ADDED uid = "
			 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Log .i ( v3,v4 );
			 /* .line 2384 */
			 /* :catchall_0 */
			 /* move-exception v4 */
			 try { // :try_start_1
				 /* monitor-exit v3 */
				 /* :try_end_1 */
				 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
				 /* throw v4 */
				 /* .line 2386 */
			 } // :cond_4
			 final String v3 = "android.intent.action.PACKAGE_REMOVED"; // const-string v3, "android.intent.action.PACKAGE_REMOVED"
			 v3 = 			 (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v3 != null) { // if-eqz v3, :cond_5
				 /* .line 2387 */
				 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$sfgetmListLock ( );
				 /* monitor-enter v3 */
				 /* .line 2388 */
				 try { // :try_start_2
					 v4 = this.this$0;
					 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmNetworkSDKUid ( v4 );
					 java.lang.Integer .toString ( v2 );
					 /* .line 2389 */
					 v4 = this.this$0;
					 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmAppUidToPackName ( v4 );
					 java.lang.Integer .valueOf ( v2 );
					 /* .line 2390 */
					 /* monitor-exit v3 */
					 /* :try_end_2 */
					 /* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
					 /* .line 2391 */
					 final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
					 /* new-instance v4, Ljava/lang/StringBuilder; */
					 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
					 final String v5 = "ACTION_PACKAGE_REMOVED uid = "; // const-string v5, "ACTION_PACKAGE_REMOVED uid = "
					 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
					 android.util.Log .i ( v3,v4 );
					 /* .line 2390 */
					 /* :catchall_1 */
					 /* move-exception v4 */
					 try { // :try_start_3
						 /* monitor-exit v3 */
						 /* :try_end_3 */
						 /* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
						 /* throw v4 */
						 /* .line 2395 */
					 } // :cond_5
				 } // :goto_0
				 v3 = this.this$0;
				 v3 = 				 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCellularNetworkSDKPN ( v3 );
				 if ( v3 != null) { // if-eqz v3, :cond_7
					 /* .line 2396 */
					 final String v3 = "android.intent.action.PACKAGE_ADDED"; // const-string v3, "android.intent.action.PACKAGE_ADDED"
					 v3 = 					 (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
					 if ( v3 != null) { // if-eqz v3, :cond_6
						 /* .line 2397 */
						 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$sfgetmCellularListLock ( );
						 /* monitor-enter v3 */
						 /* .line 2398 */
						 try { // :try_start_4
							 v4 = this.this$0;
							 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCellularNetworkSDKUid ( v4 );
							 java.lang.Integer .toString ( v2 );
							 /* .line 2399 */
							 /* monitor-exit v3 */
							 /* :try_end_4 */
							 /* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
							 /* .line 2400 */
							 final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
							 /* new-instance v4, Ljava/lang/StringBuilder; */
							 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
							 final String v5 = "Cellular ACTION_PACKAGE_ADDED uid = "; // const-string v5, "Cellular ACTION_PACKAGE_ADDED uid = "
							 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
							 (( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
							 (( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
							 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
							 android.util.Log .i ( v3,v4 );
							 /* .line 2399 */
							 /* :catchall_2 */
							 /* move-exception v4 */
							 try { // :try_start_5
								 /* monitor-exit v3 */
								 /* :try_end_5 */
								 /* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
								 /* throw v4 */
								 /* .line 2401 */
							 } // :cond_6
							 final String v3 = "android.intent.action.PACKAGE_REMOVED"; // const-string v3, "android.intent.action.PACKAGE_REMOVED"
							 v3 = 							 (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
							 if ( v3 != null) { // if-eqz v3, :cond_7
								 /* .line 2402 */
								 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$sfgetmCellularListLock ( );
								 /* monitor-enter v3 */
								 /* .line 2403 */
								 try { // :try_start_6
									 v4 = this.this$0;
									 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCellularNetworkSDKUid ( v4 );
									 java.lang.Integer .toString ( v2 );
									 /* .line 2404 */
									 /* monitor-exit v3 */
									 /* :try_end_6 */
									 /* .catchall {:try_start_6 ..:try_end_6} :catchall_3 */
									 /* .line 2405 */
									 final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
									 /* new-instance v4, Ljava/lang/StringBuilder; */
									 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
									 final String v5 = "Cellular ACTION_PACKAGE_REMOVED uid = "; // const-string v5, "Cellular ACTION_PACKAGE_REMOVED uid = "
									 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
									 (( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
									 (( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
									 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
									 android.util.Log .i ( v3,v4 );
									 /* .line 2404 */
									 /* :catchall_3 */
									 /* move-exception v4 */
									 try { // :try_start_7
										 /* monitor-exit v3 */
										 /* :try_end_7 */
										 /* .catchall {:try_start_7 ..:try_end_7} :catchall_3 */
										 /* throw v4 */
										 /* .line 2409 */
									 } // :cond_7
								 } // :goto_1
								 return;
							 } // .end method
