public class com.xiaomi.NetworkBoost.NetworkSDK.ResultInfoConstants {
	 /* .source "ResultInfoConstants.java" */
	 /* # static fields */
	 public static final java.lang.String CODE;
	 public static final Long ERROR_LONG_CODE;
	 public static final Long ERROR_LONG_PERMISSION_CODE;
	 public static final java.lang.String ERROR_STR_CODE;
	 public static final java.lang.String ERROR_STR_PERMISSION_CODE;
	 public static final java.lang.String MESSAGE;
	 public static final java.lang.String PERMISSION_ERROR;
	 public static final java.lang.String RESULT;
	 public static final Long SUCCESS_LONG_CODE;
	 public static final java.lang.String SUCCESS_STR_CODE;
	 /* # direct methods */
	 public com.xiaomi.NetworkBoost.NetworkSDK.ResultInfoConstants ( ) {
		 /* .locals 0 */
		 /* .line 3 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
