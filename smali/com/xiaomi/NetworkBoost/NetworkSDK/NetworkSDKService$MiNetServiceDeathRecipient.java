class com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$MiNetServiceDeathRecipient implements android.os.IHwBinder$DeathRecipient {
	 /* .source "NetworkSDKService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MiNetServiceDeathRecipient" */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$MiNetServiceDeathRecipient ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* .line 488 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void serviceDied ( Long p0 ) {
/* .locals 4 */
/* .param p1, "cookie" # J */
/* .line 491 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "HAL service died"; // const-string v1, "HAL service died"
android.util.Log .e ( v0,v1 );
/* .line 492 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmHandler ( v1 );
int v2 = 0; // const/4 v2, 0x0
java.lang.Integer .valueOf ( v2 );
/* const/16 v3, 0x67 */
(( android.os.Handler ) v1 ).obtainMessage ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* const-wide/16 v2, 0xfa0 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 494 */
return;
} // .end method
