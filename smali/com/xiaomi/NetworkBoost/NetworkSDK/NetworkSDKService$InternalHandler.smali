.class Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;
.super Landroid/os/Handler;
.source "NetworkSDKService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;


# direct methods
.method public constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 993
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    .line 994
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 995
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .line 998
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_3

    .line 1047
    :pswitch_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmLock(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 1048
    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmIsStartMonitorByBackground(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1049
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v3, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fputmDualWifiBackgroundCount(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;I)V

    .line 1050
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fputmIsStartMonitorByBackground(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Z)V

    .line 1051
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$mdualWifiBackgroundMonitor(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    .line 1053
    :cond_0
    monitor-exit v0

    .line 1054
    goto/16 :goto_3

    .line 1053
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1059
    :pswitch_2
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmLock(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 1060
    :try_start_1
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v1, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fputmDualWifiBackgroundCount(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;I)V

    .line 1061
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v1, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fputmIsStartMonitorByBackground(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Z)V

    .line 1062
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1063
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmIsEverClosedByBackground(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1064
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$mdualWifiBackgroundMonitorRestart(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    goto/16 :goto_3

    .line 1062
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1

    .line 1056
    :pswitch_3
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$mdualWifiBackgroundMonitor(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    .line 1057
    goto/16 :goto_3

    .line 1032
    :pswitch_4
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmLock(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 1034
    :try_start_3
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v3, v2, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$msetOrGetEverOpenedDualWifi(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;ZZ)Z

    move-result v3

    .line 1035
    .local v3, "isEverOpened":Z
    if-eqz v3, :cond_1

    .line 1036
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmSlaveWifiManager(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/net/wifi/SlaveWifiManager;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1037
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmSlaveWifiManager(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/net/wifi/SlaveWifiManager;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/net/wifi/SlaveWifiManager;->setWifiSlaveEnabled(Z)Z

    .line 1038
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v4, v1, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$msetOrGetEverOpenedDualWifi(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;ZZ)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1043
    .end local v3    # "isEverOpened":Z
    :cond_1
    goto :goto_0

    .line 1044
    :catchall_2
    move-exception v1

    goto :goto_1

    .line 1041
    :catch_0
    move-exception v1

    .line 1042
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1044
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    monitor-exit v0

    .line 1045
    goto/16 :goto_3

    .line 1044
    :goto_1
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v1

    .line 1026
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1027
    .local v0, "releaseUid":I
    const-string v1, "NetworkSDKService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "EVENT_RELEASE_DUAL_WIFI releaseUid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1028
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmSlaveWifiManager(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/net/wifi/SlaveWifiManager;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1029
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v1, v2, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$mhandleMultiAppsDualWifi(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;ZI)V

    goto/16 :goto_3

    .line 1023
    .end local v0    # "releaseUid":I
    :pswitch_6
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmDsdaCapability(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onDsdaStateChanged(Z)V

    .line 1024
    goto/16 :goto_3

    .line 1001
    :pswitch_7
    :try_start_5
    invoke-static {}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;->getService()Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$sfputmMiNetService(Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;)V

    .line 1002
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$sfgetmMiNetService()Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1003
    const-string v0, "NetworkSDKService"

    const-string v1, "HAL service is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1004
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x67

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_2

    .line 1007
    :cond_2
    const-string v0, "NetworkSDKService"

    const-string v1, "HAL service get success"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1008
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$sfgetmMiNetService()Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmDeathRecipient(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/IHwBinder$DeathRecipient;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;->linkToDeath(Landroid/os/IHwBinder$DeathRecipient;J)Z

    .line 1009
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$sfgetmMiNetService()Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    move-result-object v0

    invoke-interface {v0}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;->startMiNetd()Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 1013
    :goto_2
    goto :goto_3

    .line 1011
    :catch_1
    move-exception v0

    .line 1012
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "NetworkSDKService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_3

    .line 1017
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1018
    .local v0, "enable":Z
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 1019
    .local v1, "uid":I
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmSlaveWifiManager(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/net/wifi/SlaveWifiManager;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1020
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2, v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$mhandleMultiAppsDualWifi(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;ZI)V

    .line 1069
    .end local v0    # "enable":Z
    .end local v1    # "uid":I
    :cond_3
    :goto_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
