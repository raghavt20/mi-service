class com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$1 implements com.xiaomi.NetworkBoost.StatusManager$IAppStatusListener {
	 /* .source "NetworkSDKService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* .line 243 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAudioChanged ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mode" # I */
/* .line 272 */
return;
} // .end method
public void onForegroundInfoChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 4 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .line 246 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$msetOrGetEverOpenedDualWifi ( v0,v1,v1 );
/* if-nez v0, :cond_0 */
/* .line 247 */
return;
/* .line 249 */
} // :cond_0
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmDualWifiApps ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmDualWifiApps ( v0 );
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmStatusManager ( v1 );
/* .line 250 */
v1 = (( com.xiaomi.NetworkBoost.StatusManager ) v1 ).getForegroundUid ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->getForegroundUid()I
v0 = java.lang.Integer .valueOf ( v1 );
/* if-nez v0, :cond_1 */
/* .line 251 */
v0 = this.this$0;
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmIsEverClosedByBackground ( v0 );
/* if-nez v0, :cond_1 */
/* .line 252 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmHandler ( v1 );
/* .line 253 */
/* const/16 v2, 0x6d */
(( android.os.Handler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 252 */
/* const-wide/16 v2, 0x7530 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 259 */
} // :cond_1
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmDualWifiApps ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmDualWifiApps ( v0 );
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmStatusManager ( v1 );
/* .line 260 */
v1 = (( com.xiaomi.NetworkBoost.StatusManager ) v1 ).getForegroundUid ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->getForegroundUid()I
v0 = java.lang.Integer .valueOf ( v1 );
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 261 */
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmHandler ( v0 );
	 /* const/16 v1, 0x6b */
	 (( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
	 /* .line 262 */
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmHandler ( v0 );
	 v1 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmHandler ( v1 );
	 /* const/16 v2, 0x6c */
	 (( android.os.Handler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
	 (( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
	 /* .line 264 */
} // :cond_2
return;
} // .end method
public void onUidGone ( Integer p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "goneUid" # I */
/* .param p2, "disabled" # Z */
/* .line 268 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmDualWifiApps ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 v0 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmDualWifiApps ( v0 );
	 v0 = 	 java.lang.Integer .valueOf ( p1 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 269 */
		 v0 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmHandler ( v0 );
		 v1 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmHandler ( v1 );
		 /* const/16 v2, 0x69 */
		 java.lang.Integer .valueOf ( p1 );
		 (( android.os.Handler ) v1 ).obtainMessage ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
		 (( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
		 /* .line 270 */
	 } // :cond_0
	 return;
} // .end method
