class com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$11 extends android.content.BroadcastReceiver {
	 /* .source "NetworkSDKService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initDualDataBroadcastReceiver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$11 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* .line 2418 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 2421 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 2422 */
/* .local v0, "action":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "receive action = "; // const-string v2, "receive action = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
android.util.Log .i ( v2,v1 );
/* .line 2423 */
final String v1 = "com.android.phone.action.VIDEO_APPS_POLICY_NOTIFY"; // const-string v1, "com.android.phone.action.VIDEO_APPS_POLICY_NOTIFY"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 2424 */
	 /* const-string/jumbo v1, "type" */
	 int v3 = -1; // const/4 v3, -0x1
	 v1 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v1, v3 ); // invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 2425 */
	 /* .local v1, "type":I */
	 final String v4 = "duration"; // const-string v4, "duration"
	 v4 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v4, v3 ); // invoke-virtual {p2, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 2426 */
	 /* .local v4, "duration":I */
	 final String v5 = "length"; // const-string v5, "length"
	 v3 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v5, v3 ); // invoke-virtual {p2, v5, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 2427 */
	 /* .local v3, "length":I */
	 v5 = this.this$0;
	 (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v5 ).videoPolicyCallBackNotice ( v1, v4, v3 ); // invoke-virtual {v5, v1, v4, v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->videoPolicyCallBackNotice(III)V
	 /* .line 2428 */
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v6 = "ACTION_VIDEO_APPS_POLICY_NOTIFY, type="; // const-string v6, "ACTION_VIDEO_APPS_POLICY_NOTIFY, type="
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v6 = ", duration="; // const-string v6, ", duration="
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v6 = ", length="; // const-string v6, ", length="
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .i ( v2,v5 );
} // .end local v1 # "type":I
} // .end local v3 # "length":I
} // .end local v4 # "duration":I
/* .line 2430 */
} // :cond_0
final String v1 = "org.codeaurora.intent.action.MSIM_VOICE_CAPABILITY_CHANGED"; // const-string v1, "org.codeaurora.intent.action.MSIM_VOICE_CAPABILITY_CHANGED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2431 */
v1 = this.this$0;
v3 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mgetCurrentDSDAState ( v1 );
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fputmDsdaCapability ( v1,v3 );
/* .line 2432 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmHandler ( v1 );
/* const/16 v3, 0x68 */
(( android.os.Handler ) v1 ).obtainMessage ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 2433 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "ACTION_MSIM_VOICE_CAPABILITY_CHANGED dsdaStateChanged mDsdaCapability:"; // const-string v3, "ACTION_MSIM_VOICE_CAPABILITY_CHANGED dsdaStateChanged mDsdaCapability:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$0;
v3 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmDsdaCapability ( v3 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v2,v1 );
/* .line 2430 */
} // :cond_1
} // :goto_0
/* nop */
/* .line 2435 */
} // :goto_1
return;
} // .end method
