public class com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKUtils {
	 /* .source "NetworkSDKUtils.java" */
	 /* # direct methods */
	 public com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKUtils ( ) {
		 /* .locals 0 */
		 /* .line 6 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static synchronized com.xiaomi.NetworkBoost.NetLinkLayerQoE copyFromNetWlanLinkLayerQoE ( android.net.wifi.WlanLinkLayerQoE p0, com.xiaomi.NetworkBoost.NetLinkLayerQoE p1 ) {
		 /* .locals 3 */
		 /* .param p0, "source" # Landroid/net/wifi/WlanLinkLayerQoE; */
		 /* .param p1, "copyResult" # Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
		 /* const-class v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKUtils; */
		 /* monitor-enter v0 */
		 /* .line 10 */
		 /* if-nez p1, :cond_0 */
		 /* .line 11 */
		 try { // :try_start_0
			 /* new-instance v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
			 /* invoke-direct {v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;-><init>()V */
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* move-object p1, v1 */
			 /* .line 9 */
		 } // .end local p0 # "source":Landroid/net/wifi/WlanLinkLayerQoE;
	 } // .end local p1 # "copyResult":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
	 /* :catchall_0 */
	 /* move-exception p0 */
	 /* goto/16 :goto_1 */
	 /* .line 13 */
	 /* .restart local p0 # "source":Landroid/net/wifi/WlanLinkLayerQoE; */
	 /* .restart local p1 # "copyResult":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
} // :cond_0
} // :goto_0
/* if-nez p0, :cond_1 */
/* .line 14 */
/* monitor-exit v0 */
/* .line 16 */
} // :cond_1
try { // :try_start_1
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getVersion ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getVersion()Ljava/lang/String;
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setVersion ( v1 ); // invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setVersion(Ljava/lang/String;)V
/* .line 17 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getSsid ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getSsid()Ljava/lang/String;
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setSsid ( v1 ); // invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setSsid(Ljava/lang/String;)V
/* .line 18 */
v1 = (( android.net.wifi.WlanLinkLayerQoE ) p0 ).getRssi_mgmt ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRssi_mgmt()I
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setRssi_mgmt ( v1 ); // invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRssi_mgmt(I)V
/* .line 20 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getMpduLostRatio ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getMpduLostRatio()D
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setMpduLostRatio ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setMpduLostRatio(D)V
/* .line 21 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getRetriesRatio ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRetriesRatio()D
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setRetriesRatio ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetriesRatio(D)V
/* .line 23 */
v1 = (( android.net.wifi.WlanLinkLayerQoE ) p0 ).getFrequency ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getFrequency()I
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setFrequency ( v1 ); // invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setFrequency(I)V
/* .line 24 */
v1 = (( android.net.wifi.WlanLinkLayerQoE ) p0 ).getRadioOnTimeMs ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRadioOnTimeMs()I
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setRadioOnTimeMs ( v1 ); // invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRadioOnTimeMs(I)V
/* .line 25 */
v1 = (( android.net.wifi.WlanLinkLayerQoE ) p0 ).getCcaBusyTimeMs ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getCcaBusyTimeMs()I
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setCcaBusyTimeMs ( v1 ); // invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setCcaBusyTimeMs(I)V
/* .line 27 */
v1 = (( android.net.wifi.WlanLinkLayerQoE ) p0 ).getBw ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getBw()I
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setBw ( v1 ); // invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setBw(I)V
/* .line 28 */
v1 = (( android.net.wifi.WlanLinkLayerQoE ) p0 ).getRateMcsIdx ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRateMcsIdx()I
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setRateMcsIdx ( v1 ); // invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRateMcsIdx(I)V
/* .line 29 */
v1 = (( android.net.wifi.WlanLinkLayerQoE ) p0 ).getBitRateInKbps ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getBitRateInKbps()I
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setBitRateInKbps ( v1 ); // invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setBitRateInKbps(I)V
/* .line 31 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getRxmpdu_be ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRxmpdu_be()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setRxmpdu_be ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRxmpdu_be(J)V
/* .line 32 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getTxmpdu_be ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getTxmpdu_be()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setTxmpdu_be ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setTxmpdu_be(J)V
/* .line 33 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getLostmpdu_be ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getLostmpdu_be()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setLostmpdu_be ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setLostmpdu_be(J)V
/* .line 34 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getRetries_be ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRetries_be()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setRetries_be ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetries_be(J)V
/* .line 36 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getRxmpdu_bk ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRxmpdu_bk()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setRxmpdu_bk ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRxmpdu_bk(J)V
/* .line 37 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getTxmpdu_bk ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getTxmpdu_bk()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setTxmpdu_bk ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setTxmpdu_bk(J)V
/* .line 38 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getLostmpdu_bk ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getLostmpdu_bk()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setLostmpdu_bk ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setLostmpdu_bk(J)V
/* .line 39 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getRetries_bk ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRetries_bk()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setRetries_bk ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetries_bk(J)V
/* .line 41 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getRxmpdu_vi ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRxmpdu_vi()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setRxmpdu_vi ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRxmpdu_vi(J)V
/* .line 42 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getTxmpdu_vi ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getTxmpdu_vi()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setTxmpdu_vi ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setTxmpdu_vi(J)V
/* .line 43 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getLostmpdu_vi ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getLostmpdu_vi()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setLostmpdu_vi ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setLostmpdu_vi(J)V
/* .line 44 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getRetries_vi ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRetries_vi()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setRetries_vi ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetries_vi(J)V
/* .line 46 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getRxmpdu_vo ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRxmpdu_vo()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setRxmpdu_vo ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRxmpdu_vo(J)V
/* .line 47 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getTxmpdu_vo ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getTxmpdu_vo()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setTxmpdu_vo ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setTxmpdu_vo(J)V
/* .line 48 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getLostmpdu_vo ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getLostmpdu_vo()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setLostmpdu_vo ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setLostmpdu_vo(J)V
/* .line 49 */
(( android.net.wifi.WlanLinkLayerQoE ) p0 ).getRetries_vo ( ); // invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRetries_vo()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setRetries_vo ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetries_vo(J)V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 51 */
/* monitor-exit v0 */
/* .line 9 */
} // .end local p0 # "source":Landroid/net/wifi/WlanLinkLayerQoE;
} // .end local p1 # "copyResult":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
} // :goto_1
/* monitor-exit v0 */
/* throw p0 */
} // .end method
public static Integer intervalTransformation ( Integer p0 ) {
/* .locals 0 */
/* .param p0, "interval" # I */
/* .line 55 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 72 */
/* :pswitch_0 */
/* const/16 p0, 0x1f40 */
/* .line 73 */
/* .line 69 */
/* :pswitch_1 */
/* const/16 p0, 0xfa0 */
/* .line 70 */
/* .line 66 */
/* :pswitch_2 */
/* const/16 p0, 0x7d0 */
/* .line 67 */
/* .line 63 */
/* :pswitch_3 */
/* const/16 p0, 0x3e8 */
/* .line 64 */
/* .line 60 */
/* :pswitch_4 */
/* const/16 p0, 0xc8 */
/* .line 61 */
/* .line 57 */
/* :pswitch_5 */
/* const/16 p0, 0x64 */
/* .line 58 */
/* nop */
/* .line 78 */
} // :goto_0
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
