.class Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$6;
.super Landroid/content/BroadcastReceiver;
.source "NetworkSDKService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registDualWifiStateBroadcastReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    .line 1247
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$6;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1250
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$6;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmLock(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 1251
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 1252
    .local v1, "action":Ljava/lang/String;
    const-string v2, "android.net.wifi.WIFI_SLAVE_STATE_CHANGED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1253
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$6;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    const-string/jumbo v3, "wifi_state"

    const/16 v4, 0x12

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$mhandleDualWifiStatusChanged(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;I)V

    .line 1257
    .end local v1    # "action":Ljava/lang/String;
    :cond_0
    monitor-exit v0

    .line 1258
    return-void

    .line 1257
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
