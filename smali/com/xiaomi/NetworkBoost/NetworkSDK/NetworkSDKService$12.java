class com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$12 extends android.database.ContentObserver {
	 /* .source "NetworkSDKService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registerQEEChangeObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$12 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 2775 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .line 2778 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmContext ( v0 );
/* .line 2779 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 2778 */
final String v1 = "cloud_qee_enable"; // const-string v1, "cloud_qee_enable"
android.provider.Settings$System .getString ( v0,v1 );
final String v1 = "off"; // const-string v1, "off"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 2780 */
/* .local v0, "isQEENeedOff":Z */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "QEE cloud change, isQEENeedOff: "; // const-string v3, "QEE cloud change, isQEENeedOff: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
android.util.Log .d ( v3,v2 );
/* .line 2782 */
/* const-string/jumbo v2, "sys.network.cronet.qee" */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 2783 */
	 try { // :try_start_0
		 android.os.SystemProperties .set ( v2,v1 );
		 /* .line 2785 */
	 } // :cond_0
	 final String v1 = "on"; // const-string v1, "on"
	 android.os.SystemProperties .set ( v2,v1 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 2789 */
} // :goto_0
/* .line 2787 */
/* :catch_0 */
/* move-exception v1 */
/* .line 2788 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "QEE setprop faild"; // const-string v2, "QEE setprop faild"
android.util.Log .d ( v3,v2 );
/* .line 2790 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
