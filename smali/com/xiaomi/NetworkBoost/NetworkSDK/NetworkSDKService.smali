.class public Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
.super Ljava/lang/Object;
.source "NetworkSDKService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$MiNetServiceDeathRecipient;,
        Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;,
        Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;
    }
.end annotation


# static fields
.field private static final ACTION_MSIM_VOICE_CAPABILITY_CHANGED:Ljava/lang/String; = "org.codeaurora.intent.action.MSIM_VOICE_CAPABILITY_CHANGED"

.field private static final ACTION_VIDEO_APPS_POLICY_NOTIFY:Ljava/lang/String; = "com.android.phone.action.VIDEO_APPS_POLICY_NOTIFY"

.field public static final ADD_WHITELIST_DUAL_WIFI_TURNED_ON:I = 0x7

.field private static final APP_WIFI_SELECTION_MONITOR_MILLIS:J = 0x2710L

.field public static final BASE:I = 0x3e8

.field private static final CALLBACK_NETWORK_PRIORITY_CHANGED:I = 0x3f7

.field private static final CALLBACK_ON_DSDA_STATE_CHANGED:I = 0x3f4

.field private static final CALLBACK_ON_NETWORK_ADDED:I = 0x3ef

.field private static final CALLBACK_ON_NETWORK_REMOVEDE:I = 0x3f0

.field private static final CALLBACK_ON_SCAN_SUCCESS:I = 0x0

.field private static final CALLBACK_ON_SET_SLAVE_WIFI_RES:I = 0x1

.field private static final CALLBACK_ON_SLAVE_CONNECTED:I = 0x2

.field private static final CALLBACK_ON_SLAVE_DISCONNECTED:I = 0x3

.field private static final CALLBACK_ON_SLAVE_WIFI_ENABLE:I = 0x3f3

.field private static final CALLBACK_ON_SLAVE_WIFI_ENABLE_V1:I = 0x3f8

.field private static final CALLBACK_SET_SCREEN_STATUS:I = 0x3f6

.field private static final CALLBACK_START_WIFI_LINKSTATS:I = 0x3f1

.field private static final CALLBACK_VIDEO_POLICY_CHANGED:I = 0x3f5

.field private static final CLOUD_QEE_ENABLE:Ljava/lang/String; = "cloud_qee_enable"

.field private static final DUAL_WIFI_BACKGROUND_MONITOR_MILLIS:J = 0x7530L

.field public static final DUAL_WIFI_TURNED_OFF_SCREEN_OFF:I = 0x3

.field public static final DUAL_WIFI_TURNED_ON_SCREEN_ON:I = 0x4

.field private static final EVENT_BACKGROUND_MONITOR:I = 0x6b

.field private static final EVENT_BACKGROUND_MONITOR_RESTART:I = 0x6c

.field private static final EVENT_BACKGROUND_MONITOR_START:I = 0x6d

.field private static final EVENT_DSDA_STATE_CHANGED:I = 0x68

.field private static final EVENT_GET_HAL:I = 0x67

.field private static final EVENT_IS_EVER_OPENED_DUAL_WIFI:I = 0x6a

.field private static final EVENT_RELEASE_DUAL_WIFI:I = 0x69

.field private static final EVENT_SLAVE_WIFI_CONNECT:I = 0x65

.field private static final EVENT_SLAVE_WIFI_DISCONNECT:I = 0x66

.field private static final EVENT_SLAVE_WIFI_ENABLE:I = 0x64

.field private static final GET_SERVICE_DELAY_MILLIS:I = 0xfa0

.field public static final HANDLE_ADD_TO_WHITELIST:I = 0x2

.field public static final HANDLE_REMOVE_FROM_WHITELIST:I = 0x1

.field public static final IS_DUAL_WIFI_ENABLED:I = 0x0

.field public static final IS_OPENED_DUAL_WIFI:Ljava/lang/String; = "is_opened_dual_wifi"

.field private static final KEY_DURATION:Ljava/lang/String; = "duration"

.field private static final KEY_LENGTH:Ljava/lang/String; = "length"

.field private static final KEY_TYPE:Ljava/lang/String; = "type"

.field private static final LINK_POLL_SEND_DEVIATION:I = 0x21

.field private static final MAX_COUNT:I = 0x12

.field private static final MAX_DUAL_WIFI_BACKGROUND_COUNT:I = 0xa

.field private static final MIBRIDGE_WHITELIST:Ljava/lang/String; = "mibridge_authorized_pkg_list"

.field public static final MINETD_CMD_SETSOCKPRIO:I = 0x3e9

.field public static final MINETD_CMD_SETTCPCONGESTION:I = 0x3ea

.field public static final MINETD_CMD_SET_TRAFFIC_INTERFACE:I = 0x3ee

.field private static final MSG_START_APP_WIFI_SELECTION_MONITOR:I = 0x3f2

.field private static final NETWORK_NETWORKBOOST_WHITELIST:Ljava/lang/String; = "cloud_networkboost_whitelist"

.field private static final QCOM_STATE_DSDA:I = 0x3

.field private static final QEE_PROP:Ljava/lang/String; = "sys.network.cronet.qee"

.field public static final QUERY_IS_IN_WHITELIST:I = 0x3

.field public static final REMOVE_WHITELIST_DUAL_WIFI_TURNED_ON:I = 0x8

.field public static final SDK_TURNS_OFF_DUAL_WIFI:I = 0x6

.field public static final SDK_TURNS_OFF_DUAL_WIFI_BY_BG:I = 0xa

.field public static final SDK_TURNS_ON_DUAL_WIFI:I = 0x5

.field public static final SDK_TURNS_ON_DUAL_WIFI_BY_BG:I = 0x9

.field private static final SLAVE_WIFI_SCREEN_OFF_LONG:J = 0x7530L

.field private static final TAG:Ljava/lang/String; = "NetworkSDKService"

.field private static final TYPE_PRELOAD:I = 0x1

.field private static final TYPE_RESOLUTION_ADJUST:I = 0x2

.field public static final USER_TURNS_OFF_DUAL_WIFI:I = 0x2

.field public static final USER_TURNS_ON_DUAL_WIFI:I = 0x1

.field private static final WIFI_INTERFACE_1:Ljava/lang/String; = "wlan1"

.field private static final mCellularListLock:Ljava/lang/Object;

.field private static final mListLock:Ljava/lang/Object;

.field private static mMiNetService:Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

.field public static volatile sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;


# instance fields
.field private DECIMAL_CONVERSION:I

.field private isConnectSlaveAp:Z

.field private mAppCallBackMapToUid:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/os/IBinder;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mAppStatsPollInterval:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/os/IBinder;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mAppStatsPollMillis:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/os/IBinder;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mAppUidToPackName:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAvailableIfaces:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBssidToNetworkId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCallbackHandler:Landroid/os/Handler;

.field private mCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mCellularNetworkSDKPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCellularNetworkSDKUid:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

.field private mDownloadsUid:I

.field private mDsdaCapability:Z

.field private mDualCelluarDataService:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

.field private mDualWifiApps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mDualWifiBackgroundCount:I

.field private mDualWifiOriginStatus:I

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mIAppStatusListenr:Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;

.field private mIScreenStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

.field private mIsCloseRoaming:Z

.field private mIsDualWifiSDKChanged:Z

.field private mIsDualWifiScreenOffDisabled:Z

.field private mIsEverClosedByBackground:Z

.field private mIsOpenWifiLinkPoll:Z

.field private mIsScreenON:Z

.field private mIsStartMonitorByBackground:Z

.field private mLock:Ljava/lang/Object;

.field private mMarketUid:I

.field private mMasterNetLayerQoE:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

.field private mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

.field private mMiuiWlanLayerQoE:Landroid/net/wifi/WlanLinkLayerQoE;

.field private mNetSelectCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mNetworkPriorityListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;

.field private mNetworkSDKPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNetworkSDKUid:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOffScreenCount:I

.field private mPermission:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mQEEObserver:Landroid/database/ContentObserver;

.field private final mScanListener:Landroid/net/wifi/WifiScanner$ScanListener;

.field private mScanSettings:Landroid/net/wifi/WifiScanner$ScanSettings;

.field private mScanner:Landroid/net/wifi/WifiScanner;

.field private mScreenOffSystemTime:J

.field private mSimCardHelper:Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;

.field private mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

.field private mSlaveNetLayerQoE:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

.field private mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

.field private mStatsSession:Lmiui/securitycenter/net/MiuiNetworkSessionStats;

.field private mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

.field private mTxAndRxStatistics:[J

.field private mWifiLinkStatsPollMillis:J

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private mWifiSelectionApps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mWlanQoECallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;",
            ">;"
        }
    .end annotation
.end field

.field private networkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAppUidToPackName(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppUidToPackName:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAvailableIfaces(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAvailableIfaces:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCallbackHandler(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCellularNetworkSDKPN(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKPN:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCellularNetworkSDKUid(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKUid:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDeathRecipient(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/IHwBinder$DeathRecipient;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDownloadsUid(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDownloadsUid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmDsdaCapability(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDsdaCapability:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmDualWifiApps(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsEverClosedByBackground(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsStartMonitorByBackground(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsStartMonitorByBackground:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLock(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMarketUid(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMarketUid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmNetworkSDKPN(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKPN:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNetworkSDKUid(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKUid:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmQEEObserver(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/database/ContentObserver;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mQEEObserver:Landroid/database/ContentObserver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSlaveWifiManager(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/net/wifi/SlaveWifiManager;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmStatusManager(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Lcom/xiaomi/NetworkBoost/StatusManager;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmAvailableIfaces(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAvailableIfaces:Ljava/util/List;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDownloadsUid(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDownloadsUid:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDsdaCapability(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDsdaCapability:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDualWifiBackgroundCount(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiBackgroundCount:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsStartMonitorByBackground(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsStartMonitorByBackground:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMarketUid(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMarketUid:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mappWifiSelectionMonitor(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->appWifiSelectionMonitor()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mbroadcastPrimaryConnected(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->broadcastPrimaryConnected()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdsdaStateChangedCallBackSend(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->dsdaStateChangedCallBackSend(Landroid/os/Message;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdualWifiBackgroundMonitor(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->dualWifiBackgroundMonitor()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdualWifiBackgroundMonitorRestart(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->dualWifiBackgroundMonitorRestart()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetCurrentDSDAState(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->getCurrentDSDAState()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mhandleDualWifiStatusChanged(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->handleDualWifiStatusChanged(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleMultiAppsDualWifi(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->handleMultiAppsDualWifi(ZI)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mifaceAddCallBackSend(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->ifaceAddCallBackSend(Landroid/os/Message;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mifaceRemovedCallBackSend(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->ifaceRemovedCallBackSend(Landroid/os/Message;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mmediaPlayerPolicyCallBackSend(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mediaPlayerPolicyCallBackSend(Landroid/os/Message;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnetworkPriorityCallBackSend(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkPriorityCallBackSend(Landroid/os/Message;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetOrGetEverOpenedDualWifi(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;ZZ)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setOrGetEverOpenedDualWifi(ZZ)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msetScreenStatus(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setScreenStatus(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupDateCloudWhiteList(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->upDateCloudWhiteList()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdataCellularSdkList(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->updataCellularSdkList()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mwifiLinkLayerStatsPoll(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->wifiLinkLayerStatsPoll()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetmCellularListLock()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularListLock:Ljava/lang/Object;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmListLock()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mListLock:Ljava/lang/Object;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmMiNetService()Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiNetService:Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfputmMiNetService(Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;)V
    .locals 0

    sput-object p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiNetService:Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 185
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mListLock:Ljava/lang/Object;

    .line 186
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularListLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J

    .line 151
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiSelectionApps:Ljava/util/Set;

    .line 152
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollMillis:Ljava/util/Map;

    .line 153
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollInterval:Ljava/util/Map;

    .line 154
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppCallBackMapToUid:Ljava/util/Map;

    .line 157
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    .line 170
    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWlanLayerQoE:Landroid/net/wifi/WlanLinkLayerQoE;

    .line 171
    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMasterNetLayerQoE:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    .line 172
    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaveNetLayerQoE:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    .line 173
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsScreenON:Z

    .line 174
    const/4 v4, 0x3

    new-array v4, v4, [J

    iput-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mTxAndRxStatistics:[J

    .line 175
    const v4, 0xf4240

    iput v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->DECIMAL_CONVERSION:I

    .line 200
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDsdaCapability:Z

    .line 205
    new-instance v4, Ljava/lang/Object;

    invoke-direct {v4}, Ljava/lang/Object;-><init>()V

    iput-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    .line 206
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsOpenWifiLinkPoll:Z

    .line 207
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z

    .line 214
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAvailableIfaces:Ljava/util/List;

    .line 217
    new-instance v4, Landroid/net/wifi/WifiScanner$ScanSettings;

    invoke-direct {v4}, Landroid/net/wifi/WifiScanner$ScanSettings;-><init>()V

    iput-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanSettings:Landroid/net/wifi/WifiScanner$ScanSettings;

    .line 220
    iput v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mOffScreenCount:I

    .line 224
    const/4 v4, -0x1

    iput v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMarketUid:I

    .line 225
    iput v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDownloadsUid:I

    .line 228
    iput v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I

    .line 229
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z

    .line 230
    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    .line 231
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppUidToPackName:Ljava/util/Map;

    .line 232
    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScreenOffSystemTime:J

    .line 233
    iput v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiBackgroundCount:I

    .line 234
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z

    .line 235
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsStartMonitorByBackground:Z

    .line 241
    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mQEEObserver:Landroid/database/ContentObserver;

    .line 243
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIAppStatusListenr:Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;

    .line 275
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$2;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$2;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIScreenStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    .line 290
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$3;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$3;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkPriorityListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;

    .line 299
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$4;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$4;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanListener:Landroid/net/wifi/WifiScanner$ScanListener;

    .line 351
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$5;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$5;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    .line 496
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$MiNetServiceDeathRecipient;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$MiNetServiceDeathRecipient;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    .line 1264
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiScreenOffDisabled:Z

    .line 1827
    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mBssidToNetworkId:Ljava/util/Map;

    .line 397
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    .line 398
    return-void
.end method

.method private appWifiSelectionMonitor()V
    .locals 4

    .line 1652
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->checkAppForegroundStatus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1653
    return-void

    .line 1656
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    .line 1657
    const/16 v1, 0x3f2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 1656
    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1660
    return-void
.end method

.method private availableIfaceCheckAndQoEPull(Ljava/lang/String;)Landroid/net/wifi/WlanLinkLayerQoE;
    .locals 4
    .param p1, "ifaceName"    # Ljava/lang/String;

    .line 1541
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getAvailableIfaces()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 1542
    return-object v1

    .line 1543
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v0, :cond_2

    .line 1544
    invoke-virtual {v0, p1}, Landroid/net/wifi/MiuiWifiManager;->getQoEByAvailableIfaceName(Ljava/lang/String;)Landroid/net/wifi/WlanLinkLayerQoE;

    move-result-object v0

    .line 1545
    .local v0, "mWlanLinkLayerQoE":Landroid/net/wifi/WlanLinkLayerQoE;
    if-nez v0, :cond_1

    .line 1546
    const-string v2, "NetworkSDKService"

    const-string v3, "mMiuiWlanLayerQoE == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1547
    return-object v1

    .line 1549
    :cond_1
    return-object v0

    .line 1551
    .end local v0    # "mWlanLinkLayerQoE":Landroid/net/wifi/WlanLinkLayerQoE;
    :cond_2
    return-object v1
.end method

.method private broadcastPrimaryConnected()V
    .locals 6

    .line 1957
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1958
    :try_start_0
    iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z

    if-nez v1, :cond_0

    .line 1959
    monitor-exit v0

    return-void

    .line 1960
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1962
    .local v1, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 1964
    :try_start_1
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;

    const/4 v4, 0x2

    invoke-interface {v3, v4}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;->connectionStatusCb(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1967
    goto :goto_1

    .line 1973
    .end local v2    # "i":I
    :catchall_0
    move-exception v2

    goto :goto_3

    .line 1969
    :catch_0
    move-exception v2

    goto :goto_2

    .line 1965
    .restart local v2    # "i":I
    :catch_1
    move-exception v3

    .line 1966
    .local v3, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v4, "NetworkSDKService"

    const-string v5, "RemoteException at broadcastPrimaryConnected()"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1962
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1970
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    const-string v3, "NetworkSDKService"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1971
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1973
    .end local v2    # "e":Ljava/lang/Exception;
    :try_start_4
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    goto :goto_4

    :goto_3
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1974
    nop

    .end local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    throw v2

    .line 1973
    .restart local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    :cond_1
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    :goto_4
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1974
    nop

    .line 1975
    .end local v1    # "N":I
    monitor-exit v0

    .line 1976
    return-void

    .line 1975
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method

.method private broadcastPrimaryConnectingFailed()V
    .locals 6

    .line 1980
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1981
    :try_start_0
    iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z

    if-nez v1, :cond_0

    .line 1982
    monitor-exit v0

    return-void

    .line 1983
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1985
    .local v1, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 1987
    :try_start_1
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;

    const/4 v4, 0x3

    invoke-interface {v3, v4}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;->connectionStatusCb(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1990
    goto :goto_1

    .line 1996
    .end local v2    # "i":I
    :catchall_0
    move-exception v2

    goto :goto_3

    .line 1992
    :catch_0
    move-exception v2

    goto :goto_2

    .line 1988
    .restart local v2    # "i":I
    :catch_1
    move-exception v3

    .line 1989
    .local v3, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v4, "NetworkSDKService"

    const-string v5, "RemoteException at broadcastPrimaryConnectingFailed()"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1985
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1993
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    const-string v3, "NetworkSDKService"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1994
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1996
    .end local v2    # "e":Ljava/lang/Exception;
    :try_start_4
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    goto :goto_4

    :goto_3
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1997
    nop

    .end local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    throw v2

    .line 1996
    .restart local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    :cond_1
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    :goto_4
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1997
    nop

    .line 1998
    .end local v1    # "N":I
    monitor-exit v0

    .line 1999
    return-void

    .line 1998
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method

.method private buildAppMobileDataUsage(IJJ)Ljava/util/Map;
    .locals 23
    .param p1, "calluid"    # I
    .param p2, "startTime"    # J
    .param p4, "endTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJJ)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 913
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSimCardHelper:Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;

    const/4 v2, 0x0

    const-string v3, "NetworkSDKService"

    if-nez v0, :cond_0

    .line 914
    const-string v0, "mSimCardHelper null"

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 915
    return-object v2

    .line 918
    :cond_0
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->getCurrentMobileSlotNum()I

    move-result v4

    .line 919
    .local v4, "slotNum":I
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSimCardHelper:Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;

    invoke-virtual {v0, v4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->getSimImsi(I)Ljava/lang/String;

    move-result-object v11

    .line 921
    .local v11, "Imsi":Ljava/lang/String;
    iget-object v5, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatsSession:Lmiui/securitycenter/net/MiuiNetworkSessionStats;

    move-object v6, v11

    move-wide/from16 v7, p2

    move-wide/from16 v9, p4

    invoke-virtual/range {v5 .. v10}, Lmiui/securitycenter/net/MiuiNetworkSessionStats;->getMobileSummaryForAllUid(Ljava/lang/String;JJ)Landroid/util/SparseArray;

    move-result-object v5

    .line 923
    .local v5, "networkStats":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;>;"
    if-nez v5, :cond_1

    .line 924
    const-string v0, "buildAppMobileDataUsage networkStats null"

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 925
    return-object v2

    .line 928
    :cond_1
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v6

    .line 929
    .local v6, "statsSize":I
    const/4 v0, 0x0

    .line 932
    .local v0, "appusage":Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;
    const/4 v7, 0x0

    move/from16 v22, v7

    move-object v7, v0

    move/from16 v0, v22

    .local v0, "i":I
    .local v7, "appusage":Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;
    :goto_0
    if-ge v0, v6, :cond_3

    .line 933
    :try_start_0
    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 935
    .local v8, "uid":I
    move/from16 v9, p1

    if-ne v9, v8, :cond_2

    .line 936
    :try_start_1
    invoke-virtual {v5, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map;

    .line 938
    .local v10, "entry":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    if-eqz v10, :cond_2

    .line 939
    new-instance v21, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;

    const-string v12, "rxBytes"

    .line 940
    invoke-interface {v10, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Long;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    const-string/jumbo v12, "txBytes"

    .line 941
    invoke-interface {v10, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Long;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    const-string/jumbo v12, "txForegroundBytes"

    .line 942
    invoke-interface {v10, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Long;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    const-string v12, "rxForegroundBytes"

    .line 943
    invoke-interface {v10, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Long;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    move-object/from16 v12, v21

    invoke-direct/range {v12 .. v20}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;-><init>(JJJJ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object/from16 v7, v21

    goto :goto_1

    .line 948
    .end local v0    # "i":I
    .end local v8    # "uid":I
    .end local v10    # "entry":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    :catch_0
    move-exception v0

    goto :goto_2

    .line 932
    .restart local v0    # "i":I
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 948
    .end local v0    # "i":I
    :catch_1
    move-exception v0

    move/from16 v9, p1

    .line 949
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    const-string v8, "buildAppMobileDataUsage error"

    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 932
    .local v0, "i":I
    :cond_3
    move/from16 v9, p1

    .line 950
    .end local v0    # "i":I
    nop

    .line 952
    :goto_3
    if-nez v7, :cond_4

    goto :goto_4

    :cond_4
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->toMap(Z)Ljava/util/Map;

    move-result-object v2

    :goto_4
    return-object v2
.end method

.method private buildAppTrafficStatistics(IJJI)Ljava/util/Map;
    .locals 9
    .param p1, "type"    # I
    .param p2, "startTime"    # J
    .param p4, "endTime"    # J
    .param p6, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJJI)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 881
    const/4 v0, 0x0

    .line 882
    .local v0, "buildAppTrafficStatistics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 883
    .local v1, "mobileTrafficStatistics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 885
    .local v2, "wifiTrafficStatistics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 893
    :pswitch_0
    move-object v3, p0

    move v4, p6

    move-wide v5, p2

    move-wide v7, p4

    invoke-direct/range {v3 .. v8}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->buildAppMobileDataUsage(IJJ)Ljava/util/Map;

    move-result-object v1

    .line 894
    invoke-direct/range {v3 .. v8}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->buildAppWifiDataUsage(IJJ)Ljava/util/Map;

    move-result-object v2

    .line 896
    if-eqz v1, :cond_0

    .line 897
    invoke-interface {v1, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 898
    move-object v0, v1

    goto :goto_0

    .line 899
    :cond_0
    if-eqz v2, :cond_1

    .line 900
    move-object v0, v2

    goto :goto_0

    .line 902
    :cond_1
    const/4 v0, 0x0

    .line 904
    goto :goto_0

    .line 890
    :pswitch_1
    move-object v3, p0

    move v4, p6

    move-wide v5, p2

    move-wide v7, p4

    invoke-direct/range {v3 .. v8}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->buildAppWifiDataUsage(IJJ)Ljava/util/Map;

    move-result-object v0

    .line 891
    goto :goto_0

    .line 887
    :pswitch_2
    move-object v3, p0

    move v4, p6

    move-wide v5, p2

    move-wide v7, p4

    invoke-direct/range {v3 .. v8}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->buildAppMobileDataUsage(IJJ)Ljava/util/Map;

    move-result-object v0

    .line 888
    nop

    .line 909
    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private buildAppWifiDataUsage(IJJ)Ljava/util/Map;
    .locals 25
    .param p1, "calluid"    # I
    .param p2, "startTime"    # J
    .param p4, "endTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJJ)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 956
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatsSession:Lmiui/securitycenter/net/MiuiNetworkSessionStats;

    move-wide/from16 v2, p2

    move-wide/from16 v4, p4

    invoke-virtual {v0, v2, v3, v4, v5}, Lmiui/securitycenter/net/MiuiNetworkSessionStats;->getWifiSummaryForAllUid(JJ)Landroid/util/SparseArray;

    move-result-object v6

    .line 958
    .local v6, "networkStats":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;>;"
    const/4 v7, 0x0

    const-string v8, "NetworkSDKService"

    if-nez v6, :cond_0

    .line 959
    const-string v0, "buildAppWifiDataUsage networkStats null"

    invoke-static {v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 960
    return-object v7

    .line 963
    :cond_0
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v9

    .line 964
    .local v9, "statsSize":I
    const/4 v0, 0x0

    .line 967
    .local v0, "appusage":Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;
    const/4 v10, 0x0

    move/from16 v24, v10

    move-object v10, v0

    move/from16 v0, v24

    .local v0, "i":I
    .local v10, "appusage":Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;
    :goto_0
    if-ge v0, v9, :cond_2

    .line 968
    :try_start_0
    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v11
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 970
    .local v11, "uid":I
    move/from16 v12, p1

    if-ne v12, v11, :cond_1

    .line 971
    :try_start_1
    invoke-virtual {v6, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map;

    .line 973
    .local v13, "entry":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    if-eqz v13, :cond_1

    .line 974
    new-instance v23, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;

    const-string v14, "rxBytes"

    .line 975
    invoke-interface {v13, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Long;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    const-string/jumbo v14, "txBytes"

    .line 976
    invoke-interface {v13, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Long;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    const-string/jumbo v14, "txForegroundBytes"

    .line 977
    invoke-interface {v13, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Long;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    const-string v14, "rxForegroundBytes"

    .line 978
    invoke-interface {v13, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Long;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v21

    move-object/from16 v14, v23

    invoke-direct/range {v14 .. v22}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;-><init>(JJJJ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object/from16 v10, v23

    goto :goto_1

    .line 983
    .end local v0    # "i":I
    .end local v11    # "uid":I
    .end local v13    # "entry":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    :catch_0
    move-exception v0

    goto :goto_2

    .line 967
    .restart local v0    # "i":I
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 983
    .end local v0    # "i":I
    :catch_1
    move-exception v0

    move/from16 v12, p1

    .line 984
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 985
    const-string v11, "buildAppWifiDataUsage error"

    invoke-static {v8, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 967
    .local v0, "i":I
    :cond_2
    move/from16 v12, p1

    .line 986
    .end local v0    # "i":I
    nop

    .line 988
    :goto_3
    if-nez v10, :cond_3

    goto :goto_4

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {v10, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->toMap(Z)Ljava/util/Map;

    move-result-object v7

    :goto_4
    return-object v7
.end method

.method private calculateAllPackages(Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;)V
    .locals 8
    .param p1, "mNetLayerQoE"    # Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    .line 1570
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mTxAndRxStatistics:[J

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getLostmpdu_be()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getLostmpdu_bk()J

    move-result-wide v3

    add-long/2addr v1, v3

    .line 1571
    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getLostmpdu_vi()J

    move-result-wide v3

    add-long/2addr v1, v3

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getLostmpdu_vo()J

    move-result-wide v3

    add-long/2addr v1, v3

    const/4 v3, 0x0

    aput-wide v1, v0, v3

    .line 1573
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mTxAndRxStatistics:[J

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRetries_be()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRetries_bk()J

    move-result-wide v4

    add-long/2addr v1, v4

    .line 1574
    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRetries_vi()J

    move-result-wide v4

    add-long/2addr v1, v4

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRetries_vo()J

    move-result-wide v4

    add-long/2addr v1, v4

    const/4 v4, 0x1

    aput-wide v1, v0, v4

    .line 1576
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mTxAndRxStatistics:[J

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getTxmpdu_be()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getTxmpdu_bk()J

    move-result-wide v5

    add-long/2addr v1, v5

    .line 1577
    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getTxmpdu_vi()J

    move-result-wide v5

    add-long/2addr v1, v5

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getTxmpdu_vo()J

    move-result-wide v5

    add-long/2addr v1, v5

    .line 1578
    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRxmpdu_be()J

    move-result-wide v5

    add-long/2addr v1, v5

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRxmpdu_bk()J

    move-result-wide v5

    add-long/2addr v1, v5

    .line 1579
    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRxmpdu_vi()J

    move-result-wide v5

    add-long/2addr v1, v5

    invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRxmpdu_vo()J

    move-result-wide v5

    add-long/2addr v1, v5

    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mTxAndRxStatistics:[J

    aget-wide v6, v5, v3

    add-long/2addr v1, v6

    aget-wide v3, v5, v4

    add-long/2addr v1, v3

    const/4 v3, 0x2

    aput-wide v1, v0, v3

    .line 1581
    return-void
.end method

.method private calculateLostAndRetryRatio(Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;)V
    .locals 8
    .param p1, "mNetLayerQoE"    # Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    .line 1555
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->calculateAllPackages(Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;)V

    .line 1557
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mTxAndRxStatistics:[J

    const/4 v1, 0x2

    aget-wide v2, v0, v1

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-nez v4, :cond_0

    .line 1558
    return-void

    .line 1560
    :cond_0
    const/4 v4, 0x0

    aget-wide v4, v0, v4

    long-to-double v4, v4

    long-to-double v2, v2

    div-double/2addr v4, v2

    .line 1561
    .local v4, "curLostRatio":D
    iget v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->DECIMAL_CONVERSION:I

    int-to-double v2, v0

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-double v2, v2

    iget v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->DECIMAL_CONVERSION:I

    int-to-double v6, v0

    div-double/2addr v2, v6

    .line 1562
    .end local v4    # "curLostRatio":D
    .local v2, "curLostRatio":D
    invoke-virtual {p1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setMpduLostRatio(D)V

    .line 1564
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mTxAndRxStatistics:[J

    const/4 v4, 0x1

    aget-wide v4, v0, v4

    long-to-double v4, v4

    aget-wide v0, v0, v1

    long-to-double v0, v0

    div-double/2addr v4, v0

    .line 1565
    .local v4, "curRetryRatio":D
    iget v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->DECIMAL_CONVERSION:I

    int-to-double v0, v0

    mul-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    iget v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->DECIMAL_CONVERSION:I

    int-to-double v6, v6

    div-double/2addr v0, v6

    .line 1566
    .end local v4    # "curRetryRatio":D
    .local v0, "curRetryRatio":D
    invoke-virtual {p1, v0, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetriesRatio(D)V

    .line 1567
    return-void
.end method

.method private static channelsToSpec([I)[Landroid/net/wifi/WifiScanner$ChannelSpec;
    .locals 4
    .param p0, "channelList"    # [I

    .line 659
    array-length v0, p0

    new-array v0, v0, [Landroid/net/wifi/WifiScanner$ChannelSpec;

    .line 660
    .local v0, "channelSpecs":[Landroid/net/wifi/WifiScanner$ChannelSpec;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 661
    new-instance v2, Landroid/net/wifi/WifiScanner$ChannelSpec;

    aget v3, p0, v1

    invoke-direct {v2, v3}, Landroid/net/wifi/WifiScanner$ChannelSpec;-><init>(I)V

    aput-object v2, v0, v1

    .line 660
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 663
    .end local v1    # "i":I
    :cond_0
    return-object v0
.end method

.method private checkAppDualWifiBackgroundStatus()Z
    .locals 5

    .line 1701
    const-string v0, "NetworkSDKService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dual-wifi background monitor count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiBackgroundCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1702
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1703
    :try_start_0
    iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsScreenON:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    if-eqz v1, :cond_3

    .line 1704
    invoke-direct {p0, v2, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setOrGetEverOpenedDualWifi(ZZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1705
    invoke-virtual {p0, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSlaveWifiEnabledAndOthersOpt(I)I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 1710
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/StatusManager;->getForegroundUid()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1711
    iget v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiBackgroundCount:I

    const/4 v3, 0x1

    add-int/2addr v1, v3

    iput v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiBackgroundCount:I

    .line 1718
    const/16 v4, 0xa

    if-ne v1, v4, :cond_1

    .line 1719
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->closeDualWifiTemporarilyByBackground()V

    .line 1720
    monitor-exit v0

    return v2

    .line 1722
    :cond_1
    monitor-exit v0

    .line 1724
    return v3

    .line 1713
    :cond_2
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->resetMonitorStatus()V

    .line 1714
    monitor-exit v0

    return v2

    .line 1706
    :cond_3
    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->resetMonitorStatus()V

    .line 1707
    monitor-exit v0

    return v2

    .line 1722
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private checkAppForegroundStatus()Z
    .locals 5

    .line 1663
    const-string v0, "NetworkSDKService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mOffScreenCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mOffScreenCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1664
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1666
    :try_start_0
    iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 1667
    monitor-exit v0

    return v2

    .line 1669
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiSelectionApps:Ljava/util/Set;

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/StatusManager;->getForegroundUid()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x1

    if-nez v1, :cond_1

    .line 1670
    iget v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mOffScreenCount:I

    add-int/2addr v1, v3

    iput v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mOffScreenCount:I

    goto :goto_0

    .line 1672
    :cond_1
    iput v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mOffScreenCount:I

    .line 1675
    :goto_0
    iget v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mOffScreenCount:I

    const/16 v4, 0x12

    if-ne v1, v4, :cond_2

    .line 1676
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->clearAppWifiSelectionMonitor()V

    .line 1677
    monitor-exit v0

    return v2

    .line 1679
    :cond_2
    monitor-exit v0

    .line 1681
    return v3

    .line 1679
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private clearAppWifiSelectionMonitor()V
    .locals 1

    .line 1685
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->forceOpenFrameworkRoaming()V

    .line 1686
    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mOffScreenCount:I

    .line 1687
    return-void
.end method

.method private clearDualWifiStatus()V
    .locals 2

    .line 1232
    const-string v0, "NetworkSDKService"

    const-string v1, "clearDualWifiStatus"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1233
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1234
    const/4 v1, -0x1

    :try_start_0
    iput v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I

    .line 1235
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    .line 1236
    monitor-exit v0

    .line 1237
    return-void

    .line 1236
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private closeAppTrafficSession()V
    .locals 1

    .line 871
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatsSession:Lmiui/securitycenter/net/MiuiNetworkSessionStats;

    if-eqz v0, :cond_0

    .line 872
    invoke-virtual {v0}, Lmiui/securitycenter/net/MiuiNetworkSessionStats;->closeSession()V

    .line 873
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatsSession:Lmiui/securitycenter/net/MiuiNetworkSessionStats;

    .line 875
    :cond_0
    return-void
.end method

.method private closeDualWifiTemporarilyByBackground()V
    .locals 4

    .line 1728
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->resetMonitorStatus()V

    .line 1730
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    if-nez v0, :cond_0

    .line 1731
    return-void

    .line 1733
    :cond_0
    invoke-virtual {v0}, Landroid/net/wifi/SlaveWifiManager;->isBusySlaveWifi()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1734
    const-string v0, "NetworkSDKService"

    const-string v1, "dual-wifi background monitor slave wifi is busy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1735
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1736
    const/16 v1, 0x9

    :try_start_0
    iput v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiBackgroundCount:I

    .line 1737
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1738
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandler:Landroid/os/Handler;

    .line 1739
    const/16 v1, 0x6b

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 1738
    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1742
    return-void

    .line 1737
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 1745
    :cond_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1746
    :try_start_2
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/net/wifi/SlaveWifiManager;->setWifiSlaveEnabled(Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1747
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z

    .line 1749
    :cond_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1750
    const-string v0, "NetworkSDKService"

    const-string v1, "dual-wifi background monitor close dual wifi"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1751
    return-void

    .line 1749
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method private connectionWithBssid(Ljava/lang/String;)Z
    .locals 7
    .param p1, "bssid"    # Ljava/lang/String;

    .line 1925
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mBssidToNetworkId:Ljava/util/Map;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 1926
    return v1

    .line 1929
    :cond_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v2, :cond_2

    .line 1930
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0, p1}, Landroid/net/wifi/MiuiWifiManager;->netSDKConnectPrimaryWithBssid(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1932
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1933
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1935
    .local v1, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x1

    if-ge v2, v1, :cond_1

    .line 1937
    :try_start_1
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;

    invoke-interface {v4, v3}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;->connectionStatusCb(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1940
    goto :goto_1

    .line 1946
    .end local v2    # "i":I
    :catchall_0
    move-exception v2

    goto :goto_3

    .line 1942
    :catch_0
    move-exception v2

    goto :goto_2

    .line 1938
    .restart local v2    # "i":I
    :catch_1
    move-exception v4

    .line 1939
    .local v4, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v5, "NetworkSDKService"

    const-string v6, "RemoteException at connectionWithBssid()"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1935
    .end local v4    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1943
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    const-string v4, "NetworkSDKService"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1944
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1946
    .end local v2    # "e":Ljava/lang/Exception;
    :try_start_4
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    goto :goto_4

    :goto_3
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1947
    nop

    .end local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .end local p1    # "bssid":Ljava/lang/String;
    throw v2

    .line 1946
    .restart local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .restart local p1    # "bssid":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    :goto_4
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1947
    nop

    .line 1948
    .end local v1    # "N":I
    monitor-exit v0

    .line 1949
    return v3

    .line 1948
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1

    .line 1951
    :cond_2
    return v1
.end method

.method public static destroyInstance()V
    .locals 4

    .line 381
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    if-eqz v0, :cond_1

    .line 382
    const-class v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    monitor-enter v0

    .line 383
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 385
    :try_start_1
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onDestroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389
    goto :goto_0

    .line 387
    :catch_0
    move-exception v1

    .line 388
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "NetworkSDKService"

    const-string v3, "destroyInstance onDestroy catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 390
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    .line 392
    :cond_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 394
    :cond_1
    :goto_1
    return-void
.end method

.method private dsdaStateChangedCallBackSend(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 1448
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1449
    .local v0, "res":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 1451
    .local v1, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 1453
    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-interface {v3, v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->dsdaStateChanged(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1456
    goto :goto_1

    .line 1454
    :catch_0
    move-exception v3

    .line 1455
    .local v3, "e":Landroid/os/RemoteException;
    const-string v4, "NetworkSDKService"

    const-string v5, "RemoteException at dsdaStateChangedCallBackSend()"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1451
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1458
    .end local v2    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1459
    return-void
.end method

.method private dualWifiBackgroundMonitor()V
    .locals 4

    .line 1690
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->checkAppDualWifiBackgroundStatus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1691
    return-void

    .line 1694
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandler:Landroid/os/Handler;

    .line 1695
    const/16 v1, 0x6b

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 1694
    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1698
    return-void
.end method

.method private dualWifiBackgroundMonitorRestart()V
    .locals 3

    .line 1761
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1762
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v1, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setOrGetEverOpenedDualWifi(ZZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1763
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z

    .line 1764
    monitor-exit v0

    return-void

    .line 1767
    :cond_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    if-nez v2, :cond_1

    .line 1768
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z

    .line 1769
    monitor-exit v0

    return-void

    .line 1772
    :cond_1
    iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z

    if-eqz v1, :cond_2

    .line 1773
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    if-eqz v1, :cond_2

    .line 1774
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/net/wifi/SlaveWifiManager;->setWifiSlaveEnabled(Z)Z

    .line 1776
    :cond_2
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1778
    const-string v0, "NetworkSDKService"

    const-string v1, "dual-wifi background monitor restart dual wifi"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1779
    return-void

    .line 1776
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private forceOpenFrameworkRoaming()V
    .locals 7

    .line 2059
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2060
    :try_start_0
    iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z

    if-nez v1, :cond_0

    .line 2061
    monitor-exit v0

    return-void

    .line 2062
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2064
    .local v1, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 2067
    :try_start_1
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;

    const/4 v4, 0x4

    invoke-interface {v3, v4}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;->connectionStatusCb(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2070
    goto :goto_1

    .line 2076
    .end local v2    # "i":I
    :catchall_0
    move-exception v2

    goto :goto_3

    .line 2072
    :catch_0
    move-exception v2

    goto :goto_2

    .line 2068
    .restart local v2    # "i":I
    :catch_1
    move-exception v3

    .line 2069
    .local v3, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v4, "NetworkSDKService"

    const-string v5, "RemoteException at forceOpenFrameworkRoaming()"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2064
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2073
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    const-string v3, "NetworkSDKService"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2074
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2076
    .end local v2    # "e":Ljava/lang/Exception;
    :try_start_4
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    goto :goto_4

    :goto_3
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2077
    nop

    .end local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    throw v2

    .line 2076
    .restart local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    :cond_1
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    :goto_4
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2077
    nop

    .line 2079
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v2

    .line 2080
    .local v2, "count":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2081
    .local v3, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_5
    if-ge v4, v2, :cond_2

    .line 2082
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v5, v4}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackItem(I)Landroid/os/IInterface;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2081
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 2084
    .end local v4    # "i":I
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;

    .line 2085
    .local v5, "cb":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v6, v5}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 2086
    nop

    .end local v5    # "cb":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;
    goto :goto_6

    .line 2088
    :cond_3
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v4, :cond_4

    .line 2089
    const/4 v5, -0x1

    const/4 v6, 0x1

    invoke-virtual {v4, v6, v5}, Landroid/net/wifi/MiuiWifiManager;->netSDKSetIsDisableRoam(ZI)Z

    .line 2091
    :cond_4
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z

    .line 2092
    .end local v1    # "N":I
    .end local v2    # "count":I
    .end local v3    # "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;>;"
    monitor-exit v0

    .line 2093
    return-void

    .line 2092
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method

.method private getCurrentDSDAState()Z
    .locals 3

    .line 2646
    const-string v0, "ril.multisim.voice_capability"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x3

    if-ne v2, v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 335
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    if-nez v0, :cond_1

    .line 336
    const-class v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    monitor-enter v0

    .line 337
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    if-nez v1, :cond_0

    .line 338
    new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 340
    :try_start_1
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onCreate()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344
    goto :goto_0

    .line 342
    :catch_0
    move-exception v1

    .line 343
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "NetworkSDKService"

    const-string v3, "getInstance onCreate catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 346
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 348
    :cond_1
    :goto_1
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    return-object v0
.end method

.method private handleDualWifiStatusChanged(I)V
    .locals 8
    .param p1, "state"    # I

    .line 1266
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1267
    :try_start_0
    const-string v1, "NetworkSDKService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mIsDualWifiSDKChanged = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mDualWifiOriginStatus = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1270
    iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z

    const/4 v2, 0x5

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    .line 1271
    const/16 v1, 0x11

    if-ne p1, v1, :cond_0

    .line 1272
    invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V

    .line 1273
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z

    .line 1274
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z

    .line 1275
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsStartMonitorByBackground:Z

    .line 1277
    :cond_0
    const/16 v1, 0xf

    if-ne p1, v1, :cond_1

    .line 1278
    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V

    .line 1279
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z

    .line 1280
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z

    .line 1281
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsStartMonitorByBackground:Z

    .line 1283
    :cond_1
    monitor-exit v0

    return-void

    .line 1286
    :cond_2
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z

    .line 1287
    const/4 v1, 0x1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 1289
    :pswitch_1
    iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiScreenOffDisabled:Z

    if-eqz v2, :cond_3

    .line 1290
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiScreenOffDisabled:Z

    .line 1291
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V

    .line 1292
    goto :goto_1

    .line 1294
    :cond_3
    iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z

    if-eqz v2, :cond_4

    .line 1295
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z

    .line 1296
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsStartMonitorByBackground:Z

    .line 1297
    const/16 v1, 0x9

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V

    .line 1298
    goto :goto_1

    .line 1300
    :cond_4
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiScreenOffDisabled:Z

    .line 1301
    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V

    .line 1302
    goto :goto_1

    .line 1304
    :pswitch_2
    iget-boolean v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsScreenON:Z

    if-nez v4, :cond_5

    .line 1305
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScreenOffSystemTime:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x7530

    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    .line 1306
    const/4 v2, 0x4

    invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V

    .line 1307
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiScreenOffDisabled:Z

    .line 1308
    goto :goto_1

    .line 1311
    :cond_5
    iget-boolean v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z

    if-eqz v4, :cond_6

    .line 1312
    const/16 v1, 0xa

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1313
    goto :goto_1

    .line 1316
    :cond_6
    :try_start_1
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    if-eqz v4, :cond_7

    .line 1317
    const-string v5, ""

    const-string v6, ""

    invoke-virtual {v4, v2, v5, v6, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z

    .line 1318
    :cond_7
    invoke-direct {p0, v1, v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setOrGetEverOpenedDualWifi(ZZ)Z

    .line 1319
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v1, :cond_8

    .line 1320
    invoke-virtual {v1, v3}, Landroid/net/wifi/MiuiWifiManager;->setSlaveCandidatesRssiThreshold(Z)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1323
    :cond_8
    goto :goto_0

    .line 1321
    :catch_0
    move-exception v1

    .line 1322
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1324
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->clearDualWifiStatus()V

    .line 1325
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V

    .line 1326
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiScreenOffDisabled:Z

    .line 1327
    nop

    .line 1331
    :goto_1
    monitor-exit v0

    .line 1332
    return-void

    .line 1331
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0xf
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleMultiAppsDualWifi(ZI)V
    .locals 11
    .param p1, "enable"    # Z
    .param p2, "uid"    # I

    .line 1073
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSupportDualWifi()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1074
    return-void

    .line 1077
    :cond_0
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsScreenON:Z

    if-nez v0, :cond_1

    .line 1078
    const-string v0, "NetworkSDKService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMultiAppsDualWifi mIsScreenON = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsScreenON:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1079
    return-void

    .line 1082
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSlaveWifiEnabledAndOthersOpt(II)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move v1, v0

    .line 1084
    .local v1, "isDualWifiOpen":Z
    :goto_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    if-nez v3, :cond_3

    .line 1085
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v3

    iput-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 1087
    :cond_3
    const/4 v3, -0x1

    .line 1088
    .local v3, "originStatus":I
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 1089
    :try_start_0
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    if-nez v5, :cond_4

    .line 1090
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    .line 1092
    :cond_4
    const-string v5, "NetworkSDKService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "pre mDualWifiOriginStatus = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mIsDualWifiSDKChanged = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mDualWifiApps.size = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    .line 1094
    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1092
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1096
    iget v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_6

    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    if-nez v5, :cond_6

    .line 1097
    if-eqz v1, :cond_5

    .line 1098
    iput v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I

    goto :goto_1

    .line 1100
    :cond_5
    iput v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I

    .line 1103
    :cond_6
    :goto_1
    iget v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I

    move v3, v5

    .line 1104
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 1106
    const/4 v4, 0x0

    .line 1107
    .local v4, "packName":Ljava/lang/String;
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppUidToPackName:Ljava/util/Map;

    if-eqz v5, :cond_7

    .line 1108
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v4, v5

    check-cast v4, Ljava/lang/String;

    .line 1109
    :cond_7
    if-nez v4, :cond_8

    .line 1110
    return-void

    .line 1112
    :cond_8
    const/4 v5, 0x0

    .line 1113
    .local v5, "isMarket":Z
    iget v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMarketUid:I

    if-eq v7, v6, :cond_9

    if-ne v7, p2, :cond_9

    .line 1114
    const/4 v5, 0x1

    .line 1115
    :cond_9
    const-string v6, "NetworkSDKService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleMultiAppsDualWifi packName = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " isMarket = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1118
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 1119
    .local v6, "uidStr":Ljava/lang/String;
    if-eqz v5, :cond_a

    .line 1120
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",com.android.providers.downloads"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1121
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDownloadsUid:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1124
    :cond_a
    const/4 v7, 0x2

    if-ne v3, v2, :cond_f

    .line 1125
    if-eqz p1, :cond_c

    .line 1126
    :try_start_1
    iget-object v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v7
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1127
    :try_start_2
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1128
    const-string v0, "NetworkSDKService"

    const-string v2, "handleMultiAppsDualWifi Duplicate Request"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1129
    monitor-exit v7

    return-void

    .line 1132
    :cond_b
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1134
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-virtual {v8, v2, v4, v6, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z

    .line 1136
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDWUidToSlad()V

    .line 1137
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V

    .line 1138
    monitor-exit v7

    goto/16 :goto_7

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local v1    # "isDualWifiOpen":Z
    .end local v3    # "originStatus":I
    .end local v4    # "packName":Ljava/lang/String;
    .end local v5    # "isMarket":Z
    .end local v6    # "uidStr":Ljava/lang/String;
    .end local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .end local p1    # "enable":Z
    .end local p2    # "uid":I
    :try_start_3
    throw v0

    .line 1140
    .restart local v1    # "isDualWifiOpen":Z
    .restart local v3    # "originStatus":I
    .restart local v4    # "packName":Ljava/lang/String;
    .restart local v5    # "isMarket":Z
    .restart local v6    # "uidStr":Ljava/lang/String;
    .restart local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .restart local p1    # "enable":Z
    .restart local p2    # "uid":I
    :cond_c
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 1141
    :try_start_4
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_d

    .line 1142
    const-string v0, "NetworkSDKService"

    const-string v7, "handleMultiAppsDualWifi No request found"

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1143
    monitor-exit v2

    return-void

    .line 1146
    :cond_d
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1147
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-virtual {v8, v7, v4, v6, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z

    .line 1148
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDWUidToSlad()V

    .line 1150
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_e

    .line 1151
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->clearDualWifiStatus()V

    .line 1153
    :cond_e
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V

    .line 1154
    monitor-exit v2

    goto/16 :goto_7

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .end local v1    # "isDualWifiOpen":Z
    .end local v3    # "originStatus":I
    .end local v4    # "packName":Ljava/lang/String;
    .end local v5    # "isMarket":Z
    .end local v6    # "uidStr":Ljava/lang/String;
    .end local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .end local p1    # "enable":Z
    .end local p2    # "uid":I
    :try_start_5
    throw v0

    .line 1157
    .restart local v1    # "isDualWifiOpen":Z
    .restart local v3    # "originStatus":I
    .restart local v4    # "packName":Ljava/lang/String;
    .restart local v5    # "isMarket":Z
    .restart local v6    # "uidStr":Ljava/lang/String;
    .restart local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .restart local p1    # "enable":Z
    .restart local p2    # "uid":I
    :cond_f
    if-eqz p1, :cond_15

    .line 1158
    iget-object v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v7
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 1159
    :try_start_6
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 1160
    const-string v0, "NetworkSDKService"

    const-string v2, "handleMultiAppsDualWifi Duplicate Request"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1161
    monitor-exit v7

    return-void

    .line 1164
    :cond_10
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v8

    if-nez v8, :cond_11

    .line 1165
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    const/4 v9, 0x3

    invoke-virtual {v8, v9, v4, v6, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z

    .line 1166
    invoke-direct {p0, v2, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setOrGetEverOpenedDualWifi(ZZ)Z

    .line 1167
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v8, :cond_12

    .line 1168
    invoke-virtual {v8, v2}, Landroid/net/wifi/MiuiWifiManager;->setSlaveCandidatesRssiThreshold(Z)Z

    goto :goto_2

    .line 1170
    :cond_11
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-virtual {v8, v2, v4, v6, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z

    .line 1171
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-virtual {v8}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDWUidToSlad()V

    .line 1174
    :cond_12
    :goto_2
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1175
    iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z

    .line 1176
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1178
    :try_start_7
    iget-object v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    if-nez v7, :cond_13

    move v7, v0

    goto :goto_3

    :cond_13
    invoke-virtual {v7, p1}, Landroid/net/wifi/SlaveWifiManager;->setWifiSlaveEnabled(Z)Z

    move-result v7

    .line 1179
    .local v7, "res":Z
    :goto_3
    if-eqz p1, :cond_14

    if-eqz v7, :cond_14

    move v0, v2

    :cond_14
    invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnable(Z)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    .line 1180
    .end local v7    # "res":Z
    goto :goto_7

    .line 1176
    :catchall_2
    move-exception v0

    :try_start_8
    monitor-exit v7
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .end local v1    # "isDualWifiOpen":Z
    .end local v3    # "originStatus":I
    .end local v4    # "packName":Ljava/lang/String;
    .end local v5    # "isMarket":Z
    .end local v6    # "uidStr":Ljava/lang/String;
    .end local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .end local p1    # "enable":Z
    .end local p2    # "uid":I
    :try_start_9
    throw v0

    .line 1181
    .restart local v1    # "isDualWifiOpen":Z
    .restart local v3    # "originStatus":I
    .restart local v4    # "packName":Ljava/lang/String;
    .restart local v5    # "isMarket":Z
    .restart local v6    # "uidStr":Ljava/lang/String;
    .restart local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .restart local p1    # "enable":Z
    .restart local p2    # "uid":I
    :cond_15
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v8
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    .line 1182
    :try_start_a
    iget-object v9, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_16

    .line 1183
    const-string v0, "NetworkSDKService"

    const-string v2, "handleMultiAppsDualWifi No request found"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1184
    monitor-exit v8

    return-void

    .line 1186
    :cond_16
    iget-object v9, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1188
    iget-object v9, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    invoke-interface {v9}, Ljava/util/Set;->size()I

    move-result v9

    if-nez v9, :cond_19

    .line 1189
    iget-object v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    const-string v9, ""

    const/4 v10, 0x5

    invoke-virtual {v7, v10, v9, v6, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z

    .line 1191
    invoke-direct {p0, v2, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setOrGetEverOpenedDualWifi(ZZ)Z

    .line 1192
    iget-object v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v7, :cond_17

    .line 1193
    invoke-virtual {v7, v0}, Landroid/net/wifi/MiuiWifiManager;->setSlaveCandidatesRssiThreshold(Z)Z

    .line 1195
    :cond_17
    iget-object v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    if-nez v7, :cond_18

    :goto_4
    goto :goto_5

    :cond_18
    invoke-virtual {v7, p1}, Landroid/net/wifi/SlaveWifiManager;->setWifiSlaveEnabled(Z)Z

    move-result v0

    goto :goto_4

    .line 1196
    .local v0, "res":Z
    :goto_5
    iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z

    .line 1197
    invoke-virtual {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnable(Z)V

    .line 1198
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->clearDualWifiStatus()V

    .line 1199
    .end local v0    # "res":Z
    goto :goto_6

    .line 1200
    :cond_19
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-virtual {v2, v7, v4, v6, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z

    .line 1202
    :goto_6
    monitor-exit v8

    .line 1207
    :goto_7
    goto :goto_8

    .line 1202
    :catchall_3
    move-exception v0

    monitor-exit v8
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .end local v1    # "isDualWifiOpen":Z
    .end local v3    # "originStatus":I
    .end local v4    # "packName":Ljava/lang/String;
    .end local v5    # "isMarket":Z
    .end local v6    # "uidStr":Ljava/lang/String;
    .end local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .end local p1    # "enable":Z
    .end local p2    # "uid":I
    :try_start_b
    throw v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0

    .line 1205
    .restart local v1    # "isDualWifiOpen":Z
    .restart local v3    # "originStatus":I
    .restart local v4    # "packName":Ljava/lang/String;
    .restart local v5    # "isMarket":Z
    .restart local v6    # "uidStr":Ljava/lang/String;
    .restart local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .restart local p1    # "enable":Z
    .restart local p2    # "uid":I
    :catch_0
    move-exception v0

    .line 1206
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1209
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_8
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    if-eqz v0, :cond_1a

    .line 1210
    const-string v0, "NetworkSDKService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "after mDualWifiOriginStatus = "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " mIsDualWifiSDKChanged = "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " mDualWifiApps.size = "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiApps:Ljava/util/Set;

    .line 1212
    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1210
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 1214
    :cond_1a
    const-string v0, "NetworkSDKService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "after mDualWifiOriginStatus = "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " mIsDualWifiSDKChanged = "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1217
    :goto_9
    return-void

    .line 1104
    .end local v4    # "packName":Ljava/lang/String;
    .end local v5    # "isMarket":Z
    .end local v6    # "uidStr":Ljava/lang/String;
    :catchall_4
    move-exception v0

    :try_start_c
    monitor-exit v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    throw v0
.end method

.method private ifaceAddCallBackSend(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 1420
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 1421
    .local v0, "res":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 1423
    .local v1, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 1425
    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-interface {v3, v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->ifaceAdded(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1428
    goto :goto_1

    .line 1426
    :catch_0
    move-exception v3

    .line 1427
    .local v3, "e":Landroid/os/RemoteException;
    const-string v4, "NetworkSDKService"

    const-string v5, "RemoteException at ifaceAddCallBackSend()"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1423
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1430
    .end local v2    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1431
    return-void
.end method

.method private ifaceRemovedCallBackSend(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 1434
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 1435
    .local v0, "res":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 1437
    .local v1, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 1439
    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-interface {v3, v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->ifaceRemoved(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1442
    goto :goto_1

    .line 1440
    :catch_0
    move-exception v3

    .line 1441
    .local v3, "e":Landroid/os/RemoteException;
    const-string v4, "NetworkSDKService"

    const-string v5, "RemoteException at ifaceRemovedCallBackSend()"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1437
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1444
    .end local v2    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1445
    return-void
.end method

.method private initAppTrafficSessionAndSimCardHelper()V
    .locals 1

    .line 860
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSimCardHelper:Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;

    .line 862
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatsSession:Lmiui/securitycenter/net/MiuiNetworkSessionStats;

    if-eqz v0, :cond_0

    .line 863
    invoke-virtual {v0}, Lmiui/securitycenter/net/MiuiNetworkSessionStats;->openSession()V

    goto :goto_0

    .line 865
    :cond_0
    new-instance v0, Lmiui/securitycenter/net/MiuiNetworkSessionStats;

    invoke-direct {v0}, Lmiui/securitycenter/net/MiuiNetworkSessionStats;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatsSession:Lmiui/securitycenter/net/MiuiNetworkSessionStats;

    .line 866
    invoke-virtual {v0}, Lmiui/securitycenter/net/MiuiNetworkSessionStats;->openSession()V

    .line 868
    :goto_0
    return-void
.end method

.method private initBroadcastReceiver()V
    .locals 4

    .line 2351
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2352
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2353
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2354
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2355
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 2356
    new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$10;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$10;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    .line 2411
    .local v1, "receiver":Landroid/content/BroadcastReceiver;
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    const/4 v3, 0x4

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 2412
    return-void
.end method

.method private initCellularNetworkSDKCloudObserver()V
    .locals 4

    .line 2336
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$9;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$9;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Handler;)V

    .line 2342
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2343
    const-string v2, "cellular_networkboost_sdk_white_list_pkg_name"

    invoke-static {v2}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2342
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 2344
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$$ExternalSyntheticLambda0;

    invoke-direct {v2, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$$ExternalSyntheticLambda0;-><init>(Landroid/database/ContentObserver;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2346
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 2347
    return-void
.end method

.method private initCellularWhiteList()V
    .locals 2

    .line 2449
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKPN:Ljava/util/Set;

    .line 2450
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKUid:Ljava/util/Set;

    .line 2451
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKPN:Ljava/util/Set;

    const-string v1, "com.miui.vpnsdkmanager"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2452
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKPN:Ljava/util/Set;

    const-string v1, "com.miui.securityadd"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2453
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setCellularNetworkSDKUid()V

    .line 2454
    const-string v0, "NetworkSDKService"

    const-string v1, "Cellular white list init"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2455
    return-void
.end method

.method private initDualDataBroadcastReceiver()V
    .locals 3

    .line 2415
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2416
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.phone.action.VIDEO_APPS_POLICY_NOTIFY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2417
    const-string v1, "org.codeaurora.intent.action.MSIM_VOICE_CAPABILITY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2418
    new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$11;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$11;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    .line 2437
    .local v1, "receiver":Landroid/content/BroadcastReceiver;
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2438
    return-void
.end method

.method private initMibridgeCloudObserver()V
    .locals 4

    .line 2320
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$8;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$8;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Handler;)V

    .line 2326
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2327
    const-string v2, "mibridge_authorized_pkg_list"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2326
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 2328
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$$ExternalSyntheticLambda1;

    invoke-direct {v2, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$$ExternalSyntheticLambda1;-><init>(Landroid/database/ContentObserver;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2330
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 2331
    return-void
.end method

.method private initNetworkSDKCloudObserver()V
    .locals 4

    .line 2306
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$7;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$7;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Handler;)V

    .line 2312
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2313
    const-string v2, "cloud_networkboost_whitelist"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2312
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 2314
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$$ExternalSyntheticLambda2;

    invoke-direct {v2, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$$ExternalSyntheticLambda2;-><init>(Landroid/database/ContentObserver;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2316
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 2317
    return-void
.end method

.method private initPermission()V
    .locals 5

    .line 2278
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    .line 2279
    const-string/jumbo v1, "setSockPrio"

    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2280
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string/jumbo v1, "setTCPCongestion"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2281
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string v1, "isSupportDualWifi"

    const-string v3, "android.permission.ACCESS_WIFI_STATE"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2282
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string/jumbo v1, "setSlaveWifiEnabled"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2283
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string v1, "connectSlaveWifi"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2284
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string v1, "disconnectSlaveWifi"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2285
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string/jumbo v1, "suspendBackgroundScan"

    const-string v4, "android.permission.CHANGE_WIFI_STATE"

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2286
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string v1, "resumeBackgroundScan"

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2287
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string/jumbo v1, "suspendWifiPowerSave"

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2288
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string v1, "resumeWifiPowerSave"

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2289
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string v1, "registerWifiLinkCallback"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2290
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string/jumbo v1, "unregisterWifiLinkCallback"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2291
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string v1, "getAvailableIfaces"

    const-string v4, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2292
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string v1, "getQoEByAvailableIfaceName"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2293
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string/jumbo v1, "setTrafficTransInterface"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2294
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string v1, "requestAppTrafficStatistics"

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2295
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string v1, "registerNetLinkCallback"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2296
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string/jumbo v1, "unregisterNetLinkCallback"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2297
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string v1, "enableWifiSelectionOpt"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2298
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string/jumbo v1, "triggerWifiSelection"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2299
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string v1, "reportBssidScore"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2300
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string v1, "disableWifiSelectionOpt"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2301
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    const-string v1, "isSlaveWifiEnabledAndOthersOpt"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2302
    return-void
.end method

.method private initWhiteList()V
    .locals 2

    .line 2441
    invoke-static {}, Ljava/util/concurrent/ConcurrentHashMap;->newKeySet()Ljava/util/concurrent/ConcurrentHashMap$KeySetView;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKPN:Ljava/util/Set;

    .line 2442
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKUid:Ljava/util/Set;

    .line 2443
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKPN:Ljava/util/Set;

    const-string v1, "com.android.settings"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2444
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setNetworkSDKUid()V

    .line 2445
    const-string v0, "NetworkSDKService"

    const-string/jumbo v1, "white list init"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2446
    return-void
.end method

.method private isSystemProcess(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 2273
    const/16 v0, 0x2710

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic lambda$initCellularNetworkSDKCloudObserver$2(Landroid/database/ContentObserver;)V
    .locals 1
    .param p0, "observer"    # Landroid/database/ContentObserver;

    .line 2345
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 2346
    return-void
.end method

.method static synthetic lambda$initMibridgeCloudObserver$1(Landroid/database/ContentObserver;)V
    .locals 1
    .param p0, "observer"    # Landroid/database/ContentObserver;

    .line 2329
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 2330
    return-void
.end method

.method static synthetic lambda$initNetworkSDKCloudObserver$0(Landroid/database/ContentObserver;)V
    .locals 1
    .param p0, "observer"    # Landroid/database/ContentObserver;

    .line 2315
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 2316
    return-void
.end method

.method private mediaPlayerPolicyCallBackSend(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .line 1480
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 1481
    .local v0, "type":I
    iget v1, p1, Landroid/os/Message;->arg2:I

    .line 1482
    .local v1, "duration":I
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1483
    .local v2, "length":I
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v3

    .line 1485
    .local v3, "N":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_0

    .line 1487
    :try_start_0
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v5, v4}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-interface {v5, v0, v1, v2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->mediaPlayerPolicyNotify(III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1490
    goto :goto_1

    .line 1488
    :catch_0
    move-exception v5

    .line 1489
    .local v5, "e":Landroid/os/RemoteException;
    const-string v6, "NetworkSDKService"

    const-string v7, "RemoteException at mediaPlayerPolicyCallBackSend()"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1485
    .end local v5    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1492
    .end local v4    # "i":I
    :cond_0
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1493
    return-void
.end method

.method private mibridgeWhiteList()Ljava/lang/String;
    .locals 2

    .line 2553
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    .line 2554
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2553
    const-string v1, "mibridge_authorized_pkg_list"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private netLinkCallBackUnRegisterError()V
    .locals 15

    .line 1613
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1614
    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1615
    .local v1, "appStatsPollMillis":Ljava/util/Map;, "Ljava/util/Map<Landroid/os/IBinder;Ljava/lang/Long;>;"
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1616
    .local v2, "appStatsPollInterval":Ljava/util/Map;, "Ljava/util/Map<Landroid/os/IBinder;Ljava/lang/Long;>;"
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1618
    .local v3, "appCallBackMapToUid":Ljava/util/Map;, "Ljava/util/Map<Landroid/os/IBinder;Ljava/lang/Integer;>;"
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWlanQoECallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v4

    .line 1619
    .local v4, "count":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 1620
    .local v5, "curSystemTime":J
    const-wide/16 v7, -0x1

    .line 1621
    .local v7, "minInterval":J
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    const-wide/16 v10, -0x1

    if-ge v9, v4, :cond_2

    .line 1622
    iget-object v12, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWlanQoECallbacks:Landroid/os/RemoteCallbackList;

    .line 1623
    invoke-virtual {v12, v9}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackItem(I)Landroid/os/IInterface;

    move-result-object v12

    check-cast v12, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;

    .line 1624
    .local v12, "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    invoke-interface {v12}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;->asBinder()Landroid/os/IBinder;

    move-result-object v13

    .line 1625
    .local v13, "callBackKey":Landroid/os/IBinder;
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-interface {v1, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1626
    iget-object v14, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollInterval:Ljava/util/Map;

    invoke-interface {v14, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Long;

    invoke-interface {v2, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1627
    iget-object v14, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppCallBackMapToUid:Ljava/util/Map;

    invoke-interface {v14, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-interface {v3, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1628
    cmp-long v10, v7, v10

    if-nez v10, :cond_0

    .line 1629
    iget-object v10, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollInterval:Ljava/util/Map;

    invoke-interface {v10, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    move-wide v7, v10

    goto :goto_1

    .line 1630
    :cond_0
    iget-object v10, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollInterval:Ljava/util/Map;

    invoke-interface {v10, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v10, v10, v7

    if-gez v10, :cond_1

    .line 1631
    iget-object v10, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollInterval:Ljava/util/Map;

    invoke-interface {v10, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    move-wide v7, v10

    .line 1621
    .end local v12    # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    .end local v13    # "callBackKey":Landroid/os/IBinder;
    :cond_1
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 1634
    .end local v9    # "i":I
    :cond_2
    cmp-long v9, v7, v10

    if-eqz v9, :cond_3

    .line 1635
    iput-wide v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J

    .line 1636
    const-string v9, "NetworkSDKService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "netLinkCallBackUnRegisterError interval = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-wide v13, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1639
    :cond_3
    if-nez v4, :cond_4

    .line 1640
    const-string v9, "NetworkSDKService"

    const-string v12, "netLinkCallBackUnRegisterError interval = -1"

    invoke-static {v9, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1641
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsOpenWifiLinkPoll:Z

    .line 1642
    iput-wide v10, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J

    .line 1645
    :cond_4
    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollMillis:Ljava/util/Map;

    .line 1646
    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollInterval:Ljava/util/Map;

    .line 1647
    iput-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppCallBackMapToUid:Ljava/util/Map;

    .line 1648
    .end local v1    # "appStatsPollMillis":Ljava/util/Map;, "Ljava/util/Map<Landroid/os/IBinder;Ljava/lang/Long;>;"
    .end local v2    # "appStatsPollInterval":Ljava/util/Map;, "Ljava/util/Map<Landroid/os/IBinder;Ljava/lang/Long;>;"
    .end local v3    # "appCallBackMapToUid":Ljava/util/Map;, "Ljava/util/Map<Landroid/os/IBinder;Ljava/lang/Integer;>;"
    .end local v4    # "count":I
    .end local v5    # "curSystemTime":J
    .end local v7    # "minInterval":J
    monitor-exit v0

    .line 1649
    return-void

    .line 1648
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private networkPriorityCallBackSend(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .line 1463
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 1464
    .local v0, "priorityMode":I
    iget v1, p1, Landroid/os/Message;->arg2:I

    .line 1465
    .local v1, "trafficPolicy":I
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1466
    .local v2, "thermalLevel":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "networkPriorityCallBackNotice: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "NetworkSDKService"

    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1467
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v3

    .line 1469
    .local v3, "N":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v3, :cond_0

    .line 1471
    :try_start_0
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v6, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    check-cast v6, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-interface {v6, v0, v1, v2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onNetworkPriorityChanged(III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1474
    goto :goto_1

    .line 1472
    :catch_0
    move-exception v6

    .line 1473
    .local v6, "e":Landroid/os/RemoteException;
    const-string v7, "RemoteException at networkPriorityCallBackSend()"

    invoke-static {v4, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1469
    .end local v6    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1476
    .end local v5    # "i":I
    :cond_0
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1477
    return-void
.end method

.method private onScanSuccussed(I)V
    .locals 2
    .param p1, "result"    # I

    .line 1342
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1343
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1344
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1345
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1346
    return-void
.end method

.method private onSlaveWifiEnableV1(I)V
    .locals 2
    .param p1, "type"    # I

    .line 1335
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1336
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x3f8

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1337
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1338
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1339
    return-void
.end method

.method private registDualWifiStateBroadcastReceiver()V
    .locals 5

    .line 1240
    const-string v0, "NetworkSDKService"

    const-string v1, "registDualWifiStateBroadcastReceiver"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1241
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1242
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.WIFI_SLAVE_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1243
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1244
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1246
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$6;

    invoke-direct {v2, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$6;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V

    const/4 v4, 0x2

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 1262
    return-void
.end method

.method private registerQEEChangeObserver()V
    .locals 4

    .line 2774
    const-string v0, "NetworkSDKService"

    const-string v1, "QEE cloud observer register"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2775
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$12;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$12;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mQEEObserver:Landroid/database/ContentObserver;

    .line 2793
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2795
    const-string v1, "cloud_qee_enable"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mQEEObserver:Landroid/database/ContentObserver;

    .line 2794
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2800
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$13;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$13;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2806
    return-void
.end method

.method private resetMonitorStatus()V
    .locals 2

    .line 1754
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1755
    const/4 v1, 0x0

    :try_start_0
    iput v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiBackgroundCount:I

    .line 1756
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsStartMonitorByBackground:Z

    .line 1757
    monitor-exit v0

    .line 1758
    return-void

    .line 1757
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private resetQoECallback(Landroid/os/IBinder;J)V
    .locals 2
    .param p1, "callBackKey"    # Landroid/os/IBinder;
    .param p2, "curSystemTime"    # J

    .line 1822
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollMillis:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1823
    const-string v0, "NetworkSDKService"

    const-string/jumbo v1, "wifiLinkStatsCallbackSend: resetOneQoECallback"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1824
    return-void
.end method

.method private setCellularNetworkSDKUid()V
    .locals 10

    .line 2559
    const-string v0, ""

    .line 2561
    .local v0, "cellularWhiteString_netSDK":Ljava/lang/String;
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "cellular_networkboost_sdk_white_list_pkg_name"

    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 2565
    goto :goto_0

    .line 2563
    :catch_0
    move-exception v1

    .line 2564
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "NetworkSDKService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get Cellular cloud whitelist error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2567
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    const-string v1, "NetworkSDKService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cloud CellularNetworkSDKApp:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2568
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2569
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2570
    .local v1, "packages":[Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 2571
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v3, v1

    if-ge v2, v3, :cond_0

    .line 2572
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKPN:Ljava/util/Set;

    aget-object v4, v1, v2

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2573
    const-string v3, "NetworkSDKService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setCellularNetworkSDKUid packages:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2571
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2579
    .end local v1    # "packages":[Ljava/lang/String;
    .end local v2    # "i":I
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2580
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v2

    .line 2581
    .local v2, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageInfo;

    .line 2582
    .local v4, "app":Landroid/content/pm/PackageInfo;
    iget-object v5, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v5, :cond_1

    iget-object v5, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKPN:Ljava/util/Set;

    iget-object v6, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 2583
    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2584
    iget-object v5, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 2585
    .local v5, "uid":I
    sget-object v6, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularListLock:Ljava/lang/Object;

    monitor-enter v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2586
    :try_start_2
    iget-object v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKUid:Ljava/util/Set;

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2587
    const-string v7, "NetworkSDKService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "setCellularNetworkSDKUid uid :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2588
    monitor-exit v6

    goto :goto_3

    :catchall_0
    move-exception v3

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local v0    # "cellularWhiteString_netSDK":Ljava/lang/String;
    .end local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    :try_start_3
    throw v3
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 2590
    .end local v4    # "app":Landroid/content/pm/PackageInfo;
    .end local v5    # "uid":I
    .restart local v0    # "cellularWhiteString_netSDK":Ljava/lang/String;
    .restart local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    :cond_1
    :goto_3
    goto :goto_2

    .line 2593
    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    .end local v2    # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_2
    goto :goto_4

    .line 2591
    :catch_1
    move-exception v1

    .line 2592
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "NetworkSDKService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setCellularNetworkSDKUid error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2595
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_4
    const-string v1, ""

    .line 2596
    .local v1, "white_list":Ljava/lang/String;
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKPN:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2597
    .local v3, "pn":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2598
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2599
    .end local v3    # "pn":Ljava/lang/String;
    goto :goto_5

    .line 2600
    :cond_3
    sget-object v2, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularListLock:Ljava/lang/Object;

    monitor-enter v2

    .line 2601
    :try_start_4
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKUid:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2602
    .local v4, "uid":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v1, v5

    .line 2603
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v1, v5

    .line 2604
    .end local v4    # "uid":Ljava/lang/String;
    goto :goto_6

    .line 2605
    :cond_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2606
    const-string v2, "NetworkSDKService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setCellularNetworkSDKUid :white list pn&uid : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2607
    return-void

    .line 2605
    :catchall_1
    move-exception v3

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v3
.end method

.method private setFrameworkAndDriverRoaming(ZII)Z
    .locals 5
    .param p1, "isOpen"    # Z
    .param p2, "type"    # I
    .param p3, "uid"    # I

    .line 2024
    const/4 v0, 0x0

    .line 2025
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 2026
    const/4 v2, 0x1

    if-nez p1, :cond_2

    .line 2027
    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiSelectionApps:Ljava/util/Set;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2028
    iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z

    if-eqz v3, :cond_0

    .line 2029
    monitor-exit v1

    return v2

    .line 2032
    :cond_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v3, :cond_1

    const/4 v4, 0x3

    if-eq p2, v4, :cond_1

    .line 2033
    invoke-virtual {v3, p1, p2}, Landroid/net/wifi/MiuiWifiManager;->netSDKSetIsDisableRoam(ZI)Z

    move-result v3

    move v0, v3

    goto :goto_0

    .line 2035
    :cond_1
    const/4 v0, 0x1

    .line 2037
    :goto_0
    iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z

    .line 2038
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    const/16 v3, 0x3f2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 2040
    :cond_2
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiSelectionApps:Ljava/util/Set;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2042
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiSelectionApps:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 2043
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z

    .line 2044
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    const/4 v3, -0x1

    invoke-virtual {v2, p1, v3}, Landroid/net/wifi/MiuiWifiManager;->netSDKSetIsDisableRoam(ZI)Z

    move-result v2

    move v0, v2

    .line 2049
    :goto_1
    monitor-exit v1

    .line 2051
    return v0

    .line 2046
    :cond_3
    monitor-exit v1

    return v2

    .line 2049
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private setNetworkSDKUid()V
    .locals 10

    .line 2483
    const-string v0, ""

    .line 2485
    .local v0, "whiteString_netSDK":Ljava/lang/String;
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    .line 2486
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "cloud_networkboost_whitelist"

    .line 2485
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 2489
    goto :goto_0

    .line 2487
    :catch_0
    move-exception v1

    .line 2488
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "NetworkSDKService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get cloud whitelist error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2491
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    const-string v1, "NetworkSDKService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cloud whiteString_netSDK networkSDKApp:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2492
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2493
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2494
    .local v1, "packages":[Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 2495
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v3, v1

    if-ge v2, v3, :cond_0

    .line 2496
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKPN:Ljava/util/Set;

    aget-object v4, v1, v2

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2495
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2501
    .end local v1    # "packages":[Ljava/lang/String;
    .end local v2    # "i":I
    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mibridgeWhiteList()Ljava/lang/String;

    move-result-object v1

    .line 2502
    .local v1, "whiteString_mibridge":Ljava/lang/String;
    const-string v2, "NetworkSDKService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cloud whiteString_mibridge networkSDKApp:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2503
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2504
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2505
    .local v2, "packages":[Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 2506
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    array-length v4, v2

    if-ge v3, v4, :cond_1

    .line 2507
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKPN:Ljava/util/Set;

    aget-object v5, v2, v3

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2506
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2514
    .end local v2    # "packages":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 2515
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v3

    .line 2516
    .local v3, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/PackageInfo;

    .line 2517
    .local v5, "app":Landroid/content/pm/PackageInfo;
    iget-object v6, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKPN:Ljava/util/Set;

    iget-object v7, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 2518
    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2519
    iget-object v6, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 2520
    .local v6, "uid":I
    sget-object v7, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mListLock:Ljava/lang/Object;

    monitor-enter v7
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2521
    :try_start_2
    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKUid:Ljava/util/Set;

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2522
    monitor-exit v7

    goto :goto_4

    :catchall_0
    move-exception v4

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local v0    # "whiteString_netSDK":Ljava/lang/String;
    .end local v1    # "whiteString_mibridge":Ljava/lang/String;
    .end local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    :try_start_3
    throw v4

    .line 2525
    .end local v6    # "uid":I
    .restart local v0    # "whiteString_netSDK":Ljava/lang/String;
    .restart local v1    # "whiteString_mibridge":Ljava/lang/String;
    .restart local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    :cond_2
    :goto_4
    iget v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMarketUid:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_3

    iget-object v6, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v8, "com.xiaomi.market"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2526
    iget-object v6, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMarketUid:I

    .line 2527
    :cond_3
    iget v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDownloadsUid:I

    if-ne v6, v7, :cond_4

    iget-object v6, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v7, "com.android.providers.downloads"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2528
    iget-object v6, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDownloadsUid:I

    .line 2530
    :cond_4
    if-eqz v5, :cond_5

    iget-object v6, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v6, :cond_5

    iget-object v6, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v6, :cond_5

    .line 2531
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppUidToPackName:Ljava/util/Map;

    iget-object v7, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iget-object v8, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 2532
    .end local v5    # "app":Landroid/content/pm/PackageInfo;
    :cond_5
    goto :goto_3

    .line 2535
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    .end local v3    # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_6
    goto :goto_5

    .line 2533
    :catch_1
    move-exception v2

    .line 2534
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "NetworkSDKService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setNetworkSDKUid error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2538
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_5
    const-string v2, ""

    .line 2539
    .local v2, "white_list":Ljava/lang/String;
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKPN:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2540
    .local v4, "pn":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2541
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2542
    .end local v4    # "pn":Ljava/lang/String;
    goto :goto_6

    .line 2543
    :cond_7
    sget-object v3, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mListLock:Ljava/lang/Object;

    monitor-enter v3

    .line 2544
    :try_start_4
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKUid:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2545
    .local v5, "uid":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v2, v6

    .line 2546
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v2, v6

    .line 2547
    .end local v5    # "uid":Ljava/lang/String;
    goto :goto_7

    .line 2548
    :cond_8
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2549
    const-string v3, "NetworkSDKService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "white list pn&uid : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2550
    return-void

    .line 2548
    :catchall_1
    move-exception v4

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v4
.end method

.method private setOrGetEverOpenedDualWifi(ZZ)Z
    .locals 4
    .param p1, "isSet"    # Z
    .param p2, "isEverOpened"    # Z

    .line 1220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleMultiAppsDualWifi isSet = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isEverOpened = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NetworkSDKService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1222
    const-string v0, "is_opened_dual_wifi"

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    .line 1223
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v0, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1225
    return v1

    .line 1227
    :cond_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    move v1, v3

    :goto_0
    return v1
.end method

.method private setScreenStatus(Z)V
    .locals 3
    .param p1, "isScreenON"    # Z

    .line 1410
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1411
    :try_start_0
    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsScreenON:Z

    .line 1412
    if-nez p1, :cond_0

    .line 1413
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScreenOffSystemTime:J

    goto :goto_0

    .line 1415
    :cond_0
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScreenOffSystemTime:J

    .line 1416
    :goto_0
    monitor-exit v0

    .line 1417
    return-void

    .line 1416
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private unregisterQEEChangeObserver()V
    .locals 2

    .line 2809
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mQEEObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 2810
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mQEEObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 2811
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mQEEObserver:Landroid/database/ContentObserver;

    .line 2813
    :cond_0
    return-void
.end method

.method private upDateCloudWhiteList()V
    .locals 2

    .line 2458
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKPN:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2459
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppUidToPackName:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2460
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKPN:Ljava/util/Set;

    const-string v1, "com.android.settings"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2461
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mListLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2462
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKUid:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 2463
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2464
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setNetworkSDKUid()V

    .line 2466
    const-string v0, "NetworkSDKService"

    const-string v1, "cloud white list changed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2467
    return-void

    .line 2463
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private updataCellularSdkList()V
    .locals 2

    .line 2470
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKPN:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2471
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKPN:Ljava/util/Set;

    const-string v1, "com.miui.vpnsdkmanager"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2472
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKPN:Ljava/util/Set;

    const-string v1, "com.miui.securityadd"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2473
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularListLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2474
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKUid:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 2475
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2476
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setCellularNetworkSDKUid()V

    .line 2478
    const-string v0, "NetworkSDKService"

    const-string v1, "cloud Cellular white list changed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2479
    return-void

    .line 2475
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private wifiLinkLayerStatsPoll()V
    .locals 4

    .line 1584
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1585
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWlanQoECallbacks:Landroid/os/RemoteCallbackList;

    if-eqz v1, :cond_4

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollInterval:Ljava/util/Map;

    if-nez v2, :cond_0

    goto :goto_0

    .line 1587
    :cond_0
    iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsOpenWifiLinkPoll:Z

    if-nez v2, :cond_1

    .line 1588
    const-string v1, "NetworkSDKService"

    const-string/jumbo v2, "wifiLinkLayerStatsPoll polling normal exit"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1589
    monitor-exit v0

    return-void

    .line 1592
    :cond_1
    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v1

    .line 1593
    .local v1, "count":I
    if-nez v1, :cond_2

    .line 1594
    const-string v2, "NetworkSDKService"

    const-string/jumbo v3, "wifiLinkLayerStatsPoll polling Self-monitoring exit"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1595
    monitor-exit v0

    return-void

    .line 1597
    :cond_2
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollInterval:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 1598
    const-string v2, "NetworkSDKService"

    const-string/jumbo v3, "wifiLinkLayerStatsPoll netLinkQoE callBack unregister Error polling exit"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1599
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->netLinkCallBackUnRegisterError()V

    .line 1601
    .end local v1    # "count":I
    :cond_3
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1603
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->wifiLinkStatsCallbackSend()V

    .line 1605
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    .line 1606
    const/16 v1, 0x3f1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J

    .line 1605
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1609
    return-void

    .line 1586
    :cond_4
    :goto_0
    :try_start_1
    monitor-exit v0

    return-void

    .line 1601
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private wifiLinkStatsCallbackSend()V
    .locals 20

    .line 1782
    move-object/from16 v1, p0

    const-string/jumbo v0, "wlan0"

    invoke-virtual {v1, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->getQoEByAvailableIfaceNameV1(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    move-result-object v2

    .line 1783
    .local v2, "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    const-string/jumbo v0, "wlan1"

    invoke-virtual {v1, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->getQoEByAvailableIfaceNameV1(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    move-result-object v3

    .line 1784
    .local v3, "statsWlan1":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    iget-object v4, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 1785
    :try_start_0
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWlanQoECallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v5, v0

    .line 1786
    .local v5, "N":I
    const/4 v0, 0x0

    move v6, v0

    .local v6, "i":I
    :goto_0
    if-ge v6, v5, :cond_2

    .line 1788
    :try_start_1
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWlanQoECallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v6}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;

    .line 1789
    .local v0, "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    invoke-interface {v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;->asBinder()Landroid/os/IBinder;

    move-result-object v7

    .line 1791
    .local v7, "callBackKey":Landroid/os/IBinder;
    iget-object v8, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollInterval:Ljava/util/Map;

    invoke-interface {v8, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 1792
    .local v8, "appQoESendInterval":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 1793
    .local v10, "curSystemTime":J
    iget-object v12, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollMillis:Ljava/util/Map;

    invoke-interface {v12, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Long;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    sub-long v12, v10, v12

    .line 1794
    .local v12, "interval":J
    const-wide/16 v14, 0x64

    div-long v14, v8, v14

    const-wide/16 v16, 0x21

    mul-long v14, v14, v16

    const-wide/16 v16, 0x32

    add-long v14, v14, v16

    .line 1803
    .local v14, "deviation":J
    cmp-long v16, v12, v8

    if-ltz v16, :cond_0

    add-long v16, v8, v14

    cmp-long v16, v12, v16

    if-gtz v16, :cond_0

    .line 1805
    invoke-interface {v0, v2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;->masterQoECallBack(Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;)V

    .line 1806
    invoke-interface {v0, v3}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;->slaveQoECallBack(Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;)V

    .line 1807
    move-object/from16 v16, v0

    .end local v0    # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    .local v16, "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollMillis:Ljava/util/Map;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v17, v2

    .end local v2    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .local v17, "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    :try_start_2
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1803
    .end local v16    # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    .end local v17    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .restart local v0    # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    .restart local v2    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    :cond_0
    move-object/from16 v16, v0

    move-object/from16 v17, v2

    .line 1809
    .end local v0    # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    .end local v2    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .restart local v16    # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    .restart local v17    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    add-long v18, v8, v14

    cmp-long v0, v12, v18

    if-lez v0, :cond_1

    .line 1810
    invoke-direct {v1, v7, v10, v11}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->resetQoECallback(Landroid/os/IBinder;J)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 1812
    .end local v7    # "callBackKey":Landroid/os/IBinder;
    .end local v8    # "appQoESendInterval":J
    .end local v10    # "curSystemTime":J
    .end local v12    # "interval":J
    .end local v14    # "deviation":J
    .end local v16    # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    :catch_0
    move-exception v0

    goto :goto_2

    .line 1815
    :cond_1
    :goto_1
    goto :goto_3

    .line 1812
    .end local v17    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .restart local v2    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    :catch_1
    move-exception v0

    move-object/from16 v17, v2

    .line 1813
    .end local v2    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .local v0, "e":Landroid/os/RemoteException;
    .restart local v17    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    :goto_2
    :try_start_3
    const-string v2, "NetworkSDKService"

    const-string v7, "RemoteException at wifiLinkStatsCallbackSend()"

    invoke-static {v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1814
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1786
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_3
    add-int/lit8 v6, v6, 0x1

    move-object/from16 v2, v17

    goto :goto_0

    .end local v17    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .restart local v2    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    :cond_2
    move-object/from16 v17, v2

    .line 1817
    .end local v2    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .end local v6    # "i":I
    .restart local v17    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    iget-object v0, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWlanQoECallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1818
    .end local v5    # "N":I
    monitor-exit v4

    .line 1819
    return-void

    .line 1818
    .end local v17    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .restart local v2    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    :catchall_0
    move-exception v0

    move-object/from16 v17, v2

    .end local v2    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .restart local v17    # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    :goto_4
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_4
.end method


# virtual methods
.method public abortScan()Z
    .locals 2

    .line 667
    const-string v0, "NetworkSDKService"

    const-string v1, "abortScan: "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanner:Landroid/net/wifi/WifiScanner;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanListener:Landroid/net/wifi/WifiScanner$ScanListener;

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiScanner;->stopScan(Landroid/net/wifi/WifiScanner$ScanListener;)V

    .line 669
    const/4 v0, 0x1

    return v0
.end method

.method public activeScan([I)Z
    .locals 3
    .param p1, "channelList"    # [I

    .line 645
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "activeScan: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NetworkSDKService"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanSettings:Landroid/net/wifi/WifiScanner$ScanSettings;

    const/4 v1, 0x2

    iput v1, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->type:I

    .line 647
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanSettings:Landroid/net/wifi/WifiScanner$ScanSettings;

    const/4 v1, 0x0

    iput v1, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->band:I

    .line 648
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanSettings:Landroid/net/wifi/WifiScanner$ScanSettings;

    invoke-static {p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->channelsToSpec([I)[Landroid/net/wifi/WifiScanner$ChannelSpec;

    move-result-object v2

    iput-object v2, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->channels:[Landroid/net/wifi/WifiScanner$ChannelSpec;

    .line 649
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanSettings:Landroid/net/wifi/WifiScanner$ScanSettings;

    const/16 v2, 0x2710

    iput v2, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->periodInMs:I

    .line 650
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanSettings:Landroid/net/wifi/WifiScanner$ScanSettings;

    const/16 v2, 0x14

    iput v2, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->numBssidsPerScan:I

    .line 651
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanSettings:Landroid/net/wifi/WifiScanner$ScanSettings;

    iput v1, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->maxScansToCache:I

    .line 652
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanSettings:Landroid/net/wifi/WifiScanner$ScanSettings;

    const/4 v1, 0x3

    iput v1, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->reportEvents:I

    .line 654
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanner:Landroid/net/wifi/WifiScanner;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanSettings:Landroid/net/wifi/WifiScanner$ScanSettings;

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanListener:Landroid/net/wifi/WifiScanner$ScanListener;

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiScanner;->startScan(Landroid/net/wifi/WifiScanner$ScanSettings;Landroid/net/wifi/WifiScanner$ScanListener;)V

    .line 655
    const/4 v0, 0x1

    return v0
.end method

.method public cellularNetworkSDKPermissionCheck(Ljava/lang/String;)Z
    .locals 8
    .param p1, "callingName"    # Ljava/lang/String;

    .line 2249
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 2250
    .local v0, "uid":I
    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSystemProcess(I)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    return v2

    .line 2251
    :cond_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 2252
    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCellularNetworkSDKUid:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_3

    .line 2253
    const-string v3, "NetworkSDKService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WhiteList passed(cellular): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2258
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2259
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2260
    .local v1, "permission":Ljava/lang/String;
    if-nez v1, :cond_1

    return v2

    .line 2261
    :cond_1
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    .line 2262
    .local v3, "pid":I
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v1, v3, v0}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v5

    .line 2263
    .local v5, "checkResult":I
    if-nez v5, :cond_2

    .line 2264
    const-string v4, "NetworkSDKService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "permission granted(cellular): "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2265
    return v2

    .line 2267
    :cond_2
    const-string v2, "NetworkSDKService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "permission denied(cellular): "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2268
    return v4

    .line 2255
    .end local v1    # "permission":Ljava/lang/String;
    .end local v3    # "pid":I
    .end local v5    # "checkResult":I
    :cond_3
    :try_start_1
    const-string v2, "NetworkSDKService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "WhiteList denied(cellular): "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2256
    monitor-exit v1

    return v4

    .line 2258
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public connectSlaveWifi(I)Z
    .locals 2
    .param p1, "networkId"    # I

    .line 558
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "connectSlaveWifi: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NetworkSDKService"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    const-string v0, "connectSlaveWifi"

    invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 561
    const/4 v0, 0x0

    return v0

    .line 563
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 564
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x65

    iput v1, v0, Landroid/os/Message;->what:I

    .line 565
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 566
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 567
    const/4 v1, 0x1

    return v1
.end method

.method public disableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;)Z
    .locals 2
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;

    .line 2002
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 2003
    .local v0, "uid":I
    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->disableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;I)Z

    move-result v1

    return v1
.end method

.method public disableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;I)Z
    .locals 4
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;
    .param p2, "uid"    # I

    .line 2007
    const-string v0, "disableWifiSelectionOpt"

    invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2008
    const-string v0, "NetworkSDKService"

    const-string v1, "disableWifiSelectionOpt No permission"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2009
    const/4 v0, 0x0

    return v0

    .line 2012
    :cond_0
    const/4 v0, 0x0

    .line 2014
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 2015
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 2017
    const/4 v2, 0x1

    const/4 v3, -0x1

    invoke-direct {p0, v2, v3, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setFrameworkAndDriverRoaming(ZII)Z

    move-result v2

    move v0, v2

    .line 2018
    monitor-exit v1

    .line 2020
    return v0

    .line 2018
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public disconnectSlaveWifi()Z
    .locals 2

    .line 571
    const-string v0, "NetworkSDKService"

    const-string v1, "disconnectSlaveWifi: "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    const-string v0, "disconnectSlaveWifi"

    invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 574
    const/4 v0, 0x0

    return v0

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 577
    const/4 v0, 0x1

    return v0
.end method

.method public dumpModule(Ljava/io/PrintWriter;)V
    .locals 6
    .param p1, "writer"    # Ljava/io/PrintWriter;

    .line 2755
    const-string v0, "NetworkSDKService end.\n"

    const-string v1, "    "

    .line 2757
    .local v1, "spacePrefix":Ljava/lang/String;
    :try_start_0
    const-string v2, "NetworkSDKService begin:"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2758
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "isConnectSlaveAp:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isConnectSlaveAp:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2759
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2770
    goto :goto_0

    .line 2761
    :catch_0
    move-exception v2

    .line 2762
    .local v2, "ex":Ljava/lang/Exception;
    const-string v3, "dump failed!"

    const-string v4, "NetworkSDKService"

    invoke-static {v4, v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2764
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Dump of NetworkSDKService failed:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2765
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2769
    goto :goto_0

    .line 2767
    :catch_1
    move-exception v0

    .line 2768
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "dump failure failed:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2771
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "ex":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public enableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;I)Z
    .locals 2
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;
    .param p2, "type"    # I

    .line 1830
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 1831
    .local v0, "uid":I
    invoke-virtual {p0, p1, p2, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->enableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;II)Z

    move-result v1

    return v1
.end method

.method public enableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;II)Z
    .locals 4
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;
    .param p2, "type"    # I
    .param p3, "uid"    # I

    .line 1835
    const/4 v0, 0x0

    if-ltz p2, :cond_2

    const/4 v1, 0x3

    if-le p2, v1, :cond_0

    goto :goto_0

    .line 1838
    :cond_0
    const-string v1, "enableWifiSelectionOpt"

    invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1839
    const-string v1, "NetworkSDKService"

    const-string v2, "enableWifiSelectionOpt No permission"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1840
    return v0

    .line 1842
    :cond_1
    const/4 v1, 0x0

    .line 1844
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 1845
    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 1846
    invoke-direct {p0, v0, p2, p3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setFrameworkAndDriverRoaming(ZII)Z

    move-result v0

    move v1, v0

    .line 1847
    monitor-exit v2

    .line 1849
    return v1

    .line 1847
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1836
    .end local v1    # "ret":Z
    :cond_2
    :goto_0
    return v0
.end method

.method public getAvailableIfaces()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 581
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 583
    .local v0, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "getAvailableIfaces"

    .line 584
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "code"

    if-nez v2, :cond_0

    .line 585
    const-string v2, "1001"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 586
    const-string v2, "message"

    const-string v3, "No permission"

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 587
    return-object v0

    .line 590
    :cond_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->getAvailableIfaces()Ljava/util/List;

    move-result-object v2

    .line 591
    .local v2, "ifacesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v4, ""

    .line 592
    .local v4, "ifaces":Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_2

    .line 593
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 594
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-eq v5, v6, :cond_1

    .line 595
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 592
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 598
    .end local v5    # "i":I
    :cond_2
    const-string v5, "1000"

    invoke-interface {v0, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 599
    const-string v3, "result"

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 601
    return-object v0
.end method

.method public getQoEByAvailableIfaceName(Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .param p1, "ifaceName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1497
    const/4 v0, 0x0

    return-object v0
.end method

.method public getQoEByAvailableIfaceNameV1(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .locals 6
    .param p1, "ifaceName"    # Ljava/lang/String;

    .line 1501
    const-string v0, "getQoEByAvailableIfaceName"

    invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 1502
    const-string v0, "NetworkSDKService"

    const-string v2, "getQoEByAvailableIfaceNameV1 No permission"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1503
    return-object v1

    .line 1507
    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsScreenON:Z

    if-nez v0, :cond_1

    .line 1508
    return-object v1

    .line 1510
    :cond_1
    const/4 v0, 0x0

    .line 1511
    .local v0, "netLayerQoE":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    const-string/jumbo v2, "wlan0"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1512
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMasterNetLayerQoE:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    goto :goto_0

    .line 1513
    :cond_2
    const-string/jumbo v2, "wlan1"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1514
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaveNetLayerQoE:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    .line 1518
    :goto_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 1520
    .local v2, "callingId":J
    :try_start_0
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1521
    :try_start_1
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->availableIfaceCheckAndQoEPull(Ljava/lang/String;)Landroid/net/wifi/WlanLinkLayerQoE;

    move-result-object v5

    iput-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWlanLayerQoE:Landroid/net/wifi/WlanLinkLayerQoE;

    .line 1522
    if-nez v5, :cond_3

    .line 1523
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1534
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1523
    return-object v1

    .line 1526
    :cond_3
    :try_start_2
    invoke-static {v5, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKUtils;->copyFromNetWlanLinkLayerQoE(Landroid/net/wifi/WlanLinkLayerQoE;Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    move-result-object v1

    move-object v0, v1

    .line 1529
    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->calculateLostAndRetryRatio(Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;)V

    .line 1530
    monitor-exit v4

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local v0    # "netLayerQoE":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .end local v2    # "callingId":J
    .end local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .end local p1    # "ifaceName":Ljava/lang/String;
    :try_start_3
    throw v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1534
    .restart local v0    # "netLayerQoE":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .restart local v2    # "callingId":J
    .restart local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .restart local p1    # "ifaceName":Ljava/lang/String;
    :catchall_1
    move-exception v1

    goto :goto_2

    .line 1531
    :catch_0
    move-exception v1

    .line 1532
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1534
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1535
    nop

    .line 1537
    return-object v0

    .line 1534
    :goto_2
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1535
    throw v1

    .line 1516
    .end local v2    # "callingId":J
    :cond_4
    return-object v1
.end method

.method public ifaceAddedCallBackNotice(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1384
    .local p1, "availableIfaces":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 1385
    return-void

    .line 1387
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    .line 1388
    const/16 v1, 0x3ef

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 1387
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1390
    return-void
.end method

.method public ifaceRemovedCallBackNotice(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1393
    .local p1, "availableIfaces":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 1394
    return-void

    .line 1396
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    .line 1397
    const/16 v1, 0x3f0

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 1396
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1399
    return-void
.end method

.method public isCelluarDSDAState()Z
    .locals 2

    .line 2638
    const-string v0, "NetworkSDKService"

    const-string v1, "isCelluarDSDAState"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2640
    invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->cellularNetworkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2641
    const/4 v0, 0x0

    return v0

    .line 2642
    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDsdaCapability:Z

    return v0
.end method

.method public isSlaveWifiEnabledAndOthersOpt(I)I
    .locals 2
    .param p1, "type"    # I

    .line 2670
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 2671
    .local v0, "uid":I
    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSlaveWifiEnabledAndOthersOpt(II)I

    move-result v1

    return v1
.end method

.method public isSlaveWifiEnabledAndOthersOpt(II)I
    .locals 12
    .param p1, "type"    # I
    .param p2, "uid"    # I

    .line 2675
    const/4 v0, 0x1

    .line 2676
    .local v0, "success":I
    const/4 v1, 0x0

    .line 2677
    .local v1, "negated":I
    const/4 v2, -0x1

    .line 2680
    .local v2, "error":I
    const-string v3, "isSlaveWifiEnabledAndOthersOpt"

    invoke-virtual {p0, v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "NetworkSDKService"

    if-nez v3, :cond_0

    .line 2681
    const-string v3, "isSlaveWifiEnabledAndOthersOpt No permission"

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2682
    return v2

    .line 2685
    :cond_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    if-nez v3, :cond_1

    .line 2686
    return v2

    .line 2689
    :cond_1
    const/4 v3, 0x0

    .line 2690
    .local v3, "isMarket":Z
    iget v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMarketUid:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    if-ne v5, p2, :cond_2

    .line 2691
    const/4 v3, 0x1

    .line 2693
    :cond_2
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    if-nez v5, :cond_3

    .line 2694
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v5

    iput-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 2696
    :cond_3
    const/4 v5, 0x0

    .line 2697
    .local v5, "packName":Ljava/lang/String;
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppUidToPackName:Ljava/util/Map;

    if-eqz v6, :cond_4

    .line 2698
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v5, v6

    check-cast v5, Ljava/lang/String;

    .line 2699
    :cond_4
    if-nez v5, :cond_5

    if-eqz p1, :cond_5

    .line 2700
    return v2

    .line 2702
    :cond_5
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 2703
    .local v6, "uidStr":Ljava/lang/String;
    if-eqz v3, :cond_6

    .line 2704
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",com.android.providers.downloads"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2705
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDownloadsUid:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2707
    :cond_6
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "isSlaveWifiEnabledAndOthersOpt packName = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2709
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v7

    .line 2711
    .local v7, "callingUid":J
    const/4 v9, 0x1

    packed-switch p1, :pswitch_data_0

    .line 2745
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2746
    goto/16 :goto_1

    .line 2733
    :pswitch_0
    :try_start_0
    iget-object v9, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    const/4 v10, 0x4

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v5, v6, v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_7

    .line 2734
    nop

    .line 2745
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2734
    return v0

    .line 2736
    :cond_7
    nop

    .line 2745
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2736
    return v1

    .line 2726
    :pswitch_1
    :try_start_1
    iget-object v10, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-virtual {v10, v9, v5, v6, v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 2727
    iget-object v9, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-virtual {v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDWUidToSlad()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2728
    nop

    .line 2745
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2728
    return v0

    .line 2730
    :cond_8
    nop

    .line 2745
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2730
    return v2

    .line 2719
    :pswitch_2
    :try_start_2
    iget-object v10, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    const/4 v11, 0x2

    invoke-virtual {v10, v11, v5, v6, v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 2720
    iget-object v9, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    invoke-virtual {v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDWUidToSlad()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2721
    nop

    .line 2745
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2721
    return v0

    .line 2723
    :cond_9
    nop

    .line 2745
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2723
    return v2

    .line 2713
    :pswitch_3
    :try_start_3
    iget-object v9, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    invoke-virtual {v9}, Landroid/net/wifi/SlaveWifiManager;->isSlaveWifiEnabled()Z

    move-result v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v4, :cond_a

    .line 2714
    nop

    .line 2745
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2714
    return v0

    .line 2716
    :cond_a
    nop

    .line 2745
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2716
    return v1

    .line 2745
    :catchall_0
    move-exception v4

    goto :goto_0

    .line 2740
    :catch_0
    move-exception v9

    .line 2741
    .local v9, "e":Ljava/lang/Exception;
    :try_start_4
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isSlaveWifiEnabledAndOthersOpt clearCallingIdentity uid = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2742
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2743
    nop

    .line 2745
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2743
    return v2

    .line 2745
    .end local v9    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2746
    throw v4

    .line 2748
    :goto_1
    return v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isSupportDualCelluarData()Z
    .locals 2

    .line 2614
    const-string v0, "NetworkSDKService"

    const-string v1, "isSupportDualCelluarData"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2616
    invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->cellularNetworkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 2617
    return v1

    .line 2619
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualCelluarDataService:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    if-eqz v0, :cond_1

    .line 2620
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->supportDualCelluarData()Z

    move-result v0

    return v0

    .line 2622
    :cond_1
    return v1
.end method

.method public isSupportDualWifi()Z
    .locals 2

    .line 531
    const-string v0, "NetworkSDKService"

    const-string v1, "isSupportDualWifi"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 534
    return v1

    .line 536
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    if-eqz v0, :cond_1

    .line 537
    invoke-virtual {v0}, Landroid/net/wifi/SlaveWifiManager;->supportDualWifi()Z

    move-result v0

    return v0

    .line 539
    :cond_1
    return v1
.end method

.method public isSupportMediaPlayerPolicy()Z
    .locals 2

    .line 2626
    const-string v0, "NetworkSDKService"

    const-string v1, "isSupportMediaPlayerPolicy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2628
    invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->cellularNetworkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 2629
    return v1

    .line 2631
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualCelluarDataService:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    if-eqz v0, :cond_1

    .line 2632
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->supportMediaPlayerPolicy()Z

    move-result v0

    return v0

    .line 2634
    :cond_1
    return v1
.end method

.method public networkSDKPermissionCheck(Ljava/lang/String;)Z
    .locals 8
    .param p1, "callingName"    # Ljava/lang/String;

    .line 2224
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 2225
    .local v0, "uid":I
    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSystemProcess(I)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    return v2

    .line 2226
    :cond_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 2227
    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkSDKUid:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_3

    .line 2228
    const-string v3, "NetworkSDKService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WhiteList passed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2233
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2234
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mPermission:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2235
    .local v1, "permission":Ljava/lang/String;
    if-nez v1, :cond_1

    return v2

    .line 2236
    :cond_1
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    .line 2237
    .local v3, "pid":I
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v1, v3, v0}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v5

    .line 2238
    .local v5, "checkResult":I
    if-nez v5, :cond_2

    .line 2239
    const-string v4, "NetworkSDKService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "permission granted: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2240
    return v2

    .line 2242
    :cond_2
    const-string v2, "NetworkSDKService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "permission denied: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2243
    return v4

    .line 2230
    .end local v1    # "permission":Ljava/lang/String;
    .end local v3    # "pid":I
    .end local v5    # "checkResult":I
    :cond_3
    :try_start_1
    const-string v2, "NetworkSDKService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "WhiteList denied: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2231
    monitor-exit v1

    return v4

    .line 2233
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public onCreate()V
    .locals 5

    .line 401
    const-string v0, "Got a null instance of WifiScanner!"

    const-string v1, "onCreate "

    const-string v2, "NetworkSDKService"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    new-instance v1, Landroid/os/HandlerThread;

    const-string v3, "NetworkSDKServiceHandler"

    invoke-direct {v1, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 404
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 405
    new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, p0, v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandler:Landroid/os/Handler;

    .line 406
    new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, p0, v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    .line 408
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x67

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 412
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "wifi"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 413
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    const-string v3, "SlaveWifiService"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/SlaveWifiManager;

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    .line 414
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    const-string v3, "MiuiWifiService"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/MiuiWifiManager;

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    .line 416
    sget-boolean v1, Lmiui/util/DeviceLevel;->IS_MIUI_MIDDLE_VERSION:Z

    if-nez v1, :cond_0

    .line 417
    const-string v1, "is not MIUI Middle or is not port MIUI Middle, start DualCelluarDataService"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualCelluarDataService:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 422
    :cond_0
    goto :goto_0

    .line 420
    :catch_0
    move-exception v1

    .line 421
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSystemService Exception:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    const-class v3, Landroid/net/wifi/WifiScanner;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiScanner;

    invoke-static {v1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiScanner;

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScanner:Landroid/net/wifi/WifiScanner;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 429
    goto :goto_1

    .line 427
    :catch_1
    move-exception v1

    .line 428
    .restart local v1    # "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isConnectSlaveAp:Z

    .line 431
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->getCurrentDSDAState()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDsdaCapability:Z

    .line 433
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    .line 434
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWlanQoECallbacks:Landroid/os/RemoteCallbackList;

    .line 435
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    .line 437
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 438
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getAvailableIfaces()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAvailableIfaces:Ljava/util/List;

    .line 439
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V

    .line 440
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIScreenStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V

    .line 441
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkPriorityListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerNetworkPriorityListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;)V

    .line 442
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIAppStatusListenr:Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerAppStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;)V

    .line 443
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registDualWifiStateBroadcastReceiver()V

    .line 444
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initWhiteList()V

    .line 445
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initCellularWhiteList()V

    .line 446
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initNetworkSDKCloudObserver()V

    .line 447
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initMibridgeCloudObserver()V

    .line 448
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initCellularNetworkSDKCloudObserver()V

    .line 449
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initBroadcastReceiver()V

    .line 450
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initDualDataBroadcastReceiver()V

    .line 451
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initPermission()V

    .line 452
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initAppTrafficSessionAndSimCardHelper()V

    .line 453
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 454
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registerQEEChangeObserver()V

    .line 455
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 458
    const-string v0, "NetworkSDKService"

    const-string v1, "onDestroy "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiNetService:Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    if-eqz v0, :cond_0

    .line 462
    :try_start_0
    invoke-interface {v0}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;->stopMiNetd()Z

    .line 463
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 464
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWlanQoECallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 465
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 466
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiNetService:Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    invoke-interface {v0, v1}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;->unlinkToDeath(Landroid/os/IHwBinder$DeathRecipient;)Z

    .line 467
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->clearAppWifiSelectionMonitor()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 470
    goto :goto_0

    .line 468
    :catch_0
    move-exception v0

    .line 469
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 474
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualCelluarDataService:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    if-eqz v0, :cond_1

    .line 475
    invoke-static {}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->destroyInstance()V

    .line 476
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualCelluarDataService:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    .line 479
    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->closeAppTrafficSession()V

    .line 480
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 481
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIScreenStatusListener:Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V

    .line 482
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkInterfaceListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V

    .line 483
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetworkPriorityListener:Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterNetworkPriorityListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;)V

    .line 485
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterQEEChangeObserver()V

    .line 486
    return-void
.end method

.method onDsdaStateChanged(Z)V
    .locals 2
    .param p1, "result"    # Z

    .line 1377
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1378
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x3f4

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1379
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1380
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1381
    return-void
.end method

.method onSetSlaveWifiResult(Z)V
    .locals 2
    .param p1, "result"    # Z

    .line 1349
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1350
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1351
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1352
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1353
    return-void
.end method

.method onSlaveWifiConnected(Z)V
    .locals 2
    .param p1, "result"    # Z

    .line 1356
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1357
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1358
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1359
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1360
    return-void
.end method

.method onSlaveWifiDisconnected(Z)V
    .locals 2
    .param p1, "result"    # Z

    .line 1363
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1364
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1365
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1366
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1367
    return-void
.end method

.method onSlaveWifiEnable(Z)V
    .locals 2
    .param p1, "result"    # Z

    .line 1370
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1371
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x3f3

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1372
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1373
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1374
    return-void
.end method

.method public registerCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
    .locals 4
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    .line 619
    if-eqz p1, :cond_0

    .line 620
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 623
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getNetworkPriorityInfo()Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;

    move-result-object v0

    .line 624
    .local v0, "info":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;
    iget v1, v0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->priorityMode:I

    iget v2, v0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->trafficPolicy:I

    iget v3, v0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->thermalLevel:I

    invoke-interface {p1, v1, v2, v3}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onNetworkPriorityChanged(III)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 628
    .end local v0    # "info":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;
    goto :goto_0

    .line 626
    :catch_0
    move-exception v0

    .line 629
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 631
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public registerNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;I)Z
    .locals 2
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    .param p2, "interval"    # I

    .line 725
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 726
    .local v0, "uid":I
    invoke-virtual {p0, p1, p2, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registerNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;II)Z

    move-result v1

    return v1
.end method

.method public registerNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;II)Z
    .locals 11
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    .param p2, "interval"    # I
    .param p3, "uid"    # I

    .line 730
    const/4 v0, 0x0

    if-ltz p2, :cond_8

    const/4 v1, 0x5

    if-le p2, v1, :cond_0

    goto/16 :goto_1

    .line 733
    :cond_0
    const-string v1, "registerNetLinkCallback"

    invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 734
    return v0

    .line 736
    :cond_1
    invoke-static {p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKUtils;->intervalTransformation(I)I

    move-result p2

    .line 738
    if-eqz p1, :cond_7

    .line 739
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 740
    :try_start_0
    invoke-interface {p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 741
    .local v0, "key":Landroid/os/IBinder;
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollMillis:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    .line 742
    monitor-exit v1

    return v3

    .line 744
    :cond_2
    int-to-long v4, p2

    iget-wide v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J

    cmp-long v2, v4, v6

    if-lez v2, :cond_3

    const-wide/16 v4, -0x1

    cmp-long v2, v6, v4

    if-nez v2, :cond_4

    .line 745
    :cond_3
    int-to-long v4, p2

    iput-wide v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J

    .line 747
    :cond_4
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWlanQoECallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v2

    .line 748
    .local v2, "count":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 749
    .local v4, "curSystemTime":J
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v2, :cond_5

    .line 750
    iget-object v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWlanQoECallbacks:Landroid/os/RemoteCallbackList;

    .line 751
    invoke-virtual {v7, v6}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackItem(I)Landroid/os/IInterface;

    move-result-object v7

    check-cast v7, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;

    .line 752
    .local v7, "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    invoke-interface {v7}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;->asBinder()Landroid/os/IBinder;

    move-result-object v8

    .line 753
    .local v8, "callBackKey":Landroid/os/IBinder;
    iget-object v9, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollMillis:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v9, v8, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 754
    const-string v9, "NetworkSDKService"

    const-string v10, "registerNetLinkCallback: Reset callBacks millis"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    nop

    .end local v7    # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    .end local v8    # "callBackKey":Landroid/os/IBinder;
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 757
    .end local v6    # "i":I
    :cond_5
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWlanQoECallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v6, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 758
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollInterval:Ljava/util/Map;

    int-to-long v7, p2

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 759
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollMillis:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 760
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppCallBackMapToUid:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 762
    iget-boolean v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsOpenWifiLinkPoll:Z

    if-nez v6, :cond_6

    .line 763
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    const/16 v7, 0x3f1

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 764
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    .line 765
    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    iget-wide v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J

    .line 764
    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 769
    :cond_6
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsOpenWifiLinkPoll:Z

    .line 770
    .end local v0    # "key":Landroid/os/IBinder;
    .end local v2    # "count":I
    .end local v4    # "curSystemTime":J
    monitor-exit v1

    .line 772
    return v3

    .line 770
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 774
    :cond_7
    return v0

    .line 731
    :cond_8
    :goto_1
    return v0
.end method

.method public reportBssidScore(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1899
    .local p1, "bssidScores":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "reportBssidScore"

    invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1900
    const-string v0, "NetworkSDKService"

    const-string v1, "reportBssidScore No permission"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1901
    return-void

    .line 1904
    :cond_0
    if-eqz p1, :cond_5

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-gtz v0, :cond_1

    goto :goto_1

    .line 1907
    :cond_1
    const-string v0, ""

    .line 1908
    .local v0, "objBssid":Ljava/lang/String;
    const/4 v1, -0x1

    .line 1909
    .local v1, "score":I
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1910
    .local v3, "entry":Ljava/util/Map$Entry;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1911
    .local v4, "tmpScore":I
    if-ltz v4, :cond_2

    if-le v4, v1, :cond_2

    .line 1912
    move v1, v4

    .line 1913
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    .line 1915
    .end local v3    # "entry":Ljava/util/Map$Entry;
    .end local v4    # "tmpScore":I
    :cond_2
    goto :goto_0

    .line 1917
    :cond_3
    if-ltz v1, :cond_4

    .line 1918
    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->connectionWithBssid(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1919
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->broadcastPrimaryConnectingFailed()V

    .line 1922
    :cond_4
    return-void

    .line 1905
    .end local v0    # "objBssid":Ljava/lang/String;
    .end local v1    # "score":I
    :cond_5
    :goto_1
    return-void
.end method

.method public requestAppTrafficStatistics(IJJ)Ljava/util/Map;
    .locals 8
    .param p1, "type"    # I
    .param p2, "startTime"    # J
    .param p4, "endTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJJ)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 832
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v7

    .line 833
    .local v7, "uid":I
    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->requestAppTrafficStatistics(IJJI)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public requestAppTrafficStatistics(IJJI)Ljava/util/Map;
    .locals 3
    .param p1, "type"    # I
    .param p2, "startTime"    # J
    .param p4, "endTime"    # J
    .param p6, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJJI)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 837
    const/4 v0, 0x0

    .line 840
    .local v0, "appTrafficStatistics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "requestAppTrafficStatistics"

    invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "code"

    if-nez v1, :cond_0

    .line 841
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    move-object v0, v1

    .line 842
    const-string v1, "1002"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 843
    const-string v1, "message"

    const-string v2, "No permission"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 844
    return-object v0

    .line 847
    :cond_0
    invoke-direct/range {p0 .. p6}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->buildAppTrafficStatistics(IJJI)Ljava/util/Map;

    move-result-object v0

    .line 849
    if-nez v0, :cond_1

    .line 850
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    move-object v0, v1

    .line 851
    const-string v1, "1001"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 853
    :cond_1
    const-string v1, "1000"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 856
    :goto_0
    return-object v0
.end method

.method public resumeBackgroundScan()Z
    .locals 2

    .line 686
    const-string v0, "NetworkSDKService"

    const-string v1, "resumeBackgroundScan"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 689
    return v1

    .line 691
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v0, :cond_1

    .line 692
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/wifi/MiuiWifiManager;->setLatencyLevel(I)V

    .line 693
    return v1

    .line 695
    :cond_1
    return v1
.end method

.method public resumeWifiPowerSave()Z
    .locals 2

    .line 712
    const-string v0, "NetworkSDKService"

    const-string v1, "resumeWifiPowerSave"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 715
    return v1

    .line 717
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v0, :cond_1

    .line 718
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/wifi/MiuiWifiManager;->enablePowerSave(Z)V

    .line 719
    return v1

    .line 721
    :cond_1
    return v1
.end method

.method public setDualCelluarDataEnable(Z)Z
    .locals 7
    .param p1, "enable"    # Z

    .line 2650
    const-string v0, "NetworkSDKService"

    const-string/jumbo v1, "setDualCelluarDataEnable"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2651
    const/4 v2, 0x0

    .line 2653
    .local v2, "result":Z
    invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->cellularNetworkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2654
    const/4 v0, 0x0

    return v0

    .line 2656
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v3

    .line 2658
    .local v3, "callingId":J
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualCelluarDataService:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    if-eqz v1, :cond_1

    .line 2659
    invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->setDualCelluarDataEnable(Z)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v2, v0

    .line 2664
    :cond_1
    nop

    :goto_0
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2665
    goto :goto_1

    .line 2664
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 2661
    :catch_0
    move-exception v1

    .line 2662
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "setDualCelluarDataEnable error "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2664
    nop

    .end local v1    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 2666
    :goto_1
    return v2

    .line 2664
    :goto_2
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2665
    throw v0
.end method

.method public setSlaveWifiEnabled(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .line 543
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setSlaveWifiEnabled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NetworkSDKService"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    const-string/jumbo v0, "setSlaveWifiEnabled"

    invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 546
    const/4 v0, 0x0

    return v0

    .line 549
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 550
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x64

    iput v1, v0, Landroid/os/Message;->what:I

    .line 551
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 552
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 553
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 554
    const/4 v1, 0x1

    return v1
.end method

.method public setSockPrio(II)Z
    .locals 4
    .param p1, "fd"    # I
    .param p2, "prio"    # I

    .line 503
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setSockPrio "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "NetworkSDKService"

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    const-string/jumbo v0, "setSockPrio"

    invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 506
    const/4 v0, 0x0

    return v0

    .line 509
    :cond_0
    :try_start_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiNetService:Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x3e9

    invoke-interface {v0, v3, v1}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;->setCommon(ILjava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 512
    goto :goto_0

    .line 510
    :catch_0
    move-exception v0

    .line 511
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public setTCPCongestion(ILjava/lang/String;)Z
    .locals 4
    .param p1, "fd"    # I
    .param p2, "cc"    # Ljava/lang/String;

    .line 517
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setTCPCongestion "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "NetworkSDKService"

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    const-string/jumbo v0, "setTCPCongestion"

    invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 520
    const/4 v0, 0x0

    return v0

    .line 523
    :cond_0
    :try_start_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiNetService:Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x3ea

    invoke-interface {v0, v3, v1}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;->setCommon(ILjava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 526
    goto :goto_0

    .line 524
    :catch_0
    move-exception v0

    .line 525
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public setTrafficTransInterface(ILjava/lang/String;)Z
    .locals 4
    .param p1, "fd"    # I
    .param p2, "bindInterface"    # Ljava/lang/String;

    .line 605
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setTrafficTransInterface "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "NetworkSDKService"

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    const-string/jumbo v0, "setTrafficTransInterface"

    invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 608
    const/4 v0, 0x0

    return v0

    .line 611
    :cond_0
    :try_start_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiNetService:Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x3ee

    invoke-interface {v0, v3, v1}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService;->setCommon(ILjava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 614
    goto :goto_0

    .line 612
    :catch_0
    move-exception v0

    .line 613
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public suspendBackgroundScan()Z
    .locals 2

    .line 673
    const-string v0, "NetworkSDKService"

    const-string/jumbo v1, "suspendBackgroundScan"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 676
    return v1

    .line 678
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v0, :cond_1

    .line 679
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/net/wifi/MiuiWifiManager;->setLatencyLevel(I)V

    .line 680
    const/4 v0, 0x1

    return v0

    .line 682
    :cond_1
    return v1
.end method

.method public suspendWifiPowerSave()Z
    .locals 2

    .line 699
    const-string v0, "NetworkSDKService"

    const-string/jumbo v1, "suspendWifiPowerSave"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 702
    return v1

    .line 704
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v0, :cond_1

    .line 705
    invoke-virtual {v0, v1}, Landroid/net/wifi/MiuiWifiManager;->enablePowerSave(Z)V

    .line 706
    const/4 v0, 0x1

    return v0

    .line 708
    :cond_1
    return v1
.end method

.method public triggerWifiSelection()V
    .locals 8

    .line 1853
    const-string/jumbo v0, "triggerWifiSelection"

    invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1854
    const-string v0, "NetworkSDKService"

    const-string/jumbo v1, "triggerWifiSelection No permission"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1855
    return-void

    .line 1858
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mBssidToNetworkId:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 1859
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mBssidToNetworkId:Ljava/util/Map;

    goto :goto_0

    .line 1861
    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1863
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1864
    .local v0, "netAndBssidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v1, :cond_2

    .line 1865
    invoke-virtual {v1}, Landroid/net/wifi/MiuiWifiManager;->netSDKGetAvailableNetworkIdAndBssid()Ljava/util/List;

    move-result-object v0

    .line 1866
    if-nez v0, :cond_2

    .line 1867
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v0, v1

    .line 1870
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1871
    .local v1, "retBssidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 1872
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1873
    .local v3, "netwrokIdWithBssid":[Ljava/lang/String;
    array-length v4, v3

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 1874
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mBssidToNetworkId:Ljava/util/Map;

    const/4 v5, 0x1

    aget-object v6, v3, v5

    const/4 v7, 0x0

    aget-object v7, v3, v7

    invoke-interface {v4, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1875
    aget-object v4, v3, v5

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1871
    .end local v3    # "netwrokIdWithBssid":[Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1879
    .end local v2    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 1880
    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1882
    .local v3, "N":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v3, :cond_5

    .line 1884
    :try_start_1
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v5, v4}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;

    invoke-interface {v5, v1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;->avaliableBssidCb(Ljava/util/List;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1887
    goto :goto_3

    .line 1893
    .end local v4    # "i":I
    :catchall_0
    move-exception v4

    goto :goto_5

    .line 1889
    :catch_0
    move-exception v4

    goto :goto_4

    .line 1885
    .restart local v4    # "i":I
    :catch_1
    move-exception v5

    .line 1886
    .local v5, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v6, "NetworkSDKService"

    const-string v7, "RemoteException at triggerWifiSelection()"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1882
    .end local v5    # "e":Landroid/os/RemoteException;
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1890
    .local v4, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_3
    const-string v5, "NetworkSDKService"

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1891
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1893
    .end local v4    # "e":Ljava/lang/Exception;
    :try_start_4
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    goto :goto_6

    :goto_5
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1894
    nop

    .end local v0    # "netAndBssidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "retBssidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    throw v4

    .line 1893
    .restart local v0    # "netAndBssidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v1    # "retBssidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local p0    # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    :cond_5
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mNetSelectCallbacks:Landroid/os/RemoteCallbackList;

    :goto_6
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1894
    nop

    .line 1895
    .end local v3    # "N":I
    monitor-exit v2

    .line 1896
    return-void

    .line 1895
    :catchall_1
    move-exception v3

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v3
.end method

.method public unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
    .locals 1
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    .line 636
    if-eqz p1, :cond_0

    .line 637
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 638
    const/4 v0, 0x1

    return v0

    .line 640
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public unregisterNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;)Z
    .locals 2
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;

    .line 779
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 780
    .local v0, "uid":I
    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;I)Z

    move-result v1

    return v1
.end method

.method public unregisterNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;I)Z
    .locals 16
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    .param p2, "uid"    # I

    .line 785
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string/jumbo v0, "unregisterNetLinkCallback"

    invoke-virtual {v1, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z

    move-result v0

    const/4 v3, 0x0

    if-nez v0, :cond_0

    .line 786
    return v3

    .line 788
    :cond_0
    if-eqz v2, :cond_7

    .line 789
    iget-object v4, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 790
    :try_start_0
    invoke-interface/range {p1 .. p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 792
    .local v0, "key":Landroid/os/IBinder;
    iget-object v5, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollMillis:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    const/4 v6, 0x1

    if-nez v5, :cond_1

    .line 793
    monitor-exit v4

    return v6

    .line 795
    :cond_1
    iget-object v5, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWlanQoECallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v5, v2}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 796
    iget-object v5, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollInterval:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 797
    iget-object v5, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollMillis:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 798
    iget-object v5, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppCallBackMapToUid:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 800
    iget-object v5, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWlanQoECallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v5

    .line 801
    .local v5, "count":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 802
    .local v7, "curSystemTime":J
    const-wide/16 v9, -0x1

    .line 803
    .local v9, "minInterval":J
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    const-wide/16 v12, -0x1

    if-ge v11, v5, :cond_4

    .line 804
    iget-object v14, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWlanQoECallbacks:Landroid/os/RemoteCallbackList;

    .line 805
    invoke-virtual {v14, v11}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackItem(I)Landroid/os/IInterface;

    move-result-object v14

    check-cast v14, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;

    .line 806
    .local v14, "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    invoke-interface {v14}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;->asBinder()Landroid/os/IBinder;

    move-result-object v15

    .line 807
    .local v15, "callBackKey":Landroid/os/IBinder;
    iget-object v6, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollMillis:Ljava/util/Map;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v6, v15, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 808
    cmp-long v3, v9, v12

    if-nez v3, :cond_2

    .line 809
    iget-object v3, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollInterval:Ljava/util/Map;

    invoke-interface {v3, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    move-wide v9, v12

    goto :goto_1

    .line 810
    :cond_2
    iget-object v3, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollInterval:Ljava/util/Map;

    invoke-interface {v3, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    cmp-long v3, v12, v9

    if-gez v3, :cond_3

    .line 811
    iget-object v3, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mAppStatsPollInterval:Ljava/util/Map;

    invoke-interface {v3, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    move-wide v9, v12

    .line 803
    .end local v14    # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    .end local v15    # "callBackKey":Landroid/os/IBinder;
    :cond_3
    :goto_1
    add-int/lit8 v11, v11, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x1

    goto :goto_0

    .line 814
    .end local v11    # "i":I
    :cond_4
    cmp-long v3, v9, v12

    if-eqz v3, :cond_5

    .line 815
    iput-wide v9, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J

    .line 816
    const-string v3, "NetworkSDKService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "unregisterNetLinkCallback: reset interval = "

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v14, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    :cond_5
    if-nez v5, :cond_6

    .line 820
    const/4 v3, 0x0

    iput-boolean v3, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsOpenWifiLinkPoll:Z

    .line 821
    iput-wide v12, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J

    .line 823
    .end local v0    # "key":Landroid/os/IBinder;
    .end local v5    # "count":I
    .end local v7    # "curSystemTime":J
    .end local v9    # "minInterval":J
    :cond_6
    monitor-exit v4

    .line 825
    const/4 v0, 0x1

    return v0

    .line 823
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 827
    :cond_7
    const/4 v0, 0x0

    return v0
.end method

.method public videoPolicyCallBackNotice(III)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "duration"    # I
    .param p3, "length"    # I

    .line 1402
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 1403
    return-void

    .line 1405
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mCallbackHandler:Landroid/os/Handler;

    .line 1406
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x3f5

    invoke-virtual {v0, v2, p1, p2, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 1405
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1407
    return-void
.end method
