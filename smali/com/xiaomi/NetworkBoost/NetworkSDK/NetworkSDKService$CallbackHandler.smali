.class Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;
.super Landroid/os/Handler;
.source "NetworkSDKService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CallbackHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;


# direct methods
.method public constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 2096
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    .line 2097
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2098
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .line 2101
    const/4 v0, 0x0

    .line 2102
    .local v0, "res":Z
    const/4 v1, 0x0

    .line 2103
    .local v1, "N":I
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    goto/16 :goto_c

    .line 2184
    :sswitch_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$mnetworkPriorityCallBackSend(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Message;)V

    .line 2186
    :sswitch_1
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2187
    .local v2, "status":I
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 2188
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 2190
    :try_start_0
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-interface {v4, v2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onSlaveWifiEnableV1(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2195
    goto :goto_1

    .line 2191
    :catch_0
    move-exception v4

    .line 2194
    .local v4, "e":Landroid/os/RemoteException;
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v5}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    check-cast v6, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-virtual {v5, v6}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z

    .line 2188
    .end local v4    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2197
    .end local v3    # "i":I
    :cond_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2198
    goto/16 :goto_c

    .line 2213
    .end local v2    # "status":I
    :sswitch_2
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 2214
    .local v2, "screenStatus":Z
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v3, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$msetScreenStatus(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Z)V

    .line 2215
    goto/16 :goto_c

    .line 2203
    .end local v2    # "screenStatus":Z
    :sswitch_3
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$mmediaPlayerPolicyCallBackSend(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Message;)V

    .line 2204
    goto/16 :goto_c

    .line 2200
    :sswitch_4
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$mdsdaStateChangedCallBackSend(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Message;)V

    .line 2201
    goto/16 :goto_c

    .line 2170
    :sswitch_5
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2171
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 2172
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v1, :cond_1

    .line 2174
    :try_start_1
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-interface {v3, v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onSlaveWifiEnable(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2179
    goto :goto_3

    .line 2175
    :catch_1
    move-exception v3

    .line 2178
    .local v3, "e":Landroid/os/RemoteException;
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-virtual {v4, v5}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z

    .line 2172
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2181
    .end local v2    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2182
    goto/16 :goto_c

    .line 2207
    :sswitch_6
    :try_start_2
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$mappWifiSelectionMonitor(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 2210
    goto/16 :goto_c

    .line 2208
    :catch_2
    move-exception v2

    .line 2209
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 2211
    .end local v2    # "e":Ljava/lang/Exception;
    goto/16 :goto_c

    .line 2167
    :sswitch_7
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$mwifiLinkLayerStatsPoll(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    .line 2168
    goto/16 :goto_c

    .line 2164
    :sswitch_8
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$mifaceRemovedCallBackSend(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Message;)V

    .line 2165
    goto/16 :goto_c

    .line 2161
    :sswitch_9
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$mifaceAddCallBackSend(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Message;)V

    .line 2162
    goto/16 :goto_c

    .line 2147
    :sswitch_a
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2148
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 2149
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_4
    if-ge v2, v1, :cond_2

    .line 2151
    :try_start_3
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-interface {v3, v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onSlaveWifiDisconnected(Z)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_3

    .line 2156
    goto :goto_5

    .line 2152
    :catch_3
    move-exception v3

    .line 2155
    .restart local v3    # "e":Landroid/os/RemoteException;
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-virtual {v4, v5}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z

    .line 2149
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 2158
    .end local v2    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2159
    goto/16 :goto_c

    .line 2133
    :sswitch_b
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2134
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 2135
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_6
    if-ge v2, v1, :cond_3

    .line 2137
    :try_start_4
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-interface {v3, v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onSlaveWifiConnected(Z)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_4

    .line 2142
    goto :goto_7

    .line 2138
    :catch_4
    move-exception v3

    .line 2141
    .restart local v3    # "e":Landroid/os/RemoteException;
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-virtual {v4, v5}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z

    .line 2135
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 2144
    .end local v2    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2145
    goto/16 :goto_c

    .line 2119
    :sswitch_c
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2120
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 2121
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_8
    if-ge v2, v1, :cond_4

    .line 2123
    :try_start_5
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-interface {v3, v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onSetSlaveWifiResult(Z)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_5

    .line 2128
    goto :goto_9

    .line 2124
    :catch_5
    move-exception v3

    .line 2127
    .restart local v3    # "e":Landroid/os/RemoteException;
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-virtual {v4, v5}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z

    .line 2121
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 2130
    .end local v2    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2131
    goto :goto_c

    .line 2105
    :sswitch_d
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2106
    .local v2, "result":I
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 2107
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_a
    if-ge v3, v1, :cond_5

    .line 2109
    :try_start_6
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-interface {v4, v2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onScanSuccussed(I)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_6

    .line 2114
    goto :goto_b

    .line 2110
    :catch_6
    move-exception v4

    .line 2113
    .restart local v4    # "e":Landroid/os/RemoteException;
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v5}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    check-cast v6, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    invoke-virtual {v5, v6}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z

    .line 2107
    .end local v4    # "e":Landroid/os/RemoteException;
    :goto_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 2116
    .end local v3    # "i":I
    :cond_5
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmCallbacks(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 2117
    nop

    .line 2219
    .end local v2    # "result":I
    :goto_c
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_d
        0x1 -> :sswitch_c
        0x2 -> :sswitch_b
        0x3 -> :sswitch_a
        0x3ef -> :sswitch_9
        0x3f0 -> :sswitch_8
        0x3f1 -> :sswitch_7
        0x3f2 -> :sswitch_6
        0x3f3 -> :sswitch_5
        0x3f4 -> :sswitch_4
        0x3f5 -> :sswitch_3
        0x3f6 -> :sswitch_2
        0x3f7 -> :sswitch_0
        0x3f8 -> :sswitch_1
    .end sparse-switch
.end method
