.class public Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;
.super Ljava/lang/Object;
.source "AppDataUsage.java"


# static fields
.field public static final MOBILE:Ljava/lang/String; = "mobile-"

.field public static final RX_BACK_BYTES:Ljava/lang/String; = "rxBackgroundBytes"

.field public static final RX_BYTES:Ljava/lang/String; = "rxBytes"

.field public static final RX_FORE_BYTES:Ljava/lang/String; = "rxForegroundBytes"

.field public static final TX_BACK_BYTES:Ljava/lang/String; = "txBackgroundBytes"

.field public static final TX_BYTES:Ljava/lang/String; = "txBytes"

.field public static final TX_FORE_BYTES:Ljava/lang/String; = "txForegroundBytes"

.field public static final WIFI:Ljava/lang/String; = "wifi-"


# instance fields
.field private mRxBytes:J

.field private mTxBytes:J

.field private mrxBackgroundBytes:J

.field private mrxForegroundBytes:J

.field private mtxBackgroundBytes:J

.field private mtxForegroundBytes:J


# direct methods
.method public constructor <init>(JJJJ)V
    .locals 2
    .param p1, "mRxBytes"    # J
    .param p3, "mTxBytes"    # J
    .param p5, "mtxForegroundBytes"    # J
    .param p7, "mrxForegroundBytes"    # J

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J

    .line 8
    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J

    .line 10
    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxForegroundBytes:J

    .line 11
    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxForegroundBytes:J

    .line 13
    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxBackgroundBytes:J

    .line 14
    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxBackgroundBytes:J

    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->addRxBytes(J)V

    .line 27
    invoke-virtual {p0, p3, p4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->addTxBytes(J)V

    .line 28
    invoke-virtual {p0, p5, p6}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->addTxForeBytes(J)V

    .line 29
    invoke-virtual {p0, p7, p8}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->addRxForeBytes(J)V

    .line 30
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->calculateTxBackBytes()V

    .line 31
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->calculateRxBackBytes()V

    .line 32
    return-void
.end method


# virtual methods
.method public addRxBytes(J)V
    .locals 2
    .param p1, "bytes"    # J

    .line 43
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J

    .line 44
    return-void
.end method

.method public addRxForeBytes(J)V
    .locals 2
    .param p1, "mrxForegroundBytes"    # J

    .line 55
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxForegroundBytes:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxForegroundBytes:J

    .line 56
    return-void
.end method

.method public addTxBytes(J)V
    .locals 2
    .param p1, "bytes"    # J

    .line 47
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J

    .line 48
    return-void
.end method

.method public addTxForeBytes(J)V
    .locals 2
    .param p1, "mtxForegroundBytes"    # J

    .line 51
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxForegroundBytes:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxForegroundBytes:J

    .line 52
    return-void
.end method

.method public calculateRxBackBytes()V
    .locals 6

    .line 63
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxBackgroundBytes:J

    iget-wide v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J

    iget-wide v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxForegroundBytes:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxBackgroundBytes:J

    .line 64
    return-void
.end method

.method public calculateTxBackBytes()V
    .locals 6

    .line 59
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxBackgroundBytes:J

    iget-wide v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J

    iget-wide v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxForegroundBytes:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxBackgroundBytes:J

    .line 60
    return-void
.end method

.method public getRxBytes()J
    .locals 2

    .line 35
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J

    return-wide v0
.end method

.method public getTotal()J
    .locals 4

    .line 67
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J

    iget-wide v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public getTxBytes()J
    .locals 2

    .line 39
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J

    return-wide v0
.end method

.method public reset()V
    .locals 2

    .line 71
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J

    .line 72
    return-void
.end method

.method public toMap(Z)Ljava/util/Map;
    .locals 5
    .param p1, "isMobile"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 75
    if-eqz p1, :cond_0

    const-string v0, "mobile-"

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "wifi-"

    .line 77
    .local v0, "TYPE":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 78
    .local v1, "tomap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "rxBytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-wide v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "txBytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-wide v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "txForegroundBytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-wide v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxForegroundBytes:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "rxForegroundBytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-wide v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxForegroundBytes:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "txBackgroundBytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-wide v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxBackgroundBytes:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "rxBackgroundBytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-wide v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxBackgroundBytes:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    return-object v1
.end method
