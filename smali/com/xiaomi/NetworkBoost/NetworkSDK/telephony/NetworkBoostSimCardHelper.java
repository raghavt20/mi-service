public class com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper {
	 /* .source "NetworkBoostSimCardHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$NetworkBoostSingleSimCardHelper; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String DEFAULT_NULL_IMSI;
private static final java.lang.String TAG;
private static com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper sInstance;
/* # instance fields */
protected android.content.Context mContext;
protected java.lang.String mImsi1;
private java.lang.String mImsi2;
protected Boolean mIsSim1Inserted;
private Boolean mIsSim2Inserted;
private com.xiaomi.NetworkBoost.NetworkSDK.telephony.DualSimInfoManager$ISimInfoChangeListener mSimInfoChangeListener;
/* # direct methods */
private com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper ( ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 54 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 20 */
	 final String v0 = "default"; // const-string v0, "default"
	 this.mImsi1 = v0;
	 /* .line 21 */
	 this.mImsi2 = v0;
	 /* .line 43 */
	 /* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$1; */
	 /* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$1;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;)V */
	 this.mSimInfoChangeListener = v0;
	 /* .line 55 */
	 (( android.content.Context ) p1 ).getApplicationContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
	 this.mContext = v0;
	 /* .line 56 */
	 (( com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper ) p0 ).updateSimState ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->updateSimState()Z
	 /* .line 57 */
	 return;
} // .end method
 com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper ( ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;-><init>(Landroid/content/Context;)V */
	 return;
} // .end method
public static synchronized com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper getInstance ( android.content.Context p0 ) {
	 /* .locals 2 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* const-class v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper; */
	 /* monitor-enter v0 */
	 /* .line 31 */
	 try { // :try_start_0
		 v1 = com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper.sInstance;
		 /* if-nez v1, :cond_1 */
		 /* .line 33 */
		 miui.telephony.TelephonyManager .getDefault ( );
		 v1 = 		 (( miui.telephony.TelephonyManager ) v1 ).isMultiSimEnabled ( ); // invoke-virtual {v1}, Lmiui/telephony/TelephonyManager;->isMultiSimEnabled()Z
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 34 */
			 /* new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper; */
			 /* invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;-><init>(Landroid/content/Context;)V */
			 /* .line 36 */
		 } // :cond_0
		 /* new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$NetworkBoostSingleSimCardHelper; */
		 /* invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$NetworkBoostSingleSimCardHelper;-><init>(Landroid/content/Context;)V */
		 /* .line 38 */
	 } // :goto_0
	 v1 = com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper.sInstance;
	 /* invoke-direct {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->initForUIProcess()V */
	 /* .line 40 */
} // :cond_1
v1 = com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper.sInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit v0 */
/* .line 30 */
} // .end local p0 # "context":Landroid/content/Context;
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
} // .end method
public static void init ( android.content.Context p0 ) {
/* .locals 0 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 27 */
com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper .getInstance ( p0 );
/* .line 28 */
return;
} // .end method
private void initForUIProcess ( ) {
/* .locals 0 */
/* .line 52 */
return;
} // .end method
/* # virtual methods */
public Integer getCurrentMobileSlotNum ( ) {
/* .locals 1 */
/* .line 116 */
v0 = com.xiaomi.NetworkBoost.NetworkSDK.telephony.TelephonyUtil .getCurrentMobileSlotNum ( );
} // .end method
public java.lang.String getSimImsi ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "slotNum" # I */
/* .line 120 */
/* if-nez p1, :cond_0 */
/* .line 121 */
v0 = this.mImsi1;
/* .line 122 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_1 */
/* .line 123 */
v0 = this.mImsi2;
/* .line 125 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected Boolean isImsiMissed ( ) {
/* .locals 1 */
/* .line 112 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim1Inserted:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mImsi1;
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim2Inserted:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mImsi2;
/* if-nez v0, :cond_2 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean updateSimState ( ) {
/* .locals 7 */
/* .line 60 */
final String v0 = "NetworkBoostSimCardHelper"; // const-string v0, "NetworkBoostSimCardHelper"
int v1 = 0; // const/4 v1, 0x0
/* .line 63 */
/* .local v1, "siminfoList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;" */
try { // :try_start_0
v2 = this.mContext;
com.xiaomi.NetworkBoost.NetworkSDK.telephony.DualSimInfoManager .getSimInfoList ( v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v1, v2 */
/* .line 66 */
/* .line 64 */
/* :catch_0 */
/* move-exception v2 */
/* .line 65 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "get sim info exception!"; // const-string v3, "get sim info exception!"
android.util.Log .i ( v0,v3,v2 );
/* .line 68 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
final String v2 = "default"; // const-string v2, "default"
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
v5 = if ( v1 != null) { // if-eqz v1, :cond_5
if ( v5 != null) { // if-eqz v5, :cond_0
/* goto/16 :goto_3 */
/* .line 76 */
v5 = } // :cond_0
/* const-string/jumbo v6, "slotNum" */
/* if-ne v5, v4, :cond_3 */
/* .line 77 */
final String v5 = "one sim card inserted"; // const-string v5, "one sim card inserted"
android.util.Log .i ( v0,v5 );
/* .line 78 */
/* check-cast v0, Ljava/util/Map; */
/* .line 79 */
/* .local v0, "simInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* check-cast v5, Ljava/lang/String; */
v5 = java.lang.Integer .parseInt ( v5 );
/* .line 80 */
/* .local v5, "slotNum":I */
/* if-nez v5, :cond_1 */
/* .line 81 */
/* iput-boolean v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim1Inserted:Z */
/* .line 82 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim2Inserted:Z */
/* .line 84 */
v3 = this.mContext;
com.xiaomi.NetworkBoost.NetworkSDK.telephony.TelephonyUtil .getSubscriberId ( v3,v5 );
this.mImsi1 = v3;
/* .line 85 */
this.mImsi2 = v2;
/* .line 87 */
} // :cond_1
/* if-ne v5, v4, :cond_2 */
/* .line 88 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim1Inserted:Z */
/* .line 89 */
/* iput-boolean v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim2Inserted:Z */
/* .line 91 */
this.mImsi1 = v2;
/* .line 92 */
v2 = this.mContext;
com.xiaomi.NetworkBoost.NetworkSDK.telephony.TelephonyUtil .getSubscriberId ( v2,v5 );
this.mImsi2 = v2;
/* .line 94 */
} // .end local v0 # "simInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
} // .end local v5 # "slotNum":I
} // :cond_2
} // :goto_1
v2 = } // :cond_3
int v5 = 2; // const/4 v5, 0x2
/* if-ne v2, v5, :cond_4 */
/* .line 95 */
/* const-string/jumbo v2, "two sim cards inserted" */
android.util.Log .i ( v0,v2 );
/* .line 96 */
/* iput-boolean v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim1Inserted:Z */
/* .line 97 */
/* check-cast v0, Ljava/util/Map; */
/* .line 98 */
/* .restart local v0 # "simInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* check-cast v2, Ljava/lang/String; */
v2 = java.lang.Integer .parseInt ( v2 );
/* .line 99 */
/* .local v2, "slotNum":I */
v3 = this.mContext;
com.xiaomi.NetworkBoost.NetworkSDK.telephony.TelephonyUtil .getSubscriberId ( v3,v2 );
this.mImsi1 = v3;
/* .line 100 */
/* iput-boolean v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim2Inserted:Z */
/* .line 101 */
/* move-object v0, v3 */
/* check-cast v0, Ljava/util/Map; */
/* .line 102 */
/* check-cast v3, Ljava/lang/String; */
v2 = java.lang.Integer .parseInt ( v3 );
/* .line 103 */
v3 = this.mContext;
com.xiaomi.NetworkBoost.NetworkSDK.telephony.TelephonyUtil .getSubscriberId ( v3,v2 );
this.mImsi2 = v3;
/* .line 104 */
} // .end local v0 # "simInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
} // .end local v2 # "slotNum":I
/* .line 105 */
} // :cond_4
final String v2 = "no sim card inserted"; // const-string v2, "no sim card inserted"
android.util.Log .i ( v0,v2 );
/* .line 108 */
} // :goto_2
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper ) p0 ).isImsiMissed ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->isImsiMissed()Z
/* xor-int/2addr v0, v4 */
/* .line 69 */
} // :cond_5
} // :goto_3
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim1Inserted:Z */
/* .line 70 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim2Inserted:Z */
/* .line 71 */
this.mImsi1 = v2;
/* .line 72 */
this.mImsi2 = v2;
/* .line 73 */
} // .end method
