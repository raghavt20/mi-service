public class com.xiaomi.NetworkBoost.NetworkSDK.telephony.DualSimInfoManager {
	 /* .source "DualSimInfoManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/DualSimInfoManager$ISimInfoChangeListener; */
	 /* } */
} // .end annotation
/* # direct methods */
public com.xiaomi.NetworkBoost.NetworkSDK.telephony.DualSimInfoManager ( ) {
	 /* .locals 0 */
	 /* .line 11 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
public static java.util.List getSimInfoList ( android.content.Context p0 ) {
	 /* .locals 1 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Landroid/content/Context;", */
	 /* ")", */
	 /* "Ljava/util/List<", */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/String;", */
	 /* ">;>;" */
	 /* } */
} // .end annotation
/* .line 25 */
miui.securitycenter.DualSim.DualSimInfoManagerWrapper .getSimInfoList ( p0 );
} // .end method
public static void registerChangeListener ( android.content.Context p0, miui.securitycenter.DualSim.DualSimInfoManagerWrapper$ISimInfoChangeWrapperListener p1 ) {
/* .locals 0 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .param p1, "listener" # Lmiui/securitycenter/DualSim/DualSimInfoManagerWrapper$ISimInfoChangeWrapperListener; */
/* .line 18 */
return;
} // .end method
public static void unRegisterChangeListener ( android.content.Context p0, miui.securitycenter.DualSim.DualSimInfoManagerWrapper$ISimInfoChangeWrapperListener p1 ) {
/* .locals 0 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .param p1, "listener" # Lmiui/securitycenter/DualSim/DualSimInfoManagerWrapper$ISimInfoChangeWrapperListener; */
/* .line 21 */
miui.securitycenter.DualSim.DualSimInfoManagerWrapper .unRegisterSimInfoChangeListener ( p0,p1 );
/* .line 22 */
return;
} // .end method
