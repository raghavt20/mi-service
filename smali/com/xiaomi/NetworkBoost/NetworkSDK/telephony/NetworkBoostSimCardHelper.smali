.class public Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;
.super Ljava/lang/Object;
.source "NetworkBoostSimCardHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$NetworkBoostSingleSimCardHelper;
    }
.end annotation


# static fields
.field public static final DEFAULT_NULL_IMSI:Ljava/lang/String; = "default"

.field private static final TAG:Ljava/lang/String; = "NetworkBoostSimCardHelper"

.field private static sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mImsi1:Ljava/lang/String;

.field private mImsi2:Ljava/lang/String;

.field protected mIsSim1Inserted:Z

.field private mIsSim2Inserted:Z

.field private mSimInfoChangeListener:Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/DualSimInfoManager$ISimInfoChangeListener;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string v0, "default"

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mImsi1:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mImsi2:Ljava/lang/String;

    .line 43
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$1;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$1;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mSimInfoChangeListener:Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/DualSimInfoManager$ISimInfoChangeListener;

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mContext:Landroid/content/Context;

    .line 56
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->updateSimState()Z

    .line 57
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    const-class v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;

    monitor-enter v0

    .line 31
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;

    if-nez v1, :cond_1

    .line 33
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/telephony/TelephonyManager;->isMultiSimEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34
    new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;

    goto :goto_0

    .line 36
    :cond_0
    new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$NetworkBoostSingleSimCardHelper;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$NetworkBoostSingleSimCardHelper;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;

    .line 38
    :goto_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;

    invoke-direct {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->initForUIProcess()V

    .line 40
    :cond_1
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 30
    .end local p0    # "context":Landroid/content/Context;
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .line 27
    invoke-static {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;

    .line 28
    return-void
.end method

.method private initForUIProcess()V
    .locals 0

    .line 52
    return-void
.end method


# virtual methods
.method public getCurrentMobileSlotNum()I
    .locals 1

    .line 116
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/TelephonyUtil;->getCurrentMobileSlotNum()I

    move-result v0

    return v0
.end method

.method public getSimImsi(I)Ljava/lang/String;
    .locals 1
    .param p1, "slotNum"    # I

    .line 120
    if-nez p1, :cond_0

    .line 121
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mImsi1:Ljava/lang/String;

    return-object v0

    .line 122
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mImsi2:Ljava/lang/String;

    return-object v0

    .line 125
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isImsiMissed()Z
    .locals 1

    .line 112
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim1Inserted:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mImsi1:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim2Inserted:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mImsi2:Ljava/lang/String;

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public updateSimState()Z
    .locals 7

    .line 60
    const-string v0, "NetworkBoostSimCardHelper"

    const/4 v1, 0x0

    .line 63
    .local v1, "siminfoList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/DualSimInfoManager;->getSimInfoList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    .line 66
    goto :goto_0

    .line 64
    :catch_0
    move-exception v2

    .line 65
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "get sim info exception!"

    invoke-static {v0, v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 68
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    const-string v2, "default"

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_5

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    goto/16 :goto_3

    .line 76
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    const-string/jumbo v6, "slotNum"

    if-ne v5, v4, :cond_3

    .line 77
    const-string v5, "one sim card inserted"

    invoke-static {v0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 79
    .local v0, "simInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 80
    .local v5, "slotNum":I
    if-nez v5, :cond_1

    .line 81
    iput-boolean v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim1Inserted:Z

    .line 82
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim2Inserted:Z

    .line 84
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mContext:Landroid/content/Context;

    invoke-static {v3, v5}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/TelephonyUtil;->getSubscriberId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mImsi1:Ljava/lang/String;

    .line 85
    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mImsi2:Ljava/lang/String;

    goto :goto_1

    .line 87
    :cond_1
    if-ne v5, v4, :cond_2

    .line 88
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim1Inserted:Z

    .line 89
    iput-boolean v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim2Inserted:Z

    .line 91
    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mImsi1:Ljava/lang/String;

    .line 92
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mContext:Landroid/content/Context;

    invoke-static {v2, v5}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/TelephonyUtil;->getSubscriberId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mImsi2:Ljava/lang/String;

    .line 94
    .end local v0    # "simInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "slotNum":I
    :cond_2
    :goto_1
    goto :goto_2

    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v5, 0x2

    if-ne v2, v5, :cond_4

    .line 95
    const-string/jumbo v2, "two sim cards inserted"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iput-boolean v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim1Inserted:Z

    .line 97
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 98
    .restart local v0    # "simInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 99
    .local v2, "slotNum":I
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mContext:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/TelephonyUtil;->getSubscriberId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mImsi1:Ljava/lang/String;

    .line 100
    iput-boolean v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim2Inserted:Z

    .line 101
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/util/Map;

    .line 102
    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 103
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mContext:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/TelephonyUtil;->getSubscriberId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mImsi2:Ljava/lang/String;

    .line 104
    .end local v0    # "simInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "slotNum":I
    goto :goto_2

    .line 105
    :cond_4
    const-string v2, "no sim card inserted"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->isImsiMissed()Z

    move-result v0

    xor-int/2addr v0, v4

    return v0

    .line 69
    :cond_5
    :goto_3
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim1Inserted:Z

    .line 70
    iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mIsSim2Inserted:Z

    .line 71
    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mImsi1:Ljava/lang/String;

    .line 72
    iput-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->mImsi2:Ljava/lang/String;

    .line 73
    return v4
.end method
