.class public Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$NetworkBoostSingleSimCardHelper;
.super Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;
.source "NetworkBoostSimCardHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NetworkBoostSingleSimCardHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 132
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;-><init>(Landroid/content/Context;Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper-IA;)V

    .line 133
    return-void
.end method


# virtual methods
.method public updateSimState()Z
    .locals 3

    .line 138
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$NetworkBoostSingleSimCardHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/TelephonyUtil;->getSubscriberId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "newImsi":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 140
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$NetworkBoostSingleSimCardHelper;->mIsSim1Inserted:Z

    .line 141
    const-string v0, "default"

    goto :goto_0

    .line 143
    :cond_0
    iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$NetworkBoostSingleSimCardHelper;->mIsSim1Inserted:Z

    .line 146
    :goto_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$NetworkBoostSingleSimCardHelper;->mImsi1:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 147
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$NetworkBoostSingleSimCardHelper;->mImsi1:Ljava/lang/String;

    .line 150
    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper$NetworkBoostSingleSimCardHelper;->isImsiMissed()Z

    move-result v1

    xor-int/2addr v1, v2

    return v1
.end method
