class com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$5 implements com.xiaomi.NetworkBoost.StatusManager$INetworkInterfaceListener {
	 /* .source "NetworkSDKService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* .line 351 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onNetwrokInterfaceChange ( java.lang.String p0, Integer p1, Boolean p2, Boolean p3, Boolean p4, Boolean p5, java.lang.String p6 ) {
/* .locals 5 */
/* .param p1, "ifacename" # Ljava/lang/String; */
/* .param p2, "ifacenum" # I */
/* .param p3, "wifiready" # Z */
/* .param p4, "slavewifiready" # Z */
/* .param p5, "dataready" # Z */
/* .param p6, "slaveDataReady" # Z */
/* .param p7, "status" # Ljava/lang/String; */
/* .line 355 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 356 */
/* .local v0, "interfaceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) p1 ).split ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 357 */
/* .local v1, "interfaces":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, v1 */
/* if-ge v2, v3, :cond_0 */
/* .line 358 */
/* aget-object v3, v1, v2 */
/* .line 357 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 361 */
} // .end local v2 # "i":I
} // :cond_0
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fputmAvailableIfaces ( v2,v0 );
/* .line 363 */
final String v2 = "ON_AVAILABLE"; // const-string v2, "ON_AVAILABLE"
v2 = (( java.lang.String ) p7 ).equals ( v2 ); // invoke-virtual {p7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v3 = -1; // const/4 v3, -0x1
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 364 */
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmAvailableIfaces ( v2 );
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v2 ).ifaceAddedCallBackNotice ( v4 ); // invoke-virtual {v2, v4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->ifaceAddedCallBackNotice(Ljava/util/List;)V
/* .line 365 */
/* const-string/jumbo v2, "wlan0" */
v2 = (( java.lang.String ) p1 ).indexOf ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* if-eq v2, v3, :cond_1 */
/* .line 366 */
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mbroadcastPrimaryConnected ( v2 );
/* .line 368 */
} // :cond_1
final String v2 = "ON_LOST"; // const-string v2, "ON_LOST"
v2 = (( java.lang.String ) p7 ).equals ( v2 ); // invoke-virtual {p7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 369 */
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmAvailableIfaces ( v2 );
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v2 ).ifaceRemovedCallBackNotice ( v4 ); // invoke-virtual {v2, v4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->ifaceRemovedCallBackNotice(Ljava/util/List;)V
/* .line 370 */
} // :cond_2
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 371 */
/* const-string/jumbo v2, "wlan" */
v2 = (( java.lang.String ) p1 ).indexOf ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* if-eq v2, v3, :cond_3 */
/* .line 372 */
/* const-string/jumbo v2, "wlan1" */
v2 = (( java.lang.String ) p1 ).indexOf ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* if-eq v2, v3, :cond_3 */
/* .line 373 */
v2 = this.this$0;
int v3 = 1; // const/4 v3, 0x1
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v2 ).onSlaveWifiConnected ( v3 ); // invoke-virtual {v2, v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiConnected(Z)V
/* .line 377 */
} // :cond_3
return;
} // .end method
