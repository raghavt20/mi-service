public class com.xiaomi.NetworkBoost.NetworkSDK.AppDataUsage {
	 /* .source "AppDataUsage.java" */
	 /* # static fields */
	 public static final java.lang.String MOBILE;
	 public static final java.lang.String RX_BACK_BYTES;
	 public static final java.lang.String RX_BYTES;
	 public static final java.lang.String RX_FORE_BYTES;
	 public static final java.lang.String TX_BACK_BYTES;
	 public static final java.lang.String TX_BYTES;
	 public static final java.lang.String TX_FORE_BYTES;
	 public static final java.lang.String WIFI;
	 /* # instance fields */
	 private Long mRxBytes;
	 private Long mTxBytes;
	 private Long mrxBackgroundBytes;
	 private Long mrxForegroundBytes;
	 private Long mtxBackgroundBytes;
	 private Long mtxForegroundBytes;
	 /* # direct methods */
	 public com.xiaomi.NetworkBoost.NetworkSDK.AppDataUsage ( ) {
		 /* .locals 2 */
		 /* .param p1, "mRxBytes" # J */
		 /* .param p3, "mTxBytes" # J */
		 /* .param p5, "mtxForegroundBytes" # J */
		 /* .param p7, "mrxForegroundBytes" # J */
		 /* .line 25 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 7 */
		 /* const-wide/16 v0, 0x0 */
		 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J */
		 /* .line 8 */
		 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J */
		 /* .line 10 */
		 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxForegroundBytes:J */
		 /* .line 11 */
		 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxForegroundBytes:J */
		 /* .line 13 */
		 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxBackgroundBytes:J */
		 /* .line 14 */
		 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxBackgroundBytes:J */
		 /* .line 26 */
		 (( com.xiaomi.NetworkBoost.NetworkSDK.AppDataUsage ) p0 ).addRxBytes ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->addRxBytes(J)V
		 /* .line 27 */
		 (( com.xiaomi.NetworkBoost.NetworkSDK.AppDataUsage ) p0 ).addTxBytes ( p3, p4 ); // invoke-virtual {p0, p3, p4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->addTxBytes(J)V
		 /* .line 28 */
		 (( com.xiaomi.NetworkBoost.NetworkSDK.AppDataUsage ) p0 ).addTxForeBytes ( p5, p6 ); // invoke-virtual {p0, p5, p6}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->addTxForeBytes(J)V
		 /* .line 29 */
		 (( com.xiaomi.NetworkBoost.NetworkSDK.AppDataUsage ) p0 ).addRxForeBytes ( p7, p8 ); // invoke-virtual {p0, p7, p8}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->addRxForeBytes(J)V
		 /* .line 30 */
		 (( com.xiaomi.NetworkBoost.NetworkSDK.AppDataUsage ) p0 ).calculateTxBackBytes ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->calculateTxBackBytes()V
		 /* .line 31 */
		 (( com.xiaomi.NetworkBoost.NetworkSDK.AppDataUsage ) p0 ).calculateRxBackBytes ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->calculateRxBackBytes()V
		 /* .line 32 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void addRxBytes ( Long p0 ) {
		 /* .locals 2 */
		 /* .param p1, "bytes" # J */
		 /* .line 43 */
		 /* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J */
		 /* add-long/2addr v0, p1 */
		 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J */
		 /* .line 44 */
		 return;
	 } // .end method
	 public void addRxForeBytes ( Long p0 ) {
		 /* .locals 2 */
		 /* .param p1, "mrxForegroundBytes" # J */
		 /* .line 55 */
		 /* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxForegroundBytes:J */
		 /* add-long/2addr v0, p1 */
		 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxForegroundBytes:J */
		 /* .line 56 */
		 return;
	 } // .end method
	 public void addTxBytes ( Long p0 ) {
		 /* .locals 2 */
		 /* .param p1, "bytes" # J */
		 /* .line 47 */
		 /* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J */
		 /* add-long/2addr v0, p1 */
		 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J */
		 /* .line 48 */
		 return;
	 } // .end method
	 public void addTxForeBytes ( Long p0 ) {
		 /* .locals 2 */
		 /* .param p1, "mtxForegroundBytes" # J */
		 /* .line 51 */
		 /* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxForegroundBytes:J */
		 /* add-long/2addr v0, p1 */
		 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxForegroundBytes:J */
		 /* .line 52 */
		 return;
	 } // .end method
	 public void calculateRxBackBytes ( ) {
		 /* .locals 6 */
		 /* .line 63 */
		 /* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxBackgroundBytes:J */
		 /* iget-wide v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J */
		 /* iget-wide v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxForegroundBytes:J */
		 /* sub-long/2addr v2, v4 */
		 /* add-long/2addr v0, v2 */
		 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxBackgroundBytes:J */
		 /* .line 64 */
		 return;
	 } // .end method
	 public void calculateTxBackBytes ( ) {
		 /* .locals 6 */
		 /* .line 59 */
		 /* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxBackgroundBytes:J */
		 /* iget-wide v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J */
		 /* iget-wide v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxForegroundBytes:J */
		 /* sub-long/2addr v2, v4 */
		 /* add-long/2addr v0, v2 */
		 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxBackgroundBytes:J */
		 /* .line 60 */
		 return;
	 } // .end method
	 public Long getRxBytes ( ) {
		 /* .locals 2 */
		 /* .line 35 */
		 /* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J */
		 /* return-wide v0 */
	 } // .end method
	 public Long getTotal ( ) {
		 /* .locals 4 */
		 /* .line 67 */
		 /* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J */
		 /* iget-wide v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J */
		 /* add-long/2addr v0, v2 */
		 /* return-wide v0 */
	 } // .end method
	 public Long getTxBytes ( ) {
		 /* .locals 2 */
		 /* .line 39 */
		 /* iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J */
		 /* return-wide v0 */
	 } // .end method
	 public void reset ( ) {
		 /* .locals 2 */
		 /* .line 71 */
		 /* const-wide/16 v0, 0x0 */
		 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J */
		 /* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J */
		 /* .line 72 */
		 return;
	 } // .end method
	 public java.util.Map toMap ( Boolean p0 ) {
		 /* .locals 5 */
		 /* .param p1, "isMobile" # Z */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(Z)", */
		 /* "Ljava/util/Map<", */
		 /* "Ljava/lang/String;", */
		 /* "Ljava/lang/String;", */
		 /* ">;" */
		 /* } */
	 } // .end annotation
	 /* .line 75 */
	 if ( p1 != null) { // if-eqz p1, :cond_0
		 final String v0 = "mobile-"; // const-string v0, "mobile-"
	 } // :cond_0
	 /* const-string/jumbo v0, "wifi-" */
	 /* .line 77 */
	 /* .local v0, "TYPE":Ljava/lang/String; */
} // :goto_0
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* .line 78 */
/* .local v1, "tomap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "rxBytes"; // const-string v3, "rxBytes"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* iget-wide v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mRxBytes:J */
java.lang.String .valueOf ( v3,v4 );
/* .line 79 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "txBytes" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* iget-wide v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mTxBytes:J */
java.lang.String .valueOf ( v3,v4 );
/* .line 80 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "txForegroundBytes" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* iget-wide v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxForegroundBytes:J */
java.lang.String .valueOf ( v3,v4 );
/* .line 81 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "rxForegroundBytes"; // const-string v3, "rxForegroundBytes"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* iget-wide v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxForegroundBytes:J */
java.lang.String .valueOf ( v3,v4 );
/* .line 82 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "txBackgroundBytes" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* iget-wide v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mtxBackgroundBytes:J */
java.lang.String .valueOf ( v3,v4 );
/* .line 83 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "rxBackgroundBytes"; // const-string v3, "rxBackgroundBytes"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* iget-wide v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->mrxBackgroundBytes:J */
java.lang.String .valueOf ( v3,v4 );
/* .line 85 */
} // .end method
