class com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$InternalHandler extends android.os.Handler {
	 /* .source "NetworkSDKService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "InternalHandler" */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService this$0; //synthetic
/* # direct methods */
public com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$InternalHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 993 */
this.this$0 = p1;
/* .line 994 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 995 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 5 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 998 */
/* iget v0, p1, Landroid/os/Message;->what:I */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
/* packed-switch v0, :pswitch_data_0 */
/* :pswitch_0 */
/* goto/16 :goto_3 */
/* .line 1047 */
/* :pswitch_1 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmLock ( v0 );
/* monitor-enter v0 */
/* .line 1048 */
try { // :try_start_0
	 v3 = this.this$0;
	 v3 = 	 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmIsStartMonitorByBackground ( v3 );
	 /* if-nez v3, :cond_0 */
	 /* .line 1049 */
	 v3 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fputmDualWifiBackgroundCount ( v3,v2 );
	 /* .line 1050 */
	 v2 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fputmIsStartMonitorByBackground ( v2,v1 );
	 /* .line 1051 */
	 v1 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mdualWifiBackgroundMonitor ( v1 );
	 /* .line 1053 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1054 */
/* goto/16 :goto_3 */
/* .line 1053 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 1059 */
/* :pswitch_2 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmLock ( v0 );
/* monitor-enter v0 */
/* .line 1060 */
try { // :try_start_1
	 v1 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fputmDualWifiBackgroundCount ( v1,v2 );
	 /* .line 1061 */
	 v1 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fputmIsStartMonitorByBackground ( v1,v2 );
	 /* .line 1062 */
	 /* monitor-exit v0 */
	 /* :try_end_1 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
	 /* .line 1063 */
	 v0 = this.this$0;
	 v0 = 	 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmIsEverClosedByBackground ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_3
		 /* .line 1064 */
		 v0 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mdualWifiBackgroundMonitorRestart ( v0 );
		 /* goto/16 :goto_3 */
		 /* .line 1062 */
		 /* :catchall_1 */
		 /* move-exception v1 */
		 try { // :try_start_2
			 /* monitor-exit v0 */
			 /* :try_end_2 */
			 /* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
			 /* throw v1 */
			 /* .line 1056 */
			 /* :pswitch_3 */
			 v0 = this.this$0;
			 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mdualWifiBackgroundMonitor ( v0 );
			 /* .line 1057 */
			 /* goto/16 :goto_3 */
			 /* .line 1032 */
			 /* :pswitch_4 */
			 v0 = this.this$0;
			 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmLock ( v0 );
			 /* monitor-enter v0 */
			 /* .line 1034 */
			 try { // :try_start_3
				 v3 = this.this$0;
				 v3 = 				 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$msetOrGetEverOpenedDualWifi ( v3,v2,v2 );
				 /* .line 1035 */
				 /* .local v3, "isEverOpened":Z */
				 if ( v3 != null) { // if-eqz v3, :cond_1
					 /* .line 1036 */
					 v4 = this.this$0;
					 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmSlaveWifiManager ( v4 );
					 if ( v4 != null) { // if-eqz v4, :cond_1
						 /* .line 1037 */
						 v4 = this.this$0;
						 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmSlaveWifiManager ( v4 );
						 (( android.net.wifi.SlaveWifiManager ) v4 ).setWifiSlaveEnabled ( v2 ); // invoke-virtual {v4, v2}, Landroid/net/wifi/SlaveWifiManager;->setWifiSlaveEnabled(Z)Z
						 /* .line 1038 */
						 v4 = this.this$0;
						 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$msetOrGetEverOpenedDualWifi ( v4,v1,v2 );
						 /* :try_end_3 */
						 /* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
						 /* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
						 /* .line 1043 */
					 } // .end local v3 # "isEverOpened":Z
				 } // :cond_1
				 /* .line 1044 */
				 /* :catchall_2 */
				 /* move-exception v1 */
				 /* .line 1041 */
				 /* :catch_0 */
				 /* move-exception v1 */
				 /* .line 1042 */
				 /* .local v1, "e":Ljava/lang/Exception; */
				 try { // :try_start_4
					 (( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
					 /* .line 1044 */
				 } // .end local v1 # "e":Ljava/lang/Exception;
			 } // :goto_0
			 /* monitor-exit v0 */
			 /* .line 1045 */
			 /* goto/16 :goto_3 */
			 /* .line 1044 */
		 } // :goto_1
		 /* monitor-exit v0 */
		 /* :try_end_4 */
		 /* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
		 /* throw v1 */
		 /* .line 1026 */
		 /* :pswitch_5 */
		 v0 = this.obj;
		 /* check-cast v0, Ljava/lang/Integer; */
		 v0 = 		 (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
		 /* .line 1027 */
		 /* .local v0, "releaseUid":I */
		 final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v4 = "EVENT_RELEASE_DUAL_WIFI releaseUid = "; // const-string v4, "EVENT_RELEASE_DUAL_WIFI releaseUid = "
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .d ( v1,v3 );
		 /* .line 1028 */
		 v1 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmSlaveWifiManager ( v1 );
		 if ( v1 != null) { // if-eqz v1, :cond_3
			 /* .line 1029 */
			 v1 = this.this$0;
			 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mhandleMultiAppsDualWifi ( v1,v2,v0 );
			 /* goto/16 :goto_3 */
			 /* .line 1023 */
		 } // .end local v0 # "releaseUid":I
		 /* :pswitch_6 */
		 v0 = this.this$0;
		 v1 = 		 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmDsdaCapability ( v0 );
		 (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v0 ).onDsdaStateChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onDsdaStateChanged(Z)V
		 /* .line 1024 */
		 /* goto/16 :goto_3 */
		 /* .line 1001 */
		 /* :pswitch_7 */
		 try { // :try_start_5
			 vendor.xiaomi.hidl.minet.V1_0.IMiNetService .getService ( );
			 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$sfputmMiNetService ( v0 );
			 /* .line 1002 */
			 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$sfgetmMiNetService ( );
			 /* if-nez v0, :cond_2 */
			 /* .line 1003 */
			 final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
			 final String v1 = "HAL service is null"; // const-string v1, "HAL service is null"
			 android.util.Log .e ( v0,v1 );
			 /* .line 1004 */
			 v0 = this.this$0;
			 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmHandler ( v0 );
			 v1 = this.this$0;
			 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmHandler ( v1 );
			 java.lang.Integer .valueOf ( v2 );
			 /* const/16 v3, 0x67 */
			 (( android.os.Handler ) v1 ).obtainMessage ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
			 /* const-wide/16 v2, 0xfa0 */
			 (( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
			 /* .line 1007 */
		 } // :cond_2
		 final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
		 final String v1 = "HAL service get success"; // const-string v1, "HAL service get success"
		 android.util.Log .d ( v0,v1 );
		 /* .line 1008 */
		 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$sfgetmMiNetService ( );
		 v1 = this.this$0;
		 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmDeathRecipient ( v1 );
		 /* const-wide/16 v2, 0x0 */
		 /* .line 1009 */
		 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$sfgetmMiNetService ( );
		 /* :try_end_5 */
		 /* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_1 */
		 /* .line 1013 */
	 } // :goto_2
	 /* .line 1011 */
	 /* :catch_1 */
	 /* move-exception v0 */
	 /* .line 1012 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "Exception:"; // const-string v3, "Exception:"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .e ( v1,v2 );
	 /* .line 1014 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 1017 */
/* :pswitch_8 */
v0 = this.obj;
/* check-cast v0, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 1018 */
/* .local v0, "enable":Z */
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* .line 1019 */
/* .local v1, "uid":I */
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmSlaveWifiManager ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_3
	 /* .line 1020 */
	 v2 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mhandleMultiAppsDualWifi ( v2,v0,v1 );
	 /* .line 1069 */
} // .end local v0 # "enable":Z
} // .end local v1 # "uid":I
} // :cond_3
} // :goto_3
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x64 */
/* :pswitch_8 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
