.class public Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKUtils;
.super Ljava/lang/Object;
.source "NetworkSDKUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized copyFromNetWlanLinkLayerQoE(Landroid/net/wifi/WlanLinkLayerQoE;Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .locals 3
    .param p0, "source"    # Landroid/net/wifi/WlanLinkLayerQoE;
    .param p1, "copyResult"    # Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    const-class v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKUtils;

    monitor-enter v0

    .line 10
    if-nez p1, :cond_0

    .line 11
    :try_start_0
    new-instance v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-direct {v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object p1, v1

    goto :goto_0

    .line 9
    .end local p0    # "source":Landroid/net/wifi/WlanLinkLayerQoE;
    .end local p1    # "copyResult":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    :catchall_0
    move-exception p0

    goto/16 :goto_1

    .line 13
    .restart local p0    # "source":Landroid/net/wifi/WlanLinkLayerQoE;
    .restart local p1    # "copyResult":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    :cond_0
    :goto_0
    if-nez p0, :cond_1

    .line 14
    monitor-exit v0

    return-object p1

    .line 16
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setVersion(Ljava/lang/String;)V

    .line 17
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getSsid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setSsid(Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRssi_mgmt()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRssi_mgmt(I)V

    .line 20
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getMpduLostRatio()D

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setMpduLostRatio(D)V

    .line 21
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRetriesRatio()D

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetriesRatio(D)V

    .line 23
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getFrequency()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setFrequency(I)V

    .line 24
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRadioOnTimeMs()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRadioOnTimeMs(I)V

    .line 25
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getCcaBusyTimeMs()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setCcaBusyTimeMs(I)V

    .line 27
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getBw()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setBw(I)V

    .line 28
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRateMcsIdx()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRateMcsIdx(I)V

    .line 29
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getBitRateInKbps()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setBitRateInKbps(I)V

    .line 31
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRxmpdu_be()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRxmpdu_be(J)V

    .line 32
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getTxmpdu_be()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setTxmpdu_be(J)V

    .line 33
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getLostmpdu_be()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setLostmpdu_be(J)V

    .line 34
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRetries_be()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetries_be(J)V

    .line 36
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRxmpdu_bk()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRxmpdu_bk(J)V

    .line 37
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getTxmpdu_bk()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setTxmpdu_bk(J)V

    .line 38
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getLostmpdu_bk()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setLostmpdu_bk(J)V

    .line 39
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRetries_bk()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetries_bk(J)V

    .line 41
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRxmpdu_vi()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRxmpdu_vi(J)V

    .line 42
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getTxmpdu_vi()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setTxmpdu_vi(J)V

    .line 43
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getLostmpdu_vi()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setLostmpdu_vi(J)V

    .line 44
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRetries_vi()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetries_vi(J)V

    .line 46
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRxmpdu_vo()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRxmpdu_vo(J)V

    .line 47
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getTxmpdu_vo()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setTxmpdu_vo(J)V

    .line 48
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getLostmpdu_vo()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setLostmpdu_vo(J)V

    .line 49
    invoke-virtual {p0}, Landroid/net/wifi/WlanLinkLayerQoE;->getRetries_vo()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetries_vo(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 51
    monitor-exit v0

    return-object p1

    .line 9
    .end local p0    # "source":Landroid/net/wifi/WlanLinkLayerQoE;
    .end local p1    # "copyResult":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    :goto_1
    monitor-exit v0

    throw p0
.end method

.method public static intervalTransformation(I)I
    .locals 0
    .param p0, "interval"    # I

    .line 55
    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 72
    :pswitch_0
    const/16 p0, 0x1f40

    .line 73
    goto :goto_0

    .line 69
    :pswitch_1
    const/16 p0, 0xfa0

    .line 70
    goto :goto_0

    .line 66
    :pswitch_2
    const/16 p0, 0x7d0

    .line 67
    goto :goto_0

    .line 63
    :pswitch_3
    const/16 p0, 0x3e8

    .line 64
    goto :goto_0

    .line 60
    :pswitch_4
    const/16 p0, 0xc8

    .line 61
    goto :goto_0

    .line 57
    :pswitch_5
    const/16 p0, 0x64

    .line 58
    nop

    .line 78
    :goto_0
    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
