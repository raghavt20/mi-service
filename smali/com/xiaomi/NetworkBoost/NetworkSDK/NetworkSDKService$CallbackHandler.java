class com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$CallbackHandler extends android.os.Handler {
	 /* .source "NetworkSDKService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "CallbackHandler" */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService this$0; //synthetic
/* # direct methods */
public com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$CallbackHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 2096 */
this.this$0 = p1;
/* .line 2097 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 2098 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 7 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 2101 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2102 */
/* .local v0, "res":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 2103 */
/* .local v1, "N":I */
/* iget v2, p1, Landroid/os/Message;->what:I */
/* sparse-switch v2, :sswitch_data_0 */
/* goto/16 :goto_c */
/* .line 2184 */
/* :sswitch_0 */
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mnetworkPriorityCallBackSend ( v2,p1 );
/* .line 2186 */
/* :sswitch_1 */
v2 = this.obj;
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 2187 */
/* .local v2, "status":I */
v3 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v3 );
v1 = (( android.os.RemoteCallbackList ) v3 ).beginBroadcast ( ); // invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 2188 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v1, :cond_0 */
/* .line 2190 */
try { // :try_start_0
v4 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v4 );
(( android.os.RemoteCallbackList ) v4 ).getBroadcastItem ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v4, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 2195 */
/* .line 2191 */
/* :catch_0 */
/* move-exception v4 */
/* .line 2194 */
/* .local v4, "e":Landroid/os/RemoteException; */
v5 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v5 );
(( android.os.RemoteCallbackList ) v6 ).getBroadcastItem ( v3 ); // invoke-virtual {v6, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v6, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v5 ).unregisterCallback ( v6 ); // invoke-virtual {v5, v6}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
/* .line 2188 */
} // .end local v4 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 2197 */
} // .end local v3 # "i":I
} // :cond_0
v3 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v3 );
(( android.os.RemoteCallbackList ) v3 ).finishBroadcast ( ); // invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 2198 */
/* goto/16 :goto_c */
/* .line 2213 */
} // .end local v2 # "status":I
/* :sswitch_2 */
v2 = this.obj;
/* check-cast v2, Ljava/lang/Boolean; */
v2 = (( java.lang.Boolean ) v2 ).booleanValue ( ); // invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 2214 */
/* .local v2, "screenStatus":Z */
v3 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$msetScreenStatus ( v3,v2 );
/* .line 2215 */
/* goto/16 :goto_c */
/* .line 2203 */
} // .end local v2 # "screenStatus":Z
/* :sswitch_3 */
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mmediaPlayerPolicyCallBackSend ( v2,p1 );
/* .line 2204 */
/* goto/16 :goto_c */
/* .line 2200 */
/* :sswitch_4 */
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mdsdaStateChangedCallBackSend ( v2,p1 );
/* .line 2201 */
/* goto/16 :goto_c */
/* .line 2170 */
/* :sswitch_5 */
v2 = this.obj;
/* check-cast v2, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v2 ).booleanValue ( ); // invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 2171 */
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v2 );
v1 = (( android.os.RemoteCallbackList ) v2 ).beginBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 2172 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_2
/* if-ge v2, v1, :cond_1 */
/* .line 2174 */
try { // :try_start_1
v3 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v3 );
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 2179 */
/* .line 2175 */
/* :catch_1 */
/* move-exception v3 */
/* .line 2178 */
/* .local v3, "e":Landroid/os/RemoteException; */
v4 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v4 );
(( android.os.RemoteCallbackList ) v5 ).getBroadcastItem ( v2 ); // invoke-virtual {v5, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v4 ).unregisterCallback ( v5 ); // invoke-virtual {v4, v5}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
/* .line 2172 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 2181 */
} // .end local v2 # "i":I
} // :cond_1
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v2 );
(( android.os.RemoteCallbackList ) v2 ).finishBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 2182 */
/* goto/16 :goto_c */
/* .line 2207 */
/* :sswitch_6 */
try { // :try_start_2
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mappWifiSelectionMonitor ( v2 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_2 */
/* .line 2210 */
/* goto/16 :goto_c */
/* .line 2208 */
/* :catch_2 */
/* move-exception v2 */
/* .line 2209 */
/* .local v2, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 2211 */
} // .end local v2 # "e":Ljava/lang/Exception;
/* goto/16 :goto_c */
/* .line 2167 */
/* :sswitch_7 */
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mwifiLinkLayerStatsPoll ( v2 );
/* .line 2168 */
/* goto/16 :goto_c */
/* .line 2164 */
/* :sswitch_8 */
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mifaceRemovedCallBackSend ( v2,p1 );
/* .line 2165 */
/* goto/16 :goto_c */
/* .line 2161 */
/* :sswitch_9 */
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mifaceAddCallBackSend ( v2,p1 );
/* .line 2162 */
/* goto/16 :goto_c */
/* .line 2147 */
/* :sswitch_a */
v2 = this.obj;
/* check-cast v2, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v2 ).booleanValue ( ); // invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 2148 */
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v2 );
v1 = (( android.os.RemoteCallbackList ) v2 ).beginBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 2149 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_4
/* if-ge v2, v1, :cond_2 */
/* .line 2151 */
try { // :try_start_3
v3 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v3 );
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* :try_end_3 */
/* .catch Landroid/os/RemoteException; {:try_start_3 ..:try_end_3} :catch_3 */
/* .line 2156 */
/* .line 2152 */
/* :catch_3 */
/* move-exception v3 */
/* .line 2155 */
/* .restart local v3 # "e":Landroid/os/RemoteException; */
v4 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v4 );
(( android.os.RemoteCallbackList ) v5 ).getBroadcastItem ( v2 ); // invoke-virtual {v5, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v4 ).unregisterCallback ( v5 ); // invoke-virtual {v4, v5}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
/* .line 2149 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_5
/* add-int/lit8 v2, v2, 0x1 */
/* .line 2158 */
} // .end local v2 # "i":I
} // :cond_2
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v2 );
(( android.os.RemoteCallbackList ) v2 ).finishBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 2159 */
/* goto/16 :goto_c */
/* .line 2133 */
/* :sswitch_b */
v2 = this.obj;
/* check-cast v2, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v2 ).booleanValue ( ); // invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 2134 */
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v2 );
v1 = (( android.os.RemoteCallbackList ) v2 ).beginBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 2135 */
int v2 = 0; // const/4 v2, 0x0
/* .restart local v2 # "i":I */
} // :goto_6
/* if-ge v2, v1, :cond_3 */
/* .line 2137 */
try { // :try_start_4
v3 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v3 );
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* :try_end_4 */
/* .catch Landroid/os/RemoteException; {:try_start_4 ..:try_end_4} :catch_4 */
/* .line 2142 */
/* .line 2138 */
/* :catch_4 */
/* move-exception v3 */
/* .line 2141 */
/* .restart local v3 # "e":Landroid/os/RemoteException; */
v4 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v4 );
(( android.os.RemoteCallbackList ) v5 ).getBroadcastItem ( v2 ); // invoke-virtual {v5, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v4 ).unregisterCallback ( v5 ); // invoke-virtual {v4, v5}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
/* .line 2135 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_7
/* add-int/lit8 v2, v2, 0x1 */
/* .line 2144 */
} // .end local v2 # "i":I
} // :cond_3
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v2 );
(( android.os.RemoteCallbackList ) v2 ).finishBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 2145 */
/* goto/16 :goto_c */
/* .line 2119 */
/* :sswitch_c */
v2 = this.obj;
/* check-cast v2, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v2 ).booleanValue ( ); // invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 2120 */
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v2 );
v1 = (( android.os.RemoteCallbackList ) v2 ).beginBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 2121 */
int v2 = 0; // const/4 v2, 0x0
/* .restart local v2 # "i":I */
} // :goto_8
/* if-ge v2, v1, :cond_4 */
/* .line 2123 */
try { // :try_start_5
v3 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v3 );
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* :try_end_5 */
/* .catch Landroid/os/RemoteException; {:try_start_5 ..:try_end_5} :catch_5 */
/* .line 2128 */
/* .line 2124 */
/* :catch_5 */
/* move-exception v3 */
/* .line 2127 */
/* .restart local v3 # "e":Landroid/os/RemoteException; */
v4 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v4 );
(( android.os.RemoteCallbackList ) v5 ).getBroadcastItem ( v2 ); // invoke-virtual {v5, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v4 ).unregisterCallback ( v5 ); // invoke-virtual {v4, v5}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
/* .line 2121 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_9
/* add-int/lit8 v2, v2, 0x1 */
/* .line 2130 */
} // .end local v2 # "i":I
} // :cond_4
v2 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v2 );
(( android.os.RemoteCallbackList ) v2 ).finishBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 2131 */
/* .line 2105 */
/* :sswitch_d */
v2 = this.obj;
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 2106 */
/* .local v2, "result":I */
v3 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v3 );
v1 = (( android.os.RemoteCallbackList ) v3 ).beginBroadcast ( ); // invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 2107 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_a
/* if-ge v3, v1, :cond_5 */
/* .line 2109 */
try { // :try_start_6
v4 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v4 );
(( android.os.RemoteCallbackList ) v4 ).getBroadcastItem ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v4, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* :try_end_6 */
/* .catch Landroid/os/RemoteException; {:try_start_6 ..:try_end_6} :catch_6 */
/* .line 2114 */
/* .line 2110 */
/* :catch_6 */
/* move-exception v4 */
/* .line 2113 */
/* .restart local v4 # "e":Landroid/os/RemoteException; */
v5 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v5 );
(( android.os.RemoteCallbackList ) v6 ).getBroadcastItem ( v3 ); // invoke-virtual {v6, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v6, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v5 ).unregisterCallback ( v6 ); // invoke-virtual {v5, v6}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
/* .line 2107 */
} // .end local v4 # "e":Landroid/os/RemoteException;
} // :goto_b
/* add-int/lit8 v3, v3, 0x1 */
/* .line 2116 */
} // .end local v3 # "i":I
} // :cond_5
v3 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmCallbacks ( v3 );
(( android.os.RemoteCallbackList ) v3 ).finishBroadcast ( ); // invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 2117 */
/* nop */
/* .line 2219 */
} // .end local v2 # "result":I
} // :goto_c
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x0 -> :sswitch_d */
/* 0x1 -> :sswitch_c */
/* 0x2 -> :sswitch_b */
/* 0x3 -> :sswitch_a */
/* 0x3ef -> :sswitch_9 */
/* 0x3f0 -> :sswitch_8 */
/* 0x3f1 -> :sswitch_7 */
/* 0x3f2 -> :sswitch_6 */
/* 0x3f3 -> :sswitch_5 */
/* 0x3f4 -> :sswitch_4 */
/* 0x3f5 -> :sswitch_3 */
/* 0x3f6 -> :sswitch_2 */
/* 0x3f7 -> :sswitch_0 */
/* 0x3f8 -> :sswitch_1 */
} // .end sparse-switch
} // .end method
