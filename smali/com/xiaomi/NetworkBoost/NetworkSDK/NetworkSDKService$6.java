class com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$6 extends android.content.BroadcastReceiver {
	 /* .source "NetworkSDKService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registDualWifiStateBroadcastReceiver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$6 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* .line 1247 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 1250 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$fgetmLock ( v0 );
/* monitor-enter v0 */
/* .line 1251 */
try { // :try_start_0
	 (( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
	 /* .line 1252 */
	 /* .local v1, "action":Ljava/lang/String; */
	 final String v2 = "android.net.wifi.WIFI_SLAVE_STATE_CHANGED"; // const-string v2, "android.net.wifi.WIFI_SLAVE_STATE_CHANGED"
	 v2 = 	 (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 1253 */
		 v2 = this.this$0;
		 /* const-string/jumbo v3, "wifi_state" */
		 /* const/16 v4, 0x12 */
		 v3 = 		 (( android.content.Intent ) p2 ).getIntExtra ( v3, v4 ); // invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
		 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .-$$Nest$mhandleDualWifiStatusChanged ( v2,v3 );
		 /* .line 1257 */
	 } // .end local v1 # "action":Ljava/lang/String;
} // :cond_0
/* monitor-exit v0 */
/* .line 1258 */
return;
/* .line 1257 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
