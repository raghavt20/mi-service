.class Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;
.super Ljava/lang/Object;
.source "NetworkSDKService.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    .line 243
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioChanged(I)V
    .locals 0
    .param p1, "mode"    # I

    .line 272
    return-void
.end method

.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 4
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;

    .line 246
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    const/4 v1, 0x0

    invoke-static {v0, v1, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$msetOrGetEverOpenedDualWifi(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 247
    return-void

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmDualWifiApps(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmDualWifiApps(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmStatusManager(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v1

    .line 250
    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->getForegroundUid()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmIsEverClosedByBackground(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 252
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/Handler;

    move-result-object v1

    .line 253
    const/16 v2, 0x6d

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 252
    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmDualWifiApps(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmDualWifiApps(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmStatusManager(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v1

    .line 260
    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->getForegroundUid()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 261
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x6b

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 262
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x6c

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 264
    :cond_2
    return-void
.end method

.method public onUidGone(IZ)V
    .locals 4
    .param p1, "goneUid"    # I
    .param p2, "disabled"    # Z

    .line 268
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmDualWifiApps(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmDualWifiApps(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/Set;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x69

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 270
    :cond_0
    return-void
.end method
