.class Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$5;
.super Ljava/lang/Object;
.source "NetworkSDKService.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    .line 351
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$5;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNetwrokInterfaceChange(Ljava/lang/String;IZZZZLjava/lang/String;)V
    .locals 5
    .param p1, "ifacename"    # Ljava/lang/String;
    .param p2, "ifacenum"    # I
    .param p3, "wifiready"    # Z
    .param p4, "slavewifiready"    # Z
    .param p5, "dataready"    # Z
    .param p6, "slaveDataReady"    # Z
    .param p7, "status"    # Ljava/lang/String;

    .line 355
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 356
    .local v0, "interfaceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 357
    .local v1, "interfaces":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    .line 358
    aget-object v3, v1, v2

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 357
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 361
    .end local v2    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$5;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fputmAvailableIfaces(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Ljava/util/List;)V

    .line 363
    const-string v2, "ON_AVAILABLE"

    invoke-virtual {p7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, -0x1

    if-eqz v2, :cond_1

    .line 364
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$5;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmAvailableIfaces(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->ifaceAddedCallBackNotice(Ljava/util/List;)V

    .line 365
    const-string/jumbo v2, "wlan0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v3, :cond_1

    .line 366
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$5;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$mbroadcastPrimaryConnected(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V

    .line 368
    :cond_1
    const-string v2, "ON_LOST"

    invoke-virtual {p7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 369
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$5;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmAvailableIfaces(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->ifaceRemovedCallBackNotice(Ljava/util/List;)V

    .line 370
    :cond_2
    if-eqz p1, :cond_3

    .line 371
    const-string/jumbo v2, "wlan"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v3, :cond_3

    .line 372
    const-string/jumbo v2, "wlan1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v3, :cond_3

    .line 373
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$5;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiConnected(Z)V

    .line 377
    :cond_3
    return-void
.end method
