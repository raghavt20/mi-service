public class com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService {
	 /* .source "NetworkSDKService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$MiNetServiceDeathRecipient;, */
	 /* Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;, */
	 /* Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_MSIM_VOICE_CAPABILITY_CHANGED;
private static final java.lang.String ACTION_VIDEO_APPS_POLICY_NOTIFY;
public static final Integer ADD_WHITELIST_DUAL_WIFI_TURNED_ON;
private static final Long APP_WIFI_SELECTION_MONITOR_MILLIS;
public static final Integer BASE;
private static final Integer CALLBACK_NETWORK_PRIORITY_CHANGED;
private static final Integer CALLBACK_ON_DSDA_STATE_CHANGED;
private static final Integer CALLBACK_ON_NETWORK_ADDED;
private static final Integer CALLBACK_ON_NETWORK_REMOVEDE;
private static final Integer CALLBACK_ON_SCAN_SUCCESS;
private static final Integer CALLBACK_ON_SET_SLAVE_WIFI_RES;
private static final Integer CALLBACK_ON_SLAVE_CONNECTED;
private static final Integer CALLBACK_ON_SLAVE_DISCONNECTED;
private static final Integer CALLBACK_ON_SLAVE_WIFI_ENABLE;
private static final Integer CALLBACK_ON_SLAVE_WIFI_ENABLE_V1;
private static final Integer CALLBACK_SET_SCREEN_STATUS;
private static final Integer CALLBACK_START_WIFI_LINKSTATS;
private static final Integer CALLBACK_VIDEO_POLICY_CHANGED;
private static final java.lang.String CLOUD_QEE_ENABLE;
private static final Long DUAL_WIFI_BACKGROUND_MONITOR_MILLIS;
public static final Integer DUAL_WIFI_TURNED_OFF_SCREEN_OFF;
public static final Integer DUAL_WIFI_TURNED_ON_SCREEN_ON;
private static final Integer EVENT_BACKGROUND_MONITOR;
private static final Integer EVENT_BACKGROUND_MONITOR_RESTART;
private static final Integer EVENT_BACKGROUND_MONITOR_START;
private static final Integer EVENT_DSDA_STATE_CHANGED;
private static final Integer EVENT_GET_HAL;
private static final Integer EVENT_IS_EVER_OPENED_DUAL_WIFI;
private static final Integer EVENT_RELEASE_DUAL_WIFI;
private static final Integer EVENT_SLAVE_WIFI_CONNECT;
private static final Integer EVENT_SLAVE_WIFI_DISCONNECT;
private static final Integer EVENT_SLAVE_WIFI_ENABLE;
private static final Integer GET_SERVICE_DELAY_MILLIS;
public static final Integer HANDLE_ADD_TO_WHITELIST;
public static final Integer HANDLE_REMOVE_FROM_WHITELIST;
public static final Integer IS_DUAL_WIFI_ENABLED;
public static final java.lang.String IS_OPENED_DUAL_WIFI;
private static final java.lang.String KEY_DURATION;
private static final java.lang.String KEY_LENGTH;
private static final java.lang.String KEY_TYPE;
private static final Integer LINK_POLL_SEND_DEVIATION;
private static final Integer MAX_COUNT;
private static final Integer MAX_DUAL_WIFI_BACKGROUND_COUNT;
private static final java.lang.String MIBRIDGE_WHITELIST;
public static final Integer MINETD_CMD_SETSOCKPRIO;
public static final Integer MINETD_CMD_SETTCPCONGESTION;
public static final Integer MINETD_CMD_SET_TRAFFIC_INTERFACE;
private static final Integer MSG_START_APP_WIFI_SELECTION_MONITOR;
private static final java.lang.String NETWORK_NETWORKBOOST_WHITELIST;
private static final Integer QCOM_STATE_DSDA;
private static final java.lang.String QEE_PROP;
public static final Integer QUERY_IS_IN_WHITELIST;
public static final Integer REMOVE_WHITELIST_DUAL_WIFI_TURNED_ON;
public static final Integer SDK_TURNS_OFF_DUAL_WIFI;
public static final Integer SDK_TURNS_OFF_DUAL_WIFI_BY_BG;
public static final Integer SDK_TURNS_ON_DUAL_WIFI;
public static final Integer SDK_TURNS_ON_DUAL_WIFI_BY_BG;
private static final Long SLAVE_WIFI_SCREEN_OFF_LONG;
private static final java.lang.String TAG;
private static final Integer TYPE_PRELOAD;
private static final Integer TYPE_RESOLUTION_ADJUST;
public static final Integer USER_TURNS_OFF_DUAL_WIFI;
public static final Integer USER_TURNS_ON_DUAL_WIFI;
private static final java.lang.String WIFI_INTERFACE_1;
private static final java.lang.Object mCellularListLock;
private static final java.lang.Object mListLock;
private static vendor.xiaomi.hidl.minet.V1_0.IMiNetService mMiNetService;
public static volatile com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService sInstance;
/* # instance fields */
private Integer DECIMAL_CONVERSION;
private Boolean isConnectSlaveAp;
private java.util.Map mAppCallBackMapToUid;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Landroid/os/IBinder;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mAppStatsPollInterval;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Landroid/os/IBinder;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mAppStatsPollMillis;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Landroid/os/IBinder;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mAppUidToPackName;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mAvailableIfaces;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mBssidToNetworkId;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.os.Handler mCallbackHandler;
private android.os.RemoteCallbackList mCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/os/RemoteCallbackList<", */
/* "Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Set mCellularNetworkSDKPN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Set mCellularNetworkSDKUid;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private android.os.IHwBinder$DeathRecipient mDeathRecipient;
private Integer mDownloadsUid;
private Boolean mDsdaCapability;
private com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService mDualCelluarDataService;
private java.util.Set mDualWifiApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mDualWifiBackgroundCount;
private Integer mDualWifiOriginStatus;
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
private com.xiaomi.NetworkBoost.StatusManager$IAppStatusListener mIAppStatusListenr;
private com.xiaomi.NetworkBoost.StatusManager$IScreenStatusListener mIScreenStatusListener;
private Boolean mIsCloseRoaming;
private Boolean mIsDualWifiSDKChanged;
private Boolean mIsDualWifiScreenOffDisabled;
private Boolean mIsEverClosedByBackground;
private Boolean mIsOpenWifiLinkPoll;
private Boolean mIsScreenON;
private Boolean mIsStartMonitorByBackground;
private java.lang.Object mLock;
private Integer mMarketUid;
private com.xiaomi.NetworkBoost.NetLinkLayerQoE mMasterNetLayerQoE;
private android.net.wifi.MiuiWifiManager mMiuiWifiManager;
private android.net.wifi.WlanLinkLayerQoE mMiuiWlanLayerQoE;
private android.os.RemoteCallbackList mNetSelectCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/os/RemoteCallbackList<", */
/* "Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.xiaomi.NetworkBoost.StatusManager$INetworkPriorityListener mNetworkPriorityListener;
private java.util.Set mNetworkSDKPN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Set mNetworkSDKUid;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mOffScreenCount;
private java.util.HashMap mPermission;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.database.ContentObserver mQEEObserver;
private final android.net.wifi.WifiScanner$ScanListener mScanListener;
private android.net.wifi.WifiScanner$ScanSettings mScanSettings;
private android.net.wifi.WifiScanner mScanner;
private Long mScreenOffSystemTime;
private com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper mSimCardHelper;
private com.xiaomi.NetworkBoost.slaservice.SLAService mSlaService;
private com.xiaomi.NetworkBoost.NetLinkLayerQoE mSlaveNetLayerQoE;
private android.net.wifi.SlaveWifiManager mSlaveWifiManager;
private miui.securitycenter.net.MiuiNetworkSessionStats mStatsSession;
private com.xiaomi.NetworkBoost.StatusManager mStatusManager;
private mTxAndRxStatistics;
private Long mWifiLinkStatsPollMillis;
private android.net.wifi.WifiManager mWifiManager;
private java.util.Set mWifiSelectionApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.os.RemoteCallbackList mWlanQoECallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/os/RemoteCallbackList<", */
/* "Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.xiaomi.NetworkBoost.StatusManager$INetworkInterfaceListener networkInterfaceListener;
/* # direct methods */
static java.util.Map -$$Nest$fgetmAppUidToPackName ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAppUidToPackName;
} // .end method
static java.util.List -$$Nest$fgetmAvailableIfaces ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAvailableIfaces;
} // .end method
static android.os.Handler -$$Nest$fgetmCallbackHandler ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCallbackHandler;
} // .end method
static android.os.RemoteCallbackList -$$Nest$fgetmCallbacks ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCallbacks;
} // .end method
static java.util.Set -$$Nest$fgetmCellularNetworkSDKPN ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCellularNetworkSDKPN;
} // .end method
static java.util.Set -$$Nest$fgetmCellularNetworkSDKUid ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCellularNetworkSDKUid;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static android.os.IHwBinder$DeathRecipient -$$Nest$fgetmDeathRecipient ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDeathRecipient;
} // .end method
static Integer -$$Nest$fgetmDownloadsUid ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDownloadsUid:I */
} // .end method
static Boolean -$$Nest$fgetmDsdaCapability ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDsdaCapability:Z */
} // .end method
static java.util.Set -$$Nest$fgetmDualWifiApps ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDualWifiApps;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmIsEverClosedByBackground ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z */
} // .end method
static Boolean -$$Nest$fgetmIsStartMonitorByBackground ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsStartMonitorByBackground:Z */
} // .end method
static java.lang.Object -$$Nest$fgetmLock ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLock;
} // .end method
static Integer -$$Nest$fgetmMarketUid ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMarketUid:I */
} // .end method
static java.util.Set -$$Nest$fgetmNetworkSDKPN ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNetworkSDKPN;
} // .end method
static java.util.Set -$$Nest$fgetmNetworkSDKUid ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNetworkSDKUid;
} // .end method
static android.database.ContentObserver -$$Nest$fgetmQEEObserver ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mQEEObserver;
} // .end method
static android.net.wifi.SlaveWifiManager -$$Nest$fgetmSlaveWifiManager ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSlaveWifiManager;
} // .end method
static com.xiaomi.NetworkBoost.StatusManager -$$Nest$fgetmStatusManager ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mStatusManager;
} // .end method
static void -$$Nest$fputmAvailableIfaces ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, java.util.List p1 ) { //bridge//synthethic
/* .locals 0 */
this.mAvailableIfaces = p1;
return;
} // .end method
static void -$$Nest$fputmDownloadsUid ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDownloadsUid:I */
return;
} // .end method
static void -$$Nest$fputmDsdaCapability ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDsdaCapability:Z */
return;
} // .end method
static void -$$Nest$fputmDualWifiBackgroundCount ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiBackgroundCount:I */
return;
} // .end method
static void -$$Nest$fputmIsStartMonitorByBackground ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsStartMonitorByBackground:Z */
return;
} // .end method
static void -$$Nest$fputmMarketUid ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMarketUid:I */
return;
} // .end method
static void -$$Nest$mappWifiSelectionMonitor ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->appWifiSelectionMonitor()V */
return;
} // .end method
static void -$$Nest$mbroadcastPrimaryConnected ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->broadcastPrimaryConnected()V */
return;
} // .end method
static void -$$Nest$mdsdaStateChangedCallBackSend ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, android.os.Message p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->dsdaStateChangedCallBackSend(Landroid/os/Message;)V */
return;
} // .end method
static void -$$Nest$mdualWifiBackgroundMonitor ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->dualWifiBackgroundMonitor()V */
return;
} // .end method
static void -$$Nest$mdualWifiBackgroundMonitorRestart ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->dualWifiBackgroundMonitorRestart()V */
return;
} // .end method
static Boolean -$$Nest$mgetCurrentDSDAState ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->getCurrentDSDAState()Z */
} // .end method
static void -$$Nest$mhandleDualWifiStatusChanged ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->handleDualWifiStatusChanged(I)V */
return;
} // .end method
static void -$$Nest$mhandleMultiAppsDualWifi ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, Boolean p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->handleMultiAppsDualWifi(ZI)V */
return;
} // .end method
static void -$$Nest$mifaceAddCallBackSend ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, android.os.Message p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->ifaceAddCallBackSend(Landroid/os/Message;)V */
return;
} // .end method
static void -$$Nest$mifaceRemovedCallBackSend ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, android.os.Message p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->ifaceRemovedCallBackSend(Landroid/os/Message;)V */
return;
} // .end method
static void -$$Nest$mmediaPlayerPolicyCallBackSend ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, android.os.Message p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mediaPlayerPolicyCallBackSend(Landroid/os/Message;)V */
return;
} // .end method
static void -$$Nest$mnetworkPriorityCallBackSend ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, android.os.Message p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkPriorityCallBackSend(Landroid/os/Message;)V */
return;
} // .end method
static Boolean -$$Nest$msetOrGetEverOpenedDualWifi ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, Boolean p1, Boolean p2 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setOrGetEverOpenedDualWifi(ZZ)Z */
} // .end method
static void -$$Nest$msetScreenStatus ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setScreenStatus(Z)V */
return;
} // .end method
static void -$$Nest$mupDateCloudWhiteList ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->upDateCloudWhiteList()V */
return;
} // .end method
static void -$$Nest$mupdataCellularSdkList ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->updataCellularSdkList()V */
return;
} // .end method
static void -$$Nest$mwifiLinkLayerStatsPoll ( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->wifiLinkLayerStatsPoll()V */
return;
} // .end method
static java.lang.Object -$$Nest$sfgetmCellularListLock ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mCellularListLock;
} // .end method
static java.lang.Object -$$Nest$sfgetmListLock ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mListLock;
} // .end method
static vendor.xiaomi.hidl.minet.V1_0.IMiNetService -$$Nest$sfgetmMiNetService ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mMiNetService;
} // .end method
static void -$$Nest$sfputmMiNetService ( vendor.xiaomi.hidl.minet.V1_0.IMiNetService p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ( ) {
/* .locals 1 */
/* .line 185 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
/* .line 186 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ( ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 396 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 90 */
/* const-wide/16 v0, -0x1 */
/* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J */
/* .line 151 */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
this.mWifiSelectionApps = v2;
/* .line 152 */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
this.mAppStatsPollMillis = v2;
/* .line 153 */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
this.mAppStatsPollInterval = v2;
/* .line 154 */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
this.mAppCallBackMapToUid = v2;
/* .line 157 */
int v2 = 0; // const/4 v2, 0x0
this.mContext = v2;
/* .line 170 */
this.mMiuiWlanLayerQoE = v2;
/* .line 171 */
this.mMasterNetLayerQoE = v2;
/* .line 172 */
this.mSlaveNetLayerQoE = v2;
/* .line 173 */
int v3 = 0; // const/4 v3, 0x0
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsScreenON:Z */
/* .line 174 */
int v4 = 3; // const/4 v4, 0x3
/* new-array v4, v4, [J */
this.mTxAndRxStatistics = v4;
/* .line 175 */
/* const v4, 0xf4240 */
/* iput v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->DECIMAL_CONVERSION:I */
/* .line 200 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDsdaCapability:Z */
/* .line 205 */
/* new-instance v4, Ljava/lang/Object; */
/* invoke-direct {v4}, Ljava/lang/Object;-><init>()V */
this.mLock = v4;
/* .line 206 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsOpenWifiLinkPoll:Z */
/* .line 207 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z */
/* .line 214 */
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
this.mAvailableIfaces = v4;
/* .line 217 */
/* new-instance v4, Landroid/net/wifi/WifiScanner$ScanSettings; */
/* invoke-direct {v4}, Landroid/net/wifi/WifiScanner$ScanSettings;-><init>()V */
this.mScanSettings = v4;
/* .line 220 */
/* iput v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mOffScreenCount:I */
/* .line 224 */
int v4 = -1; // const/4 v4, -0x1
/* iput v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMarketUid:I */
/* .line 225 */
/* iput v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDownloadsUid:I */
/* .line 228 */
/* iput v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I */
/* .line 229 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z */
/* .line 230 */
this.mDualWifiApps = v2;
/* .line 231 */
/* new-instance v4, Ljava/util/HashMap; */
/* invoke-direct {v4}, Ljava/util/HashMap;-><init>()V */
this.mAppUidToPackName = v4;
/* .line 232 */
/* iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScreenOffSystemTime:J */
/* .line 233 */
/* iput v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiBackgroundCount:I */
/* .line 234 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z */
/* .line 235 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsStartMonitorByBackground:Z */
/* .line 241 */
this.mQEEObserver = v2;
/* .line 243 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$1;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V */
this.mIAppStatusListenr = v0;
/* .line 275 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$2; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$2;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V */
this.mIScreenStatusListener = v0;
/* .line 290 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$3; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$3;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V */
this.mNetworkPriorityListener = v0;
/* .line 299 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$4; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$4;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V */
this.mScanListener = v0;
/* .line 351 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$5; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$5;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V */
this.networkInterfaceListener = v0;
/* .line 496 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$MiNetServiceDeathRecipient; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$MiNetServiceDeathRecipient;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V */
this.mDeathRecipient = v0;
/* .line 1264 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiScreenOffDisabled:Z */
/* .line 1827 */
this.mBssidToNetworkId = v2;
/* .line 397 */
(( android.content.Context ) p1 ).getApplicationContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
this.mContext = v0;
/* .line 398 */
return;
} // .end method
private void appWifiSelectionMonitor ( ) {
/* .locals 4 */
/* .line 1652 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->checkAppForegroundStatus()Z */
/* if-nez v0, :cond_0 */
/* .line 1653 */
return;
/* .line 1656 */
} // :cond_0
v0 = this.mCallbackHandler;
/* .line 1657 */
/* const/16 v1, 0x3f2 */
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 1656 */
/* const-wide/16 v2, 0x2710 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1660 */
return;
} // .end method
private android.net.wifi.WlanLinkLayerQoE availableIfaceCheckAndQoEPull ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "ifaceName" # Ljava/lang/String; */
/* .line 1541 */
v0 = this.mStatusManager;
v0 = (( com.xiaomi.NetworkBoost.StatusManager ) v0 ).getAvailableIfaces ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getAvailableIfaces()Ljava/util/List;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1542 */
/* .line 1543 */
} // :cond_0
v0 = this.mMiuiWifiManager;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1544 */
(( android.net.wifi.MiuiWifiManager ) v0 ).getQoEByAvailableIfaceName ( p1 ); // invoke-virtual {v0, p1}, Landroid/net/wifi/MiuiWifiManager;->getQoEByAvailableIfaceName(Ljava/lang/String;)Landroid/net/wifi/WlanLinkLayerQoE;
/* .line 1545 */
/* .local v0, "mWlanLinkLayerQoE":Landroid/net/wifi/WlanLinkLayerQoE; */
/* if-nez v0, :cond_1 */
/* .line 1546 */
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
final String v3 = "mMiuiWlanLayerQoE == null"; // const-string v3, "mMiuiWlanLayerQoE == null"
android.util.Log .e ( v2,v3 );
/* .line 1547 */
/* .line 1549 */
} // :cond_1
/* .line 1551 */
} // .end local v0 # "mWlanLinkLayerQoE":Landroid/net/wifi/WlanLinkLayerQoE;
} // :cond_2
} // .end method
private void broadcastPrimaryConnected ( ) {
/* .locals 6 */
/* .line 1957 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1958 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z */
/* if-nez v1, :cond_0 */
/* .line 1959 */
/* monitor-exit v0 */
return;
/* .line 1960 */
} // :cond_0
v1 = this.mNetSelectCallbacks;
v1 = (( android.os.RemoteCallbackList ) v1 ).beginBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1962 */
/* .local v1, "N":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_1 */
/* .line 1964 */
try { // :try_start_1
v3 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
int v4 = 2; // const/4 v4, 0x2
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1967 */
/* .line 1973 */
} // .end local v2 # "i":I
/* :catchall_0 */
/* move-exception v2 */
/* .line 1969 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1965 */
/* .restart local v2 # "i":I */
/* :catch_1 */
/* move-exception v3 */
/* .line 1966 */
/* .local v3, "e":Landroid/os/RemoteException; */
try { // :try_start_2
final String v4 = "NetworkSDKService"; // const-string v4, "NetworkSDKService"
final String v5 = "RemoteException at broadcastPrimaryConnected()"; // const-string v5, "RemoteException at broadcastPrimaryConnected()"
android.util.Log .e ( v4,v5 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1962 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1970 */
/* .local v2, "e":Ljava/lang/Exception; */
} // :goto_2
try { // :try_start_3
final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 1971 */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1973 */
} // .end local v2 # "e":Ljava/lang/Exception;
try { // :try_start_4
v2 = this.mNetSelectCallbacks;
} // :goto_3
v3 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v3 ).finishBroadcast ( ); // invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1974 */
/* nop */
} // .end local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
/* throw v2 */
/* .line 1973 */
/* .restart local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
} // :cond_1
v2 = this.mNetSelectCallbacks;
} // :goto_4
(( android.os.RemoteCallbackList ) v2 ).finishBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1974 */
/* nop */
/* .line 1975 */
} // .end local v1 # "N":I
/* monitor-exit v0 */
/* .line 1976 */
return;
/* .line 1975 */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v1 */
} // .end method
private void broadcastPrimaryConnectingFailed ( ) {
/* .locals 6 */
/* .line 1980 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1981 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z */
/* if-nez v1, :cond_0 */
/* .line 1982 */
/* monitor-exit v0 */
return;
/* .line 1983 */
} // :cond_0
v1 = this.mNetSelectCallbacks;
v1 = (( android.os.RemoteCallbackList ) v1 ).beginBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1985 */
/* .local v1, "N":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_1 */
/* .line 1987 */
try { // :try_start_1
v3 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
int v4 = 3; // const/4 v4, 0x3
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1990 */
/* .line 1996 */
} // .end local v2 # "i":I
/* :catchall_0 */
/* move-exception v2 */
/* .line 1992 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1988 */
/* .restart local v2 # "i":I */
/* :catch_1 */
/* move-exception v3 */
/* .line 1989 */
/* .local v3, "e":Landroid/os/RemoteException; */
try { // :try_start_2
final String v4 = "NetworkSDKService"; // const-string v4, "NetworkSDKService"
final String v5 = "RemoteException at broadcastPrimaryConnectingFailed()"; // const-string v5, "RemoteException at broadcastPrimaryConnectingFailed()"
android.util.Log .e ( v4,v5 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1985 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1993 */
/* .local v2, "e":Ljava/lang/Exception; */
} // :goto_2
try { // :try_start_3
final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 1994 */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1996 */
} // .end local v2 # "e":Ljava/lang/Exception;
try { // :try_start_4
v2 = this.mNetSelectCallbacks;
} // :goto_3
v3 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v3 ).finishBroadcast ( ); // invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1997 */
/* nop */
} // .end local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
/* throw v2 */
/* .line 1996 */
/* .restart local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
} // :cond_1
v2 = this.mNetSelectCallbacks;
} // :goto_4
(( android.os.RemoteCallbackList ) v2 ).finishBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1997 */
/* nop */
/* .line 1998 */
} // .end local v1 # "N":I
/* monitor-exit v0 */
/* .line 1999 */
return;
/* .line 1998 */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v1 */
} // .end method
private java.util.Map buildAppMobileDataUsage ( Integer p0, Long p1, Long p2 ) {
/* .locals 23 */
/* .param p1, "calluid" # I */
/* .param p2, "startTime" # J */
/* .param p4, "endTime" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(IJJ)", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 913 */
/* move-object/from16 v1, p0 */
v0 = this.mSimCardHelper;
int v2 = 0; // const/4 v2, 0x0
final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
/* if-nez v0, :cond_0 */
/* .line 914 */
final String v0 = "mSimCardHelper null"; // const-string v0, "mSimCardHelper null"
android.util.Log .e ( v3,v0 );
/* .line 915 */
/* .line 918 */
} // :cond_0
v4 = (( com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper ) v0 ).getCurrentMobileSlotNum ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->getCurrentMobileSlotNum()I
/* .line 919 */
/* .local v4, "slotNum":I */
v0 = this.mSimCardHelper;
(( com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper ) v0 ).getSimImsi ( v4 ); // invoke-virtual {v0, v4}, Lcom/xiaomi/NetworkBoost/NetworkSDK/telephony/NetworkBoostSimCardHelper;->getSimImsi(I)Ljava/lang/String;
/* .line 921 */
/* .local v11, "Imsi":Ljava/lang/String; */
v5 = this.mStatsSession;
/* move-object v6, v11 */
/* move-wide/from16 v7, p2 */
/* move-wide/from16 v9, p4 */
/* invoke-virtual/range {v5 ..v10}, Lmiui/securitycenter/net/MiuiNetworkSessionStats;->getMobileSummaryForAllUid(Ljava/lang/String;JJ)Landroid/util/SparseArray; */
/* .line 923 */
/* .local v5, "networkStats":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;>;" */
/* if-nez v5, :cond_1 */
/* .line 924 */
final String v0 = "buildAppMobileDataUsage networkStats null"; // const-string v0, "buildAppMobileDataUsage networkStats null"
android.util.Log .e ( v3,v0 );
/* .line 925 */
/* .line 928 */
} // :cond_1
v6 = (( android.util.SparseArray ) v5 ).size ( ); // invoke-virtual {v5}, Landroid/util/SparseArray;->size()I
/* .line 929 */
/* .local v6, "statsSize":I */
int v0 = 0; // const/4 v0, 0x0
/* .line 932 */
/* .local v0, "appusage":Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage; */
int v7 = 0; // const/4 v7, 0x0
/* move/from16 v22, v7 */
/* move-object v7, v0 */
/* move/from16 v0, v22 */
/* .local v0, "i":I */
/* .local v7, "appusage":Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage; */
} // :goto_0
/* if-ge v0, v6, :cond_3 */
/* .line 933 */
try { // :try_start_0
v8 = (( android.util.SparseArray ) v5 ).keyAt ( v0 ); // invoke-virtual {v5, v0}, Landroid/util/SparseArray;->keyAt(I)I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 935 */
/* .local v8, "uid":I */
/* move/from16 v9, p1 */
/* if-ne v9, v8, :cond_2 */
/* .line 936 */
try { // :try_start_1
(( android.util.SparseArray ) v5 ).get ( v8 ); // invoke-virtual {v5, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v10, Ljava/util/Map; */
/* .line 938 */
/* .local v10, "entry":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
if ( v10 != null) { // if-eqz v10, :cond_2
/* .line 939 */
/* new-instance v21, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage; */
final String v12 = "rxBytes"; // const-string v12, "rxBytes"
/* .line 940 */
/* check-cast v12, Ljava/lang/Long; */
(( java.lang.Long ) v12 ).longValue ( ); // invoke-virtual {v12}, Ljava/lang/Long;->longValue()J
/* move-result-wide v13 */
/* const-string/jumbo v12, "txBytes" */
/* .line 941 */
/* check-cast v12, Ljava/lang/Long; */
(( java.lang.Long ) v12 ).longValue ( ); // invoke-virtual {v12}, Ljava/lang/Long;->longValue()J
/* move-result-wide v15 */
/* const-string/jumbo v12, "txForegroundBytes" */
/* .line 942 */
/* check-cast v12, Ljava/lang/Long; */
(( java.lang.Long ) v12 ).longValue ( ); // invoke-virtual {v12}, Ljava/lang/Long;->longValue()J
/* move-result-wide v17 */
final String v12 = "rxForegroundBytes"; // const-string v12, "rxForegroundBytes"
/* .line 943 */
/* check-cast v12, Ljava/lang/Long; */
(( java.lang.Long ) v12 ).longValue ( ); // invoke-virtual {v12}, Ljava/lang/Long;->longValue()J
/* move-result-wide v19 */
/* move-object/from16 v12, v21 */
/* invoke-direct/range {v12 ..v20}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;-><init>(JJJJ)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* move-object/from16 v7, v21 */
/* .line 948 */
} // .end local v0 # "i":I
} // .end local v8 # "uid":I
} // .end local v10 # "entry":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
/* :catch_0 */
/* move-exception v0 */
/* .line 932 */
/* .restart local v0 # "i":I */
} // :cond_2
} // :goto_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 948 */
} // .end local v0 # "i":I
/* :catch_1 */
/* move-exception v0 */
/* move/from16 v9, p1 */
/* .line 949 */
/* .local v0, "e":Ljava/lang/Exception; */
} // :goto_2
final String v8 = "buildAppMobileDataUsage error"; // const-string v8, "buildAppMobileDataUsage error"
android.util.Log .e ( v3,v8 );
/* .line 932 */
/* .local v0, "i":I */
} // :cond_3
/* move/from16 v9, p1 */
/* .line 950 */
} // .end local v0 # "i":I
/* nop */
/* .line 952 */
} // :goto_3
/* if-nez v7, :cond_4 */
} // :cond_4
int v0 = 1; // const/4 v0, 0x1
(( com.xiaomi.NetworkBoost.NetworkSDK.AppDataUsage ) v7 ).toMap ( v0 ); // invoke-virtual {v7, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->toMap(Z)Ljava/util/Map;
} // :goto_4
} // .end method
private java.util.Map buildAppTrafficStatistics ( Integer p0, Long p1, Long p2, Integer p3 ) {
/* .locals 9 */
/* .param p1, "type" # I */
/* .param p2, "startTime" # J */
/* .param p4, "endTime" # J */
/* .param p6, "uid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(IJJI)", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 881 */
int v0 = 0; // const/4 v0, 0x0
/* .line 882 */
/* .local v0, "buildAppTrafficStatistics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .line 883 */
/* .local v1, "mobileTrafficStatistics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .line 885 */
/* .local v2, "wifiTrafficStatistics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* packed-switch p1, :pswitch_data_0 */
/* .line 893 */
/* :pswitch_0 */
/* move-object v3, p0 */
/* move v4, p6 */
/* move-wide v5, p2 */
/* move-wide v7, p4 */
/* invoke-direct/range {v3 ..v8}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->buildAppMobileDataUsage(IJJ)Ljava/util/Map; */
/* .line 894 */
/* invoke-direct/range {v3 ..v8}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->buildAppWifiDataUsage(IJJ)Ljava/util/Map; */
/* .line 896 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 897 */
/* .line 898 */
/* move-object v0, v1 */
/* .line 899 */
} // :cond_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 900 */
/* move-object v0, v2 */
/* .line 902 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 904 */
/* .line 890 */
/* :pswitch_1 */
/* move-object v3, p0 */
/* move v4, p6 */
/* move-wide v5, p2 */
/* move-wide v7, p4 */
/* invoke-direct/range {v3 ..v8}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->buildAppWifiDataUsage(IJJ)Ljava/util/Map; */
/* .line 891 */
/* .line 887 */
/* :pswitch_2 */
/* move-object v3, p0 */
/* move v4, p6 */
/* move-wide v5, p2 */
/* move-wide v7, p4 */
/* invoke-direct/range {v3 ..v8}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->buildAppMobileDataUsage(IJJ)Ljava/util/Map; */
/* .line 888 */
/* nop */
/* .line 909 */
} // :goto_0
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private java.util.Map buildAppWifiDataUsage ( Integer p0, Long p1, Long p2 ) {
/* .locals 25 */
/* .param p1, "calluid" # I */
/* .param p2, "startTime" # J */
/* .param p4, "endTime" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(IJJ)", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 956 */
/* move-object/from16 v1, p0 */
v0 = this.mStatsSession;
/* move-wide/from16 v2, p2 */
/* move-wide/from16 v4, p4 */
(( miui.securitycenter.net.MiuiNetworkSessionStats ) v0 ).getWifiSummaryForAllUid ( v2, v3, v4, v5 ); // invoke-virtual {v0, v2, v3, v4, v5}, Lmiui/securitycenter/net/MiuiNetworkSessionStats;->getWifiSummaryForAllUid(JJ)Landroid/util/SparseArray;
/* .line 958 */
/* .local v6, "networkStats":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;>;" */
int v7 = 0; // const/4 v7, 0x0
final String v8 = "NetworkSDKService"; // const-string v8, "NetworkSDKService"
/* if-nez v6, :cond_0 */
/* .line 959 */
final String v0 = "buildAppWifiDataUsage networkStats null"; // const-string v0, "buildAppWifiDataUsage networkStats null"
android.util.Log .e ( v8,v0 );
/* .line 960 */
/* .line 963 */
} // :cond_0
v9 = (( android.util.SparseArray ) v6 ).size ( ); // invoke-virtual {v6}, Landroid/util/SparseArray;->size()I
/* .line 964 */
/* .local v9, "statsSize":I */
int v0 = 0; // const/4 v0, 0x0
/* .line 967 */
/* .local v0, "appusage":Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage; */
int v10 = 0; // const/4 v10, 0x0
/* move/from16 v24, v10 */
/* move-object v10, v0 */
/* move/from16 v0, v24 */
/* .local v0, "i":I */
/* .local v10, "appusage":Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage; */
} // :goto_0
/* if-ge v0, v9, :cond_2 */
/* .line 968 */
try { // :try_start_0
v11 = (( android.util.SparseArray ) v6 ).keyAt ( v0 ); // invoke-virtual {v6, v0}, Landroid/util/SparseArray;->keyAt(I)I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 970 */
/* .local v11, "uid":I */
/* move/from16 v12, p1 */
/* if-ne v12, v11, :cond_1 */
/* .line 971 */
try { // :try_start_1
(( android.util.SparseArray ) v6 ).get ( v11 ); // invoke-virtual {v6, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v13, Ljava/util/Map; */
/* .line 973 */
/* .local v13, "entry":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
if ( v13 != null) { // if-eqz v13, :cond_1
/* .line 974 */
/* new-instance v23, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage; */
final String v14 = "rxBytes"; // const-string v14, "rxBytes"
/* .line 975 */
/* check-cast v14, Ljava/lang/Long; */
(( java.lang.Long ) v14 ).longValue ( ); // invoke-virtual {v14}, Ljava/lang/Long;->longValue()J
/* move-result-wide v15 */
/* const-string/jumbo v14, "txBytes" */
/* .line 976 */
/* check-cast v14, Ljava/lang/Long; */
(( java.lang.Long ) v14 ).longValue ( ); // invoke-virtual {v14}, Ljava/lang/Long;->longValue()J
/* move-result-wide v17 */
/* const-string/jumbo v14, "txForegroundBytes" */
/* .line 977 */
/* check-cast v14, Ljava/lang/Long; */
(( java.lang.Long ) v14 ).longValue ( ); // invoke-virtual {v14}, Ljava/lang/Long;->longValue()J
/* move-result-wide v19 */
final String v14 = "rxForegroundBytes"; // const-string v14, "rxForegroundBytes"
/* .line 978 */
/* check-cast v14, Ljava/lang/Long; */
(( java.lang.Long ) v14 ).longValue ( ); // invoke-virtual {v14}, Ljava/lang/Long;->longValue()J
/* move-result-wide v21 */
/* move-object/from16 v14, v23 */
/* invoke-direct/range {v14 ..v22}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;-><init>(JJJJ)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* move-object/from16 v10, v23 */
/* .line 983 */
} // .end local v0 # "i":I
} // .end local v11 # "uid":I
} // .end local v13 # "entry":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
/* :catch_0 */
/* move-exception v0 */
/* .line 967 */
/* .restart local v0 # "i":I */
} // :cond_1
} // :goto_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 983 */
} // .end local v0 # "i":I
/* :catch_1 */
/* move-exception v0 */
/* move/from16 v12, p1 */
/* .line 984 */
/* .local v0, "e":Ljava/lang/Exception; */
} // :goto_2
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 985 */
final String v11 = "buildAppWifiDataUsage error"; // const-string v11, "buildAppWifiDataUsage error"
android.util.Log .e ( v8,v11 );
/* .line 967 */
/* .local v0, "i":I */
} // :cond_2
/* move/from16 v12, p1 */
/* .line 986 */
} // .end local v0 # "i":I
/* nop */
/* .line 988 */
} // :goto_3
/* if-nez v10, :cond_3 */
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
(( com.xiaomi.NetworkBoost.NetworkSDK.AppDataUsage ) v10 ).toMap ( v0 ); // invoke-virtual {v10, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/AppDataUsage;->toMap(Z)Ljava/util/Map;
} // :goto_4
} // .end method
private void calculateAllPackages ( com.xiaomi.NetworkBoost.NetLinkLayerQoE p0 ) {
/* .locals 8 */
/* .param p1, "mNetLayerQoE" # Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
/* .line 1570 */
v0 = this.mTxAndRxStatistics;
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getLostmpdu_be ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getLostmpdu_be()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getLostmpdu_bk ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getLostmpdu_bk()J
/* move-result-wide v3 */
/* add-long/2addr v1, v3 */
/* .line 1571 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getLostmpdu_vi ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getLostmpdu_vi()J
/* move-result-wide v3 */
/* add-long/2addr v1, v3 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getLostmpdu_vo ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getLostmpdu_vo()J
/* move-result-wide v3 */
/* add-long/2addr v1, v3 */
int v3 = 0; // const/4 v3, 0x0
/* aput-wide v1, v0, v3 */
/* .line 1573 */
v0 = this.mTxAndRxStatistics;
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getRetries_be ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRetries_be()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getRetries_bk ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRetries_bk()J
/* move-result-wide v4 */
/* add-long/2addr v1, v4 */
/* .line 1574 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getRetries_vi ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRetries_vi()J
/* move-result-wide v4 */
/* add-long/2addr v1, v4 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getRetries_vo ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRetries_vo()J
/* move-result-wide v4 */
/* add-long/2addr v1, v4 */
int v4 = 1; // const/4 v4, 0x1
/* aput-wide v1, v0, v4 */
/* .line 1576 */
v0 = this.mTxAndRxStatistics;
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getTxmpdu_be ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getTxmpdu_be()J
/* move-result-wide v1 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getTxmpdu_bk ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getTxmpdu_bk()J
/* move-result-wide v5 */
/* add-long/2addr v1, v5 */
/* .line 1577 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getTxmpdu_vi ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getTxmpdu_vi()J
/* move-result-wide v5 */
/* add-long/2addr v1, v5 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getTxmpdu_vo ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getTxmpdu_vo()J
/* move-result-wide v5 */
/* add-long/2addr v1, v5 */
/* .line 1578 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getRxmpdu_be ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRxmpdu_be()J
/* move-result-wide v5 */
/* add-long/2addr v1, v5 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getRxmpdu_bk ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRxmpdu_bk()J
/* move-result-wide v5 */
/* add-long/2addr v1, v5 */
/* .line 1579 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getRxmpdu_vi ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRxmpdu_vi()J
/* move-result-wide v5 */
/* add-long/2addr v1, v5 */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).getRxmpdu_vo ( ); // invoke-virtual {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->getRxmpdu_vo()J
/* move-result-wide v5 */
/* add-long/2addr v1, v5 */
v5 = this.mTxAndRxStatistics;
/* aget-wide v6, v5, v3 */
/* add-long/2addr v1, v6 */
/* aget-wide v3, v5, v4 */
/* add-long/2addr v1, v3 */
int v3 = 2; // const/4 v3, 0x2
/* aput-wide v1, v0, v3 */
/* .line 1581 */
return;
} // .end method
private void calculateLostAndRetryRatio ( com.xiaomi.NetworkBoost.NetLinkLayerQoE p0 ) {
/* .locals 8 */
/* .param p1, "mNetLayerQoE" # Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
/* .line 1555 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->calculateAllPackages(Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;)V */
/* .line 1557 */
v0 = this.mTxAndRxStatistics;
int v1 = 2; // const/4 v1, 0x2
/* aget-wide v2, v0, v1 */
/* const-wide/16 v4, 0x0 */
/* cmp-long v4, v2, v4 */
/* if-nez v4, :cond_0 */
/* .line 1558 */
return;
/* .line 1560 */
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
/* aget-wide v4, v0, v4 */
/* long-to-double v4, v4 */
/* long-to-double v2, v2 */
/* div-double/2addr v4, v2 */
/* .line 1561 */
/* .local v4, "curLostRatio":D */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->DECIMAL_CONVERSION:I */
/* int-to-double v2, v0 */
/* mul-double/2addr v2, v4 */
java.lang.Math .round ( v2,v3 );
/* move-result-wide v2 */
/* long-to-double v2, v2 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->DECIMAL_CONVERSION:I */
/* int-to-double v6, v0 */
/* div-double/2addr v2, v6 */
/* .line 1562 */
} // .end local v4 # "curLostRatio":D
/* .local v2, "curLostRatio":D */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setMpduLostRatio ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setMpduLostRatio(D)V
/* .line 1564 */
v0 = this.mTxAndRxStatistics;
int v4 = 1; // const/4 v4, 0x1
/* aget-wide v4, v0, v4 */
/* long-to-double v4, v4 */
/* aget-wide v0, v0, v1 */
/* long-to-double v0, v0 */
/* div-double/2addr v4, v0 */
/* .line 1565 */
/* .local v4, "curRetryRatio":D */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->DECIMAL_CONVERSION:I */
/* int-to-double v0, v0 */
/* mul-double/2addr v0, v4 */
java.lang.Math .round ( v0,v1 );
/* move-result-wide v0 */
/* long-to-double v0, v0 */
/* iget v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->DECIMAL_CONVERSION:I */
/* int-to-double v6, v6 */
/* div-double/2addr v0, v6 */
/* .line 1566 */
} // .end local v4 # "curRetryRatio":D
/* .local v0, "curRetryRatio":D */
(( com.xiaomi.NetworkBoost.NetLinkLayerQoE ) p1 ).setRetriesRatio ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetriesRatio(D)V
/* .line 1567 */
return;
} // .end method
private static android.net.wifi.WifiScanner$ChannelSpec channelsToSpec ( Integer[] p0 ) {
/* .locals 4 */
/* .param p0, "channelList" # [I */
/* .line 659 */
/* array-length v0, p0 */
/* new-array v0, v0, [Landroid/net/wifi/WifiScanner$ChannelSpec; */
/* .line 660 */
/* .local v0, "channelSpecs":[Landroid/net/wifi/WifiScanner$ChannelSpec; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, p0 */
/* if-ge v1, v2, :cond_0 */
/* .line 661 */
/* new-instance v2, Landroid/net/wifi/WifiScanner$ChannelSpec; */
/* aget v3, p0, v1 */
/* invoke-direct {v2, v3}, Landroid/net/wifi/WifiScanner$ChannelSpec;-><init>(I)V */
/* aput-object v2, v0, v1 */
/* .line 660 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 663 */
} // .end local v1 # "i":I
} // :cond_0
} // .end method
private Boolean checkAppDualWifiBackgroundStatus ( ) {
/* .locals 5 */
/* .line 1701 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "dual-wifi background monitor count = "; // const-string v2, "dual-wifi background monitor count = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiBackgroundCount:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 1702 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1703 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsScreenON:Z */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_3
v1 = this.mDualWifiApps;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1704 */
v1 = /* invoke-direct {p0, v2, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setOrGetEverOpenedDualWifi(ZZ)Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1705 */
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).isSlaveWifiEnabledAndOthersOpt ( v2 ); // invoke-virtual {p0, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSlaveWifiEnabledAndOthersOpt(I)I
/* if-nez v1, :cond_0 */
/* .line 1710 */
} // :cond_0
v1 = this.mDualWifiApps;
v3 = this.mStatusManager;
v3 = (( com.xiaomi.NetworkBoost.StatusManager ) v3 ).getForegroundUid ( ); // invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/StatusManager;->getForegroundUid()I
v1 = java.lang.Integer .valueOf ( v3 );
/* if-nez v1, :cond_2 */
/* .line 1711 */
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiBackgroundCount:I */
int v3 = 1; // const/4 v3, 0x1
/* add-int/2addr v1, v3 */
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiBackgroundCount:I */
/* .line 1718 */
/* const/16 v4, 0xa */
/* if-ne v1, v4, :cond_1 */
/* .line 1719 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->closeDualWifiTemporarilyByBackground()V */
/* .line 1720 */
/* monitor-exit v0 */
/* .line 1722 */
} // :cond_1
/* monitor-exit v0 */
/* .line 1724 */
/* .line 1713 */
} // :cond_2
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->resetMonitorStatus()V */
/* .line 1714 */
/* monitor-exit v0 */
/* .line 1706 */
} // :cond_3
} // :goto_0
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->resetMonitorStatus()V */
/* .line 1707 */
/* monitor-exit v0 */
/* .line 1722 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean checkAppForegroundStatus ( ) {
/* .locals 5 */
/* .line 1663 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mOffScreenCount = "; // const-string v2, "mOffScreenCount = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mOffScreenCount:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 1664 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1666 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 1667 */
/* monitor-exit v0 */
/* .line 1669 */
} // :cond_0
v1 = this.mWifiSelectionApps;
v3 = this.mStatusManager;
v3 = (( com.xiaomi.NetworkBoost.StatusManager ) v3 ).getForegroundUid ( ); // invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/StatusManager;->getForegroundUid()I
v1 = java.lang.Integer .valueOf ( v3 );
int v3 = 1; // const/4 v3, 0x1
/* if-nez v1, :cond_1 */
/* .line 1670 */
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mOffScreenCount:I */
/* add-int/2addr v1, v3 */
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mOffScreenCount:I */
/* .line 1672 */
} // :cond_1
/* iput v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mOffScreenCount:I */
/* .line 1675 */
} // :goto_0
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mOffScreenCount:I */
/* const/16 v4, 0x12 */
/* if-ne v1, v4, :cond_2 */
/* .line 1676 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->clearAppWifiSelectionMonitor()V */
/* .line 1677 */
/* monitor-exit v0 */
/* .line 1679 */
} // :cond_2
/* monitor-exit v0 */
/* .line 1681 */
/* .line 1679 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void clearAppWifiSelectionMonitor ( ) {
/* .locals 1 */
/* .line 1685 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->forceOpenFrameworkRoaming()V */
/* .line 1686 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mOffScreenCount:I */
/* .line 1687 */
return;
} // .end method
private void clearDualWifiStatus ( ) {
/* .locals 2 */
/* .line 1232 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "clearDualWifiStatus"; // const-string v1, "clearDualWifiStatus"
android.util.Log .d ( v0,v1 );
/* .line 1233 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1234 */
int v1 = -1; // const/4 v1, -0x1
try { // :try_start_0
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I */
/* .line 1235 */
int v1 = 0; // const/4 v1, 0x0
this.mDualWifiApps = v1;
/* .line 1236 */
/* monitor-exit v0 */
/* .line 1237 */
return;
/* .line 1236 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void closeAppTrafficSession ( ) {
/* .locals 1 */
/* .line 871 */
v0 = this.mStatsSession;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 872 */
(( miui.securitycenter.net.MiuiNetworkSessionStats ) v0 ).closeSession ( ); // invoke-virtual {v0}, Lmiui/securitycenter/net/MiuiNetworkSessionStats;->closeSession()V
/* .line 873 */
int v0 = 0; // const/4 v0, 0x0
this.mStatsSession = v0;
/* .line 875 */
} // :cond_0
return;
} // .end method
private void closeDualWifiTemporarilyByBackground ( ) {
/* .locals 4 */
/* .line 1728 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->resetMonitorStatus()V */
/* .line 1730 */
v0 = this.mSlaveWifiManager;
/* if-nez v0, :cond_0 */
/* .line 1731 */
return;
/* .line 1733 */
} // :cond_0
v0 = (( android.net.wifi.SlaveWifiManager ) v0 ).isBusySlaveWifi ( ); // invoke-virtual {v0}, Landroid/net/wifi/SlaveWifiManager;->isBusySlaveWifi()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1734 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "dual-wifi background monitor slave wifi is busy"; // const-string v1, "dual-wifi background monitor slave wifi is busy"
android.util.Log .d ( v0,v1 );
/* .line 1735 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1736 */
/* const/16 v1, 0x9 */
try { // :try_start_0
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiBackgroundCount:I */
/* .line 1737 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1738 */
v0 = this.mHandler;
/* .line 1739 */
/* const/16 v1, 0x6b */
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 1738 */
/* const-wide/16 v2, 0x7530 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1742 */
return;
/* .line 1737 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 1745 */
} // :cond_1
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1746 */
try { // :try_start_2
v1 = this.mSlaveWifiManager;
int v2 = 0; // const/4 v2, 0x0
v1 = (( android.net.wifi.SlaveWifiManager ) v1 ).setWifiSlaveEnabled ( v2 ); // invoke-virtual {v1, v2}, Landroid/net/wifi/SlaveWifiManager;->setWifiSlaveEnabled(Z)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1747 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z */
/* .line 1749 */
} // :cond_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 1750 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "dual-wifi background monitor close dual wifi"; // const-string v1, "dual-wifi background monitor close dual wifi"
android.util.Log .d ( v0,v1 );
/* .line 1751 */
return;
/* .line 1749 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_3
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v1 */
} // .end method
private Boolean connectionWithBssid ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "bssid" # Ljava/lang/String; */
/* .line 1925 */
v0 = this.mBssidToNetworkId;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1926 */
/* .line 1929 */
} // :cond_0
v2 = this.mMiuiWifiManager;
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1930 */
/* check-cast v0, Ljava/lang/String; */
v0 = (( android.net.wifi.MiuiWifiManager ) v2 ).netSDKConnectPrimaryWithBssid ( v0, p1 ); // invoke-virtual {v2, v0, p1}, Landroid/net/wifi/MiuiWifiManager;->netSDKConnectPrimaryWithBssid(Ljava/lang/String;Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1932 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1933 */
try { // :try_start_0
v1 = this.mNetSelectCallbacks;
v1 = (( android.os.RemoteCallbackList ) v1 ).beginBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1935 */
/* .local v1, "N":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
int v3 = 1; // const/4 v3, 0x1
/* if-ge v2, v1, :cond_1 */
/* .line 1937 */
try { // :try_start_1
v4 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v4 ).getBroadcastItem ( v2 ); // invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v4, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1940 */
/* .line 1946 */
} // .end local v2 # "i":I
/* :catchall_0 */
/* move-exception v2 */
/* .line 1942 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1938 */
/* .restart local v2 # "i":I */
/* :catch_1 */
/* move-exception v4 */
/* .line 1939 */
/* .local v4, "e":Landroid/os/RemoteException; */
try { // :try_start_2
final String v5 = "NetworkSDKService"; // const-string v5, "NetworkSDKService"
final String v6 = "RemoteException at connectionWithBssid()"; // const-string v6, "RemoteException at connectionWithBssid()"
android.util.Log .e ( v5,v6 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1935 */
} // .end local v4 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1943 */
/* .local v2, "e":Ljava/lang/Exception; */
} // :goto_2
try { // :try_start_3
final String v4 = "NetworkSDKService"; // const-string v4, "NetworkSDKService"
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v5 );
/* .line 1944 */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1946 */
} // .end local v2 # "e":Ljava/lang/Exception;
try { // :try_start_4
v2 = this.mNetSelectCallbacks;
} // :goto_3
v3 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v3 ).finishBroadcast ( ); // invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1947 */
/* nop */
} // .end local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
} // .end local p1 # "bssid":Ljava/lang/String;
/* throw v2 */
/* .line 1946 */
/* .restart local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* .restart local p1 # "bssid":Ljava/lang/String; */
} // :cond_1
v2 = this.mNetSelectCallbacks;
} // :goto_4
(( android.os.RemoteCallbackList ) v2 ).finishBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1947 */
/* nop */
/* .line 1948 */
} // .end local v1 # "N":I
/* monitor-exit v0 */
/* .line 1949 */
/* .line 1948 */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v1 */
/* .line 1951 */
} // :cond_2
} // .end method
public static void destroyInstance ( ) {
/* .locals 4 */
/* .line 381 */
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.sInstance;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 382 */
/* const-class v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* monitor-enter v0 */
/* .line 383 */
try { // :try_start_0
v1 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.sInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 385 */
try { // :try_start_1
v1 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.sInstance;
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).onDestroy ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onDestroy()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 389 */
/* .line 387 */
/* :catch_0 */
/* move-exception v1 */
/* .line 388 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
final String v3 = "destroyInstance onDestroy catch:"; // const-string v3, "destroyInstance onDestroy catch:"
android.util.Log .e ( v2,v3,v1 );
/* .line 390 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
/* .line 392 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 394 */
} // :cond_1
} // :goto_1
return;
} // .end method
private void dsdaStateChangedCallBackSend ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1448 */
v0 = this.obj;
/* check-cast v0, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 1449 */
/* .local v0, "res":Z */
v1 = this.mCallbacks;
v1 = (( android.os.RemoteCallbackList ) v1 ).beginBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 1451 */
/* .local v1, "N":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* .line 1453 */
try { // :try_start_0
v3 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1456 */
/* .line 1454 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1455 */
/* .local v3, "e":Landroid/os/RemoteException; */
final String v4 = "NetworkSDKService"; // const-string v4, "NetworkSDKService"
final String v5 = "RemoteException at dsdaStateChangedCallBackSend()"; // const-string v5, "RemoteException at dsdaStateChangedCallBackSend()"
android.util.Log .e ( v4,v5 );
/* .line 1451 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1458 */
} // .end local v2 # "i":I
} // :cond_0
v2 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v2 ).finishBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1459 */
return;
} // .end method
private void dualWifiBackgroundMonitor ( ) {
/* .locals 4 */
/* .line 1690 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->checkAppDualWifiBackgroundStatus()Z */
/* if-nez v0, :cond_0 */
/* .line 1691 */
return;
/* .line 1694 */
} // :cond_0
v0 = this.mHandler;
/* .line 1695 */
/* const/16 v1, 0x6b */
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 1694 */
/* const-wide/16 v2, 0x7530 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1698 */
return;
} // .end method
private void dualWifiBackgroundMonitorRestart ( ) {
/* .locals 3 */
/* .line 1761 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1762 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
v2 = /* invoke-direct {p0, v1, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setOrGetEverOpenedDualWifi(ZZ)Z */
/* if-nez v2, :cond_0 */
/* .line 1763 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z */
/* .line 1764 */
/* monitor-exit v0 */
return;
/* .line 1767 */
} // :cond_0
v2 = this.mDualWifiApps;
/* if-nez v2, :cond_1 */
/* .line 1768 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z */
/* .line 1769 */
/* monitor-exit v0 */
return;
/* .line 1772 */
} // :cond_1
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1773 */
v1 = this.mSlaveWifiManager;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1774 */
int v2 = 1; // const/4 v2, 0x1
(( android.net.wifi.SlaveWifiManager ) v1 ).setWifiSlaveEnabled ( v2 ); // invoke-virtual {v1, v2}, Landroid/net/wifi/SlaveWifiManager;->setWifiSlaveEnabled(Z)Z
/* .line 1776 */
} // :cond_2
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1778 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "dual-wifi background monitor restart dual wifi"; // const-string v1, "dual-wifi background monitor restart dual wifi"
android.util.Log .d ( v0,v1 );
/* .line 1779 */
return;
/* .line 1776 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void forceOpenFrameworkRoaming ( ) {
/* .locals 7 */
/* .line 2059 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 2060 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z */
/* if-nez v1, :cond_0 */
/* .line 2061 */
/* monitor-exit v0 */
return;
/* .line 2062 */
} // :cond_0
v1 = this.mNetSelectCallbacks;
v1 = (( android.os.RemoteCallbackList ) v1 ).beginBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 2064 */
/* .local v1, "N":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_1 */
/* .line 2067 */
try { // :try_start_1
v3 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
int v4 = 4; // const/4 v4, 0x4
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 2070 */
/* .line 2076 */
} // .end local v2 # "i":I
/* :catchall_0 */
/* move-exception v2 */
/* .line 2072 */
/* :catch_0 */
/* move-exception v2 */
/* .line 2068 */
/* .restart local v2 # "i":I */
/* :catch_1 */
/* move-exception v3 */
/* .line 2069 */
/* .local v3, "e":Landroid/os/RemoteException; */
try { // :try_start_2
final String v4 = "NetworkSDKService"; // const-string v4, "NetworkSDKService"
final String v5 = "RemoteException at forceOpenFrameworkRoaming()"; // const-string v5, "RemoteException at forceOpenFrameworkRoaming()"
android.util.Log .e ( v4,v5 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 2064 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 2073 */
/* .local v2, "e":Ljava/lang/Exception; */
} // :goto_2
try { // :try_start_3
final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 2074 */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 2076 */
} // .end local v2 # "e":Ljava/lang/Exception;
try { // :try_start_4
v2 = this.mNetSelectCallbacks;
} // :goto_3
v3 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v3 ).finishBroadcast ( ); // invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 2077 */
/* nop */
} // .end local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
/* throw v2 */
/* .line 2076 */
/* .restart local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
} // :cond_1
v2 = this.mNetSelectCallbacks;
} // :goto_4
(( android.os.RemoteCallbackList ) v2 ).finishBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 2077 */
/* nop */
/* .line 2079 */
v2 = this.mNetSelectCallbacks;
v2 = (( android.os.RemoteCallbackList ) v2 ).getRegisteredCallbackCount ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I
/* .line 2080 */
/* .local v2, "count":I */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 2081 */
/* .local v3, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;>;" */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_5
/* if-ge v4, v2, :cond_2 */
/* .line 2082 */
v5 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v5 ).getRegisteredCallbackItem ( v4 ); // invoke-virtual {v5, v4}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackItem(I)Landroid/os/IInterface;
/* check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
/* .line 2081 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 2084 */
} // .end local v4 # "i":I
} // :cond_2
v5 = } // :goto_6
if ( v5 != null) { // if-eqz v5, :cond_3
/* check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
/* .line 2085 */
/* .local v5, "cb":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
v6 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v6 ).unregister ( v5 ); // invoke-virtual {v6, v5}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 2086 */
/* nop */
} // .end local v5 # "cb":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;
/* .line 2088 */
} // :cond_3
v4 = this.mMiuiWifiManager;
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 2089 */
int v5 = -1; // const/4 v5, -0x1
int v6 = 1; // const/4 v6, 0x1
(( android.net.wifi.MiuiWifiManager ) v4 ).netSDKSetIsDisableRoam ( v6, v5 ); // invoke-virtual {v4, v6, v5}, Landroid/net/wifi/MiuiWifiManager;->netSDKSetIsDisableRoam(ZI)Z
/* .line 2091 */
} // :cond_4
int v4 = 0; // const/4 v4, 0x0
/* iput-boolean v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z */
/* .line 2092 */
} // .end local v1 # "N":I
} // .end local v2 # "count":I
} // .end local v3 # "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;>;"
/* monitor-exit v0 */
/* .line 2093 */
return;
/* .line 2092 */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v1 */
} // .end method
private Boolean getCurrentDSDAState ( ) {
/* .locals 3 */
/* .line 2646 */
final String v0 = "ril.multisim.voice_capability"; // const-string v0, "ril.multisim.voice_capability"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
int v2 = 3; // const/4 v2, 0x3
/* if-ne v2, v0, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
} // .end method
public static com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService getInstance ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 335 */
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.sInstance;
/* if-nez v0, :cond_1 */
/* .line 336 */
/* const-class v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* monitor-enter v0 */
/* .line 337 */
try { // :try_start_0
v1 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.sInstance;
/* if-nez v1, :cond_0 */
/* .line 338 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;-><init>(Landroid/content/Context;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 340 */
try { // :try_start_1
v1 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.sInstance;
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).onCreate ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onCreate()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 344 */
/* .line 342 */
/* :catch_0 */
/* move-exception v1 */
/* .line 343 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
final String v3 = "getInstance onCreate catch:"; // const-string v3, "getInstance onCreate catch:"
android.util.Log .e ( v2,v3,v1 );
/* .line 346 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 348 */
} // :cond_1
} // :goto_1
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.sInstance;
} // .end method
private void handleDualWifiStatusChanged ( Integer p0 ) {
/* .locals 8 */
/* .param p1, "state" # I */
/* .line 1266 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1267 */
try { // :try_start_0
final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "mIsDualWifiSDKChanged = "; // const-string v3, "mIsDualWifiSDKChanged = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " state = "; // const-string v3, " state = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " mDualWifiOriginStatus = "; // const-string v3, " mDualWifiOriginStatus = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 1270 */
/* iget-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z */
int v2 = 5; // const/4 v2, 0x5
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1271 */
/* const/16 v1, 0x11 */
/* if-ne p1, v1, :cond_0 */
/* .line 1272 */
/* invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V */
/* .line 1273 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z */
/* .line 1274 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z */
/* .line 1275 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsStartMonitorByBackground:Z */
/* .line 1277 */
} // :cond_0
/* const/16 v1, 0xf */
/* if-ne p1, v1, :cond_1 */
/* .line 1278 */
int v1 = 6; // const/4 v1, 0x6
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V */
/* .line 1279 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z */
/* .line 1280 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z */
/* .line 1281 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsStartMonitorByBackground:Z */
/* .line 1283 */
} // :cond_1
/* monitor-exit v0 */
return;
/* .line 1286 */
} // :cond_2
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z */
/* .line 1287 */
int v1 = 1; // const/4 v1, 0x1
/* packed-switch p1, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 1289 */
/* :pswitch_1 */
/* iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiScreenOffDisabled:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1290 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiScreenOffDisabled:Z */
/* .line 1291 */
int v1 = 3; // const/4 v1, 0x3
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V */
/* .line 1292 */
/* .line 1294 */
} // :cond_3
/* iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1295 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z */
/* .line 1296 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsStartMonitorByBackground:Z */
/* .line 1297 */
/* const/16 v1, 0x9 */
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V */
/* .line 1298 */
/* .line 1300 */
} // :cond_4
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiScreenOffDisabled:Z */
/* .line 1301 */
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V */
/* .line 1302 */
/* .line 1304 */
/* :pswitch_2 */
/* iget-boolean v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsScreenON:Z */
/* if-nez v4, :cond_5 */
/* .line 1305 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* iget-wide v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScreenOffSystemTime:J */
/* sub-long/2addr v4, v6 */
/* const-wide/16 v6, 0x7530 */
/* cmp-long v4, v4, v6 */
/* if-lez v4, :cond_5 */
/* .line 1306 */
int v2 = 4; // const/4 v2, 0x4
/* invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V */
/* .line 1307 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiScreenOffDisabled:Z */
/* .line 1308 */
/* .line 1311 */
} // :cond_5
/* iget-boolean v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsEverClosedByBackground:Z */
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 1312 */
/* const/16 v1, 0xa */
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1313 */
/* .line 1316 */
} // :cond_6
try { // :try_start_1
v4 = this.mSlaService;
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 1317 */
final String v5 = ""; // const-string v5, ""
final String v6 = ""; // const-string v6, ""
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v4 ).setDoubleWifiWhiteList ( v2, v5, v6, v3 ); // invoke-virtual {v4, v2, v5, v6, v3}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z
/* .line 1318 */
} // :cond_7
/* invoke-direct {p0, v1, v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setOrGetEverOpenedDualWifi(ZZ)Z */
/* .line 1319 */
v1 = this.mMiuiWifiManager;
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 1320 */
(( android.net.wifi.MiuiWifiManager ) v1 ).setSlaveCandidatesRssiThreshold ( v3 ); // invoke-virtual {v1, v3}, Landroid/net/wifi/MiuiWifiManager;->setSlaveCandidatesRssiThreshold(Z)Z
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1323 */
} // :cond_8
/* .line 1321 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1322 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1324 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->clearDualWifiStatus()V */
/* .line 1325 */
int v1 = 2; // const/4 v1, 0x2
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V */
/* .line 1326 */
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiScreenOffDisabled:Z */
/* .line 1327 */
/* nop */
/* .line 1331 */
} // :goto_1
/* monitor-exit v0 */
/* .line 1332 */
return;
/* .line 1331 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0xf */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private void handleMultiAppsDualWifi ( Boolean p0, Integer p1 ) {
/* .locals 11 */
/* .param p1, "enable" # Z */
/* .param p2, "uid" # I */
/* .line 1073 */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).isSupportDualWifi ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSupportDualWifi()Z
/* if-nez v0, :cond_0 */
/* .line 1074 */
return;
/* .line 1077 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsScreenON:Z */
/* if-nez v0, :cond_1 */
/* .line 1078 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "handleMultiAppsDualWifi mIsScreenON = "; // const-string v2, "handleMultiAppsDualWifi mIsScreenON = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsScreenON:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v1 );
/* .line 1079 */
return;
/* .line 1082 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).isSlaveWifiEnabledAndOthersOpt ( v0, p2 ); // invoke-virtual {p0, v0, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSlaveWifiEnabledAndOthersOpt(II)I
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_2 */
/* move v1, v2 */
} // :cond_2
/* move v1, v0 */
/* .line 1084 */
/* .local v1, "isDualWifiOpen":Z */
} // :goto_0
v3 = this.mSlaService;
/* if-nez v3, :cond_3 */
/* .line 1085 */
v3 = this.mContext;
com.xiaomi.NetworkBoost.slaservice.SLAService .getInstance ( v3 );
this.mSlaService = v3;
/* .line 1087 */
} // :cond_3
int v3 = -1; // const/4 v3, -0x1
/* .line 1088 */
/* .local v3, "originStatus":I */
v4 = this.mLock;
/* monitor-enter v4 */
/* .line 1089 */
try { // :try_start_0
v5 = this.mDualWifiApps;
/* if-nez v5, :cond_4 */
/* .line 1090 */
/* new-instance v5, Ljava/util/HashSet; */
/* invoke-direct {v5}, Ljava/util/HashSet;-><init>()V */
this.mDualWifiApps = v5;
/* .line 1092 */
} // :cond_4
final String v5 = "NetworkSDKService"; // const-string v5, "NetworkSDKService"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "pre mDualWifiOriginStatus = "; // const-string v7, "pre mDualWifiOriginStatus = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " mIsDualWifiSDKChanged = "; // const-string v7, " mIsDualWifiSDKChanged = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v7 = " mDualWifiApps.size = "; // const-string v7, " mDualWifiApps.size = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mDualWifiApps;
v7 = /* .line 1094 */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1092 */
android.util.Log .d ( v5,v6 );
/* .line 1096 */
/* iget v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I */
int v6 = -1; // const/4 v6, -0x1
/* if-ne v5, v6, :cond_6 */
v5 = v5 = this.mDualWifiApps;
/* if-nez v5, :cond_6 */
/* .line 1097 */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1098 */
/* iput v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I */
/* .line 1100 */
} // :cond_5
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I */
/* .line 1103 */
} // :cond_6
} // :goto_1
/* iget v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I */
/* move v3, v5 */
/* .line 1104 */
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_4 */
/* .line 1106 */
int v4 = 0; // const/4 v4, 0x0
/* .line 1107 */
/* .local v4, "packName":Ljava/lang/String; */
v5 = this.mAppUidToPackName;
if ( v5 != null) { // if-eqz v5, :cond_7
/* .line 1108 */
java.lang.Integer .valueOf ( p2 );
/* move-object v4, v5 */
/* check-cast v4, Ljava/lang/String; */
/* .line 1109 */
} // :cond_7
/* if-nez v4, :cond_8 */
/* .line 1110 */
return;
/* .line 1112 */
} // :cond_8
int v5 = 0; // const/4 v5, 0x0
/* .line 1113 */
/* .local v5, "isMarket":Z */
/* iget v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMarketUid:I */
/* if-eq v7, v6, :cond_9 */
/* if-ne v7, p2, :cond_9 */
/* .line 1114 */
int v5 = 1; // const/4 v5, 0x1
/* .line 1115 */
} // :cond_9
final String v6 = "NetworkSDKService"; // const-string v6, "NetworkSDKService"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "handleMultiAppsDualWifi packName = "; // const-string v8, "handleMultiAppsDualWifi packName = "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " isMarket = "; // const-string v8, " isMarket = "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v6,v7 );
/* .line 1118 */
java.lang.String .valueOf ( p2 );
/* .line 1119 */
/* .local v6, "uidStr":Ljava/lang/String; */
if ( v5 != null) { // if-eqz v5, :cond_a
/* .line 1120 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ",com.android.providers.downloads"; // const-string v8, ",com.android.providers.downloads"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1121 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ","; // const-string v8, ","
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDownloadsUid:I */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1124 */
} // :cond_a
int v7 = 2; // const/4 v7, 0x2
/* if-ne v3, v2, :cond_f */
/* .line 1125 */
if ( p1 != null) { // if-eqz p1, :cond_c
/* .line 1126 */
try { // :try_start_1
v7 = this.mLock;
/* monitor-enter v7 */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 1127 */
try { // :try_start_2
v8 = this.mDualWifiApps;
v8 = java.lang.Integer .valueOf ( p2 );
if ( v8 != null) { // if-eqz v8, :cond_b
/* .line 1128 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v2 = "handleMultiAppsDualWifi Duplicate Request"; // const-string v2, "handleMultiAppsDualWifi Duplicate Request"
android.util.Log .d ( v0,v2 );
/* .line 1129 */
/* monitor-exit v7 */
return;
/* .line 1132 */
} // :cond_b
v8 = this.mDualWifiApps;
java.lang.Integer .valueOf ( p2 );
/* .line 1134 */
v8 = this.mSlaService;
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v8 ).setDoubleWifiWhiteList ( v2, v4, v6, v0 ); // invoke-virtual {v8, v2, v4, v6, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z
/* .line 1136 */
v0 = this.mSlaService;
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v0 ).setDWUidToSlad ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDWUidToSlad()V
/* .line 1137 */
int v0 = 7; // const/4 v0, 0x7
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V */
/* .line 1138 */
/* monitor-exit v7 */
/* goto/16 :goto_7 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v7 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
} // .end local v1 # "isDualWifiOpen":Z
} // .end local v3 # "originStatus":I
} // .end local v4 # "packName":Ljava/lang/String;
} // .end local v5 # "isMarket":Z
} // .end local v6 # "uidStr":Ljava/lang/String;
} // .end local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
} // .end local p1 # "enable":Z
} // .end local p2 # "uid":I
try { // :try_start_3
/* throw v0 */
/* .line 1140 */
/* .restart local v1 # "isDualWifiOpen":Z */
/* .restart local v3 # "originStatus":I */
/* .restart local v4 # "packName":Ljava/lang/String; */
/* .restart local v5 # "isMarket":Z */
/* .restart local v6 # "uidStr":Ljava/lang/String; */
/* .restart local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* .restart local p1 # "enable":Z */
/* .restart local p2 # "uid":I */
} // :cond_c
v2 = this.mLock;
/* monitor-enter v2 */
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 1141 */
try { // :try_start_4
v8 = this.mDualWifiApps;
v8 = java.lang.Integer .valueOf ( p2 );
/* if-nez v8, :cond_d */
/* .line 1142 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v7 = "handleMultiAppsDualWifi No request found"; // const-string v7, "handleMultiAppsDualWifi No request found"
android.util.Log .d ( v0,v7 );
/* .line 1143 */
/* monitor-exit v2 */
return;
/* .line 1146 */
} // :cond_d
v8 = this.mDualWifiApps;
java.lang.Integer .valueOf ( p2 );
/* .line 1147 */
v8 = this.mSlaService;
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v8 ).setDoubleWifiWhiteList ( v7, v4, v6, v0 ); // invoke-virtual {v8, v7, v4, v6, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z
/* .line 1148 */
v0 = this.mSlaService;
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v0 ).setDWUidToSlad ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDWUidToSlad()V
/* .line 1150 */
v0 = v0 = this.mDualWifiApps;
/* if-nez v0, :cond_e */
/* .line 1151 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->clearDualWifiStatus()V */
/* .line 1153 */
} // :cond_e
/* const/16 v0, 0x8 */
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnableV1(I)V */
/* .line 1154 */
/* monitor-exit v2 */
/* goto/16 :goto_7 */
/* :catchall_1 */
/* move-exception v0 */
/* monitor-exit v2 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
} // .end local v1 # "isDualWifiOpen":Z
} // .end local v3 # "originStatus":I
} // .end local v4 # "packName":Ljava/lang/String;
} // .end local v5 # "isMarket":Z
} // .end local v6 # "uidStr":Ljava/lang/String;
} // .end local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
} // .end local p1 # "enable":Z
} // .end local p2 # "uid":I
try { // :try_start_5
/* throw v0 */
/* .line 1157 */
/* .restart local v1 # "isDualWifiOpen":Z */
/* .restart local v3 # "originStatus":I */
/* .restart local v4 # "packName":Ljava/lang/String; */
/* .restart local v5 # "isMarket":Z */
/* .restart local v6 # "uidStr":Ljava/lang/String; */
/* .restart local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* .restart local p1 # "enable":Z */
/* .restart local p2 # "uid":I */
} // :cond_f
if ( p1 != null) { // if-eqz p1, :cond_15
/* .line 1158 */
v7 = this.mLock;
/* monitor-enter v7 */
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_0 */
/* .line 1159 */
try { // :try_start_6
v8 = this.mDualWifiApps;
v8 = java.lang.Integer .valueOf ( p2 );
if ( v8 != null) { // if-eqz v8, :cond_10
/* .line 1160 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v2 = "handleMultiAppsDualWifi Duplicate Request"; // const-string v2, "handleMultiAppsDualWifi Duplicate Request"
android.util.Log .d ( v0,v2 );
/* .line 1161 */
/* monitor-exit v7 */
return;
/* .line 1164 */
} // :cond_10
v8 = v8 = this.mDualWifiApps;
/* if-nez v8, :cond_11 */
/* .line 1165 */
v8 = this.mSlaService;
int v9 = 3; // const/4 v9, 0x3
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v8 ).setDoubleWifiWhiteList ( v9, v4, v6, v0 ); // invoke-virtual {v8, v9, v4, v6, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z
/* .line 1166 */
/* invoke-direct {p0, v2, v2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setOrGetEverOpenedDualWifi(ZZ)Z */
/* .line 1167 */
v8 = this.mMiuiWifiManager;
if ( v8 != null) { // if-eqz v8, :cond_12
/* .line 1168 */
(( android.net.wifi.MiuiWifiManager ) v8 ).setSlaveCandidatesRssiThreshold ( v2 ); // invoke-virtual {v8, v2}, Landroid/net/wifi/MiuiWifiManager;->setSlaveCandidatesRssiThreshold(Z)Z
/* .line 1170 */
} // :cond_11
v8 = this.mSlaService;
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v8 ).setDoubleWifiWhiteList ( v2, v4, v6, v0 ); // invoke-virtual {v8, v2, v4, v6, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z
/* .line 1171 */
v8 = this.mSlaService;
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v8 ).setDWUidToSlad ( ); // invoke-virtual {v8}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDWUidToSlad()V
/* .line 1174 */
} // :cond_12
} // :goto_2
v8 = this.mDualWifiApps;
java.lang.Integer .valueOf ( p2 );
/* .line 1175 */
/* iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z */
/* .line 1176 */
/* monitor-exit v7 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
/* .line 1178 */
try { // :try_start_7
v7 = this.mSlaveWifiManager;
/* if-nez v7, :cond_13 */
/* move v7, v0 */
} // :cond_13
v7 = (( android.net.wifi.SlaveWifiManager ) v7 ).setWifiSlaveEnabled ( p1 ); // invoke-virtual {v7, p1}, Landroid/net/wifi/SlaveWifiManager;->setWifiSlaveEnabled(Z)Z
/* .line 1179 */
/* .local v7, "res":Z */
} // :goto_3
if ( p1 != null) { // if-eqz p1, :cond_14
if ( v7 != null) { // if-eqz v7, :cond_14
/* move v0, v2 */
} // :cond_14
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).onSlaveWifiEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnable(Z)V
/* :try_end_7 */
/* .catch Ljava/lang/Exception; {:try_start_7 ..:try_end_7} :catch_0 */
/* .line 1180 */
} // .end local v7 # "res":Z
/* .line 1176 */
/* :catchall_2 */
/* move-exception v0 */
try { // :try_start_8
/* monitor-exit v7 */
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_2 */
} // .end local v1 # "isDualWifiOpen":Z
} // .end local v3 # "originStatus":I
} // .end local v4 # "packName":Ljava/lang/String;
} // .end local v5 # "isMarket":Z
} // .end local v6 # "uidStr":Ljava/lang/String;
} // .end local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
} // .end local p1 # "enable":Z
} // .end local p2 # "uid":I
try { // :try_start_9
/* throw v0 */
/* .line 1181 */
/* .restart local v1 # "isDualWifiOpen":Z */
/* .restart local v3 # "originStatus":I */
/* .restart local v4 # "packName":Ljava/lang/String; */
/* .restart local v5 # "isMarket":Z */
/* .restart local v6 # "uidStr":Ljava/lang/String; */
/* .restart local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* .restart local p1 # "enable":Z */
/* .restart local p2 # "uid":I */
} // :cond_15
v8 = this.mLock;
/* monitor-enter v8 */
/* :try_end_9 */
/* .catch Ljava/lang/Exception; {:try_start_9 ..:try_end_9} :catch_0 */
/* .line 1182 */
try { // :try_start_a
v9 = this.mDualWifiApps;
v9 = java.lang.Integer .valueOf ( p2 );
/* if-nez v9, :cond_16 */
/* .line 1183 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v2 = "handleMultiAppsDualWifi No request found"; // const-string v2, "handleMultiAppsDualWifi No request found"
android.util.Log .d ( v0,v2 );
/* .line 1184 */
/* monitor-exit v8 */
return;
/* .line 1186 */
} // :cond_16
v9 = this.mDualWifiApps;
java.lang.Integer .valueOf ( p2 );
/* .line 1188 */
v9 = v9 = this.mDualWifiApps;
/* if-nez v9, :cond_19 */
/* .line 1189 */
v7 = this.mSlaService;
final String v9 = ""; // const-string v9, ""
int v10 = 5; // const/4 v10, 0x5
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v7 ).setDoubleWifiWhiteList ( v10, v9, v6, v0 ); // invoke-virtual {v7, v10, v9, v6, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z
/* .line 1191 */
/* invoke-direct {p0, v2, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setOrGetEverOpenedDualWifi(ZZ)Z */
/* .line 1192 */
v7 = this.mMiuiWifiManager;
if ( v7 != null) { // if-eqz v7, :cond_17
/* .line 1193 */
(( android.net.wifi.MiuiWifiManager ) v7 ).setSlaveCandidatesRssiThreshold ( v0 ); // invoke-virtual {v7, v0}, Landroid/net/wifi/MiuiWifiManager;->setSlaveCandidatesRssiThreshold(Z)Z
/* .line 1195 */
} // :cond_17
v7 = this.mSlaveWifiManager;
/* if-nez v7, :cond_18 */
} // :goto_4
} // :cond_18
v0 = (( android.net.wifi.SlaveWifiManager ) v7 ).setWifiSlaveEnabled ( p1 ); // invoke-virtual {v7, p1}, Landroid/net/wifi/SlaveWifiManager;->setWifiSlaveEnabled(Z)Z
/* .line 1196 */
/* .local v0, "res":Z */
} // :goto_5
/* iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z */
/* .line 1197 */
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).onSlaveWifiEnable ( p1 ); // invoke-virtual {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->onSlaveWifiEnable(Z)V
/* .line 1198 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->clearDualWifiStatus()V */
/* .line 1199 */
} // .end local v0 # "res":Z
/* .line 1200 */
} // :cond_19
v2 = this.mSlaService;
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v2 ).setDoubleWifiWhiteList ( v7, v4, v6, v0 ); // invoke-virtual {v2, v7, v4, v6, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z
/* .line 1202 */
} // :goto_6
/* monitor-exit v8 */
/* .line 1207 */
} // :goto_7
/* .line 1202 */
/* :catchall_3 */
/* move-exception v0 */
/* monitor-exit v8 */
/* :try_end_a */
/* .catchall {:try_start_a ..:try_end_a} :catchall_3 */
} // .end local v1 # "isDualWifiOpen":Z
} // .end local v3 # "originStatus":I
} // .end local v4 # "packName":Ljava/lang/String;
} // .end local v5 # "isMarket":Z
} // .end local v6 # "uidStr":Ljava/lang/String;
} // .end local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
} // .end local p1 # "enable":Z
} // .end local p2 # "uid":I
try { // :try_start_b
/* throw v0 */
/* :try_end_b */
/* .catch Ljava/lang/Exception; {:try_start_b ..:try_end_b} :catch_0 */
/* .line 1205 */
/* .restart local v1 # "isDualWifiOpen":Z */
/* .restart local v3 # "originStatus":I */
/* .restart local v4 # "packName":Ljava/lang/String; */
/* .restart local v5 # "isMarket":Z */
/* .restart local v6 # "uidStr":Ljava/lang/String; */
/* .restart local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* .restart local p1 # "enable":Z */
/* .restart local p2 # "uid":I */
/* :catch_0 */
/* move-exception v0 */
/* .line 1206 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1209 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_8
v0 = this.mDualWifiApps;
if ( v0 != null) { // if-eqz v0, :cond_1a
/* .line 1210 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "after mDualWifiOriginStatus = "; // const-string v7, "after mDualWifiOriginStatus = "
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I */
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " mIsDualWifiSDKChanged = "; // const-string v7, " mIsDualWifiSDKChanged = "
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z */
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v7 = " mDualWifiApps.size = "; // const-string v7, " mDualWifiApps.size = "
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mDualWifiApps;
v7 = /* .line 1212 */
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1210 */
android.util.Log .d ( v0,v2 );
/* .line 1214 */
} // :cond_1a
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "after mDualWifiOriginStatus = "; // const-string v7, "after mDualWifiOriginStatus = "
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiOriginStatus:I */
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " mIsDualWifiSDKChanged = "; // const-string v7, " mIsDualWifiSDKChanged = "
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsDualWifiSDKChanged:Z */
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v2 );
/* .line 1217 */
} // :goto_9
return;
/* .line 1104 */
} // .end local v4 # "packName":Ljava/lang/String;
} // .end local v5 # "isMarket":Z
} // .end local v6 # "uidStr":Ljava/lang/String;
/* :catchall_4 */
/* move-exception v0 */
try { // :try_start_c
/* monitor-exit v4 */
/* :try_end_c */
/* .catchall {:try_start_c ..:try_end_c} :catchall_4 */
/* throw v0 */
} // .end method
private void ifaceAddCallBackSend ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1420 */
v0 = this.obj;
/* check-cast v0, Ljava/util/List; */
/* .line 1421 */
/* .local v0, "res":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = this.mCallbacks;
v1 = (( android.os.RemoteCallbackList ) v1 ).beginBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 1423 */
/* .local v1, "N":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* .line 1425 */
try { // :try_start_0
v3 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1428 */
/* .line 1426 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1427 */
/* .local v3, "e":Landroid/os/RemoteException; */
final String v4 = "NetworkSDKService"; // const-string v4, "NetworkSDKService"
final String v5 = "RemoteException at ifaceAddCallBackSend()"; // const-string v5, "RemoteException at ifaceAddCallBackSend()"
android.util.Log .e ( v4,v5 );
/* .line 1423 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1430 */
} // .end local v2 # "i":I
} // :cond_0
v2 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v2 ).finishBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1431 */
return;
} // .end method
private void ifaceRemovedCallBackSend ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1434 */
v0 = this.obj;
/* check-cast v0, Ljava/util/List; */
/* .line 1435 */
/* .local v0, "res":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = this.mCallbacks;
v1 = (( android.os.RemoteCallbackList ) v1 ).beginBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 1437 */
/* .local v1, "N":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* .line 1439 */
try { // :try_start_0
v3 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1442 */
/* .line 1440 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1441 */
/* .local v3, "e":Landroid/os/RemoteException; */
final String v4 = "NetworkSDKService"; // const-string v4, "NetworkSDKService"
final String v5 = "RemoteException at ifaceRemovedCallBackSend()"; // const-string v5, "RemoteException at ifaceRemovedCallBackSend()"
android.util.Log .e ( v4,v5 );
/* .line 1437 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1444 */
} // .end local v2 # "i":I
} // :cond_0
v2 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v2 ).finishBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1445 */
return;
} // .end method
private void initAppTrafficSessionAndSimCardHelper ( ) {
/* .locals 1 */
/* .line 860 */
v0 = this.mContext;
com.xiaomi.NetworkBoost.NetworkSDK.telephony.NetworkBoostSimCardHelper .getInstance ( v0 );
this.mSimCardHelper = v0;
/* .line 862 */
v0 = this.mStatsSession;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 863 */
(( miui.securitycenter.net.MiuiNetworkSessionStats ) v0 ).openSession ( ); // invoke-virtual {v0}, Lmiui/securitycenter/net/MiuiNetworkSessionStats;->openSession()V
/* .line 865 */
} // :cond_0
/* new-instance v0, Lmiui/securitycenter/net/MiuiNetworkSessionStats; */
/* invoke-direct {v0}, Lmiui/securitycenter/net/MiuiNetworkSessionStats;-><init>()V */
this.mStatsSession = v0;
/* .line 866 */
(( miui.securitycenter.net.MiuiNetworkSessionStats ) v0 ).openSession ( ); // invoke-virtual {v0}, Lmiui/securitycenter/net/MiuiNetworkSessionStats;->openSession()V
/* .line 868 */
} // :goto_0
return;
} // .end method
private void initBroadcastReceiver ( ) {
/* .locals 4 */
/* .line 2351 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 2352 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.PACKAGE_ADDED"; // const-string v1, "android.intent.action.PACKAGE_ADDED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 2353 */
final String v1 = "android.intent.action.PACKAGE_REMOVED"; // const-string v1, "android.intent.action.PACKAGE_REMOVED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 2354 */
final String v1 = "android.intent.action.PACKAGE_REPLACED"; // const-string v1, "android.intent.action.PACKAGE_REPLACED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 2355 */
final String v1 = "package"; // const-string v1, "package"
(( android.content.IntentFilter ) v0 ).addDataScheme ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V
/* .line 2356 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$10; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$10;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V */
/* .line 2411 */
/* .local v1, "receiver":Landroid/content/BroadcastReceiver; */
v2 = this.mContext;
int v3 = 4; // const/4 v3, 0x4
(( android.content.Context ) v2 ).registerReceiver ( v1, v0, v3 ); // invoke-virtual {v2, v1, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 2412 */
return;
} // .end method
private void initCellularNetworkSDKCloudObserver ( ) {
/* .locals 4 */
/* .line 2336 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$9; */
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$9;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Handler;)V */
/* .line 2342 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 2343 */
final String v2 = "cellular_networkboost_sdk_white_list_pkg_name"; // const-string v2, "cellular_networkboost_sdk_white_list_pkg_name"
android.provider.Settings$Global .getUriFor ( v2 );
/* .line 2342 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 2344 */
/* new-instance v1, Ljava/lang/Thread; */
/* new-instance v2, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$$ExternalSyntheticLambda0;-><init>(Landroid/database/ContentObserver;)V */
/* invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 2346 */
(( java.lang.Thread ) v1 ).start ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->start()V
/* .line 2347 */
return;
} // .end method
private void initCellularWhiteList ( ) {
/* .locals 2 */
/* .line 2449 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mCellularNetworkSDKPN = v0;
/* .line 2450 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mCellularNetworkSDKUid = v0;
/* .line 2451 */
v0 = this.mCellularNetworkSDKPN;
final String v1 = "com.miui.vpnsdkmanager"; // const-string v1, "com.miui.vpnsdkmanager"
/* .line 2452 */
v0 = this.mCellularNetworkSDKPN;
final String v1 = "com.miui.securityadd"; // const-string v1, "com.miui.securityadd"
/* .line 2453 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setCellularNetworkSDKUid()V */
/* .line 2454 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "Cellular white list init"; // const-string v1, "Cellular white list init"
android.util.Log .d ( v0,v1 );
/* .line 2455 */
return;
} // .end method
private void initDualDataBroadcastReceiver ( ) {
/* .locals 3 */
/* .line 2415 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 2416 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "com.android.phone.action.VIDEO_APPS_POLICY_NOTIFY"; // const-string v1, "com.android.phone.action.VIDEO_APPS_POLICY_NOTIFY"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 2417 */
final String v1 = "org.codeaurora.intent.action.MSIM_VOICE_CAPABILITY_CHANGED"; // const-string v1, "org.codeaurora.intent.action.MSIM_VOICE_CAPABILITY_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 2418 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$11; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$11;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V */
/* .line 2437 */
/* .local v1, "receiver":Landroid/content/BroadcastReceiver; */
v2 = this.mContext;
(( android.content.Context ) v2 ).registerReceiver ( v1, v0 ); // invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 2438 */
return;
} // .end method
private void initMibridgeCloudObserver ( ) {
/* .locals 4 */
/* .line 2320 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$8; */
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$8;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Handler;)V */
/* .line 2326 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 2327 */
final String v2 = "mibridge_authorized_pkg_list"; // const-string v2, "mibridge_authorized_pkg_list"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 2326 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 2328 */
/* new-instance v1, Ljava/lang/Thread; */
/* new-instance v2, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$$ExternalSyntheticLambda1; */
/* invoke-direct {v2, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$$ExternalSyntheticLambda1;-><init>(Landroid/database/ContentObserver;)V */
/* invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 2330 */
(( java.lang.Thread ) v1 ).start ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->start()V
/* .line 2331 */
return;
} // .end method
private void initNetworkSDKCloudObserver ( ) {
/* .locals 4 */
/* .line 2306 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$7; */
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$7;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Handler;)V */
/* .line 2312 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 2313 */
final String v2 = "cloud_networkboost_whitelist"; // const-string v2, "cloud_networkboost_whitelist"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 2312 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 2314 */
/* new-instance v1, Ljava/lang/Thread; */
/* new-instance v2, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$$ExternalSyntheticLambda2; */
/* invoke-direct {v2, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$$ExternalSyntheticLambda2;-><init>(Landroid/database/ContentObserver;)V */
/* invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 2316 */
(( java.lang.Thread ) v1 ).start ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->start()V
/* .line 2317 */
return;
} // .end method
private void initPermission ( ) {
/* .locals 5 */
/* .line 2278 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mPermission = v0;
/* .line 2279 */
/* const-string/jumbo v1, "setSockPrio" */
final String v2 = "android.permission.CHANGE_NETWORK_STATE"; // const-string v2, "android.permission.CHANGE_NETWORK_STATE"
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2280 */
v0 = this.mPermission;
/* const-string/jumbo v1, "setTCPCongestion" */
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2281 */
v0 = this.mPermission;
final String v1 = "isSupportDualWifi"; // const-string v1, "isSupportDualWifi"
final String v3 = "android.permission.ACCESS_WIFI_STATE"; // const-string v3, "android.permission.ACCESS_WIFI_STATE"
(( java.util.HashMap ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2282 */
v0 = this.mPermission;
/* const-string/jumbo v1, "setSlaveWifiEnabled" */
(( java.util.HashMap ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2283 */
v0 = this.mPermission;
final String v1 = "connectSlaveWifi"; // const-string v1, "connectSlaveWifi"
(( java.util.HashMap ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2284 */
v0 = this.mPermission;
final String v1 = "disconnectSlaveWifi"; // const-string v1, "disconnectSlaveWifi"
(( java.util.HashMap ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2285 */
v0 = this.mPermission;
/* const-string/jumbo v1, "suspendBackgroundScan" */
final String v4 = "android.permission.CHANGE_WIFI_STATE"; // const-string v4, "android.permission.CHANGE_WIFI_STATE"
(( java.util.HashMap ) v0 ).put ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2286 */
v0 = this.mPermission;
final String v1 = "resumeBackgroundScan"; // const-string v1, "resumeBackgroundScan"
(( java.util.HashMap ) v0 ).put ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2287 */
v0 = this.mPermission;
/* const-string/jumbo v1, "suspendWifiPowerSave" */
(( java.util.HashMap ) v0 ).put ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2288 */
v0 = this.mPermission;
final String v1 = "resumeWifiPowerSave"; // const-string v1, "resumeWifiPowerSave"
(( java.util.HashMap ) v0 ).put ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2289 */
v0 = this.mPermission;
final String v1 = "registerWifiLinkCallback"; // const-string v1, "registerWifiLinkCallback"
(( java.util.HashMap ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2290 */
v0 = this.mPermission;
/* const-string/jumbo v1, "unregisterWifiLinkCallback" */
(( java.util.HashMap ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2291 */
v0 = this.mPermission;
final String v1 = "getAvailableIfaces"; // const-string v1, "getAvailableIfaces"
final String v4 = "android.permission.ACCESS_NETWORK_STATE"; // const-string v4, "android.permission.ACCESS_NETWORK_STATE"
(( java.util.HashMap ) v0 ).put ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2292 */
v0 = this.mPermission;
final String v1 = "getQoEByAvailableIfaceName"; // const-string v1, "getQoEByAvailableIfaceName"
(( java.util.HashMap ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2293 */
v0 = this.mPermission;
/* const-string/jumbo v1, "setTrafficTransInterface" */
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2294 */
v0 = this.mPermission;
final String v1 = "requestAppTrafficStatistics"; // const-string v1, "requestAppTrafficStatistics"
(( java.util.HashMap ) v0 ).put ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2295 */
v0 = this.mPermission;
final String v1 = "registerNetLinkCallback"; // const-string v1, "registerNetLinkCallback"
(( java.util.HashMap ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2296 */
v0 = this.mPermission;
/* const-string/jumbo v1, "unregisterNetLinkCallback" */
(( java.util.HashMap ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2297 */
v0 = this.mPermission;
final String v1 = "enableWifiSelectionOpt"; // const-string v1, "enableWifiSelectionOpt"
(( java.util.HashMap ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2298 */
v0 = this.mPermission;
/* const-string/jumbo v1, "triggerWifiSelection" */
(( java.util.HashMap ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2299 */
v0 = this.mPermission;
final String v1 = "reportBssidScore"; // const-string v1, "reportBssidScore"
(( java.util.HashMap ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2300 */
v0 = this.mPermission;
final String v1 = "disableWifiSelectionOpt"; // const-string v1, "disableWifiSelectionOpt"
(( java.util.HashMap ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2301 */
v0 = this.mPermission;
final String v1 = "isSlaveWifiEnabledAndOthersOpt"; // const-string v1, "isSlaveWifiEnabledAndOthersOpt"
(( java.util.HashMap ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2302 */
return;
} // .end method
private void initWhiteList ( ) {
/* .locals 2 */
/* .line 2441 */
java.util.concurrent.ConcurrentHashMap .newKeySet ( );
this.mNetworkSDKPN = v0;
/* .line 2442 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mNetworkSDKUid = v0;
/* .line 2443 */
v0 = this.mNetworkSDKPN;
final String v1 = "com.android.settings"; // const-string v1, "com.android.settings"
/* .line 2444 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setNetworkSDKUid()V */
/* .line 2445 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
/* const-string/jumbo v1, "white list init" */
android.util.Log .d ( v0,v1 );
/* .line 2446 */
return;
} // .end method
private Boolean isSystemProcess ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 2273 */
/* const/16 v0, 0x2710 */
/* if-ge p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
static void lambda$initCellularNetworkSDKCloudObserver$2 ( android.database.ContentObserver p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "observer" # Landroid/database/ContentObserver; */
/* .line 2345 */
int v0 = 0; // const/4 v0, 0x0
(( android.database.ContentObserver ) p0 ).onChange ( v0 ); // invoke-virtual {p0, v0}, Landroid/database/ContentObserver;->onChange(Z)V
/* .line 2346 */
return;
} // .end method
static void lambda$initMibridgeCloudObserver$1 ( android.database.ContentObserver p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "observer" # Landroid/database/ContentObserver; */
/* .line 2329 */
int v0 = 0; // const/4 v0, 0x0
(( android.database.ContentObserver ) p0 ).onChange ( v0 ); // invoke-virtual {p0, v0}, Landroid/database/ContentObserver;->onChange(Z)V
/* .line 2330 */
return;
} // .end method
static void lambda$initNetworkSDKCloudObserver$0 ( android.database.ContentObserver p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "observer" # Landroid/database/ContentObserver; */
/* .line 2315 */
int v0 = 0; // const/4 v0, 0x0
(( android.database.ContentObserver ) p0 ).onChange ( v0 ); // invoke-virtual {p0, v0}, Landroid/database/ContentObserver;->onChange(Z)V
/* .line 2316 */
return;
} // .end method
private void mediaPlayerPolicyCallBackSend ( android.os.Message p0 ) {
/* .locals 8 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1480 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 1481 */
/* .local v0, "type":I */
/* iget v1, p1, Landroid/os/Message;->arg2:I */
/* .line 1482 */
/* .local v1, "duration":I */
v2 = this.obj;
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 1483 */
/* .local v2, "length":I */
v3 = this.mCallbacks;
v3 = (( android.os.RemoteCallbackList ) v3 ).beginBroadcast ( ); // invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 1485 */
/* .local v3, "N":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* if-ge v4, v3, :cond_0 */
/* .line 1487 */
try { // :try_start_0
v5 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v5 ).getBroadcastItem ( v4 ); // invoke-virtual {v5, v4}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1490 */
/* .line 1488 */
/* :catch_0 */
/* move-exception v5 */
/* .line 1489 */
/* .local v5, "e":Landroid/os/RemoteException; */
final String v6 = "NetworkSDKService"; // const-string v6, "NetworkSDKService"
final String v7 = "RemoteException at mediaPlayerPolicyCallBackSend()"; // const-string v7, "RemoteException at mediaPlayerPolicyCallBackSend()"
android.util.Log .e ( v6,v7 );
/* .line 1485 */
} // .end local v5 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1492 */
} // .end local v4 # "i":I
} // :cond_0
v4 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v4 ).finishBroadcast ( ); // invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1493 */
return;
} // .end method
private java.lang.String mibridgeWhiteList ( ) {
/* .locals 2 */
/* .line 2553 */
v0 = this.mContext;
/* .line 2554 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 2553 */
final String v1 = "mibridge_authorized_pkg_list"; // const-string v1, "mibridge_authorized_pkg_list"
android.provider.Settings$System .getString ( v0,v1 );
} // .end method
private void netLinkCallBackUnRegisterError ( ) {
/* .locals 15 */
/* .line 1613 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1614 */
try { // :try_start_0
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* .line 1615 */
/* .local v1, "appStatsPollMillis":Ljava/util/Map;, "Ljava/util/Map<Landroid/os/IBinder;Ljava/lang/Long;>;" */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
/* .line 1616 */
/* .local v2, "appStatsPollInterval":Ljava/util/Map;, "Ljava/util/Map<Landroid/os/IBinder;Ljava/lang/Long;>;" */
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
/* .line 1618 */
/* .local v3, "appCallBackMapToUid":Ljava/util/Map;, "Ljava/util/Map<Landroid/os/IBinder;Ljava/lang/Integer;>;" */
v4 = this.mWlanQoECallbacks;
v4 = (( android.os.RemoteCallbackList ) v4 ).getRegisteredCallbackCount ( ); // invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I
/* .line 1619 */
/* .local v4, "count":I */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v5 */
/* .line 1620 */
/* .local v5, "curSystemTime":J */
/* const-wide/16 v7, -0x1 */
/* .line 1621 */
/* .local v7, "minInterval":J */
int v9 = 0; // const/4 v9, 0x0
/* .local v9, "i":I */
} // :goto_0
/* const-wide/16 v10, -0x1 */
/* if-ge v9, v4, :cond_2 */
/* .line 1622 */
v12 = this.mWlanQoECallbacks;
/* .line 1623 */
(( android.os.RemoteCallbackList ) v12 ).getRegisteredCallbackItem ( v9 ); // invoke-virtual {v12, v9}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackItem(I)Landroid/os/IInterface;
/* check-cast v12, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .line 1624 */
/* .local v12, "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .line 1625 */
/* .local v13, "callBackKey":Landroid/os/IBinder; */
java.lang.Long .valueOf ( v5,v6 );
/* .line 1626 */
v14 = this.mAppStatsPollInterval;
/* check-cast v14, Ljava/lang/Long; */
/* .line 1627 */
v14 = this.mAppCallBackMapToUid;
/* check-cast v14, Ljava/lang/Integer; */
/* .line 1628 */
/* cmp-long v10, v7, v10 */
/* if-nez v10, :cond_0 */
/* .line 1629 */
v10 = this.mAppStatsPollInterval;
/* check-cast v10, Ljava/lang/Long; */
(( java.lang.Long ) v10 ).longValue ( ); // invoke-virtual {v10}, Ljava/lang/Long;->longValue()J
/* move-result-wide v10 */
/* move-wide v7, v10 */
/* .line 1630 */
} // :cond_0
v10 = this.mAppStatsPollInterval;
/* check-cast v10, Ljava/lang/Long; */
(( java.lang.Long ) v10 ).longValue ( ); // invoke-virtual {v10}, Ljava/lang/Long;->longValue()J
/* move-result-wide v10 */
/* cmp-long v10, v10, v7 */
/* if-gez v10, :cond_1 */
/* .line 1631 */
v10 = this.mAppStatsPollInterval;
/* check-cast v10, Ljava/lang/Long; */
(( java.lang.Long ) v10 ).longValue ( ); // invoke-virtual {v10}, Ljava/lang/Long;->longValue()J
/* move-result-wide v10 */
/* move-wide v7, v10 */
/* .line 1621 */
} // .end local v12 # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
} // .end local v13 # "callBackKey":Landroid/os/IBinder;
} // :cond_1
} // :goto_1
/* add-int/lit8 v9, v9, 0x1 */
/* .line 1634 */
} // .end local v9 # "i":I
} // :cond_2
/* cmp-long v9, v7, v10 */
if ( v9 != null) { // if-eqz v9, :cond_3
/* .line 1635 */
/* iput-wide v7, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J */
/* .line 1636 */
final String v9 = "NetworkSDKService"; // const-string v9, "NetworkSDKService"
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
final String v13 = "netLinkCallBackUnRegisterError interval = "; // const-string v13, "netLinkCallBackUnRegisterError interval = "
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v13, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J */
(( java.lang.StringBuilder ) v12 ).append ( v13, v14 ); // invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v9,v12 );
/* .line 1639 */
} // :cond_3
/* if-nez v4, :cond_4 */
/* .line 1640 */
final String v9 = "NetworkSDKService"; // const-string v9, "NetworkSDKService"
final String v12 = "netLinkCallBackUnRegisterError interval = -1"; // const-string v12, "netLinkCallBackUnRegisterError interval = -1"
android.util.Log .e ( v9,v12 );
/* .line 1641 */
int v9 = 0; // const/4 v9, 0x0
/* iput-boolean v9, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsOpenWifiLinkPoll:Z */
/* .line 1642 */
/* iput-wide v10, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J */
/* .line 1645 */
} // :cond_4
this.mAppStatsPollMillis = v1;
/* .line 1646 */
this.mAppStatsPollInterval = v2;
/* .line 1647 */
this.mAppCallBackMapToUid = v3;
/* .line 1648 */
} // .end local v1 # "appStatsPollMillis":Ljava/util/Map;, "Ljava/util/Map<Landroid/os/IBinder;Ljava/lang/Long;>;"
} // .end local v2 # "appStatsPollInterval":Ljava/util/Map;, "Ljava/util/Map<Landroid/os/IBinder;Ljava/lang/Long;>;"
} // .end local v3 # "appCallBackMapToUid":Ljava/util/Map;, "Ljava/util/Map<Landroid/os/IBinder;Ljava/lang/Integer;>;"
} // .end local v4 # "count":I
} // .end local v5 # "curSystemTime":J
} // .end local v7 # "minInterval":J
/* monitor-exit v0 */
/* .line 1649 */
return;
/* .line 1648 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void networkPriorityCallBackSend ( android.os.Message p0 ) {
/* .locals 8 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1463 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 1464 */
/* .local v0, "priorityMode":I */
/* iget v1, p1, Landroid/os/Message;->arg2:I */
/* .line 1465 */
/* .local v1, "trafficPolicy":I */
v2 = this.obj;
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 1466 */
/* .local v2, "thermalLevel":I */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "networkPriorityCallBackNotice: "; // const-string v4, "networkPriorityCallBackNotice: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "NetworkSDKService"; // const-string v4, "NetworkSDKService"
android.util.Log .i ( v4,v3 );
/* .line 1467 */
v3 = this.mCallbacks;
v3 = (( android.os.RemoteCallbackList ) v3 ).beginBroadcast ( ); // invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 1469 */
/* .local v3, "N":I */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* if-ge v5, v3, :cond_0 */
/* .line 1471 */
try { // :try_start_0
v6 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v6 ).getBroadcastItem ( v5 ); // invoke-virtual {v6, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v6, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1474 */
/* .line 1472 */
/* :catch_0 */
/* move-exception v6 */
/* .line 1473 */
/* .local v6, "e":Landroid/os/RemoteException; */
final String v7 = "RemoteException at networkPriorityCallBackSend()"; // const-string v7, "RemoteException at networkPriorityCallBackSend()"
android.util.Log .e ( v4,v7 );
/* .line 1469 */
} // .end local v6 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v5, v5, 0x1 */
/* .line 1476 */
} // .end local v5 # "i":I
} // :cond_0
v4 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v4 ).finishBroadcast ( ); // invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1477 */
return;
} // .end method
private void onScanSuccussed ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "result" # I */
/* .line 1342 */
/* new-instance v0, Landroid/os/Message; */
/* invoke-direct {v0}, Landroid/os/Message;-><init>()V */
/* .line 1343 */
/* .local v0, "msg":Landroid/os/Message; */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 1344 */
java.lang.Integer .valueOf ( p1 );
this.obj = v1;
/* .line 1345 */
v1 = this.mCallbackHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1346 */
return;
} // .end method
private void onSlaveWifiEnableV1 ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .line 1335 */
/* new-instance v0, Landroid/os/Message; */
/* invoke-direct {v0}, Landroid/os/Message;-><init>()V */
/* .line 1336 */
/* .local v0, "msg":Landroid/os/Message; */
/* const/16 v1, 0x3f8 */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 1337 */
java.lang.Integer .valueOf ( p1 );
this.obj = v1;
/* .line 1338 */
v1 = this.mCallbackHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1339 */
return;
} // .end method
private void registDualWifiStateBroadcastReceiver ( ) {
/* .locals 5 */
/* .line 1240 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "registDualWifiStateBroadcastReceiver"; // const-string v1, "registDualWifiStateBroadcastReceiver"
android.util.Log .d ( v0,v1 );
/* .line 1241 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 1242 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.net.wifi.WIFI_SLAVE_STATE_CHANGED"; // const-string v1, "android.net.wifi.WIFI_SLAVE_STATE_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1243 */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1244 */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1246 */
v1 = this.mContext;
/* new-instance v2, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$6; */
/* invoke-direct {v2, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$6;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V */
/* new-instance v3, Landroid/content/IntentFilter; */
/* invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V */
int v4 = 2; // const/4 v4, 0x2
(( android.content.Context ) v1 ).registerReceiver ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 1262 */
return;
} // .end method
private void registerQEEChangeObserver ( ) {
/* .locals 4 */
/* .line 2774 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "QEE cloud observer register"; // const-string v1, "QEE cloud observer register"
android.util.Log .d ( v0,v1 );
/* .line 2775 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$12; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$12;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Handler;)V */
this.mQEEObserver = v0;
/* .line 2793 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 2795 */
final String v1 = "cloud_qee_enable"; // const-string v1, "cloud_qee_enable"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mQEEObserver;
/* .line 2794 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v3 ); // invoke-virtual {v0, v1, v3, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2800 */
v0 = this.mHandler;
/* new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$13; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$13;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2806 */
return;
} // .end method
private void resetMonitorStatus ( ) {
/* .locals 2 */
/* .line 1754 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1755 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDualWifiBackgroundCount:I */
/* .line 1756 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsStartMonitorByBackground:Z */
/* .line 1757 */
/* monitor-exit v0 */
/* .line 1758 */
return;
/* .line 1757 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void resetQoECallback ( android.os.IBinder p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "callBackKey" # Landroid/os/IBinder; */
/* .param p2, "curSystemTime" # J */
/* .line 1822 */
v0 = this.mAppStatsPollMillis;
java.lang.Long .valueOf ( p2,p3 );
/* .line 1823 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
/* const-string/jumbo v1, "wifiLinkStatsCallbackSend: resetOneQoECallback" */
android.util.Log .d ( v0,v1 );
/* .line 1824 */
return;
} // .end method
private void setCellularNetworkSDKUid ( ) {
/* .locals 10 */
/* .line 2559 */
final String v0 = ""; // const-string v0, ""
/* .line 2561 */
/* .local v0, "cellularWhiteString_netSDK":Ljava/lang/String; */
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "cellular_networkboost_sdk_white_list_pkg_name"; // const-string v2, "cellular_networkboost_sdk_white_list_pkg_name"
android.provider.Settings$Global .getString ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v1 */
/* .line 2565 */
/* .line 2563 */
/* :catch_0 */
/* move-exception v1 */
/* .line 2564 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "get Cellular cloud whitelist error "; // const-string v4, "get Cellular cloud whitelist error "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 2567 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Cloud CellularNetworkSDKApp:"; // const-string v3, "Cloud CellularNetworkSDKApp:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 2568 */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 2569 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 2570 */
/* .local v1, "packages":[Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2571 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_1
/* array-length v3, v1 */
/* if-ge v2, v3, :cond_0 */
/* .line 2572 */
v3 = this.mCellularNetworkSDKPN;
/* aget-object v4, v1, v2 */
/* .line 2573 */
final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "setCellularNetworkSDKUid packages:" */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v5, v1, v2 */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 2571 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 2579 */
} // .end local v1 # "packages":[Ljava/lang/String;
} // .end local v2 # "i":I
} // :cond_0
try { // :try_start_1
v1 = this.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 2580 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
int v2 = 1; // const/4 v2, 0x1
(( android.content.pm.PackageManager ) v1 ).getInstalledPackages ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;
/* .line 2581 */
/* .local v2, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Landroid/content/pm/PackageInfo; */
/* .line 2582 */
/* .local v4, "app":Landroid/content/pm/PackageInfo; */
v5 = this.packageName;
if ( v5 != null) { // if-eqz v5, :cond_1
v5 = this.applicationInfo;
if ( v5 != null) { // if-eqz v5, :cond_1
v5 = this.mCellularNetworkSDKPN;
v6 = this.packageName;
v5 = /* .line 2583 */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 2584 */
v5 = this.applicationInfo;
/* iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 2585 */
/* .local v5, "uid":I */
v6 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mCellularListLock;
/* monitor-enter v6 */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 2586 */
try { // :try_start_2
v7 = this.mCellularNetworkSDKUid;
java.lang.Integer .toString ( v5 );
/* .line 2587 */
final String v7 = "NetworkSDKService"; // const-string v7, "NetworkSDKService"
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "setCellularNetworkSDKUid uid :" */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v7,v8 );
/* .line 2588 */
/* monitor-exit v6 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v6 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
} // .end local v0 # "cellularWhiteString_netSDK":Ljava/lang/String;
} // .end local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
try { // :try_start_3
/* throw v3 */
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_1 */
/* .line 2590 */
} // .end local v4 # "app":Landroid/content/pm/PackageInfo;
} // .end local v5 # "uid":I
/* .restart local v0 # "cellularWhiteString_netSDK":Ljava/lang/String; */
/* .restart local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
} // :cond_1
} // :goto_3
/* .line 2593 */
} // .end local v1 # "pm":Landroid/content/pm/PackageManager;
} // .end local v2 # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
} // :cond_2
/* .line 2591 */
/* :catch_1 */
/* move-exception v1 */
/* .line 2592 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "setCellularNetworkSDKUid error " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 2595 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_4
final String v1 = ""; // const-string v1, ""
/* .line 2596 */
/* .local v1, "white_list":Ljava/lang/String; */
v2 = this.mCellularNetworkSDKPN;
v3 = } // :goto_5
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Ljava/lang/String; */
/* .line 2597 */
/* .local v3, "pn":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2598 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2599 */
} // .end local v3 # "pn":Ljava/lang/String;
/* .line 2600 */
} // :cond_3
v2 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mCellularListLock;
/* monitor-enter v2 */
/* .line 2601 */
try { // :try_start_4
v3 = this.mCellularNetworkSDKUid;
v4 = } // :goto_6
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Ljava/lang/String; */
/* .line 2602 */
/* .local v4, "uid":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " "; // const-string v6, " "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v1, v5 */
/* .line 2603 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v1, v5 */
/* .line 2604 */
} // .end local v4 # "uid":Ljava/lang/String;
/* .line 2605 */
} // :cond_4
/* monitor-exit v2 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 2606 */
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "setCellularNetworkSDKUid :white list pn&uid : " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 2607 */
return;
/* .line 2605 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_5
/* monitor-exit v2 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* throw v3 */
} // .end method
private Boolean setFrameworkAndDriverRoaming ( Boolean p0, Integer p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "isOpen" # Z */
/* .param p2, "type" # I */
/* .param p3, "uid" # I */
/* .line 2024 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2025 */
/* .local v0, "ret":Z */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 2026 */
int v2 = 1; // const/4 v2, 0x1
/* if-nez p1, :cond_2 */
/* .line 2027 */
try { // :try_start_0
v3 = this.mWifiSelectionApps;
java.lang.Integer .valueOf ( p3 );
/* .line 2028 */
/* iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 2029 */
/* monitor-exit v1 */
/* .line 2032 */
} // :cond_0
v3 = this.mMiuiWifiManager;
if ( v3 != null) { // if-eqz v3, :cond_1
int v4 = 3; // const/4 v4, 0x3
/* if-eq p2, v4, :cond_1 */
/* .line 2033 */
v3 = (( android.net.wifi.MiuiWifiManager ) v3 ).netSDKSetIsDisableRoam ( p1, p2 ); // invoke-virtual {v3, p1, p2}, Landroid/net/wifi/MiuiWifiManager;->netSDKSetIsDisableRoam(ZI)Z
/* move v0, v3 */
/* .line 2035 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* .line 2037 */
} // :goto_0
/* iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z */
/* .line 2038 */
v2 = this.mCallbackHandler;
/* const/16 v3, 0x3f2 */
(( android.os.Handler ) v2 ).obtainMessage ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
(( android.os.Handler ) v2 ).sendMessage ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 2040 */
} // :cond_2
v3 = this.mWifiSelectionApps;
java.lang.Integer .valueOf ( p3 );
/* .line 2042 */
v3 = this.mMiuiWifiManager;
if ( v3 != null) { // if-eqz v3, :cond_3
v3 = v3 = this.mWifiSelectionApps;
/* if-nez v3, :cond_3 */
/* .line 2043 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsCloseRoaming:Z */
/* .line 2044 */
v2 = this.mMiuiWifiManager;
int v3 = -1; // const/4 v3, -0x1
v2 = (( android.net.wifi.MiuiWifiManager ) v2 ).netSDKSetIsDisableRoam ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Landroid/net/wifi/MiuiWifiManager;->netSDKSetIsDisableRoam(ZI)Z
/* move v0, v2 */
/* .line 2049 */
} // :goto_1
/* monitor-exit v1 */
/* .line 2051 */
/* .line 2046 */
} // :cond_3
/* monitor-exit v1 */
/* .line 2049 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private void setNetworkSDKUid ( ) {
/* .locals 10 */
/* .line 2483 */
final String v0 = ""; // const-string v0, ""
/* .line 2485 */
/* .local v0, "whiteString_netSDK":Ljava/lang/String; */
try { // :try_start_0
v1 = this.mContext;
/* .line 2486 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "cloud_networkboost_whitelist"; // const-string v2, "cloud_networkboost_whitelist"
/* .line 2485 */
android.provider.Settings$System .getString ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v1 */
/* .line 2489 */
/* .line 2487 */
/* :catch_0 */
/* move-exception v1 */
/* .line 2488 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "get cloud whitelist error "; // const-string v4, "get cloud whitelist error "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 2491 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Cloud whiteString_netSDK networkSDKApp:"; // const-string v3, "Cloud whiteString_netSDK networkSDKApp:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 2492 */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 2493 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 2494 */
/* .local v1, "packages":[Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2495 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_1
/* array-length v3, v1 */
/* if-ge v2, v3, :cond_0 */
/* .line 2496 */
v3 = this.mNetworkSDKPN;
/* aget-object v4, v1, v2 */
/* .line 2495 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 2501 */
} // .end local v1 # "packages":[Ljava/lang/String;
} // .end local v2 # "i":I
} // :cond_0
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mibridgeWhiteList()Ljava/lang/String; */
/* .line 2502 */
/* .local v1, "whiteString_mibridge":Ljava/lang/String; */
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Cloud whiteString_mibridge networkSDKApp:"; // const-string v4, "Cloud whiteString_mibridge networkSDKApp:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 2503 */
v2 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v2, :cond_1 */
/* .line 2504 */
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 2505 */
/* .local v2, "packages":[Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 2506 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_2
/* array-length v4, v2 */
/* if-ge v3, v4, :cond_1 */
/* .line 2507 */
v4 = this.mNetworkSDKPN;
/* aget-object v5, v2, v3 */
/* .line 2506 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 2514 */
} // .end local v2 # "packages":[Ljava/lang/String;
} // .end local v3 # "i":I
} // :cond_1
try { // :try_start_1
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 2515 */
/* .local v2, "pm":Landroid/content/pm/PackageManager; */
int v3 = 1; // const/4 v3, 0x1
(( android.content.pm.PackageManager ) v2 ).getInstalledPackages ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;
/* .line 2516 */
/* .local v3, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
v5 = } // :goto_3
if ( v5 != null) { // if-eqz v5, :cond_6
/* check-cast v5, Landroid/content/pm/PackageInfo; */
/* .line 2517 */
/* .local v5, "app":Landroid/content/pm/PackageInfo; */
v6 = this.packageName;
if ( v6 != null) { // if-eqz v6, :cond_2
v6 = this.applicationInfo;
if ( v6 != null) { // if-eqz v6, :cond_2
v6 = this.mNetworkSDKPN;
v7 = this.packageName;
v6 = /* .line 2518 */
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 2519 */
v6 = this.applicationInfo;
/* iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 2520 */
/* .local v6, "uid":I */
v7 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mListLock;
/* monitor-enter v7 */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 2521 */
try { // :try_start_2
v8 = this.mNetworkSDKUid;
java.lang.Integer .toString ( v6 );
/* .line 2522 */
/* monitor-exit v7 */
/* :catchall_0 */
/* move-exception v4 */
/* monitor-exit v7 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
} // .end local v0 # "whiteString_netSDK":Ljava/lang/String;
} // .end local v1 # "whiteString_mibridge":Ljava/lang/String;
} // .end local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
try { // :try_start_3
/* throw v4 */
/* .line 2525 */
} // .end local v6 # "uid":I
/* .restart local v0 # "whiteString_netSDK":Ljava/lang/String; */
/* .restart local v1 # "whiteString_mibridge":Ljava/lang/String; */
/* .restart local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
} // :cond_2
} // :goto_4
/* iget v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMarketUid:I */
int v7 = -1; // const/4 v7, -0x1
/* if-ne v6, v7, :cond_3 */
v6 = this.packageName;
final String v8 = "com.xiaomi.market"; // const-string v8, "com.xiaomi.market"
v6 = (( java.lang.String ) v6 ).equals ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 2526 */
v6 = this.applicationInfo;
/* iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iput v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMarketUid:I */
/* .line 2527 */
} // :cond_3
/* iget v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDownloadsUid:I */
/* if-ne v6, v7, :cond_4 */
v6 = this.packageName;
final String v7 = "com.android.providers.downloads"; // const-string v7, "com.android.providers.downloads"
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 2528 */
v6 = this.applicationInfo;
/* iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iput v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDownloadsUid:I */
/* .line 2530 */
} // :cond_4
if ( v5 != null) { // if-eqz v5, :cond_5
v6 = this.packageName;
if ( v6 != null) { // if-eqz v6, :cond_5
v6 = this.applicationInfo;
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 2531 */
v6 = this.mAppUidToPackName;
v7 = this.applicationInfo;
/* iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I */
java.lang.Integer .valueOf ( v7 );
v8 = this.packageName;
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_1 */
/* .line 2532 */
} // .end local v5 # "app":Landroid/content/pm/PackageInfo;
} // :cond_5
/* .line 2535 */
} // .end local v2 # "pm":Landroid/content/pm/PackageManager;
} // .end local v3 # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
} // :cond_6
/* .line 2533 */
/* :catch_1 */
/* move-exception v2 */
/* .line 2534 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "setNetworkSDKUid error " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v4 );
/* .line 2538 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_5
final String v2 = ""; // const-string v2, ""
/* .line 2539 */
/* .local v2, "white_list":Ljava/lang/String; */
v3 = this.mNetworkSDKPN;
v4 = } // :goto_6
if ( v4 != null) { // if-eqz v4, :cond_7
/* check-cast v4, Ljava/lang/String; */
/* .line 2540 */
/* .local v4, "pn":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " "; // const-string v6, " "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2541 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2542 */
} // .end local v4 # "pn":Ljava/lang/String;
/* .line 2543 */
} // :cond_7
v3 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mListLock;
/* monitor-enter v3 */
/* .line 2544 */
try { // :try_start_4
v4 = this.mNetworkSDKUid;
v5 = } // :goto_7
if ( v5 != null) { // if-eqz v5, :cond_8
/* check-cast v5, Ljava/lang/String; */
/* .line 2545 */
/* .local v5, "uid":Ljava/lang/String; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " "; // const-string v7, " "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v2, v6 */
/* .line 2546 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v2, v6 */
/* .line 2547 */
} // .end local v5 # "uid":Ljava/lang/String;
/* .line 2548 */
} // :cond_8
/* monitor-exit v3 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 2549 */
final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "white list pn&uid : " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 2550 */
return;
/* .line 2548 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_5
/* monitor-exit v3 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* throw v4 */
} // .end method
private Boolean setOrGetEverOpenedDualWifi ( Boolean p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "isSet" # Z */
/* .param p2, "isEverOpened" # Z */
/* .line 1220 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handleMultiAppsDualWifi isSet = "; // const-string v1, "handleMultiAppsDualWifi isSet = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " isEverOpened = "; // const-string v1, " isEverOpened = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
android.util.Log .d ( v1,v0 );
/* .line 1222 */
final String v0 = "is_opened_dual_wifi"; // const-string v0, "is_opened_dual_wifi"
int v1 = 1; // const/4 v1, 0x1
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1223 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putInt ( v2,v0,p2 );
/* .line 1225 */
/* .line 1227 */
} // :cond_0
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getInt ( v2,v0,v3 );
/* if-ne v0, v1, :cond_1 */
} // :cond_1
/* move v1, v3 */
} // :goto_0
} // .end method
private void setScreenStatus ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "isScreenON" # Z */
/* .line 1410 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1411 */
try { // :try_start_0
/* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsScreenON:Z */
/* .line 1412 */
/* if-nez p1, :cond_0 */
/* .line 1413 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScreenOffSystemTime:J */
/* .line 1415 */
} // :cond_0
/* const-wide/16 v1, -0x1 */
/* iput-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mScreenOffSystemTime:J */
/* .line 1416 */
} // :goto_0
/* monitor-exit v0 */
/* .line 1417 */
return;
/* .line 1416 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void unregisterQEEChangeObserver ( ) {
/* .locals 2 */
/* .line 2809 */
v0 = this.mQEEObserver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2810 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.mQEEObserver;
(( android.content.ContentResolver ) v0 ).unregisterContentObserver ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
/* .line 2811 */
int v0 = 0; // const/4 v0, 0x0
this.mQEEObserver = v0;
/* .line 2813 */
} // :cond_0
return;
} // .end method
private void upDateCloudWhiteList ( ) {
/* .locals 2 */
/* .line 2458 */
v0 = this.mNetworkSDKPN;
/* .line 2459 */
v0 = this.mAppUidToPackName;
/* .line 2460 */
v0 = this.mNetworkSDKPN;
final String v1 = "com.android.settings"; // const-string v1, "com.android.settings"
/* .line 2461 */
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mListLock;
/* monitor-enter v0 */
/* .line 2462 */
try { // :try_start_0
v1 = this.mNetworkSDKUid;
/* .line 2463 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 2464 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setNetworkSDKUid()V */
/* .line 2466 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "cloud white list changed"; // const-string v1, "cloud white list changed"
android.util.Log .d ( v0,v1 );
/* .line 2467 */
return;
/* .line 2463 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void updataCellularSdkList ( ) {
/* .locals 2 */
/* .line 2470 */
v0 = this.mCellularNetworkSDKPN;
/* .line 2471 */
v0 = this.mCellularNetworkSDKPN;
final String v1 = "com.miui.vpnsdkmanager"; // const-string v1, "com.miui.vpnsdkmanager"
/* .line 2472 */
v0 = this.mCellularNetworkSDKPN;
final String v1 = "com.miui.securityadd"; // const-string v1, "com.miui.securityadd"
/* .line 2473 */
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mCellularListLock;
/* monitor-enter v0 */
/* .line 2474 */
try { // :try_start_0
v1 = this.mCellularNetworkSDKUid;
/* .line 2475 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 2476 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setCellularNetworkSDKUid()V */
/* .line 2478 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "cloud Cellular white list changed"; // const-string v1, "cloud Cellular white list changed"
android.util.Log .d ( v0,v1 );
/* .line 2479 */
return;
/* .line 2475 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void wifiLinkLayerStatsPoll ( ) {
/* .locals 4 */
/* .line 1584 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1585 */
try { // :try_start_0
v1 = this.mWlanQoECallbacks;
if ( v1 != null) { // if-eqz v1, :cond_4
v2 = this.mAppStatsPollInterval;
/* if-nez v2, :cond_0 */
/* .line 1587 */
} // :cond_0
/* iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsOpenWifiLinkPoll:Z */
/* if-nez v2, :cond_1 */
/* .line 1588 */
final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
/* const-string/jumbo v2, "wifiLinkLayerStatsPoll polling normal exit" */
android.util.Log .d ( v1,v2 );
/* .line 1589 */
/* monitor-exit v0 */
return;
/* .line 1592 */
} // :cond_1
v1 = (( android.os.RemoteCallbackList ) v1 ).getRegisteredCallbackCount ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I
/* .line 1593 */
/* .local v1, "count":I */
/* if-nez v1, :cond_2 */
/* .line 1594 */
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
/* const-string/jumbo v3, "wifiLinkLayerStatsPoll polling Self-monitoring exit" */
android.util.Log .d ( v2,v3 );
/* .line 1595 */
/* monitor-exit v0 */
return;
/* .line 1597 */
} // :cond_2
v2 = v2 = this.mAppStatsPollInterval;
/* if-eq v1, v2, :cond_3 */
/* .line 1598 */
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
/* const-string/jumbo v3, "wifiLinkLayerStatsPoll netLinkQoE callBack unregister Error polling exit" */
android.util.Log .e ( v2,v3 );
/* .line 1599 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->netLinkCallBackUnRegisterError()V */
/* .line 1601 */
} // .end local v1 # "count":I
} // :cond_3
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1603 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->wifiLinkStatsCallbackSend()V */
/* .line 1605 */
v0 = this.mCallbackHandler;
/* .line 1606 */
/* const/16 v1, 0x3f1 */
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* iget-wide v2, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J */
/* .line 1605 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1609 */
return;
/* .line 1586 */
} // :cond_4
} // :goto_0
try { // :try_start_1
/* monitor-exit v0 */
return;
/* .line 1601 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void wifiLinkStatsCallbackSend ( ) {
/* .locals 20 */
/* .line 1782 */
/* move-object/from16 v1, p0 */
/* const-string/jumbo v0, "wlan0" */
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).getQoEByAvailableIfaceNameV1 ( v0 ); // invoke-virtual {v1, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->getQoEByAvailableIfaceNameV1(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
/* .line 1783 */
/* .local v2, "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
/* const-string/jumbo v0, "wlan1" */
(( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).getQoEByAvailableIfaceNameV1 ( v0 ); // invoke-virtual {v1, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->getQoEByAvailableIfaceNameV1(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
/* .line 1784 */
/* .local v3, "statsWlan1":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
v4 = this.mLock;
/* monitor-enter v4 */
/* .line 1785 */
try { // :try_start_0
v0 = this.mWlanQoECallbacks;
v0 = (( android.os.RemoteCallbackList ) v0 ).beginBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v5, v0 */
/* .line 1786 */
/* .local v5, "N":I */
int v0 = 0; // const/4 v0, 0x0
/* move v6, v0 */
/* .local v6, "i":I */
} // :goto_0
/* if-ge v6, v5, :cond_2 */
/* .line 1788 */
try { // :try_start_1
v0 = this.mWlanQoECallbacks;
(( android.os.RemoteCallbackList ) v0 ).getBroadcastItem ( v6 ); // invoke-virtual {v0, v6}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .line 1789 */
/* .local v0, "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .line 1791 */
/* .local v7, "callBackKey":Landroid/os/IBinder; */
v8 = this.mAppStatsPollInterval;
/* check-cast v8, Ljava/lang/Long; */
(( java.lang.Long ) v8 ).longValue ( ); // invoke-virtual {v8}, Ljava/lang/Long;->longValue()J
/* move-result-wide v8 */
/* .line 1792 */
/* .local v8, "appQoESendInterval":J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v10 */
/* .line 1793 */
/* .local v10, "curSystemTime":J */
v12 = this.mAppStatsPollMillis;
/* check-cast v12, Ljava/lang/Long; */
(( java.lang.Long ) v12 ).longValue ( ); // invoke-virtual {v12}, Ljava/lang/Long;->longValue()J
/* move-result-wide v12 */
/* sub-long v12, v10, v12 */
/* .line 1794 */
/* .local v12, "interval":J */
/* const-wide/16 v14, 0x64 */
/* div-long v14, v8, v14 */
/* const-wide/16 v16, 0x21 */
/* mul-long v14, v14, v16 */
/* const-wide/16 v16, 0x32 */
/* add-long v14, v14, v16 */
/* .line 1803 */
/* .local v14, "deviation":J */
/* cmp-long v16, v12, v8 */
/* if-ltz v16, :cond_0 */
/* add-long v16, v8, v14 */
/* cmp-long v16, v12, v16 */
/* if-gtz v16, :cond_0 */
/* .line 1805 */
/* .line 1806 */
/* .line 1807 */
/* move-object/from16 v16, v0 */
} // .end local v0 # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
/* .local v16, "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
v0 = this.mAppStatsPollMillis;
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* move-object/from16 v17, v2 */
} // .end local v2 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
/* .local v17, "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
try { // :try_start_2
java.lang.Long .valueOf ( v10,v11 );
/* .line 1803 */
} // .end local v16 # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
} // .end local v17 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
/* .restart local v0 # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .restart local v2 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
} // :cond_0
/* move-object/from16 v16, v0 */
/* move-object/from16 v17, v2 */
/* .line 1809 */
} // .end local v0 # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
} // .end local v2 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
/* .restart local v16 # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .restart local v17 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
/* add-long v18, v8, v14 */
/* cmp-long v0, v12, v18 */
/* if-lez v0, :cond_1 */
/* .line 1810 */
/* invoke-direct {v1, v7, v10, v11}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->resetQoECallback(Landroid/os/IBinder;J)V */
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 1812 */
} // .end local v7 # "callBackKey":Landroid/os/IBinder;
} // .end local v8 # "appQoESendInterval":J
} // .end local v10 # "curSystemTime":J
} // .end local v12 # "interval":J
} // .end local v14 # "deviation":J
} // .end local v16 # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
/* :catch_0 */
/* move-exception v0 */
/* .line 1815 */
} // :cond_1
} // :goto_1
/* .line 1812 */
} // .end local v17 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
/* .restart local v2 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v17, v2 */
/* .line 1813 */
} // .end local v2 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
/* .local v0, "e":Landroid/os/RemoteException; */
/* .restart local v17 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
} // :goto_2
try { // :try_start_3
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
final String v7 = "RemoteException at wifiLinkStatsCallbackSend()"; // const-string v7, "RemoteException at wifiLinkStatsCallbackSend()"
android.util.Log .e ( v2,v7 );
/* .line 1814 */
(( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 1786 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_3
/* add-int/lit8 v6, v6, 0x1 */
/* move-object/from16 v2, v17 */
} // .end local v17 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
/* .restart local v2 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
} // :cond_2
/* move-object/from16 v17, v2 */
/* .line 1817 */
} // .end local v2 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
} // .end local v6 # "i":I
/* .restart local v17 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
v0 = this.mWlanQoECallbacks;
(( android.os.RemoteCallbackList ) v0 ).finishBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1818 */
} // .end local v5 # "N":I
/* monitor-exit v4 */
/* .line 1819 */
return;
/* .line 1818 */
} // .end local v17 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
/* .restart local v2 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
/* :catchall_0 */
/* move-exception v0 */
/* move-object/from16 v17, v2 */
} // .end local v2 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
/* .restart local v17 # "statsWlan0":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
} // :goto_4
/* monitor-exit v4 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v0 */
/* :catchall_1 */
/* move-exception v0 */
} // .end method
/* # virtual methods */
public Boolean abortScan ( ) {
/* .locals 2 */
/* .line 667 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "abortScan: "; // const-string v1, "abortScan: "
android.util.Log .i ( v0,v1 );
/* .line 668 */
v0 = this.mScanner;
v1 = this.mScanListener;
(( android.net.wifi.WifiScanner ) v0 ).stopScan ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/wifi/WifiScanner;->stopScan(Landroid/net/wifi/WifiScanner$ScanListener;)V
/* .line 669 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean activeScan ( Integer[] p0 ) {
/* .locals 3 */
/* .param p1, "channelList" # [I */
/* .line 645 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "activeScan: "; // const-string v1, "activeScan: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
android.util.Log .i ( v1,v0 );
/* .line 646 */
v0 = this.mScanSettings;
int v1 = 2; // const/4 v1, 0x2
/* iput v1, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->type:I */
/* .line 647 */
v0 = this.mScanSettings;
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->band:I */
/* .line 648 */
v0 = this.mScanSettings;
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService .channelsToSpec ( p1 );
this.channels = v2;
/* .line 649 */
v0 = this.mScanSettings;
/* const/16 v2, 0x2710 */
/* iput v2, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->periodInMs:I */
/* .line 650 */
v0 = this.mScanSettings;
/* const/16 v2, 0x14 */
/* iput v2, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->numBssidsPerScan:I */
/* .line 651 */
v0 = this.mScanSettings;
/* iput v1, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->maxScansToCache:I */
/* .line 652 */
v0 = this.mScanSettings;
int v1 = 3; // const/4 v1, 0x3
/* iput v1, v0, Landroid/net/wifi/WifiScanner$ScanSettings;->reportEvents:I */
/* .line 654 */
v0 = this.mScanner;
v1 = this.mScanSettings;
v2 = this.mScanListener;
(( android.net.wifi.WifiScanner ) v0 ).startScan ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiScanner;->startScan(Landroid/net/wifi/WifiScanner$ScanSettings;Landroid/net/wifi/WifiScanner$ScanListener;)V
/* .line 655 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean cellularNetworkSDKPermissionCheck ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "callingName" # Ljava/lang/String; */
/* .line 2249 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 2250 */
/* .local v0, "uid":I */
v1 = /* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSystemProcess(I)Z */
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2251 */
} // :cond_0
v1 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mCellularListLock;
/* monitor-enter v1 */
/* .line 2252 */
try { // :try_start_0
v3 = this.mCellularNetworkSDKUid;
v3 = java.lang.Integer .toString ( v0 );
int v4 = 0; // const/4 v4, 0x0
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 2253 */
final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "WhiteList passed(cellular): "; // const-string v6, "WhiteList passed(cellular): "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v5 );
/* .line 2258 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 2259 */
v1 = this.mPermission;
(( java.util.HashMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
/* .line 2260 */
/* .local v1, "permission":Ljava/lang/String; */
/* if-nez v1, :cond_1 */
/* .line 2261 */
} // :cond_1
v3 = android.os.Binder .getCallingPid ( );
/* .line 2262 */
/* .local v3, "pid":I */
v5 = this.mContext;
v5 = (( android.content.Context ) v5 ).checkPermission ( v1, v3, v0 ); // invoke-virtual {v5, v1, v3, v0}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I
/* .line 2263 */
/* .local v5, "checkResult":I */
/* if-nez v5, :cond_2 */
/* .line 2264 */
final String v4 = "NetworkSDKService"; // const-string v4, "NetworkSDKService"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "permission granted(cellular): "; // const-string v7, "permission granted(cellular): "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " "; // const-string v7, " "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v6 );
/* .line 2265 */
/* .line 2267 */
} // :cond_2
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "permission denied(cellular): "; // const-string v7, "permission denied(cellular): "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " "; // const-string v7, " "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v6 );
/* .line 2268 */
/* .line 2255 */
} // .end local v1 # "permission":Ljava/lang/String;
} // .end local v3 # "pid":I
} // .end local v5 # "checkResult":I
} // :cond_3
try { // :try_start_1
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "WhiteList denied(cellular): "; // const-string v5, "WhiteList denied(cellular): "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 2256 */
/* monitor-exit v1 */
/* .line 2258 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
public Boolean connectSlaveWifi ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "networkId" # I */
/* .line 558 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "connectSlaveWifi: "; // const-string v1, "connectSlaveWifi: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
android.util.Log .i ( v1,v0 );
/* .line 560 */
final String v0 = "connectSlaveWifi"; // const-string v0, "connectSlaveWifi"
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 561 */
int v0 = 0; // const/4 v0, 0x0
/* .line 563 */
} // :cond_0
android.os.Message .obtain ( );
/* .line 564 */
/* .local v0, "msg":Landroid/os/Message; */
/* const/16 v1, 0x65 */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 565 */
java.lang.Integer .valueOf ( p1 );
this.obj = v1;
/* .line 566 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 567 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
public Boolean disableWifiSelectionOpt ( com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback p0 ) {
/* .locals 2 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
/* .line 2002 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 2003 */
/* .local v0, "uid":I */
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).disableWifiSelectionOpt ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->disableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;I)Z
} // .end method
public Boolean disableWifiSelectionOpt ( com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
/* .param p2, "uid" # I */
/* .line 2007 */
final String v0 = "disableWifiSelectionOpt"; // const-string v0, "disableWifiSelectionOpt"
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 2008 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "disableWifiSelectionOpt No permission"; // const-string v1, "disableWifiSelectionOpt No permission"
android.util.Log .e ( v0,v1 );
/* .line 2009 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2012 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 2014 */
/* .local v0, "ret":Z */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 2015 */
try { // :try_start_0
v2 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v2 ).unregister ( p1 ); // invoke-virtual {v2, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 2017 */
int v2 = 1; // const/4 v2, 0x1
int v3 = -1; // const/4 v3, -0x1
v2 = /* invoke-direct {p0, v2, v3, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setFrameworkAndDriverRoaming(ZII)Z */
/* move v0, v2 */
/* .line 2018 */
/* monitor-exit v1 */
/* .line 2020 */
/* .line 2018 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Boolean disconnectSlaveWifi ( ) {
/* .locals 2 */
/* .line 571 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "disconnectSlaveWifi: "; // const-string v1, "disconnectSlaveWifi: "
android.util.Log .i ( v0,v1 );
/* .line 573 */
final String v0 = "disconnectSlaveWifi"; // const-string v0, "disconnectSlaveWifi"
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 574 */
int v0 = 0; // const/4 v0, 0x0
/* .line 576 */
} // :cond_0
v0 = this.mHandler;
/* const/16 v1, 0x66 */
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 577 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void dumpModule ( java.io.PrintWriter p0 ) {
/* .locals 6 */
/* .param p1, "writer" # Ljava/io/PrintWriter; */
/* .line 2755 */
final String v0 = "NetworkSDKService end.\n"; // const-string v0, "NetworkSDKService end.\n"
final String v1 = " "; // const-string v1, " "
/* .line 2757 */
/* .local v1, "spacePrefix":Ljava/lang/String; */
try { // :try_start_0
final String v2 = "NetworkSDKService begin:"; // const-string v2, "NetworkSDKService begin:"
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2758 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "isConnectSlaveAp:"; // const-string v3, "isConnectSlaveAp:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isConnectSlaveAp:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2759 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 2770 */
/* .line 2761 */
/* :catch_0 */
/* move-exception v2 */
/* .line 2762 */
/* .local v2, "ex":Ljava/lang/Exception; */
final String v3 = "dump failed!"; // const-string v3, "dump failed!"
final String v4 = "NetworkSDKService"; // const-string v4, "NetworkSDKService"
android.util.Log .e ( v4,v3,v2 );
/* .line 2764 */
try { // :try_start_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Dump of NetworkSDKService failed:"; // const-string v5, "Dump of NetworkSDKService failed:"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2765 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 2769 */
/* .line 2767 */
/* :catch_1 */
/* move-exception v0 */
/* .line 2768 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "dump failure failed:"; // const-string v5, "dump failure failed:"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v4,v3 );
/* .line 2771 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end local v2 # "ex":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public Boolean enableWifiSelectionOpt ( com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
/* .param p2, "type" # I */
/* .line 1830 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 1831 */
/* .local v0, "uid":I */
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).enableWifiSelectionOpt ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->enableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;II)Z
} // .end method
public Boolean enableWifiSelectionOpt ( com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
/* .param p2, "type" # I */
/* .param p3, "uid" # I */
/* .line 1835 */
int v0 = 0; // const/4 v0, 0x0
/* if-ltz p2, :cond_2 */
int v1 = 3; // const/4 v1, 0x3
/* if-le p2, v1, :cond_0 */
/* .line 1838 */
} // :cond_0
final String v1 = "enableWifiSelectionOpt"; // const-string v1, "enableWifiSelectionOpt"
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v1 ); // invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
/* if-nez v1, :cond_1 */
/* .line 1839 */
final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
final String v2 = "enableWifiSelectionOpt No permission"; // const-string v2, "enableWifiSelectionOpt No permission"
android.util.Log .e ( v1,v2 );
/* .line 1840 */
/* .line 1842 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 1844 */
/* .local v1, "ret":Z */
v2 = this.mLock;
/* monitor-enter v2 */
/* .line 1845 */
try { // :try_start_0
v3 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v3 ).register ( p1 ); // invoke-virtual {v3, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
/* .line 1846 */
v0 = /* invoke-direct {p0, v0, p2, p3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setFrameworkAndDriverRoaming(ZII)Z */
/* move v1, v0 */
/* .line 1847 */
/* monitor-exit v2 */
/* .line 1849 */
/* .line 1847 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
/* .line 1836 */
} // .end local v1 # "ret":Z
} // :cond_2
} // :goto_0
} // .end method
public java.util.Map getAvailableIfaces ( ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 581 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 583 */
/* .local v0, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
final String v1 = "getAvailableIfaces"; // const-string v1, "getAvailableIfaces"
/* .line 584 */
/* .local v1, "name":Ljava/lang/String; */
v2 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v1 ); // invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
final String v3 = "code"; // const-string v3, "code"
/* if-nez v2, :cond_0 */
/* .line 585 */
final String v2 = "1001"; // const-string v2, "1001"
/* .line 586 */
final String v2 = "message"; // const-string v2, "message"
final String v3 = "No permission"; // const-string v3, "No permission"
/* .line 587 */
/* .line 590 */
} // :cond_0
v2 = this.mStatusManager;
(( com.xiaomi.NetworkBoost.StatusManager ) v2 ).getAvailableIfaces ( ); // invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->getAvailableIfaces()Ljava/util/List;
/* .line 591 */
/* .local v2, "ifacesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v4 = ""; // const-string v4, ""
/* .line 592 */
/* .local v4, "ifaces":Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
v6 = } // :goto_0
/* if-ge v5, v6, :cond_2 */
/* .line 593 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v7, Ljava/lang/String; */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v6 = /* .line 594 */
/* add-int/lit8 v6, v6, -0x1 */
/* if-eq v5, v6, :cond_1 */
/* .line 595 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ","; // const-string v7, ","
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 592 */
} // :cond_1
/* add-int/lit8 v5, v5, 0x1 */
/* .line 598 */
} // .end local v5 # "i":I
} // :cond_2
final String v5 = "1000"; // const-string v5, "1000"
/* .line 599 */
final String v3 = "result"; // const-string v3, "result"
/* .line 601 */
} // .end method
public java.util.Map getQoEByAvailableIfaceName ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "ifaceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 1497 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public com.xiaomi.NetworkBoost.NetLinkLayerQoE getQoEByAvailableIfaceNameV1 ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "ifaceName" # Ljava/lang/String; */
/* .line 1501 */
final String v0 = "getQoEByAvailableIfaceName"; // const-string v0, "getQoEByAvailableIfaceName"
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1502 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v2 = "getQoEByAvailableIfaceNameV1 No permission"; // const-string v2, "getQoEByAvailableIfaceNameV1 No permission"
android.util.Log .e ( v0,v2 );
/* .line 1503 */
/* .line 1507 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsScreenON:Z */
/* if-nez v0, :cond_1 */
/* .line 1508 */
/* .line 1510 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 1511 */
/* .local v0, "netLayerQoE":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
/* const-string/jumbo v2, "wlan0" */
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1512 */
v0 = this.mMasterNetLayerQoE;
/* .line 1513 */
} // :cond_2
/* const-string/jumbo v2, "wlan1" */
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1514 */
v0 = this.mSlaveNetLayerQoE;
/* .line 1518 */
} // :goto_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v2 */
/* .line 1520 */
/* .local v2, "callingId":J */
try { // :try_start_0
v4 = this.mLock;
/* monitor-enter v4 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1521 */
try { // :try_start_1
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->availableIfaceCheckAndQoEPull(Ljava/lang/String;)Landroid/net/wifi/WlanLinkLayerQoE; */
this.mMiuiWlanLayerQoE = v5;
/* .line 1522 */
/* if-nez v5, :cond_3 */
/* .line 1523 */
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1534 */
android.os.Binder .restoreCallingIdentity ( v2,v3 );
/* .line 1523 */
/* .line 1526 */
} // :cond_3
try { // :try_start_2
com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKUtils .copyFromNetWlanLinkLayerQoE ( v5,v0 );
/* move-object v0, v1 */
/* .line 1529 */
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->calculateLostAndRetryRatio(Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;)V */
/* .line 1530 */
/* monitor-exit v4 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v4 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
} // .end local v0 # "netLayerQoE":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
} // .end local v2 # "callingId":J
} // .end local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
} // .end local p1 # "ifaceName":Ljava/lang/String;
try { // :try_start_3
/* throw v1 */
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 1534 */
/* .restart local v0 # "netLayerQoE":Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
/* .restart local v2 # "callingId":J */
/* .restart local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* .restart local p1 # "ifaceName":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v1 */
/* .line 1531 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1532 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_4
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 1534 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v2,v3 );
/* .line 1535 */
/* nop */
/* .line 1537 */
/* .line 1534 */
} // :goto_2
android.os.Binder .restoreCallingIdentity ( v2,v3 );
/* .line 1535 */
/* throw v1 */
/* .line 1516 */
} // .end local v2 # "callingId":J
} // :cond_4
} // .end method
public void ifaceAddedCallBackNotice ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1384 */
/* .local p1, "availableIfaces":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez p1, :cond_0 */
/* .line 1385 */
return;
/* .line 1387 */
} // :cond_0
v0 = this.mCallbackHandler;
/* .line 1388 */
/* const/16 v1, 0x3ef */
(( android.os.Handler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1387 */
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1390 */
return;
} // .end method
public void ifaceRemovedCallBackNotice ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1393 */
/* .local p1, "availableIfaces":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez p1, :cond_0 */
/* .line 1394 */
return;
/* .line 1396 */
} // :cond_0
v0 = this.mCallbackHandler;
/* .line 1397 */
/* const/16 v1, 0x3f0 */
(( android.os.Handler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1396 */
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1399 */
return;
} // .end method
public Boolean isCelluarDSDAState ( ) {
/* .locals 2 */
/* .line 2638 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "isCelluarDSDAState"; // const-string v1, "isCelluarDSDAState"
android.util.Log .i ( v0,v1 );
/* .line 2640 */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).cellularNetworkSDKPermissionCheck ( v1 ); // invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->cellularNetworkSDKPermissionCheck(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 2641 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2642 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDsdaCapability:Z */
} // .end method
public Integer isSlaveWifiEnabledAndOthersOpt ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .line 2670 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 2671 */
/* .local v0, "uid":I */
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).isSlaveWifiEnabledAndOthersOpt ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSlaveWifiEnabledAndOthersOpt(II)I
} // .end method
public Integer isSlaveWifiEnabledAndOthersOpt ( Integer p0, Integer p1 ) {
/* .locals 12 */
/* .param p1, "type" # I */
/* .param p2, "uid" # I */
/* .line 2675 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2676 */
/* .local v0, "success":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 2677 */
/* .local v1, "negated":I */
int v2 = -1; // const/4 v2, -0x1
/* .line 2680 */
/* .local v2, "error":I */
final String v3 = "isSlaveWifiEnabledAndOthersOpt"; // const-string v3, "isSlaveWifiEnabledAndOthersOpt"
v3 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v3 ); // invoke-virtual {p0, v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
final String v4 = "NetworkSDKService"; // const-string v4, "NetworkSDKService"
/* if-nez v3, :cond_0 */
/* .line 2681 */
final String v3 = "isSlaveWifiEnabledAndOthersOpt No permission"; // const-string v3, "isSlaveWifiEnabledAndOthersOpt No permission"
android.util.Log .e ( v4,v3 );
/* .line 2682 */
/* .line 2685 */
} // :cond_0
v3 = this.mSlaveWifiManager;
/* if-nez v3, :cond_1 */
/* .line 2686 */
/* .line 2689 */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
/* .line 2690 */
/* .local v3, "isMarket":Z */
/* iget v5, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mMarketUid:I */
int v6 = -1; // const/4 v6, -0x1
/* if-eq v5, v6, :cond_2 */
/* if-ne v5, p2, :cond_2 */
/* .line 2691 */
int v3 = 1; // const/4 v3, 0x1
/* .line 2693 */
} // :cond_2
v5 = this.mSlaService;
/* if-nez v5, :cond_3 */
/* .line 2694 */
v5 = this.mContext;
com.xiaomi.NetworkBoost.slaservice.SLAService .getInstance ( v5 );
this.mSlaService = v5;
/* .line 2696 */
} // :cond_3
int v5 = 0; // const/4 v5, 0x0
/* .line 2697 */
/* .local v5, "packName":Ljava/lang/String; */
v6 = this.mAppUidToPackName;
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 2698 */
java.lang.Integer .valueOf ( p2 );
/* move-object v5, v6 */
/* check-cast v5, Ljava/lang/String; */
/* .line 2699 */
} // :cond_4
/* if-nez v5, :cond_5 */
if ( p1 != null) { // if-eqz p1, :cond_5
/* .line 2700 */
/* .line 2702 */
} // :cond_5
java.lang.String .valueOf ( p2 );
/* .line 2703 */
/* .local v6, "uidStr":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 2704 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ",com.android.providers.downloads"; // const-string v8, ",com.android.providers.downloads"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2705 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ","; // const-string v8, ","
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDownloadsUid:I */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2707 */
} // :cond_6
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "isSlaveWifiEnabledAndOthersOpt packName = "; // const-string v8, "isSlaveWifiEnabledAndOthersOpt packName = "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v7 );
/* .line 2709 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v7 */
/* .line 2711 */
/* .local v7, "callingUid":J */
int v9 = 1; // const/4 v9, 0x1
/* packed-switch p1, :pswitch_data_0 */
/* .line 2745 */
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 2746 */
/* goto/16 :goto_1 */
/* .line 2733 */
/* :pswitch_0 */
try { // :try_start_0
v9 = this.mSlaService;
int v10 = 4; // const/4 v10, 0x4
int v11 = 0; // const/4 v11, 0x0
v4 = (( com.xiaomi.NetworkBoost.slaservice.SLAService ) v9 ).setDoubleWifiWhiteList ( v10, v5, v6, v11 ); // invoke-virtual {v9, v10, v5, v6, v11}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 2734 */
/* nop */
/* .line 2745 */
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 2734 */
/* .line 2736 */
} // :cond_7
/* nop */
/* .line 2745 */
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 2736 */
/* .line 2726 */
/* :pswitch_1 */
try { // :try_start_1
v10 = this.mSlaService;
v9 = (( com.xiaomi.NetworkBoost.slaservice.SLAService ) v10 ).setDoubleWifiWhiteList ( v9, v5, v6, v9 ); // invoke-virtual {v10, v9, v5, v6, v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z
if ( v9 != null) { // if-eqz v9, :cond_8
/* .line 2727 */
v9 = this.mSlaService;
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v9 ).setDWUidToSlad ( ); // invoke-virtual {v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDWUidToSlad()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 2728 */
/* nop */
/* .line 2745 */
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 2728 */
/* .line 2730 */
} // :cond_8
/* nop */
/* .line 2745 */
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 2730 */
/* .line 2719 */
/* :pswitch_2 */
try { // :try_start_2
v10 = this.mSlaService;
int v11 = 2; // const/4 v11, 0x2
v9 = (( com.xiaomi.NetworkBoost.slaservice.SLAService ) v10 ).setDoubleWifiWhiteList ( v11, v5, v6, v9 ); // invoke-virtual {v10, v11, v5, v6, v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDoubleWifiWhiteList(ILjava/lang/String;Ljava/lang/String;Z)Z
if ( v9 != null) { // if-eqz v9, :cond_9
/* .line 2720 */
v9 = this.mSlaService;
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v9 ).setDWUidToSlad ( ); // invoke-virtual {v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setDWUidToSlad()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 2721 */
/* nop */
/* .line 2745 */
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 2721 */
/* .line 2723 */
} // :cond_9
/* nop */
/* .line 2745 */
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 2723 */
/* .line 2713 */
/* :pswitch_3 */
try { // :try_start_3
v9 = this.mSlaveWifiManager;
v4 = (( android.net.wifi.SlaveWifiManager ) v9 ).isSlaveWifiEnabled ( ); // invoke-virtual {v9}, Landroid/net/wifi/SlaveWifiManager;->isSlaveWifiEnabled()Z
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 2714 */
/* nop */
/* .line 2745 */
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 2714 */
/* .line 2716 */
} // :cond_a
/* nop */
/* .line 2745 */
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 2716 */
/* .line 2745 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 2740 */
/* :catch_0 */
/* move-exception v9 */
/* .line 2741 */
/* .local v9, "e":Ljava/lang/Exception; */
try { // :try_start_4
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "isSlaveWifiEnabledAndOthersOpt clearCallingIdentity uid = "; // const-string v11, "isSlaveWifiEnabledAndOthersOpt clearCallingIdentity uid = "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v7, v8 ); // invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v10 );
/* .line 2742 */
(( java.lang.Exception ) v9 ).printStackTrace ( ); // invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 2743 */
/* nop */
/* .line 2745 */
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 2743 */
/* .line 2745 */
} // .end local v9 # "e":Ljava/lang/Exception;
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 2746 */
/* throw v4 */
/* .line 2748 */
} // :goto_1
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean isSupportDualCelluarData ( ) {
/* .locals 2 */
/* .line 2614 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "isSupportDualCelluarData"; // const-string v1, "isSupportDualCelluarData"
android.util.Log .i ( v0,v1 );
/* .line 2616 */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).cellularNetworkSDKPermissionCheck ( v1 ); // invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->cellularNetworkSDKPermissionCheck(Ljava/lang/String;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 2617 */
/* .line 2619 */
} // :cond_0
v0 = this.mDualCelluarDataService;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2620 */
v0 = (( com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService ) v0 ).supportDualCelluarData ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->supportDualCelluarData()Z
/* .line 2622 */
} // :cond_1
} // .end method
public Boolean isSupportDualWifi ( ) {
/* .locals 2 */
/* .line 531 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "isSupportDualWifi"; // const-string v1, "isSupportDualWifi"
android.util.Log .i ( v0,v1 );
/* .line 533 */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v1 ); // invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 534 */
/* .line 536 */
} // :cond_0
v0 = this.mSlaveWifiManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 537 */
v0 = (( android.net.wifi.SlaveWifiManager ) v0 ).supportDualWifi ( ); // invoke-virtual {v0}, Landroid/net/wifi/SlaveWifiManager;->supportDualWifi()Z
/* .line 539 */
} // :cond_1
} // .end method
public Boolean isSupportMediaPlayerPolicy ( ) {
/* .locals 2 */
/* .line 2626 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "isSupportMediaPlayerPolicy"; // const-string v1, "isSupportMediaPlayerPolicy"
android.util.Log .i ( v0,v1 );
/* .line 2628 */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).cellularNetworkSDKPermissionCheck ( v1 ); // invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->cellularNetworkSDKPermissionCheck(Ljava/lang/String;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 2629 */
/* .line 2631 */
} // :cond_0
v0 = this.mDualCelluarDataService;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2632 */
v0 = (( com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService ) v0 ).supportMediaPlayerPolicy ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->supportMediaPlayerPolicy()Z
/* .line 2634 */
} // :cond_1
} // .end method
public Boolean networkSDKPermissionCheck ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "callingName" # Ljava/lang/String; */
/* .line 2224 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 2225 */
/* .local v0, "uid":I */
v1 = /* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSystemProcess(I)Z */
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2226 */
} // :cond_0
v1 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mListLock;
/* monitor-enter v1 */
/* .line 2227 */
try { // :try_start_0
v3 = this.mNetworkSDKUid;
v3 = java.lang.Integer .toString ( v0 );
int v4 = 0; // const/4 v4, 0x0
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 2228 */
final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "WhiteList passed: "; // const-string v6, "WhiteList passed: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v5 );
/* .line 2233 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 2234 */
v1 = this.mPermission;
(( java.util.HashMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
/* .line 2235 */
/* .local v1, "permission":Ljava/lang/String; */
/* if-nez v1, :cond_1 */
/* .line 2236 */
} // :cond_1
v3 = android.os.Binder .getCallingPid ( );
/* .line 2237 */
/* .local v3, "pid":I */
v5 = this.mContext;
v5 = (( android.content.Context ) v5 ).checkPermission ( v1, v3, v0 ); // invoke-virtual {v5, v1, v3, v0}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I
/* .line 2238 */
/* .local v5, "checkResult":I */
/* if-nez v5, :cond_2 */
/* .line 2239 */
final String v4 = "NetworkSDKService"; // const-string v4, "NetworkSDKService"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "permission granted: "; // const-string v7, "permission granted: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " "; // const-string v7, " "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v6 );
/* .line 2240 */
/* .line 2242 */
} // :cond_2
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "permission denied: "; // const-string v7, "permission denied: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " "; // const-string v7, " "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v6 );
/* .line 2243 */
/* .line 2230 */
} // .end local v1 # "permission":Ljava/lang/String;
} // .end local v3 # "pid":I
} // .end local v5 # "checkResult":I
} // :cond_3
try { // :try_start_1
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "WhiteList denied: "; // const-string v5, "WhiteList denied: "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 2231 */
/* monitor-exit v1 */
/* .line 2233 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
public void onCreate ( ) {
/* .locals 5 */
/* .line 401 */
final String v0 = "Got a null instance of WifiScanner!"; // const-string v0, "Got a null instance of WifiScanner!"
final String v1 = "onCreate "; // const-string v1, "onCreate "
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
android.util.Log .i ( v2,v1 );
/* .line 403 */
/* new-instance v1, Landroid/os/HandlerThread; */
final String v3 = "NetworkSDKServiceHandler"; // const-string v3, "NetworkSDKServiceHandler"
/* invoke-direct {v1, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v1;
/* .line 404 */
(( android.os.HandlerThread ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V
/* .line 405 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler; */
v3 = this.mHandlerThread;
(( android.os.HandlerThread ) v3 ).getLooper ( ); // invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$InternalHandler;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 406 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler; */
v3 = this.mHandlerThread;
(( android.os.HandlerThread ) v3 ).getLooper ( ); // invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$CallbackHandler;-><init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Looper;)V */
this.mCallbackHandler = v1;
/* .line 408 */
v1 = this.mHandler;
/* const/16 v3, 0x67 */
(( android.os.Handler ) v1 ).sendEmptyMessage ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 412 */
try { // :try_start_0
v1 = this.mContext;
/* const-string/jumbo v3, "wifi" */
(( android.content.Context ) v1 ).getSystemService ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/net/wifi/WifiManager; */
this.mWifiManager = v1;
/* .line 413 */
v1 = this.mContext;
final String v3 = "SlaveWifiService"; // const-string v3, "SlaveWifiService"
(( android.content.Context ) v1 ).getSystemService ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/net/wifi/SlaveWifiManager; */
this.mSlaveWifiManager = v1;
/* .line 414 */
v1 = this.mContext;
final String v3 = "MiuiWifiService"; // const-string v3, "MiuiWifiService"
(( android.content.Context ) v1 ).getSystemService ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/net/wifi/MiuiWifiManager; */
this.mMiuiWifiManager = v1;
/* .line 416 */
/* sget-boolean v1, Lmiui/util/DeviceLevel;->IS_MIUI_MIDDLE_VERSION:Z */
/* if-nez v1, :cond_0 */
/* .line 417 */
final String v1 = "is not MIUI Middle or is not port MIUI Middle, start DualCelluarDataService"; // const-string v1, "is not MIUI Middle or is not port MIUI Middle, start DualCelluarDataService"
android.util.Log .d ( v2,v1 );
/* .line 418 */
v1 = this.mContext;
com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService .getInstance ( v1 );
this.mDualCelluarDataService = v1;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 422 */
} // :cond_0
/* .line 420 */
/* :catch_0 */
/* move-exception v1 */
/* .line 421 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getSystemService Exception:"; // const-string v4, "getSystemService Exception:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 425 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
try { // :try_start_1
v1 = this.mContext;
/* const-class v3, Landroid/net/wifi/WifiScanner; */
(( android.content.Context ) v1 ).getSystemService ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v1, Landroid/net/wifi/WifiScanner; */
java.util.Objects .requireNonNull ( v1,v0 );
/* check-cast v1, Landroid/net/wifi/WifiScanner; */
this.mScanner = v1;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 429 */
/* .line 427 */
/* :catch_1 */
/* move-exception v1 */
/* .line 428 */
/* .restart local v1 # "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v0 );
/* .line 430 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isConnectSlaveAp:Z */
/* .line 431 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->getCurrentDSDAState()Z */
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mDsdaCapability:Z */
/* .line 433 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mCallbacks = v0;
/* .line 434 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mWlanQoECallbacks = v0;
/* .line 435 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mNetSelectCallbacks = v0;
/* .line 437 */
v0 = this.mContext;
com.xiaomi.NetworkBoost.StatusManager .getInstance ( v0 );
this.mStatusManager = v0;
/* .line 438 */
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).getAvailableIfaces ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getAvailableIfaces()Ljava/util/List;
this.mAvailableIfaces = v0;
/* .line 439 */
v0 = this.mStatusManager;
v1 = this.networkInterfaceListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerNetworkInterfaceListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V
/* .line 440 */
v0 = this.mStatusManager;
v1 = this.mIScreenStatusListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerScreenStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V
/* .line 441 */
v0 = this.mStatusManager;
v1 = this.mNetworkPriorityListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerNetworkPriorityListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerNetworkPriorityListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;)V
/* .line 442 */
v0 = this.mStatusManager;
v1 = this.mIAppStatusListenr;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).registerAppStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerAppStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;)V
/* .line 443 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registDualWifiStateBroadcastReceiver()V */
/* .line 444 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initWhiteList()V */
/* .line 445 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initCellularWhiteList()V */
/* .line 446 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initNetworkSDKCloudObserver()V */
/* .line 447 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initMibridgeCloudObserver()V */
/* .line 448 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initCellularNetworkSDKCloudObserver()V */
/* .line 449 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initBroadcastReceiver()V */
/* .line 450 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initDualDataBroadcastReceiver()V */
/* .line 451 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initPermission()V */
/* .line 452 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->initAppTrafficSessionAndSimCardHelper()V */
/* .line 453 */
v0 = this.mHandler;
/* const/16 v1, 0x6a */
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 454 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registerQEEChangeObserver()V */
/* .line 455 */
return;
} // .end method
public void onDestroy ( ) {
/* .locals 2 */
/* .line 458 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "onDestroy "; // const-string v1, "onDestroy "
android.util.Log .i ( v0,v1 );
/* .line 460 */
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mMiNetService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 462 */
try { // :try_start_0
/* .line 463 */
v0 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v0 ).kill ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V
/* .line 464 */
v0 = this.mWlanQoECallbacks;
(( android.os.RemoteCallbackList ) v0 ).kill ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V
/* .line 465 */
v0 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v0 ).kill ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V
/* .line 466 */
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mMiNetService;
v1 = this.mDeathRecipient;
/* .line 467 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->clearAppWifiSelectionMonitor()V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 470 */
/* .line 468 */
/* :catch_0 */
/* move-exception v0 */
/* .line 469 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 474 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
v0 = this.mDualCelluarDataService;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 475 */
com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService .destroyInstance ( );
/* .line 476 */
int v0 = 0; // const/4 v0, 0x0
this.mDualCelluarDataService = v0;
/* .line 479 */
} // :cond_1
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->closeAppTrafficSession()V */
/* .line 480 */
v0 = this.mHandlerThread;
(( android.os.HandlerThread ) v0 ).quit ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
/* .line 481 */
v0 = this.mStatusManager;
v1 = this.mIScreenStatusListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterScreenStatusListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V
/* .line 482 */
v0 = this.mStatusManager;
v1 = this.networkInterfaceListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterNetworkInterfaceListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V
/* .line 483 */
v0 = this.mStatusManager;
v1 = this.mNetworkPriorityListener;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).unregisterNetworkPriorityListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterNetworkPriorityListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;)V
/* .line 485 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterQEEChangeObserver()V */
/* .line 486 */
return;
} // .end method
void onDsdaStateChanged ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "result" # Z */
/* .line 1377 */
/* new-instance v0, Landroid/os/Message; */
/* invoke-direct {v0}, Landroid/os/Message;-><init>()V */
/* .line 1378 */
/* .local v0, "msg":Landroid/os/Message; */
/* const/16 v1, 0x3f4 */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 1379 */
java.lang.Boolean .valueOf ( p1 );
this.obj = v1;
/* .line 1380 */
v1 = this.mCallbackHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1381 */
return;
} // .end method
void onSetSlaveWifiResult ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "result" # Z */
/* .line 1349 */
/* new-instance v0, Landroid/os/Message; */
/* invoke-direct {v0}, Landroid/os/Message;-><init>()V */
/* .line 1350 */
/* .local v0, "msg":Landroid/os/Message; */
int v1 = 1; // const/4 v1, 0x1
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 1351 */
java.lang.Boolean .valueOf ( p1 );
this.obj = v1;
/* .line 1352 */
v1 = this.mCallbackHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1353 */
return;
} // .end method
void onSlaveWifiConnected ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "result" # Z */
/* .line 1356 */
/* new-instance v0, Landroid/os/Message; */
/* invoke-direct {v0}, Landroid/os/Message;-><init>()V */
/* .line 1357 */
/* .local v0, "msg":Landroid/os/Message; */
int v1 = 2; // const/4 v1, 0x2
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 1358 */
java.lang.Boolean .valueOf ( p1 );
this.obj = v1;
/* .line 1359 */
v1 = this.mCallbackHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1360 */
return;
} // .end method
void onSlaveWifiDisconnected ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "result" # Z */
/* .line 1363 */
/* new-instance v0, Landroid/os/Message; */
/* invoke-direct {v0}, Landroid/os/Message;-><init>()V */
/* .line 1364 */
/* .local v0, "msg":Landroid/os/Message; */
int v1 = 3; // const/4 v1, 0x3
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 1365 */
java.lang.Boolean .valueOf ( p1 );
this.obj = v1;
/* .line 1366 */
v1 = this.mCallbackHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1367 */
return;
} // .end method
void onSlaveWifiEnable ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "result" # Z */
/* .line 1370 */
/* new-instance v0, Landroid/os/Message; */
/* invoke-direct {v0}, Landroid/os/Message;-><init>()V */
/* .line 1371 */
/* .local v0, "msg":Landroid/os/Message; */
/* const/16 v1, 0x3f3 */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 1372 */
java.lang.Boolean .valueOf ( p1 );
this.obj = v1;
/* .line 1373 */
v1 = this.mCallbackHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1374 */
return;
} // .end method
public Boolean registerCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiNetworkCallback p0 ) {
/* .locals 4 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* .line 619 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 620 */
v0 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v0 ).register ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
/* .line 623 */
try { // :try_start_0
v0 = this.mStatusManager;
(( com.xiaomi.NetworkBoost.StatusManager ) v0 ).getNetworkPriorityInfo ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getNetworkPriorityInfo()Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;
/* .line 624 */
/* .local v0, "info":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo; */
/* iget v1, v0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->priorityMode:I */
/* iget v2, v0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->trafficPolicy:I */
/* iget v3, v0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->thermalLevel:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 628 */
} // .end local v0 # "info":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;
/* .line 626 */
/* :catch_0 */
/* move-exception v0 */
/* .line 629 */
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 631 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean registerNetLinkCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .param p2, "interval" # I */
/* .line 725 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 726 */
/* .local v0, "uid":I */
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).registerNetLinkCallback ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registerNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;II)Z
} // .end method
public Boolean registerNetLinkCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback p0, Integer p1, Integer p2 ) {
/* .locals 11 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .param p2, "interval" # I */
/* .param p3, "uid" # I */
/* .line 730 */
int v0 = 0; // const/4 v0, 0x0
/* if-ltz p2, :cond_8 */
int v1 = 5; // const/4 v1, 0x5
/* if-le p2, v1, :cond_0 */
/* goto/16 :goto_1 */
/* .line 733 */
} // :cond_0
final String v1 = "registerNetLinkCallback"; // const-string v1, "registerNetLinkCallback"
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v1 ); // invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
/* if-nez v1, :cond_1 */
/* .line 734 */
/* .line 736 */
} // :cond_1
p2 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKUtils .intervalTransformation ( p2 );
/* .line 738 */
if ( p1 != null) { // if-eqz p1, :cond_7
/* .line 739 */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 740 */
try { // :try_start_0
/* .line 741 */
/* .local v0, "key":Landroid/os/IBinder; */
v2 = v2 = this.mAppStatsPollMillis;
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 742 */
/* monitor-exit v1 */
/* .line 744 */
} // :cond_2
/* int-to-long v4, p2 */
/* iget-wide v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J */
/* cmp-long v2, v4, v6 */
/* if-lez v2, :cond_3 */
/* const-wide/16 v4, -0x1 */
/* cmp-long v2, v6, v4 */
/* if-nez v2, :cond_4 */
/* .line 745 */
} // :cond_3
/* int-to-long v4, p2 */
/* iput-wide v4, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J */
/* .line 747 */
} // :cond_4
v2 = this.mWlanQoECallbacks;
v2 = (( android.os.RemoteCallbackList ) v2 ).getRegisteredCallbackCount ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I
/* .line 748 */
/* .local v2, "count":I */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* .line 749 */
/* .local v4, "curSystemTime":J */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_0
/* if-ge v6, v2, :cond_5 */
/* .line 750 */
v7 = this.mWlanQoECallbacks;
/* .line 751 */
(( android.os.RemoteCallbackList ) v7 ).getRegisteredCallbackItem ( v6 ); // invoke-virtual {v7, v6}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackItem(I)Landroid/os/IInterface;
/* check-cast v7, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .line 752 */
/* .local v7, "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .line 753 */
/* .local v8, "callBackKey":Landroid/os/IBinder; */
v9 = this.mAppStatsPollMillis;
java.lang.Long .valueOf ( v4,v5 );
/* .line 754 */
final String v9 = "NetworkSDKService"; // const-string v9, "NetworkSDKService"
final String v10 = "registerNetLinkCallback: Reset callBacks millis"; // const-string v10, "registerNetLinkCallback: Reset callBacks millis"
android.util.Log .d ( v9,v10 );
/* .line 749 */
/* nop */
} // .end local v7 # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
} // .end local v8 # "callBackKey":Landroid/os/IBinder;
/* add-int/lit8 v6, v6, 0x1 */
/* .line 757 */
} // .end local v6 # "i":I
} // :cond_5
v6 = this.mWlanQoECallbacks;
(( android.os.RemoteCallbackList ) v6 ).register ( p1 ); // invoke-virtual {v6, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
/* .line 758 */
v6 = this.mAppStatsPollInterval;
/* int-to-long v7, p2 */
java.lang.Long .valueOf ( v7,v8 );
/* .line 759 */
v6 = this.mAppStatsPollMillis;
java.lang.Long .valueOf ( v4,v5 );
/* .line 760 */
v6 = this.mAppCallBackMapToUid;
java.lang.Integer .valueOf ( p3 );
/* .line 762 */
/* iget-boolean v6, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsOpenWifiLinkPoll:Z */
/* if-nez v6, :cond_6 */
/* .line 763 */
v6 = this.mCallbackHandler;
/* const/16 v7, 0x3f1 */
(( android.os.Handler ) v6 ).removeMessages ( v7 ); // invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V
/* .line 764 */
v6 = this.mCallbackHandler;
/* .line 765 */
(( android.os.Handler ) v6 ).obtainMessage ( v7 ); // invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* iget-wide v8, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J */
/* .line 764 */
(( android.os.Handler ) v6 ).sendMessageDelayed ( v7, v8, v9 ); // invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 769 */
} // :cond_6
/* iput-boolean v3, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsOpenWifiLinkPoll:Z */
/* .line 770 */
} // .end local v0 # "key":Landroid/os/IBinder;
} // .end local v2 # "count":I
} // .end local v4 # "curSystemTime":J
/* monitor-exit v1 */
/* .line 772 */
/* .line 770 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
/* .line 774 */
} // :cond_7
/* .line 731 */
} // :cond_8
} // :goto_1
} // .end method
public void reportBssidScore ( java.util.Map p0 ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1899 */
/* .local p1, "bssidScores":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
final String v0 = "reportBssidScore"; // const-string v0, "reportBssidScore"
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 1900 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "reportBssidScore No permission"; // const-string v1, "reportBssidScore No permission"
android.util.Log .e ( v0,v1 );
/* .line 1901 */
return;
/* .line 1904 */
} // :cond_0
v0 = if ( p1 != null) { // if-eqz p1, :cond_5
/* if-gtz v0, :cond_1 */
/* .line 1907 */
} // :cond_1
final String v0 = ""; // const-string v0, ""
/* .line 1908 */
/* .local v0, "objBssid":Ljava/lang/String; */
int v1 = -1; // const/4 v1, -0x1
/* .line 1909 */
/* .local v1, "score":I */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Ljava/util/Map$Entry; */
/* .line 1910 */
/* .local v3, "entry":Ljava/util/Map$Entry; */
/* check-cast v4, Ljava/lang/String; */
java.lang.Integer .valueOf ( v4 );
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 1911 */
/* .local v4, "tmpScore":I */
/* if-ltz v4, :cond_2 */
/* if-le v4, v1, :cond_2 */
/* .line 1912 */
/* move v1, v4 */
/* .line 1913 */
/* move-object v0, v5 */
/* check-cast v0, Ljava/lang/String; */
/* .line 1915 */
} // .end local v3 # "entry":Ljava/util/Map$Entry;
} // .end local v4 # "tmpScore":I
} // :cond_2
/* .line 1917 */
} // :cond_3
/* if-ltz v1, :cond_4 */
/* .line 1918 */
v2 = /* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->connectionWithBssid(Ljava/lang/String;)Z */
/* if-nez v2, :cond_4 */
/* .line 1919 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->broadcastPrimaryConnectingFailed()V */
/* .line 1922 */
} // :cond_4
return;
/* .line 1905 */
} // .end local v0 # "objBssid":Ljava/lang/String;
} // .end local v1 # "score":I
} // :cond_5
} // :goto_1
return;
} // .end method
public java.util.Map requestAppTrafficStatistics ( Integer p0, Long p1, Long p2 ) {
/* .locals 8 */
/* .param p1, "type" # I */
/* .param p2, "startTime" # J */
/* .param p4, "endTime" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(IJJ)", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 832 */
v7 = android.os.Binder .getCallingUid ( );
/* .line 833 */
/* .local v7, "uid":I */
/* move-object v0, p0 */
/* move v1, p1 */
/* move-wide v2, p2 */
/* move-wide v4, p4 */
/* move v6, v7 */
/* invoke-virtual/range {v0 ..v6}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->requestAppTrafficStatistics(IJJI)Ljava/util/Map; */
} // .end method
public java.util.Map requestAppTrafficStatistics ( Integer p0, Long p1, Long p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .param p2, "startTime" # J */
/* .param p4, "endTime" # J */
/* .param p6, "uid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(IJJI)", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 837 */
int v0 = 0; // const/4 v0, 0x0
/* .line 840 */
/* .local v0, "appTrafficStatistics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
final String v1 = "requestAppTrafficStatistics"; // const-string v1, "requestAppTrafficStatistics"
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v1 ); // invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
final String v2 = "code"; // const-string v2, "code"
/* if-nez v1, :cond_0 */
/* .line 841 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* move-object v0, v1 */
/* .line 842 */
final String v1 = "1002"; // const-string v1, "1002"
/* .line 843 */
final String v1 = "message"; // const-string v1, "message"
final String v2 = "No permission"; // const-string v2, "No permission"
/* .line 844 */
/* .line 847 */
} // :cond_0
/* invoke-direct/range {p0 ..p6}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->buildAppTrafficStatistics(IJJI)Ljava/util/Map; */
/* .line 849 */
/* if-nez v0, :cond_1 */
/* .line 850 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* move-object v0, v1 */
/* .line 851 */
final String v1 = "1001"; // const-string v1, "1001"
/* .line 853 */
} // :cond_1
final String v1 = "1000"; // const-string v1, "1000"
/* .line 856 */
} // :goto_0
} // .end method
public Boolean resumeBackgroundScan ( ) {
/* .locals 2 */
/* .line 686 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "resumeBackgroundScan"; // const-string v1, "resumeBackgroundScan"
android.util.Log .i ( v0,v1 );
/* .line 688 */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v1 ); // invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 689 */
/* .line 691 */
} // :cond_0
v0 = this.mMiuiWifiManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 692 */
int v1 = 1; // const/4 v1, 0x1
(( android.net.wifi.MiuiWifiManager ) v0 ).setLatencyLevel ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/wifi/MiuiWifiManager;->setLatencyLevel(I)V
/* .line 693 */
/* .line 695 */
} // :cond_1
} // .end method
public Boolean resumeWifiPowerSave ( ) {
/* .locals 2 */
/* .line 712 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "resumeWifiPowerSave"; // const-string v1, "resumeWifiPowerSave"
android.util.Log .i ( v0,v1 );
/* .line 714 */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v1 ); // invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 715 */
/* .line 717 */
} // :cond_0
v0 = this.mMiuiWifiManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 718 */
int v1 = 1; // const/4 v1, 0x1
(( android.net.wifi.MiuiWifiManager ) v0 ).enablePowerSave ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/wifi/MiuiWifiManager;->enablePowerSave(Z)V
/* .line 719 */
/* .line 721 */
} // :cond_1
} // .end method
public Boolean setDualCelluarDataEnable ( Boolean p0 ) {
/* .locals 7 */
/* .param p1, "enable" # Z */
/* .line 2650 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
/* const-string/jumbo v1, "setDualCelluarDataEnable" */
android.util.Log .i ( v0,v1 );
/* .line 2651 */
int v2 = 0; // const/4 v2, 0x0
/* .line 2653 */
/* .local v2, "result":Z */
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).cellularNetworkSDKPermissionCheck ( v1 ); // invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->cellularNetworkSDKPermissionCheck(Ljava/lang/String;)Z
/* if-nez v1, :cond_0 */
/* .line 2654 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2656 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v3 */
/* .line 2658 */
/* .local v3, "callingId":J */
try { // :try_start_0
v1 = this.mDualCelluarDataService;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2659 */
v0 = (( com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService ) v1 ).setDualCelluarDataEnable ( p1 ); // invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->setDualCelluarDataEnable(Z)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v0 */
/* .line 2664 */
} // :cond_1
/* nop */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v3,v4 );
/* .line 2665 */
/* .line 2664 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 2661 */
/* :catch_0 */
/* move-exception v1 */
/* .line 2662 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_1
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "setDualCelluarDataEnable error " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v5 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 2664 */
/* nop */
} // .end local v1 # "e":Ljava/lang/Exception;
/* .line 2666 */
} // :goto_1
/* .line 2664 */
} // :goto_2
android.os.Binder .restoreCallingIdentity ( v3,v4 );
/* .line 2665 */
/* throw v0 */
} // .end method
public Boolean setSlaveWifiEnabled ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 543 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setSlaveWifiEnabled: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
android.util.Log .i ( v1,v0 );
/* .line 545 */
/* const-string/jumbo v0, "setSlaveWifiEnabled" */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 546 */
int v0 = 0; // const/4 v0, 0x0
/* .line 549 */
} // :cond_0
android.os.Message .obtain ( );
/* .line 550 */
/* .local v0, "msg":Landroid/os/Message; */
/* const/16 v1, 0x64 */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 551 */
java.lang.Boolean .valueOf ( p1 );
this.obj = v1;
/* .line 552 */
v1 = android.os.Binder .getCallingUid ( );
/* iput v1, v0, Landroid/os/Message;->arg1:I */
/* .line 553 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 554 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
public Boolean setSockPrio ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "fd" # I */
/* .param p2, "prio" # I */
/* .line 503 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setSockPrio " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ","; // const-string v1, ","
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
android.util.Log .i ( v2,v0 );
/* .line 505 */
/* const-string/jumbo v0, "setSockPrio" */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 506 */
int v0 = 0; // const/4 v0, 0x0
/* .line 509 */
} // :cond_0
try { // :try_start_0
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mMiNetService;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const/16 v3, 0x3e9 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 512 */
/* .line 510 */
/* :catch_0 */
/* move-exception v0 */
/* .line 511 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Exception:"; // const-string v3, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v1 );
/* .line 513 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean setTCPCongestion ( Integer p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "fd" # I */
/* .param p2, "cc" # Ljava/lang/String; */
/* .line 517 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setTCPCongestion " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ","; // const-string v1, ","
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
android.util.Log .i ( v2,v0 );
/* .line 519 */
/* const-string/jumbo v0, "setTCPCongestion" */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 520 */
int v0 = 0; // const/4 v0, 0x0
/* .line 523 */
} // :cond_0
try { // :try_start_0
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mMiNetService;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const/16 v3, 0x3ea */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 526 */
/* .line 524 */
/* :catch_0 */
/* move-exception v0 */
/* .line 525 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Exception:"; // const-string v3, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v1 );
/* .line 527 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean setTrafficTransInterface ( Integer p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "fd" # I */
/* .param p2, "bindInterface" # Ljava/lang/String; */
/* .line 605 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setTrafficTransInterface " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ","; // const-string v1, ","
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkSDKService"; // const-string v2, "NetworkSDKService"
android.util.Log .i ( v2,v0 );
/* .line 607 */
/* const-string/jumbo v0, "setTrafficTransInterface" */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 608 */
int v0 = 0; // const/4 v0, 0x0
/* .line 611 */
} // :cond_0
try { // :try_start_0
v0 = com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService.mMiNetService;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const/16 v3, 0x3ee */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 614 */
/* .line 612 */
/* :catch_0 */
/* move-exception v0 */
/* .line 613 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Exception:"; // const-string v3, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v1 );
/* .line 615 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean suspendBackgroundScan ( ) {
/* .locals 2 */
/* .line 673 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
/* const-string/jumbo v1, "suspendBackgroundScan" */
android.util.Log .i ( v0,v1 );
/* .line 675 */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v1 ); // invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 676 */
/* .line 678 */
} // :cond_0
v0 = this.mMiuiWifiManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 679 */
int v1 = 4; // const/4 v1, 0x4
(( android.net.wifi.MiuiWifiManager ) v0 ).setLatencyLevel ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/wifi/MiuiWifiManager;->setLatencyLevel(I)V
/* .line 680 */
int v0 = 1; // const/4 v0, 0x1
/* .line 682 */
} // :cond_1
} // .end method
public Boolean suspendWifiPowerSave ( ) {
/* .locals 2 */
/* .line 699 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
/* const-string/jumbo v1, "suspendWifiPowerSave" */
android.util.Log .i ( v0,v1 );
/* .line 701 */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v1 ); // invoke-virtual {p0, v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 702 */
/* .line 704 */
} // :cond_0
v0 = this.mMiuiWifiManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 705 */
(( android.net.wifi.MiuiWifiManager ) v0 ).enablePowerSave ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/wifi/MiuiWifiManager;->enablePowerSave(Z)V
/* .line 706 */
int v0 = 1; // const/4 v0, 0x1
/* .line 708 */
} // :cond_1
} // .end method
public void triggerWifiSelection ( ) {
/* .locals 8 */
/* .line 1853 */
/* const-string/jumbo v0, "triggerWifiSelection" */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).networkSDKPermissionCheck ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 1854 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
/* const-string/jumbo v1, "triggerWifiSelection No permission" */
android.util.Log .e ( v0,v1 );
/* .line 1855 */
return;
/* .line 1858 */
} // :cond_0
v0 = this.mBssidToNetworkId;
/* if-nez v0, :cond_1 */
/* .line 1859 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mBssidToNetworkId = v0;
/* .line 1861 */
} // :cond_1
/* .line 1863 */
} // :goto_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1864 */
/* .local v0, "netAndBssidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = this.mMiuiWifiManager;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1865 */
(( android.net.wifi.MiuiWifiManager ) v1 ).netSDKGetAvailableNetworkIdAndBssid ( ); // invoke-virtual {v1}, Landroid/net/wifi/MiuiWifiManager;->netSDKGetAvailableNetworkIdAndBssid()Ljava/util/List;
/* .line 1866 */
/* if-nez v0, :cond_2 */
/* .line 1867 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* move-object v0, v1 */
/* .line 1870 */
} // :cond_2
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 1871 */
/* .local v1, "retBssidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = } // :goto_1
/* if-ge v2, v3, :cond_4 */
/* .line 1872 */
/* check-cast v3, Ljava/lang/String; */
final String v4 = ","; // const-string v4, ","
(( java.lang.String ) v3 ).split ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1873 */
/* .local v3, "netwrokIdWithBssid":[Ljava/lang/String; */
/* array-length v4, v3 */
int v5 = 2; // const/4 v5, 0x2
/* if-ne v4, v5, :cond_3 */
/* .line 1874 */
v4 = this.mBssidToNetworkId;
int v5 = 1; // const/4 v5, 0x1
/* aget-object v6, v3, v5 */
int v7 = 0; // const/4 v7, 0x0
/* aget-object v7, v3, v7 */
/* .line 1875 */
/* aget-object v4, v3, v5 */
/* .line 1871 */
} // .end local v3 # "netwrokIdWithBssid":[Ljava/lang/String;
} // :cond_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1879 */
} // .end local v2 # "i":I
} // :cond_4
v2 = this.mLock;
/* monitor-enter v2 */
/* .line 1880 */
try { // :try_start_0
v3 = this.mNetSelectCallbacks;
v3 = (( android.os.RemoteCallbackList ) v3 ).beginBroadcast ( ); // invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1882 */
/* .local v3, "N":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_2
/* if-ge v4, v3, :cond_5 */
/* .line 1884 */
try { // :try_start_1
v5 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v5 ).getBroadcastItem ( v4 ); // invoke-virtual {v5, v4}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v5, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback; */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1887 */
/* .line 1893 */
} // .end local v4 # "i":I
/* :catchall_0 */
/* move-exception v4 */
/* .line 1889 */
/* :catch_0 */
/* move-exception v4 */
/* .line 1885 */
/* .restart local v4 # "i":I */
/* :catch_1 */
/* move-exception v5 */
/* .line 1886 */
/* .local v5, "e":Landroid/os/RemoteException; */
try { // :try_start_2
final String v6 = "NetworkSDKService"; // const-string v6, "NetworkSDKService"
final String v7 = "RemoteException at triggerWifiSelection()"; // const-string v7, "RemoteException at triggerWifiSelection()"
android.util.Log .e ( v6,v7 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1882 */
} // .end local v5 # "e":Landroid/os/RemoteException;
} // :goto_3
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1890 */
/* .local v4, "e":Ljava/lang/Exception; */
} // :goto_4
try { // :try_start_3
final String v5 = "NetworkSDKService"; // const-string v5, "NetworkSDKService"
(( java.lang.Exception ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .d ( v5,v6 );
/* .line 1891 */
(( java.lang.Exception ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1893 */
} // .end local v4 # "e":Ljava/lang/Exception;
try { // :try_start_4
v4 = this.mNetSelectCallbacks;
} // :goto_5
v5 = this.mNetSelectCallbacks;
(( android.os.RemoteCallbackList ) v5 ).finishBroadcast ( ); // invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1894 */
/* nop */
} // .end local v0 # "netAndBssidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v1 # "retBssidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
/* throw v4 */
/* .line 1893 */
/* .restart local v0 # "netAndBssidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .restart local v1 # "retBssidLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .restart local p0 # "this":Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
} // :cond_5
v4 = this.mNetSelectCallbacks;
} // :goto_6
(( android.os.RemoteCallbackList ) v4 ).finishBroadcast ( ); // invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1894 */
/* nop */
/* .line 1895 */
} // .end local v3 # "N":I
/* monitor-exit v2 */
/* .line 1896 */
return;
/* .line 1895 */
/* :catchall_1 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v3 */
} // .end method
public Boolean unregisterCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiNetworkCallback p0 ) {
/* .locals 1 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback; */
/* .line 636 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 637 */
v0 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v0 ).unregister ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 638 */
int v0 = 1; // const/4 v0, 0x1
/* .line 640 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean unregisterNetLinkCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback p0 ) {
/* .locals 2 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .line 779 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 780 */
/* .local v0, "uid":I */
v1 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) p0 ).unregisterNetLinkCallback ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;I)Z
} // .end method
public Boolean unregisterNetLinkCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback p0, Integer p1 ) {
/* .locals 16 */
/* .param p1, "cb" # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .param p2, "uid" # I */
/* .line 785 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* const-string/jumbo v0, "unregisterNetLinkCallback" */
v0 = (( com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService ) v1 ).networkSDKPermissionCheck ( v0 ); // invoke-virtual {v1, v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->networkSDKPermissionCheck(Ljava/lang/String;)Z
int v3 = 0; // const/4 v3, 0x0
/* if-nez v0, :cond_0 */
/* .line 786 */
/* .line 788 */
} // :cond_0
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 789 */
v4 = this.mLock;
/* monitor-enter v4 */
/* .line 790 */
try { // :try_start_0
/* invoke-interface/range {p1 ..p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;->asBinder()Landroid/os/IBinder; */
/* .line 792 */
/* .local v0, "key":Landroid/os/IBinder; */
v5 = v5 = this.mAppStatsPollMillis;
int v6 = 1; // const/4 v6, 0x1
/* if-nez v5, :cond_1 */
/* .line 793 */
/* monitor-exit v4 */
/* .line 795 */
} // :cond_1
v5 = this.mWlanQoECallbacks;
(( android.os.RemoteCallbackList ) v5 ).unregister ( v2 ); // invoke-virtual {v5, v2}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 796 */
v5 = this.mAppStatsPollInterval;
/* .line 797 */
v5 = this.mAppStatsPollMillis;
/* .line 798 */
v5 = this.mAppCallBackMapToUid;
/* .line 800 */
v5 = this.mWlanQoECallbacks;
v5 = (( android.os.RemoteCallbackList ) v5 ).getRegisteredCallbackCount ( ); // invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I
/* .line 801 */
/* .local v5, "count":I */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v7 */
/* .line 802 */
/* .local v7, "curSystemTime":J */
/* const-wide/16 v9, -0x1 */
/* .line 803 */
/* .local v9, "minInterval":J */
int v11 = 0; // const/4 v11, 0x0
/* .local v11, "i":I */
} // :goto_0
/* const-wide/16 v12, -0x1 */
/* if-ge v11, v5, :cond_4 */
/* .line 804 */
v14 = this.mWlanQoECallbacks;
/* .line 805 */
(( android.os.RemoteCallbackList ) v14 ).getRegisteredCallbackItem ( v11 ); // invoke-virtual {v14, v11}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackItem(I)Landroid/os/IInterface;
/* check-cast v14, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .line 806 */
/* .local v14, "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .line 807 */
/* .local v15, "callBackKey":Landroid/os/IBinder; */
v6 = this.mAppStatsPollMillis;
java.lang.Long .valueOf ( v7,v8 );
/* .line 808 */
/* cmp-long v3, v9, v12 */
/* if-nez v3, :cond_2 */
/* .line 809 */
v3 = this.mAppStatsPollInterval;
/* check-cast v3, Ljava/lang/Long; */
(( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
/* move-result-wide v12 */
/* move-wide v9, v12 */
/* .line 810 */
} // :cond_2
v3 = this.mAppStatsPollInterval;
/* check-cast v3, Ljava/lang/Long; */
(( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
/* move-result-wide v12 */
/* cmp-long v3, v12, v9 */
/* if-gez v3, :cond_3 */
/* .line 811 */
v3 = this.mAppStatsPollInterval;
/* check-cast v3, Ljava/lang/Long; */
(( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
/* move-result-wide v12 */
/* move-wide v9, v12 */
/* .line 803 */
} // .end local v14 # "callBack":Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
} // .end local v15 # "callBackKey":Landroid/os/IBinder;
} // :cond_3
} // :goto_1
/* add-int/lit8 v11, v11, 0x1 */
int v3 = 0; // const/4 v3, 0x0
int v6 = 1; // const/4 v6, 0x1
/* .line 814 */
} // .end local v11 # "i":I
} // :cond_4
/* cmp-long v3, v9, v12 */
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 815 */
/* iput-wide v9, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J */
/* .line 816 */
final String v3 = "NetworkSDKService"; // const-string v3, "NetworkSDKService"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v11, "unregisterNetLinkCallback: reset interval = " */
(( java.lang.StringBuilder ) v6 ).append ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v14, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J */
(( java.lang.StringBuilder ) v6 ).append ( v14, v15 ); // invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v6 );
/* .line 819 */
} // :cond_5
/* if-nez v5, :cond_6 */
/* .line 820 */
int v3 = 0; // const/4 v3, 0x0
/* iput-boolean v3, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mIsOpenWifiLinkPoll:Z */
/* .line 821 */
/* iput-wide v12, v1, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->mWifiLinkStatsPollMillis:J */
/* .line 823 */
} // .end local v0 # "key":Landroid/os/IBinder;
} // .end local v5 # "count":I
} // .end local v7 # "curSystemTime":J
} // .end local v9 # "minInterval":J
} // :cond_6
/* monitor-exit v4 */
/* .line 825 */
int v0 = 1; // const/4 v0, 0x1
/* .line 823 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
/* .line 827 */
} // :cond_7
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void videoPolicyCallBackNotice ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .param p2, "duration" # I */
/* .param p3, "length" # I */
/* .line 1402 */
int v0 = 1; // const/4 v0, 0x1
/* if-eq p1, v0, :cond_0 */
int v0 = 2; // const/4 v0, 0x2
/* if-eq p1, v0, :cond_0 */
/* .line 1403 */
return;
/* .line 1405 */
} // :cond_0
v0 = this.mCallbackHandler;
/* .line 1406 */
java.lang.Integer .valueOf ( p3 );
/* const/16 v2, 0x3f5 */
(( android.os.Handler ) v0 ).obtainMessage ( v2, p1, p2, v1 ); // invoke-virtual {v0, v2, p1, p2, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
/* .line 1405 */
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1407 */
return;
} // .end method
