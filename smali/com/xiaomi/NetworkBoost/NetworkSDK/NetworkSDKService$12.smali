.class Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$12;
.super Landroid/database/ContentObserver;
.source "NetworkSDKService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registerQEEChangeObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 2775
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$12;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfChange"    # Z

    .line 2778
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService$12;->this$0:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->-$$Nest$fgetmContext(Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;)Landroid/content/Context;

    move-result-object v0

    .line 2779
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2778
    const-string v1, "cloud_qee_enable"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "off"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2780
    .local v0, "isQEENeedOff":Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "QEE cloud change, isQEENeedOff: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NetworkSDKService"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2782
    const-string/jumbo v2, "sys.network.cronet.qee"

    if-eqz v0, :cond_0

    .line 2783
    :try_start_0
    invoke-static {v2, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2785
    :cond_0
    const-string v1, "on"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2789
    :goto_0
    goto :goto_1

    .line 2787
    :catch_0
    move-exception v1

    .line 2788
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "QEE setprop faild"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2790
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method
