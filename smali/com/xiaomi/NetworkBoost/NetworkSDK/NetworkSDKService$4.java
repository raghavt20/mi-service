class com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$4 implements android.net.wifi.WifiScanner$ScanListener {
	 /* .source "NetworkSDKService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.NetworkSDK.NetworkSDKService$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService; */
/* .line 299 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onFailure ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "reason" # I */
/* .param p2, "description" # Ljava/lang/String; */
/* .line 330 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ScanListener onFailure: "; // const-string v1, "ScanListener onFailure: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ": "; // const-string v1, ": "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
android.util.Log .e ( v1,v0 );
/* .line 331 */
return;
} // .end method
public void onFullResult ( android.net.wifi.ScanResult p0 ) {
/* .locals 2 */
/* .param p1, "fullScanResult" # Landroid/net/wifi/ScanResult; */
/* .line 319 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ScanListener onFullResult: "; // const-string v1, "ScanListener onFullResult: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
android.util.Log .e ( v1,v0 );
/* .line 320 */
return;
} // .end method
public void onPeriodChanged ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "periodInMs" # I */
/* .line 303 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ScanListener onPeriodChanged: "; // const-string v1, "ScanListener onPeriodChanged: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
android.util.Log .e ( v1,v0 );
/* .line 304 */
return;
} // .end method
public void onResults ( android.net.wifi.WifiScanner$ScanData[] p0 ) {
/* .locals 2 */
/* .param p1, "results" # [Landroid/net/wifi/WifiScanner$ScanData; */
/* .line 313 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ScanListener onResults: "; // const-string v1, "ScanListener onResults: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkSDKService"; // const-string v1, "NetworkSDKService"
android.util.Log .e ( v1,v0 );
/* .line 314 */
return;
} // .end method
public void onSuccess ( ) {
/* .locals 2 */
/* .line 325 */
final String v0 = "NetworkSDKService"; // const-string v0, "NetworkSDKService"
final String v1 = "ScanListener onSuccess: "; // const-string v1, "ScanListener onSuccess: "
android.util.Log .e ( v0,v1 );
/* .line 326 */
return;
} // .end method
