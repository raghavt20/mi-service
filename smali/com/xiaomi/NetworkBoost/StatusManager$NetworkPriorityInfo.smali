.class public Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;
.super Ljava/lang/Object;
.source "StatusManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/StatusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NetworkPriorityInfo"
.end annotation


# instance fields
.field public priorityMode:I

.field public thermalLevel:I

.field public trafficPolicy:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "_priorityMode"    # I
    .param p2, "_trafficPolicy"    # I
    .param p3, "_thermalLevel"    # I

    .line 453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 454
    iput p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->priorityMode:I

    .line 455
    iput p2, p0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->trafficPolicy:I

    .line 456
    iput p3, p0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->thermalLevel:I

    .line 457
    return-void
.end method
