.class public interface abstract Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
.super Ljava/lang/Object;
.source "IAIDLMiuiNetQoECallback.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback$Stub;
    }
.end annotation


# virtual methods
.method public abstract masterQoECallBack(Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract slaveQoECallBack(Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
