.class public Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;
.super Ljava/lang/Object;
.source "DualCelluarDataService.java"


# static fields
.field private static final APPLICATION_DUAL_MOBILE_DATA:Ljava/lang/String; = "application_dual_mobile_data"

.field private static final DUAL_DATA_STATE_OFF:I = 0x0

.field private static final DUAL_DATA_STATE_ON:I = 0x1

.field public static final DUAL_MOBILE_DATA:Ljava/lang/String; = "dual_mobile_data"

.field private static final ERROR_CODE_DEVICE_CAPABILITY:I = 0x64

.field private static final ERROR_CODE_DEVICE_NOT_SUPPORT:I = 0x65

.field private static final ERROR_CODE_OTHER_DUPLICATE_SETTING:I = 0xc8

.field private static final SUCCESS_CODE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "DualCelluarDataService"

.field private static sInstance:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIActivityManager:Landroid/app/IActivityManager;

.field private final mProcessObserver:Landroid/app/IProcessObserver;

.field private mRedundantModeApplicationPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$mprocessWhiteListAppDied(Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->processWhiteListAppDied(I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 32
    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->sInstance:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    new-instance v0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService$1;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService$1;-><init>(Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mProcessObserver:Landroid/app/IProcessObserver;

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mContext:Landroid/content/Context;

    .line 36
    return-void
.end method

.method public static destroyInstance()V
    .locals 4

    .line 56
    sget-object v0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->sInstance:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    if-eqz v0, :cond_1

    .line 57
    const-class v0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    monitor-enter v0

    .line 58
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->sInstance:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 60
    :try_start_1
    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->onDestroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 64
    goto :goto_0

    .line 62
    :catch_0
    move-exception v1

    .line 63
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "DualCelluarDataService"

    const-string v3, "destroyInstance onDestroy catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 65
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->sInstance:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    .line 67
    :cond_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 69
    :cond_1
    :goto_1
    return-void
.end method

.method private getDualDataEnable()Z
    .locals 3

    .line 178
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "application_dual_mobile_data"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 179
    .local v0, "enable":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 39
    sget-object v0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->sInstance:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    if-nez v0, :cond_1

    .line 40
    const-class v0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    monitor-enter v0

    .line 41
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->sInstance:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    if-nez v1, :cond_0

    .line 42
    new-instance v1, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->sInstance:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    :try_start_1
    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->onCreate()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 48
    goto :goto_0

    .line 46
    :catch_0
    move-exception v1

    .line 47
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "DualCelluarDataService"

    const-string v3, "getInstance onCreate catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 50
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 52
    :cond_1
    :goto_1
    sget-object v0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->sInstance:Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;

    return-object v0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .line 183
    const-string v0, "DualCelluarDataService"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    return-void
.end method

.method private processWhiteListAppDied(I)V
    .locals 7
    .param p1, "uid"    # I

    .line 160
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 161
    .local v0, "pkgNames":[Ljava/lang/String;
    if-eqz v0, :cond_1

    array-length v1, v0

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mRedundantModeApplicationPN:Ljava/util/Set;

    if-eqz v1, :cond_1

    .line 162
    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    .line 163
    .local v4, "packageName":Ljava/lang/String;
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mRedundantModeApplicationPN:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 164
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " processWhiteListAppDied packageName: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V

    .line 165
    invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->setDualDataEnable(Z)V

    .line 162
    .end local v4    # "packageName":Ljava/lang/String;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 169
    :cond_1
    return-void
.end method

.method private setDualDataEnable(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " setDualDataEnable"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V

    .line 173
    move v0, p1

    .line 174
    .local v0, "curState":I
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "application_dual_mobile_data"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 175
    return-void
.end method


# virtual methods
.method initRedundantModeApplicationPN()V
    .locals 2

    .line 94
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mRedundantModeApplicationPN:Ljava/util/Set;

    .line 95
    const-string v1, "com.miui.vpnsdkmanager"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mRedundantModeApplicationPN:Ljava/util/Set;

    const-string v1, "com.miui.securityadd"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mRedundantModeApplicationPN:Ljava/util/Set;

    const-string v1, "cn.wsds.gamemaster"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 98
    return-void
.end method

.method public onCreate()V
    .locals 2

    .line 72
    const-string v0, "DualCelluarDataService onCreate"

    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V

    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->setDualDataEnable(Z)V

    .line 74
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->initRedundantModeApplicationPN()V

    .line 75
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mIActivityManager:Landroid/app/IActivityManager;

    .line 77
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mProcessObserver:Landroid/app/IProcessObserver;

    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->registerProcessObserver(Landroid/app/IProcessObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    goto :goto_0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, " could not get IActivityManager"

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V

    .line 81
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 84
    const-string v0, "DualCelluarDataService onDestroy"

    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V

    .line 85
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mIActivityManager:Landroid/app/IActivityManager;

    .line 87
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mProcessObserver:Landroid/app/IProcessObserver;

    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->unregisterProcessObserver(Landroid/app/IProcessObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, " could not get IActivityManager"

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V

    .line 91
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public setDualCelluarDataEnable(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .line 131
    const/4 v0, 0x0

    .line 132
    .local v0, "errorCode":I
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->supportDualCelluarData()Z

    move-result v1

    if-nez v1, :cond_0

    .line 133
    const/16 v0, 0x65

    .line 135
    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->getDualDataEnable()Z

    move-result v1

    if-ne p1, v1, :cond_1

    .line 136
    const/16 v0, 0xc8

    .line 138
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setDualCelluarDataEnable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V

    .line 139
    if-nez v0, :cond_2

    .line 140
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->setDualDataEnable(Z)V

    .line 141
    const/4 v1, 0x1

    return v1

    .line 143
    :cond_2
    const/4 v1, 0x0

    return v1
.end method

.method public supportDualCelluarData()Z
    .locals 4

    .line 101
    const/4 v0, 0x0

    .line 102
    .local v0, "support":Z
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 103
    return v2

    .line 106
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x1105003e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 109
    nop

    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "supportDualCelluarData: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V

    .line 111
    return v0

    .line 107
    :catch_0
    move-exception v1

    .line 108
    .local v1, "exception":Ljava/lang/Exception;
    return v2
.end method

.method public supportMediaPlayerPolicy()Z
    .locals 4

    .line 116
    const/4 v0, 0x0

    .line 117
    .local v0, "support":Z
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 118
    return v2

    .line 121
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x1105005b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 124
    nop

    .line 125
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "supportMediaPlayerPolicy: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V

    .line 126
    return v0

    .line 122
    :catch_0
    move-exception v1

    .line 123
    .local v1, "exception":Ljava/lang/Exception;
    return v2
.end method
