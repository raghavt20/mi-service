public class com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService {
	 /* .source "DualCelluarDataService.java" */
	 /* # static fields */
	 private static final java.lang.String APPLICATION_DUAL_MOBILE_DATA;
	 private static final Integer DUAL_DATA_STATE_OFF;
	 private static final Integer DUAL_DATA_STATE_ON;
	 public static final java.lang.String DUAL_MOBILE_DATA;
	 private static final Integer ERROR_CODE_DEVICE_CAPABILITY;
	 private static final Integer ERROR_CODE_DEVICE_NOT_SUPPORT;
	 private static final Integer ERROR_CODE_OTHER_DUPLICATE_SETTING;
	 private static final Integer SUCCESS_CODE;
	 private static final java.lang.String TAG;
	 private static com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService sInstance;
	 /* # instance fields */
	 private android.content.Context mContext;
	 private android.app.IActivityManager mIActivityManager;
	 private final android.app.IProcessObserver mProcessObserver;
	 private java.util.Set mRedundantModeApplicationPN;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
static void -$$Nest$mprocessWhiteListAppDied ( com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->processWhiteListAppDied(I)V */
return;
} // .end method
static com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService ( ) {
/* .locals 1 */
/* .line 32 */
int v0 = 0; // const/4 v0, 0x0
return;
} // .end method
private com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 34 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 146 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService$1; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService$1;-><init>(Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;)V */
this.mProcessObserver = v0;
/* .line 35 */
(( android.content.Context ) p1 ).getApplicationContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
this.mContext = v0;
/* .line 36 */
return;
} // .end method
public static void destroyInstance ( ) {
/* .locals 4 */
/* .line 56 */
v0 = com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService.sInstance;
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 57 */
	 /* const-class v0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService; */
	 /* monitor-enter v0 */
	 /* .line 58 */
	 try { // :try_start_0
		 v1 = com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService.sInstance;
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 60 */
			 try { // :try_start_1
				 (( com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService ) v1 ).onDestroy ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->onDestroy()V
				 /* :try_end_1 */
				 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
				 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
				 /* .line 64 */
				 /* .line 62 */
				 /* :catch_0 */
				 /* move-exception v1 */
				 /* .line 63 */
				 /* .local v1, "e":Ljava/lang/Exception; */
				 try { // :try_start_2
					 final String v2 = "DualCelluarDataService"; // const-string v2, "DualCelluarDataService"
					 final String v3 = "destroyInstance onDestroy catch:"; // const-string v3, "destroyInstance onDestroy catch:"
					 android.util.Log .e ( v2,v3,v1 );
					 /* .line 65 */
				 } // .end local v1 # "e":Ljava/lang/Exception;
			 } // :goto_0
			 int v1 = 0; // const/4 v1, 0x0
			 /* .line 67 */
		 } // :cond_0
		 /* monitor-exit v0 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* :try_end_2 */
		 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
		 /* throw v1 */
		 /* .line 69 */
	 } // :cond_1
} // :goto_1
return;
} // .end method
private Boolean getDualDataEnable ( ) {
/* .locals 3 */
/* .line 178 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "application_dual_mobile_data"; // const-string v1, "application_dual_mobile_data"
int v2 = -1; // const/4 v2, -0x1
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
/* .line 179 */
/* .local v0, "enable":I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public static com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService getInstance ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 39 */
v0 = com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService.sInstance;
/* if-nez v0, :cond_1 */
/* .line 40 */
/* const-class v0, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService; */
/* monitor-enter v0 */
/* .line 41 */
try { // :try_start_0
v1 = com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService.sInstance;
/* if-nez v1, :cond_0 */
/* .line 42 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;-><init>(Landroid/content/Context;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 44 */
try { // :try_start_1
(( com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService ) v1 ).onCreate ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->onCreate()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 48 */
/* .line 46 */
/* :catch_0 */
/* move-exception v1 */
/* .line 47 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
	 final String v2 = "DualCelluarDataService"; // const-string v2, "DualCelluarDataService"
	 final String v3 = "getInstance onCreate catch:"; // const-string v3, "getInstance onCreate catch:"
	 android.util.Log .e ( v2,v3,v1 );
	 /* .line 50 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 52 */
} // :cond_1
} // :goto_1
v0 = com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService.sInstance;
} // .end method
private void log ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "s" # Ljava/lang/String; */
/* .line 183 */
final String v0 = "DualCelluarDataService"; // const-string v0, "DualCelluarDataService"
android.util.Log .d ( v0,p1 );
/* .line 184 */
return;
} // .end method
private void processWhiteListAppDied ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .line 160 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v0 ).getPackagesForUid ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;
/* .line 161 */
/* .local v0, "pkgNames":[Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* array-length v1, v0 */
/* if-lez v1, :cond_1 */
v1 = this.mRedundantModeApplicationPN;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 162 */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_1 */
/* aget-object v4, v0, v3 */
/* .line 163 */
/* .local v4, "packageName":Ljava/lang/String; */
v5 = v5 = this.mRedundantModeApplicationPN;
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 164 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " processWhiteListAppDied packageName: "; // const-string v6, " processWhiteListAppDied packageName: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v5}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V */
/* .line 165 */
/* invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->setDualDataEnable(Z)V */
/* .line 162 */
} // .end local v4 # "packageName":Ljava/lang/String;
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 169 */
} // :cond_1
return;
} // .end method
private void setDualDataEnable ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 172 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " setDualDataEnable"; // const-string v1, " setDualDataEnable"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V */
/* .line 173 */
/* move v0, p1 */
/* .line 174 */
/* .local v0, "curState":I */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "application_dual_mobile_data"; // const-string v2, "application_dual_mobile_data"
android.provider.Settings$Global .putInt ( v1,v2,v0 );
/* .line 175 */
return;
} // .end method
/* # virtual methods */
void initRedundantModeApplicationPN ( ) {
/* .locals 2 */
/* .line 94 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mRedundantModeApplicationPN = v0;
/* .line 95 */
final String v1 = "com.miui.vpnsdkmanager"; // const-string v1, "com.miui.vpnsdkmanager"
/* .line 96 */
v0 = this.mRedundantModeApplicationPN;
final String v1 = "com.miui.securityadd"; // const-string v1, "com.miui.securityadd"
/* .line 97 */
v0 = this.mRedundantModeApplicationPN;
final String v1 = "cn.wsds.gamemaster"; // const-string v1, "cn.wsds.gamemaster"
/* .line 98 */
return;
} // .end method
public void onCreate ( ) {
/* .locals 2 */
/* .line 72 */
final String v0 = "DualCelluarDataService onCreate"; // const-string v0, "DualCelluarDataService onCreate"
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V */
/* .line 73 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->setDualDataEnable(Z)V */
/* .line 74 */
(( com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService ) p0 ).initRedundantModeApplicationPN ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->initRedundantModeApplicationPN()V
/* .line 75 */
android.app.ActivityManager .getService ( );
this.mIActivityManager = v0;
/* .line 77 */
try { // :try_start_0
v1 = this.mProcessObserver;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 80 */
/* .line 78 */
/* :catch_0 */
/* move-exception v0 */
/* .line 79 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = " could not get IActivityManager"; // const-string v1, " could not get IActivityManager"
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V */
/* .line 81 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
public void onDestroy ( ) {
/* .locals 2 */
/* .line 84 */
final String v0 = "DualCelluarDataService onDestroy"; // const-string v0, "DualCelluarDataService onDestroy"
/* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V */
/* .line 85 */
android.app.ActivityManager .getService ( );
this.mIActivityManager = v0;
/* .line 87 */
try { // :try_start_0
v1 = this.mProcessObserver;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 90 */
/* .line 88 */
/* :catch_0 */
/* move-exception v0 */
/* .line 89 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = " could not get IActivityManager"; // const-string v1, " could not get IActivityManager"
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V */
/* .line 91 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
public Boolean setDualCelluarDataEnable ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 131 */
int v0 = 0; // const/4 v0, 0x0
/* .line 132 */
/* .local v0, "errorCode":I */
v1 = (( com.xiaomi.NetworkBoost.DualCelluarDataService.DualCelluarDataService ) p0 ).supportDualCelluarData ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->supportDualCelluarData()Z
/* if-nez v1, :cond_0 */
/* .line 133 */
/* const/16 v0, 0x65 */
/* .line 135 */
} // :cond_0
v1 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->getDualDataEnable()Z */
/* if-ne p1, v1, :cond_1 */
/* .line 136 */
/* const/16 v0, 0xc8 */
/* .line 138 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setDualCelluarDataEnable: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V */
/* .line 139 */
/* if-nez v0, :cond_2 */
/* .line 140 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->setDualDataEnable(Z)V */
/* .line 141 */
int v1 = 1; // const/4 v1, 0x1
/* .line 143 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean supportDualCelluarData ( ) {
/* .locals 4 */
/* .line 101 */
int v0 = 0; // const/4 v0, 0x0
/* .line 102 */
/* .local v0, "support":Z */
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 103 */
/* .line 106 */
} // :cond_0
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x1105003e */
v1 = (( android.content.res.Resources ) v1 ).getBoolean ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 109 */
/* nop */
/* .line 110 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "supportDualCelluarData: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V */
/* .line 111 */
/* .line 107 */
/* :catch_0 */
/* move-exception v1 */
/* .line 108 */
/* .local v1, "exception":Ljava/lang/Exception; */
} // .end method
public Boolean supportMediaPlayerPolicy ( ) {
/* .locals 4 */
/* .line 116 */
int v0 = 0; // const/4 v0, 0x0
/* .line 117 */
/* .local v0, "support":Z */
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 118 */
/* .line 121 */
} // :cond_0
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x1105005b */
v1 = (( android.content.res.Resources ) v1 ).getBoolean ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 124 */
/* nop */
/* .line 125 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "supportMediaPlayerPolicy: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/xiaomi/NetworkBoost/DualCelluarDataService/DualCelluarDataService;->log(Ljava/lang/String;)V */
/* .line 126 */
/* .line 122 */
/* :catch_0 */
/* move-exception v1 */
/* .line 123 */
/* .local v1, "exception":Ljava/lang/Exception; */
} // .end method
