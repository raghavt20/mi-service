.class public Lcom/xiaomi/NetworkBoost/StatusManager;
.super Ljava/lang/Object;
.source "StatusManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;,
        Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;,
        Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;,
        Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;,
        Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;,
        Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;,
        Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;,
        Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;,
        Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;,
        Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;,
        Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;
    }
.end annotation


# static fields
.field public static final CALLBACK_DEFAULT_NETWORK_INTERFACE:I = 0x6f

.field public static final CALLBACK_NETWORK_INTERFACE:I = 0x6b

.field public static final CALLBACK_NETWORK_PRIORITY_ADD:I = 0x6e

.field public static final CALLBACK_NETWORK_PRIORITY_CHANGED:I = 0x6d

.field public static final CALLBACK_SCREEN_STATUS:I = 0x6c

.field public static final CALLBACK_VPN_STATUS:I = 0x70

.field public static final DATA_READY:I = 0x3

.field private static final DELAY_TIME:I = 0x2710

.field private static final EVENT_CHECK_SENSOR_STATUS:I = 0x6a

.field private static final EVENT_DEFAULT_INTERFACE_ONAVAILABLE:I = 0x66

.field private static final EVENT_DELAY_SET_CELLULAR_CALLBACK:I = 0x67

.field private static final EVENT_GET_MODEM_SIGNAL_STRENGTH:I = 0x69

.field private static final EVENT_INTERFACE_ONAVAILABLE:I = 0x64

.field private static final EVENT_INTERFACE_ONLOST:I = 0x65

.field private static final EVENT_SIM_STATE_CHANGED:I = 0x68

.field private static final LTE_RSRP_LOWER_LIMIT:I = -0x8c

.field private static final LTE_RSRP_UPPER_LIMIT:I = -0x2b

.field private static final MOVEMENT_SENSOR_TYPE:I = 0x1fa267e

.field private static final NA_RAT:I = -0x1

.field private static final NETWORK_TYPE_LTE:I = 0xd

.field private static final NETWORK_TYPE_LTE_CA:I = 0x13

.field private static final NETWORK_TYPE_NR:I = 0x14

.field private static final NETWORK_TYPE_NR_CA:I = 0x1e

.field private static final NR_RSRP_LOWER_LIMIT:I = -0x8c

.field private static final NR_RSRP_UPPER_LIMIT:I = -0x2c

.field public static final ON_AVAILABLE:Ljava/lang/String; = "ON_AVAILABLE"

.field public static final ON_LOST:Ljava/lang/String; = "ON_LOST"

.field public static final PHONE_COUNT:I

.field private static final RSRP_POOR_THRESHOLD:I = -0x69

.field private static final SENSOR_DELAY_TIME:I = 0x1388

.field private static final SIGNAL_STRENGTH_DELAY_MIILLIS:I = 0x7530

.field private static final SIGNAL_STRENGTH_GOOD:I = 0x0

.field private static final SIGNAL_STRENGTH_POOR:I = 0x1

.field public static final SLAVE_DATA_READY:I = 0x4

.field public static final SLAVE_WIFI_READY:I = 0x2

.field private static final TAG:Ljava/lang/String; = "NetworkBoostStatusManager"

.field private static final UNAVAILABLE:I = 0x7fffffff

.field private static final VPN_STATUS_DISABLE:Z = false

.field private static final VPN_STATUS_ENABLE:Z = true

.field private static final WIFI_BAND_5_GHZ_HBS:I = 0x102

.field public static final WIFI_READY:I = 0x1

.field public static final WLAN_IFACE_0:Ljava/lang/String; = "wlan0"

.field public static final WLAN_IFACE_1:Ljava/lang/String; = "wlan1"

.field private static mConnectivityManager:Landroid/net/ConnectivityManager;

.field private static mReflectScreenState:Ljava/lang/reflect/Method;

.field private static mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

.field private static mWifiManager:Landroid/net/wifi/WifiManager;

.field public static volatile sInstance:Lcom/xiaomi/NetworkBoost/StatusManager;


# instance fields
.field private mActivityManager:Landroid/app/IActivityManager;

.field private mAppStatusListenerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;",
            ">;"
        }
    .end annotation
.end field

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAudioModeListener:Landroid/media/AudioManager$OnModeChangedListener;

.field private mCellularNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field private mContext:Landroid/content/Context;

.field private mCurrentSensorevent:Landroid/hardware/SensorEvent;

.field private mDataReady:Z

.field private mDefaultIface:Ljava/lang/String;

.field private mDefaultIfaceStatus:I

.field private mDefaultNetid:I

.field private mDefaultNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field private mDefaultNetworkInterfaceListenerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;",
            ">;"
        }
    .end annotation
.end field

.field private mFUid:I

.field private mForegroundInfoListener:Lmiui/process/IForegroundInfoListener$Stub;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mIfaceNumber:I

.field private mInterface:Ljava/lang/String;

.field private mModemSignalStrengthListener:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;",
            ">;"
        }
    .end annotation
.end field

.field private mMovementSensorStatusListenerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;",
            ">;"
        }
    .end annotation
.end field

.field private mNetworkBoostProcessMonitor:Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;

.field private mNetworkInterfaceListenerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mNetworkMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;",
            ">;"
        }
    .end annotation
.end field

.field private mNetworkPriorityListenerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;",
            ">;"
        }
    .end annotation
.end field

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPriorityMode:I

.field private mScreenStatusListenerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;",
            ">;"
        }
    .end annotation
.end field

.field private mScreenStatusReceiver:Landroid/content/BroadcastReceiver;

.field private mSensorFlag:Z

.field private final mSensorListener:Landroid/hardware/SensorEventListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSlaveDataReady:Z

.field private mSlaveWifiReady:Z

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mTelephonyStatusReceiver:Landroid/content/BroadcastReceiver;

.field private mThermalLevel:I

.field private mTrafficPolicy:I

.field private final mUidObserver:Landroid/app/IUidObserver;

.field private mVPNCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field private mVPNListener:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;",
            ">;"
        }
    .end annotation
.end field

.field private mVPNstatus:Z

.field private mWacthdogHandlerThread:Landroid/os/HandlerThread;

.field private mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field private mWifiReady:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmAppStatusListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mAppStatusListenerList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDataReady(Lcom/xiaomi/NetworkBoost/StatusManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmDefaultIface(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIface:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDefaultIfaceStatus(Lcom/xiaomi/NetworkBoost/StatusManager;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIfaceStatus:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmDefaultNetworkInterfaceListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetworkInterfaceListenerList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/StatusManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIfaceNumber(Lcom/xiaomi/NetworkBoost/StatusManager;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmInterface(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mInterface:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMovementSensorStatusListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mMovementSensorStatusListenerList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNetworkInterfaceListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkInterfaceListenerList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNetworkPriorityListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkPriorityListenerList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPowerManager(Lcom/xiaomi/NetworkBoost/StatusManager;)Landroid/os/PowerManager;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPowerManager:Landroid/os/PowerManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPriorityMode(Lcom/xiaomi/NetworkBoost/StatusManager;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPriorityMode:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmScreenStatusListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mScreenStatusListenerList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSlaveDataReady(Lcom/xiaomi/NetworkBoost/StatusManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSlaveWifiReady(Lcom/xiaomi/NetworkBoost/StatusManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiReady:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmThermalLevel(Lcom/xiaomi/NetworkBoost/StatusManager;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mThermalLevel:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTrafficPolicy(Lcom/xiaomi/NetworkBoost/StatusManager;)I
    .locals 0

    iget p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTrafficPolicy:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmVPNListener(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNListener:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmVPNstatus(Lcom/xiaomi/NetworkBoost/StatusManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNstatus:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmWifiReady(Lcom/xiaomi/NetworkBoost/StatusManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiReady:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmCurrentSensorevent(Lcom/xiaomi/NetworkBoost/StatusManager;Landroid/hardware/SensorEvent;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mCurrentSensorevent:Landroid/hardware/SensorEvent;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDefaultNetid(Lcom/xiaomi/NetworkBoost/StatusManager;I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetid:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFUid(Lcom/xiaomi/NetworkBoost/StatusManager;I)V
    .locals 0

    iput p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mFUid:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmVPNstatus(Lcom/xiaomi/NetworkBoost/StatusManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNstatus:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mcallbackDefaultNetworkInterface(Lcom/xiaomi/NetworkBoost/StatusManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->callbackDefaultNetworkInterface()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckSensorStatus(Lcom/xiaomi/NetworkBoost/StatusManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->checkSensorStatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$minterfaceOnAvailable(Lcom/xiaomi/NetworkBoost/StatusManager;Landroid/net/Network;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->interfaceOnAvailable(Landroid/net/Network;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$minterfaceOnLost(Lcom/xiaomi/NetworkBoost/StatusManager;Landroid/net/Network;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->interfaceOnLost(Landroid/net/Network;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$misModemSingnalStrengthStatus(Lcom/xiaomi/NetworkBoost/StatusManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->isModemSingnalStrengthStatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monNetworkPriorityChanged(Lcom/xiaomi/NetworkBoost/StatusManager;Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->onNetworkPriorityChanged(Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mprocessRegisterCellularCallback(Lcom/xiaomi/NetworkBoost/StatusManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->processRegisterCellularCallback()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mprocessSimStateChanged(Lcom/xiaomi/NetworkBoost/StatusManager;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->processSimStateChanged(Landroid/content/Intent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mregisterMovementSensorListener(Lcom/xiaomi/NetworkBoost/StatusManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerMovementSensorListener()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetSmartDNSInterface(Lcom/xiaomi/NetworkBoost/StatusManager;Landroid/net/Network;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->setSmartDNSInterface(Landroid/net/Network;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$munregisterMovementSensorListener(Lcom/xiaomi/NetworkBoost/StatusManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterMovementSensorListener()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 84
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getPhoneCount()I

    move-result v0

    sput v0, Lcom/xiaomi/NetworkBoost/StatusManager;->PHONE_COUNT:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    const-string v0, ""

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mInterface:Ljava/lang/String;

    .line 142
    const/4 v1, 0x0

    iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I

    .line 143
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiReady:Z

    .line 144
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiReady:Z

    .line 145
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z

    .line 146
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z

    .line 147
    const/4 v2, -0x1

    iput v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mFUid:I

    .line 150
    iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetid:I

    .line 151
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIface:Ljava/lang/String;

    .line 152
    iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIfaceStatus:I

    .line 157
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNstatus:Z

    .line 162
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 163
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mCurrentSensorevent:Landroid/hardware/SensorEvent;

    .line 164
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorFlag:Z

    .line 167
    iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPriorityMode:I

    .line 168
    iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTrafficPolicy:I

    .line 169
    iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mThermalLevel:I

    .line 191
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mAppStatusListenerList:Ljava/util/List;

    .line 192
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkInterfaceListenerList:Ljava/util/List;

    .line 193
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetworkInterfaceListenerList:Ljava/util/List;

    .line 194
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mScreenStatusListenerList:Ljava/util/List;

    .line 195
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkPriorityListenerList:Ljava/util/List;

    .line 196
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mModemSignalStrengthListener:Ljava/util/List;

    .line 197
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mMovementSensorStatusListenerList:Ljava/util/List;

    .line 198
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNListener:Ljava/util/List;

    .line 202
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    .line 297
    new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$1;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$1;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mForegroundInfoListener:Lmiui/process/IForegroundInfoListener$Stub;

    .line 337
    new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$2;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$2;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mUidObserver:Landroid/app/IUidObserver;

    .line 511
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkMap:Ljava/util/HashMap;

    .line 720
    new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$4;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$4;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 747
    new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$5;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$5;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mCellularNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 1016
    new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$6;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$6;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 1088
    new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$7;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$7;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 1592
    new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$10;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$10;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorListener:Landroid/hardware/SensorEventListener;

    .line 238
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    .line 239
    return-void
.end method

.method private callbackDefaultNetworkInterface()V
    .locals 6

    .line 972
    iget v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetid:I

    if-nez v0, :cond_0

    .line 973
    return-void

    .line 976
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 977
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkMap:Ljava/util/HashMap;

    iget v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetid:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;

    .line 978
    .local v0, "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->getInterfaceName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIface:Ljava/lang/String;

    .line 979
    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->getStatus()I

    move-result v1

    iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIfaceStatus:I

    .line 981
    const-string v1, "NetworkBoostStatusManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "callbackDefaultNetworkInterface ifacename:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIface:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ifacestatus:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIfaceStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 984
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetworkInterfaceListenerList:Ljava/util/List;

    monitor-enter v1

    .line 985
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetworkInterfaceListenerList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;

    .line 986
    .local v3, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIface:Ljava/lang/String;

    iget v5, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIfaceStatus:I

    invoke-interface {v3, v4, v5}, Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;->onDefaultNetwrokChange(Ljava/lang/String;I)V

    .line 987
    .end local v3    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;
    goto :goto_0

    .line 988
    :cond_1
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 991
    .end local v0    # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
    :cond_2
    :goto_1
    return-void
.end method

.method private callbackNetworkInterface()V
    .locals 11

    .line 895
    const-string v0, "NetworkBoostStatusManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "interface name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mInterface:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " interface number:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WifiReady:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiReady:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " SlaveWifiReady:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiReady:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " DataReady:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " SlaveDataReady:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkInterfaceListenerList:Ljava/util/List;

    monitor-enter v0

    .line 899
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkInterfaceListenerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    .line 900
    .local v3, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mInterface:Ljava/lang/String;

    iget v5, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I

    iget-boolean v6, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiReady:Z

    iget-boolean v7, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiReady:Z

    iget-boolean v8, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z

    iget-boolean v9, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z

    const-string v10, "ON_LOST"

    invoke-interface/range {v3 .. v10}, Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;->onNetwrokInterfaceChange(Ljava/lang/String;IZZZZLjava/lang/String;)V

    .line 902
    .end local v3    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;
    goto :goto_0

    .line 903
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 905
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->checkSensorStatus()V

    .line 906
    return-void

    .line 903
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private checkSensorStatus()V
    .locals 4

    .line 1609
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1610
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1611
    return-void
.end method

.method public static destroyInstance()V
    .locals 4

    .line 222
    sget-object v0, Lcom/xiaomi/NetworkBoost/StatusManager;->sInstance:Lcom/xiaomi/NetworkBoost/StatusManager;

    if-eqz v0, :cond_1

    .line 223
    const-class v0, Lcom/xiaomi/NetworkBoost/StatusManager;

    monitor-enter v0

    .line 224
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/StatusManager;->sInstance:Lcom/xiaomi/NetworkBoost/StatusManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 226
    :try_start_1
    sget-object v1, Lcom/xiaomi/NetworkBoost/StatusManager;->sInstance:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->onDestroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 230
    goto :goto_0

    .line 228
    :catch_0
    move-exception v1

    .line 229
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "NetworkBoostStatusManager"

    const-string v3, "destroyInstance onDestroy catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 231
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/xiaomi/NetworkBoost/StatusManager;->sInstance:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 233
    :cond_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 235
    :cond_1
    :goto_1
    return-void
.end method

.method private getDefaultDataSlotId()I
    .locals 1

    .line 1364
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionManager;->getDefaultDataSlotId()I

    move-result v0

    return v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 205
    sget-object v0, Lcom/xiaomi/NetworkBoost/StatusManager;->sInstance:Lcom/xiaomi/NetworkBoost/StatusManager;

    if-nez v0, :cond_1

    .line 206
    const-class v0, Lcom/xiaomi/NetworkBoost/StatusManager;

    monitor-enter v0

    .line 207
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/StatusManager;->sInstance:Lcom/xiaomi/NetworkBoost/StatusManager;

    if-nez v1, :cond_0

    .line 208
    new-instance v1, Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/StatusManager;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/NetworkBoost/StatusManager;->sInstance:Lcom/xiaomi/NetworkBoost/StatusManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    :try_start_1
    sget-object v1, Lcom/xiaomi/NetworkBoost/StatusManager;->sInstance:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->onCreate()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214
    goto :goto_0

    .line 212
    :catch_0
    move-exception v1

    .line 213
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "NetworkBoostStatusManager"

    const-string v3, "getInstance onCreate catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 216
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 218
    :cond_1
    :goto_1
    sget-object v0, Lcom/xiaomi/NetworkBoost/StatusManager;->sInstance:Lcom/xiaomi/NetworkBoost/StatusManager;

    return-object v0
.end method

.method private getMobileDataSettingName()Ljava/lang/String;
    .locals 2

    .line 1448
    sget v0, Lcom/xiaomi/NetworkBoost/StatusManager;->PHONE_COUNT:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 1449
    const-string v0, "mobile_data0"

    goto :goto_0

    :cond_0
    const-string v0, "mobile_data"

    .line 1448
    :goto_0
    return-object v0
.end method

.method private getViceDataSlotId()I
    .locals 2

    .line 1368
    const/4 v0, 0x0

    .local v0, "slotId":I
    :goto_0
    sget v1, Lcom/xiaomi/NetworkBoost/StatusManager;->PHONE_COUNT:I

    if-ge v0, v1, :cond_1

    .line 1369
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getDefaultDataSlotId()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1370
    return v0

    .line 1368
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1373
    .end local v0    # "slotId":I
    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method private inRangeOrUnavailable(III)I
    .locals 1
    .param p1, "value"    # I
    .param p2, "rangeMin"    # I
    .param p3, "rangeMax"    # I

    .line 1292
    if-lt p1, p2, :cond_1

    if-le p1, p3, :cond_0

    goto :goto_0

    .line 1293
    :cond_0
    return p1

    .line 1292
    :cond_1
    :goto_0
    const v0, 0x7fffffff

    return v0
.end method

.method private interfaceOnAvailable(Landroid/net/Network;)V
    .locals 17
    .param p1, "network"    # Landroid/net/Network;

    .line 776
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "NetworkBoostStatusManager"

    if-nez v1, :cond_0

    .line 777
    const-string v3, "interfaceOnAvailable network is null."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    return-void

    .line 781
    :cond_0
    sget-object v3, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v3, v1}, Landroid/net/ConnectivityManager;->getLinkProperties(Landroid/net/Network;)Landroid/net/LinkProperties;

    move-result-object v3

    .line 782
    .local v3, "linkProperties":Landroid/net/LinkProperties;
    sget-object v4, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v4, v1}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;

    move-result-object v4

    .line 783
    .local v4, "networkCapabilities":Landroid/net/NetworkCapabilities;
    sget-object v5, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v5, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(Landroid/net/Network;)Landroid/net/NetworkInfo;

    move-result-object v5

    .line 784
    .local v5, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_e

    if-eqz v4, :cond_e

    if-nez v5, :cond_1

    move-object/from16 v16, v4

    goto/16 :goto_6

    .line 788
    :cond_1
    invoke-virtual {v3}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/net/InetAddress;

    .line 789
    .local v7, "inetAddress":Ljava/net/InetAddress;
    instance-of v8, v7, Ljava/net/Inet4Address;

    if-eqz v8, :cond_c

    .line 790
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onAvailable interface name:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    iget-object v8, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mInterface:Ljava/lang/String;

    invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v9, -0x1

    if-ne v8, v9, :cond_b

    .line 792
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mInterface:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mInterface:Ljava/lang/String;

    .line 793
    iget v8, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I

    const/4 v9, 0x1

    add-int/2addr v8, v9

    iput v8, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I

    .line 794
    const-string v8, "WIFI"

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 795
    sget-object v8, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v8}, Landroid/net/wifi/WifiManager;->getCurrentNetwork()Landroid/net/Network;

    move-result-object v8

    .line 796
    .local v8, "currentNetwork":Landroid/net/Network;
    if-eqz v8, :cond_2

    invoke-virtual {v8, v1}, Landroid/net/Network;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 797
    const-string v10, "onAvailable WIFI master."

    invoke-static {v2, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    iput-boolean v9, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiReady:Z

    .line 799
    new-instance v10, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;

    .line 800
    invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v0, v11, v9}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;Ljava/lang/String;I)V

    move-object v9, v10

    .line 801
    .local v9, "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
    iget-object v10, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkMap:Ljava/util/HashMap;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Network;->getNetId()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 802
    .end local v9    # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
    goto :goto_1

    .line 803
    :cond_2
    const-string v10, "onAvailable WIFI slave."

    invoke-static {v2, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    iput-boolean v9, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiReady:Z

    .line 805
    new-instance v9, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;

    .line 806
    invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x2

    invoke-direct {v9, v0, v10, v11}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;Ljava/lang/String;I)V

    .line 807
    .restart local v9    # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
    iget-object v10, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkMap:Ljava/util/HashMap;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Network;->getNetId()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 809
    .end local v8    # "currentNetwork":Landroid/net/Network;
    .end local v9    # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
    :goto_1
    move-object/from16 v16, v4

    goto/16 :goto_5

    :cond_3
    const-string v8, "MOBILE"

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 810
    invoke-direct/range {p0 .. p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getDefaultDataSlotId()I

    move-result v8

    .line 811
    .local v8, "defaultDataSlotId":I
    const/4 v10, -0x1

    .line 812
    .local v10, "DataSubId":I
    invoke-direct {v0, v8}, Lcom/xiaomi/NetworkBoost/StatusManager;->isValidatePhoneId(I)Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-direct {v0, v8}, Lcom/xiaomi/NetworkBoost/StatusManager;->isSlotActivated(I)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 813
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v11

    invoke-virtual {v11, v8}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I

    move-result v10

    .line 815
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getViceDataSlotId()I

    move-result v11

    .line 816
    .local v11, "viceDataSlotId":I
    const/4 v12, -0x1

    .line 817
    .local v12, "SlaveDataSubId":I
    invoke-direct {v0, v11}, Lcom/xiaomi/NetworkBoost/StatusManager;->isValidatePhoneId(I)Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-direct {v0, v11}, Lcom/xiaomi/NetworkBoost/StatusManager;->isSlotActivated(I)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 818
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v13

    invoke-virtual {v13, v11}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I

    move-result v12

    .line 820
    :cond_5
    invoke-virtual {v4}, Landroid/net/NetworkCapabilities;->getSubscriptionIds()Ljava/util/Set;

    move-result-object v13

    .line 821
    .local v13, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {v13}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v14

    const/4 v15, 0x0

    aget-object v14, v14, v15

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 822
    .local v14, "subId":I
    if-ne v14, v10, :cond_8

    .line 823
    const-string v9, "onAvailable MOBILE master."

    invoke-static {v2, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 824
    iget-object v9, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkMap:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v15, v16

    check-cast v15, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;

    .line 825
    .local v15, "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
    invoke-virtual {v15}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->getStatus()I

    move-result v1

    move-object/from16 v16, v4

    const/4 v4, 0x3

    .end local v4    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    .local v16, "networkCapabilities":Landroid/net/NetworkCapabilities;
    if-ne v1, v4, :cond_6

    .line 826
    const/4 v1, 0x4

    invoke-virtual {v15, v1}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->setStatus(I)V

    .line 827
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z

    .line 828
    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z

    goto :goto_3

    .line 825
    :cond_6
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 830
    .end local v15    # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
    :goto_3
    move-object/from16 v1, p1

    move-object/from16 v4, v16

    goto :goto_2

    .line 831
    .end local v16    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    .restart local v4    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    :cond_7
    move-object/from16 v16, v4

    const/4 v1, 0x1

    .end local v4    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    .restart local v16    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    iput-boolean v1, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z

    .line 832
    new-instance v1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;

    .line 833
    invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v4

    const/4 v9, 0x3

    invoke-direct {v1, v0, v4, v9}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;Ljava/lang/String;I)V

    .line 834
    .local v1, "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
    iget-object v4, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkMap:Ljava/util/HashMap;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Network;->getNetId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .end local v1    # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
    goto :goto_4

    .line 835
    .end local v16    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    .restart local v4    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    :cond_8
    move-object/from16 v16, v4

    .end local v4    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    .restart local v16    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    if-ne v14, v12, :cond_9

    .line 836
    const-string v1, "onAvailable MOBILE slave."

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z

    .line 838
    new-instance v1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;

    .line 839
    invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v4

    const/4 v9, 0x4

    invoke-direct {v1, v0, v4, v9}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;Ljava/lang/String;I)V

    .line 840
    .restart local v1    # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
    iget-object v4, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkMap:Ljava/util/HashMap;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Network;->getNetId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 835
    .end local v1    # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
    :cond_9
    :goto_4
    goto :goto_5

    .line 809
    .end local v8    # "defaultDataSlotId":I
    .end local v10    # "DataSubId":I
    .end local v11    # "viceDataSlotId":I
    .end local v12    # "SlaveDataSubId":I
    .end local v13    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v14    # "subId":I
    .end local v16    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    .restart local v4    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    :cond_a
    move-object/from16 v16, v4

    .end local v4    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    .restart local v16    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    goto :goto_5

    .line 791
    .end local v16    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    .restart local v4    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    :cond_b
    move-object/from16 v16, v4

    .end local v4    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    .restart local v16    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    goto :goto_5

    .line 789
    .end local v16    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    .restart local v4    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    :cond_c
    move-object/from16 v16, v4

    .line 845
    .end local v4    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    .end local v7    # "inetAddress":Ljava/net/InetAddress;
    .restart local v16    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    :goto_5
    move-object/from16 v1, p1

    move-object/from16 v4, v16

    goto/16 :goto_0

    .line 847
    .end local v16    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    .restart local v4    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->callbackNetworkInterface()V

    .line 848
    invoke-direct/range {p0 .. p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->callbackDefaultNetworkInterface()V

    .line 849
    return-void

    .line 784
    :cond_e
    move-object/from16 v16, v4

    .line 785
    .end local v4    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    .restart local v16    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    :goto_6
    return-void
.end method

.method private interfaceOnLost(Landroid/net/Network;)V
    .locals 6
    .param p1, "network"    # Landroid/net/Network;

    .line 853
    const-string v0, "NetworkBoostStatusManager"

    if-nez p1, :cond_0

    .line 854
    const-string v1, "interfaceOnLost network is null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 855
    return-void

    .line 858
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLost interface Network:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/net/Network;->getNetId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 861
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mInterface:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/net/Network;->getNetId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->getInterfaceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 862
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mInterface:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/net/Network;->getNetId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;

    invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->getInterfaceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mInterface:Ljava/lang/String;

    .line 863
    iget v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I

    .line 864
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/net/Network;->getNetId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->getStatus()I

    move-result v0

    const/4 v2, 0x0

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 882
    :pswitch_0
    iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z

    .line 883
    goto :goto_1

    .line 872
    :pswitch_1
    iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z

    .line 873
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;

    .line 874
    .local v3, "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
    invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->getStatus()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 875
    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->setStatus(I)V

    .line 876
    iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z

    .line 877
    iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z

    .line 879
    .end local v3    # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
    :cond_1
    goto :goto_0

    .line 880
    :cond_2
    goto :goto_1

    .line 869
    :pswitch_2
    iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiReady:Z

    .line 870
    goto :goto_1

    .line 866
    :pswitch_3
    iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiReady:Z

    .line 867
    nop

    .line 886
    :goto_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/net/Network;->getNetId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 890
    :cond_3
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->callbackNetworkInterface()V

    .line 891
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private isDualDataSupported()Z
    .locals 3

    .line 1357
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    .line 1358
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1105003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 1359
    .local v0, "is_dual_data_support":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isDualDataSupported = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NetworkBoostStatusManager"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1360
    return v0
.end method

.method private isLteNrNwType(I)Z
    .locals 1
    .param p1, "rat"    # I

    .line 1318
    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->isLteNwType(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->isNrNwType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isLteNwType(I)Z
    .locals 1
    .param p1, "rat"    # I

    .line 1308
    const/16 v0, 0xd

    if-eq p1, v0, :cond_1

    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isMobileDataSettingOn()Z
    .locals 3

    .line 1444
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getMobileDataSettingName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private isModemSingnalStrengthStatus()V
    .locals 12

    .line 1240
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    .line 1241
    const-string v0, "NetworkBoostStatusManager"

    const-string v1, "TelephonyManager is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1242
    return-void

    .line 1245
    :cond_0
    const/4 v1, 0x0

    .line 1246
    .local v1, "isStatus":I
    const/4 v2, -0x1

    .line 1247
    .local v2, "rat":I
    :try_start_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSignalStrength()Landroid/telephony/SignalStrength;

    move-result-object v0

    .line 1248
    .local v0, "signalStrength":Landroid/telephony/SignalStrength;
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v3

    .line 1249
    .local v3, "serviceState":Landroid/telephony/ServiceState;
    if-eqz v0, :cond_8

    if-nez v3, :cond_1

    goto/16 :goto_3

    .line 1254
    :cond_1
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getDataNetworkType()I

    move-result v4

    move v2, v4

    .line 1256
    invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->isLteNrNwType(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1257
    return-void

    .line 1260
    :cond_2
    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getCellSignalStrengths()Ljava/util/List;

    move-result-object v4

    .line 1261
    .local v4, "cellSignalStrengths":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellSignalStrength;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/telephony/CellSignalStrength;

    .line 1262
    .local v6, "cellSignal":Landroid/telephony/CellSignalStrength;
    instance-of v7, v6, Landroid/telephony/CellSignalStrengthLte;

    const/16 v8, -0x8c

    if-eqz v7, :cond_4

    .line 1263
    move-object v7, v6

    check-cast v7, Landroid/telephony/CellSignalStrengthLte;

    .line 1264
    .local v7, "cellLteSignal":Landroid/telephony/CellSignalStrengthLte;
    invoke-virtual {v7}, Landroid/telephony/CellSignalStrengthLte;->getRsrp()I

    move-result v9

    const/16 v10, -0x2b

    invoke-direct {p0, v9, v8, v10}, Lcom/xiaomi/NetworkBoost/StatusManager;->inRangeOrUnavailable(III)I

    move-result v8

    .line 1266
    .local v8, "cellLteSignalRsrp":I
    invoke-virtual {p0, v8}, Lcom/xiaomi/NetworkBoost/StatusManager;->isPoorSignal(I)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1267
    const-string v9, "NetworkBoostStatusManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isModemSingnalStrengthStatus cellLteSignalRsrp = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1268
    const/4 v1, 0x1

    .line 1270
    .end local v7    # "cellLteSignal":Landroid/telephony/CellSignalStrengthLte;
    .end local v8    # "cellLteSignalRsrp":I
    :cond_3
    goto :goto_1

    :cond_4
    instance-of v7, v6, Landroid/telephony/CellSignalStrengthNr;

    if-eqz v7, :cond_3

    .line 1271
    move-object v7, v6

    check-cast v7, Landroid/telephony/CellSignalStrengthNr;

    .line 1272
    .local v7, "cellNrSignal":Landroid/telephony/CellSignalStrengthNr;
    invoke-virtual {v7}, Landroid/telephony/CellSignalStrengthNr;->getSsRsrp()I

    move-result v9

    const/16 v10, -0x2c

    invoke-direct {p0, v9, v8, v10}, Lcom/xiaomi/NetworkBoost/StatusManager;->inRangeOrUnavailable(III)I

    move-result v8

    .line 1274
    .local v8, "cellNrSignalRsrp":I
    invoke-virtual {p0, v8}, Lcom/xiaomi/NetworkBoost/StatusManager;->isPoorSignal(I)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1275
    const-string v9, "NetworkBoostStatusManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isModemSingnalStrengthStatus cellNrSignalRsrp = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1276
    const/4 v1, 0x1

    .line 1279
    .end local v6    # "cellSignal":Landroid/telephony/CellSignalStrength;
    .end local v7    # "cellNrSignal":Landroid/telephony/CellSignalStrengthNr;
    .end local v8    # "cellNrSignalRsrp":I
    :cond_5
    :goto_1
    goto :goto_0

    .line 1280
    :cond_6
    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mModemSignalStrengthListener:Ljava/util/List;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1281
    :try_start_1
    iget-object v6, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mModemSignalStrengthListener:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;

    .line 1282
    .local v7, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;
    invoke-interface {v7, v1}, Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;->isModemSingnalStrengthStatus(I)V

    .line 1283
    .end local v7    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;
    goto :goto_2

    .line 1284
    :cond_7
    monitor-exit v5

    .line 1288
    .end local v0    # "signalStrength":Landroid/telephony/SignalStrength;
    .end local v1    # "isStatus":I
    .end local v2    # "rat":I
    .end local v3    # "serviceState":Landroid/telephony/ServiceState;
    .end local v4    # "cellSignalStrengths":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellSignalStrength;>;"
    nop

    .line 1289
    return-void

    .line 1284
    .restart local v0    # "signalStrength":Landroid/telephony/SignalStrength;
    .restart local v1    # "isStatus":I
    .restart local v2    # "rat":I
    .restart local v3    # "serviceState":Landroid/telephony/ServiceState;
    .restart local v4    # "cellSignalStrengths":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellSignalStrength;>;"
    :catchall_0
    move-exception v6

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/xiaomi/NetworkBoost/StatusManager;
    :try_start_2
    throw v6

    .line 1250
    .end local v4    # "cellSignalStrengths":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellSignalStrength;>;"
    .restart local p0    # "this":Lcom/xiaomi/NetworkBoost/StatusManager;
    :cond_8
    :goto_3
    const-string v4, "NetworkBoostStatusManager"

    const-string v5, "isModemSingnalStrengthStatus signalStrength or serviceState is null"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1251
    return-void

    .line 1285
    .end local v0    # "signalStrength":Landroid/telephony/SignalStrength;
    .end local v1    # "isStatus":I
    .end local v2    # "rat":I
    .end local v3    # "serviceState":Landroid/telephony/ServiceState;
    :catch_0
    move-exception v0

    .line 1286
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "NetworkBoostStatusManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isModemSingnalStrengthStatus found an error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1287
    return-void
.end method

.method private isNrNwType(I)Z
    .locals 1
    .param p1, "rat"    # I

    .line 1313
    const/16 v0, 0x14

    if-eq p1, v0, :cond_1

    const/16 v0, 0x1e

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isSlotActivated(I)Z
    .locals 4
    .param p1, "slotId"    # I

    .line 1378
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v0

    .line 1379
    .local v0, "subscriptionInfo":Lmiui/telephony/SubscriptionInfo;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 1380
    const-string v2, "NetworkBoostStatusManager"

    const-string/jumbo v3, "subscriptionInfo == null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1381
    return v1

    .line 1383
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->isActivated()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public static isTetheredIfaces(Ljava/lang/String;)Z
    .locals 11
    .param p0, "iface"    # Ljava/lang/String;

    .line 918
    const-string v0, "NetworkBoostStatusManager"

    sget-object v1, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 919
    return v2

    .line 922
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v1

    .line 923
    .local v1, "teMethods":[Ljava/lang/reflect/Method;
    array-length v3, v1

    move v4, v2

    :goto_0
    if-ge v4, v3, :cond_4

    aget-object v5, v1, v4

    .line 924
    .local v5, "method":Ljava/lang/reflect/Method;
    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "getTetheredIfaces"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 926
    :try_start_0
    sget-object v6, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    new-array v7, v2, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    .line 927
    .local v6, "allTethered":[Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 928
    array-length v7, v6

    move v8, v2

    :goto_1
    if-ge v8, v7, :cond_2

    aget-object v9, v6, v8

    .line 929
    .local v9, "tethered":Ljava/lang/String;
    invoke-virtual {p0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 930
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is TetheredIfaces "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 931
    const/4 v0, 0x1

    return v0

    .line 928
    .end local v9    # "tethered":Ljava/lang/String;
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 938
    .end local v6    # "allTethered":[Ljava/lang/String;
    :cond_2
    goto :goto_2

    .line 935
    :catch_0
    move-exception v3

    .line 936
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 937
    return v2

    .line 923
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v5    # "method":Ljava/lang/reflect/Method;
    :cond_3
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 941
    :cond_4
    return v2
.end method

.method private isValidatePhoneId(I)Z
    .locals 1
    .param p1, "phoneId"    # I

    .line 1388
    if-ltz p1, :cond_0

    sget v0, Lcom/xiaomi/NetworkBoost/StatusManager;->PHONE_COUNT:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private onNetworkPriorityChanged(Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;)V
    .locals 6
    .param p1, "info"    # Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;

    .line 465
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkPriorityListenerList:Ljava/util/List;

    monitor-enter v0

    .line 466
    :try_start_0
    const-string v1, "NetworkBoostStatusManager"

    const-string v2, "CALLBACK_NETWORK_PRIORITY_CHANGED enter"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    iget v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPriorityMode:I

    iget v2, p1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->priorityMode:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTrafficPolicy:I

    iget v2, p1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->trafficPolicy:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mThermalLevel:I

    iget v2, p1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->thermalLevel:I

    if-ne v1, v2, :cond_0

    .line 471
    monitor-exit v0

    return-void

    .line 474
    :cond_0
    iget v1, p1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->priorityMode:I

    iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPriorityMode:I

    .line 475
    iget v1, p1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->trafficPolicy:I

    iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTrafficPolicy:I

    .line 476
    iget v1, p1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->thermalLevel:I

    iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mThermalLevel:I

    .line 478
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkPriorityListenerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;

    .line 479
    .local v2, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;
    iget v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPriorityMode:I

    iget v4, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTrafficPolicy:I

    iget v5, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mThermalLevel:I

    invoke-interface {v2, v3, v4, v5}, Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;->onNetworkPriorityChanged(III)V

    .line 480
    .end local v2    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;
    goto :goto_0

    .line 481
    :cond_1
    monitor-exit v0

    .line 482
    return-void

    .line 481
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private processRegisterCellularCallback()V
    .locals 12

    .line 1392
    sget-object v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const-string v1, "NetworkBoostStatusManager"

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mCellularNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    if-eqz v2, :cond_0

    .line 1394
    :try_start_0
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1397
    goto :goto_0

    .line 1395
    :catch_0
    move-exception v0

    .line 1396
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unregister network callback exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1399
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getDefaultDataSlotId()I

    move-result v0

    .line 1400
    .local v0, "defaultDataSlotId":I
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getViceDataSlotId()I

    move-result v2

    .line 1401
    .local v2, "viceDataSlotId":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "defaultDataSlotId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", viceDataSlotId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1402
    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->isValidatePhoneId(I)Z

    move-result v3

    const/4 v4, 0x4

    const/16 v5, 0xc

    const/16 v6, 0x10

    const/4 v7, 0x0

    const-string v8, ", subId = "

    if-eqz v3, :cond_1

    invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->isSlotActivated(I)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    if-eqz v3, :cond_1

    .line 1403
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I

    move-result v3

    .line 1404
    .local v3, "DataSubId":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "register default slot callback defaultslot = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1407
    new-instance v9, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v9}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 1408
    invoke-virtual {v9, v7}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v9

    .line 1409
    invoke-virtual {v9, v6}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v9

    .line 1410
    invoke-virtual {v9, v5}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v9

    .line 1411
    invoke-virtual {v9, v4}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v9

    .line 1413
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v10

    invoke-virtual {v10, v0}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    .line 1412
    invoke-virtual {v9, v10}, Landroid/net/NetworkRequest$Builder;->setNetworkSpecifier(Ljava/lang/String;)Landroid/net/NetworkRequest$Builder;

    move-result-object v9

    .line 1414
    invoke-virtual {v9}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v9

    .line 1415
    .local v9, "networkRequestCellular_0":Landroid/net/NetworkRequest;
    sget-object v10, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v11, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mCellularNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v10, v9, v11}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 1418
    .end local v3    # "DataSubId":I
    .end local v9    # "networkRequestCellular_0":Landroid/net/NetworkRequest;
    :cond_1
    invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->isValidatePhoneId(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->isSlotActivated(I)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    if-eqz v3, :cond_2

    .line 1419
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I

    move-result v3

    .line 1420
    .local v3, "SlaveDataSubId":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "register vice slot callback viceslot = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1423
    new-instance v1, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v1}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 1424
    invoke-virtual {v1, v7}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v1

    .line 1425
    invoke-virtual {v1, v6}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v1

    .line 1426
    invoke-virtual {v1, v5}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v1

    .line 1427
    invoke-virtual {v1, v4}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v1

    .line 1429
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v4

    invoke-virtual {v4, v2}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 1428
    invoke-virtual {v1, v4}, Landroid/net/NetworkRequest$Builder;->setNetworkSpecifier(Ljava/lang/String;)Landroid/net/NetworkRequest$Builder;

    move-result-object v1

    .line 1430
    invoke-virtual {v1}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v1

    .line 1431
    .local v1, "networkRequestCellular_1":Landroid/net/NetworkRequest;
    sget-object v4, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v5, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mCellularNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v4, v1, v5}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 1433
    .end local v1    # "networkRequestCellular_1":Landroid/net/NetworkRequest;
    .end local v3    # "SlaveDataSubId":I
    :cond_2
    return-void
.end method

.method private processSimStateChanged(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .line 1436
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getDefaultDataSlotId()I

    move-result v0

    .line 1437
    .local v0, "defaultDataSlotId":I
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getViceDataSlotId()I

    move-result v1

    .line 1438
    .local v1, "viceDataSlotId":I
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x67

    invoke-virtual {v2, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1439
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v4, 0x2710

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1441
    :cond_0
    return-void
.end method

.method private registerAudioModeChangedListener()V
    .locals 3

    .line 367
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mAudioManager:Landroid/media/AudioManager;

    .line 369
    new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$3;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$3;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mAudioModeListener:Landroid/media/AudioManager$OnModeChangedListener;

    .line 382
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mAudioManager:Landroid/media/AudioManager;

    .line 383
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mAudioModeListener:Landroid/media/AudioManager$OnModeChangedListener;

    .line 382
    invoke-virtual {v0, v1, v2}, Landroid/media/AudioManager;->addOnModeChangedListener(Ljava/util/concurrent/Executor;Landroid/media/AudioManager$OnModeChangedListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 386
    goto :goto_0

    .line 384
    :catch_0
    move-exception v0

    .line 387
    :goto_0
    return-void
.end method

.method private registerDefaultNetworkCallback()V
    .locals 2

    .line 994
    sget-object v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    .line 995
    return-void

    .line 998
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->registerDefaultNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 1000
    return-void
.end method

.method private registerForegroundInfoListener()V
    .locals 3

    .line 281
    :try_start_0
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkBoostProcessMonitor:Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;

    .line 282
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mForegroundInfoListener:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    goto :goto_0

    .line 283
    :catch_0
    move-exception v0

    .line 284
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NetworkBoostStatusManager"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private registerModemSignalStrengthObserver()V
    .locals 4

    .line 1229
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    .line 1230
    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 1231
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1233
    return-void
.end method

.method private registerMovementSensorListener()V
    .locals 5

    .line 1546
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorFlag:Z

    const-string v1, "NetworkBoostStatusManager"

    if-eqz v0, :cond_0

    .line 1547
    const-string v0, "other registerMovementSensorListener succeed!"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1548
    return-void

    .line 1551
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_1

    .line 1552
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "sensor"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 1555
    :cond_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorManager:Landroid/hardware/SensorManager;

    const v2, 0x1fa267e

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v0

    .line 1557
    .local v0, "movementSensor":Landroid/hardware/Sensor;
    if-nez v0, :cond_2

    .line 1558
    const-string v2, "registerMovementSensorListener is fail!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1559
    return-void

    .line 1562
    :cond_2
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorListener:Landroid/hardware/SensorEventListener;

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v0, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorFlag:Z

    .line 1564
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerMovementSensorListener sensorFlag:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorFlag:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1565
    return-void
.end method

.method private registerNetworkCallback()V
    .locals 13

    .line 619
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "NetworkInterfaceHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandlerThread:Landroid/os/HandlerThread;

    .line 620
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 621
    new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    .line 623
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    sput-object v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 624
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    sput-object v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 625
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    const-string v1, "SlaveWifiService"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/SlaveWifiManager;

    sput-object v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    .line 628
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->isDualDataSupported()Z

    move-result v0

    const-string v1, "NetworkBoostStatusManager"

    const/16 v2, 0xc

    const/16 v3, 0x10

    if-eqz v0, :cond_0

    .line 629
    const-string v0, "8550 platform version"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 632
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v4, 0x2710

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 635
    :cond_0
    const-string v0, "others platform version"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    new-instance v0, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 638
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    .line 639
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    .line 640
    invoke-virtual {v0, v3}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    .line 641
    invoke-virtual {v0, v2}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    .line 642
    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    .line 643
    .local v0, "networkRequestCellular":Landroid/net/NetworkRequest;
    sget-object v1, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mCellularNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v1, v0, v4}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 647
    .end local v0    # "networkRequestCellular":Landroid/net/NetworkRequest;
    :cond_1
    :goto_0
    new-instance v0, Landroid/net/wifi/WifiNetworkSpecifier$Builder;

    invoke-direct {v0}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;-><init>()V

    .line 648
    .local v0, "specifierBuilderWifi6G":Landroid/net/wifi/WifiNetworkSpecifier$Builder;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->setBand(I)Landroid/net/wifi/WifiNetworkSpecifier$Builder;

    .line 649
    new-instance v1, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v1}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 650
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v1

    .line 651
    invoke-virtual {v1, v2}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v1

    .line 652
    invoke-virtual {v1, v3}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v1

    .line 653
    const/16 v5, 0x11

    invoke-virtual {v1, v5}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v1

    .line 654
    const/16 v6, 0x18

    invoke-virtual {v1, v6}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v1

    .line 655
    invoke-virtual {v0}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->build()Landroid/net/wifi/WifiNetworkSpecifier;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/net/NetworkRequest$Builder;->setNetworkSpecifier(Landroid/net/NetworkSpecifier;)Landroid/net/NetworkRequest$Builder;

    move-result-object v1

    .line 656
    invoke-virtual {v1}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v1

    .line 657
    .local v1, "networkRequestWifi6G":Landroid/net/NetworkRequest;
    sget-object v7, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v8, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v7, v1, v8}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 660
    new-instance v7, Landroid/net/wifi/WifiNetworkSpecifier$Builder;

    invoke-direct {v7}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;-><init>()V

    .line 661
    .local v7, "specifierBuilderWifi5G":Landroid/net/wifi/WifiNetworkSpecifier$Builder;
    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->setBand(I)Landroid/net/wifi/WifiNetworkSpecifier$Builder;

    .line 662
    new-instance v8, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v8}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 663
    invoke-virtual {v8, v4}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v8

    .line 664
    invoke-virtual {v8, v2}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v8

    .line 665
    invoke-virtual {v8, v3}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v8

    .line 666
    invoke-virtual {v8, v5}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v8

    .line 667
    invoke-virtual {v8, v6}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v8

    .line 668
    invoke-virtual {v7}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->build()Landroid/net/wifi/WifiNetworkSpecifier;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/net/NetworkRequest$Builder;->setNetworkSpecifier(Landroid/net/NetworkSpecifier;)Landroid/net/NetworkRequest$Builder;

    move-result-object v8

    .line 669
    invoke-virtual {v8}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v8

    .line 670
    .local v8, "networkRequestWifi5G":Landroid/net/NetworkRequest;
    sget-object v9, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v10, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v9, v8, v10}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 673
    sget-object v9, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    if-eqz v9, :cond_2

    .line 674
    new-instance v9, Landroid/net/wifi/WifiNetworkSpecifier$Builder;

    invoke-direct {v9}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;-><init>()V

    .line 675
    .local v9, "specifierBuilderWifiHbs5G":Landroid/net/wifi/WifiNetworkSpecifier$Builder;
    const/16 v10, 0x102

    invoke-virtual {v9, v10}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->setBand(I)Landroid/net/wifi/WifiNetworkSpecifier$Builder;

    .line 676
    new-instance v10, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v10}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 677
    invoke-virtual {v10, v4}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v10

    .line 678
    invoke-virtual {v10, v2}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v10

    .line 679
    invoke-virtual {v10, v3}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v10

    .line 680
    invoke-virtual {v10, v5}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v10

    .line 681
    invoke-virtual {v10, v6}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v10

    .line 682
    invoke-virtual {v9}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->build()Landroid/net/wifi/WifiNetworkSpecifier;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/net/NetworkRequest$Builder;->setNetworkSpecifier(Landroid/net/NetworkSpecifier;)Landroid/net/NetworkRequest$Builder;

    move-result-object v10

    .line 683
    invoke-virtual {v10}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v10

    .line 684
    .local v10, "networkRequestWifiHbs5G":Landroid/net/NetworkRequest;
    sget-object v11, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v12, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v11, v10, v12}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 688
    .end local v9    # "specifierBuilderWifiHbs5G":Landroid/net/wifi/WifiNetworkSpecifier$Builder;
    .end local v10    # "networkRequestWifiHbs5G":Landroid/net/NetworkRequest;
    :cond_2
    new-instance v9, Landroid/net/wifi/WifiNetworkSpecifier$Builder;

    invoke-direct {v9}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;-><init>()V

    .line 689
    .local v9, "specifierBuilderWifi24G":Landroid/net/wifi/WifiNetworkSpecifier$Builder;
    invoke-virtual {v9, v4}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->setBand(I)Landroid/net/wifi/WifiNetworkSpecifier$Builder;

    .line 690
    new-instance v10, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v10}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 691
    invoke-virtual {v10, v4}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v4

    .line 692
    invoke-virtual {v4, v2}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v2

    .line 693
    invoke-virtual {v2, v3}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v2

    .line 694
    invoke-virtual {v2, v5}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v2

    .line 695
    invoke-virtual {v2, v6}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v2

    .line 696
    invoke-virtual {v9}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->build()Landroid/net/wifi/WifiNetworkSpecifier;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/NetworkRequest$Builder;->setNetworkSpecifier(Landroid/net/NetworkSpecifier;)Landroid/net/NetworkRequest$Builder;

    move-result-object v2

    .line 697
    invoke-virtual {v2}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v2

    .line 698
    .local v2, "networkRequestWifi24G":Landroid/net/NetworkRequest;
    sget-object v3, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v3, v2, v4}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 699
    return-void
.end method

.method private registerScreenStatusReceiver()V
    .locals 4

    .line 1139
    new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$8;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$8;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mScreenStatusReceiver:Landroid/content/BroadcastReceiver;

    .line 1160
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1161
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1162
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1163
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mScreenStatusReceiver:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 1165
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPowerManager:Landroid/os/PowerManager;

    .line 1166
    return-void
.end method

.method private registerTelephonyStatusReceiver()V
    .locals 3

    .line 1331
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->isDualDataSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1332
    new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$9;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$9;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTelephonyStatusReceiver:Landroid/content/BroadcastReceiver;

    .line 1342
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1343
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1344
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTelephonyStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1346
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private registerUidObserver()V
    .locals 5

    .line 321
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mActivityManager:Landroid/app/IActivityManager;

    .line 322
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mUidObserver:Landroid/app/IUidObserver;

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-interface {v0, v1, v4, v2, v3}, Landroid/app/IActivityManager;->registerUidObserver(Landroid/app/IUidObserver;IILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 326
    goto :goto_0

    .line 324
    :catch_0
    move-exception v0

    .line 325
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NetworkBoostStatusManager"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private setResloverInterfaceIdAndNetid(ILjava/lang/String;)V
    .locals 7
    .param p1, "netId"    # I
    .param p2, "interfaceId"    # Ljava/lang/String;

    .line 1526
    :try_start_0
    sget-object v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 1527
    .local v0, "ConnectivityManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v1, "setResloverInterfaceIdAndNetid"

    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-class v4, Ljava/lang/String;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    .line 1528
    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 1529
    .local v1, "setResloverInterfaceIdAndNetidMethod":Ljava/lang/reflect/Method;
    invoke-virtual {v1, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1530
    sget-object v3, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v5

    aput-object p2, v2, v6

    invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1533
    nop

    .end local v0    # "ConnectivityManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "setResloverInterfaceIdAndNetidMethod":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 1531
    :catch_0
    move-exception v0

    .line 1532
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1534
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private setSmartDNSInterface(Landroid/net/Network;)V
    .locals 13
    .param p1, "network"    # Landroid/net/Network;

    .line 1460
    const/4 v0, 0x0

    .line 1462
    .local v0, "wifiInfo":Landroid/net/wifi/WifiInfo;
    const-string v1, "NetworkBoostStatusManager"

    if-nez p1, :cond_0

    .line 1463
    const-string v2, "SmartDNS onAvailable parameter is null."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1464
    return-void

    .line 1467
    :cond_0
    sget-object v2, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2, p1}, Landroid/net/ConnectivityManager;->getNetworkInfo(Landroid/net/Network;)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1468
    .local v2, "networkInfo":Landroid/net/NetworkInfo;
    if-nez v2, :cond_1

    .line 1469
    const-string v3, "SmartDNS onAvailable getNetworkInfo error."

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1470
    return-void

    .line 1473
    :cond_1
    sget-object v3, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v3, p1}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;

    move-result-object v3

    .line 1474
    .local v3, "networkCapabilities":Landroid/net/NetworkCapabilities;
    if-eqz v3, :cond_2

    .line 1475
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/net/NetworkCapabilities;->hasTransport(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1476
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SmartDNS "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " onAvailable vpn."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1477
    return-void

    .line 1480
    :cond_2
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "WIFI"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1481
    sget-object v4, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getCurrentNetwork()Landroid/net/Network;

    move-result-object v4

    .line 1482
    .local v4, "currentNetwork":Landroid/net/Network;
    if-eqz v4, :cond_3

    invoke-virtual {v4, p1}, Landroid/net/Network;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1483
    const-string v5, "SmartDNS onAvailable WIFI master."

    invoke-static {v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1484
    sget-object v5, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    goto :goto_0

    .line 1486
    :cond_3
    const-string v5, "SmartDNS onAvailable WIFI slave"

    invoke-static {v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1487
    sget-object v5, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiManager:Landroid/net/wifi/SlaveWifiManager;

    if-eqz v5, :cond_4

    .line 1488
    invoke-virtual {v5}, Landroid/net/wifi/SlaveWifiManager;->getWifiSlaveConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 1490
    :cond_4
    :goto_0
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 1491
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SmartDNS push DnsReslover a relationship, netid "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1492
    invoke-virtual {p1}, Landroid/net/Network;->getNetId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " name "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1491
    invoke-static {v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1493
    invoke-virtual {p1}, Landroid/net/Network;->getNetId()I

    move-result v1

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v1, v5}, Lcom/xiaomi/NetworkBoost/StatusManager;->setResloverInterfaceIdAndNetid(ILjava/lang/String;)V

    .line 1495
    .end local v4    # "currentNetwork":Landroid/net/Network;
    :cond_5
    goto/16 :goto_2

    .line 1496
    :cond_6
    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    .line 1497
    const-string v5, "phone"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    .line 1498
    .local v4, "mTelephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v5

    .line 1500
    .local v5, "location":Landroid/telephony/CellLocation;
    instance-of v6, v5, Landroid/telephony/gsm/GsmCellLocation;

    const-string v7, "SmartDNS \t LAC = "

    const-wide/16 v8, 0x0

    if-eqz v6, :cond_7

    .line 1501
    move-object v6, v5

    check-cast v6, Landroid/telephony/gsm/GsmCellLocation;

    .line 1502
    .local v6, "gsmCellLocation":Landroid/telephony/gsm/GsmCellLocation;
    if-eqz v6, :cond_8

    .line 1503
    invoke-virtual {v6}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v10

    int-to-long v10, v10

    .line 1504
    .local v10, "lac":J
    invoke-virtual {v6}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v12

    .line 1505
    .local v12, "cellId":I
    cmp-long v8, v10, v8

    if-lez v8, :cond_8

    .line 1506
    invoke-virtual {p1}, Landroid/net/Network;->getNetId()I

    move-result v8

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v8, v9}, Lcom/xiaomi/NetworkBoost/StatusManager;->setResloverInterfaceIdAndNetid(ILjava/lang/String;)V

    .line 1507
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1510
    .end local v6    # "gsmCellLocation":Landroid/telephony/gsm/GsmCellLocation;
    .end local v10    # "lac":J
    .end local v12    # "cellId":I
    :cond_7
    instance-of v6, v5, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v6, :cond_8

    .line 1511
    move-object v6, v5

    check-cast v6, Landroid/telephony/cdma/CdmaCellLocation;

    .line 1512
    .local v6, "cdmaCellLocation":Landroid/telephony/cdma/CdmaCellLocation;
    if-eqz v6, :cond_9

    .line 1513
    invoke-virtual {v6}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    move-result v10

    int-to-long v10, v10

    .line 1514
    .restart local v10    # "lac":J
    invoke-virtual {v6}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    move-result v12

    .line 1515
    .restart local v12    # "cellId":I
    cmp-long v8, v10, v8

    if-lez v8, :cond_9

    .line 1516
    invoke-virtual {p1}, Landroid/net/Network;->getNetId()I

    move-result v8

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v8, v9}, Lcom/xiaomi/NetworkBoost/StatusManager;->setResloverInterfaceIdAndNetid(ILjava/lang/String;)V

    .line 1517
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1510
    .end local v6    # "cdmaCellLocation":Landroid/telephony/cdma/CdmaCellLocation;
    .end local v10    # "lac":J
    .end local v12    # "cellId":I
    :cond_8
    :goto_1
    nop

    .line 1522
    .end local v4    # "mTelephonyManager":Landroid/telephony/TelephonyManager;
    .end local v5    # "location":Landroid/telephony/CellLocation;
    :cond_9
    :goto_2
    return-void
.end method

.method private unregisterAudioModeChangedListener()V
    .locals 2

    .line 391
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mAudioModeListener:Landroid/media/AudioManager$OnModeChangedListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->removeOnModeChangedListener(Landroid/media/AudioManager$OnModeChangedListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 394
    goto :goto_0

    .line 392
    :catch_0
    move-exception v0

    .line 395
    :goto_0
    return-void
.end method

.method private unregisterDefaultNetworkCallback()V
    .locals 4

    .line 1003
    const-string v0, "NetworkBoostStatusManager"

    sget-object v1, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    if-nez v1, :cond_0

    .line 1004
    return-void

    .line 1007
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    if-eqz v2, :cond_1

    .line 1008
    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 1009
    const-string/jumbo v1, "unregisterefaultNetworkCallback"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1013
    :cond_1
    goto :goto_0

    .line 1011
    :catch_0
    move-exception v1

    .line 1012
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unregister default network callback exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :goto_0
    return-void
.end method

.method private unregisterForegroundInfoListener()V
    .locals 3

    .line 290
    :try_start_0
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkBoostProcessMonitor:Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;

    .line 291
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mForegroundInfoListener:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 294
    goto :goto_0

    .line 292
    :catch_0
    move-exception v0

    .line 293
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NetworkBoostStatusManager"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private unregisterModemSignalStrengthObserver()V
    .locals 2

    .line 1236
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1237
    return-void
.end method

.method private unregisterMovementSensorListener()V
    .locals 9

    .line 1569
    iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorFlag:Z

    if-nez v0, :cond_0

    .line 1570
    const-string v0, "NetworkBoostStatusManager"

    const-string v1, "other unregisterMovementSensorListener succeed!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1571
    return-void

    .line 1574
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_1

    .line 1575
    const-string v0, "NetworkBoostStatusManager"

    const-string/jumbo v1, "unregisterMovementSensorListener is fail!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1576
    return-void

    .line 1579
    :cond_1
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 1580
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorFlag:Z

    .line 1581
    const-string v1, "NetworkBoostStatusManager"

    const-string/jumbo v2, "unregisterMovementSensorListener"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1583
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorManager:Landroid/hardware/SensorManager;

    const v2, 0x1fa267e

    invoke-virtual {v1, v2, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v1

    .line 1584
    .local v1, "movementSensor":Landroid/hardware/Sensor;
    new-instance v2, Landroid/hardware/SensorEvent;

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v3, 0x1

    new-array v8, v3, [F

    const/4 v3, 0x0

    aput v3, v8, v0

    move-object v3, v2

    move-object v4, v1

    invoke-direct/range {v3 .. v8}, Landroid/hardware/SensorEvent;-><init>(Landroid/hardware/Sensor;IJ[F)V

    move-object v0, v2

    .line 1585
    .local v0, "event":Landroid/hardware/SensorEvent;
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mMovementSensorStatusListenerList:Ljava/util/List;

    monitor-enter v2

    .line 1586
    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mMovementSensorStatusListenerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;

    .line 1587
    .local v4, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;
    invoke-interface {v4, v0}, Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;->onSensorChanged(Landroid/hardware/SensorEvent;)V

    .line 1588
    .end local v4    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;
    goto :goto_0

    .line 1589
    :cond_2
    monitor-exit v2

    .line 1590
    return-void

    .line 1589
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private unregisterNetworkCallback()V
    .locals 4

    .line 702
    const-string v0, "NetworkBoostStatusManager"

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    .line 703
    sget-object v1, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    if-nez v1, :cond_0

    .line 704
    return-void

    .line 707
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    if-eqz v2, :cond_1

    .line 708
    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 709
    const-string/jumbo v1, "unregisterWifiNetworkCallback"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    :cond_1
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mCellularNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    if-eqz v1, :cond_2

    .line 712
    sget-object v2, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 713
    const-string/jumbo v1, "unregisterCellularNetworkCallback"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 717
    :cond_2
    goto :goto_0

    .line 715
    :catch_0
    move-exception v1

    .line 716
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unregister network callback exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :goto_0
    return-void
.end method

.method private unregisterScreenStatusReceiver()V
    .locals 2

    .line 1169
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mScreenStatusReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 1170
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1171
    const-string v0, "NetworkBoostStatusManager"

    const-string/jumbo v1, "unregisterScreenStatusReceiver"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1173
    :cond_0
    return-void
.end method

.method private unregisterTelephonyStatusReceiver()V
    .locals 2

    .line 1349
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTelephonyStatusReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 1350
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1351
    const-string v0, "NetworkBoostStatusManager"

    const-string/jumbo v1, "unregisterTelephonyStatusReceiver"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1353
    :cond_0
    return-void
.end method

.method private unregisterUidObserver()V
    .locals 3

    .line 331
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mActivityManager:Landroid/app/IActivityManager;

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mUidObserver:Landroid/app/IUidObserver;

    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->unregisterUidObserver(Landroid/app/IUidObserver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 334
    goto :goto_0

    .line 332
    :catch_0
    move-exception v0

    .line 333
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NetworkBoostStatusManager"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public dumpModule(Ljava/io/PrintWriter;)V
    .locals 6
    .param p1, "writer"    # Ljava/io/PrintWriter;

    .line 1639
    const-string v0, "NetworkBoostStatusManager end.\n"

    const-string v1, "    "

    .line 1641
    .local v1, "spacePrefix":Ljava/lang/String;
    :try_start_0
    const-string v2, "NetworkBoostStatusManager begin:"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1642
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "WifiReady:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiReady:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1643
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SlaveWifiReady:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiReady:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1644
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "DataReady:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1645
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SlaveDataReady:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1646
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mInterface:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mInterface:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1647
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mIfaceNumber:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1648
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mPriorityMode:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPriorityMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1649
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mTrafficPolicy:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTrafficPolicy:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1650
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mThermalLevel:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mThermalLevel:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1651
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mAppStatusListenerList:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mAppStatusListenerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1652
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mNetworkInterfaceListenerList:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkInterfaceListenerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1653
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mScreenStatusListenerList:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mScreenStatusListenerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1654
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mNetworkPriorityListenerList:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkPriorityListenerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1655
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1666
    goto :goto_0

    .line 1657
    :catch_0
    move-exception v2

    .line 1658
    .local v2, "ex":Ljava/lang/Exception;
    const-string v3, "dump failed!"

    const-string v4, "NetworkBoostStatusManager"

    invoke-static {v4, v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1660
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Dump of NetworkBoostStatusManager failed:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1661
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1665
    goto :goto_0

    .line 1663
    :catch_1
    move-exception v0

    .line 1664
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "dump failure failed:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1667
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "ex":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public getAvailableIfaces()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 909
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 910
    .local v0, "availableIfaces":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mInterface:Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 911
    .local v1, "interfaces":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    .line 912
    aget-object v3, v1, v2

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 911
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 914
    .end local v2    # "i":I
    :cond_0
    return-object v0
.end method

.method public getForegroundUid()I
    .locals 2

    .line 313
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mAppStatusListenerList:Ljava/util/List;

    monitor-enter v0

    .line 314
    :try_start_0
    iget v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mFUid:I

    monitor-exit v0

    return v1

    .line 315
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getNetworkBoostWatchdogHandler()Landroid/os/HandlerThread;
    .locals 1

    .line 275
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWacthdogHandlerThread:Landroid/os/HandlerThread;

    return-object v0
.end method

.method public getNetworkPriorityInfo()Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;
    .locals 5

    .line 443
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkPriorityListenerList:Ljava/util/List;

    monitor-enter v0

    .line 444
    :try_start_0
    new-instance v1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;

    iget v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPriorityMode:I

    iget v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTrafficPolicy:I

    iget v4, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mThermalLevel:I

    invoke-direct {v1, v2, v3, v4}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;-><init>(III)V

    monitor-exit v0

    return-object v1

    .line 445
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isPoorSignal(I)Z
    .locals 2
    .param p1, "curRsrp"    # I

    .line 1297
    const/4 v0, 0x0

    .line 1298
    .local v0, "rst":Z
    const v1, 0x7fffffff

    if-ne p1, v1, :cond_0

    .line 1299
    return v0

    .line 1301
    :cond_0
    const/16 v1, -0x69

    if-gt p1, v1, :cond_1

    .line 1302
    const/4 v0, 0x1

    .line 1304
    :cond_1
    return v0
.end method

.method public onCreate()V
    .locals 2

    .line 242
    const-string v0, "NetworkBoostStatusManager"

    const-string v1, "onCreate "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "NetoworkBoostWacthdogHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWacthdogHandlerThread:Landroid/os/HandlerThread;

    .line 245
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 247
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerNetworkCallback()V

    .line 248
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerVPNCallback()V

    .line 249
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerDefaultNetworkCallback()V

    .line 250
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerForegroundInfoListener()V

    .line 251
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerUidObserver()V

    .line 252
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerAudioModeChangedListener()V

    .line 253
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerScreenStatusReceiver()V

    .line 254
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerModemSignalStrengthObserver()V

    .line 255
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerTelephonyStatusReceiver()V

    .line 256
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 259
    const-string v0, "NetworkBoostStatusManager"

    const-string v1, "onDestroy "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWacthdogHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 263
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterNetworkCallback()V

    .line 264
    invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterVPNCallback()V

    .line 265
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterDefaultNetworkCallback()V

    .line 266
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterForegroundInfoListener()V

    .line 267
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterUidObserver()V

    .line 268
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterAudioModeChangedListener()V

    .line 269
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterScreenStatusReceiver()V

    .line 270
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterModemSignalStrengthObserver()V

    .line 271
    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterTelephonyStatusReceiver()V

    .line 272
    return-void
.end method

.method public onNetworkPriorityChanged(III)V
    .locals 3
    .param p1, "priorityMode"    # I
    .param p2, "trafficPolicy"    # I
    .param p3, "thermalLevel"    # I

    .line 424
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onNetworkPriorityChanged: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NetworkBoostStatusManager"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;

    invoke-direct {v1, p1, p2, p3}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;-><init>(III)V

    const/16 v2, 0x6d

    invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 427
    return-void
.end method

.method public registerAppStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;

    .line 399
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mAppStatusListenerList:Ljava/util/List;

    monitor-enter v0

    .line 400
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mAppStatusListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 401
    monitor-exit v0

    .line 402
    return-void

    .line 401
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerDefaultNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;

    .line 1043
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetworkInterfaceListenerList:Ljava/util/List;

    monitor-enter v0

    .line 1044
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetworkInterfaceListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1045
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x6f

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1046
    monitor-exit v0

    .line 1047
    return-void

    .line 1046
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerModemSignalStrengthListener(Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;

    .line 1202
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mModemSignalStrengthListener:Ljava/util/List;

    monitor-enter v0

    .line 1203
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mModemSignalStrengthListener:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1204
    monitor-exit v0

    .line 1205
    return-void

    .line 1204
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerMovementSensorStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;

    .line 1614
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mMovementSensorStatusListenerList:Ljava/util/List;

    monitor-enter v0

    .line 1615
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mMovementSensorStatusListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1616
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mCurrentSensorevent:Landroid/hardware/SensorEvent;

    if-eqz v1, :cond_0

    .line 1617
    invoke-interface {p1, v1}, Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;->onSensorChanged(Landroid/hardware/SensorEvent;)V

    .line 1619
    :cond_0
    monitor-exit v0

    .line 1620
    return-void

    .line 1619
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    .line 945
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkInterfaceListenerList:Ljava/util/List;

    monitor-enter v0

    .line 946
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkInterfaceListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 947
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x6b

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 948
    monitor-exit v0

    .line 949
    return-void

    .line 948
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerNetworkPriorityListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;

    .line 430
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkPriorityListenerList:Ljava/util/List;

    monitor-enter v0

    .line 431
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkPriorityListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 432
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x6e

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 433
    monitor-exit v0

    .line 434
    return-void

    .line 433
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    .line 1176
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mScreenStatusListenerList:Ljava/util/List;

    monitor-enter v0

    .line 1177
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mScreenStatusListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1178
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x6c

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1179
    monitor-exit v0

    .line 1180
    return-void

    .line 1179
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method registerVPNCallback()V
    .locals 3

    .line 1069
    sget-object v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    .line 1070
    return-void

    .line 1072
    :cond_0
    new-instance v0, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 1073
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    .line 1074
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    .line 1075
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    .line 1076
    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    .line 1078
    .local v0, "vpnRequest":Landroid/net/NetworkRequest;
    sget-object v1, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v1, v0, v2}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 1079
    return-void
.end method

.method public registerVPNListener(Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;

    .line 1114
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNListener:Ljava/util/List;

    monitor-enter v0

    .line 1115
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNListener:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1116
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x70

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1117
    monitor-exit v0

    .line 1118
    return-void

    .line 1117
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unregisterAppStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;

    .line 405
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mAppStatusListenerList:Ljava/util/List;

    monitor-enter v0

    .line 406
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mAppStatusListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 407
    monitor-exit v0

    .line 408
    return-void

    .line 407
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unregisterDefaultNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;

    .line 1050
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetworkInterfaceListenerList:Ljava/util/List;

    monitor-enter v0

    .line 1051
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetworkInterfaceListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1052
    monitor-exit v0

    .line 1053
    return-void

    .line 1052
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unregisterModemSignalStrengthListener(Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;

    .line 1208
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mModemSignalStrengthListener:Ljava/util/List;

    monitor-enter v0

    .line 1209
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mModemSignalStrengthListener:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1210
    monitor-exit v0

    .line 1211
    return-void

    .line 1210
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unregisterMovementSensorStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;

    .line 1622
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mMovementSensorStatusListenerList:Ljava/util/List;

    monitor-enter v0

    .line 1623
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mMovementSensorStatusListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1624
    monitor-exit v0

    .line 1625
    return-void

    .line 1624
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unregisterNetworkInterfaceListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    .line 952
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkInterfaceListenerList:Ljava/util/List;

    monitor-enter v0

    .line 953
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkInterfaceListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 954
    monitor-exit v0

    .line 955
    return-void

    .line 954
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unregisterNetworkPriorityListener(Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;

    .line 437
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkPriorityListenerList:Ljava/util/List;

    monitor-enter v0

    .line 438
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mNetworkPriorityListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 439
    monitor-exit v0

    .line 440
    return-void

    .line 439
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unregisterScreenStatusListener(Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    .line 1183
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mScreenStatusListenerList:Ljava/util/List;

    monitor-enter v0

    .line 1184
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mScreenStatusListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1185
    monitor-exit v0

    .line 1186
    return-void

    .line 1185
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method unregisterVPNCallback()V
    .locals 2

    .line 1082
    sget-object v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    .line 1083
    return-void

    .line 1085
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 1086
    return-void
.end method

.method public unregisterVPNListener(Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;

    .line 1121
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNListener:Ljava/util/List;

    monitor-enter v0

    .line 1122
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNListener:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1123
    monitor-exit v0

    .line 1124
    return-void

    .line 1123
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
