.class public final Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
.super Ljava/lang/Object;
.source "NetLinkLayerQoE.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;",
            ">;"
        }
    .end annotation
.end field

.field public static singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;


# instance fields
.field private bitRateInKbps:I

.field private bw:I

.field private ccaBusyTimeMs:I

.field private frequency:I

.field private lostmpdu_be:J

.field private lostmpdu_bk:J

.field private lostmpdu_vi:J

.field private lostmpdu_vo:J

.field private mpduLostRatio:D

.field private radioOnTimeMs:I

.field private rateMcsIdx:I

.field private retriesRatio:D

.field private retries_be:J

.field private retries_bk:J

.field private retries_vi:J

.field private retries_vo:J

.field private rssi_mgmt:I

.field private rxmpdu_be:J

.field private rxmpdu_bk:J

.field private rxmpdu_vi:J

.field private rxmpdu_vo:J

.field private ssid:Ljava/lang/String;

.field private txmpdu_be:J

.field private txmpdu_bk:J

.field private txmpdu_vi:J

.field private txmpdu_vo:J

.field private version:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE$a;

    invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE$a;-><init>()V

    sput-object v0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->version:Ljava/lang/String;

    .line 3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->ssid:Ljava/lang/String;

    .line 4
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->mpduLostRatio:D

    .line 5
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retriesRatio:D

    .line 7
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rssi_mgmt:I

    .line 8
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->frequency:I

    .line 9
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->radioOnTimeMs:I

    .line 10
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->ccaBusyTimeMs:I

    .line 11
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->bw:I

    .line 12
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rateMcsIdx:I

    .line 13
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->bitRateInKbps:I

    .line 15
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_be:J

    .line 16
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_be:J

    .line 17
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_be:J

    .line 18
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_be:J

    .line 19
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_bk:J

    .line 20
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_bk:J

    .line 21
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_bk:J

    .line 22
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_bk:J

    .line 23
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_vi:J

    .line 24
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_vi:J

    .line 25
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_vi:J

    .line 26
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_vi:J

    .line 27
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_vo:J

    .line 28
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_vo:J

    .line 29
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_vo:J

    .line 30
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_vo:J

    return-void
.end method

.method public static synthetic access$000(Landroid/os/Parcel;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->creatSingleClass(Landroid/os/Parcel;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    move-result-object p0

    return-object p0
.end method

.method private static declared-synchronized copyFrom(Landroid/os/Parcel;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .locals 4

    const-class v0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setVersion(Ljava/lang/String;)V

    .line 2
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setSsid(Ljava/lang/String;)V

    .line 4
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setMpduLostRatio(D)V

    .line 5
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetriesRatio(D)V

    .line 7
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRssi_mgmt(I)V

    .line 8
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setFrequency(I)V

    .line 9
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRadioOnTimeMs(I)V

    .line 10
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setCcaBusyTimeMs(I)V

    .line 12
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setBw(I)V

    .line 13
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRateMcsIdx(I)V

    .line 14
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setBitRateInKbps(I)V

    .line 16
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRxmpdu_be(J)V

    .line 17
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setTxmpdu_be(J)V

    .line 18
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setLostmpdu_be(J)V

    .line 19
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetries_be(J)V

    .line 21
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRxmpdu_bk(J)V

    .line 22
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setTxmpdu_bk(J)V

    .line 23
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setLostmpdu_bk(J)V

    .line 24
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetries_bk(J)V

    .line 26
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRxmpdu_vi(J)V

    .line 27
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setTxmpdu_vi(J)V

    .line 28
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setLostmpdu_vi(J)V

    .line 29
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetries_vi(J)V

    .line 31
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRxmpdu_vo(J)V

    .line 32
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setTxmpdu_vo(J)V

    .line 33
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setLostmpdu_vo(J)V

    .line 34
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->setRetries_vo(J)V

    .line 36
    sget-object p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    .line 0
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private static declared-synchronized creatSingleClass(Landroid/os/Parcel;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .locals 2

    const-class v0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;-><init>(Landroid/os/Parcel;)V

    sput-object v1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->singleClass:Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 5
    :cond_0
    :try_start_1
    invoke-static {p0}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->copyFrom(Landroid/os/Parcel;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    move-result-object p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    return-object p0

    .line 0
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getBitRateInKbps()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->bitRateInKbps:I

    return v0
.end method

.method public getBw()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->bw:I

    return v0
.end method

.method public getCcaBusyTimeMs()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->ccaBusyTimeMs:I

    return v0
.end method

.method public getFrequency()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->frequency:I

    return v0
.end method

.method public getLostmpdu_be()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_be:J

    return-wide v0
.end method

.method public getLostmpdu_bk()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_bk:J

    return-wide v0
.end method

.method public getLostmpdu_vi()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_vi:J

    return-wide v0
.end method

.method public getLostmpdu_vo()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_vo:J

    return-wide v0
.end method

.method public getMpduLostRatio()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->mpduLostRatio:D

    return-wide v0
.end method

.method public getRadioOnTimeMs()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->radioOnTimeMs:I

    return v0
.end method

.method public getRateMcsIdx()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rateMcsIdx:I

    return v0
.end method

.method public getRetriesRatio()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retriesRatio:D

    return-wide v0
.end method

.method public getRetries_be()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_be:J

    return-wide v0
.end method

.method public getRetries_bk()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_bk:J

    return-wide v0
.end method

.method public getRetries_vi()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_vi:J

    return-wide v0
.end method

.method public getRetries_vo()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_vo:J

    return-wide v0
.end method

.method public getRssi_mgmt()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rssi_mgmt:I

    return v0
.end method

.method public getRxmpdu_be()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_be:J

    return-wide v0
.end method

.method public getRxmpdu_bk()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_bk:J

    return-wide v0
.end method

.method public getRxmpdu_vi()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_vi:J

    return-wide v0
.end method

.method public getRxmpdu_vo()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_vo:J

    return-wide v0
.end method

.method public getSsid()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->ssid:Ljava/lang/String;

    return-object v0
.end method

.method public getTxmpdu_be()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_be:J

    return-wide v0
.end method

.method public getTxmpdu_bk()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_bk:J

    return-wide v0
.end method

.method public getTxmpdu_vi()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_vi:J

    return-wide v0
.end method

.method public getTxmpdu_vo()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_vo:J

    return-wide v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->version:Ljava/lang/String;

    return-object v0
.end method

.method public setBitRateInKbps(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->bitRateInKbps:I

    return-void
.end method

.method public setBw(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->bw:I

    return-void
.end method

.method public setCcaBusyTimeMs(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->ccaBusyTimeMs:I

    return-void
.end method

.method public setFrequency(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->frequency:I

    return-void
.end method

.method public setLostmpdu_be(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_be:J

    return-void
.end method

.method public setLostmpdu_bk(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_bk:J

    return-void
.end method

.method public setLostmpdu_vi(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_vi:J

    return-void
.end method

.method public setLostmpdu_vo(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_vo:J

    return-void
.end method

.method public setMpduLostRatio(D)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->mpduLostRatio:D

    return-void
.end method

.method public setRadioOnTimeMs(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->radioOnTimeMs:I

    return-void
.end method

.method public setRateMcsIdx(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rateMcsIdx:I

    return-void
.end method

.method public setRetriesRatio(D)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retriesRatio:D

    return-void
.end method

.method public setRetries_be(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_be:J

    return-void
.end method

.method public setRetries_bk(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_bk:J

    return-void
.end method

.method public setRetries_vi(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_vi:J

    return-void
.end method

.method public setRetries_vo(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_vo:J

    return-void
.end method

.method public setRssi_mgmt(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rssi_mgmt:I

    return-void
.end method

.method public setRxmpdu_be(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_be:J

    return-void
.end method

.method public setRxmpdu_bk(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_bk:J

    return-void
.end method

.method public setRxmpdu_vi(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_vi:J

    return-void
.end method

.method public setRxmpdu_vo(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_vo:J

    return-void
.end method

.method public setSsid(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->ssid:Ljava/lang/String;

    return-void
.end method

.method public setTxmpdu_be(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_be:J

    return-void
.end method

.method public setTxmpdu_bk(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_bk:J

    return-void
.end method

.method public setTxmpdu_vi(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_vi:J

    return-void
.end method

.method public setTxmpdu_vo(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_vo:J

    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->version:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NetLinkLayerQoE{version=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ssid=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->ssid:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rssi_mgmt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rssi_mgmt:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", frequency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->frequency:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mpduLostRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->mpduLostRatio:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retriesRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retriesRatio:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", radioOnTimeMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->radioOnTimeMs:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ccaBusyTimeMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->ccaBusyTimeMs:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bw="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->bw:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rateMcsIdx="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rateMcsIdx:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bitRateInKbps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->bitRateInKbps:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rxmpdu_be="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_be:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", txmpdu_be="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_be:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lostmpdu_be="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_be:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retries_be="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_be:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rxmpdu_bk="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_bk:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", txmpdu_bk="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_bk:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lostmpdu_bk="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_bk:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retries_bk="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_bk:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rxmpdu_vi="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_vi:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", txmpdu_vi="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_vi:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lostmpdu_vi="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_vi:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retries_vi="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_vi:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rxmpdu_vo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_vo:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", txmpdu_vo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_vo:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lostmpdu_vo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_vo:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retries_vo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_vo:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 1
    iget-object p2, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->version:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2
    iget-object p2, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->ssid:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->mpduLostRatio:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 5
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retriesRatio:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 7
    iget p2, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rssi_mgmt:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 8
    iget p2, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->frequency:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 9
    iget p2, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->radioOnTimeMs:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 10
    iget p2, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->ccaBusyTimeMs:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 11
    iget p2, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->bw:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 12
    iget p2, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rateMcsIdx:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 13
    iget p2, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->bitRateInKbps:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 15
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_be:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 16
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_be:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 17
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_be:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 18
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_be:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 19
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_bk:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 20
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_bk:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 21
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_bk:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 22
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_bk:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 23
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_vi:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 24
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_vi:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 25
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_vi:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 26
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_vi:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 27
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->rxmpdu_vo:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 28
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->txmpdu_vo:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 29
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->lostmpdu_vo:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 30
    iget-wide v0, p0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;->retries_vo:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
