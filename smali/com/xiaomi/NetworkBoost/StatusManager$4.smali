.class Lcom/xiaomi/NetworkBoost/StatusManager$4;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "StatusManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/StatusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/StatusManager;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 721
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$4;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAvailable(Landroid/net/Network;)V
    .locals 3
    .param p1, "network"    # Landroid/net/Network;

    .line 725
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$4;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/StatusManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$4;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/StatusManager;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x64

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 727
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$4;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$msetSmartDNSInterface(Lcom/xiaomi/NetworkBoost/StatusManager;Landroid/net/Network;)V

    .line 728
    return-void
.end method

.method public onCapabilitiesChanged(Landroid/net/Network;Landroid/net/NetworkCapabilities;)V
    .locals 0
    .param p1, "network"    # Landroid/net/Network;
    .param p2, "networkCapabilities"    # Landroid/net/NetworkCapabilities;

    .line 744
    return-void
.end method

.method public onLinkPropertiesChanged(Landroid/net/Network;Landroid/net/LinkProperties;)V
    .locals 0
    .param p1, "network"    # Landroid/net/Network;
    .param p2, "linkProperties"    # Landroid/net/LinkProperties;

    .line 738
    return-void
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 3
    .param p1, "network"    # Landroid/net/Network;

    .line 732
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$4;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/StatusManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$4;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/StatusManager;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x65

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 733
    return-void
.end method
