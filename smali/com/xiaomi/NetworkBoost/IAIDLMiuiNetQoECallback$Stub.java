public abstract class com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback$Stub extends android.os.Binder implements com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback {
	 /* .source "IAIDLMiuiNetQoECallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback$Stub$a; */
/* } */
} // .end annotation
/* # direct methods */
public com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback$Stub ( ) {
/* .locals 1 */
/* .line 1 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 2 */
final String v0 = "com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback"; // const-string v0, "com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback"
(( android.os.Binder ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Landroid/os/Binder;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return;
} // .end method
public static com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* if-nez p0, :cond_0 */
int p0 = 0; // const/4 p0, 0x0
/* .line 1 */
} // :cond_0
final String v0 = "com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback"; // const-string v0, "com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback"
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2 */
/* instance-of v1, v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 3 */
/* check-cast v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback; */
/* .line 5 */
} // :cond_1
/* new-instance v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback$Stub$a; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback$Stub$a;-><init>(Landroid/os/IBinder;)V */
} // .end method
public static com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback getDefaultImpl ( ) {
/* .locals 1 */
/* .line 1 */
v0 = com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback$Stub$a.b;
} // .end method
public static Boolean setDefaultImpl ( com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback p0 ) {
/* .locals 1 */
/* .line 1 */
v0 = com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback$Stub$a.b;
/* if-nez v0, :cond_1 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 5 */
int p0 = 1; // const/4 p0, 0x1
} // :cond_0
int p0 = 0; // const/4 p0, 0x0
/* .line 6 */
} // :cond_1
/* new-instance p0, Ljava/lang/IllegalStateException; */
/* const-string/jumbo v0, "setDefaultImpl() called twice" */
/* invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw p0 */
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1 */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback"; // const-string v1, "com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback"
int v2 = 1; // const/4 v2, 0x1
/* if-eq p1, v2, :cond_3 */
int v3 = 2; // const/4 v3, 0x2
/* if-eq p1, v3, :cond_1 */
/* const v0, 0x5f4e5446 */
/* if-eq p1, v0, :cond_0 */
/* .line 37 */
p1 = /* invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 38 */
} // :cond_0
(( android.os.Parcel ) p3 ).writeString ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 56 */
} // :cond_1
(( android.os.Parcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 58 */
p1 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 59 */
p1 = com.xiaomi.NetworkBoost.NetLinkLayerQoE.CREATOR;
/* move-object v0, p1 */
/* check-cast v0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
} // :cond_2
/* nop */
/* .line 64 */
} // :goto_0
/* .line 65 */
} // :cond_3
(( android.os.Parcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 67 */
p1 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
if ( p1 != null) { // if-eqz p1, :cond_4
/* .line 68 */
p1 = com.xiaomi.NetworkBoost.NetLinkLayerQoE.CREATOR;
/* move-object v0, p1 */
/* check-cast v0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
} // :cond_4
/* nop */
/* .line 73 */
} // :goto_1
} // .end method
