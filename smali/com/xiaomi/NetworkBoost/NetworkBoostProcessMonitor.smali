.class public Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;
.super Ljava/lang/Object;
.source "NetworkBoostProcessMonitor.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private method_getForegroundInfo:Ljava/lang/reflect/Method;

.field private method_registerForegroundInfoListener:Ljava/lang/reflect/Method;

.field private method_unregisterForegroundInfoListener:Ljava/lang/reflect/Method;

.field private processManager:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    const-string v0, "NetworkBoostProcessMonitor"

    sput-object v0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->TAG:Ljava/lang/String;

    const-string v1, "NetworkBoostProcessMonitor"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 21
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 23
    .local v0, "classLoader":Ljava/lang/ClassLoader;
    :try_start_0
    const-string v1, "miui.process.ProcessManager"

    invoke-virtual {v0, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->processManager:Ljava/lang/Class;

    .line 24
    if-eqz v1, :cond_0

    .line 25
    const-string v2, "getForegroundInfo"

    const/4 v3, 0x0

    move-object v4, v3

    check-cast v4, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->method_getForegroundInfo:Ljava/lang/reflect/Method;

    .line 26
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 27
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->processManager:Ljava/lang/Class;

    const-string v3, "registerForegroundInfoListener"

    new-array v4, v2, [Ljava/lang/Class;

    const-class v5, Lmiui/process/IForegroundInfoListener;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->method_registerForegroundInfoListener:Ljava/lang/reflect/Method;

    .line 29
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->processManager:Ljava/lang/Class;

    const-string/jumbo v3, "unregisterForegroundInfoListener"

    new-array v4, v2, [Ljava/lang/Class;

    const-class v5, Lmiui/process/IForegroundInfoListener;

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->method_unregisterForegroundInfoListener:Ljava/lang/reflect/Method;

    .line 31
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->method_registerForegroundInfoListener:Ljava/lang/reflect/Method;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    goto :goto_0

    .line 33
    :cond_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->TAG:Ljava/lang/String;

    const-string v2, "processManager is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :goto_0
    goto :goto_1

    .line 35
    :catch_0
    move-exception v1

    .line 36
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NetworkBoostProcessMonitor don\'t support ProcessManager"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method


# virtual methods
.method public getForegroundInfo()Lmiui/process/ForegroundInfo;
    .locals 5

    .line 42
    const/4 v0, 0x0

    .line 44
    .local v0, "foregroundInfo":Lmiui/process/ForegroundInfo;
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->method_getForegroundInfo:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    move-object v3, v2

    check-cast v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/process/ForegroundInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 47
    goto :goto_0

    .line 45
    :catch_0
    move-exception v1

    .line 46
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getForegroundInfo Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-object v0
.end method

.method public registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V
    .locals 4
    .param p1, "iForegroundInfoListener"    # Lmiui/process/IForegroundInfoListener;

    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->method_registerForegroundInfoListener:Ljava/lang/reflect/Method;

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    goto :goto_0

    .line 54
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerForegroundInfoListener Exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V
    .locals 4
    .param p1, "iForegroundInfoListener"    # Lmiui/process/IForegroundInfoListener;

    .line 61
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->method_unregisterForegroundInfoListener:Ljava/lang/reflect/Method;

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    goto :goto_0

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unregisterForegroundInfoListener Exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
