public class com.xiaomi.NetworkBoost.NetworkBoostManager {
	 /* .source "NetworkBoostManager.java" */
	 /* # instance fields */
	 public com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost a;
	 public com.xiaomi.NetworkBoost.NetworkBoostManager$a b;
	 public final android.content.Context c;
	 public java.lang.Object d;
	 public com.xiaomi.NetworkBoost.ServiceCallback e;
	 /* # direct methods */
	 public com.xiaomi.NetworkBoost.NetworkBoostManager ( ) {
		 /* .locals 1 */
		 /* .line 1 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 2 */
		 /* new-instance v0, Ljava/lang/Object; */
		 /* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
		 this.d = v0;
		 /* .line 14 */
		 this.c = p1;
		 /* .line 15 */
		 this.e = p2;
		 /* .line 16 */
		 /* new-instance p1, Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a; */
		 /* invoke-direct {p1, p0}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager$a;-><init>(Lcom/xiaomi/NetworkBoost/NetworkBoostManager;)V */
		 this.b = p1;
		 return;
	 } // .end method
	 public static Integer a ( java.io.FileDescriptor p0 ) {
		 /* .locals 3 */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Ljava/io/IOException; */
		 /* } */
	 } // .end annotation
	 /* .line 1 */
	 try { // :try_start_0
		 v0 = 		 (( java.io.FileDescriptor ) p0 ).valid ( ); // invoke-virtual {p0}, Ljava/io/FileDescriptor;->valid()Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 8 */
			 final String v0 = "fd"; // const-string v0, "fd"
			 /* .line 9 */
			 /* const-class v1, Ljava/io/FileDescriptor; */
			 (( java.lang.Class ) v1 ).getDeclaredField ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
			 /* .line 10 */
			 int v1 = 1; // const/4 v1, 0x1
			 (( java.lang.reflect.AccessibleObject ) v0 ).setAccessible ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V
			 /* .line 11 */
			 p0 = 			 (( java.lang.reflect.Field ) v0 ).getInt ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
			 /* int-to-long v1, p0 */
			 /* .line 12 */
			 int p0 = 0; // const/4 p0, 0x0
			 (( java.lang.reflect.AccessibleObject ) v0 ).setAccessible ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V
			 /* :try_end_0 */
			 /* .catch Ljava/lang/IllegalAccessException; {:try_start_0 ..:try_end_0} :catch_1 */
			 /* .catch Ljava/lang/NoSuchFieldException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* long-to-int p0, v1 */
		 } // :cond_0
		 int p0 = -1; // const/4 p0, -0x1
		 /* :catch_0 */
		 /* move-exception p0 */
		 /* .line 13 */
		 /* new-instance v0, Ljava/io/IOException; */
		 final String v1 = "FileDescriptor in this JVM lacks handle/fd fields"; // const-string v1, "FileDescriptor in this JVM lacks handle/fd fields"
		 /* invoke-direct {v0, v1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V */
		 /* throw v0 */
		 /* :catch_1 */
		 /* move-exception p0 */
		 /* .line 14 */
		 /* new-instance v0, Ljava/io/IOException; */
		 /* const-string/jumbo v1, "unable to access handle/fd fields in FileDescriptor" */
		 /* invoke-direct {v0, v1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V */
		 /* throw v0 */
	 } // .end method
	 public static Integer getSDKVersion ( ) {
		 /* .locals 1 */
		 int v0 = 6; // const/4 v0, 0x6
	 } // .end method
	 public static Integer getServiceVersion ( ) {
		 /* .locals 1 */
		 /* .line 1 */
		 v0 = 		 com.xiaomi.NetworkBoost.Version .getServiceVersion ( );
	 } // .end method
	 /* # virtual methods */
	 public Boolean abortScan ( ) {
		 /* .locals 2 */
		 /* .line 1 */
		 int v0 = 3; // const/4 v0, 0x3
		 v0 = 		 com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
		 int v1 = 0; // const/4 v1, 0x0
		 /* if-nez v0, :cond_0 */
		 /* .line 6 */
	 } // :cond_0
	 v0 = this.a;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 8 */
			 v0 = 		 try { // :try_start_0
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 10 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 (( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
		 } // :cond_1
	 } // .end method
	 public Boolean activeScan ( Integer[] p0 ) {
		 /* .locals 2 */
		 /* .line 1 */
		 int v0 = 3; // const/4 v0, 0x3
		 v0 = 		 com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
		 int v1 = 0; // const/4 v1, 0x0
		 /* if-nez v0, :cond_0 */
		 /* .line 6 */
	 } // :cond_0
	 v0 = this.a;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 8 */
			 p1 = 		 try { // :try_start_0
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 10 */
			 /* :catch_0 */
			 /* move-exception p1 */
			 (( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
		 } // :cond_1
	 } // .end method
	 public Boolean bindService ( ) {
		 /* .locals 7 */
		 /* .line 1 */
		 /* const-string/jumbo v0, "xiaomi.NetworkBoostServiceManager" */
		 /* .line 4 */
		 int v1 = 0; // const/4 v1, 0x0
		 int v2 = 1; // const/4 v2, 0x1
		 try { // :try_start_0
			 final String v3 = "android.os.ServiceManager"; // const-string v3, "android.os.ServiceManager"
			 java.lang.Class .forName ( v3 );
			 final String v4 = "getService"; // const-string v4, "getService"
			 /* new-array v5, v2, [Ljava/lang/Class; */
			 /* const-class v6, Ljava/lang/String; */
			 /* aput-object v6, v5, v1 */
			 (( java.lang.Class ) v3 ).getMethod ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
			 /* .line 5 */
			 /* filled-new-array {v0}, [Ljava/lang/Object; */
			 int v4 = 0; // const/4 v4, 0x0
			 (( java.lang.reflect.Method ) v3 ).invoke ( v4, v0 ); // invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
			 /* check-cast v0, Landroid/os/IBinder; */
			 /* .line 6 */
			 com.xiaomi.NetworkBoost.IAIDLMiuiNetworkBoost$Stub .asInterface ( v0 );
			 this.a = v0;
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 9 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 (( java.lang.Throwable ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
			 /* .line 11 */
		 } // :goto_0
		 v0 = this.a;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 14 */
				 v0 = 			 try { // :try_start_1
				 com.xiaomi.NetworkBoost.Version .setServiceVersion ( v0 );
				 /* .line 15 */
				 v0 = this.e;
				 /* :try_end_1 */
				 /* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
				 /* .line 17 */
				 /* :catch_1 */
				 /* move-exception v0 */
				 (( java.lang.Throwable ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
				 /* .line 18 */
				 v0 = this.e;
				 /* .line 21 */
			 } // :cond_0
			 /* new-instance v0, Landroid/content/Intent; */
			 /* const-class v3, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost; */
			 (( java.lang.Class ) v3 ).getName ( ); // invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;
			 /* invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
			 /* .line 22 */
			 final String v3 = "com.xiaomi.NetworkBoost"; // const-string v3, "com.xiaomi.NetworkBoost"
			 final String v4 = "com.xiaomi.NetworkBoost.NetworkBoostService"; // const-string v4, "com.xiaomi.NetworkBoost.NetworkBoostService"
			 (( android.content.Intent ) v0 ).setClassName ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 23 */
			 v3 = this.c;
			 v4 = this.b;
			 (( android.content.Context ) v3 ).bindService ( v0, v4, v2 ); // invoke-virtual {v3, v0, v4, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
			 /* .line 28 */
		 } // :goto_1
		 v0 = this.a;
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* move v1, v2 */
		 } // :cond_1
	 } // .end method
	 public Boolean connectSlaveWifi ( Integer p0 ) {
		 /* .locals 2 */
		 /* .line 1 */
		 /* const v0, 0x7fffffff */
		 v0 = 		 com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
		 int v1 = 0; // const/4 v1, 0x0
		 /* if-nez v0, :cond_0 */
		 /* .line 6 */
	 } // :cond_0
	 v0 = this.a;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 8 */
			 p1 = 		 try { // :try_start_0
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 10 */
			 /* :catch_0 */
			 /* move-exception p1 */
			 (( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
		 } // :cond_1
	 } // .end method
	 public Boolean disableWifiSelectionOpt ( com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback p0 ) {
		 /* .locals 2 */
		 /* .line 1 */
		 int v0 = 4; // const/4 v0, 0x4
		 v0 = 		 com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
		 int v1 = 0; // const/4 v1, 0x0
		 /* if-nez v0, :cond_0 */
		 /* .line 6 */
	 } // :cond_0
	 v0 = this.a;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 8 */
			 p1 = 		 try { // :try_start_0
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* :catch_0 */
			 /* move-exception p1 */
			 /* .line 10 */
			 (( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
			 /* .line 11 */
			 (( java.lang.Throwable ) p1 ).printStackTrace ( ); // invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
		 } // :cond_1
	 } // .end method
	 public Boolean disableWifiSelectionOptByUid ( com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback p0, Integer p1 ) {
		 /* .locals 2 */
		 /* .line 1 */
		 int v0 = 4; // const/4 v0, 0x4
		 v0 = 		 com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
		 int v1 = 0; // const/4 v1, 0x0
		 /* if-nez v0, :cond_0 */
		 /* .line 6 */
	 } // :cond_0
	 v0 = this.a;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 8 */
			 p1 = 		 try { // :try_start_0
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* :catch_0 */
			 /* move-exception p1 */
			 /* .line 10 */
			 (( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
			 /* .line 11 */
			 (( java.lang.Throwable ) p1 ).printStackTrace ( ); // invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
		 } // :cond_1
	 } // .end method
	 public Boolean disconnectSlaveWifi ( ) {
		 /* .locals 2 */
		 /* .line 1 */
		 /* const v0, 0x7fffffff */
		 v0 = 		 com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
		 int v1 = 0; // const/4 v1, 0x0
		 /* if-nez v0, :cond_0 */
		 /* .line 6 */
	 } // :cond_0
	 v0 = this.a;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 8 */
			 v0 = 		 try { // :try_start_0
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 10 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 (( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
		 } // :cond_1
	 } // .end method
	 public Boolean enableWifiSelectionOpt ( com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback p0, Integer p1 ) {
		 /* .locals 2 */
		 /* .line 1 */
		 int v0 = 4; // const/4 v0, 0x4
		 v0 = 		 com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
		 int v1 = 0; // const/4 v1, 0x0
		 /* if-nez v0, :cond_0 */
		 /* .line 6 */
	 } // :cond_0
	 v0 = this.a;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 8 */
			 p1 = 		 try { // :try_start_0
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* :catch_0 */
			 /* move-exception p1 */
			 /* .line 10 */
			 (( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
			 /* .line 11 */
			 (( java.lang.Throwable ) p1 ).printStackTrace ( ); // invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
		 } // :cond_1
	 } // .end method
	 public Boolean enableWifiSelectionOptByUid ( com.xiaomi.NetworkBoost.IAIDLMiuiNetSelectCallback p0, Integer p1, Integer p2 ) {
		 /* .locals 2 */
		 /* .line 1 */
		 int v0 = 4; // const/4 v0, 0x4
		 v0 = 		 com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
		 int v1 = 0; // const/4 v1, 0x0
		 /* if-nez v0, :cond_0 */
		 /* .line 6 */
	 } // :cond_0
	 v0 = this.a;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 8 */
			 p1 = 		 try { // :try_start_0
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* :catch_0 */
			 /* move-exception p1 */
			 /* .line 10 */
			 (( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
			 /* .line 11 */
			 (( java.lang.Throwable ) p1 ).printStackTrace ( ); // invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
		 } // :cond_1
	 } // .end method
	 public java.util.Map getAvailableIfaces ( ) {
		 /* .locals 1 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "()", */
		 /* "Ljava/util/Map<", */
		 /* "Ljava/lang/String;", */
		 /* "Ljava/lang/String;", */
		 /* ">;" */
		 /* } */
	 } // .end annotation
	 /* .line 1 */
	 int v0 = 3; // const/4 v0, 0x3
	 v0 = 	 com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
	 /* if-nez v0, :cond_0 */
	 /* .line 3 */
	 /* new-instance v0, Ljava/util/HashMap; */
	 /* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
	 /* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 8 */
	 try { // :try_start_0
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 10 */
		 (( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
		 /* .line 11 */
		 (( java.lang.Throwable ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
		 /* .line 14 */
	 } // :cond_1
	 /* new-instance v0, Ljava/util/HashMap; */
	 /* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
} // .end method
public java.util.Map getQoEByAvailableIfaceName ( java.lang.String p0 ) {
	 /* .locals 1 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Ljava/lang/String;", */
	 /* ")", */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 1 */
/* const v0, 0x7fffffff */
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
/* if-nez v0, :cond_0 */
/* .line 3 */
/* new-instance p1, Ljava/util/HashMap; */
/* invoke-direct {p1}, Ljava/util/HashMap;-><init>()V */
/* .line 5 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 7 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* :catch_0 */
/* move-exception p1 */
/* .line 9 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* .line 10 */
(( java.lang.Throwable ) p1 ).printStackTrace ( ); // invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
/* .line 13 */
} // :cond_1
/* new-instance p1, Ljava/util/HashMap; */
/* invoke-direct {p1}, Ljava/util/HashMap;-><init>()V */
} // .end method
public com.xiaomi.NetworkBoost.NetLinkLayerQoE getQoEByAvailableIfaceNameV1 ( java.lang.String p0 ) {
/* .locals 1 */
/* .line 1 */
int v0 = 4; // const/4 v0, 0x4
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
/* if-nez v0, :cond_0 */
/* .line 3 */
/* new-instance p1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
/* invoke-direct {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;-><init>()V */
/* .line 5 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 7 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* :catch_0 */
/* move-exception p1 */
/* .line 9 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* .line 10 */
(( java.lang.Throwable ) p1 ).printStackTrace ( ); // invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
/* .line 13 */
} // :cond_1
/* new-instance p1, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE; */
/* invoke-direct {p1}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;-><init>()V */
} // .end method
public Boolean isCelluarDSDAState ( ) {
/* .locals 2 */
/* .line 1 */
int v0 = 4; // const/4 v0, 0x4
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
v0 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception v0 */
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Integer isSlaveWifiEnabledAndOthersOpt ( Integer p0 ) {
/* .locals 2 */
/* .line 1 */
int v0 = 5; // const/4 v0, 0x5
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = -1; // const/4 v1, -0x1
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Integer isSlaveWifiEnabledAndOthersOptByUid ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .line 1 */
int v0 = 6; // const/4 v0, 0x6
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = -1; // const/4 v1, -0x1
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Boolean isSupportDualCelluarData ( ) {
/* .locals 2 */
/* .line 1 */
int v0 = 4; // const/4 v0, 0x4
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
v0 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception v0 */
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Boolean isSupportDualWifi ( ) {
/* .locals 2 */
/* .line 1 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
v0 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception v0 */
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Boolean isSupportMediaPlayerPolicy ( ) {
/* .locals 2 */
/* .line 1 */
int v0 = 4; // const/4 v0, 0x4
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
v0 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception v0 */
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Boolean registerCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiNetworkCallback p0 ) {
/* .locals 2 */
/* .line 1 */
int v0 = 1; // const/4 v0, 0x1
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Boolean registerNetLinkCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback p0, Integer p1 ) {
/* .locals 2 */
/* .line 1 */
int v0 = 4; // const/4 v0, 0x4
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 8 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 10 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* :catch_0 */
/* move-exception p1 */
/* .line 12 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* .line 13 */
(( java.lang.Throwable ) p1 ).printStackTrace ( ); // invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
} // :cond_1
} // .end method
public Boolean registerNetLinkCallbackByUid ( com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .line 1 */
int v0 = 4; // const/4 v0, 0x4
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 8 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 10 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* :catch_0 */
/* move-exception p1 */
/* .line 12 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* .line 13 */
(( java.lang.Throwable ) p1 ).printStackTrace ( ); // invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
} // :cond_1
} // .end method
public Boolean registerWifiLinkCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback p0 ) {
/* .locals 2 */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 1 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 8 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 10 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 12 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public void reportBssidScore ( java.util.Map p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1 */
int v0 = 4; // const/4 v0, 0x4
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
/* if-nez v0, :cond_0 */
return;
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* :catch_0 */
/* move-exception p1 */
/* .line 10 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* .line 11 */
(( java.lang.Throwable ) p1 ).printStackTrace ( ); // invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
} // :cond_1
} // :goto_0
return;
} // .end method
public java.util.Map requestAppTrafficStatistics ( Integer p0, Long p1, Long p2 ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(IJJ)", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
/* if-nez v0, :cond_0 */
/* .line 3 */
/* new-instance p1, Ljava/util/HashMap; */
/* invoke-direct {p1}, Ljava/util/HashMap;-><init>()V */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
/* move v1, p1 */
/* move-wide v2, p2 */
/* move-wide v4, p4 */
try { // :try_start_0
/* invoke-interface/range {v0 ..v5}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->requestAppTrafficStatistics(IJJ)Ljava/util/Map; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* :catch_0 */
/* move-exception p1 */
/* .line 10 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* .line 11 */
(( java.lang.Throwable ) p1 ).printStackTrace ( ); // invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
/* .line 14 */
} // :cond_1
/* new-instance p1, Ljava/util/HashMap; */
/* invoke-direct {p1}, Ljava/util/HashMap;-><init>()V */
} // .end method
public java.util.Map requestAppTrafficStatisticsByUid ( Integer p0, Long p1, Long p2, Integer p3 ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(IJJI)", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1 */
int v0 = 4; // const/4 v0, 0x4
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
/* if-nez v0, :cond_0 */
/* .line 3 */
/* new-instance p1, Ljava/util/HashMap; */
/* invoke-direct {p1}, Ljava/util/HashMap;-><init>()V */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
/* move v1, p1 */
/* move-wide v2, p2 */
/* move-wide v4, p4 */
/* move v6, p6 */
try { // :try_start_0
/* invoke-interface/range {v0 ..v6}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;->requestAppTrafficStatisticsByUid(IJJI)Ljava/util/Map; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* :catch_0 */
/* move-exception p1 */
/* .line 10 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* .line 11 */
(( java.lang.Throwable ) p1 ).printStackTrace ( ); // invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
/* .line 14 */
} // :cond_1
/* new-instance p1, Ljava/util/HashMap; */
/* invoke-direct {p1}, Ljava/util/HashMap;-><init>()V */
} // .end method
public Boolean resumeBackgroundScan ( ) {
/* .locals 2 */
/* .line 1 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
v0 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception v0 */
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Boolean resumeWifiPowerSave ( ) {
/* .locals 2 */
/* .line 1 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
v0 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception v0 */
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Boolean setDualCelluarDataEnable ( Boolean p0 ) {
/* .locals 2 */
/* .line 1 */
int v0 = 4; // const/4 v0, 0x4
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Boolean setSlaveWifiEnable ( Boolean p0 ) {
/* .locals 2 */
/* .line 1 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Boolean setSockPrio ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .line 1 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 5 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 7 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 9 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Boolean setSockPrio ( java.io.FileDescriptor p0, Integer p1 ) {
/* .locals 2 */
/* .line 10 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 15 */
} // :cond_0
try { // :try_start_0
p1 = com.xiaomi.NetworkBoost.NetworkBoostManager .a ( p1 );
p1 = (( com.xiaomi.NetworkBoost.NetworkBoostManager ) p0 ).setSockPrio ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->setSockPrio(II)Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 17 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
public Boolean setSockPrio ( java.net.Socket p0, Integer p1 ) {
/* .locals 2 */
/* .line 18 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 23 */
} // :cond_0
try { // :try_start_0
android.os.ParcelFileDescriptor .fromSocket ( p1 );
(( android.os.ParcelFileDescriptor ) p1 ).getFileDescriptor ( ); // invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
p1 = com.xiaomi.NetworkBoost.NetworkBoostManager .a ( p1 );
p1 = (( com.xiaomi.NetworkBoost.NetworkBoostManager ) p0 ).setSockPrio ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->setSockPrio(II)Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 25 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
public Boolean setTCPCongestion ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .line 1 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 5 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 7 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 9 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Boolean setTCPCongestion ( java.io.FileDescriptor p0, java.lang.String p1 ) {
/* .locals 2 */
/* .line 10 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 15 */
} // :cond_0
try { // :try_start_0
p1 = com.xiaomi.NetworkBoost.NetworkBoostManager .a ( p1 );
p1 = (( com.xiaomi.NetworkBoost.NetworkBoostManager ) p0 ).setTCPCongestion ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->setTCPCongestion(ILjava/lang/String;)Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 17 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
public Boolean setTCPCongestion ( java.net.Socket p0, java.lang.String p1 ) {
/* .locals 2 */
/* .line 18 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 23 */
} // :cond_0
try { // :try_start_0
android.os.ParcelFileDescriptor .fromSocket ( p1 );
(( android.os.ParcelFileDescriptor ) p1 ).getFileDescriptor ( ); // invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
p1 = com.xiaomi.NetworkBoost.NetworkBoostManager .a ( p1 );
p1 = (( com.xiaomi.NetworkBoost.NetworkBoostManager ) p0 ).setTCPCongestion ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->setTCPCongestion(ILjava/lang/String;)Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 25 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
public Boolean setTrafficTransInterface ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .line 1 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Boolean setTrafficTransInterface ( java.io.FileDescriptor p0, java.lang.String p1 ) {
/* .locals 2 */
/* .line 11 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 16 */
} // :cond_0
try { // :try_start_0
p1 = com.xiaomi.NetworkBoost.NetworkBoostManager .a ( p1 );
p1 = (( com.xiaomi.NetworkBoost.NetworkBoostManager ) p0 ).setTrafficTransInterface ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->setTrafficTransInterface(ILjava/lang/String;)Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 18 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
public Boolean setTrafficTransInterface ( java.net.Socket p0, java.lang.String p1 ) {
/* .locals 2 */
/* .line 19 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 24 */
} // :cond_0
try { // :try_start_0
android.os.ParcelFileDescriptor .fromSocket ( p1 );
(( android.os.ParcelFileDescriptor ) p1 ).getFileDescriptor ( ); // invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
p1 = com.xiaomi.NetworkBoost.NetworkBoostManager .a ( p1 );
p1 = (( com.xiaomi.NetworkBoost.NetworkBoostManager ) p0 ).setTrafficTransInterface ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostManager;->setTrafficTransInterface(ILjava/lang/String;)Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 26 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
public Boolean suspendBackgroundScan ( ) {
/* .locals 2 */
/* .line 1 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
v0 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception v0 */
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Boolean suspendWifiPowerSave ( ) {
/* .locals 2 */
/* .line 1 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
v0 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception v0 */
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public void triggerWifiSelection ( ) {
/* .locals 1 */
/* .line 1 */
int v0 = 4; // const/4 v0, 0x4
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
/* if-nez v0, :cond_0 */
return;
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* :catch_0 */
/* move-exception v0 */
/* .line 10 */
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* .line 11 */
(( java.lang.Throwable ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
} // :cond_1
} // :goto_0
return;
} // .end method
public void unbindService ( ) {
/* .locals 2 */
/* .line 1 */
v0 = this.c;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 3 */
try { // :try_start_0
v1 = this.b;
(( android.content.Context ) v0 ).unbindService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* :catch_0 */
/* move-exception v0 */
/* .line 9 */
} // :cond_0
} // :goto_0
v0 = this.d;
/* monitor-enter v0 */
/* .line 10 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_1
this.a = v1;
/* .line 11 */
v1 = this.d;
(( java.lang.Object ) v1 ).notifyAll ( ); // invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V
/* .line 12 */
/* monitor-exit v0 */
return;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean unregisterCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiNetworkCallback p0 ) {
/* .locals 2 */
/* .line 1 */
int v0 = 1; // const/4 v0, 0x1
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
public Boolean unregisterNetLinkCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback p0 ) {
/* .locals 2 */
/* .line 1 */
int v0 = 4; // const/4 v0, 0x4
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* :catch_0 */
/* move-exception p1 */
/* .line 10 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* .line 11 */
(( java.lang.Throwable ) p1 ).printStackTrace ( ); // invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
} // :cond_1
} // .end method
public Boolean unregisterNetLinkCallbackByUid ( com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback p0, Integer p1 ) {
/* .locals 2 */
/* .line 1 */
int v0 = 4; // const/4 v0, 0x4
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* :catch_0 */
/* move-exception p1 */
/* .line 10 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* .line 11 */
(( java.lang.Throwable ) p1 ).printStackTrace ( ); // invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
} // :cond_1
} // .end method
public Boolean unregisterWifiLinkCallback ( com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback p0 ) {
/* .locals 2 */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 1 */
int v0 = 3; // const/4 v0, 0x3
v0 = com.xiaomi.NetworkBoost.Version .isSupport ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 6 */
} // :cond_0
v0 = this.a;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 8 */
p1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 10 */
/* :catch_0 */
/* move-exception p1 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // :cond_1
} // .end method
