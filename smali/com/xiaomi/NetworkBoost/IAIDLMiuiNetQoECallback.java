public abstract class com.xiaomi.NetworkBoost.IAIDLMiuiNetQoECallback implements android.os.IInterface {
	 /* .source "IAIDLMiuiNetQoECallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback$Stub; */
	 /* } */
} // .end annotation
/* # virtual methods */
public abstract void masterQoECallBack ( com.xiaomi.NetworkBoost.NetLinkLayerQoE p0 ) {
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
} // .end method
public abstract void slaveQoECallBack ( com.xiaomi.NetworkBoost.NetLinkLayerQoE p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
