.class Lcom/xiaomi/NetworkBoost/StatusManager$3;
.super Ljava/lang/Object;
.source "StatusManager.java"

# interfaces
.implements Landroid/media/AudioManager$OnModeChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/StatusManager;->registerAudioModeChangedListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/StatusManager;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 369
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$3;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onModeChanged(I)V
    .locals 3
    .param p1, "mode"    # I

    .line 372
    const-string v0, "NetworkBoostStatusManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "audio Broadcast Receiver mode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$3;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmAppStatusListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v0

    monitor-enter v0

    .line 374
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$3;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmAppStatusListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;

    .line 375
    .local v2, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;
    invoke-interface {v2, p1}, Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;->onAudioChanged(I)V

    .line 376
    .end local v2    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;
    goto :goto_0

    .line 377
    :cond_0
    monitor-exit v0

    .line 378
    return-void

    .line 377
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
