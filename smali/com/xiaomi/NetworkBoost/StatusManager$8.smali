.class Lcom/xiaomi/NetworkBoost/StatusManager$8;
.super Landroid/content/BroadcastReceiver;
.source "StatusManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/NetworkBoost/StatusManager;->registerScreenStatusReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/StatusManager;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 1139
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$8;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1142
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1143
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$8;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmScreenStatusListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    .line 1144
    :try_start_0
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1145
    const-string v2, "NetworkBoostStatusManager"

    const-string v3, "ACTION_SCREEN_ON"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1146
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager$8;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmScreenStatusListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    .line 1147
    .local v3, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;
    invoke-interface {v3}, Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;->onScreenON()V

    .line 1148
    .end local v3    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;
    goto :goto_0

    :cond_0
    goto :goto_2

    .line 1149
    :cond_1
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1150
    const-string v2, "NetworkBoostStatusManager"

    const-string v3, "ACTION_SCREEN_OFF"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1151
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager$8;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmScreenStatusListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    .line 1152
    .restart local v3    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;
    invoke-interface {v3}, Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;->onScreenOFF()V

    .line 1153
    .end local v3    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;
    goto :goto_1

    .line 1155
    :cond_2
    :goto_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1157
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$8;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$mcheckSensorStatus(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    .line 1158
    return-void

    .line 1155
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method
