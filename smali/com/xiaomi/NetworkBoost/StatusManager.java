public class com.xiaomi.NetworkBoost.StatusManager {
	 /* .source "StatusManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;, */
	 /* Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;, */
	 /* Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;, */
	 /* Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;, */
	 /* Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;, */
	 /* Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;, */
	 /* Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;, */
	 /* Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;, */
	 /* Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;, */
	 /* Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;, */
	 /* Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer CALLBACK_DEFAULT_NETWORK_INTERFACE;
public static final Integer CALLBACK_NETWORK_INTERFACE;
public static final Integer CALLBACK_NETWORK_PRIORITY_ADD;
public static final Integer CALLBACK_NETWORK_PRIORITY_CHANGED;
public static final Integer CALLBACK_SCREEN_STATUS;
public static final Integer CALLBACK_VPN_STATUS;
public static final Integer DATA_READY;
private static final Integer DELAY_TIME;
private static final Integer EVENT_CHECK_SENSOR_STATUS;
private static final Integer EVENT_DEFAULT_INTERFACE_ONAVAILABLE;
private static final Integer EVENT_DELAY_SET_CELLULAR_CALLBACK;
private static final Integer EVENT_GET_MODEM_SIGNAL_STRENGTH;
private static final Integer EVENT_INTERFACE_ONAVAILABLE;
private static final Integer EVENT_INTERFACE_ONLOST;
private static final Integer EVENT_SIM_STATE_CHANGED;
private static final Integer LTE_RSRP_LOWER_LIMIT;
private static final Integer LTE_RSRP_UPPER_LIMIT;
private static final Integer MOVEMENT_SENSOR_TYPE;
private static final Integer NA_RAT;
private static final Integer NETWORK_TYPE_LTE;
private static final Integer NETWORK_TYPE_LTE_CA;
private static final Integer NETWORK_TYPE_NR;
private static final Integer NETWORK_TYPE_NR_CA;
private static final Integer NR_RSRP_LOWER_LIMIT;
private static final Integer NR_RSRP_UPPER_LIMIT;
public static final java.lang.String ON_AVAILABLE;
public static final java.lang.String ON_LOST;
public static final Integer PHONE_COUNT;
private static final Integer RSRP_POOR_THRESHOLD;
private static final Integer SENSOR_DELAY_TIME;
private static final Integer SIGNAL_STRENGTH_DELAY_MIILLIS;
private static final Integer SIGNAL_STRENGTH_GOOD;
private static final Integer SIGNAL_STRENGTH_POOR;
public static final Integer SLAVE_DATA_READY;
public static final Integer SLAVE_WIFI_READY;
private static final java.lang.String TAG;
private static final Integer UNAVAILABLE;
private static final Boolean VPN_STATUS_DISABLE;
private static final Boolean VPN_STATUS_ENABLE;
private static final Integer WIFI_BAND_5_GHZ_HBS;
public static final Integer WIFI_READY;
public static final java.lang.String WLAN_IFACE_0;
public static final java.lang.String WLAN_IFACE_1;
private static android.net.ConnectivityManager mConnectivityManager;
private static java.lang.reflect.Method mReflectScreenState;
private static android.net.wifi.SlaveWifiManager mSlaveWifiManager;
private static android.net.wifi.WifiManager mWifiManager;
public static volatile com.xiaomi.NetworkBoost.StatusManager sInstance;
/* # instance fields */
private android.app.IActivityManager mActivityManager;
private java.util.List mAppStatusListenerList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.media.AudioManager mAudioManager;
private android.media.AudioManager$OnModeChangedListener mAudioModeListener;
private android.net.ConnectivityManager$NetworkCallback mCellularNetworkCallback;
private android.content.Context mContext;
private android.hardware.SensorEvent mCurrentSensorevent;
private Boolean mDataReady;
private java.lang.String mDefaultIface;
private Integer mDefaultIfaceStatus;
private Integer mDefaultNetid;
private android.net.ConnectivityManager$NetworkCallback mDefaultNetworkCallback;
private java.util.List mDefaultNetworkInterfaceListenerList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mFUid;
private miui.process.IForegroundInfoListener$Stub mForegroundInfoListener;
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
private Integer mIfaceNumber;
private java.lang.String mInterface;
private java.util.List mModemSignalStrengthListener;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mMovementSensorStatusListenerList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.xiaomi.NetworkBoost.NetworkBoostProcessMonitor mNetworkBoostProcessMonitor;
private java.util.List mNetworkInterfaceListenerList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.HashMap mNetworkMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mNetworkPriorityListenerList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.os.PowerManager mPowerManager;
private Integer mPriorityMode;
private java.util.List mScreenStatusListenerList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.BroadcastReceiver mScreenStatusReceiver;
private Boolean mSensorFlag;
private final android.hardware.SensorEventListener mSensorListener;
private android.hardware.SensorManager mSensorManager;
private Boolean mSlaveDataReady;
private Boolean mSlaveWifiReady;
private android.telephony.TelephonyManager mTelephonyManager;
private android.content.BroadcastReceiver mTelephonyStatusReceiver;
private Integer mThermalLevel;
private Integer mTrafficPolicy;
private final android.app.IUidObserver mUidObserver;
private android.net.ConnectivityManager$NetworkCallback mVPNCallback;
private java.util.List mVPNListener;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mVPNstatus;
private android.os.HandlerThread mWacthdogHandlerThread;
private android.net.ConnectivityManager$NetworkCallback mWifiNetworkCallback;
private Boolean mWifiReady;
/* # direct methods */
static java.util.List -$$Nest$fgetmAppStatusListenerList ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAppStatusListenerList;
} // .end method
static Boolean -$$Nest$fgetmDataReady ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z */
} // .end method
static java.lang.String -$$Nest$fgetmDefaultIface ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDefaultIface;
} // .end method
static Integer -$$Nest$fgetmDefaultIfaceStatus ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIfaceStatus:I */
} // .end method
static java.util.List -$$Nest$fgetmDefaultNetworkInterfaceListenerList ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDefaultNetworkInterfaceListenerList;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Integer -$$Nest$fgetmIfaceNumber ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I */
} // .end method
static java.lang.String -$$Nest$fgetmInterface ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mInterface;
} // .end method
static java.util.List -$$Nest$fgetmMovementSensorStatusListenerList ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMovementSensorStatusListenerList;
} // .end method
static java.util.List -$$Nest$fgetmNetworkInterfaceListenerList ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNetworkInterfaceListenerList;
} // .end method
static java.util.List -$$Nest$fgetmNetworkPriorityListenerList ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNetworkPriorityListenerList;
} // .end method
static android.os.PowerManager -$$Nest$fgetmPowerManager ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPowerManager;
} // .end method
static Integer -$$Nest$fgetmPriorityMode ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPriorityMode:I */
} // .end method
static java.util.List -$$Nest$fgetmScreenStatusListenerList ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mScreenStatusListenerList;
} // .end method
static Boolean -$$Nest$fgetmSlaveDataReady ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z */
} // .end method
static Boolean -$$Nest$fgetmSlaveWifiReady ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiReady:Z */
} // .end method
static Integer -$$Nest$fgetmThermalLevel ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mThermalLevel:I */
} // .end method
static Integer -$$Nest$fgetmTrafficPolicy ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTrafficPolicy:I */
} // .end method
static java.util.List -$$Nest$fgetmVPNListener ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mVPNListener;
} // .end method
static Boolean -$$Nest$fgetmVPNstatus ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNstatus:Z */
} // .end method
static Boolean -$$Nest$fgetmWifiReady ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiReady:Z */
} // .end method
static void -$$Nest$fputmCurrentSensorevent ( com.xiaomi.NetworkBoost.StatusManager p0, android.hardware.SensorEvent p1 ) { //bridge//synthethic
/* .locals 0 */
this.mCurrentSensorevent = p1;
return;
} // .end method
static void -$$Nest$fputmDefaultNetid ( com.xiaomi.NetworkBoost.StatusManager p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetid:I */
return;
} // .end method
static void -$$Nest$fputmFUid ( com.xiaomi.NetworkBoost.StatusManager p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mFUid:I */
return;
} // .end method
static void -$$Nest$fputmVPNstatus ( com.xiaomi.NetworkBoost.StatusManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNstatus:Z */
return;
} // .end method
static void -$$Nest$mcallbackDefaultNetworkInterface ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->callbackDefaultNetworkInterface()V */
return;
} // .end method
static void -$$Nest$mcheckSensorStatus ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->checkSensorStatus()V */
return;
} // .end method
static void -$$Nest$minterfaceOnAvailable ( com.xiaomi.NetworkBoost.StatusManager p0, android.net.Network p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->interfaceOnAvailable(Landroid/net/Network;)V */
return;
} // .end method
static void -$$Nest$minterfaceOnLost ( com.xiaomi.NetworkBoost.StatusManager p0, android.net.Network p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->interfaceOnLost(Landroid/net/Network;)V */
return;
} // .end method
static void -$$Nest$misModemSingnalStrengthStatus ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->isModemSingnalStrengthStatus()V */
return;
} // .end method
static void -$$Nest$monNetworkPriorityChanged ( com.xiaomi.NetworkBoost.StatusManager p0, com.xiaomi.NetworkBoost.StatusManager$NetworkPriorityInfo p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->onNetworkPriorityChanged(Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;)V */
return;
} // .end method
static void -$$Nest$mprocessRegisterCellularCallback ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->processRegisterCellularCallback()V */
return;
} // .end method
static void -$$Nest$mprocessSimStateChanged ( com.xiaomi.NetworkBoost.StatusManager p0, android.content.Intent p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->processSimStateChanged(Landroid/content/Intent;)V */
return;
} // .end method
static void -$$Nest$mregisterMovementSensorListener ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerMovementSensorListener()V */
return;
} // .end method
static void -$$Nest$msetSmartDNSInterface ( com.xiaomi.NetworkBoost.StatusManager p0, android.net.Network p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->setSmartDNSInterface(Landroid/net/Network;)V */
return;
} // .end method
static void -$$Nest$munregisterMovementSensorListener ( com.xiaomi.NetworkBoost.StatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterMovementSensorListener()V */
return;
} // .end method
static com.xiaomi.NetworkBoost.StatusManager ( ) {
/* .locals 1 */
/* .line 84 */
miui.telephony.TelephonyManager .getDefault ( );
v0 = (( miui.telephony.TelephonyManager ) v0 ).getPhoneCount ( ); // invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getPhoneCount()I
return;
} // .end method
private com.xiaomi.NetworkBoost.StatusManager ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 237 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 141 */
final String v0 = ""; // const-string v0, ""
this.mInterface = v0;
/* .line 142 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I */
/* .line 143 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiReady:Z */
/* .line 144 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiReady:Z */
/* .line 145 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z */
/* .line 146 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z */
/* .line 147 */
int v2 = -1; // const/4 v2, -0x1
/* iput v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mFUid:I */
/* .line 150 */
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetid:I */
/* .line 151 */
this.mDefaultIface = v0;
/* .line 152 */
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIfaceStatus:I */
/* .line 157 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mVPNstatus:Z */
/* .line 162 */
int v0 = 0; // const/4 v0, 0x0
this.mSensorManager = v0;
/* .line 163 */
this.mCurrentSensorevent = v0;
/* .line 164 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorFlag:Z */
/* .line 167 */
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPriorityMode:I */
/* .line 168 */
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTrafficPolicy:I */
/* .line 169 */
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mThermalLevel:I */
/* .line 191 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mAppStatusListenerList = v1;
/* .line 192 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mNetworkInterfaceListenerList = v1;
/* .line 193 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mDefaultNetworkInterfaceListenerList = v1;
/* .line 194 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mScreenStatusListenerList = v1;
/* .line 195 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mNetworkPriorityListenerList = v1;
/* .line 196 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mModemSignalStrengthListener = v1;
/* .line 197 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mMovementSensorStatusListenerList = v1;
/* .line 198 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mVPNListener = v1;
/* .line 202 */
this.mContext = v0;
/* .line 297 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$1; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$1;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V */
this.mForegroundInfoListener = v0;
/* .line 337 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$2; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$2;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V */
this.mUidObserver = v0;
/* .line 511 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mNetworkMap = v0;
/* .line 720 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$4; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$4;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V */
this.mWifiNetworkCallback = v0;
/* .line 747 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$5; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$5;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V */
this.mCellularNetworkCallback = v0;
/* .line 1016 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$6; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$6;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V */
this.mDefaultNetworkCallback = v0;
/* .line 1088 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$7; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$7;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V */
this.mVPNCallback = v0;
/* .line 1592 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$10; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$10;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V */
this.mSensorListener = v0;
/* .line 238 */
(( android.content.Context ) p1 ).getApplicationContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
this.mContext = v0;
/* .line 239 */
return;
} // .end method
private void callbackDefaultNetworkInterface ( ) {
/* .locals 6 */
/* .line 972 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetid:I */
/* if-nez v0, :cond_0 */
/* .line 973 */
return;
/* .line 976 */
} // :cond_0
v1 = this.mNetworkMap;
java.lang.Integer .valueOf ( v0 );
v0 = (( java.util.HashMap ) v1 ).containsKey ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 977 */
v0 = this.mNetworkMap;
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultNetid:I */
java.lang.Integer .valueOf ( v1 );
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
/* .line 978 */
/* .local v0, "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
(( com.xiaomi.NetworkBoost.StatusManager$NetworkCallbackInterface ) v0 ).getInterfaceName ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->getInterfaceName()Ljava/lang/String;
this.mDefaultIface = v1;
/* .line 979 */
v1 = (( com.xiaomi.NetworkBoost.StatusManager$NetworkCallbackInterface ) v0 ).getStatus ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->getStatus()I
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIfaceStatus:I */
/* .line 981 */
final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "callbackDefaultNetworkInterface ifacename:"; // const-string v3, "callbackDefaultNetworkInterface ifacename:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mDefaultIface;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " ifacestatus:"; // const-string v3, " ifacestatus:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIfaceStatus:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v2 );
/* .line 984 */
v1 = this.mDefaultNetworkInterfaceListenerList;
/* monitor-enter v1 */
/* .line 985 */
try { // :try_start_0
v2 = this.mDefaultNetworkInterfaceListenerList;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener; */
/* .line 986 */
/* .local v3, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener; */
v4 = this.mDefaultIface;
/* iget v5, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDefaultIfaceStatus:I */
/* .line 987 */
} // .end local v3 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;
/* .line 988 */
} // :cond_1
/* monitor-exit v1 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
/* .line 991 */
} // .end local v0 # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
} // :cond_2
} // :goto_1
return;
} // .end method
private void callbackNetworkInterface ( ) {
/* .locals 11 */
/* .line 895 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "interface name:"; // const-string v2, "interface name:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mInterface;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " interface number:"; // const-string v2, " interface number:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " WifiReady:"; // const-string v2, " WifiReady:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiReady:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " SlaveWifiReady:"; // const-string v2, " SlaveWifiReady:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiReady:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " DataReady:"; // const-string v2, " DataReady:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " SlaveDataReady:"; // const-string v2, " SlaveDataReady:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v0,v1 );
/* .line 898 */
v0 = this.mNetworkInterfaceListenerList;
/* monitor-enter v0 */
/* .line 899 */
try { // :try_start_0
v1 = this.mNetworkInterfaceListenerList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* move-object v3, v2 */
/* check-cast v3, Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener; */
/* .line 900 */
/* .local v3, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener; */
v4 = this.mInterface;
/* iget v5, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I */
/* iget-boolean v6, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiReady:Z */
/* iget-boolean v7, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiReady:Z */
/* iget-boolean v8, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z */
/* iget-boolean v9, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z */
final String v10 = "ON_LOST"; // const-string v10, "ON_LOST"
/* invoke-interface/range {v3 ..v10}, Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;->onNetwrokInterfaceChange(Ljava/lang/String;IZZZZLjava/lang/String;)V */
/* .line 902 */
} // .end local v3 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;
/* .line 903 */
} // :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 905 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->checkSensorStatus()V */
/* .line 906 */
return;
/* .line 903 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void checkSensorStatus ( ) {
/* .locals 4 */
/* .line 1609 */
v0 = this.mHandler;
/* const/16 v1, 0x6a */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1610 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* const-wide/16 v2, 0x1388 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1611 */
return;
} // .end method
public static void destroyInstance ( ) {
/* .locals 4 */
/* .line 222 */
v0 = com.xiaomi.NetworkBoost.StatusManager.sInstance;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 223 */
/* const-class v0, Lcom/xiaomi/NetworkBoost/StatusManager; */
/* monitor-enter v0 */
/* .line 224 */
try { // :try_start_0
v1 = com.xiaomi.NetworkBoost.StatusManager.sInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 226 */
try { // :try_start_1
v1 = com.xiaomi.NetworkBoost.StatusManager.sInstance;
(( com.xiaomi.NetworkBoost.StatusManager ) v1 ).onDestroy ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->onDestroy()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 230 */
/* .line 228 */
/* :catch_0 */
/* move-exception v1 */
/* .line 229 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v2 = "NetworkBoostStatusManager"; // const-string v2, "NetworkBoostStatusManager"
final String v3 = "destroyInstance onDestroy catch:"; // const-string v3, "destroyInstance onDestroy catch:"
android.util.Log .e ( v2,v3,v1 );
/* .line 231 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
/* .line 233 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 235 */
} // :cond_1
} // :goto_1
return;
} // .end method
private Integer getDefaultDataSlotId ( ) {
/* .locals 1 */
/* .line 1364 */
miui.telephony.SubscriptionManager .getDefault ( );
v0 = (( miui.telephony.SubscriptionManager ) v0 ).getDefaultDataSlotId ( ); // invoke-virtual {v0}, Lmiui/telephony/SubscriptionManager;->getDefaultDataSlotId()I
} // .end method
public static com.xiaomi.NetworkBoost.StatusManager getInstance ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 205 */
v0 = com.xiaomi.NetworkBoost.StatusManager.sInstance;
/* if-nez v0, :cond_1 */
/* .line 206 */
/* const-class v0, Lcom/xiaomi/NetworkBoost/StatusManager; */
/* monitor-enter v0 */
/* .line 207 */
try { // :try_start_0
v1 = com.xiaomi.NetworkBoost.StatusManager.sInstance;
/* if-nez v1, :cond_0 */
/* .line 208 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/StatusManager; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/StatusManager;-><init>(Landroid/content/Context;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 210 */
try { // :try_start_1
v1 = com.xiaomi.NetworkBoost.StatusManager.sInstance;
(( com.xiaomi.NetworkBoost.StatusManager ) v1 ).onCreate ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->onCreate()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 214 */
/* .line 212 */
/* :catch_0 */
/* move-exception v1 */
/* .line 213 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v2 = "NetworkBoostStatusManager"; // const-string v2, "NetworkBoostStatusManager"
final String v3 = "getInstance onCreate catch:"; // const-string v3, "getInstance onCreate catch:"
android.util.Log .e ( v2,v3,v1 );
/* .line 216 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 218 */
} // :cond_1
} // :goto_1
v0 = com.xiaomi.NetworkBoost.StatusManager.sInstance;
} // .end method
private java.lang.String getMobileDataSettingName ( ) {
/* .locals 2 */
/* .line 1448 */
int v1 = 1; // const/4 v1, 0x1
/* if-le v0, v1, :cond_0 */
/* .line 1449 */
final String v0 = "mobile_data0"; // const-string v0, "mobile_data0"
} // :cond_0
final String v0 = "mobile_data"; // const-string v0, "mobile_data"
/* .line 1448 */
} // :goto_0
} // .end method
private Integer getViceDataSlotId ( ) {
/* .locals 2 */
/* .line 1368 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "slotId":I */
} // :goto_0
/* if-ge v0, v1, :cond_1 */
/* .line 1369 */
v1 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getDefaultDataSlotId()I */
/* if-eq v0, v1, :cond_0 */
/* .line 1370 */
/* .line 1368 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1373 */
} // .end local v0 # "slotId":I
} // :cond_1
int v0 = -1; // const/4 v0, -0x1
} // .end method
private Integer inRangeOrUnavailable ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "value" # I */
/* .param p2, "rangeMin" # I */
/* .param p3, "rangeMax" # I */
/* .line 1292 */
/* if-lt p1, p2, :cond_1 */
/* if-le p1, p3, :cond_0 */
/* .line 1293 */
} // :cond_0
/* .line 1292 */
} // :cond_1
} // :goto_0
/* const v0, 0x7fffffff */
} // .end method
private void interfaceOnAvailable ( android.net.Network p0 ) {
/* .locals 17 */
/* .param p1, "network" # Landroid/net/Network; */
/* .line 776 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
final String v2 = "NetworkBoostStatusManager"; // const-string v2, "NetworkBoostStatusManager"
/* if-nez v1, :cond_0 */
/* .line 777 */
final String v3 = "interfaceOnAvailable network is null."; // const-string v3, "interfaceOnAvailable network is null."
android.util.Log .e ( v2,v3 );
/* .line 778 */
return;
/* .line 781 */
} // :cond_0
v3 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
(( android.net.ConnectivityManager ) v3 ).getLinkProperties ( v1 ); // invoke-virtual {v3, v1}, Landroid/net/ConnectivityManager;->getLinkProperties(Landroid/net/Network;)Landroid/net/LinkProperties;
/* .line 782 */
/* .local v3, "linkProperties":Landroid/net/LinkProperties; */
v4 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
(( android.net.ConnectivityManager ) v4 ).getNetworkCapabilities ( v1 ); // invoke-virtual {v4, v1}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;
/* .line 783 */
/* .local v4, "networkCapabilities":Landroid/net/NetworkCapabilities; */
v5 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
(( android.net.ConnectivityManager ) v5 ).getNetworkInfo ( v1 ); // invoke-virtual {v5, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(Landroid/net/Network;)Landroid/net/NetworkInfo;
/* .line 784 */
/* .local v5, "networkInfo":Landroid/net/NetworkInfo; */
if ( v3 != null) { // if-eqz v3, :cond_e
if ( v4 != null) { // if-eqz v4, :cond_e
/* if-nez v5, :cond_1 */
/* move-object/from16 v16, v4 */
/* goto/16 :goto_6 */
/* .line 788 */
} // :cond_1
(( android.net.LinkProperties ) v3 ).getAddresses ( ); // invoke-virtual {v3}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/List;
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_d
/* check-cast v7, Ljava/net/InetAddress; */
/* .line 789 */
/* .local v7, "inetAddress":Ljava/net/InetAddress; */
/* instance-of v8, v7, Ljava/net/Inet4Address; */
if ( v8 != null) { // if-eqz v8, :cond_c
/* .line 790 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "onAvailable interface name:"; // const-string v9, "onAvailable interface name:"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.net.LinkProperties ) v3 ).getInterfaceName ( ); // invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v2,v8 );
/* .line 791 */
v8 = this.mInterface;
(( android.net.LinkProperties ) v3 ).getInterfaceName ( ); // invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
v8 = (( java.lang.String ) v8 ).indexOf ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
int v9 = -1; // const/4 v9, -0x1
/* if-ne v8, v9, :cond_b */
/* .line 792 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
v9 = this.mInterface;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.net.LinkProperties ) v3 ).getInterfaceName ( ); // invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = ","; // const-string v9, ","
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
this.mInterface = v8;
/* .line 793 */
/* iget v8, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I */
int v9 = 1; // const/4 v9, 0x1
/* add-int/2addr v8, v9 */
/* iput v8, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I */
/* .line 794 */
final String v8 = "WIFI"; // const-string v8, "WIFI"
(( android.net.NetworkInfo ) v5 ).getTypeName ( ); // invoke-virtual {v5}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;
v8 = (( java.lang.String ) v8 ).equals ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 795 */
v8 = com.xiaomi.NetworkBoost.StatusManager.mWifiManager;
(( android.net.wifi.WifiManager ) v8 ).getCurrentNetwork ( ); // invoke-virtual {v8}, Landroid/net/wifi/WifiManager;->getCurrentNetwork()Landroid/net/Network;
/* .line 796 */
/* .local v8, "currentNetwork":Landroid/net/Network; */
if ( v8 != null) { // if-eqz v8, :cond_2
v10 = (( android.net.Network ) v8 ).equals ( v1 ); // invoke-virtual {v8, v1}, Landroid/net/Network;->equals(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_2
/* .line 797 */
final String v10 = "onAvailable WIFI master."; // const-string v10, "onAvailable WIFI master."
android.util.Log .i ( v2,v10 );
/* .line 798 */
/* iput-boolean v9, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiReady:Z */
/* .line 799 */
/* new-instance v10, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
/* .line 800 */
(( android.net.LinkProperties ) v3 ).getInterfaceName ( ); // invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
/* invoke-direct {v10, v0, v11, v9}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;Ljava/lang/String;I)V */
/* move-object v9, v10 */
/* .line 801 */
/* .local v9, "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
v10 = this.mNetworkMap;
v11 = /* invoke-virtual/range {p1 ..p1}, Landroid/net/Network;->getNetId()I */
java.lang.Integer .valueOf ( v11 );
(( java.util.HashMap ) v10 ).put ( v11, v9 ); // invoke-virtual {v10, v11, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 802 */
} // .end local v9 # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
/* .line 803 */
} // :cond_2
final String v10 = "onAvailable WIFI slave."; // const-string v10, "onAvailable WIFI slave."
android.util.Log .i ( v2,v10 );
/* .line 804 */
/* iput-boolean v9, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiReady:Z */
/* .line 805 */
/* new-instance v9, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
/* .line 806 */
(( android.net.LinkProperties ) v3 ).getInterfaceName ( ); // invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
int v11 = 2; // const/4 v11, 0x2
/* invoke-direct {v9, v0, v10, v11}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;Ljava/lang/String;I)V */
/* .line 807 */
/* .restart local v9 # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
v10 = this.mNetworkMap;
v11 = /* invoke-virtual/range {p1 ..p1}, Landroid/net/Network;->getNetId()I */
java.lang.Integer .valueOf ( v11 );
(( java.util.HashMap ) v10 ).put ( v11, v9 ); // invoke-virtual {v10, v11, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 809 */
} // .end local v8 # "currentNetwork":Landroid/net/Network;
} // .end local v9 # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
} // :goto_1
/* move-object/from16 v16, v4 */
/* goto/16 :goto_5 */
} // :cond_3
final String v8 = "MOBILE"; // const-string v8, "MOBILE"
(( android.net.NetworkInfo ) v5 ).getTypeName ( ); // invoke-virtual {v5}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;
v8 = (( java.lang.String ) v8 ).equals ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_a
/* .line 810 */
v8 = /* invoke-direct/range {p0 ..p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getDefaultDataSlotId()I */
/* .line 811 */
/* .local v8, "defaultDataSlotId":I */
int v10 = -1; // const/4 v10, -0x1
/* .line 812 */
/* .local v10, "DataSubId":I */
v11 = /* invoke-direct {v0, v8}, Lcom/xiaomi/NetworkBoost/StatusManager;->isValidatePhoneId(I)Z */
if ( v11 != null) { // if-eqz v11, :cond_4
v11 = /* invoke-direct {v0, v8}, Lcom/xiaomi/NetworkBoost/StatusManager;->isSlotActivated(I)Z */
if ( v11 != null) { // if-eqz v11, :cond_4
/* .line 813 */
miui.telephony.SubscriptionManager .getDefault ( );
v10 = (( miui.telephony.SubscriptionManager ) v11 ).getSubscriptionIdForSlot ( v8 ); // invoke-virtual {v11, v8}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I
/* .line 815 */
} // :cond_4
v11 = /* invoke-direct/range {p0 ..p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getViceDataSlotId()I */
/* .line 816 */
/* .local v11, "viceDataSlotId":I */
int v12 = -1; // const/4 v12, -0x1
/* .line 817 */
/* .local v12, "SlaveDataSubId":I */
v13 = /* invoke-direct {v0, v11}, Lcom/xiaomi/NetworkBoost/StatusManager;->isValidatePhoneId(I)Z */
if ( v13 != null) { // if-eqz v13, :cond_5
v13 = /* invoke-direct {v0, v11}, Lcom/xiaomi/NetworkBoost/StatusManager;->isSlotActivated(I)Z */
if ( v13 != null) { // if-eqz v13, :cond_5
/* .line 818 */
miui.telephony.SubscriptionManager .getDefault ( );
v12 = (( miui.telephony.SubscriptionManager ) v13 ).getSubscriptionIdForSlot ( v11 ); // invoke-virtual {v13, v11}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I
/* .line 820 */
} // :cond_5
(( android.net.NetworkCapabilities ) v4 ).getSubscriptionIds ( ); // invoke-virtual {v4}, Landroid/net/NetworkCapabilities;->getSubscriptionIds()Ljava/util/Set;
/* .line 821 */
/* .local v13, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
int v15 = 0; // const/4 v15, 0x0
/* aget-object v14, v14, v15 */
/* check-cast v14, Ljava/lang/Integer; */
v14 = (( java.lang.Integer ) v14 ).intValue ( ); // invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I
/* .line 822 */
/* .local v14, "subId":I */
/* if-ne v14, v10, :cond_8 */
/* .line 823 */
final String v9 = "onAvailable MOBILE master."; // const-string v9, "onAvailable MOBILE master."
android.util.Log .i ( v2,v9 );
/* .line 824 */
v9 = this.mNetworkMap;
(( java.util.HashMap ) v9 ).values ( ); // invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v16 = } // :goto_2
if ( v16 != null) { // if-eqz v16, :cond_7
/* move-object/from16 v15, v16 */
/* check-cast v15, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
/* .line 825 */
/* .local v15, "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
v1 = (( com.xiaomi.NetworkBoost.StatusManager$NetworkCallbackInterface ) v15 ).getStatus ( ); // invoke-virtual {v15}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->getStatus()I
/* move-object/from16 v16, v4 */
int v4 = 3; // const/4 v4, 0x3
} // .end local v4 # "networkCapabilities":Landroid/net/NetworkCapabilities;
/* .local v16, "networkCapabilities":Landroid/net/NetworkCapabilities; */
/* if-ne v1, v4, :cond_6 */
/* .line 826 */
int v1 = 4; // const/4 v1, 0x4
(( com.xiaomi.NetworkBoost.StatusManager$NetworkCallbackInterface ) v15 ).setStatus ( v1 ); // invoke-virtual {v15, v1}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->setStatus(I)V
/* .line 827 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z */
/* .line 828 */
int v4 = 0; // const/4 v4, 0x0
/* iput-boolean v4, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z */
/* .line 825 */
} // :cond_6
int v1 = 1; // const/4 v1, 0x1
int v4 = 0; // const/4 v4, 0x0
/* .line 830 */
} // .end local v15 # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
} // :goto_3
/* move-object/from16 v1, p1 */
/* move-object/from16 v4, v16 */
/* .line 831 */
} // .end local v16 # "networkCapabilities":Landroid/net/NetworkCapabilities;
/* .restart local v4 # "networkCapabilities":Landroid/net/NetworkCapabilities; */
} // :cond_7
/* move-object/from16 v16, v4 */
int v1 = 1; // const/4 v1, 0x1
} // .end local v4 # "networkCapabilities":Landroid/net/NetworkCapabilities;
/* .restart local v16 # "networkCapabilities":Landroid/net/NetworkCapabilities; */
/* iput-boolean v1, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z */
/* .line 832 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
/* .line 833 */
(( android.net.LinkProperties ) v3 ).getInterfaceName ( ); // invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
int v9 = 3; // const/4 v9, 0x3
/* invoke-direct {v1, v0, v4, v9}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;Ljava/lang/String;I)V */
/* .line 834 */
/* .local v1, "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
v4 = this.mNetworkMap;
v9 = /* invoke-virtual/range {p1 ..p1}, Landroid/net/Network;->getNetId()I */
java.lang.Integer .valueOf ( v9 );
(( java.util.HashMap ) v4 ).put ( v9, v1 ); // invoke-virtual {v4, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
} // .end local v1 # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
/* .line 835 */
} // .end local v16 # "networkCapabilities":Landroid/net/NetworkCapabilities;
/* .restart local v4 # "networkCapabilities":Landroid/net/NetworkCapabilities; */
} // :cond_8
/* move-object/from16 v16, v4 */
} // .end local v4 # "networkCapabilities":Landroid/net/NetworkCapabilities;
/* .restart local v16 # "networkCapabilities":Landroid/net/NetworkCapabilities; */
/* if-ne v14, v12, :cond_9 */
/* .line 836 */
final String v1 = "onAvailable MOBILE slave."; // const-string v1, "onAvailable MOBILE slave."
android.util.Log .i ( v2,v1 );
/* .line 837 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, v0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z */
/* .line 838 */
/* new-instance v1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
/* .line 839 */
(( android.net.LinkProperties ) v3 ).getInterfaceName ( ); // invoke-virtual {v3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
int v9 = 4; // const/4 v9, 0x4
/* invoke-direct {v1, v0, v4, v9}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;Ljava/lang/String;I)V */
/* .line 840 */
/* .restart local v1 # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
v4 = this.mNetworkMap;
v9 = /* invoke-virtual/range {p1 ..p1}, Landroid/net/Network;->getNetId()I */
java.lang.Integer .valueOf ( v9 );
(( java.util.HashMap ) v4 ).put ( v9, v1 ); // invoke-virtual {v4, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 835 */
} // .end local v1 # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
} // :cond_9
} // :goto_4
/* .line 809 */
} // .end local v8 # "defaultDataSlotId":I
} // .end local v10 # "DataSubId":I
} // .end local v11 # "viceDataSlotId":I
} // .end local v12 # "SlaveDataSubId":I
} // .end local v13 # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // .end local v14 # "subId":I
} // .end local v16 # "networkCapabilities":Landroid/net/NetworkCapabilities;
/* .restart local v4 # "networkCapabilities":Landroid/net/NetworkCapabilities; */
} // :cond_a
/* move-object/from16 v16, v4 */
} // .end local v4 # "networkCapabilities":Landroid/net/NetworkCapabilities;
/* .restart local v16 # "networkCapabilities":Landroid/net/NetworkCapabilities; */
/* .line 791 */
} // .end local v16 # "networkCapabilities":Landroid/net/NetworkCapabilities;
/* .restart local v4 # "networkCapabilities":Landroid/net/NetworkCapabilities; */
} // :cond_b
/* move-object/from16 v16, v4 */
} // .end local v4 # "networkCapabilities":Landroid/net/NetworkCapabilities;
/* .restart local v16 # "networkCapabilities":Landroid/net/NetworkCapabilities; */
/* .line 789 */
} // .end local v16 # "networkCapabilities":Landroid/net/NetworkCapabilities;
/* .restart local v4 # "networkCapabilities":Landroid/net/NetworkCapabilities; */
} // :cond_c
/* move-object/from16 v16, v4 */
/* .line 845 */
} // .end local v4 # "networkCapabilities":Landroid/net/NetworkCapabilities;
} // .end local v7 # "inetAddress":Ljava/net/InetAddress;
/* .restart local v16 # "networkCapabilities":Landroid/net/NetworkCapabilities; */
} // :goto_5
/* move-object/from16 v1, p1 */
/* move-object/from16 v4, v16 */
/* goto/16 :goto_0 */
/* .line 847 */
} // .end local v16 # "networkCapabilities":Landroid/net/NetworkCapabilities;
/* .restart local v4 # "networkCapabilities":Landroid/net/NetworkCapabilities; */
} // :cond_d
/* invoke-direct/range {p0 ..p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->callbackNetworkInterface()V */
/* .line 848 */
/* invoke-direct/range {p0 ..p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->callbackDefaultNetworkInterface()V */
/* .line 849 */
return;
/* .line 784 */
} // :cond_e
/* move-object/from16 v16, v4 */
/* .line 785 */
} // .end local v4 # "networkCapabilities":Landroid/net/NetworkCapabilities;
/* .restart local v16 # "networkCapabilities":Landroid/net/NetworkCapabilities; */
} // :goto_6
return;
} // .end method
private void interfaceOnLost ( android.net.Network p0 ) {
/* .locals 6 */
/* .param p1, "network" # Landroid/net/Network; */
/* .line 853 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
/* if-nez p1, :cond_0 */
/* .line 854 */
final String v1 = "interfaceOnLost network is null."; // const-string v1, "interfaceOnLost network is null."
android.util.Log .e ( v0,v1 );
/* .line 855 */
return;
/* .line 858 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onLost interface Network:"; // const-string v2, "onLost interface Network:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v0,v1 );
/* .line 860 */
v0 = this.mNetworkMap;
v1 = (( android.net.Network ) p1 ).getNetId ( ); // invoke-virtual {p1}, Landroid/net/Network;->getNetId()I
java.lang.Integer .valueOf ( v1 );
v0 = (( java.util.HashMap ) v0 ).containsKey ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 861 */
v0 = this.mInterface;
v1 = this.mNetworkMap;
v2 = (( android.net.Network ) p1 ).getNetId ( ); // invoke-virtual {p1}, Landroid/net/Network;->getNetId()I
java.lang.Integer .valueOf ( v2 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
(( com.xiaomi.NetworkBoost.StatusManager$NetworkCallbackInterface ) v1 ).getInterfaceName ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->getInterfaceName()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).indexOf ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
int v1 = -1; // const/4 v1, -0x1
/* if-eq v0, v1, :cond_3 */
/* .line 862 */
v0 = this.mInterface;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.mNetworkMap;
v3 = (( android.net.Network ) p1 ).getNetId ( ); // invoke-virtual {p1}, Landroid/net/Network;->getNetId()I
java.lang.Integer .valueOf ( v3 );
(( java.util.HashMap ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
(( com.xiaomi.NetworkBoost.StatusManager$NetworkCallbackInterface ) v2 ).getInterfaceName ( ); // invoke-virtual {v2}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->getInterfaceName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ","; // const-string v2, ","
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = ""; // const-string v2, ""
(( java.lang.String ) v0 ).replaceAll ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
this.mInterface = v0;
/* .line 863 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I */
int v1 = 1; // const/4 v1, 0x1
/* sub-int/2addr v0, v1 */
/* iput v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I */
/* .line 864 */
v0 = this.mNetworkMap;
v2 = (( android.net.Network ) p1 ).getNetId ( ); // invoke-virtual {p1}, Landroid/net/Network;->getNetId()I
java.lang.Integer .valueOf ( v2 );
(( java.util.HashMap ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
v0 = (( com.xiaomi.NetworkBoost.StatusManager$NetworkCallbackInterface ) v0 ).getStatus ( ); // invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->getStatus()I
int v2 = 0; // const/4 v2, 0x0
/* packed-switch v0, :pswitch_data_0 */
/* .line 882 */
/* :pswitch_0 */
/* iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z */
/* .line 883 */
/* .line 872 */
/* :pswitch_1 */
/* iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z */
/* .line 873 */
v0 = this.mNetworkMap;
(( java.util.HashMap ) v0 ).values ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
/* .line 874 */
/* .local v3, "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface; */
v4 = (( com.xiaomi.NetworkBoost.StatusManager$NetworkCallbackInterface ) v3 ).getStatus ( ); // invoke-virtual {v3}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->getStatus()I
int v5 = 4; // const/4 v5, 0x4
/* if-ne v4, v5, :cond_1 */
/* .line 875 */
int v4 = 3; // const/4 v4, 0x3
(( com.xiaomi.NetworkBoost.StatusManager$NetworkCallbackInterface ) v3 ).setStatus ( v4 ); // invoke-virtual {v3, v4}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->setStatus(I)V
/* .line 876 */
/* iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z */
/* .line 877 */
/* iput-boolean v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z */
/* .line 879 */
} // .end local v3 # "intf":Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
} // :cond_1
/* .line 880 */
} // :cond_2
/* .line 869 */
/* :pswitch_2 */
/* iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiReady:Z */
/* .line 870 */
/* .line 866 */
/* :pswitch_3 */
/* iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiReady:Z */
/* .line 867 */
/* nop */
/* .line 886 */
} // :goto_1
v0 = this.mNetworkMap;
v1 = (( android.net.Network ) p1 ).getNetId ( ); // invoke-virtual {p1}, Landroid/net/Network;->getNetId()I
java.lang.Integer .valueOf ( v1 );
(( java.util.HashMap ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 890 */
} // :cond_3
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->callbackNetworkInterface()V */
/* .line 891 */
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean isDualDataSupported ( ) {
/* .locals 3 */
/* .line 1357 */
v0 = this.mContext;
/* .line 1358 */
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x1105003e */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* .line 1359 */
/* .local v0, "is_dual_data_support":Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "isDualDataSupported = "; // const-string v2, "isDualDataSupported = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkBoostStatusManager"; // const-string v2, "NetworkBoostStatusManager"
android.util.Log .d ( v2,v1 );
/* .line 1360 */
} // .end method
private Boolean isLteNrNwType ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "rat" # I */
/* .line 1318 */
v0 = /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->isLteNwType(I)Z */
/* if-nez v0, :cond_1 */
v0 = /* invoke-direct {p0, p1}, Lcom/xiaomi/NetworkBoost/StatusManager;->isNrNwType(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean isLteNwType ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "rat" # I */
/* .line 1308 */
/* const/16 v0, 0xd */
/* if-eq p1, v0, :cond_1 */
/* const/16 v0, 0x13 */
/* if-ne p1, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean isMobileDataSettingOn ( ) {
/* .locals 3 */
/* .line 1444 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getMobileDataSettingName()Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
} // .end method
private void isModemSingnalStrengthStatus ( ) {
/* .locals 12 */
/* .line 1240 */
v0 = this.mTelephonyManager;
/* if-nez v0, :cond_0 */
/* .line 1241 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
final String v1 = "TelephonyManager is null"; // const-string v1, "TelephonyManager is null"
android.util.Log .e ( v0,v1 );
/* .line 1242 */
return;
/* .line 1245 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 1246 */
/* .local v1, "isStatus":I */
int v2 = -1; // const/4 v2, -0x1
/* .line 1247 */
/* .local v2, "rat":I */
try { // :try_start_0
(( android.telephony.TelephonyManager ) v0 ).getSignalStrength ( ); // invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSignalStrength()Landroid/telephony/SignalStrength;
/* .line 1248 */
/* .local v0, "signalStrength":Landroid/telephony/SignalStrength; */
v3 = this.mTelephonyManager;
(( android.telephony.TelephonyManager ) v3 ).getServiceState ( ); // invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getServiceState()Landroid/telephony/ServiceState;
/* .line 1249 */
/* .local v3, "serviceState":Landroid/telephony/ServiceState; */
if ( v0 != null) { // if-eqz v0, :cond_8
/* if-nez v3, :cond_1 */
/* goto/16 :goto_3 */
/* .line 1254 */
} // :cond_1
v4 = (( android.telephony.ServiceState ) v3 ).getDataNetworkType ( ); // invoke-virtual {v3}, Landroid/telephony/ServiceState;->getDataNetworkType()I
/* move v2, v4 */
/* .line 1256 */
v4 = /* invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->isLteNrNwType(I)Z */
/* if-nez v4, :cond_2 */
/* .line 1257 */
return;
/* .line 1260 */
} // :cond_2
(( android.telephony.SignalStrength ) v0 ).getCellSignalStrengths ( ); // invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getCellSignalStrengths()Ljava/util/List;
/* .line 1261 */
/* .local v4, "cellSignalStrengths":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellSignalStrength;>;" */
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_6
/* check-cast v6, Landroid/telephony/CellSignalStrength; */
/* .line 1262 */
/* .local v6, "cellSignal":Landroid/telephony/CellSignalStrength; */
/* instance-of v7, v6, Landroid/telephony/CellSignalStrengthLte; */
/* const/16 v8, -0x8c */
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 1263 */
/* move-object v7, v6 */
/* check-cast v7, Landroid/telephony/CellSignalStrengthLte; */
/* .line 1264 */
/* .local v7, "cellLteSignal":Landroid/telephony/CellSignalStrengthLte; */
v9 = (( android.telephony.CellSignalStrengthLte ) v7 ).getRsrp ( ); // invoke-virtual {v7}, Landroid/telephony/CellSignalStrengthLte;->getRsrp()I
/* const/16 v10, -0x2b */
v8 = /* invoke-direct {p0, v9, v8, v10}, Lcom/xiaomi/NetworkBoost/StatusManager;->inRangeOrUnavailable(III)I */
/* .line 1266 */
/* .local v8, "cellLteSignalRsrp":I */
v9 = (( com.xiaomi.NetworkBoost.StatusManager ) p0 ).isPoorSignal ( v8 ); // invoke-virtual {p0, v8}, Lcom/xiaomi/NetworkBoost/StatusManager;->isPoorSignal(I)Z
if ( v9 != null) { // if-eqz v9, :cond_3
/* .line 1267 */
final String v9 = "NetworkBoostStatusManager"; // const-string v9, "NetworkBoostStatusManager"
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "isModemSingnalStrengthStatus cellLteSignalRsrp = "; // const-string v11, "isModemSingnalStrengthStatus cellLteSignalRsrp = "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v8 ); // invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v9,v10 );
/* .line 1268 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1270 */
} // .end local v7 # "cellLteSignal":Landroid/telephony/CellSignalStrengthLte;
} // .end local v8 # "cellLteSignalRsrp":I
} // :cond_3
} // :cond_4
/* instance-of v7, v6, Landroid/telephony/CellSignalStrengthNr; */
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 1271 */
/* move-object v7, v6 */
/* check-cast v7, Landroid/telephony/CellSignalStrengthNr; */
/* .line 1272 */
/* .local v7, "cellNrSignal":Landroid/telephony/CellSignalStrengthNr; */
v9 = (( android.telephony.CellSignalStrengthNr ) v7 ).getSsRsrp ( ); // invoke-virtual {v7}, Landroid/telephony/CellSignalStrengthNr;->getSsRsrp()I
/* const/16 v10, -0x2c */
v8 = /* invoke-direct {p0, v9, v8, v10}, Lcom/xiaomi/NetworkBoost/StatusManager;->inRangeOrUnavailable(III)I */
/* .line 1274 */
/* .local v8, "cellNrSignalRsrp":I */
v9 = (( com.xiaomi.NetworkBoost.StatusManager ) p0 ).isPoorSignal ( v8 ); // invoke-virtual {p0, v8}, Lcom/xiaomi/NetworkBoost/StatusManager;->isPoorSignal(I)Z
if ( v9 != null) { // if-eqz v9, :cond_5
/* .line 1275 */
final String v9 = "NetworkBoostStatusManager"; // const-string v9, "NetworkBoostStatusManager"
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "isModemSingnalStrengthStatus cellNrSignalRsrp = "; // const-string v11, "isModemSingnalStrengthStatus cellNrSignalRsrp = "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v8 ); // invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v9,v10 );
/* .line 1276 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1279 */
} // .end local v6 # "cellSignal":Landroid/telephony/CellSignalStrength;
} // .end local v7 # "cellNrSignal":Landroid/telephony/CellSignalStrengthNr;
} // .end local v8 # "cellNrSignalRsrp":I
} // :cond_5
} // :goto_1
/* .line 1280 */
} // :cond_6
v5 = this.mModemSignalStrengthListener;
/* monitor-enter v5 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1281 */
try { // :try_start_1
v6 = this.mModemSignalStrengthListener;
v7 = } // :goto_2
if ( v7 != null) { // if-eqz v7, :cond_7
/* check-cast v7, Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener; */
/* .line 1282 */
/* .local v7, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener; */
/* .line 1283 */
} // .end local v7 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener;
/* .line 1284 */
} // :cond_7
/* monitor-exit v5 */
/* .line 1288 */
} // .end local v0 # "signalStrength":Landroid/telephony/SignalStrength;
} // .end local v1 # "isStatus":I
} // .end local v2 # "rat":I
} // .end local v3 # "serviceState":Landroid/telephony/ServiceState;
} // .end local v4 # "cellSignalStrengths":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellSignalStrength;>;"
/* nop */
/* .line 1289 */
return;
/* .line 1284 */
/* .restart local v0 # "signalStrength":Landroid/telephony/SignalStrength; */
/* .restart local v1 # "isStatus":I */
/* .restart local v2 # "rat":I */
/* .restart local v3 # "serviceState":Landroid/telephony/ServiceState; */
/* .restart local v4 # "cellSignalStrengths":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellSignalStrength;>;" */
/* :catchall_0 */
/* move-exception v6 */
/* monitor-exit v5 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/xiaomi/NetworkBoost/StatusManager;
try { // :try_start_2
/* throw v6 */
/* .line 1250 */
} // .end local v4 # "cellSignalStrengths":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellSignalStrength;>;"
/* .restart local p0 # "this":Lcom/xiaomi/NetworkBoost/StatusManager; */
} // :cond_8
} // :goto_3
final String v4 = "NetworkBoostStatusManager"; // const-string v4, "NetworkBoostStatusManager"
final String v5 = "isModemSingnalStrengthStatus signalStrength or serviceState is null"; // const-string v5, "isModemSingnalStrengthStatus signalStrength or serviceState is null"
android.util.Log .d ( v4,v5 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 1251 */
return;
/* .line 1285 */
} // .end local v0 # "signalStrength":Landroid/telephony/SignalStrength;
} // .end local v1 # "isStatus":I
} // .end local v2 # "rat":I
} // .end local v3 # "serviceState":Landroid/telephony/ServiceState;
/* :catch_0 */
/* move-exception v0 */
/* .line 1286 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "isModemSingnalStrengthStatus found an error: "; // const-string v3, "isModemSingnalStrengthStatus found an error: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 1287 */
return;
} // .end method
private Boolean isNrNwType ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "rat" # I */
/* .line 1313 */
/* const/16 v0, 0x14 */
/* if-eq p1, v0, :cond_1 */
/* const/16 v0, 0x1e */
/* if-ne p1, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean isSlotActivated ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "slotId" # I */
/* .line 1378 */
miui.telephony.SubscriptionManager .getDefault ( );
(( miui.telephony.SubscriptionManager ) v0 ).getSubscriptionInfoForSlot ( p1 ); // invoke-virtual {v0, p1}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;
/* .line 1379 */
/* .local v0, "subscriptionInfo":Lmiui/telephony/SubscriptionInfo; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1380 */
final String v2 = "NetworkBoostStatusManager"; // const-string v2, "NetworkBoostStatusManager"
/* const-string/jumbo v3, "subscriptionInfo == null" */
android.util.Log .d ( v2,v3 );
/* .line 1381 */
/* .line 1383 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
v2 = (( miui.telephony.SubscriptionInfo ) v0 ).isActivated ( ); // invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->isActivated()Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
public static Boolean isTetheredIfaces ( java.lang.String p0 ) {
/* .locals 11 */
/* .param p0, "iface" # Ljava/lang/String; */
/* .line 918 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
v1 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 919 */
/* .line 922 */
} // :cond_0
(( java.lang.Object ) v1 ).getClass ( ); // invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v1 ).getDeclaredMethods ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;
/* .line 923 */
/* .local v1, "teMethods":[Ljava/lang/reflect/Method; */
/* array-length v3, v1 */
/* move v4, v2 */
} // :goto_0
/* if-ge v4, v3, :cond_4 */
/* aget-object v5, v1, v4 */
/* .line 924 */
/* .local v5, "method":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v5 ).getName ( ); // invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;
final String v7 = "getTetheredIfaces"; // const-string v7, "getTetheredIfaces"
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 926 */
try { // :try_start_0
v6 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
/* new-array v7, v2, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v5 ).invoke ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, [Ljava/lang/String; */
/* .line 927 */
/* .local v6, "allTethered":[Ljava/lang/String; */
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 928 */
/* array-length v7, v6 */
/* move v8, v2 */
} // :goto_1
/* if-ge v8, v7, :cond_2 */
/* aget-object v9, v6, v8 */
/* .line 929 */
/* .local v9, "tethered":Ljava/lang/String; */
v10 = (( java.lang.String ) p0 ).equals ( v9 ); // invoke-virtual {p0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_1
/* .line 930 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " is TetheredIfaces "; // const-string v4, " is TetheredIfaces "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 931 */
int v0 = 1; // const/4 v0, 0x1
/* .line 928 */
} // .end local v9 # "tethered":Ljava/lang/String;
} // :cond_1
/* add-int/lit8 v8, v8, 0x1 */
/* .line 938 */
} // .end local v6 # "allTethered":[Ljava/lang/String;
} // :cond_2
/* .line 935 */
/* :catch_0 */
/* move-exception v3 */
/* .line 936 */
/* .local v3, "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Exception:"; // const-string v6, "Exception:"
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v4 );
/* .line 937 */
/* .line 923 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // .end local v5 # "method":Ljava/lang/reflect/Method;
} // :cond_3
} // :goto_2
/* add-int/lit8 v4, v4, 0x1 */
/* .line 941 */
} // :cond_4
} // .end method
private Boolean isValidatePhoneId ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "phoneId" # I */
/* .line 1388 */
/* if-ltz p1, :cond_0 */
/* if-ge p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private void onNetworkPriorityChanged ( com.xiaomi.NetworkBoost.StatusManager$NetworkPriorityInfo p0 ) {
/* .locals 6 */
/* .param p1, "info" # Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo; */
/* .line 465 */
v0 = this.mNetworkPriorityListenerList;
/* monitor-enter v0 */
/* .line 466 */
try { // :try_start_0
final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
final String v2 = "CALLBACK_NETWORK_PRIORITY_CHANGED enter"; // const-string v2, "CALLBACK_NETWORK_PRIORITY_CHANGED enter"
android.util.Log .i ( v1,v2 );
/* .line 467 */
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPriorityMode:I */
/* iget v2, p1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->priorityMode:I */
/* if-ne v1, v2, :cond_0 */
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTrafficPolicy:I */
/* iget v2, p1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->trafficPolicy:I */
/* if-ne v1, v2, :cond_0 */
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mThermalLevel:I */
/* iget v2, p1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->thermalLevel:I */
/* if-ne v1, v2, :cond_0 */
/* .line 471 */
/* monitor-exit v0 */
return;
/* .line 474 */
} // :cond_0
/* iget v1, p1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->priorityMode:I */
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPriorityMode:I */
/* .line 475 */
/* iget v1, p1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->trafficPolicy:I */
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTrafficPolicy:I */
/* .line 476 */
/* iget v1, p1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;->thermalLevel:I */
/* iput v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mThermalLevel:I */
/* .line 478 */
v1 = this.mNetworkPriorityListenerList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener; */
/* .line 479 */
/* .local v2, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener; */
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPriorityMode:I */
/* iget v4, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTrafficPolicy:I */
/* iget v5, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mThermalLevel:I */
/* .line 480 */
} // .end local v2 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;
/* .line 481 */
} // :cond_1
/* monitor-exit v0 */
/* .line 482 */
return;
/* .line 481 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void processRegisterCellularCallback ( ) {
/* .locals 12 */
/* .line 1392 */
v0 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = this.mCellularNetworkCallback;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1394 */
try { // :try_start_0
(( android.net.ConnectivityManager ) v0 ).unregisterNetworkCallback ( v2 ); // invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1397 */
/* .line 1395 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1396 */
/* .local v0, "e":Ljava/lang/IllegalArgumentException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Unregister network callback exception"; // const-string v3, "Unregister network callback exception"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 1399 */
} // .end local v0 # "e":Ljava/lang/IllegalArgumentException;
} // :cond_0
} // :goto_0
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getDefaultDataSlotId()I */
/* .line 1400 */
/* .local v0, "defaultDataSlotId":I */
v2 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getViceDataSlotId()I */
/* .line 1401 */
/* .local v2, "viceDataSlotId":I */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "defaultDataSlotId = "; // const-string v4, "defaultDataSlotId = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", viceDataSlotId = "; // const-string v4, ", viceDataSlotId = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v3 );
/* .line 1402 */
v3 = /* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->isValidatePhoneId(I)Z */
int v4 = 4; // const/4 v4, 0x4
/* const/16 v5, 0xc */
/* const/16 v6, 0x10 */
int v7 = 0; // const/4 v7, 0x0
final String v8 = ", subId = "; // const-string v8, ", subId = "
if ( v3 != null) { // if-eqz v3, :cond_1
v3 = /* invoke-direct {p0, v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->isSlotActivated(I)Z */
if ( v3 != null) { // if-eqz v3, :cond_1
v3 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1403 */
miui.telephony.SubscriptionManager .getDefault ( );
v3 = (( miui.telephony.SubscriptionManager ) v3 ).getSubscriptionIdForSlot ( v0 ); // invoke-virtual {v3, v0}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I
/* .line 1404 */
/* .local v3, "DataSubId":I */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "register default slot callback defaultslot = "; // const-string v10, "register default slot callback defaultslot = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v0 ); // invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v3 ); // invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v9 );
/* .line 1407 */
/* new-instance v9, Landroid/net/NetworkRequest$Builder; */
/* invoke-direct {v9}, Landroid/net/NetworkRequest$Builder;-><init>()V */
/* .line 1408 */
(( android.net.NetworkRequest$Builder ) v9 ).addTransportType ( v7 ); // invoke-virtual {v9, v7}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;
/* .line 1409 */
(( android.net.NetworkRequest$Builder ) v9 ).addCapability ( v6 ); // invoke-virtual {v9, v6}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 1410 */
(( android.net.NetworkRequest$Builder ) v9 ).addCapability ( v5 ); // invoke-virtual {v9, v5}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 1411 */
(( android.net.NetworkRequest$Builder ) v9 ).removeCapability ( v4 ); // invoke-virtual {v9, v4}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 1413 */
miui.telephony.SubscriptionManager .getDefault ( );
v10 = (( miui.telephony.SubscriptionManager ) v10 ).getSubscriptionIdForSlot ( v0 ); // invoke-virtual {v10, v0}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I
java.lang.String .valueOf ( v10 );
/* .line 1412 */
(( android.net.NetworkRequest$Builder ) v9 ).setNetworkSpecifier ( v10 ); // invoke-virtual {v9, v10}, Landroid/net/NetworkRequest$Builder;->setNetworkSpecifier(Ljava/lang/String;)Landroid/net/NetworkRequest$Builder;
/* .line 1414 */
(( android.net.NetworkRequest$Builder ) v9 ).build ( ); // invoke-virtual {v9}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;
/* .line 1415 */
/* .local v9, "networkRequestCellular_0":Landroid/net/NetworkRequest; */
v10 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
v11 = this.mCellularNetworkCallback;
(( android.net.ConnectivityManager ) v10 ).registerNetworkCallback ( v9, v11 ); // invoke-virtual {v10, v9, v11}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 1418 */
} // .end local v3 # "DataSubId":I
} // .end local v9 # "networkRequestCellular_0":Landroid/net/NetworkRequest;
} // :cond_1
v3 = /* invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->isValidatePhoneId(I)Z */
if ( v3 != null) { // if-eqz v3, :cond_2
v3 = /* invoke-direct {p0, v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->isSlotActivated(I)Z */
if ( v3 != null) { // if-eqz v3, :cond_2
v3 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1419 */
miui.telephony.SubscriptionManager .getDefault ( );
v3 = (( miui.telephony.SubscriptionManager ) v3 ).getSubscriptionIdForSlot ( v2 ); // invoke-virtual {v3, v2}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I
/* .line 1420 */
/* .local v3, "SlaveDataSubId":I */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "register vice slot callback viceslot = "; // const-string v10, "register vice slot callback viceslot = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v2 ); // invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v8 );
/* .line 1423 */
/* new-instance v1, Landroid/net/NetworkRequest$Builder; */
/* invoke-direct {v1}, Landroid/net/NetworkRequest$Builder;-><init>()V */
/* .line 1424 */
(( android.net.NetworkRequest$Builder ) v1 ).addTransportType ( v7 ); // invoke-virtual {v1, v7}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;
/* .line 1425 */
(( android.net.NetworkRequest$Builder ) v1 ).addCapability ( v6 ); // invoke-virtual {v1, v6}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 1426 */
(( android.net.NetworkRequest$Builder ) v1 ).addCapability ( v5 ); // invoke-virtual {v1, v5}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 1427 */
(( android.net.NetworkRequest$Builder ) v1 ).removeCapability ( v4 ); // invoke-virtual {v1, v4}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 1429 */
miui.telephony.SubscriptionManager .getDefault ( );
v4 = (( miui.telephony.SubscriptionManager ) v4 ).getSubscriptionIdForSlot ( v2 ); // invoke-virtual {v4, v2}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I
java.lang.String .valueOf ( v4 );
/* .line 1428 */
(( android.net.NetworkRequest$Builder ) v1 ).setNetworkSpecifier ( v4 ); // invoke-virtual {v1, v4}, Landroid/net/NetworkRequest$Builder;->setNetworkSpecifier(Ljava/lang/String;)Landroid/net/NetworkRequest$Builder;
/* .line 1430 */
(( android.net.NetworkRequest$Builder ) v1 ).build ( ); // invoke-virtual {v1}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;
/* .line 1431 */
/* .local v1, "networkRequestCellular_1":Landroid/net/NetworkRequest; */
v4 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
v5 = this.mCellularNetworkCallback;
(( android.net.ConnectivityManager ) v4 ).registerNetworkCallback ( v1, v5 ); // invoke-virtual {v4, v1, v5}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 1433 */
} // .end local v1 # "networkRequestCellular_1":Landroid/net/NetworkRequest;
} // .end local v3 # "SlaveDataSubId":I
} // :cond_2
return;
} // .end method
private void processSimStateChanged ( android.content.Intent p0 ) {
/* .locals 6 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 1436 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getDefaultDataSlotId()I */
/* .line 1437 */
/* .local v0, "defaultDataSlotId":I */
v1 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getViceDataSlotId()I */
/* .line 1438 */
/* .local v1, "viceDataSlotId":I */
v2 = this.mHandler;
/* const/16 v3, 0x67 */
v2 = (( android.os.Handler ) v2 ).hasMessages ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->hasMessages(I)Z
/* if-nez v2, :cond_0 */
/* .line 1439 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).obtainMessage ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* const-wide/16 v4, 0x2710 */
(( android.os.Handler ) v2 ).sendMessageDelayed ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1441 */
} // :cond_0
return;
} // .end method
private void registerAudioModeChangedListener ( ) {
/* .locals 3 */
/* .line 367 */
v0 = this.mContext;
final String v1 = "audio"; // const-string v1, "audio"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
this.mAudioManager = v0;
/* .line 369 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$3; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$3;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V */
this.mAudioModeListener = v0;
/* .line 382 */
try { // :try_start_0
v0 = this.mAudioManager;
/* .line 383 */
java.util.concurrent.Executors .newSingleThreadExecutor ( );
v2 = this.mAudioModeListener;
/* .line 382 */
(( android.media.AudioManager ) v0 ).addOnModeChangedListener ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/media/AudioManager;->addOnModeChangedListener(Ljava/util/concurrent/Executor;Landroid/media/AudioManager$OnModeChangedListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 386 */
/* .line 384 */
/* :catch_0 */
/* move-exception v0 */
/* .line 387 */
} // :goto_0
return;
} // .end method
private void registerDefaultNetworkCallback ( ) {
/* .locals 2 */
/* .line 994 */
v0 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
/* if-nez v0, :cond_0 */
/* .line 995 */
return;
/* .line 998 */
} // :cond_0
v1 = this.mDefaultNetworkCallback;
(( android.net.ConnectivityManager ) v0 ).registerDefaultNetworkCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->registerDefaultNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 1000 */
return;
} // .end method
private void registerForegroundInfoListener ( ) {
/* .locals 3 */
/* .line 281 */
try { // :try_start_0
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;-><init>(Landroid/content/Context;)V */
this.mNetworkBoostProcessMonitor = v0;
/* .line 282 */
v1 = this.mForegroundInfoListener;
(( com.xiaomi.NetworkBoost.NetworkBoostProcessMonitor ) v0 ).registerForegroundInfoListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 285 */
/* .line 283 */
/* :catch_0 */
/* move-exception v0 */
/* .line 284 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkBoostStatusManager"; // const-string v2, "NetworkBoostStatusManager"
android.util.Log .e ( v2,v1 );
/* .line 286 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void registerModemSignalStrengthObserver ( ) {
/* .locals 4 */
/* .line 1229 */
v0 = this.mContext;
/* .line 1230 */
final String v1 = "phone"; // const-string v1, "phone"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/telephony/TelephonyManager; */
this.mTelephonyManager = v0;
/* .line 1231 */
v0 = this.mHandler;
/* const/16 v1, 0x69 */
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* const-wide/16 v2, 0x7530 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1233 */
return;
} // .end method
private void registerMovementSensorListener ( ) {
/* .locals 5 */
/* .line 1546 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorFlag:Z */
final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1547 */
final String v0 = "other registerMovementSensorListener succeed!"; // const-string v0, "other registerMovementSensorListener succeed!"
android.util.Log .e ( v1,v0 );
/* .line 1548 */
return;
/* .line 1551 */
} // :cond_0
v0 = this.mSensorManager;
/* if-nez v0, :cond_1 */
/* .line 1552 */
v0 = this.mContext;
/* const-string/jumbo v2, "sensor" */
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/SensorManager; */
this.mSensorManager = v0;
/* .line 1555 */
} // :cond_1
v0 = this.mSensorManager;
/* const v2, 0x1fa267e */
int v3 = 0; // const/4 v3, 0x0
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;
/* .line 1557 */
/* .local v0, "movementSensor":Landroid/hardware/Sensor; */
/* if-nez v0, :cond_2 */
/* .line 1558 */
final String v2 = "registerMovementSensorListener is fail!"; // const-string v2, "registerMovementSensorListener is fail!"
android.util.Log .e ( v1,v2 );
/* .line 1559 */
return;
/* .line 1562 */
} // :cond_2
v2 = this.mSensorManager;
v3 = this.mSensorListener;
int v4 = 3; // const/4 v4, 0x3
v2 = (( android.hardware.SensorManager ) v2 ).registerListener ( v3, v0, v4 ); // invoke-virtual {v2, v3, v0, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* iput-boolean v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorFlag:Z */
/* .line 1564 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "registerMovementSensorListener sensorFlag:"; // const-string v3, "registerMovementSensorListener sensorFlag:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorFlag:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v2 );
/* .line 1565 */
return;
} // .end method
private void registerNetworkCallback ( ) {
/* .locals 13 */
/* .line 619 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "NetworkInterfaceHandler"; // const-string v1, "NetworkInterfaceHandler"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 620 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 621 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 623 */
v0 = this.mContext;
final String v1 = "connectivity"; // const-string v1, "connectivity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/ConnectivityManager; */
/* .line 624 */
v0 = this.mContext;
/* const-string/jumbo v1, "wifi" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/WifiManager; */
/* .line 625 */
v0 = this.mContext;
final String v1 = "SlaveWifiService"; // const-string v1, "SlaveWifiService"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/SlaveWifiManager; */
/* .line 628 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->isDualDataSupported()Z */
final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
/* const/16 v2, 0xc */
/* const/16 v3, 0x10 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 629 */
final String v0 = "8550 platform version"; // const-string v0, "8550 platform version"
android.util.Log .d ( v1,v0 );
/* .line 631 */
v0 = this.mHandler;
/* const/16 v1, 0x67 */
v0 = (( android.os.Handler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z
/* if-nez v0, :cond_1 */
/* .line 632 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* const-wide/16 v4, 0x2710 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v4, v5 ); // invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 635 */
} // :cond_0
final String v0 = "others platform version"; // const-string v0, "others platform version"
android.util.Log .d ( v1,v0 );
/* .line 637 */
/* new-instance v0, Landroid/net/NetworkRequest$Builder; */
/* invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V */
/* .line 638 */
int v1 = 0; // const/4 v1, 0x0
(( android.net.NetworkRequest$Builder ) v0 ).addTransportType ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;
/* .line 639 */
int v1 = 4; // const/4 v1, 0x4
(( android.net.NetworkRequest$Builder ) v0 ).removeCapability ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 640 */
(( android.net.NetworkRequest$Builder ) v0 ).addCapability ( v3 ); // invoke-virtual {v0, v3}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 641 */
(( android.net.NetworkRequest$Builder ) v0 ).addCapability ( v2 ); // invoke-virtual {v0, v2}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 642 */
(( android.net.NetworkRequest$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;
/* .line 643 */
/* .local v0, "networkRequestCellular":Landroid/net/NetworkRequest; */
v1 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
v4 = this.mCellularNetworkCallback;
(( android.net.ConnectivityManager ) v1 ).registerNetworkCallback ( v0, v4 ); // invoke-virtual {v1, v0, v4}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 647 */
} // .end local v0 # "networkRequestCellular":Landroid/net/NetworkRequest;
} // :cond_1
} // :goto_0
/* new-instance v0, Landroid/net/wifi/WifiNetworkSpecifier$Builder; */
/* invoke-direct {v0}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;-><init>()V */
/* .line 648 */
/* .local v0, "specifierBuilderWifi6G":Landroid/net/wifi/WifiNetworkSpecifier$Builder; */
/* const/16 v1, 0x8 */
(( android.net.wifi.WifiNetworkSpecifier$Builder ) v0 ).setBand ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->setBand(I)Landroid/net/wifi/WifiNetworkSpecifier$Builder;
/* .line 649 */
/* new-instance v1, Landroid/net/NetworkRequest$Builder; */
/* invoke-direct {v1}, Landroid/net/NetworkRequest$Builder;-><init>()V */
/* .line 650 */
int v4 = 1; // const/4 v4, 0x1
(( android.net.NetworkRequest$Builder ) v1 ).addTransportType ( v4 ); // invoke-virtual {v1, v4}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;
/* .line 651 */
(( android.net.NetworkRequest$Builder ) v1 ).addCapability ( v2 ); // invoke-virtual {v1, v2}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 652 */
(( android.net.NetworkRequest$Builder ) v1 ).addCapability ( v3 ); // invoke-virtual {v1, v3}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 653 */
/* const/16 v5, 0x11 */
(( android.net.NetworkRequest$Builder ) v1 ).removeCapability ( v5 ); // invoke-virtual {v1, v5}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 654 */
/* const/16 v6, 0x18 */
(( android.net.NetworkRequest$Builder ) v1 ).removeCapability ( v6 ); // invoke-virtual {v1, v6}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 655 */
(( android.net.wifi.WifiNetworkSpecifier$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->build()Landroid/net/wifi/WifiNetworkSpecifier;
(( android.net.NetworkRequest$Builder ) v1 ).setNetworkSpecifier ( v7 ); // invoke-virtual {v1, v7}, Landroid/net/NetworkRequest$Builder;->setNetworkSpecifier(Landroid/net/NetworkSpecifier;)Landroid/net/NetworkRequest$Builder;
/* .line 656 */
(( android.net.NetworkRequest$Builder ) v1 ).build ( ); // invoke-virtual {v1}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;
/* .line 657 */
/* .local v1, "networkRequestWifi6G":Landroid/net/NetworkRequest; */
v7 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
v8 = this.mWifiNetworkCallback;
(( android.net.ConnectivityManager ) v7 ).registerNetworkCallback ( v1, v8 ); // invoke-virtual {v7, v1, v8}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 660 */
/* new-instance v7, Landroid/net/wifi/WifiNetworkSpecifier$Builder; */
/* invoke-direct {v7}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;-><init>()V */
/* .line 661 */
/* .local v7, "specifierBuilderWifi5G":Landroid/net/wifi/WifiNetworkSpecifier$Builder; */
int v8 = 2; // const/4 v8, 0x2
(( android.net.wifi.WifiNetworkSpecifier$Builder ) v7 ).setBand ( v8 ); // invoke-virtual {v7, v8}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->setBand(I)Landroid/net/wifi/WifiNetworkSpecifier$Builder;
/* .line 662 */
/* new-instance v8, Landroid/net/NetworkRequest$Builder; */
/* invoke-direct {v8}, Landroid/net/NetworkRequest$Builder;-><init>()V */
/* .line 663 */
(( android.net.NetworkRequest$Builder ) v8 ).addTransportType ( v4 ); // invoke-virtual {v8, v4}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;
/* .line 664 */
(( android.net.NetworkRequest$Builder ) v8 ).addCapability ( v2 ); // invoke-virtual {v8, v2}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 665 */
(( android.net.NetworkRequest$Builder ) v8 ).addCapability ( v3 ); // invoke-virtual {v8, v3}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 666 */
(( android.net.NetworkRequest$Builder ) v8 ).removeCapability ( v5 ); // invoke-virtual {v8, v5}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 667 */
(( android.net.NetworkRequest$Builder ) v8 ).removeCapability ( v6 ); // invoke-virtual {v8, v6}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 668 */
(( android.net.wifi.WifiNetworkSpecifier$Builder ) v7 ).build ( ); // invoke-virtual {v7}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->build()Landroid/net/wifi/WifiNetworkSpecifier;
(( android.net.NetworkRequest$Builder ) v8 ).setNetworkSpecifier ( v9 ); // invoke-virtual {v8, v9}, Landroid/net/NetworkRequest$Builder;->setNetworkSpecifier(Landroid/net/NetworkSpecifier;)Landroid/net/NetworkRequest$Builder;
/* .line 669 */
(( android.net.NetworkRequest$Builder ) v8 ).build ( ); // invoke-virtual {v8}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;
/* .line 670 */
/* .local v8, "networkRequestWifi5G":Landroid/net/NetworkRequest; */
v9 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
v10 = this.mWifiNetworkCallback;
(( android.net.ConnectivityManager ) v9 ).registerNetworkCallback ( v8, v10 ); // invoke-virtual {v9, v8, v10}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 673 */
v9 = com.xiaomi.NetworkBoost.StatusManager.mSlaveWifiManager;
if ( v9 != null) { // if-eqz v9, :cond_2
/* .line 674 */
/* new-instance v9, Landroid/net/wifi/WifiNetworkSpecifier$Builder; */
/* invoke-direct {v9}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;-><init>()V */
/* .line 675 */
/* .local v9, "specifierBuilderWifiHbs5G":Landroid/net/wifi/WifiNetworkSpecifier$Builder; */
/* const/16 v10, 0x102 */
(( android.net.wifi.WifiNetworkSpecifier$Builder ) v9 ).setBand ( v10 ); // invoke-virtual {v9, v10}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->setBand(I)Landroid/net/wifi/WifiNetworkSpecifier$Builder;
/* .line 676 */
/* new-instance v10, Landroid/net/NetworkRequest$Builder; */
/* invoke-direct {v10}, Landroid/net/NetworkRequest$Builder;-><init>()V */
/* .line 677 */
(( android.net.NetworkRequest$Builder ) v10 ).addTransportType ( v4 ); // invoke-virtual {v10, v4}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;
/* .line 678 */
(( android.net.NetworkRequest$Builder ) v10 ).addCapability ( v2 ); // invoke-virtual {v10, v2}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 679 */
(( android.net.NetworkRequest$Builder ) v10 ).addCapability ( v3 ); // invoke-virtual {v10, v3}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 680 */
(( android.net.NetworkRequest$Builder ) v10 ).removeCapability ( v5 ); // invoke-virtual {v10, v5}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 681 */
(( android.net.NetworkRequest$Builder ) v10 ).removeCapability ( v6 ); // invoke-virtual {v10, v6}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 682 */
(( android.net.wifi.WifiNetworkSpecifier$Builder ) v9 ).build ( ); // invoke-virtual {v9}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->build()Landroid/net/wifi/WifiNetworkSpecifier;
(( android.net.NetworkRequest$Builder ) v10 ).setNetworkSpecifier ( v11 ); // invoke-virtual {v10, v11}, Landroid/net/NetworkRequest$Builder;->setNetworkSpecifier(Landroid/net/NetworkSpecifier;)Landroid/net/NetworkRequest$Builder;
/* .line 683 */
(( android.net.NetworkRequest$Builder ) v10 ).build ( ); // invoke-virtual {v10}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;
/* .line 684 */
/* .local v10, "networkRequestWifiHbs5G":Landroid/net/NetworkRequest; */
v11 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
v12 = this.mWifiNetworkCallback;
(( android.net.ConnectivityManager ) v11 ).registerNetworkCallback ( v10, v12 ); // invoke-virtual {v11, v10, v12}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 688 */
} // .end local v9 # "specifierBuilderWifiHbs5G":Landroid/net/wifi/WifiNetworkSpecifier$Builder;
} // .end local v10 # "networkRequestWifiHbs5G":Landroid/net/NetworkRequest;
} // :cond_2
/* new-instance v9, Landroid/net/wifi/WifiNetworkSpecifier$Builder; */
/* invoke-direct {v9}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;-><init>()V */
/* .line 689 */
/* .local v9, "specifierBuilderWifi24G":Landroid/net/wifi/WifiNetworkSpecifier$Builder; */
(( android.net.wifi.WifiNetworkSpecifier$Builder ) v9 ).setBand ( v4 ); // invoke-virtual {v9, v4}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->setBand(I)Landroid/net/wifi/WifiNetworkSpecifier$Builder;
/* .line 690 */
/* new-instance v10, Landroid/net/NetworkRequest$Builder; */
/* invoke-direct {v10}, Landroid/net/NetworkRequest$Builder;-><init>()V */
/* .line 691 */
(( android.net.NetworkRequest$Builder ) v10 ).addTransportType ( v4 ); // invoke-virtual {v10, v4}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;
/* .line 692 */
(( android.net.NetworkRequest$Builder ) v4 ).addCapability ( v2 ); // invoke-virtual {v4, v2}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 693 */
(( android.net.NetworkRequest$Builder ) v2 ).addCapability ( v3 ); // invoke-virtual {v2, v3}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 694 */
(( android.net.NetworkRequest$Builder ) v2 ).removeCapability ( v5 ); // invoke-virtual {v2, v5}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 695 */
(( android.net.NetworkRequest$Builder ) v2 ).removeCapability ( v6 ); // invoke-virtual {v2, v6}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 696 */
(( android.net.wifi.WifiNetworkSpecifier$Builder ) v9 ).build ( ); // invoke-virtual {v9}, Landroid/net/wifi/WifiNetworkSpecifier$Builder;->build()Landroid/net/wifi/WifiNetworkSpecifier;
(( android.net.NetworkRequest$Builder ) v2 ).setNetworkSpecifier ( v3 ); // invoke-virtual {v2, v3}, Landroid/net/NetworkRequest$Builder;->setNetworkSpecifier(Landroid/net/NetworkSpecifier;)Landroid/net/NetworkRequest$Builder;
/* .line 697 */
(( android.net.NetworkRequest$Builder ) v2 ).build ( ); // invoke-virtual {v2}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;
/* .line 698 */
/* .local v2, "networkRequestWifi24G":Landroid/net/NetworkRequest; */
v3 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
v4 = this.mWifiNetworkCallback;
(( android.net.ConnectivityManager ) v3 ).registerNetworkCallback ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 699 */
return;
} // .end method
private void registerScreenStatusReceiver ( ) {
/* .locals 4 */
/* .line 1139 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$8; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$8;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V */
this.mScreenStatusReceiver = v0;
/* .line 1160 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 1161 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1162 */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1163 */
v1 = this.mContext;
v2 = this.mScreenStatusReceiver;
int v3 = 4; // const/4 v3, 0x4
(( android.content.Context ) v1 ).registerReceiver ( v2, v0, v3 ); // invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 1165 */
v1 = this.mContext;
final String v2 = "power"; // const-string v2, "power"
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/os/PowerManager; */
this.mPowerManager = v1;
/* .line 1166 */
return;
} // .end method
private void registerTelephonyStatusReceiver ( ) {
/* .locals 3 */
/* .line 1331 */
v0 = /* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->isDualDataSupported()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1332 */
/* new-instance v0, Lcom/xiaomi/NetworkBoost/StatusManager$9; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/StatusManager$9;-><init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V */
this.mTelephonyStatusReceiver = v0;
/* .line 1342 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 1343 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.SIM_STATE_CHANGED"; // const-string v1, "android.intent.action.SIM_STATE_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1344 */
v1 = this.mContext;
v2 = this.mTelephonyStatusReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 1346 */
} // .end local v0 # "filter":Landroid/content/IntentFilter;
} // :cond_0
return;
} // .end method
private void registerUidObserver ( ) {
/* .locals 5 */
/* .line 321 */
try { // :try_start_0
android.app.ActivityManagerNative .getDefault ( );
this.mActivityManager = v0;
/* .line 322 */
v1 = this.mUidObserver;
int v2 = -1; // const/4 v2, -0x1
int v3 = 0; // const/4 v3, 0x0
int v4 = 2; // const/4 v4, 0x2
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 326 */
/* .line 324 */
/* :catch_0 */
/* move-exception v0 */
/* .line 325 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkBoostStatusManager"; // const-string v2, "NetworkBoostStatusManager"
android.util.Log .e ( v2,v1 );
/* .line 327 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void setResloverInterfaceIdAndNetid ( Integer p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "netId" # I */
/* .param p2, "interfaceId" # Ljava/lang/String; */
/* .line 1526 */
try { // :try_start_0
v0 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
(( java.lang.Object ) v0 ).getClass ( ); // invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v0 ).getName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
java.lang.Class .forName ( v0 );
/* .line 1527 */
/* .local v0, "ConnectivityManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* const-string/jumbo v1, "setResloverInterfaceIdAndNetid" */
int v2 = 2; // const/4 v2, 0x2
/* new-array v3, v2, [Ljava/lang/Class; */
v4 = java.lang.Integer.TYPE;
int v5 = 0; // const/4 v5, 0x0
/* aput-object v4, v3, v5 */
/* const-class v4, Ljava/lang/String; */
int v6 = 1; // const/4 v6, 0x1
/* aput-object v4, v3, v6 */
/* .line 1528 */
(( java.lang.Class ) v0 ).getDeclaredMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 1529 */
/* .local v1, "setResloverInterfaceIdAndNetidMethod":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v1 ).setAccessible ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 1530 */
v3 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
/* new-array v2, v2, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( p1 );
/* aput-object v4, v2, v5 */
/* aput-object p2, v2, v6 */
(( java.lang.reflect.Method ) v1 ).invoke ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1533 */
/* nop */
} // .end local v0 # "ConnectivityManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v1 # "setResloverInterfaceIdAndNetidMethod":Ljava/lang/reflect/Method;
/* .line 1531 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1532 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1534 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void setSmartDNSInterface ( android.net.Network p0 ) {
/* .locals 13 */
/* .param p1, "network" # Landroid/net/Network; */
/* .line 1460 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1462 */
/* .local v0, "wifiInfo":Landroid/net/wifi/WifiInfo; */
final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
/* if-nez p1, :cond_0 */
/* .line 1463 */
final String v2 = "SmartDNS onAvailable parameter is null."; // const-string v2, "SmartDNS onAvailable parameter is null."
android.util.Log .e ( v1,v2 );
/* .line 1464 */
return;
/* .line 1467 */
} // :cond_0
v2 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
(( android.net.ConnectivityManager ) v2 ).getNetworkInfo ( p1 ); // invoke-virtual {v2, p1}, Landroid/net/ConnectivityManager;->getNetworkInfo(Landroid/net/Network;)Landroid/net/NetworkInfo;
/* .line 1468 */
/* .local v2, "networkInfo":Landroid/net/NetworkInfo; */
/* if-nez v2, :cond_1 */
/* .line 1469 */
final String v3 = "SmartDNS onAvailable getNetworkInfo error."; // const-string v3, "SmartDNS onAvailable getNetworkInfo error."
android.util.Log .e ( v1,v3 );
/* .line 1470 */
return;
/* .line 1473 */
} // :cond_1
v3 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
(( android.net.ConnectivityManager ) v3 ).getNetworkCapabilities ( p1 ); // invoke-virtual {v3, p1}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;
/* .line 1474 */
/* .local v3, "networkCapabilities":Landroid/net/NetworkCapabilities; */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1475 */
int v4 = 4; // const/4 v4, 0x4
v4 = (( android.net.NetworkCapabilities ) v3 ).hasTransport ( v4 ); // invoke-virtual {v3, v4}, Landroid/net/NetworkCapabilities;->hasTransport(I)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 1476 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "SmartDNS "; // const-string v5, "SmartDNS "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.net.NetworkInfo ) v2 ).getTypeName ( ); // invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " onAvailable vpn."; // const-string v5, " onAvailable vpn."
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v4 );
/* .line 1477 */
return;
/* .line 1480 */
} // :cond_2
(( android.net.NetworkInfo ) v2 ).getTypeName ( ); // invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;
final String v5 = "WIFI"; // const-string v5, "WIFI"
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 1481 */
v4 = com.xiaomi.NetworkBoost.StatusManager.mWifiManager;
(( android.net.wifi.WifiManager ) v4 ).getCurrentNetwork ( ); // invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getCurrentNetwork()Landroid/net/Network;
/* .line 1482 */
/* .local v4, "currentNetwork":Landroid/net/Network; */
if ( v4 != null) { // if-eqz v4, :cond_3
v5 = (( android.net.Network ) v4 ).equals ( p1 ); // invoke-virtual {v4, p1}, Landroid/net/Network;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 1483 */
final String v5 = "SmartDNS onAvailable WIFI master."; // const-string v5, "SmartDNS onAvailable WIFI master."
android.util.Log .i ( v1,v5 );
/* .line 1484 */
v5 = com.xiaomi.NetworkBoost.StatusManager.mWifiManager;
(( android.net.wifi.WifiManager ) v5 ).getConnectionInfo ( ); // invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;
/* .line 1486 */
} // :cond_3
final String v5 = "SmartDNS onAvailable WIFI slave"; // const-string v5, "SmartDNS onAvailable WIFI slave"
android.util.Log .i ( v1,v5 );
/* .line 1487 */
v5 = com.xiaomi.NetworkBoost.StatusManager.mSlaveWifiManager;
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 1488 */
(( android.net.wifi.SlaveWifiManager ) v5 ).getWifiSlaveConnectionInfo ( ); // invoke-virtual {v5}, Landroid/net/wifi/SlaveWifiManager;->getWifiSlaveConnectionInfo()Landroid/net/wifi/WifiInfo;
/* .line 1490 */
} // :cond_4
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_5
(( android.net.wifi.WifiInfo ) v0 ).getBSSID ( ); // invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 1491 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "SmartDNS push DnsReslover a relationship, netid "; // const-string v6, "SmartDNS push DnsReslover a relationship, netid "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1492 */
v6 = (( android.net.Network ) p1 ).getNetId ( ); // invoke-virtual {p1}, Landroid/net/Network;->getNetId()I
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " name "; // const-string v6, " name "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.net.wifi.WifiInfo ) v0 ).getSSID ( ); // invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1491 */
android.util.Log .i ( v1,v5 );
/* .line 1493 */
v1 = (( android.net.Network ) p1 ).getNetId ( ); // invoke-virtual {p1}, Landroid/net/Network;->getNetId()I
(( android.net.wifi.WifiInfo ) v0 ).getSSID ( ); // invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;
/* invoke-direct {p0, v1, v5}, Lcom/xiaomi/NetworkBoost/StatusManager;->setResloverInterfaceIdAndNetid(ILjava/lang/String;)V */
/* .line 1495 */
} // .end local v4 # "currentNetwork":Landroid/net/Network;
} // :cond_5
/* goto/16 :goto_2 */
/* .line 1496 */
} // :cond_6
v4 = this.mContext;
/* .line 1497 */
final String v5 = "phone"; // const-string v5, "phone"
(( android.content.Context ) v4 ).getSystemService ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v4, Landroid/telephony/TelephonyManager; */
/* .line 1498 */
/* .local v4, "mTelephonyManager":Landroid/telephony/TelephonyManager; */
(( android.telephony.TelephonyManager ) v4 ).getCellLocation ( ); // invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;
/* .line 1500 */
/* .local v5, "location":Landroid/telephony/CellLocation; */
/* instance-of v6, v5, Landroid/telephony/gsm/GsmCellLocation; */
final String v7 = "SmartDNS \t LAC = "; // const-string v7, "SmartDNS \t LAC = "
/* const-wide/16 v8, 0x0 */
if ( v6 != null) { // if-eqz v6, :cond_7
/* .line 1501 */
/* move-object v6, v5 */
/* check-cast v6, Landroid/telephony/gsm/GsmCellLocation; */
/* .line 1502 */
/* .local v6, "gsmCellLocation":Landroid/telephony/gsm/GsmCellLocation; */
if ( v6 != null) { // if-eqz v6, :cond_8
/* .line 1503 */
v10 = (( android.telephony.gsm.GsmCellLocation ) v6 ).getLac ( ); // invoke-virtual {v6}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I
/* int-to-long v10, v10 */
/* .line 1504 */
/* .local v10, "lac":J */
v12 = (( android.telephony.gsm.GsmCellLocation ) v6 ).getCid ( ); // invoke-virtual {v6}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I
/* .line 1505 */
/* .local v12, "cellId":I */
/* cmp-long v8, v10, v8 */
/* if-lez v8, :cond_8 */
/* .line 1506 */
v8 = (( android.net.Network ) p1 ).getNetId ( ); // invoke-virtual {p1}, Landroid/net/Network;->getNetId()I
java.lang.Long .toString ( v10,v11 );
/* invoke-direct {p0, v8, v9}, Lcom/xiaomi/NetworkBoost/StatusManager;->setResloverInterfaceIdAndNetid(ILjava/lang/String;)V */
/* .line 1507 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v10, v11 ); // invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v7 );
/* .line 1510 */
} // .end local v6 # "gsmCellLocation":Landroid/telephony/gsm/GsmCellLocation;
} // .end local v10 # "lac":J
} // .end local v12 # "cellId":I
} // :cond_7
/* instance-of v6, v5, Landroid/telephony/cdma/CdmaCellLocation; */
if ( v6 != null) { // if-eqz v6, :cond_8
/* .line 1511 */
/* move-object v6, v5 */
/* check-cast v6, Landroid/telephony/cdma/CdmaCellLocation; */
/* .line 1512 */
/* .local v6, "cdmaCellLocation":Landroid/telephony/cdma/CdmaCellLocation; */
if ( v6 != null) { // if-eqz v6, :cond_9
/* .line 1513 */
v10 = (( android.telephony.cdma.CdmaCellLocation ) v6 ).getNetworkId ( ); // invoke-virtual {v6}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I
/* int-to-long v10, v10 */
/* .line 1514 */
/* .restart local v10 # "lac":J */
v12 = (( android.telephony.cdma.CdmaCellLocation ) v6 ).getBaseStationId ( ); // invoke-virtual {v6}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I
/* .line 1515 */
/* .restart local v12 # "cellId":I */
/* cmp-long v8, v10, v8 */
/* if-lez v8, :cond_9 */
/* .line 1516 */
v8 = (( android.net.Network ) p1 ).getNetId ( ); // invoke-virtual {p1}, Landroid/net/Network;->getNetId()I
java.lang.Long .toString ( v10,v11 );
/* invoke-direct {p0, v8, v9}, Lcom/xiaomi/NetworkBoost/StatusManager;->setResloverInterfaceIdAndNetid(ILjava/lang/String;)V */
/* .line 1517 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v10, v11 ); // invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v7 );
/* .line 1510 */
} // .end local v6 # "cdmaCellLocation":Landroid/telephony/cdma/CdmaCellLocation;
} // .end local v10 # "lac":J
} // .end local v12 # "cellId":I
} // :cond_8
} // :goto_1
/* nop */
/* .line 1522 */
} // .end local v4 # "mTelephonyManager":Landroid/telephony/TelephonyManager;
} // .end local v5 # "location":Landroid/telephony/CellLocation;
} // :cond_9
} // :goto_2
return;
} // .end method
private void unregisterAudioModeChangedListener ( ) {
/* .locals 2 */
/* .line 391 */
try { // :try_start_0
v0 = this.mAudioManager;
v1 = this.mAudioModeListener;
(( android.media.AudioManager ) v0 ).removeOnModeChangedListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/media/AudioManager;->removeOnModeChangedListener(Landroid/media/AudioManager$OnModeChangedListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 394 */
/* .line 392 */
/* :catch_0 */
/* move-exception v0 */
/* .line 395 */
} // :goto_0
return;
} // .end method
private void unregisterDefaultNetworkCallback ( ) {
/* .locals 4 */
/* .line 1003 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
v1 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
/* if-nez v1, :cond_0 */
/* .line 1004 */
return;
/* .line 1007 */
} // :cond_0
try { // :try_start_0
v2 = this.mDefaultNetworkCallback;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1008 */
(( android.net.ConnectivityManager ) v1 ).unregisterNetworkCallback ( v2 ); // invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 1009 */
/* const-string/jumbo v1, "unregisterefaultNetworkCallback" */
android.util.Log .d ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1013 */
} // :cond_1
/* .line 1011 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1012 */
/* .local v1, "e":Ljava/lang/IllegalArgumentException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Unregister default network callback exception"; // const-string v3, "Unregister default network callback exception"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v2 );
/* .line 1014 */
} // .end local v1 # "e":Ljava/lang/IllegalArgumentException;
} // :goto_0
return;
} // .end method
private void unregisterForegroundInfoListener ( ) {
/* .locals 3 */
/* .line 290 */
try { // :try_start_0
/* new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;-><init>(Landroid/content/Context;)V */
this.mNetworkBoostProcessMonitor = v0;
/* .line 291 */
v1 = this.mForegroundInfoListener;
(( com.xiaomi.NetworkBoost.NetworkBoostProcessMonitor ) v0 ).unregisterForegroundInfoListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostProcessMonitor;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 294 */
/* .line 292 */
/* :catch_0 */
/* move-exception v0 */
/* .line 293 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkBoostStatusManager"; // const-string v2, "NetworkBoostStatusManager"
android.util.Log .e ( v2,v1 );
/* .line 295 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unregisterModemSignalStrengthObserver ( ) {
/* .locals 2 */
/* .line 1236 */
v0 = this.mHandler;
/* const/16 v1, 0x69 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1237 */
return;
} // .end method
private void unregisterMovementSensorListener ( ) {
/* .locals 9 */
/* .line 1569 */
/* iget-boolean v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorFlag:Z */
/* if-nez v0, :cond_0 */
/* .line 1570 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
final String v1 = "other unregisterMovementSensorListener succeed!"; // const-string v1, "other unregisterMovementSensorListener succeed!"
android.util.Log .e ( v0,v1 );
/* .line 1571 */
return;
/* .line 1574 */
} // :cond_0
v0 = this.mSensorManager;
/* if-nez v0, :cond_1 */
/* .line 1575 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
/* const-string/jumbo v1, "unregisterMovementSensorListener is fail!" */
android.util.Log .e ( v0,v1 );
/* .line 1576 */
return;
/* .line 1579 */
} // :cond_1
v1 = this.mSensorListener;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 1580 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSensorFlag:Z */
/* .line 1581 */
final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
/* const-string/jumbo v2, "unregisterMovementSensorListener" */
android.util.Log .i ( v1,v2 );
/* .line 1583 */
v1 = this.mSensorManager;
/* const v2, 0x1fa267e */
(( android.hardware.SensorManager ) v1 ).getDefaultSensor ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;
/* .line 1584 */
/* .local v1, "movementSensor":Landroid/hardware/Sensor; */
/* new-instance v2, Landroid/hardware/SensorEvent; */
int v5 = 0; // const/4 v5, 0x0
/* const-wide/16 v6, 0x0 */
int v3 = 1; // const/4 v3, 0x1
/* new-array v8, v3, [F */
int v3 = 0; // const/4 v3, 0x0
/* aput v3, v8, v0 */
/* move-object v3, v2 */
/* move-object v4, v1 */
/* invoke-direct/range {v3 ..v8}, Landroid/hardware/SensorEvent;-><init>(Landroid/hardware/Sensor;IJ[F)V */
/* move-object v0, v2 */
/* .line 1585 */
/* .local v0, "event":Landroid/hardware/SensorEvent; */
v2 = this.mMovementSensorStatusListenerList;
/* monitor-enter v2 */
/* .line 1586 */
try { // :try_start_0
v3 = this.mMovementSensorStatusListenerList;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener; */
/* .line 1587 */
/* .local v4, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener; */
/* .line 1588 */
} // .end local v4 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener;
/* .line 1589 */
} // :cond_2
/* monitor-exit v2 */
/* .line 1590 */
return;
/* .line 1589 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
} // .end method
private void unregisterNetworkCallback ( ) {
/* .locals 4 */
/* .line 702 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).quit ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z
/* .line 703 */
v1 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
/* if-nez v1, :cond_0 */
/* .line 704 */
return;
/* .line 707 */
} // :cond_0
try { // :try_start_0
v2 = this.mWifiNetworkCallback;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 708 */
(( android.net.ConnectivityManager ) v1 ).unregisterNetworkCallback ( v2 ); // invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 709 */
/* const-string/jumbo v1, "unregisterWifiNetworkCallback" */
android.util.Log .d ( v0,v1 );
/* .line 711 */
} // :cond_1
v1 = this.mCellularNetworkCallback;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 712 */
v2 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
(( android.net.ConnectivityManager ) v2 ).unregisterNetworkCallback ( v1 ); // invoke-virtual {v2, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 713 */
/* const-string/jumbo v1, "unregisterCellularNetworkCallback" */
android.util.Log .d ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 717 */
} // :cond_2
/* .line 715 */
/* :catch_0 */
/* move-exception v1 */
/* .line 716 */
/* .local v1, "e":Ljava/lang/IllegalArgumentException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Unregister network callback exception"; // const-string v3, "Unregister network callback exception"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v2 );
/* .line 718 */
} // .end local v1 # "e":Ljava/lang/IllegalArgumentException;
} // :goto_0
return;
} // .end method
private void unregisterScreenStatusReceiver ( ) {
/* .locals 2 */
/* .line 1169 */
v0 = this.mScreenStatusReceiver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1170 */
v1 = this.mContext;
(( android.content.Context ) v1 ).unregisterReceiver ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 1171 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
/* const-string/jumbo v1, "unregisterScreenStatusReceiver" */
android.util.Log .d ( v0,v1 );
/* .line 1173 */
} // :cond_0
return;
} // .end method
private void unregisterTelephonyStatusReceiver ( ) {
/* .locals 2 */
/* .line 1349 */
v0 = this.mTelephonyStatusReceiver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1350 */
v1 = this.mContext;
(( android.content.Context ) v1 ).unregisterReceiver ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 1351 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
/* const-string/jumbo v1, "unregisterTelephonyStatusReceiver" */
android.util.Log .d ( v0,v1 );
/* .line 1353 */
} // :cond_0
return;
} // .end method
private void unregisterUidObserver ( ) {
/* .locals 3 */
/* .line 331 */
try { // :try_start_0
v0 = this.mActivityManager;
v1 = this.mUidObserver;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 334 */
/* .line 332 */
/* :catch_0 */
/* move-exception v0 */
/* .line 333 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception:"; // const-string v2, "Exception:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkBoostStatusManager"; // const-string v2, "NetworkBoostStatusManager"
android.util.Log .e ( v2,v1 );
/* .line 335 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void dumpModule ( java.io.PrintWriter p0 ) {
/* .locals 6 */
/* .param p1, "writer" # Ljava/io/PrintWriter; */
/* .line 1639 */
final String v0 = "NetworkBoostStatusManager end.\n"; // const-string v0, "NetworkBoostStatusManager end.\n"
final String v1 = " "; // const-string v1, " "
/* .line 1641 */
/* .local v1, "spacePrefix":Ljava/lang/String; */
try { // :try_start_0
final String v2 = "NetworkBoostStatusManager begin:"; // const-string v2, "NetworkBoostStatusManager begin:"
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1642 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "WifiReady:"; // const-string v3, "WifiReady:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mWifiReady:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1643 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "SlaveWifiReady:"; // const-string v3, "SlaveWifiReady:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveWifiReady:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1644 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "DataReady:"; // const-string v3, "DataReady:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mDataReady:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1645 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "SlaveDataReady:"; // const-string v3, "SlaveDataReady:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mSlaveDataReady:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1646 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mInterface:"; // const-string v3, "mInterface:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mInterface;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1647 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mIfaceNumber:"; // const-string v3, "mIfaceNumber:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mIfaceNumber:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1648 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mPriorityMode:"; // const-string v3, "mPriorityMode:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPriorityMode:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1649 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mTrafficPolicy:"; // const-string v3, "mTrafficPolicy:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTrafficPolicy:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1650 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mThermalLevel:"; // const-string v3, "mThermalLevel:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mThermalLevel:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1651 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mAppStatusListenerList:"; // const-string v3, "mAppStatusListenerList:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = v3 = this.mAppStatusListenerList;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1652 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mNetworkInterfaceListenerList:"; // const-string v3, "mNetworkInterfaceListenerList:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = v3 = this.mNetworkInterfaceListenerList;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1653 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mScreenStatusListenerList:"; // const-string v3, "mScreenStatusListenerList:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = v3 = this.mScreenStatusListenerList;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1654 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "mNetworkPriorityListenerList:"; // const-string v3, "mNetworkPriorityListenerList:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = v3 = this.mNetworkPriorityListenerList;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1655 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1666 */
/* .line 1657 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1658 */
/* .local v2, "ex":Ljava/lang/Exception; */
final String v3 = "dump failed!"; // const-string v3, "dump failed!"
final String v4 = "NetworkBoostStatusManager"; // const-string v4, "NetworkBoostStatusManager"
android.util.Log .e ( v4,v3,v2 );
/* .line 1660 */
try { // :try_start_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Dump of NetworkBoostStatusManager failed:"; // const-string v5, "Dump of NetworkBoostStatusManager failed:"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1661 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 1665 */
/* .line 1663 */
/* :catch_1 */
/* move-exception v0 */
/* .line 1664 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "dump failure failed:"; // const-string v5, "dump failure failed:"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v4,v3 );
/* .line 1667 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end local v2 # "ex":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public java.util.List getAvailableIfaces ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 909 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 910 */
/* .local v0, "availableIfaces":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = this.mInterface;
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 911 */
/* .local v1, "interfaces":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, v1 */
/* if-ge v2, v3, :cond_0 */
/* .line 912 */
/* aget-object v3, v1, v2 */
/* .line 911 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 914 */
} // .end local v2 # "i":I
} // :cond_0
} // .end method
public Integer getForegroundUid ( ) {
/* .locals 2 */
/* .line 313 */
v0 = this.mAppStatusListenerList;
/* monitor-enter v0 */
/* .line 314 */
try { // :try_start_0
/* iget v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mFUid:I */
/* monitor-exit v0 */
/* .line 315 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public android.os.HandlerThread getNetworkBoostWatchdogHandler ( ) {
/* .locals 1 */
/* .line 275 */
v0 = this.mWacthdogHandlerThread;
} // .end method
public com.xiaomi.NetworkBoost.StatusManager$NetworkPriorityInfo getNetworkPriorityInfo ( ) {
/* .locals 5 */
/* .line 443 */
v0 = this.mNetworkPriorityListenerList;
/* monitor-enter v0 */
/* .line 444 */
try { // :try_start_0
/* new-instance v1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo; */
/* iget v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mPriorityMode:I */
/* iget v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mTrafficPolicy:I */
/* iget v4, p0, Lcom/xiaomi/NetworkBoost/StatusManager;->mThermalLevel:I */
/* invoke-direct {v1, v2, v3, v4}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;-><init>(III)V */
/* monitor-exit v0 */
/* .line 445 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isPoorSignal ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "curRsrp" # I */
/* .line 1297 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1298 */
/* .local v0, "rst":Z */
/* const v1, 0x7fffffff */
/* if-ne p1, v1, :cond_0 */
/* .line 1299 */
/* .line 1301 */
} // :cond_0
/* const/16 v1, -0x69 */
/* if-gt p1, v1, :cond_1 */
/* .line 1302 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1304 */
} // :cond_1
} // .end method
public void onCreate ( ) {
/* .locals 2 */
/* .line 242 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
final String v1 = "onCreate "; // const-string v1, "onCreate "
android.util.Log .i ( v0,v1 );
/* .line 244 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "NetoworkBoostWacthdogHandler"; // const-string v1, "NetoworkBoostWacthdogHandler"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mWacthdogHandlerThread = v0;
/* .line 245 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 247 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerNetworkCallback()V */
/* .line 248 */
(( com.xiaomi.NetworkBoost.StatusManager ) p0 ).registerVPNCallback ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerVPNCallback()V
/* .line 249 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerDefaultNetworkCallback()V */
/* .line 250 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerForegroundInfoListener()V */
/* .line 251 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerUidObserver()V */
/* .line 252 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerAudioModeChangedListener()V */
/* .line 253 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerScreenStatusReceiver()V */
/* .line 254 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerModemSignalStrengthObserver()V */
/* .line 255 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->registerTelephonyStatusReceiver()V */
/* .line 256 */
return;
} // .end method
public void onDestroy ( ) {
/* .locals 2 */
/* .line 259 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
final String v1 = "onDestroy "; // const-string v1, "onDestroy "
android.util.Log .i ( v0,v1 );
/* .line 261 */
v0 = this.mWacthdogHandlerThread;
(( android.os.HandlerThread ) v0 ).quit ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
/* .line 263 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterNetworkCallback()V */
/* .line 264 */
(( com.xiaomi.NetworkBoost.StatusManager ) p0 ).unregisterVPNCallback ( ); // invoke-virtual {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterVPNCallback()V
/* .line 265 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterDefaultNetworkCallback()V */
/* .line 266 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterForegroundInfoListener()V */
/* .line 267 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterUidObserver()V */
/* .line 268 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterAudioModeChangedListener()V */
/* .line 269 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterScreenStatusReceiver()V */
/* .line 270 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterModemSignalStrengthObserver()V */
/* .line 271 */
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/StatusManager;->unregisterTelephonyStatusReceiver()V */
/* .line 272 */
return;
} // .end method
public void onNetworkPriorityChanged ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "priorityMode" # I */
/* .param p2, "trafficPolicy" # I */
/* .param p3, "thermalLevel" # I */
/* .line 424 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onNetworkPriorityChanged: "; // const-string v1, "onNetworkPriorityChanged: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
android.util.Log .i ( v1,v0 );
/* .line 426 */
v0 = this.mHandler;
/* new-instance v1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo; */
/* invoke-direct {v1, p1, p2, p3}, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;-><init>(III)V */
/* const/16 v2, 0x6d */
(( android.os.Handler ) v0 ).obtainMessage ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 427 */
return;
} // .end method
public void registerAppStatusListener ( com.xiaomi.NetworkBoost.StatusManager$IAppStatusListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener; */
/* .line 399 */
v0 = this.mAppStatusListenerList;
/* monitor-enter v0 */
/* .line 400 */
try { // :try_start_0
v1 = this.mAppStatusListenerList;
/* .line 401 */
/* monitor-exit v0 */
/* .line 402 */
return;
/* .line 401 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void registerDefaultNetworkInterfaceListener ( com.xiaomi.NetworkBoost.StatusManager$IDefaultNetworkInterfaceListener p0 ) {
/* .locals 3 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener; */
/* .line 1043 */
v0 = this.mDefaultNetworkInterfaceListenerList;
/* monitor-enter v0 */
/* .line 1044 */
try { // :try_start_0
v1 = this.mDefaultNetworkInterfaceListenerList;
/* .line 1045 */
v1 = this.mHandler;
/* const/16 v2, 0x6f */
(( android.os.Handler ) v1 ).obtainMessage ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Handler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1046 */
/* monitor-exit v0 */
/* .line 1047 */
return;
/* .line 1046 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void registerModemSignalStrengthListener ( com.xiaomi.NetworkBoost.StatusManager$IModemSignalStrengthListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener; */
/* .line 1202 */
v0 = this.mModemSignalStrengthListener;
/* monitor-enter v0 */
/* .line 1203 */
try { // :try_start_0
v1 = this.mModemSignalStrengthListener;
/* .line 1204 */
/* monitor-exit v0 */
/* .line 1205 */
return;
/* .line 1204 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void registerMovementSensorStatusListener ( com.xiaomi.NetworkBoost.StatusManager$IMovementSensorStatusListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener; */
/* .line 1614 */
v0 = this.mMovementSensorStatusListenerList;
/* monitor-enter v0 */
/* .line 1615 */
try { // :try_start_0
v1 = this.mMovementSensorStatusListenerList;
/* .line 1616 */
v1 = this.mCurrentSensorevent;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1617 */
/* .line 1619 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1620 */
return;
/* .line 1619 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void registerNetworkInterfaceListener ( com.xiaomi.NetworkBoost.StatusManager$INetworkInterfaceListener p0 ) {
/* .locals 3 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener; */
/* .line 945 */
v0 = this.mNetworkInterfaceListenerList;
/* monitor-enter v0 */
/* .line 946 */
try { // :try_start_0
v1 = this.mNetworkInterfaceListenerList;
/* .line 947 */
v1 = this.mHandler;
/* const/16 v2, 0x6b */
(( android.os.Handler ) v1 ).obtainMessage ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Handler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 948 */
/* monitor-exit v0 */
/* .line 949 */
return;
/* .line 948 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void registerNetworkPriorityListener ( com.xiaomi.NetworkBoost.StatusManager$INetworkPriorityListener p0 ) {
/* .locals 3 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener; */
/* .line 430 */
v0 = this.mNetworkPriorityListenerList;
/* monitor-enter v0 */
/* .line 431 */
try { // :try_start_0
v1 = this.mNetworkPriorityListenerList;
/* .line 432 */
v1 = this.mHandler;
/* const/16 v2, 0x6e */
(( android.os.Handler ) v1 ).obtainMessage ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Handler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 433 */
/* monitor-exit v0 */
/* .line 434 */
return;
/* .line 433 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void registerScreenStatusListener ( com.xiaomi.NetworkBoost.StatusManager$IScreenStatusListener p0 ) {
/* .locals 3 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener; */
/* .line 1176 */
v0 = this.mScreenStatusListenerList;
/* monitor-enter v0 */
/* .line 1177 */
try { // :try_start_0
v1 = this.mScreenStatusListenerList;
/* .line 1178 */
v1 = this.mHandler;
/* const/16 v2, 0x6c */
(( android.os.Handler ) v1 ).obtainMessage ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Handler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1179 */
/* monitor-exit v0 */
/* .line 1180 */
return;
/* .line 1179 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void registerVPNCallback ( ) {
/* .locals 3 */
/* .line 1069 */
v0 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
/* if-nez v0, :cond_0 */
/* .line 1070 */
return;
/* .line 1072 */
} // :cond_0
/* new-instance v0, Landroid/net/NetworkRequest$Builder; */
/* invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V */
/* .line 1073 */
int v1 = 4; // const/4 v1, 0x4
(( android.net.NetworkRequest$Builder ) v0 ).addTransportType ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;
/* .line 1074 */
/* const/16 v1, 0xf */
(( android.net.NetworkRequest$Builder ) v0 ).removeCapability ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 1075 */
/* const/16 v1, 0xc */
(( android.net.NetworkRequest$Builder ) v0 ).removeCapability ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
/* .line 1076 */
(( android.net.NetworkRequest$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;
/* .line 1078 */
/* .local v0, "vpnRequest":Landroid/net/NetworkRequest; */
v1 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
v2 = this.mVPNCallback;
(( android.net.ConnectivityManager ) v1 ).registerNetworkCallback ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 1079 */
return;
} // .end method
public void registerVPNListener ( com.xiaomi.NetworkBoost.StatusManager$IVPNListener p0 ) {
/* .locals 3 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener; */
/* .line 1114 */
v0 = this.mVPNListener;
/* monitor-enter v0 */
/* .line 1115 */
try { // :try_start_0
v1 = this.mVPNListener;
/* .line 1116 */
v1 = this.mHandler;
/* const/16 v2, 0x70 */
(( android.os.Handler ) v1 ).obtainMessage ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Handler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1117 */
/* monitor-exit v0 */
/* .line 1118 */
return;
/* .line 1117 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void unregisterAppStatusListener ( com.xiaomi.NetworkBoost.StatusManager$IAppStatusListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener; */
/* .line 405 */
v0 = this.mAppStatusListenerList;
/* monitor-enter v0 */
/* .line 406 */
try { // :try_start_0
v1 = this.mAppStatusListenerList;
/* .line 407 */
/* monitor-exit v0 */
/* .line 408 */
return;
/* .line 407 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void unregisterDefaultNetworkInterfaceListener ( com.xiaomi.NetworkBoost.StatusManager$IDefaultNetworkInterfaceListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener; */
/* .line 1050 */
v0 = this.mDefaultNetworkInterfaceListenerList;
/* monitor-enter v0 */
/* .line 1051 */
try { // :try_start_0
v1 = this.mDefaultNetworkInterfaceListenerList;
/* .line 1052 */
/* monitor-exit v0 */
/* .line 1053 */
return;
/* .line 1052 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void unregisterModemSignalStrengthListener ( com.xiaomi.NetworkBoost.StatusManager$IModemSignalStrengthListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$IModemSignalStrengthListener; */
/* .line 1208 */
v0 = this.mModemSignalStrengthListener;
/* monitor-enter v0 */
/* .line 1209 */
try { // :try_start_0
v1 = this.mModemSignalStrengthListener;
/* .line 1210 */
/* monitor-exit v0 */
/* .line 1211 */
return;
/* .line 1210 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void unregisterMovementSensorStatusListener ( com.xiaomi.NetworkBoost.StatusManager$IMovementSensorStatusListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$IMovementSensorStatusListener; */
/* .line 1622 */
v0 = this.mMovementSensorStatusListenerList;
/* monitor-enter v0 */
/* .line 1623 */
try { // :try_start_0
v1 = this.mMovementSensorStatusListenerList;
/* .line 1624 */
/* monitor-exit v0 */
/* .line 1625 */
return;
/* .line 1624 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void unregisterNetworkInterfaceListener ( com.xiaomi.NetworkBoost.StatusManager$INetworkInterfaceListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener; */
/* .line 952 */
v0 = this.mNetworkInterfaceListenerList;
/* monitor-enter v0 */
/* .line 953 */
try { // :try_start_0
v1 = this.mNetworkInterfaceListenerList;
/* .line 954 */
/* monitor-exit v0 */
/* .line 955 */
return;
/* .line 954 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void unregisterNetworkPriorityListener ( com.xiaomi.NetworkBoost.StatusManager$INetworkPriorityListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener; */
/* .line 437 */
v0 = this.mNetworkPriorityListenerList;
/* monitor-enter v0 */
/* .line 438 */
try { // :try_start_0
v1 = this.mNetworkPriorityListenerList;
/* .line 439 */
/* monitor-exit v0 */
/* .line 440 */
return;
/* .line 439 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void unregisterScreenStatusListener ( com.xiaomi.NetworkBoost.StatusManager$IScreenStatusListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener; */
/* .line 1183 */
v0 = this.mScreenStatusListenerList;
/* monitor-enter v0 */
/* .line 1184 */
try { // :try_start_0
v1 = this.mScreenStatusListenerList;
/* .line 1185 */
/* monitor-exit v0 */
/* .line 1186 */
return;
/* .line 1185 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void unregisterVPNCallback ( ) {
/* .locals 2 */
/* .line 1082 */
v0 = com.xiaomi.NetworkBoost.StatusManager.mConnectivityManager;
/* if-nez v0, :cond_0 */
/* .line 1083 */
return;
/* .line 1085 */
} // :cond_0
v1 = this.mVPNCallback;
(( android.net.ConnectivityManager ) v0 ).unregisterNetworkCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 1086 */
return;
} // .end method
public void unregisterVPNListener ( com.xiaomi.NetworkBoost.StatusManager$IVPNListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener; */
/* .line 1121 */
v0 = this.mVPNListener;
/* monitor-enter v0 */
/* .line 1122 */
try { // :try_start_0
v1 = this.mVPNListener;
/* .line 1123 */
/* monitor-exit v0 */
/* .line 1124 */
return;
/* .line 1123 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
