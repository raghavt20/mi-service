.class Lcom/xiaomi/NetworkBoost/StatusManager$7;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "StatusManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/StatusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/StatusManager;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 1089
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$7;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAvailable(Landroid/net/Network;)V
    .locals 4
    .param p1, "network"    # Landroid/net/Network;

    .line 1092
    const-string v0, "NetworkBoostStatusManager"

    const-string v1, "VPN Callback onAvailable"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1093
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$7;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmVPNListener(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v0

    monitor-enter v0

    .line 1094
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$7;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fputmVPNstatus(Lcom/xiaomi/NetworkBoost/StatusManager;Z)V

    .line 1095
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$7;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmVPNListener(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;

    .line 1096
    .local v2, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager$7;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmVPNstatus(Lcom/xiaomi/NetworkBoost/StatusManager;)Z

    move-result v3

    invoke-interface {v2, v3}, Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;->onVPNStatusChange(Z)V

    .line 1097
    .end local v2    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;
    goto :goto_0

    .line 1098
    :cond_0
    monitor-exit v0

    .line 1099
    return-void

    .line 1098
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 4
    .param p1, "network"    # Landroid/net/Network;

    .line 1103
    const-string v0, "NetworkBoostStatusManager"

    const-string v1, "VPN Callback onLost"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1104
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$7;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmVPNListener(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v0

    monitor-enter v0

    .line 1105
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$7;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fputmVPNstatus(Lcom/xiaomi/NetworkBoost/StatusManager;Z)V

    .line 1106
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$7;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmVPNListener(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;

    .line 1107
    .local v2, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;
    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager$7;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmVPNstatus(Lcom/xiaomi/NetworkBoost/StatusManager;)Z

    move-result v3

    invoke-interface {v2, v3}, Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;->onVPNStatusChange(Z)V

    .line 1108
    .end local v2    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;
    goto :goto_0

    .line 1109
    :cond_0
    monitor-exit v0

    .line 1110
    return-void

    .line 1109
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
