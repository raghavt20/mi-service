.class Lcom/xiaomi/NetworkBoost/StatusManager$1;
.super Lmiui/process/IForegroundInfoListener$Stub;
.source "StatusManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/StatusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/StatusManager;


# direct methods
.method constructor <init>(Lcom/xiaomi/NetworkBoost/StatusManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 297
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$1;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 3
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 300
    const-string v0, "NetworkBoostStatusManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onForegroundInfoChanged uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isColdStart:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 301
    invoke-virtual {p1}, Lmiui/process/ForegroundInfo;->isColdStart()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 300
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$1;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmAppStatusListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v0

    monitor-enter v0

    .line 303
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$1;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    iget v2, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-static {v1, v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fputmFUid(Lcom/xiaomi/NetworkBoost/StatusManager;I)V

    .line 304
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$1;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmAppStatusListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;

    .line 305
    .local v2, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;
    invoke-interface {v2, p1}, Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;->onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V

    .line 306
    .end local v2    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;
    goto :goto_0

    .line 307
    :cond_0
    monitor-exit v0

    .line 308
    return-void

    .line 307
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
