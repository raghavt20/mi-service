.class Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;
.super Ljava/lang/Object;
.source "StatusManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/StatusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NetworkCallbackInterface"
.end annotation


# instance fields
.field private interfaceName:Ljava/lang/String;

.field private status:I

.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/StatusManager;


# direct methods
.method public constructor <init>(Lcom/xiaomi/NetworkBoost/StatusManager;Ljava/lang/String;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/StatusManager;
    .param p2, "interfaceName"    # Ljava/lang/String;
    .param p3, "status"    # I

    .line 493
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 494
    iput-object p2, p0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->interfaceName:Ljava/lang/String;

    .line 495
    iput p3, p0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->status:I

    .line 496
    return-void
.end method


# virtual methods
.method public getInterfaceName()Ljava/lang/String;
    .locals 1

    .line 499
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->interfaceName:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()I
    .locals 1

    .line 503
    iget v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->status:I

    return v0
.end method

.method public setStatus(I)V
    .locals 0
    .param p1, "status"    # I

    .line 507
    iput p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->status:I

    .line 508
    return-void
.end method
