.class public Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;
.super Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub;
.source "NetworkBoostService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/NetworkBoostService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "NetworkBoostServiceManager"
.end annotation


# static fields
.field public static final SERVICE_NAME:Ljava/lang/String; = "xiaomi.NetworkBoostServiceManager"


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;


# direct methods
.method public constructor <init>(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    .line 87
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public abortScan()Z
    .locals 2

    .line 183
    const/4 v0, 0x0

    .line 184
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->abortScan()Z

    move-result v0

    .line 187
    :cond_0
    return v0
.end method

.method public activeScan([I)Z
    .locals 2
    .param p1, "channelList"    # [I

    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "activeScan:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NetworkBoostService"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    const/4 v0, 0x0

    .line 175
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->activeScan([I)Z

    move-result v0

    .line 178
    :cond_0
    return v0
.end method

.method public connectSlaveWifi(I)Z
    .locals 1
    .param p1, "networkId"    # I

    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public disableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;)Z
    .locals 4
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;

    .line 443
    const/4 v0, 0x0

    .line 444
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 446
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->disableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 450
    goto :goto_0

    .line 447
    :catch_0
    move-exception v1

    .line 448
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " disableWifiSelectionOpt:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NetworkBoostService"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 452
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return v0
.end method

.method public disableWifiSelectionOptByUid(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;I)Z
    .locals 4
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;
    .param p2, "uid"    # I

    .line 386
    const/4 v0, 0x0

    .line 387
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$misSystemProcess(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 388
    return v0

    .line 390
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 392
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->disableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;I)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 396
    goto :goto_0

    .line 393
    :catch_0
    move-exception v1

    .line 394
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " disableWifiSelectionOpt:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NetworkBoostService"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 398
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    return v0
.end method

.method public disconnectSlaveWifi()Z
    .locals 1

    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "writer"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 524
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$sfgetmContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.DUMP"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 526
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: can\'t dump xiaomi.NetworkBoostServiceManager from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 527
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " due to missing android.permission.DUMP permission"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 526
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 529
    return-void

    .line 531
    :cond_0
    invoke-static {p1, p2, p3}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->dumpModule(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 532
    return-void
.end method

.method public enableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;I)Z
    .locals 4
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;
    .param p2, "type"    # I

    .line 403
    const/4 v0, 0x0

    .line 405
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 407
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->enableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;I)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 411
    goto :goto_0

    .line 408
    :catch_0
    move-exception v1

    .line 409
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " enableWifiSelectionOpt:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NetworkBoostService"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 414
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return v0
.end method

.method public enableWifiSelectionOptByUid(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;II)Z
    .locals 4
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;
    .param p2, "type"    # I
    .param p3, "uid"    # I

    .line 368
    const/4 v0, 0x0

    .line 369
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$misSystemProcess(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 370
    return v0

    .line 372
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 374
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->enableWifiSelectionOpt(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetSelectCallback;II)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 378
    goto :goto_0

    .line 375
    :catch_0
    move-exception v1

    .line 376
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " enableWifiSelectionOpt:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NetworkBoostService"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 381
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    return v0
.end method

.method public getAvailableIfaces()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 238
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 240
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->getAvailableIfaces()Ljava/util/Map;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 241
    :catch_0
    move-exception v0

    .line 242
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " getAvailableIfaces:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NetworkBoostService"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 247
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method public getQoEByAvailableIfaceName(Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .param p1, "ifaceName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 159
    const/4 v0, 0x0

    return-object v0
.end method

.method public getQoEByAvailableIfaceNameV1(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;
    .locals 3
    .param p1, "ifaceName"    # Ljava/lang/String;

    .line 275
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 277
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->getQoEByAvailableIfaceNameV1(Ljava/lang/String;)Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 278
    :catch_0
    move-exception v0

    .line 279
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " getQoEByAvailableIfaceNameV1:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NetworkBoostService"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 284
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;

    invoke-direct {v0}, Lcom/xiaomi/NetworkBoost/NetLinkLayerQoE;-><init>()V

    return-object v0
.end method

.method public getServiceVersion()I
    .locals 2

    .line 266
    const-string v0, "NetworkBoostService"

    const-string v1, "Service Version:6"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 268
    const/4 v0, 0x6

    return v0

    .line 270
    :cond_0
    const/16 v0, 0x64

    return v0
.end method

.method public isCelluarDSDAState()Z
    .locals 2

    .line 475
    const/4 v0, 0x0

    .line 476
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 477
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isCelluarDSDAState()Z

    move-result v0

    .line 479
    :cond_0
    return v0
.end method

.method public isSlaveWifiEnabledAndOthersOpt(I)I
    .locals 4
    .param p1, "type"    # I

    .line 493
    const/4 v0, -0x1

    .line 494
    .local v0, "ret":I
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 496
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSlaveWifiEnabledAndOthersOpt(I)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 500
    goto :goto_0

    .line 497
    :catch_0
    move-exception v1

    .line 498
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isSlaveWifiEnabledAndOthersOpt:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NetworkBoostService"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 502
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return v0
.end method

.method public isSlaveWifiEnabledAndOthersOptByUid(II)I
    .locals 4
    .param p1, "type"    # I
    .param p2, "uid"    # I

    .line 507
    const/4 v0, -0x1

    .line 508
    .local v0, "ret":I
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$misSystemProcess(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 509
    const/4 v1, -0x1

    return v1

    .line 511
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 513
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSlaveWifiEnabledAndOthersOpt(II)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 517
    goto :goto_0

    .line 514
    :catch_0
    move-exception v1

    .line 515
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isSlaveWifiEnabledAndOthersOptByUid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NetworkBoostService"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 519
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    return v0
.end method

.method public isSupportDualCelluarData()Z
    .locals 2

    .line 457
    const/4 v0, 0x0

    .line 458
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 459
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSupportDualCelluarData()Z

    move-result v0

    .line 461
    :cond_0
    return v0
.end method

.method public isSupportDualWifi()Z
    .locals 2

    .line 113
    const/4 v0, 0x0

    .line 114
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 115
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSupportDualWifi()Z

    move-result v0

    .line 117
    :cond_0
    return v0
.end method

.method public isSupportMediaPlayerPolicy()Z
    .locals 2

    .line 466
    const/4 v0, 0x0

    .line 467
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 468
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->isSupportMediaPlayerPolicy()Z

    move-result v0

    .line 470
    :cond_0
    return v0
.end method

.method public registerCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
    .locals 2
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    .line 141
    const/4 v0, 0x0

    .line 142
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 143
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registerCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z

    move-result v0

    .line 145
    :cond_0
    return v0
.end method

.method public registerNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;I)Z
    .locals 4
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    .param p2, "interval"    # I

    .line 289
    const/4 v0, 0x0

    .line 290
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 292
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registerNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;I)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 296
    goto :goto_0

    .line 293
    :catch_0
    move-exception v1

    .line 294
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " registerNetLinkCallback:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NetworkBoostService"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 298
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return v0
.end method

.method public registerNetLinkCallbackByUid(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;II)Z
    .locals 4
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    .param p2, "interval"    # I
    .param p3, "uid"    # I

    .line 317
    const/4 v0, 0x0

    .line 318
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$misSystemProcess(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 319
    return v0

    .line 321
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 323
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->registerNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;II)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 327
    goto :goto_0

    .line 324
    :catch_0
    move-exception v1

    .line 325
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " registerNetLinkCallback:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NetworkBoostService"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 329
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    return v0
.end method

.method public registerWifiLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback;)Z
    .locals 1
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback;

    .line 192
    const/4 v0, 0x0

    return v0
.end method

.method public reportBssidScore(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 431
    .local p1, "bssidScores":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 433
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->reportBssidScore(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 437
    goto :goto_0

    .line 434
    :catch_0
    move-exception v0

    .line 435
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " reportBssidScore:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NetworkBoostService"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 439
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method

.method public requestAppTrafficStatistics(IJJ)Ljava/util/Map;
    .locals 7
    .param p1, "type"    # I
    .param p2, "startTime"    # J
    .param p4, "endTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJJ)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 252
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 254
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    move v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-virtual/range {v1 .. v6}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->requestAppTrafficStatistics(IJJ)Ljava/util/Map;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 255
    :catch_0
    move-exception v0

    .line 256
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " requestAppTrafficStatistics:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NetworkBoostService"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 261
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method public requestAppTrafficStatisticsByUid(IJJI)Ljava/util/Map;
    .locals 8
    .param p1, "type"    # I
    .param p2, "startTime"    # J
    .param p4, "endTime"    # J
    .param p6, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJJI)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 351
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$misSystemProcess(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 352
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 356
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    move v2, p1

    move-wide v3, p2

    move-wide v5, p4

    move v7, p6

    invoke-virtual/range {v1 .. v7}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->requestAppTrafficStatistics(IJJI)Ljava/util/Map;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 357
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " requestAppTrafficStatistics:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NetworkBoostService"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 363
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method public resumeBackgroundScan()Z
    .locals 2

    .line 211
    const/4 v0, 0x0

    .line 212
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->resumeBackgroundScan()Z

    move-result v0

    .line 215
    :cond_0
    return v0
.end method

.method public resumeWifiPowerSave()Z
    .locals 2

    .line 229
    const/4 v0, 0x0

    .line 230
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 231
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->resumeWifiPowerSave()Z

    move-result v0

    .line 233
    :cond_0
    return v0
.end method

.method public setDualCelluarDataEnable(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .line 484
    const/4 v0, 0x0

    .line 485
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 486
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setDualCelluarDataEnable(Z)Z

    move-result v0

    .line 488
    :cond_0
    return v0
.end method

.method public setSlaveWifiEnable(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .line 122
    const/4 v0, 0x0

    .line 123
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 124
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setSlaveWifiEnabled(Z)Z

    move-result v0

    .line 126
    :cond_0
    return v0
.end method

.method public setSockPrio(II)Z
    .locals 2
    .param p1, "fd"    # I
    .param p2, "prio"    # I

    .line 95
    const/4 v0, 0x0

    .line 96
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setSockPrio(II)Z

    move-result v0

    .line 99
    :cond_0
    return v0
.end method

.method public setTCPCongestion(ILjava/lang/String;)Z
    .locals 2
    .param p1, "fd"    # I
    .param p2, "cc"    # Ljava/lang/String;

    .line 104
    const/4 v0, 0x0

    .line 105
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 106
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setTCPCongestion(ILjava/lang/String;)Z

    move-result v0

    .line 108
    :cond_0
    return v0
.end method

.method public setTrafficTransInterface(ILjava/lang/String;)Z
    .locals 2
    .param p1, "fd"    # I
    .param p2, "bindInterface"    # Ljava/lang/String;

    .line 164
    const/4 v0, 0x0

    .line 165
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 166
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->setTrafficTransInterface(ILjava/lang/String;)Z

    move-result v0

    .line 168
    :cond_0
    return v0
.end method

.method public suspendBackgroundScan()Z
    .locals 2

    .line 197
    const/4 v0, 0x0

    .line 198
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 199
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->suspendBackgroundScan()Z

    move-result v0

    .line 201
    :cond_0
    return v0
.end method

.method public suspendWifiPowerSave()Z
    .locals 2

    .line 220
    const/4 v0, 0x0

    .line 221
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 222
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->suspendWifiPowerSave()Z

    move-result v0

    .line 224
    :cond_0
    return v0
.end method

.method public triggerWifiSelection()V
    .locals 3

    .line 419
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 421
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->triggerWifiSelection()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 425
    goto :goto_0

    .line 422
    :catch_0
    move-exception v0

    .line 423
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " triggerWifiSelection:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NetworkBoostService"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 427
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method

.method public unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
    .locals 2
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    .line 150
    const/4 v0, 0x0

    .line 151
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z

    move-result v0

    .line 154
    :cond_0
    return v0
.end method

.method public unregisterNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;)Z
    .locals 4
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;

    .line 303
    const/4 v0, 0x0

    .line 304
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 306
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 310
    goto :goto_0

    .line 307
    :catch_0
    move-exception v1

    .line 308
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " unregisterNetLinkCallback:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NetworkBoostService"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 312
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return v0
.end method

.method public unregisterNetLinkCallbackByUid(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;I)Z
    .locals 4
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;
    .param p2, "uid"    # I

    .line 334
    const/4 v0, 0x0

    .line 335
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$misSystemProcess(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 336
    return v0

    .line 338
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 340
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;->this$0:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->-$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->unregisterNetLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetQoECallback;I)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 344
    goto :goto_0

    .line 341
    :catch_0
    move-exception v1

    .line 342
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " unregisterNetLinkCallback:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NetworkBoostService"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 346
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    return v0
.end method

.method public unregisterWifiLinkCallback(Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback;)Z
    .locals 1
    .param p1, "cb"    # Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback;

    .line 206
    const/4 v0, 0x0

    return v0
.end method
