class com.xiaomi.NetworkBoost.StatusManager$NetworkCallbackInterface {
	 /* .source "StatusManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/StatusManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "NetworkCallbackInterface" */
} // .end annotation
/* # instance fields */
private java.lang.String interfaceName;
private Integer status;
final com.xiaomi.NetworkBoost.StatusManager this$0; //synthetic
/* # direct methods */
public com.xiaomi.NetworkBoost.StatusManager$NetworkCallbackInterface ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/StatusManager; */
/* .param p2, "interfaceName" # Ljava/lang/String; */
/* .param p3, "status" # I */
/* .line 493 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 494 */
this.interfaceName = p2;
/* .line 495 */
/* iput p3, p0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->status:I */
/* .line 496 */
return;
} // .end method
/* # virtual methods */
public java.lang.String getInterfaceName ( ) {
/* .locals 1 */
/* .line 499 */
v0 = this.interfaceName;
} // .end method
public Integer getStatus ( ) {
/* .locals 1 */
/* .line 503 */
/* iget v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->status:I */
} // .end method
public void setStatus ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "status" # I */
/* .line 507 */
/* iput p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkCallbackInterface;->status:I */
/* .line 508 */
return;
} // .end method
