.class Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;
.super Landroid/os/Handler;
.source "StatusManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/StatusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/NetworkBoost/StatusManager;


# direct methods
.method public constructor <init>(Lcom/xiaomi/NetworkBoost/StatusManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 516
    iput-object p1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 517
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 518
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .line 521
    const-string v0, "NetworkBoostStatusManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handle: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_2

    .line 595
    :pswitch_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmVPNListener(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v0

    monitor-enter v0

    .line 596
    :try_start_0
    const-string v1, "NetworkBoostStatusManager"

    const-string v2, "CALLBACK_VPN_STATUS enter"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;

    .line 598
    .local v1, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmVPNstatus(Lcom/xiaomi/NetworkBoost/StatusManager;)Z

    move-result v2

    invoke-interface {v1, v2}, Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;->onVPNStatusChange(Z)V

    .line 599
    .end local v1    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;
    monitor-exit v0

    .line 600
    goto/16 :goto_2

    .line 599
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 588
    :pswitch_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmDefaultNetworkInterfaceListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v0

    monitor-enter v0

    .line 589
    :try_start_1
    const-string v1, "NetworkBoostStatusManager"

    const-string v2, "CALLBACK_NETWORK_PRIORITY_ADD enter"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;

    .line 591
    .local v1, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmDefaultIface(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmDefaultIfaceStatus(Lcom/xiaomi/NetworkBoost/StatusManager;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;->onDefaultNetwrokChange(Ljava/lang/String;I)V

    .line 592
    .end local v1    # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;
    monitor-exit v0

    .line 593
    goto/16 :goto_2

    .line 592
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v1

    .line 563
    :pswitch_2
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmNetworkPriorityListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v0

    monitor-enter v0

    .line 564
    :try_start_2
    const-string v1, "NetworkBoostStatusManager"

    const-string v2, "CALLBACK_NETWORK_PRIORITY_ADD enter"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;

    .line 566
    .local v1, "networkListener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmPriorityMode(Lcom/xiaomi/NetworkBoost/StatusManager;)I

    move-result v2

    iget-object v3, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v3}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmTrafficPolicy(Lcom/xiaomi/NetworkBoost/StatusManager;)I

    move-result v3

    iget-object v4, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v4}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmThermalLevel(Lcom/xiaomi/NetworkBoost/StatusManager;)I

    move-result v4

    invoke-interface {v1, v2, v3, v4}, Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;->onNetworkPriorityChanged(III)V

    .line 567
    .end local v1    # "networkListener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;
    monitor-exit v0

    .line 568
    goto/16 :goto_2

    .line 567
    :catchall_2
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v1

    .line 560
    :pswitch_3
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$monNetworkPriorityChanged(Lcom/xiaomi/NetworkBoost/StatusManager;Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo;)V

    .line 561
    goto/16 :goto_2

    .line 546
    :pswitch_4
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmScreenStatusListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v0

    monitor-enter v0

    .line 547
    :try_start_3
    const-string v1, "NetworkBoostStatusManager"

    const-string v2, "CALLBACK_SCREEN_STATUS enter"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;

    .line 549
    .local v1, "screenListener":Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmPowerManager(Lcom/xiaomi/NetworkBoost/StatusManager;)Landroid/os/PowerManager;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 550
    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmPowerManager(Lcom/xiaomi/NetworkBoost/StatusManager;)Landroid/os/PowerManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    .line 551
    .local v2, "isScreenOn":Z
    if-eqz v2, :cond_0

    .line 552
    invoke-interface {v1}, Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;->onScreenON()V

    goto :goto_0

    .line 554
    :cond_0
    invoke-interface {v1}, Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;->onScreenOFF()V

    .line 557
    .end local v1    # "screenListener":Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;
    .end local v2    # "isScreenOn":Z
    :cond_1
    :goto_0
    monitor-exit v0

    .line 558
    goto/16 :goto_2

    .line 557
    :catchall_3
    move-exception v1

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    throw v1

    .line 538
    :pswitch_5
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmNetworkInterfaceListenerList(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/util/List;

    move-result-object v0

    monitor-enter v0

    .line 539
    :try_start_4
    const-string v1, "NetworkBoostStatusManager"

    const-string v2, "CALLBACK_NETWORK_INTERFACE enter"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v2, v1

    check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;

    .line 541
    .local v2, "networkListener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmInterface(Lcom/xiaomi/NetworkBoost/StatusManager;)Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmIfaceNumber(Lcom/xiaomi/NetworkBoost/StatusManager;)I

    move-result v4

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmWifiReady(Lcom/xiaomi/NetworkBoost/StatusManager;)Z

    move-result v5

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmSlaveWifiReady(Lcom/xiaomi/NetworkBoost/StatusManager;)Z

    move-result v6

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmDataReady(Lcom/xiaomi/NetworkBoost/StatusManager;)Z

    move-result v7

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmSlaveDataReady(Lcom/xiaomi/NetworkBoost/StatusManager;)Z

    move-result v8

    const-string v9, "ON_AVAILABLE"

    invoke-interface/range {v2 .. v9}, Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;->onNetwrokInterfaceChange(Ljava/lang/String;IZZZZLjava/lang/String;)V

    .line 543
    .end local v2    # "networkListener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;
    monitor-exit v0

    .line 544
    goto/16 :goto_2

    .line 543
    :catchall_4
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    throw v1

    .line 602
    :pswitch_6
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmPowerManager(Lcom/xiaomi/NetworkBoost/StatusManager;)Landroid/os/PowerManager;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 603
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmPowerManager(Lcom/xiaomi/NetworkBoost/StatusManager;)Landroid/os/PowerManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    .line 604
    .local v0, "isScreenOn":Z
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmIfaceNumber(Lcom/xiaomi/NetworkBoost/StatusManager;)I

    move-result v1

    if-lez v1, :cond_2

    .line 605
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$mregisterMovementSensorListener(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    goto :goto_1

    .line 607
    :cond_2
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$munregisterMovementSensorListener(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    .line 609
    .end local v0    # "isScreenOn":Z
    :goto_1
    goto :goto_2

    .line 578
    :pswitch_7
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmPowerManager(Lcom/xiaomi/NetworkBoost/StatusManager;)Landroid/os/PowerManager;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 579
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmPowerManager(Lcom/xiaomi/NetworkBoost/StatusManager;)Landroid/os/PowerManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    .line 580
    .restart local v0    # "isScreenOn":Z
    if-eqz v0, :cond_3

    .line 581
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$misModemSingnalStrengthStatus(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    .line 584
    .end local v0    # "isScreenOn":Z
    :cond_3
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/StatusManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fgetmHandler(Lcom/xiaomi/NetworkBoost/StatusManager;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x69

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 586
    goto :goto_2

    .line 575
    :pswitch_8
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/content/Intent;

    invoke-static {v0, v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$mprocessSimStateChanged(Lcom/xiaomi/NetworkBoost/StatusManager;Landroid/content/Intent;)V

    .line 576
    goto :goto_2

    .line 571
    :pswitch_9
    const-string v0, "NetworkBoostStatusManager"

    const-string v1, "receive EVENT_DELAY_SET_CELLULAR_CALLBACK"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$mprocessRegisterCellularCallback(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    .line 573
    goto :goto_2

    .line 532
    :pswitch_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/net/Network;

    .line 533
    .local v0, "defaultNetwork":Landroid/net/Network;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-virtual {v0}, Landroid/net/Network;->getNetId()I

    move-result v2

    invoke-static {v1, v2}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$fputmDefaultNetid(Lcom/xiaomi/NetworkBoost/StatusManager;I)V

    .line 534
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$mcallbackDefaultNetworkInterface(Lcom/xiaomi/NetworkBoost/StatusManager;)V

    .line 535
    goto :goto_2

    .line 528
    .end local v0    # "defaultNetwork":Landroid/net/Network;
    :pswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/net/Network;

    .line 529
    .local v0, "onLostNetwork":Landroid/net/Network;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1, v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$minterfaceOnLost(Lcom/xiaomi/NetworkBoost/StatusManager;Landroid/net/Network;)V

    .line 530
    goto :goto_2

    .line 524
    .end local v0    # "onLostNetwork":Landroid/net/Network;
    :pswitch_c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/net/Network;

    .line 525
    .local v0, "onAvailableNetwork":Landroid/net/Network;
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/StatusManager$InternalHandler;->this$0:Lcom/xiaomi/NetworkBoost/StatusManager;

    invoke-static {v1, v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->-$$Nest$minterfaceOnAvailable(Lcom/xiaomi/NetworkBoost/StatusManager;Landroid/net/Network;)V

    .line 526
    nop

    .line 614
    .end local v0    # "onAvailableNetwork":Landroid/net/Network;
    :cond_4
    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
