class com.xiaomi.NetworkBoost.StatusManager$9 extends android.content.BroadcastReceiver {
	 /* .source "StatusManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/xiaomi/NetworkBoost/StatusManager;->registerTelephonyStatusReceiver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.StatusManager this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.StatusManager$9 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/StatusManager; */
/* .line 1332 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 1335 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 1336 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.intent.action.SIM_STATE_CHANGED"; // const-string v1, "android.intent.action.SIM_STATE_CHANGED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 1337 */
	 final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
	 final String v2 = "receive sim state change"; // const-string v2, "receive sim state change"
	 android.util.Log .i ( v1,v2 );
	 /* .line 1338 */
	 v1 = this.this$0;
	 com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmHandler ( v1 );
	 /* const/16 v2, 0x68 */
	 (( android.os.Handler ) v1 ).obtainMessage ( v2, p2 ); // invoke-virtual {v1, v2, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
	 (( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
	 /* .line 1340 */
} // :cond_0
return;
} // .end method
