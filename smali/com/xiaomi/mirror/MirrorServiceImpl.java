public class com.xiaomi.mirror.MirrorServiceImpl extends com.xiaomi.mirror.MirrorServiceStub {
	 /* .source "MirrorServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.xiaomi.mirror.MirrorServiceStub$$" */
} // .end annotation
/* # direct methods */
public com.xiaomi.mirror.MirrorServiceImpl ( ) {
	 /* .locals 0 */
	 /* .line 12 */
	 /* invoke-direct {p0}, Lcom/xiaomi/mirror/MirrorServiceStub;-><init>()V */
	 return;
} // .end method
/* # virtual methods */
public Boolean dragDropActiveLocked ( ) {
	 /* .locals 1 */
	 /* .line 35 */
	 com.xiaomi.mirror.service.MirrorService .get ( );
	 v0 = 	 (( com.xiaomi.mirror.service.MirrorService ) v0 ).getSystemReady ( ); // invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getSystemReady()Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 36 */
		 com.xiaomi.mirror.service.MirrorService .get ( );
		 (( com.xiaomi.mirror.service.MirrorService ) v0 ).getDragDropController ( ); // invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getDragDropController()Lcom/android/server/wm/MiuiMirrorDragDropController;
		 v0 = 		 (( com.android.server.wm.MiuiMirrorDragDropController ) v0 ).dragDropActiveLocked ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMirrorDragDropController;->dragDropActiveLocked()Z
		 /* .line 38 */
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean dragDropActiveLockedWithMirrorEnabled ( ) {
	 /* .locals 1 */
	 /* .line 60 */
	 v0 = 	 (( com.xiaomi.mirror.MirrorServiceImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/xiaomi/mirror/MirrorServiceImpl;->isEnabled()Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 61 */
		 v0 = 		 (( com.xiaomi.mirror.MirrorServiceImpl ) p0 ).dragDropActiveLocked ( ); // invoke-virtual {p0}, Lcom/xiaomi/mirror/MirrorServiceImpl;->dragDropActiveLocked()Z
		 /* .line 63 */
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean getAllowGrant ( ) {
	 /* .locals 1 */
	 /* .line 50 */
	 com.xiaomi.mirror.service.MirrorService .get ( );
	 v0 = 	 (( com.xiaomi.mirror.service.MirrorService ) v0 ).getAllowGrant ( ); // invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getAllowGrant()Z
} // .end method
public Boolean isEnabled ( ) {
	 /* .locals 3 */
	 /* .line 15 */
	 com.xiaomi.mirror.service.MirrorService .get ( );
	 /* .line 16 */
	 /* .local v0, "mirrorService":Lcom/xiaomi/mirror/service/MirrorService; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* if-nez v0, :cond_0 */
	 /* .line 17 */
	 /* .line 19 */
} // :cond_0
v2 = (( com.xiaomi.mirror.service.MirrorService ) v0 ).getDelegatePid ( ); // invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getDelegatePid()I
if ( v2 != null) { // if-eqz v2, :cond_1
	 int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
public Boolean isGrantAllowed ( android.net.Uri p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "uri" # Landroid/net/Uri; */
/* .param p2, "targetPkg" # Ljava/lang/String; */
/* .line 24 */
final String v0 = "com.xiaomi.mirror.remoteprovider"; // const-string v0, "com.xiaomi.mirror.remoteprovider"
(( android.net.Uri ) p1 ).getAuthority ( ); // invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 25 */
com.xiaomi.mirror.service.MirrorService .get ( );
(( com.xiaomi.mirror.service.MirrorService ) v0 ).getDelegatePackageName ( ); // invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getDelegatePackageName()Ljava/lang/String;
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 24 */
} // :goto_1
} // .end method
public void notifyPointerIconChanged ( Integer p0, android.view.PointerIcon p1 ) {
/* .locals 1 */
/* .param p1, "iconId" # I */
/* .param p2, "customIcon" # Landroid/view/PointerIcon; */
/* .line 55 */
com.xiaomi.mirror.service.MirrorService .get ( );
(( com.xiaomi.mirror.service.MirrorService ) v0 ).notifyPointerIconChanged ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/xiaomi/mirror/service/MirrorService;->notifyPointerIconChanged(ILandroid/view/PointerIcon;)V
/* .line 56 */
return;
} // .end method
public void sendDragStartedIfNeededLocked ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "window" # Ljava/lang/Object; */
/* .line 43 */
com.xiaomi.mirror.service.MirrorService .get ( );
v0 = (( com.xiaomi.mirror.service.MirrorService ) v0 ).getSystemReady ( ); // invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getSystemReady()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 44 */
com.xiaomi.mirror.service.MirrorService .get ( );
(( com.xiaomi.mirror.service.MirrorService ) v0 ).getDragDropController ( ); // invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getDragDropController()Lcom/android/server/wm/MiuiMirrorDragDropController;
(( com.android.server.wm.MiuiMirrorDragDropController ) v0 ).sendDragStartedIfNeededLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiMirrorDragDropController;->sendDragStartedIfNeededLocked(Ljava/lang/Object;)V
/* .line 46 */
} // :cond_0
return;
} // .end method
public void setAllowGrant ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "allowGrant" # Z */
/* .line 30 */
com.xiaomi.mirror.service.MirrorService .get ( );
(( com.xiaomi.mirror.service.MirrorService ) v0 ).setAllowGrant ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/mirror/service/MirrorService;->setAllowGrant(Z)V
/* .line 31 */
return;
} // .end method
