.class public abstract Lcom/xiaomi/mirror/service/MirrorServiceInternal;
.super Ljava/lang/Object;
.source "MirrorServiceInternal.java"


# static fields
.field static sInstance:Lcom/xiaomi/mirror/service/MirrorServiceInternal;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->sInstance:Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/xiaomi/mirror/service/MirrorServiceInternal;
    .locals 1

    .line 15
    sget-object v0, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->sInstance:Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    if-nez v0, :cond_0

    .line 16
    const-class v0, Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    sput-object v0, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->sInstance:Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    .line 18
    :cond_0
    sget-object v0, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->sInstance:Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    return-object v0
.end method


# virtual methods
.method public abstract getDelegatePid()I
.end method

.method public abstract isNeedFinishAnimator()Z
.end method

.method public abstract notifyDragFinish(Ljava/lang/String;Z)V
.end method

.method public abstract notifyDragResult(Z)V
.end method

.method public abstract notifyDragStart(IILandroid/content/ClipData;I)V
.end method

.method public abstract notifyDragStart(Landroid/content/ClipData;III)V
.end method

.method public abstract onDelegatePermissionReleased(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract tryToShareDrag(Ljava/lang/String;ILandroid/content/ClipData;)Z
.end method
