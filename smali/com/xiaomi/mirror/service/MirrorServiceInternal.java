public abstract class com.xiaomi.mirror.service.MirrorServiceInternal {
	 /* .source "MirrorServiceInternal.java" */
	 /* # static fields */
	 static com.xiaomi.mirror.service.MirrorServiceInternal sInstance;
	 /* # direct methods */
	 static com.xiaomi.mirror.service.MirrorServiceInternal ( ) {
		 /* .locals 1 */
		 /* .line 12 */
		 int v0 = 0; // const/4 v0, 0x0
		 return;
	 } // .end method
	 public com.xiaomi.mirror.service.MirrorServiceInternal ( ) {
		 /* .locals 0 */
		 /* .line 10 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static com.xiaomi.mirror.service.MirrorServiceInternal getInstance ( ) {
		 /* .locals 1 */
		 /* .line 15 */
		 v0 = com.xiaomi.mirror.service.MirrorServiceInternal.sInstance;
		 /* if-nez v0, :cond_0 */
		 /* .line 16 */
		 /* const-class v0, Lcom/xiaomi/mirror/service/MirrorServiceInternal; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Lcom/xiaomi/mirror/service/MirrorServiceInternal; */
		 /* .line 18 */
	 } // :cond_0
	 v0 = com.xiaomi.mirror.service.MirrorServiceInternal.sInstance;
} // .end method
/* # virtual methods */
public abstract Integer getDelegatePid ( ) {
} // .end method
public abstract Boolean isNeedFinishAnimator ( ) {
} // .end method
public abstract void notifyDragFinish ( java.lang.String p0, Boolean p1 ) {
} // .end method
public abstract void notifyDragResult ( Boolean p0 ) {
} // .end method
public abstract void notifyDragStart ( Integer p0, Integer p1, android.content.ClipData p2, Integer p3 ) {
} // .end method
public abstract void notifyDragStart ( android.content.ClipData p0, Integer p1, Integer p2, Integer p3 ) {
} // .end method
public abstract void onDelegatePermissionReleased ( java.util.List p0 ) {
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Ljava/util/List<", */
	 /* "Landroid/net/Uri;", */
	 /* ">;)V" */
	 /* } */
} // .end annotation
} // .end method
public abstract Boolean tryToShareDrag ( java.lang.String p0, Integer p1, android.content.ClipData p2 ) {
} // .end method
