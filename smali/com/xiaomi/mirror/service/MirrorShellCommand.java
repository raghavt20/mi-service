public class com.xiaomi.mirror.service.MirrorShellCommand extends android.os.ShellCommand {
	 /* .source "MirrorShellCommand.java" */
	 /* # direct methods */
	 public com.xiaomi.mirror.service.MirrorShellCommand ( ) {
		 /* .locals 0 */
		 /* .line 7 */
		 /* invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Integer onCommand ( java.lang.String p0 ) {
		 /* .locals 4 */
		 /* .param p1, "cmd" # Ljava/lang/String; */
		 /* .line 10 */
		 /* if-nez p1, :cond_0 */
		 /* .line 11 */
		 v0 = 		 (( com.xiaomi.mirror.service.MirrorShellCommand ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/xiaomi/mirror/service/MirrorShellCommand;->handleDefaultCommands(Ljava/lang/String;)I
		 /* .line 14 */
	 } // :cond_0
	 (( com.xiaomi.mirror.service.MirrorShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/xiaomi/mirror/service/MirrorShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
	 /* .line 16 */
	 /* .local v0, "pw":Ljava/io/PrintWriter; */
	 try { // :try_start_0
		 v1 = 		 (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
		 int v2 = 0; // const/4 v2, 0x0
		 int v3 = -1; // const/4 v3, -0x1
		 /* packed-switch v1, :pswitch_data_0 */
	 } // :cond_1
	 /* :pswitch_0 */
	 final String v1 = "reset-drag-and-drop"; // const-string v1, "reset-drag-and-drop"
	 v1 = 	 (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* move v1, v2 */
	 } // :goto_0
	 /* move v1, v3 */
} // :goto_1
/* packed-switch v1, :pswitch_data_1 */
/* .line 23 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 24 */
/* .line 18 */
/* :pswitch_1 */
try { // :try_start_1
	 com.xiaomi.mirror.service.MirrorService .get ( );
	 (( com.xiaomi.mirror.service.MirrorService ) v1 ).resetDragAndDropController ( ); // invoke-virtual {v1}, Lcom/xiaomi/mirror/service/MirrorService;->resetDragAndDropController()V
	 /* .line 19 */
	 final String v1 = "Reset successfully"; // const-string v1, "Reset successfully"
	 (( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
	 /* :try_end_1 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* .line 20 */
	 /* nop */
	 /* .line 23 */
	 (( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
	 /* .line 20 */
	 /* .line 25 */
} // :goto_2
/* .line 23 */
/* :catchall_0 */
/* move-exception v1 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 24 */
/* throw v1 */
/* :pswitch_data_0 */
/* .packed-switch -0x153b1260 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public void onHelp ( ) {
/* .locals 2 */
/* .line 30 */
(( com.xiaomi.mirror.service.MirrorShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/xiaomi/mirror/service/MirrorShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 31 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
final String v1 = "No help."; // const-string v1, "No help."
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 32 */
return;
} // .end method
