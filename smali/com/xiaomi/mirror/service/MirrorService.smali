.class public Lcom/xiaomi/mirror/service/MirrorService;
.super Lcom/xiaomi/mirror/IMirrorService$Stub;
.source "MirrorService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/mirror/service/MirrorService$LocalService;,
        Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;,
        Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;,
        Lcom/xiaomi/mirror/service/MirrorService$Lifecycle;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MirrorService"

.field private static sAllowGrant:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sInstance:Lcom/xiaomi/mirror/service/MirrorService;

.field private static final sLock:Ljava/lang/Object;


# instance fields
.field private mAtms:Landroid/app/IActivityTaskManager;

.field private mCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Lcom/xiaomi/mirror/IMirrorStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentShadowImage:Landroid/graphics/Bitmap;

.field private final mCursorPositionChangedListeners:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mCursorPositionListener:Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;

.field private final mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

.field private mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

.field private mDelegatePackageName:Ljava/lang/String;

.field private mDelegatePid:I

.field private mDelegateUid:I

.field private mDragAcceptable:Z

.field private final mDragListeners:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mIsInMouseShareMode:Z

.field private mLastNotifiedUid:I

.field private mMiuiMirrorDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;

.field private mNeedFinishAnimator:Z

.field private mPermissionOwner:Landroid/os/IBinder;

.field private mSystemReady:Z

.field private final mTempCursorPositionChangedListenersToNotify:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mTempDragListenersToNotify:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mUgm:Landroid/app/IUriGrantsManager;

.field private mUgmInternal:Lcom/android/server/uri/UriGrantsManagerInternal;


# direct methods
.method public static synthetic $r8$lambda$40IgGNuL8A02JN3QWxAKmHz_VbE(Lcom/xiaomi/mirror/service/MirrorService;IFF)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/mirror/service/MirrorService;->deliverCursorPositionChanged(IFF)V

    return-void
.end method

.method public static synthetic $r8$lambda$8otWs1Mx74AE1TBuPWyNcg4jJzI(Lcom/xiaomi/mirror/service/MirrorService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorService;->delegateLost()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentShadowImage(Lcom/xiaomi/mirror/service/MirrorService;)Landroid/graphics/Bitmap;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCurrentShadowImage:Landroid/graphics/Bitmap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$monCursorPositionChangedListenerDied(Lcom/xiaomi/mirror/service/MirrorService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/mirror/service/MirrorService;->onCursorPositionChangedListenerDied(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monDragListenerDied(Lcom/xiaomi/mirror/service/MirrorService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/mirror/service/MirrorService;->onDragListenerDied(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msystemReady(Lcom/xiaomi/mirror/service/MirrorService;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/mirror/service/MirrorService;->systemReady(Landroid/content/Context;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 52
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/xiaomi/mirror/service/MirrorService;->sLock:Ljava/lang/Object;

    .line 323
    new-instance v0, Lcom/xiaomi/mirror/service/MirrorService$1;

    invoke-direct {v0}, Lcom/xiaomi/mirror/service/MirrorService$1;-><init>()V

    sput-object v0, Lcom/xiaomi/mirror/service/MirrorService;->sAllowGrant:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 48
    invoke-direct {p0}, Lcom/xiaomi/mirror/IMirrorService$Stub;-><init>()V

    .line 61
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCallbacks:Landroid/os/RemoteCallbackList;

    .line 64
    new-instance v0, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda1;-><init>(Lcom/xiaomi/mirror/service/MirrorService;)V

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z

    .line 520
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    .line 522
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mTempCursorPositionChangedListenersToNotify:Ljava/util/ArrayList;

    .line 651
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mNeedFinishAnimator:Z

    .line 662
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragListeners:Landroid/util/SparseArray;

    .line 663
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mTempDragListenersToNotify:Ljava/util/ArrayList;

    return-void
.end method

.method private broadcastDelegateStateChanged()V
    .locals 5

    .line 344
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 345
    .local v0, "hasDelegate":Z
    :goto_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 347
    .local v2, "ident":J
    :try_start_0
    iget-object v4, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-int/2addr v4, v1

    .local v4, "i":I
    :goto_1
    if-ltz v4, :cond_1

    .line 349
    :try_start_1
    iget-object v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, v4}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/mirror/IMirrorStateListener;

    invoke-interface {v1, v0}, Lcom/xiaomi/mirror/IMirrorStateListener;->onDelegateStateChanged(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 352
    goto :goto_2

    .line 350
    :catch_0
    move-exception v1

    .line 347
    :goto_2
    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    .line 354
    .end local v4    # "i":I
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 356
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 357
    nop

    .line 358
    return-void

    .line 356
    :catchall_0
    move-exception v1

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 357
    throw v1
.end method

.method private delegateLost()V
    .locals 2

    .line 361
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    .line 362
    const/4 v1, 0x0

    iput v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegateUid:I

    .line 363
    iput v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegatePid:I

    .line 364
    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegatePackageName:Ljava/lang/String;

    .line 365
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mMiuiMirrorDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMirrorDragDropController;->shutdownDragAndDropIfNeeded()V

    .line 366
    invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorService;->broadcastDelegateStateChanged()V

    .line 367
    invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorService;->resetSystemProperties()V

    .line 368
    return-void
.end method

.method private deliverCursorPositionChanged(IFF)V
    .locals 5
    .param p1, "deviceId"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F

    .line 603
    iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mIsInMouseShareMode:Z

    if-nez v0, :cond_0

    .line 604
    return-void

    .line 606
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mTempCursorPositionChangedListenersToNotify:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 607
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    monitor-enter v0

    .line 608
    const/4 v1, 0x0

    .local v1, "i":I
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    .local v2, "size":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 609
    iget-object v3, p0, Lcom/xiaomi/mirror/service/MirrorService;->mTempCursorPositionChangedListenersToNotify:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    .line 610
    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 608
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 612
    .end local v1    # "i":I
    .end local v2    # "size":I
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 613
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mTempCursorPositionChangedListenersToNotify:Ljava/util/ArrayList;

    new-instance v1, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1, p2, p3}, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda0;-><init>(IFF)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->forEach(Ljava/util/function/Consumer;)V

    .line 615
    return-void

    .line 612
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static get()Lcom/xiaomi/mirror/service/MirrorService;
    .locals 2

    .line 91
    sget-object v0, Lcom/xiaomi/mirror/service/MirrorService;->sInstance:Lcom/xiaomi/mirror/service/MirrorService;

    if-nez v0, :cond_1

    .line 92
    sget-object v0, Lcom/xiaomi/mirror/service/MirrorService;->sLock:Ljava/lang/Object;

    monitor-enter v0

    .line 93
    :try_start_0
    sget-object v1, Lcom/xiaomi/mirror/service/MirrorService;->sInstance:Lcom/xiaomi/mirror/service/MirrorService;

    if-nez v1, :cond_0

    .line 94
    new-instance v1, Lcom/xiaomi/mirror/service/MirrorService;

    invoke-direct {v1}, Lcom/xiaomi/mirror/service/MirrorService;-><init>()V

    sput-object v1, Lcom/xiaomi/mirror/service/MirrorService;->sInstance:Lcom/xiaomi/mirror/service/MirrorService;

    .line 96
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 98
    :cond_1
    :goto_0
    sget-object v0, Lcom/xiaomi/mirror/service/MirrorService;->sInstance:Lcom/xiaomi/mirror/service/MirrorService;

    return-object v0
.end method

.method private grantItem(Landroid/content/ClipData$Item;ILjava/lang/String;II)V
    .locals 8
    .param p1, "item"    # Landroid/content/ClipData$Item;
    .param p2, "sourceUid"    # I
    .param p3, "targetPkg"    # Ljava/lang/String;
    .param p4, "targetUserId"    # I
    .param p5, "mode"    # I

    .line 460
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 461
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v2

    move-object v1, p0

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/xiaomi/mirror/service/MirrorService;->grantUri(Landroid/net/Uri;ILjava/lang/String;II)V

    .line 463
    :cond_0
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 464
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 465
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    move-object v2, p0

    move v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v2 .. v7}, Lcom/xiaomi/mirror/service/MirrorService;->grantUri(Landroid/net/Uri;ILjava/lang/String;II)V

    .line 467
    :cond_1
    return-void
.end method

.method private grantPermissions(ILandroid/content/ClipData;I)V
    .locals 9
    .param p1, "sourceUid"    # I
    .param p2, "data"    # Landroid/content/ClipData;
    .param p3, "flags"    # I

    .line 446
    and-int/lit16 v6, p3, 0xc3

    .line 451
    .local v6, "mode":I
    invoke-virtual {p2}, Landroid/content/ClipData;->getItemCount()I

    move-result v7

    .line 452
    .local v7, "N":I
    const/4 v0, 0x0

    move v8, v0

    .local v8, "i":I
    :goto_0
    if-ge v8, v7, :cond_0

    .line 453
    invoke-virtual {p2, v8}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v1

    iget-object v3, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegatePackageName:Ljava/lang/String;

    iget v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegateUid:I

    .line 454
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v4

    .line 453
    move-object v0, p0

    move v2, p1

    move v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/xiaomi/mirror/service/MirrorService;->grantItem(Landroid/content/ClipData$Item;ILjava/lang/String;II)V

    .line 452
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 456
    .end local v8    # "i":I
    :cond_0
    return-void
.end method

.method private grantUri(Landroid/net/Uri;ILjava/lang/String;II)V
    .locals 14
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "sourceUid"    # I
    .param p3, "targetPkg"    # Ljava/lang/String;
    .param p4, "targetUserId"    # I
    .param p5, "mode"    # I

    .line 470
    move-object v1, p0

    move-object v2, p1

    if-eqz v2, :cond_1

    const-string v0, "content"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 472
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v3

    .line 473
    .local v3, "ident":J
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/mirror/service/MirrorService;->setAllowGrant(Z)V

    .line 475
    const/4 v5, 0x0

    :try_start_0
    iget-object v6, v1, Lcom/xiaomi/mirror/service/MirrorService;->mUgm:Landroid/app/IUriGrantsManager;

    iget-object v7, v1, Lcom/xiaomi/mirror/service/MirrorService;->mPermissionOwner:Landroid/os/IBinder;

    .line 476
    invoke-static {p1}, Landroid/content/ContentProvider;->getUriWithoutUserId(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v10

    .line 478
    invoke-static/range {p2 .. p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    invoke-static {p1, v0}, Landroid/content/ContentProvider;->getUserIdFromUri(Landroid/net/Uri;I)I

    move-result v12

    .line 475
    move/from16 v8, p2

    move-object/from16 v9, p3

    move/from16 v11, p5

    move/from16 v13, p4

    invoke-interface/range {v6 .. v13}, Landroid/app/IUriGrantsManager;->grantUriPermissionFromOwner(Landroid/os/IBinder;ILjava/lang/String;Landroid/net/Uri;III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 483
    :catchall_0
    move-exception v0

    invoke-virtual {p0, v5}, Lcom/xiaomi/mirror/service/MirrorService;->setAllowGrant(Z)V

    .line 484
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 485
    throw v0

    .line 480
    :catch_0
    move-exception v0

    .line 483
    :goto_0
    invoke-virtual {p0, v5}, Lcom/xiaomi/mirror/service/MirrorService;->setAllowGrant(Z)V

    .line 484
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 485
    nop

    .line 486
    return-void

    .line 470
    .end local v3    # "ident":J
    :cond_1
    :goto_1
    return-void
.end method

.method static synthetic lambda$deliverCursorPositionChanged$0(IFFLcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;)V
    .locals 0
    .param p0, "deviceId"    # I
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "cursorPositionChangedListenerRecord"    # Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;

    .line 614
    invoke-virtual {p3, p0, p1, p2}, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;->notifyCursorPositionChanged(IFF)V

    return-void
.end method

.method static synthetic lambda$notifyDragFinish$2(Ljava/lang/String;ZLcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;)V
    .locals 0
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "dragResult"    # Z
    .param p2, "dragListenerRecord"    # Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;

    .line 740
    invoke-virtual {p2, p0, p1}, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;->notifyDragFinish(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic lambda$notifyDragStart$1(Landroid/content/ClipData;IIILcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;)V
    .locals 0
    .param p0, "data"    # Landroid/content/ClipData;
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "flag"    # I
    .param p4, "dragListenerRecord"    # Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;

    .line 726
    invoke-virtual {p4, p0, p1, p2, p3}, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;->notifyDragStart(Landroid/content/ClipData;III)V

    return-void
.end method

.method private onCursorPositionChangedListenerDied(I)V
    .locals 2
    .param p1, "pid"    # I

    .line 597
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    monitor-enter v0

    .line 598
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 599
    monitor-exit v0

    .line 600
    return-void

    .line 599
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private onDragListenerDied(I)V
    .locals 2
    .param p1, "pid"    # I

    .line 709
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragListeners:Landroid/util/SparseArray;

    monitor-enter v0

    .line 710
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragListeners:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 711
    monitor-exit v0

    .line 712
    return-void

    .line 711
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private populateCursorPositionListenerLocked()V
    .locals 2

    .line 556
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionListener:Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 559
    :cond_0
    const-string v0, "MirrorService"

    const-string v1, "register cursor position listener add to MiuiCursorPositionListenerService"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    new-instance v0, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0}, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda4;-><init>(Lcom/xiaomi/mirror/service/MirrorService;)V

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionListener:Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;

    .line 561
    invoke-static {}, Lcom/miui/server/input/MiuiCursorPositionListenerService;->getInstance()Lcom/miui/server/input/MiuiCursorPositionListenerService;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionListener:Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;

    .line 562
    invoke-virtual {v0, v1}, Lcom/miui/server/input/MiuiCursorPositionListenerService;->registerOnCursorPositionChangedListener(Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;)V

    .line 563
    return-void

    .line 557
    :cond_1
    :goto_0
    return-void
.end method

.method private resetSystemProperties()V
    .locals 3

    .line 371
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "miui_mirror_dnd_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 372
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_project_in_screening"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 373
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_project_hang_up_on"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 374
    return-void
.end method

.method private revokeItem(Landroid/content/ClipData$Item;I)V
    .locals 2
    .param p1, "item"    # Landroid/content/ClipData$Item;
    .param p2, "sourceUid"    # I

    .line 496
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 497
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/xiaomi/mirror/service/MirrorService;->revokeUri(Landroid/net/Uri;I)V

    .line 499
    :cond_0
    invoke-virtual {p1}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 500
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 501
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/xiaomi/mirror/service/MirrorService;->revokeUri(Landroid/net/Uri;I)V

    .line 503
    :cond_1
    return-void
.end method

.method private revokePermissionsInternal(Landroid/content/ClipData;I)V
    .locals 3
    .param p1, "data"    # Landroid/content/ClipData;
    .param p2, "sourceUid"    # I

    .line 489
    invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I

    move-result v0

    .line 490
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 491
    invoke-virtual {p1, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lcom/xiaomi/mirror/service/MirrorService;->revokeItem(Landroid/content/ClipData$Item;I)V

    .line 490
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 493
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method private revokeUri(Landroid/net/Uri;I)V
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "sourceUid"    # I

    .line 506
    if-eqz p1, :cond_1

    const-string v0, "content"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 508
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 510
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mUgmInternal:Lcom/android/server/uri/UriGrantsManagerInternal;

    iget-object v3, p0, Lcom/xiaomi/mirror/service/MirrorService;->mPermissionOwner:Landroid/os/IBinder;

    .line 511
    invoke-static {p1}, Landroid/content/ContentProvider;->getUriWithoutUserId(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v4

    .line 513
    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v5

    invoke-static {p1, v5}, Landroid/content/ContentProvider;->getUserIdFromUri(Landroid/net/Uri;I)I

    move-result v5

    .line 510
    const/4 v6, 0x1

    invoke-interface {v2, v3, v4, v6, v5}, Lcom/android/server/uri/UriGrantsManagerInternal;->revokeUriPermissionFromOwner(Landroid/os/IBinder;Landroid/net/Uri;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 515
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 516
    nop

    .line 517
    return-void

    .line 515
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 516
    throw v2

    .line 506
    .end local v0    # "ident":J
    :cond_1
    :goto_0
    return-void
.end method

.method private systemReady(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 102
    iput-object p1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mContext:Landroid/content/Context;

    .line 103
    invoke-static {}, Landroid/app/UriGrantsManager;->getService()Landroid/app/IUriGrantsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mUgm:Landroid/app/IUriGrantsManager;

    .line 104
    const-class v0, Lcom/android/server/uri/UriGrantsManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/uri/UriGrantsManagerInternal;

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mUgmInternal:Lcom/android/server/uri/UriGrantsManagerInternal;

    .line 105
    const-string v1, "mirror"

    invoke-interface {v0, v1}, Lcom/android/server/uri/UriGrantsManagerInternal;->newUriPermissionOwner(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mPermissionOwner:Landroid/os/IBinder;

    .line 106
    new-instance v0, Lcom/android/server/wm/MiuiMirrorDragDropController;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiMirrorDragDropController;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mMiuiMirrorDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;

    .line 107
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mAtms:Landroid/app/IActivityTaskManager;

    .line 108
    const-class v0, Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    new-instance v1, Lcom/xiaomi/mirror/service/MirrorService$LocalService;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/xiaomi/mirror/service/MirrorService$LocalService;-><init>(Lcom/xiaomi/mirror/service/MirrorService;Lcom/xiaomi/mirror/service/MirrorService$LocalService-IA;)V

    invoke-static {v0, v1}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z

    .line 110
    return-void
.end method

.method private unPopulateCursorPositionListenerLocked()V
    .locals 2

    .line 586
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionListener:Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    goto :goto_0

    .line 589
    :cond_0
    const-string v0, "MirrorService"

    const-string/jumbo v1, "unregister cursor position listener from MiuiCursorPositionListenerService"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    invoke-static {}, Lcom/miui/server/input/MiuiCursorPositionListenerService;->getInstance()Lcom/miui/server/input/MiuiCursorPositionListenerService;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionListener:Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;

    .line 591
    invoke-virtual {v0, v1}, Lcom/miui/server/input/MiuiCursorPositionListenerService;->unregisterOnCursorPositionChangedListener(Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;)V

    .line 592
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionListener:Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;

    .line 593
    return-void

    .line 587
    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public addMirrorStateListener(Lcom/xiaomi/mirror/IMirrorStateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/xiaomi/mirror/IMirrorStateListener;

    .line 142
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 144
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1, v0}, Lcom/xiaomi/mirror/IMirrorStateListener;->onDelegateStateChanged(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    goto :goto_1

    .line 145
    :catch_0
    move-exception v0

    .line 147
    :goto_1
    return-void
.end method

.method public cancelCurrentDrag()Z
    .locals 1

    .line 648
    invoke-static {}, Lcom/android/server/wm/DragDropControllerStub;->get()Lcom/android/server/wm/DragDropControllerStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/wm/DragDropControllerStub;->cancelCurrentDrag()Z

    move-result v0

    return v0
.end method

.method public cancelDragAndDrop(Landroid/os/IBinder;)V
    .locals 3
    .param p1, "dragToken"    # Landroid/os/IBinder;

    .line 253
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 255
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mMiuiMirrorDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;

    invoke-virtual {v2, p1}, Lcom/android/server/wm/MiuiMirrorDragDropController;->cancelDragAndDrop(Landroid/os/IBinder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 258
    nop

    .line 259
    return-void

    .line 257
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 258
    throw v2
.end method

.method public getAllowGrant()Z
    .locals 1

    .line 337
    iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 338
    sget-object v0, Lcom/xiaomi/mirror/service/MirrorService;->sAllowGrant:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 340
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getDelegatePackageName()Ljava/lang/String;
    .locals 1

    .line 134
    iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegatePackageName:Ljava/lang/String;

    return-object v0

    .line 137
    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getDelegatePid()I
    .locals 1

    .line 127
    iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 128
    iget v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegatePid:I

    return v0

    .line 130
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getDragDropController()Lcom/android/server/wm/MiuiMirrorDragDropController;
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mMiuiMirrorDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;

    return-object v0
.end method

.method public getSystemReady()Z
    .locals 1

    .line 113
    iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z

    return v0
.end method

.method public grantUriPermissionsToPackage(Ljava/util/List;ILjava/lang/String;II)V
    .locals 8
    .param p2, "sourceUid"    # I
    .param p3, "targetPkg"    # Ljava/lang/String;
    .param p4, "targetUserId"    # I
    .param p5, "mode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;I",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .line 195
    .local p1, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mContext:Landroid/content/Context;

    const-string v1, "com.xiaomi.mirror.permission.REGISTER_MIRROR_DELEGATE"

    const-string v2, "grantUriPermissionsToPackage"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 198
    .local v1, "uri":Landroid/net/Uri;
    move-object v2, p0

    move-object v3, v1

    move v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v2 .. v7}, Lcom/xiaomi/mirror/service/MirrorService;->grantUri(Landroid/net/Uri;ILjava/lang/String;II)V

    .line 199
    .end local v1    # "uri":Landroid/net/Uri;
    goto :goto_0

    .line 200
    :cond_0
    return-void
.end method

.method public injectDragEvent(IIFF)V
    .locals 3
    .param p1, "action"    # I
    .param p2, "displayId"    # I
    .param p3, "newX"    # F
    .param p4, "newY"    # F

    .line 232
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 234
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mMiuiMirrorDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiMirrorDragDropController;->injectDragEvent(IIFF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 237
    nop

    .line 238
    return-void

    .line 236
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 237
    throw v2
.end method

.method public isInMouseShareMode()Z
    .locals 1

    .line 799
    iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mIsInMouseShareMode:Z

    return v0
.end method

.method public isNeedFinishAnimator()Z
    .locals 1

    .line 659
    iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mNeedFinishAnimator:Z

    return v0
.end method

.method public notifyDragFinish(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "dragResult"    # Z

    .line 730
    iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mIsInMouseShareMode:Z

    if-nez v0, :cond_0

    .line 731
    return-void

    .line 733
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mTempDragListenersToNotify:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 734
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragListeners:Landroid/util/SparseArray;

    monitor-enter v0

    .line 735
    const/4 v1, 0x0

    .local v1, "i":I
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragListeners:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    .local v2, "size":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 736
    iget-object v3, p0, Lcom/xiaomi/mirror/service/MirrorService;->mTempDragListenersToNotify:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragListeners:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 735
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 738
    .end local v1    # "i":I
    .end local v2    # "size":I
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 739
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mTempDragListenersToNotify:Ljava/util/ArrayList;

    new-instance v1, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda3;

    invoke-direct {v1, p1, p2}, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda3;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->forEach(Ljava/util/function/Consumer;)V

    .line 741
    return-void

    .line 738
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public notifyDragResult(Z)V
    .locals 5
    .param p1, "result"    # Z

    .line 405
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    if-eqz v0, :cond_0

    .line 406
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 408
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    invoke-interface {v2, p1}, Lcom/xiaomi/mirror/IMirrorDelegate;->onDragResult(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    nop

    :goto_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 413
    goto :goto_2

    .line 412
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 409
    :catch_0
    move-exception v2

    .line 410
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v3, "MirrorService"

    const-string v4, "notify drag cancel failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412
    nop

    .end local v2    # "e":Landroid/os/RemoteException;
    goto :goto_0

    :goto_1
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 413
    throw v2

    .line 415
    .end local v0    # "ident":J
    :cond_0
    :goto_2
    return-void
.end method

.method public notifyDragStart(IILandroid/content/ClipData;I)V
    .locals 5
    .param p1, "sourceUid"    # I
    .param p2, "sourcePid"    # I
    .param p3, "clipData"    # Landroid/content/ClipData;
    .param p4, "flags"    # I

    .line 377
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    if-eqz v0, :cond_0

    .line 378
    invoke-direct {p0, p1, p3, p4}, Lcom/xiaomi/mirror/service/MirrorService;->grantPermissions(ILandroid/content/ClipData;I)V

    .line 379
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 381
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    invoke-interface {v2, p3, p1, p2, p4}, Lcom/xiaomi/mirror/IMirrorDelegate;->onDragStart(Landroid/content/ClipData;III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    nop

    :goto_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 386
    goto :goto_2

    .line 385
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 382
    :catch_0
    move-exception v2

    .line 383
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v3, "MirrorService"

    const-string v4, "notify drag start failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 385
    nop

    .end local v2    # "e":Landroid/os/RemoteException;
    goto :goto_0

    :goto_1
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 386
    throw v2

    .line 388
    .end local v0    # "ident":J
    :cond_0
    :goto_2
    return-void
.end method

.method public notifyDragStart(Landroid/content/ClipData;III)V
    .locals 5
    .param p1, "data"    # Landroid/content/ClipData;
    .param p2, "uid"    # I
    .param p3, "pid"    # I
    .param p4, "flag"    # I

    .line 715
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mNeedFinishAnimator:Z

    .line 716
    iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mIsInMouseShareMode:Z

    if-nez v0, :cond_0

    .line 717
    return-void

    .line 719
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mTempDragListenersToNotify:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 720
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragListeners:Landroid/util/SparseArray;

    monitor-enter v0

    .line 721
    const/4 v1, 0x0

    .local v1, "i":I
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragListeners:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    .local v2, "size":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 722
    iget-object v3, p0, Lcom/xiaomi/mirror/service/MirrorService;->mTempDragListenersToNotify:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragListeners:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 721
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 724
    .end local v1    # "i":I
    .end local v2    # "size":I
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 725
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mTempDragListenersToNotify:Ljava/util/ArrayList;

    new-instance v1, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda2;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda2;-><init>(Landroid/content/ClipData;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->forEach(Ljava/util/function/Consumer;)V

    .line 727
    return-void

    .line 724
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public notifyMouseShareModeState(Z)V
    .locals 4
    .param p1, "isInMouseShareMode"    # Z

    .line 782
    iput-boolean p1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mIsInMouseShareMode:Z

    .line 783
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 785
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v2, v2, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_0

    .line 787
    :try_start_1
    iget-object v3, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/mirror/IMirrorStateListener;

    invoke-interface {v3, p1}, Lcom/xiaomi/mirror/IMirrorStateListener;->onMouseShareModeStateChanged(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 790
    goto :goto_1

    .line 788
    :catch_0
    move-exception v3

    .line 785
    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 792
    .end local v2    # "i":I
    :cond_0
    :try_start_2
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 794
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 795
    nop

    .line 796
    return-void

    .line 794
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 795
    throw v2
.end method

.method public notifyPointerIconChanged(ILandroid/view/PointerIcon;)V
    .locals 5
    .param p1, "iconId"    # I
    .param p2, "customIcon"    # Landroid/view/PointerIcon;

    .line 431
    iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    if-eqz v0, :cond_0

    .line 433
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 435
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    invoke-interface {v2, p1, p2}, Lcom/xiaomi/mirror/IMirrorDelegate;->onPointerIconChanged(ILandroid/view/PointerIcon;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 439
    nop

    :goto_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 440
    goto :goto_2

    .line 439
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 436
    :catch_0
    move-exception v2

    .line 437
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v3, "MirrorService"

    const-string v4, "notify pointer icon change failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 439
    nop

    .end local v2    # "e":Landroid/os/RemoteException;
    goto :goto_0

    :goto_1
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 440
    throw v2

    .line 443
    .end local v0    # "ident":J
    :cond_0
    :goto_2
    return-void
.end method

.method public notifyShadowImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "shadowImage"    # Landroid/graphics/Bitmap;

    .line 777
    iput-object p1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCurrentShadowImage:Landroid/graphics/Bitmap;

    .line 778
    return-void
.end method

.method public onDelegatePermissionReleased(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .line 418
    .local p1, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    if-eqz v0, :cond_0

    .line 419
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 421
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    invoke-interface {v2, p1}, Lcom/xiaomi/mirror/IMirrorDelegate;->onDelegatePermissionReleased(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 425
    nop

    :goto_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 426
    goto :goto_2

    .line 425
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 422
    :catch_0
    move-exception v2

    .line 423
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v3, "MirrorService"

    const-string v4, "notify permission release failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 425
    nop

    .end local v2    # "e":Landroid/os/RemoteException;
    goto :goto_0

    :goto_1
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 426
    throw v2

    .line 428
    .end local v0    # "ident":J
    :cond_0
    :goto_2
    return-void
.end method

.method public onRemoteMenuActionCall(Lcom/xiaomi/mirror/MirrorMenu;I)V
    .locals 7
    .param p1, "menu"    # Lcom/xiaomi/mirror/MirrorMenu;
    .param p2, "displayId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 302
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    if-eqz v0, :cond_1

    .line 303
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 304
    .local v0, "uid":I
    invoke-virtual {p1}, Lcom/xiaomi/mirror/MirrorMenu;->getUri()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 305
    invoke-virtual {p1}, Lcom/xiaomi/mirror/MirrorMenu;->getUri()Landroid/net/Uri;

    move-result-object v2

    iget-object v4, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegatePackageName:Ljava/lang/String;

    iget v5, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegateUid:I

    const/4 v6, 0x3

    move-object v1, p0

    move v3, v0

    invoke-direct/range {v1 .. v6}, Lcom/xiaomi/mirror/service/MirrorService;->grantUri(Landroid/net/Uri;ILjava/lang/String;II)V

    .line 308
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 310
    .local v1, "ident":J
    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    invoke-interface {v3, p1, p2}, Lcom/xiaomi/mirror/IMirrorDelegate;->onRemoteMenuActionCall(Lcom/xiaomi/mirror/MirrorMenu;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 313
    goto :goto_0

    .line 312
    :catchall_0
    move-exception v3

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 313
    throw v3

    .line 315
    .end local v0    # "uid":I
    .end local v1    # "ident":J
    :cond_1
    :goto_0
    return-void
.end method

.method public onShellCommand(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)V
    .locals 8
    .param p1, "in"    # Ljava/io/FileDescriptor;
    .param p2, "out"    # Ljava/io/FileDescriptor;
    .param p3, "err"    # Ljava/io/FileDescriptor;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "callback"    # Landroid/os/ShellCallback;
    .param p6, "resultReceiver"    # Landroid/os/ResultReceiver;

    .line 320
    new-instance v0, Lcom/xiaomi/mirror/service/MirrorShellCommand;

    invoke-direct {v0}, Lcom/xiaomi/mirror/service/MirrorShellCommand;-><init>()V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/xiaomi/mirror/service/MirrorShellCommand;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I

    .line 321
    return-void
.end method

.method public performDrag(Landroid/view/IWindow;IILandroid/content/ClipData;Landroid/graphics/Bitmap;)Landroid/os/IBinder;
    .locals 13
    .param p1, "window"    # Landroid/view/IWindow;
    .param p2, "flags"    # I
    .param p3, "sourceDisplayId"    # I
    .param p4, "data"    # Landroid/content/ClipData;
    .param p5, "shadow"    # Landroid/graphics/Bitmap;

    .line 212
    move-object v1, p0

    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->getCallingPid()I

    move-result v9

    .line 213
    .local v9, "callerPid":I
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->getCallingUid()I

    move-result v10

    .line 214
    .local v10, "callerUid":I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v11

    .line 217
    .local v11, "ident":J
    :try_start_0
    iget-object v2, v1, Lcom/xiaomi/mirror/service/MirrorService;->mMiuiMirrorDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;

    move v3, v9

    move v4, v10

    move-object v5, p1

    move v6, p2

    move/from16 v7, p3

    move-object/from16 v8, p4

    invoke-virtual/range {v2 .. v8}, Lcom/android/server/wm/MiuiMirrorDragDropController;->performDrag(IILandroid/view/IWindow;IILandroid/content/ClipData;)Landroid/os/IBinder;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 219
    .local v0, "token":Landroid/os/IBinder;
    if-eqz v0, :cond_0

    .line 220
    move-object/from16 v2, p5

    :try_start_1
    invoke-virtual {p0, v2}, Lcom/xiaomi/mirror/service/MirrorService;->updateShadow(Landroid/graphics/Bitmap;)V

    .line 221
    const/4 v3, 0x0

    iput-boolean v3, v1, Lcom/xiaomi/mirror/service/MirrorService;->mDragAcceptable:Z

    .line 222
    iput v3, v1, Lcom/xiaomi/mirror/service/MirrorService;->mLastNotifiedUid:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 226
    .end local v0    # "token":Landroid/os/IBinder;
    :catchall_0
    move-exception v0

    goto :goto_1

    .line 219
    .restart local v0    # "token":Landroid/os/IBinder;
    :cond_0
    move-object/from16 v2, p5

    .line 224
    :goto_0
    nop

    .line 226
    invoke-static {v11, v12}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 224
    return-object v0

    .line 226
    .end local v0    # "token":Landroid/os/IBinder;
    :catchall_1
    move-exception v0

    move-object/from16 v2, p5

    :goto_1
    invoke-static {v11, v12}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 227
    throw v0
.end method

.method public registerCursorPositionChangedListener(Lcom/xiaomi/mirror/ICursorPositionChangedListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/xiaomi/mirror/ICursorPositionChangedListener;

    .line 527
    if-eqz p1, :cond_2

    .line 530
    iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mIsInMouseShareMode:Z

    if-eqz v0, :cond_1

    .line 533
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    monitor-enter v0

    .line 534
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    .line 535
    .local v1, "callingPid":I
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 539
    new-instance v2, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;

    invoke-direct {v2, p0, v1, p1}, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;-><init>(Lcom/xiaomi/mirror/service/MirrorService;ILcom/xiaomi/mirror/ICursorPositionChangedListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 542
    .local v2, "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;
    :try_start_1
    invoke-interface {p1}, Lcom/xiaomi/mirror/ICursorPositionChangedListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    .line 543
    .local v3, "binder":Landroid/os/IBinder;
    const/4 v4, 0x0

    invoke-interface {v3, v2, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 547
    .end local v3    # "binder":Landroid/os/IBinder;
    nop

    .line 548
    :try_start_2
    const-string v3, "MirrorService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "a cursor listener register, from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", current size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    .line 549
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 548
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    iget-object v3, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 551
    invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorService;->populateCursorPositionListenerLocked()V

    .line 552
    .end local v1    # "callingPid":I
    .end local v2    # "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;
    monitor-exit v0

    .line 553
    return-void

    .line 544
    .restart local v1    # "callingPid":I
    .restart local v2    # "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;
    :catch_0
    move-exception v3

    .line 546
    .local v3, "e":Landroid/os/RemoteException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/xiaomi/mirror/service/MirrorService;
    .end local p1    # "listener":Lcom/xiaomi/mirror/ICursorPositionChangedListener;
    throw v4

    .line 536
    .end local v2    # "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;
    .end local v3    # "e":Landroid/os/RemoteException;
    .restart local p0    # "this":Lcom/xiaomi/mirror/service/MirrorService;
    .restart local p1    # "listener":Lcom/xiaomi/mirror/ICursorPositionChangedListener;
    :cond_0
    new-instance v2, Ljava/lang/SecurityException;

    const-string v3, "The calling process has alreadyregistered an CursorPositionChangedListener."

    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/xiaomi/mirror/service/MirrorService;
    .end local p1    # "listener":Lcom/xiaomi/mirror/ICursorPositionChangedListener;
    throw v2

    .line 552
    .end local v1    # "callingPid":I
    .restart local p0    # "this":Lcom/xiaomi/mirror/service/MirrorService;
    .restart local p1    # "listener":Lcom/xiaomi/mirror/ICursorPositionChangedListener;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 531
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "current is not in mouse share mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 528
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public registerDelegate(Lcom/xiaomi/mirror/IMirrorDelegate;Ljava/lang/String;)V
    .locals 4
    .param p1, "delegate"    # Lcom/xiaomi/mirror/IMirrorDelegate;
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 156
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mContext:Landroid/content/Context;

    const-string v1, "com.xiaomi.mirror.permission.REGISTER_MIRROR_DELEGATE"

    const-string v2, "registerDelegate"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    if-eqz v0, :cond_0

    .line 159
    const-string v0, "MirrorService"

    const-string v1, "already delegating!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    return-void

    .line 162
    :cond_0
    if-eqz p1, :cond_1

    .line 165
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->getCallingUid()I

    move-result v0

    .line 166
    .local v0, "uid":I
    iget-object v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mContext:Landroid/content/Context;

    const-class v2, Landroid/app/AppOpsManager;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AppOpsManager;

    invoke-virtual {v1, v0, p2}, Landroid/app/AppOpsManager;->checkPackage(ILjava/lang/String;)V

    .line 167
    invoke-interface {p1}, Lcom/xiaomi/mirror/IMirrorDelegate;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 168
    iput-object p1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    .line 169
    iput v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegateUid:I

    .line 170
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->getCallingPid()I

    move-result v1

    iput v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegatePid:I

    .line 171
    iput-object p2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegatePackageName:Ljava/lang/String;

    .line 172
    invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorService;->broadcastDelegateStateChanged()V

    .line 173
    return-void

    .line 163
    .end local v0    # "uid":I
    :cond_1
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
.end method

.method public registerDragListener(Lcom/xiaomi/mirror/IDragListener;)V
    .locals 5
    .param p1, "listener"    # Lcom/xiaomi/mirror/IDragListener;

    .line 667
    if-eqz p1, :cond_2

    .line 670
    iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mIsInMouseShareMode:Z

    if-eqz v0, :cond_1

    .line 673
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragListeners:Landroid/util/SparseArray;

    monitor-enter v0

    .line 674
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    .line 675
    .local v1, "callingPid":I
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragListeners:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 679
    new-instance v2, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;

    invoke-direct {v2, p0, v1, p1}, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;-><init>(Lcom/xiaomi/mirror/service/MirrorService;ILcom/xiaomi/mirror/IDragListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 682
    .local v2, "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;
    :try_start_1
    invoke-interface {p1}, Lcom/xiaomi/mirror/IDragListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    .line 683
    .local v3, "binder":Landroid/os/IBinder;
    const/4 v4, 0x0

    invoke-interface {v3, v2, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 687
    .end local v3    # "binder":Landroid/os/IBinder;
    nop

    .line 688
    :try_start_2
    iget-object v3, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragListeners:Landroid/util/SparseArray;

    invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 689
    .end local v1    # "callingPid":I
    .end local v2    # "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;
    monitor-exit v0

    .line 690
    return-void

    .line 684
    .restart local v1    # "callingPid":I
    .restart local v2    # "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;
    :catch_0
    move-exception v3

    .line 686
    .local v3, "e":Landroid/os/RemoteException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/xiaomi/mirror/service/MirrorService;
    .end local p1    # "listener":Lcom/xiaomi/mirror/IDragListener;
    throw v4

    .line 676
    .end local v2    # "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;
    .end local v3    # "e":Landroid/os/RemoteException;
    .restart local p0    # "this":Lcom/xiaomi/mirror/service/MirrorService;
    .restart local p1    # "listener":Lcom/xiaomi/mirror/IDragListener;
    :cond_0
    new-instance v2, Ljava/lang/SecurityException;

    const-string v3, "The calling process has alreadyregistered a drag listener."

    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/xiaomi/mirror/service/MirrorService;
    .end local p1    # "listener":Lcom/xiaomi/mirror/IDragListener;
    throw v2

    .line 689
    .end local v1    # "callingPid":I
    .restart local p0    # "this":Lcom/xiaomi/mirror/service/MirrorService;
    .restart local p1    # "listener":Lcom/xiaomi/mirror/IDragListener;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 671
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "current is not in mouse share mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 668
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeMirrorStateListener(Lcom/xiaomi/mirror/IMirrorStateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/xiaomi/mirror/IMirrorStateListener;

    .line 151
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 152
    return-void
.end method

.method public reportDropResult(Landroid/view/IWindow;Z)V
    .locals 4
    .param p1, "window"    # Landroid/view/IWindow;
    .param p2, "consumed"    # Z

    .line 242
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 243
    .local v0, "pid":I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 245
    .local v1, "ident":J
    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/mirror/service/MirrorService;->mMiuiMirrorDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;

    invoke-virtual {v3, v0, p1, p2}, Lcom/android/server/wm/MiuiMirrorDragDropController;->reportDropResult(ILandroid/view/IWindow;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 248
    nop

    .line 249
    return-void

    .line 247
    :catchall_0
    move-exception v3

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 248
    throw v3
.end method

.method resetDragAndDropController()V
    .locals 1

    .line 121
    iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 122
    new-instance v0, Lcom/android/server/wm/MiuiMirrorDragDropController;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiMirrorDragDropController;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mMiuiMirrorDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;

    .line 124
    :cond_0
    return-void
.end method

.method public revokePermissions(Landroid/content/ClipData;I)V
    .locals 3
    .param p1, "data"    # Landroid/content/ClipData;
    .param p2, "sourceUid"    # I

    .line 204
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mContext:Landroid/content/Context;

    const-string v1, "com.xiaomi.mirror.permission.REGISTER_MIRROR_DELEGATE"

    const-string v2, "revokeDragDataPermissions"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-direct {p0, p1, p2}, Lcom/xiaomi/mirror/service/MirrorService;->revokePermissionsInternal(Landroid/content/ClipData;I)V

    .line 207
    return-void
.end method

.method public setAllowGrant(Z)V
    .locals 2
    .param p1, "allowGrant"    # Z

    .line 331
    iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 332
    sget-object v0, Lcom/xiaomi/mirror/service/MirrorService;->sAllowGrant:Ljava/lang/ThreadLocal;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 334
    :cond_0
    return-void
.end method

.method public setDragAcceptable(Z)V
    .locals 6
    .param p1, "acceptable"    # Z

    .line 276
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mMiuiMirrorDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMirrorDragDropController;->dragDropActiveLocked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    return-void

    .line 279
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 280
    .local v0, "uid":I
    iget-object v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mMiuiMirrorDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiMirrorDragDropController;->lastTargetUid()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 281
    return-void

    .line 283
    :cond_1
    iget v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mLastNotifiedUid:I

    if-ne v1, v0, :cond_2

    iget-boolean v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragAcceptable:Z

    if-ne v1, p1, :cond_2

    .line 284
    return-void

    .line 286
    :cond_2
    iput v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mLastNotifiedUid:I

    .line 287
    iput-boolean p1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragAcceptable:Z

    .line 288
    iget-object v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    if-eqz v1, :cond_3

    .line 289
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 291
    .local v1, "ident":J
    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    invoke-interface {v3, v0, p1}, Lcom/xiaomi/mirror/IMirrorDelegate;->onAcceptableChanged(IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    nop

    :goto_0
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 296
    goto :goto_2

    .line 295
    :catchall_0
    move-exception v3

    goto :goto_1

    .line 292
    :catch_0
    move-exception v3

    .line 293
    .local v3, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v4, "MirrorService"

    const-string v5, "notify drag acceptable failed"

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 295
    nop

    .end local v3    # "e":Landroid/os/RemoteException;
    goto :goto_0

    :goto_1
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 296
    throw v3

    .line 298
    .end local v1    # "ident":J
    :cond_3
    :goto_2
    return-void
.end method

.method public setDragSurfaceVisible(Z)Z
    .locals 1
    .param p1, "visible"    # Z

    .line 643
    invoke-static {}, Lcom/android/server/wm/DragDropControllerStub;->get()Lcom/android/server/wm/DragDropControllerStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/wm/DragDropControllerStub;->setDragSurfaceVisible(Z)Z

    move-result v0

    return v0
.end method

.method public setFinishAnimatorNeeded(Z)V
    .locals 0
    .param p1, "needFinishAnimator"    # Z

    .line 655
    iput-boolean p1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mNeedFinishAnimator:Z

    .line 656
    return-void
.end method

.method public tryToShareDrag(Ljava/lang/String;ILandroid/content/ClipData;)Z
    .locals 5
    .param p1, "targetPkg"    # Ljava/lang/String;
    .param p2, "taskId"    # I
    .param p3, "data"    # Landroid/content/ClipData;

    .line 391
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    if-eqz v0, :cond_0

    .line 392
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 394
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    invoke-interface {v2, p1, p2, p3}, Lcom/xiaomi/mirror/IMirrorDelegate;->tryToShareDrag(Ljava/lang/String;ILandroid/content/ClipData;)Z

    move-result v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 394
    return v2

    .line 398
    :catchall_0
    move-exception v2

    goto :goto_0

    .line 395
    :catch_0
    move-exception v2

    .line 396
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v3, "MirrorService"

    const-string/jumbo v4, "try to share drag failed"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 398
    nop

    .end local v2    # "e":Landroid/os/RemoteException;
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 399
    goto :goto_1

    .line 398
    :goto_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 399
    throw v2

    .line 401
    .end local v0    # "ident":J
    :cond_0
    :goto_1
    const/4 v0, 0x0

    return v0
.end method

.method public unregisterCursorPositionChangedListener(Lcom/xiaomi/mirror/ICursorPositionChangedListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/xiaomi/mirror/ICursorPositionChangedListener;

    .line 567
    if-eqz p1, :cond_1

    .line 570
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    monitor-enter v0

    .line 571
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    .line 572
    .local v1, "callingPid":I
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    .line 573
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;

    .line 574
    .local v2, "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;
    if-eqz v2, :cond_0

    .line 578
    const-string v3, "MirrorService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "a cursor listener unregister, from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", current size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/xiaomi/mirror/service/MirrorService;->mCursorPositionChangedListeners:Landroid/util/SparseArray;

    .line 579
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 578
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    invoke-direct {p0, v1}, Lcom/xiaomi/mirror/service/MirrorService;->onCursorPositionChangedListenerDied(I)V

    .line 581
    invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorService;->unPopulateCursorPositionListenerLocked()V

    .line 582
    .end local v1    # "callingPid":I
    .end local v2    # "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;
    monitor-exit v0

    .line 583
    return-void

    .line 575
    .restart local v1    # "callingPid":I
    .restart local v2    # "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;
    :cond_0
    new-instance v3, Ljava/lang/SecurityException;

    const-string v4, "The calling process not register an CursorPositionChangedListener."

    invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/xiaomi/mirror/service/MirrorService;
    .end local p1    # "listener":Lcom/xiaomi/mirror/ICursorPositionChangedListener;
    throw v3

    .line 582
    .end local v1    # "callingPid":I
    .end local v2    # "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;
    .restart local p0    # "this":Lcom/xiaomi/mirror/service/MirrorService;
    .restart local p1    # "listener":Lcom/xiaomi/mirror/ICursorPositionChangedListener;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 568
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public unregisterDelegate(Lcom/xiaomi/mirror/IMirrorDelegate;)V
    .locals 3
    .param p1, "delegate"    # Lcom/xiaomi/mirror/IMirrorDelegate;

    .line 177
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mContext:Landroid/content/Context;

    const-string v1, "com.xiaomi.mirror.permission.REGISTER_MIRROR_DELEGATE"

    const-string/jumbo v2, "unregisterDelegate"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    const-string v1, "MirrorService"

    if-nez v0, :cond_0

    .line 180
    const-string v0, "already undelegated"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    return-void

    .line 183
    :cond_0
    invoke-interface {p1}, Lcom/xiaomi/mirror/IMirrorDelegate;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    invoke-interface {v2}, Lcom/xiaomi/mirror/IMirrorDelegate;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    if-eq v0, v2, :cond_1

    .line 184
    const-string v0, "binder doesn\'t match!"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    return-void

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    invoke-interface {v0}, Lcom/xiaomi/mirror/IMirrorDelegate;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 188
    invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorService;->delegateLost()V

    .line 190
    :cond_2
    return-void
.end method

.method public unregisterDragListener(Lcom/xiaomi/mirror/IDragListener;)V
    .locals 5
    .param p1, "listener"    # Lcom/xiaomi/mirror/IDragListener;

    .line 694
    if-eqz p1, :cond_1

    .line 697
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragListeners:Landroid/util/SparseArray;

    monitor-enter v0

    .line 698
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    .line 699
    .local v1, "callingPid":I
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragListeners:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;

    .line 700
    .local v2, "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;
    if-eqz v2, :cond_0

    .line 704
    invoke-direct {p0, v1}, Lcom/xiaomi/mirror/service/MirrorService;->onDragListenerDied(I)V

    .line 705
    .end local v1    # "callingPid":I
    .end local v2    # "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;
    monitor-exit v0

    .line 706
    return-void

    .line 701
    .restart local v1    # "callingPid":I
    .restart local v2    # "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;
    :cond_0
    new-instance v3, Ljava/lang/SecurityException;

    const-string v4, "The calling process not register an drag listener"

    invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/xiaomi/mirror/service/MirrorService;
    .end local p1    # "listener":Lcom/xiaomi/mirror/IDragListener;
    throw v3

    .line 705
    .end local v1    # "callingPid":I
    .end local v2    # "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;
    .restart local p0    # "this":Lcom/xiaomi/mirror/service/MirrorService;
    .restart local p1    # "listener":Lcom/xiaomi/mirror/IDragListener;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 695
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public updateShadow(Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "shadow"    # Landroid/graphics/Bitmap;

    .line 263
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 264
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 266
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegate:Lcom/xiaomi/mirror/IMirrorDelegate;

    invoke-interface {v2, p1}, Lcom/xiaomi/mirror/IMirrorDelegate;->onShadowChanged(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270
    nop

    :goto_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 271
    goto :goto_2

    .line 270
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 267
    :catch_0
    move-exception v2

    .line 268
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v3, "MirrorService"

    const-string v4, "notify drag shadow failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270
    nop

    .end local v2    # "e":Landroid/os/RemoteException;
    goto :goto_0

    :goto_1
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 271
    throw v2

    .line 273
    .end local v0    # "ident":J
    :cond_0
    :goto_2
    return-void
.end method
