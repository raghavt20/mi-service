.class public final Lcom/xiaomi/mirror/service/MirrorService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MirrorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/mirror/service/MirrorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/xiaomi/mirror/service/MirrorService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 78
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 80
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->get()Lcom/xiaomi/mirror/service/MirrorService;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService$Lifecycle;->mService:Lcom/xiaomi/mirror/service/MirrorService;

    .line 81
    invoke-static {v0, p1}, Lcom/xiaomi/mirror/service/MirrorService;->-$$Nest$msystemReady(Lcom/xiaomi/mirror/service/MirrorService;Landroid/content/Context;)V

    .line 82
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 86
    const-string v0, "miui.mirror_service"

    iget-object v1, p0, Lcom/xiaomi/mirror/service/MirrorService$Lifecycle;->mService:Lcom/xiaomi/mirror/service/MirrorService;

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/mirror/service/MirrorService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 87
    return-void
.end method
