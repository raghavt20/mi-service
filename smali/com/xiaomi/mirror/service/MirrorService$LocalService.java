class com.xiaomi.mirror.service.MirrorService$LocalService extends com.xiaomi.mirror.service.MirrorServiceInternal {
	 /* .source "MirrorService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/mirror/service/MirrorService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "LocalService" */
} // .end annotation
/* # instance fields */
final com.xiaomi.mirror.service.MirrorService this$0; //synthetic
/* # direct methods */
private com.xiaomi.mirror.service.MirrorService$LocalService ( ) {
/* .locals 0 */
/* .line 802 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;-><init>()V */
return;
} // .end method
 com.xiaomi.mirror.service.MirrorService$LocalService ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/mirror/service/MirrorService$LocalService;-><init>(Lcom/xiaomi/mirror/service/MirrorService;)V */
return;
} // .end method
/* # virtual methods */
public Integer getDelegatePid ( ) {
/* .locals 1 */
/* .line 826 */
v0 = this.this$0;
v0 = (( com.xiaomi.mirror.service.MirrorService ) v0 ).getDelegatePid ( ); // invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getDelegatePid()I
} // .end method
public Boolean isNeedFinishAnimator ( ) {
/* .locals 1 */
/* .line 806 */
v0 = this.this$0;
v0 = (( com.xiaomi.mirror.service.MirrorService ) v0 ).isNeedFinishAnimator ( ); // invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->isNeedFinishAnimator()Z
} // .end method
public void notifyDragFinish ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "dragResult" # Z */
/* .line 821 */
v0 = this.this$0;
(( com.xiaomi.mirror.service.MirrorService ) v0 ).notifyDragFinish ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/xiaomi/mirror/service/MirrorService;->notifyDragFinish(Ljava/lang/String;Z)V
/* .line 822 */
return;
} // .end method
public void notifyDragResult ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "result" # Z */
/* .line 836 */
v0 = this.this$0;
(( com.xiaomi.mirror.service.MirrorService ) v0 ).notifyDragResult ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/mirror/service/MirrorService;->notifyDragResult(Z)V
/* .line 837 */
return;
} // .end method
public void notifyDragStart ( Integer p0, Integer p1, android.content.ClipData p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "sourceUid" # I */
/* .param p2, "sourcePid" # I */
/* .param p3, "clipData" # Landroid/content/ClipData; */
/* .param p4, "flags" # I */
/* .line 816 */
v0 = this.this$0;
(( com.xiaomi.mirror.service.MirrorService ) v0 ).notifyDragStart ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/xiaomi/mirror/service/MirrorService;->notifyDragStart(IILandroid/content/ClipData;I)V
/* .line 817 */
return;
} // .end method
public void notifyDragStart ( android.content.ClipData p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "data" # Landroid/content/ClipData; */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .param p4, "flag" # I */
/* .line 811 */
v0 = this.this$0;
(( com.xiaomi.mirror.service.MirrorService ) v0 ).notifyDragStart ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/xiaomi/mirror/service/MirrorService;->notifyDragStart(Landroid/content/ClipData;III)V
/* .line 812 */
return;
} // .end method
public void onDelegatePermissionReleased ( java.util.List p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/net/Uri;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 841 */
/* .local p1, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;" */
v0 = this.this$0;
(( com.xiaomi.mirror.service.MirrorService ) v0 ).onDelegatePermissionReleased ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/mirror/service/MirrorService;->onDelegatePermissionReleased(Ljava/util/List;)V
/* .line 842 */
return;
} // .end method
public Boolean tryToShareDrag ( java.lang.String p0, Integer p1, android.content.ClipData p2 ) {
/* .locals 1 */
/* .param p1, "targetPkg" # Ljava/lang/String; */
/* .param p2, "taskId" # I */
/* .param p3, "data" # Landroid/content/ClipData; */
/* .line 831 */
v0 = this.this$0;
v0 = (( com.xiaomi.mirror.service.MirrorService ) v0 ).tryToShareDrag ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/xiaomi/mirror/service/MirrorService;->tryToShareDrag(Ljava/lang/String;ILandroid/content/ClipData;)Z
} // .end method
