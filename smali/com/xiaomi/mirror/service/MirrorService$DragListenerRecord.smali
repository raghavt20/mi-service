.class final Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;
.super Ljava/lang/Object;
.source "MirrorService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/mirror/service/MirrorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DragListenerRecord"
.end annotation


# instance fields
.field private final mListener:Lcom/xiaomi/mirror/IDragListener;

.field private final mPid:I

.field final synthetic this$0:Lcom/xiaomi/mirror/service/MirrorService;


# direct methods
.method public constructor <init>(Lcom/xiaomi/mirror/service/MirrorService;ILcom/xiaomi/mirror/IDragListener;)V
    .locals 0
    .param p2, "mPid"    # I
    .param p3, "mListener"    # Lcom/xiaomi/mirror/IDragListener;

    .line 748
    iput-object p1, p0, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;->this$0:Lcom/xiaomi/mirror/service/MirrorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 749
    iput p2, p0, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;->mPid:I

    .line 750
    iput-object p3, p0, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;->mListener:Lcom/xiaomi/mirror/IDragListener;

    .line 751
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 2

    .line 755
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;->this$0:Lcom/xiaomi/mirror/service/MirrorService;

    iget v1, p0, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;->mPid:I

    invoke-static {v0, v1}, Lcom/xiaomi/mirror/service/MirrorService;->-$$Nest$monDragListenerDied(Lcom/xiaomi/mirror/service/MirrorService;I)V

    .line 756
    return-void
.end method

.method public notifyDragFinish(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "dragResult"    # Z

    .line 768
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;->mListener:Lcom/xiaomi/mirror/IDragListener;

    invoke-interface {v0, p1, p2}, Lcom/xiaomi/mirror/IDragListener;->onDragFinish(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 771
    goto :goto_0

    .line 769
    :catch_0
    move-exception v0

    .line 770
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {p0}, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;->binderDied()V

    .line 772
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public notifyDragStart(Landroid/content/ClipData;III)V
    .locals 6
    .param p1, "data"    # Landroid/content/ClipData;
    .param p2, "uid"    # I
    .param p3, "pid"    # I
    .param p4, "flag"    # I

    .line 760
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;->mListener:Lcom/xiaomi/mirror/IDragListener;

    iget-object v1, p0, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;->this$0:Lcom/xiaomi/mirror/service/MirrorService;

    invoke-static {v1}, Lcom/xiaomi/mirror/service/MirrorService;->-$$Nest$fgetmCurrentShadowImage(Lcom/xiaomi/mirror/service/MirrorService;)Landroid/graphics/Bitmap;

    move-result-object v5

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/xiaomi/mirror/IDragListener;->onDragStart(Landroid/content/ClipData;IIILandroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 763
    goto :goto_0

    .line 761
    :catch_0
    move-exception v0

    .line 762
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {p0}, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;->binderDied()V

    .line 764
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method
