.class Lcom/xiaomi/mirror/service/MirrorService$LocalService;
.super Lcom/xiaomi/mirror/service/MirrorServiceInternal;
.source "MirrorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/mirror/service/MirrorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/mirror/service/MirrorService;


# direct methods
.method private constructor <init>(Lcom/xiaomi/mirror/service/MirrorService;)V
    .locals 0

    .line 802
    iput-object p1, p0, Lcom/xiaomi/mirror/service/MirrorService$LocalService;->this$0:Lcom/xiaomi/mirror/service/MirrorService;

    invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/xiaomi/mirror/service/MirrorService;Lcom/xiaomi/mirror/service/MirrorService$LocalService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/mirror/service/MirrorService$LocalService;-><init>(Lcom/xiaomi/mirror/service/MirrorService;)V

    return-void
.end method


# virtual methods
.method public getDelegatePid()I
    .locals 1

    .line 826
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService$LocalService;->this$0:Lcom/xiaomi/mirror/service/MirrorService;

    invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getDelegatePid()I

    move-result v0

    return v0
.end method

.method public isNeedFinishAnimator()Z
    .locals 1

    .line 806
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService$LocalService;->this$0:Lcom/xiaomi/mirror/service/MirrorService;

    invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->isNeedFinishAnimator()Z

    move-result v0

    return v0
.end method

.method public notifyDragFinish(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "dragResult"    # Z

    .line 821
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService$LocalService;->this$0:Lcom/xiaomi/mirror/service/MirrorService;

    invoke-virtual {v0, p1, p2}, Lcom/xiaomi/mirror/service/MirrorService;->notifyDragFinish(Ljava/lang/String;Z)V

    .line 822
    return-void
.end method

.method public notifyDragResult(Z)V
    .locals 1
    .param p1, "result"    # Z

    .line 836
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService$LocalService;->this$0:Lcom/xiaomi/mirror/service/MirrorService;

    invoke-virtual {v0, p1}, Lcom/xiaomi/mirror/service/MirrorService;->notifyDragResult(Z)V

    .line 837
    return-void
.end method

.method public notifyDragStart(IILandroid/content/ClipData;I)V
    .locals 1
    .param p1, "sourceUid"    # I
    .param p2, "sourcePid"    # I
    .param p3, "clipData"    # Landroid/content/ClipData;
    .param p4, "flags"    # I

    .line 816
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService$LocalService;->this$0:Lcom/xiaomi/mirror/service/MirrorService;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/xiaomi/mirror/service/MirrorService;->notifyDragStart(IILandroid/content/ClipData;I)V

    .line 817
    return-void
.end method

.method public notifyDragStart(Landroid/content/ClipData;III)V
    .locals 1
    .param p1, "data"    # Landroid/content/ClipData;
    .param p2, "uid"    # I
    .param p3, "pid"    # I
    .param p4, "flag"    # I

    .line 811
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService$LocalService;->this$0:Lcom/xiaomi/mirror/service/MirrorService;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/xiaomi/mirror/service/MirrorService;->notifyDragStart(Landroid/content/ClipData;III)V

    .line 812
    return-void
.end method

.method public onDelegatePermissionReleased(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .line 841
    .local p1, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService$LocalService;->this$0:Lcom/xiaomi/mirror/service/MirrorService;

    invoke-virtual {v0, p1}, Lcom/xiaomi/mirror/service/MirrorService;->onDelegatePermissionReleased(Ljava/util/List;)V

    .line 842
    return-void
.end method

.method public tryToShareDrag(Ljava/lang/String;ILandroid/content/ClipData;)Z
    .locals 1
    .param p1, "targetPkg"    # Ljava/lang/String;
    .param p2, "taskId"    # I
    .param p3, "data"    # Landroid/content/ClipData;

    .line 831
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService$LocalService;->this$0:Lcom/xiaomi/mirror/service/MirrorService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/xiaomi/mirror/service/MirrorService;->tryToShareDrag(Ljava/lang/String;ILandroid/content/ClipData;)Z

    move-result v0

    return v0
.end method
