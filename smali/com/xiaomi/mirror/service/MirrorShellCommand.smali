.class public Lcom/xiaomi/mirror/service/MirrorShellCommand;
.super Landroid/os/ShellCommand;
.source "MirrorShellCommand.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V

    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)I
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;

    .line 10
    if-nez p1, :cond_0

    .line 11
    invoke-virtual {p0, p1}, Lcom/xiaomi/mirror/service/MirrorShellCommand;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 14
    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/mirror/service/MirrorShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 16
    .local v0, "pw":Ljava/io/PrintWriter;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    packed-switch v1, :pswitch_data_0

    :cond_1
    goto :goto_0

    :pswitch_0
    const-string v1, "reset-drag-and-drop"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    move v1, v2

    goto :goto_1

    :goto_0
    move v1, v3

    :goto_1
    packed-switch v1, :pswitch_data_1

    .line 23
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 24
    goto :goto_2

    .line 18
    :pswitch_1
    :try_start_1
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->get()Lcom/xiaomi/mirror/service/MirrorService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/mirror/service/MirrorService;->resetDragAndDropController()V

    .line 19
    const-string v1, "Reset successfully"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 20
    nop

    .line 23
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 20
    return v2

    .line 25
    :goto_2
    return v3

    .line 23
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 24
    throw v1

    :pswitch_data_0
    .packed-switch -0x153b1260
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public onHelp()V
    .locals 2

    .line 30
    invoke-virtual {p0}, Lcom/xiaomi/mirror/service/MirrorShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 31
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string v1, "No help."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 32
    return-void
.end method
