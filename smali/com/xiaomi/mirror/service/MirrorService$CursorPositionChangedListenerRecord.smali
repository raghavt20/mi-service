.class final Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;
.super Ljava/lang/Object;
.source "MirrorService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/mirror/service/MirrorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CursorPositionChangedListenerRecord"
.end annotation


# instance fields
.field private final mListener:Lcom/xiaomi/mirror/ICursorPositionChangedListener;

.field private final mPid:I

.field final synthetic this$0:Lcom/xiaomi/mirror/service/MirrorService;


# direct methods
.method public constructor <init>(Lcom/xiaomi/mirror/service/MirrorService;ILcom/xiaomi/mirror/ICursorPositionChangedListener;)V
    .locals 0
    .param p2, "mPid"    # I
    .param p3, "mListener"    # Lcom/xiaomi/mirror/ICursorPositionChangedListener;

    .line 622
    iput-object p1, p0, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;->this$0:Lcom/xiaomi/mirror/service/MirrorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623
    iput p2, p0, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;->mPid:I

    .line 624
    iput-object p3, p0, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;->mListener:Lcom/xiaomi/mirror/ICursorPositionChangedListener;

    .line 625
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 2

    .line 629
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;->this$0:Lcom/xiaomi/mirror/service/MirrorService;

    iget v1, p0, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;->mPid:I

    invoke-static {v0, v1}, Lcom/xiaomi/mirror/service/MirrorService;->-$$Nest$monCursorPositionChangedListenerDied(Lcom/xiaomi/mirror/service/MirrorService;I)V

    .line 630
    return-void
.end method

.method public notifyCursorPositionChanged(IFF)V
    .locals 1
    .param p1, "deviceId"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F

    .line 634
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;->mListener:Lcom/xiaomi/mirror/ICursorPositionChangedListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/xiaomi/mirror/ICursorPositionChangedListener;->onCursorPositionChanged(IFF)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 637
    goto :goto_0

    .line 635
    :catch_0
    move-exception v0

    .line 636
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {p0}, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;->binderDied()V

    .line 638
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method
