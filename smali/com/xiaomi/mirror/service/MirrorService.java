public class com.xiaomi.mirror.service.MirrorService extends com.xiaomi.mirror.IMirrorService$Stub {
	 /* .source "MirrorService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/mirror/service/MirrorService$LocalService;, */
	 /* Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;, */
	 /* Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;, */
	 /* Lcom/xiaomi/mirror/service/MirrorService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static java.lang.ThreadLocal sAllowGrant;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ThreadLocal<", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static volatile com.xiaomi.mirror.service.MirrorService sInstance;
private static final java.lang.Object sLock;
/* # instance fields */
private android.app.IActivityTaskManager mAtms;
private android.os.RemoteCallbackList mCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/os/RemoteCallbackList<", */
/* "Lcom/xiaomi/mirror/IMirrorStateListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private android.graphics.Bitmap mCurrentShadowImage;
private final android.util.SparseArray mCursorPositionChangedListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.miui.server.input.MiuiCursorPositionListenerService$OnCursorPositionChangedListener mCursorPositionListener;
private final android.os.IBinder$DeathRecipient mDeathRecipient;
private com.xiaomi.mirror.IMirrorDelegate mDelegate;
private java.lang.String mDelegatePackageName;
private Integer mDelegatePid;
private Integer mDelegateUid;
private Boolean mDragAcceptable;
private final android.util.SparseArray mDragListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mIsInMouseShareMode;
private Integer mLastNotifiedUid;
private com.android.server.wm.MiuiMirrorDragDropController mMiuiMirrorDragDropController;
private Boolean mNeedFinishAnimator;
private android.os.IBinder mPermissionOwner;
private Boolean mSystemReady;
private final java.util.ArrayList mTempCursorPositionChangedListenersToNotify;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.ArrayList mTempDragListenersToNotify;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.app.IUriGrantsManager mUgm;
private com.android.server.uri.UriGrantsManagerInternal mUgmInternal;
/* # direct methods */
public static void $r8$lambda$40IgGNuL8A02JN3QWxAKmHz_VbE ( com.xiaomi.mirror.service.MirrorService p0, Integer p1, Float p2, Float p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/mirror/service/MirrorService;->deliverCursorPositionChanged(IFF)V */
return;
} // .end method
public static void $r8$lambda$8otWs1Mx74AE1TBuPWyNcg4jJzI ( com.xiaomi.mirror.service.MirrorService p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorService;->delegateLost()V */
return;
} // .end method
static android.graphics.Bitmap -$$Nest$fgetmCurrentShadowImage ( com.xiaomi.mirror.service.MirrorService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCurrentShadowImage;
} // .end method
static void -$$Nest$monCursorPositionChangedListenerDied ( com.xiaomi.mirror.service.MirrorService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/mirror/service/MirrorService;->onCursorPositionChangedListenerDied(I)V */
return;
} // .end method
static void -$$Nest$monDragListenerDied ( com.xiaomi.mirror.service.MirrorService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/mirror/service/MirrorService;->onDragListenerDied(I)V */
return;
} // .end method
static void -$$Nest$msystemReady ( com.xiaomi.mirror.service.MirrorService p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/mirror/service/MirrorService;->systemReady(Landroid/content/Context;)V */
return;
} // .end method
static com.xiaomi.mirror.service.MirrorService ( ) {
/* .locals 1 */
/* .line 52 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
/* .line 323 */
/* new-instance v0, Lcom/xiaomi/mirror/service/MirrorService$1; */
/* invoke-direct {v0}, Lcom/xiaomi/mirror/service/MirrorService$1;-><init>()V */
return;
} // .end method
public com.xiaomi.mirror.service.MirrorService ( ) {
/* .locals 1 */
/* .line 48 */
/* invoke-direct {p0}, Lcom/xiaomi/mirror/IMirrorService$Stub;-><init>()V */
/* .line 61 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mCallbacks = v0;
/* .line 64 */
/* new-instance v0, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda1; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda1;-><init>(Lcom/xiaomi/mirror/service/MirrorService;)V */
this.mDeathRecipient = v0;
/* .line 72 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z */
/* .line 520 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mCursorPositionChangedListeners = v0;
/* .line 522 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mTempCursorPositionChangedListenersToNotify = v0;
/* .line 651 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mNeedFinishAnimator:Z */
/* .line 662 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mDragListeners = v0;
/* .line 663 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mTempDragListenersToNotify = v0;
return;
} // .end method
private void broadcastDelegateStateChanged ( ) {
/* .locals 5 */
/* .line 344 */
v0 = this.mDelegate;
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 345 */
/* .local v0, "hasDelegate":Z */
} // :goto_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v2 */
/* .line 347 */
/* .local v2, "ident":J */
try { // :try_start_0
v4 = this.mCallbacks;
v4 = (( android.os.RemoteCallbackList ) v4 ).beginBroadcast ( ); // invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* sub-int/2addr v4, v1 */
/* .local v4, "i":I */
} // :goto_1
/* if-ltz v4, :cond_1 */
/* .line 349 */
try { // :try_start_1
v1 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v1 ).getBroadcastItem ( v4 ); // invoke-virtual {v1, v4}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v1, Lcom/xiaomi/mirror/IMirrorStateListener; */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 352 */
/* .line 350 */
/* :catch_0 */
/* move-exception v1 */
/* .line 347 */
} // :goto_2
/* add-int/lit8 v4, v4, -0x1 */
/* .line 354 */
} // .end local v4 # "i":I
} // :cond_1
try { // :try_start_2
v1 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v1 ).finishBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 356 */
android.os.Binder .restoreCallingIdentity ( v2,v3 );
/* .line 357 */
/* nop */
/* .line 358 */
return;
/* .line 356 */
/* :catchall_0 */
/* move-exception v1 */
android.os.Binder .restoreCallingIdentity ( v2,v3 );
/* .line 357 */
/* throw v1 */
} // .end method
private void delegateLost ( ) {
/* .locals 2 */
/* .line 361 */
int v0 = 0; // const/4 v0, 0x0
this.mDelegate = v0;
/* .line 362 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegateUid:I */
/* .line 363 */
/* iput v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegatePid:I */
/* .line 364 */
this.mDelegatePackageName = v0;
/* .line 365 */
v0 = this.mMiuiMirrorDragDropController;
(( com.android.server.wm.MiuiMirrorDragDropController ) v0 ).shutdownDragAndDropIfNeeded ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMirrorDragDropController;->shutdownDragAndDropIfNeeded()V
/* .line 366 */
/* invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorService;->broadcastDelegateStateChanged()V */
/* .line 367 */
/* invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorService;->resetSystemProperties()V */
/* .line 368 */
return;
} // .end method
private void deliverCursorPositionChanged ( Integer p0, Float p1, Float p2 ) {
/* .locals 5 */
/* .param p1, "deviceId" # I */
/* .param p2, "x" # F */
/* .param p3, "y" # F */
/* .line 603 */
/* iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mIsInMouseShareMode:Z */
/* if-nez v0, :cond_0 */
/* .line 604 */
return;
/* .line 606 */
} // :cond_0
v0 = this.mTempCursorPositionChangedListenersToNotify;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 607 */
v0 = this.mCursorPositionChangedListeners;
/* monitor-enter v0 */
/* .line 608 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
try { // :try_start_0
v2 = this.mCursorPositionChangedListeners;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* .local v2, "size":I */
} // :goto_0
/* if-ge v1, v2, :cond_1 */
/* .line 609 */
v3 = this.mTempCursorPositionChangedListenersToNotify;
v4 = this.mCursorPositionChangedListeners;
/* .line 610 */
(( android.util.SparseArray ) v4 ).valueAt ( v1 ); // invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord; */
(( java.util.ArrayList ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 608 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 612 */
} // .end local v1 # "i":I
} // .end local v2 # "size":I
} // :cond_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 613 */
v0 = this.mTempCursorPositionChangedListenersToNotify;
/* new-instance v1, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p1, p2, p3}, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda0;-><init>(IFF)V */
(( java.util.ArrayList ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->forEach(Ljava/util/function/Consumer;)V
/* .line 615 */
return;
/* .line 612 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public static com.xiaomi.mirror.service.MirrorService get ( ) {
/* .locals 2 */
/* .line 91 */
v0 = com.xiaomi.mirror.service.MirrorService.sInstance;
/* if-nez v0, :cond_1 */
/* .line 92 */
v0 = com.xiaomi.mirror.service.MirrorService.sLock;
/* monitor-enter v0 */
/* .line 93 */
try { // :try_start_0
v1 = com.xiaomi.mirror.service.MirrorService.sInstance;
/* if-nez v1, :cond_0 */
/* .line 94 */
/* new-instance v1, Lcom/xiaomi/mirror/service/MirrorService; */
/* invoke-direct {v1}, Lcom/xiaomi/mirror/service/MirrorService;-><init>()V */
/* .line 96 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 98 */
} // :cond_1
} // :goto_0
v0 = com.xiaomi.mirror.service.MirrorService.sInstance;
} // .end method
private void grantItem ( android.content.ClipData$Item p0, Integer p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 8 */
/* .param p1, "item" # Landroid/content/ClipData$Item; */
/* .param p2, "sourceUid" # I */
/* .param p3, "targetPkg" # Ljava/lang/String; */
/* .param p4, "targetUserId" # I */
/* .param p5, "mode" # I */
/* .line 460 */
(( android.content.ClipData$Item ) p1 ).getUri ( ); // invoke-virtual {p1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 461 */
(( android.content.ClipData$Item ) p1 ).getUri ( ); // invoke-virtual {p1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;
/* move-object v1, p0 */
/* move v3, p2 */
/* move-object v4, p3 */
/* move v5, p4 */
/* move v6, p5 */
/* invoke-direct/range {v1 ..v6}, Lcom/xiaomi/mirror/service/MirrorService;->grantUri(Landroid/net/Uri;ILjava/lang/String;II)V */
/* .line 463 */
} // :cond_0
(( android.content.ClipData$Item ) p1 ).getIntent ( ); // invoke-virtual {p1}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;
/* .line 464 */
/* .local v0, "intent":Landroid/content/Intent; */
if ( v0 != null) { // if-eqz v0, :cond_1
(( android.content.Intent ) v0 ).getData ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 465 */
(( android.content.Intent ) v0 ).getData ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;
/* move-object v2, p0 */
/* move v4, p2 */
/* move-object v5, p3 */
/* move v6, p4 */
/* move v7, p5 */
/* invoke-direct/range {v2 ..v7}, Lcom/xiaomi/mirror/service/MirrorService;->grantUri(Landroid/net/Uri;ILjava/lang/String;II)V */
/* .line 467 */
} // :cond_1
return;
} // .end method
private void grantPermissions ( Integer p0, android.content.ClipData p1, Integer p2 ) {
/* .locals 9 */
/* .param p1, "sourceUid" # I */
/* .param p2, "data" # Landroid/content/ClipData; */
/* .param p3, "flags" # I */
/* .line 446 */
/* and-int/lit16 v6, p3, 0xc3 */
/* .line 451 */
/* .local v6, "mode":I */
v7 = (( android.content.ClipData ) p2 ).getItemCount ( ); // invoke-virtual {p2}, Landroid/content/ClipData;->getItemCount()I
/* .line 452 */
/* .local v7, "N":I */
int v0 = 0; // const/4 v0, 0x0
/* move v8, v0 */
/* .local v8, "i":I */
} // :goto_0
/* if-ge v8, v7, :cond_0 */
/* .line 453 */
(( android.content.ClipData ) p2 ).getItemAt ( v8 ); // invoke-virtual {p2, v8}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;
v3 = this.mDelegatePackageName;
/* iget v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegateUid:I */
/* .line 454 */
v4 = android.os.UserHandle .getUserId ( v0 );
/* .line 453 */
/* move-object v0, p0 */
/* move v2, p1 */
/* move v5, v6 */
/* invoke-direct/range {v0 ..v5}, Lcom/xiaomi/mirror/service/MirrorService;->grantItem(Landroid/content/ClipData$Item;ILjava/lang/String;II)V */
/* .line 452 */
/* add-int/lit8 v8, v8, 0x1 */
/* .line 456 */
} // .end local v8 # "i":I
} // :cond_0
return;
} // .end method
private void grantUri ( android.net.Uri p0, Integer p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 14 */
/* .param p1, "uri" # Landroid/net/Uri; */
/* .param p2, "sourceUid" # I */
/* .param p3, "targetPkg" # Ljava/lang/String; */
/* .param p4, "targetUserId" # I */
/* .param p5, "mode" # I */
/* .line 470 */
/* move-object v1, p0 */
/* move-object v2, p1 */
if ( v2 != null) { // if-eqz v2, :cond_1
final String v0 = "content"; // const-string v0, "content"
(( android.net.Uri ) p1 ).getScheme ( ); // invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 472 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v3 */
/* .line 473 */
/* .local v3, "ident":J */
int v0 = 1; // const/4 v0, 0x1
(( com.xiaomi.mirror.service.MirrorService ) p0 ).setAllowGrant ( v0 ); // invoke-virtual {p0, v0}, Lcom/xiaomi/mirror/service/MirrorService;->setAllowGrant(Z)V
/* .line 475 */
int v5 = 0; // const/4 v5, 0x0
try { // :try_start_0
v6 = this.mUgm;
v7 = this.mPermissionOwner;
/* .line 476 */
android.content.ContentProvider .getUriWithoutUserId ( p1 );
/* .line 478 */
v0 = /* invoke-static/range {p2 ..p2}, Landroid/os/UserHandle;->getUserId(I)I */
v12 = android.content.ContentProvider .getUserIdFromUri ( p1,v0 );
/* .line 475 */
/* move/from16 v8, p2 */
/* move-object/from16 v9, p3 */
/* move/from16 v11, p5 */
/* move/from16 v13, p4 */
/* invoke-interface/range {v6 ..v13}, Landroid/app/IUriGrantsManager;->grantUriPermissionFromOwner(Landroid/os/IBinder;ILjava/lang/String;Landroid/net/Uri;III)V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 483 */
/* :catchall_0 */
/* move-exception v0 */
(( com.xiaomi.mirror.service.MirrorService ) p0 ).setAllowGrant ( v5 ); // invoke-virtual {p0, v5}, Lcom/xiaomi/mirror/service/MirrorService;->setAllowGrant(Z)V
/* .line 484 */
android.os.Binder .restoreCallingIdentity ( v3,v4 );
/* .line 485 */
/* throw v0 */
/* .line 480 */
/* :catch_0 */
/* move-exception v0 */
/* .line 483 */
} // :goto_0
(( com.xiaomi.mirror.service.MirrorService ) p0 ).setAllowGrant ( v5 ); // invoke-virtual {p0, v5}, Lcom/xiaomi/mirror/service/MirrorService;->setAllowGrant(Z)V
/* .line 484 */
android.os.Binder .restoreCallingIdentity ( v3,v4 );
/* .line 485 */
/* nop */
/* .line 486 */
return;
/* .line 470 */
} // .end local v3 # "ident":J
} // :cond_1
} // :goto_1
return;
} // .end method
static void lambda$deliverCursorPositionChanged$0 ( Integer p0, Float p1, Float p2, com.xiaomi.mirror.service.MirrorService$CursorPositionChangedListenerRecord p3 ) { //synthethic
/* .locals 0 */
/* .param p0, "deviceId" # I */
/* .param p1, "x" # F */
/* .param p2, "y" # F */
/* .param p3, "cursorPositionChangedListenerRecord" # Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord; */
/* .line 614 */
(( com.xiaomi.mirror.service.MirrorService$CursorPositionChangedListenerRecord ) p3 ).notifyCursorPositionChanged ( p0, p1, p2 ); // invoke-virtual {p3, p0, p1, p2}, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;->notifyCursorPositionChanged(IFF)V
return;
} // .end method
static void lambda$notifyDragFinish$2 ( java.lang.String p0, Boolean p1, com.xiaomi.mirror.service.MirrorService$DragListenerRecord p2 ) { //synthethic
/* .locals 0 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .param p1, "dragResult" # Z */
/* .param p2, "dragListenerRecord" # Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord; */
/* .line 740 */
(( com.xiaomi.mirror.service.MirrorService$DragListenerRecord ) p2 ).notifyDragFinish ( p0, p1 ); // invoke-virtual {p2, p0, p1}, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;->notifyDragFinish(Ljava/lang/String;Z)V
return;
} // .end method
static void lambda$notifyDragStart$1 ( android.content.ClipData p0, Integer p1, Integer p2, Integer p3, com.xiaomi.mirror.service.MirrorService$DragListenerRecord p4 ) { //synthethic
/* .locals 0 */
/* .param p0, "data" # Landroid/content/ClipData; */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "flag" # I */
/* .param p4, "dragListenerRecord" # Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord; */
/* .line 726 */
(( com.xiaomi.mirror.service.MirrorService$DragListenerRecord ) p4 ).notifyDragStart ( p0, p1, p2, p3 ); // invoke-virtual {p4, p0, p1, p2, p3}, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;->notifyDragStart(Landroid/content/ClipData;III)V
return;
} // .end method
private void onCursorPositionChangedListenerDied ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 597 */
v0 = this.mCursorPositionChangedListeners;
/* monitor-enter v0 */
/* .line 598 */
try { // :try_start_0
v1 = this.mCursorPositionChangedListeners;
(( android.util.SparseArray ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V
/* .line 599 */
/* monitor-exit v0 */
/* .line 600 */
return;
/* .line 599 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void onDragListenerDied ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 709 */
v0 = this.mDragListeners;
/* monitor-enter v0 */
/* .line 710 */
try { // :try_start_0
v1 = this.mDragListeners;
(( android.util.SparseArray ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V
/* .line 711 */
/* monitor-exit v0 */
/* .line 712 */
return;
/* .line 711 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void populateCursorPositionListenerLocked ( ) {
/* .locals 2 */
/* .line 556 */
v0 = this.mCursorPositionListener;
/* if-nez v0, :cond_1 */
v0 = this.mCursorPositionChangedListeners;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* if-nez v0, :cond_0 */
/* .line 559 */
} // :cond_0
final String v0 = "MirrorService"; // const-string v0, "MirrorService"
final String v1 = "register cursor position listener add to MiuiCursorPositionListenerService"; // const-string v1, "register cursor position listener add to MiuiCursorPositionListenerService"
android.util.Slog .d ( v0,v1 );
/* .line 560 */
/* new-instance v0, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda4; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda4;-><init>(Lcom/xiaomi/mirror/service/MirrorService;)V */
this.mCursorPositionListener = v0;
/* .line 561 */
com.miui.server.input.MiuiCursorPositionListenerService .getInstance ( );
v1 = this.mCursorPositionListener;
/* .line 562 */
(( com.miui.server.input.MiuiCursorPositionListenerService ) v0 ).registerOnCursorPositionChangedListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/MiuiCursorPositionListenerService;->registerOnCursorPositionChangedListener(Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;)V
/* .line 563 */
return;
/* .line 557 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void resetSystemProperties ( ) {
/* .locals 3 */
/* .line 371 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "miui_mirror_dnd_mode"; // const-string v1, "miui_mirror_dnd_mode"
int v2 = 0; // const/4 v2, 0x0
android.provider.Settings$Secure .putInt ( v0,v1,v2 );
/* .line 372 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "screen_project_in_screening" */
android.provider.Settings$Secure .putInt ( v0,v1,v2 );
/* .line 373 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "screen_project_hang_up_on" */
android.provider.Settings$Secure .putInt ( v0,v1,v2 );
/* .line 374 */
return;
} // .end method
private void revokeItem ( android.content.ClipData$Item p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "item" # Landroid/content/ClipData$Item; */
/* .param p2, "sourceUid" # I */
/* .line 496 */
(( android.content.ClipData$Item ) p1 ).getUri ( ); // invoke-virtual {p1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 497 */
(( android.content.ClipData$Item ) p1 ).getUri ( ); // invoke-virtual {p1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;
/* invoke-direct {p0, v0, p2}, Lcom/xiaomi/mirror/service/MirrorService;->revokeUri(Landroid/net/Uri;I)V */
/* .line 499 */
} // :cond_0
(( android.content.ClipData$Item ) p1 ).getIntent ( ); // invoke-virtual {p1}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;
/* .line 500 */
/* .local v0, "intent":Landroid/content/Intent; */
if ( v0 != null) { // if-eqz v0, :cond_1
(( android.content.Intent ) v0 ).getData ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 501 */
(( android.content.Intent ) v0 ).getData ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;
/* invoke-direct {p0, v1, p2}, Lcom/xiaomi/mirror/service/MirrorService;->revokeUri(Landroid/net/Uri;I)V */
/* .line 503 */
} // :cond_1
return;
} // .end method
private void revokePermissionsInternal ( android.content.ClipData p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "data" # Landroid/content/ClipData; */
/* .param p2, "sourceUid" # I */
/* .line 489 */
v0 = (( android.content.ClipData ) p1 ).getItemCount ( ); // invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I
/* .line 490 */
/* .local v0, "N":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_0 */
/* .line 491 */
(( android.content.ClipData ) p1 ).getItemAt ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;
/* invoke-direct {p0, v2, p2}, Lcom/xiaomi/mirror/service/MirrorService;->revokeItem(Landroid/content/ClipData$Item;I)V */
/* .line 490 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 493 */
} // .end local v1 # "i":I
} // :cond_0
return;
} // .end method
private void revokeUri ( android.net.Uri p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "uri" # Landroid/net/Uri; */
/* .param p2, "sourceUid" # I */
/* .line 506 */
if ( p1 != null) { // if-eqz p1, :cond_1
final String v0 = "content"; // const-string v0, "content"
(( android.net.Uri ) p1 ).getScheme ( ); // invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 508 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 510 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = this.mUgmInternal;
v3 = this.mPermissionOwner;
/* .line 511 */
android.content.ContentProvider .getUriWithoutUserId ( p1 );
/* .line 513 */
v5 = android.os.UserHandle .getUserId ( p2 );
v5 = android.content.ContentProvider .getUserIdFromUri ( p1,v5 );
/* .line 510 */
int v6 = 1; // const/4 v6, 0x1
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 515 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 516 */
/* nop */
/* .line 517 */
return;
/* .line 515 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 516 */
/* throw v2 */
/* .line 506 */
} // .end local v0 # "ident":J
} // :cond_1
} // :goto_0
return;
} // .end method
private void systemReady ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 102 */
this.mContext = p1;
/* .line 103 */
android.app.UriGrantsManager .getService ( );
this.mUgm = v0;
/* .line 104 */
/* const-class v0, Lcom/android/server/uri/UriGrantsManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/uri/UriGrantsManagerInternal; */
this.mUgmInternal = v0;
/* .line 105 */
final String v1 = "mirror"; // const-string v1, "mirror"
this.mPermissionOwner = v0;
/* .line 106 */
/* new-instance v0, Lcom/android/server/wm/MiuiMirrorDragDropController; */
/* invoke-direct {v0}, Lcom/android/server/wm/MiuiMirrorDragDropController;-><init>()V */
this.mMiuiMirrorDragDropController = v0;
/* .line 107 */
android.app.ActivityTaskManager .getService ( );
this.mAtms = v0;
/* .line 108 */
/* const-class v0, Lcom/xiaomi/mirror/service/MirrorServiceInternal; */
/* new-instance v1, Lcom/xiaomi/mirror/service/MirrorService$LocalService; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/xiaomi/mirror/service/MirrorService$LocalService;-><init>(Lcom/xiaomi/mirror/service/MirrorService;Lcom/xiaomi/mirror/service/MirrorService$LocalService-IA;)V */
com.android.server.LocalServices .addService ( v0,v1 );
/* .line 109 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z */
/* .line 110 */
return;
} // .end method
private void unPopulateCursorPositionListenerLocked ( ) {
/* .locals 2 */
/* .line 586 */
v0 = this.mCursorPositionListener;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mCursorPositionChangedListeners;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* if-lez v0, :cond_0 */
/* .line 589 */
} // :cond_0
final String v0 = "MirrorService"; // const-string v0, "MirrorService"
/* const-string/jumbo v1, "unregister cursor position listener from MiuiCursorPositionListenerService" */
android.util.Slog .d ( v0,v1 );
/* .line 590 */
com.miui.server.input.MiuiCursorPositionListenerService .getInstance ( );
v1 = this.mCursorPositionListener;
/* .line 591 */
(( com.miui.server.input.MiuiCursorPositionListenerService ) v0 ).unregisterOnCursorPositionChangedListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/MiuiCursorPositionListenerService;->unregisterOnCursorPositionChangedListener(Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;)V
/* .line 592 */
int v0 = 0; // const/4 v0, 0x0
this.mCursorPositionListener = v0;
/* .line 593 */
return;
/* .line 587 */
} // :cond_1
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void addMirrorStateListener ( com.xiaomi.mirror.IMirrorStateListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lcom/xiaomi/mirror/IMirrorStateListener; */
/* .line 142 */
v0 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v0 ).register ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
/* .line 144 */
try { // :try_start_0
v0 = this.mDelegate;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 146 */
/* .line 145 */
/* :catch_0 */
/* move-exception v0 */
/* .line 147 */
} // :goto_1
return;
} // .end method
public Boolean cancelCurrentDrag ( ) {
/* .locals 1 */
/* .line 648 */
v0 = com.android.server.wm.DragDropControllerStub .get ( );
} // .end method
public void cancelDragAndDrop ( android.os.IBinder p0 ) {
/* .locals 3 */
/* .param p1, "dragToken" # Landroid/os/IBinder; */
/* .line 253 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 255 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = this.mMiuiMirrorDragDropController;
(( com.android.server.wm.MiuiMirrorDragDropController ) v2 ).cancelDragAndDrop ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/wm/MiuiMirrorDragDropController;->cancelDragAndDrop(Landroid/os/IBinder;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 257 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 258 */
/* nop */
/* .line 259 */
return;
/* .line 257 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 258 */
/* throw v2 */
} // .end method
public Boolean getAllowGrant ( ) {
/* .locals 1 */
/* .line 337 */
/* iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 338 */
v0 = com.xiaomi.mirror.service.MirrorService.sAllowGrant;
(( java.lang.ThreadLocal ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 340 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String getDelegatePackageName ( ) {
/* .locals 1 */
/* .line 134 */
/* iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 135 */
v0 = this.mDelegatePackageName;
/* .line 137 */
} // :cond_0
final String v0 = ""; // const-string v0, ""
} // .end method
public Integer getDelegatePid ( ) {
/* .locals 1 */
/* .line 127 */
/* iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 128 */
/* iget v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegatePid:I */
/* .line 130 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public com.android.server.wm.MiuiMirrorDragDropController getDragDropController ( ) {
/* .locals 1 */
/* .line 117 */
v0 = this.mMiuiMirrorDragDropController;
} // .end method
public Boolean getSystemReady ( ) {
/* .locals 1 */
/* .line 113 */
/* iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z */
} // .end method
public void grantUriPermissionsToPackage ( java.util.List p0, Integer p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 8 */
/* .param p2, "sourceUid" # I */
/* .param p3, "targetPkg" # Ljava/lang/String; */
/* .param p4, "targetUserId" # I */
/* .param p5, "mode" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/net/Uri;", */
/* ">;I", */
/* "Ljava/lang/String;", */
/* "II)V" */
/* } */
} // .end annotation
/* .line 195 */
/* .local p1, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;" */
v0 = this.mContext;
final String v1 = "com.xiaomi.mirror.permission.REGISTER_MIRROR_DELEGATE"; // const-string v1, "com.xiaomi.mirror.permission.REGISTER_MIRROR_DELEGATE"
final String v2 = "grantUriPermissionsToPackage"; // const-string v2, "grantUriPermissionsToPackage"
(( android.content.Context ) v0 ).enforceCallingPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 197 */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Landroid/net/Uri; */
/* .line 198 */
/* .local v1, "uri":Landroid/net/Uri; */
/* move-object v2, p0 */
/* move-object v3, v1 */
/* move v4, p2 */
/* move-object v5, p3 */
/* move v6, p4 */
/* move v7, p5 */
/* invoke-direct/range {v2 ..v7}, Lcom/xiaomi/mirror/service/MirrorService;->grantUri(Landroid/net/Uri;ILjava/lang/String;II)V */
/* .line 199 */
} // .end local v1 # "uri":Landroid/net/Uri;
/* .line 200 */
} // :cond_0
return;
} // .end method
public void injectDragEvent ( Integer p0, Integer p1, Float p2, Float p3 ) {
/* .locals 3 */
/* .param p1, "action" # I */
/* .param p2, "displayId" # I */
/* .param p3, "newX" # F */
/* .param p4, "newY" # F */
/* .line 232 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 234 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = this.mMiuiMirrorDragDropController;
(( com.android.server.wm.MiuiMirrorDragDropController ) v2 ).injectDragEvent ( p1, p2, p3, p4 ); // invoke-virtual {v2, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiMirrorDragDropController;->injectDragEvent(IIFF)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 236 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 237 */
/* nop */
/* .line 238 */
return;
/* .line 236 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 237 */
/* throw v2 */
} // .end method
public Boolean isInMouseShareMode ( ) {
/* .locals 1 */
/* .line 799 */
/* iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mIsInMouseShareMode:Z */
} // .end method
public Boolean isNeedFinishAnimator ( ) {
/* .locals 1 */
/* .line 659 */
/* iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mNeedFinishAnimator:Z */
} // .end method
public void notifyDragFinish ( java.lang.String p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "dragResult" # Z */
/* .line 730 */
/* iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mIsInMouseShareMode:Z */
/* if-nez v0, :cond_0 */
/* .line 731 */
return;
/* .line 733 */
} // :cond_0
v0 = this.mTempDragListenersToNotify;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 734 */
v0 = this.mDragListeners;
/* monitor-enter v0 */
/* .line 735 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
try { // :try_start_0
v2 = this.mDragListeners;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* .local v2, "size":I */
} // :goto_0
/* if-ge v1, v2, :cond_1 */
/* .line 736 */
v3 = this.mTempDragListenersToNotify;
v4 = this.mDragListeners;
(( android.util.SparseArray ) v4 ).valueAt ( v1 ); // invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord; */
(( java.util.ArrayList ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 735 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 738 */
} // .end local v1 # "i":I
} // .end local v2 # "size":I
} // :cond_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 739 */
v0 = this.mTempDragListenersToNotify;
/* new-instance v1, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p1, p2}, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda3;-><init>(Ljava/lang/String;Z)V */
(( java.util.ArrayList ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->forEach(Ljava/util/function/Consumer;)V
/* .line 741 */
return;
/* .line 738 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public void notifyDragResult ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "result" # Z */
/* .line 405 */
v0 = this.mDelegate;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 406 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 408 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = this.mDelegate;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 412 */
/* nop */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 413 */
/* .line 412 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 409 */
/* :catch_0 */
/* move-exception v2 */
/* .line 410 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_1
final String v3 = "MirrorService"; // const-string v3, "MirrorService"
final String v4 = "notify drag cancel failed"; // const-string v4, "notify drag cancel failed"
android.util.Slog .w ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 412 */
/* nop */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 413 */
/* throw v2 */
/* .line 415 */
} // .end local v0 # "ident":J
} // :cond_0
} // :goto_2
return;
} // .end method
public void notifyDragStart ( Integer p0, Integer p1, android.content.ClipData p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "sourceUid" # I */
/* .param p2, "sourcePid" # I */
/* .param p3, "clipData" # Landroid/content/ClipData; */
/* .param p4, "flags" # I */
/* .line 377 */
v0 = this.mDelegate;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 378 */
/* invoke-direct {p0, p1, p3, p4}, Lcom/xiaomi/mirror/service/MirrorService;->grantPermissions(ILandroid/content/ClipData;I)V */
/* .line 379 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 381 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = this.mDelegate;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 385 */
/* nop */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 386 */
/* .line 385 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 382 */
/* :catch_0 */
/* move-exception v2 */
/* .line 383 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_1
final String v3 = "MirrorService"; // const-string v3, "MirrorService"
final String v4 = "notify drag start failed"; // const-string v4, "notify drag start failed"
android.util.Slog .w ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 385 */
/* nop */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 386 */
/* throw v2 */
/* .line 388 */
} // .end local v0 # "ident":J
} // :cond_0
} // :goto_2
return;
} // .end method
public void notifyDragStart ( android.content.ClipData p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "data" # Landroid/content/ClipData; */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .param p4, "flag" # I */
/* .line 715 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mNeedFinishAnimator:Z */
/* .line 716 */
/* iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mIsInMouseShareMode:Z */
/* if-nez v0, :cond_0 */
/* .line 717 */
return;
/* .line 719 */
} // :cond_0
v0 = this.mTempDragListenersToNotify;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 720 */
v0 = this.mDragListeners;
/* monitor-enter v0 */
/* .line 721 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
try { // :try_start_0
v2 = this.mDragListeners;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* .local v2, "size":I */
} // :goto_0
/* if-ge v1, v2, :cond_1 */
/* .line 722 */
v3 = this.mTempDragListenersToNotify;
v4 = this.mDragListeners;
(( android.util.SparseArray ) v4 ).valueAt ( v1 ); // invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord; */
(( java.util.ArrayList ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 721 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 724 */
} // .end local v1 # "i":I
} // .end local v2 # "size":I
} // :cond_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 725 */
v0 = this.mTempDragListenersToNotify;
/* new-instance v1, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p1, p2, p3, p4}, Lcom/xiaomi/mirror/service/MirrorService$$ExternalSyntheticLambda2;-><init>(Landroid/content/ClipData;III)V */
(( java.util.ArrayList ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->forEach(Ljava/util/function/Consumer;)V
/* .line 727 */
return;
/* .line 724 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public void notifyMouseShareModeState ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "isInMouseShareMode" # Z */
/* .line 782 */
/* iput-boolean p1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mIsInMouseShareMode:Z */
/* .line 783 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 785 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = this.mCallbacks;
v2 = (( android.os.RemoteCallbackList ) v2 ).beginBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_0 */
/* .line 787 */
try { // :try_start_1
v3 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Lcom/xiaomi/mirror/IMirrorStateListener; */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 790 */
/* .line 788 */
/* :catch_0 */
/* move-exception v3 */
/* .line 785 */
} // :goto_1
/* add-int/lit8 v2, v2, -0x1 */
/* .line 792 */
} // .end local v2 # "i":I
} // :cond_0
try { // :try_start_2
v2 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v2 ).finishBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 794 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 795 */
/* nop */
/* .line 796 */
return;
/* .line 794 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 795 */
/* throw v2 */
} // .end method
public void notifyPointerIconChanged ( Integer p0, android.view.PointerIcon p1 ) {
/* .locals 5 */
/* .param p1, "iconId" # I */
/* .param p2, "customIcon" # Landroid/view/PointerIcon; */
/* .line 431 */
/* iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 432 */
v0 = this.mDelegate;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 433 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 435 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = this.mDelegate;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 439 */
/* nop */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 440 */
/* .line 439 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 436 */
/* :catch_0 */
/* move-exception v2 */
/* .line 437 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_1
final String v3 = "MirrorService"; // const-string v3, "MirrorService"
final String v4 = "notify pointer icon change failed"; // const-string v4, "notify pointer icon change failed"
android.util.Slog .w ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 439 */
/* nop */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 440 */
/* throw v2 */
/* .line 443 */
} // .end local v0 # "ident":J
} // :cond_0
} // :goto_2
return;
} // .end method
public void notifyShadowImage ( android.graphics.Bitmap p0 ) {
/* .locals 0 */
/* .param p1, "shadowImage" # Landroid/graphics/Bitmap; */
/* .line 777 */
this.mCurrentShadowImage = p1;
/* .line 778 */
return;
} // .end method
public void onDelegatePermissionReleased ( java.util.List p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/net/Uri;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 418 */
/* .local p1, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;" */
v0 = this.mDelegate;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 419 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 421 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = this.mDelegate;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 425 */
/* nop */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 426 */
/* .line 425 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 422 */
/* :catch_0 */
/* move-exception v2 */
/* .line 423 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_1
final String v3 = "MirrorService"; // const-string v3, "MirrorService"
final String v4 = "notify permission release failed"; // const-string v4, "notify permission release failed"
android.util.Slog .w ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 425 */
/* nop */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 426 */
/* throw v2 */
/* .line 428 */
} // .end local v0 # "ident":J
} // :cond_0
} // :goto_2
return;
} // .end method
public void onRemoteMenuActionCall ( com.xiaomi.mirror.MirrorMenu p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "menu" # Lcom/xiaomi/mirror/MirrorMenu; */
/* .param p2, "displayId" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 302 */
v0 = this.mDelegate;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 303 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 304 */
/* .local v0, "uid":I */
(( com.xiaomi.mirror.MirrorMenu ) p1 ).getUri ( ); // invoke-virtual {p1}, Lcom/xiaomi/mirror/MirrorMenu;->getUri()Landroid/net/Uri;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 305 */
(( com.xiaomi.mirror.MirrorMenu ) p1 ).getUri ( ); // invoke-virtual {p1}, Lcom/xiaomi/mirror/MirrorMenu;->getUri()Landroid/net/Uri;
v4 = this.mDelegatePackageName;
/* iget v5, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegateUid:I */
int v6 = 3; // const/4 v6, 0x3
/* move-object v1, p0 */
/* move v3, v0 */
/* invoke-direct/range {v1 ..v6}, Lcom/xiaomi/mirror/service/MirrorService;->grantUri(Landroid/net/Uri;ILjava/lang/String;II)V */
/* .line 308 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 310 */
/* .local v1, "ident":J */
try { // :try_start_0
v3 = this.mDelegate;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 312 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 313 */
/* .line 312 */
/* :catchall_0 */
/* move-exception v3 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 313 */
/* throw v3 */
/* .line 315 */
} // .end local v0 # "uid":I
} // .end local v1 # "ident":J
} // :cond_1
} // :goto_0
return;
} // .end method
public void onShellCommand ( java.io.FileDescriptor p0, java.io.FileDescriptor p1, java.io.FileDescriptor p2, java.lang.String[] p3, android.os.ShellCallback p4, android.os.ResultReceiver p5 ) {
/* .locals 8 */
/* .param p1, "in" # Ljava/io/FileDescriptor; */
/* .param p2, "out" # Ljava/io/FileDescriptor; */
/* .param p3, "err" # Ljava/io/FileDescriptor; */
/* .param p4, "args" # [Ljava/lang/String; */
/* .param p5, "callback" # Landroid/os/ShellCallback; */
/* .param p6, "resultReceiver" # Landroid/os/ResultReceiver; */
/* .line 320 */
/* new-instance v0, Lcom/xiaomi/mirror/service/MirrorShellCommand; */
/* invoke-direct {v0}, Lcom/xiaomi/mirror/service/MirrorShellCommand;-><init>()V */
/* move-object v1, p0 */
/* move-object v2, p1 */
/* move-object v3, p2 */
/* move-object v4, p3 */
/* move-object v5, p4 */
/* move-object v6, p5 */
/* move-object v7, p6 */
/* invoke-virtual/range {v0 ..v7}, Lcom/xiaomi/mirror/service/MirrorShellCommand;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I */
/* .line 321 */
return;
} // .end method
public android.os.IBinder performDrag ( android.view.IWindow p0, Integer p1, Integer p2, android.content.ClipData p3, android.graphics.Bitmap p4 ) {
/* .locals 13 */
/* .param p1, "window" # Landroid/view/IWindow; */
/* .param p2, "flags" # I */
/* .param p3, "sourceDisplayId" # I */
/* .param p4, "data" # Landroid/content/ClipData; */
/* .param p5, "shadow" # Landroid/graphics/Bitmap; */
/* .line 212 */
/* move-object v1, p0 */
v9 = com.xiaomi.mirror.service.MirrorService .getCallingPid ( );
/* .line 213 */
/* .local v9, "callerPid":I */
v10 = com.xiaomi.mirror.service.MirrorService .getCallingUid ( );
/* .line 214 */
/* .local v10, "callerUid":I */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v11 */
/* .line 217 */
/* .local v11, "ident":J */
try { // :try_start_0
v2 = this.mMiuiMirrorDragDropController;
/* move v3, v9 */
/* move v4, v10 */
/* move-object v5, p1 */
/* move v6, p2 */
/* move/from16 v7, p3 */
/* move-object/from16 v8, p4 */
/* invoke-virtual/range {v2 ..v8}, Lcom/android/server/wm/MiuiMirrorDragDropController;->performDrag(IILandroid/view/IWindow;IILandroid/content/ClipData;)Landroid/os/IBinder; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 219 */
/* .local v0, "token":Landroid/os/IBinder; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 220 */
/* move-object/from16 v2, p5 */
try { // :try_start_1
(( com.xiaomi.mirror.service.MirrorService ) p0 ).updateShadow ( v2 ); // invoke-virtual {p0, v2}, Lcom/xiaomi/mirror/service/MirrorService;->updateShadow(Landroid/graphics/Bitmap;)V
/* .line 221 */
int v3 = 0; // const/4 v3, 0x0
/* iput-boolean v3, v1, Lcom/xiaomi/mirror/service/MirrorService;->mDragAcceptable:Z */
/* .line 222 */
/* iput v3, v1, Lcom/xiaomi/mirror/service/MirrorService;->mLastNotifiedUid:I */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 226 */
} // .end local v0 # "token":Landroid/os/IBinder;
/* :catchall_0 */
/* move-exception v0 */
/* .line 219 */
/* .restart local v0 # "token":Landroid/os/IBinder; */
} // :cond_0
/* move-object/from16 v2, p5 */
/* .line 224 */
} // :goto_0
/* nop */
/* .line 226 */
android.os.Binder .restoreCallingIdentity ( v11,v12 );
/* .line 224 */
/* .line 226 */
} // .end local v0 # "token":Landroid/os/IBinder;
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v2, p5 */
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v11,v12 );
/* .line 227 */
/* throw v0 */
} // .end method
public void registerCursorPositionChangedListener ( com.xiaomi.mirror.ICursorPositionChangedListener p0 ) {
/* .locals 6 */
/* .param p1, "listener" # Lcom/xiaomi/mirror/ICursorPositionChangedListener; */
/* .line 527 */
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 530 */
/* iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mIsInMouseShareMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 533 */
v0 = this.mCursorPositionChangedListeners;
/* monitor-enter v0 */
/* .line 534 */
try { // :try_start_0
v1 = android.os.Binder .getCallingPid ( );
/* .line 535 */
/* .local v1, "callingPid":I */
v2 = this.mCursorPositionChangedListeners;
(( android.util.SparseArray ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* if-nez v2, :cond_0 */
/* .line 539 */
/* new-instance v2, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord; */
/* invoke-direct {v2, p0, v1, p1}, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;-><init>(Lcom/xiaomi/mirror/service/MirrorService;ILcom/xiaomi/mirror/ICursorPositionChangedListener;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 542 */
/* .local v2, "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord; */
try { // :try_start_1
/* .line 543 */
/* .local v3, "binder":Landroid/os/IBinder; */
int v4 = 0; // const/4 v4, 0x0
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 547 */
} // .end local v3 # "binder":Landroid/os/IBinder;
/* nop */
/* .line 548 */
try { // :try_start_2
final String v3 = "MirrorService"; // const-string v3, "MirrorService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "a cursor listener register, from "; // const-string v5, "a cursor listener register, from "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", current size = "; // const-string v5, ", current size = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mCursorPositionChangedListeners;
/* .line 549 */
v5 = (( android.util.SparseArray ) v5 ).size ( ); // invoke-virtual {v5}, Landroid/util/SparseArray;->size()I
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 548 */
android.util.Slog .d ( v3,v4 );
/* .line 550 */
v3 = this.mCursorPositionChangedListeners;
(( android.util.SparseArray ) v3 ).put ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 551 */
/* invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorService;->populateCursorPositionListenerLocked()V */
/* .line 552 */
} // .end local v1 # "callingPid":I
} // .end local v2 # "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;
/* monitor-exit v0 */
/* .line 553 */
return;
/* .line 544 */
/* .restart local v1 # "callingPid":I */
/* .restart local v2 # "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord; */
/* :catch_0 */
/* move-exception v3 */
/* .line 546 */
/* .local v3, "e":Landroid/os/RemoteException; */
/* new-instance v4, Ljava/lang/RuntimeException; */
/* invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V */
} // .end local p0 # "this":Lcom/xiaomi/mirror/service/MirrorService;
} // .end local p1 # "listener":Lcom/xiaomi/mirror/ICursorPositionChangedListener;
/* throw v4 */
/* .line 536 */
} // .end local v2 # "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;
} // .end local v3 # "e":Landroid/os/RemoteException;
/* .restart local p0 # "this":Lcom/xiaomi/mirror/service/MirrorService; */
/* .restart local p1 # "listener":Lcom/xiaomi/mirror/ICursorPositionChangedListener; */
} // :cond_0
/* new-instance v2, Ljava/lang/SecurityException; */
final String v3 = "The calling process has alreadyregistered an CursorPositionChangedListener."; // const-string v3, "The calling process has alreadyregistered an CursorPositionChangedListener."
/* invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/xiaomi/mirror/service/MirrorService;
} // .end local p1 # "listener":Lcom/xiaomi/mirror/ICursorPositionChangedListener;
/* throw v2 */
/* .line 552 */
} // .end local v1 # "callingPid":I
/* .restart local p0 # "this":Lcom/xiaomi/mirror/service/MirrorService; */
/* .restart local p1 # "listener":Lcom/xiaomi/mirror/ICursorPositionChangedListener; */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 531 */
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "current is not in mouse share mode"; // const-string v1, "current is not in mouse share mode"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 528 */
} // :cond_2
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "listener must not be null"; // const-string v1, "listener must not be null"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void registerDelegate ( com.xiaomi.mirror.IMirrorDelegate p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "delegate" # Lcom/xiaomi/mirror/IMirrorDelegate; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 156 */
v0 = this.mContext;
final String v1 = "com.xiaomi.mirror.permission.REGISTER_MIRROR_DELEGATE"; // const-string v1, "com.xiaomi.mirror.permission.REGISTER_MIRROR_DELEGATE"
final String v2 = "registerDelegate"; // const-string v2, "registerDelegate"
(( android.content.Context ) v0 ).enforceCallingPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 158 */
v0 = this.mDelegate;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 159 */
final String v0 = "MirrorService"; // const-string v0, "MirrorService"
final String v1 = "already delegating!"; // const-string v1, "already delegating!"
android.util.Slog .d ( v0,v1 );
/* .line 160 */
return;
/* .line 162 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 165 */
v0 = com.xiaomi.mirror.service.MirrorService .getCallingUid ( );
/* .line 166 */
/* .local v0, "uid":I */
v1 = this.mContext;
/* const-class v2, Landroid/app/AppOpsManager; */
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v1, Landroid/app/AppOpsManager; */
(( android.app.AppOpsManager ) v1 ).checkPackage ( v0, p2 ); // invoke-virtual {v1, v0, p2}, Landroid/app/AppOpsManager;->checkPackage(ILjava/lang/String;)V
/* .line 167 */
v2 = this.mDeathRecipient;
int v3 = 0; // const/4 v3, 0x0
/* .line 168 */
this.mDelegate = p1;
/* .line 169 */
/* iput v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegateUid:I */
/* .line 170 */
v1 = com.xiaomi.mirror.service.MirrorService .getCallingPid ( );
/* iput v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDelegatePid:I */
/* .line 171 */
this.mDelegatePackageName = p2;
/* .line 172 */
/* invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorService;->broadcastDelegateStateChanged()V */
/* .line 173 */
return;
/* .line 163 */
} // .end local v0 # "uid":I
} // :cond_1
/* new-instance v0, Ljava/lang/NullPointerException; */
/* invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V */
/* throw v0 */
} // .end method
public void registerDragListener ( com.xiaomi.mirror.IDragListener p0 ) {
/* .locals 5 */
/* .param p1, "listener" # Lcom/xiaomi/mirror/IDragListener; */
/* .line 667 */
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 670 */
/* iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mIsInMouseShareMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 673 */
v0 = this.mDragListeners;
/* monitor-enter v0 */
/* .line 674 */
try { // :try_start_0
v1 = android.os.Binder .getCallingPid ( );
/* .line 675 */
/* .local v1, "callingPid":I */
v2 = this.mDragListeners;
(( android.util.SparseArray ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* if-nez v2, :cond_0 */
/* .line 679 */
/* new-instance v2, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord; */
/* invoke-direct {v2, p0, v1, p1}, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;-><init>(Lcom/xiaomi/mirror/service/MirrorService;ILcom/xiaomi/mirror/IDragListener;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 682 */
/* .local v2, "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord; */
try { // :try_start_1
/* .line 683 */
/* .local v3, "binder":Landroid/os/IBinder; */
int v4 = 0; // const/4 v4, 0x0
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 687 */
} // .end local v3 # "binder":Landroid/os/IBinder;
/* nop */
/* .line 688 */
try { // :try_start_2
v3 = this.mDragListeners;
(( android.util.SparseArray ) v3 ).put ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 689 */
} // .end local v1 # "callingPid":I
} // .end local v2 # "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;
/* monitor-exit v0 */
/* .line 690 */
return;
/* .line 684 */
/* .restart local v1 # "callingPid":I */
/* .restart local v2 # "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord; */
/* :catch_0 */
/* move-exception v3 */
/* .line 686 */
/* .local v3, "e":Landroid/os/RemoteException; */
/* new-instance v4, Ljava/lang/RuntimeException; */
/* invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V */
} // .end local p0 # "this":Lcom/xiaomi/mirror/service/MirrorService;
} // .end local p1 # "listener":Lcom/xiaomi/mirror/IDragListener;
/* throw v4 */
/* .line 676 */
} // .end local v2 # "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;
} // .end local v3 # "e":Landroid/os/RemoteException;
/* .restart local p0 # "this":Lcom/xiaomi/mirror/service/MirrorService; */
/* .restart local p1 # "listener":Lcom/xiaomi/mirror/IDragListener; */
} // :cond_0
/* new-instance v2, Ljava/lang/SecurityException; */
final String v3 = "The calling process has alreadyregistered a drag listener."; // const-string v3, "The calling process has alreadyregistered a drag listener."
/* invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/xiaomi/mirror/service/MirrorService;
} // .end local p1 # "listener":Lcom/xiaomi/mirror/IDragListener;
/* throw v2 */
/* .line 689 */
} // .end local v1 # "callingPid":I
/* .restart local p0 # "this":Lcom/xiaomi/mirror/service/MirrorService; */
/* .restart local p1 # "listener":Lcom/xiaomi/mirror/IDragListener; */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 671 */
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "current is not in mouse share mode"; // const-string v1, "current is not in mouse share mode"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 668 */
} // :cond_2
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "listener must not be null"; // const-string v1, "listener must not be null"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void removeMirrorStateListener ( com.xiaomi.mirror.IMirrorStateListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lcom/xiaomi/mirror/IMirrorStateListener; */
/* .line 151 */
v0 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v0 ).register ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
/* .line 152 */
return;
} // .end method
public void reportDropResult ( android.view.IWindow p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "window" # Landroid/view/IWindow; */
/* .param p2, "consumed" # Z */
/* .line 242 */
v0 = android.os.Binder .getCallingPid ( );
/* .line 243 */
/* .local v0, "pid":I */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 245 */
/* .local v1, "ident":J */
try { // :try_start_0
v3 = this.mMiuiMirrorDragDropController;
(( com.android.server.wm.MiuiMirrorDragDropController ) v3 ).reportDropResult ( v0, p1, p2 ); // invoke-virtual {v3, v0, p1, p2}, Lcom/android/server/wm/MiuiMirrorDragDropController;->reportDropResult(ILandroid/view/IWindow;Z)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 247 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 248 */
/* nop */
/* .line 249 */
return;
/* .line 247 */
/* :catchall_0 */
/* move-exception v3 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 248 */
/* throw v3 */
} // .end method
void resetDragAndDropController ( ) {
/* .locals 1 */
/* .line 121 */
/* iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 122 */
/* new-instance v0, Lcom/android/server/wm/MiuiMirrorDragDropController; */
/* invoke-direct {v0}, Lcom/android/server/wm/MiuiMirrorDragDropController;-><init>()V */
this.mMiuiMirrorDragDropController = v0;
/* .line 124 */
} // :cond_0
return;
} // .end method
public void revokePermissions ( android.content.ClipData p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "data" # Landroid/content/ClipData; */
/* .param p2, "sourceUid" # I */
/* .line 204 */
v0 = this.mContext;
final String v1 = "com.xiaomi.mirror.permission.REGISTER_MIRROR_DELEGATE"; // const-string v1, "com.xiaomi.mirror.permission.REGISTER_MIRROR_DELEGATE"
final String v2 = "revokeDragDataPermissions"; // const-string v2, "revokeDragDataPermissions"
(( android.content.Context ) v0 ).enforceCallingPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 206 */
/* invoke-direct {p0, p1, p2}, Lcom/xiaomi/mirror/service/MirrorService;->revokePermissionsInternal(Landroid/content/ClipData;I)V */
/* .line 207 */
return;
} // .end method
public void setAllowGrant ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "allowGrant" # Z */
/* .line 331 */
/* iget-boolean v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 332 */
v0 = com.xiaomi.mirror.service.MirrorService.sAllowGrant;
java.lang.Boolean .valueOf ( p1 );
(( java.lang.ThreadLocal ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V
/* .line 334 */
} // :cond_0
return;
} // .end method
public void setDragAcceptable ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "acceptable" # Z */
/* .line 276 */
v0 = this.mMiuiMirrorDragDropController;
v0 = (( com.android.server.wm.MiuiMirrorDragDropController ) v0 ).dragDropActiveLocked ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMirrorDragDropController;->dragDropActiveLocked()Z
/* if-nez v0, :cond_0 */
/* .line 277 */
return;
/* .line 279 */
} // :cond_0
v0 = android.os.Binder .getCallingUid ( );
/* .line 280 */
/* .local v0, "uid":I */
v1 = this.mMiuiMirrorDragDropController;
v1 = (( com.android.server.wm.MiuiMirrorDragDropController ) v1 ).lastTargetUid ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiMirrorDragDropController;->lastTargetUid()I
/* if-eq v0, v1, :cond_1 */
/* .line 281 */
return;
/* .line 283 */
} // :cond_1
/* iget v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mLastNotifiedUid:I */
/* if-ne v1, v0, :cond_2 */
/* iget-boolean v1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragAcceptable:Z */
/* if-ne v1, p1, :cond_2 */
/* .line 284 */
return;
/* .line 286 */
} // :cond_2
/* iput v0, p0, Lcom/xiaomi/mirror/service/MirrorService;->mLastNotifiedUid:I */
/* .line 287 */
/* iput-boolean p1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mDragAcceptable:Z */
/* .line 288 */
v1 = this.mDelegate;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 289 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 291 */
/* .local v1, "ident":J */
try { // :try_start_0
v3 = this.mDelegate;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 295 */
/* nop */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 296 */
/* .line 295 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 292 */
/* :catch_0 */
/* move-exception v3 */
/* .line 293 */
/* .local v3, "e":Landroid/os/RemoteException; */
try { // :try_start_1
final String v4 = "MirrorService"; // const-string v4, "MirrorService"
final String v5 = "notify drag acceptable failed"; // const-string v5, "notify drag acceptable failed"
android.util.Slog .w ( v4,v5 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 295 */
/* nop */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 296 */
/* throw v3 */
/* .line 298 */
} // .end local v1 # "ident":J
} // :cond_3
} // :goto_2
return;
} // .end method
public Boolean setDragSurfaceVisible ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "visible" # Z */
/* .line 643 */
v0 = com.android.server.wm.DragDropControllerStub .get ( );
} // .end method
public void setFinishAnimatorNeeded ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "needFinishAnimator" # Z */
/* .line 655 */
/* iput-boolean p1, p0, Lcom/xiaomi/mirror/service/MirrorService;->mNeedFinishAnimator:Z */
/* .line 656 */
return;
} // .end method
public Boolean tryToShareDrag ( java.lang.String p0, Integer p1, android.content.ClipData p2 ) {
/* .locals 5 */
/* .param p1, "targetPkg" # Ljava/lang/String; */
/* .param p2, "taskId" # I */
/* .param p3, "data" # Landroid/content/ClipData; */
/* .line 391 */
v0 = this.mDelegate;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 392 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 394 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = v2 = this.mDelegate;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 398 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 394 */
/* .line 398 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 395 */
/* :catch_0 */
/* move-exception v2 */
/* .line 396 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_1
final String v3 = "MirrorService"; // const-string v3, "MirrorService"
/* const-string/jumbo v4, "try to share drag failed" */
android.util.Slog .w ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 398 */
/* nop */
} // .end local v2 # "e":Landroid/os/RemoteException;
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 399 */
/* .line 398 */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 399 */
/* throw v2 */
/* .line 401 */
} // .end local v0 # "ident":J
} // :cond_0
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void unregisterCursorPositionChangedListener ( com.xiaomi.mirror.ICursorPositionChangedListener p0 ) {
/* .locals 6 */
/* .param p1, "listener" # Lcom/xiaomi/mirror/ICursorPositionChangedListener; */
/* .line 567 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 570 */
v0 = this.mCursorPositionChangedListeners;
/* monitor-enter v0 */
/* .line 571 */
try { // :try_start_0
v1 = android.os.Binder .getCallingPid ( );
/* .line 572 */
/* .local v1, "callingPid":I */
v2 = this.mCursorPositionChangedListeners;
/* .line 573 */
(( android.util.SparseArray ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord; */
/* .line 574 */
/* .local v2, "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 578 */
final String v3 = "MirrorService"; // const-string v3, "MirrorService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "a cursor listener unregister, from "; // const-string v5, "a cursor listener unregister, from "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", current size = "; // const-string v5, ", current size = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mCursorPositionChangedListeners;
/* .line 579 */
v5 = (( android.util.SparseArray ) v5 ).size ( ); // invoke-virtual {v5}, Landroid/util/SparseArray;->size()I
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 578 */
android.util.Slog .d ( v3,v4 );
/* .line 580 */
/* invoke-direct {p0, v1}, Lcom/xiaomi/mirror/service/MirrorService;->onCursorPositionChangedListenerDied(I)V */
/* .line 581 */
/* invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorService;->unPopulateCursorPositionListenerLocked()V */
/* .line 582 */
} // .end local v1 # "callingPid":I
} // .end local v2 # "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;
/* monitor-exit v0 */
/* .line 583 */
return;
/* .line 575 */
/* .restart local v1 # "callingPid":I */
/* .restart local v2 # "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord; */
} // :cond_0
/* new-instance v3, Ljava/lang/SecurityException; */
final String v4 = "The calling process not register an CursorPositionChangedListener."; // const-string v4, "The calling process not register an CursorPositionChangedListener."
/* invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/xiaomi/mirror/service/MirrorService;
} // .end local p1 # "listener":Lcom/xiaomi/mirror/ICursorPositionChangedListener;
/* throw v3 */
/* .line 582 */
} // .end local v1 # "callingPid":I
} // .end local v2 # "record":Lcom/xiaomi/mirror/service/MirrorService$CursorPositionChangedListenerRecord;
/* .restart local p0 # "this":Lcom/xiaomi/mirror/service/MirrorService; */
/* .restart local p1 # "listener":Lcom/xiaomi/mirror/ICursorPositionChangedListener; */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 568 */
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "listener must not be null"; // const-string v1, "listener must not be null"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void unregisterDelegate ( com.xiaomi.mirror.IMirrorDelegate p0 ) {
/* .locals 3 */
/* .param p1, "delegate" # Lcom/xiaomi/mirror/IMirrorDelegate; */
/* .line 177 */
v0 = this.mContext;
final String v1 = "com.xiaomi.mirror.permission.REGISTER_MIRROR_DELEGATE"; // const-string v1, "com.xiaomi.mirror.permission.REGISTER_MIRROR_DELEGATE"
/* const-string/jumbo v2, "unregisterDelegate" */
(( android.content.Context ) v0 ).enforceCallingPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 179 */
v0 = this.mDelegate;
final String v1 = "MirrorService"; // const-string v1, "MirrorService"
/* if-nez v0, :cond_0 */
/* .line 180 */
final String v0 = "already undelegated"; // const-string v0, "already undelegated"
android.util.Slog .d ( v1,v0 );
/* .line 181 */
return;
/* .line 183 */
} // :cond_0
v2 = this.mDelegate;
/* if-eq v0, v2, :cond_1 */
/* .line 184 */
final String v0 = "binder doesn\'t match!"; // const-string v0, "binder doesn\'t match!"
android.util.Slog .w ( v1,v0 );
/* .line 185 */
return;
/* .line 187 */
} // :cond_1
v0 = this.mDelegate;
v1 = this.mDeathRecipient;
v0 = int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 188 */
/* invoke-direct {p0}, Lcom/xiaomi/mirror/service/MirrorService;->delegateLost()V */
/* .line 190 */
} // :cond_2
return;
} // .end method
public void unregisterDragListener ( com.xiaomi.mirror.IDragListener p0 ) {
/* .locals 5 */
/* .param p1, "listener" # Lcom/xiaomi/mirror/IDragListener; */
/* .line 694 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 697 */
v0 = this.mDragListeners;
/* monitor-enter v0 */
/* .line 698 */
try { // :try_start_0
v1 = android.os.Binder .getCallingPid ( );
/* .line 699 */
/* .local v1, "callingPid":I */
v2 = this.mDragListeners;
(( android.util.SparseArray ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord; */
/* .line 700 */
/* .local v2, "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 704 */
/* invoke-direct {p0, v1}, Lcom/xiaomi/mirror/service/MirrorService;->onDragListenerDied(I)V */
/* .line 705 */
} // .end local v1 # "callingPid":I
} // .end local v2 # "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;
/* monitor-exit v0 */
/* .line 706 */
return;
/* .line 701 */
/* .restart local v1 # "callingPid":I */
/* .restart local v2 # "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord; */
} // :cond_0
/* new-instance v3, Ljava/lang/SecurityException; */
final String v4 = "The calling process not register an drag listener"; // const-string v4, "The calling process not register an drag listener"
/* invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/xiaomi/mirror/service/MirrorService;
} // .end local p1 # "listener":Lcom/xiaomi/mirror/IDragListener;
/* throw v3 */
/* .line 705 */
} // .end local v1 # "callingPid":I
} // .end local v2 # "record":Lcom/xiaomi/mirror/service/MirrorService$DragListenerRecord;
/* .restart local p0 # "this":Lcom/xiaomi/mirror/service/MirrorService; */
/* .restart local p1 # "listener":Lcom/xiaomi/mirror/IDragListener; */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 695 */
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "listener must not be null"; // const-string v1, "listener must not be null"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void updateShadow ( android.graphics.Bitmap p0 ) {
/* .locals 5 */
/* .param p1, "shadow" # Landroid/graphics/Bitmap; */
/* .line 263 */
v0 = this.mDelegate;
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 264 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 266 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = this.mDelegate;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 270 */
/* nop */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 271 */
/* .line 270 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 267 */
/* :catch_0 */
/* move-exception v2 */
/* .line 268 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_1
final String v3 = "MirrorService"; // const-string v3, "MirrorService"
final String v4 = "notify drag shadow failed"; // const-string v4, "notify drag shadow failed"
android.util.Slog .w ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 270 */
/* nop */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 271 */
/* throw v2 */
/* .line 273 */
} // .end local v0 # "ident":J
} // :cond_0
} // :goto_2
return;
} // .end method
