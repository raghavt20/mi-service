.class public Lcom/xiaomi/mirror/MirrorServiceImpl;
.super Lcom/xiaomi/mirror/MirrorServiceStub;
.source "MirrorServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.xiaomi.mirror.MirrorServiceStub$$"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/xiaomi/mirror/MirrorServiceStub;-><init>()V

    return-void
.end method


# virtual methods
.method public dragDropActiveLocked()Z
    .locals 1

    .line 35
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->get()Lcom/xiaomi/mirror/service/MirrorService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getSystemReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->get()Lcom/xiaomi/mirror/service/MirrorService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getDragDropController()Lcom/android/server/wm/MiuiMirrorDragDropController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMirrorDragDropController;->dragDropActiveLocked()Z

    move-result v0

    return v0

    .line 38
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public dragDropActiveLockedWithMirrorEnabled()Z
    .locals 1

    .line 60
    invoke-virtual {p0}, Lcom/xiaomi/mirror/MirrorServiceImpl;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/xiaomi/mirror/MirrorServiceImpl;->dragDropActiveLocked()Z

    move-result v0

    return v0

    .line 63
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getAllowGrant()Z
    .locals 1

    .line 50
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->get()Lcom/xiaomi/mirror/service/MirrorService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getAllowGrant()Z

    move-result v0

    return v0
.end method

.method public isEnabled()Z
    .locals 3

    .line 15
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->get()Lcom/xiaomi/mirror/service/MirrorService;

    move-result-object v0

    .line 16
    .local v0, "mirrorService":Lcom/xiaomi/mirror/service/MirrorService;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 17
    return v1

    .line 19
    :cond_0
    invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getDelegatePid()I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public isGrantAllowed(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "targetPkg"    # Ljava/lang/String;

    .line 24
    const-string v0, "com.xiaomi.mirror.remoteprovider"

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 25
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->get()Lcom/xiaomi/mirror/service/MirrorService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getDelegatePackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 24
    :goto_1
    return v0
.end method

.method public notifyPointerIconChanged(ILandroid/view/PointerIcon;)V
    .locals 1
    .param p1, "iconId"    # I
    .param p2, "customIcon"    # Landroid/view/PointerIcon;

    .line 55
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->get()Lcom/xiaomi/mirror/service/MirrorService;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/xiaomi/mirror/service/MirrorService;->notifyPointerIconChanged(ILandroid/view/PointerIcon;)V

    .line 56
    return-void
.end method

.method public sendDragStartedIfNeededLocked(Ljava/lang/Object;)V
    .locals 1
    .param p1, "window"    # Ljava/lang/Object;

    .line 43
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->get()Lcom/xiaomi/mirror/service/MirrorService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getSystemReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->get()Lcom/xiaomi/mirror/service/MirrorService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mirror/service/MirrorService;->getDragDropController()Lcom/android/server/wm/MiuiMirrorDragDropController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiMirrorDragDropController;->sendDragStartedIfNeededLocked(Ljava/lang/Object;)V

    .line 46
    :cond_0
    return-void
.end method

.method public setAllowGrant(Z)V
    .locals 1
    .param p1, "allowGrant"    # Z

    .line 30
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorService;->get()Lcom/xiaomi/mirror/service/MirrorService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/mirror/service/MirrorService;->setAllowGrant(Z)V

    .line 31
    return-void
.end method
