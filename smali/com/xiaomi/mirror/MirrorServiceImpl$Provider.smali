.class public final Lcom/xiaomi/mirror/MirrorServiceImpl$Provider;
.super Ljava/lang/Object;
.source "MirrorServiceImpl$Provider.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/mirror/MirrorServiceImpl$Provider$SINGLETON;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
        "Lcom/xiaomi/mirror/MirrorServiceImpl;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provideNewInstance()Lcom/xiaomi/mirror/MirrorServiceImpl;
    .locals 1

    .line 17
    new-instance v0, Lcom/xiaomi/mirror/MirrorServiceImpl;

    invoke-direct {v0}, Lcom/xiaomi/mirror/MirrorServiceImpl;-><init>()V

    return-object v0
.end method

.method public bridge synthetic provideNewInstance()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/xiaomi/mirror/MirrorServiceImpl$Provider;->provideNewInstance()Lcom/xiaomi/mirror/MirrorServiceImpl;

    move-result-object v0

    return-object v0
.end method

.method public provideSingleton()Lcom/xiaomi/mirror/MirrorServiceImpl;
    .locals 1

    .line 13
    sget-object v0, Lcom/xiaomi/mirror/MirrorServiceImpl$Provider$SINGLETON;->INSTANCE:Lcom/xiaomi/mirror/MirrorServiceImpl;

    return-object v0
.end method

.method public bridge synthetic provideSingleton()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/xiaomi/mirror/MirrorServiceImpl$Provider;->provideSingleton()Lcom/xiaomi/mirror/MirrorServiceImpl;

    move-result-object v0

    return-object v0
.end method
