public class com.xiaomi.abtest.ABTestConfig$Builder {
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/abtest/ABTestConfig; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "Builder" */
} // .end annotation
/* # instance fields */
private java.lang.String a;
private java.lang.String b;
private java.lang.String c;
private java.lang.String d;
private Integer e;
private Boolean f;
/* # direct methods */
public com.xiaomi.abtest.ABTestConfig$Builder ( ) {
/* .locals 0 */
/* .line 71 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static java.lang.String a ( com.xiaomi.abtest.ABTestConfig$Builder p0 ) { //synthethic
/* .locals 0 */
/* .line 71 */
p0 = this.a;
} // .end method
static java.lang.String b ( com.xiaomi.abtest.ABTestConfig$Builder p0 ) { //synthethic
/* .locals 0 */
/* .line 71 */
p0 = this.b;
} // .end method
static java.lang.String c ( com.xiaomi.abtest.ABTestConfig$Builder p0 ) { //synthethic
/* .locals 0 */
/* .line 71 */
p0 = this.c;
} // .end method
static java.lang.String d ( com.xiaomi.abtest.ABTestConfig$Builder p0 ) { //synthethic
/* .locals 0 */
/* .line 71 */
p0 = this.d;
} // .end method
static Integer e ( com.xiaomi.abtest.ABTestConfig$Builder p0 ) { //synthethic
/* .locals 0 */
/* .line 71 */
/* iget p0, p0, Lcom/xiaomi/abtest/ABTestConfig$Builder;->e:I */
} // .end method
static Boolean f ( com.xiaomi.abtest.ABTestConfig$Builder p0 ) { //synthethic
/* .locals 0 */
/* .line 71 */
/* iget-boolean p0, p0, Lcom/xiaomi/abtest/ABTestConfig$Builder;->f:Z */
} // .end method
/* # virtual methods */
public com.xiaomi.abtest.ABTestConfig build ( ) {
/* .locals 2 */
/* .line 110 */
/* new-instance v0, Lcom/xiaomi/abtest/ABTestConfig; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/abtest/ABTestConfig;-><init>(Lcom/xiaomi/abtest/ABTestConfig$Builder;Lcom/xiaomi/abtest/ABTestConfig$1;)V */
} // .end method
public com.xiaomi.abtest.ABTestConfig$Builder setAppName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "appName" # Ljava/lang/String; */
/* .line 80 */
this.a = p1;
/* .line 81 */
} // .end method
public com.xiaomi.abtest.ABTestConfig$Builder setDeviceId ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "deviceId" # Ljava/lang/String; */
/* .line 95 */
this.d = p1;
/* .line 96 */
} // .end method
public com.xiaomi.abtest.ABTestConfig$Builder setDisableLoadTimer ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "disableLoadTimer" # Z */
/* .line 105 */
/* iput-boolean p1, p0, Lcom/xiaomi/abtest/ABTestConfig$Builder;->f:Z */
/* .line 106 */
} // .end method
public com.xiaomi.abtest.ABTestConfig$Builder setLayerName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "layerName" # Ljava/lang/String; */
/* .line 85 */
this.b = p1;
/* .line 86 */
} // .end method
public com.xiaomi.abtest.ABTestConfig$Builder setLoadConfigInterval ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "interval" # I */
/* .line 100 */
/* iput p1, p0, Lcom/xiaomi/abtest/ABTestConfig$Builder;->e:I */
/* .line 101 */
} // .end method
public com.xiaomi.abtest.ABTestConfig$Builder setUserId ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "userId" # Ljava/lang/String; */
/* .line 90 */
this.c = p1;
/* .line 91 */
} // .end method
