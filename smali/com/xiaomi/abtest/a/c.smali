.class Lcom/xiaomi/abtest/a/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/xiaomi/abtest/a/a;


# direct methods
.method constructor <init>(Lcom/xiaomi/abtest/a/a;I)V
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/xiaomi/abtest/a/c;->b:Lcom/xiaomi/abtest/a/a;

    iput p2, p0, Lcom/xiaomi/abtest/a/c;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mIsAppForeground: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/abtest/a/c;->b:Lcom/xiaomi/abtest/a/a;

    invoke-static {v1}, Lcom/xiaomi/abtest/a/a;->a(Lcom/xiaomi/abtest/a/a;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsLoadConfigWhenBackground: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/abtest/a/c;->b:Lcom/xiaomi/abtest/a/a;

    invoke-static {v1}, Lcom/xiaomi/abtest/a/a;->b(Lcom/xiaomi/abtest/a/a;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExpPlatformManager"

    invoke-static {v1, v0}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/xiaomi/abtest/a/c;->b:Lcom/xiaomi/abtest/a/a;

    invoke-static {v0}, Lcom/xiaomi/abtest/a/a;->a(Lcom/xiaomi/abtest/a/a;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/abtest/a/c;->b:Lcom/xiaomi/abtest/a/a;

    invoke-static {v0}, Lcom/xiaomi/abtest/a/a;->b(Lcom/xiaomi/abtest/a/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/abtest/a/c;->b:Lcom/xiaomi/abtest/a/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/xiaomi/abtest/a/a;->a(Lcom/xiaomi/abtest/ABTest$OnLoadRemoteConfigListener;)V

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/xiaomi/abtest/a/c;->b:Lcom/xiaomi/abtest/a/a;

    invoke-static {v0}, Lcom/xiaomi/abtest/a/a;->c(Lcom/xiaomi/abtest/a/a;)Landroid/os/Handler;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/abtest/a/c;->a:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 141
    return-void
.end method
