.class public Lcom/xiaomi/abtest/a/a;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String; = "ExpPlatformManager"

.field private static final b:Ljava/lang/String; = "id"

.field private static final c:Ljava/lang/String; = "type"

.field private static final d:Ljava/lang/String; = "status"

.field private static final e:Ljava/lang/String; = "bucketIds"

.field private static final f:Ljava/lang/String; = "name"

.field private static final g:Ljava/lang/String; = "fid"

.field private static final h:Ljava/lang/String; = "fPath"

.field private static final i:Ljava/lang/String; = "xPath"

.field private static final j:Ljava/lang/String; = "conditionString"

.field private static final k:Ljava/lang/String; = "diversionType"

.field private static final l:Ljava/lang/String; = "hashSeed"

.field private static final m:Ljava/lang/String; = "parameters"

.field private static final n:Ljava/lang/String; = "children"

.field private static final o:Ljava/util/concurrent/ExecutorService;

.field private static volatile y:Lcom/xiaomi/abtest/a/a;


# instance fields
.field private A:Landroid/app/Application$ActivityLifecycleCallbacks;

.field private p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/abtest/c/b;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/abtest/ExperimentInfo;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private s:I

.field private t:Z

.field private u:Z

.field private v:Landroid/os/Handler;

.field private w:Z

.field private x:Ljava/lang/String;

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 53
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/abtest/a/a;->o:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method private constructor <init>(Lcom/xiaomi/abtest/ABTestConfig;)V
    .locals 2

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/abtest/a/a;->p:Ljava/util/Set;

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/abtest/a/a;->q:Ljava/util/Map;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/abtest/a/a;->r:Ljava/util/Map;

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/abtest/a/a;->t:Z

    .line 446
    new-instance v0, Lcom/xiaomi/abtest/a/e;

    invoke-direct {v0, p0}, Lcom/xiaomi/abtest/a/e;-><init>(Lcom/xiaomi/abtest/a/a;)V

    iput-object v0, p0, Lcom/xiaomi/abtest/a/a;->A:Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 104
    if-eqz p1, :cond_1

    .line 105
    invoke-virtual {p1}, Lcom/xiaomi/abtest/ABTestConfig;->getLayerName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/abtest/a/a;->x:Ljava/lang/String;

    .line 106
    invoke-virtual {p1}, Lcom/xiaomi/abtest/ABTestConfig;->isDisableLoadTimer()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/abtest/a/a;->w:Z

    .line 107
    if-nez v0, :cond_1

    .line 108
    invoke-virtual {p1}, Lcom/xiaomi/abtest/ABTestConfig;->getLoadConfigInterval()I

    move-result v0

    if-lez v0, :cond_1

    .line 110
    invoke-virtual {p1}, Lcom/xiaomi/abtest/ABTestConfig;->getLoadConfigInterval()I

    move-result v0

    const/16 v1, 0x1c20

    if-ge v0, v1, :cond_0

    .line 111
    iput v1, p0, Lcom/xiaomi/abtest/a/a;->s:I

    goto :goto_0

    .line 114
    :cond_0
    invoke-virtual {p1}, Lcom/xiaomi/abtest/ABTestConfig;->getLoadConfigInterval()I

    move-result p1

    iput p1, p0, Lcom/xiaomi/abtest/a/a;->s:I

    .line 120
    :cond_1
    :goto_0
    sget-object p1, Lcom/xiaomi/abtest/a/a;->o:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/xiaomi/abtest/a/b;

    invoke-direct {v0, p0}, Lcom/xiaomi/abtest/a/b;-><init>(Lcom/xiaomi/abtest/a/a;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 127
    invoke-static {}, Lcom/xiaomi/abtest/d/a;->a()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/xiaomi/abtest/a/a;->a(Landroid/content/Context;)V

    .line 128
    return-void
.end method

.method private a(ILcom/xiaomi/abtest/c/e;)I
    .locals 1

    .line 430
    invoke-virtual {p2}, Lcom/xiaomi/abtest/c/e;->a()I

    move-result v0

    if-ne v0, p1, :cond_0

    invoke-virtual {p2}, Lcom/xiaomi/abtest/c/e;->d()I

    move-result p1

    return p1

    .line 431
    :cond_0
    invoke-virtual {p2}, Lcom/xiaomi/abtest/c/e;->g()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 432
    invoke-virtual {p2}, Lcom/xiaomi/abtest/c/e;->g()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/abtest/c/e;

    .line 433
    invoke-direct {p0, p1, v0}, Lcom/xiaomi/abtest/a/a;->a(ILcom/xiaomi/abtest/c/e;)I

    move-result v0

    .line 434
    if-lez v0, :cond_1

    .line 435
    return v0

    .line 437
    :cond_1
    goto :goto_0

    .line 439
    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method private a(Lorg/json/JSONObject;)Lcom/xiaomi/abtest/ExperimentInfo;
    .locals 5

    .line 306
    if-nez p1, :cond_0

    .line 307
    const/4 p1, 0x0

    return-object p1

    .line 310
    :cond_0
    new-instance v0, Lcom/xiaomi/abtest/ExperimentInfo;

    invoke-direct {v0}, Lcom/xiaomi/abtest/ExperimentInfo;-><init>()V

    .line 311
    const-string v1, "expId"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/xiaomi/abtest/ExperimentInfo;->expId:I

    .line 312
    const-string/jumbo v1, "xPath"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/xiaomi/abtest/ExperimentInfo;->xpath:Ljava/lang/String;

    .line 313
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, v0, Lcom/xiaomi/abtest/ExperimentInfo;->params:Ljava/util/Map;

    .line 315
    const-string v1, "params"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 316
    if-eqz p1, :cond_1

    .line 317
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v1

    .line 318
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 319
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 320
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 321
    iget-object v4, v0, Lcom/xiaomi/abtest/ExperimentInfo;->params:Ljava/util/Map;

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    goto :goto_0

    .line 325
    :cond_1
    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .line 443
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Application;

    iget-object v0, p0, Lcom/xiaomi/abtest/a/a;->A:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {p1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 444
    return-void
.end method

.method public static a(Lcom/xiaomi/abtest/ABTestConfig;)V
    .locals 2

    .line 76
    sget-object v0, Lcom/xiaomi/abtest/a/a;->y:Lcom/xiaomi/abtest/a/a;

    if-nez v0, :cond_1

    .line 77
    const-class v0, Lcom/xiaomi/abtest/a/a;

    monitor-enter v0

    .line 78
    :try_start_0
    sget-object v1, Lcom/xiaomi/abtest/a/a;->y:Lcom/xiaomi/abtest/a/a;

    if-nez v1, :cond_0

    .line 79
    new-instance v1, Lcom/xiaomi/abtest/a/a;

    invoke-direct {v1, p0}, Lcom/xiaomi/abtest/a/a;-><init>(Lcom/xiaomi/abtest/ABTestConfig;)V

    sput-object v1, Lcom/xiaomi/abtest/a/a;->y:Lcom/xiaomi/abtest/a/a;

    .line 81
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 83
    :cond_1
    :goto_0
    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 19

    .line 218
    move-object/from16 v0, p0

    const-string v1, "abtest"

    const-string v2, "save the data to local file, data : "

    const-string v3, "parse expConfig start"

    const-string v4, "ExpPlatformManager"

    invoke-static {v4, v3}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    nop

    .line 221
    const/4 v3, 0x0

    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    move-object/from16 v6, p1

    invoke-direct {v5, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 222
    :try_start_1
    const-string v3, "expInfo"

    invoke-virtual {v5, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 224
    invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v6

    .line 225
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 226
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 227
    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 228
    invoke-direct {v0, v8}, Lcom/xiaomi/abtest/a/a;->b(Lorg/json/JSONObject;)Lcom/xiaomi/abtest/c/e;

    move-result-object v8

    .line 229
    iget-object v9, v0, Lcom/xiaomi/abtest/a/a;->q:Ljava/util/Map;

    check-cast v8, Lcom/xiaomi/abtest/c/b;

    invoke-interface {v9, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    goto :goto_0

    .line 232
    :cond_0
    const/4 v3, 0x0

    const/4 v6, 0x1

    if-eqz p2, :cond_1

    .line 233
    const-string v7, "parse expInfo finished. It takes %s ms from the beginning of reading the local config to now"

    new-array v8, v6, [Ljava/lang/Object;

    .line 234
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    iget-wide v11, v0, Lcom/xiaomi/abtest/a/a;->z:J

    sub-long/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v3

    .line 233
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_1
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 238
    const-string/jumbo v8, "whitelist"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 239
    invoke-virtual {v8}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v9

    .line 240
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a

    .line 241
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 242
    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 243
    invoke-virtual {v11}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v12

    .line 246
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 248
    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_8

    .line 249
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 250
    invoke-virtual {v11, v14}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v15

    .line 251
    invoke-virtual {v15}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v16

    .line 253
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 254
    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_6

    .line 255
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v6, v17

    check-cast v6, Ljava/lang/String;

    .line 257
    move-object/from16 v17, v8

    iget-object v8, v0, Lcom/xiaomi/abtest/a/a;->x:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 258
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v18, v11

    const-string v11, "/"

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v11, "/ExpLayer/ExpDomain/"

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 259
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v8

    iget-object v11, v0, Lcom/xiaomi/abtest/a/a;->x:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 260
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "the fPath "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "doesn\'t meet the filter conditions, skip it!"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->remove()V

    .line 262
    move-object/from16 v8, v17

    move-object/from16 v11, v18

    const/4 v6, 0x1

    goto :goto_3

    .line 257
    :cond_3
    move-object/from16 v18, v11

    .line 266
    :cond_4
    invoke-virtual {v15, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    invoke-direct {v0, v8}, Lcom/xiaomi/abtest/a/a;->a(Lorg/json/JSONObject;)Lcom/xiaomi/abtest/ExperimentInfo;

    move-result-object v8

    .line 267
    if-eqz v8, :cond_5

    .line 268
    invoke-interface {v3, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    :cond_5
    move-object/from16 v8, v17

    move-object/from16 v11, v18

    const/4 v6, 0x1

    goto/16 :goto_3

    .line 272
    :cond_6
    move-object/from16 v17, v8

    move-object/from16 v18, v11

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v6

    if-nez v6, :cond_7

    .line 273
    invoke-interface {v12}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 275
    :cond_7
    invoke-interface {v13, v14, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    :goto_4
    move-object/from16 v8, v17

    move-object/from16 v11, v18

    const/4 v3, 0x0

    const/4 v6, 0x1

    goto/16 :goto_2

    .line 279
    :cond_8
    move-object/from16 v17, v8

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v3

    if-nez v3, :cond_9

    .line 280
    invoke-interface {v9}, Ljava/util/Iterator;->remove()V

    goto :goto_5

    .line 282
    :cond_9
    invoke-interface {v7, v10, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    :goto_5
    move-object/from16 v8, v17

    const/4 v3, 0x0

    const/4 v6, 0x1

    goto/16 :goto_1

    .line 285
    :cond_a
    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v3

    if-lez v3, :cond_b

    .line 286
    iput-object v7, v0, Lcom/xiaomi/abtest/a/a;->r:Ljava/util/Map;

    .line 289
    :cond_b
    if-eqz p2, :cond_c

    .line 290
    const-string v3, "parse whitelist finished. It takes %s ms from the beginning of reading the local config to now"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    .line 291
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    iget-wide v9, v0, Lcom/xiaomi/abtest/a/a;->z:J

    sub-long/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/4 v7, 0x0

    aput-object v0, v6, v7

    .line 290
    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 296
    :cond_c
    nop

    .line 297
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 298
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_7

    .line 296
    :catchall_0
    move-exception v0

    move-object v3, v5

    goto :goto_8

    .line 293
    :catch_0
    move-exception v0

    move-object v3, v5

    goto :goto_6

    .line 296
    :catchall_1
    move-exception v0

    goto :goto_8

    .line 293
    :catch_1
    move-exception v0

    .line 294
    :goto_6
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "parseExpConfig error : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/xiaomi/abtest/d/k;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 296
    if-eqz v3, :cond_d

    .line 297
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 298
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    :goto_7
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-static {v1, v0}, Lcom/xiaomi/abtest/d/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    nop

    .line 303
    :cond_d
    return-void

    .line 296
    :goto_8
    if-eqz v3, :cond_e

    .line 297
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    .line 298
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-static {v1, v3}, Lcom/xiaomi/abtest/d/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    :cond_e
    throw v0
.end method

.method private a(Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/abtest/ExperimentInfo;",
            ">;)V"
        }
    .end annotation

    .line 334
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const-string v2, "/ExpLayer/NonOverLapDomain/"

    const-string v3, "/ExpLayer/ExpDomain/"

    const-string v4, "/LaunchLayer/LaunchDomain/"

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 335
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 336
    invoke-virtual {v5, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 337
    invoke-virtual {v5, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 338
    :cond_1
    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 339
    invoke-virtual {v5, v2, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 345
    :goto_1
    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 346
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/abtest/ExperimentInfo;

    .line 347
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/abtest/ExperimentInfo;

    .line 350
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 351
    invoke-virtual {v2}, Lcom/xiaomi/abtest/ExperimentInfo;->getParams()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 352
    invoke-virtual {v1}, Lcom/xiaomi/abtest/ExperimentInfo;->getParams()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 354
    invoke-virtual {v1, v3}, Lcom/xiaomi/abtest/ExperimentInfo;->setParams(Ljava/util/Map;)Lcom/xiaomi/abtest/ExperimentInfo;

    .line 356
    :cond_2
    goto :goto_0

    .line 358
    :cond_3
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 361
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 362
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 363
    invoke-virtual {v6, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 365
    invoke-virtual {v6, v4, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 366
    invoke-interface {p1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 367
    goto :goto_2

    .line 370
    :cond_4
    invoke-virtual {v6, v4, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 371
    invoke-interface {p1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 372
    goto :goto_2

    .line 375
    :cond_5
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v0, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v0, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    :cond_6
    goto :goto_2

    .line 379
    :cond_7
    invoke-interface {p1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 380
    return-void
.end method

.method static synthetic a(Lcom/xiaomi/abtest/a/a;)Z
    .locals 0

    .line 36
    iget-boolean p0, p0, Lcom/xiaomi/abtest/a/a;->t:Z

    return p0
.end method

.method static synthetic a(Lcom/xiaomi/abtest/a/a;Z)Z
    .locals 0

    .line 36
    iput-boolean p1, p0, Lcom/xiaomi/abtest/a/a;->t:Z

    return p1
.end method

.method public static b()Lcom/xiaomi/abtest/a/a;
    .locals 1

    .line 100
    sget-object v0, Lcom/xiaomi/abtest/a/a;->y:Lcom/xiaomi/abtest/a/a;

    return-object v0
.end method

.method private b(Lorg/json/JSONObject;)Lcom/xiaomi/abtest/c/e;
    .locals 17

    .line 491
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string/jumbo v2, "type"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->valueOf(Ljava/lang/String;)Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    move-result-object v6

    .line 492
    const-string/jumbo v2, "status"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;->valueOf(Ljava/lang/String;)Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    move-result-object v8

    .line 493
    nop

    .line 494
    sget-object v2, Lcom/xiaomi/abtest/a/f;->a:[I

    invoke-virtual {v6}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const-string v3, "conditionString"

    const-string v4, "hashSeed"

    const-string v5, "bucketIds"

    const-string v7, "ExpPlatformManager"

    const/4 v13, 0x0

    const-string v9, "fPath"

    const-string/jumbo v10, "xPath"

    const-string v11, "fid"

    const-string v12, "id"

    const-string v14, "diversionType"

    const-string v15, "name"

    const/16 v16, 0x0

    packed-switch v2, :pswitch_data_0

    move-object/from16 v2, v16

    goto/16 :goto_2

    .line 544
    :pswitch_0
    invoke-virtual {v1, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/abtest/EnumType$DiversionType;->valueOf(Ljava/lang/String;)Lcom/xiaomi/abtest/EnumType$DiversionType;

    move-result-object v2

    .line 545
    if-nez v2, :cond_0

    .line 546
    invoke-virtual {v1, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "invalid diversionType:"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/xiaomi/abtest/d/k;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    return-object v16

    .line 549
    :cond_0
    new-instance v16, Lcom/xiaomi/abtest/c/c;

    invoke-virtual {v1, v12}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 550
    invoke-virtual {v1, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v12

    .line 551
    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v3, v16

    move v4, v5

    move-object v5, v7

    move v7, v11

    move v9, v12

    move-object v10, v2

    move-object v11, v14

    move-object v12, v15

    invoke-direct/range {v3 .. v12}, Lcom/xiaomi/abtest/c/c;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;ILcom/xiaomi/abtest/EnumType$DiversionType;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v2, v16

    goto/16 :goto_2

    .line 533
    :pswitch_1
    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 534
    new-instance v14, Ljava/util/TreeSet;

    invoke-direct {v14}, Ljava/util/TreeSet;-><init>()V

    .line 535
    move v4, v13

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 536
    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->optInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 535
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 538
    :cond_1
    new-instance v16, Lcom/xiaomi/abtest/c/d;

    invoke-virtual {v1, v12}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 539
    invoke-virtual {v1, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    .line 540
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v3, v16

    move-object v9, v14

    move-object v10, v2

    invoke-direct/range {v3 .. v12}, Lcom/xiaomi/abtest/c/d;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Ljava/util/TreeSet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    move-object/from16 v2, v16

    goto/16 :goto_2

    .line 508
    :pswitch_2
    invoke-virtual {v1, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 509
    iget-object v3, v0, Lcom/xiaomi/abtest/a/a;->x:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 510
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 511
    const-string v3, "LaunchLayer"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 512
    const-string v3, "ExpLayer"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 513
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    iget-object v5, v0, Lcom/xiaomi/abtest/a/a;->x:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 514
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "the layerName "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "doesn\'t meet the filter conditions, skip it!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    return-object v16

    .line 519
    :cond_3
    invoke-virtual {v1, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/abtest/EnumType$DiversionType;->valueOf(Ljava/lang/String;)Lcom/xiaomi/abtest/EnumType$DiversionType;

    move-result-object v2

    .line 520
    if-nez v2, :cond_4

    .line 521
    invoke-virtual {v1, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "invalid diversionType:%s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/xiaomi/abtest/d/k;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    return-object v16

    .line 525
    :cond_4
    new-instance v16, Lcom/xiaomi/abtest/c/g;

    invoke-virtual {v1, v12}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 526
    invoke-virtual {v1, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v11

    .line 527
    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v3, v16

    move v4, v5

    move-object v5, v7

    move v7, v11

    move-object v9, v2

    move v10, v12

    move-object v11, v14

    move-object v12, v15

    invoke-direct/range {v3 .. v12}, Lcom/xiaomi/abtest/c/g;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Lcom/xiaomi/abtest/EnumType$DiversionType;ILjava/lang/String;Ljava/lang/String;)V

    .line 530
    move-object/from16 v2, v16

    goto :goto_2

    .line 496
    :pswitch_3
    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 497
    new-instance v14, Ljava/util/TreeSet;

    invoke-direct {v14}, Ljava/util/TreeSet;-><init>()V

    .line 498
    move v4, v13

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_5

    .line 499
    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->optInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 498
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 501
    :cond_5
    new-instance v16, Lcom/xiaomi/abtest/c/b;

    invoke-virtual {v1, v12}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 502
    invoke-virtual {v1, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    .line 503
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v3, v16

    move-object v9, v14

    move-object v10, v2

    invoke-direct/range {v3 .. v12}, Lcom/xiaomi/abtest/c/b;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Ljava/util/TreeSet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    move-object/from16 v2, v16

    .line 556
    :goto_2
    const-string v3, "parameters"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 557
    invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 558
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 560
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 561
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 562
    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    goto :goto_3

    .line 564
    :cond_6
    invoke-virtual {v2, v5}, Lcom/xiaomi/abtest/c/e;->a(Ljava/util/Map;)V

    .line 566
    const-string v3, "children"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 567
    if-eqz v1, :cond_8

    .line 568
    nop

    :goto_4
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v13, v3, :cond_8

    .line 569
    invoke-virtual {v1, v13}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/xiaomi/abtest/a/a;->b(Lorg/json/JSONObject;)Lcom/xiaomi/abtest/c/e;

    move-result-object v3

    .line 570
    if-eqz v3, :cond_7

    .line 571
    invoke-virtual {v2, v3}, Lcom/xiaomi/abtest/c/e;->a(Lcom/xiaomi/abtest/c/e;)V

    goto :goto_5

    .line 573
    :cond_7
    invoke-virtual {v1, v13}, Lorg/json/JSONArray;->remove(I)Ljava/lang/Object;

    .line 574
    add-int/lit8 v13, v13, -0x1

    .line 568
    :goto_5
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 579
    :cond_8
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/xiaomi/abtest/a/a;)Z
    .locals 0

    .line 36
    iget-boolean p0, p0, Lcom/xiaomi/abtest/a/a;->u:Z

    return p0
.end method

.method static synthetic c(Lcom/xiaomi/abtest/a/a;)Landroid/os/Handler;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/xiaomi/abtest/a/a;->v:Landroid/os/Handler;

    return-object p0
.end method

.method private d()V
    .locals 5

    .line 131
    iget v0, p0, Lcom/xiaomi/abtest/a/a;->s:I

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x1c20

    .line 132
    :goto_0
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/xiaomi/abtest/a/a;->v:Landroid/os/Handler;

    .line 133
    new-instance v1, Lcom/xiaomi/abtest/a/c;

    invoke-direct {v1, p0, v0}, Lcom/xiaomi/abtest/a/c;-><init>(Lcom/xiaomi/abtest/a/a;I)V

    .line 143
    iget-object v2, p0, Lcom/xiaomi/abtest/a/a;->v:Landroid/os/Handler;

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v3, v0

    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 144
    return-void
.end method

.method static synthetic d(Lcom/xiaomi/abtest/a/a;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/xiaomi/abtest/a/a;->e()V

    return-void
.end method

.method private e()V
    .locals 5

    .line 187
    iget-object v0, p0, Lcom/xiaomi/abtest/a/a;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const-string v1, "ExpPlatformManager"

    if-nez v0, :cond_0

    .line 188
    const-string v0, "no appNames to load remote configuration"

    invoke-static {v1, v0}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 192
    iget-object v2, p0, Lcom/xiaomi/abtest/a/a;->p:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 193
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    const-string v3, "_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    goto :goto_0

    .line 197
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_4

    .line 198
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://cdn.exp.xiaomi.com/service/getClientExpConf/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 199
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v3}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    iget-object v0, p0, Lcom/xiaomi/abtest/a/a;->x:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 201
    const-string v0, "/"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    iget-object v0, p0, Lcom/xiaomi/abtest/a/a;->x:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    :cond_2
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "load remote configuration, url: : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/abtest/d/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 207
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "load remote configuration, response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    if-eqz v0, :cond_3

    .line 209
    invoke-direct {p0, v0, v4}, Lcom/xiaomi/abtest/a/a;->a(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    :cond_3
    goto :goto_1

    .line 211
    :catch_0
    move-exception v0

    .line 212
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "load remote configuration error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/xiaomi/abtest/d/k;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :cond_4
    :goto_1
    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/abtest/b/a;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/abtest/b/a;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/abtest/ExperimentInfo;",
            ">;"
        }
    .end annotation

    .line 385
    iget-object v0, p0, Lcom/xiaomi/abtest/a/a;->q:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/abtest/c/b;

    .line 386
    const-string v1, "ExpPlatformManager"

    if-nez v0, :cond_0

    .line 387
    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->d()Ljava/lang/String;

    move-result-object p1

    filled-new-array {v0, p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "no appDomain found for appId:%s,appName:%s"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/xiaomi/abtest/d/k;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    const/4 p1, 0x0

    return-object p1

    .line 391
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 392
    invoke-virtual {v0, p1, v2}, Lcom/xiaomi/abtest/c/b;->a(Lcom/xiaomi/abtest/b/a;Ljava/util/List;)V

    .line 394
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 396
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/abtest/c/e;

    .line 397
    new-instance v5, Lcom/xiaomi/abtest/ExperimentInfo;

    invoke-direct {v5}, Lcom/xiaomi/abtest/ExperimentInfo;-><init>()V

    .line 398
    invoke-virtual {v4}, Lcom/xiaomi/abtest/c/e;->a()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/xiaomi/abtest/ExperimentInfo;->setExpId(I)Lcom/xiaomi/abtest/ExperimentInfo;

    .line 399
    invoke-virtual {v4}, Lcom/xiaomi/abtest/c/e;->d()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/xiaomi/abtest/ExperimentInfo;->setContainerId(I)V

    .line 400
    invoke-virtual {v4}, Lcom/xiaomi/abtest/c/e;->d()I

    move-result v6

    invoke-direct {p0, v6, v0}, Lcom/xiaomi/abtest/a/a;->a(ILcom/xiaomi/abtest/c/e;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/xiaomi/abtest/ExperimentInfo;->setLayerId(I)V

    .line 401
    invoke-virtual {v4}, Lcom/xiaomi/abtest/c/e;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/xiaomi/abtest/ExperimentInfo;->setXpath(Ljava/lang/String;)Lcom/xiaomi/abtest/ExperimentInfo;

    .line 402
    invoke-virtual {v4}, Lcom/xiaomi/abtest/c/e;->i()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/xiaomi/abtest/ExperimentInfo;->setParams(Ljava/util/Map;)Lcom/xiaomi/abtest/ExperimentInfo;

    .line 403
    invoke-virtual {v4}, Lcom/xiaomi/abtest/c/e;->k()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    goto :goto_0

    .line 406
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get data from the expInfo: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    iget-object v0, p0, Lcom/xiaomi/abtest/a/a;->r:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/abtest/a/a;->r:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 410
    iget-object v0, p0, Lcom/xiaomi/abtest/a/a;->r:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    .line 411
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 412
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    goto :goto_1

    .line 414
    :cond_2
    goto :goto_2

    .line 415
    :cond_3
    const-string p1, "no data needed in whitelist"

    invoke-static {v1, p1}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    :goto_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "get data from the expInfo and whitelist: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    invoke-direct {p0, v3}, Lcom/xiaomi/abtest/a/a;->a(Ljava/util/Map;)V

    .line 423
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "get data from the expInfo and whitelist and launch params: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    return-object v3
.end method

.method public a()V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/xiaomi/abtest/a/a;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 87
    iget-object v0, p0, Lcom/xiaomi/abtest/a/a;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 88
    iget-object v0, p0, Lcom/xiaomi/abtest/a/a;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 89
    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/abtest/a/a;->s:I

    .line 90
    iput-boolean v0, p0, Lcom/xiaomi/abtest/a/a;->u:Z

    .line 91
    iput-boolean v0, p0, Lcom/xiaomi/abtest/a/a;->w:Z

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/abtest/a/a;->t:Z

    .line 93
    const-string v0, ""

    iput-object v0, p0, Lcom/xiaomi/abtest/a/a;->x:Ljava/lang/String;

    .line 94
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/abtest/a/a;->z:J

    .line 95
    invoke-static {}, Lcom/xiaomi/abtest/d/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/xiaomi/abtest/a/a;->A:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {v0, v1}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 96
    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/abtest/a/a;->y:Lcom/xiaomi/abtest/a/a;

    .line 97
    return-void
.end method

.method public a(Lcom/xiaomi/abtest/ABTest$OnLoadRemoteConfigListener;)V
    .locals 2

    .line 175
    sget-object v0, Lcom/xiaomi/abtest/a/a;->o:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/xiaomi/abtest/a/d;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/abtest/a/d;-><init>(Lcom/xiaomi/abtest/a/a;Lcom/xiaomi/abtest/ABTest$OnLoadRemoteConfigListener;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 184
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .line 151
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    const-string p1, "ExpPlatformManager"

    const-string v0, "appName is empty, skip it!"

    invoke-static {p1, v0}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    return-void

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/abtest/a/a;->p:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 157
    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/xiaomi/abtest/a/a;->a(Lcom/xiaomi/abtest/ABTest$OnLoadRemoteConfigListener;)V

    .line 158
    iget-boolean p1, p0, Lcom/xiaomi/abtest/a/a;->w:Z

    if-nez p1, :cond_1

    .line 159
    invoke-direct {p0}, Lcom/xiaomi/abtest/a/a;->d()V

    .line 161
    :cond_1
    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 147
    iput-boolean p1, p0, Lcom/xiaomi/abtest/a/a;->u:Z

    .line 148
    return-void
.end method

.method public c()V
    .locals 5

    .line 164
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/abtest/a/a;->z:J

    .line 165
    const-string v0, "abtest"

    invoke-static {v0}, Lcom/xiaomi/abtest/d/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 166
    nop

    .line 167
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/xiaomi/abtest/a/a;->z:J

    sub-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    .line 166
    const-string v2, "load local config finished, cost: %s ms"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ExpPlatformManager"

    invoke-static {v2, v1}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "localConfig : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-static {v0}, Lcom/xiaomi/abtest/d/l;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/abtest/a/a;->a(Ljava/lang/String;Z)V

    .line 172
    :cond_0
    return-void
.end method
