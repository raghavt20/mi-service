.class Lcom/xiaomi/abtest/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# instance fields
.field final synthetic a:Lcom/xiaomi/abtest/a/a;

.field private b:I

.field private c:Z


# direct methods
.method constructor <init>(Lcom/xiaomi/abtest/a/a;)V
    .locals 0

    .line 446
    iput-object p1, p0, Lcom/xiaomi/abtest/a/e;->a:Lcom/xiaomi/abtest/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448
    const/4 p1, 0x0

    iput p1, p0, Lcom/xiaomi/abtest/a/e;->b:I

    .line 449
    iput-boolean p1, p0, Lcom/xiaomi/abtest/a/e;->c:Z

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 453
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .line 487
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .line 468
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .line 464
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "outState"    # Landroid/os/Bundle;

    .line 483
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .line 457
    iget v0, p0, Lcom/xiaomi/abtest/a/e;->b:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/abtest/a/e;->b:I

    if-lt v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/xiaomi/abtest/a/e;->c:Z

    if-nez v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/xiaomi/abtest/a/e;->a:Lcom/xiaomi/abtest/a/a;

    invoke-static {v0, v1}, Lcom/xiaomi/abtest/a/a;->a(Lcom/xiaomi/abtest/a/a;Z)Z

    .line 460
    :cond_0
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .line 472
    invoke-virtual {p1}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/abtest/a/e;->c:Z

    .line 473
    iget v1, p0, Lcom/xiaomi/abtest/a/e;->b:I

    if-lez v1, :cond_0

    .line 474
    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/xiaomi/abtest/a/e;->b:I

    .line 476
    :cond_0
    iget v1, p0, Lcom/xiaomi/abtest/a/e;->b:I

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    .line 477
    iget-object v0, p0, Lcom/xiaomi/abtest/a/e;->a:Lcom/xiaomi/abtest/a/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/abtest/a/a;->a(Lcom/xiaomi/abtest/a/a;Z)Z

    .line 479
    :cond_1
    return-void
.end method
