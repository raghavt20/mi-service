public class com.xiaomi.abtest.a.a {
	 /* # static fields */
	 private static final java.lang.String a;
	 private static final java.lang.String b;
	 private static final java.lang.String c;
	 private static final java.lang.String d;
	 private static final java.lang.String e;
	 private static final java.lang.String f;
	 private static final java.lang.String g;
	 private static final java.lang.String h;
	 private static final java.lang.String i;
	 private static final java.lang.String j;
	 private static final java.lang.String k;
	 private static final java.lang.String l;
	 private static final java.lang.String m;
	 private static final java.lang.String n;
	 private static final java.util.concurrent.ExecutorService o;
	 private static volatile com.xiaomi.abtest.a.a y;
	 /* # instance fields */
	 private android.app.Application$ActivityLifecycleCallbacks A;
	 private java.util.Set p;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.util.Map q;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/xiaomi/abtest/c/b;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map r;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/xiaomi/abtest/ExperimentInfo;", */
/* ">;>;>;" */
/* } */
} // .end annotation
} // .end field
private Integer s;
private Boolean t;
private Boolean u;
private android.os.Handler v;
private Boolean w;
private java.lang.String x;
private Long z;
/* # direct methods */
static com.xiaomi.abtest.a.a ( ) {
/* .locals 1 */
/* .line 53 */
java.util.concurrent.Executors .newSingleThreadExecutor ( );
return;
} // .end method
private com.xiaomi.abtest.a.a ( ) {
/* .locals 2 */
/* .line 103 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 55 */
/* new-instance v0, Ljava/util/TreeSet; */
/* invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V */
this.p = v0;
/* .line 57 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.q = v0;
/* .line 59 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.r = v0;
/* .line 62 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/xiaomi/abtest/a/a;->t:Z */
/* .line 446 */
/* new-instance v0, Lcom/xiaomi/abtest/a/e; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/abtest/a/e;-><init>(Lcom/xiaomi/abtest/a/a;)V */
this.A = v0;
/* .line 104 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 105 */
(( com.xiaomi.abtest.ABTestConfig ) p1 ).getLayerName ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/ABTestConfig;->getLayerName()Ljava/lang/String;
this.x = v0;
/* .line 106 */
v0 = (( com.xiaomi.abtest.ABTestConfig ) p1 ).isDisableLoadTimer ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/ABTestConfig;->isDisableLoadTimer()Z
/* iput-boolean v0, p0, Lcom/xiaomi/abtest/a/a;->w:Z */
/* .line 107 */
/* if-nez v0, :cond_1 */
/* .line 108 */
v0 = (( com.xiaomi.abtest.ABTestConfig ) p1 ).getLoadConfigInterval ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/ABTestConfig;->getLoadConfigInterval()I
/* if-lez v0, :cond_1 */
/* .line 110 */
v0 = (( com.xiaomi.abtest.ABTestConfig ) p1 ).getLoadConfigInterval ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/ABTestConfig;->getLoadConfigInterval()I
/* const/16 v1, 0x1c20 */
/* if-ge v0, v1, :cond_0 */
/* .line 111 */
/* iput v1, p0, Lcom/xiaomi/abtest/a/a;->s:I */
/* .line 114 */
} // :cond_0
p1 = (( com.xiaomi.abtest.ABTestConfig ) p1 ).getLoadConfigInterval ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/ABTestConfig;->getLoadConfigInterval()I
/* iput p1, p0, Lcom/xiaomi/abtest/a/a;->s:I */
/* .line 120 */
} // :cond_1
} // :goto_0
p1 = com.xiaomi.abtest.a.a.o;
/* new-instance v0, Lcom/xiaomi/abtest/a/b; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/abtest/a/b;-><init>(Lcom/xiaomi/abtest/a/a;)V */
/* .line 127 */
com.xiaomi.abtest.d.a .a ( );
/* invoke-direct {p0, p1}, Lcom/xiaomi/abtest/a/a;->a(Landroid/content/Context;)V */
/* .line 128 */
return;
} // .end method
private Integer a ( Integer p0, Object p1 ) {
/* .locals 1 */
/* .line 430 */
v0 = (( com.xiaomi.abtest.c.e ) p2 ).a ( ); // invoke-virtual {p2}, Lcom/xiaomi/abtest/c/e;->a()I
/* if-ne v0, p1, :cond_0 */
p1 = (( com.xiaomi.abtest.c.e ) p2 ).d ( ); // invoke-virtual {p2}, Lcom/xiaomi/abtest/c/e;->d()I
/* .line 431 */
} // :cond_0
(( com.xiaomi.abtest.c.e ) p2 ).g ( ); // invoke-virtual {p2}, Lcom/xiaomi/abtest/c/e;->g()Ljava/util/List;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 432 */
(( com.xiaomi.abtest.c.e ) p2 ).g ( ); // invoke-virtual {p2}, Lcom/xiaomi/abtest/c/e;->g()Ljava/util/List;
v0 = } // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* check-cast v0, Lcom/xiaomi/abtest/c/e; */
/* .line 433 */
v0 = /* invoke-direct {p0, p1, v0}, Lcom/xiaomi/abtest/a/a;->a(ILcom/xiaomi/abtest/c/e;)I */
/* .line 434 */
/* if-lez v0, :cond_1 */
/* .line 435 */
/* .line 437 */
} // :cond_1
/* .line 439 */
} // :cond_2
int p1 = 0; // const/4 p1, 0x0
} // .end method
private com.xiaomi.abtest.ExperimentInfo a ( org.json.JSONObject p0 ) {
/* .locals 5 */
/* .line 306 */
/* if-nez p1, :cond_0 */
/* .line 307 */
int p1 = 0; // const/4 p1, 0x0
/* .line 310 */
} // :cond_0
/* new-instance v0, Lcom/xiaomi/abtest/ExperimentInfo; */
/* invoke-direct {v0}, Lcom/xiaomi/abtest/ExperimentInfo;-><init>()V */
/* .line 311 */
final String v1 = "expId"; // const-string v1, "expId"
v1 = (( org.json.JSONObject ) p1 ).optInt ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
/* iput v1, v0, Lcom/xiaomi/abtest/ExperimentInfo;->expId:I */
/* .line 312 */
/* const-string/jumbo v1, "xPath" */
(( org.json.JSONObject ) p1 ).optString ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
this.xpath = v1;
/* .line 313 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
this.params = v1;
/* .line 315 */
final String v1 = "params"; // const-string v1, "params"
(( org.json.JSONObject ) p1 ).optJSONObject ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 316 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 317 */
(( org.json.JSONObject ) p1 ).keys ( ); // invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
/* .line 318 */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 319 */
/* check-cast v2, Ljava/lang/String; */
/* .line 320 */
(( org.json.JSONObject ) p1 ).optString ( v2 ); // invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 321 */
v4 = this.params;
/* .line 322 */
/* .line 325 */
} // :cond_1
} // .end method
private void a ( android.content.Context p0 ) {
/* .locals 1 */
/* .line 443 */
(( android.content.Context ) p1 ).getApplicationContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
/* check-cast p1, Landroid/app/Application; */
v0 = this.A;
(( android.app.Application ) p1 ).registerActivityLifecycleCallbacks ( v0 ); // invoke-virtual {p1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V
/* .line 444 */
return;
} // .end method
public static void a ( com.xiaomi.abtest.ABTestConfig p0 ) {
/* .locals 2 */
/* .line 76 */
v0 = com.xiaomi.abtest.a.a.y;
/* if-nez v0, :cond_1 */
/* .line 77 */
/* const-class v0, Lcom/xiaomi/abtest/a/a; */
/* monitor-enter v0 */
/* .line 78 */
try { // :try_start_0
v1 = com.xiaomi.abtest.a.a.y;
/* if-nez v1, :cond_0 */
/* .line 79 */
/* new-instance v1, Lcom/xiaomi/abtest/a/a; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/abtest/a/a;-><init>(Lcom/xiaomi/abtest/ABTestConfig;)V */
/* .line 81 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw p0 */
/* .line 83 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void a ( java.lang.String p0, Boolean p1 ) {
/* .locals 19 */
/* .line 218 */
/* move-object/from16 v0, p0 */
final String v1 = "abtest"; // const-string v1, "abtest"
final String v2 = "save the data to local file, data : "; // const-string v2, "save the data to local file, data : "
final String v3 = "parse expConfig start"; // const-string v3, "parse expConfig start"
final String v4 = "ExpPlatformManager"; // const-string v4, "ExpPlatformManager"
com.xiaomi.abtest.d.k .a ( v4,v3 );
/* .line 219 */
/* nop */
/* .line 221 */
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
/* new-instance v5, Lorg/json/JSONObject; */
/* move-object/from16 v6, p1 */
/* invoke-direct {v5, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 222 */
try { // :try_start_1
final String v3 = "expInfo"; // const-string v3, "expInfo"
(( org.json.JSONObject ) v5 ).optJSONObject ( v3 ); // invoke-virtual {v5, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 224 */
(( org.json.JSONObject ) v3 ).keys ( ); // invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
/* .line 225 */
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 226 */
/* check-cast v7, Ljava/lang/String; */
/* .line 227 */
(( org.json.JSONObject ) v3 ).optJSONObject ( v7 ); // invoke-virtual {v3, v7}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 228 */
/* invoke-direct {v0, v8}, Lcom/xiaomi/abtest/a/a;->b(Lorg/json/JSONObject;)Lcom/xiaomi/abtest/c/e; */
/* .line 229 */
v9 = this.q;
/* check-cast v8, Lcom/xiaomi/abtest/c/b; */
/* .line 230 */
/* .line 232 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
int v6 = 1; // const/4 v6, 0x1
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 233 */
final String v7 = "parse expInfo finished.It takes %s ms from the beginning of reading the local config to now"; // const-string v7, "parse expInfo finished.It takes %s ms from the beginning of reading the local config to now"
/* new-array v8, v6, [Ljava/lang/Object; */
/* .line 234 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v9 */
/* iget-wide v11, v0, Lcom/xiaomi/abtest/a/a;->z:J */
/* sub-long/2addr v9, v11 */
java.lang.Long .valueOf ( v9,v10 );
/* aput-object v9, v8, v3 */
/* .line 233 */
java.lang.String .format ( v7,v8 );
com.xiaomi.abtest.d.k .a ( v4,v7 );
/* .line 237 */
} // :cond_1
/* new-instance v7, Ljava/util/HashMap; */
/* invoke-direct {v7}, Ljava/util/HashMap;-><init>()V */
/* .line 238 */
/* const-string/jumbo v8, "whitelist" */
(( org.json.JSONObject ) v5 ).optJSONObject ( v8 ); // invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 239 */
(( org.json.JSONObject ) v8 ).keys ( ); // invoke-virtual {v8}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
/* .line 240 */
v10 = } // :goto_1
if ( v10 != null) { // if-eqz v10, :cond_a
/* .line 241 */
/* check-cast v10, Ljava/lang/String; */
/* .line 242 */
(( org.json.JSONObject ) v8 ).optJSONObject ( v10 ); // invoke-virtual {v8, v10}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 243 */
(( org.json.JSONObject ) v11 ).keys ( ); // invoke-virtual {v11}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
/* .line 246 */
/* new-instance v13, Ljava/util/HashMap; */
/* invoke-direct {v13}, Ljava/util/HashMap;-><init>()V */
/* .line 248 */
v14 = } // :goto_2
if ( v14 != null) { // if-eqz v14, :cond_8
/* .line 249 */
/* check-cast v14, Ljava/lang/String; */
/* .line 250 */
(( org.json.JSONObject ) v11 ).optJSONObject ( v14 ); // invoke-virtual {v11, v14}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 251 */
(( org.json.JSONObject ) v15 ).keys ( ); // invoke-virtual {v15}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
/* .line 253 */
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
/* .line 254 */
} // :goto_3
v17 = /* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->hasNext()Z */
if ( v17 != null) { // if-eqz v17, :cond_6
/* .line 255 */
/* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
/* move-object/from16 v6, v17 */
/* check-cast v6, Ljava/lang/String; */
/* .line 257 */
/* move-object/from16 v17, v8 */
v8 = this.x;
v8 = android.text.TextUtils .isEmpty ( v8 );
/* if-nez v8, :cond_3 */
/* .line 258 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v18, v11 */
final String v11 = "/"; // const-string v11, "/"
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = "/ExpLayer/ExpDomain/"; // const-string v11, "/ExpLayer/ExpDomain/"
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v8 = (( java.lang.String ) v8 ).length ( ); // invoke-virtual {v8}, Ljava/lang/String;->length()I
(( java.lang.String ) v6 ).substring ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 259 */
v11 = android.text.TextUtils .isEmpty ( v8 );
/* if-nez v11, :cond_2 */
(( java.lang.String ) v8 ).toUpperCase ( ); // invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
v11 = this.x;
(( java.lang.String ) v11 ).toUpperCase ( ); // invoke-virtual {v11}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
v8 = (( java.lang.String ) v8 ).contains ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v8, :cond_4 */
/* .line 260 */
} // :cond_2
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v11, "the fPath " */
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = "doesn\'t meet the filter conditions, skip it!"; // const-string v8, "doesn\'t meet the filter conditions, skip it!"
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.abtest.d.k .a ( v4,v6 );
/* .line 261 */
/* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->remove()V */
/* .line 262 */
/* move-object/from16 v8, v17 */
/* move-object/from16 v11, v18 */
int v6 = 1; // const/4 v6, 0x1
/* .line 257 */
} // :cond_3
/* move-object/from16 v18, v11 */
/* .line 266 */
} // :cond_4
(( org.json.JSONObject ) v15 ).optJSONObject ( v6 ); // invoke-virtual {v15, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* invoke-direct {v0, v8}, Lcom/xiaomi/abtest/a/a;->a(Lorg/json/JSONObject;)Lcom/xiaomi/abtest/ExperimentInfo; */
/* .line 267 */
if ( v8 != null) { // if-eqz v8, :cond_5
/* .line 268 */
/* .line 270 */
} // :cond_5
/* move-object/from16 v8, v17 */
/* move-object/from16 v11, v18 */
int v6 = 1; // const/4 v6, 0x1
/* goto/16 :goto_3 */
/* .line 272 */
} // :cond_6
/* move-object/from16 v17, v8 */
v6 = /* move-object/from16 v18, v11 */
/* if-nez v6, :cond_7 */
/* .line 273 */
/* .line 275 */
} // :cond_7
/* .line 277 */
} // :goto_4
/* move-object/from16 v8, v17 */
/* move-object/from16 v11, v18 */
int v3 = 0; // const/4 v3, 0x0
int v6 = 1; // const/4 v6, 0x1
/* goto/16 :goto_2 */
/* .line 279 */
} // :cond_8
v3 = /* move-object/from16 v17, v8 */
/* if-nez v3, :cond_9 */
/* .line 280 */
/* .line 282 */
} // :cond_9
/* .line 284 */
} // :goto_5
/* move-object/from16 v8, v17 */
int v3 = 0; // const/4 v3, 0x0
int v6 = 1; // const/4 v6, 0x1
/* goto/16 :goto_1 */
/* .line 285 */
v3 = } // :cond_a
/* if-lez v3, :cond_b */
/* .line 286 */
this.r = v7;
/* .line 289 */
} // :cond_b
if ( p2 != null) { // if-eqz p2, :cond_c
/* .line 290 */
final String v3 = "parse whitelist finished.It takes %s ms from the beginning of reading the local config to now"; // const-string v3, "parse whitelist finished.It takes %s ms from the beginning of reading the local config to now"
int v6 = 1; // const/4 v6, 0x1
/* new-array v6, v6, [Ljava/lang/Object; */
/* .line 291 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v7 */
/* iget-wide v9, v0, Lcom/xiaomi/abtest/a/a;->z:J */
/* sub-long/2addr v7, v9 */
java.lang.Long .valueOf ( v7,v8 );
int v7 = 0; // const/4 v7, 0x0
/* aput-object v0, v6, v7 */
/* .line 290 */
java.lang.String .format ( v3,v6 );
com.xiaomi.abtest.d.k .a ( v4,v0 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 296 */
} // :cond_c
/* nop */
/* .line 297 */
(( org.json.JSONObject ) v5 ).toString ( ); // invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* .line 298 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 296 */
/* :catchall_0 */
/* move-exception v0 */
/* move-object v3, v5 */
/* .line 293 */
/* :catch_0 */
/* move-exception v0 */
/* move-object v3, v5 */
/* .line 296 */
/* :catchall_1 */
/* move-exception v0 */
/* .line 293 */
/* :catch_1 */
/* move-exception v0 */
/* .line 294 */
} // :goto_6
try { // :try_start_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "parseExpConfig error : "; // const-string v6, "parseExpConfig error : "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.abtest.d.k .b ( v4,v0 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 296 */
if ( v3 != null) { // if-eqz v3, :cond_d
/* .line 297 */
(( org.json.JSONObject ) v3 ).toString ( ); // invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* .line 298 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_7
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.abtest.d.k .a ( v4,v2 );
/* .line 300 */
com.xiaomi.abtest.d.f .a ( v1,v0 );
/* .line 301 */
/* nop */
/* .line 303 */
} // :cond_d
return;
/* .line 296 */
} // :goto_8
if ( v3 != null) { // if-eqz v3, :cond_e
/* .line 297 */
(( org.json.JSONObject ) v3 ).toString ( ); // invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* .line 298 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.abtest.d.k .a ( v4,v2 );
/* .line 300 */
com.xiaomi.abtest.d.f .a ( v1,v3 );
/* .line 302 */
} // :cond_e
/* throw v0 */
} // .end method
private void a ( java.util.Map p0 ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/xiaomi/abtest/ExperimentInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 334 */
} // :cond_0
v1 = } // :goto_0
final String v2 = "/ExpLayer/NonOverLapDomain/"; // const-string v2, "/ExpLayer/NonOverLapDomain/"
final String v3 = "/ExpLayer/ExpDomain/"; // const-string v3, "/ExpLayer/ExpDomain/"
final String v4 = "/LaunchLayer/LaunchDomain/"; // const-string v4, "/LaunchLayer/LaunchDomain/"
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 335 */
/* check-cast v5, Ljava/lang/String; */
/* .line 336 */
v6 = (( java.lang.String ) v5 ).contains ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 337 */
(( java.lang.String ) v5 ).replaceFirst ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 338 */
} // :cond_1
v3 = (( java.lang.String ) v5 ).contains ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 339 */
(( java.lang.String ) v5 ).replaceFirst ( v2, v4 ); // invoke-virtual {v5, v2, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 345 */
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 346 */
/* check-cast v2, Lcom/xiaomi/abtest/ExperimentInfo; */
/* .line 347 */
/* check-cast v1, Lcom/xiaomi/abtest/ExperimentInfo; */
/* .line 350 */
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
/* .line 351 */
(( com.xiaomi.abtest.ExperimentInfo ) v2 ).getParams ( ); // invoke-virtual {v2}, Lcom/xiaomi/abtest/ExperimentInfo;->getParams()Ljava/util/Map;
/* .line 352 */
(( com.xiaomi.abtest.ExperimentInfo ) v1 ).getParams ( ); // invoke-virtual {v1}, Lcom/xiaomi/abtest/ExperimentInfo;->getParams()Ljava/util/Map;
/* .line 354 */
(( com.xiaomi.abtest.ExperimentInfo ) v1 ).setParams ( v3 ); // invoke-virtual {v1, v3}, Lcom/xiaomi/abtest/ExperimentInfo;->setParams(Ljava/util/Map;)Lcom/xiaomi/abtest/ExperimentInfo;
/* .line 356 */
} // :cond_2
/* .line 358 */
} // :cond_3
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 361 */
v5 = } // :goto_2
if ( v5 != null) { // if-eqz v5, :cond_7
/* check-cast v5, Ljava/util/Map$Entry; */
/* .line 362 */
/* check-cast v6, Ljava/lang/String; */
/* .line 363 */
v7 = (( java.lang.String ) v6 ).contains ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v7 != null) { // if-eqz v7, :cond_6
/* .line 365 */
(( java.lang.String ) v6 ).replaceFirst ( v4, v3 ); // invoke-virtual {v6, v4, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
v8 = /* .line 366 */
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 367 */
/* .line 370 */
} // :cond_4
(( java.lang.String ) v6 ).replaceFirst ( v4, v2 ); // invoke-virtual {v6, v4, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
v8 = /* .line 371 */
if ( v8 != null) { // if-eqz v8, :cond_5
/* .line 372 */
/* .line 375 */
} // :cond_5
/* .line 376 */
/* .line 378 */
} // :cond_6
/* .line 379 */
} // :cond_7
/* .line 380 */
return;
} // .end method
static Boolean a ( Object p0 ) { //synthethic
/* .locals 0 */
/* .line 36 */
/* iget-boolean p0, p0, Lcom/xiaomi/abtest/a/a;->t:Z */
} // .end method
static Boolean a ( Object p0, Boolean p1 ) { //synthethic
/* .locals 0 */
/* .line 36 */
/* iput-boolean p1, p0, Lcom/xiaomi/abtest/a/a;->t:Z */
} // .end method
public static com.xiaomi.abtest.a.a b ( ) {
/* .locals 1 */
/* .line 100 */
v0 = com.xiaomi.abtest.a.a.y;
} // .end method
private com.xiaomi.abtest.c.e b ( org.json.JSONObject p0 ) {
/* .locals 17 */
/* .line 491 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* const-string/jumbo v2, "type" */
(( org.json.JSONObject ) v1 ).optString ( v2 ); // invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
com.xiaomi.abtest.EnumType$FlowUnitType .valueOf ( v2 );
/* .line 492 */
/* const-string/jumbo v2, "status" */
(( org.json.JSONObject ) v1 ).optString ( v2 ); // invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
com.xiaomi.abtest.EnumType$FlowUnitStatus .valueOf ( v2 );
/* .line 493 */
/* nop */
/* .line 494 */
v2 = com.xiaomi.abtest.a.f.a;
v3 = (( com.xiaomi.abtest.EnumType$FlowUnitType ) v6 ).ordinal ( ); // invoke-virtual {v6}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->ordinal()I
/* aget v2, v2, v3 */
final String v3 = "conditionString"; // const-string v3, "conditionString"
final String v4 = "hashSeed"; // const-string v4, "hashSeed"
final String v5 = "bucketIds"; // const-string v5, "bucketIds"
final String v7 = "ExpPlatformManager"; // const-string v7, "ExpPlatformManager"
int v13 = 0; // const/4 v13, 0x0
final String v9 = "fPath"; // const-string v9, "fPath"
/* const-string/jumbo v10, "xPath" */
final String v11 = "fid"; // const-string v11, "fid"
final String v12 = "id"; // const-string v12, "id"
final String v14 = "diversionType"; // const-string v14, "diversionType"
final String v15 = "name"; // const-string v15, "name"
/* const/16 v16, 0x0 */
/* packed-switch v2, :pswitch_data_0 */
/* move-object/from16 v2, v16 */
/* goto/16 :goto_2 */
/* .line 544 */
/* :pswitch_0 */
(( org.json.JSONObject ) v1 ).optString ( v14 ); // invoke-virtual {v1, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
com.xiaomi.abtest.EnumType$DiversionType .valueOf ( v2 );
/* .line 545 */
/* if-nez v2, :cond_0 */
/* .line 546 */
(( org.json.JSONObject ) v1 ).optString ( v14 ); // invoke-virtual {v1, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* filled-new-array {v0}, [Ljava/lang/Object; */
final String v1 = "invalid diversionType:"; // const-string v1, "invalid diversionType:"
java.lang.String .format ( v1,v0 );
com.xiaomi.abtest.d.k .c ( v7,v0 );
/* .line 547 */
/* .line 549 */
} // :cond_0
/* new-instance v16, Lcom/xiaomi/abtest/c/c; */
v5 = (( org.json.JSONObject ) v1 ).optInt ( v12 ); // invoke-virtual {v1, v12}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
(( org.json.JSONObject ) v1 ).optString ( v15 ); // invoke-virtual {v1, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 550 */
v11 = (( org.json.JSONObject ) v1 ).optInt ( v11 ); // invoke-virtual {v1, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
v12 = (( org.json.JSONObject ) v1 ).optInt ( v4 ); // invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
/* .line 551 */
(( org.json.JSONObject ) v1 ).optString ( v10 ); // invoke-virtual {v1, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
(( org.json.JSONObject ) v1 ).optString ( v9 ); // invoke-virtual {v1, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* move-object/from16 v3, v16 */
/* move v4, v5 */
/* move-object v5, v7 */
/* move v7, v11 */
/* move v9, v12 */
/* move-object v10, v2 */
/* move-object v11, v14 */
/* move-object v12, v15 */
/* invoke-direct/range {v3 ..v12}, Lcom/xiaomi/abtest/c/c;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;ILcom/xiaomi/abtest/EnumType$DiversionType;Ljava/lang/String;Ljava/lang/String;)V */
/* move-object/from16 v2, v16 */
/* goto/16 :goto_2 */
/* .line 533 */
/* :pswitch_1 */
(( org.json.JSONObject ) v1 ).optJSONArray ( v5 ); // invoke-virtual {v1, v5}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 534 */
/* new-instance v14, Ljava/util/TreeSet; */
/* invoke-direct {v14}, Ljava/util/TreeSet;-><init>()V */
/* .line 535 */
/* move v4, v13 */
} // :goto_0
v5 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v4, v5, :cond_1 */
/* .line 536 */
v5 = (( org.json.JSONArray ) v2 ).optInt ( v4 ); // invoke-virtual {v2, v4}, Lorg/json/JSONArray;->optInt(I)I
java.lang.Integer .valueOf ( v5 );
(( java.util.TreeSet ) v14 ).add ( v5 ); // invoke-virtual {v14, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
/* .line 535 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 538 */
} // :cond_1
/* new-instance v16, Lcom/xiaomi/abtest/c/d; */
v4 = (( org.json.JSONObject ) v1 ).optInt ( v12 ); // invoke-virtual {v1, v12}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
(( org.json.JSONObject ) v1 ).optString ( v15 ); // invoke-virtual {v1, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 539 */
v7 = (( org.json.JSONObject ) v1 ).optInt ( v11 ); // invoke-virtual {v1, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
/* .line 540 */
(( org.json.JSONObject ) v1 ).optString ( v3 ); // invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
(( org.json.JSONObject ) v1 ).optString ( v10 ); // invoke-virtual {v1, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
(( org.json.JSONObject ) v1 ).optString ( v9 ); // invoke-virtual {v1, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* move-object/from16 v3, v16 */
/* move-object v9, v14 */
/* move-object v10, v2 */
/* invoke-direct/range {v3 ..v12}, Lcom/xiaomi/abtest/c/d;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Ljava/util/TreeSet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 542 */
/* move-object/from16 v2, v16 */
/* goto/16 :goto_2 */
/* .line 508 */
/* :pswitch_2 */
(( org.json.JSONObject ) v1 ).optString ( v15 ); // invoke-virtual {v1, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 509 */
v3 = this.x;
v3 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v3, :cond_3 */
/* .line 510 */
v3 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v3, :cond_2 */
/* .line 511 */
final String v3 = "LaunchLayer"; // const-string v3, "LaunchLayer"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_3 */
/* .line 512 */
final String v3 = "ExpLayer"; // const-string v3, "ExpLayer"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_3 */
/* .line 513 */
(( java.lang.String ) v2 ).toUpperCase ( ); // invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
v5 = this.x;
(( java.lang.String ) v5 ).toUpperCase ( ); // invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).contains ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v3, :cond_3 */
/* .line 514 */
} // :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "the layerName " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "doesn\'t meet the filter conditions, skip it!"; // const-string v1, "doesn\'t meet the filter conditions, skip it!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.abtest.d.k .a ( v7,v0 );
/* .line 515 */
/* .line 519 */
} // :cond_3
(( org.json.JSONObject ) v1 ).optString ( v14 ); // invoke-virtual {v1, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
com.xiaomi.abtest.EnumType$DiversionType .valueOf ( v2 );
/* .line 520 */
/* if-nez v2, :cond_4 */
/* .line 521 */
(( org.json.JSONObject ) v1 ).optString ( v14 ); // invoke-virtual {v1, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* filled-new-array {v0}, [Ljava/lang/Object; */
final String v1 = "invalid diversionType:%s"; // const-string v1, "invalid diversionType:%s"
java.lang.String .format ( v1,v0 );
com.xiaomi.abtest.d.k .c ( v7,v0 );
/* .line 522 */
/* .line 525 */
} // :cond_4
/* new-instance v16, Lcom/xiaomi/abtest/c/g; */
v5 = (( org.json.JSONObject ) v1 ).optInt ( v12 ); // invoke-virtual {v1, v12}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
(( org.json.JSONObject ) v1 ).optString ( v15 ); // invoke-virtual {v1, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 526 */
v11 = (( org.json.JSONObject ) v1 ).optInt ( v11 ); // invoke-virtual {v1, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
/* .line 527 */
v12 = (( org.json.JSONObject ) v1 ).optInt ( v4 ); // invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
(( org.json.JSONObject ) v1 ).optString ( v10 ); // invoke-virtual {v1, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
(( org.json.JSONObject ) v1 ).optString ( v9 ); // invoke-virtual {v1, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* move-object/from16 v3, v16 */
/* move v4, v5 */
/* move-object v5, v7 */
/* move v7, v11 */
/* move-object v9, v2 */
/* move v10, v12 */
/* move-object v11, v14 */
/* move-object v12, v15 */
/* invoke-direct/range {v3 ..v12}, Lcom/xiaomi/abtest/c/g;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Lcom/xiaomi/abtest/EnumType$DiversionType;ILjava/lang/String;Ljava/lang/String;)V */
/* .line 530 */
/* move-object/from16 v2, v16 */
/* .line 496 */
/* :pswitch_3 */
(( org.json.JSONObject ) v1 ).optJSONArray ( v5 ); // invoke-virtual {v1, v5}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 497 */
/* new-instance v14, Ljava/util/TreeSet; */
/* invoke-direct {v14}, Ljava/util/TreeSet;-><init>()V */
/* .line 498 */
/* move v4, v13 */
} // :goto_1
v5 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v4, v5, :cond_5 */
/* .line 499 */
v5 = (( org.json.JSONArray ) v2 ).optInt ( v4 ); // invoke-virtual {v2, v4}, Lorg/json/JSONArray;->optInt(I)I
java.lang.Integer .valueOf ( v5 );
(( java.util.TreeSet ) v14 ).add ( v5 ); // invoke-virtual {v14, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
/* .line 498 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 501 */
} // :cond_5
/* new-instance v16, Lcom/xiaomi/abtest/c/b; */
v4 = (( org.json.JSONObject ) v1 ).optInt ( v12 ); // invoke-virtual {v1, v12}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
(( org.json.JSONObject ) v1 ).optString ( v15 ); // invoke-virtual {v1, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 502 */
v7 = (( org.json.JSONObject ) v1 ).optInt ( v11 ); // invoke-virtual {v1, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
/* .line 503 */
(( org.json.JSONObject ) v1 ).optString ( v3 ); // invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
(( org.json.JSONObject ) v1 ).optString ( v10 ); // invoke-virtual {v1, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
(( org.json.JSONObject ) v1 ).optString ( v9 ); // invoke-virtual {v1, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* move-object/from16 v3, v16 */
/* move-object v9, v14 */
/* move-object v10, v2 */
/* invoke-direct/range {v3 ..v12}, Lcom/xiaomi/abtest/c/b;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Ljava/util/TreeSet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 505 */
/* move-object/from16 v2, v16 */
/* .line 556 */
} // :goto_2
final String v3 = "parameters"; // const-string v3, "parameters"
(( org.json.JSONObject ) v1 ).optJSONObject ( v3 ); // invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 557 */
(( org.json.JSONObject ) v3 ).keys ( ); // invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
/* .line 558 */
/* new-instance v5, Ljava/util/HashMap; */
/* invoke-direct {v5}, Ljava/util/HashMap;-><init>()V */
/* .line 560 */
v6 = } // :goto_3
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 561 */
/* check-cast v6, Ljava/lang/String; */
/* .line 562 */
(( org.json.JSONObject ) v3 ).optString ( v6 ); // invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 563 */
/* .line 564 */
} // :cond_6
(( com.xiaomi.abtest.c.e ) v2 ).a ( v5 ); // invoke-virtual {v2, v5}, Lcom/xiaomi/abtest/c/e;->a(Ljava/util/Map;)V
/* .line 566 */
final String v3 = "children"; // const-string v3, "children"
(( org.json.JSONObject ) v1 ).optJSONArray ( v3 ); // invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 567 */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 568 */
/* nop */
} // :goto_4
v3 = (( org.json.JSONArray ) v1 ).length ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
/* if-ge v13, v3, :cond_8 */
/* .line 569 */
(( org.json.JSONArray ) v1 ).optJSONObject ( v13 ); // invoke-virtual {v1, v13}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;
/* invoke-direct {v0, v3}, Lcom/xiaomi/abtest/a/a;->b(Lorg/json/JSONObject;)Lcom/xiaomi/abtest/c/e; */
/* .line 570 */
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 571 */
(( com.xiaomi.abtest.c.e ) v2 ).a ( v3 ); // invoke-virtual {v2, v3}, Lcom/xiaomi/abtest/c/e;->a(Lcom/xiaomi/abtest/c/e;)V
/* .line 573 */
} // :cond_7
(( org.json.JSONArray ) v1 ).remove ( v13 ); // invoke-virtual {v1, v13}, Lorg/json/JSONArray;->remove(I)Ljava/lang/Object;
/* .line 574 */
/* add-int/lit8 v13, v13, -0x1 */
/* .line 568 */
} // :goto_5
/* add-int/lit8 v13, v13, 0x1 */
/* .line 579 */
} // :cond_8
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
static Boolean b ( Object p0 ) { //synthethic
/* .locals 0 */
/* .line 36 */
/* iget-boolean p0, p0, Lcom/xiaomi/abtest/a/a;->u:Z */
} // .end method
static android.os.Handler c ( Object p0 ) { //synthethic
/* .locals 0 */
/* .line 36 */
p0 = this.v;
} // .end method
private void d ( ) {
/* .locals 5 */
/* .line 131 */
/* iget v0, p0, Lcom/xiaomi/abtest/a/a;->s:I */
/* if-lez v0, :cond_0 */
} // :cond_0
/* const/16 v0, 0x1c20 */
/* .line 132 */
} // :goto_0
/* new-instance v1, Landroid/os/Handler; */
android.os.Looper .getMainLooper ( );
/* invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.v = v1;
/* .line 133 */
/* new-instance v1, Lcom/xiaomi/abtest/a/c; */
/* invoke-direct {v1, p0, v0}, Lcom/xiaomi/abtest/a/c;-><init>(Lcom/xiaomi/abtest/a/a;I)V */
/* .line 143 */
v2 = this.v;
/* mul-int/lit16 v0, v0, 0x3e8 */
/* int-to-long v3, v0 */
(( android.os.Handler ) v2 ).postDelayed ( v1, v3, v4 ); // invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 144 */
return;
} // .end method
static void d ( Object p0 ) { //synthethic
/* .locals 0 */
/* .line 36 */
/* invoke-direct {p0}, Lcom/xiaomi/abtest/a/a;->e()V */
return;
} // .end method
private void e ( ) {
/* .locals 5 */
/* .line 187 */
v0 = v0 = this.p;
final String v1 = "ExpPlatformManager"; // const-string v1, "ExpPlatformManager"
/* if-nez v0, :cond_0 */
/* .line 188 */
final String v0 = "no appNames to load remote configuration"; // const-string v0, "no appNames to load remote configuration"
com.xiaomi.abtest.d.k .a ( v1,v0 );
/* .line 191 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 192 */
v2 = this.p;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Ljava/lang/String; */
/* .line 193 */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 194 */
final String v3 = "_"; // const-string v3, "_"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 195 */
/* .line 197 */
} // :cond_1
v2 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v2, :cond_4 */
/* .line 198 */
/* new-instance v2, Ljava/lang/StringBuilder; */
final String v3 = "https://cdn.exp.xiaomi.com/service/getClientExpConf/"; // const-string v3, "https://cdn.exp.xiaomi.com/service/getClientExpConf/"
/* invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 199 */
v3 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* add-int/lit8 v3, v3, -0x1 */
int v4 = 0; // const/4 v4, 0x0
(( java.lang.StringBuilder ) v0 ).substring ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 200 */
v0 = this.x;
v0 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v0, :cond_2 */
/* .line 201 */
final String v0 = "/"; // const-string v0, "/"
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 202 */
v0 = this.x;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 205 */
} // :cond_2
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "load remote configuration, url: : "; // const-string v3, "load remote configuration, url: : "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.abtest.d.k .a ( v1,v0 );
/* .line 206 */
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.abtest.d.i .a ( v0 );
/* .line 207 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "load remote configuration, response: "; // const-string v3, "load remote configuration, response: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.abtest.d.k .a ( v1,v2 );
/* .line 208 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 209 */
/* invoke-direct {p0, v0, v4}, Lcom/xiaomi/abtest/a/a;->a(Ljava/lang/String;Z)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 213 */
} // :cond_3
/* .line 211 */
/* :catch_0 */
/* move-exception v0 */
/* .line 212 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "load remote configuration error : "; // const-string v3, "load remote configuration error : "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.abtest.d.k .b ( v1,v0 );
/* .line 215 */
} // :cond_4
} // :goto_1
return;
} // .end method
/* # virtual methods */
public java.util.Map a ( Object p0 ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/xiaomi/abtest/b/a;", */
/* ")", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/xiaomi/abtest/ExperimentInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 385 */
v0 = this.q;
(( com.xiaomi.abtest.b.a ) p1 ).d ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->d()Ljava/lang/String;
/* check-cast v0, Lcom/xiaomi/abtest/c/b; */
/* .line 386 */
final String v1 = "ExpPlatformManager"; // const-string v1, "ExpPlatformManager"
/* if-nez v0, :cond_0 */
/* .line 387 */
(( com.xiaomi.abtest.b.a ) p1 ).a ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;
(( com.xiaomi.abtest.b.a ) p1 ).d ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->d()Ljava/lang/String;
/* filled-new-array {v0, p1}, [Ljava/lang/Object; */
final String v0 = "no appDomain found for appId:%s,appName:%s"; // const-string v0, "no appDomain found for appId:%s,appName:%s"
java.lang.String .format ( v0,p1 );
com.xiaomi.abtest.d.k .c ( v1,p1 );
/* .line 388 */
int p1 = 0; // const/4 p1, 0x0
/* .line 391 */
} // :cond_0
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 392 */
(( com.xiaomi.abtest.c.b ) v0 ).a ( p1, v2 ); // invoke-virtual {v0, p1, v2}, Lcom/xiaomi/abtest/c/b;->a(Lcom/xiaomi/abtest/b/a;Ljava/util/List;)V
/* .line 394 */
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
/* .line 396 */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Lcom/xiaomi/abtest/c/e; */
/* .line 397 */
/* new-instance v5, Lcom/xiaomi/abtest/ExperimentInfo; */
/* invoke-direct {v5}, Lcom/xiaomi/abtest/ExperimentInfo;-><init>()V */
/* .line 398 */
v6 = (( com.xiaomi.abtest.c.e ) v4 ).a ( ); // invoke-virtual {v4}, Lcom/xiaomi/abtest/c/e;->a()I
(( com.xiaomi.abtest.ExperimentInfo ) v5 ).setExpId ( v6 ); // invoke-virtual {v5, v6}, Lcom/xiaomi/abtest/ExperimentInfo;->setExpId(I)Lcom/xiaomi/abtest/ExperimentInfo;
/* .line 399 */
v6 = (( com.xiaomi.abtest.c.e ) v4 ).d ( ); // invoke-virtual {v4}, Lcom/xiaomi/abtest/c/e;->d()I
(( com.xiaomi.abtest.ExperimentInfo ) v5 ).setContainerId ( v6 ); // invoke-virtual {v5, v6}, Lcom/xiaomi/abtest/ExperimentInfo;->setContainerId(I)V
/* .line 400 */
v6 = (( com.xiaomi.abtest.c.e ) v4 ).d ( ); // invoke-virtual {v4}, Lcom/xiaomi/abtest/c/e;->d()I
v6 = /* invoke-direct {p0, v6, v0}, Lcom/xiaomi/abtest/a/a;->a(ILcom/xiaomi/abtest/c/e;)I */
(( com.xiaomi.abtest.ExperimentInfo ) v5 ).setLayerId ( v6 ); // invoke-virtual {v5, v6}, Lcom/xiaomi/abtest/ExperimentInfo;->setLayerId(I)V
/* .line 401 */
(( com.xiaomi.abtest.c.e ) v4 ).j ( ); // invoke-virtual {v4}, Lcom/xiaomi/abtest/c/e;->j()Ljava/lang/String;
(( com.xiaomi.abtest.ExperimentInfo ) v5 ).setXpath ( v6 ); // invoke-virtual {v5, v6}, Lcom/xiaomi/abtest/ExperimentInfo;->setXpath(Ljava/lang/String;)Lcom/xiaomi/abtest/ExperimentInfo;
/* .line 402 */
(( com.xiaomi.abtest.c.e ) v4 ).i ( ); // invoke-virtual {v4}, Lcom/xiaomi/abtest/c/e;->i()Ljava/util/Map;
(( com.xiaomi.abtest.ExperimentInfo ) v5 ).setParams ( v6 ); // invoke-virtual {v5, v6}, Lcom/xiaomi/abtest/ExperimentInfo;->setParams(Ljava/util/Map;)Lcom/xiaomi/abtest/ExperimentInfo;
/* .line 403 */
(( com.xiaomi.abtest.c.e ) v4 ).k ( ); // invoke-virtual {v4}, Lcom/xiaomi/abtest/c/e;->k()Ljava/lang/String;
/* .line 404 */
/* .line 406 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "get data from the expInfo: "; // const-string v2, "get data from the expInfo: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Object ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.abtest.d.k .a ( v1,v0 );
/* .line 409 */
v0 = this.r;
(( com.xiaomi.abtest.b.a ) p1 ).d ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->d()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.r;
(( com.xiaomi.abtest.b.a ) p1 ).d ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->d()Ljava/lang/String;
/* check-cast v0, Ljava/util/Map; */
v0 = (( com.xiaomi.abtest.b.a ) p1 ).a ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 410 */
v0 = this.r;
(( com.xiaomi.abtest.b.a ) p1 ).d ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->d()Ljava/lang/String;
/* check-cast v0, Ljava/util/Map; */
(( com.xiaomi.abtest.b.a ) p1 ).a ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;
/* check-cast p1, Ljava/util/Map; */
/* .line 411 */
v0 = } // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_2
/* check-cast v0, Ljava/util/Map$Entry; */
/* .line 412 */
/* .line 413 */
/* .line 414 */
} // :cond_2
/* .line 415 */
} // :cond_3
final String p1 = "no data needed in whitelist"; // const-string p1, "no data needed in whitelist"
com.xiaomi.abtest.d.k .a ( v1,p1 );
/* .line 418 */
} // :goto_2
/* new-instance p1, Ljava/lang/StringBuilder; */
/* invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V */
final String v0 = "get data from the expInfo and whitelist: "; // const-string v0, "get data from the expInfo and whitelist: "
(( java.lang.StringBuilder ) p1 ).append ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Object ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) p1 ).append ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.abtest.d.k .a ( v1,p1 );
/* .line 421 */
/* invoke-direct {p0, v3}, Lcom/xiaomi/abtest/a/a;->a(Ljava/util/Map;)V */
/* .line 423 */
/* new-instance p1, Ljava/lang/StringBuilder; */
/* invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V */
final String v0 = "get data from the expInfo and whitelist and launch params: "; // const-string v0, "get data from the expInfo and whitelist and launch params: "
(( java.lang.StringBuilder ) p1 ).append ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Object ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) p1 ).append ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.abtest.d.k .a ( v1,p1 );
/* .line 425 */
} // .end method
public void a ( ) {
/* .locals 2 */
/* .line 86 */
v0 = this.p;
/* .line 87 */
v0 = this.q;
/* .line 88 */
v0 = this.r;
/* .line 89 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/xiaomi/abtest/a/a;->s:I */
/* .line 90 */
/* iput-boolean v0, p0, Lcom/xiaomi/abtest/a/a;->u:Z */
/* .line 91 */
/* iput-boolean v0, p0, Lcom/xiaomi/abtest/a/a;->w:Z */
/* .line 92 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/xiaomi/abtest/a/a;->t:Z */
/* .line 93 */
final String v0 = ""; // const-string v0, ""
this.x = v0;
/* .line 94 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/xiaomi/abtest/a/a;->z:J */
/* .line 95 */
com.xiaomi.abtest.d.a .a ( );
(( android.content.Context ) v0 ).getApplicationContext ( ); // invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
/* check-cast v0, Landroid/app/Application; */
v1 = this.A;
(( android.app.Application ) v0 ).unregisterActivityLifecycleCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V
/* .line 96 */
int v0 = 0; // const/4 v0, 0x0
/* .line 97 */
return;
} // .end method
public void a ( com.xiaomi.abtest.ABTest$OnLoadRemoteConfigListener p0 ) {
/* .locals 2 */
/* .line 175 */
v0 = com.xiaomi.abtest.a.a.o;
/* new-instance v1, Lcom/xiaomi/abtest/a/d; */
/* invoke-direct {v1, p0, p1}, Lcom/xiaomi/abtest/a/d;-><init>(Lcom/xiaomi/abtest/a/a;Lcom/xiaomi/abtest/ABTest$OnLoadRemoteConfigListener;)V */
/* .line 184 */
return;
} // .end method
public void a ( java.lang.String p0 ) {
/* .locals 1 */
/* .line 151 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 152 */
final String p1 = "ExpPlatformManager"; // const-string p1, "ExpPlatformManager"
final String v0 = "appName is empty, skip it!"; // const-string v0, "appName is empty, skip it!"
com.xiaomi.abtest.d.k .a ( p1,v0 );
/* .line 153 */
return;
/* .line 156 */
} // :cond_0
v0 = this.p;
/* .line 157 */
int p1 = 0; // const/4 p1, 0x0
(( com.xiaomi.abtest.a.a ) p0 ).a ( p1 ); // invoke-virtual {p0, p1}, Lcom/xiaomi/abtest/a/a;->a(Lcom/xiaomi/abtest/ABTest$OnLoadRemoteConfigListener;)V
/* .line 158 */
/* iget-boolean p1, p0, Lcom/xiaomi/abtest/a/a;->w:Z */
/* if-nez p1, :cond_1 */
/* .line 159 */
/* invoke-direct {p0}, Lcom/xiaomi/abtest/a/a;->d()V */
/* .line 161 */
} // :cond_1
return;
} // .end method
public void a ( Boolean p0 ) {
/* .locals 0 */
/* .line 147 */
/* iput-boolean p1, p0, Lcom/xiaomi/abtest/a/a;->u:Z */
/* .line 148 */
return;
} // .end method
public void c ( ) {
/* .locals 5 */
/* .line 164 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/xiaomi/abtest/a/a;->z:J */
/* .line 165 */
final String v0 = "abtest"; // const-string v0, "abtest"
com.xiaomi.abtest.d.f .a ( v0 );
/* .line 166 */
/* nop */
/* .line 167 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
/* iget-wide v3, p0, Lcom/xiaomi/abtest/a/a;->z:J */
/* sub-long/2addr v1, v3 */
java.lang.Long .valueOf ( v1,v2 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
/* .line 166 */
final String v2 = "load local config finished, cost: %s ms"; // const-string v2, "load local config finished, cost: %s ms"
java.lang.String .format ( v2,v1 );
final String v2 = "ExpPlatformManager"; // const-string v2, "ExpPlatformManager"
com.xiaomi.abtest.d.k .a ( v2,v1 );
/* .line 168 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "localConfig : "; // const-string v3, "localConfig : "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.xiaomi.abtest.d.k .a ( v2,v1 );
/* .line 169 */
v1 = com.xiaomi.abtest.d.l .b ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 170 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, v0, v1}, Lcom/xiaomi/abtest/a/a;->a(Ljava/lang/String;Z)V */
/* .line 172 */
} // :cond_0
return;
} // .end method
