public class com.xiaomi.abtest.ExperimentInfo {
	 /* # instance fields */
	 public Integer containerId;
	 public Integer expId;
	 public Integer layerId;
	 public java.util.Map params;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
public java.lang.String xpath;
/* # direct methods */
public com.xiaomi.abtest.ExperimentInfo ( ) {
/* .locals 0 */
/* .line 5 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Integer getContainerId ( ) {
/* .locals 1 */
/* .line 13 */
/* iget v0, p0, Lcom/xiaomi/abtest/ExperimentInfo;->containerId:I */
} // .end method
public Integer getExpId ( ) {
/* .locals 1 */
/* .line 39 */
/* iget v0, p0, Lcom/xiaomi/abtest/ExperimentInfo;->expId:I */
} // .end method
public Integer getLayerId ( ) {
/* .locals 1 */
/* .line 21 */
/* iget v0, p0, Lcom/xiaomi/abtest/ExperimentInfo;->layerId:I */
} // .end method
public java.util.Map getParams ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 49 */
v0 = this.params;
} // .end method
public java.lang.String getXpath ( ) {
/* .locals 1 */
/* .line 30 */
v0 = this.xpath;
} // .end method
public void setContainerId ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "containerId" # I */
/* .line 17 */
/* iput p1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->containerId:I */
/* .line 18 */
return;
} // .end method
public com.xiaomi.abtest.ExperimentInfo setExpId ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "expId" # I */
/* .line 43 */
/* iput p1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->expId:I */
/* .line 44 */
} // .end method
public void setLayerId ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "layerId" # I */
/* .line 25 */
/* iput p1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->layerId:I */
/* .line 26 */
return;
} // .end method
public com.xiaomi.abtest.ExperimentInfo setParams ( java.util.Map p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)", */
/* "Lcom/xiaomi/abtest/ExperimentInfo;" */
/* } */
} // .end annotation
/* .line 53 */
/* .local p1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
this.params = p1;
/* .line 54 */
} // .end method
public com.xiaomi.abtest.ExperimentInfo setXpath ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "xpath" # Ljava/lang/String; */
/* .line 34 */
this.xpath = p1;
/* .line 35 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 59 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ExperimentInfo{expId="; // const-string v1, "ExperimentInfo{expId="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->expId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", containerId="; // const-string v1, ", containerId="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->containerId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", layerId="; // const-string v1, ", layerId="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->layerId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", xpath=\'"; // const-string v1, ", xpath=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.xpath;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v1 = ", params="; // const-string v1, ", params="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.params;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
