.class public Lcom/xiaomi/abtest/ABTestConfig;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/abtest/ABTestConfig$Builder;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Z


# direct methods
.method private constructor <init>(Lcom/xiaomi/abtest/ABTestConfig$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/xiaomi/abtest/ABTestConfig$Builder;

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-static {p1}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->a(Lcom/xiaomi/abtest/ABTestConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->a:Ljava/lang/String;

    .line 64
    invoke-static {p1}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->b(Lcom/xiaomi/abtest/ABTestConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->b:Ljava/lang/String;

    .line 65
    invoke-static {p1}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->c(Lcom/xiaomi/abtest/ABTestConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->c:Ljava/lang/String;

    .line 66
    invoke-static {p1}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->d(Lcom/xiaomi/abtest/ABTestConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->d:Ljava/lang/String;

    .line 67
    invoke-static {p1}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->e(Lcom/xiaomi/abtest/ABTestConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->e:I

    .line 68
    invoke-static {p1}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->f(Lcom/xiaomi/abtest/ABTestConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->f:Z

    .line 69
    return-void
.end method

.method synthetic constructor <init>(Lcom/xiaomi/abtest/ABTestConfig$Builder;Lcom/xiaomi/abtest/ABTestConfig$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/xiaomi/abtest/ABTestConfig$Builder;
    .param p2, "x1"    # Lcom/xiaomi/abtest/ABTestConfig$1;

    .line 3
    invoke-direct {p0, p1}, Lcom/xiaomi/abtest/ABTestConfig;-><init>(Lcom/xiaomi/abtest/ABTestConfig$Builder;)V

    return-void
.end method


# virtual methods
.method public getAppName()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getLayerName()Ljava/lang/String;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getLoadConfigInterval()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->e:I

    return v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->c:Ljava/lang/String;

    return-object v0
.end method

.method public isDisableLoadTimer()Z
    .locals 1

    .line 43
    iget-boolean v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->f:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ABTestConfig{mAppName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/abtest/ABTestConfig;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mLayerName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/abtest/ABTestConfig;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mUserId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/abtest/ABTestConfig;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mDeviceId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/abtest/ABTestConfig;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLoadConfigInterval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/abtest/ABTestConfig;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDisableLoadTimer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/xiaomi/abtest/ABTestConfig;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
