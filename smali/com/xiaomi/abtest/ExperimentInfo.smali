.class public Lcom/xiaomi/abtest/ExperimentInfo;
.super Ljava/lang/Object;


# instance fields
.field public containerId:I

.field public expId:I

.field public layerId:I

.field public params:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public xpath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContainerId()I
    .locals 1

    .line 13
    iget v0, p0, Lcom/xiaomi/abtest/ExperimentInfo;->containerId:I

    return v0
.end method

.method public getExpId()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/xiaomi/abtest/ExperimentInfo;->expId:I

    return v0
.end method

.method public getLayerId()I
    .locals 1

    .line 21
    iget v0, p0, Lcom/xiaomi/abtest/ExperimentInfo;->layerId:I

    return v0
.end method

.method public getParams()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/xiaomi/abtest/ExperimentInfo;->params:Ljava/util/Map;

    return-object v0
.end method

.method public getXpath()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/xiaomi/abtest/ExperimentInfo;->xpath:Ljava/lang/String;

    return-object v0
.end method

.method public setContainerId(I)V
    .locals 0
    .param p1, "containerId"    # I

    .line 17
    iput p1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->containerId:I

    .line 18
    return-void
.end method

.method public setExpId(I)Lcom/xiaomi/abtest/ExperimentInfo;
    .locals 0
    .param p1, "expId"    # I

    .line 43
    iput p1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->expId:I

    .line 44
    return-object p0
.end method

.method public setLayerId(I)V
    .locals 0
    .param p1, "layerId"    # I

    .line 25
    iput p1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->layerId:I

    .line 26
    return-void
.end method

.method public setParams(Ljava/util/Map;)Lcom/xiaomi/abtest/ExperimentInfo;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/xiaomi/abtest/ExperimentInfo;"
        }
    .end annotation

    .line 53
    .local p1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->params:Ljava/util/Map;

    .line 54
    return-object p0
.end method

.method public setXpath(Ljava/lang/String;)Lcom/xiaomi/abtest/ExperimentInfo;
    .locals 0
    .param p1, "xpath"    # Ljava/lang/String;

    .line 34
    iput-object p1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->xpath:Ljava/lang/String;

    .line 35
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ExperimentInfo{expId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->expId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", containerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->containerId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", layerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->layerId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", xpath=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->xpath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/abtest/ExperimentInfo;->params:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
