public class com.xiaomi.abtest.d.k {
	 /* # static fields */
	 public static Boolean a;
	 private static final Integer b;
	 private static final java.lang.String c;
	 private static final Integer d;
	 private static final Integer e;
	 private static final Integer f;
	 private static final Integer g;
	 private static final Integer h;
	 private static Boolean i;
	 private static Boolean j;
	 /* # direct methods */
	 static com.xiaomi.abtest.d.k ( ) {
		 /* .locals 1 */
		 /* .line 16 */
		 int v0 = 0; // const/4 v0, 0x0
		 com.xiaomi.abtest.d.k.a = (v0!= 0);
		 /* .line 17 */
		 com.xiaomi.abtest.d.k.i = (v0!= 0);
		 /* .line 18 */
		 com.xiaomi.abtest.d.k.j = (v0!= 0);
		 return;
	 } // .end method
	 public com.xiaomi.abtest.d.k ( ) {
		 /* .locals 0 */
		 /* .line 6 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static java.lang.String a ( java.lang.String p0 ) {
		 /* .locals 2 */
		 /* .line 115 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "ABTest-Api-"; // const-string v1, "ABTest-Api-"
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) p0 ).toString ( ); // invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
	 public static void a ( ) {
		 /* .locals 5 */
		 /* .line 22 */
		 final String v0 = "ABTestSdk"; // const-string v0, "ABTestSdk"
		 try { // :try_start_0
			 com.xiaomi.abtest.d.a .d ( );
			 /* .line 23 */
			 final String v2 = "debug.abtest.log"; // const-string v2, "debug.abtest.log"
			 com.xiaomi.abtest.d.m .a ( v2 );
			 /* .line 24 */
			 /* new-instance v3, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v4 = "logOn: "; // const-string v4, "logOn: "
			 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 final String v4 = ",pkg:"; // const-string v4, ",pkg:"
			 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Log .d ( v0,v3 );
			 /* .line 25 */
			 v3 = 			 android.text.TextUtils .isEmpty ( v2 );
			 /* if-nez v3, :cond_0 */
			 v3 = 			 android.text.TextUtils .isEmpty ( v1 );
			 /* if-nez v3, :cond_0 */
			 v1 = 			 android.text.TextUtils .equals ( v1,v2 );
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 int v1 = 1; // const/4 v1, 0x1
			 } // :cond_0
			 int v1 = 0; // const/4 v1, 0x0
		 } // :goto_0
		 com.xiaomi.abtest.d.k.j = (v1!= 0);
		 /* .line 27 */
		 com.xiaomi.abtest.d.k .b ( );
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 31 */
		 /* .line 29 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 30 */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "LogUtil static initializer: "; // const-string v3, "LogUtil static initializer: "
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .e ( v0,v1 );
		 /* .line 33 */
	 } // :goto_1
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "log on: "; // const-string v2, "log on: "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* sget-boolean v2, Lcom/xiaomi/abtest/d/k;->j:Z */
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .d ( v0,v1 );
	 /* .line 34 */
	 return;
} // .end method
public static void a ( java.lang.String p0, java.lang.String p1 ) {
	 /* .locals 1 */
	 /* .line 38 */
	 /* sget-boolean v0, Lcom/xiaomi/abtest/d/k;->a:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 39 */
		 com.xiaomi.abtest.d.k .a ( p0 );
		 int v0 = 3; // const/4 v0, 0x3
		 com.xiaomi.abtest.d.k .a ( p0,p1,v0 );
		 /* .line 41 */
	 } // :cond_0
	 return;
} // .end method
private static void a ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
	 /* .locals 4 */
	 /* .line 86 */
	 /* if-nez p1, :cond_0 */
	 /* .line 87 */
	 return;
	 /* .line 88 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
v1 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* div-int/lit16 v1, v1, 0xbb8 */
/* if-gt v0, v1, :cond_2 */
/* .line 89 */
/* mul-int/lit16 v1, v0, 0xbb8 */
/* .line 90 */
v2 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* add-int/lit8 v0, v0, 0x1 */
/* mul-int/lit16 v3, v0, 0xbb8 */
v2 = java.lang.Math .min ( v2,v3 );
/* .line 91 */
/* if-ge v1, v2, :cond_1 */
/* .line 92 */
(( java.lang.String ) p1 ).substring ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 93 */
/* packed-switch p2, :pswitch_data_0 */
/* .line 107 */
/* :pswitch_0 */
android.util.Log .v ( p0,v1 );
/* .line 104 */
/* :pswitch_1 */
android.util.Log .d ( p0,v1 );
/* .line 105 */
/* .line 101 */
/* :pswitch_2 */
android.util.Log .i ( p0,v1 );
/* .line 102 */
/* .line 98 */
/* :pswitch_3 */
android.util.Log .w ( p0,v1 );
/* .line 99 */
/* .line 95 */
/* :pswitch_4 */
android.util.Log .e ( p0,v1 );
/* .line 96 */
/* nop */
/* .line 88 */
} // :cond_1
} // :goto_1
/* .line 112 */
} // :cond_2
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public static void a ( java.lang.String p0, java.lang.String p1, java.lang.Throwable p2 ) {
/* .locals 1 */
/* .line 44 */
/* sget-boolean v0, Lcom/xiaomi/abtest/d/k;->a:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 45 */
com.xiaomi.abtest.d.k .a ( p0 );
android.util.Log .d ( p0,p1,p2 );
/* .line 47 */
} // :cond_0
return;
} // .end method
public static void a ( Boolean p0 ) {
/* .locals 0 */
/* .line 119 */
com.xiaomi.abtest.d.k.i = (p0!= 0);
/* .line 120 */
com.xiaomi.abtest.d.k .b ( );
/* .line 121 */
return;
} // .end method
private static void b ( ) {
/* .locals 2 */
/* .line 124 */
/* sget-boolean v0, Lcom/xiaomi/abtest/d/k;->i:Z */
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lcom/xiaomi/abtest/d/k;->j:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
com.xiaomi.abtest.d.k.a = (v0!= 0);
/* .line 125 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateDebugSwitch sEnable: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/xiaomi/abtest/d/k;->a:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " sDebugMode\uff1a"; // const-string v1, " sDebugMode\uff1a"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/xiaomi/abtest/d/k;->i:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " sDebugProperty\uff1a"; // const-string v1, " sDebugProperty\uff1a"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/xiaomi/abtest/d/k;->j:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ABTestSdk"; // const-string v1, "ABTestSdk"
android.util.Log .d ( v1,v0 );
/* .line 126 */
return;
} // .end method
public static void b ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .line 50 */
/* sget-boolean v0, Lcom/xiaomi/abtest/d/k;->a:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 51 */
com.xiaomi.abtest.d.k .a ( p0 );
int v0 = 0; // const/4 v0, 0x0
com.xiaomi.abtest.d.k .a ( p0,p1,v0 );
/* .line 53 */
} // :cond_0
return;
} // .end method
public static void b ( java.lang.String p0, java.lang.String p1, java.lang.Throwable p2 ) {
/* .locals 1 */
/* .line 56 */
/* sget-boolean v0, Lcom/xiaomi/abtest/d/k;->a:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 57 */
com.xiaomi.abtest.d.k .a ( p0 );
android.util.Log .e ( p0,p1,p2 );
/* .line 59 */
} // :cond_0
return;
} // .end method
public static void c ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .line 62 */
/* sget-boolean v0, Lcom/xiaomi/abtest/d/k;->a:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 63 */
com.xiaomi.abtest.d.k .a ( p0 );
int v0 = 1; // const/4 v0, 0x1
com.xiaomi.abtest.d.k .a ( p0,p1,v0 );
/* .line 65 */
} // :cond_0
return;
} // .end method
public static void c ( java.lang.String p0, java.lang.String p1, java.lang.Throwable p2 ) {
/* .locals 1 */
/* .line 68 */
/* sget-boolean v0, Lcom/xiaomi/abtest/d/k;->a:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 69 */
com.xiaomi.abtest.d.k .a ( p0 );
android.util.Log .w ( p0,p1,p2 );
/* .line 71 */
} // :cond_0
return;
} // .end method
public static void d ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .line 74 */
/* sget-boolean v0, Lcom/xiaomi/abtest/d/k;->a:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 75 */
com.xiaomi.abtest.d.k .a ( p0 );
int v0 = 2; // const/4 v0, 0x2
com.xiaomi.abtest.d.k .a ( p0,p1,v0 );
/* .line 77 */
} // :cond_0
return;
} // .end method
public static void d ( java.lang.String p0, java.lang.String p1, java.lang.Throwable p2 ) {
/* .locals 1 */
/* .line 80 */
/* sget-boolean v0, Lcom/xiaomi/abtest/d/k;->a:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 81 */
com.xiaomi.abtest.d.k .a ( p0 );
android.util.Log .i ( p0,p1,p2 );
/* .line 83 */
} // :cond_0
return;
} // .end method
