public class com.xiaomi.abtest.d.m {
	 /* # static fields */
	 private static final java.lang.String a;
	 /* # direct methods */
	 public com.xiaomi.abtest.d.m ( ) {
		 /* .locals 0 */
		 /* .line 5 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Long a ( java.lang.String p0, java.lang.Long p1 ) {
		 /* .locals 5 */
		 /* .line 26 */
		 try { // :try_start_0
			 final String v0 = "android.os.SystemProperties"; // const-string v0, "android.os.SystemProperties"
			 java.lang.Class .forName ( v0 );
			 final String v1 = "getLong"; // const-string v1, "getLong"
			 int v2 = 2; // const/4 v2, 0x2
			 /* new-array v2, v2, [Ljava/lang/Class; */
			 /* const-class v3, Ljava/lang/String; */
			 int v4 = 0; // const/4 v4, 0x0
			 /* aput-object v3, v2, v4 */
			 v3 = java.lang.Long.TYPE;
			 int v4 = 1; // const/4 v4, 0x1
			 /* aput-object v3, v2, v4 */
			 /* .line 27 */
			 (( java.lang.Class ) v0 ).getMethod ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
			 /* filled-new-array {p0, p1}, [Ljava/lang/Object; */
			 /* .line 28 */
			 int v1 = 0; // const/4 v1, 0x0
			 (( java.lang.reflect.Method ) v0 ).invoke ( v1, p0 ); // invoke-virtual {v0, v1, p0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
			 /* check-cast p0, Ljava/lang/Long; */
			 /* .line 26 */
			 (( java.lang.Long ) p0 ).longValue ( ); // invoke-virtual {p0}, Ljava/lang/Long;->longValue()J
			 /* move-result-wide p0 */
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* return-wide p0 */
			 /* .line 29 */
			 /* :catch_0 */
			 /* move-exception p0 */
			 /* .line 30 */
			 final String v0 = "SystemProperties"; // const-string v0, "SystemProperties"
			 com.xiaomi.abtest.d.k .a ( v0 );
			 final String v1 = "getLong e"; // const-string v1, "getLong e"
			 android.util.Log .e ( v0,v1,p0 );
			 /* .line 32 */
			 (( java.lang.Long ) p1 ).longValue ( ); // invoke-virtual {p1}, Ljava/lang/Long;->longValue()J
			 /* move-result-wide p0 */
			 /* return-wide p0 */
		 } // .end method
		 public static java.lang.String a ( java.lang.String p0 ) {
			 /* .locals 1 */
			 /* .line 21 */
			 final String v0 = ""; // const-string v0, ""
			 com.xiaomi.abtest.d.m .a ( p0,v0 );
		 } // .end method
		 public static java.lang.String a ( java.lang.String p0, java.lang.String p1 ) {
			 /* .locals 5 */
			 /* .line 10 */
			 try { // :try_start_0
				 final String v0 = "android.os.SystemProperties"; // const-string v0, "android.os.SystemProperties"
				 java.lang.Class .forName ( v0 );
				 final String v1 = "get"; // const-string v1, "get"
				 int v2 = 2; // const/4 v2, 0x2
				 /* new-array v2, v2, [Ljava/lang/Class; */
				 /* const-class v3, Ljava/lang/String; */
				 int v4 = 0; // const/4 v4, 0x0
				 /* aput-object v3, v2, v4 */
				 /* const-class v3, Ljava/lang/String; */
				 int v4 = 1; // const/4 v4, 0x1
				 /* aput-object v3, v2, v4 */
				 /* .line 11 */
				 (( java.lang.Class ) v0 ).getMethod ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
				 /* filled-new-array {p0, p1}, [Ljava/lang/Object; */
				 /* .line 12 */
				 int v1 = 0; // const/4 v1, 0x0
				 (( java.lang.reflect.Method ) v0 ).invoke ( v1, p0 ); // invoke-virtual {v0, v1, p0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
				 /* check-cast p0, Ljava/lang/String; */
				 /* :try_end_0 */
				 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .line 13 */
				 /* .line 14 */
				 /* :catch_0 */
				 /* move-exception p0 */
				 /* .line 15 */
				 final String v0 = "SystemProperties"; // const-string v0, "SystemProperties"
				 com.xiaomi.abtest.d.k .a ( v0 );
				 final String v1 = "get e"; // const-string v1, "get e"
				 android.util.Log .e ( v0,v1,p0 );
				 /* .line 17 */
			 } // .end method
