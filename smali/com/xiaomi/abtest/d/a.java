public class com.xiaomi.abtest.d.a {
	 /* # static fields */
	 private static android.content.Context a;
	 private static android.content.Context b;
	 private static Integer c;
	 private static java.lang.String d;
	 private static java.lang.String e;
	 private static Long f;
	 private static volatile Boolean g;
	 /* # direct methods */
	 static com.xiaomi.abtest.d.a ( ) {
		 /* .locals 1 */
		 /* .line 19 */
		 int v0 = 0; // const/4 v0, 0x0
		 com.xiaomi.abtest.d.a.g = (v0!= 0);
		 return;
	 } // .end method
	 public com.xiaomi.abtest.d.a ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static android.content.Context a ( ) {
		 /* .locals 2 */
		 /* .line 86 */
		 v0 = com.xiaomi.abtest.d.a.a;
		 v0 = 		 com.xiaomi.abtest.d.e .d ( v0 );
		 /* .line 87 */
		 if ( v0 != null) { // if-eqz v0, :cond_2
			 /* .line 88 */
			 v0 = com.xiaomi.abtest.d.a.b;
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 89 */
				 /* .line 91 */
			 } // :cond_0
			 /* const-class v0, Lcom/xiaomi/abtest/d/a; */
			 /* monitor-enter v0 */
			 /* .line 92 */
			 try { // :try_start_0
				 v1 = com.xiaomi.abtest.d.a.b;
				 /* if-nez v1, :cond_1 */
				 /* .line 93 */
				 v1 = com.xiaomi.abtest.d.a.a;
				 com.xiaomi.abtest.d.e .a ( v1 );
				 /* .line 95 */
			 } // :cond_1
			 /* monitor-exit v0 */
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* .line 96 */
			 v0 = com.xiaomi.abtest.d.a.b;
			 /* .line 95 */
			 /* :catchall_0 */
			 /* move-exception v1 */
			 try { // :try_start_1
				 /* monitor-exit v0 */
				 /* :try_end_1 */
				 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
				 /* throw v1 */
				 /* .line 98 */
			 } // :cond_2
			 v0 = com.xiaomi.abtest.d.a.a;
		 } // .end method
		 public static android.content.pm.PackageInfo a ( android.content.Context p0, java.lang.String p1, Integer p2 ) {
			 /* .locals 0 */
			 /* .line 61 */
			 try { // :try_start_0
				 (( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
				 (( android.content.pm.PackageManager ) p0 ).getPackageInfo ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
				 /* :try_end_0 */
				 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .line 62 */
				 /* :catch_0 */
				 /* move-exception p0 */
				 /* .line 63 */
				 (( java.lang.Exception ) p0 ).printStackTrace ( ); // invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V
				 /* .line 65 */
				 int p0 = 0; // const/4 p0, 0x0
			 } // .end method
			 public static void a ( android.content.Context p0 ) {
				 /* .locals 3 */
				 /* .line 22 */
				 /* sget-boolean v0, Lcom/xiaomi/abtest/d/a;->g:Z */
				 if ( v0 != null) { // if-eqz v0, :cond_0
					 /* .line 23 */
					 return;
					 /* .line 25 */
				 } // :cond_0
				 /* const-class v0, Lcom/xiaomi/abtest/d/a; */
				 /* monitor-enter v0 */
				 /* .line 26 */
				 try { // :try_start_0
					 /* sget-boolean v1, Lcom/xiaomi/abtest/d/a;->g:Z */
					 if ( v1 != null) { // if-eqz v1, :cond_1
						 /* .line 27 */
						 /* monitor-exit v0 */
						 return;
						 /* .line 29 */
					 } // :cond_1
					 /* :try_end_0 */
					 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
					 /* .line 31 */
					 try { // :try_start_1
						 (( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
						 /* .line 32 */
						 v1 = com.xiaomi.abtest.d.a.a;
						 /* .line 33 */
						 (( android.content.Context ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
						 /* .line 32 */
						 int v2 = 0; // const/4 v2, 0x0
						 (( android.content.pm.PackageManager ) p0 ).getPackageInfo ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
						 /* .line 34 */
						 /* iget v1, p0, Landroid/content/pm/PackageInfo;->versionCode:I */
						 /* .line 35 */
						 v1 = this.versionName;
						 /* .line 36 */
						 /* iget-wide v1, p0, Landroid/content/pm/PackageInfo;->lastUpdateTime:J */
						 /* sput-wide v1, Lcom/xiaomi/abtest/d/a;->f:J */
						 /* .line 37 */
						 p0 = com.xiaomi.abtest.d.a.a;
						 (( android.content.Context ) p0 ).getPackageName ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
						 /* :try_end_1 */
						 /* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 ..:try_end_1} :catch_0 */
						 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
						 /* .line 40 */
						 /* .line 38 */
						 /* :catch_0 */
						 /* move-exception p0 */
						 /* .line 39 */
						 try { // :try_start_2
							 (( android.content.pm.PackageManager$NameNotFoundException ) p0 ).printStackTrace ( ); // invoke-virtual {p0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
							 /* .line 41 */
						 } // :goto_0
						 int p0 = 1; // const/4 p0, 0x1
						 com.xiaomi.abtest.d.a.g = (p0!= 0);
						 /* .line 42 */
						 /* monitor-exit v0 */
						 /* .line 43 */
						 return;
						 /* .line 42 */
						 /* :catchall_0 */
						 /* move-exception p0 */
						 /* monitor-exit v0 */
						 /* :try_end_2 */
						 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
						 /* throw p0 */
					 } // .end method
					 public static Boolean a ( android.content.Context p0, java.lang.String p1 ) {
						 /* .locals 1 */
						 /* .line 51 */
						 int v0 = 0; // const/4 v0, 0x0
						 try { // :try_start_0
							 com.xiaomi.abtest.d.a .a ( p0,p1,v0 );
							 /* .line 52 */
							 p0 = this.applicationInfo;
							 p0 = 							 com.xiaomi.abtest.d.a .a ( p0 );
							 /* :try_end_0 */
							 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
							 /* .line 53 */
							 /* :catch_0 */
							 /* move-exception p0 */
							 /* .line 56 */
						 } // .end method
						 public static Boolean a ( android.content.pm.ApplicationInfo p0 ) {
							 /* .locals 1 */
							 /* .line 46 */
							 /* iget p0, p0, Landroid/content/pm/ApplicationInfo;->flags:I */
							 int v0 = 1; // const/4 v0, 0x1
							 /* and-int/2addr p0, v0 */
							 if ( p0 != null) { // if-eqz p0, :cond_0
							 } // :cond_0
							 int v0 = 0; // const/4 v0, 0x0
						 } // :goto_0
					 } // .end method
					 public static java.lang.String b ( ) {
						 /* .locals 1 */
						 /* .line 102 */
						 v0 = com.xiaomi.abtest.d.a.d;
					 } // .end method
					 public static Boolean b ( android.content.Context p0, java.lang.String p1 ) {
						 /* .locals 1 */
						 /* .line 69 */
						 int v0 = 0; // const/4 v0, 0x0
						 com.xiaomi.abtest.d.a .a ( p0,p1,v0 );
						 /* .line 70 */
						 if ( p0 != null) { // if-eqz p0, :cond_0
							 p0 = this.applicationInfo;
							 if ( p0 != null) { // if-eqz p0, :cond_0
								 int v0 = 1; // const/4 v0, 0x1
							 } // :cond_0
						 } // .end method
						 public static Integer c ( ) {
							 /* .locals 1 */
							 /* .line 106 */
						 } // .end method
						 public static java.lang.String c ( android.content.Context p0, java.lang.String p1 ) {
							 /* .locals 1 */
							 /* .line 74 */
							 (( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
							 /* .line 76 */
							 int v0 = 0; // const/4 v0, 0x0
							 try { // :try_start_0
								 (( android.content.pm.PackageManager ) p0 ).getApplicationInfo ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
								 /* .line 77 */
								 (( android.content.pm.ApplicationInfo ) p1 ).loadLabel ( p0 ); // invoke-virtual {p1, p0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
								 /* :try_end_0 */
								 /* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
								 /* .line 79 */
								 /* :catch_0 */
								 /* move-exception p0 */
								 /* .line 80 */
								 (( android.content.pm.PackageManager$NameNotFoundException ) p0 ).printStackTrace ( ); // invoke-virtual {p0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
								 /* .line 82 */
								 final String p0 = ""; // const-string p0, ""
							 } // .end method
							 public static java.lang.String d ( ) {
								 /* .locals 1 */
								 /* .line 110 */
								 v0 = com.xiaomi.abtest.d.a.e;
							 } // .end method
							 public static Long e ( ) {
								 /* .locals 2 */
								 /* .line 114 */
								 /* sget-wide v0, Lcom/xiaomi/abtest/d/a;->f:J */
								 /* return-wide v0 */
							 } // .end method
