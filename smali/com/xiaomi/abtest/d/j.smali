.class public Lcom/xiaomi/abtest/d/j;
.super Ljava/lang/Object;


# static fields
.field public static final a:I = -0x1

.field private static final b:Ljava/lang/String; = "IOUtil"

.field private static final c:I = 0x1000


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 51
    const/16 v0, 0x1000

    invoke-static {p0, p1, v0}, Lcom/xiaomi/abtest/d/j;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)J

    move-result-wide p0

    return-wide p0
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;I)J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 56
    nop

    .line 58
    new-array p2, p2, [B

    const-wide/16 v0, 0x0

    .line 59
    :goto_0
    invoke-virtual {p0, p2}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v3, v2, :cond_0

    .line 60
    const/4 v3, 0x0

    invoke-virtual {p1, p2, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 61
    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_0

    .line 63
    :cond_0
    return-wide v0
.end method

.method public static a([B)Ljava/lang/String;
    .locals 6

    .line 67
    if-nez p0, :cond_0

    .line 68
    const-string p0, ""

    return-object p0

    .line 70
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 71
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-byte v3, p0, v2

    .line 72
    and-int/lit16 v3, v3, 0xff

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    .line 73
    :goto_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_1

    .line 74
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 76
    :cond_1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/io/Closeable;)V
    .locals 2

    .line 36
    if-eqz p0, :cond_0

    .line 37
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 39
    :catch_0
    move-exception p0

    .line 40
    const-string v0, "IOUtil"

    const-string v1, "closeQuietly e"

    invoke-static {v0, v1, p0}, Lcom/xiaomi/abtest/d/k;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 41
    :cond_0
    :goto_0
    nop

    .line 42
    :goto_1
    return-void
.end method

.method public static a(Ljava/io/InputStream;)V
    .locals 0

    .line 27
    invoke-static {p0}, Lcom/xiaomi/abtest/d/j;->a(Ljava/io/Closeable;)V

    .line 28
    return-void
.end method

.method public static a(Ljava/io/OutputStream;)V
    .locals 0

    .line 31
    invoke-static {p0}, Lcom/xiaomi/abtest/d/j;->a(Ljava/io/Closeable;)V

    .line 32
    return-void
.end method

.method public static a(Ljava/net/HttpURLConnection;)V
    .locals 2

    .line 18
    if-eqz p0, :cond_0

    .line 19
    :try_start_0
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 21
    :catch_0
    move-exception p0

    .line 22
    const-string v0, "IOUtil"

    const-string v1, "close e"

    invoke-static {v0, v1, p0}, Lcom/xiaomi/abtest/d/k;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 23
    :cond_0
    :goto_0
    nop

    .line 24
    :goto_1
    return-void
.end method

.method public static b(Ljava/io/Closeable;)V
    .locals 2

    .line 82
    if-eqz p0, :cond_0

    instance-of v0, p0, Ljava/io/Closeable;

    if-eqz v0, :cond_0

    .line 84
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    goto :goto_0

    .line 85
    :catch_0
    move-exception p0

    .line 86
    const-string v0, "IOUtil"

    const-string v1, "closeSafely e"

    invoke-static {v0, v1, p0}, Lcom/xiaomi/abtest/d/k;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 89
    :cond_0
    :goto_0
    return-void
.end method

.method public static b(Ljava/io/InputStream;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 45
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 46
    invoke-static {p0, v0}, Lcom/xiaomi/abtest/d/j;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 47
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    return-object p0
.end method
