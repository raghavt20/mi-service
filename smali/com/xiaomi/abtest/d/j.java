public class com.xiaomi.abtest.d.j {
	 /* # static fields */
	 public static final Integer a;
	 private static final java.lang.String b;
	 private static final Integer c;
	 /* # direct methods */
	 public com.xiaomi.abtest.d.j ( ) {
		 /* .locals 0 */
		 /* .line 10 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Long a ( java.io.InputStream p0, java.io.OutputStream p1 ) {
		 /* .locals 1 */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Ljava/io/IOException; */
		 /* } */
	 } // .end annotation
	 /* .line 51 */
	 /* const/16 v0, 0x1000 */
	 com.xiaomi.abtest.d.j .a ( p0,p1,v0 );
	 /* move-result-wide p0 */
	 /* return-wide p0 */
} // .end method
public static Long a ( java.io.InputStream p0, java.io.OutputStream p1, Integer p2 ) {
	 /* .locals 4 */
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Ljava/io/IOException; */
	 /* } */
} // .end annotation
/* .line 56 */
/* nop */
/* .line 58 */
/* new-array p2, p2, [B */
/* const-wide/16 v0, 0x0 */
/* .line 59 */
} // :goto_0
v2 = (( java.io.InputStream ) p0 ).read ( p2 ); // invoke-virtual {p0, p2}, Ljava/io/InputStream;->read([B)I
int v3 = -1; // const/4 v3, -0x1
/* if-eq v3, v2, :cond_0 */
/* .line 60 */
int v3 = 0; // const/4 v3, 0x0
(( java.io.OutputStream ) p1 ).write ( p2, v3, v2 ); // invoke-virtual {p1, p2, v3, v2}, Ljava/io/OutputStream;->write([BII)V
/* .line 61 */
/* int-to-long v2, v2 */
/* add-long/2addr v0, v2 */
/* .line 63 */
} // :cond_0
/* return-wide v0 */
} // .end method
public static java.lang.String a ( Object[] p0 ) {
/* .locals 6 */
/* .line 67 */
/* if-nez p0, :cond_0 */
/* .line 68 */
final String p0 = ""; // const-string p0, ""
/* .line 70 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 71 */
/* array-length v1, p0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_2 */
/* aget-byte v3, p0, v2 */
/* .line 72 */
/* and-int/lit16 v3, v3, 0xff */
java.lang.Integer .toHexString ( v3 );
/* .line 73 */
} // :goto_1
v4 = (( java.lang.String ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/lang/String;->length()I
int v5 = 2; // const/4 v5, 0x2
/* if-ge v4, v5, :cond_1 */
/* .line 74 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "0"; // const-string v5, "0"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 76 */
} // :cond_1
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 71 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 78 */
} // :cond_2
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static void a ( java.io.Closeable p0 ) {
/* .locals 2 */
/* .line 36 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 37 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 39 */
/* :catch_0 */
/* move-exception p0 */
/* .line 40 */
final String v0 = "IOUtil"; // const-string v0, "IOUtil"
final String v1 = "closeQuietly e"; // const-string v1, "closeQuietly e"
com.xiaomi.abtest.d.k .b ( v0,v1,p0 );
/* .line 41 */
} // :cond_0
} // :goto_0
/* nop */
/* .line 42 */
} // :goto_1
return;
} // .end method
public static void a ( java.io.InputStream p0 ) {
/* .locals 0 */
/* .line 27 */
com.xiaomi.abtest.d.j .a ( p0 );
/* .line 28 */
return;
} // .end method
public static void a ( java.io.OutputStream p0 ) {
/* .locals 0 */
/* .line 31 */
com.xiaomi.abtest.d.j .a ( p0 );
/* .line 32 */
return;
} // .end method
public static void a ( java.net.HttpURLConnection p0 ) {
/* .locals 2 */
/* .line 18 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 19 */
try { // :try_start_0
(( java.net.HttpURLConnection ) p0 ).disconnect ( ); // invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 21 */
/* :catch_0 */
/* move-exception p0 */
/* .line 22 */
final String v0 = "IOUtil"; // const-string v0, "IOUtil"
final String v1 = "close e"; // const-string v1, "close e"
com.xiaomi.abtest.d.k .b ( v0,v1,p0 );
/* .line 23 */
} // :cond_0
} // :goto_0
/* nop */
/* .line 24 */
} // :goto_1
return;
} // .end method
public static void b ( java.io.Closeable p0 ) {
/* .locals 2 */
/* .line 82 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* instance-of v0, p0, Ljava/io/Closeable; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 84 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 87 */
/* .line 85 */
/* :catch_0 */
/* move-exception p0 */
/* .line 86 */
final String v0 = "IOUtil"; // const-string v0, "IOUtil"
final String v1 = "closeSafely e"; // const-string v1, "closeSafely e"
com.xiaomi.abtest.d.k .b ( v0,v1,p0 );
/* .line 89 */
} // :cond_0
} // :goto_0
return;
} // .end method
public static b ( java.io.InputStream p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 45 */
/* new-instance v0, Ljava/io/ByteArrayOutputStream; */
/* invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V */
/* .line 46 */
com.xiaomi.abtest.d.j .a ( p0,v0 );
/* .line 47 */
(( java.io.ByteArrayOutputStream ) v0 ).toByteArray ( ); // invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
} // .end method
