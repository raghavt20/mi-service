public class com.xiaomi.abtest.d.e {
	 /* # static fields */
	 public static final Integer a;
	 public static final Integer b;
	 private static final java.lang.String c;
	 /* # direct methods */
	 private com.xiaomi.abtest.d.e ( ) {
		 /* .locals 0 */
		 /* .line 15 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 16 */
		 return;
	 } // .end method
	 public static android.content.Context a ( android.content.Context p0 ) {
		 /* .locals 2 */
		 /* .line 19 */
		 v0 = 		 com.xiaomi.abtest.d.e .e ( p0 );
		 final String v1 = "FbeUtil"; // const-string v1, "FbeUtil"
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 20 */
			 final String v0 = "getSafeContext return origin ctx"; // const-string v0, "getSafeContext return origin ctx"
			 com.xiaomi.abtest.d.k .a ( v1,v0 );
			 /* .line 21 */
			 /* .line 23 */
		 } // :cond_0
		 final String v0 = "getSafeContext , create the safe ctx"; // const-string v0, "getSafeContext , create the safe ctx"
		 com.xiaomi.abtest.d.k .a ( v1,v0 );
		 /* .line 24 */
		 (( android.content.Context ) p0 ).createDeviceProtectedStorageContext ( ); // invoke-virtual {p0}, Landroid/content/Context;->createDeviceProtectedStorageContext()Landroid/content/Context;
	 } // .end method
	 public static void a ( android.preference.PreferenceManager p0 ) {
		 /* .locals 0 */
		 /* .line 41 */
		 /* nop */
		 /* .line 42 */
		 /* nop */
		 /* .line 43 */
		 /* nop */
		 /* .line 44 */
		 /* nop */
		 /* .line 45 */
		 (( android.preference.PreferenceManager ) p0 ).setStorageDeviceProtected ( ); // invoke-virtual {p0}, Landroid/preference/PreferenceManager;->setStorageDeviceProtected()V
		 /* .line 47 */
		 return;
	 } // .end method
	 public static Boolean a ( ) {
		 /* .locals 6 */
		 /* .line 30 */
		 final String v0 = "isFileEncryptedNativeOrEmulated"; // const-string v0, "isFileEncryptedNativeOrEmulated"
		 int v1 = 0; // const/4 v1, 0x0
		 try { // :try_start_0
			 /* const-class v2, Landroid/os/storage/StorageManager; */
			 /* new-array v3, v1, [Ljava/lang/Class; */
			 (( java.lang.Class ) v2 ).getDeclaredMethod ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
			 /* new-array v3, v1, [Ljava/lang/Object; */
			 /* .line 31 */
			 int v4 = 0; // const/4 v4, 0x0
			 (( java.lang.reflect.Method ) v2 ).invoke ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
			 /* .line 32 */
			 (( java.lang.Object ) v2 ).getClass ( ); // invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
			 int v3 = 1; // const/4 v3, 0x1
			 /* new-array v3, v3, [Ljava/lang/Class; */
			 v5 = java.lang.Boolean.TYPE;
			 /* aput-object v5, v3, v1 */
			 (( java.lang.Class ) v2 ).getDeclaredMethod ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
			 /* new-array v2, v1, [Ljava/lang/Object; */
			 /* .line 33 */
			 (( java.lang.reflect.Method ) v0 ).invoke ( v4, v2 ); // invoke-virtual {v0, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
			 /* check-cast v0, Ljava/lang/Boolean; */
			 /* .line 32 */
			 v0 = 			 (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 34 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 /* .line 35 */
			 /* new-instance v2, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v3 = "*** "; // const-string v3, "*** "
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 final String v2 = "FbeUtil"; // const-string v2, "FbeUtil"
			 com.xiaomi.abtest.d.k .b ( v2,v0 );
			 /* .line 37 */
		 } // .end method
		 public static Boolean b ( android.content.Context p0 ) {
			 /* .locals 3 */
			 /* .line 50 */
			 /* nop */
			 /* .line 52 */
			 int v0 = 0; // const/4 v0, 0x0
			 try { // :try_start_0
				 final String v1 = "keyguard"; // const-string v1, "keyguard"
				 (( android.content.Context ) p0 ).getSystemService ( v1 ); // invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
				 /* check-cast p0, Landroid/app/KeyguardManager; */
				 /* .line 53 */
				 v1 = 				 com.xiaomi.abtest.d.e .a ( );
				 if ( v1 != null) { // if-eqz v1, :cond_0
					 if ( p0 != null) { // if-eqz p0, :cond_0
						 p0 = 						 (( android.app.KeyguardManager ) p0 ).isKeyguardSecure ( ); // invoke-virtual {p0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z
						 /* :try_end_0 */
						 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
						 if ( p0 != null) { // if-eqz p0, :cond_0
							 int v0 = 1; // const/4 v0, 0x1
						 } // :cond_0
						 /* .line 54 */
						 /* :catch_0 */
						 /* move-exception p0 */
						 /* .line 55 */
						 /* new-instance v1, Ljava/lang/StringBuilder; */
						 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
						 final String v2 = "FBEDeviceAndSetedUpScreenLock Exception: "; // const-string v2, "FBEDeviceAndSetedUpScreenLock Exception: "
						 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
						 (( java.lang.Exception ) p0 ).getMessage ( ); // invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
						 (( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
						 (( java.lang.StringBuilder ) p0 ).toString ( ); // invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
						 final String v1 = "FbeUtil"; // const-string v1, "FbeUtil"
						 com.xiaomi.abtest.d.k .a ( v1,p0 );
						 /* .line 57 */
					 } // .end method
					 public static Boolean c ( android.content.Context p0 ) {
						 /* .locals 1 */
						 /* .line 64 */
						 /* nop */
						 /* .line 65 */
						 v0 = 						 com.xiaomi.abtest.d.e .b ( p0 );
						 if ( v0 != null) { // if-eqz v0, :cond_0
							 p0 = 							 com.xiaomi.abtest.d.e .e ( p0 );
							 /* if-nez p0, :cond_0 */
							 int p0 = 1; // const/4 p0, 0x1
						 } // :cond_0
						 int p0 = 0; // const/4 p0, 0x0
					 } // :goto_0
				 } // .end method
				 public static Boolean d ( android.content.Context p0 ) {
					 /* .locals 0 */
					 /* .line 72 */
					 p0 = 					 com.xiaomi.abtest.d.e .e ( p0 );
					 if ( p0 != null) { // if-eqz p0, :cond_0
						 /* .line 73 */
						 int p0 = 0; // const/4 p0, 0x0
						 /* .line 75 */
					 } // :cond_0
					 int p0 = 1; // const/4 p0, 0x1
				 } // .end method
				 private static Boolean e ( android.content.Context p0 ) {
					 /* .locals 3 */
					 /* .line 80 */
					 /* nop */
					 /* .line 82 */
					 int v0 = 0; // const/4 v0, 0x0
					 try { // :try_start_0
						 /* const-string/jumbo v1, "user" */
						 (( android.content.Context ) p0 ).getSystemService ( v1 ); // invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
						 /* check-cast p0, Landroid/os/UserManager; */
						 /* .line 83 */
						 if ( p0 != null) { // if-eqz p0, :cond_0
							 p0 = 							 (( android.os.UserManager ) p0 ).isUserUnlocked ( ); // invoke-virtual {p0}, Landroid/os/UserManager;->isUserUnlocked()Z
							 /* :try_end_0 */
							 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
							 if ( p0 != null) { // if-eqz p0, :cond_0
								 int v0 = 1; // const/4 v0, 0x1
							 } // :cond_0
							 /* .line 84 */
							 /* :catch_0 */
							 /* move-exception p0 */
							 /* .line 85 */
							 /* new-instance v1, Ljava/lang/StringBuilder; */
							 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
							 final String v2 = "isUserUnlocked Exception: "; // const-string v2, "isUserUnlocked Exception: "
							 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
							 (( java.lang.Exception ) p0 ).getMessage ( ); // invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
							 (( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
							 (( java.lang.StringBuilder ) p0 ).toString ( ); // invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
							 final String v1 = "FbeUtil"; // const-string v1, "FbeUtil"
							 com.xiaomi.abtest.d.k .a ( v1,p0 );
							 /* .line 87 */
						 } // .end method
						 private static Boolean f ( android.content.Context p0 ) {
							 /* .locals 0 */
							 /* .line 94 */
							 /* nop */
							 /* .line 97 */
							 /* nop */
							 /* .line 98 */
							 int p0 = 0; // const/4 p0, 0x0
						 } // .end method
