.class public final enum Lcom/xiaomi/abtest/EnumType$ConditionOperator;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/abtest/EnumType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConditionOperator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/xiaomi/abtest/EnumType$ConditionOperator;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum OP_EQ:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

.field public static final enum OP_GE:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

.field public static final enum OP_GT:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

.field public static final enum OP_IN:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

.field public static final enum OP_LE:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

.field public static final enum OP_LT:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

.field private static final synthetic a:[Lcom/xiaomi/abtest/EnumType$ConditionOperator;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 17
    new-instance v0, Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    const-string v1, "OP_EQ"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/xiaomi/abtest/EnumType$ConditionOperator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_EQ:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    .line 18
    new-instance v1, Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    const-string v2, "OP_GT"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/xiaomi/abtest/EnumType$ConditionOperator;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_GT:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    .line 19
    new-instance v2, Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    const-string v3, "OP_GE"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lcom/xiaomi/abtest/EnumType$ConditionOperator;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_GE:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    .line 20
    new-instance v3, Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    const-string v4, "OP_LT"

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5}, Lcom/xiaomi/abtest/EnumType$ConditionOperator;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_LT:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    .line 21
    new-instance v4, Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    const-string v5, "OP_LE"

    const/4 v6, 0x4

    invoke-direct {v4, v5, v6}, Lcom/xiaomi/abtest/EnumType$ConditionOperator;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_LE:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    .line 22
    new-instance v5, Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    const-string v6, "OP_IN"

    const/4 v7, 0x5

    invoke-direct {v5, v6, v7}, Lcom/xiaomi/abtest/EnumType$ConditionOperator;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_IN:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    .line 16
    filled-new-array/range {v0 .. v5}, [Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->a:[Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    return-void
.end method

.method public static valueOf(I)Lcom/xiaomi/abtest/EnumType$ConditionOperator;
    .locals 1
    .param p0, "value"    # I

    .line 28
    packed-switch p0, :pswitch_data_0

    .line 42
    const/4 v0, 0x0

    return-object v0

    .line 40
    :pswitch_0
    sget-object v0, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_IN:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    return-object v0

    .line 38
    :pswitch_1
    sget-object v0, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_LE:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    return-object v0

    .line 36
    :pswitch_2
    sget-object v0, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_LT:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    return-object v0

    .line 34
    :pswitch_3
    sget-object v0, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_GE:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    return-object v0

    .line 32
    :pswitch_4
    sget-object v0, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_GT:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    return-object v0

    .line 30
    :pswitch_5
    sget-object v0, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_EQ:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/abtest/EnumType$ConditionOperator;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 16
    const-class v0, Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/abtest/EnumType$ConditionOperator;
    .locals 1

    .line 16
    sget-object v0, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->a:[Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    invoke-virtual {v0}, [Lcom/xiaomi/abtest/EnumType$ConditionOperator;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    return-object v0
.end method
