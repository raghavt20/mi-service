.class public Lcom/xiaomi/abtest/ABTest;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/abtest/ABTest$OnLoadRemoteConfigListener;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "ABTest"


# instance fields
.field private b:Lcom/xiaomi/abtest/ABTestConfig;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static abTestWithConfig(Landroid/content/Context;Lcom/xiaomi/abtest/ABTestConfig;)Lcom/xiaomi/abtest/ABTest;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "config"    # Lcom/xiaomi/abtest/ABTestConfig;

    .line 19
    invoke-static {p0}, Lcom/xiaomi/abtest/d/a;->a(Landroid/content/Context;)V

    .line 20
    invoke-static {}, Lcom/xiaomi/abtest/d/k;->a()V

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "abTestWithConfig start,config: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-nez p1, :cond_0

    const-string v1, ""

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/xiaomi/abtest/ABTestConfig;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ABTest"

    invoke-static {v1, v0}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    new-instance v0, Lcom/xiaomi/abtest/ABTest;

    invoke-direct {v0}, Lcom/xiaomi/abtest/ABTest;-><init>()V

    .line 24
    iput-object p1, v0, Lcom/xiaomi/abtest/ABTest;->b:Lcom/xiaomi/abtest/ABTestConfig;

    .line 25
    invoke-static {p1}, Lcom/xiaomi/abtest/a/a;->a(Lcom/xiaomi/abtest/ABTestConfig;)V

    .line 26
    invoke-static {}, Lcom/xiaomi/abtest/a/a;->b()Lcom/xiaomi/abtest/a/a;

    move-result-object v1

    invoke-virtual {p1}, Lcom/xiaomi/abtest/ABTestConfig;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/abtest/a/a;->a(Ljava/lang/String;)V

    .line 28
    return-object v0
.end method

.method public static setIsLoadConfigWhenBackground(Z)V
    .locals 1
    .param p0, "isLoad"    # Z

    .line 36
    invoke-static {}, Lcom/xiaomi/abtest/a/a;->b()Lcom/xiaomi/abtest/a/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/xiaomi/abtest/a/a;->a(Z)V

    .line 37
    return-void
.end method


# virtual methods
.method public clearSingleInstance()V
    .locals 1

    .line 62
    invoke-static {}, Lcom/xiaomi/abtest/a/a;->b()Lcom/xiaomi/abtest/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/abtest/a/a;->a()V

    .line 63
    return-void
.end method

.method public getExperiments(Ljava/util/Map;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/abtest/ExperimentInfo;",
            ">;"
        }
    .end annotation

    .line 44
    .local p1, "conditions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/xiaomi/abtest/a/a;->b()Lcom/xiaomi/abtest/a/a;

    move-result-object v0

    .line 45
    new-instance v1, Lcom/xiaomi/abtest/b/a;

    invoke-direct {v1}, Lcom/xiaomi/abtest/b/a;-><init>()V

    .line 46
    iget-object v2, p0, Lcom/xiaomi/abtest/ABTest;->b:Lcom/xiaomi/abtest/ABTestConfig;

    invoke-virtual {v2}, Lcom/xiaomi/abtest/ABTestConfig;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/abtest/b/a;->c(Ljava/lang/String;)V

    .line 47
    iget-object v2, p0, Lcom/xiaomi/abtest/ABTest;->b:Lcom/xiaomi/abtest/ABTestConfig;

    invoke-virtual {v2}, Lcom/xiaomi/abtest/ABTestConfig;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/abtest/b/a;->b(Ljava/lang/String;)V

    .line 48
    invoke-virtual {v1, p1}, Lcom/xiaomi/abtest/b/a;->a(Ljava/util/Map;)V

    .line 49
    iget-object v2, p0, Lcom/xiaomi/abtest/ABTest;->b:Lcom/xiaomi/abtest/ABTestConfig;

    invoke-virtual {v2}, Lcom/xiaomi/abtest/ABTestConfig;->getUserId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/abtest/b/a;->a(Ljava/lang/String;)V

    .line 50
    invoke-virtual {v0, v1}, Lcom/xiaomi/abtest/a/a;->a(Lcom/xiaomi/abtest/b/a;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public loadRemoteConfig(Lcom/xiaomi/abtest/ABTest$OnLoadRemoteConfigListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/xiaomi/abtest/ABTest$OnLoadRemoteConfigListener;

    .line 54
    invoke-static {}, Lcom/xiaomi/abtest/a/a;->b()Lcom/xiaomi/abtest/a/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/abtest/a/a;->a(Lcom/xiaomi/abtest/ABTest$OnLoadRemoteConfigListener;)V

    .line 55
    return-void
.end method
