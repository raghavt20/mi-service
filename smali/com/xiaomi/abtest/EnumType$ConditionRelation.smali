.class public final enum Lcom/xiaomi/abtest/EnumType$ConditionRelation;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/abtest/EnumType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConditionRelation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/xiaomi/abtest/EnumType$ConditionRelation;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AND:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

.field public static final enum OR:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

.field private static final synthetic a:[Lcom/xiaomi/abtest/EnumType$ConditionRelation;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 9
    new-instance v0, Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    const-string v1, "AND"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/xiaomi/abtest/EnumType$ConditionRelation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/abtest/EnumType$ConditionRelation;->AND:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    .line 10
    new-instance v1, Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    const-string v2, "OR"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/xiaomi/abtest/EnumType$ConditionRelation;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/xiaomi/abtest/EnumType$ConditionRelation;->OR:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    .line 8
    filled-new-array {v0, v1}, [Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/abtest/EnumType$ConditionRelation;->a:[Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 13
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/abtest/EnumType$ConditionRelation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 8
    const-class v0, Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/abtest/EnumType$ConditionRelation;
    .locals 1

    .line 8
    sget-object v0, Lcom/xiaomi/abtest/EnumType$ConditionRelation;->a:[Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    invoke-virtual {v0}, [Lcom/xiaomi/abtest/EnumType$ConditionRelation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    return-object v0
.end method
