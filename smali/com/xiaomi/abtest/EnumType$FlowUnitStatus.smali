.class public final enum Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/abtest/EnumType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FlowUnitStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum STATUS_DELETED:Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

.field public static final enum STATUS_INVALID:Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

.field public static final enum STATUS_VALID:Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

.field private static final synthetic a:[Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 48
    new-instance v0, Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    const-string v1, "STATUS_VALID"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;->STATUS_VALID:Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    .line 49
    new-instance v1, Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    const-string v2, "STATUS_INVALID"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;->STATUS_INVALID:Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    .line 50
    new-instance v2, Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    const-string v3, "STATUS_DELETED"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;->STATUS_DELETED:Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    .line 47
    filled-new-array {v0, v1, v2}, [Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;->a:[Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 53
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 47
    const-class v0, Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;
    .locals 1

    .line 47
    sget-object v0, Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;->a:[Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    invoke-virtual {v0}, [Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    return-object v0
.end method
