public class inal extends java.lang.Enum {
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/abtest/EnumType; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x4019 */
/* name = "FlowUnitType" */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Enum<", */
/* "Lcom/xiaomi/abtest/EnumType$FlowUnitType;", */
/* ">;" */
/* } */
} // .end annotation
/* # static fields */
public static final com.xiaomi.abtest.EnumType$FlowUnitType TYPE_DOMAIN;
public static final com.xiaomi.abtest.EnumType$FlowUnitType TYPE_EXPERIMENT;
public static final com.xiaomi.abtest.EnumType$FlowUnitType TYPE_EXP_CONTAINER;
public static final com.xiaomi.abtest.EnumType$FlowUnitType TYPE_LAYER;
public static final com.xiaomi.abtest.EnumType$FlowUnitType TYPE_UNKNOWN;
private static final com.xiaomi.abtest.EnumType$FlowUnitType a; //synthetic
/* # direct methods */
static inal ( ) {
/* .locals 7 */
/* .line 57 */
/* new-instance v0, Lcom/xiaomi/abtest/EnumType$FlowUnitType; */
final String v1 = "TYPE_UNKNOWN"; // const-string v1, "TYPE_UNKNOWN"
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v0, v1, v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;-><init>(Ljava/lang/String;I)V */
/* .line 58 */
/* new-instance v1, Lcom/xiaomi/abtest/EnumType$FlowUnitType; */
final String v2 = "TYPE_DOMAIN"; // const-string v2, "TYPE_DOMAIN"
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {v1, v2, v3}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;-><init>(Ljava/lang/String;I)V */
/* .line 59 */
/* new-instance v2, Lcom/xiaomi/abtest/EnumType$FlowUnitType; */
final String v3 = "TYPE_LAYER"; // const-string v3, "TYPE_LAYER"
int v4 = 2; // const/4 v4, 0x2
/* invoke-direct {v2, v3, v4}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;-><init>(Ljava/lang/String;I)V */
/* .line 60 */
/* new-instance v3, Lcom/xiaomi/abtest/EnumType$FlowUnitType; */
final String v4 = "TYPE_EXP_CONTAINER"; // const-string v4, "TYPE_EXP_CONTAINER"
int v5 = 3; // const/4 v5, 0x3
/* invoke-direct {v3, v4, v5}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;-><init>(Ljava/lang/String;I)V */
/* .line 61 */
/* new-instance v4, Lcom/xiaomi/abtest/EnumType$FlowUnitType; */
final String v5 = "TYPE_EXPERIMENT"; // const-string v5, "TYPE_EXPERIMENT"
int v6 = 4; // const/4 v6, 0x4
/* invoke-direct {v4, v5, v6}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;-><init>(Ljava/lang/String;I)V */
/* .line 56 */
/* filled-new-array {v0, v1, v2, v3, v4}, [Lcom/xiaomi/abtest/EnumType$FlowUnitType; */
return;
} // .end method
private inal ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 56 */
/* invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V */
return;
} // .end method
public static com.xiaomi.abtest.EnumType$FlowUnitType valueOf ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "name" # Ljava/lang/String; */
/* .line 56 */
/* const-class v0, Lcom/xiaomi/abtest/EnumType$FlowUnitType; */
java.lang.Enum .valueOf ( v0,p0 );
/* check-cast v0, Lcom/xiaomi/abtest/EnumType$FlowUnitType; */
} // .end method
public static com.xiaomi.abtest.EnumType$FlowUnitType values ( ) {
/* .locals 1 */
/* .line 56 */
v0 = com.xiaomi.abtest.EnumType$FlowUnitType.a;
(( com.xiaomi.abtest.EnumType$FlowUnitType ) v0 ).clone ( ); // invoke-virtual {v0}, [Lcom/xiaomi/abtest/EnumType$FlowUnitType;->clone()Ljava/lang/Object;
/* check-cast v0, [Lcom/xiaomi/abtest/EnumType$FlowUnitType; */
} // .end method
