.class public Lcom/xiaomi/abtest/c/c;
.super Lcom/xiaomi/abtest/c/e;


# static fields
.field private static final n:Ljava/lang/String; = "ExpContainer"


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;ILcom/xiaomi/abtest/EnumType$DiversionType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 14
    invoke-direct/range {p0 .. p9}, Lcom/xiaomi/abtest/c/e;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;ILcom/xiaomi/abtest/EnumType$DiversionType;Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/abtest/c/c;->i:Ljava/util/List;

    .line 16
    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/abtest/b/a;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/abtest/b/a;",
            "Ljava/util/List<",
            "Lcom/xiaomi/abtest/c/e;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-virtual {p0}, Lcom/xiaomi/abtest/c/c;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/abtest/c/c;->b()Ljava/lang/String;

    move-result-object v1

    filled-new-array {v0, v1}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "id: %d, name: %s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExpContainer"

    invoke-static {v1, v0}, Lcom/xiaomi/abtest/d/k;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/xiaomi/abtest/c/c;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 32
    const-string p1, "no experiment in this ExpContainer."

    invoke-static {v1, p1}, Lcom/xiaomi/abtest/d/k;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    return-void

    .line 36
    :cond_0
    invoke-virtual {p0, p1}, Lcom/xiaomi/abtest/c/c;->b(Lcom/xiaomi/abtest/b/a;)I

    move-result v0

    .line 37
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "bucketId: %s"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xiaomi/abtest/d/k;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const/4 v2, -0x1

    if-eq v0, v2, :cond_3

    .line 39
    iget-object v1, p0, Lcom/xiaomi/abtest/c/c;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/abtest/c/e;

    .line 40
    invoke-virtual {v2, v0}, Lcom/xiaomi/abtest/c/e;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, p1}, Lcom/xiaomi/abtest/c/e;->a(Lcom/xiaomi/abtest/b/a;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 41
    invoke-virtual {v2, p1, p2}, Lcom/xiaomi/abtest/c/e;->a(Lcom/xiaomi/abtest/b/a;Ljava/util/List;)V

    .line 42
    goto :goto_1

    .line 44
    :cond_1
    goto :goto_0

    :cond_2
    :goto_1
    goto :goto_2

    .line 46
    :cond_3
    const-string p1, "bucketId is illegal, stop traffic"

    invoke-static {v1, p1}, Lcom/xiaomi/abtest/d/k;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :goto_2
    return-void
.end method

.method public a(I)Z
    .locals 0

    .line 20
    const/4 p1, 0x1

    return p1
.end method

.method public a(Lcom/xiaomi/abtest/b/a;)Z
    .locals 0

    .line 25
    const/4 p1, 0x1

    return p1
.end method
