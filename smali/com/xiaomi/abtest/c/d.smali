.class public Lcom/xiaomi/abtest/c/d;
.super Lcom/xiaomi/abtest/c/e;


# static fields
.field private static final n:Ljava/lang/String; = "Experiment"


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Ljava/util/TreeSet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lcom/xiaomi/abtest/EnumType$FlowUnitType;",
            "I",
            "Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;",
            "Ljava/util/TreeSet<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 19
    move-object v8, p0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Lcom/xiaomi/abtest/c/e;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    move-object v0, p6

    iput-object v0, v8, Lcom/xiaomi/abtest/c/d;->j:Ljava/util/Set;

    .line 21
    invoke-static/range {p7 .. p7}, Lcom/xiaomi/abtest/c/a;->a(Ljava/lang/String;)Lcom/xiaomi/abtest/c/a;

    move-result-object v0

    iput-object v0, v8, Lcom/xiaomi/abtest/c/d;->l:Lcom/xiaomi/abtest/c/a;

    .line 22
    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/abtest/b/a;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/abtest/b/a;",
            "Ljava/util/List<",
            "Lcom/xiaomi/abtest/c/e;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-virtual {p0}, Lcom/xiaomi/abtest/c/d;->a()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0}, Lcom/xiaomi/abtest/c/d;->b()Ljava/lang/String;

    move-result-object v0

    filled-new-array {p1, v0}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "Add this experiment, id: %d, name: %s"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "Experiment"

    invoke-static {v0, p1}, Lcom/xiaomi/abtest/d/k;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method
