.class public Lcom/xiaomi/abtest/c/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/abtest/c/a$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "Condition"


# instance fields
.field private b:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/abtest/c/a$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/xiaomi/abtest/c/a;
    .locals 10

    .line 38
    const/4 v0, 0x0

    if-eqz p0, :cond_4

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_2

    .line 42
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 44
    new-instance p0, Lcom/xiaomi/abtest/c/a;

    invoke-direct {p0}, Lcom/xiaomi/abtest/c/a;-><init>()V

    .line 45
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/xiaomi/abtest/c/a;->c:Ljava/util/ArrayList;

    .line 46
    const-string v2, "relation"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/abtest/EnumType$ConditionRelation;->valueOf(Ljava/lang/String;)Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    move-result-object v2

    iput-object v2, p0, Lcom/xiaomi/abtest/c/a;->b:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    .line 48
    const-string v2, "filters"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 49
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 50
    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 51
    new-instance v5, Lcom/xiaomi/abtest/c/a$a;

    invoke-direct {v5}, Lcom/xiaomi/abtest/c/a$a;-><init>()V

    .line 52
    const-string v6, "propertyName"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/xiaomi/abtest/c/a$a;->a:Ljava/lang/String;

    .line 53
    const-string v6, "propertyValue"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    .line 54
    const-string v6, "operator"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->valueOf(I)Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    move-result-object v4

    iput-object v4, v5, Lcom/xiaomi/abtest/c/a$a;->b:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    .line 55
    iget-object v4, v5, Lcom/xiaomi/abtest/c/a$a;->b:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    sget-object v6, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_IN:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    if-ne v4, v6, :cond_2

    .line 56
    iget-object v4, v5, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    .line 57
    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 58
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 59
    array-length v7, v4

    move v8, v2

    :goto_1
    if-ge v8, v7, :cond_1

    aget-object v9, v4, v8

    .line 60
    invoke-interface {v6, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 62
    :cond_1
    iput-object v6, v5, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    .line 64
    :cond_2
    iget-object v4, p0, Lcom/xiaomi/abtest/c/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 66
    :cond_3
    return-object p0

    .line 67
    :catch_0
    move-exception p0

    .line 68
    const-string v1, "Condition"

    const-string v2, ""

    invoke-static {v1, v2, p0}, Lcom/xiaomi/abtest/d/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 70
    return-object v0

    .line 39
    :cond_4
    :goto_2
    return-object v0
.end method


# virtual methods
.method public a()Lcom/xiaomi/abtest/EnumType$ConditionRelation;
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/xiaomi/abtest/c/a;->b:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    return-object v0
.end method

.method public a(Lcom/xiaomi/abtest/EnumType$ConditionRelation;)V
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/xiaomi/abtest/c/a;->b:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    .line 184
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/abtest/c/a$a;",
            ">;)V"
        }
    .end annotation

    .line 187
    iput-object p1, p0, Lcom/xiaomi/abtest/c/a;->c:Ljava/util/ArrayList;

    .line 188
    return-void
.end method

.method public a(Ljava/util/Map;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 75
    const/4 v0, 0x0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/abtest/c/a;->b()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 76
    return v0

    .line 81
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/abtest/c/a;->b:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    sget-object v2, Lcom/xiaomi/abtest/EnumType$ConditionRelation;->AND:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    const/4 v3, 0x1

    if-ne v1, v2, :cond_1

    .line 82
    move v1, v3

    goto :goto_0

    .line 83
    :cond_1
    iget-object v1, p0, Lcom/xiaomi/abtest/c/a;->b:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    sget-object v2, Lcom/xiaomi/abtest/EnumType$ConditionRelation;->OR:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    if-ne v1, v2, :cond_26

    .line 84
    move v1, v0

    .line 89
    :goto_0
    iget-object v2, p0, Lcom/xiaomi/abtest/c/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/abtest/c/a$a;

    .line 90
    iget-object v5, v4, Lcom/xiaomi/abtest/c/a$a;->a:Ljava/lang/String;

    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 91
    if-nez v5, :cond_2

    .line 93
    return v0

    .line 96
    :cond_2
    nop

    .line 98
    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->b:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    sget-object v7, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_EQ:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    if-ne v6, v7, :cond_3

    .line 99
    iget-object v4, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto/16 :goto_7

    .line 100
    :cond_3
    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->b:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    sget-object v7, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_GT:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    const/4 v8, 0x2

    const-string v9, "null"

    const/4 v10, 0x3

    const-string v11, "%s value type not match, get:%s but need:%s"

    if-ne v6, v7, :cond_9

    .line 101
    invoke-static {v5}, Lcom/xiaomi/abtest/d/l;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Lcom/xiaomi/abtest/d/l;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 102
    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    iget-object v4, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v7

    cmpl-double v4, v5, v7

    if-lez v4, :cond_4

    move v4, v3

    goto/16 :goto_7

    :cond_4
    move v4, v0

    goto/16 :goto_7

    .line 103
    :cond_5
    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    instance-of v6, v6, Ljava/lang/String;

    if-eqz v6, :cond_7

    .line 104
    iget-object v4, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_6

    move v4, v3

    goto/16 :goto_7

    :cond_6
    move v4, v0

    goto/16 :goto_7

    .line 106
    :cond_7
    new-array p1, v10, [Ljava/lang/Object;

    iget-object v1, v4, Lcom/xiaomi/abtest/c/a$a;->a:Ljava/lang/String;

    aput-object v1, p1, v0

    if-nez v5, :cond_8

    goto :goto_2

    .line 107
    :cond_8
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    :goto_2
    aput-object v9, p1, v3

    iget-object v0, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v8

    .line 106
    invoke-static {v11, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 108
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_9
    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->b:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    sget-object v7, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_GE:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    if-ne v6, v7, :cond_f

    .line 111
    invoke-static {v5}, Lcom/xiaomi/abtest/d/l;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Lcom/xiaomi/abtest/d/l;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 112
    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    iget-object v4, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v7

    cmpl-double v4, v5, v7

    if-ltz v4, :cond_a

    move v4, v3

    goto/16 :goto_7

    :cond_a
    move v4, v0

    goto/16 :goto_7

    .line 113
    :cond_b
    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    instance-of v6, v6, Ljava/lang/String;

    if-eqz v6, :cond_d

    .line 114
    if-eqz v1, :cond_c

    iget-object v1, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_c

    move v1, v3

    goto :goto_3

    :cond_c
    move v1, v0

    :goto_3
    move v4, v0

    goto/16 :goto_7

    .line 116
    :cond_d
    new-array p1, v10, [Ljava/lang/Object;

    iget-object v1, v4, Lcom/xiaomi/abtest/c/a$a;->a:Ljava/lang/String;

    aput-object v1, p1, v0

    if-nez v5, :cond_e

    goto :goto_4

    .line 117
    :cond_e
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    :goto_4
    aput-object v9, p1, v3

    iget-object v0, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v8

    .line 116
    invoke-static {v11, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 118
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_f
    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->b:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    sget-object v7, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_LT:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    if-ne v6, v7, :cond_15

    .line 121
    invoke-static {v5}, Lcom/xiaomi/abtest/d/l;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_11

    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Lcom/xiaomi/abtest/d/l;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 122
    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    iget-object v4, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v7

    cmpg-double v4, v5, v7

    if-gez v4, :cond_10

    move v4, v3

    goto/16 :goto_7

    :cond_10
    move v4, v0

    goto/16 :goto_7

    .line 123
    :cond_11
    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    instance-of v6, v6, Ljava/lang/String;

    if-eqz v6, :cond_13

    .line 124
    iget-object v4, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_12

    move v4, v3

    goto/16 :goto_7

    :cond_12
    move v4, v0

    goto/16 :goto_7

    .line 126
    :cond_13
    new-array p1, v10, [Ljava/lang/Object;

    iget-object v1, v4, Lcom/xiaomi/abtest/c/a$a;->a:Ljava/lang/String;

    aput-object v1, p1, v0

    if-nez v5, :cond_14

    goto :goto_5

    .line 127
    :cond_14
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    :goto_5
    aput-object v9, p1, v3

    iget-object v0, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v8

    .line 126
    invoke-static {v11, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 128
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_15
    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->b:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    sget-object v7, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_LE:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    if-ne v6, v7, :cond_1b

    .line 131
    invoke-static {v5}, Lcom/xiaomi/abtest/d/l;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_17

    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Lcom/xiaomi/abtest/d/l;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 132
    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    iget-object v4, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v7

    cmpg-double v4, v5, v7

    if-gtz v4, :cond_16

    move v4, v3

    goto :goto_7

    :cond_16
    move v4, v0

    goto :goto_7

    .line 133
    :cond_17
    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    instance-of v6, v6, Ljava/lang/String;

    if-eqz v6, :cond_19

    .line 134
    iget-object v4, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-gtz v4, :cond_18

    move v4, v3

    goto :goto_7

    :cond_18
    move v4, v0

    goto :goto_7

    .line 136
    :cond_19
    new-array p1, v10, [Ljava/lang/Object;

    iget-object v1, v4, Lcom/xiaomi/abtest/c/a$a;->a:Ljava/lang/String;

    aput-object v1, p1, v0

    if-nez v5, :cond_1a

    goto :goto_6

    .line 137
    :cond_1a
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    :goto_6
    aput-object v9, p1, v3

    iget-object v0, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v8

    .line 136
    invoke-static {v11, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 138
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_1b
    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->b:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    sget-object v7, Lcom/xiaomi/abtest/EnumType$ConditionOperator;->OP_IN:Lcom/xiaomi/abtest/EnumType$ConditionOperator;

    if-ne v6, v7, :cond_24

    .line 142
    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    instance-of v6, v6, Ljava/util/HashSet;

    if-eqz v6, :cond_23

    .line 145
    iget-object v6, v4, Lcom/xiaomi/abtest/c/a$a;->c:Ljava/lang/Object;

    check-cast v6, Ljava/util/HashSet;

    invoke-virtual {v6, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    .line 146
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v4, v4, Lcom/xiaomi/abtest/c/a$a;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    filled-new-array {v4, v5, v8}, [Ljava/lang/Object;

    move-result-object v4

    const-string v5, "%s contains %s:%s\n"

    invoke-virtual {v7, v5, v4}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    move v4, v6

    .line 154
    :goto_7
    if-nez v4, :cond_1c

    iget-object v5, p0, Lcom/xiaomi/abtest/c/a;->b:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    sget-object v6, Lcom/xiaomi/abtest/EnumType$ConditionRelation;->AND:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    if-ne v5, v6, :cond_1c

    .line 155
    return v0

    .line 159
    :cond_1c
    if-ne v4, v3, :cond_1d

    iget-object v5, p0, Lcom/xiaomi/abtest/c/a;->b:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    sget-object v6, Lcom/xiaomi/abtest/EnumType$ConditionRelation;->OR:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    if-ne v5, v6, :cond_1d

    .line 160
    return v3

    .line 163
    :cond_1d
    iget-object v5, p0, Lcom/xiaomi/abtest/c/a;->b:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    sget-object v6, Lcom/xiaomi/abtest/EnumType$ConditionRelation;->AND:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    if-ne v5, v6, :cond_1f

    .line 164
    if-eqz v1, :cond_1e

    if-eqz v4, :cond_1e

    move v1, v3

    goto :goto_9

    :cond_1e
    move v1, v0

    goto :goto_9

    .line 165
    :cond_1f
    iget-object v5, p0, Lcom/xiaomi/abtest/c/a;->b:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    sget-object v6, Lcom/xiaomi/abtest/EnumType$ConditionRelation;->OR:Lcom/xiaomi/abtest/EnumType$ConditionRelation;

    if-ne v5, v6, :cond_22

    .line 166
    if-nez v1, :cond_21

    if-eqz v4, :cond_20

    goto :goto_8

    :cond_20
    move v1, v0

    goto :goto_9

    :cond_21
    :goto_8
    move v1, v3

    .line 168
    :cond_22
    :goto_9
    goto/16 :goto_1

    .line 143
    :cond_23
    new-instance p1, Ljava/lang/Exception;

    const-string v0, "operator is IN, but property value is not a SET"

    invoke-direct {p1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p1

    .line 149
    :cond_24
    new-instance p1, Ljava/lang/Exception;

    const-string v0, "invalid operator"

    invoke-direct {p1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p1

    .line 170
    :cond_25
    return v1

    .line 86
    :cond_26
    new-instance p1, Ljava/lang/Exception;

    const-string v0, "invalid relation"

    invoke-direct {p1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/xiaomi/abtest/c/a$a;",
            ">;"
        }
    .end annotation

    .line 179
    iget-object v0, p0, Lcom/xiaomi/abtest/c/a;->c:Ljava/util/ArrayList;

    return-object v0
.end method
