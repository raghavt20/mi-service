public class com.xiaomi.abtest.c.g extends com.xiaomi.abtest.c.e {
	 /* # static fields */
	 private static final java.lang.String n;
	 /* # instance fields */
	 private com.xiaomi.abtest.EnumType$FlowUnitType o;
	 /* # direct methods */
	 public com.xiaomi.abtest.c.g ( ) {
		 /* .locals 11 */
		 /* .line 22 */
		 /* move-object v10, p0 */
		 /* move-object v0, p0 */
		 /* move v1, p1 */
		 /* move-object v2, p2 */
		 /* move-object v3, p3 */
		 /* move v4, p4 */
		 /* move-object/from16 v5, p5 */
		 /* move/from16 v6, p7 */
		 /* move-object/from16 v7, p6 */
		 /* move-object/from16 v8, p8 */
		 /* move-object/from16 v9, p9 */
		 /* invoke-direct/range {v0 ..v9}, Lcom/xiaomi/abtest/c/e;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;ILcom/xiaomi/abtest/EnumType$DiversionType;Ljava/lang/String;Ljava/lang/String;)V */
		 /* .line 24 */
		 v0 = com.xiaomi.abtest.EnumType$FlowUnitType.TYPE_UNKNOWN;
		 this.o = v0;
		 /* .line 25 */
		 /* new-instance v0, Ljava/util/ArrayList; */
		 /* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
		 this.i = v0;
		 /* .line 26 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void a ( Object p0, java.util.List p1 ) {
		 /* .locals 5 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Lcom/xiaomi/abtest/b/a;", */
		 /* "Ljava/util/List<", */
		 /* "Lcom/xiaomi/abtest/c/e;", */
		 /* ">;)V" */
		 /* } */
	 } // .end annotation
	 /* .line 59 */
	 v0 = 	 (( com.xiaomi.abtest.c.g ) p0 ).a ( ); // invoke-virtual {p0}, Lcom/xiaomi/abtest/c/g;->a()I
	 java.lang.Integer .valueOf ( v0 );
	 (( com.xiaomi.abtest.c.g ) p0 ).b ( ); // invoke-virtual {p0}, Lcom/xiaomi/abtest/c/g;->b()Ljava/lang/String;
	 /* filled-new-array {v0, v1}, [Ljava/lang/Object; */
	 final String v1 = "id: %d, name: %s"; // const-string v1, "id: %d, name: %s"
	 java.lang.String .format ( v1,v0 );
	 final String v1 = "Layer"; // const-string v1, "Layer"
	 com.xiaomi.abtest.d.k .d ( v1,v0 );
	 /* .line 60 */
	 v0 = 	 v0 = this.i;
	 /* if-gtz v0, :cond_0 */
	 /* .line 61 */
	 final String p1 = "no subdomain or experiment in this layer."; // const-string p1, "no subdomain or experiment in this layer."
	 com.xiaomi.abtest.d.k .d ( v1,p1 );
	 /* .line 62 */
	 return;
	 /* .line 65 */
} // :cond_0
v0 = (( com.xiaomi.abtest.c.g ) p0 ).b ( p1 ); // invoke-virtual {p0, p1}, Lcom/xiaomi/abtest/c/g;->b(Lcom/xiaomi/abtest/b/a;)I
/* .line 66 */
java.lang.Integer .valueOf ( v0 );
/* filled-new-array {v2}, [Ljava/lang/Object; */
final String v3 = "bucketId: %s"; // const-string v3, "bucketId: %s"
java.lang.String .format ( v3,v2 );
com.xiaomi.abtest.d.k .d ( v1,v2 );
/* .line 67 */
int v2 = -1; // const/4 v2, -0x1
/* if-eq v0, v2, :cond_4 */
/* .line 68 */
v2 = this.i;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Lcom/xiaomi/abtest/c/e; */
/* .line 69 */
v4 = (( com.xiaomi.abtest.c.e ) v3 ).a ( v0 ); // invoke-virtual {v3, v0}, Lcom/xiaomi/abtest/c/e;->a(I)Z
if ( v4 != null) { // if-eqz v4, :cond_2
	 /* .line 70 */
	 v4 = 	 (( com.xiaomi.abtest.c.e ) v3 ).a ( p1 ); // invoke-virtual {v3, p1}, Lcom/xiaomi/abtest/c/e;->a(Lcom/xiaomi/abtest/b/a;)Z
	 if ( v4 != null) { // if-eqz v4, :cond_1
		 /* .line 71 */
		 (( com.xiaomi.abtest.c.e ) v3 ).a ( p1, p2 ); // invoke-virtual {v3, p1, p2}, Lcom/xiaomi/abtest/c/e;->a(Lcom/xiaomi/abtest/b/a;Ljava/util/List;)V
		 /* .line 73 */
	 } // :cond_1
	 (( com.xiaomi.abtest.c.e ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/xiaomi/abtest/c/e;->toString()Ljava/lang/String;
	 /* filled-new-array {v3}, [Ljava/lang/Object; */
	 final String v4 = "check condition failed for:%s"; // const-string v4, "check condition failed for:%s"
	 java.lang.String .format ( v4,v3 );
	 com.xiaomi.abtest.d.k .d ( v1,v3 );
	 /* .line 76 */
} // :cond_2
} // :goto_1
} // :cond_3
/* .line 78 */
} // :cond_4
final String p1 = "bucketId is illegal, stop traffic"; // const-string p1, "bucketId is illegal, stop traffic"
com.xiaomi.abtest.d.k .d ( v1,p1 );
/* .line 80 */
} // :goto_2
return;
} // .end method
public void a ( Object p0 ) {
/* .locals 3 */
/* .line 31 */
v0 = this.i;
final String v1 = "Layer"; // const-string v1, "Layer"
/* if-nez v0, :cond_0 */
/* .line 32 */
final String p1 = "children haven\'t been initialized"; // const-string p1, "children haven\'t been initialized"
com.xiaomi.abtest.d.k .c ( v1,p1 );
/* .line 33 */
return;
/* .line 37 */
} // :cond_0
(( com.xiaomi.abtest.c.e ) p1 ).c ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/c/e;->c()Lcom/xiaomi/abtest/EnumType$FlowUnitType;
v2 = com.xiaomi.abtest.EnumType$FlowUnitType.TYPE_DOMAIN;
v0 = (( com.xiaomi.abtest.EnumType$FlowUnitType ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
(( com.xiaomi.abtest.c.e ) p1 ).c ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/c/e;->c()Lcom/xiaomi/abtest/EnumType$FlowUnitType;
v2 = com.xiaomi.abtest.EnumType$FlowUnitType.TYPE_EXPERIMENT;
v0 = (( com.xiaomi.abtest.EnumType$FlowUnitType ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
(( com.xiaomi.abtest.c.e ) p1 ).c ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/c/e;->c()Lcom/xiaomi/abtest/EnumType$FlowUnitType;
v2 = com.xiaomi.abtest.EnumType$FlowUnitType.TYPE_EXP_CONTAINER;
v0 = (( com.xiaomi.abtest.EnumType$FlowUnitType ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 38 */
final String p1 = "added child must be TYPE_DOMAIN or TYPE_EXPERIMENT or TYPE_EXPERIMENT"; // const-string p1, "added child must be TYPE_DOMAIN or TYPE_EXPERIMENT or TYPE_EXPERIMENT"
com.xiaomi.abtest.d.k .c ( v1,p1 );
/* .line 39 */
return;
/* .line 43 */
} // :cond_1
v0 = this.o;
v2 = com.xiaomi.abtest.EnumType$FlowUnitType.TYPE_UNKNOWN;
v0 = (( com.xiaomi.abtest.EnumType$FlowUnitType ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_3 */
v0 = this.o;
(( com.xiaomi.abtest.c.e ) p1 ).c ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/c/e;->c()Lcom/xiaomi/abtest/EnumType$FlowUnitType;
v0 = (( com.xiaomi.abtest.EnumType$FlowUnitType ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 48 */
} // :cond_2
final String p1 = "child of layer must be "; // const-string p1, "child of layer must be "
com.xiaomi.abtest.d.k .c ( v1,p1 );
/* .line 46 */
} // :cond_3
} // :goto_0
v0 = this.i;
/* .line 50 */
} // :goto_1
return;
} // .end method
public Boolean a ( Object p0 ) {
/* .locals 0 */
/* .line 54 */
int p1 = 0; // const/4 p1, 0x0
} // .end method
