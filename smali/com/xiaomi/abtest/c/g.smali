.class public Lcom/xiaomi/abtest/c/g;
.super Lcom/xiaomi/abtest/c/e;


# static fields
.field private static final n:Ljava/lang/String; = "Layer"


# instance fields
.field private o:Lcom/xiaomi/abtest/EnumType$FlowUnitType;


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Lcom/xiaomi/abtest/EnumType$DiversionType;ILjava/lang/String;Ljava/lang/String;)V
    .locals 11

    .line 22
    move-object v10, p0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p7

    move-object/from16 v7, p6

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v9}, Lcom/xiaomi/abtest/c/e;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;ILcom/xiaomi/abtest/EnumType$DiversionType;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    sget-object v0, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->TYPE_UNKNOWN:Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    iput-object v0, v10, Lcom/xiaomi/abtest/c/g;->o:Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v10, Lcom/xiaomi/abtest/c/g;->i:Ljava/util/List;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/abtest/b/a;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/abtest/b/a;",
            "Ljava/util/List<",
            "Lcom/xiaomi/abtest/c/e;",
            ">;)V"
        }
    .end annotation

    .line 59
    invoke-virtual {p0}, Lcom/xiaomi/abtest/c/g;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/abtest/c/g;->b()Ljava/lang/String;

    move-result-object v1

    filled-new-array {v0, v1}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "id: %d, name: %s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Layer"

    invoke-static {v1, v0}, Lcom/xiaomi/abtest/d/k;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/xiaomi/abtest/c/g;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 61
    const-string p1, "no subdomain or experiment in this layer."

    invoke-static {v1, p1}, Lcom/xiaomi/abtest/d/k;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    return-void

    .line 65
    :cond_0
    invoke-virtual {p0, p1}, Lcom/xiaomi/abtest/c/g;->b(Lcom/xiaomi/abtest/b/a;)I

    move-result v0

    .line 66
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "bucketId: %s"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xiaomi/abtest/d/k;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const/4 v2, -0x1

    if-eq v0, v2, :cond_4

    .line 68
    iget-object v2, p0, Lcom/xiaomi/abtest/c/g;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/abtest/c/e;

    .line 69
    invoke-virtual {v3, v0}, Lcom/xiaomi/abtest/c/e;->a(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 70
    invoke-virtual {v3, p1}, Lcom/xiaomi/abtest/c/e;->a(Lcom/xiaomi/abtest/b/a;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 71
    invoke-virtual {v3, p1, p2}, Lcom/xiaomi/abtest/c/e;->a(Lcom/xiaomi/abtest/b/a;Ljava/util/List;)V

    goto :goto_1

    .line 73
    :cond_1
    invoke-virtual {v3}, Lcom/xiaomi/abtest/c/e;->toString()Ljava/lang/String;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    const-string v4, "check condition failed for:%s"

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/xiaomi/abtest/d/k;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_2
    :goto_1
    goto :goto_0

    :cond_3
    goto :goto_2

    .line 78
    :cond_4
    const-string p1, "bucketId is illegal, stop traffic"

    invoke-static {v1, p1}, Lcom/xiaomi/abtest/d/k;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :goto_2
    return-void
.end method

.method public a(Lcom/xiaomi/abtest/c/e;)V
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/xiaomi/abtest/c/g;->i:Ljava/util/List;

    const-string v1, "Layer"

    if-nez v0, :cond_0

    .line 32
    const-string p1, "children haven\'t been initialized"

    invoke-static {v1, p1}, Lcom/xiaomi/abtest/d/k;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    return-void

    .line 37
    :cond_0
    invoke-virtual {p1}, Lcom/xiaomi/abtest/c/e;->c()Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->TYPE_DOMAIN:Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    invoke-virtual {v0, v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/xiaomi/abtest/c/e;->c()Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->TYPE_EXPERIMENT:Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    invoke-virtual {v0, v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/xiaomi/abtest/c/e;->c()Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->TYPE_EXP_CONTAINER:Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    invoke-virtual {v0, v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 38
    const-string p1, "added child must be TYPE_DOMAIN or TYPE_EXPERIMENT or TYPE_EXPERIMENT"

    invoke-static {v1, p1}, Lcom/xiaomi/abtest/d/k;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void

    .line 43
    :cond_1
    iget-object v0, p0, Lcom/xiaomi/abtest/c/g;->o:Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    sget-object v2, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->TYPE_UNKNOWN:Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    invoke-virtual {v0, v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/abtest/c/g;->o:Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    invoke-virtual {p1}, Lcom/xiaomi/abtest/c/e;->c()Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 48
    :cond_2
    const-string p1, "child of layer must be "

    invoke-static {v1, p1}, Lcom/xiaomi/abtest/d/k;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 46
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/abtest/c/g;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    :goto_1
    return-void
.end method

.method public a(Lcom/xiaomi/abtest/b/a;)Z
    .locals 0

    .line 54
    const/4 p1, 0x0

    return p1
.end method
