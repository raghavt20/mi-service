public class com.xiaomi.abtest.c.b extends com.xiaomi.abtest.c.e {
	 /* # static fields */
	 private static final java.lang.String n;
	 /* # direct methods */
	 public com.xiaomi.abtest.c.b ( ) {
		 /* .locals 9 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(I", */
		 /* "Ljava/lang/String;", */
		 /* "Lcom/xiaomi/abtest/EnumType$FlowUnitType;", */
		 /* "I", */
		 /* "Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;", */
		 /* "Ljava/util/TreeSet<", */
		 /* "Ljava/lang/Integer;", */
		 /* ">;", */
		 /* "Ljava/lang/String;", */
		 /* "Ljava/lang/String;", */
		 /* "Ljava/lang/String;", */
		 /* ")V" */
		 /* } */
	 } // .end annotation
	 /* .line 19 */
	 /* move-object v8, p0 */
	 /* move-object v0, p0 */
	 /* move v1, p1 */
	 /* move-object v2, p2 */
	 /* move-object v3, p3 */
	 /* move v4, p4 */
	 /* move-object v5, p5 */
	 /* move-object/from16 v6, p8 */
	 /* move-object/from16 v7, p9 */
	 /* invoke-direct/range {v0 ..v7}, Lcom/xiaomi/abtest/c/e;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Ljava/lang/String;Ljava/lang/String;)V */
	 /* .line 21 */
	 /* new-instance v0, Ljava/util/ArrayList; */
	 /* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
	 this.i = v0;
	 /* .line 22 */
	 /* move-object v0, p6 */
	 this.j = v0;
	 /* .line 23 */
	 /* invoke-static/range {p7 ..p7}, Lcom/xiaomi/abtest/c/a;->a(Ljava/lang/String;)Lcom/xiaomi/abtest/c/a; */
	 this.l = v0;
	 /* .line 24 */
	 return;
} // .end method
/* # virtual methods */
public void a ( Object p0, java.util.List p1 ) {
	 /* .locals 2 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Lcom/xiaomi/abtest/b/a;", */
	 /* "Ljava/util/List<", */
	 /* "Lcom/xiaomi/abtest/c/e;", */
	 /* ">;)V" */
	 /* } */
} // .end annotation
/* .line 44 */
v0 = (( com.xiaomi.abtest.c.b ) p0 ).a ( ); // invoke-virtual {p0}, Lcom/xiaomi/abtest/c/b;->a()I
java.lang.Integer .valueOf ( v0 );
(( com.xiaomi.abtest.c.b ) p0 ).b ( ); // invoke-virtual {p0}, Lcom/xiaomi/abtest/c/b;->b()Ljava/lang/String;
/* filled-new-array {v0, v1}, [Ljava/lang/Object; */
final String v1 = "id: %d, name: %s"; // const-string v1, "id: %d, name: %s"
java.lang.String .format ( v1,v0 );
final String v1 = "Domain"; // const-string v1, "Domain"
com.xiaomi.abtest.d.k .d ( v1,v0 );
/* .line 45 */
v0 = v0 = this.i;
/* if-gtz v0, :cond_0 */
/* .line 46 */
final String p1 = "no layer in this domain."; // const-string p1, "no layer in this domain."
com.xiaomi.abtest.d.k .d ( v1,p1 );
/* .line 47 */
return;
/* .line 50 */
} // :cond_0
v0 = this.i;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/xiaomi/abtest/c/e; */
/* .line 51 */
(( com.xiaomi.abtest.c.e ) v1 ).a ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/xiaomi/abtest/c/e;->a(Lcom/xiaomi/abtest/b/a;Ljava/util/List;)V
/* .line 52 */
/* .line 53 */
} // :cond_1
return;
} // .end method
public void a ( Object p0 ) {
/* .locals 3 */
/* .line 28 */
v0 = this.i;
final String v1 = "Domain"; // const-string v1, "Domain"
/* if-nez v0, :cond_0 */
/* .line 29 */
final String p1 = "children haven\'t been initialized"; // const-string p1, "children haven\'t been initialized"
com.xiaomi.abtest.d.k .c ( v1,p1 );
/* .line 30 */
return;
/* .line 34 */
} // :cond_0
(( com.xiaomi.abtest.c.e ) p1 ).c ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/c/e;->c()Lcom/xiaomi/abtest/EnumType$FlowUnitType;
v2 = com.xiaomi.abtest.EnumType$FlowUnitType.TYPE_LAYER;
v0 = (( com.xiaomi.abtest.EnumType$FlowUnitType ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 35 */
final String p1 = "added child must be TYPE_LAYER"; // const-string p1, "added child must be TYPE_LAYER"
com.xiaomi.abtest.d.k .c ( v1,p1 );
/* .line 36 */
return;
/* .line 39 */
} // :cond_1
v0 = this.i;
/* .line 40 */
return;
} // .end method
