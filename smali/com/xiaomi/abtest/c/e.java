public class com.xiaomi.abtest.c.e {
	 /* # static fields */
	 private static final java.lang.String n;
	 /* # instance fields */
	 protected Integer a;
	 protected java.lang.String b;
	 protected com.xiaomi.abtest.EnumType$FlowUnitType c;
	 protected Integer d;
	 protected com.xiaomi.abtest.EnumType$FlowUnitStatus e;
	 protected java.lang.String f;
	 protected java.lang.String g;
	 protected com.xiaomi.abtest.EnumType$DiversionType h;
	 protected java.util.List i;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Lcom/xiaomi/abtest/c/e;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
protected java.util.Set j;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
protected java.util.Map k;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
protected com.xiaomi.abtest.c.a l;
protected Integer m;
/* # direct methods */
public com.xiaomi.abtest.c.e ( ) {
/* .locals 9 */
/* .line 51 */
/* move-object v8, p0 */
/* move-object v0, p0 */
/* move v1, p1 */
/* move-object v2, p2 */
/* move-object v3, p3 */
/* move v4, p4 */
/* move-object v5, p5 */
/* move-object/from16 v6, p8 */
/* move-object/from16 v7, p9 */
/* invoke-direct/range {v0 ..v7}, Lcom/xiaomi/abtest/c/e;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 52 */
/* move v0, p6 */
/* iput v0, v8, Lcom/xiaomi/abtest/c/e;->m:I */
/* .line 53 */
/* move-object/from16 v0, p7 */
this.h = v0;
/* .line 54 */
return;
} // .end method
public com.xiaomi.abtest.c.e ( ) {
/* .locals 0 */
/* .line 39 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 40 */
/* iput p1, p0, Lcom/xiaomi/abtest/c/e;->a:I */
/* .line 41 */
this.b = p2;
/* .line 42 */
this.c = p3;
/* .line 43 */
/* iput p4, p0, Lcom/xiaomi/abtest/c/e;->d:I */
/* .line 44 */
this.e = p5;
/* .line 45 */
this.f = p6;
/* .line 46 */
this.g = p7;
/* .line 47 */
/* new-instance p1, Ljava/util/HashMap; */
/* invoke-direct {p1}, Ljava/util/HashMap;-><init>()V */
this.k = p1;
/* .line 48 */
return;
} // .end method
/* # virtual methods */
public Integer a ( ) {
/* .locals 1 */
/* .line 185 */
/* iget v0, p0, Lcom/xiaomi/abtest/c/e;->a:I */
} // .end method
public void a ( com.xiaomi.abtest.EnumType$DiversionType p0 ) {
/* .locals 0 */
/* .line 176 */
this.h = p1;
/* .line 177 */
return;
} // .end method
public void a ( com.xiaomi.abtest.EnumType$FlowUnitStatus p0 ) {
/* .locals 0 */
/* .line 168 */
this.e = p1;
/* .line 169 */
return;
} // .end method
public void a ( com.xiaomi.abtest.EnumType$FlowUnitType p0 ) {
/* .locals 0 */
/* .line 160 */
this.c = p1;
/* .line 161 */
return;
} // .end method
public void a ( Object p0, java.util.List p1 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/xiaomi/abtest/b/a;", */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/abtest/c/e;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 99 */
final String p1 = "FlowUnit"; // const-string p1, "FlowUnit"
final String p2 = "Base class FlowUnit has no traffic method"; // const-string p2, "Base class FlowUnit has no traffic method"
com.xiaomi.abtest.d.k .c ( p1,p2 );
/* .line 100 */
return;
} // .end method
public void a ( Object p0 ) {
/* .locals 1 */
/* .line 74 */
v0 = this.i;
/* .line 75 */
return;
} // .end method
public void a ( java.lang.String p0 ) {
/* .locals 0 */
/* .line 156 */
this.b = p1;
/* .line 157 */
return;
} // .end method
public void a ( java.util.Map p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 148 */
v0 = this.k;
/* .line 149 */
return;
} // .end method
public Boolean a ( Integer p0 ) {
/* .locals 1 */
/* .line 78 */
v0 = this.j;
p1 = java.lang.Integer .valueOf ( p1 );
} // .end method
public Boolean a ( Object p0 ) {
/* .locals 1 */
/* .line 85 */
v0 = this.l;
/* if-nez v0, :cond_0 */
/* .line 86 */
int p1 = 1; // const/4 p1, 0x1
/* .line 90 */
} // :cond_0
try { // :try_start_0
(( com.xiaomi.abtest.b.a ) p1 ).c ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->c()Ljava/util/Map;
p1 = (( com.xiaomi.abtest.c.a ) v0 ).a ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/abtest/c/a;->a(Ljava/util/Map;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 91 */
/* :catch_0 */
/* move-exception p1 */
/* .line 92 */
final String v0 = "FlowUnit"; // const-string v0, "FlowUnit"
(( java.lang.Exception ) p1 ).getMessage ( ); // invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
com.xiaomi.abtest.d.k .b ( v0,p1 );
/* .line 93 */
int p1 = 0; // const/4 p1, 0x0
} // .end method
public Integer b ( Object p0 ) {
/* .locals 2 */
/* .line 103 */
/* nop */
/* .line 104 */
v0 = com.xiaomi.abtest.c.f.a;
v1 = this.h;
v1 = (( com.xiaomi.abtest.EnumType$DiversionType ) v1 ).ordinal ( ); // invoke-virtual {v1}, Lcom/xiaomi/abtest/EnumType$DiversionType;->ordinal()I
/* aget v0, v0, v1 */
final String v1 = "-"; // const-string v1, "-"
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 132 */
/* :pswitch_0 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.xiaomi.abtest.b.a ) p1 ).a ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) p1 ).append ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Calendar .getInstance ( );
int v1 = 2; // const/4 v1, 0x2
v0 = (( java.util.Calendar ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I
(( java.lang.StringBuilder ) p1 ).append ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
/* goto/16 :goto_1 */
/* .line 129 */
/* :pswitch_1 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.xiaomi.abtest.b.a ) p1 ).a ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) p1 ).append ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Calendar .getInstance ( );
int v1 = 3; // const/4 v1, 0x3
v0 = (( java.util.Calendar ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I
(( java.lang.StringBuilder ) p1 ).append ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
/* .line 130 */
/* goto/16 :goto_1 */
/* .line 126 */
/* :pswitch_2 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.xiaomi.abtest.b.a ) p1 ).a ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) p1 ).append ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Calendar .getInstance ( );
int v1 = 6; // const/4 v1, 0x6
v0 = (( java.util.Calendar ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I
(( java.lang.StringBuilder ) p1 ).append ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
/* .line 127 */
/* .line 122 */
/* :pswitch_3 */
/* const/16 p1, 0x14 */
/* new-array p1, p1, [B */
/* .line 123 */
/* new-instance v0, Ljava/util/Random; */
/* invoke-direct {v0}, Ljava/util/Random;-><init>()V */
(( java.util.Random ) v0 ).nextBytes ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/Random;->nextBytes([B)V
/* .line 124 */
/* .line 117 */
/* :pswitch_4 */
(( com.xiaomi.abtest.b.a ) p1 ).b ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->b()Ljava/lang/String;
v0 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v0, :cond_3 */
/* .line 118 */
(( com.xiaomi.abtest.b.a ) p1 ).b ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->b()Ljava/lang/String;
(( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
/* .line 106 */
/* :pswitch_5 */
(( com.xiaomi.abtest.b.a ) p1 ).a ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;
v0 = com.xiaomi.abtest.d.l .b ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 107 */
(( com.xiaomi.abtest.b.a ) p1 ).a ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;
(( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
/* .line 108 */
} // :cond_0
final String v0 = "ExpLayer"; // const-string v0, "ExpLayer"
(( com.xiaomi.abtest.c.e ) p0 ).b ( ); // invoke-virtual {p0}, Lcom/xiaomi/abtest/c/e;->b()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
final String v0 = "LaunchLayer"; // const-string v0, "LaunchLayer"
(( com.xiaomi.abtest.c.e ) p0 ).b ( ); // invoke-virtual {p0}, Lcom/xiaomi/abtest/c/e;->b()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 109 */
} // :cond_1
(( com.xiaomi.abtest.b.a ) p1 ).b ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->b()Ljava/lang/String;
v0 = com.xiaomi.abtest.d.l .b ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 110 */
(( com.xiaomi.abtest.b.a ) p1 ).b ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->b()Ljava/lang/String;
(( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
/* .line 112 */
} // :cond_2
final String p1 = "000"; // const-string p1, "000"
(( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
/* .line 143 */
} // :cond_3
} // :goto_0
int p1 = 0; // const/4 p1, 0x0
} // :goto_1
/* if-nez p1, :cond_4 */
int p1 = -1; // const/4 p1, -0x1
} // :cond_4
/* array-length v0, p1 */
/* iget v1, p0, Lcom/xiaomi/abtest/c/e;->m:I */
p1 = com.xiaomi.abtest.d.h .a ( p1,v0,v1 );
v0 = com.xiaomi.abtest.d.d.a;
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* rem-int/2addr p1, v0 */
} // :goto_2
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public java.lang.String b ( ) {
/* .locals 1 */
/* .line 189 */
v0 = this.b;
} // .end method
public void b ( Integer p0 ) {
/* .locals 0 */
/* .line 152 */
/* iput p1, p0, Lcom/xiaomi/abtest/c/e;->a:I */
/* .line 153 */
return;
} // .end method
public void b ( java.lang.String p0 ) {
/* .locals 0 */
/* .line 172 */
this.f = p1;
/* .line 173 */
return;
} // .end method
public com.xiaomi.abtest.EnumType$FlowUnitType c ( ) {
/* .locals 1 */
/* .line 193 */
v0 = this.c;
} // .end method
public void c ( Integer p0 ) {
/* .locals 0 */
/* .line 164 */
/* iput p1, p0, Lcom/xiaomi/abtest/c/e;->d:I */
/* .line 165 */
return;
} // .end method
public void c ( java.lang.String p0 ) {
/* .locals 0 */
/* .line 181 */
this.g = p1;
/* .line 182 */
return;
} // .end method
public Integer d ( ) {
/* .locals 1 */
/* .line 197 */
/* iget v0, p0, Lcom/xiaomi/abtest/c/e;->d:I */
} // .end method
public com.xiaomi.abtest.EnumType$FlowUnitStatus e ( ) {
/* .locals 1 */
/* .line 201 */
v0 = this.e;
} // .end method
public com.xiaomi.abtest.EnumType$DiversionType f ( ) {
/* .locals 1 */
/* .line 205 */
v0 = this.h;
} // .end method
public java.util.List g ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/abtest/c/e;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 209 */
v0 = this.i;
} // .end method
public java.util.Set h ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 213 */
v0 = this.j;
} // .end method
public java.util.Map i ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 217 */
v0 = this.k;
} // .end method
public java.lang.String j ( ) {
/* .locals 1 */
/* .line 221 */
v0 = this.f;
} // .end method
public java.lang.String k ( ) {
/* .locals 1 */
/* .line 225 */
v0 = this.g;
} // .end method
public java.lang.String toString ( ) {
/* .locals 6 */
/* .line 58 */
/* nop */
/* .line 59 */
/* nop */
/* .line 60 */
/* nop */
/* .line 61 */
v0 = this.k;
final String v1 = ""; // const-string v1, ""
/* move-object v2, v1 */
v3 = } // :goto_0
final String v4 = ","; // const-string v4, ","
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Ljava/util/Map$Entry; */
/* .line 62 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v5, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "="; // const-string v5, "="
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v3, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 63 */
/* .line 64 */
} // :cond_0
v0 = this.j;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 65 */
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Ljava/lang/Integer; */
/* .line 66 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( v3 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 67 */
/* .line 69 */
} // :cond_1
v0 = (( com.xiaomi.abtest.c.e ) p0 ).a ( ); // invoke-virtual {p0}, Lcom/xiaomi/abtest/c/e;->a()I
java.lang.Integer .valueOf ( v0 );
(( com.xiaomi.abtest.c.e ) p0 ).b ( ); // invoke-virtual {p0}, Lcom/xiaomi/abtest/c/e;->b()Ljava/lang/String;
(( com.xiaomi.abtest.c.e ) p0 ).c ( ); // invoke-virtual {p0}, Lcom/xiaomi/abtest/c/e;->c()Lcom/xiaomi/abtest/EnumType$FlowUnitType;
(( com.xiaomi.abtest.EnumType$FlowUnitType ) v4 ).toString ( ); // invoke-virtual {v4}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->toString()Ljava/lang/String;
/* filled-new-array {v0, v3, v4, v2, v1}, [Ljava/lang/Object; */
final String v1 = "%d,%s,%s,params:[%s] bucketIds:[%s]"; // const-string v1, "%d,%s,%s,params:[%s] bucketIds:[%s]"
java.lang.String .format ( v1,v0 );
/* .line 70 */
} // .end method
