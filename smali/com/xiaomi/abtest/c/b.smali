.class public Lcom/xiaomi/abtest/c/b;
.super Lcom/xiaomi/abtest/c/e;


# static fields
.field private static final n:Ljava/lang/String; = "Domain"


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Ljava/util/TreeSet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lcom/xiaomi/abtest/EnumType$FlowUnitType;",
            "I",
            "Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;",
            "Ljava/util/TreeSet<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 19
    move-object v8, p0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Lcom/xiaomi/abtest/c/e;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v8, Lcom/xiaomi/abtest/c/b;->i:Ljava/util/List;

    .line 22
    move-object v0, p6

    iput-object v0, v8, Lcom/xiaomi/abtest/c/b;->j:Ljava/util/Set;

    .line 23
    invoke-static/range {p7 .. p7}, Lcom/xiaomi/abtest/c/a;->a(Ljava/lang/String;)Lcom/xiaomi/abtest/c/a;

    move-result-object v0

    iput-object v0, v8, Lcom/xiaomi/abtest/c/b;->l:Lcom/xiaomi/abtest/c/a;

    .line 24
    return-void
.end method


# virtual methods
.method public a(Lcom/xiaomi/abtest/b/a;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/abtest/b/a;",
            "Ljava/util/List<",
            "Lcom/xiaomi/abtest/c/e;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-virtual {p0}, Lcom/xiaomi/abtest/c/b;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/abtest/c/b;->b()Ljava/lang/String;

    move-result-object v1

    filled-new-array {v0, v1}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "id: %d, name: %s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Domain"

    invoke-static {v1, v0}, Lcom/xiaomi/abtest/d/k;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/xiaomi/abtest/c/b;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 46
    const-string p1, "no layer in this domain."

    invoke-static {v1, p1}, Lcom/xiaomi/abtest/d/k;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    return-void

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/abtest/c/b;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/abtest/c/e;

    .line 51
    invoke-virtual {v1, p1, p2}, Lcom/xiaomi/abtest/c/e;->a(Lcom/xiaomi/abtest/b/a;Ljava/util/List;)V

    .line 52
    goto :goto_0

    .line 53
    :cond_1
    return-void
.end method

.method public a(Lcom/xiaomi/abtest/c/e;)V
    .locals 3

    .line 28
    iget-object v0, p0, Lcom/xiaomi/abtest/c/b;->i:Ljava/util/List;

    const-string v1, "Domain"

    if-nez v0, :cond_0

    .line 29
    const-string p1, "children haven\'t been initialized"

    invoke-static {v1, p1}, Lcom/xiaomi/abtest/d/k;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    return-void

    .line 34
    :cond_0
    invoke-virtual {p1}, Lcom/xiaomi/abtest/c/e;->c()Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->TYPE_LAYER:Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    invoke-virtual {v0, v2}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 35
    const-string p1, "added child must be TYPE_LAYER"

    invoke-static {v1, p1}, Lcom/xiaomi/abtest/d/k;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    return-void

    .line 39
    :cond_1
    iget-object v0, p0, Lcom/xiaomi/abtest/c/b;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method
