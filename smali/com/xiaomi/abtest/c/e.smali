.class public Lcom/xiaomi/abtest/c/e;
.super Ljava/lang/Object;


# static fields
.field private static final n:Ljava/lang/String; = "FlowUnit"


# instance fields
.field protected a:I

.field protected b:Ljava/lang/String;

.field protected c:Lcom/xiaomi/abtest/EnumType$FlowUnitType;

.field protected d:I

.field protected e:Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

.field protected f:Ljava/lang/String;

.field protected g:Ljava/lang/String;

.field protected h:Lcom/xiaomi/abtest/EnumType$DiversionType;

.field protected i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/abtest/c/e;",
            ">;"
        }
    .end annotation
.end field

.field protected j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected l:Lcom/xiaomi/abtest/c/a;

.field protected m:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;ILcom/xiaomi/abtest/EnumType$DiversionType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .line 51
    move-object v8, p0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Lcom/xiaomi/abtest/c/e;-><init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    move v0, p6

    iput v0, v8, Lcom/xiaomi/abtest/c/e;->m:I

    .line 53
    move-object/from16 v0, p7

    iput-object v0, v8, Lcom/xiaomi/abtest/c/e;->h:Lcom/xiaomi/abtest/EnumType$DiversionType;

    .line 54
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Lcom/xiaomi/abtest/EnumType$FlowUnitType;ILcom/xiaomi/abtest/EnumType$FlowUnitStatus;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput p1, p0, Lcom/xiaomi/abtest/c/e;->a:I

    .line 41
    iput-object p2, p0, Lcom/xiaomi/abtest/c/e;->b:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lcom/xiaomi/abtest/c/e;->c:Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    .line 43
    iput p4, p0, Lcom/xiaomi/abtest/c/e;->d:I

    .line 44
    iput-object p5, p0, Lcom/xiaomi/abtest/c/e;->e:Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    .line 45
    iput-object p6, p0, Lcom/xiaomi/abtest/c/e;->f:Ljava/lang/String;

    .line 46
    iput-object p7, p0, Lcom/xiaomi/abtest/c/e;->g:Ljava/lang/String;

    .line 47
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/abtest/c/e;->k:Ljava/util/Map;

    .line 48
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 185
    iget v0, p0, Lcom/xiaomi/abtest/c/e;->a:I

    return v0
.end method

.method public a(Lcom/xiaomi/abtest/EnumType$DiversionType;)V
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/xiaomi/abtest/c/e;->h:Lcom/xiaomi/abtest/EnumType$DiversionType;

    .line 177
    return-void
.end method

.method public a(Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;)V
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/xiaomi/abtest/c/e;->e:Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    .line 169
    return-void
.end method

.method public a(Lcom/xiaomi/abtest/EnumType$FlowUnitType;)V
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/xiaomi/abtest/c/e;->c:Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    .line 161
    return-void
.end method

.method public a(Lcom/xiaomi/abtest/b/a;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/abtest/b/a;",
            "Ljava/util/List<",
            "Lcom/xiaomi/abtest/c/e;",
            ">;)V"
        }
    .end annotation

    .line 99
    const-string p1, "FlowUnit"

    const-string p2, "Base class FlowUnit has no traffic method"

    invoke-static {p1, p2}, Lcom/xiaomi/abtest/d/k;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method public a(Lcom/xiaomi/abtest/c/e;)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/xiaomi/abtest/c/e;->b:Ljava/lang/String;

    .line 157
    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 148
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->k:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 149
    return-void
.end method

.method public a(I)Z
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->j:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public a(Lcom/xiaomi/abtest/b/a;)Z
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->l:Lcom/xiaomi/abtest/c/a;

    if-nez v0, :cond_0

    .line 86
    const/4 p1, 0x1

    return p1

    .line 90
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->c()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/xiaomi/abtest/c/a;->a(Ljava/util/Map;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    .line 91
    :catch_0
    move-exception p1

    .line 92
    const-string v0, "FlowUnit"

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/xiaomi/abtest/d/k;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const/4 p1, 0x0

    return p1
.end method

.method public b(Lcom/xiaomi/abtest/b/a;)I
    .locals 2

    .line 103
    nop

    .line 104
    sget-object v0, Lcom/xiaomi/abtest/c/f;->a:[I

    iget-object v1, p0, Lcom/xiaomi/abtest/c/e;->h:Lcom/xiaomi/abtest/EnumType$DiversionType;

    invoke-virtual {v1}, Lcom/xiaomi/abtest/EnumType$DiversionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const-string v1, "-"

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 132
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    goto/16 :goto_1

    .line 129
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    .line 130
    goto/16 :goto_1

    .line 126
    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    .line 127
    goto :goto_1

    .line 122
    :pswitch_3
    const/16 p1, 0x14

    new-array p1, p1, [B

    .line 123
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/Random;->nextBytes([B)V

    .line 124
    goto :goto_1

    .line 117
    :pswitch_4
    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 118
    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    goto :goto_1

    .line 106
    :pswitch_5
    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/abtest/d/l;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    goto :goto_1

    .line 108
    :cond_0
    const-string v0, "ExpLayer"

    invoke-virtual {p0}, Lcom/xiaomi/abtest/c/e;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "LaunchLayer"

    invoke-virtual {p0}, Lcom/xiaomi/abtest/c/e;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 109
    :cond_1
    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/abtest/d/l;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    invoke-virtual {p1}, Lcom/xiaomi/abtest/b/a;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    goto :goto_1

    .line 112
    :cond_2
    const-string p1, "000"

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    goto :goto_1

    .line 143
    :cond_3
    :goto_0
    const/4 p1, 0x0

    :goto_1
    if-nez p1, :cond_4

    const/4 p1, -0x1

    goto :goto_2

    :cond_4
    array-length v0, p1

    iget v1, p0, Lcom/xiaomi/abtest/c/e;->m:I

    invoke-static {p1, v0, v1}, Lcom/xiaomi/abtest/d/h;->a([BII)I

    move-result p1

    sget-object v0, Lcom/xiaomi/abtest/d/d;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    rem-int/2addr p1, v0

    :goto_2
    return p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)V
    .locals 0

    .line 152
    iput p1, p0, Lcom/xiaomi/abtest/c/e;->a:I

    .line 153
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/xiaomi/abtest/c/e;->f:Ljava/lang/String;

    .line 173
    return-void
.end method

.method public c()Lcom/xiaomi/abtest/EnumType$FlowUnitType;
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->c:Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    return-object v0
.end method

.method public c(I)V
    .locals 0

    .line 164
    iput p1, p0, Lcom/xiaomi/abtest/c/e;->d:I

    .line 165
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/xiaomi/abtest/c/e;->g:Ljava/lang/String;

    .line 182
    return-void
.end method

.method public d()I
    .locals 1

    .line 197
    iget v0, p0, Lcom/xiaomi/abtest/c/e;->d:I

    return v0
.end method

.method public e()Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;
    .locals 1

    .line 201
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->e:Lcom/xiaomi/abtest/EnumType$FlowUnitStatus;

    return-object v0
.end method

.method public f()Lcom/xiaomi/abtest/EnumType$DiversionType;
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->h:Lcom/xiaomi/abtest/EnumType$DiversionType;

    return-object v0
.end method

.method public g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/xiaomi/abtest/c/e;",
            ">;"
        }
    .end annotation

    .line 209
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->i:Ljava/util/List;

    return-object v0
.end method

.method public h()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 213
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->j:Ljava/util/Set;

    return-object v0
.end method

.method public i()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 217
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->k:Ljava/util/Map;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .line 221
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->f:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->g:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .line 58
    nop

    .line 59
    nop

    .line 60
    nop

    .line 61
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-string v1, ""

    move-object v2, v1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const-string v4, ","

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 62
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 63
    goto :goto_0

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/abtest/c/e;->j:Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 65
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 66
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 67
    goto :goto_1

    .line 69
    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/abtest/c/e;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/abtest/c/e;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/xiaomi/abtest/c/e;->c()Lcom/xiaomi/abtest/EnumType$FlowUnitType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/xiaomi/abtest/EnumType$FlowUnitType;->toString()Ljava/lang/String;

    move-result-object v4

    filled-new-array {v0, v3, v4, v2, v1}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "%d,%s,%s,params:[%s] bucketIds:[%s]"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 70
    return-object v0
.end method
