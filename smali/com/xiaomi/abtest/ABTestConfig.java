public class com.xiaomi.abtest.ABTestConfig {
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/abtest/ABTestConfig$Builder; */
	 /* } */
} // .end annotation
/* # instance fields */
private java.lang.String a;
private java.lang.String b;
private java.lang.String c;
private java.lang.String d;
private Integer e;
private Boolean f;
/* # direct methods */
private com.xiaomi.abtest.ABTestConfig ( ) {
	 /* .locals 1 */
	 /* .param p1, "builder" # Lcom/xiaomi/abtest/ABTestConfig$Builder; */
	 /* .line 62 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 63 */
	 com.xiaomi.abtest.ABTestConfig$Builder .a ( p1 );
	 this.a = v0;
	 /* .line 64 */
	 com.xiaomi.abtest.ABTestConfig$Builder .b ( p1 );
	 this.b = v0;
	 /* .line 65 */
	 com.xiaomi.abtest.ABTestConfig$Builder .c ( p1 );
	 this.c = v0;
	 /* .line 66 */
	 com.xiaomi.abtest.ABTestConfig$Builder .d ( p1 );
	 this.d = v0;
	 /* .line 67 */
	 v0 = 	 com.xiaomi.abtest.ABTestConfig$Builder .e ( p1 );
	 /* iput v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->e:I */
	 /* .line 68 */
	 v0 = 	 com.xiaomi.abtest.ABTestConfig$Builder .f ( p1 );
	 /* iput-boolean v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->f:Z */
	 /* .line 69 */
	 return;
} // .end method
 com.xiaomi.abtest.ABTestConfig ( ) { //synthethic
	 /* .locals 0 */
	 /* .param p1, "x0" # Lcom/xiaomi/abtest/ABTestConfig$Builder; */
	 /* .param p2, "x1" # Lcom/xiaomi/abtest/ABTestConfig$1; */
	 /* .line 3 */
	 /* invoke-direct {p0, p1}, Lcom/xiaomi/abtest/ABTestConfig;-><init>(Lcom/xiaomi/abtest/ABTestConfig$Builder;)V */
	 return;
} // .end method
/* # virtual methods */
public java.lang.String getAppName ( ) {
	 /* .locals 1 */
	 /* .line 27 */
	 v0 = this.a;
} // .end method
public java.lang.String getDeviceId ( ) {
	 /* .locals 1 */
	 /* .line 35 */
	 v0 = this.d;
} // .end method
public java.lang.String getLayerName ( ) {
	 /* .locals 1 */
	 /* .line 47 */
	 v0 = this.b;
} // .end method
public Integer getLoadConfigInterval ( ) {
	 /* .locals 1 */
	 /* .line 39 */
	 /* iget v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->e:I */
} // .end method
public java.lang.String getUserId ( ) {
	 /* .locals 1 */
	 /* .line 31 */
	 v0 = this.c;
} // .end method
public Boolean isDisableLoadTimer ( ) {
	 /* .locals 1 */
	 /* .line 43 */
	 /* iget-boolean v0, p0, Lcom/xiaomi/abtest/ABTestConfig;->f:Z */
} // .end method
public java.lang.String toString ( ) {
	 /* .locals 3 */
	 /* .line 52 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "ABTestConfig{mAppName=\'"; // const-string v1, "ABTestConfig{mAppName=\'"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.a;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* const/16 v1, 0x27 */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
	 final String v2 = ", mLayerName=\'"; // const-string v2, ", mLayerName=\'"
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.b;
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
	 final String v2 = ", mUserId=\'"; // const-string v2, ", mUserId=\'"
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.c;
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
	 final String v2 = ", mDeviceId=\'"; // const-string v2, ", mDeviceId=\'"
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.d;
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
	 final String v1 = ", mLoadConfigInterval="; // const-string v1, ", mLoadConfigInterval="
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/xiaomi/abtest/ABTestConfig;->e:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v1 = ", mDisableLoadTimer="; // const-string v1, ", mDisableLoadTimer="
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget-boolean v1, p0, Lcom/xiaomi/abtest/ABTestConfig;->f:Z */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 /* const/16 v1, 0x7d */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
