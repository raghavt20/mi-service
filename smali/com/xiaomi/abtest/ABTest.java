public class com.xiaomi.abtest.ABTest {
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/abtest/ABTest$OnLoadRemoteConfigListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String a;
/* # instance fields */
private com.xiaomi.abtest.ABTestConfig b;
/* # direct methods */
private com.xiaomi.abtest.ABTest ( ) {
	 /* .locals 0 */
	 /* .line 16 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
public static com.xiaomi.abtest.ABTest abTestWithConfig ( android.content.Context p0, com.xiaomi.abtest.ABTestConfig p1 ) {
	 /* .locals 3 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .param p1, "config" # Lcom/xiaomi/abtest/ABTestConfig; */
	 /* .line 19 */
	 com.xiaomi.abtest.d.a .a ( p0 );
	 /* .line 20 */
	 com.xiaomi.abtest.d.k .a ( );
	 /* .line 22 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "abTestWithConfig start,config: "; // const-string v1, "abTestWithConfig start,config: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* if-nez p1, :cond_0 */
	 final String v1 = ""; // const-string v1, ""
} // :cond_0
(( com.xiaomi.abtest.ABTestConfig ) p1 ).toString ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/ABTestConfig;->toString()Ljava/lang/String;
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ABTest"; // const-string v1, "ABTest"
com.xiaomi.abtest.d.k .a ( v1,v0 );
/* .line 23 */
/* new-instance v0, Lcom/xiaomi/abtest/ABTest; */
/* invoke-direct {v0}, Lcom/xiaomi/abtest/ABTest;-><init>()V */
/* .line 24 */
this.b = p1;
/* .line 25 */
com.xiaomi.abtest.a.a .a ( p1 );
/* .line 26 */
com.xiaomi.abtest.a.a .b ( );
(( com.xiaomi.abtest.ABTestConfig ) p1 ).getAppName ( ); // invoke-virtual {p1}, Lcom/xiaomi/abtest/ABTestConfig;->getAppName()Ljava/lang/String;
(( com.xiaomi.abtest.a.a ) v1 ).a ( v2 ); // invoke-virtual {v1, v2}, Lcom/xiaomi/abtest/a/a;->a(Ljava/lang/String;)V
/* .line 28 */
} // .end method
public static void setIsLoadConfigWhenBackground ( Boolean p0 ) {
/* .locals 1 */
/* .param p0, "isLoad" # Z */
/* .line 36 */
com.xiaomi.abtest.a.a .b ( );
(( com.xiaomi.abtest.a.a ) v0 ).a ( p0 ); // invoke-virtual {v0, p0}, Lcom/xiaomi/abtest/a/a;->a(Z)V
/* .line 37 */
return;
} // .end method
/* # virtual methods */
public void clearSingleInstance ( ) {
/* .locals 1 */
/* .line 62 */
com.xiaomi.abtest.a.a .b ( );
(( com.xiaomi.abtest.a.a ) v0 ).a ( ); // invoke-virtual {v0}, Lcom/xiaomi/abtest/a/a;->a()V
/* .line 63 */
return;
} // .end method
public java.util.Map getExperiments ( java.util.Map p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/xiaomi/abtest/ExperimentInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 44 */
/* .local p1, "conditions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
com.xiaomi.abtest.a.a .b ( );
/* .line 45 */
/* new-instance v1, Lcom/xiaomi/abtest/b/a; */
/* invoke-direct {v1}, Lcom/xiaomi/abtest/b/a;-><init>()V */
/* .line 46 */
v2 = this.b;
(( com.xiaomi.abtest.ABTestConfig ) v2 ).getAppName ( ); // invoke-virtual {v2}, Lcom/xiaomi/abtest/ABTestConfig;->getAppName()Ljava/lang/String;
(( com.xiaomi.abtest.b.a ) v1 ).c ( v2 ); // invoke-virtual {v1, v2}, Lcom/xiaomi/abtest/b/a;->c(Ljava/lang/String;)V
/* .line 47 */
v2 = this.b;
(( com.xiaomi.abtest.ABTestConfig ) v2 ).getDeviceId ( ); // invoke-virtual {v2}, Lcom/xiaomi/abtest/ABTestConfig;->getDeviceId()Ljava/lang/String;
(( com.xiaomi.abtest.b.a ) v1 ).b ( v2 ); // invoke-virtual {v1, v2}, Lcom/xiaomi/abtest/b/a;->b(Ljava/lang/String;)V
/* .line 48 */
(( com.xiaomi.abtest.b.a ) v1 ).a ( p1 ); // invoke-virtual {v1, p1}, Lcom/xiaomi/abtest/b/a;->a(Ljava/util/Map;)V
/* .line 49 */
v2 = this.b;
(( com.xiaomi.abtest.ABTestConfig ) v2 ).getUserId ( ); // invoke-virtual {v2}, Lcom/xiaomi/abtest/ABTestConfig;->getUserId()Ljava/lang/String;
(( com.xiaomi.abtest.b.a ) v1 ).a ( v2 ); // invoke-virtual {v1, v2}, Lcom/xiaomi/abtest/b/a;->a(Ljava/lang/String;)V
/* .line 50 */
(( com.xiaomi.abtest.a.a ) v0 ).a ( v1 ); // invoke-virtual {v0, v1}, Lcom/xiaomi/abtest/a/a;->a(Lcom/xiaomi/abtest/b/a;)Ljava/util/Map;
} // .end method
public void loadRemoteConfig ( com.xiaomi.abtest.ABTest$OnLoadRemoteConfigListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lcom/xiaomi/abtest/ABTest$OnLoadRemoteConfigListener; */
/* .line 54 */
com.xiaomi.abtest.a.a .b ( );
(( com.xiaomi.abtest.a.a ) v0 ).a ( p1 ); // invoke-virtual {v0, p1}, Lcom/xiaomi/abtest/a/a;->a(Lcom/xiaomi/abtest/ABTest$OnLoadRemoteConfigListener;)V
/* .line 55 */
return;
} // .end method
