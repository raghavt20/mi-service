class com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface$Stub$Proxy implements com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface {
	 /* .source "IPhashSceneRecognizeInterface.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # static fields */
public static com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface sDefaultImpl;
/* # instance fields */
private android.os.IBinder mRemote;
/* # direct methods */
 com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface$Stub$Proxy ( ) {
/* .locals 0 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 84 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 85 */
this.mRemote = p1;
/* .line 86 */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 90 */
v0 = this.mRemote;
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 94 */
final String v0 = "com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface"; // const-string v0, "com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface"
} // .end method
public java.lang.String recognizeScene ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 99 */
android.os.Parcel .obtain ( );
/* .line 100 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 103 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface"; // const-string v2, "com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 104 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 105 */
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 106 */
/* .local v2, "_status":Z */
/* if-nez v2, :cond_0 */
com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface$Stub .getDefaultImpl ( );
if ( v3 != null) { // if-eqz v3, :cond_0
	 /* .line 107 */
	 com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface$Stub .getDefaultImpl ( );
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 112 */
	 (( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
	 /* .line 113 */
	 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
	 /* .line 107 */
	 /* .line 109 */
} // :cond_0
try { // :try_start_1
	 (( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
	 /* .line 110 */
	 (( android.os.Parcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
	 /* :try_end_1 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* move-object v2, v3 */
	 /* .line 112 */
	 /* .local v2, "_result":Ljava/lang/String; */
	 (( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
	 /* .line 113 */
	 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
	 /* .line 114 */
	 /* nop */
	 /* .line 115 */
	 /* .line 112 */
} // .end local v2 # "_result":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 113 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 114 */
/* throw v2 */
} // .end method
