public abstract class com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface$Stub extends android.os.Binder implements com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface {
	 /* .source "IPhashSceneRecognizeInterface.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface$Stub$Proxy; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String DESCRIPTOR;
static final Integer TRANSACTION_recognizeScene;
/* # direct methods */
public com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface$Stub ( ) {
/* .locals 1 */
/* .line 34 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 35 */
final String v0 = "com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface"; // const-string v0, "com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface"
(( com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface$Stub ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
/* .line 36 */
return;
} // .end method
public static com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p0, "obj" # Landroid/os/IBinder; */
/* .line 43 */
/* if-nez p0, :cond_0 */
/* .line 44 */
int v0 = 0; // const/4 v0, 0x0
/* .line 46 */
} // :cond_0
final String v0 = "com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface"; // const-string v0, "com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface"
/* .line 47 */
/* .local v0, "iin":Landroid/os/IInterface; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* instance-of v1, v0, Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 48 */
/* move-object v1, v0 */
/* check-cast v1, Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface; */
/* .line 50 */
} // :cond_1
/* new-instance v1, Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface$Stub$Proxy; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface$Stub$Proxy;-><init>(Landroid/os/IBinder;)V */
} // .end method
public static com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface getDefaultImpl ( ) {
/* .locals 1 */
/* .line 132 */
v0 = com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface$Stub$Proxy.sDefaultImpl;
} // .end method
public static Boolean setDefaultImpl ( com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface p0 ) {
/* .locals 1 */
/* .param p0, "impl" # Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface; */
/* .line 124 */
v0 = com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface$Stub$Proxy.sDefaultImpl;
/* if-nez v0, :cond_0 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 125 */
/* .line 126 */
int v0 = 1; // const/4 v0, 0x1
/* .line 128 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
/* .line 55 */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 4 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 60 */
final String v0 = "com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface"; // const-string v0, "com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface"
/* .line 61 */
/* .local v0, "descriptor":Ljava/lang/String; */
int v1 = 1; // const/4 v1, 0x1
/* sparse-switch p1, :sswitch_data_0 */
/* .line 76 */
v1 = /* invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 63 */
/* :sswitch_0 */
(( android.os.Parcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 64 */
/* .line 67 */
/* :sswitch_1 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 69 */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 70 */
/* .local v2, "_arg0":Ljava/lang/String; */
(( com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface$Stub ) p0 ).recognizeScene ( v2 ); // invoke-virtual {p0, v2}, Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface$Stub;->recognizeScene(Ljava/lang/String;)Ljava/lang/String;
/* .line 71 */
/* .local v3, "_result":Ljava/lang/String; */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 72 */
(( android.os.Parcel ) p3 ).writeString ( v3 ); // invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 73 */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_1 */
/* 0x5f4e5446 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
