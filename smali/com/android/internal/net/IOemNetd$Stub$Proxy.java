class com.android.internal.net.IOemNetd$Stub$Proxy implements com.android.internal.net.IOemNetd {
	 /* .source "IOemNetd.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/internal/net/IOemNetd$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private android.os.IBinder mRemote;
/* # direct methods */
 com.android.internal.net.IOemNetd$Stub$Proxy ( ) {
/* .locals 0 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 604 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 605 */
this.mRemote = p1;
/* .line 606 */
return;
} // .end method
/* # virtual methods */
public Boolean addMiuiFirewallSharedUid ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 788 */
android.os.Parcel .obtain ( );
/* .line 789 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 792 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 793 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 794 */
v2 = this.mRemote;
/* const/16 v3, 0xa */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 795 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 796 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 799 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 800 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 801 */
/* nop */
/* .line 802 */
/* .line 799 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 800 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 801 */
/* throw v2 */
} // .end method
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 609 */
v0 = this.mRemote;
} // .end method
public Boolean attachXdpProg ( Boolean p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "isNative" # Z */
/* .param p2, "ifaceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1137 */
android.os.Parcel .obtain ( );
/* .line 1138 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1141 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1142 */
(( android.os.Parcel ) v0 ).writeBoolean ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 1143 */
(( android.os.Parcel ) v0 ).writeString ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1144 */
v2 = this.mRemote;
/* const/16 v3, 0x1d */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1145 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1146 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 1149 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1150 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1151 */
/* nop */
/* .line 1152 */
/* .line 1149 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1150 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1151 */
/* throw v2 */
} // .end method
public Integer bindPort ( android.os.ParcelFileDescriptor p0, Integer p1, java.lang.String p2 ) {
/* .locals 5 */
/* .param p1, "sockFd" # Landroid/os/ParcelFileDescriptor; */
/* .param p2, "port" # I */
/* .param p3, "localMac" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1174 */
android.os.Parcel .obtain ( );
/* .line 1175 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1178 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1179 */
int v2 = 0; // const/4 v2, 0x0
(( android.os.Parcel ) v0 ).writeTypedObject ( p1, v2 ); // invoke-virtual {v0, p1, v2}, Landroid/os/Parcel;->writeTypedObject(Landroid/os/Parcelable;I)V
/* .line 1180 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1181 */
(( android.os.Parcel ) v0 ).writeString ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1182 */
v3 = this.mRemote;
v2 = /* const/16 v4, 0x1f */
/* .line 1183 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1184 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 1187 */
/* .local v2, "_result":I */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1188 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1189 */
/* nop */
/* .line 1190 */
/* .line 1187 */
} // .end local v2 # "_result":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1188 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1189 */
/* throw v2 */
} // .end method
public Boolean clearAllMap ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1231 */
android.os.Parcel .obtain ( );
/* .line 1232 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1235 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1236 */
v2 = this.mRemote;
/* const/16 v3, 0x22 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1237 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1238 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 1241 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1242 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1243 */
/* nop */
/* .line 1244 */
/* .line 1241 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1242 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1243 */
/* throw v2 */
} // .end method
public void closeUnreachedPortFilter ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1121 */
android.os.Parcel .obtain ( );
/* .line 1122 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1124 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1125 */
v2 = this.mRemote;
/* const/16 v3, 0x1c */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1126 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1129 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1130 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1131 */
/* nop */
/* .line 1132 */
return;
/* .line 1129 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1130 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1131 */
/* throw v2 */
} // .end method
public Boolean detachXdpProg ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "ifaceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1156 */
android.os.Parcel .obtain ( );
/* .line 1157 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1160 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1161 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1162 */
v2 = this.mRemote;
/* const/16 v3, 0x1e */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1163 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1164 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 1167 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1168 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1169 */
/* nop */
/* .line 1170 */
/* .line 1167 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1168 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1169 */
/* throw v2 */
} // .end method
public Integer enableAutoForward ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 5 */
/* .param p1, "addr" # Ljava/lang/String; */
/* .param p2, "fwmark" # I */
/* .param p3, "enabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1052 */
android.os.Parcel .obtain ( );
/* .line 1053 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1056 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1057 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1058 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1059 */
(( android.os.Parcel ) v0 ).writeBoolean ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 1060 */
v2 = this.mRemote;
/* const/16 v3, 0x18 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1061 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1062 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 1065 */
/* .local v2, "_result":I */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1066 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1067 */
/* nop */
/* .line 1068 */
/* .line 1065 */
} // .end local v2 # "_result":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1066 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1067 */
/* throw v2 */
} // .end method
public Boolean enableIptablesRestore ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "enabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 730 */
android.os.Parcel .obtain ( );
/* .line 731 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 734 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 735 */
(( android.os.Parcel ) v0 ).writeBoolean ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 736 */
v2 = this.mRemote;
int v3 = 7; // const/4 v3, 0x7
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 737 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 738 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 741 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 742 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 743 */
/* nop */
/* .line 744 */
/* .line 741 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 742 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 743 */
/* throw v2 */
} // .end method
public Boolean enableLimitter ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "enabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 655 */
android.os.Parcel .obtain ( );
/* .line 656 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 659 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 660 */
(( android.os.Parcel ) v0 ).writeBoolean ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 661 */
v2 = this.mRemote;
int v3 = 3; // const/4 v3, 0x3
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 662 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 663 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 666 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 667 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 668 */
/* nop */
/* .line 669 */
/* .line 666 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 667 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 668 */
/* throw v2 */
} // .end method
public Boolean enableMobileTrafficLimit ( Boolean p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "enabled" # Z */
/* .param p2, "iface" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 939 */
android.os.Parcel .obtain ( );
/* .line 940 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 943 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 944 */
(( android.os.Parcel ) v0 ).writeBoolean ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 945 */
(( android.os.Parcel ) v0 ).writeString ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 946 */
v2 = this.mRemote;
/* const/16 v3, 0x12 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 947 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 948 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 951 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 952 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 953 */
/* nop */
/* .line 954 */
/* .line 951 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 952 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 953 */
/* throw v2 */
} // .end method
public Boolean enableQos ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "enabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 883 */
android.os.Parcel .obtain ( );
/* .line 884 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 887 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 888 */
(( android.os.Parcel ) v0 ).writeBoolean ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 889 */
v2 = this.mRemote;
/* const/16 v3, 0xf */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 890 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 891 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 894 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 895 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 896 */
/* nop */
/* .line 897 */
/* .line 894 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 895 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 896 */
/* throw v2 */
} // .end method
public Boolean enableRps ( java.lang.String p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "iface" # Ljava/lang/String; */
/* .param p2, "enable" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 845 */
android.os.Parcel .obtain ( );
/* .line 846 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 849 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 850 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 851 */
(( android.os.Parcel ) v0 ).writeBoolean ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 852 */
v2 = this.mRemote;
/* const/16 v3, 0xd */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 853 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 854 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 857 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 858 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 859 */
/* nop */
/* .line 860 */
/* .line 857 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 858 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 859 */
/* throw v2 */
} // .end method
public Boolean enableWmmer ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "enabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 637 */
android.os.Parcel .obtain ( );
/* .line 638 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 641 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 642 */
(( android.os.Parcel ) v0 ).writeBoolean ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 643 */
v2 = this.mRemote;
int v3 = 2; // const/4 v3, 0x2
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 644 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 645 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 648 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 649 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 650 */
/* nop */
/* .line 651 */
/* .line 648 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 649 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 650 */
/* throw v2 */
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 613 */
v0 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
} // .end method
public Long getMiuiSlmVoipUdpAddress ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1014 */
android.os.Parcel .obtain ( );
/* .line 1015 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1018 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1019 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1020 */
v2 = this.mRemote;
/* const/16 v3, 0x16 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1021 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1022 */
(( android.os.Parcel ) v1 ).readLong ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-wide v2, v3 */
/* .line 1025 */
/* .local v2, "_result":J */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1026 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1027 */
/* nop */
/* .line 1028 */
/* return-wide v2 */
/* .line 1025 */
} // .end local v2 # "_result":J
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1026 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1027 */
/* throw v2 */
} // .end method
public Integer getMiuiSlmVoipUdpPort ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1032 */
android.os.Parcel .obtain ( );
/* .line 1033 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1036 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1037 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1038 */
v2 = this.mRemote;
/* const/16 v3, 0x17 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1039 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1040 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 1043 */
/* .local v2, "_result":I */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1044 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1045 */
/* nop */
/* .line 1046 */
/* .line 1043 */
} // .end local v2 # "_result":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1044 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1045 */
/* throw v2 */
} // .end method
public Long getShareStats ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "type" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1266 */
android.os.Parcel .obtain ( );
/* .line 1267 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1270 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1271 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1272 */
v2 = this.mRemote;
/* const/16 v3, 0x24 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1273 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1274 */
(( android.os.Parcel ) v1 ).readLong ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-wide v2, v3 */
/* .line 1277 */
/* .local v2, "_result":J */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1278 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1279 */
/* nop */
/* .line 1280 */
/* return-wide v2 */
/* .line 1277 */
} // .end local v2 # "_result":J
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1278 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1279 */
/* throw v2 */
} // .end method
public Boolean isAlive ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 619 */
android.os.Parcel .obtain ( );
/* .line 620 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 623 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 624 */
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 625 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 626 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 629 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 630 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 631 */
/* nop */
/* .line 632 */
/* .line 629 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 630 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 631 */
/* throw v2 */
} // .end method
public Boolean listenUidDataActivity ( Integer p0, Integer p1, Integer p2, Integer p3, Boolean p4 ) {
/* .locals 5 */
/* .param p1, "protocol" # I */
/* .param p2, "uid" # I */
/* .param p3, "label" # I */
/* .param p4, "timeout" # I */
/* .param p5, "listen" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 748 */
android.os.Parcel .obtain ( );
/* .line 749 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 752 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 753 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 754 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 755 */
(( android.os.Parcel ) v0 ).writeInt ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 756 */
(( android.os.Parcel ) v0 ).writeInt ( p4 ); // invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 757 */
(( android.os.Parcel ) v0 ).writeBoolean ( p5 ); // invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 758 */
v2 = this.mRemote;
/* const/16 v3, 0x8 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 759 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 760 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 763 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 764 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 765 */
/* nop */
/* .line 766 */
/* .line 763 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 764 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 765 */
/* throw v2 */
} // .end method
public Integer lookupPort ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "type" # I */
/* .param p2, "value" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1212 */
android.os.Parcel .obtain ( );
/* .line 1213 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1216 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1217 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1218 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1219 */
v2 = this.mRemote;
/* const/16 v3, 0x21 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1220 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1221 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 1224 */
/* .local v2, "_result":I */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1225 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1226 */
/* nop */
/* .line 1227 */
/* .line 1224 */
} // .end local v2 # "_result":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1225 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1226 */
/* throw v2 */
} // .end method
public void notifyFirewallBlocked ( Integer p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "code" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 864 */
android.os.Parcel .obtain ( );
/* .line 865 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 867 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 868 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 869 */
(( android.os.Parcel ) v0 ).writeString ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 870 */
v2 = this.mRemote;
/* const/16 v3, 0xe */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 871 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 874 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 875 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 876 */
/* nop */
/* .line 877 */
return;
/* .line 874 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 875 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 876 */
/* throw v2 */
} // .end method
public void notifyUnreachedPort ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 5 */
/* .param p1, "port" # I */
/* .param p2, "ip" # I */
/* .param p3, "interfaceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1075 */
android.os.Parcel .obtain ( );
/* .line 1076 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1078 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1079 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1080 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1081 */
(( android.os.Parcel ) v0 ).writeString ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1082 */
v2 = this.mRemote;
/* const/16 v3, 0x19 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1083 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1086 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1087 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1088 */
/* nop */
/* .line 1089 */
return;
/* .line 1086 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1087 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1088 */
/* throw v2 */
} // .end method
public void openUnreachedPortFilter ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1107 */
android.os.Parcel .obtain ( );
/* .line 1108 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1110 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1111 */
v2 = this.mRemote;
/* const/16 v3, 0x1b */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1112 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1115 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1116 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1117 */
/* nop */
/* .line 1118 */
return;
/* .line 1115 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1116 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1117 */
/* throw v2 */
} // .end method
public void registerOemUnsolicitedEventListener ( com.android.internal.net.IOemNetdUnsolicitedEventListener p0 ) {
/* .locals 5 */
/* .param p1, "listener" # Lcom/android/internal/net/IOemNetdUnsolicitedEventListener; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1290 */
android.os.Parcel .obtain ( );
/* .line 1291 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1293 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1294 */
(( android.os.Parcel ) v0 ).writeStrongInterface ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V
/* .line 1295 */
v2 = this.mRemote;
/* const/16 v3, 0x25 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1296 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1299 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1300 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1301 */
/* nop */
/* .line 1302 */
return;
/* .line 1299 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1300 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1301 */
/* throw v2 */
} // .end method
public Boolean setCurrentNetworkState ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "state" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 827 */
android.os.Parcel .obtain ( );
/* .line 828 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 831 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 832 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 833 */
v2 = this.mRemote;
/* const/16 v3, 0xc */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 834 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 835 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 838 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 839 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 840 */
/* nop */
/* .line 841 */
/* .line 838 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 839 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 840 */
/* throw v2 */
} // .end method
public void setEthernetVlan ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1250 */
android.os.Parcel .obtain ( );
/* .line 1251 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1253 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1254 */
v2 = this.mRemote;
/* const/16 v3, 0x23 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1255 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1258 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1259 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1260 */
/* nop */
/* .line 1261 */
return;
/* .line 1258 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1259 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1260 */
/* throw v2 */
} // .end method
public Boolean setLimit ( Boolean p0, Long p1 ) {
/* .locals 5 */
/* .param p1, "enabled" # Z */
/* .param p2, "rate" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 711 */
android.os.Parcel .obtain ( );
/* .line 712 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 715 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 716 */
(( android.os.Parcel ) v0 ).writeBoolean ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 717 */
(( android.os.Parcel ) v0 ).writeLong ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V
/* .line 718 */
v2 = this.mRemote;
int v3 = 6; // const/4 v3, 0x6
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 719 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 720 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 723 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 724 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 725 */
/* nop */
/* .line 726 */
/* .line 723 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 724 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 725 */
/* throw v2 */
} // .end method
public Boolean setMiuiFirewallRule ( java.lang.String p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "rule" # I */
/* .param p4, "type" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 806 */
android.os.Parcel .obtain ( );
/* .line 807 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 810 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 811 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 812 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 813 */
(( android.os.Parcel ) v0 ).writeInt ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 814 */
(( android.os.Parcel ) v0 ).writeInt ( p4 ); // invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 815 */
v2 = this.mRemote;
/* const/16 v3, 0xb */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 816 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 817 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 820 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 821 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 822 */
/* nop */
/* .line 823 */
/* .line 820 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 821 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 822 */
/* throw v2 */
} // .end method
public Boolean setMiuiSlmBpfUid ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 996 */
android.os.Parcel .obtain ( );
/* .line 997 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1000 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1001 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1002 */
v2 = this.mRemote;
/* const/16 v3, 0x15 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1003 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1004 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 1007 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1008 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1009 */
/* nop */
/* .line 1010 */
/* .line 1007 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1008 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1009 */
/* throw v2 */
} // .end method
public Boolean setMobileTrafficLimit ( Boolean p0, Long p1 ) {
/* .locals 5 */
/* .param p1, "enabled" # Z */
/* .param p2, "rate" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 958 */
android.os.Parcel .obtain ( );
/* .line 959 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 962 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 963 */
(( android.os.Parcel ) v0 ).writeBoolean ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 964 */
(( android.os.Parcel ) v0 ).writeLong ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V
/* .line 965 */
v2 = this.mRemote;
/* const/16 v3, 0x13 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 966 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 967 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 970 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 971 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 972 */
/* nop */
/* .line 973 */
/* .line 970 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 971 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 972 */
/* throw v2 */
} // .end method
public void setPidForPackage ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "pid" # I */
/* .param p3, "uid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 922 */
android.os.Parcel .obtain ( );
/* .line 923 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 925 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 926 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 927 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 928 */
(( android.os.Parcel ) v0 ).writeInt ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 929 */
v2 = this.mRemote;
/* const/16 v3, 0x11 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 930 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 933 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 934 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 935 */
/* nop */
/* .line 936 */
return;
/* .line 933 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 934 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 935 */
/* throw v2 */
} // .end method
public Boolean setQos ( Integer p0, Integer p1, Integer p2, Boolean p3 ) {
/* .locals 5 */
/* .param p1, "protocol" # I */
/* .param p2, "uid" # I */
/* .param p3, "tos" # I */
/* .param p4, "add" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 901 */
android.os.Parcel .obtain ( );
/* .line 902 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 905 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 906 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 907 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 908 */
(( android.os.Parcel ) v0 ).writeInt ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 909 */
(( android.os.Parcel ) v0 ).writeBoolean ( p4 ); // invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 910 */
v2 = this.mRemote;
/* const/16 v3, 0x10 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 911 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 912 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 915 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 916 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 917 */
/* nop */
/* .line 918 */
/* .line 915 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 916 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 917 */
/* throw v2 */
} // .end method
public Boolean unbindPort ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "port" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1194 */
android.os.Parcel .obtain ( );
/* .line 1195 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1198 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1199 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1200 */
v2 = this.mRemote;
/* const/16 v3, 0x20 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1201 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1202 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 1205 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1206 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1207 */
/* nop */
/* .line 1208 */
/* .line 1205 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1206 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1207 */
/* throw v2 */
} // .end method
public void unregisterOemUnsolicitedEventListener ( com.android.internal.net.IOemNetdUnsolicitedEventListener p0 ) {
/* .locals 5 */
/* .param p1, "listener" # Lcom/android/internal/net/IOemNetdUnsolicitedEventListener; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1092 */
android.os.Parcel .obtain ( );
/* .line 1093 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1095 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1096 */
(( android.os.Parcel ) v0 ).writeStrongInterface ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V
/* .line 1097 */
v2 = this.mRemote;
/* const/16 v3, 0x1a */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1098 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1101 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1102 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1103 */
/* nop */
/* .line 1104 */
return;
/* .line 1101 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1102 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1103 */
/* throw v2 */
} // .end method
public Boolean updateIface ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "iface" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 770 */
android.os.Parcel .obtain ( );
/* .line 771 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 774 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 775 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 776 */
v2 = this.mRemote;
/* const/16 v3, 0x9 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 777 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 778 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 781 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 782 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 783 */
/* nop */
/* .line 784 */
/* .line 781 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 782 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 783 */
/* throw v2 */
} // .end method
public Boolean updateWmm ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "wmm" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 673 */
android.os.Parcel .obtain ( );
/* .line 674 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 677 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 678 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 679 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 680 */
v2 = this.mRemote;
int v3 = 4; // const/4 v3, 0x4
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 681 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 682 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 685 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 686 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 687 */
/* nop */
/* .line 688 */
/* .line 685 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 686 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 687 */
/* throw v2 */
} // .end method
public Boolean whiteListUid ( Integer p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "add" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 692 */
android.os.Parcel .obtain ( );
/* .line 693 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 696 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 697 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 698 */
(( android.os.Parcel ) v0 ).writeBoolean ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 699 */
v2 = this.mRemote;
int v3 = 5; // const/4 v3, 0x5
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 700 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 701 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 704 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 705 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 706 */
/* nop */
/* .line 707 */
/* .line 704 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 705 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 706 */
/* throw v2 */
} // .end method
public Boolean whiteListUidForMobileTraffic ( Integer p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "add" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 977 */
android.os.Parcel .obtain ( );
/* .line 978 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 981 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.android.internal.net.IOemNetd$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 982 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 983 */
(( android.os.Parcel ) v0 ).writeBoolean ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 984 */
v2 = this.mRemote;
/* const/16 v3, 0x14 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 985 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 986 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 989 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 990 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 991 */
/* nop */
/* .line 992 */
/* .line 989 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 990 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 991 */
/* throw v2 */
} // .end method
