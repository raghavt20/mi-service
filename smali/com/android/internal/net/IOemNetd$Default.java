public class com.android.internal.net.IOemNetd$Default implements com.android.internal.net.IOemNetd {
	 /* .source "IOemNetd.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/internal/net/IOemNetd; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "Default" */
} // .end annotation
/* # direct methods */
public com.android.internal.net.IOemNetd$Default ( ) {
/* .locals 0 */
/* .line 9 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Boolean addMiuiFirewallSharedUid ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 52 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 176 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean attachXdpProg ( Boolean p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "isNative" # Z */
/* .param p2, "ifaceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 132 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer bindPort ( android.os.ParcelFileDescriptor p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "sockFd" # Landroid/os/ParcelFileDescriptor; */
/* .param p2, "port" # I */
/* .param p3, "localMac" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 140 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean clearAllMap ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 152 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void closeUnreachedPortFilter ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 127 */
return;
} // .end method
public Boolean detachXdpProg ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "ifaceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 136 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer enableAutoForward ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "addr" # Ljava/lang/String; */
/* .param p2, "fwmark" # I */
/* .param p3, "enabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 111 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean enableIptablesRestore ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 40 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean enableLimitter ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 24 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean enableMobileTrafficLimit ( Boolean p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "enabled" # Z */
/* .param p2, "iface" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 85 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean enableQos ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 74 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean enableRps ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "iface" # Ljava/lang/String; */
/* .param p2, "enable" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 64 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean enableWmmer ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 20 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Long getMiuiSlmVoipUdpAddress ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 101 */
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
} // .end method
public Integer getMiuiSlmVoipUdpPort ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 105 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Long getShareStats ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 163 */
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
} // .end method
public Boolean isAlive ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 15 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean listenUidDataActivity ( Integer p0, Integer p1, Integer p2, Integer p3, Boolean p4 ) {
/* .locals 1 */
/* .param p1, "protocol" # I */
/* .param p2, "uid" # I */
/* .param p3, "label" # I */
/* .param p4, "timeout" # I */
/* .param p5, "listen" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 44 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer lookupPort ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "value" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 148 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void notifyFirewallBlocked ( Integer p0, java.lang.String p1 ) {
/* .locals 0 */
/* .param p1, "code" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 68 */
return;
} // .end method
public void notifyUnreachedPort ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 0 */
/* .param p1, "port" # I */
/* .param p2, "ip" # I */
/* .param p3, "interfaceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 118 */
return;
} // .end method
public void openUnreachedPortFilter ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 124 */
return;
} // .end method
public void registerOemUnsolicitedEventListener ( com.android.internal.net.IOemNetdUnsolicitedEventListener p0 ) {
/* .locals 0 */
/* .param p1, "listener" # Lcom/android/internal/net/IOemNetdUnsolicitedEventListener; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 173 */
return;
} // .end method
public Boolean setCurrentNetworkState ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "state" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 60 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void setEthernetVlan ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 158 */
return;
} // .end method
public Boolean setLimit ( Boolean p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "enabled" # Z */
/* .param p2, "rate" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 36 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setMiuiFirewallRule ( java.lang.String p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "rule" # I */
/* .param p4, "type" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 56 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setMiuiSlmBpfUid ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 97 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setMobileTrafficLimit ( Boolean p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "enabled" # Z */
/* .param p2, "rate" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 89 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void setPidForPackage ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 0 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "pid" # I */
/* .param p3, "uid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 82 */
return;
} // .end method
public Boolean setQos ( Integer p0, Integer p1, Integer p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "protocol" # I */
/* .param p2, "uid" # I */
/* .param p3, "tos" # I */
/* .param p4, "add" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 78 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean unbindPort ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "port" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 144 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void unregisterOemUnsolicitedEventListener ( com.android.internal.net.IOemNetdUnsolicitedEventListener p0 ) {
/* .locals 0 */
/* .param p1, "listener" # Lcom/android/internal/net/IOemNetdUnsolicitedEventListener; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 121 */
return;
} // .end method
public Boolean updateIface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "iface" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 48 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean updateWmm ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "wmm" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 28 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean whiteListUid ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "add" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 32 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean whiteListUidForMobileTraffic ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "add" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 93 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
