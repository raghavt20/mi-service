.class public abstract Lcom/android/internal/net/IOemNetd$Stub;
.super Landroid/os/Binder;
.source "IOemNetd.java"

# interfaces
.implements Lcom/android/internal/net/IOemNetd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/net/IOemNetd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/net/IOemNetd$Stub$Proxy;
    }
.end annotation


# static fields
.field static final TRANSACTION_addMiuiFirewallSharedUid:I = 0xa

.field static final TRANSACTION_attachXdpProg:I = 0x1d

.field static final TRANSACTION_bindPort:I = 0x1f

.field static final TRANSACTION_clearAllMap:I = 0x22

.field static final TRANSACTION_closeUnreachedPortFilter:I = 0x1c

.field static final TRANSACTION_detachXdpProg:I = 0x1e

.field static final TRANSACTION_enableAutoForward:I = 0x18

.field static final TRANSACTION_enableIptablesRestore:I = 0x7

.field static final TRANSACTION_enableLimitter:I = 0x3

.field static final TRANSACTION_enableMobileTrafficLimit:I = 0x12

.field static final TRANSACTION_enableQos:I = 0xf

.field static final TRANSACTION_enableRps:I = 0xd

.field static final TRANSACTION_enableWmmer:I = 0x2

.field static final TRANSACTION_getMiuiSlmVoipUdpAddress:I = 0x16

.field static final TRANSACTION_getMiuiSlmVoipUdpPort:I = 0x17

.field static final TRANSACTION_getShareStats:I = 0x24

.field static final TRANSACTION_isAlive:I = 0x1

.field static final TRANSACTION_listenUidDataActivity:I = 0x8

.field static final TRANSACTION_lookupPort:I = 0x21

.field static final TRANSACTION_notifyFirewallBlocked:I = 0xe

.field static final TRANSACTION_notifyUnreachedPort:I = 0x19

.field static final TRANSACTION_openUnreachedPortFilter:I = 0x1b

.field static final TRANSACTION_registerOemUnsolicitedEventListener:I = 0x25

.field static final TRANSACTION_setCurrentNetworkState:I = 0xc

.field static final TRANSACTION_setEthernetVlan:I = 0x23

.field static final TRANSACTION_setLimit:I = 0x6

.field static final TRANSACTION_setMiuiFirewallRule:I = 0xb

.field static final TRANSACTION_setMiuiSlmBpfUid:I = 0x15

.field static final TRANSACTION_setMobileTrafficLimit:I = 0x13

.field static final TRANSACTION_setPidForPackage:I = 0x11

.field static final TRANSACTION_setQos:I = 0x10

.field static final TRANSACTION_unbindPort:I = 0x20

.field static final TRANSACTION_unregisterOemUnsolicitedEventListener:I = 0x1a

.field static final TRANSACTION_updateIface:I = 0x9

.field static final TRANSACTION_updateWmm:I = 0x4

.field static final TRANSACTION_whiteListUid:I = 0x5

.field static final TRANSACTION_whiteListUidForMobileTraffic:I = 0x14


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 184
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 185
    sget-object v0, Lcom/android/internal/net/IOemNetd$Stub;->DESCRIPTOR:Ljava/lang/String;

    invoke-virtual {p0, p0, v0}, Lcom/android/internal/net/IOemNetd$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/net/IOemNetd;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .line 193
    if-nez p0, :cond_0

    .line 194
    const/4 v0, 0x0

    return-object v0

    .line 196
    :cond_0
    sget-object v0, Lcom/android/internal/net/IOemNetd$Stub;->DESCRIPTOR:Ljava/lang/String;

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 197
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/android/internal/net/IOemNetd;

    if-eqz v1, :cond_1

    .line 198
    move-object v1, v0

    check-cast v1, Lcom/android/internal/net/IOemNetd;

    return-object v1

    .line 200
    :cond_1
    new-instance v1, Lcom/android/internal/net/IOemNetd$Stub$Proxy;

    invoke-direct {v1, p0}, Lcom/android/internal/net/IOemNetd$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    return-object v1
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .line 204
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 17
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 208
    move-object/from16 v6, p0

    move/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    sget-object v10, Lcom/android/internal/net/IOemNetd$Stub;->DESCRIPTOR:Ljava/lang/String;

    .line 209
    .local v10, "descriptor":Ljava/lang/String;
    const/4 v11, 0x1

    if-lt v7, v11, :cond_0

    const v0, 0xffffff

    if-gt v7, v0, :cond_0

    .line 210
    invoke-virtual {v8, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 212
    :cond_0
    packed-switch v7, :pswitch_data_0

    .line 220
    packed-switch v7, :pswitch_data_1

    .line 595
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0

    .line 216
    :pswitch_0
    invoke-virtual {v9, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 217
    return v11

    .line 588
    :pswitch_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/net/IOemNetdUnsolicitedEventListener;

    move-result-object v0

    .line 589
    .local v0, "_arg0":Lcom/android/internal/net/IOemNetdUnsolicitedEventListener;
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->registerOemUnsolicitedEventListener(Lcom/android/internal/net/IOemNetdUnsolicitedEventListener;)V

    .line 590
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 591
    goto/16 :goto_0

    .line 579
    .end local v0    # "_arg0":Lcom/android/internal/net/IOemNetdUnsolicitedEventListener;
    :pswitch_2
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 580
    .local v0, "_arg0":I
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->getShareStats(I)J

    move-result-wide v1

    .line 581
    .local v1, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 582
    invoke-virtual {v9, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 583
    goto/16 :goto_0

    .line 572
    .end local v0    # "_arg0":I
    .end local v1    # "_result":J
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/net/IOemNetd$Stub;->setEthernetVlan()V

    .line 573
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 574
    goto/16 :goto_0

    .line 565
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/net/IOemNetd$Stub;->clearAllMap()Z

    move-result v0

    .line 566
    .local v0, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 567
    invoke-virtual {v9, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 568
    goto/16 :goto_0

    .line 555
    .end local v0    # "_result":Z
    :pswitch_5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 557
    .local v0, "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 558
    .local v1, "_arg1":I
    invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->lookupPort(II)I

    move-result v2

    .line 559
    .local v2, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 560
    invoke-virtual {v9, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 561
    goto/16 :goto_0

    .line 546
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":I
    .end local v2    # "_result":I
    :pswitch_6
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 547
    .restart local v0    # "_arg0":I
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->unbindPort(I)Z

    move-result v1

    .line 548
    .local v1, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 549
    invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 550
    goto/16 :goto_0

    .line 533
    .end local v0    # "_arg0":I
    .end local v1    # "_result":Z
    :pswitch_7
    sget-object v0, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {v8, v0}, Landroid/os/Parcel;->readTypedObject(Landroid/os/Parcelable$Creator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    .line 535
    .local v0, "_arg0":Landroid/os/ParcelFileDescriptor;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 537
    .local v1, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 538
    .local v2, "_arg2":Ljava/lang/String;
    invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/net/IOemNetd$Stub;->bindPort(Landroid/os/ParcelFileDescriptor;ILjava/lang/String;)I

    move-result v3

    .line 539
    .local v3, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 540
    invoke-virtual {v9, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 541
    goto/16 :goto_0

    .line 524
    .end local v0    # "_arg0":Landroid/os/ParcelFileDescriptor;
    .end local v1    # "_arg1":I
    .end local v2    # "_arg2":Ljava/lang/String;
    .end local v3    # "_result":I
    :pswitch_8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 525
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->detachXdpProg(Ljava/lang/String;)Z

    move-result v1

    .line 526
    .local v1, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 527
    invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 528
    goto/16 :goto_0

    .line 513
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_result":Z
    :pswitch_9
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    .line 515
    .local v0, "_arg0":Z
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 516
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->attachXdpProg(ZLjava/lang/String;)Z

    move-result v2

    .line 517
    .local v2, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 518
    invoke-virtual {v9, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 519
    goto/16 :goto_0

    .line 506
    .end local v0    # "_arg0":Z
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v2    # "_result":Z
    :pswitch_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/net/IOemNetd$Stub;->closeUnreachedPortFilter()V

    .line 507
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 508
    goto/16 :goto_0

    .line 500
    :pswitch_b
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/net/IOemNetd$Stub;->openUnreachedPortFilter()V

    .line 501
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 502
    goto/16 :goto_0

    .line 493
    :pswitch_c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/net/IOemNetdUnsolicitedEventListener;

    move-result-object v0

    .line 494
    .local v0, "_arg0":Lcom/android/internal/net/IOemNetdUnsolicitedEventListener;
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->unregisterOemUnsolicitedEventListener(Lcom/android/internal/net/IOemNetdUnsolicitedEventListener;)V

    .line 495
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 496
    goto/16 :goto_0

    .line 481
    .end local v0    # "_arg0":Lcom/android/internal/net/IOemNetdUnsolicitedEventListener;
    :pswitch_d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 483
    .local v0, "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 485
    .local v1, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 486
    .local v2, "_arg2":Ljava/lang/String;
    invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/net/IOemNetd$Stub;->notifyUnreachedPort(IILjava/lang/String;)V

    .line 487
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 488
    goto/16 :goto_0

    .line 468
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":I
    .end local v2    # "_arg2":Ljava/lang/String;
    :pswitch_e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 470
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 472
    .restart local v1    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v2

    .line 473
    .local v2, "_arg2":Z
    invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/net/IOemNetd$Stub;->enableAutoForward(Ljava/lang/String;IZ)I

    move-result v3

    .line 474
    .restart local v3    # "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 475
    invoke-virtual {v9, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 476
    goto/16 :goto_0

    .line 459
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v2    # "_arg2":Z
    .end local v3    # "_result":I
    :pswitch_f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 460
    .local v0, "_arg0":I
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->getMiuiSlmVoipUdpPort(I)I

    move-result v1

    .line 461
    .local v1, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 462
    invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 463
    goto/16 :goto_0

    .line 450
    .end local v0    # "_arg0":I
    .end local v1    # "_result":I
    :pswitch_10
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 451
    .restart local v0    # "_arg0":I
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->getMiuiSlmVoipUdpAddress(I)J

    move-result-wide v1

    .line 452
    .local v1, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 453
    invoke-virtual {v9, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 454
    goto/16 :goto_0

    .line 441
    .end local v0    # "_arg0":I
    .end local v1    # "_result":J
    :pswitch_11
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 442
    .restart local v0    # "_arg0":I
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->setMiuiSlmBpfUid(I)Z

    move-result v1

    .line 443
    .local v1, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 444
    invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 445
    goto/16 :goto_0

    .line 430
    .end local v0    # "_arg0":I
    .end local v1    # "_result":Z
    :pswitch_12
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 432
    .restart local v0    # "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v1

    .line 433
    .local v1, "_arg1":Z
    invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->whiteListUidForMobileTraffic(IZ)Z

    move-result v2

    .line 434
    .local v2, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 435
    invoke-virtual {v9, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 436
    goto/16 :goto_0

    .line 419
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Z
    .end local v2    # "_result":Z
    :pswitch_13
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    .line 421
    .local v0, "_arg0":Z
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    .line 422
    .local v1, "_arg1":J
    invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/net/IOemNetd$Stub;->setMobileTrafficLimit(ZJ)Z

    move-result v3

    .line 423
    .local v3, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 424
    invoke-virtual {v9, v3}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 425
    goto/16 :goto_0

    .line 408
    .end local v0    # "_arg0":Z
    .end local v1    # "_arg1":J
    .end local v3    # "_result":Z
    :pswitch_14
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    .line 410
    .restart local v0    # "_arg0":Z
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 411
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->enableMobileTrafficLimit(ZLjava/lang/String;)Z

    move-result v2

    .line 412
    .restart local v2    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 413
    invoke-virtual {v9, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 414
    goto/16 :goto_0

    .line 396
    .end local v0    # "_arg0":Z
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v2    # "_result":Z
    :pswitch_15
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 398
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 400
    .local v1, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 401
    .local v2, "_arg2":I
    invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/net/IOemNetd$Stub;->setPidForPackage(Ljava/lang/String;II)V

    .line 402
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 403
    goto/16 :goto_0

    .line 381
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v2    # "_arg2":I
    :pswitch_16
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 383
    .local v0, "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 385
    .restart local v1    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 387
    .restart local v2    # "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v3

    .line 388
    .local v3, "_arg3":Z
    invoke-virtual {v6, v0, v1, v2, v3}, Lcom/android/internal/net/IOemNetd$Stub;->setQos(IIIZ)Z

    move-result v4

    .line 389
    .local v4, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 390
    invoke-virtual {v9, v4}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 391
    goto/16 :goto_0

    .line 372
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":I
    .end local v2    # "_arg2":I
    .end local v3    # "_arg3":Z
    .end local v4    # "_result":Z
    :pswitch_17
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    .line 373
    .local v0, "_arg0":Z
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->enableQos(Z)Z

    move-result v1

    .line 374
    .local v1, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 375
    invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 376
    goto/16 :goto_0

    .line 362
    .end local v0    # "_arg0":Z
    .end local v1    # "_result":Z
    :pswitch_18
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 364
    .local v0, "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 365
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->notifyFirewallBlocked(ILjava/lang/String;)V

    .line 366
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 367
    goto/16 :goto_0

    .line 351
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Ljava/lang/String;
    :pswitch_19
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 353
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v1

    .line 354
    .local v1, "_arg1":Z
    invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->enableRps(Ljava/lang/String;Z)Z

    move-result v2

    .line 355
    .local v2, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 356
    invoke-virtual {v9, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 357
    goto/16 :goto_0

    .line 342
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Z
    .end local v2    # "_result":Z
    :pswitch_1a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 343
    .local v0, "_arg0":I
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->setCurrentNetworkState(I)Z

    move-result v1

    .line 344
    .local v1, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 345
    invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 346
    goto/16 :goto_0

    .line 327
    .end local v0    # "_arg0":I
    .end local v1    # "_result":Z
    :pswitch_1b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 329
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 331
    .local v1, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 333
    .local v2, "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 334
    .local v3, "_arg3":I
    invoke-virtual {v6, v0, v1, v2, v3}, Lcom/android/internal/net/IOemNetd$Stub;->setMiuiFirewallRule(Ljava/lang/String;III)Z

    move-result v4

    .line 335
    .restart local v4    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 336
    invoke-virtual {v9, v4}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 337
    goto/16 :goto_0

    .line 318
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v2    # "_arg2":I
    .end local v3    # "_arg3":I
    .end local v4    # "_result":Z
    :pswitch_1c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 319
    .local v0, "_arg0":I
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->addMiuiFirewallSharedUid(I)Z

    move-result v1

    .line 320
    .local v1, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 321
    invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 322
    goto/16 :goto_0

    .line 309
    .end local v0    # "_arg0":I
    .end local v1    # "_result":Z
    :pswitch_1d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 310
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->updateIface(Ljava/lang/String;)Z

    move-result v1

    .line 311
    .restart local v1    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 312
    invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 313
    goto/16 :goto_0

    .line 292
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_result":Z
    :pswitch_1e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v12

    .line 294
    .local v12, "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v13

    .line 296
    .local v13, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v14

    .line 298
    .local v14, "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v15

    .line 300
    .local v15, "_arg3":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v16

    .line 301
    .local v16, "_arg4":Z
    move-object/from16 v0, p0

    move v1, v12

    move v2, v13

    move v3, v14

    move v4, v15

    move/from16 v5, v16

    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/net/IOemNetd$Stub;->listenUidDataActivity(IIIIZ)Z

    move-result v0

    .line 302
    .local v0, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 303
    invoke-virtual {v9, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 304
    goto/16 :goto_0

    .line 283
    .end local v0    # "_result":Z
    .end local v12    # "_arg0":I
    .end local v13    # "_arg1":I
    .end local v14    # "_arg2":I
    .end local v15    # "_arg3":I
    .end local v16    # "_arg4":Z
    :pswitch_1f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    .line 284
    .local v0, "_arg0":Z
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->enableIptablesRestore(Z)Z

    move-result v1

    .line 285
    .restart local v1    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 286
    invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 287
    goto :goto_0

    .line 272
    .end local v0    # "_arg0":Z
    .end local v1    # "_result":Z
    :pswitch_20
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    .line 274
    .restart local v0    # "_arg0":Z
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    .line 275
    .local v1, "_arg1":J
    invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/net/IOemNetd$Stub;->setLimit(ZJ)Z

    move-result v3

    .line 276
    .local v3, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 277
    invoke-virtual {v9, v3}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 278
    goto :goto_0

    .line 261
    .end local v0    # "_arg0":Z
    .end local v1    # "_arg1":J
    .end local v3    # "_result":Z
    :pswitch_21
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 263
    .local v0, "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v1

    .line 264
    .local v1, "_arg1":Z
    invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->whiteListUid(IZ)Z

    move-result v2

    .line 265
    .local v2, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 266
    invoke-virtual {v9, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 267
    goto :goto_0

    .line 250
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Z
    .end local v2    # "_result":Z
    :pswitch_22
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 252
    .restart local v0    # "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 253
    .local v1, "_arg1":I
    invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->updateWmm(II)Z

    move-result v2

    .line 254
    .restart local v2    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 255
    invoke-virtual {v9, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 256
    goto :goto_0

    .line 241
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":I
    .end local v2    # "_result":Z
    :pswitch_23
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    .line 242
    .local v0, "_arg0":Z
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->enableLimitter(Z)Z

    move-result v1

    .line 243
    .local v1, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 244
    invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 245
    goto :goto_0

    .line 232
    .end local v0    # "_arg0":Z
    .end local v1    # "_result":Z
    :pswitch_24
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    .line 233
    .restart local v0    # "_arg0":Z
    invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->enableWmmer(Z)Z

    move-result v1

    .line 234
    .restart local v1    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 235
    invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 236
    goto :goto_0

    .line 224
    .end local v0    # "_arg0":Z
    .end local v1    # "_result":Z
    :pswitch_25
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/net/IOemNetd$Stub;->isAlive()Z

    move-result v0

    .line 225
    .local v0, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 226
    invoke-virtual {v9, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 227
    nop

    .line 598
    .end local v0    # "_result":Z
    :goto_0
    return v11

    :pswitch_data_0
    .packed-switch 0x5f4e5446
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
