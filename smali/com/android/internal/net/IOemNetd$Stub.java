public abstract class com.android.internal.net.IOemNetd$Stub extends android.os.Binder implements com.android.internal.net.IOemNetd {
	 /* .source "IOemNetd.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/internal/net/IOemNetd; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/internal/net/IOemNetd$Stub$Proxy; */
/* } */
} // .end annotation
/* # static fields */
static final Integer TRANSACTION_addMiuiFirewallSharedUid;
static final Integer TRANSACTION_attachXdpProg;
static final Integer TRANSACTION_bindPort;
static final Integer TRANSACTION_clearAllMap;
static final Integer TRANSACTION_closeUnreachedPortFilter;
static final Integer TRANSACTION_detachXdpProg;
static final Integer TRANSACTION_enableAutoForward;
static final Integer TRANSACTION_enableIptablesRestore;
static final Integer TRANSACTION_enableLimitter;
static final Integer TRANSACTION_enableMobileTrafficLimit;
static final Integer TRANSACTION_enableQos;
static final Integer TRANSACTION_enableRps;
static final Integer TRANSACTION_enableWmmer;
static final Integer TRANSACTION_getMiuiSlmVoipUdpAddress;
static final Integer TRANSACTION_getMiuiSlmVoipUdpPort;
static final Integer TRANSACTION_getShareStats;
static final Integer TRANSACTION_isAlive;
static final Integer TRANSACTION_listenUidDataActivity;
static final Integer TRANSACTION_lookupPort;
static final Integer TRANSACTION_notifyFirewallBlocked;
static final Integer TRANSACTION_notifyUnreachedPort;
static final Integer TRANSACTION_openUnreachedPortFilter;
static final Integer TRANSACTION_registerOemUnsolicitedEventListener;
static final Integer TRANSACTION_setCurrentNetworkState;
static final Integer TRANSACTION_setEthernetVlan;
static final Integer TRANSACTION_setLimit;
static final Integer TRANSACTION_setMiuiFirewallRule;
static final Integer TRANSACTION_setMiuiSlmBpfUid;
static final Integer TRANSACTION_setMobileTrafficLimit;
static final Integer TRANSACTION_setPidForPackage;
static final Integer TRANSACTION_setQos;
static final Integer TRANSACTION_unbindPort;
static final Integer TRANSACTION_unregisterOemUnsolicitedEventListener;
static final Integer TRANSACTION_updateIface;
static final Integer TRANSACTION_updateWmm;
static final Integer TRANSACTION_whiteListUid;
static final Integer TRANSACTION_whiteListUidForMobileTraffic;
/* # direct methods */
public com.android.internal.net.IOemNetd$Stub ( ) {
/* .locals 1 */
/* .line 184 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 185 */
v0 = com.android.internal.net.IOemNetd$Stub.DESCRIPTOR;
(( com.android.internal.net.IOemNetd$Stub ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Lcom/android/internal/net/IOemNetd$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
/* .line 186 */
return;
} // .end method
public static com.android.internal.net.IOemNetd asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p0, "obj" # Landroid/os/IBinder; */
/* .line 193 */
/* if-nez p0, :cond_0 */
/* .line 194 */
int v0 = 0; // const/4 v0, 0x0
/* .line 196 */
} // :cond_0
v0 = com.android.internal.net.IOemNetd$Stub.DESCRIPTOR;
/* .line 197 */
/* .local v0, "iin":Landroid/os/IInterface; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* instance-of v1, v0, Lcom/android/internal/net/IOemNetd; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 198 */
/* move-object v1, v0 */
/* check-cast v1, Lcom/android/internal/net/IOemNetd; */
/* .line 200 */
} // :cond_1
/* new-instance v1, Lcom/android/internal/net/IOemNetd$Stub$Proxy; */
/* invoke-direct {v1, p0}, Lcom/android/internal/net/IOemNetd$Stub$Proxy;-><init>(Landroid/os/IBinder;)V */
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
/* .line 204 */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 17 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 208 */
/* move-object/from16 v6, p0 */
/* move/from16 v7, p1 */
/* move-object/from16 v8, p2 */
/* move-object/from16 v9, p3 */
v10 = com.android.internal.net.IOemNetd$Stub.DESCRIPTOR;
/* .line 209 */
/* .local v10, "descriptor":Ljava/lang/String; */
int v11 = 1; // const/4 v11, 0x1
/* if-lt v7, v11, :cond_0 */
/* const v0, 0xffffff */
/* if-gt v7, v0, :cond_0 */
/* .line 210 */
(( android.os.Parcel ) v8 ).enforceInterface ( v10 ); // invoke-virtual {v8, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 212 */
} // :cond_0
/* packed-switch v7, :pswitch_data_0 */
/* .line 220 */
/* packed-switch v7, :pswitch_data_1 */
/* .line 595 */
v0 = /* invoke-super/range {p0 ..p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 216 */
/* :pswitch_0 */
(( android.os.Parcel ) v9 ).writeString ( v10 ); // invoke-virtual {v9, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 217 */
/* .line 588 */
/* :pswitch_1 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder; */
com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub .asInterface ( v0 );
/* .line 589 */
/* .local v0, "_arg0":Lcom/android/internal/net/IOemNetdUnsolicitedEventListener; */
(( com.android.internal.net.IOemNetd$Stub ) v6 ).registerOemUnsolicitedEventListener ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->registerOemUnsolicitedEventListener(Lcom/android/internal/net/IOemNetdUnsolicitedEventListener;)V
/* .line 590 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 591 */
/* goto/16 :goto_0 */
/* .line 579 */
} // .end local v0 # "_arg0":Lcom/android/internal/net/IOemNetdUnsolicitedEventListener;
/* :pswitch_2 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 580 */
/* .local v0, "_arg0":I */
(( com.android.internal.net.IOemNetd$Stub ) v6 ).getShareStats ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->getShareStats(I)J
/* move-result-wide v1 */
/* .line 581 */
/* .local v1, "_result":J */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 582 */
(( android.os.Parcel ) v9 ).writeLong ( v1, v2 ); // invoke-virtual {v9, v1, v2}, Landroid/os/Parcel;->writeLong(J)V
/* .line 583 */
/* goto/16 :goto_0 */
/* .line 572 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_result":J
/* :pswitch_3 */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/internal/net/IOemNetd$Stub;->setEthernetVlan()V */
/* .line 573 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 574 */
/* goto/16 :goto_0 */
/* .line 565 */
/* :pswitch_4 */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/internal/net/IOemNetd$Stub;->clearAllMap()Z */
/* .line 566 */
/* .local v0, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 567 */
(( android.os.Parcel ) v9 ).writeBoolean ( v0 ); // invoke-virtual {v9, v0}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 568 */
/* goto/16 :goto_0 */
/* .line 555 */
} // .end local v0 # "_result":Z
/* :pswitch_5 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 557 */
/* .local v0, "_arg0":I */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 558 */
/* .local v1, "_arg1":I */
v2 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).lookupPort ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->lookupPort(II)I
/* .line 559 */
/* .local v2, "_result":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 560 */
(( android.os.Parcel ) v9 ).writeInt ( v2 ); // invoke-virtual {v9, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 561 */
/* goto/16 :goto_0 */
/* .line 546 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_arg1":I
} // .end local v2 # "_result":I
/* :pswitch_6 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 547 */
/* .restart local v0 # "_arg0":I */
v1 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).unbindPort ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->unbindPort(I)Z
/* .line 548 */
/* .local v1, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 549 */
(( android.os.Parcel ) v9 ).writeBoolean ( v1 ); // invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 550 */
/* goto/16 :goto_0 */
/* .line 533 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_result":Z
/* :pswitch_7 */
v0 = android.os.ParcelFileDescriptor.CREATOR;
(( android.os.Parcel ) v8 ).readTypedObject ( v0 ); // invoke-virtual {v8, v0}, Landroid/os/Parcel;->readTypedObject(Landroid/os/Parcelable$Creator;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/ParcelFileDescriptor; */
/* .line 535 */
/* .local v0, "_arg0":Landroid/os/ParcelFileDescriptor; */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 537 */
/* .local v1, "_arg1":I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 538 */
/* .local v2, "_arg2":Ljava/lang/String; */
v3 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).bindPort ( v0, v1, v2 ); // invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/net/IOemNetd$Stub;->bindPort(Landroid/os/ParcelFileDescriptor;ILjava/lang/String;)I
/* .line 539 */
/* .local v3, "_result":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 540 */
(( android.os.Parcel ) v9 ).writeInt ( v3 ); // invoke-virtual {v9, v3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 541 */
/* goto/16 :goto_0 */
/* .line 524 */
} // .end local v0 # "_arg0":Landroid/os/ParcelFileDescriptor;
} // .end local v1 # "_arg1":I
} // .end local v2 # "_arg2":Ljava/lang/String;
} // .end local v3 # "_result":I
/* :pswitch_8 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 525 */
/* .local v0, "_arg0":Ljava/lang/String; */
v1 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).detachXdpProg ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->detachXdpProg(Ljava/lang/String;)Z
/* .line 526 */
/* .local v1, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 527 */
(( android.os.Parcel ) v9 ).writeBoolean ( v1 ); // invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 528 */
/* goto/16 :goto_0 */
/* .line 513 */
} // .end local v0 # "_arg0":Ljava/lang/String;
} // .end local v1 # "_result":Z
/* :pswitch_9 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 515 */
/* .local v0, "_arg0":Z */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 516 */
/* .local v1, "_arg1":Ljava/lang/String; */
v2 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).attachXdpProg ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->attachXdpProg(ZLjava/lang/String;)Z
/* .line 517 */
/* .local v2, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 518 */
(( android.os.Parcel ) v9 ).writeBoolean ( v2 ); // invoke-virtual {v9, v2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 519 */
/* goto/16 :goto_0 */
/* .line 506 */
} // .end local v0 # "_arg0":Z
} // .end local v1 # "_arg1":Ljava/lang/String;
} // .end local v2 # "_result":Z
/* :pswitch_a */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/internal/net/IOemNetd$Stub;->closeUnreachedPortFilter()V */
/* .line 507 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 508 */
/* goto/16 :goto_0 */
/* .line 500 */
/* :pswitch_b */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/internal/net/IOemNetd$Stub;->openUnreachedPortFilter()V */
/* .line 501 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 502 */
/* goto/16 :goto_0 */
/* .line 493 */
/* :pswitch_c */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder; */
com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub .asInterface ( v0 );
/* .line 494 */
/* .local v0, "_arg0":Lcom/android/internal/net/IOemNetdUnsolicitedEventListener; */
(( com.android.internal.net.IOemNetd$Stub ) v6 ).unregisterOemUnsolicitedEventListener ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->unregisterOemUnsolicitedEventListener(Lcom/android/internal/net/IOemNetdUnsolicitedEventListener;)V
/* .line 495 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 496 */
/* goto/16 :goto_0 */
/* .line 481 */
} // .end local v0 # "_arg0":Lcom/android/internal/net/IOemNetdUnsolicitedEventListener;
/* :pswitch_d */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 483 */
/* .local v0, "_arg0":I */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 485 */
/* .local v1, "_arg1":I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 486 */
/* .local v2, "_arg2":Ljava/lang/String; */
(( com.android.internal.net.IOemNetd$Stub ) v6 ).notifyUnreachedPort ( v0, v1, v2 ); // invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/net/IOemNetd$Stub;->notifyUnreachedPort(IILjava/lang/String;)V
/* .line 487 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 488 */
/* goto/16 :goto_0 */
/* .line 468 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_arg1":I
} // .end local v2 # "_arg2":Ljava/lang/String;
/* :pswitch_e */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 470 */
/* .local v0, "_arg0":Ljava/lang/String; */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 472 */
/* .restart local v1 # "_arg1":I */
v2 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 473 */
/* .local v2, "_arg2":Z */
v3 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).enableAutoForward ( v0, v1, v2 ); // invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/net/IOemNetd$Stub;->enableAutoForward(Ljava/lang/String;IZ)I
/* .line 474 */
/* .restart local v3 # "_result":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 475 */
(( android.os.Parcel ) v9 ).writeInt ( v3 ); // invoke-virtual {v9, v3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 476 */
/* goto/16 :goto_0 */
/* .line 459 */
} // .end local v0 # "_arg0":Ljava/lang/String;
} // .end local v1 # "_arg1":I
} // .end local v2 # "_arg2":Z
} // .end local v3 # "_result":I
/* :pswitch_f */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 460 */
/* .local v0, "_arg0":I */
v1 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).getMiuiSlmVoipUdpPort ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->getMiuiSlmVoipUdpPort(I)I
/* .line 461 */
/* .local v1, "_result":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 462 */
(( android.os.Parcel ) v9 ).writeInt ( v1 ); // invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 463 */
/* goto/16 :goto_0 */
/* .line 450 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_result":I
/* :pswitch_10 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 451 */
/* .restart local v0 # "_arg0":I */
(( com.android.internal.net.IOemNetd$Stub ) v6 ).getMiuiSlmVoipUdpAddress ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->getMiuiSlmVoipUdpAddress(I)J
/* move-result-wide v1 */
/* .line 452 */
/* .local v1, "_result":J */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 453 */
(( android.os.Parcel ) v9 ).writeLong ( v1, v2 ); // invoke-virtual {v9, v1, v2}, Landroid/os/Parcel;->writeLong(J)V
/* .line 454 */
/* goto/16 :goto_0 */
/* .line 441 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_result":J
/* :pswitch_11 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 442 */
/* .restart local v0 # "_arg0":I */
v1 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).setMiuiSlmBpfUid ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->setMiuiSlmBpfUid(I)Z
/* .line 443 */
/* .local v1, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 444 */
(( android.os.Parcel ) v9 ).writeBoolean ( v1 ); // invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 445 */
/* goto/16 :goto_0 */
/* .line 430 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_result":Z
/* :pswitch_12 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 432 */
/* .restart local v0 # "_arg0":I */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 433 */
/* .local v1, "_arg1":Z */
v2 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).whiteListUidForMobileTraffic ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->whiteListUidForMobileTraffic(IZ)Z
/* .line 434 */
/* .local v2, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 435 */
(( android.os.Parcel ) v9 ).writeBoolean ( v2 ); // invoke-virtual {v9, v2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 436 */
/* goto/16 :goto_0 */
/* .line 419 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_arg1":Z
} // .end local v2 # "_result":Z
/* :pswitch_13 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 421 */
/* .local v0, "_arg0":Z */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readLong()J */
/* move-result-wide v1 */
/* .line 422 */
/* .local v1, "_arg1":J */
v3 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).setMobileTrafficLimit ( v0, v1, v2 ); // invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/net/IOemNetd$Stub;->setMobileTrafficLimit(ZJ)Z
/* .line 423 */
/* .local v3, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 424 */
(( android.os.Parcel ) v9 ).writeBoolean ( v3 ); // invoke-virtual {v9, v3}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 425 */
/* goto/16 :goto_0 */
/* .line 408 */
} // .end local v0 # "_arg0":Z
} // .end local v1 # "_arg1":J
} // .end local v3 # "_result":Z
/* :pswitch_14 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 410 */
/* .restart local v0 # "_arg0":Z */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 411 */
/* .local v1, "_arg1":Ljava/lang/String; */
v2 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).enableMobileTrafficLimit ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->enableMobileTrafficLimit(ZLjava/lang/String;)Z
/* .line 412 */
/* .restart local v2 # "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 413 */
(( android.os.Parcel ) v9 ).writeBoolean ( v2 ); // invoke-virtual {v9, v2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 414 */
/* goto/16 :goto_0 */
/* .line 396 */
} // .end local v0 # "_arg0":Z
} // .end local v1 # "_arg1":Ljava/lang/String;
} // .end local v2 # "_result":Z
/* :pswitch_15 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 398 */
/* .local v0, "_arg0":Ljava/lang/String; */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 400 */
/* .local v1, "_arg1":I */
v2 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 401 */
/* .local v2, "_arg2":I */
(( com.android.internal.net.IOemNetd$Stub ) v6 ).setPidForPackage ( v0, v1, v2 ); // invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/net/IOemNetd$Stub;->setPidForPackage(Ljava/lang/String;II)V
/* .line 402 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 403 */
/* goto/16 :goto_0 */
/* .line 381 */
} // .end local v0 # "_arg0":Ljava/lang/String;
} // .end local v1 # "_arg1":I
} // .end local v2 # "_arg2":I
/* :pswitch_16 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 383 */
/* .local v0, "_arg0":I */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 385 */
/* .restart local v1 # "_arg1":I */
v2 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 387 */
/* .restart local v2 # "_arg2":I */
v3 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 388 */
/* .local v3, "_arg3":Z */
v4 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).setQos ( v0, v1, v2, v3 ); // invoke-virtual {v6, v0, v1, v2, v3}, Lcom/android/internal/net/IOemNetd$Stub;->setQos(IIIZ)Z
/* .line 389 */
/* .local v4, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 390 */
(( android.os.Parcel ) v9 ).writeBoolean ( v4 ); // invoke-virtual {v9, v4}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 391 */
/* goto/16 :goto_0 */
/* .line 372 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_arg1":I
} // .end local v2 # "_arg2":I
} // .end local v3 # "_arg3":Z
} // .end local v4 # "_result":Z
/* :pswitch_17 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 373 */
/* .local v0, "_arg0":Z */
v1 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).enableQos ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->enableQos(Z)Z
/* .line 374 */
/* .local v1, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 375 */
(( android.os.Parcel ) v9 ).writeBoolean ( v1 ); // invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 376 */
/* goto/16 :goto_0 */
/* .line 362 */
} // .end local v0 # "_arg0":Z
} // .end local v1 # "_result":Z
/* :pswitch_18 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 364 */
/* .local v0, "_arg0":I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 365 */
/* .local v1, "_arg1":Ljava/lang/String; */
(( com.android.internal.net.IOemNetd$Stub ) v6 ).notifyFirewallBlocked ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->notifyFirewallBlocked(ILjava/lang/String;)V
/* .line 366 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 367 */
/* goto/16 :goto_0 */
/* .line 351 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_arg1":Ljava/lang/String;
/* :pswitch_19 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 353 */
/* .local v0, "_arg0":Ljava/lang/String; */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 354 */
/* .local v1, "_arg1":Z */
v2 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).enableRps ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->enableRps(Ljava/lang/String;Z)Z
/* .line 355 */
/* .local v2, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 356 */
(( android.os.Parcel ) v9 ).writeBoolean ( v2 ); // invoke-virtual {v9, v2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 357 */
/* goto/16 :goto_0 */
/* .line 342 */
} // .end local v0 # "_arg0":Ljava/lang/String;
} // .end local v1 # "_arg1":Z
} // .end local v2 # "_result":Z
/* :pswitch_1a */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 343 */
/* .local v0, "_arg0":I */
v1 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).setCurrentNetworkState ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->setCurrentNetworkState(I)Z
/* .line 344 */
/* .local v1, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 345 */
(( android.os.Parcel ) v9 ).writeBoolean ( v1 ); // invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 346 */
/* goto/16 :goto_0 */
/* .line 327 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_result":Z
/* :pswitch_1b */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 329 */
/* .local v0, "_arg0":Ljava/lang/String; */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 331 */
/* .local v1, "_arg1":I */
v2 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 333 */
/* .local v2, "_arg2":I */
v3 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 334 */
/* .local v3, "_arg3":I */
v4 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).setMiuiFirewallRule ( v0, v1, v2, v3 ); // invoke-virtual {v6, v0, v1, v2, v3}, Lcom/android/internal/net/IOemNetd$Stub;->setMiuiFirewallRule(Ljava/lang/String;III)Z
/* .line 335 */
/* .restart local v4 # "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 336 */
(( android.os.Parcel ) v9 ).writeBoolean ( v4 ); // invoke-virtual {v9, v4}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 337 */
/* goto/16 :goto_0 */
/* .line 318 */
} // .end local v0 # "_arg0":Ljava/lang/String;
} // .end local v1 # "_arg1":I
} // .end local v2 # "_arg2":I
} // .end local v3 # "_arg3":I
} // .end local v4 # "_result":Z
/* :pswitch_1c */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 319 */
/* .local v0, "_arg0":I */
v1 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).addMiuiFirewallSharedUid ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->addMiuiFirewallSharedUid(I)Z
/* .line 320 */
/* .local v1, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 321 */
(( android.os.Parcel ) v9 ).writeBoolean ( v1 ); // invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 322 */
/* goto/16 :goto_0 */
/* .line 309 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_result":Z
/* :pswitch_1d */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 310 */
/* .local v0, "_arg0":Ljava/lang/String; */
v1 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).updateIface ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->updateIface(Ljava/lang/String;)Z
/* .line 311 */
/* .restart local v1 # "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 312 */
(( android.os.Parcel ) v9 ).writeBoolean ( v1 ); // invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 313 */
/* goto/16 :goto_0 */
/* .line 292 */
} // .end local v0 # "_arg0":Ljava/lang/String;
} // .end local v1 # "_result":Z
/* :pswitch_1e */
v12 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 294 */
/* .local v12, "_arg0":I */
v13 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 296 */
/* .local v13, "_arg1":I */
v14 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 298 */
/* .local v14, "_arg2":I */
v15 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 300 */
/* .local v15, "_arg3":I */
v16 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 301 */
/* .local v16, "_arg4":Z */
/* move-object/from16 v0, p0 */
/* move v1, v12 */
/* move v2, v13 */
/* move v3, v14 */
/* move v4, v15 */
/* move/from16 v5, v16 */
v0 = /* invoke-virtual/range {v0 ..v5}, Lcom/android/internal/net/IOemNetd$Stub;->listenUidDataActivity(IIIIZ)Z */
/* .line 302 */
/* .local v0, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 303 */
(( android.os.Parcel ) v9 ).writeBoolean ( v0 ); // invoke-virtual {v9, v0}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 304 */
/* goto/16 :goto_0 */
/* .line 283 */
} // .end local v0 # "_result":Z
} // .end local v12 # "_arg0":I
} // .end local v13 # "_arg1":I
} // .end local v14 # "_arg2":I
} // .end local v15 # "_arg3":I
} // .end local v16 # "_arg4":Z
/* :pswitch_1f */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 284 */
/* .local v0, "_arg0":Z */
v1 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).enableIptablesRestore ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->enableIptablesRestore(Z)Z
/* .line 285 */
/* .restart local v1 # "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 286 */
(( android.os.Parcel ) v9 ).writeBoolean ( v1 ); // invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 287 */
/* .line 272 */
} // .end local v0 # "_arg0":Z
} // .end local v1 # "_result":Z
/* :pswitch_20 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 274 */
/* .restart local v0 # "_arg0":Z */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readLong()J */
/* move-result-wide v1 */
/* .line 275 */
/* .local v1, "_arg1":J */
v3 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).setLimit ( v0, v1, v2 ); // invoke-virtual {v6, v0, v1, v2}, Lcom/android/internal/net/IOemNetd$Stub;->setLimit(ZJ)Z
/* .line 276 */
/* .local v3, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 277 */
(( android.os.Parcel ) v9 ).writeBoolean ( v3 ); // invoke-virtual {v9, v3}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 278 */
/* .line 261 */
} // .end local v0 # "_arg0":Z
} // .end local v1 # "_arg1":J
} // .end local v3 # "_result":Z
/* :pswitch_21 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 263 */
/* .local v0, "_arg0":I */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 264 */
/* .local v1, "_arg1":Z */
v2 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).whiteListUid ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->whiteListUid(IZ)Z
/* .line 265 */
/* .local v2, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 266 */
(( android.os.Parcel ) v9 ).writeBoolean ( v2 ); // invoke-virtual {v9, v2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 267 */
/* .line 250 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_arg1":Z
} // .end local v2 # "_result":Z
/* :pswitch_22 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 252 */
/* .restart local v0 # "_arg0":I */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 253 */
/* .local v1, "_arg1":I */
v2 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).updateWmm ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Lcom/android/internal/net/IOemNetd$Stub;->updateWmm(II)Z
/* .line 254 */
/* .restart local v2 # "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 255 */
(( android.os.Parcel ) v9 ).writeBoolean ( v2 ); // invoke-virtual {v9, v2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 256 */
/* .line 241 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_arg1":I
} // .end local v2 # "_result":Z
/* :pswitch_23 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 242 */
/* .local v0, "_arg0":Z */
v1 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).enableLimitter ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->enableLimitter(Z)Z
/* .line 243 */
/* .local v1, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 244 */
(( android.os.Parcel ) v9 ).writeBoolean ( v1 ); // invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 245 */
/* .line 232 */
} // .end local v0 # "_arg0":Z
} // .end local v1 # "_result":Z
/* :pswitch_24 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 233 */
/* .restart local v0 # "_arg0":Z */
v1 = (( com.android.internal.net.IOemNetd$Stub ) v6 ).enableWmmer ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/internal/net/IOemNetd$Stub;->enableWmmer(Z)Z
/* .line 234 */
/* .restart local v1 # "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 235 */
(( android.os.Parcel ) v9 ).writeBoolean ( v1 ); // invoke-virtual {v9, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 236 */
/* .line 224 */
} // .end local v0 # "_arg0":Z
} // .end local v1 # "_result":Z
/* :pswitch_25 */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/internal/net/IOemNetd$Stub;->isAlive()Z */
/* .line 225 */
/* .local v0, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 226 */
(( android.os.Parcel ) v9 ).writeBoolean ( v0 ); // invoke-virtual {v9, v0}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 227 */
/* nop */
/* .line 598 */
} // .end local v0 # "_result":Z
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x5f4e5446 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x1 */
/* :pswitch_25 */
/* :pswitch_24 */
/* :pswitch_23 */
/* :pswitch_22 */
/* :pswitch_21 */
/* :pswitch_20 */
/* :pswitch_1f */
/* :pswitch_1e */
/* :pswitch_1d */
/* :pswitch_1c */
/* :pswitch_1b */
/* :pswitch_1a */
/* :pswitch_19 */
/* :pswitch_18 */
/* :pswitch_17 */
/* :pswitch_16 */
/* :pswitch_15 */
/* :pswitch_14 */
/* :pswitch_13 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
