public abstract class com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub extends android.os.Binder implements com.android.internal.net.IOemNetdUnsolicitedEventListener {
	 /* .source "IOemNetdUnsolicitedEventListener.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/internal/net/IOemNetdUnsolicitedEventListener; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Stub$Proxy; */
/* } */
} // .end annotation
/* # static fields */
static final Integer TRANSACTION_onFirewallBlocked;
static final Integer TRANSACTION_onRegistered;
static final Integer TRANSACTION_onUnreachedPort;
/* # direct methods */
public com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub ( ) {
/* .locals 1 */
/* .line 38 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 39 */
v0 = com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub.DESCRIPTOR;
(( com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
/* .line 40 */
return;
} // .end method
public static com.android.internal.net.IOemNetdUnsolicitedEventListener asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p0, "obj" # Landroid/os/IBinder; */
/* .line 47 */
/* if-nez p0, :cond_0 */
/* .line 48 */
int v0 = 0; // const/4 v0, 0x0
/* .line 50 */
} // :cond_0
v0 = com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub.DESCRIPTOR;
/* .line 51 */
/* .local v0, "iin":Landroid/os/IInterface; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* instance-of v1, v0, Lcom/android/internal/net/IOemNetdUnsolicitedEventListener; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 52 */
/* move-object v1, v0 */
/* check-cast v1, Lcom/android/internal/net/IOemNetdUnsolicitedEventListener; */
/* .line 54 */
} // :cond_1
/* new-instance v1, Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Stub$Proxy; */
/* invoke-direct {v1, p0}, Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V */
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
/* .line 58 */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 62 */
v0 = com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub.DESCRIPTOR;
/* .line 63 */
/* .local v0, "descriptor":Ljava/lang/String; */
int v1 = 1; // const/4 v1, 0x1
/* if-lt p1, v1, :cond_0 */
/* const v2, 0xffffff */
/* if-gt p1, v2, :cond_0 */
/* .line 64 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 66 */
} // :cond_0
/* packed-switch p1, :pswitch_data_0 */
/* .line 74 */
/* packed-switch p1, :pswitch_data_1 */
/* .line 103 */
v1 = /* invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 70 */
/* :pswitch_0 */
(( android.os.Parcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 71 */
/* .line 93 */
/* :pswitch_1 */
v2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 95 */
/* .local v2, "_arg0":I */
v3 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 97 */
/* .local v3, "_arg1":I */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 98 */
/* .local v4, "_arg2":Ljava/lang/String; */
(( com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub ) p0 ).onUnreachedPort ( v2, v3, v4 ); // invoke-virtual {p0, v2, v3, v4}, Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Stub;->onUnreachedPort(IILjava/lang/String;)V
/* .line 99 */
/* .line 84 */
} // .end local v2 # "_arg0":I
} // .end local v3 # "_arg1":I
} // .end local v4 # "_arg2":Ljava/lang/String;
/* :pswitch_2 */
v2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 86 */
/* .restart local v2 # "_arg0":I */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 87 */
/* .local v3, "_arg1":Ljava/lang/String; */
(( com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub ) p0 ).onFirewallBlocked ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Stub;->onFirewallBlocked(ILjava/lang/String;)V
/* .line 88 */
/* .line 78 */
} // .end local v2 # "_arg0":I
} // .end local v3 # "_arg1":Ljava/lang/String;
/* :pswitch_3 */
(( com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub ) p0 ).onRegistered ( ); // invoke-virtual {p0}, Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Stub;->onRegistered()V
/* .line 79 */
/* nop */
/* .line 106 */
} // :goto_0
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x5f4e5446 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
