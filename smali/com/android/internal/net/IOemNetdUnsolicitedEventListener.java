public abstract class com.android.internal.net.IOemNetdUnsolicitedEventListener implements android.os.IInterface {
	 /* .source "IOemNetdUnsolicitedEventListener.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Stub;, */
	 /* Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Default; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String DESCRIPTOR;
/* # direct methods */
static com.android.internal.net.IOemNetdUnsolicitedEventListener ( ) {
	 /* .locals 3 */
	 /* .line 174 */
	 /* const/16 v0, 0x24 */
	 /* const/16 v1, 0x2e */
	 final String v2 = "com$android$internal$net$IOemNetdUnsolicitedEventListener"; // const-string v2, "com$android$internal$net$IOemNetdUnsolicitedEventListener"
	 (( java.lang.String ) v2 ).replace ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;
	 return;
} // .end method
/* # virtual methods */
public abstract void onFirewallBlocked ( Integer p0, java.lang.String p1 ) {
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
} // .end method
public abstract void onRegistered ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onUnreachedPort ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
