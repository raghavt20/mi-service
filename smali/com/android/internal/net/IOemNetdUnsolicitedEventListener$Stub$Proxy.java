class com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub$Proxy implements com.android.internal.net.IOemNetdUnsolicitedEventListener {
	 /* .source "IOemNetdUnsolicitedEventListener.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private android.os.IBinder mRemote;
/* # direct methods */
 com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub$Proxy ( ) {
/* .locals 0 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 112 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 113 */
this.mRemote = p1;
/* .line 114 */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 117 */
v0 = this.mRemote;
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 121 */
v0 = com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub$Proxy.DESCRIPTOR;
} // .end method
public void onFirewallBlocked ( Integer p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "code" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 142 */
android.os.Parcel .obtain ( );
/* .line 144 */
/* .local v0, "_data":Landroid/os/Parcel; */
try { // :try_start_0
v1 = com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 145 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 146 */
(( android.os.Parcel ) v0 ).writeString ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 147 */
v1 = this.mRemote;
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
int v4 = 2; // const/4 v4, 0x2
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 150 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 151 */
/* nop */
/* .line 152 */
return;
/* .line 150 */
/* :catchall_0 */
/* move-exception v1 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 151 */
/* throw v1 */
} // .end method
public void onRegistered ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 130 */
android.os.Parcel .obtain ( );
/* .line 132 */
/* .local v0, "_data":Landroid/os/Parcel; */
try { // :try_start_0
v1 = com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 133 */
v1 = this.mRemote;
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 136 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 137 */
/* nop */
/* .line 138 */
return;
/* .line 136 */
/* :catchall_0 */
/* move-exception v1 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 137 */
/* throw v1 */
} // .end method
public void onUnreachedPort ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 5 */
/* .param p1, "port" # I */
/* .param p2, "ip" # I */
/* .param p3, "interfaceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 157 */
android.os.Parcel .obtain ( );
/* .line 159 */
/* .local v0, "_data":Landroid/os/Parcel; */
try { // :try_start_0
v1 = com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 160 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 161 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 162 */
(( android.os.Parcel ) v0 ).writeString ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 163 */
v1 = this.mRemote;
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
int v4 = 3; // const/4 v4, 0x3
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 166 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 167 */
/* nop */
/* .line 168 */
return;
/* .line 166 */
/* :catchall_0 */
/* move-exception v1 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 167 */
/* throw v1 */
} // .end method
