.class Lcom/android/server/DarkModeOneTrackHelper$1;
.super Ljava/lang/Object;
.source "DarkModeOneTrackHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/DarkModeOneTrackHelper;->uploadToOneTrack(Landroid/content/Context;Lcom/android/server/DarkModeEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$newEvent:Lcom/android/server/DarkModeEvent;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/android/server/DarkModeEvent;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 93
    iput-object p1, p0, Lcom/android/server/DarkModeOneTrackHelper$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/server/DarkModeOneTrackHelper$1;->val$newEvent:Lcom/android/server/DarkModeEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 96
    const-string v0, "DarkModeOneTrackHelper"

    new-instance v1, Landroid/content/Intent;

    const-string v2, "onetrack.action.TRACK_EVENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 97
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.miui.analytics"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    const-string v2, "APP_ID"

    const-string v3, "31000000485"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    iget-object v2, p0, Lcom/android/server/DarkModeOneTrackHelper$1;->val$context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PACKAGE"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    iget-object v2, p0, Lcom/android/server/DarkModeOneTrackHelper$1;->val$newEvent:Lcom/android/server/DarkModeEvent;

    invoke-static {v1, v2}, Lcom/android/server/DarkModeOneTrackHelper;->-$$Nest$smupdateDarkModeIntent(Landroid/content/Intent;Lcom/android/server/DarkModeEvent;)V

    .line 102
    :try_start_0
    iget-object v2, p0, Lcom/android/server/DarkModeOneTrackHelper$1;->val$context:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    .line 103
    const-string/jumbo v2, "uploadDataToOneTrack Success"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    goto :goto_0

    .line 104
    :catch_0
    move-exception v2

    .line 105
    .local v2, "e":Ljava/lang/IllegalStateException;
    const-string v3, "Filed to upload DarkModeOneTrackInfo "

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 107
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :goto_0
    return-void
.end method
