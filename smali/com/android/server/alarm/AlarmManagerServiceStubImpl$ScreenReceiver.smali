.class Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AlarmManagerServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/alarm/AlarmManagerServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)V
    .locals 0

    .line 206
    iput-object p1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;-><init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 208
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, "action":Ljava/lang/String;
    const/4 v1, 0x0

    .line 210
    .local v1, "change":Z
    invoke-static {}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$sfgetsLock()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 211
    :try_start_0
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v3, :cond_0

    .line 212
    iget-object v3, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v3, v5}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fputmScreenOn(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Z)V

    goto :goto_0

    .line 213
    :cond_0
    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 214
    iget-object v3, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v3, v4}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fputmScreenOn(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Z)V

    .line 216
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v3}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fgetmScreenOn(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v3}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fgetmNetAvailable(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    move v4, v5

    :cond_3
    move v3, v4

    .line 217
    .local v3, "allow":Z
    iget-object v4, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v4}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fgetmPendingAllowed(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Z

    move-result v4

    if-eq v4, v3, :cond_4

    .line 218
    iget-object v4, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v4, v3}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fputmPendingAllowed(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Z)V

    .line 219
    const/4 v1, 0x1

    .line 221
    .end local v3    # "allow":Z
    :cond_4
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v2}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$mupdateAlarmPendingState(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)V

    .line 223
    :cond_5
    return-void

    .line 221
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method
