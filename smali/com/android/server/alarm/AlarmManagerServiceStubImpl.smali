.class public Lcom/android/server/alarm/AlarmManagerServiceStubImpl;
.super Lcom/android/server/alarm/AlarmManagerServiceStub;
.source "AlarmManagerServiceStubImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.alarm.AlarmManagerServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;,
        Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final DEFAULT_ALIGN_PERIOD:I = 0x493e0

.field private static final FOREGROUND_APP_ADJ:I = 0x0

.field private static final MIN_ALIGN_PERIOD:I = 0x395f8

.field private static final PENDING_DELAY_TIME:J = 0xf731400L

.field private static final PERCEPTIBLE_APP_ADJ:I = 0xc8

.field private static final TAG:Ljava/lang/String; = "AlarmManager"

.field private static final XMSF_HEART_BEAT:Ljava/lang/String; = "*walarm*:com.xiaomi.push.PING_TIMER"

.field private static final sLock:Ljava/lang/Object;


# instance fields
.field private ADJUST_WHITE_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private PERSIST_PACKAGES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mAdjustTask:Ljava/lang/Runnable;

.field private mAlarmService:Lcom/android/server/alarm/AlarmManagerService;

.field private mBaseAlignTime:J

.field private mCurAlignPeriod:J

.field private volatile mDeskclockDelivering:Z

.field private volatile mNetAvailable:Z

.field private volatile mPendingAllowed:Z

.field private volatile mScreenOn:Z

.field private mXmsfUid:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmAlarmService(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Lcom/android/server/alarm/AlarmManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mAlarmService:Lcom/android/server/alarm/AlarmManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNetAvailable(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mNetAvailable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPendingAllowed(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mPendingAllowed:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmScreenOn(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mScreenOn:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmNetAvailable(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mNetAvailable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmPendingAllowed(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mPendingAllowed:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmScreenOn(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mScreenOn:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$madjustAlarmLocked(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Lcom/android/server/alarm/Alarm;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->adjustAlarmLocked(Lcom/android/server/alarm/Alarm;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdateAlarmPendingState(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->updateAlarmPendingState()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetsLock()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->sLock:Ljava/lang/Object;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 35
    invoke-static {}, Lmiui/os/Build;->isDebuggable()Z

    move-result v0

    sput-boolean v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->DEBUG:Z

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->sLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 33
    invoke-direct {p0}, Lcom/android/server/alarm/AlarmManagerServiceStub;-><init>()V

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mNetAvailable:Z

    .line 40
    iput-boolean v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mScreenOn:Z

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mPendingAllowed:Z

    .line 42
    iput-boolean v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mDeskclockDelivering:Z

    .line 46
    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J

    .line 47
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mBaseAlignTime:J

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mXmsfUid:I

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->ADJUST_WHITE_LIST:Ljava/util/List;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->PERSIST_PACKAGES:Ljava/util/List;

    .line 187
    new-instance v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2;

    invoke-direct {v0, p0}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2;-><init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)V

    iput-object v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mAdjustTask:Ljava/lang/Runnable;

    return-void
.end method

.method public static CheckIfAlarmGenralRistrictApply(II)Z
    .locals 5
    .param p0, "uid"    # I
    .param p1, "pid"    # I

    .line 172
    const/16 v0, 0x2710

    const/4 v1, 0x0

    if-gt p0, v0, :cond_0

    .line 173
    return v1

    .line 176
    :cond_0
    invoke-static {p1}, Lcom/android/server/am/ProcessUtils;->getCurAdjByPid(I)I

    move-result v0

    .line 177
    .local v0, "curAdj":I
    invoke-static {p1}, Lcom/android/server/am/ProcessUtils;->getProcStateByPid(I)I

    move-result v2

    .line 178
    .local v2, "procState":I
    invoke-static {p1}, Lcom/android/server/am/ProcessUtils;->hasForegroundActivities(I)Z

    move-result v3

    .line 180
    .local v3, "hasForegroundActivities":Z
    if-nez v3, :cond_2

    if-ltz v0, :cond_1

    const/16 v4, 0xc8

    if-gt v0, v4, :cond_1

    const/16 v4, 0xb

    if-eq v2, v4, :cond_1

    goto :goto_0

    .line 184
    :cond_1
    const/4 v1, 0x1

    return v1

    .line 182
    :cond_2
    :goto_0
    return v1
.end method

.method private adjustAlarmLocked(Lcom/android/server/alarm/Alarm;)Z
    .locals 6
    .param p1, "a"    # Lcom/android/server/alarm/Alarm;

    .line 149
    iget v0, p1, Lcom/android/server/alarm/Alarm;->flags:I

    and-int/lit8 v0, v0, 0xf

    if-nez v0, :cond_4

    iget-object v0, p1, Lcom/android/server/alarm/Alarm;->alarmClock:Landroid/app/AlarmManager$AlarmClockInfo;

    if-nez v0, :cond_4

    .line 154
    invoke-static {p1}, Lcom/android/server/alarm/AlarmManagerService;->isTimeTickAlarm(Lcom/android/server/alarm/Alarm;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->ADJUST_WHITE_LIST:Ljava/util/List;

    iget-object v1, p1, Lcom/android/server/alarm/Alarm;->sourcePackage:Ljava/lang/String;

    .line 155
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 158
    :cond_0
    const-wide/16 v0, 0x0

    .line 159
    .local v0, "temp":J
    iget-boolean v2, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mScreenOn:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mNetAvailable:Z

    if-nez v2, :cond_3

    .line 160
    :cond_1
    sget-boolean v2, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 161
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pending alarm: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/android/server/alarm/Alarm;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/android/server/alarm/Alarm;->statsTag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", origWhen: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 162
    invoke-virtual {p1}, Lcom/android/server/alarm/Alarm;->getWhenElapsed()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 163
    .local v2, "stringBuilder":Ljava/lang/String;
    const-string v3, "AlarmManager"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    .end local v2    # "stringBuilder":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/android/server/alarm/Alarm;->getRequestedElapsed()J

    move-result-wide v2

    const-wide/32 v4, 0xf731400

    add-long v0, v2, v4

    .line 167
    :cond_3
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0, v1}, Lcom/android/server/alarm/Alarm;->setPolicyElapsed(IJ)Z

    move-result v2

    return v2

    .line 156
    .end local v0    # "temp":J
    :cond_4
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private updateAlarmPendingState()V
    .locals 4

    .line 200
    iget-object v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mAlarmService:Lcom/android/server/alarm/AlarmManagerService;

    iget-object v0, v0, Lcom/android/server/alarm/AlarmManagerService;->mHandler:Lcom/android/server/alarm/AlarmManagerService$AlarmHandler;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mAlarmService:Lcom/android/server/alarm/AlarmManagerService;

    iget-object v0, v0, Lcom/android/server/alarm/AlarmManagerService;->mHandler:Lcom/android/server/alarm/AlarmManagerService$AlarmHandler;

    iget-object v1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mAdjustTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/server/alarm/AlarmManagerService$AlarmHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 202
    iget-object v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mAlarmService:Lcom/android/server/alarm/AlarmManagerService;

    iget-object v0, v0, Lcom/android/server/alarm/AlarmManagerService;->mHandler:Lcom/android/server/alarm/AlarmManagerService$AlarmHandler;

    iget-object v1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mAdjustTask:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/alarm/AlarmManagerService$AlarmHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 204
    :cond_0
    return-void
.end method


# virtual methods
.method public alignAlarmLocked(Lcom/android/server/alarm/Alarm;)Z
    .locals 21
    .param p1, "a"    # Lcom/android/server/alarm/Alarm;

    .line 94
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 95
    .local v2, "now":J
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/alarm/Alarm;->getRequestedElapsed()J

    move-result-wide v4

    .line 96
    .local v4, "requestedElapsed":J
    iget-boolean v6, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mScreenOn:Z

    const/4 v7, 0x0

    if-nez v6, :cond_13

    sub-long v8, v4, v2

    const-wide/32 v10, 0xea60

    cmp-long v6, v8, v10

    const-wide/16 v8, 0x0

    if-gtz v6, :cond_1

    iget-wide v10, v1, Lcom/android/server/alarm/Alarm;->windowLength:J

    cmp-long v6, v10, v8

    if-eqz v6, :cond_0

    goto :goto_0

    :cond_0
    move-wide v15, v2

    goto/16 :goto_6

    :cond_1
    :goto_0
    iget v6, v1, Lcom/android/server/alarm/Alarm;->flags:I

    and-int/lit8 v6, v6, 0x10

    if-nez v6, :cond_13

    iget-object v6, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->ADJUST_WHITE_LIST:Ljava/util/List;

    iget-object v10, v1, Lcom/android/server/alarm/Alarm;->sourcePackage:Ljava/lang/String;

    .line 98
    invoke-interface {v6, v10}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_12

    .line 99
    invoke-static {}, Lcom/miui/whetstone/PowerKeeperPolicy;->getInstance()Lcom/miui/whetstone/PowerKeeperPolicy;

    move-result-object v6

    iget v10, v1, Lcom/android/server/alarm/Alarm;->uid:I

    iget-object v11, v1, Lcom/android/server/alarm/Alarm;->statsTag:Ljava/lang/String;

    invoke-virtual {v6, v10, v11}, Lcom/miui/whetstone/PowerKeeperPolicy;->isAlarmAlignEnable(ILjava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 100
    invoke-static/range {p1 .. p1}, Lcom/android/server/alarm/AlarmManagerService;->isTimeTickAlarm(Lcom/android/server/alarm/Alarm;)Z

    move-result v6

    if-nez v6, :cond_10

    iget-object v6, v1, Lcom/android/server/alarm/Alarm;->alarmClock:Landroid/app/AlarmManager$AlarmClockInfo;

    if-eqz v6, :cond_2

    move-wide v15, v2

    goto/16 :goto_6

    .line 104
    :cond_2
    iget v6, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mXmsfUid:I

    const/4 v10, -0x1

    const-string v11, "AlarmManager"

    const-wide/16 v14, 0x1

    const-wide/32 v16, 0x493e0

    if-eq v6, v10, :cond_3

    iget v6, v1, Lcom/android/server/alarm/Alarm;->uid:I

    iget v10, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mXmsfUid:I

    if-ne v6, v10, :cond_6

    :cond_3
    iget-object v6, v1, Lcom/android/server/alarm/Alarm;->statsTag:Ljava/lang/String;

    const-string v10, "*walarm*:com.xiaomi.push.PING_TIMER"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 105
    iget v6, v1, Lcom/android/server/alarm/Alarm;->uid:I

    iput v6, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mXmsfUid:I

    .line 107
    iget-wide v8, v1, Lcom/android/server/alarm/Alarm;->origWhen:J

    sub-long/2addr v8, v2

    cmp-long v6, v8, v16

    if-gez v6, :cond_5

    .line 108
    iget-wide v8, v1, Lcom/android/server/alarm/Alarm;->origWhen:J

    sub-long/2addr v8, v2

    sub-long/2addr v8, v14

    const-wide/16 v18, 0x1388

    div-long v8, v8, v18

    add-long/2addr v8, v14

    mul-long v8, v8, v18

    .line 109
    .local v8, "alignPeriod":J
    iget-wide v14, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J

    const-wide/32 v12, 0x395f8

    cmp-long v6, v14, v12

    if-gez v6, :cond_4

    iput-wide v12, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J

    .line 110
    :cond_4
    sub-long v12, v4, v8

    iput-wide v12, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mBaseAlignTime:J

    goto :goto_1

    .line 112
    .end local v8    # "alignPeriod":J
    :cond_5
    const-wide/32 v8, 0x493e0

    .line 113
    .restart local v8    # "alignPeriod":J
    const-wide/16 v12, 0x2

    mul-long v14, v8, v12

    sub-long v12, v4, v14

    iput-wide v12, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mBaseAlignTime:J

    .line 115
    :goto_1
    iget-wide v12, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J

    cmp-long v6, v12, v8

    if-eqz v6, :cond_6

    .line 116
    iput-wide v8, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J

    .line 117
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "align period changed mCurAlignPeriod="

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v12, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, " mBaseAlignTime="

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v12, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mBaseAlignTime:J

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v11, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    return v7

    .line 121
    .end local v8    # "alignPeriod":J
    :cond_6
    iget-wide v8, v1, Lcom/android/server/alarm/Alarm;->windowLength:J

    const-wide/16 v12, 0x0

    cmp-long v6, v8, v12

    if-lez v6, :cond_7

    .line 122
    iget-wide v8, v1, Lcom/android/server/alarm/Alarm;->windowLength:J

    iget-wide v12, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J

    div-long/2addr v8, v12

    mul-long/2addr v8, v12

    iput-wide v8, v1, Lcom/android/server/alarm/Alarm;->windowLength:J

    .line 124
    :cond_7
    iget-wide v8, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mBaseAlignTime:J

    sub-long v12, v4, v8

    const-wide/16 v14, 0x1

    sub-long/2addr v12, v14

    move-wide/from16 v18, v8

    iget-wide v7, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J

    div-long/2addr v12, v7

    add-long/2addr v12, v14

    mul-long/2addr v12, v7

    add-long v12, v12, v18

    .line 125
    .local v12, "temp":J
    sub-long v7, v12, v2

    .line 126
    .local v7, "delta":J
    sub-long v9, v4, v2

    .line 127
    .local v9, "requestedDelta":J
    iget v14, v1, Lcom/android/server/alarm/Alarm;->type:I

    if-nez v14, :cond_8

    const/4 v14, 0x1

    goto :goto_2

    :cond_8
    const/4 v14, 0x0

    :goto_2
    iget v6, v1, Lcom/android/server/alarm/Alarm;->type:I

    const/4 v15, 0x2

    if-ne v6, v15, :cond_9

    const/4 v6, 0x1

    goto :goto_3

    :cond_9
    const/4 v6, 0x0

    :goto_3
    or-int/2addr v6, v14

    .line 128
    .local v6, "isWakeup":Z
    iget-wide v14, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J

    cmp-long v19, v14, v16

    if-gez v19, :cond_a

    cmp-long v19, v9, v16

    if-gez v19, :cond_a

    cmp-long v16, v7, v16

    if-gtz v16, :cond_c

    :cond_a
    cmp-long v16, v9, v14

    if-gez v16, :cond_b

    const-wide/16 v16, 0x3

    mul-long v16, v16, v14

    const-wide/16 v19, 0x2

    div-long v16, v16, v19

    cmp-long v16, v7, v16

    if-gtz v16, :cond_c

    goto :goto_4

    :cond_b
    const-wide/16 v19, 0x2

    :goto_4
    mul-long v16, v14, v19

    cmp-long v16, v9, v16

    if-gez v16, :cond_d

    mul-long v14, v14, v19

    cmp-long v14, v7, v14

    if-lez v14, :cond_d

    :cond_c
    iget-wide v14, v1, Lcom/android/server/alarm/Alarm;->windowLength:J

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-nez v14, :cond_d

    if-eqz v6, :cond_d

    .line 132
    iget-wide v14, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J

    sub-long/2addr v12, v14

    .line 134
    :cond_d
    cmp-long v14, v12, v4

    if-eqz v14, :cond_f

    .line 135
    sget-boolean v14, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->DEBUG:Z

    if-eqz v14, :cond_e

    .line 136
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "alignAlarms: tag="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v1, Lcom/android/server/alarm/Alarm;->statsTag:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " whenElapsed="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-wide v15, v2

    .end local v2    # "now":J
    .local v15, "now":J
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/alarm/Alarm;->getWhenElapsed()J

    move-result-wide v2

    invoke-virtual {v14, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " aligntime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 135
    .end local v15    # "now":J
    .restart local v2    # "now":J
    :cond_e
    move-wide v15, v2

    .line 137
    .end local v2    # "now":J
    .restart local v15    # "now":J
    :goto_5
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v12, v13}, Lcom/android/server/alarm/Alarm;->setPolicyElapsed(IJ)Z

    move-result v2

    return v2

    .line 139
    .end local v15    # "now":J
    .restart local v2    # "now":J
    :cond_f
    move-wide v15, v2

    const/4 v2, 0x0

    .end local v2    # "now":J
    .restart local v15    # "now":J
    return v2

    .line 100
    .end local v6    # "isWakeup":Z
    .end local v7    # "delta":J
    .end local v9    # "requestedDelta":J
    .end local v12    # "temp":J
    .end local v15    # "now":J
    .restart local v2    # "now":J
    :cond_10
    move-wide v15, v2

    .end local v2    # "now":J
    .restart local v15    # "now":J
    goto :goto_6

    .line 99
    .end local v15    # "now":J
    .restart local v2    # "now":J
    :cond_11
    move-wide v15, v2

    .end local v2    # "now":J
    .restart local v15    # "now":J
    goto :goto_6

    .line 98
    .end local v15    # "now":J
    .restart local v2    # "now":J
    :cond_12
    move-wide v15, v2

    .end local v2    # "now":J
    .restart local v15    # "now":J
    goto :goto_6

    .line 96
    .end local v15    # "now":J
    .restart local v2    # "now":J
    :cond_13
    move-wide v15, v2

    .line 102
    .end local v2    # "now":J
    .restart local v15    # "now":J
    :goto_6
    const/4 v2, 0x0

    return v2
.end method

.method public checkAlarmIsAllowedSend(Landroid/content/Context;Lcom/android/server/alarm/Alarm;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "alarm"    # Lcom/android/server/alarm/Alarm;

    .line 143
    if-eqz p2, :cond_1

    iget-object v0, p2, Lcom/android/server/alarm/Alarm;->operation:Landroid/app/PendingIntent;

    if-nez v0, :cond_0

    goto :goto_0

    .line 144
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    iget-object v1, p2, Lcom/android/server/alarm/Alarm;->operation:Landroid/app/PendingIntent;

    invoke-virtual {v1}, Landroid/app/PendingIntent;->getCreatorUid()I

    move-result v1

    iget-object v2, p2, Lcom/android/server/alarm/Alarm;->statsTag:Ljava/lang/String;

    iget-object v3, p2, Lcom/android/server/alarm/Alarm;->operation:Landroid/app/PendingIntent;

    .line 145
    invoke-virtual {v3}, Landroid/app/PendingIntent;->getCreatorUid()I

    move-result v3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-static {v3, v4}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->CheckIfAlarmGenralRistrictApply(II)Z

    move-result v3

    .line 144
    invoke-static {v0, v1, v2, v3}, Lcom/miui/whetstone/client/WhetstoneClientManager;->isAlarmAllowedLocked(IILjava/lang/String;Z)Z

    move-result v0

    return v0

    .line 143
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public checkDeskclockDelivering(Ljava/lang/String;)V
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 88
    if-eqz p1, :cond_0

    const-string v0, "com.android.deskclock"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mDeskclockDelivering:Z

    .line 91
    :cond_0
    return-void
.end method

.method public filterPersistPackages([Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .param p1, "pkgList"    # [Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 57
    .local v0, "filteredPkgList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    if-eqz p1, :cond_1

    array-length v2, p1

    if-lez v2, :cond_1

    .line 58
    array-length v2, p1

    move v3, v1

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, p1, v3

    .line 59
    .local v4, "pkg":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->PERSIST_PACKAGES:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 60
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    .end local v4    # "pkg":Ljava/lang/String;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 64
    :cond_1
    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    return-object v1
.end method

.method public init(Lcom/android/server/alarm/AlarmManagerService;)V
    .locals 6
    .param p1, "service"    # Lcom/android/server/alarm/AlarmManagerService;

    .line 256
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/android/server/alarm/AlarmManagerService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x11030000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->ADJUST_WHITE_LIST:Ljava/util/List;

    .line 257
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/android/server/alarm/AlarmManagerService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x11030002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->PERSIST_PACKAGES:Ljava/util/List;

    .line 258
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    .line 259
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/android/server/alarm/AlarmManagerService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x11030001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 260
    .local v0, "ADJUST_WHITE_LIST_GLOBAL":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 261
    .local v2, "pkgName":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->ADJUST_WHITE_LIST:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    .end local v2    # "pkgName":Ljava/lang/String;
    goto :goto_0

    .line 264
    .end local v0    # "ADJUST_WHITE_LIST_GLOBAL":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    iput-object p1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mAlarmService:Lcom/android/server/alarm/AlarmManagerService;

    .line 265
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 266
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 267
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 268
    invoke-virtual {p1}, Lcom/android/server/alarm/AlarmManagerService;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;-><init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver-IA;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 269
    new-instance v1, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;

    invoke-direct {v1, p0, v3}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;-><init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl-IA;)V

    .line 270
    .local v1, "networkCallback":Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;
    new-instance v2, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v2}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 271
    .local v2, "builder":Landroid/net/NetworkRequest$Builder;
    invoke-virtual {v2}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v3

    .line 272
    .local v3, "request":Landroid/net/NetworkRequest;
    invoke-virtual {p1}, Lcom/android/server/alarm/AlarmManagerService;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/ConnectivityManager;

    .line 273
    .local v4, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v4, v3, v1}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 274
    return-void
.end method

.method public releaseWakeLock(Lcom/android/server/alarm/AlarmManagerService$AlarmHandler;Landroid/os/PowerManager$WakeLock;Ljava/lang/Object;)V
    .locals 3
    .param p1, "handler"    # Lcom/android/server/alarm/AlarmManagerService$AlarmHandler;
    .param p2, "wakeLock"    # Landroid/os/PowerManager$WakeLock;
    .param p3, "aLock"    # Ljava/lang/Object;

    .line 69
    iget-boolean v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mDeskclockDelivering:Z

    if-eqz v0, :cond_0

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mDeskclockDelivering:Z

    .line 71
    const-string v0, "AlarmManager"

    const-string v1, "Delay release wakelock for deskclock."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    new-instance v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$1;

    invoke-direct {v0, p0, p3, p2}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$1;-><init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Ljava/lang/Object;Landroid/os/PowerManager$WakeLock;)V

    const-wide/16 v1, 0x12c

    invoke-virtual {p1, v0, v1, v2}, Lcom/android/server/alarm/AlarmManagerService$AlarmHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 82
    :cond_0
    invoke-virtual {p2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 84
    :goto_0
    return-void
.end method
