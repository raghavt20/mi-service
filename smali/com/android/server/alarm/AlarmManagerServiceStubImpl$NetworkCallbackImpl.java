class com.android.server.alarm.AlarmManagerServiceStubImpl$NetworkCallbackImpl extends android.net.ConnectivityManager$NetworkCallback {
	 /* .source "AlarmManagerServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/alarm/AlarmManagerServiceStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "NetworkCallbackImpl" */
} // .end annotation
/* # instance fields */
final com.android.server.alarm.AlarmManagerServiceStubImpl this$0; //synthetic
/* # direct methods */
private com.android.server.alarm.AlarmManagerServiceStubImpl$NetworkCallbackImpl ( ) {
/* .locals 0 */
/* .line 226 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V */
return;
} // .end method
 com.android.server.alarm.AlarmManagerServiceStubImpl$NetworkCallbackImpl ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;-><init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)V */
return;
} // .end method
/* # virtual methods */
public void onAvailable ( android.net.Network p0 ) {
/* .locals 4 */
/* .param p1, "network" # Landroid/net/Network; */
/* .line 229 */
int v0 = 0; // const/4 v0, 0x0
/* .line 230 */
/* .local v0, "change":Z */
com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$sfgetsLock ( );
/* monitor-enter v1 */
/* .line 231 */
try { // :try_start_0
	 v2 = this.this$0;
	 int v3 = 1; // const/4 v3, 0x1
	 com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fputmNetAvailable ( v2,v3 );
	 /* .line 232 */
	 v2 = this.this$0;
	 v2 = 	 com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fgetmScreenOn ( v2 );
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 v2 = this.this$0;
		 v2 = 		 com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fgetmNetAvailable ( v2 );
		 /* if-nez v2, :cond_0 */
	 } // :cond_0
	 int v3 = 0; // const/4 v3, 0x0
} // :cond_1
} // :goto_0
/* move v2, v3 */
/* .line 233 */
/* .local v2, "allow":Z */
v3 = this.this$0;
v3 = com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fgetmPendingAllowed ( v3 );
/* if-eq v3, v2, :cond_2 */
/* .line 234 */
v3 = this.this$0;
com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fputmPendingAllowed ( v3,v2 );
/* .line 235 */
int v0 = 1; // const/4 v0, 0x1
/* .line 237 */
} // .end local v2 # "allow":Z
} // :cond_2
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 238 */
if ( v0 != null) { // if-eqz v0, :cond_3
v1 = this.this$0;
com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$mupdateAlarmPendingState ( v1 );
/* .line 239 */
} // :cond_3
return;
/* .line 237 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
public void onLost ( android.net.Network p0 ) {
/* .locals 4 */
/* .param p1, "network" # Landroid/net/Network; */
/* .line 242 */
int v0 = 0; // const/4 v0, 0x0
/* .line 243 */
/* .local v0, "change":Z */
com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$sfgetsLock ( );
/* monitor-enter v1 */
/* .line 244 */
try { // :try_start_0
v2 = this.this$0;
int v3 = 0; // const/4 v3, 0x0
com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fputmNetAvailable ( v2,v3 );
/* .line 245 */
v2 = this.this$0;
v2 = com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fgetmScreenOn ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.this$0;
v2 = com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fgetmNetAvailable ( v2 );
/* if-nez v2, :cond_1 */
} // :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_1
/* move v2, v3 */
/* .line 246 */
/* .local v2, "allow":Z */
v3 = this.this$0;
v3 = com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fgetmPendingAllowed ( v3 );
/* if-eq v3, v2, :cond_2 */
/* .line 247 */
v3 = this.this$0;
com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fputmPendingAllowed ( v3,v2 );
/* .line 248 */
int v0 = 1; // const/4 v0, 0x1
/* .line 250 */
} // .end local v2 # "allow":Z
} // :cond_2
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 251 */
if ( v0 != null) { // if-eqz v0, :cond_3
v1 = this.this$0;
com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$mupdateAlarmPendingState ( v1 );
/* .line 252 */
} // :cond_3
return;
/* .line 250 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
