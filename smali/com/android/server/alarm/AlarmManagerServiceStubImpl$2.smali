.class Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2;
.super Ljava/lang/Object;
.source "AlarmManagerServiceStubImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/alarm/AlarmManagerServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;


# direct methods
.method public static synthetic $r8$lambda$sLxqjIXUK85gdj51Xogem8tKV8U(Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2;Lcom/android/server/alarm/Alarm;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2;->lambda$run$0(Lcom/android/server/alarm/Alarm;)Z

    move-result p0

    return p0
.end method

.method constructor <init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    .line 187
    iput-object p1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synthetic lambda$run$0(Lcom/android/server/alarm/Alarm;)Z
    .locals 1
    .param p1, "a"    # Lcom/android/server/alarm/Alarm;

    .line 192
    iget-object v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v0, p1}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$madjustAlarmLocked(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Lcom/android/server/alarm/Alarm;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 190
    iget-object v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fgetmAlarmService(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Lcom/android/server/alarm/AlarmManagerService;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/alarm/AlarmManagerService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 191
    :try_start_0
    iget-object v1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fgetmAlarmService(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Lcom/android/server/alarm/AlarmManagerService;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/alarm/AlarmManagerService;->mAlarmStore:Lcom/android/server/alarm/AlarmStore;

    .line 192
    .local v1, "store":Lcom/android/server/alarm/AlarmStore;
    if-eqz v1, :cond_0

    new-instance v2, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2;)V

    invoke-interface {v1, v2}, Lcom/android/server/alarm/AlarmStore;->updateAlarmDeliveries(Lcom/android/server/alarm/AlarmStore$AlarmDeliveryCalculator;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 193
    iget-object v2, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v2}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fgetmAlarmService(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Lcom/android/server/alarm/AlarmManagerService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/alarm/AlarmManagerService;->rescheduleKernelAlarmsLocked()V

    .line 195
    .end local v1    # "store":Lcom/android/server/alarm/AlarmStore;
    :cond_0
    monitor-exit v0

    .line 196
    return-void

    .line 195
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
