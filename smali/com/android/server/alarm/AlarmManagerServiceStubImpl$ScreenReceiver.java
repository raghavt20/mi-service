class com.android.server.alarm.AlarmManagerServiceStubImpl$ScreenReceiver extends android.content.BroadcastReceiver {
	 /* .source "AlarmManagerServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/alarm/AlarmManagerServiceStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "ScreenReceiver" */
} // .end annotation
/* # instance fields */
final com.android.server.alarm.AlarmManagerServiceStubImpl this$0; //synthetic
/* # direct methods */
private com.android.server.alarm.AlarmManagerServiceStubImpl$ScreenReceiver ( ) {
/* .locals 0 */
/* .line 206 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
 com.android.server.alarm.AlarmManagerServiceStubImpl$ScreenReceiver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;-><init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 208 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 209 */
/* .local v0, "action":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .line 210 */
/* .local v1, "change":Z */
com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$sfgetsLock ( );
/* monitor-enter v2 */
/* .line 211 */
try { // :try_start_0
	 final String v3 = "android.intent.action.SCREEN_ON"; // const-string v3, "android.intent.action.SCREEN_ON"
	 v3 = 	 (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 int v4 = 0; // const/4 v4, 0x0
	 int v5 = 1; // const/4 v5, 0x1
	 if ( v3 != null) { // if-eqz v3, :cond_0
		 /* .line 212 */
		 v3 = this.this$0;
		 com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fputmScreenOn ( v3,v5 );
		 /* .line 213 */
	 } // :cond_0
	 final String v3 = "android.intent.action.SCREEN_OFF"; // const-string v3, "android.intent.action.SCREEN_OFF"
	 v3 = 	 (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 /* .line 214 */
		 v3 = this.this$0;
		 com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fputmScreenOn ( v3,v4 );
		 /* .line 216 */
	 } // :cond_1
} // :goto_0
v3 = this.this$0;
v3 = com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fgetmScreenOn ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_2
	 v3 = this.this$0;
	 v3 = 	 com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fgetmNetAvailable ( v3 );
	 /* if-nez v3, :cond_3 */
} // :cond_2
/* move v4, v5 */
} // :cond_3
/* move v3, v4 */
/* .line 217 */
/* .local v3, "allow":Z */
v4 = this.this$0;
v4 = com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fgetmPendingAllowed ( v4 );
/* if-eq v4, v3, :cond_4 */
/* .line 218 */
v4 = this.this$0;
com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fputmPendingAllowed ( v4,v3 );
/* .line 219 */
int v1 = 1; // const/4 v1, 0x1
/* .line 221 */
} // .end local v3 # "allow":Z
} // :cond_4
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 222 */
if ( v1 != null) { // if-eqz v1, :cond_5
v2 = this.this$0;
com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$mupdateAlarmPendingState ( v2 );
/* .line 223 */
} // :cond_5
return;
/* .line 221 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v3 */
} // .end method
