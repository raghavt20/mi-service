class com.android.server.alarm.AlarmManagerServiceStubImpl$2 implements java.lang.Runnable {
	 /* .source "AlarmManagerServiceStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/alarm/AlarmManagerServiceStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.alarm.AlarmManagerServiceStubImpl this$0; //synthetic
/* # direct methods */
public static Boolean $r8$lambda$sLxqjIXUK85gdj51Xogem8tKV8U ( com.android.server.alarm.AlarmManagerServiceStubImpl$2 p0, com.android.server.alarm.Alarm p1 ) { //synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2;->lambda$run$0(Lcom/android/server/alarm/Alarm;)Z */
} // .end method
 com.android.server.alarm.AlarmManagerServiceStubImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/alarm/AlarmManagerServiceStubImpl; */
/* .line 187 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private Boolean lambda$run$0 ( com.android.server.alarm.Alarm p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "a" # Lcom/android/server/alarm/Alarm; */
/* .line 192 */
v0 = this.this$0;
v0 = com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$madjustAlarmLocked ( v0,p1 );
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 3 */
/* .line 190 */
v0 = this.this$0;
com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fgetmAlarmService ( v0 );
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 191 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fgetmAlarmService ( v1 );
	 v1 = this.mAlarmStore;
	 /* .line 192 */
	 /* .local v1, "store":Lcom/android/server/alarm/AlarmStore; */
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* new-instance v2, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2$$ExternalSyntheticLambda0; */
		 v2 = 		 /* invoke-direct {v2, p0}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2;)V */
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* .line 193 */
			 v2 = this.this$0;
			 com.android.server.alarm.AlarmManagerServiceStubImpl .-$$Nest$fgetmAlarmService ( v2 );
			 (( com.android.server.alarm.AlarmManagerService ) v2 ).rescheduleKernelAlarmsLocked ( ); // invoke-virtual {v2}, Lcom/android/server/alarm/AlarmManagerService;->rescheduleKernelAlarmsLocked()V
			 /* .line 195 */
		 } // .end local v1 # "store":Lcom/android/server/alarm/AlarmStore;
	 } // :cond_0
	 /* monitor-exit v0 */
	 /* .line 196 */
	 return;
	 /* .line 195 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
