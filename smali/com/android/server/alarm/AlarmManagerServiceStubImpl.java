public class com.android.server.alarm.AlarmManagerServiceStubImpl extends com.android.server.alarm.AlarmManagerServiceStub {
	 /* .source "AlarmManagerServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.alarm.AlarmManagerServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;, */
/* Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl; */
/* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
private static final Integer DEFAULT_ALIGN_PERIOD;
private static final Integer FOREGROUND_APP_ADJ;
private static final Integer MIN_ALIGN_PERIOD;
private static final Long PENDING_DELAY_TIME;
private static final Integer PERCEPTIBLE_APP_ADJ;
private static final java.lang.String TAG;
private static final java.lang.String XMSF_HEART_BEAT;
private static final java.lang.Object sLock;
/* # instance fields */
private java.util.List ADJUST_WHITE_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List PERSIST_PACKAGES;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
java.lang.Runnable mAdjustTask;
private com.android.server.alarm.AlarmManagerService mAlarmService;
private Long mBaseAlignTime;
private Long mCurAlignPeriod;
private volatile Boolean mDeskclockDelivering;
private volatile Boolean mNetAvailable;
private volatile Boolean mPendingAllowed;
private volatile Boolean mScreenOn;
private Integer mXmsfUid;
/* # direct methods */
static com.android.server.alarm.AlarmManagerService -$$Nest$fgetmAlarmService ( com.android.server.alarm.AlarmManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAlarmService;
} // .end method
static Boolean -$$Nest$fgetmNetAvailable ( com.android.server.alarm.AlarmManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mNetAvailable:Z */
} // .end method
static Boolean -$$Nest$fgetmPendingAllowed ( com.android.server.alarm.AlarmManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mPendingAllowed:Z */
} // .end method
static Boolean -$$Nest$fgetmScreenOn ( com.android.server.alarm.AlarmManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mScreenOn:Z */
} // .end method
static void -$$Nest$fputmNetAvailable ( com.android.server.alarm.AlarmManagerServiceStubImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mNetAvailable:Z */
return;
} // .end method
static void -$$Nest$fputmPendingAllowed ( com.android.server.alarm.AlarmManagerServiceStubImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mPendingAllowed:Z */
return;
} // .end method
static void -$$Nest$fputmScreenOn ( com.android.server.alarm.AlarmManagerServiceStubImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mScreenOn:Z */
return;
} // .end method
static Boolean -$$Nest$madjustAlarmLocked ( com.android.server.alarm.AlarmManagerServiceStubImpl p0, com.android.server.alarm.Alarm p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->adjustAlarmLocked(Lcom/android/server/alarm/Alarm;)Z */
} // .end method
static void -$$Nest$mupdateAlarmPendingState ( com.android.server.alarm.AlarmManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->updateAlarmPendingState()V */
return;
} // .end method
static java.lang.Object -$$Nest$sfgetsLock ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.alarm.AlarmManagerServiceStubImpl.sLock;
} // .end method
static com.android.server.alarm.AlarmManagerServiceStubImpl ( ) {
/* .locals 1 */
/* .line 35 */
v0 = miui.os.Build .isDebuggable ( );
com.android.server.alarm.AlarmManagerServiceStubImpl.DEBUG = (v0!= 0);
/* .line 38 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public com.android.server.alarm.AlarmManagerServiceStubImpl ( ) {
/* .locals 2 */
/* .line 33 */
/* invoke-direct {p0}, Lcom/android/server/alarm/AlarmManagerServiceStub;-><init>()V */
/* .line 39 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mNetAvailable:Z */
/* .line 40 */
/* iput-boolean v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mScreenOn:Z */
/* .line 41 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mPendingAllowed:Z */
/* .line 42 */
/* iput-boolean v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mDeskclockDelivering:Z */
/* .line 46 */
/* const-wide/32 v0, 0x493e0 */
/* iput-wide v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J */
/* .line 47 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mBaseAlignTime:J */
/* .line 49 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mXmsfUid:I */
/* .line 51 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.ADJUST_WHITE_LIST = v0;
/* .line 52 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.PERSIST_PACKAGES = v0;
/* .line 187 */
/* new-instance v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$2;-><init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)V */
this.mAdjustTask = v0;
return;
} // .end method
public static Boolean CheckIfAlarmGenralRistrictApply ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p0, "uid" # I */
/* .param p1, "pid" # I */
/* .line 172 */
/* const/16 v0, 0x2710 */
int v1 = 0; // const/4 v1, 0x0
/* if-gt p0, v0, :cond_0 */
/* .line 173 */
/* .line 176 */
} // :cond_0
v0 = com.android.server.am.ProcessUtils .getCurAdjByPid ( p1 );
/* .line 177 */
/* .local v0, "curAdj":I */
v2 = com.android.server.am.ProcessUtils .getProcStateByPid ( p1 );
/* .line 178 */
/* .local v2, "procState":I */
v3 = com.android.server.am.ProcessUtils .hasForegroundActivities ( p1 );
/* .line 180 */
/* .local v3, "hasForegroundActivities":Z */
/* if-nez v3, :cond_2 */
/* if-ltz v0, :cond_1 */
/* const/16 v4, 0xc8 */
/* if-gt v0, v4, :cond_1 */
/* const/16 v4, 0xb */
/* if-eq v2, v4, :cond_1 */
/* .line 184 */
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* .line 182 */
} // :cond_2
} // :goto_0
} // .end method
private Boolean adjustAlarmLocked ( com.android.server.alarm.Alarm p0 ) {
/* .locals 6 */
/* .param p1, "a" # Lcom/android/server/alarm/Alarm; */
/* .line 149 */
/* iget v0, p1, Lcom/android/server/alarm/Alarm;->flags:I */
/* and-int/lit8 v0, v0, 0xf */
/* if-nez v0, :cond_4 */
v0 = this.alarmClock;
/* if-nez v0, :cond_4 */
/* .line 154 */
v0 = com.android.server.alarm.AlarmManagerService .isTimeTickAlarm ( p1 );
/* if-nez v0, :cond_4 */
v0 = this.ADJUST_WHITE_LIST;
v1 = this.sourcePackage;
v0 = /* .line 155 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 158 */
} // :cond_0
/* const-wide/16 v0, 0x0 */
/* .line 159 */
/* .local v0, "temp":J */
/* iget-boolean v2, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mScreenOn:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* iget-boolean v2, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mNetAvailable:Z */
/* if-nez v2, :cond_3 */
/* .line 160 */
} // :cond_1
/* sget-boolean v2, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 161 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "pending alarm: "; // const-string v3, "pending alarm: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.packageName;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", tag: "; // const-string v3, ", tag: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.statsTag;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", origWhen: "; // const-string v3, ", origWhen: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 162 */
(( com.android.server.alarm.Alarm ) p1 ).getWhenElapsed ( ); // invoke-virtual {p1}, Lcom/android/server/alarm/Alarm;->getWhenElapsed()J
/* move-result-wide v3 */
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 163 */
/* .local v2, "stringBuilder":Ljava/lang/String; */
final String v3 = "AlarmManager"; // const-string v3, "AlarmManager"
android.util.Log .d ( v3,v2 );
/* .line 165 */
} // .end local v2 # "stringBuilder":Ljava/lang/String;
} // :cond_2
(( com.android.server.alarm.Alarm ) p1 ).getRequestedElapsed ( ); // invoke-virtual {p1}, Lcom/android/server/alarm/Alarm;->getRequestedElapsed()J
/* move-result-wide v2 */
/* const-wide/32 v4, 0xf731400 */
/* add-long v0, v2, v4 */
/* .line 167 */
} // :cond_3
int v2 = 5; // const/4 v2, 0x5
v2 = (( com.android.server.alarm.Alarm ) p1 ).setPolicyElapsed ( v2, v0, v1 ); // invoke-virtual {p1, v2, v0, v1}, Lcom/android/server/alarm/Alarm;->setPolicyElapsed(IJ)Z
/* .line 156 */
} // .end local v0 # "temp":J
} // :cond_4
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void updateAlarmPendingState ( ) {
/* .locals 4 */
/* .line 200 */
v0 = this.mAlarmService;
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 201 */
v0 = this.mAlarmService;
v0 = this.mHandler;
v1 = this.mAdjustTask;
(( com.android.server.alarm.AlarmManagerService$AlarmHandler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/alarm/AlarmManagerService$AlarmHandler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 202 */
v0 = this.mAlarmService;
v0 = this.mHandler;
v1 = this.mAdjustTask;
/* const-wide/32 v2, 0xea60 */
(( com.android.server.alarm.AlarmManagerService$AlarmHandler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/alarm/AlarmManagerService$AlarmHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 204 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public Boolean alignAlarmLocked ( com.android.server.alarm.Alarm p0 ) {
/* .locals 21 */
/* .param p1, "a" # Lcom/android/server/alarm/Alarm; */
/* .line 94 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
/* .line 95 */
/* .local v2, "now":J */
/* invoke-virtual/range {p1 ..p1}, Lcom/android/server/alarm/Alarm;->getRequestedElapsed()J */
/* move-result-wide v4 */
/* .line 96 */
/* .local v4, "requestedElapsed":J */
/* iget-boolean v6, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mScreenOn:Z */
int v7 = 0; // const/4 v7, 0x0
/* if-nez v6, :cond_13 */
/* sub-long v8, v4, v2 */
/* const-wide/32 v10, 0xea60 */
/* cmp-long v6, v8, v10 */
/* const-wide/16 v8, 0x0 */
/* if-gtz v6, :cond_1 */
/* iget-wide v10, v1, Lcom/android/server/alarm/Alarm;->windowLength:J */
/* cmp-long v6, v10, v8 */
if ( v6 != null) { // if-eqz v6, :cond_0
} // :cond_0
/* move-wide v15, v2 */
/* goto/16 :goto_6 */
} // :cond_1
} // :goto_0
/* iget v6, v1, Lcom/android/server/alarm/Alarm;->flags:I */
/* and-int/lit8 v6, v6, 0x10 */
/* if-nez v6, :cond_13 */
v6 = this.ADJUST_WHITE_LIST;
v10 = this.sourcePackage;
v6 = /* .line 98 */
/* if-nez v6, :cond_12 */
/* .line 99 */
com.miui.whetstone.PowerKeeperPolicy .getInstance ( );
/* iget v10, v1, Lcom/android/server/alarm/Alarm;->uid:I */
v11 = this.statsTag;
v6 = (( com.miui.whetstone.PowerKeeperPolicy ) v6 ).isAlarmAlignEnable ( v10, v11 ); // invoke-virtual {v6, v10, v11}, Lcom/miui/whetstone/PowerKeeperPolicy;->isAlarmAlignEnable(ILjava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_11
/* .line 100 */
v6 = /* invoke-static/range {p1 ..p1}, Lcom/android/server/alarm/AlarmManagerService;->isTimeTickAlarm(Lcom/android/server/alarm/Alarm;)Z */
/* if-nez v6, :cond_10 */
v6 = this.alarmClock;
if ( v6 != null) { // if-eqz v6, :cond_2
/* move-wide v15, v2 */
/* goto/16 :goto_6 */
/* .line 104 */
} // :cond_2
/* iget v6, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mXmsfUid:I */
int v10 = -1; // const/4 v10, -0x1
final String v11 = "AlarmManager"; // const-string v11, "AlarmManager"
/* const-wide/16 v14, 0x1 */
/* const-wide/32 v16, 0x493e0 */
/* if-eq v6, v10, :cond_3 */
/* iget v6, v1, Lcom/android/server/alarm/Alarm;->uid:I */
/* iget v10, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mXmsfUid:I */
/* if-ne v6, v10, :cond_6 */
} // :cond_3
v6 = this.statsTag;
final String v10 = "*walarm*:com.xiaomi.push.PING_TIMER"; // const-string v10, "*walarm*:com.xiaomi.push.PING_TIMER"
v6 = (( java.lang.String ) v6 ).startsWith ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 105 */
/* iget v6, v1, Lcom/android/server/alarm/Alarm;->uid:I */
/* iput v6, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mXmsfUid:I */
/* .line 107 */
/* iget-wide v8, v1, Lcom/android/server/alarm/Alarm;->origWhen:J */
/* sub-long/2addr v8, v2 */
/* cmp-long v6, v8, v16 */
/* if-gez v6, :cond_5 */
/* .line 108 */
/* iget-wide v8, v1, Lcom/android/server/alarm/Alarm;->origWhen:J */
/* sub-long/2addr v8, v2 */
/* sub-long/2addr v8, v14 */
/* const-wide/16 v18, 0x1388 */
/* div-long v8, v8, v18 */
/* add-long/2addr v8, v14 */
/* mul-long v8, v8, v18 */
/* .line 109 */
/* .local v8, "alignPeriod":J */
/* iget-wide v14, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J */
/* const-wide/32 v12, 0x395f8 */
/* cmp-long v6, v14, v12 */
/* if-gez v6, :cond_4 */
/* iput-wide v12, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J */
/* .line 110 */
} // :cond_4
/* sub-long v12, v4, v8 */
/* iput-wide v12, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mBaseAlignTime:J */
/* .line 112 */
} // .end local v8 # "alignPeriod":J
} // :cond_5
/* const-wide/32 v8, 0x493e0 */
/* .line 113 */
/* .restart local v8 # "alignPeriod":J */
/* const-wide/16 v12, 0x2 */
/* mul-long v14, v8, v12 */
/* sub-long v12, v4, v14 */
/* iput-wide v12, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mBaseAlignTime:J */
/* .line 115 */
} // :goto_1
/* iget-wide v12, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J */
/* cmp-long v6, v12, v8 */
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 116 */
/* iput-wide v8, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J */
/* .line 117 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "align period changed mCurAlignPeriod="; // const-string v10, "align period changed mCurAlignPeriod="
(( java.lang.StringBuilder ) v6 ).append ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v12, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J */
(( java.lang.StringBuilder ) v6 ).append ( v12, v13 ); // invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v10 = " mBaseAlignTime="; // const-string v10, " mBaseAlignTime="
(( java.lang.StringBuilder ) v6 ).append ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v12, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mBaseAlignTime:J */
(( java.lang.StringBuilder ) v6 ).append ( v12, v13 ); // invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v11,v6 );
/* .line 118 */
/* .line 121 */
} // .end local v8 # "alignPeriod":J
} // :cond_6
/* iget-wide v8, v1, Lcom/android/server/alarm/Alarm;->windowLength:J */
/* const-wide/16 v12, 0x0 */
/* cmp-long v6, v8, v12 */
/* if-lez v6, :cond_7 */
/* .line 122 */
/* iget-wide v8, v1, Lcom/android/server/alarm/Alarm;->windowLength:J */
/* iget-wide v12, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J */
/* div-long/2addr v8, v12 */
/* mul-long/2addr v8, v12 */
/* iput-wide v8, v1, Lcom/android/server/alarm/Alarm;->windowLength:J */
/* .line 124 */
} // :cond_7
/* iget-wide v8, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mBaseAlignTime:J */
/* sub-long v12, v4, v8 */
/* const-wide/16 v14, 0x1 */
/* sub-long/2addr v12, v14 */
/* move-wide/from16 v18, v8 */
/* iget-wide v7, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J */
/* div-long/2addr v12, v7 */
/* add-long/2addr v12, v14 */
/* mul-long/2addr v12, v7 */
/* add-long v12, v12, v18 */
/* .line 125 */
/* .local v12, "temp":J */
/* sub-long v7, v12, v2 */
/* .line 126 */
/* .local v7, "delta":J */
/* sub-long v9, v4, v2 */
/* .line 127 */
/* .local v9, "requestedDelta":J */
/* iget v14, v1, Lcom/android/server/alarm/Alarm;->type:I */
/* if-nez v14, :cond_8 */
int v14 = 1; // const/4 v14, 0x1
} // :cond_8
int v14 = 0; // const/4 v14, 0x0
} // :goto_2
/* iget v6, v1, Lcom/android/server/alarm/Alarm;->type:I */
int v15 = 2; // const/4 v15, 0x2
/* if-ne v6, v15, :cond_9 */
int v6 = 1; // const/4 v6, 0x1
} // :cond_9
int v6 = 0; // const/4 v6, 0x0
} // :goto_3
/* or-int/2addr v6, v14 */
/* .line 128 */
/* .local v6, "isWakeup":Z */
/* iget-wide v14, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J */
/* cmp-long v19, v14, v16 */
/* if-gez v19, :cond_a */
/* cmp-long v19, v9, v16 */
/* if-gez v19, :cond_a */
/* cmp-long v16, v7, v16 */
/* if-gtz v16, :cond_c */
} // :cond_a
/* cmp-long v16, v9, v14 */
/* if-gez v16, :cond_b */
/* const-wide/16 v16, 0x3 */
/* mul-long v16, v16, v14 */
/* const-wide/16 v19, 0x2 */
/* div-long v16, v16, v19 */
/* cmp-long v16, v7, v16 */
/* if-gtz v16, :cond_c */
} // :cond_b
/* const-wide/16 v19, 0x2 */
} // :goto_4
/* mul-long v16, v14, v19 */
/* cmp-long v16, v9, v16 */
/* if-gez v16, :cond_d */
/* mul-long v14, v14, v19 */
/* cmp-long v14, v7, v14 */
/* if-lez v14, :cond_d */
} // :cond_c
/* iget-wide v14, v1, Lcom/android/server/alarm/Alarm;->windowLength:J */
/* const-wide/16 v16, 0x0 */
/* cmp-long v14, v14, v16 */
/* if-nez v14, :cond_d */
if ( v6 != null) { // if-eqz v6, :cond_d
/* .line 132 */
/* iget-wide v14, v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mCurAlignPeriod:J */
/* sub-long/2addr v12, v14 */
/* .line 134 */
} // :cond_d
/* cmp-long v14, v12, v4 */
if ( v14 != null) { // if-eqz v14, :cond_f
/* .line 135 */
/* sget-boolean v14, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->DEBUG:Z */
if ( v14 != null) { // if-eqz v14, :cond_e
/* .line 136 */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "alignAlarms: tag="; // const-string v15, "alignAlarms: tag="
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v15 = this.statsTag;
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = " whenElapsed="; // const-string v15, " whenElapsed="
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-wide v15, v2 */
} // .end local v2 # "now":J
/* .local v15, "now":J */
/* invoke-virtual/range {p1 ..p1}, Lcom/android/server/alarm/Alarm;->getWhenElapsed()J */
/* move-result-wide v2 */
(( java.lang.StringBuilder ) v14 ).append ( v2, v3 ); // invoke-virtual {v14, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = " aligntime="; // const-string v3, " aligntime="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v12, v13 ); // invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v11,v2 );
/* .line 135 */
} // .end local v15 # "now":J
/* .restart local v2 # "now":J */
} // :cond_e
/* move-wide v15, v2 */
/* .line 137 */
} // .end local v2 # "now":J
/* .restart local v15 # "now":J */
} // :goto_5
int v2 = 0; // const/4 v2, 0x0
v2 = (( com.android.server.alarm.Alarm ) v1 ).setPolicyElapsed ( v2, v12, v13 ); // invoke-virtual {v1, v2, v12, v13}, Lcom/android/server/alarm/Alarm;->setPolicyElapsed(IJ)Z
/* .line 139 */
} // .end local v15 # "now":J
/* .restart local v2 # "now":J */
} // :cond_f
/* move-wide v15, v2 */
int v2 = 0; // const/4 v2, 0x0
} // .end local v2 # "now":J
/* .restart local v15 # "now":J */
/* .line 100 */
} // .end local v6 # "isWakeup":Z
} // .end local v7 # "delta":J
} // .end local v9 # "requestedDelta":J
} // .end local v12 # "temp":J
} // .end local v15 # "now":J
/* .restart local v2 # "now":J */
} // :cond_10
/* move-wide v15, v2 */
} // .end local v2 # "now":J
/* .restart local v15 # "now":J */
/* .line 99 */
} // .end local v15 # "now":J
/* .restart local v2 # "now":J */
} // :cond_11
/* move-wide v15, v2 */
} // .end local v2 # "now":J
/* .restart local v15 # "now":J */
/* .line 98 */
} // .end local v15 # "now":J
/* .restart local v2 # "now":J */
} // :cond_12
/* move-wide v15, v2 */
} // .end local v2 # "now":J
/* .restart local v15 # "now":J */
/* .line 96 */
} // .end local v15 # "now":J
/* .restart local v2 # "now":J */
} // :cond_13
/* move-wide v15, v2 */
/* .line 102 */
} // .end local v2 # "now":J
/* .restart local v15 # "now":J */
} // :goto_6
int v2 = 0; // const/4 v2, 0x0
} // .end method
public Boolean checkAlarmIsAllowedSend ( android.content.Context p0, com.android.server.alarm.Alarm p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "alarm" # Lcom/android/server/alarm/Alarm; */
/* .line 143 */
if ( p2 != null) { // if-eqz p2, :cond_1
v0 = this.operation;
/* if-nez v0, :cond_0 */
/* .line 144 */
} // :cond_0
v0 = android.os.Binder .getCallingPid ( );
v1 = this.operation;
v1 = (( android.app.PendingIntent ) v1 ).getCreatorUid ( ); // invoke-virtual {v1}, Landroid/app/PendingIntent;->getCreatorUid()I
v2 = this.statsTag;
v3 = this.operation;
/* .line 145 */
v3 = (( android.app.PendingIntent ) v3 ).getCreatorUid ( ); // invoke-virtual {v3}, Landroid/app/PendingIntent;->getCreatorUid()I
v4 = android.os.Binder .getCallingPid ( );
v3 = com.android.server.alarm.AlarmManagerServiceStubImpl .CheckIfAlarmGenralRistrictApply ( v3,v4 );
/* .line 144 */
v0 = com.miui.whetstone.client.WhetstoneClientManager .isAlarmAllowedLocked ( v0,v1,v2,v3 );
/* .line 143 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void checkDeskclockDelivering ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 88 */
if ( p1 != null) { // if-eqz p1, :cond_0
final String v0 = "com.android.deskclock"; // const-string v0, "com.android.deskclock"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 89 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mDeskclockDelivering:Z */
/* .line 91 */
} // :cond_0
return;
} // .end method
public java.lang.String filterPersistPackages ( java.lang.String[] p0 ) {
/* .locals 6 */
/* .param p1, "pkgList" # [Ljava/lang/String; */
/* .line 56 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 57 */
/* .local v0, "filteredPkgList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_1
/* array-length v2, p1 */
/* if-lez v2, :cond_1 */
/* .line 58 */
/* array-length v2, p1 */
/* move v3, v1 */
} // :goto_0
/* if-ge v3, v2, :cond_1 */
/* aget-object v4, p1, v3 */
/* .line 59 */
/* .local v4, "pkg":Ljava/lang/String; */
v5 = v5 = this.PERSIST_PACKAGES;
/* if-nez v5, :cond_0 */
/* .line 60 */
/* .line 58 */
} // .end local v4 # "pkg":Ljava/lang/String;
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 64 */
} // :cond_1
/* new-array v1, v1, [Ljava/lang/String; */
/* check-cast v1, [Ljava/lang/String; */
} // .end method
public void init ( com.android.server.alarm.AlarmManagerService p0 ) {
/* .locals 6 */
/* .param p1, "service" # Lcom/android/server/alarm/AlarmManagerService; */
/* .line 256 */
/* new-instance v0, Ljava/util/ArrayList; */
(( com.android.server.alarm.AlarmManagerService ) p1 ).getContext ( ); // invoke-virtual {p1}, Lcom/android/server/alarm/AlarmManagerService;->getContext()Landroid/content/Context;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const/high16 v2, 0x11030000 */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.ADJUST_WHITE_LIST = v0;
/* .line 257 */
/* new-instance v0, Ljava/util/ArrayList; */
(( com.android.server.alarm.AlarmManagerService ) p1 ).getContext ( ); // invoke-virtual {p1}, Lcom/android/server/alarm/AlarmManagerService;->getContext()Landroid/content/Context;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x11030002 */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.PERSIST_PACKAGES = v0;
/* .line 258 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 259 */
/* new-instance v0, Ljava/util/ArrayList; */
(( com.android.server.alarm.AlarmManagerService ) p1 ).getContext ( ); // invoke-virtual {p1}, Lcom/android/server/alarm/AlarmManagerService;->getContext()Landroid/content/Context;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x11030001 */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 260 */
/* .local v0, "ADJUST_WHITE_LIST_GLOBAL":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/lang/String; */
/* .line 261 */
/* .local v2, "pkgName":Ljava/lang/String; */
v3 = this.ADJUST_WHITE_LIST;
/* .line 262 */
} // .end local v2 # "pkgName":Ljava/lang/String;
/* .line 264 */
} // .end local v0 # "ADJUST_WHITE_LIST_GLOBAL":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_0
this.mAlarmService = p1;
/* .line 265 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 266 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 267 */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 268 */
(( com.android.server.alarm.AlarmManagerService ) p1 ).getContext ( ); // invoke-virtual {p1}, Lcom/android/server/alarm/AlarmManagerService;->getContext()Landroid/content/Context;
/* new-instance v2, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, p0, v3}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver;-><init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Lcom/android/server/alarm/AlarmManagerServiceStubImpl$ScreenReceiver-IA;)V */
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 269 */
/* new-instance v1, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl; */
/* invoke-direct {v1, p0, v3}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;-><init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl-IA;)V */
/* .line 270 */
/* .local v1, "networkCallback":Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl; */
/* new-instance v2, Landroid/net/NetworkRequest$Builder; */
/* invoke-direct {v2}, Landroid/net/NetworkRequest$Builder;-><init>()V */
/* .line 271 */
/* .local v2, "builder":Landroid/net/NetworkRequest$Builder; */
(( android.net.NetworkRequest$Builder ) v2 ).build ( ); // invoke-virtual {v2}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;
/* .line 272 */
/* .local v3, "request":Landroid/net/NetworkRequest; */
(( com.android.server.alarm.AlarmManagerService ) p1 ).getContext ( ); // invoke-virtual {p1}, Lcom/android/server/alarm/AlarmManagerService;->getContext()Landroid/content/Context;
final String v5 = "connectivity"; // const-string v5, "connectivity"
(( android.content.Context ) v4 ).getSystemService ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v4, Landroid/net/ConnectivityManager; */
/* .line 273 */
/* .local v4, "connectivityManager":Landroid/net/ConnectivityManager; */
(( android.net.ConnectivityManager ) v4 ).registerNetworkCallback ( v3, v1 ); // invoke-virtual {v4, v3, v1}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 274 */
return;
} // .end method
public void releaseWakeLock ( com.android.server.alarm.AlarmManagerService$AlarmHandler p0, android.os.PowerManager$WakeLock p1, java.lang.Object p2 ) {
/* .locals 3 */
/* .param p1, "handler" # Lcom/android/server/alarm/AlarmManagerService$AlarmHandler; */
/* .param p2, "wakeLock" # Landroid/os/PowerManager$WakeLock; */
/* .param p3, "aLock" # Ljava/lang/Object; */
/* .line 69 */
/* iget-boolean v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mDeskclockDelivering:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 70 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->mDeskclockDelivering:Z */
/* .line 71 */
final String v0 = "AlarmManager"; // const-string v0, "AlarmManager"
final String v1 = "Delay release wakelock for deskclock."; // const-string v1, "Delay release wakelock for deskclock."
android.util.Log .d ( v0,v1 );
/* .line 72 */
/* new-instance v0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$1; */
/* invoke-direct {v0, p0, p3, p2}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$1;-><init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Ljava/lang/Object;Landroid/os/PowerManager$WakeLock;)V */
/* const-wide/16 v1, 0x12c */
(( com.android.server.alarm.AlarmManagerService$AlarmHandler ) p1 ).postDelayed ( v0, v1, v2 ); // invoke-virtual {p1, v0, v1, v2}, Lcom/android/server/alarm/AlarmManagerService$AlarmHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 82 */
} // :cond_0
(( android.os.PowerManager$WakeLock ) p2 ).release ( ); // invoke-virtual {p2}, Landroid/os/PowerManager$WakeLock;->release()V
/* .line 84 */
} // :goto_0
return;
} // .end method
