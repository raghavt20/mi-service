class com.android.server.alarm.AlarmManagerServiceStubImpl$1 implements java.lang.Runnable {
	 /* .source "AlarmManagerServiceStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->releaseWakeLock(Lcom/android/server/alarm/AlarmManagerService$AlarmHandler;Landroid/os/PowerManager$WakeLock;Ljava/lang/Object;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.alarm.AlarmManagerServiceStubImpl this$0; //synthetic
final java.lang.Object val$aLock; //synthetic
final android.os.PowerManager$WakeLock val$wakeLock; //synthetic
/* # direct methods */
 com.android.server.alarm.AlarmManagerServiceStubImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/alarm/AlarmManagerServiceStubImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 72 */
this.this$0 = p1;
this.val$aLock = p2;
this.val$wakeLock = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 3 */
/* .line 75 */
v0 = this.val$aLock;
/* monitor-enter v0 */
/* .line 76 */
try { // :try_start_0
v1 = this.val$wakeLock;
(( android.os.PowerManager$WakeLock ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V
/* .line 77 */
final String v1 = "AlarmManager"; // const-string v1, "AlarmManager"
final String v2 = "Wakelock for deskclock is released."; // const-string v2, "Wakelock for deskclock is released."
android.util.Log .d ( v1,v2 );
/* .line 78 */
/* monitor-exit v0 */
/* .line 79 */
return;
/* .line 78 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
