.class Lcom/android/server/alarm/AlarmManagerServiceStubImpl$1;
.super Ljava/lang/Object;
.source "AlarmManagerServiceStubImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->releaseWakeLock(Lcom/android/server/alarm/AlarmManagerService$AlarmHandler;Landroid/os/PowerManager$WakeLock;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

.field final synthetic val$aLock:Ljava/lang/Object;

.field final synthetic val$wakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method constructor <init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Ljava/lang/Object;Landroid/os/PowerManager$WakeLock;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/alarm/AlarmManagerServiceStubImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 72
    iput-object p1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$1;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    iput-object p2, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$1;->val$aLock:Ljava/lang/Object;

    iput-object p3, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$1;->val$wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 75
    iget-object v0, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$1;->val$aLock:Ljava/lang/Object;

    monitor-enter v0

    .line 76
    :try_start_0
    iget-object v1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$1;->val$wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 77
    const-string v1, "AlarmManager"

    const-string v2, "Wakelock for deskclock is released."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    monitor-exit v0

    .line 79
    return-void

    .line 78
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
