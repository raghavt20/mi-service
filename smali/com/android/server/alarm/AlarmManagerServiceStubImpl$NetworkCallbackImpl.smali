.class Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "AlarmManagerServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/alarm/AlarmManagerServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NetworkCallbackImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)V
    .locals 0

    .line 226
    iput-object p1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;-><init>(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)V

    return-void
.end method


# virtual methods
.method public onAvailable(Landroid/net/Network;)V
    .locals 4
    .param p1, "network"    # Landroid/net/Network;

    .line 229
    const/4 v0, 0x0

    .line 230
    .local v0, "change":Z
    invoke-static {}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$sfgetsLock()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 231
    :try_start_0
    iget-object v2, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fputmNetAvailable(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Z)V

    .line 232
    iget-object v2, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v2}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fgetmScreenOn(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v2}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fgetmNetAvailable(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :cond_1
    :goto_0
    move v2, v3

    .line 233
    .local v2, "allow":Z
    iget-object v3, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v3}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fgetmPendingAllowed(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Z

    move-result v3

    if-eq v3, v2, :cond_2

    .line 234
    iget-object v3, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v3, v2}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fputmPendingAllowed(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Z)V

    .line 235
    const/4 v0, 0x1

    .line 237
    .end local v2    # "allow":Z
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$mupdateAlarmPendingState(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)V

    .line 239
    :cond_3
    return-void

    .line 237
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 4
    .param p1, "network"    # Landroid/net/Network;

    .line 242
    const/4 v0, 0x0

    .line 243
    .local v0, "change":Z
    invoke-static {}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$sfgetsLock()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 244
    :try_start_0
    iget-object v2, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fputmNetAvailable(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Z)V

    .line 245
    iget-object v2, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v2}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fgetmScreenOn(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v2}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fgetmNetAvailable(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/4 v3, 0x1

    :cond_1
    move v2, v3

    .line 246
    .local v2, "allow":Z
    iget-object v3, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v3}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fgetmPendingAllowed(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)Z

    move-result v3

    if-eq v3, v2, :cond_2

    .line 247
    iget-object v3, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v3, v2}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$fputmPendingAllowed(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;Z)V

    .line 248
    const/4 v0, 0x1

    .line 250
    .end local v2    # "allow":Z
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/android/server/alarm/AlarmManagerServiceStubImpl$NetworkCallbackImpl;->this$0:Lcom/android/server/alarm/AlarmManagerServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/alarm/AlarmManagerServiceStubImpl;->-$$Nest$mupdateAlarmPendingState(Lcom/android/server/alarm/AlarmManagerServiceStubImpl;)V

    .line 252
    :cond_3
    return-void

    .line 250
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method
