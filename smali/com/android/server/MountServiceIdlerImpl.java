class com.android.server.MountServiceIdlerImpl implements com.android.server.MountServiceIdlerStub {
	 /* .source "MountServiceIdlerImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Long FINISH_INTERVAL_TIME;
	 private static final Integer MINIMUM_BATTERY_LEVEL;
	 private static final Long MINIMUM_INTERVAL_TIME;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.content.BroadcastReceiver mScreenOnReceiver;
	 android.os.storage.IStorageManager mSm;
	 private Long sNextTrimDuration;
	 /* # direct methods */
	 com.android.server.MountServiceIdlerImpl ( ) {
		 /* .locals 2 */
		 /* .line 20 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 30 */
		 /* const-wide/32 v0, 0x6ddd00 */
		 /* iput-wide v0, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J */
		 /* .line 34 */
		 /* new-instance v0, Lcom/android/server/MountServiceIdlerImpl$1; */
		 /* invoke-direct {v0, p0}, Lcom/android/server/MountServiceIdlerImpl$1;-><init>(Lcom/android/server/MountServiceIdlerImpl;)V */
		 this.mScreenOnReceiver = v0;
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void addScreenOnFilter ( android.content.Context p0 ) {
		 /* .locals 3 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 93 */
		 /* sget-boolean v0, Landroid/os/Build;->IS_MIUI:Z */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 94 */
			 /* new-instance v0, Landroid/content/IntentFilter; */
			 /* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
			 /* .line 95 */
			 /* .local v0, "screenOnFilter":Landroid/content/IntentFilter; */
			 final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
			 (( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
			 /* .line 96 */
			 v1 = this.mScreenOnReceiver;
			 int v2 = 2; // const/4 v2, 0x2
			 (( android.content.Context ) p1 ).registerReceiver ( v1, v0, v2 ); // invoke-virtual {p1, v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
			 /* .line 98 */
		 } // .end local v0 # "screenOnFilter":Landroid/content/IntentFilter;
	 } // :cond_0
	 return;
} // .end method
public Boolean internalScheduleIdlePass ( android.content.Context p0, Integer p1, android.content.ComponentName p2 ) {
	 /* .locals 5 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "jobId" # I */
	 /* .param p3, "componentName" # Landroid/content/ComponentName; */
	 /* .line 77 */
	 final String v0 = "jobscheduler"; // const-string v0, "jobscheduler"
	 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/app/job/JobScheduler; */
	 /* .line 79 */
	 /* .local v0, "tm":Landroid/app/job/JobScheduler; */
	 /* iget-wide v1, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J */
	 /* const-wide/32 v3, 0x1b7740 */
	 /* cmp-long v1, v1, v3 */
	 /* if-gez v1, :cond_0 */
	 /* .line 80 */
	 /* iput-wide v3, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J */
	 /* .line 83 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "sNextTrimDuration : "; // const-string v2, "sNextTrimDuration : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MountServiceIdlerImpl"; // const-string v2, "MountServiceIdlerImpl"
android.util.Slog .i ( v2,v1 );
/* .line 84 */
/* new-instance v1, Landroid/app/job/JobInfo$Builder; */
/* invoke-direct {v1, p2, p3}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V */
/* .line 85 */
/* .local v1, "builder":Landroid/app/job/JobInfo$Builder; */
/* iget-wide v2, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J */
(( android.app.job.JobInfo$Builder ) v1 ).setMinimumLatency ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;
/* .line 86 */
(( android.app.job.JobInfo$Builder ) v1 ).build ( ); // invoke-virtual {v1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;
(( android.app.job.JobScheduler ) v0 ).schedule ( v2 ); // invoke-virtual {v0, v2}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I
/* .line 88 */
int v2 = 1; // const/4 v2, 0x1
} // .end method
public Boolean runIdleMaint ( android.content.Context p0, Integer p1, android.content.ComponentName p2 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "jobId" # I */
/* .param p3, "componentName" # Landroid/content/ComponentName; */
/* .line 59 */
final String v0 = "batterymanager"; // const-string v0, "batterymanager"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/BatteryManager; */
/* .line 60 */
/* .local v0, "bm":Landroid/os/BatteryManager; */
int v1 = 4; // const/4 v1, 0x4
v1 = (( android.os.BatteryManager ) v0 ).getIntProperty ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/BatteryManager;->getIntProperty(I)I
/* .line 62 */
/* .local v1, "batteryLevel":I */
final String v2 = "power"; // const-string v2, "power"
(( android.content.Context ) p1 ).getSystemService ( v2 ); // invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Landroid/os/PowerManager; */
/* .line 63 */
/* .local v2, "pm":Landroid/os/PowerManager; */
v3 = (( android.os.PowerManager ) v2 ).isInteractive ( ); // invoke-virtual {v2}, Landroid/os/PowerManager;->isInteractive()Z
/* .line 65 */
/* .local v3, "isInteractive":Z */
/* if-nez v3, :cond_0 */
/* const/16 v4, 0xa */
/* if-lt v1, v4, :cond_0 */
/* .line 66 */
/* const-wide/32 v4, 0x6ddd00 */
/* iput-wide v4, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J */
/* .line 67 */
int v4 = 0; // const/4 v4, 0x0
/* .line 69 */
} // :cond_0
/* iget-wide v4, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J */
int v6 = 1; // const/4 v6, 0x1
/* shr-long/2addr v4, v6 */
/* iput-wide v4, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J */
/* .line 70 */
(( com.android.server.MountServiceIdlerImpl ) p0 ).internalScheduleIdlePass ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/MountServiceIdlerImpl;->internalScheduleIdlePass(Landroid/content/Context;ILandroid/content/ComponentName;)Z
/* .line 71 */
} // .end method
