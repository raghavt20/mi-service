class com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler$1 extends android.database.ContentObserver {
	 /* .source "MiuiBatteryIntelligence.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler this$1; //synthetic
/* # direct methods */
 com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler$1 ( ) {
/* .locals 0 */
/* .param p1, "this$1" # Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 231 */
this.this$1 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .line 234 */
v0 = this.this$1;
com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler .-$$Nest$fgetmContentResolver ( v0 );
final String v1 = "pc_low_battery_fast_charge"; // const-string v1, "pc_low_battery_fast_charge"
int v2 = -1; // const/4 v2, -0x1
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
/* .line 235 */
/* .local v0, "state":I */
final String v1 = "MiuiBatteryIntelligence"; // const-string v1, "MiuiBatteryIntelligence"
/* if-ne v0, v2, :cond_0 */
/* .line 236 */
final String v2 = "Failed to get settings"; // const-string v2, "Failed to get settings"
android.util.Slog .e ( v1,v2 );
/* .line 237 */
return;
/* .line 240 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Settings onChange and low-Battery-Charge State = "; // const-string v3, "Settings onChange and low-Battery-Charge State = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 241 */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 242 */
v1 = this.this$1;
final String v2 = "9"; // const-string v2, "9"
com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler .-$$Nest$msetSmartChargeFunction ( v1,v2 );
/* .line 243 */
} // :cond_1
/* if-nez v0, :cond_2 */
/* .line 244 */
v1 = this.this$1;
final String v2 = "8"; // const-string v2, "8"
com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler .-$$Nest$msetSmartChargeFunction ( v1,v2 );
/* .line 246 */
} // :cond_2
} // :goto_0
return;
} // .end method
