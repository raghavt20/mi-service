.class Lcom/android/server/MiuiBatteryServiceImpl$11;
.super Ljava/lang/Object;
.source "MiuiBatteryServiceImpl.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiBatteryServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryServiceImpl;

    .line 444
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$11;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 2
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .line 448
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 450
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$11;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    move-object v1, p2

    check-cast v1, Landroid/bluetooth/BluetoothA2dp;

    invoke-static {v0, v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputmA2dp(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/bluetooth/BluetoothA2dp;)V

    .line 451
    goto :goto_0

    .line 453
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$11;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    move-object v1, p2

    check-cast v1, Landroid/bluetooth/BluetoothHeadset;

    invoke-static {v0, v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputmHeadset(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/bluetooth/BluetoothHeadset;)V

    .line 454
    nop

    .line 458
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onServiceDisconnected(I)V
    .locals 2
    .param p1, "profile"    # I

    .line 463
    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 465
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$11;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputmA2dp(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/bluetooth/BluetoothA2dp;)V

    .line 466
    goto :goto_0

    .line 468
    :pswitch_1
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$11;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputmHeadset(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/bluetooth/BluetoothHeadset;)V

    .line 469
    nop

    .line 473
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
