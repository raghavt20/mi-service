.class Lcom/android/server/FixedFileObserver$1;
.super Landroid/os/FileObserver;
.source "FixedFileObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/FixedFileObserver;->startWatching()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/FixedFileObserver;

.field final synthetic val$fixedObservers:Ljava/util/Set;


# direct methods
.method constructor <init>(Lcom/android/server/FixedFileObserver;Ljava/lang/String;Ljava/util/Set;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/FixedFileObserver;
    .param p2, "path"    # Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lcom/android/server/FixedFileObserver$1;->this$0:Lcom/android/server/FixedFileObserver;

    iput-object p3, p0, Lcom/android/server/FixedFileObserver$1;->val$fixedObservers:Ljava/util/Set;

    invoke-direct {p0, p2}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onEvent(ILjava/lang/String;)V
    .locals 3
    .param p1, "event"    # I
    .param p2, "path"    # Ljava/lang/String;

    .line 35
    iget-object v0, p0, Lcom/android/server/FixedFileObserver$1;->val$fixedObservers:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/FixedFileObserver;

    .line 36
    .local v1, "fixedObserver":Lcom/android/server/FixedFileObserver;
    invoke-static {v1}, Lcom/android/server/FixedFileObserver;->-$$Nest$fgetmMask(Lcom/android/server/FixedFileObserver;)I

    move-result v2

    and-int/2addr v2, p1

    if-eqz v2, :cond_0

    invoke-virtual {v1, p1, p2}, Lcom/android/server/FixedFileObserver;->onEvent(ILjava/lang/String;)V

    goto :goto_0

    .line 37
    .end local v1    # "fixedObserver":Lcom/android/server/FixedFileObserver;
    :cond_1
    return-void
.end method
