.class Lcom/android/server/DarkModeTimeModeManager$2;
.super Landroid/database/ContentObserver;
.source "DarkModeTimeModeManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/DarkModeTimeModeManager;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/DarkModeTimeModeManager;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/server/DarkModeTimeModeManager;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/DarkModeTimeModeManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 102
    iput-object p1, p0, Lcom/android/server/DarkModeTimeModeManager$2;->this$0:Lcom/android/server/DarkModeTimeModeManager;

    iput-object p3, p0, Lcom/android/server/DarkModeTimeModeManager$2;->val$context:Landroid/content/Context;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .line 105
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 106
    sget-boolean v0, Lcom/android/server/DarkModeTimeModeManager;->SUPPORT_DARK_MODE_NOTIFY:Z

    if-eqz v0, :cond_0

    .line 107
    invoke-static {}, Lmiui/hardware/display/DisplayFeatureManager;->getInstance()Lmiui/hardware/display/DisplayFeatureManager;

    move-result-object v0

    .line 108
    iget-object v1, p0, Lcom/android/server/DarkModeTimeModeManager$2;->val$context:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeEnable(Landroid/content/Context;)Z

    move-result v1

    .line 107
    const/16 v2, 0x26

    invoke-virtual {v0, v2, v1}, Lmiui/hardware/display/DisplayFeatureManager;->setScreenEffect(II)V

    .line 110
    :cond_0
    return-void
.end method
