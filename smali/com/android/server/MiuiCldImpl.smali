.class Lcom/android/server/MiuiCldImpl;
.super Lcom/android/server/MiuiCldStub;
.source "MiuiCldImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.MiuiCldStub$$"
.end annotation


# static fields
.field private static final CLD_ERROR:I = 0x2

.field private static final CLD_FREQUENT:I = 0x1

.field private static final CLD_NOT_SUPPORT:I = -0x1

.field private static final CLD_SUCCESS:I = 0x0

.field private static final DEFAULT_MINIMUM_CLD_INTERVAL:J = 0x5265c00L

.field private static final LAST_CLD_FILE:Ljava/lang/String; = "last-cld"

.field private static final MIUI_CLD_PROCESSED_DONE:Ljava/lang/String; = "miui.intent.action.MIUI_CLD_PROCESSED_DONE"

.field private static final TAG:Ljava/lang/String; = "MiuiCldImpl"


# instance fields
.field private final mCldListener:Landroid/os/IVoldTaskListener;

.field private mContext:Landroid/content/Context;

.field private mLastCldFile:Ljava/io/File;

.field private volatile mVold:Landroid/os/IVold;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 25
    invoke-direct {p0}, Lcom/android/server/MiuiCldStub;-><init>()V

    .line 39
    new-instance v0, Lcom/android/server/MiuiCldImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/MiuiCldImpl$1;-><init>(Lcom/android/server/MiuiCldImpl;)V

    iput-object v0, p0, Lcom/android/server/MiuiCldImpl;->mCldListener:Landroid/os/IVoldTaskListener;

    return-void
.end method


# virtual methods
.method public getCldFragLevel()I
    .locals 5

    .line 88
    const-string v0, "MiuiCldImpl"

    :try_start_0
    const-class v1, Landroid/os/IVold;

    const-string v2, "getCldFragLevel"

    const/4 v3, 0x0

    new-array v4, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 89
    .local v1, "method":Ljava/lang/reflect/Method;
    iget-object v2, p0, Lcom/android/server/MiuiCldImpl;->mVold:Landroid/os/IVold;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 90
    .local v2, "fragLevel":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "frag level: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    return v2

    .line 92
    .end local v1    # "method":Ljava/lang/reflect/Method;
    .end local v2    # "fragLevel":I
    :catch_0
    move-exception v1

    .line 93
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v0, v1}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 94
    const/4 v0, -0x1

    return v0
.end method

.method public initCldListener(Landroid/os/IVold;Landroid/content/Context;)V
    .locals 8
    .param p1, "vold"    # Landroid/os/IVold;
    .param p2, "context"    # Landroid/content/Context;

    .line 53
    iput-object p1, p0, Lcom/android/server/MiuiCldImpl;->mVold:Landroid/os/IVold;

    .line 54
    iput-object p2, p0, Lcom/android/server/MiuiCldImpl;->mContext:Landroid/content/Context;

    .line 55
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    .line 56
    .local v0, "dataDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string/jumbo v2, "system"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 57
    .local v1, "systemDir":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string v3, "last-cld"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/MiuiCldImpl;->mLastCldFile:Ljava/io/File;

    .line 59
    :try_start_0
    const-class v2, Landroid/os/IVold;

    const-string/jumbo v3, "setCldListener"

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Class;

    const-class v6, Landroid/os/IVoldTaskListener;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 60
    .local v2, "setCldListener":Ljava/lang/reflect/Method;
    iget-object v3, p0, Lcom/android/server/MiuiCldImpl;->mVold:Landroid/os/IVold;

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/server/MiuiCldImpl;->mCldListener:Landroid/os/IVoldTaskListener;

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    nop

    .end local v2    # "setCldListener":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 61
    :catch_0
    move-exception v2

    .line 62
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "MiuiCldImpl"

    const-string v4, "Failed on initCldListener"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 64
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method needPostCldResult()Z
    .locals 5

    .line 99
    const-string v0, "MiuiCldImpl"

    const/4 v1, 0x0

    .line 100
    .local v1, "post":Z
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/android/server/MiuiCldImpl;->mLastCldFile:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    .local v2, "fis":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileInputStream;->read()I

    move-result v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v4, 0x1

    if-ne v4, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    move v1, v4

    .line 102
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 100
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "post":Z
    .end local p0    # "this":Lcom/android/server/MiuiCldImpl;
    :goto_1
    throw v3
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 105
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "post":Z
    .restart local p0    # "this":Lcom/android/server/MiuiCldImpl;
    :catch_0
    move-exception v2

    .line 106
    .local v2, "e":Ljava/io/IOException;
    const-string v3, "Failed reading last-cld"

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 102
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 104
    .local v2, "e":Ljava/io/FileNotFoundException;
    const-string v3, "File last-cld not exist"

    invoke-static {v0, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :goto_2
    nop

    .line 108
    :goto_3
    return v1
.end method

.method postCldResult(Landroid/os/PersistableBundle;)V
    .locals 6
    .param p1, "extras"    # Landroid/os/PersistableBundle;

    .line 113
    const/4 v0, -0x1

    const-string v1, "frag_level"

    invoke-virtual {p1, v1, v0}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 114
    .local v0, "frag_level":I
    new-instance v2, Landroid/content/Intent;

    const-string v3, "miui.intent.action.MIUI_CLD_PROCESSED_DONE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 115
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 116
    iget-object v1, p0, Lcom/android/server/MiuiCldImpl;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 117
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Broadcast cld processed done complete with level "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "MiuiCldImpl"

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v4, p0, Lcom/android/server/MiuiCldImpl;->mLastCldFile:Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    .local v1, "fos":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    :try_start_1
    invoke-virtual {v1, v4}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 122
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 124
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 120
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v4

    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v5

    :try_start_4
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "frag_level":I
    .end local v2    # "intent":Landroid/content/Intent;
    .end local p0    # "this":Lcom/android/server/MiuiCldImpl;
    .end local p1    # "extras":Landroid/os/PersistableBundle;
    :goto_0
    throw v4
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 122
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "frag_level":I
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local p0    # "this":Lcom/android/server/MiuiCldImpl;
    .restart local p1    # "extras":Landroid/os/PersistableBundle;
    :catch_0
    move-exception v1

    .line 123
    .local v1, "e":Ljava/io/IOException;
    const-string v4, "Failed recording last-cld"

    invoke-static {v3, v4, v1}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 125
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    return-void
.end method

.method public triggerCld()I
    .locals 7

    .line 68
    const/4 v0, 0x2

    .line 69
    .local v0, "result":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/android/server/MiuiCldImpl;->mLastCldFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    sub-long/2addr v1, v3

    .line 70
    .local v1, "timeSinceLast":J
    iget-object v3, p0, Lcom/android/server/MiuiCldImpl;->mLastCldFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    const-string v4, "MiuiCldImpl"

    if-eqz v3, :cond_1

    const-wide/32 v5, 0x5265c00

    cmp-long v3, v1, v5

    if-lez v3, :cond_0

    goto :goto_0

    .line 79
    :cond_0
    const/4 v0, 0x1

    .line 80
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Last cld run in "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-wide/16 v5, 0x3e8

    div-long v5, v1, v5

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "s ago. donot run cld too frequently"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 71
    :cond_1
    :goto_0
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lcom/android/server/MiuiCldImpl;->mLastCldFile:Ljava/io/File;

    invoke-direct {v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    .local v3, "fos":Ljava/io/FileOutputStream;
    const/4 v5, 0x1

    :try_start_1
    invoke-virtual {v3, v5}, Ljava/io/FileOutputStream;->write(I)V

    .line 73
    const/4 v0, 0x0

    .line 74
    const-string v5, "Trigger cld success"

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 71
    :catchall_0
    move-exception v5

    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v6

    :try_start_4
    invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "result":I
    .end local v1    # "timeSinceLast":J
    .end local p0    # "this":Lcom/android/server/MiuiCldImpl;
    :goto_1
    throw v5
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 75
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "result":I
    .restart local v1    # "timeSinceLast":J
    .restart local p0    # "this":Lcom/android/server/MiuiCldImpl;
    :catch_0
    move-exception v3

    .line 76
    .local v3, "e":Ljava/io/IOException;
    const-string v5, "Failed recording last-cld"

    invoke-static {v4, v5, v3}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 77
    .end local v3    # "e":Ljava/io/IOException;
    :goto_2
    nop

    .line 82
    :goto_3
    return v0
.end method
