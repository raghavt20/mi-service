class com.android.server.appcacheopt.AppCacheOptimizer$1 extends android.content.BroadcastReceiver {
	 /* .source "AppCacheOptimizer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/appcacheopt/AppCacheOptimizer;->init()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.appcacheopt.AppCacheOptimizer this$0; //synthetic
/* # direct methods */
 com.android.server.appcacheopt.AppCacheOptimizer$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/appcacheopt/AppCacheOptimizer; */
/* .line 259 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 262 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 263 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.intent.action.PACKAGE_ADDED"; // const-string v1, "android.intent.action.PACKAGE_ADDED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 264 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "package add:"; // const-string v2, "package add:"
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( android.content.Intent ) p2 ).getData ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;
	 (( android.net.Uri ) v2 ).toString ( ); // invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v2 = "APPCacheOptimization"; // const-string v2, "APPCacheOptimization"
	 android.util.Slog .d ( v2,v1 );
	 /* .line 265 */
	 (( android.content.Intent ) p2 ).getData ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;
	 (( android.net.Uri ) v1 ).toString ( ); // invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;
	 final String v2 = ":"; // const-string v2, ":"
	 (( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
	 int v2 = 1; // const/4 v2, 0x1
	 /* aget-object v1, v1, v2 */
	 /* .line 266 */
	 /* .local v1, "addPackageName":Ljava/lang/String; */
	 v2 = this.this$0;
	 com.android.server.appcacheopt.AppCacheOptimizer .-$$Nest$monPackageAdded ( v2,v1 );
	 /* .line 268 */
} // .end local v1 # "addPackageName":Ljava/lang/String;
} // :cond_0
return;
} // .end method
