.class public Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;
.super Ljava/lang/Object;
.source "AppCacheOptimizerConfig.java"


# instance fields
.field private mIsOptimizeEnable:I

.field private mOptimizePath:[Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getIsOptimizeEnable()I
    .locals 1

    .line 23
    iget v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->mIsOptimizeEnable:I

    return v0
.end method

.method public getOptimizePath()[Ljava/lang/String;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->mOptimizePath:[Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public setIsOptimizeEnable(I)V
    .locals 0
    .param p1, "mIsOptimizeEnable"    # I

    .line 27
    iput p1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->mIsOptimizeEnable:I

    .line 28
    return-void
.end method

.method public setOptimizePath([Ljava/lang/String;)V
    .locals 0
    .param p1, "mOptimizePath"    # [Ljava/lang/String;

    .line 35
    iput-object p1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->mOptimizePath:[Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPackageName"    # Ljava/lang/String;

    .line 19
    iput-object p1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->mPackageName:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppConfig{mPackageName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsOptimizeEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->mIsOptimizeEnable:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mOptimizePath="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->mOptimizePath:[Ljava/lang/String;

    .line 43
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 40
    return-object v0
.end method
