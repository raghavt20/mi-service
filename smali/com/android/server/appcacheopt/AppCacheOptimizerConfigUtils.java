public class com.android.server.appcacheopt.AppCacheOptimizerConfigUtils {
	 /* .source "AppCacheOptimizerConfigUtils.java" */
	 /* # static fields */
	 private static final java.lang.String OPTIMIZE_ENABLE;
	 private static final java.lang.String OPTIMIZE_PATH;
	 private static final java.lang.String PACKAGE_NAME;
	 private static final java.lang.String TAG;
	 private static final java.lang.String WATERMARK;
	 /* # direct methods */
	 public com.android.server.appcacheopt.AppCacheOptimizerConfigUtils ( ) {
		 /* .locals 0 */
		 /* .line 13 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static java.util.ArrayList parseAppConfig ( java.lang.String p0 ) {
		 /* .locals 11 */
		 /* .param p0, "configString" # Ljava/lang/String; */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/lang/String;", */
		 /* ")", */
		 /* "Ljava/util/ArrayList<", */
		 /* "Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;", */
		 /* ">;" */
		 /* } */
	 } // .end annotation
	 /* .line 25 */
	 final String v0 = "optimizePath"; // const-string v0, "optimizePath"
	 final String v1 = "isOptimizeEnable"; // const-string v1, "isOptimizeEnable"
	 final String v2 = "packageName"; // const-string v2, "packageName"
	 final String v3 = "apps"; // const-string v3, "apps"
	 v4 = 	 android.text.TextUtils .isEmpty ( p0 );
	 if ( v4 != null) { // if-eqz v4, :cond_0
		 /* .line 26 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 28 */
	 } // :cond_0
	 /* new-instance v4, Ljava/util/ArrayList; */
	 /* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
	 /* .line 30 */
	 /* .local v4, "appConfigs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;>;" */
	 try { // :try_start_0
		 /* new-instance v5, Lorg/json/JSONObject; */
		 /* invoke-direct {v5, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
		 /* .line 31 */
		 /* .local v5, "jsonObject":Lorg/json/JSONObject; */
		 v6 = 		 (( org.json.JSONObject ) v5 ).has ( v3 ); // invoke-virtual {v5, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
		 if ( v6 != null) { // if-eqz v6, :cond_4
			 /* .line 32 */
			 (( org.json.JSONObject ) v5 ).getJSONArray ( v3 ); // invoke-virtual {v5, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
			 /* .line 34 */
			 /* .local v3, "apps":Lorg/json/JSONArray; */
			 int v6 = 0; // const/4 v6, 0x0
			 /* .local v6, "i":I */
		 } // :goto_0
		 v7 = 		 (( org.json.JSONArray ) v3 ).length ( ); // invoke-virtual {v3}, Lorg/json/JSONArray;->length()I
		 /* if-ge v6, v7, :cond_4 */
		 /* .line 35 */
		 (( org.json.JSONArray ) v3 ).getJSONObject ( v6 ); // invoke-virtual {v3, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
		 /* .line 36 */
		 /* .local v7, "appJsonObject":Lorg/json/JSONObject; */
		 /* new-instance v8, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig; */
		 /* invoke-direct {v8}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;-><init>()V */
		 /* .line 38 */
		 /* .local v8, "appConfig":Lcom/android/server/appcacheopt/AppCacheOptimizerConfig; */
		 v9 = 		 (( org.json.JSONObject ) v7 ).has ( v2 ); // invoke-virtual {v7, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
		 if ( v9 != null) { // if-eqz v9, :cond_1
			 /* .line 39 */
			 (( org.json.JSONObject ) v7 ).getString ( v2 ); // invoke-virtual {v7, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
			 /* .line 40 */
			 /* .local v9, "packageName":Ljava/lang/String; */
			 (( com.android.server.appcacheopt.AppCacheOptimizerConfig ) v8 ).setPackageName ( v9 ); // invoke-virtual {v8, v9}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->setPackageName(Ljava/lang/String;)V
			 /* .line 42 */
		 } // .end local v9 # "packageName":Ljava/lang/String;
	 } // :cond_1
	 v9 = 	 (( org.json.JSONObject ) v7 ).has ( v1 ); // invoke-virtual {v7, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
	 if ( v9 != null) { // if-eqz v9, :cond_2
		 /* .line 43 */
		 v9 = 		 (( org.json.JSONObject ) v7 ).getInt ( v1 ); // invoke-virtual {v7, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
		 /* .line 44 */
		 /* .local v9, "isOptimizeEnable":I */
		 (( com.android.server.appcacheopt.AppCacheOptimizerConfig ) v8 ).setIsOptimizeEnable ( v9 ); // invoke-virtual {v8, v9}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->setIsOptimizeEnable(I)V
		 /* .line 46 */
	 } // .end local v9 # "isOptimizeEnable":I
} // :cond_2
v9 = (( org.json.JSONObject ) v7 ).has ( v0 ); // invoke-virtual {v7, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v9 != null) { // if-eqz v9, :cond_3
	 /* .line 47 */
	 (( org.json.JSONObject ) v7 ).getString ( v0 ); // invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
	 final String v10 = ";"; // const-string v10, ";"
	 (( java.lang.String ) v9 ).split ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
	 /* .line 48 */
	 /* .local v9, "optimizePath":[Ljava/lang/String; */
	 (( com.android.server.appcacheopt.AppCacheOptimizerConfig ) v8 ).setOptimizePath ( v9 ); // invoke-virtual {v8, v9}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->setOptimizePath([Ljava/lang/String;)V
	 /* .line 51 */
} // .end local v9 # "optimizePath":[Ljava/lang/String;
} // :cond_3
(( java.util.ArrayList ) v4 ).add ( v8 ); // invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 34 */
/* nop */
} // .end local v7 # "appJsonObject":Lorg/json/JSONObject;
} // .end local v8 # "appConfig":Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;
/* add-int/lit8 v6, v6, 0x1 */
/* .line 57 */
} // .end local v3 # "apps":Lorg/json/JSONArray;
} // .end local v5 # "jsonObject":Lorg/json/JSONObject;
} // .end local v6 # "i":I
} // :cond_4
/* .line 55 */
/* :catch_0 */
/* move-exception v0 */
/* .line 56 */
/* .local v0, "e":Lorg/json/JSONException; */
final String v1 = "AppConfigUtils"; // const-string v1, "AppConfigUtils"
(( org.json.JSONException ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v2 );
/* .line 58 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_1
} // .end method
public static Long parseWatermarkConfig ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "configString" # Ljava/lang/String; */
/* .line 62 */
v0 = android.text.TextUtils .isEmpty ( p0 );
/* const-wide/16 v1, -0x1 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 63 */
/* return-wide v1 */
/* .line 66 */
} // :cond_0
try { // :try_start_0
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 67 */
/* .local v0, "object":Lorg/json/JSONObject; */
/* const-string/jumbo v3, "watermark" */
(( org.json.JSONObject ) v0 ).optLong ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J
/* move-result-wide v1 */
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* return-wide v1 */
/* .line 68 */
} // .end local v0 # "object":Lorg/json/JSONObject;
/* :catch_0 */
/* move-exception v0 */
/* .line 69 */
/* .local v0, "e":Lorg/json/JSONException; */
/* return-wide v1 */
} // .end method
