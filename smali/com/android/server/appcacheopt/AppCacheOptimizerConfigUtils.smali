.class public Lcom/android/server/appcacheopt/AppCacheOptimizerConfigUtils;
.super Ljava/lang/Object;
.source "AppCacheOptimizerConfigUtils.java"


# static fields
.field private static final OPTIMIZE_ENABLE:Ljava/lang/String; = "isOptimizeEnable"

.field private static final OPTIMIZE_PATH:Ljava/lang/String; = "optimizePath"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field private static final TAG:Ljava/lang/String; = "AppConfigUtils"

.field private static final WATERMARK:Ljava/lang/String; = "watermark"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parseAppConfig(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 11
    .param p0, "configString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;",
            ">;"
        }
    .end annotation

    .line 25
    const-string v0, "optimizePath"

    const-string v1, "isOptimizeEnable"

    const-string v2, "packageName"

    const-string v3, "apps"

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 26
    const/4 v0, 0x0

    return-object v0

    .line 28
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 30
    .local v4, "appConfigs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;>;"
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 31
    .local v5, "jsonObject":Lorg/json/JSONObject;
    invoke-virtual {v5, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 32
    invoke-virtual {v5, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 34
    .local v3, "apps":Lorg/json/JSONArray;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v6, v7, :cond_4

    .line 35
    invoke-virtual {v3, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 36
    .local v7, "appJsonObject":Lorg/json/JSONObject;
    new-instance v8, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;

    invoke-direct {v8}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;-><init>()V

    .line 38
    .local v8, "appConfig":Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;
    invoke-virtual {v7, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 39
    invoke-virtual {v7, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 40
    .local v9, "packageName":Ljava/lang/String;
    invoke-virtual {v8, v9}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->setPackageName(Ljava/lang/String;)V

    .line 42
    .end local v9    # "packageName":Ljava/lang/String;
    :cond_1
    invoke-virtual {v7, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 43
    invoke-virtual {v7, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 44
    .local v9, "isOptimizeEnable":I
    invoke-virtual {v8, v9}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->setIsOptimizeEnable(I)V

    .line 46
    .end local v9    # "isOptimizeEnable":I
    :cond_2
    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 47
    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, ";"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 48
    .local v9, "optimizePath":[Ljava/lang/String;
    invoke-virtual {v8, v9}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->setOptimizePath([Ljava/lang/String;)V

    .line 51
    .end local v9    # "optimizePath":[Ljava/lang/String;
    :cond_3
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    nop

    .end local v7    # "appJsonObject":Lorg/json/JSONObject;
    .end local v8    # "appConfig":Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 57
    .end local v3    # "apps":Lorg/json/JSONArray;
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    .end local v6    # "i":I
    :cond_4
    goto :goto_1

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Lorg/json/JSONException;
    const-string v1, "AppConfigUtils"

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_1
    return-object v4
.end method

.method public static parseWatermarkConfig(Ljava/lang/String;)J
    .locals 4
    .param p0, "configString"    # Ljava/lang/String;

    .line 62
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-wide/16 v1, -0x1

    if-eqz v0, :cond_0

    .line 63
    return-wide v1

    .line 66
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 67
    .local v0, "object":Lorg/json/JSONObject;
    const-string/jumbo v3, "watermark"

    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v1

    .line 68
    .end local v0    # "object":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 69
    .local v0, "e":Lorg/json/JSONException;
    return-wide v1
.end method
