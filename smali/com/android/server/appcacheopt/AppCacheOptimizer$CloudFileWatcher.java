class com.android.server.appcacheopt.AppCacheOptimizer$CloudFileWatcher extends android.os.FileObserver {
	 /* .source "AppCacheOptimizer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/appcacheopt/AppCacheOptimizer; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "CloudFileWatcher" */
} // .end annotation
/* # instance fields */
final com.android.server.appcacheopt.AppCacheOptimizer this$0; //synthetic
/* # direct methods */
public com.android.server.appcacheopt.AppCacheOptimizer$CloudFileWatcher ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/appcacheopt/AppCacheOptimizer; */
/* .line 652 */
this.this$0 = p1;
/* .line 653 */
com.android.server.appcacheopt.AppCacheOptimizer .-$$Nest$sfgetCLOUD_CONFIG_FILE_FOLD ( );
/* const/16 v1, 0xfff */
/* invoke-direct {p0, v0, v1}, Landroid/os/FileObserver;-><init>(Ljava/io/File;I)V */
/* .line 654 */
return;
} // .end method
/* # virtual methods */
public void onEvent ( Integer p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "event" # I */
/* .param p2, "path" # Ljava/lang/String; */
/* .line 658 */
final String v0 = "APPCacheOptimization"; // const-string v0, "APPCacheOptimization"
/* sparse-switch p1, :sswitch_data_0 */
/* .line 663 */
/* :sswitch_0 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "DELETE:512 path:"; // const-string v2, "DELETE:512 path:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 664 */
/* .line 660 */
/* :sswitch_1 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "CREATE:256 path:"; // const-string v2, "CREATE:256 path:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 661 */
/* .line 666 */
/* :sswitch_2 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "MODIFY:2 path:"; // const-string v2, "MODIFY:2 path:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 667 */
v0 = this.this$0;
com.android.server.appcacheopt.AppCacheOptimizer .-$$Nest$fgetmParseMountDirs ( v0 );
(( android.util.ArrayMap ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V
/* .line 668 */
v0 = this.this$0;
com.android.server.appcacheopt.AppCacheOptimizer .-$$Nest$fgetmParseUnmountDirs ( v0 );
(( android.util.ArrayMap ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V
/* .line 669 */
v0 = this.this$0;
(( com.android.server.appcacheopt.AppCacheOptimizer ) v0 ).loadDefaultConfigData ( ); // invoke-virtual {v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->loadDefaultConfigData()V
/* .line 670 */
/* nop */
/* .line 674 */
} // :goto_0
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x2 -> :sswitch_2 */
/* 0x100 -> :sswitch_1 */
/* 0x200 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
