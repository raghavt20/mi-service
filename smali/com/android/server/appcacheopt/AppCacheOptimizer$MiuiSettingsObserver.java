class com.android.server.appcacheopt.AppCacheOptimizer$MiuiSettingsObserver extends android.database.ContentObserver {
	 /* .source "AppCacheOptimizer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/appcacheopt/AppCacheOptimizer; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MiuiSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.appcacheopt.AppCacheOptimizer this$0; //synthetic
/* # direct methods */
public com.android.server.appcacheopt.AppCacheOptimizer$MiuiSettingsObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 775 */
this.this$0 = p1;
/* .line 776 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 777 */
return;
} // .end method
/* # virtual methods */
void observe ( ) {
/* .locals 4 */
/* .line 780 */
v0 = this.this$0;
com.android.server.appcacheopt.AppCacheOptimizer .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 781 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
/* nop */
/* .line 782 */
final String v1 = "miui_app_cache_optimization"; // const-string v1, "miui_app_cache_optimization"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 781 */
int v2 = 0; // const/4 v2, 0x0
int v3 = -2; // const/4 v3, -0x2
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 784 */
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 788 */
v0 = this.this$0;
(( com.android.server.appcacheopt.AppCacheOptimizer ) v0 ).updateAppCacheFeatureFromSettings ( ); // invoke-virtual {v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updateAppCacheFeatureFromSettings()V
/* .line 790 */
return;
} // .end method
