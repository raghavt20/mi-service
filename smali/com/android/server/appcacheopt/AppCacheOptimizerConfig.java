public class com.android.server.appcacheopt.AppCacheOptimizerConfig {
	 /* .source "AppCacheOptimizerConfig.java" */
	 /* # instance fields */
	 private Integer mIsOptimizeEnable;
	 private java.lang.String mOptimizePath;
	 private java.lang.String mPackageName;
	 /* # direct methods */
	 public com.android.server.appcacheopt.AppCacheOptimizerConfig ( ) {
		 /* .locals 0 */
		 /* .line 6 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Integer getIsOptimizeEnable ( ) {
		 /* .locals 1 */
		 /* .line 23 */
		 /* iget v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->mIsOptimizeEnable:I */
	 } // .end method
	 public java.lang.String getOptimizePath ( ) {
		 /* .locals 1 */
		 /* .line 31 */
		 v0 = this.mOptimizePath;
	 } // .end method
	 public java.lang.String getPackageName ( ) {
		 /* .locals 1 */
		 /* .line 15 */
		 v0 = this.mPackageName;
	 } // .end method
	 public void setIsOptimizeEnable ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "mIsOptimizeEnable" # I */
		 /* .line 27 */
		 /* iput p1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->mIsOptimizeEnable:I */
		 /* .line 28 */
		 return;
	 } // .end method
	 public void setOptimizePath ( java.lang.String[] p0 ) {
		 /* .locals 0 */
		 /* .param p1, "mOptimizePath" # [Ljava/lang/String; */
		 /* .line 35 */
		 this.mOptimizePath = p1;
		 /* .line 36 */
		 return;
	 } // .end method
	 public void setPackageName ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "mPackageName" # Ljava/lang/String; */
		 /* .line 19 */
		 this.mPackageName = p1;
		 /* .line 20 */
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 2 */
		 /* .line 40 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "AppConfig{mPackageName=\'"; // const-string v1, "AppConfig{mPackageName=\'"
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v1 = this.mPackageName;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x27 */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 final String v1 = ", mIsOptimizeEnable="; // const-string v1, ", mIsOptimizeEnable="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->mIsOptimizeEnable:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", mOptimizePath="; // const-string v1, ", mOptimizePath="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v1 = this.mOptimizePath;
		 /* .line 43 */
		 java.util.Arrays .toString ( v1 );
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x7d */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* .line 40 */
	 } // .end method
