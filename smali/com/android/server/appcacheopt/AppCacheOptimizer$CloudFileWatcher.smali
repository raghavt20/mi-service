.class Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher;
.super Landroid/os/FileObserver;
.source "AppCacheOptimizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/appcacheopt/AppCacheOptimizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CloudFileWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/appcacheopt/AppCacheOptimizer;


# direct methods
.method public constructor <init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/appcacheopt/AppCacheOptimizer;

    .line 652
    iput-object p1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher;->this$0:Lcom/android/server/appcacheopt/AppCacheOptimizer;

    .line 653
    invoke-static {}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->-$$Nest$sfgetCLOUD_CONFIG_FILE_FOLD()Ljava/io/File;

    move-result-object v0

    const/16 v1, 0xfff

    invoke-direct {p0, v0, v1}, Landroid/os/FileObserver;-><init>(Ljava/io/File;I)V

    .line 654
    return-void
.end method


# virtual methods
.method public onEvent(ILjava/lang/String;)V
    .locals 3
    .param p1, "event"    # I
    .param p2, "path"    # Ljava/lang/String;

    .line 658
    const-string v0, "APPCacheOptimization"

    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 663
    :sswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DELETE:512 path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    goto :goto_0

    .line 660
    :sswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CREATE:256 path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    goto :goto_0

    .line 666
    :sswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MODIFY:2 path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher;->this$0:Lcom/android/server/appcacheopt/AppCacheOptimizer;

    invoke-static {v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->-$$Nest$fgetmParseMountDirs(Lcom/android/server/appcacheopt/AppCacheOptimizer;)Landroid/util/ArrayMap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V

    .line 668
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher;->this$0:Lcom/android/server/appcacheopt/AppCacheOptimizer;

    invoke-static {v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->-$$Nest$fgetmParseUnmountDirs(Lcom/android/server/appcacheopt/AppCacheOptimizer;)Landroid/util/ArrayMap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V

    .line 669
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher;->this$0:Lcom/android/server/appcacheopt/AppCacheOptimizer;

    invoke-virtual {v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->loadDefaultConfigData()V

    .line 670
    nop

    .line 674
    :goto_0
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x100 -> :sswitch_1
        0x200 -> :sswitch_0
    .end sparse-switch
.end method
