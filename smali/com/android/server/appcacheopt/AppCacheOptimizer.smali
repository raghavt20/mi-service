.class public Lcom/android/server/appcacheopt/AppCacheOptimizer;
.super Ljava/lang/Object;
.source "AppCacheOptimizer.java"

# interfaces
.implements Lcom/android/server/appcacheopt/AppCacheOptimizerStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher;,
        Lcom/android/server/appcacheopt/AppCacheOptimizer$MiuiSettingsObserver;
    }
.end annotation


# static fields
.field private static final APP_CACHE_OPTIMIZATION_ENABLED:Z

.field private static final APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z

.field private static final CLOUD_CONFIG_FILE_FOLD:Ljava/io/File;

.field private static final CLOUD_CONFIG_FILE_PATH:Ljava/lang/String; = "/data/system/app_cache_optimization/app_cache_optimization_package_path_cfg.json"

.field private static final CONNECT_RETRY_DELAY_MS:J = 0x3e8L

.field private static DEBUG:Z = false

.field private static final DEFAULT_CONFIG_FILE_PATH:Ljava/lang/String; = "/product/etc/app_cache_optimization/app_cache_optimization_package_path_cfg.json"

.field private static final MEM_PRESSURE_REPORT_REFRESH_SECONDS:J = 0x7530L

.field private static final REPORT_MEM_PRESSURE_LIMIT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "APPCacheOptimization"


# instance fields
.field private lastReportMemPressureTime:J

.field private mAppCacheOptimizationEnabled:Z

.field private mAppConfigs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;",
            ">;"
        }
    .end annotation
.end field

.field private mConfigChange:Z

.field private mContext:Landroid/content/Context;

.field private mForeground:Ljava/lang/String;

.field private mInitialized:Z

.field private mInstalld:Landroid/os/IInstalld;

.field private final mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mParseMountDirs:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mParseUnmountDirs:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mVold:Landroid/os/IVold;

.field private mWatcher:Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher;

.field private mWatermark:J

.field private memPressureStorageUsageWatermark:J

.field private reportMemPressureCnt:I

.field private totalStorageUsage:J


# direct methods
.method public static synthetic $r8$lambda$9fZzZylwwmMFfM529QHUQ9PQJlA(Lcom/android/server/appcacheopt/AppCacheOptimizer;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lambda$updatePathMapOnInstalld$4(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$IPGPeb9PnN7WqEo8J6s9VXYH_xg(Lcom/android/server/appcacheopt/AppCacheOptimizer;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->connectInstalld()Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$JyvOSSaTsWOmeYRYiSy_yPeJ2ZE(Lcom/android/server/appcacheopt/AppCacheOptimizer;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->connectVold()Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$NO48QPdLV2XvZBpPmUQVlYWTiTE(Lcom/android/server/appcacheopt/AppCacheOptimizer;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lambda$reportMemPressure$0(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$daKLwtw0CJH-NJ4YGDhiFaCPx-A(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lambda$connectInstalld$2()V

    return-void
.end method

.method public static synthetic $r8$lambda$xxTFoYMV0Mr9kP-npcvAgcEW4qE(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lambda$connectVold$3()V

    return-void
.end method

.method public static synthetic $r8$lambda$y6A4M-cwXe7Ie8gmOaNBlJqYTqc(Lcom/android/server/appcacheopt/AppCacheOptimizer;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lambda$reportMemPressure$1(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/appcacheopt/AppCacheOptimizer;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmParseMountDirs(Lcom/android/server/appcacheopt/AppCacheOptimizer;)Landroid/util/ArrayMap;
    .locals 0

    iget-object p0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mParseMountDirs:Landroid/util/ArrayMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmParseUnmountDirs(Lcom/android/server/appcacheopt/AppCacheOptimizer;)Landroid/util/ArrayMap;
    .locals 0

    iget-object p0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mParseUnmountDirs:Landroid/util/ArrayMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$monPackageAdded(Lcom/android/server/appcacheopt/AppCacheOptimizer;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->onPackageAdded(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetCLOUD_CONFIG_FILE_FOLD()Ljava/io/File;
    .locals 1

    sget-object v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->CLOUD_CONFIG_FILE_FOLD:Ljava/io/File;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 59
    const-string/jumbo v0, "sys.debug.appCacheOptimization"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->DEBUG:Z

    .line 66
    new-instance v0, Ljava/io/File;

    const-string v2, "/data/system/app_cache_optimization"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->CLOUD_CONFIG_FILE_FOLD:Ljava/io/File;

    .line 90
    nop

    .line 91
    const-string v0, "persist.sys.performance.appCacheOptimization"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z

    .line 93
    invoke-static {}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->memoryCheck()Z

    move-result v0

    sput-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    .line 71
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mParseMountDirs:Landroid/util/ArrayMap;

    .line 73
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mParseUnmountDirs:Landroid/util/ArrayMap;

    .line 79
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mForeground:Ljava/lang/String;

    .line 80
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->memPressureStorageUsageWatermark:J

    .line 81
    const-wide/16 v0, 0x400

    iput-wide v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mWatermark:J

    .line 82
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lastReportMemPressureTime:J

    .line 83
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->reportMemPressureCnt:I

    .line 84
    iput-wide v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J

    .line 88
    iput-boolean v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mConfigChange:Z

    .line 95
    iput-boolean v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z

    .line 96
    iput-boolean v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInitialized:Z

    return-void
.end method

.method private cloudFileChanage(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "mountPath"    # [Ljava/lang/String;
    .param p3, "path"    # [Ljava/lang/String;

    .line 482
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 484
    .local v0, "mountSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/HashSet;

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 486
    .local v1, "pathSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 489
    .local v2, "changeMountSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const-string v5, "APPCacheOptimization"

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 490
    .local v4, "i":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 491
    const-string/jumbo v6, "umount skip, because already in mMountDirs"

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 493
    :cond_0
    filled-new-array {v4}, [Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, p1, v6}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->umount(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 494
    const-string/jumbo v6, "umount single dir success"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    invoke-virtual {v2, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 497
    :cond_1
    const-string/jumbo v6, "umount single dir fail"

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    .end local v4    # "i":Ljava/lang/String;
    :goto_1
    goto :goto_0

    .line 503
    :cond_2
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 504
    .restart local v4    # "i":Ljava/lang/String;
    invoke-virtual {v0, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 505
    const-string v6, "mount skip, because already in mMountDirs"

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 507
    :cond_3
    filled-new-array {v4}, [Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p1, v6}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mount(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 508
    const-string v6, "mount single dir success"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 511
    :cond_4
    const-string v6, "mount single dir fail"

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    .end local v4    # "i":Ljava/lang/String;
    :goto_3
    goto :goto_2

    .line 516
    :cond_5
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    .line 517
    .local v3, "changePath":[Ljava/lang/String;
    invoke-virtual {v2, v3}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 518
    iget-object v4, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, p1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 519
    invoke-direct {p0, p1, v3}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updatePathMapOnInstalld(Ljava/lang/String;[Ljava/lang/String;)V

    .line 520
    return-void
.end method

.method private connectAndInit()V
    .locals 2

    .line 237
    const-string v0, "APPCacheOptimization"

    const-string v1, "connectAndInit"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->connectInstalld()Z

    .line 239
    invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->connectVold()Z

    .line 240
    return-void
.end method

.method private connectInstalld()Z
    .locals 6

    .line 275
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInstalld:Landroid/os/IInstalld;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 276
    return v1

    .line 278
    :cond_0
    const-string v0, "installd"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 279
    .local v0, "binder":Landroid/os/IBinder;
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 281
    :try_start_0
    new-instance v3, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V

    invoke-interface {v0, v3, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    goto :goto_0

    .line 288
    :catch_0
    move-exception v3

    .line 289
    .local v3, "e":Landroid/os/RemoteException;
    const/4 v0, 0x0

    .line 293
    .end local v3    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_0
    const-string v3, "APPCacheOptimization"

    if-eqz v0, :cond_2

    .line 294
    invoke-static {v0}, Landroid/os/IInstalld$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IInstalld;

    move-result-object v2

    .line 295
    .local v2, "installd":Landroid/os/IInstalld;
    iput-object v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInstalld:Landroid/os/IInstalld;

    .line 296
    .end local v2    # "installd":Landroid/os/IInstalld;
    nop

    .line 301
    const-string v2, "installd connect success"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->init()V

    .line 303
    return v1

    .line 297
    :cond_2
    const-string v1, "installd not found; trying again"

    invoke-static {v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v3, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda1;

    invoke-direct {v3, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 299
    return v2
.end method

.method private connectVold()Z
    .locals 6

    .line 307
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mVold:Landroid/os/IVold;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 308
    return v1

    .line 310
    :cond_0
    const-string/jumbo v0, "vold"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 311
    .local v0, "binder":Landroid/os/IBinder;
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 313
    :try_start_0
    new-instance v3, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda5;

    invoke-direct {v3, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V

    invoke-interface {v0, v3, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    goto :goto_0

    .line 318
    :catch_0
    move-exception v3

    .line 319
    .local v3, "e":Landroid/os/RemoteException;
    const/4 v0, 0x0

    .line 323
    .end local v3    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_0
    const-string v3, "APPCacheOptimization"

    if-eqz v0, :cond_2

    .line 324
    invoke-static {v0}, Landroid/os/IVold$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IVold;

    move-result-object v2

    .line 325
    .local v2, "vold":Landroid/os/IVold;
    iput-object v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mVold:Landroid/os/IVold;

    .line 326
    .end local v2    # "vold":Landroid/os/IVold;
    nop

    .line 332
    const-string/jumbo v2, "vold connect success"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->init()V

    .line 334
    return v1

    .line 328
    :cond_2
    const-string/jumbo v1, "vold not found; trying again"

    invoke-static {v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v3, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda6;

    invoke-direct {v3, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 330
    return v2
.end method

.method private getAbsolutePath(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "path"    # [Ljava/lang/String;

    .line 338
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 339
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/data/data/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p2, v0

    .line 338
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 341
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private init()V
    .locals 3

    .line 243
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInstalld:Landroid/os/IInstalld;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mVold:Landroid/os/IVold;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInitialized:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 246
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInitialized:Z

    .line 247
    const-string v0, "APPCacheOptimization"

    const-string v1, "init"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    new-instance v0, Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher;

    invoke-direct {v0, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V

    iput-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mWatcher:Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher;

    .line 249
    invoke-virtual {v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher;->startWatching()V

    .line 252
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 254
    invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->loadMountDirs()V

    .line 256
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 257
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 258
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 259
    iget-object v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/server/appcacheopt/AppCacheOptimizer$1;

    invoke-direct {v2, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$1;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 271
    invoke-virtual {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->loadDefaultConfigData()V

    .line 272
    return-void

    .line 244
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    :cond_1
    :goto_0
    return-void
.end method

.method private isAppRunning(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .line 678
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 679
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    .line 680
    .local v1, "appProcessInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 681
    return v2

    .line 683
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 684
    .local v4, "appProcess":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v5, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 685
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isAppRunning:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "APPCacheOptimization"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    const/4 v2, 0x1

    return v2

    .line 688
    .end local v4    # "appProcess":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    goto :goto_0

    .line 689
    :cond_2
    return v2
.end method

.method private synthetic lambda$connectInstalld$2()V
    .locals 2

    .line 282
    const-string v0, "APPCacheOptimization"

    const-string v1, "installd died; reconnecting"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInstalld:Landroid/os/IInstalld;

    .line 284
    invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->connectInstalld()Z

    .line 285
    iget-wide v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mWatermark:J

    invoke-direct {p0, v0, v1}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updateWatermark(J)V

    .line 286
    invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updatePathMapOnInstalld()V

    .line 287
    return-void
.end method

.method private synthetic lambda$connectVold$3()V
    .locals 2

    .line 314
    const-string v0, "APPCacheOptimization"

    const-string/jumbo v1, "vold died; reconnecting"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mVold:Landroid/os/IVold;

    .line 316
    invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->connectVold()Z

    .line 317
    return-void
.end method

.method private synthetic lambda$reportMemPressure$0(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5
    .param p1, "k"    # Ljava/lang/String;
    .param p2, "v"    # [Ljava/lang/String;

    .line 179
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mForeground:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p2, v1

    .line 181
    .local v2, "s":Ljava/lang/String;
    const-wide/16 v3, 0x32

    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->releaseCacheStorageByPercent(Ljava/lang/String;Ljava/lang/String;J)V

    .line 180
    .end local v2    # "s":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 184
    :cond_0
    return-void
.end method

.method private synthetic lambda$reportMemPressure$1(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5
    .param p1, "k"    # Ljava/lang/String;
    .param p2, "v"    # [Ljava/lang/String;

    .line 191
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mForeground:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p2, v1

    .line 193
    .local v2, "s":Ljava/lang/String;
    const-wide/16 v3, 0x50

    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->releaseCacheStorageByPercent(Ljava/lang/String;Ljava/lang/String;J)V

    .line 192
    .end local v2    # "s":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 196
    :cond_0
    return-void
.end method

.method private synthetic lambda$updatePathMapOnInstalld$4(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "paths"    # [Ljava/lang/String;

    .line 636
    :try_start_0
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInstalld:Landroid/os/IInstalld;

    invoke-interface {v0, p1, p2}, Landroid/os/IInstalld;->updateAppCacheMountPath(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 639
    goto :goto_0

    .line 637
    :catch_0
    move-exception v0

    .line 638
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "APPCacheOptimization"

    const-string/jumbo v2, "updatePathMapOnInstalld failed!"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 640
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private loadMountDirs()V
    .locals 10

    .line 344
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    .line 346
    .local v0, "mMountFileDirs":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    const-string v1, "/proc/mounts"

    invoke-static {v1}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->readLocalFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 347
    .local v1, "data":Ljava/lang/String;
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 348
    .local v2, "dataSplit":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_2

    .line 349
    aget-object v4, v2, v3

    const-string/jumbo v5, "tmpfs /data_mirror"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    aget-object v4, v2, v3

    const-string v5, "com"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 350
    aget-object v4, v2, v3

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 351
    .local v4, "startIndex":I
    aget-object v5, v2, v3

    const-string v6, " "

    invoke-virtual {v5, v6, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v5

    .line 352
    .local v5, "endIndex":I
    aget-object v6, v2, v3

    invoke-virtual {v6, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 354
    .local v6, "absPath":Ljava/lang/String;
    const/4 v7, 0x0

    const-string v8, "/"

    invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v6, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 356
    .local v7, "packageName":Ljava/lang/String;
    invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 357
    .local v8, "rePath":Ljava/lang/String;
    invoke-virtual {v0, v7}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 358
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 359
    .local v9, "pathArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    invoke-virtual {v0, v7, v9}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    .end local v9    # "pathArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_1

    .line 362
    :cond_0
    invoke-virtual {v0, v7}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/ArrayList;

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    .end local v4    # "startIndex":I
    .end local v5    # "endIndex":I
    .end local v6    # "absPath":Ljava/lang/String;
    .end local v7    # "packageName":Ljava/lang/String;
    .end local v8    # "rePath":Ljava/lang/String;
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 366
    .end local v3    # "i":I
    :cond_2
    invoke-virtual {v0}, Landroid/util/ArrayMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 367
    invoke-virtual {v0}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 368
    .local v4, "key":Ljava/lang/String;
    invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    .line 369
    .local v5, "arr":[Ljava/lang/String;
    invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 370
    invoke-direct {p0, v4, v5}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->getAbsolutePath(Ljava/lang/String;[Ljava/lang/String;)V

    .line 371
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "APPCacheOptimization"

    invoke-static {v7, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    iget-object v6, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "arr":[Ljava/lang/String;
    goto :goto_2

    .line 375
    :cond_3
    return-void
.end method

.method private static memoryCheck()Z
    .locals 12

    .line 795
    const-string v0, "memoryCheck: "

    const-string v1, "APPCacheOptimization"

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    new-instance v4, Lcom/android/internal/util/MemInfoReader;

    invoke-direct {v4}, Lcom/android/internal/util/MemInfoReader;-><init>()V

    .line 796
    .local v4, "memInfo":Lcom/android/internal/util/MemInfoReader;
    invoke-virtual {v4}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V

    .line 797
    invoke-virtual {v4}, Lcom/android/internal/util/MemInfoReader;->getRawInfo()[J

    move-result-object v5

    .line 798
    .local v5, "infos":[J
    aget-wide v6, v5, v3

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    div-long/2addr v6, v8
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 799
    .local v6, "temp":J
    long-to-int v8, v6

    .line 800
    .local v8, "mem_gb":I
    const/16 v9, 0x17

    const-string v10, " GB"

    if-ge v8, v9, :cond_0

    .line 801
    :try_start_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "memoryCheck:false, mem: "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 802
    return v3

    .line 804
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "memoryCheck:true, mem: "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 805
    return v2

    .line 810
    .end local v4    # "memInfo":Lcom/android/internal/util/MemInfoReader;
    .end local v5    # "infos":[J
    .end local v6    # "temp":J
    .end local v8    # "mem_gb":I
    :catch_0
    move-exception v2

    .line 811
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {v1, v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 812
    return v3

    .line 807
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 808
    .local v3, "cce":Ljava/lang/ClassCastException;
    invoke-static {v1, v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 809
    return v2
.end method

.method private mount(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 10
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "path"    # [Ljava/lang/String;

    .line 529
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start mount "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "APPCacheOptimization"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    const/4 v0, 0x0

    .line 532
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 534
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 535
    .local v3, "info":Landroid/content/pm/ApplicationInfo;
    iget-object v4, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInstalld:Landroid/os/IInstalld;

    const-string/jumbo v7, "u:object_r:system_data_file:s0"

    iget v8, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    iget v9, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    move-object v5, p1

    move-object v6, p2

    invoke-interface/range {v4 .. v9}, Landroid/os/IInstalld;->setFileConRecursive(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v4

    move v0, v4

    .line 537
    if-nez v0, :cond_0

    .line 538
    const-string v4, "installd setFileConRecursive fail before mount"

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 540
    :cond_0
    iget-object v4, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mVold:Landroid/os/IVold;

    iget v5, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    iget v6, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-interface {v4, p2, v5, v6}, Landroid/os/IVold;->mountTmpfs([Ljava/lang/String;II)Z

    move-result v4

    if-nez v4, :cond_1

    .line 541
    const-string/jumbo v4, "vold mountTmpfs fail"

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    const/4 v0, 0x0

    .line 546
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInstalld:Landroid/os/IInstalld;

    const-string/jumbo v7, "u:object_r:app_data_file:s0"

    iget v8, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    iget v9, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    move-object v5, p1

    move-object v6, p2

    invoke-interface/range {v4 .. v9}, Landroid/os/IInstalld;->setFileConRecursive(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v4

    if-nez v4, :cond_2

    .line 548
    const-string v4, "installd setFileConRecursive fail after mount"

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 549
    const/4 v0, 0x0

    .line 555
    :cond_2
    goto :goto_1

    .line 553
    .end local v3    # "info":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v3

    .line 554
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, "mount exception"

    invoke-static {v1, v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 551
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 552
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "package:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " doesn\'t exsits!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    nop

    .line 556
    :goto_1
    return v0
.end method

.method private onPackageAdded(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 378
    invoke-direct {p0, p1}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->isAppRunning(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "APPCacheOptimization"

    if-eqz v0, :cond_0

    .line 379
    const-string v0, "app is running."

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mParseMountDirs:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 382
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "add Package:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 384
    const-string v0, "mount app because package add"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mParseMountDirs:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mount(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 386
    const-string v0, "mount success"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mParseMountDirs:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mParseMountDirs:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updatePathMapOnInstalld(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 390
    :cond_1
    const-string v0, "mount fail"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 393
    :cond_2
    const-string v0, "mount skip, because already in mMountDirs"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    :cond_3
    :goto_0
    return-void
.end method

.method private parseData(Ljava/lang/String;)V
    .locals 4
    .param p1, "data"    # Ljava/lang/String;

    .line 523
    invoke-static {p1}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfigUtils;->parseAppConfig(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppConfigs:Ljava/util/ArrayList;

    .line 524
    invoke-static {p1}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfigUtils;->parseWatermarkConfig(Ljava/lang/String;)J

    move-result-wide v0

    .line 525
    .local v0, "tmp":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    iget-wide v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mWatermark:J

    goto :goto_0

    :cond_0
    move-wide v2, v0

    :goto_0
    iput-wide v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mWatermark:J

    .line 526
    return-void
.end method

.method public static readLocalFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "filePath"    # Ljava/lang/String;

    .line 591
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 592
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 593
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    const-string v3, "APPCacheOptimization"

    if-eqz v2, :cond_1

    .line 594
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 595
    .local v2, "inputStream":Ljava/io/InputStream;
    const/16 v4, 0x400

    :try_start_1
    new-array v4, v4, [B

    .line 597
    .local v4, "buffer":[B
    :goto_0
    invoke-virtual {v2, v4}, Ljava/io/InputStream;->read([B)I

    move-result v5

    move v6, v5

    .local v6, "lenth":I
    const/4 v7, -0x1

    if-eq v5, v7, :cond_0

    .line 598
    new-instance v5, Ljava/lang/String;

    const/4 v7, 0x0

    invoke-direct {v5, v4, v7, v6}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 600
    .end local v4    # "buffer":[B
    .end local v6    # "lenth":I
    :cond_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 594
    :catchall_0
    move-exception v4

    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v5

    :try_start_4
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "stringBuilder":Ljava/lang/StringBuilder;
    .end local v1    # "file":Ljava/io/File;
    .end local p0    # "filePath":Ljava/lang/String;
    :goto_1
    throw v4
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 600
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v0    # "stringBuilder":Ljava/lang/StringBuilder;
    .restart local v1    # "file":Ljava/io/File;
    .restart local p0    # "filePath":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 601
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "exception when readLocalFile: "

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 602
    .end local v2    # "e":Ljava/io/IOException;
    :goto_2
    goto :goto_3

    .line 604
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " does not exist!"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    :goto_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private releaseCacheStorageByPercent(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "dataPath"    # Ljava/lang/String;
    .param p3, "percent"    # J

    .line 613
    :try_start_0
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInstalld:Landroid/os/IInstalld;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/os/IInstalld;->releaseAppStorageByPercent(Ljava/lang/String;Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 616
    goto :goto_0

    .line 614
    :catch_0
    move-exception v0

    .line 615
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "APPCacheOptimization"

    const-string v2, "releaseAppStorageByPercent failed!"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 617
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private reportLowMemPressureLimit(I)Z
    .locals 5
    .param p1, "pressureState"    # I

    .line 218
    const/4 v0, 0x1

    if-gt p1, v0, :cond_0

    .line 219
    return v0

    .line 221
    :cond_0
    iget-wide v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J

    iget-wide v3, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->memPressureStorageUsageWatermark:J

    cmp-long v1, v1, v3

    if-gtz v1, :cond_1

    .line 222
    return v0

    .line 224
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lastReportMemPressureTime:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x7530

    cmp-long v1, v1, v3

    const/4 v2, 0x0

    if-ltz v1, :cond_2

    .line 225
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lastReportMemPressureTime:J

    .line 226
    iput v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->reportMemPressureCnt:I

    .line 228
    :cond_2
    iget v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->reportMemPressureCnt:I

    if-lt v1, v0, :cond_3

    .line 229
    return v0

    .line 231
    :cond_3
    add-int/2addr v1, v0

    iput v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->reportMemPressureCnt:I

    .line 232
    return v2
.end method

.method private updatePathMapOnInstalld()V
    .locals 2

    .line 634
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v1, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 641
    return-void
.end method

.method private updatePathMapOnInstalld(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "paths"    # [Ljava/lang/String;

    .line 624
    :try_start_0
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInstalld:Landroid/os/IInstalld;

    invoke-interface {v0, p1, p2}, Landroid/os/IInstalld;->updateAppCacheMountPath(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 627
    goto :goto_0

    .line 625
    :catch_0
    move-exception v0

    .line 626
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "APPCacheOptimization"

    const-string/jumbo v2, "updatePathMapOnInstalld failed!"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 628
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private updateWatermark(J)V
    .locals 3
    .param p1, "newWatermark"    # J

    .line 645
    :try_start_0
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInstalld:Landroid/os/IInstalld;

    invoke-interface {v0, p1, p2}, Landroid/os/IInstalld;->updateWatermark(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 648
    goto :goto_0

    .line 646
    :catch_0
    move-exception v0

    .line 647
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "APPCacheOptimization"

    const-string/jumbo v2, "updateWatermark failed!"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 649
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public beforePackageRemoved(ILjava/lang/String;)V
    .locals 3
    .param p1, "userId"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 121
    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z

    if-eqz v0, :cond_7

    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z

    if-nez v0, :cond_0

    goto :goto_1

    .line 124
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z

    const-string v1, "APPCacheOptimization"

    if-nez v0, :cond_1

    .line 125
    const-string v0, "beforePackageRemoved enabled: false"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    return-void

    .line 128
    :cond_1
    if-eqz p1, :cond_2

    .line 129
    return-void

    .line 131
    :cond_2
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mContext:Landroid/content/Context;

    if-nez v0, :cond_3

    .line 132
    const-string v0, "beforePackageRemoved mContext is null"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    return-void

    .line 135
    :cond_3
    invoke-direct {p0, p2}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->isAppRunning(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 136
    const-string v0, "app is running."

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "beforePackageRemoved :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 140
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p0, p2, v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->umount(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 141
    const-string/jumbo v0, "umount success"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p2, v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updatePathMapOnInstalld(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 145
    :cond_5
    const-string/jumbo v0, "umount fail"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_6
    :goto_0
    return-void

    .line 122
    :cond_7
    :goto_1
    return-void
.end method

.method public loadDefaultConfigData()V
    .locals 12

    .line 399
    const-string v0, "loading cloud config file. file path:/data/system/app_cache_optimization/app_cache_optimization_package_path_cfg.json"

    const-string v1, "APPCacheOptimization"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    const-string v0, "/data/system/app_cache_optimization/app_cache_optimization_package_path_cfg.json"

    invoke-static {v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->readLocalFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 401
    .local v0, "data":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 402
    const-string v2, "loading local config file. file path:/product/etc/app_cache_optimization/app_cache_optimization_package_path_cfg.json"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    const-string v2, "/product/etc/app_cache_optimization/app_cache_optimization_package_path_cfg.json"

    invoke-static {v2}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->readLocalFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 405
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->parseData(Ljava/lang/String;)V

    .line 406
    iget-wide v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mWatermark:J

    invoke-direct {p0, v2, v3}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updateWatermark(J)V

    .line 407
    iget-object v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppConfigs:Ljava/util/ArrayList;

    if-eqz v2, :cond_c

    .line 408
    const-string v2, "config file parse success."

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppConfigs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_b

    .line 410
    iget-object v3, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppConfigs:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;

    invoke-virtual {v3}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 411
    .local v3, "packageName":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppConfigs:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;

    invoke-virtual {v4}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->getOptimizePath()[Ljava/lang/String;

    move-result-object v4

    .line 413
    .local v4, "path":[Ljava/lang/String;
    new-instance v5, Ljava/util/HashSet;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 414
    .local v5, "pathSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v5, v4}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 415
    iget-object v6, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppConfigs:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;

    invoke-virtual {v6}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->getIsOptimizeEnable()I

    move-result v6

    const-string/jumbo v7, "umount success"

    const-string/jumbo v8, "umount fail"

    const-string v9, "app is running."

    const/4 v10, 0x0

    const/4 v11, 0x1

    if-ne v6, v11, :cond_6

    .line 416
    invoke-direct {p0, v3}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->isAppRunning(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 417
    invoke-static {v1, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    :cond_1
    array-length v6, v4

    if-ne v6, v11, :cond_3

    aget-object v6, v4, v10

    const-string v9, ""

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 421
    const-string v6, "path in config file is empty"

    invoke-static {v1, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    iget-object v6, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 424
    iget-object v6, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    invoke-virtual {p0, v3, v6}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->umount(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 425
    invoke-static {v1, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    iget-object v6, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    new-array v6, v10, [Ljava/lang/String;

    invoke-direct {p0, v3, v6}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updatePathMapOnInstalld(Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_1

    .line 429
    :cond_2
    invoke-static {v1, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 434
    :cond_3
    invoke-direct {p0, v3, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->getAbsolutePath(Ljava/lang/String;[Ljava/lang/String;)V

    .line 435
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "    "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v8, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppConfigs:Ljava/util/ArrayList;

    .line 436
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;

    invoke-virtual {v8}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->getIsOptimizeEnable()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 437
    invoke-static {v4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 435
    invoke-static {v1, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    iget-object v6, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mParseMountDirs:Landroid/util/ArrayMap;

    invoke-virtual {v6, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    iget-object v6, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 442
    invoke-direct {p0, v3, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mount(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 443
    const-string v6, "mount success"

    invoke-static {v1, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    iget-object v6, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    invoke-direct {p0, v3, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updatePathMapOnInstalld(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_1

    .line 447
    :cond_4
    const-string v6, "mount fail"

    invoke-static {v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 451
    :cond_5
    iget-object v6, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    invoke-direct {p0, v3, v6, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->cloudFileChanage(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_1

    .line 453
    :cond_6
    iget-object v6, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppConfigs:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;

    invoke-virtual {v6}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->getIsOptimizeEnable()I

    move-result v6

    if-nez v6, :cond_a

    .line 454
    invoke-direct {p0, v3}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->isAppRunning(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 455
    invoke-static {v1, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :cond_7
    invoke-direct {p0, v3, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->getAbsolutePath(Ljava/lang/String;[Ljava/lang/String;)V

    .line 459
    iget-object v6, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mParseUnmountDirs:Landroid/util/ArrayMap;

    invoke-virtual {v6, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    iget-object v6, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 462
    invoke-virtual {p0, v3, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->umount(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 463
    invoke-static {v1, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    iget-object v6, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    new-array v6, v10, [Ljava/lang/String;

    invoke-direct {p0, v3, v6}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updatePathMapOnInstalld(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_1

    .line 467
    :cond_8
    invoke-static {v1, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 470
    :cond_9
    const-string/jumbo v6, "umount fail, because not in mMountDirs"

    invoke-static {v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "path":[Ljava/lang/String;
    .end local v5    # "pathSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_a
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .end local v2    # "i":I
    :cond_b
    goto :goto_2

    .line 475
    :cond_c
    const-string/jumbo v2, "\u914d\u7f6e\u6587\u4ef6\u4e3a\u7a7a"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :goto_2
    return-void
.end method

.method public mountByPackageName(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 733
    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z

    if-nez v0, :cond_0

    goto :goto_1

    .line 736
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z

    if-nez v0, :cond_1

    .line 737
    return-void

    .line 739
    :cond_1
    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->DEBUG:Z

    const-string v1, "APPCacheOptimization"

    if-eqz v0, :cond_2

    .line 740
    const-string/jumbo v0, "start mountByPackageName"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    :cond_2
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->onPackageAdded(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 746
    goto :goto_0

    .line 744
    :catch_0
    move-exception v0

    .line 745
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "mountByPackageName fail!"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 747
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 734
    :cond_3
    :goto_1
    return-void
.end method

.method notifyAppCacheOptimizationEnabled()V
    .locals 2

    .line 765
    iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z

    const-string v1, "APPCacheOptimization"

    if-eqz v0, :cond_0

    .line 766
    const-string v0, "Enable from DeveloperOptions"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 768
    :cond_0
    const-string v0, "Disable from DeveloperOptions"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    :goto_0
    return-void
.end method

.method public onBootCompleted(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 101
    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 105
    :cond_0
    const-string v0, "APPCacheOptimization"

    if-nez p1, :cond_1

    .line 106
    const-string v1, "onBootCompleted context is null"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    return-void

    .line 109
    :cond_1
    iput-object p1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mContext:Landroid/content/Context;

    .line 110
    invoke-virtual {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updateAppCacheFeatureFromSettings()V

    .line 111
    iget-boolean v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z

    if-nez v1, :cond_2

    .line 112
    const-string v1, "onBootCompleted: disable AppCacheOptimization"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    return-void

    .line 115
    :cond_2
    const-string v1, "onBootCompleted: enable AppCacheOptimization"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->connectAndInit()V

    .line 117
    return-void

    .line 102
    :cond_3
    :goto_0
    return-void
.end method

.method public onForegroundActivityChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 152
    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 155
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z

    if-nez v0, :cond_1

    .line 157
    return-void

    .line 159
    :cond_1
    iput-object p1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mForeground:Ljava/lang/String;

    .line 160
    return-void

    .line 153
    :cond_2
    :goto_0
    return-void
.end method

.method public reportMemPressure(I)V
    .locals 9
    .param p1, "pressureState"    # I

    .line 164
    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z

    if-eqz v0, :cond_4

    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z

    if-nez v0, :cond_0

    goto :goto_2

    .line 167
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z

    if-nez v0, :cond_1

    .line 169
    return-void

    .line 171
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->reportLowMemPressureLimit(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 172
    return-void

    .line 175
    :cond_2
    const-wide/16 v0, 0x2

    const-string v2, "APPCacheOptimization"

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 188
    :pswitch_0
    const-string v3, "MEM_PRESSURE_HIGH: release 80% storage of all background APP, 50% storage of mForeground APP"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    iget-object v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v3, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda3;

    invoke-direct {v3, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 197
    iget-object v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mForeground:Ljava/lang/String;

    .line 198
    .local v2, "fg":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    .line 199
    .local v3, "paths":[Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 200
    array-length v4, v3

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_3

    aget-object v6, v3, v5

    .line 201
    .local v6, "path":Ljava/lang/String;
    const-wide/16 v7, 0x32

    invoke-direct {p0, v2, v6, v7, v8}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->releaseCacheStorageByPercent(Ljava/lang/String;Ljava/lang/String;J)V

    .line 200
    .end local v6    # "path":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 204
    :cond_3
    iget-wide v4, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J

    div-long/2addr v4, v0

    iput-wide v4, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J

    .line 205
    goto :goto_1

    .line 177
    .end local v2    # "fg":Ljava/lang/String;
    .end local v3    # "paths":[Ljava/lang/String;
    :pswitch_1
    const-string v3, "MEM_PRESSURE_MEDIUM: release 50% storage of all background APP"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mMountDirs:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v3, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda2;

    invoke-direct {v3, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 185
    iget-wide v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J

    div-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J

    .line 186
    nop

    .line 209
    :goto_1
    return-void

    .line 165
    :cond_4
    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public umount(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 10
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "path"    # [Ljava/lang/String;

    .line 560
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start umount "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "APPCacheOptimization"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    const/4 v0, 0x0

    .line 563
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 565
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 566
    .local v3, "info":Landroid/content/pm/ApplicationInfo;
    iget-object v4, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInstalld:Landroid/os/IInstalld;

    const-string/jumbo v7, "u:object_r:system_data_file:s0"

    iget v8, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    iget v9, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    move-object v5, p1

    move-object v6, p2

    invoke-interface/range {v4 .. v9}, Landroid/os/IInstalld;->setFileConRecursive(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v4

    move v0, v4

    .line 568
    if-nez v0, :cond_0

    .line 569
    const-string v4, "installd setFileConRecursive fail before umount"

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 571
    :cond_0
    iget-object v4, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mVold:Landroid/os/IVold;

    invoke-interface {v4, p2}, Landroid/os/IVold;->umountTmpfs([Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 572
    const-string/jumbo v4, "vold umountTmpfs fail"

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    const/4 v0, 0x0

    .line 577
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInstalld:Landroid/os/IInstalld;

    const-string/jumbo v7, "u:object_r:app_data_file:s0"

    iget v8, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    iget v9, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    move-object v5, p1

    move-object v6, p2

    invoke-interface/range {v4 .. v9}, Landroid/os/IInstalld;->setFileConRecursive(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v4

    if-nez v4, :cond_2

    .line 579
    const-string v4, "installd setFileConRecursive fail after umount"

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 580
    const/4 v0, 0x0

    .line 586
    :cond_2
    goto :goto_1

    .line 584
    .end local v3    # "info":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v3

    .line 585
    .local v3, "e":Ljava/lang/Exception;
    const-string/jumbo v4, "umount exception"

    invoke-static {v1, v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 582
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 583
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "package:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " doesn\'t exsits!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    nop

    .line 587
    :goto_1
    return v0
.end method

.method public umountByPackageName(Ljava/lang/String;I)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 715
    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z

    if-nez v0, :cond_0

    goto :goto_1

    .line 718
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z

    if-nez v0, :cond_1

    .line 719
    return-void

    .line 721
    :cond_1
    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->DEBUG:Z

    const-string v1, "APPCacheOptimization"

    if-eqz v0, :cond_2

    .line 722
    const-string/jumbo v0, "start umountByPackageName"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    :cond_2
    :try_start_0
    invoke-virtual {p0, p2, p1}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->beforePackageRemoved(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 728
    goto :goto_0

    .line 726
    :catch_0
    move-exception v0

    .line 727
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "umountByPackageName fail!"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 729
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 716
    :cond_3
    :goto_1
    return-void
.end method

.method updateAppCacheFeatureFromSettings()V
    .locals 4

    .line 750
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "miui_app_cache_optimization"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 752
    .local v0, "newValue":I
    if-nez v0, :cond_0

    .line 753
    iget-boolean v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z

    if-eqz v1, :cond_1

    .line 754
    iput-boolean v3, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z

    goto :goto_0

    .line 757
    :cond_0
    iget-boolean v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z

    if-nez v1, :cond_1

    .line 758
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z

    .line 761
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->notifyAppCacheOptimizationEnabled()V

    .line 762
    return-void
.end method

.method public updateAppStorageUsageInfo()V
    .locals 4

    .line 694
    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z

    if-eqz v0, :cond_4

    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z

    if-nez v0, :cond_0

    goto :goto_1

    .line 697
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z

    if-nez v0, :cond_1

    .line 698
    return-void

    .line 700
    :cond_1
    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->DEBUG:Z

    const-string v1, "APPCacheOptimization"

    if-eqz v0, :cond_2

    .line 701
    const-string/jumbo v0, "start updateAppStorageUsageInfo"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInstalld:Landroid/os/IInstalld;

    invoke-interface {v0}, Landroid/os/IInstalld;->updateAppStorageUsageInfo()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J

    .line 705
    sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->DEBUG:Z

    if-eqz v0, :cond_3

    .line 706
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "storage usage info updated: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " MB"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 710
    :cond_3
    goto :goto_0

    .line 708
    :catch_0
    move-exception v0

    .line 709
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "updateAppStorageUsageInfo fail!"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 711
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 695
    :cond_4
    :goto_1
    return-void
.end method
