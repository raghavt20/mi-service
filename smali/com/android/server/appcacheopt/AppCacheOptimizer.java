public class com.android.server.appcacheopt.AppCacheOptimizer implements com.android.server.appcacheopt.AppCacheOptimizerStub {
	 /* .source "AppCacheOptimizer.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher;, */
	 /* Lcom/android/server/appcacheopt/AppCacheOptimizer$MiuiSettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean APP_CACHE_OPTIMIZATION_ENABLED;
private static final Boolean APP_CACHE_OPTIMIZATION_MEMORY_CHECK;
private static final java.io.File CLOUD_CONFIG_FILE_FOLD;
private static final java.lang.String CLOUD_CONFIG_FILE_PATH;
private static final Long CONNECT_RETRY_DELAY_MS;
private static Boolean DEBUG;
private static final java.lang.String DEFAULT_CONFIG_FILE_PATH;
private static final Long MEM_PRESSURE_REPORT_REFRESH_SECONDS;
private static final Integer REPORT_MEM_PRESSURE_LIMIT;
private static final java.lang.String TAG;
/* # instance fields */
private Long lastReportMemPressureTime;
private Boolean mAppCacheOptimizationEnabled;
private java.util.ArrayList mAppConfigs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mConfigChange;
private android.content.Context mContext;
private java.lang.String mForeground;
private Boolean mInitialized;
private android.os.IInstalld mInstalld;
private final java.util.concurrent.ConcurrentHashMap mMountDirs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "[", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.ArrayMap mParseMountDirs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "[", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.ArrayMap mParseUnmountDirs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "[", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.os.IVold mVold;
private com.android.server.appcacheopt.AppCacheOptimizer$CloudFileWatcher mWatcher;
private Long mWatermark;
private Long memPressureStorageUsageWatermark;
private Integer reportMemPressureCnt;
private Long totalStorageUsage;
/* # direct methods */
public static void $r8$lambda$9fZzZylwwmMFfM529QHUQ9PQJlA ( com.android.server.appcacheopt.AppCacheOptimizer p0, java.lang.String p1, java.lang.String[] p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lambda$updatePathMapOnInstalld$4(Ljava/lang/String;[Ljava/lang/String;)V */
return;
} // .end method
public static Boolean $r8$lambda$IPGPeb9PnN7WqEo8J6s9VXYH_xg ( com.android.server.appcacheopt.AppCacheOptimizer p0 ) { //synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->connectInstalld()Z */
} // .end method
public static Boolean $r8$lambda$JyvOSSaTsWOmeYRYiSy_yPeJ2ZE ( com.android.server.appcacheopt.AppCacheOptimizer p0 ) { //synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->connectVold()Z */
} // .end method
public static void $r8$lambda$NO48QPdLV2XvZBpPmUQVlYWTiTE ( com.android.server.appcacheopt.AppCacheOptimizer p0, java.lang.String p1, java.lang.String[] p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lambda$reportMemPressure$0(Ljava/lang/String;[Ljava/lang/String;)V */
return;
} // .end method
public static void $r8$lambda$daKLwtw0CJH-NJ4YGDhiFaCPx-A ( com.android.server.appcacheopt.AppCacheOptimizer p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lambda$connectInstalld$2()V */
return;
} // .end method
public static void $r8$lambda$xxTFoYMV0Mr9kP-npcvAgcEW4qE ( com.android.server.appcacheopt.AppCacheOptimizer p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lambda$connectVold$3()V */
return;
} // .end method
public static void $r8$lambda$y6A4M-cwXe7Ie8gmOaNBlJqYTqc ( com.android.server.appcacheopt.AppCacheOptimizer p0, java.lang.String p1, java.lang.String[] p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lambda$reportMemPressure$1(Ljava/lang/String;[Ljava/lang/String;)V */
return;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.appcacheopt.AppCacheOptimizer p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static android.util.ArrayMap -$$Nest$fgetmParseMountDirs ( com.android.server.appcacheopt.AppCacheOptimizer p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mParseMountDirs;
} // .end method
static android.util.ArrayMap -$$Nest$fgetmParseUnmountDirs ( com.android.server.appcacheopt.AppCacheOptimizer p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mParseUnmountDirs;
} // .end method
static void -$$Nest$monPackageAdded ( com.android.server.appcacheopt.AppCacheOptimizer p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->onPackageAdded(Ljava/lang/String;)V */
return;
} // .end method
static java.io.File -$$Nest$sfgetCLOUD_CONFIG_FILE_FOLD ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.appcacheopt.AppCacheOptimizer.CLOUD_CONFIG_FILE_FOLD;
} // .end method
static com.android.server.appcacheopt.AppCacheOptimizer ( ) {
/* .locals 3 */
/* .line 59 */
/* const-string/jumbo v0, "sys.debug.appCacheOptimization" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.appcacheopt.AppCacheOptimizer.DEBUG = (v0!= 0);
/* .line 66 */
/* new-instance v0, Ljava/io/File; */
final String v2 = "/data/system/app_cache_optimization"; // const-string v2, "/data/system/app_cache_optimization"
/* invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 90 */
/* nop */
/* .line 91 */
final String v0 = "persist.sys.performance.appCacheOptimization"; // const-string v0, "persist.sys.performance.appCacheOptimization"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.appcacheopt.AppCacheOptimizer.APP_CACHE_OPTIMIZATION_ENABLED = (v0!= 0);
/* .line 93 */
v0 = com.android.server.appcacheopt.AppCacheOptimizer .memoryCheck ( );
com.android.server.appcacheopt.AppCacheOptimizer.APP_CACHE_OPTIMIZATION_MEMORY_CHECK = (v0!= 0);
return;
} // .end method
public com.android.server.appcacheopt.AppCacheOptimizer ( ) {
/* .locals 3 */
/* .line 57 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 69 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mMountDirs = v0;
/* .line 71 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mParseMountDirs = v0;
/* .line 73 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mParseUnmountDirs = v0;
/* .line 79 */
final String v0 = ""; // const-string v0, ""
this.mForeground = v0;
/* .line 80 */
/* const-wide/16 v0, 0xc8 */
/* iput-wide v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->memPressureStorageUsageWatermark:J */
/* .line 81 */
/* const-wide/16 v0, 0x400 */
/* iput-wide v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mWatermark:J */
/* .line 82 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lastReportMemPressureTime:J */
/* .line 83 */
int v2 = 0; // const/4 v2, 0x0
/* iput v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->reportMemPressureCnt:I */
/* .line 84 */
/* iput-wide v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J */
/* .line 88 */
/* iput-boolean v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mConfigChange:Z */
/* .line 95 */
/* iput-boolean v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z */
/* .line 96 */
/* iput-boolean v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInitialized:Z */
return;
} // .end method
private void cloudFileChanage ( java.lang.String p0, java.lang.String[] p1, java.lang.String[] p2 ) {
/* .locals 7 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "mountPath" # [Ljava/lang/String; */
/* .param p3, "path" # [Ljava/lang/String; */
/* .line 482 */
/* new-instance v0, Ljava/util/HashSet; */
java.util.Arrays .asList ( p2 );
/* invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
/* .line 484 */
/* .local v0, "mountSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
/* new-instance v1, Ljava/util/HashSet; */
java.util.Arrays .asList ( p3 );
/* invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
/* .line 486 */
/* .local v1, "pathSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
/* new-instance v2, Ljava/util/HashSet; */
java.util.Arrays .asList ( p2 );
/* invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
/* .line 489 */
/* .local v2, "changeMountSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
(( java.util.HashSet ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v4 = } // :goto_0
final String v5 = "APPCacheOptimization"; // const-string v5, "APPCacheOptimization"
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Ljava/lang/String; */
/* .line 490 */
/* .local v4, "i":Ljava/lang/String; */
v6 = (( java.util.HashSet ) v1 ).contains ( v4 ); // invoke-virtual {v1, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 491 */
/* const-string/jumbo v6, "umount skip, because already in mMountDirs" */
android.util.Slog .w ( v5,v6 );
/* .line 493 */
} // :cond_0
/* filled-new-array {v4}, [Ljava/lang/String; */
v6 = (( com.android.server.appcacheopt.AppCacheOptimizer ) p0 ).umount ( p1, v6 ); // invoke-virtual {p0, p1, v6}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->umount(Ljava/lang/String;[Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 494 */
/* const-string/jumbo v6, "umount single dir success" */
android.util.Slog .i ( v5,v6 );
/* .line 495 */
(( java.util.HashSet ) v2 ).remove ( v4 ); // invoke-virtual {v2, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
/* .line 497 */
} // :cond_1
/* const-string/jumbo v6, "umount single dir fail" */
android.util.Slog .e ( v5,v6 );
/* .line 500 */
} // .end local v4 # "i":Ljava/lang/String;
} // :goto_1
/* .line 503 */
} // :cond_2
(( java.util.HashSet ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_5
/* check-cast v4, Ljava/lang/String; */
/* .line 504 */
/* .restart local v4 # "i":Ljava/lang/String; */
v6 = (( java.util.HashSet ) v0 ).contains ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 505 */
final String v6 = "mount skip, because already in mMountDirs"; // const-string v6, "mount skip, because already in mMountDirs"
android.util.Slog .w ( v5,v6 );
/* .line 507 */
} // :cond_3
/* filled-new-array {v4}, [Ljava/lang/String; */
v6 = /* invoke-direct {p0, p1, v6}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mount(Ljava/lang/String;[Ljava/lang/String;)Z */
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 508 */
final String v6 = "mount single dir success"; // const-string v6, "mount single dir success"
android.util.Slog .i ( v5,v6 );
/* .line 509 */
(( java.util.HashSet ) v2 ).add ( v4 ); // invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 511 */
} // :cond_4
final String v6 = "mount single dir fail"; // const-string v6, "mount single dir fail"
android.util.Slog .e ( v5,v6 );
/* .line 514 */
} // .end local v4 # "i":Ljava/lang/String;
} // :goto_3
/* .line 516 */
} // :cond_5
v3 = (( java.util.HashSet ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/HashSet;->size()I
/* new-array v3, v3, [Ljava/lang/String; */
/* .line 517 */
/* .local v3, "changePath":[Ljava/lang/String; */
(( java.util.HashSet ) v2 ).toArray ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
/* .line 518 */
v4 = this.mMountDirs;
(( java.util.concurrent.ConcurrentHashMap ) v4 ).put ( p1, v3 ); // invoke-virtual {v4, p1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 519 */
/* invoke-direct {p0, p1, v3}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updatePathMapOnInstalld(Ljava/lang/String;[Ljava/lang/String;)V */
/* .line 520 */
return;
} // .end method
private void connectAndInit ( ) {
/* .locals 2 */
/* .line 237 */
final String v0 = "APPCacheOptimization"; // const-string v0, "APPCacheOptimization"
final String v1 = "connectAndInit"; // const-string v1, "connectAndInit"
android.util.Slog .d ( v0,v1 );
/* .line 238 */
/* invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->connectInstalld()Z */
/* .line 239 */
/* invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->connectVold()Z */
/* .line 240 */
return;
} // .end method
private Boolean connectInstalld ( ) {
/* .locals 6 */
/* .line 275 */
v0 = this.mInstalld;
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 276 */
/* .line 278 */
} // :cond_0
final String v0 = "installd"; // const-string v0, "installd"
android.os.ServiceManager .getService ( v0 );
/* .line 279 */
/* .local v0, "binder":Landroid/os/IBinder; */
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 281 */
try { // :try_start_0
/* new-instance v3, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda0; */
/* invoke-direct {v3, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 290 */
/* .line 288 */
/* :catch_0 */
/* move-exception v3 */
/* .line 289 */
/* .local v3, "e":Landroid/os/RemoteException; */
int v0 = 0; // const/4 v0, 0x0
/* .line 293 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_0
final String v3 = "APPCacheOptimization"; // const-string v3, "APPCacheOptimization"
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 294 */
android.os.IInstalld$Stub .asInterface ( v0 );
/* .line 295 */
/* .local v2, "installd":Landroid/os/IInstalld; */
this.mInstalld = v2;
/* .line 296 */
} // .end local v2 # "installd":Landroid/os/IInstalld;
/* nop */
/* .line 301 */
final String v2 = "installd connect success"; // const-string v2, "installd connect success"
android.util.Slog .i ( v3,v2 );
/* .line 302 */
/* invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->init()V */
/* .line 303 */
/* .line 297 */
} // :cond_2
final String v1 = "installd not found; trying again"; // const-string v1, "installd not found; trying again"
android.util.Slog .w ( v3,v1 );
/* .line 298 */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v3, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda1; */
/* invoke-direct {v3, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V */
/* const-wide/16 v4, 0x3e8 */
(( android.os.Handler ) v1 ).postDelayed ( v3, v4, v5 ); // invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 299 */
} // .end method
private Boolean connectVold ( ) {
/* .locals 6 */
/* .line 307 */
v0 = this.mVold;
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 308 */
/* .line 310 */
} // :cond_0
/* const-string/jumbo v0, "vold" */
android.os.ServiceManager .getService ( v0 );
/* .line 311 */
/* .local v0, "binder":Landroid/os/IBinder; */
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 313 */
try { // :try_start_0
/* new-instance v3, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda5; */
/* invoke-direct {v3, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 320 */
/* .line 318 */
/* :catch_0 */
/* move-exception v3 */
/* .line 319 */
/* .local v3, "e":Landroid/os/RemoteException; */
int v0 = 0; // const/4 v0, 0x0
/* .line 323 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_0
final String v3 = "APPCacheOptimization"; // const-string v3, "APPCacheOptimization"
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 324 */
android.os.IVold$Stub .asInterface ( v0 );
/* .line 325 */
/* .local v2, "vold":Landroid/os/IVold; */
this.mVold = v2;
/* .line 326 */
} // .end local v2 # "vold":Landroid/os/IVold;
/* nop */
/* .line 332 */
/* const-string/jumbo v2, "vold connect success" */
android.util.Slog .i ( v3,v2 );
/* .line 333 */
/* invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->init()V */
/* .line 334 */
/* .line 328 */
} // :cond_2
/* const-string/jumbo v1, "vold not found; trying again" */
android.util.Slog .w ( v3,v1 );
/* .line 329 */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v3, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda6; */
/* invoke-direct {v3, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V */
/* const-wide/16 v4, 0x3e8 */
(( android.os.Handler ) v1 ).postDelayed ( v3, v4, v5 ); // invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 330 */
} // .end method
private void getAbsolutePath ( java.lang.String p0, java.lang.String[] p1 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "path" # [Ljava/lang/String; */
/* .line 338 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* array-length v1, p2 */
/* if-ge v0, v1, :cond_0 */
/* .line 339 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "/data/data/"; // const-string v2, "/data/data/"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "/"; // const-string v2, "/"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v2, p2, v0 */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* aput-object v1, p2, v0 */
/* .line 338 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 341 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
private void init ( ) {
/* .locals 3 */
/* .line 243 */
v0 = this.mInstalld;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mVold;
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInitialized:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 246 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mInitialized:Z */
/* .line 247 */
final String v0 = "APPCacheOptimization"; // const-string v0, "APPCacheOptimization"
final String v1 = "init"; // const-string v1, "init"
android.util.Slog .d ( v0,v1 );
/* .line 248 */
/* new-instance v0, Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher; */
/* invoke-direct {v0, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V */
this.mWatcher = v0;
/* .line 249 */
(( com.android.server.appcacheopt.AppCacheOptimizer$CloudFileWatcher ) v0 ).startWatching ( ); // invoke-virtual {v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$CloudFileWatcher;->startWatching()V
/* .line 252 */
v0 = this.mMountDirs;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
/* .line 254 */
/* invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->loadMountDirs()V */
/* .line 256 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 257 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.PACKAGE_ADDED"; // const-string v1, "android.intent.action.PACKAGE_ADDED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 258 */
final String v1 = "package"; // const-string v1, "package"
(( android.content.IntentFilter ) v0 ).addDataScheme ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V
/* .line 259 */
v1 = this.mContext;
/* new-instance v2, Lcom/android/server/appcacheopt/AppCacheOptimizer$1; */
/* invoke-direct {v2, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$1;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V */
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 271 */
(( com.android.server.appcacheopt.AppCacheOptimizer ) p0 ).loadDefaultConfigData ( ); // invoke-virtual {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->loadDefaultConfigData()V
/* .line 272 */
return;
/* .line 244 */
} // .end local v0 # "intentFilter":Landroid/content/IntentFilter;
} // :cond_1
} // :goto_0
return;
} // .end method
private Boolean isAppRunning ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 678 */
v0 = this.mContext;
final String v1 = "activity"; // const-string v1, "activity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
/* .line 679 */
/* .local v0, "activityManager":Landroid/app/ActivityManager; */
(( android.app.ActivityManager ) v0 ).getRunningAppProcesses ( ); // invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
/* .line 680 */
/* .local v1, "appProcessInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 681 */
/* .line 683 */
} // :cond_0
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* .line 684 */
/* .local v4, "appProcess":Landroid/app/ActivityManager$RunningAppProcessInfo; */
v5 = this.processName;
v5 = (( java.lang.String ) v5 ).equals ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 685 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "isAppRunning:"; // const-string v3, "isAppRunning:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "APPCacheOptimization"; // const-string v3, "APPCacheOptimization"
android.util.Slog .i ( v3,v2 );
/* .line 686 */
int v2 = 1; // const/4 v2, 0x1
/* .line 688 */
} // .end local v4 # "appProcess":Landroid/app/ActivityManager$RunningAppProcessInfo;
} // :cond_1
/* .line 689 */
} // :cond_2
} // .end method
private void lambda$connectInstalld$2 ( ) { //synthethic
/* .locals 2 */
/* .line 282 */
final String v0 = "APPCacheOptimization"; // const-string v0, "APPCacheOptimization"
final String v1 = "installd died; reconnecting"; // const-string v1, "installd died; reconnecting"
android.util.Slog .w ( v0,v1 );
/* .line 283 */
int v0 = 0; // const/4 v0, 0x0
this.mInstalld = v0;
/* .line 284 */
/* invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->connectInstalld()Z */
/* .line 285 */
/* iget-wide v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mWatermark:J */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updateWatermark(J)V */
/* .line 286 */
/* invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updatePathMapOnInstalld()V */
/* .line 287 */
return;
} // .end method
private void lambda$connectVold$3 ( ) { //synthethic
/* .locals 2 */
/* .line 314 */
final String v0 = "APPCacheOptimization"; // const-string v0, "APPCacheOptimization"
/* const-string/jumbo v1, "vold died; reconnecting" */
android.util.Slog .w ( v0,v1 );
/* .line 315 */
int v0 = 0; // const/4 v0, 0x0
this.mVold = v0;
/* .line 316 */
/* invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->connectVold()Z */
/* .line 317 */
return;
} // .end method
private void lambda$reportMemPressure$0 ( java.lang.String p0, java.lang.String[] p1 ) { //synthethic
/* .locals 5 */
/* .param p1, "k" # Ljava/lang/String; */
/* .param p2, "v" # [Ljava/lang/String; */
/* .line 179 */
v0 = this.mForeground;
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 180 */
/* array-length v0, p2 */
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* if-ge v1, v0, :cond_0 */
/* aget-object v2, p2, v1 */
/* .line 181 */
/* .local v2, "s":Ljava/lang/String; */
/* const-wide/16 v3, 0x32 */
/* invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->releaseCacheStorageByPercent(Ljava/lang/String;Ljava/lang/String;J)V */
/* .line 180 */
} // .end local v2 # "s":Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 184 */
} // :cond_0
return;
} // .end method
private void lambda$reportMemPressure$1 ( java.lang.String p0, java.lang.String[] p1 ) { //synthethic
/* .locals 5 */
/* .param p1, "k" # Ljava/lang/String; */
/* .param p2, "v" # [Ljava/lang/String; */
/* .line 191 */
v0 = this.mForeground;
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 192 */
/* array-length v0, p2 */
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* if-ge v1, v0, :cond_0 */
/* aget-object v2, p2, v1 */
/* .line 193 */
/* .local v2, "s":Ljava/lang/String; */
/* const-wide/16 v3, 0x50 */
/* invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->releaseCacheStorageByPercent(Ljava/lang/String;Ljava/lang/String;J)V */
/* .line 192 */
} // .end local v2 # "s":Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 196 */
} // :cond_0
return;
} // .end method
private void lambda$updatePathMapOnInstalld$4 ( java.lang.String p0, java.lang.String[] p1 ) { //synthethic
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "paths" # [Ljava/lang/String; */
/* .line 636 */
try { // :try_start_0
v0 = this.mInstalld;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 639 */
/* .line 637 */
/* :catch_0 */
/* move-exception v0 */
/* .line 638 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "APPCacheOptimization"; // const-string v1, "APPCacheOptimization"
/* const-string/jumbo v2, "updatePathMapOnInstalld failed!" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 640 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void loadMountDirs ( ) {
/* .locals 10 */
/* .line 344 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
/* .line 346 */
/* .local v0, "mMountFileDirs":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;" */
final String v1 = "/proc/mounts"; // const-string v1, "/proc/mounts"
com.android.server.appcacheopt.AppCacheOptimizer .readLocalFile ( v1 );
/* .line 347 */
/* .local v1, "data":Ljava/lang/String; */
final String v2 = "\n"; // const-string v2, "\n"
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 348 */
/* .local v2, "dataSplit":[Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* array-length v4, v2 */
/* if-ge v3, v4, :cond_2 */
/* .line 349 */
/* aget-object v4, v2, v3 */
/* const-string/jumbo v5, "tmpfs /data_mirror" */
v4 = (( java.lang.String ) v4 ).startsWith ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* aget-object v4, v2, v3 */
final String v5 = "com"; // const-string v5, "com"
v4 = (( java.lang.String ) v4 ).contains ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 350 */
/* aget-object v4, v2, v3 */
v4 = (( java.lang.String ) v4 ).indexOf ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* .line 351 */
/* .local v4, "startIndex":I */
/* aget-object v5, v2, v3 */
final String v6 = " "; // const-string v6, " "
v5 = (( java.lang.String ) v5 ).indexOf ( v6, v4 ); // invoke-virtual {v5, v6, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I
/* .line 352 */
/* .local v5, "endIndex":I */
/* aget-object v6, v2, v3 */
(( java.lang.String ) v6 ).substring ( v4, v5 ); // invoke-virtual {v6, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 354 */
/* .local v6, "absPath":Ljava/lang/String; */
int v7 = 0; // const/4 v7, 0x0
final String v8 = "/"; // const-string v8, "/"
v9 = (( java.lang.String ) v6 ).indexOf ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
(( java.lang.String ) v6 ).substring ( v7, v9 ); // invoke-virtual {v6, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 356 */
/* .local v7, "packageName":Ljava/lang/String; */
v8 = (( java.lang.String ) v6 ).indexOf ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* add-int/lit8 v8, v8, 0x1 */
(( java.lang.String ) v6 ).substring ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 357 */
/* .local v8, "rePath":Ljava/lang/String; */
v9 = (( android.util.ArrayMap ) v0 ).containsKey ( v7 ); // invoke-virtual {v0, v7}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v9, :cond_0 */
/* .line 358 */
/* new-instance v9, Ljava/util/ArrayList; */
/* invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V */
/* .line 359 */
/* .local v9, "pathArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( java.util.ArrayList ) v9 ).add ( v8 ); // invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 360 */
(( android.util.ArrayMap ) v0 ).put ( v7, v9 ); // invoke-virtual {v0, v7, v9}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 361 */
} // .end local v9 # "pathArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .line 362 */
} // :cond_0
(( android.util.ArrayMap ) v0 ).get ( v7 ); // invoke-virtual {v0, v7}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v9, Ljava/util/ArrayList; */
(( java.util.ArrayList ) v9 ).add ( v8 ); // invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 348 */
} // .end local v4 # "startIndex":I
} // .end local v5 # "endIndex":I
} // .end local v6 # "absPath":Ljava/lang/String;
} // .end local v7 # "packageName":Ljava/lang/String;
} // .end local v8 # "rePath":Ljava/lang/String;
} // :cond_1
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 366 */
} // .end local v3 # "i":I
} // :cond_2
v3 = (( android.util.ArrayMap ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->isEmpty()Z
/* if-nez v3, :cond_3 */
/* .line 367 */
(( android.util.ArrayMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Ljava/lang/String; */
/* .line 368 */
/* .local v4, "key":Ljava/lang/String; */
(( android.util.ArrayMap ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Ljava/util/ArrayList; */
v5 = (( java.util.ArrayList ) v5 ).size ( ); // invoke-virtual {v5}, Ljava/util/ArrayList;->size()I
/* new-array v5, v5, [Ljava/lang/String; */
/* .line 369 */
/* .local v5, "arr":[Ljava/lang/String; */
(( android.util.ArrayMap ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, Ljava/util/ArrayList; */
(( java.util.ArrayList ) v6 ).toArray ( v5 ); // invoke-virtual {v6, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
/* .line 370 */
/* invoke-direct {p0, v4, v5}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->getAbsolutePath(Ljava/lang/String;[Ljava/lang/String;)V */
/* .line 371 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " = "; // const-string v7, " = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v5 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v7 = "APPCacheOptimization"; // const-string v7, "APPCacheOptimization"
android.util.Slog .i ( v7,v6 );
/* .line 372 */
v6 = this.mMountDirs;
(( java.util.concurrent.ConcurrentHashMap ) v6 ).put ( v4, v5 ); // invoke-virtual {v6, v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 373 */
} // .end local v4 # "key":Ljava/lang/String;
} // .end local v5 # "arr":[Ljava/lang/String;
/* .line 375 */
} // :cond_3
return;
} // .end method
private static Boolean memoryCheck ( ) {
/* .locals 12 */
/* .line 795 */
final String v0 = "memoryCheck: "; // const-string v0, "memoryCheck: "
final String v1 = "APPCacheOptimization"; // const-string v1, "APPCacheOptimization"
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
/* new-instance v4, Lcom/android/internal/util/MemInfoReader; */
/* invoke-direct {v4}, Lcom/android/internal/util/MemInfoReader;-><init>()V */
/* .line 796 */
/* .local v4, "memInfo":Lcom/android/internal/util/MemInfoReader; */
(( com.android.internal.util.MemInfoReader ) v4 ).readMemInfo ( ); // invoke-virtual {v4}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V
/* .line 797 */
(( com.android.internal.util.MemInfoReader ) v4 ).getRawInfo ( ); // invoke-virtual {v4}, Lcom/android/internal/util/MemInfoReader;->getRawInfo()[J
/* .line 798 */
/* .local v5, "infos":[J */
/* aget-wide v6, v5, v3 */
/* const-wide/16 v8, 0x3e8 */
/* div-long/2addr v6, v8 */
/* div-long/2addr v6, v8 */
/* :try_end_0 */
/* .catch Ljava/lang/ClassCastException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 799 */
/* .local v6, "temp":J */
/* long-to-int v8, v6 */
/* .line 800 */
/* .local v8, "mem_gb":I */
/* const/16 v9, 0x17 */
final String v10 = " GB"; // const-string v10, " GB"
/* if-ge v8, v9, :cond_0 */
/* .line 801 */
try { // :try_start_1
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "memoryCheck:false, mem: "; // const-string v11, "memoryCheck:false, mem: "
(( java.lang.StringBuilder ) v9 ).append ( v11 ); // invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v9 );
/* .line 802 */
/* .line 804 */
} // :cond_0
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "memoryCheck:true, mem: "; // const-string v11, "memoryCheck:true, mem: "
(( java.lang.StringBuilder ) v9 ).append ( v11 ); // invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v9 );
/* :try_end_1 */
/* .catch Ljava/lang/ClassCastException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 805 */
/* .line 810 */
} // .end local v4 # "memInfo":Lcom/android/internal/util/MemInfoReader;
} // .end local v5 # "infos":[J
} // .end local v6 # "temp":J
} // .end local v8 # "mem_gb":I
/* :catch_0 */
/* move-exception v2 */
/* .line 811 */
/* .local v2, "e":Ljava/lang/Exception; */
android.util.Slog .e ( v1,v0,v2 );
/* .line 812 */
/* .line 807 */
} // .end local v2 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v3 */
/* .line 808 */
/* .local v3, "cce":Ljava/lang/ClassCastException; */
android.util.Slog .e ( v1,v0,v3 );
/* .line 809 */
} // .end method
private Boolean mount ( java.lang.String p0, java.lang.String[] p1 ) {
/* .locals 10 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "path" # [Ljava/lang/String; */
/* .line 529 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "start mount " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "APPCacheOptimization"; // const-string v1, "APPCacheOptimization"
android.util.Slog .i ( v1,v0 );
/* .line 530 */
int v0 = 0; // const/4 v0, 0x0
/* .line 532 */
/* .local v0, "ret":Z */
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 534 */
/* .local v2, "pm":Landroid/content/pm/PackageManager; */
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
(( android.content.pm.PackageManager ) v2 ).getApplicationInfo ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* .line 535 */
/* .local v3, "info":Landroid/content/pm/ApplicationInfo; */
v4 = this.mInstalld;
/* const-string/jumbo v7, "u:object_r:system_data_file:s0" */
/* iget v8, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iget v9, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* move-object v5, p1 */
/* move-object v6, p2 */
v4 = /* invoke-interface/range {v4 ..v9}, Landroid/os/IInstalld;->setFileConRecursive(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;II)Z */
/* move v0, v4 */
/* .line 537 */
/* if-nez v0, :cond_0 */
/* .line 538 */
final String v4 = "installd setFileConRecursive fail before mount"; // const-string v4, "installd setFileConRecursive fail before mount"
android.util.Slog .e ( v1,v4 );
/* .line 540 */
} // :cond_0
v4 = this.mVold;
/* iget v5, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
v4 = /* iget v6, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* if-nez v4, :cond_1 */
/* .line 541 */
/* const-string/jumbo v4, "vold mountTmpfs fail" */
android.util.Slog .e ( v1,v4 );
/* .line 542 */
int v0 = 0; // const/4 v0, 0x0
/* .line 546 */
} // :cond_1
} // :goto_0
v4 = this.mInstalld;
/* const-string/jumbo v7, "u:object_r:app_data_file:s0" */
/* iget v8, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iget v9, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* move-object v5, p1 */
/* move-object v6, p2 */
v4 = /* invoke-interface/range {v4 ..v9}, Landroid/os/IInstalld;->setFileConRecursive(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;II)Z */
/* if-nez v4, :cond_2 */
/* .line 548 */
final String v4 = "installd setFileConRecursive fail after mount"; // const-string v4, "installd setFileConRecursive fail after mount"
android.util.Slog .e ( v1,v4 );
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 549 */
int v0 = 0; // const/4 v0, 0x0
/* .line 555 */
} // :cond_2
/* .line 553 */
} // .end local v3 # "info":Landroid/content/pm/ApplicationInfo;
/* :catch_0 */
/* move-exception v3 */
/* .line 554 */
/* .local v3, "e":Ljava/lang/Exception; */
final String v4 = "mount exception"; // const-string v4, "mount exception"
android.util.Slog .e ( v1,v4,v3 );
/* .line 551 */
} // .end local v3 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v3 */
/* .line 552 */
/* .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "package:"; // const-string v5, "package:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " doesn\'t exsits!"; // const-string v5, " doesn\'t exsits!"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v4 );
/* .line 555 */
} // .end local v3 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
/* nop */
/* .line 556 */
} // :goto_1
} // .end method
private void onPackageAdded ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 378 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->isAppRunning(Ljava/lang/String;)Z */
final String v1 = "APPCacheOptimization"; // const-string v1, "APPCacheOptimization"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 379 */
final String v0 = "app is running."; // const-string v0, "app is running."
android.util.Slog .w ( v1,v0 );
/* .line 381 */
} // :cond_0
v0 = this.mParseMountDirs;
v0 = (( android.util.ArrayMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 382 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "add Package:"; // const-string v2, "add Package:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 383 */
v0 = this.mMountDirs;
v0 = (( java.util.concurrent.ConcurrentHashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v0, :cond_2 */
/* .line 384 */
final String v0 = "mount app because package add"; // const-string v0, "mount app because package add"
android.util.Slog .d ( v1,v0 );
/* .line 385 */
v0 = this.mParseMountDirs;
(( android.util.ArrayMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, [Ljava/lang/String; */
v0 = /* invoke-direct {p0, p1, v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mount(Ljava/lang/String;[Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 386 */
final String v0 = "mount success"; // const-string v0, "mount success"
android.util.Slog .i ( v1,v0 );
/* .line 387 */
v0 = this.mMountDirs;
v1 = this.mParseMountDirs;
(( android.util.ArrayMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, [Ljava/lang/String; */
(( java.util.concurrent.ConcurrentHashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 388 */
v0 = this.mParseMountDirs;
(( android.util.ArrayMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, [Ljava/lang/String; */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updatePathMapOnInstalld(Ljava/lang/String;[Ljava/lang/String;)V */
/* .line 390 */
} // :cond_1
final String v0 = "mount fail"; // const-string v0, "mount fail"
android.util.Slog .e ( v1,v0 );
/* .line 393 */
} // :cond_2
final String v0 = "mount skip, because already in mMountDirs"; // const-string v0, "mount skip, because already in mMountDirs"
android.util.Slog .e ( v1,v0 );
/* .line 396 */
} // :cond_3
} // :goto_0
return;
} // .end method
private void parseData ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "data" # Ljava/lang/String; */
/* .line 523 */
com.android.server.appcacheopt.AppCacheOptimizerConfigUtils .parseAppConfig ( p1 );
this.mAppConfigs = v0;
/* .line 524 */
com.android.server.appcacheopt.AppCacheOptimizerConfigUtils .parseWatermarkConfig ( p1 );
/* move-result-wide v0 */
/* .line 525 */
/* .local v0, "tmp":J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v2, v0, v2 */
/* if-gez v2, :cond_0 */
/* iget-wide v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mWatermark:J */
} // :cond_0
/* move-wide v2, v0 */
} // :goto_0
/* iput-wide v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mWatermark:J */
/* .line 526 */
return;
} // .end method
public static java.lang.String readLocalFile ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p0, "filePath" # Ljava/lang/String; */
/* .line 591 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 592 */
/* .local v0, "stringBuilder":Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/io/File; */
/* invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 593 */
/* .local v1, "file":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
final String v3 = "APPCacheOptimization"; // const-string v3, "APPCacheOptimization"
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 594 */
try { // :try_start_0
/* new-instance v2, Ljava/io/FileInputStream; */
/* invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 595 */
/* .local v2, "inputStream":Ljava/io/InputStream; */
/* const/16 v4, 0x400 */
try { // :try_start_1
/* new-array v4, v4, [B */
/* .line 597 */
/* .local v4, "buffer":[B */
} // :goto_0
v5 = (( java.io.InputStream ) v2 ).read ( v4 ); // invoke-virtual {v2, v4}, Ljava/io/InputStream;->read([B)I
/* move v6, v5 */
/* .local v6, "lenth":I */
int v7 = -1; // const/4 v7, -0x1
/* if-eq v5, v7, :cond_0 */
/* .line 598 */
/* new-instance v5, Ljava/lang/String; */
int v7 = 0; // const/4 v7, 0x0
/* invoke-direct {v5, v4, v7, v6}, Ljava/lang/String;-><init>([BII)V */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 600 */
} // .end local v4 # "buffer":[B
} // .end local v6 # "lenth":I
} // :cond_0
try { // :try_start_2
(( java.io.InputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/InputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 594 */
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_3
(( java.io.InputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/InputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v5 */
try { // :try_start_4
(( java.lang.Throwable ) v4 ).addSuppressed ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "stringBuilder":Ljava/lang/StringBuilder;
} // .end local v1 # "file":Ljava/io/File;
} // .end local p0 # "filePath":Ljava/lang/String;
} // :goto_1
/* throw v4 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 600 */
} // .end local v2 # "inputStream":Ljava/io/InputStream;
/* .restart local v0 # "stringBuilder":Ljava/lang/StringBuilder; */
/* .restart local v1 # "file":Ljava/io/File; */
/* .restart local p0 # "filePath":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v2 */
/* .line 601 */
/* .local v2, "e":Ljava/io/IOException; */
final String v4 = "exception when readLocalFile: "; // const-string v4, "exception when readLocalFile: "
android.util.Slog .e ( v3,v4,v2 );
/* .line 602 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_2
/* .line 604 */
} // :cond_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " does not exist!"; // const-string v4, " does not exist!"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v2 );
/* .line 607 */
} // :goto_3
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private void releaseCacheStorageByPercent ( java.lang.String p0, java.lang.String p1, Long p2 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "dataPath" # Ljava/lang/String; */
/* .param p3, "percent" # J */
/* .line 613 */
try { // :try_start_0
v0 = this.mInstalld;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 616 */
/* .line 614 */
/* :catch_0 */
/* move-exception v0 */
/* .line 615 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "APPCacheOptimization"; // const-string v1, "APPCacheOptimization"
final String v2 = "releaseAppStorageByPercent failed!"; // const-string v2, "releaseAppStorageByPercent failed!"
android.util.Slog .e ( v1,v2,v0 );
/* .line 617 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private Boolean reportLowMemPressureLimit ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "pressureState" # I */
/* .line 218 */
int v0 = 1; // const/4 v0, 0x1
/* if-gt p1, v0, :cond_0 */
/* .line 219 */
/* .line 221 */
} // :cond_0
/* iget-wide v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J */
/* iget-wide v3, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->memPressureStorageUsageWatermark:J */
/* cmp-long v1, v1, v3 */
/* if-gtz v1, :cond_1 */
/* .line 222 */
/* .line 224 */
} // :cond_1
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
/* iget-wide v3, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lastReportMemPressureTime:J */
/* sub-long/2addr v1, v3 */
/* const-wide/16 v3, 0x7530 */
/* cmp-long v1, v1, v3 */
int v2 = 0; // const/4 v2, 0x0
/* if-ltz v1, :cond_2 */
/* .line 225 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* iput-wide v3, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->lastReportMemPressureTime:J */
/* .line 226 */
/* iput v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->reportMemPressureCnt:I */
/* .line 228 */
} // :cond_2
/* iget v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->reportMemPressureCnt:I */
/* if-lt v1, v0, :cond_3 */
/* .line 229 */
/* .line 231 */
} // :cond_3
/* add-int/2addr v1, v0 */
/* iput v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->reportMemPressureCnt:I */
/* .line 232 */
} // .end method
private void updatePathMapOnInstalld ( ) {
/* .locals 2 */
/* .line 634 */
v0 = this.mMountDirs;
/* new-instance v1, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V */
(( java.util.concurrent.ConcurrentHashMap ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 641 */
return;
} // .end method
private void updatePathMapOnInstalld ( java.lang.String p0, java.lang.String[] p1 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "paths" # [Ljava/lang/String; */
/* .line 624 */
try { // :try_start_0
v0 = this.mInstalld;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 627 */
/* .line 625 */
/* :catch_0 */
/* move-exception v0 */
/* .line 626 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "APPCacheOptimization"; // const-string v1, "APPCacheOptimization"
/* const-string/jumbo v2, "updatePathMapOnInstalld failed!" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 628 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void updateWatermark ( Long p0 ) {
/* .locals 3 */
/* .param p1, "newWatermark" # J */
/* .line 645 */
try { // :try_start_0
v0 = this.mInstalld;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 648 */
/* .line 646 */
/* :catch_0 */
/* move-exception v0 */
/* .line 647 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "APPCacheOptimization"; // const-string v1, "APPCacheOptimization"
/* const-string/jumbo v2, "updateWatermark failed!" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 649 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void beforePackageRemoved ( Integer p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 121 */
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z */
/* if-nez v0, :cond_0 */
/* .line 124 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z */
final String v1 = "APPCacheOptimization"; // const-string v1, "APPCacheOptimization"
/* if-nez v0, :cond_1 */
/* .line 125 */
final String v0 = "beforePackageRemoved enabled: false"; // const-string v0, "beforePackageRemoved enabled: false"
android.util.Slog .i ( v1,v0 );
/* .line 126 */
return;
/* .line 128 */
} // :cond_1
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 129 */
return;
/* .line 131 */
} // :cond_2
v0 = this.mContext;
/* if-nez v0, :cond_3 */
/* .line 132 */
final String v0 = "beforePackageRemoved mContext is null"; // const-string v0, "beforePackageRemoved mContext is null"
android.util.Slog .e ( v1,v0 );
/* .line 133 */
return;
/* .line 135 */
} // :cond_3
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->isAppRunning(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 136 */
final String v0 = "app is running."; // const-string v0, "app is running."
android.util.Slog .e ( v1,v0 );
/* .line 138 */
} // :cond_4
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "beforePackageRemoved :"; // const-string v2, "beforePackageRemoved :"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 139 */
v0 = this.mMountDirs;
v0 = (( java.util.concurrent.ConcurrentHashMap ) v0 ).containsKey ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 140 */
v0 = this.mMountDirs;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, [Ljava/lang/String; */
v0 = (( com.android.server.appcacheopt.AppCacheOptimizer ) p0 ).umount ( p2, v0 ); // invoke-virtual {p0, p2, v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->umount(Ljava/lang/String;[Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 141 */
/* const-string/jumbo v0, "umount success" */
android.util.Slog .i ( v1,v0 );
/* .line 142 */
v0 = this.mMountDirs;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).remove ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 143 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Ljava/lang/String; */
/* invoke-direct {p0, p2, v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updatePathMapOnInstalld(Ljava/lang/String;[Ljava/lang/String;)V */
/* .line 145 */
} // :cond_5
/* const-string/jumbo v0, "umount fail" */
android.util.Slog .e ( v1,v0 );
/* .line 148 */
} // :cond_6
} // :goto_0
return;
/* .line 122 */
} // :cond_7
} // :goto_1
return;
} // .end method
public void loadDefaultConfigData ( ) {
/* .locals 12 */
/* .line 399 */
final String v0 = "loading cloud config file.file path:/data/system/app_cache_optimization/app_cache_optimization_package_path_cfg.json"; // const-string v0, "loading cloud config file.file path:/data/system/app_cache_optimization/app_cache_optimization_package_path_cfg.json"
final String v1 = "APPCacheOptimization"; // const-string v1, "APPCacheOptimization"
android.util.Slog .d ( v1,v0 );
/* .line 400 */
final String v0 = "/data/system/app_cache_optimization/app_cache_optimization_package_path_cfg.json"; // const-string v0, "/data/system/app_cache_optimization/app_cache_optimization_package_path_cfg.json"
com.android.server.appcacheopt.AppCacheOptimizer .readLocalFile ( v0 );
/* .line 401 */
/* .local v0, "data":Ljava/lang/String; */
v2 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 402 */
final String v2 = "loading local config file.file path:/product/etc/app_cache_optimization/app_cache_optimization_package_path_cfg.json"; // const-string v2, "loading local config file.file path:/product/etc/app_cache_optimization/app_cache_optimization_package_path_cfg.json"
android.util.Slog .d ( v1,v2 );
/* .line 403 */
final String v2 = "/product/etc/app_cache_optimization/app_cache_optimization_package_path_cfg.json"; // const-string v2, "/product/etc/app_cache_optimization/app_cache_optimization_package_path_cfg.json"
com.android.server.appcacheopt.AppCacheOptimizer .readLocalFile ( v2 );
/* .line 405 */
} // :cond_0
/* invoke-direct {p0, v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->parseData(Ljava/lang/String;)V */
/* .line 406 */
/* iget-wide v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mWatermark:J */
/* invoke-direct {p0, v2, v3}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updateWatermark(J)V */
/* .line 407 */
v2 = this.mAppConfigs;
if ( v2 != null) { // if-eqz v2, :cond_c
/* .line 408 */
final String v2 = "config file parse success."; // const-string v2, "config file parse success."
android.util.Slog .i ( v1,v2 );
/* .line 409 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = this.mAppConfigs;
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* if-ge v2, v3, :cond_b */
/* .line 410 */
v3 = this.mAppConfigs;
(( java.util.ArrayList ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig; */
(( com.android.server.appcacheopt.AppCacheOptimizerConfig ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->getPackageName()Ljava/lang/String;
/* .line 411 */
/* .local v3, "packageName":Ljava/lang/String; */
v4 = this.mAppConfigs;
(( java.util.ArrayList ) v4 ).get ( v2 ); // invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig; */
(( com.android.server.appcacheopt.AppCacheOptimizerConfig ) v4 ).getOptimizePath ( ); // invoke-virtual {v4}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->getOptimizePath()[Ljava/lang/String;
/* .line 413 */
/* .local v4, "path":[Ljava/lang/String; */
/* new-instance v5, Ljava/util/HashSet; */
java.util.Arrays .asList ( v4 );
/* invoke-direct {v5, v6}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
/* .line 414 */
/* .local v5, "pathSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
(( java.util.HashSet ) v5 ).toArray ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
/* .line 415 */
v6 = this.mAppConfigs;
(( java.util.ArrayList ) v6 ).get ( v2 ); // invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig; */
v6 = (( com.android.server.appcacheopt.AppCacheOptimizerConfig ) v6 ).getIsOptimizeEnable ( ); // invoke-virtual {v6}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->getIsOptimizeEnable()I
/* const-string/jumbo v7, "umount success" */
/* const-string/jumbo v8, "umount fail" */
final String v9 = "app is running."; // const-string v9, "app is running."
int v10 = 0; // const/4 v10, 0x0
int v11 = 1; // const/4 v11, 0x1
/* if-ne v6, v11, :cond_6 */
/* .line 416 */
v6 = /* invoke-direct {p0, v3}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->isAppRunning(Ljava/lang/String;)Z */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 417 */
android.util.Slog .i ( v1,v9 );
/* .line 419 */
} // :cond_1
/* array-length v6, v4 */
/* if-ne v6, v11, :cond_3 */
/* aget-object v6, v4, v10 */
final String v9 = ""; // const-string v9, ""
v6 = (( java.lang.String ) v6 ).equals ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 421 */
final String v6 = "path in config file is empty"; // const-string v6, "path in config file is empty"
android.util.Slog .w ( v1,v6 );
/* .line 423 */
v6 = this.mMountDirs;
v6 = (( java.util.concurrent.ConcurrentHashMap ) v6 ).containsKey ( v3 ); // invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_a
/* .line 424 */
v6 = this.mMountDirs;
(( java.util.concurrent.ConcurrentHashMap ) v6 ).get ( v3 ); // invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, [Ljava/lang/String; */
v6 = (( com.android.server.appcacheopt.AppCacheOptimizer ) p0 ).umount ( v3, v6 ); // invoke-virtual {p0, v3, v6}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->umount(Ljava/lang/String;[Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 425 */
android.util.Slog .i ( v1,v7 );
/* .line 426 */
v6 = this.mMountDirs;
(( java.util.concurrent.ConcurrentHashMap ) v6 ).remove ( v3 ); // invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 427 */
/* new-array v6, v10, [Ljava/lang/String; */
/* invoke-direct {p0, v3, v6}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updatePathMapOnInstalld(Ljava/lang/String;[Ljava/lang/String;)V */
/* goto/16 :goto_1 */
/* .line 429 */
} // :cond_2
android.util.Slog .e ( v1,v8 );
/* goto/16 :goto_1 */
/* .line 434 */
} // :cond_3
/* invoke-direct {p0, v3, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->getAbsolutePath(Ljava/lang/String;[Ljava/lang/String;)V */
/* .line 435 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " "; // const-string v7, " "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mAppConfigs;
/* .line 436 */
(( java.util.ArrayList ) v8 ).get ( v2 ); // invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v8, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig; */
v8 = (( com.android.server.appcacheopt.AppCacheOptimizerConfig ) v8 ).getIsOptimizeEnable ( ); // invoke-virtual {v8}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->getIsOptimizeEnable()I
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 437 */
java.util.Arrays .toString ( v4 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 435 */
android.util.Slog .i ( v1,v6 );
/* .line 439 */
v6 = this.mParseMountDirs;
(( android.util.ArrayMap ) v6 ).put ( v3, v4 ); // invoke-virtual {v6, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 441 */
v6 = this.mMountDirs;
v6 = (( java.util.concurrent.ConcurrentHashMap ) v6 ).containsKey ( v3 ); // invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v6, :cond_5 */
/* .line 442 */
v6 = /* invoke-direct {p0, v3, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mount(Ljava/lang/String;[Ljava/lang/String;)Z */
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 443 */
final String v6 = "mount success"; // const-string v6, "mount success"
android.util.Slog .i ( v1,v6 );
/* .line 444 */
v6 = this.mMountDirs;
(( java.util.concurrent.ConcurrentHashMap ) v6 ).put ( v3, v4 ); // invoke-virtual {v6, v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 445 */
/* invoke-direct {p0, v3, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updatePathMapOnInstalld(Ljava/lang/String;[Ljava/lang/String;)V */
/* .line 447 */
} // :cond_4
final String v6 = "mount fail"; // const-string v6, "mount fail"
android.util.Slog .e ( v1,v6 );
/* .line 451 */
} // :cond_5
v6 = this.mMountDirs;
(( java.util.concurrent.ConcurrentHashMap ) v6 ).get ( v3 ); // invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, [Ljava/lang/String; */
/* invoke-direct {p0, v3, v6, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->cloudFileChanage(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V */
/* .line 453 */
} // :cond_6
v6 = this.mAppConfigs;
(( java.util.ArrayList ) v6 ).get ( v2 ); // invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig; */
v6 = (( com.android.server.appcacheopt.AppCacheOptimizerConfig ) v6 ).getIsOptimizeEnable ( ); // invoke-virtual {v6}, Lcom/android/server/appcacheopt/AppCacheOptimizerConfig;->getIsOptimizeEnable()I
/* if-nez v6, :cond_a */
/* .line 454 */
v6 = /* invoke-direct {p0, v3}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->isAppRunning(Ljava/lang/String;)Z */
if ( v6 != null) { // if-eqz v6, :cond_7
/* .line 455 */
android.util.Slog .i ( v1,v9 );
/* .line 457 */
} // :cond_7
/* invoke-direct {p0, v3, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->getAbsolutePath(Ljava/lang/String;[Ljava/lang/String;)V */
/* .line 459 */
v6 = this.mParseUnmountDirs;
(( android.util.ArrayMap ) v6 ).put ( v3, v4 ); // invoke-virtual {v6, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 461 */
v6 = this.mMountDirs;
v6 = (( java.util.concurrent.ConcurrentHashMap ) v6 ).containsKey ( v3 ); // invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_9
/* .line 462 */
v6 = (( com.android.server.appcacheopt.AppCacheOptimizer ) p0 ).umount ( v3, v4 ); // invoke-virtual {p0, v3, v4}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->umount(Ljava/lang/String;[Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_8
/* .line 463 */
android.util.Slog .i ( v1,v7 );
/* .line 464 */
v6 = this.mMountDirs;
(( java.util.concurrent.ConcurrentHashMap ) v6 ).remove ( v3 ); // invoke-virtual {v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 465 */
/* new-array v6, v10, [Ljava/lang/String; */
/* invoke-direct {p0, v3, v6}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updatePathMapOnInstalld(Ljava/lang/String;[Ljava/lang/String;)V */
/* .line 467 */
} // :cond_8
android.util.Slog .e ( v1,v8 );
/* .line 470 */
} // :cond_9
/* const-string/jumbo v6, "umount fail, because not in mMountDirs" */
android.util.Slog .e ( v1,v6 );
/* .line 409 */
} // .end local v3 # "packageName":Ljava/lang/String;
} // .end local v4 # "path":[Ljava/lang/String;
} // .end local v5 # "pathSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
} // :cond_a
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* goto/16 :goto_0 */
} // .end local v2 # "i":I
} // :cond_b
/* .line 475 */
} // :cond_c
/* const-string/jumbo v2, "\u914d\u7f6e\u6587\u4ef6\u4e3a\u7a7a" */
android.util.Slog .i ( v1,v2 );
/* .line 477 */
} // :goto_2
return;
} // .end method
public void mountByPackageName ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 733 */
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z */
/* if-nez v0, :cond_0 */
/* .line 736 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z */
/* if-nez v0, :cond_1 */
/* .line 737 */
return;
/* .line 739 */
} // :cond_1
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->DEBUG:Z */
final String v1 = "APPCacheOptimization"; // const-string v1, "APPCacheOptimization"
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 740 */
/* const-string/jumbo v0, "start mountByPackageName" */
android.util.Slog .d ( v1,v0 );
/* .line 743 */
} // :cond_2
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->onPackageAdded(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 746 */
/* .line 744 */
/* :catch_0 */
/* move-exception v0 */
/* .line 745 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v2 = "mountByPackageName fail!"; // const-string v2, "mountByPackageName fail!"
android.util.Slog .e ( v1,v2,v0 );
/* .line 747 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 734 */
} // :cond_3
} // :goto_1
return;
} // .end method
void notifyAppCacheOptimizationEnabled ( ) {
/* .locals 2 */
/* .line 765 */
/* iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z */
final String v1 = "APPCacheOptimization"; // const-string v1, "APPCacheOptimization"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 766 */
final String v0 = "Enable from DeveloperOptions"; // const-string v0, "Enable from DeveloperOptions"
android.util.Slog .d ( v1,v0 );
/* .line 768 */
} // :cond_0
final String v0 = "Disable from DeveloperOptions"; // const-string v0, "Disable from DeveloperOptions"
android.util.Slog .d ( v1,v0 );
/* .line 770 */
} // :goto_0
return;
} // .end method
public void onBootCompleted ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 101 */
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z */
/* if-nez v0, :cond_0 */
/* .line 105 */
} // :cond_0
final String v0 = "APPCacheOptimization"; // const-string v0, "APPCacheOptimization"
/* if-nez p1, :cond_1 */
/* .line 106 */
final String v1 = "onBootCompleted context is null"; // const-string v1, "onBootCompleted context is null"
android.util.Slog .e ( v0,v1 );
/* .line 107 */
return;
/* .line 109 */
} // :cond_1
this.mContext = p1;
/* .line 110 */
(( com.android.server.appcacheopt.AppCacheOptimizer ) p0 ).updateAppCacheFeatureFromSettings ( ); // invoke-virtual {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updateAppCacheFeatureFromSettings()V
/* .line 111 */
/* iget-boolean v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z */
/* if-nez v1, :cond_2 */
/* .line 112 */
final String v1 = "onBootCompleted: disable AppCacheOptimization"; // const-string v1, "onBootCompleted: disable AppCacheOptimization"
android.util.Slog .i ( v0,v1 );
/* .line 113 */
return;
/* .line 115 */
} // :cond_2
final String v1 = "onBootCompleted: enable AppCacheOptimization"; // const-string v1, "onBootCompleted: enable AppCacheOptimization"
android.util.Slog .i ( v0,v1 );
/* .line 116 */
/* invoke-direct {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->connectAndInit()V */
/* .line 117 */
return;
/* .line 102 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void onForegroundActivityChanged ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 152 */
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z */
/* if-nez v0, :cond_0 */
/* .line 155 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z */
/* if-nez v0, :cond_1 */
/* .line 157 */
return;
/* .line 159 */
} // :cond_1
this.mForeground = p1;
/* .line 160 */
return;
/* .line 153 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void reportMemPressure ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "pressureState" # I */
/* .line 164 */
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z */
/* if-nez v0, :cond_0 */
/* .line 167 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z */
/* if-nez v0, :cond_1 */
/* .line 169 */
return;
/* .line 171 */
} // :cond_1
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->reportLowMemPressureLimit(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 172 */
return;
/* .line 175 */
} // :cond_2
/* const-wide/16 v0, 0x2 */
final String v2 = "APPCacheOptimization"; // const-string v2, "APPCacheOptimization"
/* packed-switch p1, :pswitch_data_0 */
/* .line 188 */
/* :pswitch_0 */
final String v3 = "MEM_PRESSURE_HIGH: release 80% storage of all background APP, 50% storage of mForeground APP"; // const-string v3, "MEM_PRESSURE_HIGH: release 80% storage of all background APP, 50% storage of mForeground APP"
android.util.Slog .d ( v2,v3 );
/* .line 190 */
v2 = this.mMountDirs;
/* new-instance v3, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda3; */
/* invoke-direct {v3, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V */
(( java.util.concurrent.ConcurrentHashMap ) v2 ).forEach ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 197 */
v2 = this.mForeground;
/* .line 198 */
/* .local v2, "fg":Ljava/lang/String; */
v3 = this.mMountDirs;
(( java.util.concurrent.ConcurrentHashMap ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, [Ljava/lang/String; */
/* .line 199 */
/* .local v3, "paths":[Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 200 */
/* array-length v4, v3 */
int v5 = 0; // const/4 v5, 0x0
} // :goto_0
/* if-ge v5, v4, :cond_3 */
/* aget-object v6, v3, v5 */
/* .line 201 */
/* .local v6, "path":Ljava/lang/String; */
/* const-wide/16 v7, 0x32 */
/* invoke-direct {p0, v2, v6, v7, v8}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->releaseCacheStorageByPercent(Ljava/lang/String;Ljava/lang/String;J)V */
/* .line 200 */
} // .end local v6 # "path":Ljava/lang/String;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 204 */
} // :cond_3
/* iget-wide v4, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J */
/* div-long/2addr v4, v0 */
/* iput-wide v4, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J */
/* .line 205 */
/* .line 177 */
} // .end local v2 # "fg":Ljava/lang/String;
} // .end local v3 # "paths":[Ljava/lang/String;
/* :pswitch_1 */
final String v3 = "MEM_PRESSURE_MEDIUM: release 50% storage of all background APP"; // const-string v3, "MEM_PRESSURE_MEDIUM: release 50% storage of all background APP"
android.util.Slog .d ( v2,v3 );
/* .line 178 */
v2 = this.mMountDirs;
/* new-instance v3, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda2; */
/* invoke-direct {v3, p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;)V */
(( java.util.concurrent.ConcurrentHashMap ) v2 ).forEach ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 185 */
/* iget-wide v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J */
/* div-long/2addr v2, v0 */
/* iput-wide v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J */
/* .line 186 */
/* nop */
/* .line 209 */
} // :goto_1
return;
/* .line 165 */
} // :cond_4
} // :goto_2
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean umount ( java.lang.String p0, java.lang.String[] p1 ) {
/* .locals 10 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "path" # [Ljava/lang/String; */
/* .line 560 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "start umount " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "APPCacheOptimization"; // const-string v1, "APPCacheOptimization"
android.util.Slog .i ( v1,v0 );
/* .line 561 */
int v0 = 0; // const/4 v0, 0x0
/* .line 563 */
/* .local v0, "ret":Z */
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 565 */
/* .local v2, "pm":Landroid/content/pm/PackageManager; */
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
(( android.content.pm.PackageManager ) v2 ).getApplicationInfo ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* .line 566 */
/* .local v3, "info":Landroid/content/pm/ApplicationInfo; */
v4 = this.mInstalld;
/* const-string/jumbo v7, "u:object_r:system_data_file:s0" */
/* iget v8, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iget v9, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* move-object v5, p1 */
/* move-object v6, p2 */
v4 = /* invoke-interface/range {v4 ..v9}, Landroid/os/IInstalld;->setFileConRecursive(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;II)Z */
/* move v0, v4 */
/* .line 568 */
/* if-nez v0, :cond_0 */
/* .line 569 */
final String v4 = "installd setFileConRecursive fail before umount"; // const-string v4, "installd setFileConRecursive fail before umount"
android.util.Slog .e ( v1,v4 );
/* .line 571 */
} // :cond_0
v4 = v4 = this.mVold;
/* if-nez v4, :cond_1 */
/* .line 572 */
/* const-string/jumbo v4, "vold umountTmpfs fail" */
android.util.Slog .e ( v1,v4 );
/* .line 573 */
int v0 = 0; // const/4 v0, 0x0
/* .line 577 */
} // :cond_1
} // :goto_0
v4 = this.mInstalld;
/* const-string/jumbo v7, "u:object_r:app_data_file:s0" */
/* iget v8, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iget v9, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* move-object v5, p1 */
/* move-object v6, p2 */
v4 = /* invoke-interface/range {v4 ..v9}, Landroid/os/IInstalld;->setFileConRecursive(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;II)Z */
/* if-nez v4, :cond_2 */
/* .line 579 */
final String v4 = "installd setFileConRecursive fail after umount"; // const-string v4, "installd setFileConRecursive fail after umount"
android.util.Slog .e ( v1,v4 );
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 580 */
int v0 = 0; // const/4 v0, 0x0
/* .line 586 */
} // :cond_2
/* .line 584 */
} // .end local v3 # "info":Landroid/content/pm/ApplicationInfo;
/* :catch_0 */
/* move-exception v3 */
/* .line 585 */
/* .local v3, "e":Ljava/lang/Exception; */
/* const-string/jumbo v4, "umount exception" */
android.util.Slog .e ( v1,v4,v3 );
/* .line 582 */
} // .end local v3 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v3 */
/* .line 583 */
/* .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "package:"; // const-string v5, "package:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " doesn\'t exsits!"; // const-string v5, " doesn\'t exsits!"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v4 );
/* .line 586 */
} // .end local v3 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
/* nop */
/* .line 587 */
} // :goto_1
} // .end method
public void umountByPackageName ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 715 */
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z */
/* if-nez v0, :cond_0 */
/* .line 718 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z */
/* if-nez v0, :cond_1 */
/* .line 719 */
return;
/* .line 721 */
} // :cond_1
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->DEBUG:Z */
final String v1 = "APPCacheOptimization"; // const-string v1, "APPCacheOptimization"
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 722 */
/* const-string/jumbo v0, "start umountByPackageName" */
android.util.Slog .d ( v1,v0 );
/* .line 725 */
} // :cond_2
try { // :try_start_0
(( com.android.server.appcacheopt.AppCacheOptimizer ) p0 ).beforePackageRemoved ( p2, p1 ); // invoke-virtual {p0, p2, p1}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->beforePackageRemoved(ILjava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 728 */
/* .line 726 */
/* :catch_0 */
/* move-exception v0 */
/* .line 727 */
/* .local v0, "e":Ljava/lang/Exception; */
/* const-string/jumbo v2, "umountByPackageName fail!" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 729 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 716 */
} // :cond_3
} // :goto_1
return;
} // .end method
void updateAppCacheFeatureFromSettings ( ) {
/* .locals 4 */
/* .line 750 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "miui_app_cache_optimization"; // const-string v2, "miui_app_cache_optimization"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
/* .line 752 */
/* .local v0, "newValue":I */
/* if-nez v0, :cond_0 */
/* .line 753 */
/* iget-boolean v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 754 */
/* iput-boolean v3, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z */
/* .line 757 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z */
/* if-nez v1, :cond_1 */
/* .line 758 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z */
/* .line 761 */
} // :cond_1
} // :goto_0
(( com.android.server.appcacheopt.AppCacheOptimizer ) p0 ).notifyAppCacheOptimizationEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->notifyAppCacheOptimizationEnabled()V
/* .line 762 */
return;
} // .end method
public void updateAppStorageUsageInfo ( ) {
/* .locals 4 */
/* .line 694 */
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->APP_CACHE_OPTIMIZATION_MEMORY_CHECK:Z */
/* if-nez v0, :cond_0 */
/* .line 697 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->mAppCacheOptimizationEnabled:Z */
/* if-nez v0, :cond_1 */
/* .line 698 */
return;
/* .line 700 */
} // :cond_1
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->DEBUG:Z */
final String v1 = "APPCacheOptimization"; // const-string v1, "APPCacheOptimization"
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 701 */
/* const-string/jumbo v0, "start updateAppStorageUsageInfo" */
android.util.Slog .d ( v1,v0 );
/* .line 704 */
} // :cond_2
try { // :try_start_0
v0 = this.mInstalld;
/* move-result-wide v2 */
/* iput-wide v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J */
/* .line 705 */
/* sget-boolean v0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 706 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "storage usage info updated: " */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer;->totalStorageUsage:J */
(( java.lang.StringBuilder ) v0 ).append ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = " MB"; // const-string v2, " MB"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 710 */
} // :cond_3
/* .line 708 */
/* :catch_0 */
/* move-exception v0 */
/* .line 709 */
/* .local v0, "e":Ljava/lang/Exception; */
/* const-string/jumbo v2, "updateAppStorageUsageInfo fail!" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 711 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 695 */
} // :cond_4
} // :goto_1
return;
} // .end method
