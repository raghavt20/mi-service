.class Lcom/android/server/appcacheopt/AppCacheOptimizer$MiuiSettingsObserver;
.super Landroid/database/ContentObserver;
.source "AppCacheOptimizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/appcacheopt/AppCacheOptimizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MiuiSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/appcacheopt/AppCacheOptimizer;


# direct methods
.method public constructor <init>(Lcom/android/server/appcacheopt/AppCacheOptimizer;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 775
    iput-object p1, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer$MiuiSettingsObserver;->this$0:Lcom/android/server/appcacheopt/AppCacheOptimizer;

    .line 776
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 777
    return-void
.end method


# virtual methods
.method observe()V
    .locals 4

    .line 780
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer$MiuiSettingsObserver;->this$0:Lcom/android/server/appcacheopt/AppCacheOptimizer;

    invoke-static {v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->-$$Nest$fgetmContext(Lcom/android/server/appcacheopt/AppCacheOptimizer;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 781
    .local v0, "resolver":Landroid/content/ContentResolver;
    nop

    .line 782
    const-string v1, "miui_app_cache_optimization"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 781
    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 784
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 788
    iget-object v0, p0, Lcom/android/server/appcacheopt/AppCacheOptimizer$MiuiSettingsObserver;->this$0:Lcom/android/server/appcacheopt/AppCacheOptimizer;

    invoke-virtual {v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer;->updateAppCacheFeatureFromSettings()V

    .line 790
    return-void
.end method
