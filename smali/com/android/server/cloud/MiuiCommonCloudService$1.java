class com.android.server.cloud.MiuiCommonCloudService$1 extends android.database.ContentObserver {
	 /* .source "MiuiCommonCloudService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/cloud/MiuiCommonCloudService;->registerCloudDataObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.cloud.MiuiCommonCloudService this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$EFpkJfUsFDa9zEvEiLi_8Uxzw8U ( com.android.server.cloud.MiuiCommonCloudService$1 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/cloud/MiuiCommonCloudService$1;->lambda$onChange$0()V */
return;
} // .end method
 com.android.server.cloud.MiuiCommonCloudService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/cloud/MiuiCommonCloudService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 29 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
private void lambda$onChange$0 ( ) { //synthethic
/* .locals 1 */
/* .line 36 */
v0 = this.this$0;
com.android.server.cloud.MiuiCommonCloudService .-$$Nest$fgetmMiuiCloudParsingCollection ( v0 );
(( com.android.server.cloud.MiuiCloudParsingCollection ) v0 ).syncLocalBackupFromCloud ( ); // invoke-virtual {v0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->syncLocalBackupFromCloud()V
/* .line 37 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .line 32 */
v0 = this.this$0;
com.android.server.cloud.MiuiCommonCloudService .-$$Nest$fgetmMiuiCloudParsingCollection ( v0 );
v0 = (( com.android.server.cloud.MiuiCloudParsingCollection ) v0 ).updateDataFromCloud ( ); // invoke-virtual {v0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->updateDataFromCloud()Z
/* .line 33 */
/* .local v0, "changed":Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Cloud Data changed: "; // const-string v2, "Cloud Data changed: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiCommonCloudService"; // const-string v2, "MiuiCommonCloudService"
android.util.Slog .d ( v2,v1 );
/* .line 34 */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 35 */
	 com.android.internal.os.BackgroundThread .getHandler ( );
	 /* new-instance v2, Lcom/android/server/cloud/MiuiCommonCloudService$1$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v2, p0}, Lcom/android/server/cloud/MiuiCommonCloudService$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/cloud/MiuiCommonCloudService$1;)V */
	 (( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
	 /* .line 38 */
	 v1 = this.this$0;
	 com.android.server.cloud.MiuiCommonCloudService .-$$Nest$fgetmMiuiCloudParsingCollection ( v1 );
	 (( com.android.server.cloud.MiuiCloudParsingCollection ) v1 ).notifyAllObservers ( ); // invoke-virtual {v1}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->notifyAllObservers()V
	 /* .line 40 */
} // :cond_0
return;
} // .end method
