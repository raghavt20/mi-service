public class com.android.server.cloud.MiuiCommonCloudHelper extends com.android.server.MiuiCommonCloudHelperStub {
	 /* .source "MiuiCommonCloudHelper.java" */
	 /* # static fields */
	 private static final java.lang.String CLOUD_BACKUP_FILE_ROOT_ELEMENT;
	 private static final java.lang.String CLOUD_BACKUP_FILE_TEST_CRT_TAG;
	 private static final java.lang.String CLOUD_CTRL_ITEM_TAG;
	 private static final java.lang.String CLOUD_MODULE_BEAN_PATH;
	 private static final java.lang.String CLOUD_MODULE_NAME;
	 private static final java.lang.String CLOUD_MODULE_STRING;
	 private static final java.lang.String CLOUD_MODULE_VERSION;
	 private static final java.lang.String MODULE_DATA_VERSION;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private java.lang.String mCloudFilePath;
	 private android.content.Context mContext;
	 private com.google.gson.Gson mGson;
	 private android.os.Handler mHandler;
	 private java.util.HashMap mModuleNameBeanClassPathMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.util.HashMap mModuleNameClassObjectMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashMap mModuleNameDataVersionMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashMap mModuleNameStrMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.ArrayList mObservers;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/MiuiCommonCloudHelperStub$Observer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$gw7JyJp4wE5WoHhbNB6P0gdNhd0 ( com.android.server.cloud.MiuiCommonCloudHelper p0, java.lang.String p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->lambda$initFromPath$1(Ljava/lang/String;)V */
return;
} // .end method
public static void $r8$lambda$vM_H3z5Sj_pN1ngB3s5ZI2VZhEY ( com.android.server.cloud.MiuiCommonCloudHelper p0, java.lang.String p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->lambda$initFromAssets$0(Ljava/lang/String;)V */
return;
} // .end method
public static void $r8$lambda$wQgFWhUNh9RV7pB82MdT48oBvWI ( com.android.server.cloud.MiuiCommonCloudHelper p0, org.xmlpull.v1.XmlSerializer p1, java.lang.String p2, java.lang.String p3, java.lang.String p4 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->lambda$writeElementOfModuleListToXml$2(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
return;
} // .end method
static java.lang.String -$$Nest$fgetmCloudFilePath ( com.android.server.cloud.MiuiCommonCloudHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCloudFilePath;
} // .end method
static void -$$Nest$msyncLocalBackupFromCloud ( com.android.server.cloud.MiuiCommonCloudHelper p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->syncLocalBackupFromCloud(Ljava/lang/String;)V */
return;
} // .end method
static Boolean -$$Nest$mupdateDataFromCloud ( com.android.server.cloud.MiuiCommonCloudHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->updateDataFromCloud()Z */
} // .end method
public com.android.server.cloud.MiuiCommonCloudHelper ( ) {
/* .locals 1 */
/* .line 35 */
/* invoke-direct {p0}, Lcom/android/server/MiuiCommonCloudHelperStub;-><init>()V */
/* .line 49 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mModuleNameStrMap = v0;
/* .line 50 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mModuleNameBeanClassPathMap = v0;
/* .line 51 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mModuleNameDataVersionMap = v0;
/* .line 52 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mModuleNameClassObjectMap = v0;
/* .line 53 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mObservers = v0;
return;
} // .end method
private void checkFileStatus ( java.io.File p0 ) {
/* .locals 4 */
/* .param p1, "file" # Ljava/io/File; */
/* .line 191 */
final String v0 = "MiuiCommonCloudHelper"; // const-string v0, "MiuiCommonCloudHelper"
if ( p1 != null) { // if-eqz p1, :cond_0
try { // :try_start_0
v1 = (( java.io.File ) p1 ).exists ( ); // invoke-virtual {p1}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_1 */
/* .line 192 */
} // :cond_0
(( java.io.File ) p1 ).createNewFile ( ); // invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z
/* .line 193 */
final String v1 = "Local cloud_backup file dose not exist, create file end"; // const-string v1, "Local cloud_backup file dose not exist, create file end"
android.util.Slog .d ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 197 */
} // :cond_1
/* .line 195 */
/* :catch_0 */
/* move-exception v1 */
/* .line 196 */
/* .local v1, "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Create local cloud_backup file fail "; // const-string v3, "Create local cloud_backup file fail "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 198 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
private java.lang.String getStringByModuleName ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "moduleName" # Ljava/lang/String; */
/* .line 417 */
v0 = this.mModuleNameStrMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
} // .end method
private void lambda$initFromAssets$0 ( java.lang.String p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "assetsConfigFilePath" # Ljava/lang/String; */
/* .line 75 */
/* invoke-direct {p0, p1}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->loadLocalInitConfigFromAssets(Ljava/lang/String;)V */
/* .line 76 */
v0 = this.mCloudFilePath;
/* invoke-direct {p0, v0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->loadLocalCloudBackup(Ljava/lang/String;)V */
/* .line 77 */
/* invoke-direct {p0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->parseStringToClassObject()V */
/* .line 78 */
(( com.android.server.cloud.MiuiCommonCloudHelper ) p0 ).notifyAllObservers ( ); // invoke-virtual {p0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->notifyAllObservers()V
/* .line 79 */
return;
} // .end method
private void lambda$initFromPath$1 ( java.lang.String p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "configFilePath" # Ljava/lang/String; */
/* .line 90 */
/* invoke-direct {p0, p1}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->loadLocalInitConfigFromPath(Ljava/lang/String;)V */
/* .line 91 */
v0 = this.mCloudFilePath;
/* invoke-direct {p0, v0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->loadLocalCloudBackup(Ljava/lang/String;)V */
/* .line 92 */
/* invoke-direct {p0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->parseStringToClassObject()V */
/* .line 93 */
(( com.android.server.cloud.MiuiCommonCloudHelper ) p0 ).notifyAllObservers ( ); // invoke-virtual {p0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->notifyAllObservers()V
/* .line 94 */
return;
} // .end method
private void lambda$writeElementOfModuleListToXml$2 ( org.xmlpull.v1.XmlSerializer p0, java.lang.String p1, java.lang.String p2, java.lang.String p3 ) { //synthethic
/* .locals 3 */
/* .param p1, "serializer" # Lorg/xmlpull/v1/XmlSerializer; */
/* .param p2, "tagString" # Ljava/lang/String; */
/* .param p3, "key" # Ljava/lang/String; */
/* .param p4, "value" # Ljava/lang/String; */
/* .line 391 */
if ( p4 != null) { // if-eqz p4, :cond_0
/* .line 393 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
/* .line 394 */
final String v1 = "moduleName"; // const-string v1, "moduleName"
/* .line 395 */
final String v1 = "moduleVersion"; // const-string v1, "moduleVersion"
v2 = this.mModuleNameDataVersionMap;
(( java.util.HashMap ) v2 ).get ( p3 ); // invoke-virtual {v2, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
/* .line 396 */
final String v1 = "moduleBeanPath"; // const-string v1, "moduleBeanPath"
v2 = this.mModuleNameBeanClassPathMap;
(( java.util.HashMap ) v2 ).get ( p3 ); // invoke-virtual {v2, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
/* .line 397 */
final String v1 = "moduleString"; // const-string v1, "moduleString"
/* .line 398 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 402 */
/* .line 399 */
/* :catch_0 */
/* move-exception v0 */
/* .line 400 */
/* .local v0, "e":Ljava/io/IOException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to write element of app list to xml"; // const-string v2, "Failed to write element of app list to xml"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiCommonCloudHelper"; // const-string v2, "MiuiCommonCloudHelper"
android.util.Slog .e ( v2,v1 );
/* .line 401 */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
/* .line 404 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_0
return;
} // .end method
private void loadLocalCloudBackup ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "cloudPath" # Ljava/lang/String; */
/* .line 139 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 140 */
/* .local v0, "mCloudBackFile":Ljava/io/File; */
/* invoke-direct {p0, v0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->checkFileStatus(Ljava/io/File;)V */
/* .line 141 */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
/* invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 142 */
/* .local v1, "inputStream":Ljava/io/FileInputStream; */
/* nop */
/* .line 143 */
try { // :try_start_1
/* invoke-direct {p0, v1}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->loadLocalFile(Ljava/io/InputStream;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 147 */
try { // :try_start_2
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 149 */
} // .end local v1 # "inputStream":Ljava/io/FileInputStream;
/* .line 141 */
/* .restart local v1 # "inputStream":Ljava/io/FileInputStream; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "mCloudBackFile":Ljava/io/File;
} // .end local p0 # "this":Lcom/android/server/cloud/MiuiCommonCloudHelper;
} // .end local p1 # "cloudPath":Ljava/lang/String;
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 147 */
} // .end local v1 # "inputStream":Ljava/io/FileInputStream;
/* .restart local v0 # "mCloudBackFile":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/android/server/cloud/MiuiCommonCloudHelper; */
/* .restart local p1 # "cloudPath":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v1 */
/* .line 148 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "MiuiCommonCloudHelper"; // const-string v2, "MiuiCommonCloudHelper"
final String v3 = "Failed to read local cloud_backup file "; // const-string v3, "Failed to read local cloud_backup file "
android.util.Slog .e ( v2,v3,v1 );
/* .line 150 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void loadLocalFile ( java.io.InputStream p0 ) {
/* .locals 13 */
/* .param p1, "inputStream" # Ljava/io/InputStream; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/xmlpull/v1/XmlPullParserException;, */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 158 */
android.util.Xml .newPullParser ( );
/* .line 159 */
/* .local v0, "parser":Lorg/xmlpull/v1/XmlPullParser; */
v1 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v1 ).name ( ); // invoke-virtual {v1}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
v1 = /* .line 160 */
/* .line 161 */
/* .local v1, "eventType":I */
} // :goto_0
int v2 = 1; // const/4 v2, 0x1
/* if-eq v1, v2, :cond_2 */
/* .line 162 */
/* packed-switch v1, :pswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 164 */
/* :pswitch_0 */
/* .line 165 */
/* .local v2, "tagName":Ljava/lang/String; */
final String v3 = "cloud-data-item"; // const-string v3, "cloud-data-item"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 166 */
final String v3 = "moduleName"; // const-string v3, "moduleName"
int v4 = 0; // const/4 v4, 0x0
/* .line 167 */
/* .local v3, "moduleName":Ljava/lang/String; */
final String v5 = "moduleVersion"; // const-string v5, "moduleVersion"
/* .line 168 */
/* .local v5, "moduleVersion":Ljava/lang/String; */
final String v6 = "moduleBeanPath"; // const-string v6, "moduleBeanPath"
/* .line 169 */
/* .local v6, "moduleBeanPath":Ljava/lang/String; */
final String v7 = "moduleString"; // const-string v7, "moduleString"
/* .line 170 */
/* .local v4, "moduleString":Ljava/lang/String; */
java.lang.Long .parseLong ( v5 );
/* move-result-wide v7 */
java.lang.Long .valueOf ( v7,v8 );
/* .line 171 */
/* .local v7, "lastStoredVersion":Ljava/lang/Long; */
v8 = this.mModuleNameDataVersionMap;
final String v9 = "-1"; // const-string v9, "-1"
(( java.util.HashMap ) v8 ).getOrDefault ( v3, v9 ); // invoke-virtual {v8, v3, v9}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v8, Ljava/lang/String; */
java.lang.Long .parseLong ( v8 );
/* move-result-wide v8 */
java.lang.Long .valueOf ( v8,v9 );
/* .line 172 */
/* .local v8, "initVersion":Ljava/lang/Long; */
(( java.lang.Long ) v7 ).longValue ( ); // invoke-virtual {v7}, Ljava/lang/Long;->longValue()J
/* move-result-wide v9 */
(( java.lang.Long ) v8 ).longValue ( ); // invoke-virtual {v8}, Ljava/lang/Long;->longValue()J
/* move-result-wide v11 */
/* cmp-long v9, v9, v11 */
/* if-lez v9, :cond_0 */
/* .line 173 */
v9 = this.mModuleNameStrMap;
(( java.util.HashMap ) v9 ).put ( v3, v4 ); // invoke-virtual {v9, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 174 */
v9 = this.mModuleNameDataVersionMap;
(( java.util.HashMap ) v9 ).put ( v3, v5 ); // invoke-virtual {v9, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 175 */
v9 = this.mModuleNameBeanClassPathMap;
(( java.util.HashMap ) v9 ).put ( v3, v6 ); // invoke-virtual {v9, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 176 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Last stored cloud data version: "; // const-string v10, "Last stored cloud data version: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v10 = " is higher than init data version: "; // const-string v10, " is higher than init data version: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v10 = ", so use last stored cloud data"; // const-string v10, ", so use last stored cloud data"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v10 = "MiuiCommonCloudHelper"; // const-string v10, "MiuiCommonCloudHelper"
android.util.Slog .d ( v10,v9 );
/* .line 180 */
} // .end local v3 # "moduleName":Ljava/lang/String;
} // .end local v4 # "moduleString":Ljava/lang/String;
} // .end local v5 # "moduleVersion":Ljava/lang/String;
} // .end local v6 # "moduleBeanPath":Ljava/lang/String;
} // .end local v7 # "lastStoredVersion":Ljava/lang/Long;
} // .end local v8 # "initVersion":Ljava/lang/Long;
} // :cond_0
/* nop */
/* .line 185 */
} // .end local v2 # "tagName":Ljava/lang/String;
} // :cond_1
v1 = } // :goto_1
/* goto/16 :goto_0 */
/* .line 187 */
} // :cond_2
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void loadLocalInitConfigFromAssets ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "assetsConfigFilePath" # Ljava/lang/String; */
/* .line 103 */
final String v0 = "MiuiCommonCloudHelper"; // const-string v0, "MiuiCommonCloudHelper"
android.content.res.Resources .getSystem ( );
(( android.content.res.Resources ) v1 ).getAssets ( ); // invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;
/* .line 104 */
/* .local v1, "assetManager":Landroid/content/res/AssetManager; */
try { // :try_start_0
(( android.content.res.AssetManager ) v1 ).open ( p1 ); // invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 105 */
/* .local v2, "inputStream":Ljava/io/InputStream; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 106 */
try { // :try_start_1
/* invoke-direct {p0, v2}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->loadLocalFile(Ljava/io/InputStream;)V */
/* .line 104 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 108 */
} // :cond_0
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Local init config file "; // const-string v4, "Local init config file "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " does not exits "; // const-string v4, " does not exits "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 110 */
} // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
try { // :try_start_2
(( java.io.InputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/InputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 112 */
} // .end local v2 # "inputStream":Ljava/io/InputStream;
} // :cond_1
/* .line 104 */
/* .restart local v2 # "inputStream":Ljava/io/InputStream; */
} // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_2
try { // :try_start_3
(( java.io.InputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/InputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_4
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "assetManager":Landroid/content/res/AssetManager;
} // .end local p0 # "this":Lcom/android/server/cloud/MiuiCommonCloudHelper;
} // .end local p1 # "assetsConfigFilePath":Ljava/lang/String;
} // :cond_2
} // :goto_2
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 110 */
} // .end local v2 # "inputStream":Ljava/io/InputStream;
/* .restart local v1 # "assetManager":Landroid/content/res/AssetManager; */
/* .restart local p0 # "this":Lcom/android/server/cloud/MiuiCommonCloudHelper; */
/* .restart local p1 # "assetsConfigFilePath":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v2 */
/* .line 111 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to read local init config file "; // const-string v4, "Failed to read local init config file "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3,v2 );
/* .line 113 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_3
return;
} // .end method
private void loadLocalInitConfigFromPath ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 122 */
try { // :try_start_0
/* new-instance v0, Ljava/io/FileInputStream; */
/* invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* .line 123 */
/* .local v0, "inputStream":Ljava/io/InputStream; */
/* nop */
/* .line 124 */
/* invoke-direct {p0, v0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->loadLocalFile(Ljava/io/InputStream;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 130 */
} // .end local v0 # "inputStream":Ljava/io/InputStream;
/* .line 128 */
/* :catch_0 */
/* move-exception v0 */
/* .line 129 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to read local init config file "; // const-string v2, "Failed to read local init config file "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiCommonCloudHelper"; // const-string v2, "MiuiCommonCloudHelper"
android.util.Slog .e ( v2,v1,v0 );
/* .line 131 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private java.lang.Object parseStringToClass ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "classPath" # Ljava/lang/String; */
/* .line 249 */
try { // :try_start_0
v0 = (( java.lang.String ) p2 ).hashCode ( ); // invoke-virtual {p2}, Ljava/lang/String;->hashCode()I
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
/* const-string/jumbo v0, "short" */
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 5; // const/4 v0, 0x5
/* :sswitch_1 */
final String v0 = "float"; // const-string v0, "float"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 2; // const/4 v0, 0x2
/* :sswitch_2 */
final String v0 = "boolean"; // const-string v0, "boolean"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 0; // const/4 v0, 0x0
/* :sswitch_3 */
final String v0 = "long"; // const-string v0, "long"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 6; // const/4 v0, 0x6
/* :sswitch_4 */
final String v0 = "char"; // const-string v0, "char"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 3; // const/4 v0, 0x3
/* :sswitch_5 */
final String v0 = "byte"; // const-string v0, "byte"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 4; // const/4 v0, 0x4
/* :sswitch_6 */
final String v0 = "int"; // const-string v0, "int"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* :sswitch_7 */
final String v0 = "double"; // const-string v0, "double"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 7; // const/4 v0, 0x7
/* :sswitch_8 */
final String v0 = "String"; // const-string v0, "String"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x8 */
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 278 */
java.lang.Class .forName ( p2 );
/* .line 275 */
/* :pswitch_0 */
/* const-class v0, Ljava/lang/String; */
/* .line 276 */
/* .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* .line 272 */
} // .end local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
/* :pswitch_1 */
v0 = java.lang.Double.TYPE;
/* .line 273 */
/* .restart local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* .line 269 */
} // .end local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
/* :pswitch_2 */
v0 = java.lang.Long.TYPE;
/* .line 270 */
/* .restart local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* .line 266 */
} // .end local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
/* :pswitch_3 */
v0 = java.lang.Short.TYPE;
/* .line 267 */
/* .restart local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* .line 263 */
} // .end local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
/* :pswitch_4 */
v0 = java.lang.Byte.TYPE;
/* .line 264 */
/* .restart local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* .line 260 */
} // .end local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
/* :pswitch_5 */
v0 = java.lang.Character.TYPE;
/* .line 261 */
/* .restart local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* .line 257 */
} // .end local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
/* :pswitch_6 */
v0 = java.lang.Float.TYPE;
/* .line 258 */
/* .restart local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* .line 254 */
} // .end local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
/* :pswitch_7 */
v0 = java.lang.Integer.TYPE;
/* .line 255 */
/* .restart local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* .line 251 */
} // .end local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
/* :pswitch_8 */
v0 = java.lang.Boolean.TYPE;
/* .line 252 */
/* .restart local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* nop */
/* .line 280 */
} // :goto_2
v1 = this.mGson;
(( com.google.gson.Gson ) v1 ).fromJson ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 281 */
} // .end local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
/* :catch_0 */
/* move-exception v0 */
/* .line 282 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "parseStringToClass exception "; // const-string v2, "parseStringToClass exception "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiCommonCloudHelper"; // const-string v2, "MiuiCommonCloudHelper"
android.util.Slog .e ( v2,v1 );
/* .line 283 */
int v1 = 0; // const/4 v1, 0x0
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x6bc5b3cf -> :sswitch_8 */
/* -0x4f08842f -> :sswitch_7 */
/* 0x197ef -> :sswitch_6 */
/* 0x2e6108 -> :sswitch_5 */
/* 0x2e9356 -> :sswitch_4 */
/* 0x32c67c -> :sswitch_3 */
/* 0x3db6c28 -> :sswitch_2 */
/* 0x5d0225c -> :sswitch_1 */
/* 0x685847c -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void parseStringToClassObject ( ) {
/* .locals 6 */
/* .line 226 */
v0 = this.mModuleNameStrMap;
(( java.util.HashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/lang/String; */
/* .line 227 */
/* .local v1, "moduleName":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 228 */
/* .local v2, "classObj":Ljava/lang/Object; */
v3 = this.mModuleNameStrMap;
(( java.util.HashMap ) v3 ).get ( v1 ); // invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/String; */
/* .line 229 */
/* .local v3, "data":Ljava/lang/String; */
v4 = this.mModuleNameBeanClassPathMap;
(( java.util.HashMap ) v4 ).get ( v1 ); // invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/String; */
/* .line 230 */
/* .local v4, "beanPath":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_0
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 231 */
/* invoke-direct {p0, v3, v4}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->parseStringToClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object; */
/* .line 233 */
} // :cond_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 234 */
v5 = this.mModuleNameClassObjectMap;
(( java.util.HashMap ) v5 ).put ( v1, v2 ); // invoke-virtual {v5, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 236 */
} // .end local v1 # "moduleName":Ljava/lang/String;
} // .end local v2 # "classObj":Ljava/lang/Object;
} // .end local v3 # "data":Ljava/lang/String;
} // .end local v4 # "beanPath":Ljava/lang/String;
} // :cond_1
/* .line 237 */
} // :cond_2
return;
} // .end method
private void syncLocalBackupFromCloud ( java.lang.String p0 ) {
/* .locals 11 */
/* .param p1, "cloudFilePath" # Ljava/lang/String; */
/* .line 357 */
final String v0 = "cloud-control"; // const-string v0, "cloud-control"
final String v1 = "common-cloud-config"; // const-string v1, "common-cloud-config"
final String v2 = "MiuiCommonCloudHelper"; // const-string v2, "MiuiCommonCloudHelper"
/* new-instance v3, Ljava/io/File; */
/* invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 358 */
/* .local v3, "mCloudBackFile":Ljava/io/File; */
/* invoke-direct {p0, v3}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->checkFileStatus(Ljava/io/File;)V */
/* .line 359 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* .line 360 */
/* .local v4, "startTime":J */
try { // :try_start_0
/* new-instance v6, Ljava/io/BufferedOutputStream; */
/* new-instance v7, Ljava/io/FileOutputStream; */
/* invoke-direct {v7, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* invoke-direct {v6, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 361 */
/* .local v6, "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream; */
try { // :try_start_1
android.util.Xml .newSerializer ( );
/* .line 362 */
/* .local v7, "serializer":Lorg/xmlpull/v1/XmlSerializer; */
v8 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v8 ).name ( ); // invoke-virtual {v8}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
/* .line 363 */
v8 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v8 ).name ( ); // invoke-virtual {v8}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
int v9 = 1; // const/4 v9, 0x1
java.lang.Boolean .valueOf ( v9 );
/* .line 364 */
final String v8 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v8, "http://xmlpull.org/v1/doc/features.html#indent-output"
/* .line 365 */
int v8 = 0; // const/4 v8, 0x0
/* .line 366 */
/* .line 367 */
v9 = this.mModuleNameStrMap;
final String v10 = "cloud-data-item"; // const-string v10, "cloud-data-item"
/* invoke-direct {p0, v7, v9, v10}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->writeElementOfModuleListToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;Ljava/lang/String;)V */
/* .line 368 */
/* .line 369 */
/* .line 370 */
/* .line 371 */
(( java.io.BufferedOutputStream ) v6 ).flush ( ); // invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->flush()V
/* .line 372 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Write local cloud_backup file took "; // const-string v1, "Write local cloud_backup file took "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 373 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v8 */
/* sub-long/2addr v8, v4 */
(( java.lang.StringBuilder ) v0 ).append ( v8, v9 ); // invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " ms "; // const-string v1, " ms "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 372 */
android.util.Slog .d ( v2,v0 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 374 */
} // .end local v7 # "serializer":Lorg/xmlpull/v1/XmlSerializer;
try { // :try_start_2
(( java.io.BufferedOutputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 379 */
} // .end local v6 # "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream;
/* .line 360 */
/* .restart local v6 # "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream; */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_3
(( java.io.BufferedOutputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_4
(( java.lang.Throwable ) v0 ).addSuppressed ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v3 # "mCloudBackFile":Ljava/io/File;
} // .end local v4 # "startTime":J
} // .end local p0 # "this":Lcom/android/server/cloud/MiuiCommonCloudHelper;
} // .end local p1 # "cloudFilePath":Ljava/lang/String;
} // :goto_0
/* throw v0 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 374 */
} // .end local v6 # "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream;
/* .restart local v3 # "mCloudBackFile":Ljava/io/File; */
/* .restart local v4 # "startTime":J */
/* .restart local p0 # "this":Lcom/android/server/cloud/MiuiCommonCloudHelper; */
/* .restart local p1 # "cloudFilePath":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v0 */
/* .line 375 */
/* .local v0, "e":Ljava/io/IOException; */
v1 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = (( java.io.File ) v3 ).delete ( ); // invoke-virtual {v3}, Ljava/io/File;->delete()Z
/* if-nez v1, :cond_0 */
/* .line 376 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Failed to clean up mangled file: "; // const-string v6, "Failed to clean up mangled file: "
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v1 );
/* .line 378 */
} // :cond_0
final String v1 = "Unable to write local cloud_backup file"; // const-string v1, "Unable to write local cloud_backup file"
android.util.Slog .e ( v2,v1,v0 );
/* .line 380 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_1
return;
} // .end method
private Boolean updateCloudData ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p1, "moduleName" # Ljava/lang/String; */
/* .line 307 */
v0 = this.mModuleNameDataVersionMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
/* .line 308 */
/* .local v0, "latestVersion":Ljava/lang/String; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "mVersion"; // const-string v2, "mVersion"
int v3 = 0; // const/4 v3, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v1,p1,v2,v3 );
/* .line 310 */
/* .local v1, "remoteVersion":Ljava/lang/String; */
v2 = this.mContext;
/* .line 311 */
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.MiuiSettings$SettingsCloudData .getCloudDataList ( v2,p1 );
/* .line 312 */
/* .local v2, "cloudDataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;" */
int v3 = 0; // const/4 v3, 0x0
final String v4 = "MiuiCommonCloudHelper"; // const-string v4, "MiuiCommonCloudHelper"
/* if-nez v1, :cond_0 */
/* .line 313 */
final String v5 = " remoteVersion is null, don\'t update "; // const-string v5, " remoteVersion is null, don\'t update "
android.util.Slog .d ( v4,v5 );
/* .line 314 */
/* .line 316 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
java.lang.Long .parseLong ( v1 );
/* move-result-wide v5 */
java.lang.Long .parseLong ( v0 );
/* move-result-wide v7 */
/* cmp-long v5, v5, v7 */
/* if-lez v5, :cond_1 */
/* .line 317 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " moduleName: "; // const-string v5, " moduleName: "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", lastVersion: "; // const-string v5, ", lastVersion: "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", remoteVersion: "; // const-string v5, ", remoteVersion: "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", so update"; // const-string v5, ", so update"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v3 );
/* .line 319 */
v3 = /* invoke-direct {p0, p1, v1, v2}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->updateModuleData(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z */
/* .line 321 */
} // :cond_1
} // .end method
private Boolean updateDataFromCloud ( ) {
/* .locals 4 */
/* .line 293 */
int v0 = 0; // const/4 v0, 0x0
/* .line 294 */
/* .local v0, "changed":Z */
v1 = this.mModuleNameStrMap;
(( java.util.HashMap ) v1 ).keySet ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/lang/String; */
/* .line 295 */
/* .local v2, "moduleName":Ljava/lang/String; */
v3 = /* invoke-direct {p0, v2}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->updateCloudData(Ljava/lang/String;)Z */
/* or-int/2addr v0, v3 */
/* .line 296 */
} // .end local v2 # "moduleName":Ljava/lang/String;
/* .line 297 */
} // :cond_0
} // .end method
private Boolean updateModuleData ( java.lang.String p0, java.lang.String p1, java.util.List p2 ) {
/* .locals 7 */
/* .param p1, "moduleName" # Ljava/lang/String; */
/* .param p2, "remoteVersion" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 333 */
/* .local p3, "cloudDataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;" */
int v0 = 0; // const/4 v0, 0x0
/* check-cast v1, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v1 ).toString ( ); // invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;
/* .line 334 */
/* .local v1, "cloudData":Ljava/lang/String; */
v2 = this.mModuleNameBeanClassPathMap;
(( java.util.HashMap ) v2 ).get ( p1 ); // invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
/* .line 335 */
/* .local v2, "beanPath":Ljava/lang/String; */
final String v3 = "MiuiCommonCloudHelper"; // const-string v3, "MiuiCommonCloudHelper"
if ( v1 != null) { // if-eqz v1, :cond_1
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 336 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->parseStringToClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object; */
/* .line 337 */
/* .local v4, "classObject":Ljava/lang/Object; */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 338 */
v0 = this.mModuleNameDataVersionMap;
(( java.util.HashMap ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 339 */
v0 = this.mModuleNameStrMap;
(( java.util.HashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 340 */
v0 = this.mModuleNameClassObjectMap;
(( java.util.HashMap ) v0 ).put ( p1, v4 ); // invoke-virtual {v0, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 341 */
int v0 = 1; // const/4 v0, 0x1
/* .line 343 */
} // :cond_0
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " parsed classObject is null, please check cloud data and bean properties"; // const-string v6, " parsed classObject is null, please check cloud data and bean properties"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v5 );
/* .line 344 */
} // .end local v4 # "classObject":Ljava/lang/Object;
/* nop */
/* .line 348 */
/* .line 345 */
} // :cond_1
final String v4 = "moduleName or beanPath is null"; // const-string v4, "moduleName or beanPath is null"
android.util.Slog .e ( v3,v4 );
/* .line 346 */
} // .end method
private void writeElementOfModuleListToXml ( org.xmlpull.v1.XmlSerializer p0, java.util.HashMap p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "serializer" # Lorg/xmlpull/v1/XmlSerializer; */
/* .param p3, "tagString" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lorg/xmlpull/v1/XmlSerializer;", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 390 */
/* .local p2, "strMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;" */
/* new-instance v0, Lcom/android/server/cloud/MiuiCommonCloudHelper$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0, p1, p3}, Lcom/android/server/cloud/MiuiCommonCloudHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/cloud/MiuiCommonCloudHelper;Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V */
(( java.util.HashMap ) p2 ).forEach ( v0 ); // invoke-virtual {p2, v0}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 405 */
return;
} // .end method
/* # virtual methods */
public java.lang.Object getDataByModuleName ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "moduleName" # Ljava/lang/String; */
/* .line 413 */
v0 = this.mModuleNameClassObjectMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public void init ( android.content.Context p0, android.os.Handler p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "cloudFilePath" # Ljava/lang/String; */
/* .line 61 */
this.mContext = p1;
/* .line 62 */
/* new-instance v0, Lcom/google/gson/Gson; */
/* invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V */
this.mGson = v0;
/* .line 63 */
this.mHandler = p2;
/* .line 64 */
this.mCloudFilePath = p3;
/* .line 65 */
return;
} // .end method
public void initFromAssets ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "assetsConfigFilePath" # Ljava/lang/String; */
/* .line 74 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/cloud/MiuiCommonCloudHelper$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/cloud/MiuiCommonCloudHelper$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/cloud/MiuiCommonCloudHelper;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 80 */
return;
} // .end method
public void initFromPath ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "configFilePath" # Ljava/lang/String; */
/* .line 89 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/cloud/MiuiCommonCloudHelper$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/cloud/MiuiCommonCloudHelper$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/cloud/MiuiCommonCloudHelper;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 95 */
return;
} // .end method
public void notifyAllObservers ( ) {
/* .locals 2 */
/* .line 436 */
v0 = this.mObservers;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/android/server/MiuiCommonCloudHelperStub$Observer; */
/* .line 437 */
/* .local v1, "observer":Lcom/android/server/MiuiCommonCloudHelperStub$Observer; */
/* .line 438 */
} // .end local v1 # "observer":Lcom/android/server/MiuiCommonCloudHelperStub$Observer;
/* .line 439 */
} // :cond_0
return;
} // .end method
public void registerObserver ( com.android.server.MiuiCommonCloudHelperStub$Observer p0 ) {
/* .locals 1 */
/* .param p1, "observer" # Lcom/android/server/MiuiCommonCloudHelperStub$Observer; */
/* .line 422 */
final String v0 = "observer may not be null"; // const-string v0, "observer may not be null"
java.util.Objects .requireNonNull ( p1,v0 );
/* .line 423 */
v0 = this.mObservers;
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 424 */
v0 = this.mObservers;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 426 */
} // :cond_0
return;
} // .end method
public void startCloudObserve ( ) {
/* .locals 4 */
/* .line 205 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 206 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/cloud/MiuiCommonCloudHelper$1; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/cloud/MiuiCommonCloudHelper$1;-><init>(Lcom/android/server/cloud/MiuiCommonCloudHelper;Landroid/os/Handler;)V */
/* .line 205 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 220 */
return;
} // .end method
public void unregisterObserver ( com.android.server.MiuiCommonCloudHelperStub$Observer p0 ) {
/* .locals 1 */
/* .param p1, "observer" # Lcom/android/server/MiuiCommonCloudHelperStub$Observer; */
/* .line 430 */
final String v0 = "observer may not be null"; // const-string v0, "observer may not be null"
java.util.Objects .requireNonNull ( p1,v0 );
/* .line 431 */
v0 = this.mObservers;
(( java.util.ArrayList ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 432 */
return;
} // .end method
