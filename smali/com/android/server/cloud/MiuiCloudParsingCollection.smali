.class public Lcom/android/server/cloud/MiuiCloudParsingCollection;
.super Ljava/lang/Object;
.source "MiuiCloudParsingCollection.java"


# static fields
.field private static final ATTR_CONFIG_FILE_NAME:Ljava/lang/String; = "attributes_config.xml"

.field private static final CLOUD_BACKUP_FILE_NAME:Ljava/lang/String; = "common_cloud_backup.xml"

.field private static final CLOUD_BACKUP_FILE_ROOT_ELEMENT:Ljava/lang/String; = "common-cloud-config"

.field private static final CLOUD_BACKUP_FILE_TEST_CRT_TAG:Ljava/lang/String; = "cloud-control"

.field private static final CLOUD_CTRL_ITEM_TAG:Ljava/lang/String; = "cloud-data-item"

.field private static final CLOUD_MODULE_BEAN_PATH:Ljava/lang/String; = "moduleBeanPath"

.field private static final CLOUD_MODULE_NAME:Ljava/lang/String; = "moduleName"

.field private static final CLOUD_MODULE_STRING:Ljava/lang/String; = "moduleString"

.field private static final CLOUD_MODULE_VERSION:Ljava/lang/String; = "moduleVersion"

.field private static final MODULE_DATA_VERSION:Ljava/lang/String; = "version"

.field private static final TAG:Ljava/lang/String; = "MiuiCloudParsingCollection"


# instance fields
.field private mAssets:Landroid/content/res/AssetManager;

.field private mCloudBackFile:Ljava/io/File;

.field private mContext:Landroid/content/Context;

.field private mModuleNameBeanClassPathMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mModuleNameClassObjectMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mModuleNameDataVersionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mModuleNameStrMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mObservers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/MiuiCommonCloudServiceStub$Observer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$47XtPO154zNtS6w4EBX6AkGKZHM(Lcom/android/server/cloud/MiuiCloudParsingCollection;Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->lambda$writeElementOfModuleListToXml$1(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$J_xYjwtg7AVUycV1NxfOshkYwc0(Lcom/android/server/cloud/MiuiCloudParsingCollection;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->lambda$new$0()V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameStrMap:Ljava/util/HashMap;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameBeanClassPathMap:Ljava/util/HashMap;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameDataVersionMap:Ljava/util/HashMap;

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameClassObjectMap:Ljava/util/HashMap;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mObservers:Ljava/util/ArrayList;

    .line 56
    iput-object p1, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mContext:Landroid/content/Context;

    .line 57
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/cloud/MiuiCloudParsingCollection$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/cloud/MiuiCloudParsingCollection$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/cloud/MiuiCloudParsingCollection;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 60
    return-void
.end method

.method private checkFileStatus(Ljava/io/File;)V
    .locals 4
    .param p1, "file"    # Ljava/io/File;

    .line 136
    const-string v0, "MiuiCloudParsingCollection"

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 137
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z

    .line 138
    const-string v1, "common_cloud_backup not exist, create file end"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :cond_1
    goto :goto_0

    .line 140
    :catch_0
    move-exception v1

    .line 141
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "create common_cloud_backup fail "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    .end local v1    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method private initialization()V
    .locals 0

    .line 63
    invoke-direct {p0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->loadLocalInitConfig()V

    .line 64
    invoke-direct {p0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->loadLocalCloudBackup()V

    .line 65
    invoke-direct {p0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->parseStringToClassObject()V

    .line 66
    invoke-virtual {p0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->notifyAllObservers()V

    .line 67
    return-void
.end method

.method private synthetic lambda$new$0()V
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->initialization()V

    .line 59
    return-void
.end method

.method private synthetic lambda$writeElementOfModuleListToXml$1(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "serializer"    # Lorg/xmlpull/v1/XmlSerializer;
    .param p2, "tagString"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;
    .param p4, "value"    # Ljava/lang/String;

    .line 270
    if-eqz p4, :cond_0

    .line 272
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p1, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 273
    const-string v1, "moduleName"

    invoke-interface {p1, v0, v1, p3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 274
    const-string v1, "moduleVersion"

    iget-object v2, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameDataVersionMap:Ljava/util/HashMap;

    invoke-virtual {v2, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 275
    const-string v1, "moduleBeanPath"

    iget-object v2, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameBeanClassPathMap:Ljava/util/HashMap;

    invoke-virtual {v2, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 276
    const-string v1, "moduleString"

    invoke-interface {p1, v0, v1, p4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 277
    invoke-interface {p1, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    goto :goto_0

    .line 278
    :catch_0
    move-exception v0

    .line 279
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to write element of app list to xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiCloudParsingCollection"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 283
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    return-void
.end method

.method private loadLocalCloudBackup()V
    .locals 3

    .line 83
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataSystemDeDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "common_cloud_backup.xml"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mCloudBackFile:Ljava/io/File;

    .line 84
    invoke-direct {p0, v0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->checkFileStatus(Ljava/io/File;)V

    .line 85
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mCloudBackFile:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    .local v0, "inputStream":Ljava/io/FileInputStream;
    nop

    .line 87
    :try_start_1
    invoke-direct {p0, v0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->loadLocalFile(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 93
    .end local v0    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_1

    .line 85
    .restart local v0    # "inputStream":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v2

    :try_start_4
    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/cloud/MiuiCloudParsingCollection;
    :goto_0
    throw v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 91
    .end local v0    # "inputStream":Ljava/io/FileInputStream;
    .restart local p0    # "this":Lcom/android/server/cloud/MiuiCloudParsingCollection;
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MiuiCloudParsingCollection"

    const-string v2, "Failed to read common_cloud_backup.xml "

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 94
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private loadLocalFile(Ljava/io/InputStream;)V
    .locals 13
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 102
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 103
    .local v0, "parser":Lorg/xmlpull/v1/XmlPullParser;
    sget-object v1, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v1}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 104
    const/4 v1, 0x0

    .line 105
    .local v1, "tagName":Ljava/lang/String;
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    .line 106
    .local v2, "eventType":I
    :goto_0
    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    .line 107
    packed-switch v2, :pswitch_data_0

    goto/16 :goto_1

    .line 109
    :pswitch_0
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 110
    const-string v3, "cloud-data-item"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 111
    const-string v3, "moduleName"

    const/4 v4, 0x0

    invoke-interface {v0, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 112
    .local v3, "moduleName":Ljava/lang/String;
    const-string v5, "moduleVersion"

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 113
    .local v5, "moduleVersion":Ljava/lang/String;
    const-string v6, "moduleBeanPath"

    invoke-interface {v0, v4, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 114
    .local v6, "moduleBeanPath":Ljava/lang/String;
    const-string v7, "moduleString"

    invoke-interface {v0, v4, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 115
    .local v4, "moduleString":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 116
    .local v7, "lastStoredVersion":Ljava/lang/Long;
    iget-object v8, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameDataVersionMap:Ljava/util/HashMap;

    const-string v9, "-1"

    invoke-virtual {v8, v3, v9}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 117
    .local v8, "initVersion":Ljava/lang/Long;
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    cmp-long v9, v9, v11

    if-lez v9, :cond_0

    .line 118
    iget-object v9, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameStrMap:Ljava/util/HashMap;

    invoke-virtual {v9, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    iget-object v9, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameDataVersionMap:Ljava/util/HashMap;

    invoke-virtual {v9, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    iget-object v9, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameBeanClassPathMap:Ljava/util/HashMap;

    invoke-virtual {v9, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "last stored cloud data version: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is higher than init data version; "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", so use last stored cloud data"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "MiuiCloudParsingCollection"

    invoke-static {v10, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    .end local v3    # "moduleName":Ljava/lang/String;
    .end local v4    # "moduleString":Ljava/lang/String;
    .end local v5    # "moduleVersion":Ljava/lang/String;
    .end local v6    # "moduleBeanPath":Ljava/lang/String;
    .end local v7    # "lastStoredVersion":Ljava/lang/Long;
    .end local v8    # "initVersion":Ljava/lang/Long;
    :cond_0
    nop

    .line 130
    :cond_1
    :goto_1
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    goto/16 :goto_0

    .line 132
    :cond_2
    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private loadLocalInitConfig()V
    .locals 4

    .line 70
    const-string v0, "MiuiCloudParsingCollection"

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mAssets:Landroid/content/res/AssetManager;

    .line 71
    :try_start_0
    const-string v2, "attributes_config.xml"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    .local v1, "inputStream":Ljava/io/InputStream;
    if-eqz v1, :cond_0

    .line 73
    :try_start_1
    invoke-direct {p0, v1}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->loadLocalFile(Ljava/io/InputStream;)V

    goto :goto_0

    .line 71
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 75
    :cond_0
    const-string v2, "file attributes_config not exits "

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    :goto_0
    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 79
    .end local v1    # "inputStream":Ljava/io/InputStream;
    :cond_1
    goto :goto_3

    .line 71
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    :goto_1
    if-eqz v1, :cond_2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/cloud/MiuiCloudParsingCollection;
    :cond_2
    :goto_2
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 77
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .restart local p0    # "this":Lcom/android/server/cloud/MiuiCloudParsingCollection;
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "Failed to read attributes_config.xml "

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 80
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_3
    return-void
.end method

.method private parseStringToClassObject()V
    .locals 6

    .line 146
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameStrMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 147
    .local v1, "moduleName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 148
    .local v2, "classObj":Ljava/lang/Object;
    iget-object v3, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameStrMap:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 149
    .local v3, "data":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameBeanClassPathMap:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 151
    .local v4, "beanPath":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-eqz v4, :cond_0

    .line 152
    invoke-virtual {p0, v3, v4}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->parseStringToClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 154
    :cond_0
    if-eqz v2, :cond_1

    .line 155
    iget-object v5, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameClassObjectMap:Ljava/util/HashMap;

    invoke-virtual {v5, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    .end local v1    # "moduleName":Ljava/lang/String;
    .end local v2    # "classObj":Ljava/lang/Object;
    .end local v3    # "data":Ljava/lang/String;
    .end local v4    # "beanPath":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 158
    :cond_2
    return-void
.end method

.method private updateCloudData(Ljava/lang/String;)Z
    .locals 9
    .param p1, "moduleName"    # Ljava/lang/String;

    .line 190
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameDataVersionMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 191
    .local v0, "latestVersion":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "version"

    const/4 v3, 0x0

    invoke-static {v1, p1, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 193
    .local v1, "remoteVersion":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mContext:Landroid/content/Context;

    .line 194
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataList(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 196
    .local v2, "cloudDataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    const/4 v3, 0x0

    const-string v4, "MiuiCloudParsingCollection"

    if-nez v1, :cond_0

    .line 197
    const-string v5, " remoteVersion is null, Don\'t update "

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    return v3

    .line 200
    :cond_0
    if-eqz v0, :cond_1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-lez v5, :cond_1

    .line 201
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " moduleName: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", lastVersion: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", remoteVersion: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", so update"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    invoke-direct {p0, p1, v1, v2}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->updateMultiMap(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v3

    return v3

    .line 205
    :cond_1
    return v3
.end method

.method private updateMultiMap(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z
    .locals 7
    .param p1, "moduleName"    # Ljava/lang/String;
    .param p2, "remoteVersion"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;",
            ">;)Z"
        }
    .end annotation

    .line 217
    .local p3, "cloudDataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    const-string v0, "MiuiCloudParsingCollection"

    const/4 v1, 0x0

    if-nez p3, :cond_0

    .line 218
    const-string v2, "cloudDataList is null, see early log for detail"

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    return v1

    .line 221
    :cond_0
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    invoke-virtual {v2}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;

    move-result-object v2

    .line 222
    .local v2, "cloudData":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameBeanClassPathMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 223
    .local v3, "beanPath":Ljava/lang/String;
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 224
    invoke-virtual {p0, v2, v3}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->parseStringToClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 225
    .local v4, "classObject":Ljava/lang/Object;
    if-eqz v4, :cond_1

    .line 226
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameDataVersionMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameStrMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameClassObjectMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    const/4 v0, 0x1

    return v0

    .line 231
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " parsed classObject is null, check cloud data and Bean properties"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    .end local v4    # "classObject":Ljava/lang/Object;
    nop

    .line 236
    return v1

    .line 233
    :cond_2
    const-string v4, "moduleName or beanPath is null"

    invoke-static {v0, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    return v1
.end method

.method private writeElementOfModuleListToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 1
    .param p1, "serializer"    # Lorg/xmlpull/v1/XmlSerializer;
    .param p3, "tagString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlSerializer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 269
    .local p2, "strMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lcom/android/server/cloud/MiuiCloudParsingCollection$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1, p3}, Lcom/android/server/cloud/MiuiCloudParsingCollection$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/cloud/MiuiCloudParsingCollection;Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 284
    return-void
.end method


# virtual methods
.method public getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "moduleName"    # Ljava/lang/String;

    .line 287
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameClassObjectMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getStringByModuleName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "moduleName"    # Ljava/lang/String;

    .line 291
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameStrMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public notifyAllObservers()V
    .locals 2

    .line 307
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/MiuiCommonCloudServiceStub$Observer;

    .line 308
    .local v1, "observer":Lcom/android/server/MiuiCommonCloudServiceStub$Observer;
    invoke-virtual {v1}, Lcom/android/server/MiuiCommonCloudServiceStub$Observer;->update()V

    .line 309
    .end local v1    # "observer":Lcom/android/server/MiuiCommonCloudServiceStub$Observer;
    goto :goto_0

    .line 310
    :cond_0
    return-void
.end method

.method public parseStringToClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "classPath"    # Ljava/lang/String;

    .line 162
    :try_start_0
    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 163
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p1, v0}, Lcom/android/server/cloud/JsonUtil;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 164
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "parseStringToClass exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiCloudParsingCollection"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    const/4 v1, 0x0

    return-object v1
.end method

.method public registerObserver(Lcom/android/server/MiuiCommonCloudServiceStub$Observer;)V
    .locals 1
    .param p1, "observer"    # Lcom/android/server/MiuiCommonCloudServiceStub$Observer;

    .line 295
    const-string v0, "observer may not be null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 296
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 299
    :cond_0
    return-void
.end method

.method public syncLocalBackupFromCloud()V
    .locals 10

    .line 240
    const-string v0, "cloud-control"

    const-string v1, "common-cloud-config"

    const-string v2, "MiuiCloudParsingCollection"

    new-instance v3, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataSystemDeDirectory()Ljava/io/File;

    move-result-object v4

    const-string v5, "common_cloud_backup.xml"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mCloudBackFile:Ljava/io/File;

    .line 241
    invoke-direct {p0, v3}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->checkFileStatus(Ljava/io/File;)V

    .line 242
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    .line 243
    .local v3, "startTime":J
    :try_start_0
    new-instance v5, Ljava/io/BufferedOutputStream;

    new-instance v6, Ljava/io/FileOutputStream;

    iget-object v7, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mCloudBackFile:Ljava/io/File;

    invoke-direct {v6, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v5, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    .local v5, "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream;
    :try_start_1
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v6

    .line 245
    .local v6, "serializer":Lorg/xmlpull/v1/XmlSerializer;
    sget-object v7, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v7}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 246
    sget-object v7, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v7}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-interface {v6, v7, v9}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 247
    const-string v7, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v6, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 248
    const/4 v7, 0x0

    invoke-interface {v6, v7, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 250
    invoke-interface {v6, v7, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 251
    iget-object v8, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameStrMap:Ljava/util/HashMap;

    const-string v9, "cloud-data-item"

    invoke-direct {p0, v6, v8, v9}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->writeElementOfModuleListToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;Ljava/lang/String;)V

    .line 252
    invoke-interface {v6, v7, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 254
    invoke-interface {v6, v7, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 255
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 256
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->flush()V

    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Write common_cloud_backup.xml took "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 258
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v3

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 257
    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 259
    .end local v6    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    :try_start_2
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 265
    .end local v5    # "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream;
    goto :goto_1

    .line 243
    .restart local v5    # "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream;
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v1

    :try_start_4
    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v3    # "startTime":J
    .end local p0    # "this":Lcom/android/server/cloud/MiuiCloudParsingCollection;
    :goto_0
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 259
    .end local v5    # "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v3    # "startTime":J
    .restart local p0    # "this":Lcom/android/server/cloud/MiuiCloudParsingCollection;
    :catch_0
    move-exception v0

    .line 260
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mCloudBackFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mCloudBackFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 261
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to clean up mangled file: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mCloudBackFile:Ljava/io/File;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_0
    const-string v1, "Unable to write cloud backup xml,current changes will be lost at reboot"

    invoke-static {v2, v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 266
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    return-void
.end method

.method public unregisterObserver(Lcom/android/server/MiuiCommonCloudServiceStub$Observer;)V
    .locals 1
    .param p1, "observer"    # Lcom/android/server/MiuiCommonCloudServiceStub$Observer;

    .line 302
    const-string v0, "observer may not be null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 303
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 304
    return-void
.end method

.method updateDataFromCloud()Z
    .locals 4

    .line 176
    const/4 v0, 0x0

    .line 177
    .local v0, "changed":Z
    iget-object v1, p0, Lcom/android/server/cloud/MiuiCloudParsingCollection;->mModuleNameStrMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 178
    .local v2, "moduleName":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->updateCloudData(Ljava/lang/String;)Z

    move-result v3

    or-int/2addr v0, v3

    .line 179
    .end local v2    # "moduleName":Ljava/lang/String;
    goto :goto_0

    .line 180
    :cond_0
    return v0
.end method
