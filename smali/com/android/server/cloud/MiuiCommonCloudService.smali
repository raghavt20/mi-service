.class public Lcom/android/server/cloud/MiuiCommonCloudService;
.super Lcom/android/server/MiuiCommonCloudServiceStub;
.source "MiuiCommonCloudService.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.MiuiCommonCloudServiceStub$$"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiCommonCloudService"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMiuiCloudParsingCollection:Lcom/android/server/cloud/MiuiCloudParsingCollection;


# direct methods
.method static bridge synthetic -$$Nest$fgetmMiuiCloudParsingCollection(Lcom/android/server/cloud/MiuiCommonCloudService;)Lcom/android/server/cloud/MiuiCloudParsingCollection;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cloud/MiuiCommonCloudService;->mMiuiCloudParsingCollection:Lcom/android/server/cloud/MiuiCloudParsingCollection;

    return-object p0
.end method

.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/android/server/MiuiCommonCloudServiceStub;-><init>()V

    return-void
.end method

.method private registerCloudDataObserver()V
    .locals 4

    .line 27
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 28
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/cloud/MiuiCommonCloudService$1;

    .line 29
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/server/cloud/MiuiCommonCloudService$1;-><init>(Lcom/android/server/cloud/MiuiCommonCloudService;Landroid/os/Handler;)V

    .line 27
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 42
    return-void
.end method


# virtual methods
.method public getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "moduleName"    # Ljava/lang/String;

    .line 70
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudService;->mMiuiCloudParsingCollection:Lcom/android/server/cloud/MiuiCloudParsingCollection;

    invoke-virtual {v0, p1}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getStringByModuleName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "moduleName"    # Ljava/lang/String;

    .line 80
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudService;->mMiuiCloudParsingCollection:Lcom/android/server/cloud/MiuiCloudParsingCollection;

    invoke-virtual {v0, p1}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->getStringByModuleName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 21
    iput-object p1, p0, Lcom/android/server/cloud/MiuiCommonCloudService;->mContext:Landroid/content/Context;

    .line 22
    new-instance v0, Lcom/android/server/cloud/MiuiCloudParsingCollection;

    invoke-direct {v0, p1}, Lcom/android/server/cloud/MiuiCloudParsingCollection;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudService;->mMiuiCloudParsingCollection:Lcom/android/server/cloud/MiuiCloudParsingCollection;

    .line 23
    invoke-direct {p0}, Lcom/android/server/cloud/MiuiCommonCloudService;->registerCloudDataObserver()V

    .line 24
    return-void
.end method

.method public registerObserver(Lcom/android/server/MiuiCommonCloudServiceStub$Observer;)V
    .locals 1
    .param p1, "observer"    # Lcom/android/server/MiuiCommonCloudServiceStub$Observer;

    .line 51
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudService;->mMiuiCloudParsingCollection:Lcom/android/server/cloud/MiuiCloudParsingCollection;

    invoke-virtual {v0, p1}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->registerObserver(Lcom/android/server/MiuiCommonCloudServiceStub$Observer;)V

    .line 52
    return-void
.end method

.method public unregisterObserver(Lcom/android/server/MiuiCommonCloudServiceStub$Observer;)V
    .locals 1
    .param p1, "observer"    # Lcom/android/server/MiuiCommonCloudServiceStub$Observer;

    .line 60
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudService;->mMiuiCloudParsingCollection:Lcom/android/server/cloud/MiuiCloudParsingCollection;

    invoke-virtual {v0, p1}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->unregisterObserver(Lcom/android/server/MiuiCommonCloudServiceStub$Observer;)V

    .line 61
    return-void
.end method
