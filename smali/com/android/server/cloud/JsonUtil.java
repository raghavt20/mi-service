public class com.android.server.cloud.JsonUtil {
	 /* .source "JsonUtil.java" */
	 /* # static fields */
	 private static com.google.gson.Gson sGson;
	 /* # direct methods */
	 private com.android.server.cloud.JsonUtil ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 13 */
		 return;
	 } // .end method
	 public static java.lang.Object fromJson ( java.lang.String p0, java.lang.Class p1 ) {
		 /* .locals 2 */
		 /* .param p0, "data" # Ljava/lang/String; */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "<T:", */
		 /* "Ljava/lang/Object;", */
		 /* ">(", */
		 /* "Ljava/lang/String;", */
		 /* "Ljava/lang/Class<", */
		 /* "TT;>;)TT;" */
		 /* } */
	 } // .end annotation
	 /* .line 28 */
	 /* .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
	 try { // :try_start_0
		 com.android.server.cloud.JsonUtil .getInstance ( );
		 (( com.google.gson.Gson ) v0 ).fromJson ( p0, p1 ); // invoke-virtual {v0, p0, p1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 29 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 30 */
		 /* .local v0, "e":Ljava/lang/Exception; */
		 int v1 = 0; // const/4 v1, 0x0
	 } // .end method
	 public static synchronized com.google.gson.Gson getInstance ( ) {
		 /* .locals 2 */
		 /* const-class v0, Lcom/android/server/cloud/JsonUtil; */
		 /* monitor-enter v0 */
		 /* .line 16 */
		 try { // :try_start_0
			 v1 = com.android.server.cloud.JsonUtil.sGson;
			 /* if-nez v1, :cond_0 */
			 /* .line 17 */
			 /* new-instance v1, Lcom/google/gson/Gson; */
			 /* invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V */
			 /* .line 19 */
		 } // :cond_0
		 v1 = com.android.server.cloud.JsonUtil.sGson;
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* monitor-exit v0 */
		 /* .line 15 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* throw v1 */
	 } // .end method
	 public static java.lang.String toJson ( java.lang.Object p0 ) {
		 /* .locals 1 */
		 /* .param p0, "object" # Ljava/lang/Object; */
		 /* .line 23 */
		 com.android.server.cloud.JsonUtil .getInstance ( );
		 (( com.google.gson.Gson ) v0 ).toJson ( p0 ); // invoke-virtual {v0, p0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;
	 } // .end method
