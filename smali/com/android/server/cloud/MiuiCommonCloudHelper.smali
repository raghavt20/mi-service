.class public Lcom/android/server/cloud/MiuiCommonCloudHelper;
.super Lcom/android/server/MiuiCommonCloudHelperStub;
.source "MiuiCommonCloudHelper.java"


# static fields
.field private static final CLOUD_BACKUP_FILE_ROOT_ELEMENT:Ljava/lang/String; = "common-cloud-config"

.field private static final CLOUD_BACKUP_FILE_TEST_CRT_TAG:Ljava/lang/String; = "cloud-control"

.field private static final CLOUD_CTRL_ITEM_TAG:Ljava/lang/String; = "cloud-data-item"

.field private static final CLOUD_MODULE_BEAN_PATH:Ljava/lang/String; = "moduleBeanPath"

.field private static final CLOUD_MODULE_NAME:Ljava/lang/String; = "moduleName"

.field private static final CLOUD_MODULE_STRING:Ljava/lang/String; = "moduleString"

.field private static final CLOUD_MODULE_VERSION:Ljava/lang/String; = "moduleVersion"

.field private static final MODULE_DATA_VERSION:Ljava/lang/String; = "mVersion"

.field private static final TAG:Ljava/lang/String; = "MiuiCommonCloudHelper"


# instance fields
.field private mCloudFilePath:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mGson:Lcom/google/gson/Gson;

.field private mHandler:Landroid/os/Handler;

.field private mModuleNameBeanClassPathMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mModuleNameClassObjectMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mModuleNameDataVersionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mModuleNameStrMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mObservers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/MiuiCommonCloudHelperStub$Observer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$gw7JyJp4wE5WoHhbNB6P0gdNhd0(Lcom/android/server/cloud/MiuiCommonCloudHelper;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->lambda$initFromPath$1(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$vM_H3z5Sj_pN1ngB3s5ZI2VZhEY(Lcom/android/server/cloud/MiuiCommonCloudHelper;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->lambda$initFromAssets$0(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$wQgFWhUNh9RV7pB82MdT48oBvWI(Lcom/android/server/cloud/MiuiCommonCloudHelper;Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->lambda$writeElementOfModuleListToXml$2(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmCloudFilePath(Lcom/android/server/cloud/MiuiCommonCloudHelper;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mCloudFilePath:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$msyncLocalBackupFromCloud(Lcom/android/server/cloud/MiuiCommonCloudHelper;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->syncLocalBackupFromCloud(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateDataFromCloud(Lcom/android/server/cloud/MiuiCommonCloudHelper;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->updateDataFromCloud()Z

    move-result p0

    return p0
.end method

.method public constructor <init>()V
    .locals 1

    .line 35
    invoke-direct {p0}, Lcom/android/server/MiuiCommonCloudHelperStub;-><init>()V

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameStrMap:Ljava/util/HashMap;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameBeanClassPathMap:Ljava/util/HashMap;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameDataVersionMap:Ljava/util/HashMap;

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameClassObjectMap:Ljava/util/HashMap;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mObservers:Ljava/util/ArrayList;

    return-void
.end method

.method private checkFileStatus(Ljava/io/File;)V
    .locals 4
    .param p1, "file"    # Ljava/io/File;

    .line 191
    const-string v0, "MiuiCommonCloudHelper"

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 192
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z

    .line 193
    const-string v1, "Local cloud_backup file dose not exist, create file end"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :cond_1
    goto :goto_0

    .line 195
    :catch_0
    move-exception v1

    .line 196
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Create local cloud_backup file fail "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    .end local v1    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method private getStringByModuleName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "moduleName"    # Ljava/lang/String;

    .line 417
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameStrMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private synthetic lambda$initFromAssets$0(Ljava/lang/String;)V
    .locals 1
    .param p1, "assetsConfigFilePath"    # Ljava/lang/String;

    .line 75
    invoke-direct {p0, p1}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->loadLocalInitConfigFromAssets(Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mCloudFilePath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->loadLocalCloudBackup(Ljava/lang/String;)V

    .line 77
    invoke-direct {p0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->parseStringToClassObject()V

    .line 78
    invoke-virtual {p0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->notifyAllObservers()V

    .line 79
    return-void
.end method

.method private synthetic lambda$initFromPath$1(Ljava/lang/String;)V
    .locals 1
    .param p1, "configFilePath"    # Ljava/lang/String;

    .line 90
    invoke-direct {p0, p1}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->loadLocalInitConfigFromPath(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mCloudFilePath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->loadLocalCloudBackup(Ljava/lang/String;)V

    .line 92
    invoke-direct {p0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->parseStringToClassObject()V

    .line 93
    invoke-virtual {p0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->notifyAllObservers()V

    .line 94
    return-void
.end method

.method private synthetic lambda$writeElementOfModuleListToXml$2(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "serializer"    # Lorg/xmlpull/v1/XmlSerializer;
    .param p2, "tagString"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;
    .param p4, "value"    # Ljava/lang/String;

    .line 391
    if-eqz p4, :cond_0

    .line 393
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p1, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 394
    const-string v1, "moduleName"

    invoke-interface {p1, v0, v1, p3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 395
    const-string v1, "moduleVersion"

    iget-object v2, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameDataVersionMap:Ljava/util/HashMap;

    invoke-virtual {v2, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 396
    const-string v1, "moduleBeanPath"

    iget-object v2, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameBeanClassPathMap:Ljava/util/HashMap;

    invoke-virtual {v2, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 397
    const-string v1, "moduleString"

    invoke-interface {p1, v0, v1, p4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 398
    invoke-interface {p1, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 402
    goto :goto_0

    .line 399
    :catch_0
    move-exception v0

    .line 400
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to write element of app list to xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiCommonCloudHelper"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 404
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    return-void
.end method

.method private loadLocalCloudBackup(Ljava/lang/String;)V
    .locals 4
    .param p1, "cloudPath"    # Ljava/lang/String;

    .line 139
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 140
    .local v0, "mCloudBackFile":Ljava/io/File;
    invoke-direct {p0, v0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->checkFileStatus(Ljava/io/File;)V

    .line 141
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    .local v1, "inputStream":Ljava/io/FileInputStream;
    nop

    .line 143
    :try_start_1
    invoke-direct {p0, v1}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->loadLocalFile(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 149
    .end local v1    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_1

    .line 141
    .restart local v1    # "inputStream":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "mCloudBackFile":Ljava/io/File;
    .end local p0    # "this":Lcom/android/server/cloud/MiuiCommonCloudHelper;
    .end local p1    # "cloudPath":Ljava/lang/String;
    :goto_0
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 147
    .end local v1    # "inputStream":Ljava/io/FileInputStream;
    .restart local v0    # "mCloudBackFile":Ljava/io/File;
    .restart local p0    # "this":Lcom/android/server/cloud/MiuiCommonCloudHelper;
    .restart local p1    # "cloudPath":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 148
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "MiuiCommonCloudHelper"

    const-string v3, "Failed to read local cloud_backup file "

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 150
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private loadLocalFile(Ljava/io/InputStream;)V
    .locals 13
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 158
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 159
    .local v0, "parser":Lorg/xmlpull/v1/XmlPullParser;
    sget-object v1, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v1}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 160
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    .line 161
    .local v1, "eventType":I
    :goto_0
    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    .line 162
    packed-switch v1, :pswitch_data_0

    goto/16 :goto_1

    .line 164
    :pswitch_0
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 165
    .local v2, "tagName":Ljava/lang/String;
    const-string v3, "cloud-data-item"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 166
    const-string v3, "moduleName"

    const/4 v4, 0x0

    invoke-interface {v0, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 167
    .local v3, "moduleName":Ljava/lang/String;
    const-string v5, "moduleVersion"

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 168
    .local v5, "moduleVersion":Ljava/lang/String;
    const-string v6, "moduleBeanPath"

    invoke-interface {v0, v4, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 169
    .local v6, "moduleBeanPath":Ljava/lang/String;
    const-string v7, "moduleString"

    invoke-interface {v0, v4, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 170
    .local v4, "moduleString":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 171
    .local v7, "lastStoredVersion":Ljava/lang/Long;
    iget-object v8, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameDataVersionMap:Ljava/util/HashMap;

    const-string v9, "-1"

    invoke-virtual {v8, v3, v9}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 172
    .local v8, "initVersion":Ljava/lang/Long;
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    cmp-long v9, v9, v11

    if-lez v9, :cond_0

    .line 173
    iget-object v9, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameStrMap:Ljava/util/HashMap;

    invoke-virtual {v9, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    iget-object v9, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameDataVersionMap:Ljava/util/HashMap;

    invoke-virtual {v9, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    iget-object v9, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameBeanClassPathMap:Ljava/util/HashMap;

    invoke-virtual {v9, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Last stored cloud data version: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is higher than init data version: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", so use last stored cloud data"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "MiuiCommonCloudHelper"

    invoke-static {v10, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    .end local v3    # "moduleName":Ljava/lang/String;
    .end local v4    # "moduleString":Ljava/lang/String;
    .end local v5    # "moduleVersion":Ljava/lang/String;
    .end local v6    # "moduleBeanPath":Ljava/lang/String;
    .end local v7    # "lastStoredVersion":Ljava/lang/Long;
    .end local v8    # "initVersion":Ljava/lang/Long;
    :cond_0
    nop

    .line 185
    .end local v2    # "tagName":Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    goto/16 :goto_0

    .line 187
    :cond_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private loadLocalInitConfigFromAssets(Ljava/lang/String;)V
    .locals 5
    .param p1, "assetsConfigFilePath"    # Ljava/lang/String;

    .line 103
    const-string v0, "MiuiCommonCloudHelper"

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 104
    .local v1, "assetManager":Landroid/content/res/AssetManager;
    :try_start_0
    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    .local v2, "inputStream":Ljava/io/InputStream;
    if-eqz v2, :cond_0

    .line 106
    :try_start_1
    invoke-direct {p0, v2}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->loadLocalFile(Ljava/io/InputStream;)V

    goto :goto_0

    .line 104
    :catchall_0
    move-exception v3

    goto :goto_1

    .line 108
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Local init config file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " does not exits "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 110
    :goto_0
    if-eqz v2, :cond_1

    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 112
    .end local v2    # "inputStream":Ljava/io/InputStream;
    :cond_1
    goto :goto_3

    .line 104
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    :goto_1
    if-eqz v2, :cond_2

    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "assetManager":Landroid/content/res/AssetManager;
    .end local p0    # "this":Lcom/android/server/cloud/MiuiCommonCloudHelper;
    .end local p1    # "assetsConfigFilePath":Ljava/lang/String;
    :cond_2
    :goto_2
    throw v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 110
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "assetManager":Landroid/content/res/AssetManager;
    .restart local p0    # "this":Lcom/android/server/cloud/MiuiCommonCloudHelper;
    .restart local p1    # "assetsConfigFilePath":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 111
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to read local init config file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 113
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_3
    return-void
.end method

.method private loadLocalInitConfigFromPath(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .line 122
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 123
    .local v0, "inputStream":Ljava/io/InputStream;
    nop

    .line 124
    invoke-direct {p0, v0}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->loadLocalFile(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    .end local v0    # "inputStream":Ljava/io/InputStream;
    goto :goto_0

    .line 128
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to read local init config file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiCommonCloudHelper"

    invoke-static {v2, v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 131
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private parseStringToClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "classPath"    # Ljava/lang/String;

    .line 249
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string/jumbo v0, "short"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_1
    const-string v0, "float"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_2
    const-string v0, "boolean"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_3
    const-string v0, "long"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_4
    const-string v0, "char"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_5
    const-string v0, "byte"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_6
    const-string v0, "int"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_7
    const-string v0, "double"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto :goto_1

    :sswitch_8
    const-string v0, "String"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 278
    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_2

    .line 275
    :pswitch_0
    const-class v0, Ljava/lang/String;

    .line 276
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_2

    .line 272
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :pswitch_1
    sget-object v0, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    .line 273
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_2

    .line 269
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :pswitch_2
    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    .line 270
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_2

    .line 266
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :pswitch_3
    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    .line 267
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_2

    .line 263
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :pswitch_4
    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    .line 264
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_2

    .line 260
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :pswitch_5
    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    .line 261
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_2

    .line 257
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :pswitch_6
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 258
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_2

    .line 254
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :pswitch_7
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 255
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_2

    .line 251
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :pswitch_8
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    .line 252
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    nop

    .line 280
    :goto_2
    iget-object v1, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mGson:Lcom/google/gson/Gson;

    invoke-virtual {v1, p1, v0}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 281
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_0
    move-exception v0

    .line 282
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "parseStringToClass exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiCommonCloudHelper"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    const/4 v1, 0x0

    return-object v1

    :sswitch_data_0
    .sparse-switch
        -0x6bc5b3cf -> :sswitch_8
        -0x4f08842f -> :sswitch_7
        0x197ef -> :sswitch_6
        0x2e6108 -> :sswitch_5
        0x2e9356 -> :sswitch_4
        0x32c67c -> :sswitch_3
        0x3db6c28 -> :sswitch_2
        0x5d0225c -> :sswitch_1
        0x685847c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private parseStringToClassObject()V
    .locals 6

    .line 226
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameStrMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 227
    .local v1, "moduleName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 228
    .local v2, "classObj":Ljava/lang/Object;
    iget-object v3, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameStrMap:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 229
    .local v3, "data":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameBeanClassPathMap:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 230
    .local v4, "beanPath":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-eqz v4, :cond_0

    .line 231
    invoke-direct {p0, v3, v4}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->parseStringToClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 233
    :cond_0
    if-eqz v2, :cond_1

    .line 234
    iget-object v5, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameClassObjectMap:Ljava/util/HashMap;

    invoke-virtual {v5, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    .end local v1    # "moduleName":Ljava/lang/String;
    .end local v2    # "classObj":Ljava/lang/Object;
    .end local v3    # "data":Ljava/lang/String;
    .end local v4    # "beanPath":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 237
    :cond_2
    return-void
.end method

.method private syncLocalBackupFromCloud(Ljava/lang/String;)V
    .locals 11
    .param p1, "cloudFilePath"    # Ljava/lang/String;

    .line 357
    const-string v0, "cloud-control"

    const-string v1, "common-cloud-config"

    const-string v2, "MiuiCommonCloudHelper"

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 358
    .local v3, "mCloudBackFile":Ljava/io/File;
    invoke-direct {p0, v3}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->checkFileStatus(Ljava/io/File;)V

    .line 359
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 360
    .local v4, "startTime":J
    :try_start_0
    new-instance v6, Ljava/io/BufferedOutputStream;

    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v6, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 361
    .local v6, "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream;
    :try_start_1
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v7

    .line 362
    .local v7, "serializer":Lorg/xmlpull/v1/XmlSerializer;
    sget-object v8, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v8}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v6, v8}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 363
    sget-object v8, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v8}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-interface {v7, v8, v10}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 364
    const-string v8, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 365
    const/4 v8, 0x0

    invoke-interface {v7, v8, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 366
    invoke-interface {v7, v8, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 367
    iget-object v9, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameStrMap:Ljava/util/HashMap;

    const-string v10, "cloud-data-item"

    invoke-direct {p0, v7, v9, v10}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->writeElementOfModuleListToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;Ljava/lang/String;)V

    .line 368
    invoke-interface {v7, v8, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 369
    invoke-interface {v7, v8, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 370
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 371
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->flush()V

    .line 372
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Write local cloud_backup file took "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 373
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v4

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 372
    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 374
    .end local v7    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    :try_start_2
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 379
    .end local v6    # "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream;
    goto :goto_1

    .line 360
    .restart local v6    # "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream;
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v1

    :try_start_4
    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v3    # "mCloudBackFile":Ljava/io/File;
    .end local v4    # "startTime":J
    .end local p0    # "this":Lcom/android/server/cloud/MiuiCommonCloudHelper;
    .end local p1    # "cloudFilePath":Ljava/lang/String;
    :goto_0
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 374
    .end local v6    # "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v3    # "mCloudBackFile":Ljava/io/File;
    .restart local v4    # "startTime":J
    .restart local p0    # "this":Lcom/android/server/cloud/MiuiCommonCloudHelper;
    .restart local p1    # "cloudFilePath":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 375
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 376
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to clean up mangled file: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    :cond_0
    const-string v1, "Unable to write local cloud_backup file"

    invoke-static {v2, v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 380
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    return-void
.end method

.method private updateCloudData(Ljava/lang/String;)Z
    .locals 9
    .param p1, "moduleName"    # Ljava/lang/String;

    .line 307
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameDataVersionMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 308
    .local v0, "latestVersion":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "mVersion"

    const/4 v3, 0x0

    invoke-static {v1, p1, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 310
    .local v1, "remoteVersion":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mContext:Landroid/content/Context;

    .line 311
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataList(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 312
    .local v2, "cloudDataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    const/4 v3, 0x0

    const-string v4, "MiuiCommonCloudHelper"

    if-nez v1, :cond_0

    .line 313
    const-string v5, " remoteVersion is null, don\'t update "

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    return v3

    .line 316
    :cond_0
    if-eqz v0, :cond_1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-lez v5, :cond_1

    .line 317
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " moduleName: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", lastVersion: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", remoteVersion: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", so update"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    invoke-direct {p0, p1, v1, v2}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->updateModuleData(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v3

    return v3

    .line 321
    :cond_1
    return v3
.end method

.method private updateDataFromCloud()Z
    .locals 4

    .line 293
    const/4 v0, 0x0

    .line 294
    .local v0, "changed":Z
    iget-object v1, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameStrMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 295
    .local v2, "moduleName":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->updateCloudData(Ljava/lang/String;)Z

    move-result v3

    or-int/2addr v0, v3

    .line 296
    .end local v2    # "moduleName":Ljava/lang/String;
    goto :goto_0

    .line 297
    :cond_0
    return v0
.end method

.method private updateModuleData(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z
    .locals 7
    .param p1, "moduleName"    # Ljava/lang/String;
    .param p2, "remoteVersion"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;",
            ">;)Z"
        }
    .end annotation

    .line 333
    .local p3, "cloudDataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;

    move-result-object v1

    .line 334
    .local v1, "cloudData":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameBeanClassPathMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 335
    .local v2, "beanPath":Ljava/lang/String;
    const-string v3, "MiuiCommonCloudHelper"

    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    .line 336
    invoke-direct {p0, v1, v2}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->parseStringToClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 337
    .local v4, "classObject":Ljava/lang/Object;
    if-eqz v4, :cond_0

    .line 338
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameDataVersionMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameStrMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameClassObjectMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    const/4 v0, 0x1

    return v0

    .line 343
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " parsed classObject is null, please check cloud data and bean properties"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    .end local v4    # "classObject":Ljava/lang/Object;
    nop

    .line 348
    return v0

    .line 345
    :cond_1
    const-string v4, "moduleName or beanPath is null"

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    return v0
.end method

.method private writeElementOfModuleListToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 1
    .param p1, "serializer"    # Lorg/xmlpull/v1/XmlSerializer;
    .param p3, "tagString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlSerializer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 390
    .local p2, "strMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lcom/android/server/cloud/MiuiCommonCloudHelper$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1, p3}, Lcom/android/server/cloud/MiuiCommonCloudHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/cloud/MiuiCommonCloudHelper;Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 405
    return-void
.end method


# virtual methods
.method public getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "moduleName"    # Ljava/lang/String;

    .line 413
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mModuleNameClassObjectMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public init(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "cloudFilePath"    # Ljava/lang/String;

    .line 61
    iput-object p1, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mContext:Landroid/content/Context;

    .line 62
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    iput-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mGson:Lcom/google/gson/Gson;

    .line 63
    iput-object p2, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mHandler:Landroid/os/Handler;

    .line 64
    iput-object p3, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mCloudFilePath:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public initFromAssets(Ljava/lang/String;)V
    .locals 2
    .param p1, "assetsConfigFilePath"    # Ljava/lang/String;

    .line 74
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/cloud/MiuiCommonCloudHelper$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1}, Lcom/android/server/cloud/MiuiCommonCloudHelper$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/cloud/MiuiCommonCloudHelper;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 80
    return-void
.end method

.method public initFromPath(Ljava/lang/String;)V
    .locals 2
    .param p1, "configFilePath"    # Ljava/lang/String;

    .line 89
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/cloud/MiuiCommonCloudHelper$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1}, Lcom/android/server/cloud/MiuiCommonCloudHelper$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/cloud/MiuiCommonCloudHelper;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 95
    return-void
.end method

.method public notifyAllObservers()V
    .locals 2

    .line 436
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/MiuiCommonCloudHelperStub$Observer;

    .line 437
    .local v1, "observer":Lcom/android/server/MiuiCommonCloudHelperStub$Observer;
    invoke-interface {v1}, Lcom/android/server/MiuiCommonCloudHelperStub$Observer;->update()V

    .line 438
    .end local v1    # "observer":Lcom/android/server/MiuiCommonCloudHelperStub$Observer;
    goto :goto_0

    .line 439
    :cond_0
    return-void
.end method

.method public registerObserver(Lcom/android/server/MiuiCommonCloudHelperStub$Observer;)V
    .locals 1
    .param p1, "observer"    # Lcom/android/server/MiuiCommonCloudHelperStub$Observer;

    .line 422
    const-string v0, "observer may not be null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 423
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 426
    :cond_0
    return-void
.end method

.method public startCloudObserve()V
    .locals 4

    .line 205
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 206
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/cloud/MiuiCommonCloudHelper$1;

    iget-object v3, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/cloud/MiuiCommonCloudHelper$1;-><init>(Lcom/android/server/cloud/MiuiCommonCloudHelper;Landroid/os/Handler;)V

    .line 205
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 220
    return-void
.end method

.method public unregisterObserver(Lcom/android/server/MiuiCommonCloudHelperStub$Observer;)V
    .locals 1
    .param p1, "observer"    # Lcom/android/server/MiuiCommonCloudHelperStub$Observer;

    .line 430
    const-string v0, "observer may not be null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 431
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudHelper;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 432
    return-void
.end method
