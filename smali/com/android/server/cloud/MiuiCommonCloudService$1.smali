.class Lcom/android/server/cloud/MiuiCommonCloudService$1;
.super Landroid/database/ContentObserver;
.source "MiuiCommonCloudService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/cloud/MiuiCommonCloudService;->registerCloudDataObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/cloud/MiuiCommonCloudService;


# direct methods
.method public static synthetic $r8$lambda$EFpkJfUsFDa9zEvEiLi_8Uxzw8U(Lcom/android/server/cloud/MiuiCommonCloudService$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/cloud/MiuiCommonCloudService$1;->lambda$onChange$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/cloud/MiuiCommonCloudService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/cloud/MiuiCommonCloudService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 29
    iput-object p1, p0, Lcom/android/server/cloud/MiuiCommonCloudService$1;->this$0:Lcom/android/server/cloud/MiuiCommonCloudService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method

.method private synthetic lambda$onChange$0()V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudService$1;->this$0:Lcom/android/server/cloud/MiuiCommonCloudService;

    invoke-static {v0}, Lcom/android/server/cloud/MiuiCommonCloudService;->-$$Nest$fgetmMiuiCloudParsingCollection(Lcom/android/server/cloud/MiuiCommonCloudService;)Lcom/android/server/cloud/MiuiCloudParsingCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->syncLocalBackupFromCloud()V

    .line 37
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .line 32
    iget-object v0, p0, Lcom/android/server/cloud/MiuiCommonCloudService$1;->this$0:Lcom/android/server/cloud/MiuiCommonCloudService;

    invoke-static {v0}, Lcom/android/server/cloud/MiuiCommonCloudService;->-$$Nest$fgetmMiuiCloudParsingCollection(Lcom/android/server/cloud/MiuiCommonCloudService;)Lcom/android/server/cloud/MiuiCloudParsingCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->updateDataFromCloud()Z

    move-result v0

    .line 33
    .local v0, "changed":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cloud Data changed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiCommonCloudService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    if-eqz v0, :cond_0

    .line 35
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/cloud/MiuiCommonCloudService$1$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/cloud/MiuiCommonCloudService$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/cloud/MiuiCommonCloudService$1;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 38
    iget-object v1, p0, Lcom/android/server/cloud/MiuiCommonCloudService$1;->this$0:Lcom/android/server/cloud/MiuiCommonCloudService;

    invoke-static {v1}, Lcom/android/server/cloud/MiuiCommonCloudService;->-$$Nest$fgetmMiuiCloudParsingCollection(Lcom/android/server/cloud/MiuiCommonCloudService;)Lcom/android/server/cloud/MiuiCloudParsingCollection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->notifyAllObservers()V

    .line 40
    :cond_0
    return-void
.end method
