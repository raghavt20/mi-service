public class com.android.server.cloud.MiuiCommonCloudService extends com.android.server.MiuiCommonCloudServiceStub {
	 /* .source "MiuiCommonCloudService.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.MiuiCommonCloudServiceStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private android.content.Context mContext;
private com.android.server.cloud.MiuiCloudParsingCollection mMiuiCloudParsingCollection;
/* # direct methods */
static com.android.server.cloud.MiuiCloudParsingCollection -$$Nest$fgetmMiuiCloudParsingCollection ( com.android.server.cloud.MiuiCommonCloudService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mMiuiCloudParsingCollection;
} // .end method
public com.android.server.cloud.MiuiCommonCloudService ( ) {
	 /* .locals 0 */
	 /* .line 15 */
	 /* invoke-direct {p0}, Lcom/android/server/MiuiCommonCloudServiceStub;-><init>()V */
	 return;
} // .end method
private void registerCloudDataObserver ( ) {
	 /* .locals 4 */
	 /* .line 27 */
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 /* .line 28 */
	 android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
	 /* new-instance v2, Lcom/android/server/cloud/MiuiCommonCloudService$1; */
	 /* .line 29 */
	 com.android.internal.os.BackgroundThread .getHandler ( );
	 /* invoke-direct {v2, p0, v3}, Lcom/android/server/cloud/MiuiCommonCloudService$1;-><init>(Lcom/android/server/cloud/MiuiCommonCloudService;Landroid/os/Handler;)V */
	 /* .line 27 */
	 int v3 = 1; // const/4 v3, 0x1
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
	 /* .line 42 */
	 return;
} // .end method
/* # virtual methods */
public java.lang.Object getDataByModuleName ( java.lang.String p0 ) {
	 /* .locals 1 */
	 /* .param p1, "moduleName" # Ljava/lang/String; */
	 /* .line 70 */
	 v0 = this.mMiuiCloudParsingCollection;
	 (( com.android.server.cloud.MiuiCloudParsingCollection ) v0 ).getDataByModuleName ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;
} // .end method
public java.lang.String getStringByModuleName ( java.lang.String p0 ) {
	 /* .locals 1 */
	 /* .param p1, "moduleName" # Ljava/lang/String; */
	 /* .line 80 */
	 v0 = this.mMiuiCloudParsingCollection;
	 (( com.android.server.cloud.MiuiCloudParsingCollection ) v0 ).getStringByModuleName ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->getStringByModuleName(Ljava/lang/String;)Ljava/lang/String;
} // .end method
public void init ( android.content.Context p0 ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 21 */
	 this.mContext = p1;
	 /* .line 22 */
	 /* new-instance v0, Lcom/android/server/cloud/MiuiCloudParsingCollection; */
	 /* invoke-direct {v0, p1}, Lcom/android/server/cloud/MiuiCloudParsingCollection;-><init>(Landroid/content/Context;)V */
	 this.mMiuiCloudParsingCollection = v0;
	 /* .line 23 */
	 /* invoke-direct {p0}, Lcom/android/server/cloud/MiuiCommonCloudService;->registerCloudDataObserver()V */
	 /* .line 24 */
	 return;
} // .end method
public void registerObserver ( com.android.server.MiuiCommonCloudServiceStub$Observer p0 ) {
	 /* .locals 1 */
	 /* .param p1, "observer" # Lcom/android/server/MiuiCommonCloudServiceStub$Observer; */
	 /* .line 51 */
	 v0 = this.mMiuiCloudParsingCollection;
	 (( com.android.server.cloud.MiuiCloudParsingCollection ) v0 ).registerObserver ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->registerObserver(Lcom/android/server/MiuiCommonCloudServiceStub$Observer;)V
	 /* .line 52 */
	 return;
} // .end method
public void unregisterObserver ( com.android.server.MiuiCommonCloudServiceStub$Observer p0 ) {
	 /* .locals 1 */
	 /* .param p1, "observer" # Lcom/android/server/MiuiCommonCloudServiceStub$Observer; */
	 /* .line 60 */
	 v0 = this.mMiuiCloudParsingCollection;
	 (( com.android.server.cloud.MiuiCloudParsingCollection ) v0 ).unregisterObserver ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->unregisterObserver(Lcom/android/server/MiuiCommonCloudServiceStub$Observer;)V
	 /* .line 61 */
	 return;
} // .end method
