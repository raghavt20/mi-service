class com.android.server.cloud.MiuiCommonCloudHelper$1 extends android.database.ContentObserver {
	 /* .source "MiuiCommonCloudHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/cloud/MiuiCommonCloudHelper;->startCloudObserve()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.cloud.MiuiCommonCloudHelper this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$hTDWv3d1sxyz8s9a7svch-otUu0 ( com.android.server.cloud.MiuiCommonCloudHelper$1 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/cloud/MiuiCommonCloudHelper$1;->lambda$onChange$0()V */
return;
} // .end method
 com.android.server.cloud.MiuiCommonCloudHelper$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/cloud/MiuiCommonCloudHelper; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 207 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
private void lambda$onChange$0 ( ) { //synthethic
/* .locals 2 */
/* .line 214 */
v0 = this.this$0;
com.android.server.cloud.MiuiCommonCloudHelper .-$$Nest$fgetmCloudFilePath ( v0 );
com.android.server.cloud.MiuiCommonCloudHelper .-$$Nest$msyncLocalBackupFromCloud ( v0,v1 );
/* .line 215 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .line 210 */
v0 = this.this$0;
v0 = com.android.server.cloud.MiuiCommonCloudHelper .-$$Nest$mupdateDataFromCloud ( v0 );
/* .line 211 */
/* .local v0, "changed":Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Cloud data changed: "; // const-string v2, "Cloud data changed: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiCommonCloudHelper"; // const-string v2, "MiuiCommonCloudHelper"
android.util.Slog .d ( v2,v1 );
/* .line 212 */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 213 */
	 com.android.internal.os.BackgroundThread .getHandler ( );
	 /* new-instance v2, Lcom/android/server/cloud/MiuiCommonCloudHelper$1$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v2, p0}, Lcom/android/server/cloud/MiuiCommonCloudHelper$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/cloud/MiuiCommonCloudHelper$1;)V */
	 (( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
	 /* .line 216 */
	 v1 = this.this$0;
	 (( com.android.server.cloud.MiuiCommonCloudHelper ) v1 ).notifyAllObservers ( ); // invoke-virtual {v1}, Lcom/android/server/cloud/MiuiCommonCloudHelper;->notifyAllObservers()V
	 /* .line 218 */
} // :cond_0
return;
} // .end method
