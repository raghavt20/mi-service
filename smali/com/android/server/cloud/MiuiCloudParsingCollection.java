public class com.android.server.cloud.MiuiCloudParsingCollection {
	 /* .source "MiuiCloudParsingCollection.java" */
	 /* # static fields */
	 private static final java.lang.String ATTR_CONFIG_FILE_NAME;
	 private static final java.lang.String CLOUD_BACKUP_FILE_NAME;
	 private static final java.lang.String CLOUD_BACKUP_FILE_ROOT_ELEMENT;
	 private static final java.lang.String CLOUD_BACKUP_FILE_TEST_CRT_TAG;
	 private static final java.lang.String CLOUD_CTRL_ITEM_TAG;
	 private static final java.lang.String CLOUD_MODULE_BEAN_PATH;
	 private static final java.lang.String CLOUD_MODULE_NAME;
	 private static final java.lang.String CLOUD_MODULE_STRING;
	 private static final java.lang.String CLOUD_MODULE_VERSION;
	 private static final java.lang.String MODULE_DATA_VERSION;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.content.res.AssetManager mAssets;
	 private java.io.File mCloudBackFile;
	 private android.content.Context mContext;
	 private java.util.HashMap mModuleNameBeanClassPathMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.util.HashMap mModuleNameClassObjectMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashMap mModuleNameDataVersionMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashMap mModuleNameStrMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.ArrayList mObservers;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/MiuiCommonCloudServiceStub$Observer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$47XtPO154zNtS6w4EBX6AkGKZHM ( com.android.server.cloud.MiuiCloudParsingCollection p0, org.xmlpull.v1.XmlSerializer p1, java.lang.String p2, java.lang.String p3, java.lang.String p4 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->lambda$writeElementOfModuleListToXml$1(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
return;
} // .end method
public static void $r8$lambda$J_xYjwtg7AVUycV1NxfOshkYwc0 ( com.android.server.cloud.MiuiCloudParsingCollection p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->lambda$new$0()V */
return;
} // .end method
 com.android.server.cloud.MiuiCloudParsingCollection ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 55 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 49 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mModuleNameStrMap = v0;
/* .line 50 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mModuleNameBeanClassPathMap = v0;
/* .line 51 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mModuleNameDataVersionMap = v0;
/* .line 52 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mModuleNameClassObjectMap = v0;
/* .line 53 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mObservers = v0;
/* .line 56 */
this.mContext = p1;
/* .line 57 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* new-instance v1, Lcom/android/server/cloud/MiuiCloudParsingCollection$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/cloud/MiuiCloudParsingCollection$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/cloud/MiuiCloudParsingCollection;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 60 */
return;
} // .end method
private void checkFileStatus ( java.io.File p0 ) {
/* .locals 4 */
/* .param p1, "file" # Ljava/io/File; */
/* .line 136 */
final String v0 = "MiuiCloudParsingCollection"; // const-string v0, "MiuiCloudParsingCollection"
if ( p1 != null) { // if-eqz p1, :cond_0
try { // :try_start_0
v1 = (( java.io.File ) p1 ).exists ( ); // invoke-virtual {p1}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_1 */
/* .line 137 */
} // :cond_0
(( java.io.File ) p1 ).createNewFile ( ); // invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z
/* .line 138 */
final String v1 = "common_cloud_backup not exist, create file end"; // const-string v1, "common_cloud_backup not exist, create file end"
android.util.Slog .d ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 142 */
} // :cond_1
/* .line 140 */
/* :catch_0 */
/* move-exception v1 */
/* .line 141 */
/* .local v1, "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "create common_cloud_backup fail "; // const-string v3, "create common_cloud_backup fail "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* .line 143 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
private void initialization ( ) {
/* .locals 0 */
/* .line 63 */
/* invoke-direct {p0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->loadLocalInitConfig()V */
/* .line 64 */
/* invoke-direct {p0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->loadLocalCloudBackup()V */
/* .line 65 */
/* invoke-direct {p0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->parseStringToClassObject()V */
/* .line 66 */
(( com.android.server.cloud.MiuiCloudParsingCollection ) p0 ).notifyAllObservers ( ); // invoke-virtual {p0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->notifyAllObservers()V
/* .line 67 */
return;
} // .end method
private void lambda$new$0 ( ) { //synthethic
/* .locals 0 */
/* .line 58 */
/* invoke-direct {p0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->initialization()V */
/* .line 59 */
return;
} // .end method
private void lambda$writeElementOfModuleListToXml$1 ( org.xmlpull.v1.XmlSerializer p0, java.lang.String p1, java.lang.String p2, java.lang.String p3 ) { //synthethic
/* .locals 3 */
/* .param p1, "serializer" # Lorg/xmlpull/v1/XmlSerializer; */
/* .param p2, "tagString" # Ljava/lang/String; */
/* .param p3, "key" # Ljava/lang/String; */
/* .param p4, "value" # Ljava/lang/String; */
/* .line 270 */
if ( p4 != null) { // if-eqz p4, :cond_0
/* .line 272 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
/* .line 273 */
final String v1 = "moduleName"; // const-string v1, "moduleName"
/* .line 274 */
final String v1 = "moduleVersion"; // const-string v1, "moduleVersion"
v2 = this.mModuleNameDataVersionMap;
(( java.util.HashMap ) v2 ).get ( p3 ); // invoke-virtual {v2, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
/* .line 275 */
final String v1 = "moduleBeanPath"; // const-string v1, "moduleBeanPath"
v2 = this.mModuleNameBeanClassPathMap;
(( java.util.HashMap ) v2 ).get ( p3 ); // invoke-virtual {v2, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
/* .line 276 */
final String v1 = "moduleString"; // const-string v1, "moduleString"
/* .line 277 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 281 */
/* .line 278 */
/* :catch_0 */
/* move-exception v0 */
/* .line 279 */
/* .local v0, "e":Ljava/io/IOException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to write element of app list to xml"; // const-string v2, "Failed to write element of app list to xml"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiCloudParsingCollection"; // const-string v2, "MiuiCloudParsingCollection"
android.util.Slog .e ( v2,v1 );
/* .line 280 */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
/* .line 283 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_0
return;
} // .end method
private void loadLocalCloudBackup ( ) {
/* .locals 3 */
/* .line 83 */
/* new-instance v0, Ljava/io/File; */
android.os.Environment .getDataSystemDeDirectory ( );
final String v2 = "common_cloud_backup.xml"; // const-string v2, "common_cloud_backup.xml"
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mCloudBackFile = v0;
/* .line 84 */
/* invoke-direct {p0, v0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->checkFileStatus(Ljava/io/File;)V */
/* .line 85 */
try { // :try_start_0
/* new-instance v0, Ljava/io/FileInputStream; */
v1 = this.mCloudBackFile;
/* invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 86 */
/* .local v0, "inputStream":Ljava/io/FileInputStream; */
/* nop */
/* .line 87 */
try { // :try_start_1
/* invoke-direct {p0, v0}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->loadLocalFile(Ljava/io/InputStream;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 91 */
try { // :try_start_2
(( java.io.FileInputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 93 */
} // .end local v0 # "inputStream":Ljava/io/FileInputStream;
/* .line 85 */
/* .restart local v0 # "inputStream":Ljava/io/FileInputStream; */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_3
(( java.io.FileInputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v2 */
try { // :try_start_4
(( java.lang.Throwable ) v1 ).addSuppressed ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/android/server/cloud/MiuiCloudParsingCollection;
} // :goto_0
/* throw v1 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 91 */
} // .end local v0 # "inputStream":Ljava/io/FileInputStream;
/* .restart local p0 # "this":Lcom/android/server/cloud/MiuiCloudParsingCollection; */
/* :catch_0 */
/* move-exception v0 */
/* .line 92 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "MiuiCloudParsingCollection"; // const-string v1, "MiuiCloudParsingCollection"
final String v2 = "Failed to read common_cloud_backup.xml "; // const-string v2, "Failed to read common_cloud_backup.xml "
android.util.Slog .e ( v1,v2,v0 );
/* .line 94 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void loadLocalFile ( java.io.InputStream p0 ) {
/* .locals 13 */
/* .param p1, "inputStream" # Ljava/io/InputStream; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/xmlpull/v1/XmlPullParserException;, */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 102 */
android.util.Xml .newPullParser ( );
/* .line 103 */
/* .local v0, "parser":Lorg/xmlpull/v1/XmlPullParser; */
v1 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v1 ).name ( ); // invoke-virtual {v1}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
/* .line 104 */
int v1 = 0; // const/4 v1, 0x0
/* .line 105 */
v2 = /* .local v1, "tagName":Ljava/lang/String; */
/* .line 106 */
/* .local v2, "eventType":I */
} // :goto_0
int v3 = 1; // const/4 v3, 0x1
/* if-eq v2, v3, :cond_2 */
/* .line 107 */
/* packed-switch v2, :pswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 109 */
/* :pswitch_0 */
/* .line 110 */
final String v3 = "cloud-data-item"; // const-string v3, "cloud-data-item"
v3 = (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 111 */
final String v3 = "moduleName"; // const-string v3, "moduleName"
int v4 = 0; // const/4 v4, 0x0
/* .line 112 */
/* .local v3, "moduleName":Ljava/lang/String; */
final String v5 = "moduleVersion"; // const-string v5, "moduleVersion"
/* .line 113 */
/* .local v5, "moduleVersion":Ljava/lang/String; */
final String v6 = "moduleBeanPath"; // const-string v6, "moduleBeanPath"
/* .line 114 */
/* .local v6, "moduleBeanPath":Ljava/lang/String; */
final String v7 = "moduleString"; // const-string v7, "moduleString"
/* .line 115 */
/* .local v4, "moduleString":Ljava/lang/String; */
java.lang.Long .parseLong ( v5 );
/* move-result-wide v7 */
java.lang.Long .valueOf ( v7,v8 );
/* .line 116 */
/* .local v7, "lastStoredVersion":Ljava/lang/Long; */
v8 = this.mModuleNameDataVersionMap;
final String v9 = "-1"; // const-string v9, "-1"
(( java.util.HashMap ) v8 ).getOrDefault ( v3, v9 ); // invoke-virtual {v8, v3, v9}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v8, Ljava/lang/String; */
java.lang.Long .parseLong ( v8 );
/* move-result-wide v8 */
java.lang.Long .valueOf ( v8,v9 );
/* .line 117 */
/* .local v8, "initVersion":Ljava/lang/Long; */
(( java.lang.Long ) v7 ).longValue ( ); // invoke-virtual {v7}, Ljava/lang/Long;->longValue()J
/* move-result-wide v9 */
(( java.lang.Long ) v8 ).longValue ( ); // invoke-virtual {v8}, Ljava/lang/Long;->longValue()J
/* move-result-wide v11 */
/* cmp-long v9, v9, v11 */
/* if-lez v9, :cond_0 */
/* .line 118 */
v9 = this.mModuleNameStrMap;
(( java.util.HashMap ) v9 ).put ( v3, v4 ); // invoke-virtual {v9, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 119 */
v9 = this.mModuleNameDataVersionMap;
(( java.util.HashMap ) v9 ).put ( v3, v5 ); // invoke-virtual {v9, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 120 */
v9 = this.mModuleNameBeanClassPathMap;
(( java.util.HashMap ) v9 ).put ( v3, v6 ); // invoke-virtual {v9, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 121 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "last stored cloud data version: "; // const-string v10, "last stored cloud data version: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v10 = " is higher than init data version; "; // const-string v10, " is higher than init data version; "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v10 = ", so use last stored cloud data"; // const-string v10, ", so use last stored cloud data"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v10 = "MiuiCloudParsingCollection"; // const-string v10, "MiuiCloudParsingCollection"
android.util.Slog .d ( v10,v9 );
/* .line 125 */
} // .end local v3 # "moduleName":Ljava/lang/String;
} // .end local v4 # "moduleString":Ljava/lang/String;
} // .end local v5 # "moduleVersion":Ljava/lang/String;
} // .end local v6 # "moduleBeanPath":Ljava/lang/String;
} // .end local v7 # "lastStoredVersion":Ljava/lang/Long;
} // .end local v8 # "initVersion":Ljava/lang/Long;
} // :cond_0
/* nop */
/* .line 130 */
} // :cond_1
v2 = } // :goto_1
/* goto/16 :goto_0 */
/* .line 132 */
} // :cond_2
return;
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void loadLocalInitConfig ( ) {
/* .locals 4 */
/* .line 70 */
final String v0 = "MiuiCloudParsingCollection"; // const-string v0, "MiuiCloudParsingCollection"
android.content.res.Resources .getSystem ( );
(( android.content.res.Resources ) v1 ).getAssets ( ); // invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;
this.mAssets = v1;
/* .line 71 */
try { // :try_start_0
final String v2 = "attributes_config.xml"; // const-string v2, "attributes_config.xml"
(( android.content.res.AssetManager ) v1 ).open ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 72 */
/* .local v1, "inputStream":Ljava/io/InputStream; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 73 */
try { // :try_start_1
/* invoke-direct {p0, v1}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->loadLocalFile(Ljava/io/InputStream;)V */
/* .line 71 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 75 */
} // :cond_0
final String v2 = "file attributes_config not exits "; // const-string v2, "file attributes_config not exits "
android.util.Slog .e ( v0,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 77 */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
try { // :try_start_2
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 79 */
} // .end local v1 # "inputStream":Ljava/io/InputStream;
} // :cond_1
/* .line 71 */
/* .restart local v1 # "inputStream":Ljava/io/InputStream; */
} // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_2
try { // :try_start_3
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/android/server/cloud/MiuiCloudParsingCollection;
} // :cond_2
} // :goto_2
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 77 */
} // .end local v1 # "inputStream":Ljava/io/InputStream;
/* .restart local p0 # "this":Lcom/android/server/cloud/MiuiCloudParsingCollection; */
/* :catch_0 */
/* move-exception v1 */
/* .line 78 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "Failed to read attributes_config.xml "; // const-string v2, "Failed to read attributes_config.xml "
android.util.Slog .e ( v0,v2,v1 );
/* .line 80 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_3
return;
} // .end method
private void parseStringToClassObject ( ) {
/* .locals 6 */
/* .line 146 */
v0 = this.mModuleNameStrMap;
(( java.util.HashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/lang/String; */
/* .line 147 */
/* .local v1, "moduleName":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 148 */
/* .local v2, "classObj":Ljava/lang/Object; */
v3 = this.mModuleNameStrMap;
(( java.util.HashMap ) v3 ).get ( v1 ); // invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/String; */
/* .line 149 */
/* .local v3, "data":Ljava/lang/String; */
v4 = this.mModuleNameBeanClassPathMap;
(( java.util.HashMap ) v4 ).get ( v1 ); // invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/String; */
/* .line 151 */
/* .local v4, "beanPath":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_0
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 152 */
(( com.android.server.cloud.MiuiCloudParsingCollection ) p0 ).parseStringToClass ( v3, v4 ); // invoke-virtual {p0, v3, v4}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->parseStringToClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
/* .line 154 */
} // :cond_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 155 */
v5 = this.mModuleNameClassObjectMap;
(( java.util.HashMap ) v5 ).put ( v1, v2 ); // invoke-virtual {v5, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 157 */
} // .end local v1 # "moduleName":Ljava/lang/String;
} // .end local v2 # "classObj":Ljava/lang/Object;
} // .end local v3 # "data":Ljava/lang/String;
} // .end local v4 # "beanPath":Ljava/lang/String;
} // :cond_1
/* .line 158 */
} // :cond_2
return;
} // .end method
private Boolean updateCloudData ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p1, "moduleName" # Ljava/lang/String; */
/* .line 190 */
v0 = this.mModuleNameDataVersionMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
/* .line 191 */
/* .local v0, "latestVersion":Ljava/lang/String; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "version" */
int v3 = 0; // const/4 v3, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v1,p1,v2,v3 );
/* .line 193 */
/* .local v1, "remoteVersion":Ljava/lang/String; */
v2 = this.mContext;
/* .line 194 */
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.MiuiSettings$SettingsCloudData .getCloudDataList ( v2,p1 );
/* .line 196 */
/* .local v2, "cloudDataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;" */
int v3 = 0; // const/4 v3, 0x0
final String v4 = "MiuiCloudParsingCollection"; // const-string v4, "MiuiCloudParsingCollection"
/* if-nez v1, :cond_0 */
/* .line 197 */
final String v5 = " remoteVersion is null, Don\'t update "; // const-string v5, " remoteVersion is null, Don\'t update "
android.util.Slog .d ( v4,v5 );
/* .line 198 */
/* .line 200 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
java.lang.Long .parseLong ( v1 );
/* move-result-wide v5 */
java.lang.Long .parseLong ( v0 );
/* move-result-wide v7 */
/* cmp-long v5, v5, v7 */
/* if-lez v5, :cond_1 */
/* .line 201 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " moduleName: "; // const-string v5, " moduleName: "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", lastVersion: "; // const-string v5, ", lastVersion: "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", remoteVersion: "; // const-string v5, ", remoteVersion: "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", so update"; // const-string v5, ", so update"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v3 );
/* .line 203 */
v3 = /* invoke-direct {p0, p1, v1, v2}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->updateMultiMap(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z */
/* .line 205 */
} // :cond_1
} // .end method
private Boolean updateMultiMap ( java.lang.String p0, java.lang.String p1, java.util.List p2 ) {
/* .locals 7 */
/* .param p1, "moduleName" # Ljava/lang/String; */
/* .param p2, "remoteVersion" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 217 */
/* .local p3, "cloudDataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;" */
final String v0 = "MiuiCloudParsingCollection"; // const-string v0, "MiuiCloudParsingCollection"
int v1 = 0; // const/4 v1, 0x0
/* if-nez p3, :cond_0 */
/* .line 218 */
final String v2 = "cloudDataList is null, see early log for detail"; // const-string v2, "cloudDataList is null, see early log for detail"
android.util.Slog .e ( v0,v2 );
/* .line 219 */
/* .line 221 */
} // :cond_0
/* check-cast v2, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v2 ).toString ( ); // invoke-virtual {v2}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;
/* .line 222 */
/* .local v2, "cloudData":Ljava/lang/String; */
v3 = this.mModuleNameBeanClassPathMap;
(( java.util.HashMap ) v3 ).get ( p1 ); // invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/String; */
/* .line 223 */
/* .local v3, "beanPath":Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_2
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 224 */
(( com.android.server.cloud.MiuiCloudParsingCollection ) p0 ).parseStringToClass ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->parseStringToClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
/* .line 225 */
/* .local v4, "classObject":Ljava/lang/Object; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 226 */
v0 = this.mModuleNameDataVersionMap;
(( java.util.HashMap ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 227 */
v0 = this.mModuleNameStrMap;
(( java.util.HashMap ) v0 ).put ( p1, v2 ); // invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 228 */
v0 = this.mModuleNameClassObjectMap;
(( java.util.HashMap ) v0 ).put ( p1, v4 ); // invoke-virtual {v0, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 229 */
int v0 = 1; // const/4 v0, 0x1
/* .line 231 */
} // :cond_1
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " parsed classObject is null, check cloud data and Bean properties"; // const-string v6, " parsed classObject is null, check cloud data and Bean properties"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v5 );
/* .line 232 */
} // .end local v4 # "classObject":Ljava/lang/Object;
/* nop */
/* .line 236 */
/* .line 233 */
} // :cond_2
final String v4 = "moduleName or beanPath is null"; // const-string v4, "moduleName or beanPath is null"
android.util.Slog .e ( v0,v4 );
/* .line 234 */
} // .end method
private void writeElementOfModuleListToXml ( org.xmlpull.v1.XmlSerializer p0, java.util.HashMap p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "serializer" # Lorg/xmlpull/v1/XmlSerializer; */
/* .param p3, "tagString" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lorg/xmlpull/v1/XmlSerializer;", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 269 */
/* .local p2, "strMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;" */
/* new-instance v0, Lcom/android/server/cloud/MiuiCloudParsingCollection$$ExternalSyntheticLambda1; */
/* invoke-direct {v0, p0, p1, p3}, Lcom/android/server/cloud/MiuiCloudParsingCollection$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/cloud/MiuiCloudParsingCollection;Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V */
(( java.util.HashMap ) p2 ).forEach ( v0 ); // invoke-virtual {p2, v0}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 284 */
return;
} // .end method
/* # virtual methods */
public java.lang.Object getDataByModuleName ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "moduleName" # Ljava/lang/String; */
/* .line 287 */
v0 = this.mModuleNameClassObjectMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public java.lang.String getStringByModuleName ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "moduleName" # Ljava/lang/String; */
/* .line 291 */
v0 = this.mModuleNameStrMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
} // .end method
public void notifyAllObservers ( ) {
/* .locals 2 */
/* .line 307 */
v0 = this.mObservers;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/android/server/MiuiCommonCloudServiceStub$Observer; */
/* .line 308 */
/* .local v1, "observer":Lcom/android/server/MiuiCommonCloudServiceStub$Observer; */
(( com.android.server.MiuiCommonCloudServiceStub$Observer ) v1 ).update ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiCommonCloudServiceStub$Observer;->update()V
/* .line 309 */
} // .end local v1 # "observer":Lcom/android/server/MiuiCommonCloudServiceStub$Observer;
/* .line 310 */
} // :cond_0
return;
} // .end method
public java.lang.Object parseStringToClass ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "classPath" # Ljava/lang/String; */
/* .line 162 */
try { // :try_start_0
java.lang.Class .forName ( p2 );
/* .line 163 */
/* .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
com.android.server.cloud.JsonUtil .fromJson ( p1,v0 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 164 */
} // .end local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
/* :catch_0 */
/* move-exception v0 */
/* .line 165 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "parseStringToClass exception "; // const-string v2, "parseStringToClass exception "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiCloudParsingCollection"; // const-string v2, "MiuiCloudParsingCollection"
android.util.Slog .e ( v2,v1 );
/* .line 166 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void registerObserver ( com.android.server.MiuiCommonCloudServiceStub$Observer p0 ) {
/* .locals 1 */
/* .param p1, "observer" # Lcom/android/server/MiuiCommonCloudServiceStub$Observer; */
/* .line 295 */
final String v0 = "observer may not be null"; // const-string v0, "observer may not be null"
java.util.Objects .requireNonNull ( p1,v0 );
/* .line 296 */
v0 = this.mObservers;
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 297 */
v0 = this.mObservers;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 299 */
} // :cond_0
return;
} // .end method
public void syncLocalBackupFromCloud ( ) {
/* .locals 10 */
/* .line 240 */
final String v0 = "cloud-control"; // const-string v0, "cloud-control"
final String v1 = "common-cloud-config"; // const-string v1, "common-cloud-config"
final String v2 = "MiuiCloudParsingCollection"; // const-string v2, "MiuiCloudParsingCollection"
/* new-instance v3, Ljava/io/File; */
android.os.Environment .getDataSystemDeDirectory ( );
final String v5 = "common_cloud_backup.xml"; // const-string v5, "common_cloud_backup.xml"
/* invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mCloudBackFile = v3;
/* .line 241 */
/* invoke-direct {p0, v3}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->checkFileStatus(Ljava/io/File;)V */
/* .line 242 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* .line 243 */
/* .local v3, "startTime":J */
try { // :try_start_0
/* new-instance v5, Ljava/io/BufferedOutputStream; */
/* new-instance v6, Ljava/io/FileOutputStream; */
v7 = this.mCloudBackFile;
/* invoke-direct {v6, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* invoke-direct {v5, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 244 */
/* .local v5, "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream; */
try { // :try_start_1
android.util.Xml .newSerializer ( );
/* .line 245 */
/* .local v6, "serializer":Lorg/xmlpull/v1/XmlSerializer; */
v7 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v7 ).name ( ); // invoke-virtual {v7}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
/* .line 246 */
v7 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v7 ).name ( ); // invoke-virtual {v7}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
int v8 = 1; // const/4 v8, 0x1
java.lang.Boolean .valueOf ( v8 );
/* .line 247 */
final String v7 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v7, "http://xmlpull.org/v1/doc/features.html#indent-output"
/* .line 248 */
int v7 = 0; // const/4 v7, 0x0
/* .line 250 */
/* .line 251 */
v8 = this.mModuleNameStrMap;
final String v9 = "cloud-data-item"; // const-string v9, "cloud-data-item"
/* invoke-direct {p0, v6, v8, v9}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->writeElementOfModuleListToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;Ljava/lang/String;)V */
/* .line 252 */
/* .line 254 */
/* .line 255 */
/* .line 256 */
(( java.io.BufferedOutputStream ) v5 ).flush ( ); // invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->flush()V
/* .line 257 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Write common_cloud_backup.xml took "; // const-string v1, "Write common_cloud_backup.xml took "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 258 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v7 */
/* sub-long/2addr v7, v3 */
(( java.lang.StringBuilder ) v0 ).append ( v7, v8 ); // invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " ms "; // const-string v1, " ms "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 257 */
android.util.Slog .d ( v2,v0 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 259 */
} // .end local v6 # "serializer":Lorg/xmlpull/v1/XmlSerializer;
try { // :try_start_2
(( java.io.BufferedOutputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 265 */
} // .end local v5 # "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream;
/* .line 243 */
/* .restart local v5 # "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream; */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_3
(( java.io.BufferedOutputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_4
(( java.lang.Throwable ) v0 ).addSuppressed ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v3 # "startTime":J
} // .end local p0 # "this":Lcom/android/server/cloud/MiuiCloudParsingCollection;
} // :goto_0
/* throw v0 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 259 */
} // .end local v5 # "backUpBufferedOutputStream":Ljava/io/BufferedOutputStream;
/* .restart local v3 # "startTime":J */
/* .restart local p0 # "this":Lcom/android/server/cloud/MiuiCloudParsingCollection; */
/* :catch_0 */
/* move-exception v0 */
/* .line 260 */
/* .local v0, "e":Ljava/io/IOException; */
v1 = this.mCloudBackFile;
v1 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mCloudBackFile;
v1 = (( java.io.File ) v1 ).delete ( ); // invoke-virtual {v1}, Ljava/io/File;->delete()Z
/* if-nez v1, :cond_0 */
/* .line 261 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Failed to clean up mangled file: "; // const-string v5, "Failed to clean up mangled file: "
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mCloudBackFile;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v1 );
/* .line 263 */
} // :cond_0
final String v1 = "Unable to write cloud backup xml,current changes will be lost at reboot"; // const-string v1, "Unable to write cloud backup xml,current changes will be lost at reboot"
android.util.Slog .e ( v2,v1,v0 );
/* .line 266 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_1
return;
} // .end method
public void unregisterObserver ( com.android.server.MiuiCommonCloudServiceStub$Observer p0 ) {
/* .locals 1 */
/* .param p1, "observer" # Lcom/android/server/MiuiCommonCloudServiceStub$Observer; */
/* .line 302 */
final String v0 = "observer may not be null"; // const-string v0, "observer may not be null"
java.util.Objects .requireNonNull ( p1,v0 );
/* .line 303 */
v0 = this.mObservers;
(( java.util.ArrayList ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 304 */
return;
} // .end method
Boolean updateDataFromCloud ( ) {
/* .locals 4 */
/* .line 176 */
int v0 = 0; // const/4 v0, 0x0
/* .line 177 */
/* .local v0, "changed":Z */
v1 = this.mModuleNameStrMap;
(( java.util.HashMap ) v1 ).keySet ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/lang/String; */
/* .line 178 */
/* .local v2, "moduleName":Ljava/lang/String; */
v3 = /* invoke-direct {p0, v2}, Lcom/android/server/cloud/MiuiCloudParsingCollection;->updateCloudData(Ljava/lang/String;)Z */
/* or-int/2addr v0, v3 */
/* .line 179 */
} // .end local v2 # "moduleName":Ljava/lang/String;
/* .line 180 */
} // :cond_0
} // .end method
