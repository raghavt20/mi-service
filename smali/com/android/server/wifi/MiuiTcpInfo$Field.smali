.class public final enum Lcom/android/server/wifi/MiuiTcpInfo$Field;
.super Ljava/lang/Enum;
.source "MiuiTcpInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wifi/MiuiTcpInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Field"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/server/wifi/MiuiTcpInfo$Field;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum ADVMSS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum ATO:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum BACKOFF:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum BUSY_TIME:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum BYTES_ACKED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum BYTES_RECEIVED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum BYTES_RETRANS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum BYTES_SEND:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum CASTATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum DACK_DUPS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum DATA_SEGS_IN:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum DATA_SEGS_OUT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum DELIVERED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum DELIVERED_CE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum DELIVERY_RATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum DELIVERY_RATE_APP_LIMITED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum FACKETS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum LAST_ACK_RECV:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum LAST_ACK_SENT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum LAST_DATA_RECV:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum LAST_DATA_SENT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum LOST:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum MAX_PACING_RATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum MIN_RTT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum NOTSENT_BYTES:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum OPTIONS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum PACING_RATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum PMTU:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum PROBES:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum RCV_MSS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum RCV_OOOPACK:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum RCV_RTT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum RCV_SPACE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum RCV_SSTHRESH:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum RECORD_SEEN:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum REORDERING:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum RETRANS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum RETRANSMITS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum RTO:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum RTT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum RTTVAR:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum RWND_LIMITED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum SACKED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum SEGS_IN:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum SEGS_OUT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum SNDBUF_LIMITED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum SND_CWND:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum SND_MSS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum SND_SSTHRESH:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum SND_WND:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum STATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum TOTAL_RETRANS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum UNACKED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

.field public static final enum WSCALE:Lcom/android/server/wifi/MiuiTcpInfo$Field;


# instance fields
.field public final size:I


# direct methods
.method private static synthetic $values()[Lcom/android/server/wifi/MiuiTcpInfo$Field;
    .locals 54

    .line 22
    sget-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->STATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v1, Lcom/android/server/wifi/MiuiTcpInfo$Field;->CASTATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v2, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RETRANSMITS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v3, Lcom/android/server/wifi/MiuiTcpInfo$Field;->PROBES:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v4, Lcom/android/server/wifi/MiuiTcpInfo$Field;->BACKOFF:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v5, Lcom/android/server/wifi/MiuiTcpInfo$Field;->OPTIONS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v6, Lcom/android/server/wifi/MiuiTcpInfo$Field;->WSCALE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v7, Lcom/android/server/wifi/MiuiTcpInfo$Field;->DELIVERY_RATE_APP_LIMITED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v8, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RTO:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v9, Lcom/android/server/wifi/MiuiTcpInfo$Field;->ATO:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v10, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SND_MSS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v11, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RCV_MSS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v12, Lcom/android/server/wifi/MiuiTcpInfo$Field;->UNACKED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v13, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SACKED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v14, Lcom/android/server/wifi/MiuiTcpInfo$Field;->LOST:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v15, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RETRANS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v16, Lcom/android/server/wifi/MiuiTcpInfo$Field;->FACKETS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v17, Lcom/android/server/wifi/MiuiTcpInfo$Field;->LAST_DATA_SENT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v18, Lcom/android/server/wifi/MiuiTcpInfo$Field;->LAST_ACK_SENT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v19, Lcom/android/server/wifi/MiuiTcpInfo$Field;->LAST_DATA_RECV:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v20, Lcom/android/server/wifi/MiuiTcpInfo$Field;->LAST_ACK_RECV:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v21, Lcom/android/server/wifi/MiuiTcpInfo$Field;->PMTU:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v22, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RCV_SSTHRESH:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v23, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RTT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v24, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RTTVAR:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v25, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SND_SSTHRESH:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v26, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SND_CWND:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v27, Lcom/android/server/wifi/MiuiTcpInfo$Field;->ADVMSS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v28, Lcom/android/server/wifi/MiuiTcpInfo$Field;->REORDERING:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v29, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RCV_RTT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v30, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RCV_SPACE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v31, Lcom/android/server/wifi/MiuiTcpInfo$Field;->TOTAL_RETRANS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v32, Lcom/android/server/wifi/MiuiTcpInfo$Field;->PACING_RATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v33, Lcom/android/server/wifi/MiuiTcpInfo$Field;->MAX_PACING_RATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v34, Lcom/android/server/wifi/MiuiTcpInfo$Field;->BYTES_ACKED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v35, Lcom/android/server/wifi/MiuiTcpInfo$Field;->BYTES_RECEIVED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v36, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SEGS_OUT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v37, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SEGS_IN:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v38, Lcom/android/server/wifi/MiuiTcpInfo$Field;->NOTSENT_BYTES:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v39, Lcom/android/server/wifi/MiuiTcpInfo$Field;->MIN_RTT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v40, Lcom/android/server/wifi/MiuiTcpInfo$Field;->DATA_SEGS_IN:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v41, Lcom/android/server/wifi/MiuiTcpInfo$Field;->DATA_SEGS_OUT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v42, Lcom/android/server/wifi/MiuiTcpInfo$Field;->DELIVERY_RATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v43, Lcom/android/server/wifi/MiuiTcpInfo$Field;->BUSY_TIME:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v44, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RWND_LIMITED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v45, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SNDBUF_LIMITED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v46, Lcom/android/server/wifi/MiuiTcpInfo$Field;->DELIVERED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v47, Lcom/android/server/wifi/MiuiTcpInfo$Field;->DELIVERED_CE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v48, Lcom/android/server/wifi/MiuiTcpInfo$Field;->BYTES_SEND:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v49, Lcom/android/server/wifi/MiuiTcpInfo$Field;->BYTES_RETRANS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v50, Lcom/android/server/wifi/MiuiTcpInfo$Field;->DACK_DUPS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v51, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RECORD_SEEN:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v52, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RCV_OOOPACK:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    sget-object v53, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SND_WND:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    filled-new-array/range {v0 .. v53}, [Lcom/android/server/wifi/MiuiTcpInfo$Field;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 5

    .line 23
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "STATE"

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->STATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 24
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "CASTATE"

    invoke-direct {v0, v1, v3, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->CASTATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 25
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "RETRANSMITS"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RETRANSMITS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 26
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "PROBES"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->PROBES:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 27
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "BACKOFF"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->BACKOFF:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 28
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "OPTIONS"

    const/4 v4, 0x5

    invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->OPTIONS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 29
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "WSCALE"

    const/4 v4, 0x6

    invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->WSCALE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 30
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "DELIVERY_RATE_APP_LIMITED"

    const/4 v4, 0x7

    invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->DELIVERY_RATE_APP_LIMITED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 32
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "RTO"

    const/16 v3, 0x8

    invoke-direct {v0, v1, v3, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RTO:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 33
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "ATO"

    const/16 v4, 0x9

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->ATO:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 36
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "SND_MSS"

    const/16 v4, 0xa

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SND_MSS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 37
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "RCV_MSS"

    const/16 v4, 0xb

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RCV_MSS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 38
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "UNACKED"

    const/16 v4, 0xc

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->UNACKED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 39
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "SACKED"

    const/16 v4, 0xd

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SACKED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 40
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "LOST"

    const/16 v4, 0xe

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->LOST:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 41
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "RETRANS"

    const/16 v4, 0xf

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RETRANS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 42
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "FACKETS"

    const/16 v4, 0x10

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->FACKETS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 44
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "LAST_DATA_SENT"

    const/16 v4, 0x11

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->LAST_DATA_SENT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 45
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "LAST_ACK_SENT"

    const/16 v4, 0x12

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->LAST_ACK_SENT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 46
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "LAST_DATA_RECV"

    const/16 v4, 0x13

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->LAST_DATA_RECV:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 47
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "LAST_ACK_RECV"

    const/16 v4, 0x14

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->LAST_ACK_RECV:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 49
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "PMTU"

    const/16 v4, 0x15

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->PMTU:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 50
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "RCV_SSTHRESH"

    const/16 v4, 0x16

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RCV_SSTHRESH:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 51
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "RTT"

    const/16 v4, 0x17

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RTT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 52
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "RTTVAR"

    const/16 v4, 0x18

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RTTVAR:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 53
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "SND_SSTHRESH"

    const/16 v4, 0x19

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SND_SSTHRESH:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 54
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "SND_CWND"

    const/16 v4, 0x1a

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SND_CWND:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 55
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "ADVMSS"

    const/16 v4, 0x1b

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->ADVMSS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 56
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "REORDERING"

    const/16 v4, 0x1c

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->REORDERING:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 57
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "RCV_RTT"

    const/16 v4, 0x1d

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RCV_RTT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 59
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "RCV_SPACE"

    const/16 v4, 0x1e

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RCV_SPACE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 61
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "TOTAL_RETRANS"

    const/16 v4, 0x1f

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->TOTAL_RETRANS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 63
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "PACING_RATE"

    const/16 v4, 0x20

    invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->PACING_RATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 64
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "MAX_PACING_RATE"

    const/16 v4, 0x21

    invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->MAX_PACING_RATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 65
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "BYTES_ACKED"

    const/16 v4, 0x22

    invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->BYTES_ACKED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 66
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "BYTES_RECEIVED"

    const/16 v4, 0x23

    invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->BYTES_RECEIVED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 67
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "SEGS_OUT"

    const/16 v4, 0x24

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SEGS_OUT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 68
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "SEGS_IN"

    const/16 v4, 0x25

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SEGS_IN:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 69
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "NOTSENT_BYTES"

    const/16 v4, 0x26

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->NOTSENT_BYTES:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 70
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "MIN_RTT"

    const/16 v4, 0x27

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->MIN_RTT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 71
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "DATA_SEGS_IN"

    const/16 v4, 0x28

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->DATA_SEGS_IN:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 72
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "DATA_SEGS_OUT"

    const/16 v4, 0x29

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->DATA_SEGS_OUT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 73
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "DELIVERY_RATE"

    const/16 v4, 0x2a

    invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->DELIVERY_RATE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 74
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "BUSY_TIME"

    const/16 v4, 0x2b

    invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->BUSY_TIME:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 75
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "RWND_LIMITED"

    const/16 v4, 0x2c

    invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RWND_LIMITED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 76
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "SNDBUF_LIMITED"

    const/16 v4, 0x2d

    invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SNDBUF_LIMITED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 77
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "DELIVERED"

    const/16 v4, 0x2e

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->DELIVERED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 78
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "DELIVERED_CE"

    const/16 v4, 0x2f

    invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->DELIVERED_CE:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 79
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "BYTES_SEND"

    const/16 v4, 0x30

    invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->BYTES_SEND:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 80
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "BYTES_RETRANS"

    const/16 v4, 0x31

    invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->BYTES_RETRANS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 81
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "DACK_DUPS"

    const/16 v3, 0x32

    invoke-direct {v0, v1, v3, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->DACK_DUPS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 82
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "RECORD_SEEN"

    const/16 v3, 0x33

    invoke-direct {v0, v1, v3, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RECORD_SEEN:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 83
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "RCV_OOOPACK"

    const/16 v3, 0x34

    invoke-direct {v0, v1, v3, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RCV_OOOPACK:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 84
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    const-string v1, "SND_WND"

    const/16 v3, 0x35

    invoke-direct {v0, v1, v3, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SND_WND:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 22
    invoke-static {}, Lcom/android/server/wifi/MiuiTcpInfo$Field;->$values()[Lcom/android/server/wifi/MiuiTcpInfo$Field;

    move-result-object v0

    sput-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->$VALUES:[Lcom/android/server/wifi/MiuiTcpInfo$Field;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "s"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 89
    iput p3, p0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->size:I

    .line 90
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/wifi/MiuiTcpInfo$Field;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 22
    const-class v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;

    return-object v0
.end method

.method public static values()[Lcom/android/server/wifi/MiuiTcpInfo$Field;
    .locals 1

    .line 22
    sget-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->$VALUES:[Lcom/android/server/wifi/MiuiTcpInfo$Field;

    invoke-virtual {v0}, [Lcom/android/server/wifi/MiuiTcpInfo$Field;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/wifi/MiuiTcpInfo$Field;

    return-object v0
.end method
