.class Lcom/android/server/wifi/MiuiTcpSocketTracker$1;
.super Ljava/lang/Object;
.source "MiuiTcpSocketTracker.java"

# interfaces
.implements Landroid/provider/DeviceConfig$OnPropertiesChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wifi/MiuiTcpSocketTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wifi/MiuiTcpSocketTracker;


# direct methods
.method constructor <init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wifi/MiuiTcpSocketTracker;

    .line 143
    iput-object p1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$1;->this$0:Lcom/android/server/wifi/MiuiTcpSocketTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPropertiesChanged(Landroid/provider/DeviceConfig$Properties;)V
    .locals 5
    .param p1, "properties"    # Landroid/provider/DeviceConfig$Properties;

    .line 146
    iget-object v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$1;->this$0:Lcom/android/server/wifi/MiuiTcpSocketTracker;

    invoke-static {v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->-$$Nest$fgetmDependencies(Lcom/android/server/wifi/MiuiTcpSocketTracker;)Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;

    move-result-object v1

    const-string/jumbo v2, "tcp_min_packets_threshold"

    const/16 v3, 0xa

    const-string v4, "connectivity"

    invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->getDeviceConfigPropertyInt(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->-$$Nest$fputmMinPacketsThreshold(Lcom/android/server/wifi/MiuiTcpSocketTracker;I)V

    .line 150
    iget-object v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$1;->this$0:Lcom/android/server/wifi/MiuiTcpSocketTracker;

    invoke-static {v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->-$$Nest$fgetmDependencies(Lcom/android/server/wifi/MiuiTcpSocketTracker;)Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;

    move-result-object v1

    const-string/jumbo v2, "tcp_packets_fail_percentage"

    const/16 v3, 0x50

    invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->getDeviceConfigPropertyInt(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->-$$Nest$fputmTcpPacketsFailRateThreshold(Lcom/android/server/wifi/MiuiTcpSocketTracker;I)V

    .line 154
    return-void
.end method
