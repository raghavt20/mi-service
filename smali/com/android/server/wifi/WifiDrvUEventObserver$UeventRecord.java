class com.android.server.wifi.WifiDrvUEventObserver$UeventRecord {
	 /* .source "WifiDrvUEventObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wifi/WifiDrvUEventObserver; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "UeventRecord" */
} // .end annotation
/* # instance fields */
private java.lang.String mEvent;
private java.lang.String mInfo;
private Long mTime;
/* # direct methods */
public com.android.server.wifi.WifiDrvUEventObserver$UeventRecord ( ) {
/* .locals 2 */
/* .param p1, "event" # Ljava/lang/String; */
/* .param p2, "info" # Ljava/lang/String; */
/* .line 196 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 197 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;->mTime:J */
/* .line 198 */
this.mEvent = p1;
/* .line 199 */
this.mInfo = p2;
/* .line 200 */
return;
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 9 */
/* .line 204 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 205 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* const-string/jumbo v1, "time=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 206 */
java.util.Calendar .getInstance ( );
/* .line 207 */
/* .local v1, "c":Ljava/util/Calendar; */
/* iget-wide v2, p0, Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;->mTime:J */
(( java.util.Calendar ) v1 ).setTimeInMillis ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V
/* .line 208 */
final String v8 = "%tm-%td %tH:%tM:%tS.%tL"; // const-string v8, "%tm-%td %tH:%tM:%tS.%tL"
/* move-object v2, v1 */
/* move-object v3, v1 */
/* move-object v4, v1 */
/* move-object v5, v1 */
/* move-object v6, v1 */
/* move-object v7, v1 */
/* filled-new-array/range {v2 ..v7}, [Ljava/lang/Object; */
java.lang.String .format ( v8,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 209 */
final String v2 = " event="; // const-string v2, " event="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 210 */
v2 = this.mEvent;
final String v3 = "<null>"; // const-string v3, "<null>"
/* if-nez v2, :cond_0 */
/* move-object v2, v3 */
} // :cond_0
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 211 */
final String v2 = " info="; // const-string v2, " info="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 212 */
v2 = this.mInfo;
/* if-nez v2, :cond_1 */
} // :cond_1
/* move-object v3, v2 */
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 214 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
