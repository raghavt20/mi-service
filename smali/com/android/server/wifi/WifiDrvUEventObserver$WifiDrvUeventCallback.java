public abstract class com.android.server.wifi.WifiDrvUEventObserver$WifiDrvUeventCallback {
	 /* .source "WifiDrvUEventObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wifi/WifiDrvUEventObserver; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "WifiDrvUeventCallback" */
} // .end annotation
/* # virtual methods */
public abstract void onCoexModeChange ( Integer p0 ) {
} // .end method
public abstract void onDataStall ( Integer p0 ) {
} // .end method
public abstract void onRecoveryComplete ( Integer p0, Boolean p1 ) {
} // .end method
