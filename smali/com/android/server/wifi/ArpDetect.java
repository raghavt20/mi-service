public class com.android.server.wifi.ArpDetect {
	 /* .source "ArpDetect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wifi/ArpDetect$MessageHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ARP_DETECT_TIMEOUT_MS;
private static final Integer ARP_GATEWAY_DETECT_TIMES;
private static final Integer ARP_PACKET_INTERVAL_NORMAL_MS;
private static final Integer ARP_PACKET_INTERVAL_QUICK_MS;
private static final java.lang.String CLOUD_MULTI_GW_DETECT_ENABLED;
private static final Boolean DEBUG;
private static final java.lang.String EXTRA_ARP_DETECT_GATEWAY_SIZE;
private static final Integer FD_EVENTS;
private static final java.net.Inet4Address IPV4_ADDR_ANY;
private static final Integer MAX_GATEWAY_LIST_LEN;
private static final Integer MAX_PACKET_LEN;
public static final java.lang.String MULTI_GATEWAY_DETECT_STATE_CHANGED;
private static final Integer MULTI_GW_RECOVERY_TIMEOUT_MS;
public static Integer STATE_MULTI_GW_RECOVERY_STARTED;
public static Integer STATE_MULTI_GW_RECOVERY_STOPED;
private static final java.lang.String TAG;
private static final Integer UNREGISTER_THIS_FD;
private static com.android.server.wifi.ArpDetect sIntance;
/* # instance fields */
private final Integer CMD_MULTI_GW_RECOVERY_STOP;
private final Integer CMD_STOP_RECV_ARP_PACKET;
private Boolean isMultiGwFind;
private java.io.FileDescriptor mArpRecvSock;
private java.io.FileDescriptor mArpSendSock;
private android.content.Context mContext;
private Integer mCurRecoveryState;
private java.net.Inet4Address mGatewayAddress;
private Integer mGatewayChangeCount;
private java.util.ArrayList mGatewayMacList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
} // .end field
private android.os.Handler mHandler;
private java.lang.String mIfaceName;
private java.net.SocketAddress mInterfaceBroadcastAddr;
private java.net.Inet4Address mLocalIpAddress;
private mLocalMacAddress;
private Integer mNetworkId;
private android.os.MessageQueue mQueue;
private com.android.server.wifi.ArpDetect$MessageHandler msgHandler;
private Boolean stopDetect;
/* # direct methods */
public static Integer $r8$lambda$1dmcpER0oVUAqBGZYlT-XOrbP8I ( com.android.server.wifi.ArpDetect p0, java.io.FileDescriptor p1, Integer p2 ) { //synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/wifi/ArpDetect;->lambda$registerArpRecvQueue$0(Ljava/io/FileDescriptor;I)I */
} // .end method
static java.lang.String -$$Nest$mhidenPrivateInfo ( com.android.server.wifi.ArpDetect p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String; */
} // .end method
static void -$$Nest$mlogd ( com.android.server.wifi.ArpDetect p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wifi/ArpDetect;->logd(Ljava/lang/String;)V */
return;
} // .end method
static Boolean -$$Nest$msendArpPacket ( com.android.server.wifi.ArpDetect p0, Object[] p1, java.net.Inet4Address p2, Object[] p3, java.net.Inet4Address p4, Integer p5, Integer p6 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct/range {p0 ..p6}, Lcom/android/server/wifi/ArpDetect;->sendArpPacket([BLjava/net/Inet4Address;[BLjava/net/Inet4Address;II)Z */
} // .end method
static void -$$Nest$msetMultiGwRecoveryState ( com.android.server.wifi.ArpDetect p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wifi/ArpDetect;->setMultiGwRecoveryState(I)V */
return;
} // .end method
static void -$$Nest$munregisterArpRecvQueue ( com.android.server.wifi.ArpDetect p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->unregisterArpRecvQueue()V */
return;
} // .end method
static com.android.server.wifi.ArpDetect ( ) {
/* .locals 2 */
/* .line 70 */
int v0 = 0; // const/4 v0, 0x0
com.android.net.module.util.Inet4AddressUtils .intToInet4AddressHTH ( v0 );
/* .line 81 */
/* .line 82 */
int v0 = 1; // const/4 v0, 0x1
return;
} // .end method
public com.android.server.wifi.ArpDetect ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 98 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 78 */
/* const/16 v0, 0x64 */
/* iput v0, p0, Lcom/android/server/wifi/ArpDetect;->CMD_STOP_RECV_ARP_PACKET:I */
/* .line 79 */
/* const/16 v0, 0x65 */
/* iput v0, p0, Lcom/android/server/wifi/ArpDetect;->CMD_MULTI_GW_RECOVERY_STOP:I */
/* .line 90 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->isMultiGwFind:Z */
/* .line 91 */
/* iput-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->stopDetect:Z */
/* .line 92 */
/* iput v0, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayChangeCount:I */
/* .line 93 */
/* iput v0, p0, Lcom/android/server/wifi/ArpDetect;->mCurRecoveryState:I */
/* .line 99 */
this.mContext = p1;
/* .line 100 */
return;
} // .end method
private void checkIsMultiGwFind ( ) {
/* .locals 5 */
/* .line 554 */
v0 = this.mGatewayMacList;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
int v1 = 2; // const/4 v1, 0x2
/* if-lt v0, v1, :cond_1 */
/* .line 555 */
final String v0 = "MultiGateway detected!"; // const-string v0, "MultiGateway detected!"
final String v1 = "ArpDetect"; // const-string v1, "ArpDetect"
android.util.Log .d ( v1,v0 );
/* .line 556 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->isMultiGwFind:Z */
/* .line 557 */
v0 = this.msgHandler;
/* const/16 v2, 0x65 */
/* const-wide/32 v3, 0x1d4c0 */
(( com.android.server.wifi.ArpDetect$MessageHandler ) v0 ).sendEmptyMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v0, v2, v3, v4}, Lcom/android/server/wifi/ArpDetect$MessageHandler;->sendEmptyMessageDelayed(IJ)Z
/* .line 560 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v2 = this.mGatewayMacList;
v2 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* if-ge v0, v2, :cond_0 */
/* .line 561 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "mGatewayMacList["; // const-string v3, "mGatewayMacList["
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "] : "; // const-string v3, "] : "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mGatewayMacList;
/* .line 562 */
(( java.util.ArrayList ) v3 ).get ( v0 ); // invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, [B */
com.android.internal.util.HexDump .toHexString ( v3 );
/* invoke-direct {p0, v3}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " ,size:"; // const-string v3, " ,size:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mGatewayMacList;
/* .line 563 */
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 561 */
android.util.Log .d ( v1,v2 );
/* .line 560 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 565 */
} // .end local v0 # "i":I
} // :cond_0
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "android.net.wifi.GATEWAY_DETECT_STATE_CHANGED"; // const-string v1, "android.net.wifi.GATEWAY_DETECT_STATE_CHANGED"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 566 */
/* .local v0, "intent":Landroid/content/Intent; */
v1 = this.mGatewayMacList;
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
final String v2 = "extra_arp_detect_gateway_size"; // const-string v2, "extra_arp_detect_gateway_size"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 567 */
v1 = this.mContext;
v2 = android.os.UserHandle.SYSTEM;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 569 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_1
return;
} // .end method
private void closeSocket ( java.io.FileDescriptor p0 ) {
/* .locals 3 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .line 645 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 646 */
try { // :try_start_0
android.net.util.SocketUtils .closeSocket ( p1 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 648 */
/* :catch_0 */
/* move-exception v0 */
/* .line 649 */
/* .local v0, "ignored":Ljava/io/IOException; */
final String v1 = "ArpDetect"; // const-string v1, "ArpDetect"
final String v2 = "close socket fail"; // const-string v2, "close socket fail"
android.util.Log .e ( v1,v2 );
/* .line 650 */
} // .end local v0 # "ignored":Ljava/io/IOException;
} // :cond_0
} // :goto_0
/* nop */
/* .line 651 */
} // :goto_1
return;
} // .end method
private getCurGatewayMacFromRoute ( ) {
/* .locals 2 */
/* .line 419 */
v0 = this.mGatewayAddress;
(( java.net.Inet4Address ) v0 ).getHostAddress ( ); // invoke-virtual {v0}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;
v1 = this.mIfaceName;
/* invoke-direct {p0, v0, v1}, Lcom/android/server/wifi/ArpDetect;->getMacAddrFromRoute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
/* .line 420 */
/* .local v0, "curGatewayMac":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
/* .line 421 */
v1 = libcore.util.EmptyArray.BYTE;
/* .line 423 */
} // :cond_0
android.net.MacAddress .byteAddrFromStringAddr ( v0 );
} // .end method
public static com.android.server.wifi.ArpDetect getInstance ( ) {
/* .locals 1 */
/* .line 108 */
v0 = com.android.server.wifi.ArpDetect.sIntance;
} // .end method
private java.lang.String getMacAddrFromRoute ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 9 */
/* .param p1, "ipAddress" # Ljava/lang/String; */
/* .param p2, "ifaceName" # Ljava/lang/String; */
/* .line 508 */
final String v0 = "ArpDetect"; // const-string v0, "ArpDetect"
if ( p1 != null) { // if-eqz p1, :cond_7
/* if-nez p2, :cond_0 */
/* goto/16 :goto_7 */
/* .line 512 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 513 */
/* .local v1, "macAddress":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 515 */
/* .local v2, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v3, Ljava/io/BufferedReader; */
/* new-instance v4, Ljava/io/FileReader; */
final String v5 = "/proc/net/arp"; // const-string v5, "/proc/net/arp"
/* invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v2, v3 */
/* .line 517 */
(( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* .line 518 */
/* .local v3, "line":Ljava/lang/String; */
} // :goto_0
(( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v3, v4 */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 519 */
final String v4 = "[ ]+"; // const-string v4, "[ ]+"
(( java.lang.String ) v3 ).split ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 520 */
/* .local v4, "tokens":[Ljava/lang/String; */
/* array-length v5, v4 */
int v6 = 6; // const/4 v6, 0x6
/* if-ge v5, v6, :cond_1 */
/* .line 521 */
/* .line 525 */
} // :cond_1
int v5 = 0; // const/4 v5, 0x0
/* aget-object v5, v4, v5 */
/* .line 526 */
/* .local v5, "ip":Ljava/lang/String; */
int v6 = 3; // const/4 v6, 0x3
/* aget-object v6, v4, v6 */
/* .line 527 */
/* .local v6, "mac":Ljava/lang/String; */
int v7 = 5; // const/4 v7, 0x5
/* aget-object v7, v4, v7 */
/* .line 529 */
/* .local v7, "curIfaceName":Ljava/lang/String; */
v8 = (( java.lang.String ) p1 ).equals ( v5 ); // invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_2
v8 = (( java.lang.String ) p2 ).equals ( v7 ); // invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 530 */
/* move-object v1, v6 */
/* .line 531 */
/* .line 533 */
} // .end local v4 # "tokens":[Ljava/lang/String;
} // .end local v5 # "ip":Ljava/lang/String;
} // .end local v6 # "mac":Ljava/lang/String;
} // .end local v7 # "curIfaceName":Ljava/lang/String;
} // :cond_2
/* .line 534 */
} // :cond_3
} // :goto_1
/* if-nez v1, :cond_4 */
/* .line 535 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Did not find remoteAddress {"; // const-string v5, "Did not find remoteAddress {"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v5, "} in /proc/net/arp" */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v4 );
/* :try_end_0 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 543 */
} // .end local v3 # "line":Ljava/lang/String;
} // :cond_4
/* nop */
/* .line 544 */
try { // :try_start_1
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 548 */
} // :cond_5
} // :goto_2
/* .line 546 */
/* :catch_0 */
/* move-exception v0 */
/* .line 549 */
/* .line 542 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 539 */
/* :catch_1 */
/* move-exception v3 */
/* .line 540 */
/* .local v3, "e":Ljava/io/IOException; */
try { // :try_start_2
final String v4 = "Could not read /proc/net/arp to lookup mac address"; // const-string v4, "Could not read /proc/net/arp to lookup mac address"
android.util.Log .e ( v0,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 543 */
} // .end local v3 # "e":Ljava/io/IOException;
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 544 */
try { // :try_start_3
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 537 */
/* :catch_2 */
/* move-exception v3 */
/* .line 538 */
/* .local v3, "e":Ljava/io/FileNotFoundException; */
try { // :try_start_4
final String v4 = "Could not open /proc/net/arp to lookup mac address"; // const-string v4, "Could not open /proc/net/arp to lookup mac address"
android.util.Log .e ( v0,v4 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 543 */
} // .end local v3 # "e":Ljava/io/FileNotFoundException;
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 544 */
try { // :try_start_5
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_0 */
/* .line 550 */
} // :goto_3
/* .line 543 */
} // :goto_4
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 544 */
try { // :try_start_6
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_3 */
/* .line 546 */
/* :catch_3 */
/* move-exception v3 */
/* .line 548 */
} // :cond_6
} // :goto_5
/* nop */
/* .line 549 */
} // :goto_6
/* throw v0 */
/* .line 509 */
} // .end local v1 # "macAddress":Ljava/lang/String;
} // .end local v2 # "reader":Ljava/io/BufferedReader;
} // :cond_7
} // :goto_7
int v0 = 0; // const/4 v0, 0x0
} // .end method
private java.net.NetworkInterface getNetworkInterfaceByName ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 125 */
try { // :try_start_0
java.net.NetworkInterface .getByName ( p1 );
/* :try_end_0 */
/* .catch Ljava/lang/NullPointerException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/net/SocketException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 126 */
/* :catch_0 */
/* move-exception v0 */
/* .line 127 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "ArpDetect"; // const-string v1, "ArpDetect"
final String v2 = "get NetworkInterface faile:"; // const-string v2, "get NetworkInterface faile:"
android.util.Log .e ( v1,v2,v0 );
/* .line 128 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
private Integer getNextGatewayIndex ( ) {
/* .locals 5 */
/* .line 427 */
int v0 = -1; // const/4 v0, -0x1
/* .line 428 */
/* .local v0, "curGatewayIndex":I */
int v1 = -1; // const/4 v1, -0x1
/* .line 430 */
/* .local v1, "nextGatewayIndex":I */
/* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->getCurGatewayMacFromRoute()[B */
/* .line 431 */
/* .local v2, "curGatewayMac":[B */
if ( v2 != null) { // if-eqz v2, :cond_4
/* array-length v3, v2 */
/* if-lez v3, :cond_4 */
v3 = this.mGatewayMacList;
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 432 */
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
int v4 = 2; // const/4 v4, 0x2
/* if-ge v3, v4, :cond_0 */
/* .line 436 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = this.mGatewayMacList;
v4 = (( java.util.ArrayList ) v4 ).size ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
/* if-ge v3, v4, :cond_2 */
/* .line 437 */
v4 = this.mGatewayMacList;
(( java.util.ArrayList ) v4 ).get ( v3 ); // invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v4, [B */
v4 = java.util.Arrays .equals ( v4,v2 );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 438 */
/* move v0, v3 */
/* .line 436 */
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 441 */
} // .end local v3 # "i":I
} // :cond_2
v3 = this.mGatewayMacList;
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v3, v3, -0x1 */
/* if-ge v0, v3, :cond_3 */
/* .line 442 */
/* add-int/lit8 v1, v0, 0x1 */
/* .line 444 */
} // :cond_3
int v1 = 0; // const/4 v1, 0x0
/* .line 446 */
} // :goto_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Current gateway index "; // const-string v4, "Current gateway index "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " ,mac "; // const-string v4, " ,mac "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 447 */
com.android.internal.util.HexDump .toHexString ( v2 );
/* invoke-direct {p0, v4}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 446 */
final String v4 = "ArpDetect"; // const-string v4, "ArpDetect"
android.util.Log .d ( v4,v3 );
/* .line 449 */
/* .line 433 */
} // :cond_4
} // :goto_2
int v3 = -1; // const/4 v3, -0x1
} // .end method
private Boolean handlePacket ( ) {
/* .locals 8 */
/* .line 572 */
final String v0 = ", mac "; // const-string v0, ", mac "
final String v1 = "ArpDetect"; // const-string v1, "ArpDetect"
/* const/16 v2, 0x5dc */
/* new-array v2, v2, [B */
/* .line 574 */
/* .local v2, "mPacket":[B */
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
v4 = this.mArpRecvSock;
/* array-length v5, v2 */
v4 = android.system.Os .read ( v4,v2,v3,v5 );
/* .line 575 */
/* .local v4, "length":I */
com.android.server.wifi.ArpPacket .parseArpPacket ( v2,v4 );
/* .line 576 */
/* .local v5, "packet":Lcom/android/server/wifi/ArpPacket; */
if ( v5 != null) { // if-eqz v5, :cond_0
/* iget-short v6, v5, Lcom/android/server/wifi/ArpPacket;->mOpCode:S */
int v7 = 2; // const/4 v7, 0x2
/* if-ne v6, v7, :cond_0 */
/* .line 577 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Received Arp response from IP "; // const-string v7, "Received Arp response from IP "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mSenderIp;
/* .line 578 */
(( java.net.Inet4Address ) v7 ).getHostAddress ( ); // invoke-virtual {v7}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;
/* invoke-direct {p0, v7}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mSenderHwAddress;
/* .line 579 */
com.android.internal.util.HexDump .toHexString ( v7 );
/* invoke-direct {p0, v7}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " --> target IP "; // const-string v7, " --> target IP "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mTargetIp;
/* .line 580 */
(( java.net.Inet4Address ) v7 ).getHostAddress ( ); // invoke-virtual {v7}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;
/* invoke-direct {p0, v7}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mTtargetHwAddress;
/* .line 582 */
com.android.internal.util.HexDump .toHexString ( v6 );
/* invoke-direct {p0, v6}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 577 */
/* invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->logd(Ljava/lang/String;)V */
/* .line 584 */
v0 = this.mSenderIp;
v6 = this.mGatewayAddress;
v0 = (( java.net.Inet4Address ) v0 ).equals ( v6 ); // invoke-virtual {v0, v6}, Ljava/net/Inet4Address;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mSenderHwAddress;
v6 = this.mLocalMacAddress;
/* .line 585 */
v0 = java.util.Arrays .equals ( v0,v6 );
/* if-nez v0, :cond_0 */
v0 = this.mSenderHwAddress;
/* .line 586 */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->isGatewayMacSaved([B)Z */
/* if-nez v0, :cond_0 */
v0 = this.mGatewayMacList;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 588 */
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
int v6 = 4; // const/4 v6, 0x4
/* if-ge v0, v6, :cond_0 */
/* .line 589 */
v0 = this.mGatewayMacList;
v6 = this.mSenderHwAddress;
(( java.util.ArrayList ) v0 ).add ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 590 */
/* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->checkIsMultiGwFind()V */
/* :try_end_0 */
/* .catch Landroid/system/ErrnoException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Lcom/android/server/wifi/ArpPacket$ParseException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 599 */
} // .end local v4 # "length":I
} // .end local v5 # "packet":Lcom/android/server/wifi/ArpPacket;
} // :cond_0
/* .line 597 */
/* :catch_0 */
/* move-exception v0 */
/* .line 598 */
/* .local v0, "e":Lcom/android/server/wifi/ArpPacket$ParseException; */
final String v3 = "Can\'t parse ARP packet: "; // const-string v3, "Can\'t parse ARP packet: "
android.util.Log .e ( v1,v3,v0 );
/* .line 601 */
} // .end local v0 # "e":Lcom/android/server/wifi/ArpPacket$ParseException;
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 593 */
/* :catch_1 */
/* move-exception v0 */
/* .line 594 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 595 */
final String v4 = "handlePacket error"; // const-string v4, "handlePacket error"
android.util.Log .e ( v1,v4 );
/* .line 596 */
} // .end method
private java.lang.String hidenPrivateInfo ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "str" # Ljava/lang/String; */
/* .line 117 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
int v1 = 7; // const/4 v1, 0x7
/* if-ge v0, v1, :cond_0 */
/* .line 119 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
int v1 = 0; // const/4 v1, 0x0
int v2 = 3; // const/4 v2, 0x3
(( java.lang.String ) p1 ).substring ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "****"; // const-string v1, "****"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* sub-int/2addr v1, v2 */
(( java.lang.String ) p1 ).substring ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 120 */
/* .local v0, "newStr":Ljava/lang/String; */
/* .line 118 */
} // .end local v0 # "newStr":Ljava/lang/String;
} // :cond_1
} // :goto_0
} // .end method
private void initArpRecvSocket ( ) {
/* .locals 5 */
/* .line 151 */
try { // :try_start_0
v0 = this.mIfaceName;
/* invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->getNetworkInterfaceByName(Ljava/lang/String;)Ljava/net/NetworkInterface; */
/* .line 152 */
/* .local v0, "iface":Ljava/net/NetworkInterface; */
/* if-nez v0, :cond_0 */
/* .line 153 */
return;
/* .line 155 */
} // :cond_0
v1 = (( java.net.NetworkInterface ) v0 ).getIndex ( ); // invoke-virtual {v0}, Ljava/net/NetworkInterface;->getIndex()I
/* .line 156 */
/* .local v1, "ifaceIndex":I */
/* or-int/2addr v3, v4 */
int v4 = 0; // const/4 v4, 0x0
android.system.Os .socket ( v2,v3,v4 );
this.mArpRecvSock = v2;
/* .line 157 */
android.net.util.SocketUtils .makePacketSocketAddress ( v2,v1 );
/* .line 158 */
/* .local v2, "addr":Ljava/net/SocketAddress; */
v3 = this.mArpRecvSock;
android.system.Os .bind ( v3,v2 );
/* :try_end_0 */
/* .catch Ljava/net/SocketException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/system/ErrnoException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 163 */
} // .end local v0 # "iface":Ljava/net/NetworkInterface;
} // .end local v1 # "ifaceIndex":I
} // .end local v2 # "addr":Ljava/net/SocketAddress;
/* .line 159 */
/* :catch_0 */
/* move-exception v0 */
/* .line 160 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "ArpDetect"; // const-string v1, "ArpDetect"
final String v2 = "Error creating ARP recv socket"; // const-string v2, "Error creating ARP recv socket"
android.util.Log .e ( v1,v2,v0 );
/* .line 161 */
v1 = this.mArpRecvSock;
/* invoke-direct {p0, v1}, Lcom/android/server/wifi/ArpDetect;->closeSocket(Ljava/io/FileDescriptor;)V */
/* .line 162 */
int v1 = 0; // const/4 v1, 0x0
this.mArpRecvSock = v1;
/* .line 164 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void initArpSendSocket ( ) {
/* .locals 5 */
/* .line 134 */
try { // :try_start_0
v0 = this.mIfaceName;
/* invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->getNetworkInterfaceByName(Ljava/lang/String;)Ljava/net/NetworkInterface; */
/* .line 135 */
/* .local v0, "iface":Ljava/net/NetworkInterface; */
/* if-nez v0, :cond_0 */
/* .line 136 */
return;
/* .line 138 */
} // :cond_0
v1 = (( java.net.NetworkInterface ) v0 ).getIndex ( ); // invoke-virtual {v0}, Ljava/net/NetworkInterface;->getIndex()I
/* .line 139 */
/* .local v1, "ifaceIndex":I */
/* or-int/2addr v3, v4 */
int v4 = 0; // const/4 v4, 0x0
android.system.Os .socket ( v2,v3,v4 );
this.mArpSendSock = v2;
/* .line 140 */
android.net.util.SocketUtils .makePacketSocketAddress ( v2,v1 );
/* .line 141 */
/* .local v2, "addr":Ljava/net/SocketAddress; */
v3 = this.mArpSendSock;
android.system.Os .bind ( v3,v2 );
/* :try_end_0 */
/* .catch Ljava/net/SocketException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/system/ErrnoException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 146 */
} // .end local v0 # "iface":Ljava/net/NetworkInterface;
} // .end local v1 # "ifaceIndex":I
} // .end local v2 # "addr":Ljava/net/SocketAddress;
/* .line 142 */
/* :catch_0 */
/* move-exception v0 */
/* .line 143 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "ArpDetect"; // const-string v1, "ArpDetect"
final String v2 = "Error creating ARP send socket"; // const-string v2, "Error creating ARP send socket"
android.util.Log .e ( v1,v2,v0 );
/* .line 144 */
v1 = this.mArpSendSock;
/* invoke-direct {p0, v1}, Lcom/android/server/wifi/ArpDetect;->closeSocket(Ljava/io/FileDescriptor;)V */
/* .line 145 */
int v1 = 0; // const/4 v1, 0x0
this.mArpSendSock = v1;
/* .line 147 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private Boolean initSocket ( ) {
/* .locals 4 */
/* .line 167 */
v0 = this.mArpSendSock;
/* if-nez v0, :cond_0 */
/* .line 168 */
/* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->initArpSendSocket()V */
/* .line 169 */
} // :cond_0
v0 = this.mArpRecvSock;
/* if-nez v0, :cond_1 */
/* .line 170 */
/* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->initArpRecvSocket()V */
/* .line 171 */
} // :cond_1
v0 = this.mInterfaceBroadcastAddr;
/* if-nez v0, :cond_2 */
/* .line 172 */
v0 = this.mIfaceName;
/* invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->getNetworkInterfaceByName(Ljava/lang/String;)Ljava/net/NetworkInterface; */
/* .line 173 */
/* .local v0, "iface":Ljava/net/NetworkInterface; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 174 */
v1 = (( java.net.NetworkInterface ) v0 ).getIndex ( ); // invoke-virtual {v0}, Ljava/net/NetworkInterface;->getIndex()I
/* .line 175 */
/* .local v1, "ifaceIndex":I */
v3 = com.android.server.wifi.ArpPacket.ETHER_BROADCAST;
android.net.util.SocketUtils .makePacketSocketAddress ( v2,v1,v3 );
this.mInterfaceBroadcastAddr = v2;
/* .line 179 */
} // .end local v0 # "iface":Ljava/net/NetworkInterface;
} // .end local v1 # "ifaceIndex":I
} // :cond_2
v0 = this.mArpSendSock;
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = this.mArpRecvSock;
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = this.mInterfaceBroadcastAddr;
/* if-nez v0, :cond_3 */
/* .line 183 */
} // :cond_3
int v0 = 1; // const/4 v0, 0x1
/* .line 180 */
} // :cond_4
} // :goto_0
final String v0 = "ArpDetect"; // const-string v0, "ArpDetect"
final String v1 = "initArpSocket fail"; // const-string v1, "initArpSocket fail"
android.util.Log .e ( v0,v1 );
/* .line 181 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isGatewayMacSaved ( Object[] p0 ) {
/* .locals 3 */
/* .param p1, "macAddr" # [B */
/* .line 453 */
v0 = this.mGatewayMacList;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-gtz v0, :cond_0 */
/* .line 454 */
/* .line 455 */
} // :cond_0
v0 = this.mGatewayMacList;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-lez v0, :cond_2 */
/* .line 456 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v2 = this.mGatewayMacList;
v2 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* if-ge v0, v2, :cond_2 */
/* .line 457 */
v2 = this.mGatewayMacList;
(( java.util.ArrayList ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, [B */
v2 = java.util.Arrays .equals ( v2,p1 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 458 */
int v1 = 1; // const/4 v1, 0x1
/* .line 456 */
} // :cond_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 461 */
} // .end local v0 # "i":I
} // :cond_2
} // .end method
private Boolean isMultiGatewayExist ( ) {
/* .locals 1 */
/* .line 345 */
/* iget-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->isMultiGwFind:Z */
} // .end method
private Boolean isPermArpRoute ( java.lang.String p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 11 */
/* .param p1, "ipAddress" # Ljava/lang/String; */
/* .param p2, "macAddress" # Ljava/lang/String; */
/* .param p3, "ifaceName" # Ljava/lang/String; */
/* .line 465 */
final String v0 = "ArpDetect"; // const-string v0, "ArpDetect"
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_6
if ( p2 != null) { // if-eqz p2, :cond_6
/* if-nez p3, :cond_0 */
/* goto/16 :goto_7 */
/* .line 469 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 471 */
/* .local v2, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v3, Ljava/io/BufferedReader; */
/* new-instance v4, Ljava/io/FileReader; */
final String v5 = "/proc/net/arp"; // const-string v5, "/proc/net/arp"
/* invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v2, v3 */
/* .line 473 */
(( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* .line 474 */
/* .local v3, "line":Ljava/lang/String; */
} // :goto_0
(( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v3, v4 */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 475 */
final String v4 = "[ ]+"; // const-string v4, "[ ]+"
(( java.lang.String ) v3 ).split ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 476 */
/* .local v4, "tokens":[Ljava/lang/String; */
/* array-length v5, v4 */
int v6 = 6; // const/4 v6, 0x6
/* if-ge v5, v6, :cond_1 */
/* .line 477 */
/* .line 481 */
} // :cond_1
/* aget-object v5, v4, v1 */
/* .line 482 */
/* .local v5, "ip":Ljava/lang/String; */
int v6 = 2; // const/4 v6, 0x2
/* aget-object v6, v4, v6 */
/* .line 483 */
/* .local v6, "flags":Ljava/lang/String; */
int v7 = 3; // const/4 v7, 0x3
/* aget-object v7, v4, v7 */
/* .line 484 */
/* .local v7, "mac":Ljava/lang/String; */
int v8 = 5; // const/4 v8, 0x5
/* aget-object v8, v4, v8 */
/* .line 485 */
/* .local v8, "curIfaceName":Ljava/lang/String; */
final String v9 = "0x6"; // const-string v9, "0x6"
v9 = (( java.lang.String ) v6 ).equals ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_2
v9 = (( java.lang.String ) p3 ).equals ( v8 ); // invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_2
v9 = (( java.lang.String ) p1 ).equals ( v5 ); // invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_2
/* .line 486 */
(( java.lang.String ) p2 ).toLowerCase ( ); // invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
(( java.lang.String ) v7 ).toLowerCase ( ); // invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
v9 = (( java.lang.String ) v9 ).equals ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_2
/* .line 487 */
final String v9 = "neigh state is permanent"; // const-string v9, "neigh state is permanent"
android.util.Log .d ( v0,v9 );
/* :try_end_0 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 488 */
/* nop */
/* .line 497 */
/* nop */
/* .line 498 */
try { // :try_start_1
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 502 */
/* .line 500 */
/* :catch_0 */
/* move-exception v0 */
/* .line 488 */
} // :goto_1
int v0 = 1; // const/4 v0, 0x1
/* .line 490 */
} // .end local v4 # "tokens":[Ljava/lang/String;
} // .end local v5 # "ip":Ljava/lang/String;
} // .end local v6 # "flags":Ljava/lang/String;
} // .end local v7 # "mac":Ljava/lang/String;
} // .end local v8 # "curIfaceName":Ljava/lang/String;
} // :cond_2
/* .line 497 */
} // .end local v3 # "line":Ljava/lang/String;
} // :cond_3
/* nop */
/* .line 498 */
try { // :try_start_2
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 502 */
} // :cond_4
} // :goto_2
/* .line 500 */
/* :catch_1 */
/* move-exception v0 */
/* .line 503 */
/* .line 496 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 493 */
/* :catch_2 */
/* move-exception v3 */
/* .line 494 */
/* .local v3, "e":Ljava/io/IOException; */
try { // :try_start_3
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Could not read /proc/net/arp "; // const-string v5, "Could not read /proc/net/arp "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v4 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 497 */
} // .end local v3 # "e":Ljava/io/IOException;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 498 */
try { // :try_start_4
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .line 491 */
/* :catch_3 */
/* move-exception v3 */
/* .line 492 */
/* .local v3, "e":Ljava/io/FileNotFoundException; */
try { // :try_start_5
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Could not open /proc/net/arp "; // const-string v5, "Could not open /proc/net/arp "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v4 );
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
/* .line 497 */
} // .end local v3 # "e":Ljava/io/FileNotFoundException;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 498 */
try { // :try_start_6
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_1 */
/* .line 504 */
} // :goto_3
/* .line 497 */
} // :goto_4
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 498 */
try { // :try_start_7
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_4 */
/* .line 500 */
/* :catch_4 */
/* move-exception v1 */
/* .line 502 */
} // :cond_5
} // :goto_5
/* nop */
/* .line 503 */
} // :goto_6
/* throw v0 */
/* .line 466 */
} // .end local v2 # "reader":Ljava/io/BufferedReader;
} // :cond_6
} // :goto_7
} // .end method
private Integer lambda$registerArpRecvQueue$0 ( java.io.FileDescriptor p0, Integer p1 ) { //synthethic
/* .locals 1 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "events" # I */
/* .line 618 */
/* iget-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->stopDetect:Z */
/* if-nez v0, :cond_1 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->handlePacket()Z */
/* if-nez v0, :cond_0 */
/* .line 622 */
} // :cond_0
int v0 = 5; // const/4 v0, 0x5
/* .line 619 */
} // :cond_1
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->unregisterArpRecvQueue()V */
/* .line 620 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void logd ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "str" # Ljava/lang/String; */
/* .line 114 */
return;
} // .end method
public static com.android.server.wifi.ArpDetect makeInstance ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 103 */
/* new-instance v0, Lcom/android/server/wifi/ArpDetect; */
/* invoke-direct {v0, p0}, Lcom/android/server/wifi/ArpDetect;-><init>(Landroid/content/Context;)V */
/* .line 104 */
} // .end method
private Boolean registerArpRecvQueue ( ) {
/* .locals 4 */
/* .line 605 */
final String v0 = "ArpDetect"; // const-string v0, "ArpDetect"
final String v1 = "enter registerArpRecvQueue"; // const-string v1, "enter registerArpRecvQueue"
android.util.Log .d ( v0,v1 );
/* .line 606 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->stopDetect:Z */
/* .line 607 */
v1 = /* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->initSocket()Z */
/* if-nez v1, :cond_0 */
/* .line 608 */
/* .line 611 */
} // :cond_0
/* new-instance v0, Landroid/os/Handler; */
/* invoke-direct {v0}, Landroid/os/Handler;-><init>()V */
this.mHandler = v0;
/* .line 612 */
(( android.os.Handler ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
(( android.os.Looper ) v0 ).getQueue ( ); // invoke-virtual {v0}, Landroid/os/Looper;->getQueue()Landroid/os/MessageQueue;
this.mQueue = v0;
/* .line 614 */
v1 = this.mArpRecvSock;
/* new-instance v2, Lcom/android/server/wifi/ArpDetect$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0}, Lcom/android/server/wifi/ArpDetect$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wifi/ArpDetect;)V */
int v3 = 5; // const/4 v3, 0x5
(( android.os.MessageQueue ) v0 ).addOnFileDescriptorEventListener ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/os/MessageQueue;->addOnFileDescriptorEventListener(Ljava/io/FileDescriptor;ILandroid/os/MessageQueue$OnFileDescriptorEventListener;)V
/* .line 624 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean sendArpPacket ( Object[] p0, java.net.Inet4Address p1, Object[] p2, java.net.Inet4Address p3, Integer p4, Integer p5 ) {
/* .locals 7 */
/* .param p1, "senderMac" # [B */
/* .param p2, "senderIp" # Ljava/net/Inet4Address; */
/* .param p3, "targetMac" # [B */
/* .param p4, "targetIp" # Ljava/net/Inet4Address; */
/* .param p5, "retryTimes" # I */
/* .param p6, "interval" # I */
/* .line 395 */
v0 = com.android.server.wifi.ArpPacket.ETHER_BROADCAST;
/* .line 396 */
(( java.net.Inet4Address ) p2 ).getAddress ( ); // invoke-virtual {p2}, Ljava/net/Inet4Address;->getAddress()[B
/* .line 397 */
(( java.net.Inet4Address ) p4 ).getAddress ( ); // invoke-virtual {p4}, Ljava/net/Inet4Address;->getAddress()[B
int v6 = 1; // const/4 v6, 0x1
/* .line 395 */
/* move-object v1, p1 */
/* move-object v2, p1 */
/* move-object v4, p3 */
/* invoke-static/range {v0 ..v6}, Lcom/android/server/wifi/ArpPacket;->buildArpPacket([B[B[B[B[B[BS)Ljava/nio/ByteBuffer; */
/* .line 398 */
/* .local v0, "packet":Ljava/nio/ByteBuffer; */
v1 = /* invoke-direct {p0, v0, p5, p6}, Lcom/android/server/wifi/ArpDetect;->transmitPacket(Ljava/nio/ByteBuffer;II)Z */
} // .end method
private void setMultiGwRecoveryState ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "state" # I */
/* .line 654 */
/* iget v0, p0, Lcom/android/server/wifi/ArpDetect;->mCurRecoveryState:I */
/* if-ne p1, v0, :cond_0 */
/* .line 655 */
return;
/* .line 657 */
} // :cond_0
/* iput p1, p0, Lcom/android/server/wifi/ArpDetect;->mCurRecoveryState:I */
/* .line 658 */
/* if-ne p1, v0, :cond_1 */
v0 = this.msgHandler;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 659 */
/* const/16 v1, 0x65 */
(( com.android.server.wifi.ArpDetect$MessageHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wifi/ArpDetect$MessageHandler;->removeMessages(I)V
/* .line 661 */
} // :cond_1
return;
} // .end method
private void startMultiGatewayDetect ( Object[] p0, java.net.Inet4Address p1, java.net.Inet4Address p2, Integer p3 ) {
/* .locals 8 */
/* .param p1, "senderMac" # [B */
/* .param p2, "senderIp" # Ljava/net/Inet4Address; */
/* .param p3, "targetIp" # Ljava/net/Inet4Address; */
/* .param p4, "retryTimes" # I */
/* .line 372 */
final String v0 = "ArpDetect"; // const-string v0, "ArpDetect"
final String v1 = "Start multiple Gatway Detect.."; // const-string v1, "Start multiple Gatway Detect.."
android.util.Log .d ( v0,v1 );
/* .line 373 */
/* new-instance v0, Ljava/lang/Thread; */
/* new-instance v7, Lcom/android/server/wifi/ArpDetect$1; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p3 */
/* move-object v4, p1 */
/* move-object v5, p2 */
/* move v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wifi/ArpDetect$1;-><init>(Lcom/android/server/wifi/ArpDetect;Ljava/net/Inet4Address;[BLjava/net/Inet4Address;I)V */
/* invoke-direct {v0, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 389 */
/* .local v0, "thread":Ljava/lang/Thread; */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 390 */
return;
} // .end method
private Boolean transmitPacket ( java.nio.ByteBuffer p0, Integer p1, Integer p2 ) {
/* .locals 8 */
/* .param p1, "buf" # Ljava/nio/ByteBuffer; */
/* .param p2, "retryTimes" # I */
/* .param p3, "interval" # I */
/* .line 403 */
int v0 = 1; // const/4 v0, 0x1
/* .local v0, "i":I */
} // :goto_0
/* if-gt v0, p2, :cond_1 */
/* .line 404 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/android/server/wifi/ArpDetect;->stopDetect:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 405 */
/* .line 407 */
} // :cond_0
v2 = this.mArpSendSock;
(( java.nio.ByteBuffer ) p1 ).array ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B
int v4 = 0; // const/4 v4, 0x0
v5 = (( java.nio.ByteBuffer ) p1 ).limit ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I
int v6 = 0; // const/4 v6, 0x0
v7 = this.mInterfaceBroadcastAddr;
/* invoke-static/range {v2 ..v7}, Landroid/system/Os;->sendto(Ljava/io/FileDescriptor;[BIIILjava/net/SocketAddress;)I */
/* .line 408 */
/* int-to-long v1, p3 */
java.lang.Thread .sleep ( v1,v2 );
/* :try_end_0 */
/* .catch Landroid/system/ErrnoException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 403 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 410 */
} // .end local v0 # "i":I
/* :catch_0 */
/* move-exception v0 */
/* .line 411 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "ArpDetect"; // const-string v1, "ArpDetect"
/* const-string/jumbo v2, "send packet error: " */
android.util.Log .e ( v1,v2,v0 );
/* .line 412 */
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v1 ).interrupt ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
/* .line 413 */
int v1 = 0; // const/4 v1, 0x0
/* .line 414 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_1
/* nop */
/* .line 415 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void unregisterArpRecvQueue ( ) {
/* .locals 2 */
/* .line 628 */
v0 = this.msgHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 629 */
/* const/16 v1, 0x64 */
(( com.android.server.wifi.ArpDetect$MessageHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wifi/ArpDetect$MessageHandler;->removeMessages(I)V
/* .line 632 */
} // :cond_0
v0 = this.mQueue;
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = this.mArpRecvSock;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 633 */
(( android.os.MessageQueue ) v0 ).removeOnFileDescriptorEventListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->removeOnFileDescriptorEventListener(Ljava/io/FileDescriptor;)V
/* .line 634 */
final String v0 = "ArpDetect"; // const-string v0, "ArpDetect"
final String v1 = "removed socket listen"; // const-string v1, "removed socket listen"
android.util.Log .d ( v0,v1 );
/* .line 636 */
} // :cond_1
v0 = this.mArpRecvSock;
/* invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->closeSocket(Ljava/io/FileDescriptor;)V */
/* .line 637 */
int v0 = 0; // const/4 v0, 0x0
this.mArpRecvSock = v0;
/* .line 638 */
this.mQueue = v0;
/* .line 639 */
this.mHandler = v0;
/* .line 640 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->stopDetect:Z */
/* .line 641 */
return;
} // .end method
private static Boolean updateNeighbor ( java.net.Inet4Address p0, Object p1, Integer p2, Object[] p3 ) {
/* .locals 6 */
/* .param p0, "ipAddr" # Ljava/net/Inet4Address; */
/* .param p1, "nudState" # S */
/* .param p2, "ifIndex" # I */
/* .param p3, "macAddr" # [B */
/* .line 350 */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "ArpDetect"; // const-string v1, "ArpDetect"
if ( p0 != null) { // if-eqz p0, :cond_3
/* if-nez p3, :cond_0 */
/* .line 353 */
} // :cond_0
(( java.net.Inet4Address ) p0 ).getHostAddress ( ); // invoke-virtual {p0}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;
v3 = com.android.server.wifi.ArpDetect.IPV4_ADDR_ANY;
(( java.net.Inet4Address ) v3 ).getHostAddress ( ); // invoke-virtual {v3}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
v2 = com.android.server.wifi.ArpPacket.ETHER_ANY;
/* .line 354 */
v2 = java.util.Arrays .equals ( p3,v2 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 359 */
} // :cond_1
int v2 = 1; // const/4 v2, 0x1
com.android.net.module.util.netlink.RtNetlinkNeighborMessage .newNewNeighborMessage ( v2,p0,p1,p2,p3 );
/* .line 362 */
/* .local v3, "msg":[B */
try { // :try_start_0
com.android.net.module.util.netlink.NetlinkUtils .sendOneShotKernelMessage ( v4,v3 );
/* :try_end_0 */
/* .catch Landroid/system/ErrnoException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 366 */
/* nop */
/* .line 367 */
/* .line 363 */
/* :catch_0 */
/* move-exception v2 */
/* .line 364 */
/* .local v2, "e":Landroid/system/ErrnoException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "sendOneShotKernelMessage error:" */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v4 );
/* .line 365 */
/* .line 355 */
} // .end local v2 # "e":Landroid/system/ErrnoException;
} // .end local v3 # "msg":[B
} // :cond_2
} // :goto_0
/* const-string/jumbo v2, "update neigh fail,invaild ip or mac addr" */
android.util.Log .e ( v1,v2 );
/* .line 356 */
/* .line 351 */
} // :cond_3
} // :goto_1
/* const-string/jumbo v2, "update neigh fail,param is null" */
android.util.Log .e ( v1,v2 );
/* .line 352 */
} // .end method
/* # virtual methods */
public Boolean isMultiGatewayNetwork ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "netId" # I */
/* .line 240 */
/* iget v0, p0, Lcom/android/server/wifi/ArpDetect;->mNetworkId:I */
/* if-ne p1, v0, :cond_0 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->isMultiGatewayExist()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isRecoveredOneRound ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "netId" # I */
/* .line 252 */
v0 = (( com.android.server.wifi.ArpDetect ) p0 ).isMultiGatewayNetwork ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wifi/ArpDetect;->isMultiGatewayNetwork(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mGatewayMacList;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v1, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayChangeCount:I */
/* .line 253 */
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
int v2 = 1; // const/4 v2, 0x1
/* sub-int/2addr v0, v2 */
/* if-lt v1, v0, :cond_0 */
/* .line 254 */
/* .line 256 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isRecoveryOngoing ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "netId" # I */
/* .line 244 */
v0 = (( com.android.server.wifi.ArpDetect ) p0 ).isMultiGatewayNetwork ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wifi/ArpDetect;->isMultiGatewayNetwork(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p0, Lcom/android/server/wifi/ArpDetect;->mCurRecoveryState:I */
/* if-eq v0, v1, :cond_0 */
/* .line 246 */
int v0 = 1; // const/4 v0, 0x1
/* .line 248 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean resetGatewayNeighState ( ) {
/* .locals 8 */
/* .line 322 */
int v0 = 0; // const/4 v0, 0x0
/* .line 323 */
/* .local v0, "status":Z */
v1 = /* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->isMultiGatewayExist()Z */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 324 */
/* .line 327 */
} // :cond_0
v1 = this.mIfaceName;
/* invoke-direct {p0, v1}, Lcom/android/server/wifi/ArpDetect;->getNetworkInterfaceByName(Ljava/lang/String;)Ljava/net/NetworkInterface; */
/* .line 328 */
/* .local v1, "iface":Ljava/net/NetworkInterface; */
final String v3 = "ArpDetect"; // const-string v3, "ArpDetect"
/* if-nez v1, :cond_1 */
/* .line 329 */
final String v4 = "get iface fail"; // const-string v4, "get iface fail"
android.util.Log .d ( v3,v4 );
/* .line 330 */
/* .line 332 */
} // :cond_1
v2 = (( java.net.NetworkInterface ) v1 ).getIndex ( ); // invoke-virtual {v1}, Ljava/net/NetworkInterface;->getIndex()I
/* .line 333 */
/* .local v2, "ifaceIndex":I */
/* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->getCurGatewayMacFromRoute()[B */
/* .line 334 */
/* .local v4, "curGatewayMac":[B */
if ( v4 != null) { // if-eqz v4, :cond_2
/* array-length v5, v4 */
/* if-lez v5, :cond_2 */
v5 = this.mGatewayAddress;
/* .line 335 */
(( java.net.Inet4Address ) v5 ).getHostAddress ( ); // invoke-virtual {v5}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;
/* .line 336 */
android.net.MacAddress .stringAddrFromByteAddr ( v4 );
v7 = this.mIfaceName;
/* .line 335 */
v5 = /* invoke-direct {p0, v5, v6, v7}, Lcom/android/server/wifi/ArpDetect;->isPermArpRoute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 337 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "reset current gateway neigh state for "; // const-string v6, "reset current gateway neigh state for "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mIfaceName;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v5 );
/* .line 338 */
v3 = this.mGatewayAddress;
int v5 = 2; // const/4 v5, 0x2
v0 = com.android.server.wifi.ArpDetect .updateNeighbor ( v3,v5,v2,v4 );
/* .line 341 */
} // :cond_2
} // .end method
public Boolean setCurGatewayPerm ( ) {
/* .locals 7 */
/* .line 260 */
int v0 = 0; // const/4 v0, 0x0
/* .line 261 */
/* .local v0, "status":Z */
v1 = /* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->isMultiGatewayExist()Z */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_3
/* iget v1, p0, Lcom/android/server/wifi/ArpDetect;->mNetworkId:I */
v1 = (( com.android.server.wifi.ArpDetect ) p0 ).isRecoveryOngoing ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wifi/ArpDetect;->isRecoveryOngoing(I)Z
/* if-nez v1, :cond_0 */
/* .line 265 */
} // :cond_0
v1 = this.mIfaceName;
/* invoke-direct {p0, v1}, Lcom/android/server/wifi/ArpDetect;->getNetworkInterfaceByName(Ljava/lang/String;)Ljava/net/NetworkInterface; */
/* .line 266 */
/* .local v1, "iface":Ljava/net/NetworkInterface; */
final String v3 = "ArpDetect"; // const-string v3, "ArpDetect"
/* if-nez v1, :cond_1 */
/* .line 267 */
final String v4 = "get iface fail"; // const-string v4, "get iface fail"
android.util.Log .d ( v3,v4 );
/* .line 268 */
/* .line 270 */
} // :cond_1
v2 = (( java.net.NetworkInterface ) v1 ).getIndex ( ); // invoke-virtual {v1}, Ljava/net/NetworkInterface;->getIndex()I
/* .line 271 */
/* .local v2, "ifaceIndex":I */
/* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->getCurGatewayMacFromRoute()[B */
/* .line 272 */
/* .local v4, "curGatewayMac":[B */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "set current gateway " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 273 */
android.net.MacAddress .stringAddrFromByteAddr ( v4 );
/* invoke-direct {p0, v6}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " permanent for "; // const-string v6, " permanent for "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mIfaceName;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 272 */
android.util.Log .d ( v3,v5 );
/* .line 275 */
if ( v4 != null) { // if-eqz v4, :cond_2
/* array-length v3, v4 */
/* if-lez v3, :cond_2 */
v3 = this.mGatewayAddress;
/* .line 276 */
(( java.net.Inet4Address ) v3 ).getHostAddress ( ); // invoke-virtual {v3}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;
/* .line 277 */
android.net.MacAddress .stringAddrFromByteAddr ( v4 );
v6 = this.mIfaceName;
/* .line 276 */
v3 = /* invoke-direct {p0, v3, v5, v6}, Lcom/android/server/wifi/ArpDetect;->isPermArpRoute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* if-nez v3, :cond_2 */
/* .line 278 */
v3 = this.mGatewayAddress;
/* const/16 v5, 0x80 */
v0 = com.android.server.wifi.ArpDetect .updateNeighbor ( v3,v5,v2,v4 );
/* .line 281 */
} // :cond_2
/* invoke-direct {p0, v3}, Lcom/android/server/wifi/ArpDetect;->setMultiGwRecoveryState(I)V */
/* .line 282 */
/* .line 262 */
} // .end local v1 # "iface":Ljava/net/NetworkInterface;
} // .end local v2 # "ifaceIndex":I
} // .end local v4 # "curGatewayMac":[B
} // :cond_3
} // :goto_0
} // .end method
public void startGatewayDetect ( Integer p0, java.lang.String p1, java.net.Inet4Address p2, java.net.Inet4Address p3, Object[] p4 ) {
/* .locals 4 */
/* .param p1, "networkId" # I */
/* .param p2, "ifaceName" # Ljava/lang/String; */
/* .param p3, "localIpAddress" # Ljava/net/Inet4Address; */
/* .param p4, "gatewayeIpAddress" # Ljava/net/Inet4Address; */
/* .param p5, "localMacAddress" # [B */
/* .line 188 */
final String v0 = "ArpDetect"; // const-string v0, "ArpDetect"
if ( p2 != null) { // if-eqz p2, :cond_1
if ( p3 != null) { // if-eqz p3, :cond_1
if ( p4 != null) { // if-eqz p4, :cond_1
if ( p5 != null) { // if-eqz p5, :cond_1
/* array-length v1, p5 */
/* if-nez v1, :cond_0 */
/* .line 193 */
} // :cond_0
(( com.android.server.wifi.ArpDetect ) p0 ).stopGatewayDetect ( ); // invoke-virtual {p0}, Lcom/android/server/wifi/ArpDetect;->stopGatewayDetect()V
/* .line 195 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Start ArpDetect on "; // const-string v2, "Start ArpDetect on "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " for netId: "; // const-string v2, " for netId: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 196 */
/* iput p1, p0, Lcom/android/server/wifi/ArpDetect;->mNetworkId:I */
/* .line 197 */
this.mIfaceName = p2;
/* .line 198 */
this.mLocalIpAddress = p3;
/* .line 199 */
this.mGatewayAddress = p4;
/* .line 200 */
this.mLocalMacAddress = p5;
/* .line 202 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mGatewayMacList = v0;
/* .line 203 */
/* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->registerArpRecvQueue()Z */
/* .line 205 */
/* new-instance v0, Lcom/android/server/wifi/ArpDetect$MessageHandler; */
/* invoke-direct {v0, p0}, Lcom/android/server/wifi/ArpDetect$MessageHandler;-><init>(Lcom/android/server/wifi/ArpDetect;)V */
this.msgHandler = v0;
/* .line 206 */
/* const/16 v1, 0x64 */
/* const-wide/16 v2, 0x1f40 */
(( com.android.server.wifi.ArpDetect$MessageHandler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/wifi/ArpDetect$MessageHandler;->sendEmptyMessageDelayed(IJ)Z
/* .line 208 */
/* invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->setMultiGwRecoveryState(I)V */
/* .line 209 */
v0 = this.mLocalMacAddress;
v1 = this.mLocalIpAddress;
v2 = this.mGatewayAddress;
int v3 = 6; // const/4 v3, 0x6
/* invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/server/wifi/ArpDetect;->startMultiGatewayDetect([BLjava/net/Inet4Address;Ljava/net/Inet4Address;I)V */
/* .line 211 */
return;
/* .line 190 */
} // :cond_1
} // :goto_0
final String v1 = "Invalid param"; // const-string v1, "Invalid param"
android.util.Log .e ( v0,v1 );
/* .line 191 */
return;
} // .end method
public void stopGatewayDetect ( ) {
/* .locals 2 */
/* .line 214 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Stop ArpDetect on "; // const-string v1, "Stop ArpDetect on "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mIfaceName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ArpDetect"; // const-string v1, "ArpDetect"
android.util.Log .d ( v1,v0 );
/* .line 216 */
/* iget-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->stopDetect:Z */
/* if-nez v0, :cond_0 */
/* .line 217 */
/* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->unregisterArpRecvQueue()V */
/* .line 220 */
} // :cond_0
/* invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->setMultiGwRecoveryState(I)V */
/* .line 221 */
v0 = this.mArpSendSock;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 222 */
/* invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->closeSocket(Ljava/io/FileDescriptor;)V */
/* .line 223 */
this.mArpSendSock = v1;
/* .line 225 */
} // :cond_1
v0 = this.mArpRecvSock;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 226 */
/* invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->closeSocket(Ljava/io/FileDescriptor;)V */
/* .line 227 */
this.mArpRecvSock = v1;
/* .line 229 */
} // :cond_2
v0 = this.mGatewayMacList;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 230 */
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 231 */
this.mGatewayMacList = v1;
/* .line 233 */
} // :cond_3
this.msgHandler = v1;
/* .line 234 */
this.mInterfaceBroadcastAddr = v1;
/* .line 235 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->isMultiGwFind:Z */
/* .line 236 */
/* iput v0, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayChangeCount:I */
/* .line 237 */
return;
} // .end method
public Boolean tryOtherGateway ( ) {
/* .locals 8 */
/* .line 286 */
int v0 = 0; // const/4 v0, 0x0
/* .line 287 */
/* .local v0, "status":Z */
v1 = /* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->isMultiGatewayExist()Z */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_5
/* iget v1, p0, Lcom/android/server/wifi/ArpDetect;->mNetworkId:I */
v1 = (( com.android.server.wifi.ArpDetect ) p0 ).isRecoveryOngoing ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wifi/ArpDetect;->isRecoveryOngoing(I)Z
/* if-nez v1, :cond_0 */
/* goto/16 :goto_0 */
/* .line 290 */
} // :cond_0
v1 = this.mGatewayMacList;
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget v3, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayChangeCount:I */
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-lt v3, v1, :cond_1 */
/* .line 291 */
(( com.android.server.wifi.ArpDetect ) p0 ).resetGatewayNeighState ( ); // invoke-virtual {p0}, Lcom/android/server/wifi/ArpDetect;->resetGatewayNeighState()Z
/* .line 292 */
/* invoke-direct {p0, v1}, Lcom/android/server/wifi/ArpDetect;->setMultiGwRecoveryState(I)V */
/* .line 293 */
/* .line 296 */
} // :cond_1
v1 = this.mGatewayMacList;
final String v3 = "ArpDetect"; // const-string v3, "ArpDetect"
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
int v4 = 2; // const/4 v4, 0x2
/* if-ge v1, v4, :cond_2 */
/* .line 297 */
final String v1 = "Only one gateway device"; // const-string v1, "Only one gateway device"
android.util.Log .e ( v3,v1 );
/* .line 298 */
/* .line 301 */
} // :cond_2
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "try other gateway for " */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mIfaceName;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v1 );
/* .line 302 */
v1 = this.mIfaceName;
/* invoke-direct {p0, v1}, Lcom/android/server/wifi/ArpDetect;->getNetworkInterfaceByName(Ljava/lang/String;)Ljava/net/NetworkInterface; */
/* .line 303 */
/* .local v1, "iface":Ljava/net/NetworkInterface; */
/* if-nez v1, :cond_3 */
/* .line 304 */
final String v4 = "get iface fail"; // const-string v4, "get iface fail"
android.util.Log .d ( v3,v4 );
/* .line 305 */
/* .line 307 */
} // :cond_3
v2 = (( java.net.NetworkInterface ) v1 ).getIndex ( ); // invoke-virtual {v1}, Ljava/net/NetworkInterface;->getIndex()I
/* .line 308 */
/* .local v2, "ifaceIndex":I */
v4 = /* invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->getNextGatewayIndex()I */
/* .line 309 */
/* .local v4, "nextGatewayIndex":I */
/* if-ltz v4, :cond_4 */
v5 = this.mGatewayMacList;
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 310 */
(( java.util.ArrayList ) v5 ).get ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, [B */
/* .line 311 */
/* .local v5, "nextGatewayMac":[B */
v6 = this.mGatewayAddress;
/* const/16 v7, 0x80 */
v0 = com.android.server.wifi.ArpDetect .updateNeighbor ( v6,v7,v2,v5 );
/* .line 313 */
/* iget v6, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayChangeCount:I */
/* add-int/lit8 v6, v6, 0x1 */
/* iput v6, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayChangeCount:I */
/* .line 314 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "New gateway index "; // const-string v7, "New gateway index "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " ,mac "; // const-string v7, " ,mac "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 315 */
com.android.internal.util.HexDump .toHexString ( v5 );
/* invoke-direct {p0, v7}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 314 */
android.util.Log .d ( v3,v6 );
/* .line 318 */
} // .end local v5 # "nextGatewayMac":[B
} // :cond_4
/* .line 288 */
} // .end local v1 # "iface":Ljava/net/NetworkInterface;
} // .end local v2 # "ifaceIndex":I
} // .end local v4 # "nextGatewayIndex":I
} // :cond_5
} // :goto_0
} // .end method
