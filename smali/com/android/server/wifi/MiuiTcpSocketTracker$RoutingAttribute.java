class com.android.server.wifi.MiuiTcpSocketTracker$RoutingAttribute {
	 /* .source "MiuiTcpSocketTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wifi/MiuiTcpSocketTracker; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "RoutingAttribute" */
} // .end annotation
/* # static fields */
public static final Integer HEADER_LENGTH;
public static final Integer INET_DIAG_INFO;
public static final Integer INET_DIAG_MARK;
/* # instance fields */
public final Object rtaLen;
public final Object rtaType;
final com.android.server.wifi.MiuiTcpSocketTracker this$0; //synthetic
/* # direct methods */
 com.android.server.wifi.MiuiTcpSocketTracker$RoutingAttribute ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wifi/MiuiTcpSocketTracker; */
/* .param p2, "len" # S */
/* .param p3, "type" # S */
/* .line 623 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 624 */
/* iput-short p2, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;->rtaLen:S */
/* .line 625 */
/* iput-short p3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;->rtaType:S */
/* .line 626 */
return;
} // .end method
/* # virtual methods */
public Object getDataLength ( ) {
/* .locals 1 */
/* .line 628 */
/* iget-short v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;->rtaLen:S */
/* add-int/lit8 v0, v0, -0x4 */
/* int-to-short v0, v0 */
} // .end method
