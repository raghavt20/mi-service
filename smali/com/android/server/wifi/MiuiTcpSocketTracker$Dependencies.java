public class com.android.server.wifi.MiuiTcpSocketTracker$Dependencies {
	 /* .source "MiuiTcpSocketTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wifi/MiuiTcpSocketTracker; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "Dependencies" */
} // .end annotation
/* # static fields */
private static final Integer DEFAULT_RECV_BUFSIZE;
private static final Long IO_TIMEOUT;
/* # instance fields */
private final android.content.Context mContext;
/* # direct methods */
public com.android.server.wifi.MiuiTcpSocketTracker$Dependencies ( ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 636 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 637 */
this.mContext = p1;
/* .line 638 */
return;
} // .end method
/* # virtual methods */
public void addDeviceConfigChangedListener ( android.provider.DeviceConfig$OnPropertiesChangedListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Landroid/provider/DeviceConfig$OnPropertiesChangedListener; */
/* .line 718 */
final String v0 = "connectivity"; // const-string v0, "connectivity"
v1 = android.os.AsyncTask.THREAD_POOL_EXECUTOR;
android.provider.DeviceConfig .addOnPropertiesChangedListener ( v0,v1,p1 );
/* .line 720 */
return;
} // .end method
public java.io.FileDescriptor connectToKernel ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException;, */
/* Ljava/net/SocketException; */
/* } */
} // .end annotation
/* .line 647 */
/* or-int/2addr v1, v2 */
/* .line 648 */
android.system.Os .socket ( v0,v1,v2 );
/* .line 649 */
/* .local v0, "fd":Ljava/io/FileDescriptor; */
/* nop */
/* .line 650 */
int v1 = 0; // const/4 v1, 0x0
android.net.util.SocketUtils .makeNetlinkSocketAddress ( v1,v1 );
/* .line 649 */
android.system.Os .connect ( v0,v1 );
/* .line 652 */
} // .end method
public android.content.Context getContext ( ) {
/* .locals 1 */
/* .line 729 */
v0 = this.mContext;
} // .end method
public Integer getDeviceConfigPropertyInt ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "namespace" # Ljava/lang/String; */
/* .param p2, "name" # Ljava/lang/String; */
/* .param p3, "defaultValue" # I */
/* .line 679 */
android.provider.DeviceConfig .getProperty ( p1,p2 );
/* .line 680 */
/* .local v0, "value1":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object v1, v0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 682 */
/* .local v1, "value":Ljava/lang/String; */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
try { // :try_start_0
v2 = java.lang.Integer .parseInt ( v1 );
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 683 */
/* :catch_0 */
/* move-exception v2 */
/* .line 684 */
/* .local v2, "e":Ljava/lang/NumberFormatException; */
/* .line 682 */
} // .end local v2 # "e":Ljava/lang/NumberFormatException;
} // :cond_1
/* move v2, p3 */
} // :goto_1
} // .end method
public android.net.INetd getNetd ( ) {
/* .locals 2 */
/* .line 711 */
v0 = this.mContext;
/* .line 712 */
final String v1 = "netd"; // const-string v1, "netd"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/IBinder; */
/* .line 711 */
android.net.INetd$Stub .asInterface ( v0 );
} // .end method
public Boolean isTcpInfoParsingSupported ( ) {
/* .locals 3 */
/* .line 694 */
/* .line 695 */
final String v1 = "REL"; // const-string v1, "REL"
v2 = android.os.Build$VERSION.CODENAME;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 1; // const/4 v2, 0x1
/* xor-int/2addr v1, v2 */
/* add-int/2addr v0, v1 */
/* .line 696 */
/* .local v0, "devApiLevel":I */
/* const/16 v1, 0x1d */
/* if-le v0, v1, :cond_0 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
} // .end method
public java.nio.ByteBuffer recvMessage ( java.io.FileDescriptor p0 ) {
/* .locals 3 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/io/InterruptedIOException; */
/* } */
} // .end annotation
/* .line 704 */
/* const v0, 0xea60 */
/* const-wide/16 v1, 0x3e8 */
com.android.net.module.util.netlink.NetlinkUtils .recvMessage ( p1,v0,v1,v2 );
} // .end method
public void removeDeviceConfigChangedListener ( android.provider.DeviceConfig$OnPropertiesChangedListener p0 ) {
/* .locals 0 */
/* .param p1, "listener" # Landroid/provider/DeviceConfig$OnPropertiesChangedListener; */
/* .line 725 */
android.provider.DeviceConfig .removeOnPropertiesChangedListener ( p1 );
/* .line 726 */
return;
} // .end method
public void sendPollingRequest ( java.io.FileDescriptor p0, Object[] p1 ) {
/* .locals 4 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "msg" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException;, */
/* Ljava/io/InterruptedIOException; */
/* } */
} // .end annotation
/* .line 664 */
/* .line 665 */
/* const-wide/16 v2, 0x3e8 */
android.system.StructTimeval .fromMillis ( v2,v3 );
/* .line 664 */
android.system.Os .setsockoptTimeval ( p1,v0,v1,v2 );
/* .line 666 */
int v0 = 0; // const/4 v0, 0x0
/* array-length v1, p2 */
android.system.Os .write ( p1,p2,v0,v1 );
/* .line 667 */
return;
} // .end method
