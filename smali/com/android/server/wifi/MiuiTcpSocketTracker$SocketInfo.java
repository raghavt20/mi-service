class com.android.server.wifi.MiuiTcpSocketTracker$SocketInfo {
	 /* .source "MiuiTcpSocketTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wifi/MiuiTcpSocketTracker; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "SocketInfo" */
} // .end annotation
/* # static fields */
public static final Integer INIT_MARK_VALUE;
/* # instance fields */
public final Integer fwmark;
public final Integer ipFamily;
public final com.android.server.wifi.MiuiTcpInfo tcpInfo;
final com.android.server.wifi.MiuiTcpSocketTracker this$0; //synthetic
public final Long updateTime;
/* # direct methods */
 com.android.server.wifi.MiuiTcpSocketTracker$SocketInfo ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wifi/MiuiTcpSocketTracker; */
/* .param p2, "info" # Lcom/android/server/wifi/MiuiTcpInfo; */
/* .param p3, "family" # I */
/* .param p4, "mark" # I */
/* .param p5, "time" # J */
/* .line 581 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 582 */
this.tcpInfo = p2;
/* .line 583 */
/* iput p3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->ipFamily:I */
/* .line 584 */
/* iput-wide p5, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->updateTime:J */
/* .line 585 */
/* iput p4, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->fwmark:I */
/* .line 586 */
return;
} // .end method
private java.lang.String ipTypeToString ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .line 595 */
/* if-ne p1, v0, :cond_0 */
/* .line 596 */
final String v0 = "IP"; // const-string v0, "IP"
/* .line 597 */
} // :cond_0
/* if-ne p1, v0, :cond_1 */
/* .line 598 */
final String v0 = "IPV6"; // const-string v0, "IPV6"
/* .line 600 */
} // :cond_1
final String v0 = "UNKNOWN"; // const-string v0, "UNKNOWN"
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 590 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "SocketInfo {Type:"; // const-string v1, "SocketInfo {Type:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->ipFamily:I */
/* invoke-direct {p0, v1}, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->ipTypeToString(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", "; // const-string v1, ", "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.tcpInfo;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", mark:"; // const-string v1, ", mark:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->fwmark:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " updated at "; // const-string v1, " updated at "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->updateTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
