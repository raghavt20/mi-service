public class com.android.server.wifi.WifiDrvUEventObserver extends android.os.UEventObserver {
	 /* .source "WifiDrvUEventObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;, */
	 /* Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ACTION_CHIP_RESET;
private static final Integer ACTION_L1_SER;
private static final Integer ACTION_L3_SER;
private static final Integer ACTION_ROAMING_NOTIFY;
private static final Integer ACTION_WIFI_RESET;
public static final Integer COEX_FDD_MODE;
public static final Integer COEX_HBD_MODE;
private static final java.lang.String COEX_MODE_EVENT;
public static final Integer COEX_NO_BT;
public static final Integer COEX_TDD_MODE;
private static final Boolean DEBUG;
private static final java.lang.String EXTRA_WBH_MODE_TYPE;
private static final java.lang.String L1_SER;
private static final java.lang.String L3_SER;
private static final Integer MAX_REC_SIZE;
public static final Integer MTK_DATASTALL_COEX_LOW;
public static final Integer MTK_DATASTALL_FCS_ERROR;
public static final Integer MTK_DATASTALL_NONE;
public static final Integer MTK_DATASTALL_NUD_FAILURE;
public static final Integer MTK_DATASTALL_PM_CHANGE_FAIL;
public static final Integer MTK_DATASTALL_RX_REORDER;
public static final Integer MTK_DATASTALL_TX_DROP;
public static final Integer MTK_DATASTALL_TX_HANG;
private static final java.lang.String MTK_RECOVERY_EVENT;
private static final java.lang.String MTK_ROAM_NOTIFY_EVENT;
private static final java.lang.String MTK_ROAM_SUCCESS;
private static final java.lang.String MTK_TRX_ABNORMAL_EVENT;
private static final java.lang.String TAG;
private static final java.lang.String UEVENT_PATH;
public static final java.lang.String WBH_MODE_CHANGED;
private static final java.lang.String WIFI_RESET;
private static java.util.LinkedList mUeventRecord;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedList<", */
/* "Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private final java.util.Map MTK_DATASTALL_TYPE;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer lastCoexMode;
private com.android.server.wifi.WifiDrvUEventObserver$WifiDrvUeventCallback mCallback;
private android.content.Context mContext;
/* # direct methods */
static com.android.server.wifi.WifiDrvUEventObserver ( ) {
/* .locals 1 */
/* .line 71 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
return;
} // .end method
public com.android.server.wifi.WifiDrvUEventObserver ( ) {
/* .locals 16 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "callback" # Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback; */
/* .line 77 */
/* move-object/from16 v0, p0 */
/* invoke-direct/range {p0 ..p0}, Landroid/os/UEventObserver;-><init>()V */
/* .line 62 */
/* nop */
/* .line 63 */
/* const/16 v1, 0x200 */
java.lang.Integer .valueOf ( v1 );
final String v3 = "DIR:TX,Event:Hang"; // const-string v3, "DIR:TX,Event:Hang"
/* .line 64 */
/* const/16 v1, 0x201 */
java.lang.Integer .valueOf ( v1 );
final String v5 = "DIR:TX,event:AbDrop"; // const-string v5, "DIR:TX,event:AbDrop"
/* .line 65 */
/* const/16 v1, 0x203 */
java.lang.Integer .valueOf ( v1 );
final String v7 = "DIR:RX,Event:AbCRC"; // const-string v7, "DIR:RX,Event:AbCRC"
/* .line 66 */
/* const/16 v1, 0x202 */
java.lang.Integer .valueOf ( v1 );
final String v9 = "DIR:RX,event:AbReorder"; // const-string v9, "DIR:RX,event:AbReorder"
/* .line 67 */
/* const/16 v1, 0x204 */
java.lang.Integer .valueOf ( v1 );
final String v11 = "DIR:TXRX,event:BTWifiCoexLow"; // const-string v11, "DIR:TXRX,event:BTWifiCoexLow"
/* .line 68 */
/* const/16 v1, 0x205 */
java.lang.Integer .valueOf ( v1 );
final String v13 = "DIR:TX,Event:pmStateChangeFail"; // const-string v13, "DIR:TX,Event:pmStateChangeFail"
/* .line 69 */
/* const/16 v1, 0x206 */
java.lang.Integer .valueOf ( v1 );
final String v15 = "DIR:RX,event:AbArpNoResponse"; // const-string v15, "DIR:RX,event:AbArpNoResponse"
/* .line 62 */
/* invoke-static/range {v2 ..v15}, Ljava/util/Map;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map; */
this.MTK_DATASTALL_TYPE = v1;
/* .line 73 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Lcom/android/server/wifi/WifiDrvUEventObserver;->lastCoexMode:I */
/* .line 78 */
/* move-object/from16 v1, p1 */
this.mContext = v1;
/* .line 79 */
/* move-object/from16 v2, p2 */
this.mCallback = v2;
/* .line 80 */
return;
} // .end method
private void addUeventRec ( com.android.server.wifi.WifiDrvUEventObserver$UeventRecord p0 ) {
/* .locals 2 */
/* .param p1, "rec" # Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord; */
/* .line 225 */
if ( p1 != null) { // if-eqz p1, :cond_2
v0 = com.android.server.wifi.WifiDrvUEventObserver.mUeventRecord;
/* if-nez v0, :cond_0 */
/* .line 229 */
} // :cond_0
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
/* const/16 v1, 0x1e */
/* if-lt v0, v1, :cond_1 */
/* .line 230 */
v0 = com.android.server.wifi.WifiDrvUEventObserver.mUeventRecord;
(( java.util.LinkedList ) v0 ).removeFirst ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
/* .line 231 */
} // :cond_1
v0 = com.android.server.wifi.WifiDrvUEventObserver.mUeventRecord;
(( java.util.LinkedList ) v0 ).addLast ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
/* .line 232 */
return;
/* .line 226 */
} // :cond_2
} // :goto_0
return;
} // .end method
public static void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 3 */
/* .param p0, "fd" # Ljava/io/FileDescriptor; */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .line 241 */
final String v0 = "WifiDrvUEvent:"; // const-string v0, "WifiDrvUEvent:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 242 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " total records="; // const-string v1, " total records="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.android.server.wifi.WifiDrvUEventObserver .getUeventRecCount ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 243 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = com.android.server.wifi.WifiDrvUEventObserver .getUeventRecCount ( );
/* if-ge v0, v1, :cond_0 */
/* .line 244 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " rec["; // const-string v2, " rec["
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "]: "; // const-string v2, "]: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.wifi.WifiDrvUEventObserver .getUeventRec ( v0 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 245 */
(( java.io.PrintWriter ) p1 ).flush ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->flush()V
/* .line 243 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 247 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
private Integer getDatastallType ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "datastallEvent" # Ljava/lang/String; */
/* .line 169 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "event: "; // const-string v1, "event: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "WifiDrvUEventObserver"; // const-string v1, "WifiDrvUEventObserver"
android.util.Log .d ( v1,v0 );
/* .line 170 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 171 */
/* .line 173 */
} // :cond_0
v0 = this.MTK_DATASTALL_TYPE;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 174 */
/* .local v3, "key":I */
v4 = this.MTK_DATASTALL_TYPE;
java.lang.Integer .valueOf ( v3 );
/* check-cast v4, Ljava/lang/CharSequence; */
v4 = android.text.TextUtils .isEmpty ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 175 */
/* .line 177 */
} // :cond_1
v4 = this.MTK_DATASTALL_TYPE;
java.lang.Integer .valueOf ( v3 );
/* check-cast v4, Ljava/lang/CharSequence; */
v4 = (( java.lang.String ) p1 ).contains ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 178 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "datastall type: "; // const-string v2, "datastall type: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 179 */
/* .line 181 */
} // .end local v3 # "key":I
} // :cond_2
/* .line 182 */
} // :cond_3
} // .end method
private static com.android.server.wifi.WifiDrvUEventObserver$UeventRecord getUeventRec ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "index" # I */
/* .line 235 */
v0 = com.android.server.wifi.WifiDrvUEventObserver.mUeventRecord;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = com.android.server.wifi.WifiDrvUEventObserver .getUeventRecCount ( );
/* if-lt p0, v0, :cond_0 */
/* .line 237 */
} // :cond_0
v0 = com.android.server.wifi.WifiDrvUEventObserver.mUeventRecord;
(( java.util.LinkedList ) v0 ).get ( p0 ); // invoke-virtual {v0, p0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord; */
/* .line 236 */
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static Integer getUeventRecCount ( ) {
/* .locals 1 */
/* .line 219 */
v0 = com.android.server.wifi.WifiDrvUEventObserver.mUeventRecord;
/* if-nez v0, :cond_0 */
/* .line 220 */
int v0 = 0; // const/4 v0, 0x0
/* .line 221 */
} // :cond_0
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
} // .end method
/* # virtual methods */
public void onUEvent ( android.os.UEventObserver$UEvent p0 ) {
/* .locals 18 */
/* .param p1, "event" # Landroid/os/UEventObserver$UEvent; */
/* .line 85 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
final String v0 = "abnormaltrx"; // const-string v0, "abnormaltrx"
final String v3 = "coex_mode"; // const-string v3, "coex_mode"
final String v4 = ""; // const-string v4, ""
try { // :try_start_0
(( android.os.UEventObserver$UEvent ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;
/* .line 86 */
/* .local v5, "coexMode":Ljava/lang/String; */
(( android.os.UEventObserver$UEvent ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;
/* .line 87 */
/* .local v6, "mtkTrxAbnormal":Ljava/lang/String; */
final String v7 = "recoveryNotify"; // const-string v7, "recoveryNotify"
(( android.os.UEventObserver$UEvent ) v2 ).get ( v7 ); // invoke-virtual {v2, v7}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;
/* .line 88 */
/* .local v7, "mtkRecoveryEvent":Ljava/lang/String; */
final String v8 = "roam"; // const-string v8, "roam"
(( android.os.UEventObserver$UEvent ) v2 ).get ( v8 ); // invoke-virtual {v2, v8}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;
/* .line 89 */
/* .local v8, "mtkRoamNotify":Ljava/lang/String; */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 90 */
v9 = java.lang.Integer .parseInt ( v5 );
/* .line 91 */
/* .local v9, "curCoexMode":I */
/* iget v10, v1, Lcom/android/server/wifi/WifiDrvUEventObserver;->lastCoexMode:I */
/* if-eq v9, v10, :cond_1 */
int v11 = 2; // const/4 v11, 0x2
/* if-ge v9, v11, :cond_0 */
/* if-lt v10, v11, :cond_1 */
/* .line 93 */
} // :cond_0
/* new-instance v10, Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord; */
/* invoke-direct {v10, v3, v5}, Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v3, v10 */
/* .line 94 */
/* .local v3, "uRec":Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord; */
/* invoke-direct {v1, v3}, Lcom/android/server/wifi/WifiDrvUEventObserver;->addUeventRec(Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;)V */
/* .line 95 */
v10 = this.mCallback;
/* .line 96 */
/* new-instance v10, Landroid/content/Intent; */
final String v11 = "android.net.wifi.COEX_MODE_STATE_CHANGED"; // const-string v11, "android.net.wifi.COEX_MODE_STATE_CHANGED"
/* invoke-direct {v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 97 */
/* .local v10, "intent":Landroid/content/Intent; */
final String v11 = "extra_wbh_mode_type"; // const-string v11, "extra_wbh_mode_type"
(( android.content.Intent ) v10 ).putExtra ( v11, v9 ); // invoke-virtual {v10, v11, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 98 */
v11 = this.mContext;
v12 = android.os.UserHandle.SYSTEM;
(( android.content.Context ) v11 ).sendBroadcastAsUser ( v10, v12 ); // invoke-virtual {v11, v10, v12}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 100 */
} // .end local v3 # "uRec":Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;
} // .end local v10 # "intent":Landroid/content/Intent;
} // :cond_1
/* iput v9, v1, Lcom/android/server/wifi/WifiDrvUEventObserver;->lastCoexMode:I */
/* .line 103 */
} // .end local v9 # "curCoexMode":I
} // :cond_2
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 104 */
v3 = /* invoke-direct {v1, v6}, Lcom/android/server/wifi/WifiDrvUEventObserver;->getDatastallType(Ljava/lang/String;)I */
/* .line 105 */
/* .local v3, "type":I */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 106 */
v9 = this.mCallback;
/* .line 108 */
} // :cond_3
/* new-instance v9, Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord; */
/* invoke-direct {v9, v0, v6}, Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v0, v9 */
/* .line 109 */
/* .local v0, "uRec":Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord; */
/* invoke-direct {v1, v0}, Lcom/android/server/wifi/WifiDrvUEventObserver;->addUeventRec(Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;)V */
/* .line 112 */
} // .end local v0 # "uRec":Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;
} // .end local v3 # "type":I
} // :cond_4
if ( v7 != null) { // if-eqz v7, :cond_e
/* .line 113 */
int v0 = -1; // const/4 v0, -0x1
/* .line 114 */
/* .local v0, "action":I */
int v3 = 0; // const/4 v3, 0x0
java.lang.Boolean .valueOf ( v3 );
/* .line 115 */
/* .local v9, "recoveryState":Ljava/lang/Boolean; */
int v10 = -1; // const/4 v10, -0x1
/* .line 116 */
/* .local v10, "reason":I */
final String v11 = ","; // const-string v11, ","
(( java.lang.String ) v7 ).split ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 117 */
/* .local v11, "recoveryInfo":[Ljava/lang/String; */
/* array-length v12, v11 */
/* move v13, v3 */
} // :goto_0
/* if-ge v13, v12, :cond_d */
/* aget-object v15, v11, v13 */
/* .line 118 */
/* .local v15, "str":Ljava/lang/String; */
if ( v15 != null) { // if-eqz v15, :cond_b
v16 = (( java.lang.String ) v15 ).equals ( v4 ); // invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v16, :cond_b */
/* .line 119 */
/* const/16 v14, 0x3a */
v3 = (( java.lang.String ) v15 ).indexOf ( v14 ); // invoke-virtual {v15, v14}, Ljava/lang/String;->indexOf(I)I
int v14 = 0; // const/4 v14, 0x0
(( java.lang.String ) v15 ).substring ( v14, v3 ); // invoke-virtual {v15, v14, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 120 */
/* .local v3, "key":Ljava/lang/String; */
/* const/16 v14, 0x3a */
v14 = (( java.lang.String ) v15 ).indexOf ( v14 ); // invoke-virtual {v15, v14}, Ljava/lang/String;->indexOf(I)I
/* const/16 v16, 0x1 */
/* add-int/lit8 v14, v14, 0x1 */
(( java.lang.String ) v15 ).substring ( v14 ); // invoke-virtual {v15, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 121 */
/* .local v14, "value":Ljava/lang/String; */
/* move-object/from16 v17, v5 */
} // .end local v5 # "coexMode":Ljava/lang/String;
/* .local v17, "coexMode":Ljava/lang/String; */
final String v5 = "reset"; // const-string v5, "reset"
v5 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 123 */
int v0 = 1; // const/4 v0, 0x1
/* .line 124 */
if ( v14 != null) { // if-eqz v14, :cond_c
v5 = (( java.lang.String ) v14 ).equals ( v4 ); // invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_c */
/* .line 125 */
v5 = java.lang.Integer .parseInt ( v14 );
/* if-nez v5, :cond_5 */
/* const/16 v16, 0x1 */
} // :cond_5
/* const/16 v16, 0x0 */
} // :goto_1
/* invoke-static/range {v16 ..v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean; */
/* move-object v9, v5 */
} // .end local v9 # "recoveryState":Ljava/lang/Boolean;
/* .local v5, "recoveryState":Ljava/lang/Boolean; */
/* .line 127 */
} // .end local v5 # "recoveryState":Ljava/lang/Boolean;
/* .restart local v9 # "recoveryState":Ljava/lang/Boolean; */
} // :cond_6
final String v5 = "l1ser"; // const-string v5, "l1ser"
v5 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 128 */
int v0 = 2; // const/4 v0, 0x2
/* .line 129 */
if ( v14 != null) { // if-eqz v14, :cond_c
v5 = (( java.lang.String ) v14 ).equals ( v4 ); // invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_c */
/* .line 130 */
v5 = java.lang.Integer .parseInt ( v14 );
/* if-nez v5, :cond_7 */
/* const/16 v16, 0x1 */
} // :cond_7
/* const/16 v16, 0x0 */
} // :goto_2
/* invoke-static/range {v16 ..v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean; */
/* move-object v9, v5 */
} // .end local v9 # "recoveryState":Ljava/lang/Boolean;
/* .restart local v5 # "recoveryState":Ljava/lang/Boolean; */
/* .line 132 */
} // .end local v5 # "recoveryState":Ljava/lang/Boolean;
/* .restart local v9 # "recoveryState":Ljava/lang/Boolean; */
} // :cond_8
final String v5 = "l3ser"; // const-string v5, "l3ser"
v5 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_a
/* .line 133 */
int v0 = 3; // const/4 v0, 0x3
/* .line 134 */
if ( v14 != null) { // if-eqz v14, :cond_c
v5 = (( java.lang.String ) v14 ).equals ( v4 ); // invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_c */
/* .line 135 */
v5 = java.lang.Integer .parseInt ( v14 );
/* if-nez v5, :cond_9 */
/* const/16 v16, 0x1 */
} // :cond_9
/* const/16 v16, 0x0 */
} // :goto_3
/* invoke-static/range {v16 ..v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean; */
/* move-object v9, v5 */
} // .end local v9 # "recoveryState":Ljava/lang/Boolean;
/* .restart local v5 # "recoveryState":Ljava/lang/Boolean; */
/* .line 137 */
} // .end local v5 # "recoveryState":Ljava/lang/Boolean;
/* .restart local v9 # "recoveryState":Ljava/lang/Boolean; */
} // :cond_a
final String v5 = "reason"; // const-string v5, "reason"
v5 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_c
/* .line 138 */
if ( v14 != null) { // if-eqz v14, :cond_c
v5 = (( java.lang.String ) v14 ).equals ( v4 ); // invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_c */
/* .line 139 */
v5 = java.lang.Integer .parseInt ( v14 );
/* move v10, v5 */
} // .end local v10 # "reason":I
/* .local v5, "reason":I */
/* .line 118 */
} // .end local v3 # "key":Ljava/lang/String;
} // .end local v14 # "value":Ljava/lang/String;
} // .end local v17 # "coexMode":Ljava/lang/String;
/* .local v5, "coexMode":Ljava/lang/String; */
/* .restart local v10 # "reason":I */
} // :cond_b
/* move-object/from16 v17, v5 */
/* .line 117 */
} // .end local v5 # "coexMode":Ljava/lang/String;
} // .end local v15 # "str":Ljava/lang/String;
/* .restart local v17 # "coexMode":Ljava/lang/String; */
} // :cond_c
} // :goto_4
/* add-int/lit8 v13, v13, 0x1 */
/* move-object/from16 v5, v17 */
int v3 = 0; // const/4 v3, 0x0
/* goto/16 :goto_0 */
/* .line 144 */
} // .end local v17 # "coexMode":Ljava/lang/String;
/* .restart local v5 # "coexMode":Ljava/lang/String; */
} // :cond_d
/* move-object/from16 v17, v5 */
} // .end local v5 # "coexMode":Ljava/lang/String;
/* .restart local v17 # "coexMode":Ljava/lang/String; */
int v3 = 1; // const/4 v3, 0x1
/* if-ne v10, v3, :cond_f */
/* .line 145 */
v3 = this.mCallback;
v4 = (( java.lang.Boolean ) v9 ).booleanValue ( ); // invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 112 */
} // .end local v0 # "action":I
} // .end local v9 # "recoveryState":Ljava/lang/Boolean;
} // .end local v10 # "reason":I
} // .end local v11 # "recoveryInfo":[Ljava/lang/String;
} // .end local v17 # "coexMode":Ljava/lang/String;
/* .restart local v5 # "coexMode":Ljava/lang/String; */
} // :cond_e
/* move-object/from16 v17, v5 */
/* .line 149 */
} // .end local v5 # "coexMode":Ljava/lang/String;
/* .restart local v17 # "coexMode":Ljava/lang/String; */
} // :cond_f
} // :goto_5
if ( v8 != null) { // if-eqz v8, :cond_10
/* .line 150 */
final String v0 = "Status:SUCCESS"; // const-string v0, "Status:SUCCESS"
v0 = (( java.lang.String ) v8 ).contains ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* .line 151 */
/* .local v0, "isRoamSuccess":Z */
v3 = this.mCallback;
/* const/16 v4, 0xa */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 155 */
} // .end local v0 # "isRoamSuccess":Z
} // .end local v6 # "mtkTrxAbnormal":Ljava/lang/String;
} // .end local v7 # "mtkRecoveryEvent":Ljava/lang/String;
} // .end local v8 # "mtkRoamNotify":Ljava/lang/String;
} // .end local v17 # "coexMode":Ljava/lang/String;
} // :cond_10
/* .line 153 */
/* :catch_0 */
/* move-exception v0 */
/* .line 154 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Could not parse event "; // const-string v4, "Could not parse event "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "WifiDrvUEventObserver"; // const-string v4, "WifiDrvUEventObserver"
android.util.Log .e ( v4,v3 );
/* .line 156 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_6
return;
} // .end method
public void start ( ) {
/* .locals 2 */
/* .line 159 */
final String v0 = "WifiDrvUEventObserver"; // const-string v0, "WifiDrvUEventObserver"
/* const-string/jumbo v1, "startObserving" */
android.util.Log .d ( v0,v1 );
/* .line 160 */
final String v0 = "DEVPATH=/devices/virtual/misc/wlan"; // const-string v0, "DEVPATH=/devices/virtual/misc/wlan"
(( com.android.server.wifi.WifiDrvUEventObserver ) p0 ).startObserving ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wifi/WifiDrvUEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 161 */
return;
} // .end method
public void stop ( ) {
/* .locals 2 */
/* .line 164 */
final String v0 = "WifiDrvUEventObserver"; // const-string v0, "WifiDrvUEventObserver"
/* const-string/jumbo v1, "stopObserving" */
android.util.Log .d ( v0,v1 );
/* .line 165 */
(( com.android.server.wifi.WifiDrvUEventObserver ) p0 ).stopObserving ( ); // invoke-virtual {p0}, Lcom/android/server/wifi/WifiDrvUEventObserver;->stopObserving()V
/* .line 166 */
return;
} // .end method
