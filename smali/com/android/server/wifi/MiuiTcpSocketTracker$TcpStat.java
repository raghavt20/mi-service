public class com.android.server.wifi.MiuiTcpSocketTracker$TcpStat {
	 /* .source "MiuiTcpSocketTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wifi/MiuiTcpSocketTracker; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "TcpStat" */
} // .end annotation
/* # instance fields */
public Integer avgRcvRtt;
public Integer avgRtt;
public Integer avgRttVar;
public Integer lostCount;
public Integer minRtt;
public Integer recvCount;
public Integer retans;
public Integer retransmit;
public Integer sentCount;
public Integer socketCnt;
public Integer tcpCount;
final com.android.server.wifi.MiuiTcpSocketTracker this$0; //synthetic
public Integer totalretrans;
public Integer unacked;
/* # direct methods */
public com.android.server.wifi.MiuiTcpSocketTracker$TcpStat ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/wifi/MiuiTcpSocketTracker; */
/* .line 513 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 505 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->socketCnt:I */
/* .line 507 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRtt:I */
/* .line 508 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRttVar:I */
/* .line 509 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRcvRtt:I */
/* .line 510 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->minRtt:I */
/* .line 511 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->tcpCount:I */
/* .line 514 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I */
/* .line 515 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I */
/* .line 516 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I */
/* .line 517 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I */
/* .line 518 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I */
/* .line 519 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I */
/* .line 520 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I */
/* .line 522 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRtt:I */
/* .line 523 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRttVar:I */
/* .line 524 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRcvRtt:I */
/* .line 525 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->minRtt:I */
/* .line 526 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->tcpCount:I */
/* .line 527 */
return;
} // .end method
/* # virtual methods */
void accumulate ( com.android.server.wifi.MiuiTcpSocketTracker$TcpStat p0 ) {
/* .locals 2 */
/* .param p1, "stat" # Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat; */
/* .line 529 */
/* if-nez p1, :cond_0 */
return;
/* .line 530 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I */
/* iget v1, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I */
/* add-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I */
/* .line 531 */
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I */
/* iget v1, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I */
/* add-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I */
/* .line 532 */
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I */
/* iget v1, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I */
/* add-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I */
/* .line 533 */
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I */
/* iget v1, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I */
/* add-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I */
/* .line 534 */
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I */
/* iget v1, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I */
/* add-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I */
/* .line 535 */
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I */
/* iget v1, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I */
/* add-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I */
/* .line 536 */
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I */
/* iget v1, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I */
/* add-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I */
/* .line 537 */
return;
} // .end method
void avg ( com.android.server.wifi.MiuiTcpInfo p0 ) {
/* .locals 3 */
/* .param p1, "tcpInfo" # Lcom/android/server/wifi/MiuiTcpInfo; */
/* .line 540 */
/* if-nez p1, :cond_0 */
return;
/* .line 541 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRtt:I */
/* iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->tcpCount:I */
/* mul-int/2addr v0, v1 */
/* iget v1, p1, Lcom/android/server/wifi/MiuiTcpInfo;->mRtt:I */
/* add-int/2addr v0, v1 */
/* iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->tcpCount:I */
/* add-int/lit8 v2, v1, 0x1 */
/* div-int/2addr v0, v2 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRtt:I */
/* .line 542 */
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRttVar:I */
/* mul-int/2addr v0, v1 */
/* iget v1, p1, Lcom/android/server/wifi/MiuiTcpInfo;->mRttVar:I */
/* add-int/2addr v0, v1 */
/* iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->tcpCount:I */
/* add-int/lit8 v2, v1, 0x1 */
/* div-int/2addr v0, v2 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRttVar:I */
/* .line 543 */
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRtt:I */
/* mul-int/2addr v0, v1 */
/* iget v1, p1, Lcom/android/server/wifi/MiuiTcpInfo;->mRcvRtt:I */
/* add-int/2addr v0, v1 */
/* iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->tcpCount:I */
/* add-int/lit8 v2, v1, 0x1 */
/* div-int/2addr v0, v2 */
/* iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRcvRtt:I */
/* .line 545 */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->tcpCount:I */
/* .line 546 */
return;
} // .end method
public Integer getFailPercent ( ) {
/* .locals 4 */
/* .line 555 */
int v0 = -1; // const/4 v0, -0x1
/* .line 556 */
/* .local v0, "failpercent":I */
/* iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I */
/* if-lez v1, :cond_1 */
/* .line 557 */
/* iget v2, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I */
/* iget v3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I */
/* add-int/2addr v2, v3 */
/* iget v3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I */
/* add-int/2addr v2, v3 */
/* iget v3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I */
/* add-int/2addr v2, v3 */
/* const/16 v3, 0x64 */
/* mul-int/2addr v2, v3 */
/* div-int/2addr v2, v1 */
/* .line 558 */
} // .end local v0 # "failpercent":I
/* .local v2, "failpercent":I */
/* if-le v2, v3, :cond_0 */
} // :cond_0
/* move v3, v2 */
} // :goto_0
/* move v0, v3 */
/* .line 560 */
} // .end local v2 # "failpercent":I
/* .restart local v0 # "failpercent":I */
} // :cond_1
} // .end method
public java.lang.String printMsg ( ) {
/* .locals 2 */
/* .line 549 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " failpercent="; // const-string v1, " failpercent="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.android.server.wifi.MiuiTcpSocketTracker .-$$Nest$fgetmLatestPacketFailPercentage ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " { out="; // const-string v1, " { out="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " in="; // const-string v1, " in="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " lost="; // const-string v1, " lost="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " inretrans="; // const-string v1, " inretrans="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " total retrans="; // const-string v1, " total retrans="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " tmo="; // const-string v1, " tmo="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " inflight="; // const-string v1, " inflight="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
