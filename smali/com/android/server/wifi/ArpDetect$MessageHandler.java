class com.android.server.wifi.ArpDetect$MessageHandler extends android.os.Handler {
	 /* .source "ArpDetect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wifi/ArpDetect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MessageHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.wifi.ArpDetect this$0; //synthetic
/* # direct methods */
 com.android.server.wifi.ArpDetect$MessageHandler ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wifi/ArpDetect; */
/* .line 663 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/Handler;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 667 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 668 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 674 */
/* :pswitch_0 */
v0 = this.this$0;
(( com.android.server.wifi.ArpDetect ) v0 ).resetGatewayNeighState ( ); // invoke-virtual {v0}, Lcom/android/server/wifi/ArpDetect;->resetGatewayNeighState()Z
/* .line 675 */
v0 = this.this$0;
com.android.server.wifi.ArpDetect .-$$Nest$msetMultiGwRecoveryState ( v0,v1 );
/* .line 676 */
/* .line 670 */
/* :pswitch_1 */
v0 = this.this$0;
/* const-string/jumbo v1, "stop recv arp packet" */
com.android.server.wifi.ArpDetect .-$$Nest$mlogd ( v0,v1 );
/* .line 671 */
v0 = this.this$0;
com.android.server.wifi.ArpDetect .-$$Nest$munregisterArpRecvQueue ( v0 );
/* .line 672 */
/* nop */
/* .line 680 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x64 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
