.class public Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;
.super Ljava/lang/Object;
.source "MiuiTcpSocketTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wifi/MiuiTcpSocketTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Dependencies"
.end annotation


# static fields
.field private static final DEFAULT_RECV_BUFSIZE:I = 0xea60

.field private static final IO_TIMEOUT:J = 0x3e8L


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 637
    iput-object p1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->mContext:Landroid/content/Context;

    .line 638
    return-void
.end method


# virtual methods
.method public addDeviceConfigChangedListener(Landroid/provider/DeviceConfig$OnPropertiesChangedListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/provider/DeviceConfig$OnPropertiesChangedListener;

    .line 718
    const-string v0, "connectivity"

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, p1}, Landroid/provider/DeviceConfig;->addOnPropertiesChangedListener(Ljava/lang/String;Ljava/util/concurrent/Executor;Landroid/provider/DeviceConfig$OnPropertiesChangedListener;)V

    .line 720
    return-void
.end method

.method public connectToKernel()Ljava/io/FileDescriptor;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;,
            Ljava/net/SocketException;
        }
    .end annotation

    .line 647
    sget v0, Landroid/system/OsConstants;->AF_NETLINK:I

    sget v1, Landroid/system/OsConstants;->SOCK_DGRAM:I

    sget v2, Landroid/system/OsConstants;->SOCK_CLOEXEC:I

    or-int/2addr v1, v2

    sget v2, Landroid/system/OsConstants;->NETLINK_INET_DIAG:I

    .line 648
    invoke-static {v0, v1, v2}, Landroid/system/Os;->socket(III)Ljava/io/FileDescriptor;

    move-result-object v0

    .line 649
    .local v0, "fd":Ljava/io/FileDescriptor;
    nop

    .line 650
    const/4 v1, 0x0

    invoke-static {v1, v1}, Landroid/net/util/SocketUtils;->makeNetlinkSocketAddress(II)Ljava/net/SocketAddress;

    move-result-object v1

    .line 649
    invoke-static {v0, v1}, Landroid/system/Os;->connect(Ljava/io/FileDescriptor;Ljava/net/SocketAddress;)V

    .line 652
    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .line 729
    iget-object v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getDeviceConfigPropertyInt(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 3
    .param p1, "namespace"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "defaultValue"    # I

    .line 679
    invoke-static {p1, p2}, Landroid/provider/DeviceConfig;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 680
    .local v0, "value1":Ljava/lang/String;
    if-eqz v0, :cond_0

    move-object v1, v0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 682
    .local v1, "value":Ljava/lang/String;
    :goto_0
    if-eqz v1, :cond_1

    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 683
    :catch_0
    move-exception v2

    .line 684
    .local v2, "e":Ljava/lang/NumberFormatException;
    return p3

    .line 682
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_1
    move v2, p3

    :goto_1
    return v2
.end method

.method public getNetd()Landroid/net/INetd;
    .locals 2

    .line 711
    iget-object v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->mContext:Landroid/content/Context;

    .line 712
    const-string v1, "netd"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    .line 711
    invoke-static {v0}, Landroid/net/INetd$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetd;

    move-result-object v0

    return-object v0
.end method

.method public isTcpInfoParsingSupported()Z
    .locals 3

    .line 694
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 695
    const-string v1, "REL"

    sget-object v2, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 696
    .local v0, "devApiLevel":I
    const/16 v1, 0x1d

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method public recvMessage(Ljava/io/FileDescriptor;)Ljava/nio/ByteBuffer;
    .locals 3
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/io/InterruptedIOException;
        }
    .end annotation

    .line 704
    const v0, 0xea60

    const-wide/16 v1, 0x3e8

    invoke-static {p1, v0, v1, v2}, Lcom/android/net/module/util/netlink/NetlinkUtils;->recvMessage(Ljava/io/FileDescriptor;IJ)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public removeDeviceConfigChangedListener(Landroid/provider/DeviceConfig$OnPropertiesChangedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/provider/DeviceConfig$OnPropertiesChangedListener;

    .line 725
    invoke-static {p1}, Landroid/provider/DeviceConfig;->removeOnPropertiesChangedListener(Landroid/provider/DeviceConfig$OnPropertiesChangedListener;)V

    .line 726
    return-void
.end method

.method public sendPollingRequest(Ljava/io/FileDescriptor;[B)V
    .locals 4
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "msg"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;,
            Ljava/io/InterruptedIOException;
        }
    .end annotation

    .line 664
    sget v0, Landroid/system/OsConstants;->SOL_SOCKET:I

    sget v1, Landroid/system/OsConstants;->SO_SNDTIMEO:I

    .line 665
    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Landroid/system/StructTimeval;->fromMillis(J)Landroid/system/StructTimeval;

    move-result-object v2

    .line 664
    invoke-static {p1, v0, v1, v2}, Landroid/system/Os;->setsockoptTimeval(Ljava/io/FileDescriptor;IILandroid/system/StructTimeval;)V

    .line 666
    const/4 v0, 0x0

    array-length v1, p2

    invoke-static {p1, p2, v0, v1}, Landroid/system/Os;->write(Ljava/io/FileDescriptor;[BII)I

    .line 667
    return-void
.end method
