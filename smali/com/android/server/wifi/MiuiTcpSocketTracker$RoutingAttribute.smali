.class Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;
.super Ljava/lang/Object;
.source "MiuiTcpSocketTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wifi/MiuiTcpSocketTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RoutingAttribute"
.end annotation


# static fields
.field public static final HEADER_LENGTH:I = 0x4

.field public static final INET_DIAG_INFO:I = 0x2

.field public static final INET_DIAG_MARK:I = 0xf


# instance fields
.field public final rtaLen:S

.field public final rtaType:S

.field final synthetic this$0:Lcom/android/server/wifi/MiuiTcpSocketTracker;


# direct methods
.method constructor <init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;SS)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wifi/MiuiTcpSocketTracker;
    .param p2, "len"    # S
    .param p3, "type"    # S

    .line 623
    iput-object p1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;->this$0:Lcom/android/server/wifi/MiuiTcpSocketTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624
    iput-short p2, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;->rtaLen:S

    .line 625
    iput-short p3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;->rtaType:S

    .line 626
    return-void
.end method


# virtual methods
.method public getDataLength()S
    .locals 1

    .line 628
    iget-short v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;->rtaLen:S

    add-int/lit8 v0, v0, -0x4

    int-to-short v0, v0

    return v0
.end method
