public class inal extends java.lang.Enum {
	 /* .source "MiuiTcpInfo.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wifi/MiuiTcpInfo; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x4019 */
/* name = "Field" */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Enum<", */
/* "Lcom/android/server/wifi/MiuiTcpInfo$Field;", */
/* ">;" */
/* } */
} // .end annotation
/* # static fields */
private static final com.android.server.wifi.MiuiTcpInfo$Field $VALUES; //synthetic
public static final com.android.server.wifi.MiuiTcpInfo$Field ADVMSS;
public static final com.android.server.wifi.MiuiTcpInfo$Field ATO;
public static final com.android.server.wifi.MiuiTcpInfo$Field BACKOFF;
public static final com.android.server.wifi.MiuiTcpInfo$Field BUSY_TIME;
public static final com.android.server.wifi.MiuiTcpInfo$Field BYTES_ACKED;
public static final com.android.server.wifi.MiuiTcpInfo$Field BYTES_RECEIVED;
public static final com.android.server.wifi.MiuiTcpInfo$Field BYTES_RETRANS;
public static final com.android.server.wifi.MiuiTcpInfo$Field BYTES_SEND;
public static final com.android.server.wifi.MiuiTcpInfo$Field CASTATE;
public static final com.android.server.wifi.MiuiTcpInfo$Field DACK_DUPS;
public static final com.android.server.wifi.MiuiTcpInfo$Field DATA_SEGS_IN;
public static final com.android.server.wifi.MiuiTcpInfo$Field DATA_SEGS_OUT;
public static final com.android.server.wifi.MiuiTcpInfo$Field DELIVERED;
public static final com.android.server.wifi.MiuiTcpInfo$Field DELIVERED_CE;
public static final com.android.server.wifi.MiuiTcpInfo$Field DELIVERY_RATE;
public static final com.android.server.wifi.MiuiTcpInfo$Field DELIVERY_RATE_APP_LIMITED;
public static final com.android.server.wifi.MiuiTcpInfo$Field FACKETS;
public static final com.android.server.wifi.MiuiTcpInfo$Field LAST_ACK_RECV;
public static final com.android.server.wifi.MiuiTcpInfo$Field LAST_ACK_SENT;
public static final com.android.server.wifi.MiuiTcpInfo$Field LAST_DATA_RECV;
public static final com.android.server.wifi.MiuiTcpInfo$Field LAST_DATA_SENT;
public static final com.android.server.wifi.MiuiTcpInfo$Field LOST;
public static final com.android.server.wifi.MiuiTcpInfo$Field MAX_PACING_RATE;
public static final com.android.server.wifi.MiuiTcpInfo$Field MIN_RTT;
public static final com.android.server.wifi.MiuiTcpInfo$Field NOTSENT_BYTES;
public static final com.android.server.wifi.MiuiTcpInfo$Field OPTIONS;
public static final com.android.server.wifi.MiuiTcpInfo$Field PACING_RATE;
public static final com.android.server.wifi.MiuiTcpInfo$Field PMTU;
public static final com.android.server.wifi.MiuiTcpInfo$Field PROBES;
public static final com.android.server.wifi.MiuiTcpInfo$Field RCV_MSS;
public static final com.android.server.wifi.MiuiTcpInfo$Field RCV_OOOPACK;
public static final com.android.server.wifi.MiuiTcpInfo$Field RCV_RTT;
public static final com.android.server.wifi.MiuiTcpInfo$Field RCV_SPACE;
public static final com.android.server.wifi.MiuiTcpInfo$Field RCV_SSTHRESH;
public static final com.android.server.wifi.MiuiTcpInfo$Field RECORD_SEEN;
public static final com.android.server.wifi.MiuiTcpInfo$Field REORDERING;
public static final com.android.server.wifi.MiuiTcpInfo$Field RETRANS;
public static final com.android.server.wifi.MiuiTcpInfo$Field RETRANSMITS;
public static final com.android.server.wifi.MiuiTcpInfo$Field RTO;
public static final com.android.server.wifi.MiuiTcpInfo$Field RTT;
public static final com.android.server.wifi.MiuiTcpInfo$Field RTTVAR;
public static final com.android.server.wifi.MiuiTcpInfo$Field RWND_LIMITED;
public static final com.android.server.wifi.MiuiTcpInfo$Field SACKED;
public static final com.android.server.wifi.MiuiTcpInfo$Field SEGS_IN;
public static final com.android.server.wifi.MiuiTcpInfo$Field SEGS_OUT;
public static final com.android.server.wifi.MiuiTcpInfo$Field SNDBUF_LIMITED;
public static final com.android.server.wifi.MiuiTcpInfo$Field SND_CWND;
public static final com.android.server.wifi.MiuiTcpInfo$Field SND_MSS;
public static final com.android.server.wifi.MiuiTcpInfo$Field SND_SSTHRESH;
public static final com.android.server.wifi.MiuiTcpInfo$Field SND_WND;
public static final com.android.server.wifi.MiuiTcpInfo$Field STATE;
public static final com.android.server.wifi.MiuiTcpInfo$Field TOTAL_RETRANS;
public static final com.android.server.wifi.MiuiTcpInfo$Field UNACKED;
public static final com.android.server.wifi.MiuiTcpInfo$Field WSCALE;
/* # instance fields */
public final Integer size;
/* # direct methods */
private static com.android.server.wifi.MiuiTcpInfo$Field $values ( ) { //synthethic
/* .locals 54 */
/* .line 22 */
v0 = com.android.server.wifi.MiuiTcpInfo$Field.STATE;
v1 = com.android.server.wifi.MiuiTcpInfo$Field.CASTATE;
v2 = com.android.server.wifi.MiuiTcpInfo$Field.RETRANSMITS;
v3 = com.android.server.wifi.MiuiTcpInfo$Field.PROBES;
v4 = com.android.server.wifi.MiuiTcpInfo$Field.BACKOFF;
v5 = com.android.server.wifi.MiuiTcpInfo$Field.OPTIONS;
v6 = com.android.server.wifi.MiuiTcpInfo$Field.WSCALE;
v7 = com.android.server.wifi.MiuiTcpInfo$Field.DELIVERY_RATE_APP_LIMITED;
v8 = com.android.server.wifi.MiuiTcpInfo$Field.RTO;
v9 = com.android.server.wifi.MiuiTcpInfo$Field.ATO;
v10 = com.android.server.wifi.MiuiTcpInfo$Field.SND_MSS;
v11 = com.android.server.wifi.MiuiTcpInfo$Field.RCV_MSS;
v12 = com.android.server.wifi.MiuiTcpInfo$Field.UNACKED;
v13 = com.android.server.wifi.MiuiTcpInfo$Field.SACKED;
v14 = com.android.server.wifi.MiuiTcpInfo$Field.LOST;
v15 = com.android.server.wifi.MiuiTcpInfo$Field.RETRANS;
v16 = com.android.server.wifi.MiuiTcpInfo$Field.FACKETS;
v17 = com.android.server.wifi.MiuiTcpInfo$Field.LAST_DATA_SENT;
v18 = com.android.server.wifi.MiuiTcpInfo$Field.LAST_ACK_SENT;
v19 = com.android.server.wifi.MiuiTcpInfo$Field.LAST_DATA_RECV;
v20 = com.android.server.wifi.MiuiTcpInfo$Field.LAST_ACK_RECV;
v21 = com.android.server.wifi.MiuiTcpInfo$Field.PMTU;
v22 = com.android.server.wifi.MiuiTcpInfo$Field.RCV_SSTHRESH;
v23 = com.android.server.wifi.MiuiTcpInfo$Field.RTT;
v24 = com.android.server.wifi.MiuiTcpInfo$Field.RTTVAR;
v25 = com.android.server.wifi.MiuiTcpInfo$Field.SND_SSTHRESH;
v26 = com.android.server.wifi.MiuiTcpInfo$Field.SND_CWND;
v27 = com.android.server.wifi.MiuiTcpInfo$Field.ADVMSS;
v28 = com.android.server.wifi.MiuiTcpInfo$Field.REORDERING;
v29 = com.android.server.wifi.MiuiTcpInfo$Field.RCV_RTT;
v30 = com.android.server.wifi.MiuiTcpInfo$Field.RCV_SPACE;
v31 = com.android.server.wifi.MiuiTcpInfo$Field.TOTAL_RETRANS;
v32 = com.android.server.wifi.MiuiTcpInfo$Field.PACING_RATE;
v33 = com.android.server.wifi.MiuiTcpInfo$Field.MAX_PACING_RATE;
v34 = com.android.server.wifi.MiuiTcpInfo$Field.BYTES_ACKED;
v35 = com.android.server.wifi.MiuiTcpInfo$Field.BYTES_RECEIVED;
v36 = com.android.server.wifi.MiuiTcpInfo$Field.SEGS_OUT;
v37 = com.android.server.wifi.MiuiTcpInfo$Field.SEGS_IN;
v38 = com.android.server.wifi.MiuiTcpInfo$Field.NOTSENT_BYTES;
v39 = com.android.server.wifi.MiuiTcpInfo$Field.MIN_RTT;
v40 = com.android.server.wifi.MiuiTcpInfo$Field.DATA_SEGS_IN;
v41 = com.android.server.wifi.MiuiTcpInfo$Field.DATA_SEGS_OUT;
v42 = com.android.server.wifi.MiuiTcpInfo$Field.DELIVERY_RATE;
v43 = com.android.server.wifi.MiuiTcpInfo$Field.BUSY_TIME;
v44 = com.android.server.wifi.MiuiTcpInfo$Field.RWND_LIMITED;
v45 = com.android.server.wifi.MiuiTcpInfo$Field.SNDBUF_LIMITED;
v46 = com.android.server.wifi.MiuiTcpInfo$Field.DELIVERED;
v47 = com.android.server.wifi.MiuiTcpInfo$Field.DELIVERED_CE;
v48 = com.android.server.wifi.MiuiTcpInfo$Field.BYTES_SEND;
v49 = com.android.server.wifi.MiuiTcpInfo$Field.BYTES_RETRANS;
v50 = com.android.server.wifi.MiuiTcpInfo$Field.DACK_DUPS;
v51 = com.android.server.wifi.MiuiTcpInfo$Field.RECORD_SEEN;
v52 = com.android.server.wifi.MiuiTcpInfo$Field.RCV_OOOPACK;
v53 = com.android.server.wifi.MiuiTcpInfo$Field.SND_WND;
/* filled-new-array/range {v0 ..v53}, [Lcom/android/server/wifi/MiuiTcpInfo$Field; */
} // .end method
static inal ( ) {
/* .locals 5 */
/* .line 23 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "STATE"; // const-string v1, "STATE"
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {v0, v1, v2, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 24 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "CASTATE"; // const-string v1, "CASTATE"
/* invoke-direct {v0, v1, v3, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 25 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "RETRANSMITS"; // const-string v1, "RETRANSMITS"
int v2 = 2; // const/4 v2, 0x2
/* invoke-direct {v0, v1, v2, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 26 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "PROBES"; // const-string v1, "PROBES"
int v2 = 3; // const/4 v2, 0x3
/* invoke-direct {v0, v1, v2, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 27 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "BACKOFF"; // const-string v1, "BACKOFF"
int v2 = 4; // const/4 v2, 0x4
/* invoke-direct {v0, v1, v2, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 28 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "OPTIONS"; // const-string v1, "OPTIONS"
int v4 = 5; // const/4 v4, 0x5
/* invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 29 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "WSCALE"; // const-string v1, "WSCALE"
int v4 = 6; // const/4 v4, 0x6
/* invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 30 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "DELIVERY_RATE_APP_LIMITED"; // const-string v1, "DELIVERY_RATE_APP_LIMITED"
int v4 = 7; // const/4 v4, 0x7
/* invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 32 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "RTO"; // const-string v1, "RTO"
/* const/16 v3, 0x8 */
/* invoke-direct {v0, v1, v3, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 33 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "ATO"; // const-string v1, "ATO"
/* const/16 v4, 0x9 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 36 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "SND_MSS"; // const-string v1, "SND_MSS"
/* const/16 v4, 0xa */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 37 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "RCV_MSS"; // const-string v1, "RCV_MSS"
/* const/16 v4, 0xb */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 38 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "UNACKED"; // const-string v1, "UNACKED"
/* const/16 v4, 0xc */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 39 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "SACKED"; // const-string v1, "SACKED"
/* const/16 v4, 0xd */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 40 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "LOST"; // const-string v1, "LOST"
/* const/16 v4, 0xe */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 41 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "RETRANS"; // const-string v1, "RETRANS"
/* const/16 v4, 0xf */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 42 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "FACKETS"; // const-string v1, "FACKETS"
/* const/16 v4, 0x10 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 44 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "LAST_DATA_SENT"; // const-string v1, "LAST_DATA_SENT"
/* const/16 v4, 0x11 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 45 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "LAST_ACK_SENT"; // const-string v1, "LAST_ACK_SENT"
/* const/16 v4, 0x12 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 46 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "LAST_DATA_RECV"; // const-string v1, "LAST_DATA_RECV"
/* const/16 v4, 0x13 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 47 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "LAST_ACK_RECV"; // const-string v1, "LAST_ACK_RECV"
/* const/16 v4, 0x14 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 49 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "PMTU"; // const-string v1, "PMTU"
/* const/16 v4, 0x15 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 50 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "RCV_SSTHRESH"; // const-string v1, "RCV_SSTHRESH"
/* const/16 v4, 0x16 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 51 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "RTT"; // const-string v1, "RTT"
/* const/16 v4, 0x17 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 52 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "RTTVAR"; // const-string v1, "RTTVAR"
/* const/16 v4, 0x18 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 53 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "SND_SSTHRESH"; // const-string v1, "SND_SSTHRESH"
/* const/16 v4, 0x19 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 54 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "SND_CWND"; // const-string v1, "SND_CWND"
/* const/16 v4, 0x1a */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 55 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "ADVMSS"; // const-string v1, "ADVMSS"
/* const/16 v4, 0x1b */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 56 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "REORDERING"; // const-string v1, "REORDERING"
/* const/16 v4, 0x1c */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 57 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "RCV_RTT"; // const-string v1, "RCV_RTT"
/* const/16 v4, 0x1d */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 59 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "RCV_SPACE"; // const-string v1, "RCV_SPACE"
/* const/16 v4, 0x1e */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 61 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "TOTAL_RETRANS"; // const-string v1, "TOTAL_RETRANS"
/* const/16 v4, 0x1f */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 63 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "PACING_RATE"; // const-string v1, "PACING_RATE"
/* const/16 v4, 0x20 */
/* invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 64 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "MAX_PACING_RATE"; // const-string v1, "MAX_PACING_RATE"
/* const/16 v4, 0x21 */
/* invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 65 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "BYTES_ACKED"; // const-string v1, "BYTES_ACKED"
/* const/16 v4, 0x22 */
/* invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 66 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "BYTES_RECEIVED"; // const-string v1, "BYTES_RECEIVED"
/* const/16 v4, 0x23 */
/* invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 67 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "SEGS_OUT"; // const-string v1, "SEGS_OUT"
/* const/16 v4, 0x24 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 68 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "SEGS_IN"; // const-string v1, "SEGS_IN"
/* const/16 v4, 0x25 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 69 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "NOTSENT_BYTES"; // const-string v1, "NOTSENT_BYTES"
/* const/16 v4, 0x26 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 70 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "MIN_RTT"; // const-string v1, "MIN_RTT"
/* const/16 v4, 0x27 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 71 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "DATA_SEGS_IN"; // const-string v1, "DATA_SEGS_IN"
/* const/16 v4, 0x28 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 72 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "DATA_SEGS_OUT"; // const-string v1, "DATA_SEGS_OUT"
/* const/16 v4, 0x29 */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 73 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "DELIVERY_RATE"; // const-string v1, "DELIVERY_RATE"
/* const/16 v4, 0x2a */
/* invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 74 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "BUSY_TIME"; // const-string v1, "BUSY_TIME"
/* const/16 v4, 0x2b */
/* invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 75 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "RWND_LIMITED"; // const-string v1, "RWND_LIMITED"
/* const/16 v4, 0x2c */
/* invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 76 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "SNDBUF_LIMITED"; // const-string v1, "SNDBUF_LIMITED"
/* const/16 v4, 0x2d */
/* invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 77 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "DELIVERED"; // const-string v1, "DELIVERED"
/* const/16 v4, 0x2e */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 78 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "DELIVERED_CE"; // const-string v1, "DELIVERED_CE"
/* const/16 v4, 0x2f */
/* invoke-direct {v0, v1, v4, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 79 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "BYTES_SEND"; // const-string v1, "BYTES_SEND"
/* const/16 v4, 0x30 */
/* invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 80 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "BYTES_RETRANS"; // const-string v1, "BYTES_RETRANS"
/* const/16 v4, 0x31 */
/* invoke-direct {v0, v1, v4, v3}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 81 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "DACK_DUPS"; // const-string v1, "DACK_DUPS"
/* const/16 v3, 0x32 */
/* invoke-direct {v0, v1, v3, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 82 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "RECORD_SEEN"; // const-string v1, "RECORD_SEEN"
/* const/16 v3, 0x33 */
/* invoke-direct {v0, v1, v3, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 83 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "RCV_OOOPACK"; // const-string v1, "RCV_OOOPACK"
/* const/16 v3, 0x34 */
/* invoke-direct {v0, v1, v3, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 84 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
final String v1 = "SND_WND"; // const-string v1, "SND_WND"
/* const/16 v3, 0x35 */
/* invoke-direct {v0, v1, v3, v2}, Lcom/android/server/wifi/MiuiTcpInfo$Field;-><init>(Ljava/lang/String;II)V */
/* .line 22 */
com.android.server.wifi.MiuiTcpInfo$Field .$values ( );
return;
} // .end method
private inal ( ) {
/* .locals 0 */
/* .param p3, "s" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)V" */
/* } */
} // .end annotation
/* .line 88 */
/* invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V */
/* .line 89 */
/* iput p3, p0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->size:I */
/* .line 90 */
return;
} // .end method
public static com.android.server.wifi.MiuiTcpInfo$Field valueOf ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "name" # Ljava/lang/String; */
/* .line 22 */
/* const-class v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
java.lang.Enum .valueOf ( v0,p0 );
/* check-cast v0, Lcom/android/server/wifi/MiuiTcpInfo$Field; */
} // .end method
public static com.android.server.wifi.MiuiTcpInfo$Field values ( ) {
/* .locals 1 */
/* .line 22 */
v0 = com.android.server.wifi.MiuiTcpInfo$Field.$VALUES;
(( com.android.server.wifi.MiuiTcpInfo$Field ) v0 ).clone ( ); // invoke-virtual {v0}, [Lcom/android/server/wifi/MiuiTcpInfo$Field;->clone()Ljava/lang/Object;
/* check-cast v0, [Lcom/android/server/wifi/MiuiTcpInfo$Field; */
} // .end method
