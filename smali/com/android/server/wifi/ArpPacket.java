class com.android.server.wifi.ArpPacket {
	 /* .source "ArpPacket.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wifi/ArpPacket$ParseException; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer ARP_ETHER_IPV4_LEN;
public static final Integer ARP_HWTYPE_ETHER;
public static final Integer ARP_PAYLOAD_LEN;
public static final Integer ETHER_ADDR_LEN;
public static final ETHER_ANY;
public static final ETHER_BROADCAST;
public static final Integer ETHER_HEADER_LEN;
public static final java.net.Inet4Address INADDR_ANY;
public static final Integer IPV4_ADDR_LEN;
public static final Object OPCODE_ARP_REQUEST;
public static final Object OPCODE_ARP_RESPONSE;
/* # instance fields */
public final Object mOpCode;
public final mSenderHwAddress;
public final java.net.Inet4Address mSenderIp;
public final java.net.Inet4Address mTargetIp;
public final mTtargetHwAddress;
/* # direct methods */
static com.android.server.wifi.ArpPacket ( ) {
	 /* .locals 2 */
	 /* .line 21 */
	 v0 = java.net.Inet4Address.ANY;
	 /* check-cast v0, Ljava/net/Inet4Address; */
	 /* .line 22 */
	 int v0 = 6; // const/4 v0, 0x6
	 /* new-array v1, v0, [B */
	 /* fill-array-data v1, :array_0 */
	 /* .line 26 */
	 /* new-array v0, v0, [B */
	 /* fill-array-data v0, :array_1 */
	 return;
	 /* :array_0 */
	 /* .array-data 1 */
	 /* -0x1t */
	 /* -0x1t */
	 /* -0x1t */
	 /* -0x1t */
	 /* -0x1t */
	 /* -0x1t */
} // .end array-data
/* nop */
/* :array_1 */
/* .array-data 1 */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
} // .end array-data
} // .end method
 com.android.server.wifi.ArpPacket ( ) {
/* .locals 0 */
/* .param p1, "opCode" # S */
/* .param p2, "senderHwAddress" # [B */
/* .param p3, "senderIp" # Ljava/net/Inet4Address; */
/* .param p4, "targetHwAddress" # [B */
/* .param p5, "targetIp" # Ljava/net/Inet4Address; */
/* .line 63 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 64 */
/* iput-short p1, p0, Lcom/android/server/wifi/ArpPacket;->mOpCode:S */
/* .line 65 */
this.mSenderHwAddress = p2;
/* .line 66 */
this.mSenderIp = p3;
/* .line 67 */
this.mTtargetHwAddress = p4;
/* .line 68 */
this.mTargetIp = p5;
/* .line 69 */
return;
} // .end method
public static java.nio.ByteBuffer buildArpPacket ( Object[] p0, Object[] p1, Object[] p2, Object[] p3, Object[] p4, Object[] p5, Object p6 ) {
/* .locals 2 */
/* .param p0, "dstMac" # [B */
/* .param p1, "srcMac" # [B */
/* .param p2, "sendHwAddress" # [B */
/* .param p3, "senderIp" # [B */
/* .param p4, "targetHwAddress" # [B */
/* .param p5, "targetIp" # [B */
/* .param p6, "opCode" # S */
/* .line 79 */
/* const/16 v0, 0x2a */
java.nio.ByteBuffer .allocate ( v0 );
/* .line 80 */
/* .local v0, "buf":Ljava/nio/ByteBuffer; */
(( java.nio.ByteBuffer ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;
/* .line 81 */
v1 = java.nio.ByteOrder.BIG_ENDIAN;
(( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 84 */
(( java.nio.ByteBuffer ) v0 ).put ( p0 ); // invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 85 */
(( java.nio.ByteBuffer ) v0 ).put ( p1 ); // invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 86 */
/* int-to-short v1, v1 */
(( java.nio.ByteBuffer ) v0 ).putShort ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 89 */
int v1 = 1; // const/4 v1, 0x1
(( java.nio.ByteBuffer ) v0 ).putShort ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 90 */
/* int-to-short v1, v1 */
(( java.nio.ByteBuffer ) v0 ).putShort ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 91 */
int v1 = 6; // const/4 v1, 0x6
(( java.nio.ByteBuffer ) v0 ).put ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 92 */
int v1 = 4; // const/4 v1, 0x4
(( java.nio.ByteBuffer ) v0 ).put ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 93 */
(( java.nio.ByteBuffer ) v0 ).putShort ( p6 ); // invoke-virtual {v0, p6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 94 */
(( java.nio.ByteBuffer ) v0 ).put ( p2 ); // invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 95 */
(( java.nio.ByteBuffer ) v0 ).put ( p3 ); // invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 96 */
(( java.nio.ByteBuffer ) v0 ).put ( p4 ); // invoke-virtual {v0, p4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 97 */
(( java.nio.ByteBuffer ) v0 ).put ( p5 ); // invoke-virtual {v0, p5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 98 */
(( java.nio.ByteBuffer ) v0 ).flip ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;
/* .line 99 */
} // .end method
public static com.android.server.wifi.ArpPacket parseArpPacket ( Object[] p0, Integer p1 ) {
/* .locals 22 */
/* .param p0, "recvbuf" # [B */
/* .param p1, "length" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lcom/android/server/wifi/ArpPacket$ParseException; */
/* } */
} // .end annotation
/* .line 108 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p1 */
/* const/16 v0, 0x2a */
/* if-lt v2, v0, :cond_7 */
try { // :try_start_0
/* array-length v0, v1 */
/* if-lt v0, v2, :cond_7 */
/* .line 112 */
int v0 = 0; // const/4 v0, 0x0
java.nio.ByteBuffer .wrap ( v1,v0,v2 );
v3 = java.nio.ByteOrder.BIG_ENDIAN;
/* .line 113 */
(( java.nio.ByteBuffer ) v0 ).order ( v3 ); // invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 115 */
/* .local v0, "buffer":Ljava/nio/ByteBuffer; */
int v3 = 6; // const/4 v3, 0x6
/* new-array v4, v3, [B */
/* .line 116 */
/* .local v4, "l2dst":[B */
/* new-array v5, v3, [B */
/* .line 117 */
/* .local v5, "l2src":[B */
(( java.nio.ByteBuffer ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 118 */
(( java.nio.ByteBuffer ) v0 ).get ( v5 ); // invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 120 */
v6 = (( java.nio.ByteBuffer ) v0 ).getShort ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S
/* .line 121 */
/* .local v6, "etherType":S */
/* if-ne v6, v7, :cond_6 */
/* .line 125 */
v7 = (( java.nio.ByteBuffer ) v0 ).getShort ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S
/* .line 126 */
/* .local v7, "hwType":S */
int v8 = 1; // const/4 v8, 0x1
/* if-ne v7, v8, :cond_5 */
/* .line 130 */
v9 = (( java.nio.ByteBuffer ) v0 ).getShort ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S
/* .line 131 */
/* .local v9, "protoType":S */
/* if-ne v9, v10, :cond_4 */
/* .line 135 */
v10 = (( java.nio.ByteBuffer ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B
/* .line 136 */
/* .local v10, "hwAddrLength":B */
/* if-ne v10, v3, :cond_3 */
/* .line 141 */
v11 = (( java.nio.ByteBuffer ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B
/* .line 142 */
/* .local v11, "ipAddrLength":B */
int v12 = 4; // const/4 v12, 0x4
/* if-ne v11, v12, :cond_2 */
/* .line 147 */
v13 = (( java.nio.ByteBuffer ) v0 ).getShort ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S
/* .line 148 */
/* .local v13, "opCode":S */
/* if-eq v13, v8, :cond_1 */
int v8 = 2; // const/4 v8, 0x2
/* if-ne v13, v8, :cond_0 */
/* .line 149 */
} // :cond_0
/* new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "Incorrect opCode: "; // const-string v12, "Incorrect opCode: "
(( java.lang.StringBuilder ) v8 ).append ( v12 ); // invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v13 ); // invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v8}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "recvbuf":[B
} // .end local p1 # "length":I
/* throw v3 */
/* .line 152 */
/* .restart local p0 # "recvbuf":[B */
/* .restart local p1 # "length":I */
} // :cond_1
} // :goto_0
/* new-array v8, v3, [B */
/* .line 153 */
/* .local v8, "senderHwAddress":[B */
/* new-array v14, v12, [B */
/* move-object v15, v14 */
/* .line 154 */
/* .local v15, "senderIp":[B */
(( java.nio.ByteBuffer ) v0 ).get ( v8 ); // invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 155 */
(( java.nio.ByteBuffer ) v0 ).get ( v15 ); // invoke-virtual {v0, v15}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 157 */
/* new-array v3, v3, [B */
/* .line 158 */
/* .local v3, "targetHwAddress":[B */
/* new-array v12, v12, [B */
/* .line 159 */
/* .local v12, "targetIp":[B */
(( java.nio.ByteBuffer ) v0 ).get ( v3 ); // invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 160 */
(( java.nio.ByteBuffer ) v0 ).get ( v12 ); // invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 162 */
/* new-instance v20, Lcom/android/server/wifi/ArpPacket; */
/* .line 163 */
java.net.InetAddress .getByAddress ( v15 );
/* move-object/from16 v17, v14 */
/* check-cast v17, Ljava/net/Inet4Address; */
/* .line 164 */
java.net.InetAddress .getByAddress ( v12 );
/* move-object/from16 v19, v14 */
/* check-cast v19, Ljava/net/Inet4Address; */
/* move-object/from16 v14, v20 */
/* move-object/from16 v21, v15 */
} // .end local v15 # "senderIp":[B
/* .local v21, "senderIp":[B */
/* move v15, v13 */
/* move-object/from16 v16, v8 */
/* move-object/from16 v18, v3 */
/* invoke-direct/range {v14 ..v19}, Lcom/android/server/wifi/ArpPacket;-><init>(S[BLjava/net/Inet4Address;[BLjava/net/Inet4Address;)V */
/* .line 162 */
/* .line 143 */
} // .end local v3 # "targetHwAddress":[B
} // .end local v8 # "senderHwAddress":[B
} // .end local v12 # "targetIp":[B
} // .end local v13 # "opCode":S
} // .end local v21 # "senderIp":[B
} // :cond_2
/* new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "Incorrect Protocol address length: "; // const-string v12, "Incorrect Protocol address length: "
(( java.lang.StringBuilder ) v8 ).append ( v12 ); // invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v8}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "recvbuf":[B
} // .end local p1 # "length":I
/* throw v3 */
/* .line 137 */
} // .end local v11 # "ipAddrLength":B
/* .restart local p0 # "recvbuf":[B */
/* .restart local p1 # "length":I */
} // :cond_3
/* new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "Incorrect HW address length: "; // const-string v11, "Incorrect HW address length: "
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v8}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "recvbuf":[B
} // .end local p1 # "length":I
/* throw v3 */
/* .line 132 */
} // .end local v10 # "hwAddrLength":B
/* .restart local p0 # "recvbuf":[B */
/* .restart local p1 # "length":I */
} // :cond_4
/* new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Incorrect Protocol Type: "; // const-string v10, "Incorrect Protocol Type: "
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v8}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "recvbuf":[B
} // .end local p1 # "length":I
/* throw v3 */
/* .line 127 */
} // .end local v9 # "protoType":S
/* .restart local p0 # "recvbuf":[B */
/* .restart local p1 # "length":I */
} // :cond_5
/* new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Incorrect HW Type: "; // const-string v9, "Incorrect HW Type: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v8}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "recvbuf":[B
} // .end local p1 # "length":I
/* throw v3 */
/* .line 122 */
} // .end local v7 # "hwType":S
/* .restart local p0 # "recvbuf":[B */
/* .restart local p1 # "length":I */
} // :cond_6
/* new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Incorrect Ether Type: "; // const-string v8, "Incorrect Ether Type: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v7}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "recvbuf":[B
} // .end local p1 # "length":I
/* throw v3 */
/* .line 109 */
} // .end local v0 # "buffer":Ljava/nio/ByteBuffer;
} // .end local v4 # "l2dst":[B
} // .end local v5 # "l2src":[B
} // .end local v6 # "etherType":S
/* .restart local p0 # "recvbuf":[B */
/* .restart local p1 # "length":I */
} // :cond_7
/* new-instance v0, Lcom/android/server/wifi/ArpPacket$ParseException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Invalid packet length: "; // const-string v4, "Invalid packet length: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v3}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "recvbuf":[B
} // .end local p1 # "length":I
/* throw v0 */
/* :try_end_0 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catch Ljava/nio/BufferUnderflowException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/net/UnknownHostException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 172 */
/* .restart local p0 # "recvbuf":[B */
/* .restart local p1 # "length":I */
/* :catch_0 */
/* move-exception v0 */
/* .line 173 */
/* .local v0, "e":Ljava/net/UnknownHostException; */
/* new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException; */
final String v4 = "Invalid IP address of Host"; // const-string v4, "Invalid IP address of Host"
/* invoke-direct {v3, v4}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
/* .line 170 */
} // .end local v0 # "e":Ljava/net/UnknownHostException;
/* :catch_1 */
/* move-exception v0 */
/* .line 171 */
/* .local v0, "e":Ljava/lang/IllegalArgumentException; */
/* new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException; */
final String v4 = "Invalid MAC address representation"; // const-string v4, "Invalid MAC address representation"
/* invoke-direct {v3, v4}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
/* .line 168 */
} // .end local v0 # "e":Ljava/lang/IllegalArgumentException;
/* :catch_2 */
/* move-exception v0 */
/* .line 169 */
/* .local v0, "e":Ljava/nio/BufferUnderflowException; */
/* new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException; */
final String v4 = "Invalid buffer position"; // const-string v4, "Invalid buffer position"
/* invoke-direct {v3, v4}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
/* .line 165 */
} // .end local v0 # "e":Ljava/nio/BufferUnderflowException;
/* :catch_3 */
/* move-exception v0 */
/* .line 166 */
/* .local v0, "e":Ljava/lang/IndexOutOfBoundsException; */
/* new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException; */
final String v4 = "Invalid index when wrapping a byte array into a buffer"; // const-string v4, "Invalid index when wrapping a byte array into a buffer"
/* invoke-direct {v3, v4}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
} // .end method
