public class com.android.server.wifi.MiuiTcpSocketTracker {
	 /* .source "MiuiTcpSocketTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;, */
	 /* Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;, */
	 /* Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;, */
	 /* Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute; */
	 /* } */
} // .end annotation
/* # static fields */
private static final ADDRESS_FAMILIES;
public static final java.lang.String CONFIG_MIN_PACKETS_THRESHOLD;
public static final java.lang.String CONFIG_TCP_PACKETS_FAIL_PERCENTAGE;
public static final Integer DEFAULT_DATA_STALL_MIN_PACKETS_THRESHOLD;
public static final Integer DEFAULT_NLMSG_DONE_PACKET_SIZE;
public static final Integer DEFAULT_TCP_PACKETS_FAIL_PERCENTAGE;
private static final Integer IDIAG_COOKIE_OFFSET;
private static final Integer IDIAG_UID2COOKIE_OFFSET;
private static final Integer NULL_MASK;
private static final java.lang.String TAG;
public static final Integer TCP_ESTABLISHED;
public static final Integer TCP_MONITOR_STATE_FILTER;
public static final Integer TCP_SYN_RECV;
public static final Integer TCP_SYN_SENT;
private static final Integer THRESHOLD_OF_SENT;
private static final Integer UNKNOWN_MARK;
/* # instance fields */
protected final android.provider.DeviceConfig$OnPropertiesChangedListener mConfigListener;
private final com.android.server.wifi.MiuiTcpSocketTracker$Dependencies mDependencies;
private Integer mLatestPacketFailPercentage;
private Integer mLatestReceivedCount;
private com.android.server.wifi.MiuiTcpSocketTracker$TcpStat mLatestTcpStats;
private Integer mMinPacketsThreshold;
private final android.net.INetd mNetd;
private final android.net.Network mNetwork;
private final Integer mNetworkMark;
private final Integer mNetworkMask;
private Integer mSentSinceLastRecv;
private java.util.Set mSkipUidList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.SparseArray mSockDiagMsg;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "[B>;" */
/* } */
} // .end annotation
} // .end field
private final android.util.LongSparseArray mSocketInfos;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/LongSparseArray<", */
/* "Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mTcpPacketsFailRateThreshold;
private final android.util.LongSparseArray mToRemovedSocketInfos;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/LongSparseArray<", */
/* "Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String msgRecord;
/* # direct methods */
static com.android.server.wifi.MiuiTcpSocketTracker$Dependencies -$$Nest$fgetmDependencies ( com.android.server.wifi.MiuiTcpSocketTracker p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDependencies;
} // .end method
static Integer -$$Nest$fgetmLatestPacketFailPercentage ( com.android.server.wifi.MiuiTcpSocketTracker p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mLatestPacketFailPercentage:I */
} // .end method
static void -$$Nest$fputmMinPacketsThreshold ( com.android.server.wifi.MiuiTcpSocketTracker p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mMinPacketsThreshold:I */
return;
} // .end method
static void -$$Nest$fputmTcpPacketsFailRateThreshold ( com.android.server.wifi.MiuiTcpSocketTracker p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mTcpPacketsFailRateThreshold:I */
return;
} // .end method
static com.android.server.wifi.MiuiTcpSocketTracker ( ) {
/* .locals 2 */
/* .line 75 */
/* filled-new-array {v0, v1}, [I */
return;
} // .end method
public com.android.server.wifi.MiuiTcpSocketTracker ( ) {
/* .locals 17 */
/* .param p1, "dps" # Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies; */
/* .param p2, "network" # Landroid/net/Network; */
/* .line 157 */
/* move-object/from16 v0, p0 */
/* invoke-direct/range {p0 ..p0}, Ljava/lang/Object;-><init>()V */
/* .line 76 */
/* new-instance v1, Landroid/util/SparseArray; */
/* invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V */
this.mSockDiagMsg = v1;
/* .line 117 */
/* const/16 v1, 0xa */
/* iput v1, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mMinPacketsThreshold:I */
/* .line 118 */
/* const/16 v1, 0x50 */
/* iput v1, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mTcpPacketsFailRateThreshold:I */
/* .line 127 */
/* new-instance v1, Landroid/util/LongSparseArray; */
/* invoke-direct {v1}, Landroid/util/LongSparseArray;-><init>()V */
this.mSocketInfos = v1;
/* .line 128 */
/* new-instance v1, Landroid/util/LongSparseArray; */
/* invoke-direct {v1}, Landroid/util/LongSparseArray;-><init>()V */
this.mToRemovedSocketInfos = v1;
/* .line 132 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mLatestPacketFailPercentage:I */
/* .line 140 */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
java.util.Collections .synchronizedSet ( v2 );
this.mSkipUidList = v2;
/* .line 142 */
/* new-instance v2, Lcom/android/server/wifi/MiuiTcpSocketTracker$1; */
/* invoke-direct {v2, v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$1;-><init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;)V */
this.mConfigListener = v2;
/* .line 158 */
/* move-object/from16 v2, p1 */
this.mDependencies = v2;
/* .line 159 */
/* move-object/from16 v3, p2 */
this.mNetwork = v3;
/* .line 160 */
/* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->getNetd()Landroid/net/INetd; */
this.mNetd = v4;
/* .line 163 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->getNetworkMarkMask()Landroid/net/MarkMaskParcel; */
/* .line 164 */
/* .local v4, "parcel":Landroid/net/MarkMaskParcel; */
if ( v4 != null) { // if-eqz v4, :cond_0
/* iget v5, v4, Landroid/net/MarkMaskParcel;->mark:I */
} // :cond_0
int v5 = -1; // const/4 v5, -0x1
} // :goto_0
/* iput v5, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mNetworkMark:I */
/* .line 165 */
if ( v4 != null) { // if-eqz v4, :cond_1
/* iget v5, v4, Landroid/net/MarkMaskParcel;->mask:I */
} // :cond_1
/* move v5, v1 */
} // :goto_1
/* iput v5, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mNetworkMask:I */
/* .line 169 */
v5 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->isTcpInfoParsingSupported()Z */
/* if-nez v5, :cond_2 */
return;
/* .line 171 */
} // :cond_2
v5 = com.android.server.wifi.MiuiTcpSocketTracker.ADDRESS_FAMILIES;
/* array-length v6, v5 */
} // :goto_2
/* if-ge v1, v6, :cond_3 */
/* aget v15, v5, v1 */
/* .line 172 */
/* .local v15, "family":I */
v14 = this.mSockDiagMsg;
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
/* const/16 v11, 0x301 */
int v12 = 0; // const/4 v12, 0x0
int v13 = 2; // const/4 v13, 0x2
/* const/16 v16, 0xe */
/* .line 174 */
/* move v10, v15 */
/* move-object v2, v14 */
/* move/from16 v14, v16 */
/* invoke-static/range {v7 ..v14}, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagReqV2(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISIII)[B */
/* .line 172 */
(( android.util.SparseArray ) v2 ).put ( v15, v7 ); // invoke-virtual {v2, v15, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 171 */
} // .end local v15 # "family":I
/* add-int/lit8 v1, v1, 0x1 */
/* move-object/from16 v2, p1 */
/* .line 183 */
} // :cond_3
v1 = this.mDependencies;
v2 = this.mConfigListener;
(( com.android.server.wifi.MiuiTcpSocketTracker$Dependencies ) v1 ).addDeviceConfigChangedListener ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->addDeviceConfigChangedListener(Landroid/provider/DeviceConfig$OnPropertiesChangedListener;)V
/* .line 184 */
return;
} // .end method
private com.android.server.wifi.MiuiTcpSocketTracker$TcpStat calculateLatestPacketsStat ( com.android.server.wifi.MiuiTcpSocketTracker$SocketInfo p0, Long p1, Integer p2 ) {
/* .locals 6 */
/* .param p1, "current" # Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo; */
/* .param p2, "cookies" # J */
/* .param p4, "nlmsgUid" # I */
/* .line 410 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat; */
/* invoke-direct {v0, p0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;-><init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;)V */
/* .line 411 */
/* .local v0, "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat; */
v1 = this.mSocketInfos;
(( android.util.LongSparseArray ) v1 ).get ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo; */
/* .line 413 */
/* .local v1, "previous":Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo; */
/* iget v2, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->fwmark:I */
/* iget v3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mNetworkMask:I */
/* and-int/2addr v2, v3 */
/* iget v3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mNetworkMark:I */
int v4 = 0; // const/4 v4, 0x0
final String v5 = "MiuiTcpSocketTracker"; // const-string v5, "MiuiTcpSocketTracker"
/* if-eq v2, v3, :cond_0 */
/* .line 414 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "skip mismatch tcpInfo:" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.tcpInfo;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v5,v2 );
/* .line 415 */
/* .line 418 */
} // :cond_0
v2 = this.tcpInfo;
/* if-nez v2, :cond_1 */
/* .line 419 */
final String v2 = "Current tcpInfo is null."; // const-string v2, "Current tcpInfo is null."
android.util.Log .d ( v5,v2 );
/* .line 420 */
/* .line 423 */
} // :cond_1
v2 = this.tcpInfo;
/* iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsOut:I */
/* iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I */
/* .line 424 */
v2 = this.tcpInfo;
/* iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mLost:I */
/* iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I */
/* .line 425 */
v2 = this.tcpInfo;
/* iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mRetans:I */
/* iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I */
/* .line 426 */
v2 = this.tcpInfo;
/* iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mRetransmits:I */
/* iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I */
/* .line 427 */
v2 = this.tcpInfo;
/* iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mUnacked:I */
/* iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I */
/* .line 428 */
v2 = this.tcpInfo;
/* iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mTotalRetrans:I */
/* iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I */
/* .line 429 */
v2 = this.tcpInfo;
/* iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsIn:I */
/* iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I */
/* .line 430 */
v2 = this.tcpInfo;
/* iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mRtt:I */
/* iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRtt:I */
/* .line 431 */
v2 = this.tcpInfo;
/* iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mRcvRtt:I */
/* iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRcvRtt:I */
/* .line 432 */
v2 = this.tcpInfo;
/* iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mRttVar:I */
/* iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRttVar:I */
/* .line 433 */
v2 = this.tcpInfo;
/* iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mMinRtt:I */
/* iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->minRtt:I */
/* .line 434 */
if ( v1 != null) { // if-eqz v1, :cond_2
v2 = this.tcpInfo;
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 435 */
/* iget v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I */
v3 = this.tcpInfo;
/* iget v3, v3, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsOut:I */
/* sub-int/2addr v2, v3 */
/* iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I */
/* .line 436 */
/* iget v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I */
v3 = this.tcpInfo;
/* iget v3, v3, Lcom/android/server/wifi/MiuiTcpInfo;->mTotalRetrans:I */
/* sub-int/2addr v2, v3 */
/* iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I */
/* .line 437 */
/* iget v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I */
v3 = this.tcpInfo;
/* iget v3, v3, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsIn:I */
/* sub-int/2addr v2, v3 */
/* iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I */
/* .line 439 */
} // :cond_2
/* iget v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I */
/* const/16 v3, 0x3e8 */
/* if-le v2, v3, :cond_3 */
v2 = this.mSkipUidList;
v2 = java.lang.Integer .valueOf ( p4 );
/* if-nez v2, :cond_3 */
/* .line 440 */
v2 = this.mSkipUidList;
java.lang.Integer .valueOf ( p4 );
/* .line 441 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p4 ); // invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " sentCound is greater than 1000 and is "; // const-string v3, " sentCound is greater than 1000 and is "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v5,v2 );
/* .line 443 */
} // :cond_3
} // .end method
private void cleanupSocketInfo ( Long p0 ) {
/* .locals 10 */
/* .param p1, "time" # J */
/* .line 322 */
v0 = this.mSocketInfos;
v0 = (( android.util.LongSparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I
/* .line 323 */
/* .local v0, "size":I */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 324 */
/* .local v1, "toRemove":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v0, :cond_1 */
/* .line 325 */
v3 = this.mSocketInfos;
(( android.util.LongSparseArray ) v3 ).keyAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/LongSparseArray;->keyAt(I)J
/* move-result-wide v3 */
/* .line 326 */
/* .local v3, "key":J */
v5 = this.mSocketInfos;
(( android.util.LongSparseArray ) v5 ).get ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo; */
/* iget-wide v5, v5, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->updateTime:J */
/* cmp-long v5, v5, p1 */
/* if-gez v5, :cond_0 */
/* .line 327 */
java.lang.Long .valueOf ( v3,v4 );
/* .line 324 */
} // .end local v3 # "key":J
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 330 */
} // .end local v2 # "i":I
} // :cond_1
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Ljava/lang/Long; */
/* .line 331 */
/* .local v3, "key":Ljava/lang/Long; */
v4 = this.mToRemovedSocketInfos;
(( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
/* move-result-wide v5 */
v7 = this.mSocketInfos;
(( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
/* move-result-wide v8 */
(( android.util.LongSparseArray ) v7 ).get ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;
/* check-cast v7, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo; */
(( android.util.LongSparseArray ) v4 ).put ( v5, v6, v7 ); // invoke-virtual {v4, v5, v6, v7}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
/* .line 332 */
v4 = this.mSocketInfos;
(( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
/* move-result-wide v5 */
(( android.util.LongSparseArray ) v4 ).remove ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Landroid/util/LongSparseArray;->remove(J)V
/* .line 333 */
} // .end local v3 # "key":Ljava/lang/Long;
/* .line 334 */
} // :cond_2
return;
} // .end method
public static void closeSocketQuietly ( java.io.FileDescriptor p0 ) {
/* .locals 1 */
/* .param p0, "fd" # Ljava/io/FileDescriptor; */
/* .line 316 */
try { // :try_start_0
android.net.util.SocketUtils .closeSocket ( p0 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 318 */
/* .line 317 */
/* :catch_0 */
/* move-exception v0 */
/* .line 319 */
} // :goto_0
return;
} // .end method
static Boolean enoughBytesRemainForValidNlMsg ( java.nio.ByteBuffer p0 ) {
/* .locals 2 */
/* .param p0, "bytes" # Ljava/nio/ByteBuffer; */
/* .line 380 */
v0 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
/* const/16 v1, 0x10 */
/* if-lt v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Integer getMinPacketsThreshold ( ) {
/* .locals 1 */
/* .line 462 */
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mMinPacketsThreshold:I */
} // .end method
private android.net.MarkMaskParcel getNetworkMarkMask ( ) {
/* .locals 3 */
/* .line 190 */
try { // :try_start_0
v0 = this.mNetwork;
v0 = (( android.net.Network ) v0 ).getNetId ( ); // invoke-virtual {v0}, Landroid/net/Network;->getNetId()I
/* .line 191 */
/* .local v0, "netId":I */
v1 = this.mNetd;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 192 */
} // .end local v0 # "netId":I
/* :catch_0 */
/* move-exception v0 */
/* .line 193 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "MiuiTcpSocketTracker"; // const-string v1, "MiuiTcpSocketTracker"
final String v2 = "Get netId is not available in this API level, "; // const-string v2, "Get netId is not available in this API level, "
android.util.Log .e ( v1,v2,v0 );
/* .line 195 */
} // .end local v0 # "e":Ljava/lang/Exception;
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Integer getTcpPacketsFailRateThreshold ( ) {
/* .locals 1 */
/* .line 466 */
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mTcpPacketsFailRateThreshold:I */
} // .end method
private static Boolean isValidInetDiagMsgSize ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "nlMsgLen" # I */
/* .line 384 */
/* const/16 v0, 0x58 */
/* if-lt p0, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private void skipRemainingAttributesBytesAligned ( java.nio.ByteBuffer p0, Object p1 ) {
/* .locals 2 */
/* .param p1, "buffer" # Ljava/nio/ByteBuffer; */
/* .param p2, "len" # S */
/* .line 490 */
v0 = (( java.nio.ByteBuffer ) p1 ).position ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I
/* .line 491 */
/* .local v0, "cur":I */
v1 = com.android.net.module.util.netlink.NetlinkConstants .alignedLengthOf ( p2 );
/* add-int/2addr v1, v0 */
(( java.nio.ByteBuffer ) p1 ).position ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 492 */
return;
} // .end method
/* # virtual methods */
public com.android.server.wifi.MiuiTcpSocketTracker$TcpStat getInvalidTcpInfo ( ) {
/* .locals 7 */
/* .line 337 */
v0 = this.mToRemovedSocketInfos;
v0 = (( android.util.LongSparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I
/* .line 338 */
/* .local v0, "size":I */
/* new-instance v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat; */
/* invoke-direct {v1, p0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;-><init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;)V */
/* .line 339 */
/* .local v1, "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v0, :cond_0 */
/* .line 340 */
v3 = this.mToRemovedSocketInfos;
(( android.util.LongSparseArray ) v3 ).keyAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/LongSparseArray;->keyAt(I)J
/* move-result-wide v3 */
/* .line 341 */
/* .local v3, "key":J */
/* iget v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I */
v6 = this.mToRemovedSocketInfos;
(( android.util.LongSparseArray ) v6 ).get ( v3, v4 ); // invoke-virtual {v6, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo; */
v6 = this.tcpInfo;
/* iget v6, v6, Lcom/android/server/wifi/MiuiTcpInfo;->mRetransmits:I */
/* add-int/2addr v5, v6 */
/* iput v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I */
/* .line 342 */
/* iget v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I */
v6 = this.mToRemovedSocketInfos;
(( android.util.LongSparseArray ) v6 ).get ( v3, v4 ); // invoke-virtual {v6, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo; */
v6 = this.tcpInfo;
/* iget v6, v6, Lcom/android/server/wifi/MiuiTcpInfo;->mUnacked:I */
/* add-int/2addr v5, v6 */
/* iput v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I */
/* .line 343 */
/* iget v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I */
v6 = this.mToRemovedSocketInfos;
(( android.util.LongSparseArray ) v6 ).get ( v3, v4 ); // invoke-virtual {v6, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo; */
v6 = this.tcpInfo;
/* iget v6, v6, Lcom/android/server/wifi/MiuiTcpInfo;->mRetans:I */
/* add-int/2addr v5, v6 */
/* iput v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I */
/* .line 344 */
/* iget v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I */
v6 = this.mToRemovedSocketInfos;
(( android.util.LongSparseArray ) v6 ).get ( v3, v4 ); // invoke-virtual {v6, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo; */
v6 = this.tcpInfo;
/* iget v6, v6, Lcom/android/server/wifi/MiuiTcpInfo;->mLost:I */
/* add-int/2addr v5, v6 */
/* iput v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I */
/* .line 339 */
} // .end local v3 # "key":J
/* add-int/lit8 v2, v2, 0x1 */
/* .line 346 */
} // .end local v2 # "i":I
} // :cond_0
} // .end method
public Integer getLatestPacketFailPercentage ( ) {
/* .locals 1 */
/* .line 393 */
v0 = this.mDependencies;
v0 = (( com.android.server.wifi.MiuiTcpSocketTracker$Dependencies ) v0 ).isTcpInfoParsingSupported ( ); // invoke-virtual {v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->isTcpInfoParsingSupported()Z
/* if-nez v0, :cond_0 */
int v0 = -1; // const/4 v0, -0x1
/* .line 395 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mLatestPacketFailPercentage:I */
} // .end method
public Integer getLatestReceivedCount ( ) {
/* .locals 1 */
/* .line 457 */
v0 = this.mDependencies;
v0 = (( com.android.server.wifi.MiuiTcpSocketTracker$Dependencies ) v0 ).isTcpInfoParsingSupported ( ); // invoke-virtual {v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->isTcpInfoParsingSupported()Z
/* if-nez v0, :cond_0 */
int v0 = -1; // const/4 v0, -0x1
/* .line 458 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mLatestReceivedCount:I */
} // .end method
public com.android.server.wifi.MiuiTcpSocketTracker$TcpStat getLatestTcpStats ( ) {
/* .locals 1 */
/* .line 399 */
v0 = this.mLatestTcpStats;
} // .end method
public Integer getSentSinceLastRecv ( ) {
/* .locals 1 */
/* .line 451 */
v0 = this.mDependencies;
v0 = (( com.android.server.wifi.MiuiTcpSocketTracker$Dependencies ) v0 ).isTcpInfoParsingSupported ( ); // invoke-virtual {v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->isTcpInfoParsingSupported()Z
/* if-nez v0, :cond_0 */
int v0 = -1; // const/4 v0, -0x1
/* .line 452 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSentSinceLastRecv:I */
} // .end method
public java.util.Set getSkipUidList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 403 */
v0 = this.mSkipUidList;
} // .end method
public java.lang.String getTcpInfo ( ) {
/* .locals 1 */
/* .line 311 */
v0 = this.msgRecord;
} // .end method
com.android.server.wifi.MiuiTcpSocketTracker$SocketInfo parseSockInfo ( java.nio.ByteBuffer p0, Integer p1, Integer p2, Long p3 ) {
/* .locals 13 */
/* .param p1, "bytes" # Ljava/nio/ByteBuffer; */
/* .param p2, "family" # I */
/* .param p3, "nlmsgLen" # I */
/* .param p4, "time" # J */
/* .line 353 */
/* move-object v7, p0 */
/* move-object v8, p1 */
v0 = (( java.nio.ByteBuffer ) p1 ).position ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I
/* add-int v0, v0, p3 */
/* add-int/lit8 v9, v0, -0x58 */
/* .line 354 */
/* .local v9, "remainingDataSize":I */
int v0 = 0; // const/4 v0, 0x0
/* .line 355 */
/* .local v0, "tcpInfo":Lcom/android/server/wifi/MiuiTcpInfo; */
int v1 = 0; // const/4 v1, 0x0
/* move-object v10, v0 */
/* move v11, v1 */
/* .line 357 */
} // .end local v0 # "tcpInfo":Lcom/android/server/wifi/MiuiTcpInfo;
/* .local v10, "tcpInfo":Lcom/android/server/wifi/MiuiTcpInfo; */
/* .local v11, "mark":I */
} // :goto_0
v0 = (( java.nio.ByteBuffer ) p1 ).position ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I
/* if-ge v0, v9, :cond_2 */
/* .line 358 */
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute; */
/* .line 359 */
v1 = (( java.nio.ByteBuffer ) p1 ).getShort ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S
v2 = (( java.nio.ByteBuffer ) p1 ).getShort ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S
/* invoke-direct {v0, p0, v1, v2}, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;-><init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;SS)V */
/* .line 360 */
/* .local v0, "rtattr":Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute; */
v1 = (( com.android.server.wifi.MiuiTcpSocketTracker$RoutingAttribute ) v0 ).getDataLength ( ); // invoke-virtual {v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;->getDataLength()S
/* .line 361 */
/* .local v1, "dataLen":S */
/* iget-short v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;->rtaType:S */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_0 */
/* .line 362 */
com.android.server.wifi.MiuiTcpInfo .parse ( p1,v1 );
/* move-object v10, v2 */
} // .end local v10 # "tcpInfo":Lcom/android/server/wifi/MiuiTcpInfo;
/* .local v2, "tcpInfo":Lcom/android/server/wifi/MiuiTcpInfo; */
/* .line 363 */
} // .end local v2 # "tcpInfo":Lcom/android/server/wifi/MiuiTcpInfo;
/* .restart local v10 # "tcpInfo":Lcom/android/server/wifi/MiuiTcpInfo; */
} // :cond_0
/* iget-short v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;->rtaType:S */
/* const/16 v3, 0xf */
/* if-ne v2, v3, :cond_1 */
/* .line 364 */
v2 = (( java.nio.ByteBuffer ) p1 ).getInt ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I
/* move v11, v2 */
} // .end local v11 # "mark":I
/* .local v2, "mark":I */
/* .line 369 */
} // .end local v2 # "mark":I
/* .restart local v11 # "mark":I */
} // :cond_1
/* invoke-direct {p0, p1, v1}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->skipRemainingAttributesBytesAligned(Ljava/nio/ByteBuffer;S)V */
/* .line 371 */
} // .end local v0 # "rtattr":Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;
} // .end local v1 # "dataLen":S
} // :goto_1
/* .line 373 */
} // :cond_2
/* new-instance v12, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo; */
/* move-object v0, v12 */
/* move-object v1, p0 */
/* move-object v2, v10 */
/* move v3, p2 */
/* move v4, v11 */
/* move-wide/from16 v5, p4 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;-><init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;Lcom/android/server/wifi/MiuiTcpInfo;IIJ)V */
/* .line 374 */
/* .local v0, "info":Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo; */
} // .end method
public Boolean pollSocketsInfo ( java.util.Set p0 ) {
/* .locals 24 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 210 */
/* .local p1, "uidList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p1 */
v0 = this.mDependencies;
v0 = (( com.android.server.wifi.MiuiTcpSocketTracker$Dependencies ) v0 ).isTcpInfoParsingSupported ( ); // invoke-virtual {v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->isTcpInfoParsingSupported()Z
int v9 = 0; // const/4 v9, 0x0
/* if-nez v0, :cond_0 */
/* .line 211 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 212 */
/* .local v1, "readBytes":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 213 */
/* .local v2, "fd":Ljava/io/FileDescriptor; */
v0 = this.mSkipUidList;
/* .line 214 */
/* .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
v0 = } // :goto_0
final String v11 = "MiuiTcpSocketTracker"; // const-string v11, "MiuiTcpSocketTracker"
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 215 */
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 216 */
/* .local v0, "uid":I */
v3 = java.lang.Integer .valueOf ( v0 );
/* if-nez v3, :cond_1 */
/* .line 217 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "pollSocketsInfo remove old skip uid is "; // const-string v4, "pollSocketsInfo remove old skip uid is "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v11,v3 );
/* .line 218 */
/* .line 220 */
} // .end local v0 # "uid":I
} // :cond_1
/* .line 221 */
} // :cond_2
v0 = this.mToRemovedSocketInfos;
(( android.util.LongSparseArray ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V
/* .line 223 */
try { // :try_start_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v3 */
/* move-wide v12, v3 */
/* .line 224 */
/* .local v12, "time":J */
v0 = this.mDependencies;
(( com.android.server.wifi.MiuiTcpSocketTracker$Dependencies ) v0 ).connectToKernel ( ); // invoke-virtual {v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->connectToKernel()Ljava/io/FileDescriptor;
/* :try_end_0 */
/* .catch Landroid/system/ErrnoException; {:try_start_0 ..:try_end_0} :catch_e */
/* .catch Ljava/net/SocketException; {:try_start_0 ..:try_end_0} :catch_e */
/* .catch Ljava/io/InterruptedIOException; {:try_start_0 ..:try_end_0} :catch_e */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_7 */
/* move-object v14, v0 */
/* .line 225 */
} // .end local v2 # "fd":Ljava/io/FileDescriptor;
/* .local v14, "fd":Ljava/io/FileDescriptor; */
try { // :try_start_1
/* new-instance v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat; */
/* invoke-direct {v0, v7}, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;-><init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;)V */
/* move-object v15, v0 */
/* .line 226 */
/* .local v15, "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat; */
v5 = com.android.server.wifi.MiuiTcpSocketTracker.ADDRESS_FAMILIES;
/* array-length v6, v5 */
/* move v3, v9 */
} // :goto_1
/* if-ge v3, v6, :cond_a */
/* aget v0, v5, v3 */
/* move v2, v0 */
/* .line 227 */
/* .local v2, "family":I */
v0 = this.mDependencies;
v4 = this.mSockDiagMsg;
(( android.util.SparseArray ) v4 ).get ( v2 ); // invoke-virtual {v4, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v4, [B */
(( com.android.server.wifi.MiuiTcpSocketTracker$Dependencies ) v0 ).sendPollingRequest ( v14, v4 ); // invoke-virtual {v0, v14, v4}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->sendPollingRequest(Ljava/io/FileDescriptor;[B)V
/* .line 240 */
} // :goto_2
v0 = this.mDependencies;
(( com.android.server.wifi.MiuiTcpSocketTracker$Dependencies ) v0 ).recvMessage ( v14 ); // invoke-virtual {v0, v14}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->recvMessage(Ljava/io/FileDescriptor;)Ljava/nio/ByteBuffer;
/* move-object v4, v0 */
/* .line 241 */
/* .local v4, "bytes":Ljava/nio/ByteBuffer; */
v0 = (( java.nio.ByteBuffer ) v4 ).limit ( ); // invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I
/* :try_end_1 */
/* .catch Landroid/system/ErrnoException; {:try_start_1 ..:try_end_1} :catch_d */
/* .catch Ljava/net/SocketException; {:try_start_1 ..:try_end_1} :catch_d */
/* .catch Ljava/io/InterruptedIOException; {:try_start_1 ..:try_end_1} :catch_d */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_6 */
/* move v1, v0 */
/* .line 243 */
} // :goto_3
try { // :try_start_2
v0 = com.android.server.wifi.MiuiTcpSocketTracker .enoughBytesRemainForValidNlMsg ( v4 );
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 244 */
com.android.net.module.util.netlink.StructNlMsgHdr .parse ( v4 );
/* :try_end_2 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_2 ..:try_end_2} :catch_a */
/* .catch Ljava/nio/BufferUnderflowException; {:try_start_2 ..:try_end_2} :catch_a */
/* .catch Landroid/system/ErrnoException; {:try_start_2 ..:try_end_2} :catch_9 */
/* .catch Ljava/net/SocketException; {:try_start_2 ..:try_end_2} :catch_9 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_2 ..:try_end_2} :catch_9 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_3 */
/* .line 245 */
/* .local v0, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* if-nez v0, :cond_3 */
/* .line 246 */
try { // :try_start_3
final String v9 = "Badly formatted data."; // const-string v9, "Badly formatted data."
android.util.Log .e ( v11,v9 );
/* :try_end_3 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catch Ljava/nio/BufferUnderflowException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catch Landroid/system/ErrnoException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catch Ljava/net/SocketException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 247 */
/* move/from16 v17, v2 */
/* move/from16 v19, v3 */
/* move-object v9, v4 */
/* move-object/from16 v22, v5 */
/* move/from16 v23, v6 */
/* move-object/from16 v16, v10 */
/* move v10, v1 */
/* goto/16 :goto_5 */
/* .line 305 */
} // .end local v0 # "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
} // .end local v2 # "family":I
} // .end local v4 # "bytes":Ljava/nio/ByteBuffer;
} // .end local v12 # "time":J
} // .end local v15 # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
/* :catchall_0 */
/* move-exception v0 */
/* move-object/from16 v16, v10 */
/* move-object v2, v14 */
/* goto/16 :goto_9 */
/* .line 302 */
/* :catch_0 */
/* move-exception v0 */
/* move-object/from16 v16, v10 */
/* move-object v2, v14 */
/* goto/16 :goto_8 */
/* .line 288 */
/* .restart local v2 # "family":I */
/* .restart local v4 # "bytes":Ljava/nio/ByteBuffer; */
/* .restart local v12 # "time":J */
/* .restart local v15 # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat; */
/* :catch_1 */
/* move-exception v0 */
/* move/from16 v17, v2 */
/* move/from16 v19, v3 */
/* move-object v9, v4 */
/* move-object/from16 v22, v5 */
/* move/from16 v23, v6 */
/* move-object/from16 v16, v10 */
/* move v10, v1 */
/* goto/16 :goto_6 */
/* .line 249 */
/* .restart local v0 # "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
} // :cond_3
try { // :try_start_4
/* iget v9, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I */
/* :try_end_4 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_4 ..:try_end_4} :catch_a */
/* .catch Ljava/nio/BufferUnderflowException; {:try_start_4 ..:try_end_4} :catch_a */
/* .catch Landroid/system/ErrnoException; {:try_start_4 ..:try_end_4} :catch_9 */
/* .catch Ljava/net/SocketException; {:try_start_4 ..:try_end_4} :catch_9 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_4 ..:try_end_4} :catch_9 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_3 */
/* move-object/from16 v16, v4 */
} // .end local v4 # "bytes":Ljava/nio/ByteBuffer;
/* .local v16, "bytes":Ljava/nio/ByteBuffer; */
/* move v4, v9 */
/* .line 251 */
/* .local v4, "nlmsgLen":I */
try { // :try_start_5
/* iget-short v9, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
/* :try_end_5 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_5 ..:try_end_5} :catch_8 */
/* .catch Ljava/nio/BufferUnderflowException; {:try_start_5 ..:try_end_5} :catch_8 */
/* .catch Landroid/system/ErrnoException; {:try_start_5 ..:try_end_5} :catch_9 */
/* .catch Ljava/net/SocketException; {:try_start_5 ..:try_end_5} :catch_9 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_5 ..:try_end_5} :catch_9 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_3 */
/* move/from16 v17, v1 */
} // .end local v1 # "readBytes":I
/* .local v17, "readBytes":I */
int v1 = 3; // const/4 v1, 0x3
/* if-ne v9, v1, :cond_4 */
/* .line 252 */
/* move/from16 v19, v3 */
/* move-object/from16 v22, v5 */
/* move/from16 v23, v6 */
/* move-object/from16 v9, v16 */
/* move-object/from16 v16, v10 */
/* move/from16 v10, v17 */
/* move/from16 v17, v2 */
/* goto/16 :goto_5 */
/* .line 255 */
} // :cond_4
try { // :try_start_6
/* iget-short v1, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
/* :try_end_6 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_6 ..:try_end_6} :catch_7 */
/* .catch Ljava/nio/BufferUnderflowException; {:try_start_6 ..:try_end_6} :catch_7 */
/* .catch Landroid/system/ErrnoException; {:try_start_6 ..:try_end_6} :catch_6 */
/* .catch Ljava/net/SocketException; {:try_start_6 ..:try_end_6} :catch_6 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_6 ..:try_end_6} :catch_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
/* const/16 v9, 0x14 */
/* if-eq v1, v9, :cond_5 */
/* .line 256 */
try { // :try_start_7
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Expect to get family "; // const-string v9, "Expect to get family "
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = " SOCK_DIAG_BY_FAMILY message but get "; // const-string v9, " SOCK_DIAG_BY_FAMILY message but get "
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v9, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v11,v1 );
/* :try_end_7 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_7 ..:try_end_7} :catch_3 */
/* .catch Ljava/nio/BufferUnderflowException; {:try_start_7 ..:try_end_7} :catch_3 */
/* .catch Landroid/system/ErrnoException; {:try_start_7 ..:try_end_7} :catch_2 */
/* .catch Ljava/net/SocketException; {:try_start_7 ..:try_end_7} :catch_2 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_7 ..:try_end_7} :catch_2 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_1 */
/* .line 259 */
/* move/from16 v19, v3 */
/* move-object/from16 v22, v5 */
/* move/from16 v23, v6 */
/* move-object/from16 v9, v16 */
/* move-object/from16 v16, v10 */
/* move/from16 v10, v17 */
/* move/from16 v17, v2 */
/* goto/16 :goto_5 */
/* .line 305 */
} // .end local v0 # "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
} // .end local v2 # "family":I
} // .end local v4 # "nlmsgLen":I
} // .end local v12 # "time":J
} // .end local v15 # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
} // .end local v16 # "bytes":Ljava/nio/ByteBuffer;
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v16, v10 */
/* move-object v2, v14 */
/* move/from16 v1, v17 */
/* goto/16 :goto_9 */
/* .line 302 */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v16, v10 */
/* move-object v2, v14 */
/* move/from16 v1, v17 */
/* goto/16 :goto_8 */
/* .line 288 */
/* .restart local v2 # "family":I */
/* .restart local v12 # "time":J */
/* .restart local v15 # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat; */
/* .restart local v16 # "bytes":Ljava/nio/ByteBuffer; */
/* :catch_3 */
/* move-exception v0 */
/* move/from16 v19, v3 */
/* move-object/from16 v22, v5 */
/* move/from16 v23, v6 */
/* move-object/from16 v9, v16 */
/* move-object/from16 v16, v10 */
/* move/from16 v10, v17 */
/* move/from16 v17, v2 */
/* goto/16 :goto_6 */
/* .line 262 */
/* .restart local v0 # "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* .restart local v4 # "nlmsgLen":I */
} // :cond_5
try { // :try_start_8
v1 = com.android.server.wifi.MiuiTcpSocketTracker .isValidInetDiagMsgSize ( v4 );
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 266 */
v1 = /* invoke-virtual/range {v16 ..v16}, Ljava/nio/ByteBuffer;->position()I */
/* :try_end_8 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_8 ..:try_end_8} :catch_7 */
/* .catch Ljava/nio/BufferUnderflowException; {:try_start_8 ..:try_end_8} :catch_7 */
/* .catch Landroid/system/ErrnoException; {:try_start_8 ..:try_end_8} :catch_6 */
/* .catch Ljava/net/SocketException; {:try_start_8 ..:try_end_8} :catch_6 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_8 ..:try_end_8} :catch_6 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_2 */
/* add-int/lit8 v1, v1, 0x2c */
/* move-object/from16 v9, v16 */
} // .end local v16 # "bytes":Ljava/nio/ByteBuffer;
/* .local v9, "bytes":Ljava/nio/ByteBuffer; */
try { // :try_start_9
(( java.nio.ByteBuffer ) v9 ).position ( v1 ); // invoke-virtual {v9, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 268 */
(( java.nio.ByteBuffer ) v9 ).getLong ( ); // invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getLong()J
/* move-result-wide v18 */
/* move-wide/from16 v20, v18 */
/* .line 269 */
/* .local v20, "cookie":J */
v1 = (( java.nio.ByteBuffer ) v9 ).position ( ); // invoke-virtual {v9}, Ljava/nio/ByteBuffer;->position()I
/* add-int/lit8 v1, v1, 0xc */
(( java.nio.ByteBuffer ) v9 ).position ( v1 ); // invoke-virtual {v9, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 270 */
v1 = (( java.nio.ByteBuffer ) v9 ).getInt ( ); // invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I
/* .line 273 */
/* .local v1, "nlmsgUid":I */
v16 = (( java.nio.ByteBuffer ) v9 ).position ( ); // invoke-virtual {v9}, Ljava/nio/ByteBuffer;->position()I
/* add-int/lit8 v16, v16, 0x48 */
/* add-int/lit8 v16, v16, -0x2c */
/* add-int/lit8 v16, v16, -0x8 */
/* add-int/lit8 v16, v16, -0xc */
/* move-object/from16 v18, v0 */
} // .end local v0 # "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
/* .local v18, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* add-int/lit8 v0, v16, -0x4 */
(( java.nio.ByteBuffer ) v9 ).position ( v0 ); // invoke-virtual {v9, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* :try_end_9 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_9 ..:try_end_9} :catch_5 */
/* .catch Ljava/nio/BufferUnderflowException; {:try_start_9 ..:try_end_9} :catch_5 */
/* .catch Landroid/system/ErrnoException; {:try_start_9 ..:try_end_9} :catch_6 */
/* .catch Ljava/net/SocketException; {:try_start_9 ..:try_end_9} :catch_6 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_9 ..:try_end_9} :catch_6 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_2 */
/* .line 277 */
/* move v0, v1 */
/* move-object/from16 v16, v10 */
/* move/from16 v10, v17 */
} // .end local v1 # "nlmsgUid":I
} // .end local v17 # "readBytes":I
/* .local v0, "nlmsgUid":I */
/* .local v10, "readBytes":I */
/* .local v16, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* move-object/from16 v1, p0 */
/* move/from16 v17, v2 */
} // .end local v2 # "family":I
/* .local v17, "family":I */
/* move-object v2, v9 */
/* move/from16 v19, v3 */
/* move/from16 v3, v17 */
/* move-object/from16 v22, v5 */
/* move/from16 v23, v6 */
/* move-wide v5, v12 */
try { // :try_start_a
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->parseSockInfo(Ljava/nio/ByteBuffer;IIJ)Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo; */
/* .line 278 */
/* .local v1, "info":Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo; */
v2 = java.lang.Integer .valueOf ( v0 );
/* if-nez v2, :cond_6 */
/* .line 279 */
/* move-object v4, v9 */
/* move v1, v10 */
/* move-object/from16 v10, v16 */
/* move/from16 v2, v17 */
/* move/from16 v3, v19 */
/* move-object/from16 v5, v22 */
/* move/from16 v6, v23 */
int v9 = 0; // const/4 v9, 0x0
/* goto/16 :goto_3 */
/* .line 282 */
} // :cond_6
/* nop */
/* .line 283 */
/* move-wide/from16 v2, v20 */
} // .end local v20 # "cookie":J
/* .local v2, "cookie":J */
/* invoke-direct {v7, v1, v2, v3, v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->calculateLatestPacketsStat(Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;JI)Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat; */
/* .line 282 */
(( com.android.server.wifi.MiuiTcpSocketTracker$TcpStat ) v15 ).accumulate ( v5 ); // invoke-virtual {v15, v5}, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->accumulate(Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;)V
/* .line 284 */
v5 = this.tcpInfo;
(( com.android.server.wifi.MiuiTcpSocketTracker$TcpStat ) v15 ).avg ( v5 ); // invoke-virtual {v15, v5}, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avg(Lcom/android/server/wifi/MiuiTcpInfo;)V
/* .line 285 */
v5 = this.mSocketInfos;
(( android.util.LongSparseArray ) v5 ).put ( v2, v3, v1 ); // invoke-virtual {v5, v2, v3, v1}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
/* :try_end_a */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_a ..:try_end_a} :catch_4 */
/* .catch Ljava/nio/BufferUnderflowException; {:try_start_a ..:try_end_a} :catch_4 */
/* .catch Landroid/system/ErrnoException; {:try_start_a ..:try_end_a} :catch_b */
/* .catch Ljava/net/SocketException; {:try_start_a ..:try_end_a} :catch_b */
/* .catch Ljava/io/InterruptedIOException; {:try_start_a ..:try_end_a} :catch_b */
/* .catchall {:try_start_a ..:try_end_a} :catchall_4 */
/* .line 288 */
} // .end local v0 # "nlmsgUid":I
} // .end local v1 # "info":Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;
} // .end local v2 # "cookie":J
} // .end local v4 # "nlmsgLen":I
} // .end local v18 # "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
/* :catch_4 */
/* move-exception v0 */
/* goto/16 :goto_6 */
} // .end local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .local v2, "family":I */
/* .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .local v17, "readBytes":I */
/* :catch_5 */
/* move-exception v0 */
/* move/from16 v19, v3 */
/* move-object/from16 v22, v5 */
/* move/from16 v23, v6 */
/* move-object/from16 v16, v10 */
/* move/from16 v10, v17 */
/* move/from16 v17, v2 */
} // .end local v2 # "family":I
/* .local v10, "readBytes":I */
/* .restart local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .local v17, "family":I */
/* goto/16 :goto_6 */
/* .line 262 */
} // .end local v9 # "bytes":Ljava/nio/ByteBuffer;
/* .local v0, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* .restart local v2 # "family":I */
/* .restart local v4 # "nlmsgLen":I */
/* .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .local v16, "bytes":Ljava/nio/ByteBuffer; */
/* .local v17, "readBytes":I */
} // :cond_7
/* move-object/from16 v18, v0 */
/* move/from16 v19, v3 */
/* move-object/from16 v22, v5 */
/* move/from16 v23, v6 */
/* move-object/from16 v9, v16 */
/* move-object/from16 v16, v10 */
/* move/from16 v10, v17 */
/* move/from16 v17, v2 */
/* .line 287 */
} // .end local v0 # "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
} // .end local v2 # "family":I
} // .end local v4 # "nlmsgLen":I
/* .restart local v9 # "bytes":Ljava/nio/ByteBuffer; */
/* .local v10, "readBytes":I */
/* .local v16, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .local v17, "family":I */
} // :goto_4
/* move-object v4, v9 */
/* move v1, v10 */
/* move-object/from16 v10, v16 */
/* move/from16 v2, v17 */
/* move/from16 v3, v19 */
/* move-object/from16 v5, v22 */
/* move/from16 v6, v23 */
int v9 = 0; // const/4 v9, 0x0
/* goto/16 :goto_3 */
/* .line 305 */
} // .end local v9 # "bytes":Ljava/nio/ByteBuffer;
} // .end local v12 # "time":J
} // .end local v15 # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
} // .end local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .local v17, "readBytes":I */
/* :catchall_2 */
/* move-exception v0 */
/* move-object/from16 v16, v10 */
/* move/from16 v10, v17 */
/* move v1, v10 */
/* move-object v2, v14 */
} // .end local v17 # "readBytes":I
/* .local v10, "readBytes":I */
/* .restart local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* goto/16 :goto_9 */
/* .line 302 */
} // .end local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .restart local v17 # "readBytes":I */
/* :catch_6 */
/* move-exception v0 */
/* move-object/from16 v16, v10 */
/* move/from16 v10, v17 */
/* move v1, v10 */
/* move-object v2, v14 */
} // .end local v17 # "readBytes":I
/* .local v10, "readBytes":I */
/* .restart local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* goto/16 :goto_8 */
/* .line 288 */
/* .restart local v2 # "family":I */
/* .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .restart local v12 # "time":J */
/* .restart local v15 # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat; */
/* .local v16, "bytes":Ljava/nio/ByteBuffer; */
/* .restart local v17 # "readBytes":I */
/* :catch_7 */
/* move-exception v0 */
/* move/from16 v19, v3 */
/* move-object/from16 v22, v5 */
/* move/from16 v23, v6 */
/* move-object/from16 v9, v16 */
/* move-object/from16 v16, v10 */
/* move/from16 v10, v17 */
/* move/from16 v17, v2 */
} // .end local v2 # "family":I
/* .restart local v9 # "bytes":Ljava/nio/ByteBuffer; */
/* .local v10, "readBytes":I */
/* .local v16, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .local v17, "family":I */
} // .end local v9 # "bytes":Ljava/nio/ByteBuffer;
} // .end local v17 # "family":I
/* .local v1, "readBytes":I */
/* .restart local v2 # "family":I */
/* .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .local v16, "bytes":Ljava/nio/ByteBuffer; */
/* :catch_8 */
/* move-exception v0 */
/* move/from16 v17, v2 */
/* move/from16 v19, v3 */
/* move-object/from16 v22, v5 */
/* move/from16 v23, v6 */
/* move-object/from16 v9, v16 */
/* move-object/from16 v16, v10 */
/* move v10, v1 */
} // .end local v1 # "readBytes":I
} // .end local v2 # "family":I
/* .restart local v9 # "bytes":Ljava/nio/ByteBuffer; */
/* .local v10, "readBytes":I */
/* .local v16, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .restart local v17 # "family":I */
/* .line 243 */
} // .end local v9 # "bytes":Ljava/nio/ByteBuffer;
} // .end local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
} // .end local v17 # "family":I
/* .restart local v1 # "readBytes":I */
/* .restart local v2 # "family":I */
/* .local v4, "bytes":Ljava/nio/ByteBuffer; */
/* .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
} // :cond_8
/* move/from16 v17, v2 */
/* move/from16 v19, v3 */
/* move-object v9, v4 */
/* move-object/from16 v22, v5 */
/* move/from16 v23, v6 */
/* move-object/from16 v16, v10 */
/* move v10, v1 */
/* .line 292 */
} // .end local v1 # "readBytes":I
} // .end local v2 # "family":I
} // .end local v4 # "bytes":Ljava/nio/ByteBuffer;
/* .restart local v9 # "bytes":Ljava/nio/ByteBuffer; */
/* .local v10, "readBytes":I */
/* .restart local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .restart local v17 # "family":I */
} // :goto_5
/* move/from16 v2, v17 */
/* .line 305 */
} // .end local v9 # "bytes":Ljava/nio/ByteBuffer;
} // .end local v12 # "time":J
} // .end local v15 # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
} // .end local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
} // .end local v17 # "family":I
/* .restart local v1 # "readBytes":I */
/* .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* :catchall_3 */
/* move-exception v0 */
/* move-object/from16 v16, v10 */
/* move v10, v1 */
/* move-object v2, v14 */
} // .end local v1 # "readBytes":I
/* .local v10, "readBytes":I */
/* .restart local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* goto/16 :goto_9 */
/* .line 302 */
} // .end local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .restart local v1 # "readBytes":I */
/* .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* :catch_9 */
/* move-exception v0 */
/* move-object/from16 v16, v10 */
/* move v10, v1 */
/* move-object v2, v14 */
} // .end local v1 # "readBytes":I
/* .local v10, "readBytes":I */
/* .restart local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* goto/16 :goto_8 */
/* .line 288 */
} // .end local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .restart local v1 # "readBytes":I */
/* .restart local v2 # "family":I */
/* .restart local v4 # "bytes":Ljava/nio/ByteBuffer; */
/* .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .restart local v12 # "time":J */
/* .restart local v15 # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat; */
/* :catch_a */
/* move-exception v0 */
/* move/from16 v17, v2 */
/* move/from16 v19, v3 */
/* move-object v9, v4 */
/* move-object/from16 v22, v5 */
/* move/from16 v23, v6 */
/* move-object/from16 v16, v10 */
/* move v10, v1 */
/* .line 289 */
} // .end local v1 # "readBytes":I
} // .end local v2 # "family":I
} // .end local v4 # "bytes":Ljava/nio/ByteBuffer;
/* .local v0, "e":Ljava/lang/RuntimeException; */
/* .restart local v9 # "bytes":Ljava/nio/ByteBuffer; */
/* .local v10, "readBytes":I */
/* .restart local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .restart local v17 # "family":I */
} // :goto_6
try { // :try_start_b
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Unexpected socket info parsing, family "; // const-string v2, "Unexpected socket info parsing, family "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v2, v17 */
} // .end local v17 # "family":I
/* .restart local v2 # "family":I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " buffer:"; // const-string v3, " buffer:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 291 */
java.util.Base64 .getEncoder ( );
(( java.nio.ByteBuffer ) v9 ).array ( ); // invoke-virtual {v9}, Ljava/nio/ByteBuffer;->array()[B
(( java.util.Base64$Encoder ) v3 ).encodeToString ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/Base64$Encoder;->encodeToString([B)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 289 */
android.util.Log .wtf ( v11,v1,v0 );
/* :try_end_b */
/* .catch Landroid/system/ErrnoException; {:try_start_b ..:try_end_b} :catch_b */
/* .catch Ljava/net/SocketException; {:try_start_b ..:try_end_b} :catch_b */
/* .catch Ljava/io/InterruptedIOException; {:try_start_b ..:try_end_b} :catch_b */
/* .catchall {:try_start_b ..:try_end_b} :catchall_4 */
/* .line 293 */
} // .end local v0 # "e":Ljava/lang/RuntimeException;
} // .end local v9 # "bytes":Ljava/nio/ByteBuffer;
} // :goto_7
/* const/16 v1, 0x14 */
/* if-gt v10, v1, :cond_9 */
/* .line 226 */
} // .end local v2 # "family":I
/* add-int/lit8 v3, v19, 0x1 */
/* move v1, v10 */
/* move-object/from16 v10, v16 */
/* move-object/from16 v5, v22 */
/* move/from16 v6, v23 */
int v9 = 0; // const/4 v9, 0x0
/* goto/16 :goto_1 */
/* .line 293 */
/* .restart local v2 # "family":I */
} // :cond_9
/* move v1, v10 */
/* move-object/from16 v10, v16 */
/* move/from16 v3, v19 */
/* move-object/from16 v5, v22 */
/* move/from16 v6, v23 */
int v9 = 0; // const/4 v9, 0x0
/* goto/16 :goto_2 */
/* .line 305 */
} // .end local v2 # "family":I
} // .end local v12 # "time":J
} // .end local v15 # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
/* :catchall_4 */
/* move-exception v0 */
/* move v1, v10 */
/* move-object v2, v14 */
/* .line 302 */
/* :catch_b */
/* move-exception v0 */
/* move v1, v10 */
/* move-object v2, v14 */
/* .line 296 */
} // .end local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .restart local v1 # "readBytes":I */
/* .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .restart local v12 # "time":J */
/* .restart local v15 # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat; */
} // :cond_a
/* move-object/from16 v16, v10 */
} // .end local v10 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .restart local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
try { // :try_start_c
v0 = (( com.android.server.wifi.MiuiTcpSocketTracker$TcpStat ) v15 ).getFailPercent ( ); // invoke-virtual {v15}, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->getFailPercent()I
/* iput v0, v7, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mLatestPacketFailPercentage:I */
/* .line 297 */
(( com.android.server.wifi.MiuiTcpSocketTracker$TcpStat ) v15 ).printMsg ( ); // invoke-virtual {v15}, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->printMsg()Ljava/lang/String;
this.msgRecord = v0;
/* .line 298 */
this.mLatestTcpStats = v15;
/* .line 300 */
/* invoke-direct {v7, v12, v13}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->cleanupSocketInfo(J)V */
/* :try_end_c */
/* .catch Landroid/system/ErrnoException; {:try_start_c ..:try_end_c} :catch_c */
/* .catch Ljava/net/SocketException; {:try_start_c ..:try_end_c} :catch_c */
/* .catch Ljava/io/InterruptedIOException; {:try_start_c ..:try_end_c} :catch_c */
/* .catchall {:try_start_c ..:try_end_c} :catchall_5 */
/* .line 301 */
/* nop */
/* .line 305 */
com.android.server.wifi.MiuiTcpSocketTracker .closeSocketQuietly ( v14 );
/* .line 301 */
int v0 = 1; // const/4 v0, 0x1
/* .line 305 */
} // .end local v12 # "time":J
} // .end local v15 # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
/* :catchall_5 */
/* move-exception v0 */
/* move-object v2, v14 */
/* .line 302 */
/* :catch_c */
/* move-exception v0 */
/* move-object v2, v14 */
/* .line 305 */
} // .end local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .restart local v10 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* :catchall_6 */
/* move-exception v0 */
/* move-object/from16 v16, v10 */
/* move-object v2, v14 */
} // .end local v10 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .restart local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .line 302 */
} // .end local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .restart local v10 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* :catch_d */
/* move-exception v0 */
/* move-object/from16 v16, v10 */
/* move-object v2, v14 */
} // .end local v10 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .restart local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .line 305 */
} // .end local v14 # "fd":Ljava/io/FileDescriptor;
} // .end local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .local v2, "fd":Ljava/io/FileDescriptor; */
/* .restart local v10 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* :catchall_7 */
/* move-exception v0 */
/* move-object/from16 v16, v10 */
} // .end local v10 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .restart local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* .line 302 */
} // .end local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .restart local v10 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
/* :catch_e */
/* move-exception v0 */
/* move-object/from16 v16, v10 */
/* .line 303 */
} // .end local v10 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v16 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
} // :goto_8
try { // :try_start_d
final String v3 = "Fail to get TCP info via netlink."; // const-string v3, "Fail to get TCP info via netlink."
android.util.Log .e ( v11,v3,v0 );
/* :try_end_d */
/* .catchall {:try_start_d ..:try_end_d} :catchall_8 */
/* .line 305 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
com.android.server.wifi.MiuiTcpSocketTracker .closeSocketQuietly ( v2 );
/* .line 306 */
/* nop */
/* .line 307 */
int v3 = 0; // const/4 v3, 0x0
/* .line 305 */
/* :catchall_8 */
/* move-exception v0 */
} // :goto_9
com.android.server.wifi.MiuiTcpSocketTracker .closeSocketQuietly ( v2 );
/* .line 306 */
/* throw v0 */
} // .end method
public void quit ( ) {
/* .locals 2 */
/* .line 200 */
v0 = this.mDependencies;
v1 = this.mConfigListener;
(( com.android.server.wifi.MiuiTcpSocketTracker$Dependencies ) v0 ).removeDeviceConfigChangedListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->removeDeviceConfigChangedListener(Landroid/provider/DeviceConfig$OnPropertiesChangedListener;)V
/* .line 201 */
return;
} // .end method
