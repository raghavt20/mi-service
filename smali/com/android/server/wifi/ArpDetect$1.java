class com.android.server.wifi.ArpDetect$1 implements java.lang.Runnable {
	 /* .source "ArpDetect.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wifi/ArpDetect;->startMultiGatewayDetect([BLjava/net/Inet4Address;Ljava/net/Inet4Address;I)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wifi.ArpDetect this$0; //synthetic
final Integer val$retryTimes; //synthetic
final java.net.Inet4Address val$senderIp; //synthetic
final val$senderMac; //synthetic
final java.net.Inet4Address val$targetIp; //synthetic
/* # direct methods */
 com.android.server.wifi.ArpDetect$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wifi/ArpDetect; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 373 */
this.this$0 = p1;
this.val$targetIp = p2;
this.val$senderMac = p3;
this.val$senderIp = p4;
/* iput p5, p0, Lcom/android/server/wifi/ArpDetect$1;->val$retryTimes:I */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 9 */
/* .line 377 */
try { // :try_start_0
final String v0 = "ArpDetect"; // const-string v0, "ArpDetect"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "arp detect for "; // const-string v2, "arp detect for "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v3 = this.val$targetIp;
(( java.net.Inet4Address ) v3 ).getHostAddress ( ); // invoke-virtual {v3}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;
com.android.server.wifi.ArpDetect .-$$Nest$mhidenPrivateInfo ( v2,v3 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 378 */
v2 = this.this$0;
v3 = this.val$senderMac;
v4 = this.val$senderIp;
v5 = com.android.server.wifi.ArpPacket.ETHER_ANY;
v6 = this.val$targetIp;
/* iget v0, p0, Lcom/android/server/wifi/ArpDetect$1;->val$retryTimes:I */
/* div-int/lit8 v7, v0, 0x2 */
/* const/16 v8, 0x32 */
/* invoke-static/range {v2 ..v8}, Lcom/android/server/wifi/ArpDetect;->-$$Nest$msendArpPacket(Lcom/android/server/wifi/ArpDetect;[BLjava/net/Inet4Address;[BLjava/net/Inet4Address;II)Z */
/* .line 380 */
/* const-wide/16 v0, 0x64 */
java.lang.Thread .sleep ( v0,v1 );
/* .line 381 */
v2 = this.this$0;
v3 = this.val$senderMac;
v4 = this.val$senderIp;
v5 = com.android.server.wifi.ArpPacket.ETHER_ANY;
v6 = this.val$targetIp;
/* iget v0, p0, Lcom/android/server/wifi/ArpDetect$1;->val$retryTimes:I */
/* div-int/lit8 v7, v0, 0x2 */
/* const/16 v8, 0x1f4 */
/* invoke-static/range {v2 ..v8}, Lcom/android/server/wifi/ArpDetect;->-$$Nest$msendArpPacket(Lcom/android/server/wifi/ArpDetect;[BLjava/net/Inet4Address;[BLjava/net/Inet4Address;II)Z */
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 386 */
/* .line 383 */
/* :catch_0 */
/* move-exception v0 */
/* .line 384 */
/* .local v0, "e":Ljava/lang/InterruptedException; */
(( java.lang.InterruptedException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
/* .line 385 */
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v1 ).interrupt ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
/* .line 387 */
} // .end local v0 # "e":Ljava/lang/InterruptedException;
} // :goto_0
return;
} // .end method
