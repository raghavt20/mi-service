.class public interface abstract Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;
.super Ljava/lang/Object;
.source "WifiDrvUEventObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wifi/WifiDrvUEventObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WifiDrvUeventCallback"
.end annotation


# virtual methods
.method public abstract onCoexModeChange(I)V
.end method

.method public abstract onDataStall(I)V
.end method

.method public abstract onRecoveryComplete(IZ)V
.end method
