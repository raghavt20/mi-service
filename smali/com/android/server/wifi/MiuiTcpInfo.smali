.class public Lcom/android/server/wifi/MiuiTcpInfo;
.super Ljava/lang/Object;
.source "MiuiTcpInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wifi/MiuiTcpInfo$Field;
    }
.end annotation


# static fields
.field public static final ESTABLISHED_STATE:I = 0x1

.field static final LOST_OFFSET:I

.field static final MIN_RTT_OFFSET:I

.field static final RCV_RTT_OFFSET:I

.field static final RETRANSMITS_OFFSET:I

.field static final RETRANS_OFFSET:I

.field static final RTTVAR_OFFSET:I

.field static final RTT_OFFSET:I

.field static final SEGS_IN_OFFSET:I

.field static final SEGS_OUT_OFFSET:I

.field public static final SYN_RECV_STATE:I = 0x3

.field public static final SYN_SENT_STATE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "TcpInfo"

.field static final TORALRETRANS_OFFSET:I

.field static final UNACKED_OFFSET:I


# instance fields
.field final mLost:I

.field final mMinRtt:I

.field final mRcvRtt:I

.field final mRetans:I

.field final mRetransmits:I

.field final mRtt:I

.field final mRttVar:I

.field final mSegsIn:I

.field final mSegsOut:I

.field final mTotalRetrans:I

.field final mUnacked:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 98
    sget-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->LOST:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    invoke-static {v0}, Lcom/android/server/wifi/MiuiTcpInfo;->getFieldOffset(Lcom/android/server/wifi/MiuiTcpInfo$Field;)I

    move-result v0

    sput v0, Lcom/android/server/wifi/MiuiTcpInfo;->LOST_OFFSET:I

    .line 100
    sget-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RETRANSMITS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    invoke-static {v0}, Lcom/android/server/wifi/MiuiTcpInfo;->getFieldOffset(Lcom/android/server/wifi/MiuiTcpInfo$Field;)I

    move-result v0

    sput v0, Lcom/android/server/wifi/MiuiTcpInfo;->RETRANSMITS_OFFSET:I

    .line 102
    sget-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RETRANS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    invoke-static {v0}, Lcom/android/server/wifi/MiuiTcpInfo;->getFieldOffset(Lcom/android/server/wifi/MiuiTcpInfo$Field;)I

    move-result v0

    sput v0, Lcom/android/server/wifi/MiuiTcpInfo;->RETRANS_OFFSET:I

    .line 104
    sget-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SEGS_IN:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    invoke-static {v0}, Lcom/android/server/wifi/MiuiTcpInfo;->getFieldOffset(Lcom/android/server/wifi/MiuiTcpInfo$Field;)I

    move-result v0

    sput v0, Lcom/android/server/wifi/MiuiTcpInfo;->SEGS_IN_OFFSET:I

    .line 106
    sget-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SEGS_OUT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    invoke-static {v0}, Lcom/android/server/wifi/MiuiTcpInfo;->getFieldOffset(Lcom/android/server/wifi/MiuiTcpInfo$Field;)I

    move-result v0

    sput v0, Lcom/android/server/wifi/MiuiTcpInfo;->SEGS_OUT_OFFSET:I

    .line 108
    sget-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->UNACKED:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    invoke-static {v0}, Lcom/android/server/wifi/MiuiTcpInfo;->getFieldOffset(Lcom/android/server/wifi/MiuiTcpInfo$Field;)I

    move-result v0

    sput v0, Lcom/android/server/wifi/MiuiTcpInfo;->UNACKED_OFFSET:I

    .line 110
    sget-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->TOTAL_RETRANS:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    invoke-static {v0}, Lcom/android/server/wifi/MiuiTcpInfo;->getFieldOffset(Lcom/android/server/wifi/MiuiTcpInfo$Field;)I

    move-result v0

    sput v0, Lcom/android/server/wifi/MiuiTcpInfo;->TORALRETRANS_OFFSET:I

    .line 112
    sget-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RTT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    invoke-static {v0}, Lcom/android/server/wifi/MiuiTcpInfo;->getFieldOffset(Lcom/android/server/wifi/MiuiTcpInfo$Field;)I

    move-result v0

    sput v0, Lcom/android/server/wifi/MiuiTcpInfo;->RTT_OFFSET:I

    .line 114
    sget-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RTTVAR:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    invoke-static {v0}, Lcom/android/server/wifi/MiuiTcpInfo;->getFieldOffset(Lcom/android/server/wifi/MiuiTcpInfo$Field;)I

    move-result v0

    sput v0, Lcom/android/server/wifi/MiuiTcpInfo;->RTTVAR_OFFSET:I

    .line 116
    sget-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->RCV_RTT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    invoke-static {v0}, Lcom/android/server/wifi/MiuiTcpInfo;->getFieldOffset(Lcom/android/server/wifi/MiuiTcpInfo$Field;)I

    move-result v0

    sput v0, Lcom/android/server/wifi/MiuiTcpInfo;->RCV_RTT_OFFSET:I

    .line 118
    sget-object v0, Lcom/android/server/wifi/MiuiTcpInfo$Field;->MIN_RTT:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    invoke-static {v0}, Lcom/android/server/wifi/MiuiTcpInfo;->getFieldOffset(Lcom/android/server/wifi/MiuiTcpInfo$Field;)I

    move-result v0

    sput v0, Lcom/android/server/wifi/MiuiTcpInfo;->MIN_RTT_OFFSET:I

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mRetransmits:I

    .line 169
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mRetans:I

    .line 170
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mLost:I

    .line 171
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsOut:I

    .line 172
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsIn:I

    .line 173
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mUnacked:I

    .line 174
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mTotalRetrans:I

    .line 175
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mRtt:I

    .line 176
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mRttVar:I

    .line 177
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mRcvRtt:I

    .line 178
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mMinRtt:I

    .line 179
    return-void
.end method

.method private constructor <init>(Ljava/nio/ByteBuffer;I)V
    .locals 3
    .param p1, "bytes"    # Ljava/nio/ByteBuffer;
    .param p2, "infolen"    # I

    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    sget v0, Lcom/android/server/wifi/MiuiTcpInfo;->SEGS_IN_OFFSET:I

    sget-object v1, Lcom/android/server/wifi/MiuiTcpInfo$Field;->SEGS_IN:Lcom/android/server/wifi/MiuiTcpInfo$Field;

    iget v1, v1, Lcom/android/server/wifi/MiuiTcpInfo$Field;->size:I

    add-int/2addr v1, v0

    if-gt v1, p2, :cond_0

    .line 147
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 148
    .local v1, "start":I
    sget v2, Lcom/android/server/wifi/MiuiTcpInfo;->SEGS_OUT_OFFSET:I

    add-int/2addr v2, v1

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsOut:I

    .line 149
    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsIn:I

    .line 150
    sget v0, Lcom/android/server/wifi/MiuiTcpInfo;->LOST_OFFSET:I

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mLost:I

    .line 151
    sget v0, Lcom/android/server/wifi/MiuiTcpInfo;->RETRANSMITS_OFFSET:I

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mRetransmits:I

    .line 152
    sget v0, Lcom/android/server/wifi/MiuiTcpInfo;->RETRANS_OFFSET:I

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mRetans:I

    .line 153
    sget v0, Lcom/android/server/wifi/MiuiTcpInfo;->UNACKED_OFFSET:I

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mUnacked:I

    .line 154
    sget v0, Lcom/android/server/wifi/MiuiTcpInfo;->TORALRETRANS_OFFSET:I

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mTotalRetrans:I

    .line 155
    sget v0, Lcom/android/server/wifi/MiuiTcpInfo;->RTT_OFFSET:I

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mRtt:I

    .line 156
    sget v0, Lcom/android/server/wifi/MiuiTcpInfo;->RTTVAR_OFFSET:I

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mRttVar:I

    .line 157
    sget v0, Lcom/android/server/wifi/MiuiTcpInfo;->RCV_RTT_OFFSET:I

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mRcvRtt:I

    .line 158
    sget v0, Lcom/android/server/wifi/MiuiTcpInfo;->MIN_RTT_OFFSET:I

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mMinRtt:I

    .line 163
    add-int v0, p2, v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 164
    return-void

    .line 145
    .end local v1    # "start":I
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Length "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is less than required."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static decodeWscale(B)Ljava/lang/String;
    .locals 2
    .param p0, "num"    # B

    .line 194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    shr-int/lit8 v1, p0, 0x4

    and-int/lit8 v1, v1, 0xf

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    and-int/lit8 v1, p0, 0xf

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getFieldOffset(Lcom/android/server/wifi/MiuiTcpInfo$Field;)I
    .locals 6
    .param p0, "needle"    # Lcom/android/server/wifi/MiuiTcpInfo$Field;

    .line 133
    const/4 v0, 0x0

    .line 134
    .local v0, "offset":I
    invoke-static {}, Lcom/android/server/wifi/MiuiTcpInfo$Field;->values()[Lcom/android/server/wifi/MiuiTcpInfo$Field;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 135
    .local v4, "field":Lcom/android/server/wifi/MiuiTcpInfo$Field;
    if-ne v4, p0, :cond_0

    return v0

    .line 136
    :cond_0
    iget v5, v4, Lcom/android/server/wifi/MiuiTcpInfo$Field;->size:I

    add-int/2addr v0, v5

    .line 134
    .end local v4    # "field":Lcom/android/server/wifi/MiuiTcpInfo$Field;
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 138
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unknown field"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static getTcpStateName(I)Ljava/lang/String;
    .locals 2
    .param p0, "state"    # I

    .line 203
    packed-switch p0, :pswitch_data_0

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UNKNOWN:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 214
    :pswitch_0
    const-string v0, "TCP_CLOSING"

    return-object v0

    .line 213
    :pswitch_1
    const-string v0, "TCP_LISTEN"

    return-object v0

    .line 212
    :pswitch_2
    const-string v0, "TCP_LAST_ACK"

    return-object v0

    .line 211
    :pswitch_3
    const-string v0, "TCP_CLOSE_WAIT"

    return-object v0

    .line 210
    :pswitch_4
    const-string v0, "TCP_CLOSE"

    return-object v0

    .line 209
    :pswitch_5
    const-string v0, "TCP_TIME_WAIT"

    return-object v0

    .line 208
    :pswitch_6
    const-string v0, "TCP_FIN_WAIT2"

    return-object v0

    .line 207
    :pswitch_7
    const-string v0, "TCP_FIN_WAIT1"

    return-object v0

    .line 206
    :pswitch_8
    const-string v0, "TCP_SYN_RECV"

    return-object v0

    .line 205
    :pswitch_9
    const-string v0, "TCP_SYN_SENT"

    return-object v0

    .line 204
    :pswitch_a
    const-string v0, "TCP_ESTABLISHED"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static parse(Ljava/nio/ByteBuffer;I)Lcom/android/server/wifi/MiuiTcpInfo;
    .locals 3
    .param p0, "bytes"    # Ljava/nio/ByteBuffer;
    .param p1, "infolen"    # I

    .line 185
    :try_start_0
    new-instance v0, Lcom/android/server/wifi/MiuiTcpInfo;

    invoke-direct {v0, p0, p1}, Lcom/android/server/wifi/MiuiTcpInfo;-><init>(Ljava/nio/ByteBuffer;I)V
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/BufferOverflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 186
    :catch_0
    move-exception v0

    .line 188
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "TcpInfo"

    const-string v2, "parsing error."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 189
    const/4 v1, 0x0

    return-object v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .line 221
    instance-of v0, p1, Lcom/android/server/wifi/MiuiTcpInfo;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 222
    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/android/server/wifi/MiuiTcpInfo;

    .line 224
    .local v0, "other":Lcom/android/server/wifi/MiuiTcpInfo;
    iget v2, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsOut:I

    iget v3, v0, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsOut:I

    if-ne v2, v3, :cond_1

    iget v2, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mRetransmits:I

    iget v3, v0, Lcom/android/server/wifi/MiuiTcpInfo;->mRetransmits:I

    if-ne v2, v3, :cond_1

    iget v2, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mLost:I

    iget v3, v0, Lcom/android/server/wifi/MiuiTcpInfo;->mLost:I

    if-ne v2, v3, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public hashCode()I
    .locals 4

    .line 230
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mLost:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mRetransmits:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsOut:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mRetans:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    filled-new-array {v0, v1, v2, v3}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 235
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TcpInfo{lost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mLost:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retransmit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mRetransmits:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsOut:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
