.class public Lcom/android/server/wifi/WifiDrvUEventObserver;
.super Landroid/os/UEventObserver;
.source "WifiDrvUEventObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;,
        Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;
    }
.end annotation


# static fields
.field private static final ACTION_CHIP_RESET:I = 0x0

.field private static final ACTION_L1_SER:I = 0x2

.field private static final ACTION_L3_SER:I = 0x3

.field private static final ACTION_ROAMING_NOTIFY:I = 0xa

.field private static final ACTION_WIFI_RESET:I = 0x1

.field public static final COEX_FDD_MODE:I = 0x3

.field public static final COEX_HBD_MODE:I = 0x2

.field private static final COEX_MODE_EVENT:Ljava/lang/String; = "coex_mode"

.field public static final COEX_NO_BT:I = 0x0

.field public static final COEX_TDD_MODE:I = 0x1

.field private static final DEBUG:Z = false

.field private static final EXTRA_WBH_MODE_TYPE:Ljava/lang/String; = "extra_wbh_mode_type"

.field private static final L1_SER:Ljava/lang/String; = "l1ser"

.field private static final L3_SER:Ljava/lang/String; = "l3ser"

.field private static final MAX_REC_SIZE:I = 0x1e

.field public static final MTK_DATASTALL_COEX_LOW:I = 0x204

.field public static final MTK_DATASTALL_FCS_ERROR:I = 0x203

.field public static final MTK_DATASTALL_NONE:I = 0x0

.field public static final MTK_DATASTALL_NUD_FAILURE:I = 0x206

.field public static final MTK_DATASTALL_PM_CHANGE_FAIL:I = 0x205

.field public static final MTK_DATASTALL_RX_REORDER:I = 0x202

.field public static final MTK_DATASTALL_TX_DROP:I = 0x201

.field public static final MTK_DATASTALL_TX_HANG:I = 0x200

.field private static final MTK_RECOVERY_EVENT:Ljava/lang/String; = "recoveryNotify"

.field private static final MTK_ROAM_NOTIFY_EVENT:Ljava/lang/String; = "roam"

.field private static final MTK_ROAM_SUCCESS:Ljava/lang/String; = "Status:SUCCESS"

.field private static final MTK_TRX_ABNORMAL_EVENT:Ljava/lang/String; = "abnormaltrx"

.field private static final TAG:Ljava/lang/String; = "WifiDrvUEventObserver"

.field private static final UEVENT_PATH:Ljava/lang/String; = "DEVPATH=/devices/virtual/misc/wlan"

.field public static final WBH_MODE_CHANGED:Ljava/lang/String; = "android.net.wifi.COEX_MODE_STATE_CHANGED"

.field private static final WIFI_RESET:Ljava/lang/String; = "reset"

.field private static mUeventRecord:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final MTK_DATASTALL_TYPE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private lastCoexMode:I

.field private mCallback:Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 71
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/android/server/wifi/WifiDrvUEventObserver;->mUeventRecord:Ljava/util/LinkedList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callback"    # Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;

    .line 77
    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Landroid/os/UEventObserver;-><init>()V

    .line 62
    nop

    .line 63
    const/16 v1, 0x200

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "DIR:TX,Event:Hang"

    .line 64
    const/16 v1, 0x201

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "DIR:TX,event:AbDrop"

    .line 65
    const/16 v1, 0x203

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v7, "DIR:RX,Event:AbCRC"

    .line 66
    const/16 v1, 0x202

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const-string v9, "DIR:RX,event:AbReorder"

    .line 67
    const/16 v1, 0x204

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const-string v11, "DIR:TXRX,event:BTWifiCoexLow"

    .line 68
    const/16 v1, 0x205

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const-string v13, "DIR:TX,Event:pmStateChangeFail"

    .line 69
    const/16 v1, 0x206

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    const-string v15, "DIR:RX,event:AbArpNoResponse"

    .line 62
    invoke-static/range {v2 .. v15}, Ljava/util/Map;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/android/server/wifi/WifiDrvUEventObserver;->MTK_DATASTALL_TYPE:Ljava/util/Map;

    .line 73
    const/4 v1, 0x0

    iput v1, v0, Lcom/android/server/wifi/WifiDrvUEventObserver;->lastCoexMode:I

    .line 78
    move-object/from16 v1, p1

    iput-object v1, v0, Lcom/android/server/wifi/WifiDrvUEventObserver;->mContext:Landroid/content/Context;

    .line 79
    move-object/from16 v2, p2

    iput-object v2, v0, Lcom/android/server/wifi/WifiDrvUEventObserver;->mCallback:Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;

    .line 80
    return-void
.end method

.method private addUeventRec(Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;)V
    .locals 2
    .param p1, "rec"    # Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;

    .line 225
    if-eqz p1, :cond_2

    sget-object v0, Lcom/android/server/wifi/WifiDrvUEventObserver;->mUeventRecord:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    goto :goto_0

    .line 229
    :cond_0
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_1

    .line 230
    sget-object v0, Lcom/android/server/wifi/WifiDrvUEventObserver;->mUeventRecord:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 231
    :cond_1
    sget-object v0, Lcom/android/server/wifi/WifiDrvUEventObserver;->mUeventRecord:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 232
    return-void

    .line 226
    :cond_2
    :goto_0
    return-void
.end method

.method public static dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;

    .line 241
    const-string v0, "WifiDrvUEvent:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " total records="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/android/server/wifi/WifiDrvUEventObserver;->getUeventRecCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 243
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-static {}, Lcom/android/server/wifi/WifiDrvUEventObserver;->getUeventRecCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 244
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " rec["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lcom/android/server/wifi/WifiDrvUEventObserver;->getUeventRec(I)Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 245
    invoke-virtual {p1}, Ljava/io/PrintWriter;->flush()V

    .line 243
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private getDatastallType(Ljava/lang/String;)I
    .locals 6
    .param p1, "datastallEvent"    # Ljava/lang/String;

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "WifiDrvUEventObserver"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 171
    return v2

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/android/server/wifi/WifiDrvUEventObserver;->MTK_DATASTALL_TYPE:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 174
    .local v3, "key":I
    iget-object v4, p0, Lcom/android/server/wifi/WifiDrvUEventObserver;->MTK_DATASTALL_TYPE:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 175
    goto :goto_0

    .line 177
    :cond_1
    iget-object v4, p0, Lcom/android/server/wifi/WifiDrvUEventObserver;->MTK_DATASTALL_TYPE:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {p1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "datastall type: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    return v3

    .line 181
    .end local v3    # "key":I
    :cond_2
    goto :goto_0

    .line 182
    :cond_3
    return v2
.end method

.method private static getUeventRec(I)Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;
    .locals 1
    .param p0, "index"    # I

    .line 235
    sget-object v0, Lcom/android/server/wifi/WifiDrvUEventObserver;->mUeventRecord:Ljava/util/LinkedList;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/server/wifi/WifiDrvUEventObserver;->getUeventRecCount()I

    move-result v0

    if-lt p0, v0, :cond_0

    goto :goto_0

    .line 237
    :cond_0
    sget-object v0, Lcom/android/server/wifi/WifiDrvUEventObserver;->mUeventRecord:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;

    return-object v0

    .line 236
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private static getUeventRecCount()I
    .locals 1

    .line 219
    sget-object v0, Lcom/android/server/wifi/WifiDrvUEventObserver;->mUeventRecord:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    .line 220
    const/4 v0, 0x0

    return v0

    .line 221
    :cond_0
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method


# virtual methods
.method public onUEvent(Landroid/os/UEventObserver$UEvent;)V
    .locals 18
    .param p1, "event"    # Landroid/os/UEventObserver$UEvent;

    .line 85
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string v0, "abnormaltrx"

    const-string v3, "coex_mode"

    const-string v4, ""

    :try_start_0
    invoke-virtual {v2, v3}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 86
    .local v5, "coexMode":Ljava/lang/String;
    invoke-virtual {v2, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 87
    .local v6, "mtkTrxAbnormal":Ljava/lang/String;
    const-string v7, "recoveryNotify"

    invoke-virtual {v2, v7}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 88
    .local v7, "mtkRecoveryEvent":Ljava/lang/String;
    const-string v8, "roam"

    invoke-virtual {v2, v8}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 89
    .local v8, "mtkRoamNotify":Ljava/lang/String;
    if-eqz v5, :cond_2

    .line 90
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 91
    .local v9, "curCoexMode":I
    iget v10, v1, Lcom/android/server/wifi/WifiDrvUEventObserver;->lastCoexMode:I

    if-eq v9, v10, :cond_1

    const/4 v11, 0x2

    if-ge v9, v11, :cond_0

    if-lt v10, v11, :cond_1

    .line 93
    :cond_0
    new-instance v10, Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;

    invoke-direct {v10, v3, v5}, Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v10

    .line 94
    .local v3, "uRec":Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;
    invoke-direct {v1, v3}, Lcom/android/server/wifi/WifiDrvUEventObserver;->addUeventRec(Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;)V

    .line 95
    iget-object v10, v1, Lcom/android/server/wifi/WifiDrvUEventObserver;->mCallback:Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;

    invoke-interface {v10, v9}, Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;->onCoexModeChange(I)V

    .line 96
    new-instance v10, Landroid/content/Intent;

    const-string v11, "android.net.wifi.COEX_MODE_STATE_CHANGED"

    invoke-direct {v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 97
    .local v10, "intent":Landroid/content/Intent;
    const-string v11, "extra_wbh_mode_type"

    invoke-virtual {v10, v11, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 98
    iget-object v11, v1, Lcom/android/server/wifi/WifiDrvUEventObserver;->mContext:Landroid/content/Context;

    sget-object v12, Landroid/os/UserHandle;->SYSTEM:Landroid/os/UserHandle;

    invoke-virtual {v11, v10, v12}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 100
    .end local v3    # "uRec":Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;
    .end local v10    # "intent":Landroid/content/Intent;
    :cond_1
    iput v9, v1, Lcom/android/server/wifi/WifiDrvUEventObserver;->lastCoexMode:I

    .line 103
    .end local v9    # "curCoexMode":I
    :cond_2
    if-eqz v6, :cond_4

    .line 104
    invoke-direct {v1, v6}, Lcom/android/server/wifi/WifiDrvUEventObserver;->getDatastallType(Ljava/lang/String;)I

    move-result v3

    .line 105
    .local v3, "type":I
    if-eqz v3, :cond_3

    .line 106
    iget-object v9, v1, Lcom/android/server/wifi/WifiDrvUEventObserver;->mCallback:Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;

    invoke-interface {v9, v3}, Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;->onDataStall(I)V

    .line 108
    :cond_3
    new-instance v9, Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;

    invoke-direct {v9, v0, v6}, Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v9

    .line 109
    .local v0, "uRec":Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;
    invoke-direct {v1, v0}, Lcom/android/server/wifi/WifiDrvUEventObserver;->addUeventRec(Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;)V

    .line 112
    .end local v0    # "uRec":Lcom/android/server/wifi/WifiDrvUEventObserver$UeventRecord;
    .end local v3    # "type":I
    :cond_4
    if-eqz v7, :cond_e

    .line 113
    const/4 v0, -0x1

    .line 114
    .local v0, "action":I
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 115
    .local v9, "recoveryState":Ljava/lang/Boolean;
    const/4 v10, -0x1

    .line 116
    .local v10, "reason":I
    const-string v11, ","

    invoke-virtual {v7, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 117
    .local v11, "recoveryInfo":[Ljava/lang/String;
    array-length v12, v11

    move v13, v3

    :goto_0
    if-ge v13, v12, :cond_d

    aget-object v15, v11, v13

    .line 118
    .local v15, "str":Ljava/lang/String;
    if-eqz v15, :cond_b

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_b

    .line 119
    const/16 v14, 0x3a

    invoke-virtual {v15, v14}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    const/4 v14, 0x0

    invoke-virtual {v15, v14, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 120
    .local v3, "key":Ljava/lang/String;
    const/16 v14, 0x3a

    invoke-virtual {v15, v14}, Ljava/lang/String;->indexOf(I)I

    move-result v14

    const/16 v16, 0x1

    add-int/lit8 v14, v14, 0x1

    invoke-virtual {v15, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 121
    .local v14, "value":Ljava/lang/String;
    move-object/from16 v17, v5

    .end local v5    # "coexMode":Ljava/lang/String;
    .local v17, "coexMode":Ljava/lang/String;
    const-string v5, "reset"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 123
    const/4 v0, 0x1

    .line 124
    if-eqz v14, :cond_c

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 125
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_5

    const/16 v16, 0x1

    goto :goto_1

    :cond_5
    const/16 v16, 0x0

    :goto_1
    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object v9, v5

    .end local v9    # "recoveryState":Ljava/lang/Boolean;
    .local v5, "recoveryState":Ljava/lang/Boolean;
    goto :goto_4

    .line 127
    .end local v5    # "recoveryState":Ljava/lang/Boolean;
    .restart local v9    # "recoveryState":Ljava/lang/Boolean;
    :cond_6
    const-string v5, "l1ser"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 128
    const/4 v0, 0x2

    .line 129
    if-eqz v14, :cond_c

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 130
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_7

    const/16 v16, 0x1

    goto :goto_2

    :cond_7
    const/16 v16, 0x0

    :goto_2
    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object v9, v5

    .end local v9    # "recoveryState":Ljava/lang/Boolean;
    .restart local v5    # "recoveryState":Ljava/lang/Boolean;
    goto :goto_4

    .line 132
    .end local v5    # "recoveryState":Ljava/lang/Boolean;
    .restart local v9    # "recoveryState":Ljava/lang/Boolean;
    :cond_8
    const-string v5, "l3ser"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 133
    const/4 v0, 0x3

    .line 134
    if-eqz v14, :cond_c

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 135
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_9

    const/16 v16, 0x1

    goto :goto_3

    :cond_9
    const/16 v16, 0x0

    :goto_3
    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object v9, v5

    .end local v9    # "recoveryState":Ljava/lang/Boolean;
    .restart local v5    # "recoveryState":Ljava/lang/Boolean;
    goto :goto_4

    .line 137
    .end local v5    # "recoveryState":Ljava/lang/Boolean;
    .restart local v9    # "recoveryState":Ljava/lang/Boolean;
    :cond_a
    const-string v5, "reason"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 138
    if-eqz v14, :cond_c

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 139
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    move v10, v5

    .end local v10    # "reason":I
    .local v5, "reason":I
    goto :goto_4

    .line 118
    .end local v3    # "key":Ljava/lang/String;
    .end local v14    # "value":Ljava/lang/String;
    .end local v17    # "coexMode":Ljava/lang/String;
    .local v5, "coexMode":Ljava/lang/String;
    .restart local v10    # "reason":I
    :cond_b
    move-object/from16 v17, v5

    .line 117
    .end local v5    # "coexMode":Ljava/lang/String;
    .end local v15    # "str":Ljava/lang/String;
    .restart local v17    # "coexMode":Ljava/lang/String;
    :cond_c
    :goto_4
    add-int/lit8 v13, v13, 0x1

    move-object/from16 v5, v17

    const/4 v3, 0x0

    goto/16 :goto_0

    .line 144
    .end local v17    # "coexMode":Ljava/lang/String;
    .restart local v5    # "coexMode":Ljava/lang/String;
    :cond_d
    move-object/from16 v17, v5

    .end local v5    # "coexMode":Ljava/lang/String;
    .restart local v17    # "coexMode":Ljava/lang/String;
    const/4 v3, 0x1

    if-ne v10, v3, :cond_f

    .line 145
    iget-object v3, v1, Lcom/android/server/wifi/WifiDrvUEventObserver;->mCallback:Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-interface {v3, v0, v4}, Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;->onRecoveryComplete(IZ)V

    goto :goto_5

    .line 112
    .end local v0    # "action":I
    .end local v9    # "recoveryState":Ljava/lang/Boolean;
    .end local v10    # "reason":I
    .end local v11    # "recoveryInfo":[Ljava/lang/String;
    .end local v17    # "coexMode":Ljava/lang/String;
    .restart local v5    # "coexMode":Ljava/lang/String;
    :cond_e
    move-object/from16 v17, v5

    .line 149
    .end local v5    # "coexMode":Ljava/lang/String;
    .restart local v17    # "coexMode":Ljava/lang/String;
    :cond_f
    :goto_5
    if-eqz v8, :cond_10

    .line 150
    const-string v0, "Status:SUCCESS"

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 151
    .local v0, "isRoamSuccess":Z
    iget-object v3, v1, Lcom/android/server/wifi/WifiDrvUEventObserver;->mCallback:Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;

    const/16 v4, 0xa

    invoke-interface {v3, v4, v0}, Lcom/android/server/wifi/WifiDrvUEventObserver$WifiDrvUeventCallback;->onRecoveryComplete(IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    .end local v0    # "isRoamSuccess":Z
    .end local v6    # "mtkTrxAbnormal":Ljava/lang/String;
    .end local v7    # "mtkRecoveryEvent":Ljava/lang/String;
    .end local v8    # "mtkRoamNotify":Ljava/lang/String;
    .end local v17    # "coexMode":Ljava/lang/String;
    :cond_10
    goto :goto_6

    .line 153
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not parse event "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "WifiDrvUEventObserver"

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_6
    return-void
.end method

.method public start()V
    .locals 2

    .line 159
    const-string v0, "WifiDrvUEventObserver"

    const-string/jumbo v1, "startObserving"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const-string v0, "DEVPATH=/devices/virtual/misc/wlan"

    invoke-virtual {p0, v0}, Lcom/android/server/wifi/WifiDrvUEventObserver;->startObserving(Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method public stop()V
    .locals 2

    .line 164
    const-string v0, "WifiDrvUEventObserver"

    const-string/jumbo v1, "stopObserving"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    invoke-virtual {p0}, Lcom/android/server/wifi/WifiDrvUEventObserver;->stopObserving()V

    .line 166
    return-void
.end method
