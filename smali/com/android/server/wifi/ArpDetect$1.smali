.class Lcom/android/server/wifi/ArpDetect$1;
.super Ljava/lang/Object;
.source "ArpDetect.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wifi/ArpDetect;->startMultiGatewayDetect([BLjava/net/Inet4Address;Ljava/net/Inet4Address;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wifi/ArpDetect;

.field final synthetic val$retryTimes:I

.field final synthetic val$senderIp:Ljava/net/Inet4Address;

.field final synthetic val$senderMac:[B

.field final synthetic val$targetIp:Ljava/net/Inet4Address;


# direct methods
.method constructor <init>(Lcom/android/server/wifi/ArpDetect;Ljava/net/Inet4Address;[BLjava/net/Inet4Address;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wifi/ArpDetect;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 373
    iput-object p1, p0, Lcom/android/server/wifi/ArpDetect$1;->this$0:Lcom/android/server/wifi/ArpDetect;

    iput-object p2, p0, Lcom/android/server/wifi/ArpDetect$1;->val$targetIp:Ljava/net/Inet4Address;

    iput-object p3, p0, Lcom/android/server/wifi/ArpDetect$1;->val$senderMac:[B

    iput-object p4, p0, Lcom/android/server/wifi/ArpDetect$1;->val$senderIp:Ljava/net/Inet4Address;

    iput p5, p0, Lcom/android/server/wifi/ArpDetect$1;->val$retryTimes:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 377
    :try_start_0
    const-string v0, "ArpDetect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "arp detect for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wifi/ArpDetect$1;->this$0:Lcom/android/server/wifi/ArpDetect;

    iget-object v3, p0, Lcom/android/server/wifi/ArpDetect$1;->val$targetIp:Ljava/net/Inet4Address;

    invoke-virtual {v3}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/wifi/ArpDetect;->-$$Nest$mhidenPrivateInfo(Lcom/android/server/wifi/ArpDetect;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    iget-object v2, p0, Lcom/android/server/wifi/ArpDetect$1;->this$0:Lcom/android/server/wifi/ArpDetect;

    iget-object v3, p0, Lcom/android/server/wifi/ArpDetect$1;->val$senderMac:[B

    iget-object v4, p0, Lcom/android/server/wifi/ArpDetect$1;->val$senderIp:Ljava/net/Inet4Address;

    sget-object v5, Lcom/android/server/wifi/ArpPacket;->ETHER_ANY:[B

    iget-object v6, p0, Lcom/android/server/wifi/ArpDetect$1;->val$targetIp:Ljava/net/Inet4Address;

    iget v0, p0, Lcom/android/server/wifi/ArpDetect$1;->val$retryTimes:I

    div-int/lit8 v7, v0, 0x2

    const/16 v8, 0x32

    invoke-static/range {v2 .. v8}, Lcom/android/server/wifi/ArpDetect;->-$$Nest$msendArpPacket(Lcom/android/server/wifi/ArpDetect;[BLjava/net/Inet4Address;[BLjava/net/Inet4Address;II)Z

    .line 380
    const-wide/16 v0, 0x64

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 381
    iget-object v2, p0, Lcom/android/server/wifi/ArpDetect$1;->this$0:Lcom/android/server/wifi/ArpDetect;

    iget-object v3, p0, Lcom/android/server/wifi/ArpDetect$1;->val$senderMac:[B

    iget-object v4, p0, Lcom/android/server/wifi/ArpDetect$1;->val$senderIp:Ljava/net/Inet4Address;

    sget-object v5, Lcom/android/server/wifi/ArpPacket;->ETHER_ANY:[B

    iget-object v6, p0, Lcom/android/server/wifi/ArpDetect$1;->val$targetIp:Ljava/net/Inet4Address;

    iget v0, p0, Lcom/android/server/wifi/ArpDetect$1;->val$retryTimes:I

    div-int/lit8 v7, v0, 0x2

    const/16 v8, 0x1f4

    invoke-static/range {v2 .. v8}, Lcom/android/server/wifi/ArpDetect;->-$$Nest$msendArpPacket(Lcom/android/server/wifi/ArpDetect;[BLjava/net/Inet4Address;[BLjava/net/Inet4Address;II)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 386
    goto :goto_0

    .line 383
    :catch_0
    move-exception v0

    .line 384
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 385
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 387
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_0
    return-void
.end method
