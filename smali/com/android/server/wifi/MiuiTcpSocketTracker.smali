.class public Lcom/android/server/wifi/MiuiTcpSocketTracker;
.super Ljava/lang/Object;
.source "MiuiTcpSocketTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;,
        Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;,
        Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;,
        Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;
    }
.end annotation


# static fields
.field private static final ADDRESS_FAMILIES:[I

.field public static final CONFIG_MIN_PACKETS_THRESHOLD:Ljava/lang/String; = "tcp_min_packets_threshold"

.field public static final CONFIG_TCP_PACKETS_FAIL_PERCENTAGE:Ljava/lang/String; = "tcp_packets_fail_percentage"

.field public static final DEFAULT_DATA_STALL_MIN_PACKETS_THRESHOLD:I = 0xa

.field public static final DEFAULT_NLMSG_DONE_PACKET_SIZE:I = 0x14

.field public static final DEFAULT_TCP_PACKETS_FAIL_PERCENTAGE:I = 0x50

.field private static final IDIAG_COOKIE_OFFSET:I = 0x2c

.field private static final IDIAG_UID2COOKIE_OFFSET:I = 0xc

.field private static final NULL_MASK:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MiuiTcpSocketTracker"

.field public static final TCP_ESTABLISHED:I = 0x1

.field public static final TCP_MONITOR_STATE_FILTER:I = 0xe

.field public static final TCP_SYN_RECV:I = 0x3

.field public static final TCP_SYN_SENT:I = 0x2

.field private static final THRESHOLD_OF_SENT:I = 0x3e8

.field private static final UNKNOWN_MARK:I = -0x1


# instance fields
.field protected final mConfigListener:Landroid/provider/DeviceConfig$OnPropertiesChangedListener;

.field private final mDependencies:Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;

.field private mLatestPacketFailPercentage:I

.field private mLatestReceivedCount:I

.field private mLatestTcpStats:Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;

.field private mMinPacketsThreshold:I

.field private final mNetd:Landroid/net/INetd;

.field private final mNetwork:Landroid/net/Network;

.field private final mNetworkMark:I

.field private final mNetworkMask:I

.field private mSentSinceLastRecv:I

.field private mSkipUidList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mSockDiagMsg:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "[B>;"
        }
    .end annotation
.end field

.field private final mSocketInfos:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray<",
            "Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTcpPacketsFailRateThreshold:I

.field private final mToRemovedSocketInfos:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray<",
            "Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;",
            ">;"
        }
    .end annotation
.end field

.field private msgRecord:Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$fgetmDependencies(Lcom/android/server/wifi/MiuiTcpSocketTracker;)Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mDependencies:Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLatestPacketFailPercentage(Lcom/android/server/wifi/MiuiTcpSocketTracker;)I
    .locals 0

    iget p0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mLatestPacketFailPercentage:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmMinPacketsThreshold(Lcom/android/server/wifi/MiuiTcpSocketTracker;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mMinPacketsThreshold:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTcpPacketsFailRateThreshold(Lcom/android/server/wifi/MiuiTcpSocketTracker;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mTcpPacketsFailRateThreshold:I

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 75
    sget v0, Landroid/system/OsConstants;->AF_INET6:I

    sget v1, Landroid/system/OsConstants;->AF_INET:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->ADDRESS_FAMILIES:[I

    return-void
.end method

.method public constructor <init>(Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;Landroid/net/Network;)V
    .locals 17
    .param p1, "dps"    # Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;
    .param p2, "network"    # Landroid/net/Network;

    .line 157
    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSockDiagMsg:Landroid/util/SparseArray;

    .line 117
    const/16 v1, 0xa

    iput v1, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mMinPacketsThreshold:I

    .line 118
    const/16 v1, 0x50

    iput v1, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mTcpPacketsFailRateThreshold:I

    .line 127
    new-instance v1, Landroid/util/LongSparseArray;

    invoke-direct {v1}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v1, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSocketInfos:Landroid/util/LongSparseArray;

    .line 128
    new-instance v1, Landroid/util/LongSparseArray;

    invoke-direct {v1}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v1, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mToRemovedSocketInfos:Landroid/util/LongSparseArray;

    .line 132
    const/4 v1, 0x0

    iput v1, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mLatestPacketFailPercentage:I

    .line 140
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSkipUidList:Ljava/util/Set;

    .line 142
    new-instance v2, Lcom/android/server/wifi/MiuiTcpSocketTracker$1;

    invoke-direct {v2, v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$1;-><init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;)V

    iput-object v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mConfigListener:Landroid/provider/DeviceConfig$OnPropertiesChangedListener;

    .line 158
    move-object/from16 v2, p1

    iput-object v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mDependencies:Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;

    .line 159
    move-object/from16 v3, p2

    iput-object v3, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mNetwork:Landroid/net/Network;

    .line 160
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->getNetd()Landroid/net/INetd;

    move-result-object v4

    iput-object v4, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mNetd:Landroid/net/INetd;

    .line 163
    invoke-direct/range {p0 .. p0}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->getNetworkMarkMask()Landroid/net/MarkMaskParcel;

    move-result-object v4

    .line 164
    .local v4, "parcel":Landroid/net/MarkMaskParcel;
    if-eqz v4, :cond_0

    iget v5, v4, Landroid/net/MarkMaskParcel;->mark:I

    goto :goto_0

    :cond_0
    const/4 v5, -0x1

    :goto_0
    iput v5, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mNetworkMark:I

    .line 165
    if-eqz v4, :cond_1

    iget v5, v4, Landroid/net/MarkMaskParcel;->mask:I

    goto :goto_1

    :cond_1
    move v5, v1

    :goto_1
    iput v5, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mNetworkMask:I

    .line 169
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->isTcpInfoParsingSupported()Z

    move-result v5

    if-nez v5, :cond_2

    return-void

    .line 171
    :cond_2
    sget-object v5, Lcom/android/server/wifi/MiuiTcpSocketTracker;->ADDRESS_FAMILIES:[I

    array-length v6, v5

    :goto_2
    if-ge v1, v6, :cond_3

    aget v15, v5, v1

    .line 172
    .local v15, "family":I
    iget-object v14, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSockDiagMsg:Landroid/util/SparseArray;

    sget v7, Landroid/system/OsConstants;->IPPROTO_TCP:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x301

    const/4 v12, 0x0

    const/4 v13, 0x2

    const/16 v16, 0xe

    .line 174
    move v10, v15

    move-object v2, v14

    move/from16 v14, v16

    invoke-static/range {v7 .. v14}, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagReqV2(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISIII)[B

    move-result-object v7

    .line 172
    invoke-virtual {v2, v15, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 171
    .end local v15    # "family":I
    add-int/lit8 v1, v1, 0x1

    move-object/from16 v2, p1

    goto :goto_2

    .line 183
    :cond_3
    iget-object v1, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mDependencies:Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;

    iget-object v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mConfigListener:Landroid/provider/DeviceConfig$OnPropertiesChangedListener;

    invoke-virtual {v1, v2}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->addDeviceConfigChangedListener(Landroid/provider/DeviceConfig$OnPropertiesChangedListener;)V

    .line 184
    return-void
.end method

.method private calculateLatestPacketsStat(Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;JI)Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    .locals 6
    .param p1, "current"    # Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;
    .param p2, "cookies"    # J
    .param p4, "nlmsgUid"    # I

    .line 410
    new-instance v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;

    invoke-direct {v0, p0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;-><init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;)V

    .line 411
    .local v0, "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    iget-object v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v1, p2, p3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;

    .line 413
    .local v1, "previous":Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;
    iget v2, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->fwmark:I

    iget v3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mNetworkMask:I

    and-int/2addr v2, v3

    iget v3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mNetworkMark:I

    const/4 v4, 0x0

    const-string v5, "MiuiTcpSocketTracker"

    if-eq v2, v3, :cond_0

    .line 414
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "skip mismatch tcpInfo:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    return-object v4

    .line 418
    :cond_0
    iget-object v2, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    if-nez v2, :cond_1

    .line 419
    const-string v2, "Current tcpInfo is null."

    invoke-static {v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    return-object v4

    .line 423
    :cond_1
    iget-object v2, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsOut:I

    iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I

    .line 424
    iget-object v2, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mLost:I

    iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I

    .line 425
    iget-object v2, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mRetans:I

    iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I

    .line 426
    iget-object v2, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mRetransmits:I

    iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I

    .line 427
    iget-object v2, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mUnacked:I

    iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I

    .line 428
    iget-object v2, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mTotalRetrans:I

    iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I

    .line 429
    iget-object v2, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsIn:I

    iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I

    .line 430
    iget-object v2, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mRtt:I

    iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRtt:I

    .line 431
    iget-object v2, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mRcvRtt:I

    iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRcvRtt:I

    .line 432
    iget-object v2, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mRttVar:I

    iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRttVar:I

    .line 433
    iget-object v2, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v2, v2, Lcom/android/server/wifi/MiuiTcpInfo;->mMinRtt:I

    iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->minRtt:I

    .line 434
    if-eqz v1, :cond_2

    iget-object v2, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    if-eqz v2, :cond_2

    .line 435
    iget v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I

    iget-object v3, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v3, v3, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsOut:I

    sub-int/2addr v2, v3

    iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I

    .line 436
    iget v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I

    iget-object v3, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v3, v3, Lcom/android/server/wifi/MiuiTcpInfo;->mTotalRetrans:I

    sub-int/2addr v2, v3

    iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I

    .line 437
    iget v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I

    iget-object v3, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v3, v3, Lcom/android/server/wifi/MiuiTcpInfo;->mSegsIn:I

    sub-int/2addr v2, v3

    iput v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I

    .line 439
    :cond_2
    iget v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I

    const/16 v3, 0x3e8

    if-le v2, v3, :cond_3

    iget-object v2, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSkipUidList:Ljava/util/Set;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 440
    iget-object v2, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSkipUidList:Ljava/util/Set;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 441
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " sentCound is greater than 1000 and is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    :cond_3
    return-object v0
.end method

.method private cleanupSocketInfo(J)V
    .locals 10
    .param p1, "time"    # J

    .line 322
    iget-object v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    .line 323
    .local v0, "size":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 324
    .local v1, "toRemove":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 325
    iget-object v3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v2}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v3

    .line 326
    .local v3, "key":J
    iget-object v5, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v5, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;

    iget-wide v5, v5, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->updateTime:J

    cmp-long v5, v5, p1

    if-gez v5, :cond_0

    .line 327
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 324
    .end local v3    # "key":J
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 330
    .end local v2    # "i":I
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 331
    .local v3, "key":Ljava/lang/Long;
    iget-object v4, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mToRemovedSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iget-object v7, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;

    invoke-virtual {v4, v5, v6, v7}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 332
    iget-object v4, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Landroid/util/LongSparseArray;->remove(J)V

    .line 333
    .end local v3    # "key":Ljava/lang/Long;
    goto :goto_1

    .line 334
    :cond_2
    return-void
.end method

.method public static closeSocketQuietly(Ljava/io/FileDescriptor;)V
    .locals 1
    .param p0, "fd"    # Ljava/io/FileDescriptor;

    .line 316
    :try_start_0
    invoke-static {p0}, Landroid/net/util/SocketUtils;->closeSocket(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 318
    goto :goto_0

    .line 317
    :catch_0
    move-exception v0

    .line 319
    :goto_0
    return-void
.end method

.method static enoughBytesRemainForValidNlMsg(Ljava/nio/ByteBuffer;)Z
    .locals 2
    .param p0, "bytes"    # Ljava/nio/ByteBuffer;

    .line 380
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private getMinPacketsThreshold()I
    .locals 1

    .line 462
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mMinPacketsThreshold:I

    return v0
.end method

.method private getNetworkMarkMask()Landroid/net/MarkMaskParcel;
    .locals 3

    .line 190
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mNetwork:Landroid/net/Network;

    invoke-virtual {v0}, Landroid/net/Network;->getNetId()I

    move-result v0

    .line 191
    .local v0, "netId":I
    iget-object v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mNetd:Landroid/net/INetd;

    invoke-interface {v1, v0}, Landroid/net/INetd;->getFwmarkForNetwork(I)Landroid/net/MarkMaskParcel;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 192
    .end local v0    # "netId":I
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MiuiTcpSocketTracker"

    const-string v2, "Get netId is not available in this API level, "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 195
    .end local v0    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return-object v0
.end method

.method private getTcpPacketsFailRateThreshold()I
    .locals 1

    .line 466
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mTcpPacketsFailRateThreshold:I

    return v0
.end method

.method private static isValidInetDiagMsgSize(I)Z
    .locals 1
    .param p0, "nlMsgLen"    # I

    .line 384
    const/16 v0, 0x58

    if-lt p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private skipRemainingAttributesBytesAligned(Ljava/nio/ByteBuffer;S)V
    .locals 2
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
    .param p2, "len"    # S

    .line 490
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    .line 491
    .local v0, "cur":I
    invoke-static {p2}, Lcom/android/net/module/util/netlink/NetlinkConstants;->alignedLengthOf(S)I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 492
    return-void
.end method


# virtual methods
.method public getInvalidTcpInfo()Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    .locals 7

    .line 337
    iget-object v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mToRemovedSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    .line 338
    .local v0, "size":I
    new-instance v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;

    invoke-direct {v1, p0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;-><init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;)V

    .line 339
    .local v1, "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 340
    iget-object v3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mToRemovedSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v2}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v3

    .line 341
    .local v3, "key":J
    iget v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I

    iget-object v6, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mToRemovedSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v6, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;

    iget-object v6, v6, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v6, v6, Lcom/android/server/wifi/MiuiTcpInfo;->mRetransmits:I

    add-int/2addr v5, v6

    iput v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I

    .line 342
    iget v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I

    iget-object v6, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mToRemovedSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v6, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;

    iget-object v6, v6, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v6, v6, Lcom/android/server/wifi/MiuiTcpInfo;->mUnacked:I

    add-int/2addr v5, v6

    iput v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I

    .line 343
    iget v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I

    iget-object v6, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mToRemovedSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v6, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;

    iget-object v6, v6, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v6, v6, Lcom/android/server/wifi/MiuiTcpInfo;->mRetans:I

    add-int/2addr v5, v6

    iput v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I

    .line 344
    iget v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I

    iget-object v6, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mToRemovedSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v6, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;

    iget-object v6, v6, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    iget v6, v6, Lcom/android/server/wifi/MiuiTcpInfo;->mLost:I

    add-int/2addr v5, v6

    iput v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I

    .line 339
    .end local v3    # "key":J
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 346
    .end local v2    # "i":I
    :cond_0
    return-object v1
.end method

.method public getLatestPacketFailPercentage()I
    .locals 1

    .line 393
    iget-object v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mDependencies:Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;

    invoke-virtual {v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->isTcpInfoParsingSupported()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    .line 395
    :cond_0
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mLatestPacketFailPercentage:I

    return v0
.end method

.method public getLatestReceivedCount()I
    .locals 1

    .line 457
    iget-object v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mDependencies:Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;

    invoke-virtual {v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->isTcpInfoParsingSupported()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    .line 458
    :cond_0
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mLatestReceivedCount:I

    return v0
.end method

.method public getLatestTcpStats()Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    .locals 1

    .line 399
    iget-object v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mLatestTcpStats:Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;

    return-object v0
.end method

.method public getSentSinceLastRecv()I
    .locals 1

    .line 451
    iget-object v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mDependencies:Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;

    invoke-virtual {v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->isTcpInfoParsingSupported()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    .line 452
    :cond_0
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSentSinceLastRecv:I

    return v0
.end method

.method public getSkipUidList()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 403
    iget-object v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSkipUidList:Ljava/util/Set;

    return-object v0
.end method

.method public getTcpInfo()Ljava/lang/String;
    .locals 1

    .line 311
    iget-object v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->msgRecord:Ljava/lang/String;

    return-object v0
.end method

.method parseSockInfo(Ljava/nio/ByteBuffer;IIJ)Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;
    .locals 13
    .param p1, "bytes"    # Ljava/nio/ByteBuffer;
    .param p2, "family"    # I
    .param p3, "nlmsgLen"    # I
    .param p4, "time"    # J

    .line 353
    move-object v7, p0

    move-object v8, p1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int v0, v0, p3

    add-int/lit8 v9, v0, -0x58

    .line 354
    .local v9, "remainingDataSize":I
    const/4 v0, 0x0

    .line 355
    .local v0, "tcpInfo":Lcom/android/server/wifi/MiuiTcpInfo;
    const/4 v1, 0x0

    move-object v10, v0

    move v11, v1

    .line 357
    .end local v0    # "tcpInfo":Lcom/android/server/wifi/MiuiTcpInfo;
    .local v10, "tcpInfo":Lcom/android/server/wifi/MiuiTcpInfo;
    .local v11, "mark":I
    :goto_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-ge v0, v9, :cond_2

    .line 358
    new-instance v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;

    .line 359
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;-><init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;SS)V

    .line 360
    .local v0, "rtattr":Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;
    invoke-virtual {v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;->getDataLength()S

    move-result v1

    .line 361
    .local v1, "dataLen":S
    iget-short v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;->rtaType:S

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 362
    invoke-static {p1, v1}, Lcom/android/server/wifi/MiuiTcpInfo;->parse(Ljava/nio/ByteBuffer;I)Lcom/android/server/wifi/MiuiTcpInfo;

    move-result-object v2

    move-object v10, v2

    .end local v10    # "tcpInfo":Lcom/android/server/wifi/MiuiTcpInfo;
    .local v2, "tcpInfo":Lcom/android/server/wifi/MiuiTcpInfo;
    goto :goto_1

    .line 363
    .end local v2    # "tcpInfo":Lcom/android/server/wifi/MiuiTcpInfo;
    .restart local v10    # "tcpInfo":Lcom/android/server/wifi/MiuiTcpInfo;
    :cond_0
    iget-short v2, v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;->rtaType:S

    const/16 v3, 0xf

    if-ne v2, v3, :cond_1

    .line 364
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    move v11, v2

    .end local v11    # "mark":I
    .local v2, "mark":I
    goto :goto_1

    .line 369
    .end local v2    # "mark":I
    .restart local v11    # "mark":I
    :cond_1
    invoke-direct {p0, p1, v1}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->skipRemainingAttributesBytesAligned(Ljava/nio/ByteBuffer;S)V

    .line 371
    .end local v0    # "rtattr":Lcom/android/server/wifi/MiuiTcpSocketTracker$RoutingAttribute;
    .end local v1    # "dataLen":S
    :goto_1
    goto :goto_0

    .line 373
    :cond_2
    new-instance v12, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;

    move-object v0, v12

    move-object v1, p0

    move-object v2, v10

    move v3, p2

    move v4, v11

    move-wide/from16 v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;-><init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;Lcom/android/server/wifi/MiuiTcpInfo;IIJ)V

    .line 374
    .local v0, "info":Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;
    return-object v0
.end method

.method public pollSocketsInfo(Ljava/util/Set;)Z
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .line 210
    .local p1, "uidList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    iget-object v0, v7, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mDependencies:Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;

    invoke-virtual {v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->isTcpInfoParsingSupported()Z

    move-result v0

    const/4 v9, 0x0

    if-nez v0, :cond_0

    return v9

    .line 211
    :cond_0
    const/4 v1, 0x0

    .line 212
    .local v1, "readBytes":I
    const/4 v2, 0x0

    .line 213
    .local v2, "fd":Ljava/io/FileDescriptor;
    iget-object v0, v7, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSkipUidList:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 214
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const-string v11, "MiuiTcpSocketTracker"

    if-eqz v0, :cond_2

    .line 215
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 216
    .local v0, "uid":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v8, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 217
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pollSocketsInfo remove old skip uid is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    invoke-interface {v10}, Ljava/util/Iterator;->remove()V

    .line 220
    .end local v0    # "uid":I
    :cond_1
    goto :goto_0

    .line 221
    :cond_2
    iget-object v0, v7, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mToRemovedSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 223
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    move-wide v12, v3

    .line 224
    .local v12, "time":J
    iget-object v0, v7, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mDependencies:Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;

    invoke-virtual {v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->connectToKernel()Ljava/io/FileDescriptor;

    move-result-object v0
    :try_end_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_e
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_e
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_e
    .catchall {:try_start_0 .. :try_end_0} :catchall_7

    move-object v14, v0

    .line 225
    .end local v2    # "fd":Ljava/io/FileDescriptor;
    .local v14, "fd":Ljava/io/FileDescriptor;
    :try_start_1
    new-instance v0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;

    invoke-direct {v0, v7}, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;-><init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;)V

    move-object v15, v0

    .line 226
    .local v15, "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    sget-object v5, Lcom/android/server/wifi/MiuiTcpSocketTracker;->ADDRESS_FAMILIES:[I

    array-length v6, v5

    move v3, v9

    :goto_1
    if-ge v3, v6, :cond_a

    aget v0, v5, v3

    move v2, v0

    .line 227
    .local v2, "family":I
    iget-object v0, v7, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mDependencies:Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;

    iget-object v4, v7, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSockDiagMsg:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    invoke-virtual {v0, v14, v4}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->sendPollingRequest(Ljava/io/FileDescriptor;[B)V

    .line 240
    :goto_2
    iget-object v0, v7, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mDependencies:Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;

    invoke-virtual {v0, v14}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->recvMessage(Ljava/io/FileDescriptor;)Ljava/nio/ByteBuffer;

    move-result-object v0

    move-object v4, v0

    .line 241
    .local v4, "bytes":Ljava/nio/ByteBuffer;
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0
    :try_end_1
    .catch Landroid/system/ErrnoException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1} :catch_d
    .catchall {:try_start_1 .. :try_end_1} :catchall_6

    move v1, v0

    .line 243
    :goto_3
    :try_start_2
    invoke-static {v4}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->enoughBytesRemainForValidNlMsg(Ljava/nio/ByteBuffer;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 244
    invoke-static {v4}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/nio/BufferUnderflowException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Landroid/system/ErrnoException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_9
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 245
    .local v0, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    if-nez v0, :cond_3

    .line 246
    :try_start_3
    const-string v9, "Badly formatted data."

    invoke-static {v11, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/system/ErrnoException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 247
    move/from16 v17, v2

    move/from16 v19, v3

    move-object v9, v4

    move-object/from16 v22, v5

    move/from16 v23, v6

    move-object/from16 v16, v10

    move v10, v1

    goto/16 :goto_5

    .line 305
    .end local v0    # "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .end local v2    # "family":I
    .end local v4    # "bytes":Ljava/nio/ByteBuffer;
    .end local v12    # "time":J
    .end local v15    # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    :catchall_0
    move-exception v0

    move-object/from16 v16, v10

    move-object v2, v14

    goto/16 :goto_9

    .line 302
    :catch_0
    move-exception v0

    move-object/from16 v16, v10

    move-object v2, v14

    goto/16 :goto_8

    .line 288
    .restart local v2    # "family":I
    .restart local v4    # "bytes":Ljava/nio/ByteBuffer;
    .restart local v12    # "time":J
    .restart local v15    # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    :catch_1
    move-exception v0

    move/from16 v17, v2

    move/from16 v19, v3

    move-object v9, v4

    move-object/from16 v22, v5

    move/from16 v23, v6

    move-object/from16 v16, v10

    move v10, v1

    goto/16 :goto_6

    .line 249
    .restart local v0    # "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    :cond_3
    :try_start_4
    iget v9, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Ljava/nio/BufferUnderflowException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Landroid/system/ErrnoException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/io/InterruptedIOException; {:try_start_4 .. :try_end_4} :catch_9
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-object/from16 v16, v4

    .end local v4    # "bytes":Ljava/nio/ByteBuffer;
    .local v16, "bytes":Ljava/nio/ByteBuffer;
    move v4, v9

    .line 251
    .local v4, "nlmsgLen":I
    :try_start_5
    iget-short v9, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_8
    .catch Ljava/nio/BufferUnderflowException; {:try_start_5 .. :try_end_5} :catch_8
    .catch Landroid/system/ErrnoException; {:try_start_5 .. :try_end_5} :catch_9
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_9
    .catch Ljava/io/InterruptedIOException; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move/from16 v17, v1

    .end local v1    # "readBytes":I
    .local v17, "readBytes":I
    const/4 v1, 0x3

    if-ne v9, v1, :cond_4

    .line 252
    move/from16 v19, v3

    move-object/from16 v22, v5

    move/from16 v23, v6

    move-object/from16 v9, v16

    move-object/from16 v16, v10

    move/from16 v10, v17

    move/from16 v17, v2

    goto/16 :goto_5

    .line 255
    :cond_4
    :try_start_6
    iget-short v1, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Ljava/nio/BufferUnderflowException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Landroid/system/ErrnoException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/io/InterruptedIOException; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    const/16 v9, 0x14

    if-eq v1, v9, :cond_5

    .line 256
    :try_start_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Expect to get family "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, " SOCK_DIAG_BY_FAMILY message but get "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-short v9, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v11, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/nio/BufferUnderflowException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Landroid/system/ErrnoException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/net/SocketException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/InterruptedIOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 259
    move/from16 v19, v3

    move-object/from16 v22, v5

    move/from16 v23, v6

    move-object/from16 v9, v16

    move-object/from16 v16, v10

    move/from16 v10, v17

    move/from16 v17, v2

    goto/16 :goto_5

    .line 305
    .end local v0    # "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .end local v2    # "family":I
    .end local v4    # "nlmsgLen":I
    .end local v12    # "time":J
    .end local v15    # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    .end local v16    # "bytes":Ljava/nio/ByteBuffer;
    :catchall_1
    move-exception v0

    move-object/from16 v16, v10

    move-object v2, v14

    move/from16 v1, v17

    goto/16 :goto_9

    .line 302
    :catch_2
    move-exception v0

    move-object/from16 v16, v10

    move-object v2, v14

    move/from16 v1, v17

    goto/16 :goto_8

    .line 288
    .restart local v2    # "family":I
    .restart local v12    # "time":J
    .restart local v15    # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    .restart local v16    # "bytes":Ljava/nio/ByteBuffer;
    :catch_3
    move-exception v0

    move/from16 v19, v3

    move-object/from16 v22, v5

    move/from16 v23, v6

    move-object/from16 v9, v16

    move-object/from16 v16, v10

    move/from16 v10, v17

    move/from16 v17, v2

    goto/16 :goto_6

    .line 262
    .restart local v0    # "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .restart local v4    # "nlmsgLen":I
    :cond_5
    :try_start_8
    invoke-static {v4}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->isValidInetDiagMsgSize(I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 266
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->position()I

    move-result v1
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_7
    .catch Ljava/nio/BufferUnderflowException; {:try_start_8 .. :try_end_8} :catch_7
    .catch Landroid/system/ErrnoException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/net/SocketException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/io/InterruptedIOException; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    add-int/lit8 v1, v1, 0x2c

    move-object/from16 v9, v16

    .end local v16    # "bytes":Ljava/nio/ByteBuffer;
    .local v9, "bytes":Ljava/nio/ByteBuffer;
    :try_start_9
    invoke-virtual {v9, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 268
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v18

    move-wide/from16 v20, v18

    .line 269
    .local v20, "cookie":J
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    add-int/lit8 v1, v1, 0xc

    invoke-virtual {v9, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 270
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    .line 273
    .local v1, "nlmsgUid":I
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->position()I

    move-result v16

    add-int/lit8 v16, v16, 0x48

    add-int/lit8 v16, v16, -0x2c

    add-int/lit8 v16, v16, -0x8

    add-int/lit8 v16, v16, -0xc

    move-object/from16 v18, v0

    .end local v0    # "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .local v18, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    add-int/lit8 v0, v16, -0x4

    invoke-virtual {v9, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/nio/BufferUnderflowException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Landroid/system/ErrnoException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljava/net/SocketException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljava/io/InterruptedIOException; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 277
    move v0, v1

    move-object/from16 v16, v10

    move/from16 v10, v17

    .end local v1    # "nlmsgUid":I
    .end local v17    # "readBytes":I
    .local v0, "nlmsgUid":I
    .local v10, "readBytes":I
    .local v16, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    move-object/from16 v1, p0

    move/from16 v17, v2

    .end local v2    # "family":I
    .local v17, "family":I
    move-object v2, v9

    move/from16 v19, v3

    move/from16 v3, v17

    move-object/from16 v22, v5

    move/from16 v23, v6

    move-wide v5, v12

    :try_start_a
    invoke-virtual/range {v1 .. v6}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->parseSockInfo(Ljava/nio/ByteBuffer;IIJ)Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;

    move-result-object v1

    .line 278
    .local v1, "info":Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 279
    move-object v4, v9

    move v1, v10

    move-object/from16 v10, v16

    move/from16 v2, v17

    move/from16 v3, v19

    move-object/from16 v5, v22

    move/from16 v6, v23

    const/4 v9, 0x0

    goto/16 :goto_3

    .line 282
    :cond_6
    nop

    .line 283
    move-wide/from16 v2, v20

    .end local v20    # "cookie":J
    .local v2, "cookie":J
    invoke-direct {v7, v1, v2, v3, v0}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->calculateLatestPacketsStat(Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;JI)Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;

    move-result-object v5

    .line 282
    invoke-virtual {v15, v5}, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->accumulate(Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;)V

    .line 284
    iget-object v5, v1, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    invoke-virtual {v15, v5}, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avg(Lcom/android/server/wifi/MiuiTcpInfo;)V

    .line 285
    iget-object v5, v7, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mSocketInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v5, v2, v3, v1}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
    :try_end_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/nio/BufferUnderflowException; {:try_start_a .. :try_end_a} :catch_4
    .catch Landroid/system/ErrnoException; {:try_start_a .. :try_end_a} :catch_b
    .catch Ljava/net/SocketException; {:try_start_a .. :try_end_a} :catch_b
    .catch Ljava/io/InterruptedIOException; {:try_start_a .. :try_end_a} :catch_b
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    goto :goto_4

    .line 288
    .end local v0    # "nlmsgUid":I
    .end local v1    # "info":Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;
    .end local v2    # "cookie":J
    .end local v4    # "nlmsgLen":I
    .end local v18    # "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    :catch_4
    move-exception v0

    goto/16 :goto_6

    .end local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .local v2, "family":I
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .local v17, "readBytes":I
    :catch_5
    move-exception v0

    move/from16 v19, v3

    move-object/from16 v22, v5

    move/from16 v23, v6

    move-object/from16 v16, v10

    move/from16 v10, v17

    move/from16 v17, v2

    .end local v2    # "family":I
    .local v10, "readBytes":I
    .restart local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .local v17, "family":I
    goto/16 :goto_6

    .line 262
    .end local v9    # "bytes":Ljava/nio/ByteBuffer;
    .local v0, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .restart local v2    # "family":I
    .restart local v4    # "nlmsgLen":I
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .local v16, "bytes":Ljava/nio/ByteBuffer;
    .local v17, "readBytes":I
    :cond_7
    move-object/from16 v18, v0

    move/from16 v19, v3

    move-object/from16 v22, v5

    move/from16 v23, v6

    move-object/from16 v9, v16

    move-object/from16 v16, v10

    move/from16 v10, v17

    move/from16 v17, v2

    .line 287
    .end local v0    # "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .end local v2    # "family":I
    .end local v4    # "nlmsgLen":I
    .restart local v9    # "bytes":Ljava/nio/ByteBuffer;
    .local v10, "readBytes":I
    .local v16, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .local v17, "family":I
    :goto_4
    move-object v4, v9

    move v1, v10

    move-object/from16 v10, v16

    move/from16 v2, v17

    move/from16 v3, v19

    move-object/from16 v5, v22

    move/from16 v6, v23

    const/4 v9, 0x0

    goto/16 :goto_3

    .line 305
    .end local v9    # "bytes":Ljava/nio/ByteBuffer;
    .end local v12    # "time":J
    .end local v15    # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    .end local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .local v17, "readBytes":I
    :catchall_2
    move-exception v0

    move-object/from16 v16, v10

    move/from16 v10, v17

    move v1, v10

    move-object v2, v14

    .end local v17    # "readBytes":I
    .local v10, "readBytes":I
    .restart local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    goto/16 :goto_9

    .line 302
    .end local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v17    # "readBytes":I
    :catch_6
    move-exception v0

    move-object/from16 v16, v10

    move/from16 v10, v17

    move v1, v10

    move-object v2, v14

    .end local v17    # "readBytes":I
    .local v10, "readBytes":I
    .restart local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    goto/16 :goto_8

    .line 288
    .restart local v2    # "family":I
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v12    # "time":J
    .restart local v15    # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    .local v16, "bytes":Ljava/nio/ByteBuffer;
    .restart local v17    # "readBytes":I
    :catch_7
    move-exception v0

    move/from16 v19, v3

    move-object/from16 v22, v5

    move/from16 v23, v6

    move-object/from16 v9, v16

    move-object/from16 v16, v10

    move/from16 v10, v17

    move/from16 v17, v2

    .end local v2    # "family":I
    .restart local v9    # "bytes":Ljava/nio/ByteBuffer;
    .local v10, "readBytes":I
    .local v16, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .local v17, "family":I
    goto :goto_6

    .end local v9    # "bytes":Ljava/nio/ByteBuffer;
    .end local v17    # "family":I
    .local v1, "readBytes":I
    .restart local v2    # "family":I
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .local v16, "bytes":Ljava/nio/ByteBuffer;
    :catch_8
    move-exception v0

    move/from16 v17, v2

    move/from16 v19, v3

    move-object/from16 v22, v5

    move/from16 v23, v6

    move-object/from16 v9, v16

    move-object/from16 v16, v10

    move v10, v1

    .end local v1    # "readBytes":I
    .end local v2    # "family":I
    .restart local v9    # "bytes":Ljava/nio/ByteBuffer;
    .local v10, "readBytes":I
    .local v16, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v17    # "family":I
    goto :goto_6

    .line 243
    .end local v9    # "bytes":Ljava/nio/ByteBuffer;
    .end local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v17    # "family":I
    .restart local v1    # "readBytes":I
    .restart local v2    # "family":I
    .local v4, "bytes":Ljava/nio/ByteBuffer;
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_8
    move/from16 v17, v2

    move/from16 v19, v3

    move-object v9, v4

    move-object/from16 v22, v5

    move/from16 v23, v6

    move-object/from16 v16, v10

    move v10, v1

    .line 292
    .end local v1    # "readBytes":I
    .end local v2    # "family":I
    .end local v4    # "bytes":Ljava/nio/ByteBuffer;
    .restart local v9    # "bytes":Ljava/nio/ByteBuffer;
    .local v10, "readBytes":I
    .restart local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v17    # "family":I
    :goto_5
    move/from16 v2, v17

    goto :goto_7

    .line 305
    .end local v9    # "bytes":Ljava/nio/ByteBuffer;
    .end local v12    # "time":J
    .end local v15    # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    .end local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v17    # "family":I
    .restart local v1    # "readBytes":I
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :catchall_3
    move-exception v0

    move-object/from16 v16, v10

    move v10, v1

    move-object v2, v14

    .end local v1    # "readBytes":I
    .local v10, "readBytes":I
    .restart local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    goto/16 :goto_9

    .line 302
    .end local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v1    # "readBytes":I
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :catch_9
    move-exception v0

    move-object/from16 v16, v10

    move v10, v1

    move-object v2, v14

    .end local v1    # "readBytes":I
    .local v10, "readBytes":I
    .restart local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    goto/16 :goto_8

    .line 288
    .end local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v1    # "readBytes":I
    .restart local v2    # "family":I
    .restart local v4    # "bytes":Ljava/nio/ByteBuffer;
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v12    # "time":J
    .restart local v15    # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    :catch_a
    move-exception v0

    move/from16 v17, v2

    move/from16 v19, v3

    move-object v9, v4

    move-object/from16 v22, v5

    move/from16 v23, v6

    move-object/from16 v16, v10

    move v10, v1

    .line 289
    .end local v1    # "readBytes":I
    .end local v2    # "family":I
    .end local v4    # "bytes":Ljava/nio/ByteBuffer;
    .local v0, "e":Ljava/lang/RuntimeException;
    .restart local v9    # "bytes":Ljava/nio/ByteBuffer;
    .local v10, "readBytes":I
    .restart local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v17    # "family":I
    :goto_6
    :try_start_b
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected socket info parsing, family "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v2, v17

    .end local v17    # "family":I
    .restart local v2    # "family":I
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " buffer:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 291
    invoke-static {}, Ljava/util/Base64;->getEncoder()Ljava/util/Base64$Encoder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Base64$Encoder;->encodeToString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 289
    invoke-static {v11, v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b
    .catch Landroid/system/ErrnoException; {:try_start_b .. :try_end_b} :catch_b
    .catch Ljava/net/SocketException; {:try_start_b .. :try_end_b} :catch_b
    .catch Ljava/io/InterruptedIOException; {:try_start_b .. :try_end_b} :catch_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    .line 293
    .end local v0    # "e":Ljava/lang/RuntimeException;
    .end local v9    # "bytes":Ljava/nio/ByteBuffer;
    :goto_7
    const/16 v1, 0x14

    if-gt v10, v1, :cond_9

    .line 226
    .end local v2    # "family":I
    add-int/lit8 v3, v19, 0x1

    move v1, v10

    move-object/from16 v10, v16

    move-object/from16 v5, v22

    move/from16 v6, v23

    const/4 v9, 0x0

    goto/16 :goto_1

    .line 293
    .restart local v2    # "family":I
    :cond_9
    move v1, v10

    move-object/from16 v10, v16

    move/from16 v3, v19

    move-object/from16 v5, v22

    move/from16 v6, v23

    const/4 v9, 0x0

    goto/16 :goto_2

    .line 305
    .end local v2    # "family":I
    .end local v12    # "time":J
    .end local v15    # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    :catchall_4
    move-exception v0

    move v1, v10

    move-object v2, v14

    goto :goto_9

    .line 302
    :catch_b
    move-exception v0

    move v1, v10

    move-object v2, v14

    goto :goto_8

    .line 296
    .end local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v1    # "readBytes":I
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v12    # "time":J
    .restart local v15    # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    :cond_a
    move-object/from16 v16, v10

    .end local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :try_start_c
    invoke-virtual {v15}, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->getFailPercent()I

    move-result v0

    iput v0, v7, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mLatestPacketFailPercentage:I

    .line 297
    invoke-virtual {v15}, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->printMsg()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/android/server/wifi/MiuiTcpSocketTracker;->msgRecord:Ljava/lang/String;

    .line 298
    iput-object v15, v7, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mLatestTcpStats:Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;

    .line 300
    invoke-direct {v7, v12, v13}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->cleanupSocketInfo(J)V
    :try_end_c
    .catch Landroid/system/ErrnoException; {:try_start_c .. :try_end_c} :catch_c
    .catch Ljava/net/SocketException; {:try_start_c .. :try_end_c} :catch_c
    .catch Ljava/io/InterruptedIOException; {:try_start_c .. :try_end_c} :catch_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    .line 301
    nop

    .line 305
    invoke-static {v14}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->closeSocketQuietly(Ljava/io/FileDescriptor;)V

    .line 301
    const/4 v0, 0x1

    return v0

    .line 305
    .end local v12    # "time":J
    .end local v15    # "stat":Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
    :catchall_5
    move-exception v0

    move-object v2, v14

    goto :goto_9

    .line 302
    :catch_c
    move-exception v0

    move-object v2, v14

    goto :goto_8

    .line 305
    .end local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :catchall_6
    move-exception v0

    move-object/from16 v16, v10

    move-object v2, v14

    .end local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    goto :goto_9

    .line 302
    .end local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :catch_d
    move-exception v0

    move-object/from16 v16, v10

    move-object v2, v14

    .end local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    goto :goto_8

    .line 305
    .end local v14    # "fd":Ljava/io/FileDescriptor;
    .end local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .local v2, "fd":Ljava/io/FileDescriptor;
    .restart local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :catchall_7
    move-exception v0

    move-object/from16 v16, v10

    .end local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    goto :goto_9

    .line 302
    .end local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :catch_e
    move-exception v0

    move-object/from16 v16, v10

    .line 303
    .end local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .local v0, "e":Ljava/lang/Exception;
    .restart local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :goto_8
    :try_start_d
    const-string v3, "Fail to get TCP info via netlink."

    invoke-static {v11, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_8

    .line 305
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->closeSocketQuietly(Ljava/io/FileDescriptor;)V

    .line 306
    nop

    .line 307
    const/4 v3, 0x0

    return v3

    .line 305
    :catchall_8
    move-exception v0

    :goto_9
    invoke-static {v2}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->closeSocketQuietly(Ljava/io/FileDescriptor;)V

    .line 306
    throw v0
.end method

.method public quit()V
    .locals 2

    .line 200
    iget-object v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mDependencies:Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;

    iget-object v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker;->mConfigListener:Landroid/provider/DeviceConfig$OnPropertiesChangedListener;

    invoke-virtual {v0, v1}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->removeDeviceConfigChangedListener(Landroid/provider/DeviceConfig$OnPropertiesChangedListener;)V

    .line 201
    return-void
.end method
