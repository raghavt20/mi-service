.class Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;
.super Ljava/lang/Object;
.source "MiuiTcpSocketTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wifi/MiuiTcpSocketTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SocketInfo"
.end annotation


# static fields
.field public static final INIT_MARK_VALUE:I


# instance fields
.field public final fwmark:I

.field public final ipFamily:I

.field public final tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

.field final synthetic this$0:Lcom/android/server/wifi/MiuiTcpSocketTracker;

.field public final updateTime:J


# direct methods
.method constructor <init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;Lcom/android/server/wifi/MiuiTcpInfo;IIJ)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wifi/MiuiTcpSocketTracker;
    .param p2, "info"    # Lcom/android/server/wifi/MiuiTcpInfo;
    .param p3, "family"    # I
    .param p4, "mark"    # I
    .param p5, "time"    # J

    .line 581
    iput-object p1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->this$0:Lcom/android/server/wifi/MiuiTcpSocketTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 582
    iput-object p2, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    .line 583
    iput p3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->ipFamily:I

    .line 584
    iput-wide p5, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->updateTime:J

    .line 585
    iput p4, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->fwmark:I

    .line 586
    return-void
.end method

.method private ipTypeToString(I)Ljava/lang/String;
    .locals 1
    .param p1, "type"    # I

    .line 595
    sget v0, Landroid/system/OsConstants;->AF_INET:I

    if-ne p1, v0, :cond_0

    .line 596
    const-string v0, "IP"

    return-object v0

    .line 597
    :cond_0
    sget v0, Landroid/system/OsConstants;->AF_INET6:I

    if-ne p1, v0, :cond_1

    .line 598
    const-string v0, "IPV6"

    return-object v0

    .line 600
    :cond_1
    const-string v0, "UNKNOWN"

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 590
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SocketInfo {Type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->ipFamily:I

    invoke-direct {p0, v1}, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->ipTypeToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->tcpInfo:Lcom/android/server/wifi/MiuiTcpInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mark:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->fwmark:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " updated at "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$SocketInfo;->updateTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
