.class Lcom/android/server/wifi/ArpPacket;
.super Ljava/lang/Object;
.source "ArpPacket.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wifi/ArpPacket$ParseException;
    }
.end annotation


# static fields
.field public static final ARP_ETHER_IPV4_LEN:I = 0x2a

.field public static final ARP_HWTYPE_ETHER:I = 0x1

.field public static final ARP_PAYLOAD_LEN:I = 0x1c

.field public static final ETHER_ADDR_LEN:I = 0x6

.field public static final ETHER_ANY:[B

.field public static final ETHER_BROADCAST:[B

.field public static final ETHER_HEADER_LEN:I = 0xe

.field public static final INADDR_ANY:Ljava/net/Inet4Address;

.field public static final IPV4_ADDR_LEN:I = 0x4

.field public static final OPCODE_ARP_REQUEST:S = 0x1s

.field public static final OPCODE_ARP_RESPONSE:S = 0x2s


# instance fields
.field public final mOpCode:S

.field public final mSenderHwAddress:[B

.field public final mSenderIp:Ljava/net/Inet4Address;

.field public final mTargetIp:Ljava/net/Inet4Address;

.field public final mTtargetHwAddress:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 21
    sget-object v0, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    check-cast v0, Ljava/net/Inet4Address;

    sput-object v0, Lcom/android/server/wifi/ArpPacket;->INADDR_ANY:Ljava/net/Inet4Address;

    .line 22
    const/4 v0, 0x6

    new-array v1, v0, [B

    fill-array-data v1, :array_0

    sput-object v1, Lcom/android/server/wifi/ArpPacket;->ETHER_BROADCAST:[B

    .line 26
    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/server/wifi/ArpPacket;->ETHER_ANY:[B

    return-void

    :array_0
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method constructor <init>(S[BLjava/net/Inet4Address;[BLjava/net/Inet4Address;)V
    .locals 0
    .param p1, "opCode"    # S
    .param p2, "senderHwAddress"    # [B
    .param p3, "senderIp"    # Ljava/net/Inet4Address;
    .param p4, "targetHwAddress"    # [B
    .param p5, "targetIp"    # Ljava/net/Inet4Address;

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-short p1, p0, Lcom/android/server/wifi/ArpPacket;->mOpCode:S

    .line 65
    iput-object p2, p0, Lcom/android/server/wifi/ArpPacket;->mSenderHwAddress:[B

    .line 66
    iput-object p3, p0, Lcom/android/server/wifi/ArpPacket;->mSenderIp:Ljava/net/Inet4Address;

    .line 67
    iput-object p4, p0, Lcom/android/server/wifi/ArpPacket;->mTtargetHwAddress:[B

    .line 68
    iput-object p5, p0, Lcom/android/server/wifi/ArpPacket;->mTargetIp:Ljava/net/Inet4Address;

    .line 69
    return-void
.end method

.method public static buildArpPacket([B[B[B[B[B[BS)Ljava/nio/ByteBuffer;
    .locals 2
    .param p0, "dstMac"    # [B
    .param p1, "srcMac"    # [B
    .param p2, "sendHwAddress"    # [B
    .param p3, "senderIp"    # [B
    .param p4, "targetHwAddress"    # [B
    .param p5, "targetIp"    # [B
    .param p6, "opCode"    # S

    .line 79
    const/16 v0, 0x2a

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 80
    .local v0, "buf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 81
    sget-object v1, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 84
    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 85
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 86
    sget v1, Landroid/system/OsConstants;->ETH_P_ARP:I

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 89
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 90
    sget v1, Landroid/system/OsConstants;->ETH_P_IP:I

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 91
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 92
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 93
    invoke-virtual {v0, p6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 94
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 95
    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 96
    invoke-virtual {v0, p4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 97
    invoke-virtual {v0, p5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 98
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 99
    return-object v0
.end method

.method public static parseArpPacket([BI)Lcom/android/server/wifi/ArpPacket;
    .locals 22
    .param p0, "recvbuf"    # [B
    .param p1, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/server/wifi/ArpPacket$ParseException;
        }
    .end annotation

    .line 108
    move-object/from16 v1, p0

    move/from16 v2, p1

    const/16 v0, 0x2a

    if-lt v2, v0, :cond_7

    :try_start_0
    array-length v0, v1

    if-lt v0, v2, :cond_7

    .line 112
    const/4 v0, 0x0

    invoke-static {v1, v0, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v3, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    .line 113
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 115
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    const/4 v3, 0x6

    new-array v4, v3, [B

    .line 116
    .local v4, "l2dst":[B
    new-array v5, v3, [B

    .line 117
    .local v5, "l2src":[B
    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 118
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 120
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v6

    .line 121
    .local v6, "etherType":S
    sget v7, Landroid/system/OsConstants;->ETH_P_ARP:I

    if-ne v6, v7, :cond_6

    .line 125
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v7

    .line 126
    .local v7, "hwType":S
    const/4 v8, 0x1

    if-ne v7, v8, :cond_5

    .line 130
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v9

    .line 131
    .local v9, "protoType":S
    sget v10, Landroid/system/OsConstants;->ETH_P_IP:I

    if-ne v9, v10, :cond_4

    .line 135
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v10

    .line 136
    .local v10, "hwAddrLength":B
    if-ne v10, v3, :cond_3

    .line 141
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v11

    .line 142
    .local v11, "ipAddrLength":B
    const/4 v12, 0x4

    if-ne v11, v12, :cond_2

    .line 147
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v13

    .line 148
    .local v13, "opCode":S
    if-eq v13, v8, :cond_1

    const/4 v8, 0x2

    if-ne v13, v8, :cond_0

    goto :goto_0

    .line 149
    :cond_0
    new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Incorrect opCode: "

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V

    .end local p0    # "recvbuf":[B
    .end local p1    # "length":I
    throw v3

    .line 152
    .restart local p0    # "recvbuf":[B
    .restart local p1    # "length":I
    :cond_1
    :goto_0
    new-array v8, v3, [B

    .line 153
    .local v8, "senderHwAddress":[B
    new-array v14, v12, [B

    move-object v15, v14

    .line 154
    .local v15, "senderIp":[B
    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 155
    invoke-virtual {v0, v15}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 157
    new-array v3, v3, [B

    .line 158
    .local v3, "targetHwAddress":[B
    new-array v12, v12, [B

    .line 159
    .local v12, "targetIp":[B
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 160
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 162
    new-instance v20, Lcom/android/server/wifi/ArpPacket;

    .line 163
    invoke-static {v15}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v14

    move-object/from16 v17, v14

    check-cast v17, Ljava/net/Inet4Address;

    .line 164
    invoke-static {v12}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v14

    move-object/from16 v19, v14

    check-cast v19, Ljava/net/Inet4Address;

    move-object/from16 v14, v20

    move-object/from16 v21, v15

    .end local v15    # "senderIp":[B
    .local v21, "senderIp":[B
    move v15, v13

    move-object/from16 v16, v8

    move-object/from16 v18, v3

    invoke-direct/range {v14 .. v19}, Lcom/android/server/wifi/ArpPacket;-><init>(S[BLjava/net/Inet4Address;[BLjava/net/Inet4Address;)V

    .line 162
    return-object v20

    .line 143
    .end local v3    # "targetHwAddress":[B
    .end local v8    # "senderHwAddress":[B
    .end local v12    # "targetIp":[B
    .end local v13    # "opCode":S
    .end local v21    # "senderIp":[B
    :cond_2
    new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Incorrect Protocol address length: "

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V

    .end local p0    # "recvbuf":[B
    .end local p1    # "length":I
    throw v3

    .line 137
    .end local v11    # "ipAddrLength":B
    .restart local p0    # "recvbuf":[B
    .restart local p1    # "length":I
    :cond_3
    new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Incorrect HW address length: "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V

    .end local p0    # "recvbuf":[B
    .end local p1    # "length":I
    throw v3

    .line 132
    .end local v10    # "hwAddrLength":B
    .restart local p0    # "recvbuf":[B
    .restart local p1    # "length":I
    :cond_4
    new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Incorrect Protocol Type: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V

    .end local p0    # "recvbuf":[B
    .end local p1    # "length":I
    throw v3

    .line 127
    .end local v9    # "protoType":S
    .restart local p0    # "recvbuf":[B
    .restart local p1    # "length":I
    :cond_5
    new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Incorrect HW Type: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V

    .end local p0    # "recvbuf":[B
    .end local p1    # "length":I
    throw v3

    .line 122
    .end local v7    # "hwType":S
    .restart local p0    # "recvbuf":[B
    .restart local p1    # "length":I
    :cond_6
    new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Incorrect Ether Type: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v7}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V

    .end local p0    # "recvbuf":[B
    .end local p1    # "length":I
    throw v3

    .line 109
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v4    # "l2dst":[B
    .end local v5    # "l2src":[B
    .end local v6    # "etherType":S
    .restart local p0    # "recvbuf":[B
    .restart local p1    # "length":I
    :cond_7
    new-instance v0, Lcom/android/server/wifi/ArpPacket$ParseException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid packet length: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V

    .end local p0    # "recvbuf":[B
    .end local p1    # "length":I
    throw v0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    .restart local p0    # "recvbuf":[B
    .restart local p1    # "length":I
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Ljava/net/UnknownHostException;
    new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException;

    const-string v4, "Invalid IP address of Host"

    invoke-direct {v3, v4}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 170
    .end local v0    # "e":Ljava/net/UnknownHostException;
    :catch_1
    move-exception v0

    .line 171
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException;

    const-string v4, "Invalid MAC address representation"

    invoke-direct {v3, v4}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 168
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 169
    .local v0, "e":Ljava/nio/BufferUnderflowException;
    new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException;

    const-string v4, "Invalid buffer position"

    invoke-direct {v3, v4}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 165
    .end local v0    # "e":Ljava/nio/BufferUnderflowException;
    :catch_3
    move-exception v0

    .line 166
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    new-instance v3, Lcom/android/server/wifi/ArpPacket$ParseException;

    const-string v4, "Invalid index when wrapping a byte array into a buffer"

    invoke-direct {v3, v4}, Lcom/android/server/wifi/ArpPacket$ParseException;-><init>(Ljava/lang/String;)V

    throw v3
.end method
