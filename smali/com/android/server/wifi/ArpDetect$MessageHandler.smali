.class Lcom/android/server/wifi/ArpDetect$MessageHandler;
.super Landroid/os/Handler;
.source "ArpDetect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wifi/ArpDetect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MessageHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wifi/ArpDetect;


# direct methods
.method constructor <init>(Lcom/android/server/wifi/ArpDetect;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wifi/ArpDetect;

    .line 663
    iput-object p1, p0, Lcom/android/server/wifi/ArpDetect$MessageHandler;->this$0:Lcom/android/server/wifi/ArpDetect;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 667
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 668
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 674
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect$MessageHandler;->this$0:Lcom/android/server/wifi/ArpDetect;

    invoke-virtual {v0}, Lcom/android/server/wifi/ArpDetect;->resetGatewayNeighState()Z

    .line 675
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect$MessageHandler;->this$0:Lcom/android/server/wifi/ArpDetect;

    sget v1, Lcom/android/server/wifi/ArpDetect;->STATE_MULTI_GW_RECOVERY_STOPED:I

    invoke-static {v0, v1}, Lcom/android/server/wifi/ArpDetect;->-$$Nest$msetMultiGwRecoveryState(Lcom/android/server/wifi/ArpDetect;I)V

    .line 676
    goto :goto_0

    .line 670
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect$MessageHandler;->this$0:Lcom/android/server/wifi/ArpDetect;

    const-string/jumbo v1, "stop recv arp packet"

    invoke-static {v0, v1}, Lcom/android/server/wifi/ArpDetect;->-$$Nest$mlogd(Lcom/android/server/wifi/ArpDetect;Ljava/lang/String;)V

    .line 671
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect$MessageHandler;->this$0:Lcom/android/server/wifi/ArpDetect;

    invoke-static {v0}, Lcom/android/server/wifi/ArpDetect;->-$$Nest$munregisterArpRecvQueue(Lcom/android/server/wifi/ArpDetect;)V

    .line 672
    nop

    .line 680
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
