class com.android.server.wifi.MiuiTcpSocketTracker$1 implements android.provider.DeviceConfig$OnPropertiesChangedListener {
	 /* .source "MiuiTcpSocketTracker.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wifi/MiuiTcpSocketTracker; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wifi.MiuiTcpSocketTracker this$0; //synthetic
/* # direct methods */
 com.android.server.wifi.MiuiTcpSocketTracker$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wifi/MiuiTcpSocketTracker; */
/* .line 143 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onPropertiesChanged ( android.provider.DeviceConfig$Properties p0 ) {
/* .locals 5 */
/* .param p1, "properties" # Landroid/provider/DeviceConfig$Properties; */
/* .line 146 */
v0 = this.this$0;
com.android.server.wifi.MiuiTcpSocketTracker .-$$Nest$fgetmDependencies ( v0 );
/* const-string/jumbo v2, "tcp_min_packets_threshold" */
/* const/16 v3, 0xa */
final String v4 = "connectivity"; // const-string v4, "connectivity"
v1 = (( com.android.server.wifi.MiuiTcpSocketTracker$Dependencies ) v1 ).getDeviceConfigPropertyInt ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->getDeviceConfigPropertyInt(Ljava/lang/String;Ljava/lang/String;I)I
com.android.server.wifi.MiuiTcpSocketTracker .-$$Nest$fputmMinPacketsThreshold ( v0,v1 );
/* .line 150 */
v0 = this.this$0;
com.android.server.wifi.MiuiTcpSocketTracker .-$$Nest$fgetmDependencies ( v0 );
/* const-string/jumbo v2, "tcp_packets_fail_percentage" */
/* const/16 v3, 0x50 */
v1 = (( com.android.server.wifi.MiuiTcpSocketTracker$Dependencies ) v1 ).getDeviceConfigPropertyInt ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/wifi/MiuiTcpSocketTracker$Dependencies;->getDeviceConfigPropertyInt(Ljava/lang/String;Ljava/lang/String;I)I
com.android.server.wifi.MiuiTcpSocketTracker .-$$Nest$fputmTcpPacketsFailRateThreshold ( v0,v1 );
/* .line 154 */
return;
} // .end method
