.class public Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;
.super Ljava/lang/Object;
.source "MiuiTcpSocketTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wifi/MiuiTcpSocketTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TcpStat"
.end annotation


# instance fields
.field public avgRcvRtt:I

.field public avgRtt:I

.field public avgRttVar:I

.field public lostCount:I

.field public minRtt:I

.field public recvCount:I

.field public retans:I

.field public retransmit:I

.field public sentCount:I

.field public socketCnt:I

.field public tcpCount:I

.field final synthetic this$0:Lcom/android/server/wifi/MiuiTcpSocketTracker;

.field public totalretrans:I

.field public unacked:I


# direct methods
.method public constructor <init>(Lcom/android/server/wifi/MiuiTcpSocketTracker;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/wifi/MiuiTcpSocketTracker;

    .line 513
    iput-object p1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->this$0:Lcom/android/server/wifi/MiuiTcpSocketTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 505
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->socketCnt:I

    .line 507
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRtt:I

    .line 508
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRttVar:I

    .line 509
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRcvRtt:I

    .line 510
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->minRtt:I

    .line 511
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->tcpCount:I

    .line 514
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I

    .line 515
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I

    .line 516
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I

    .line 517
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I

    .line 518
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I

    .line 519
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I

    .line 520
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I

    .line 522
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRtt:I

    .line 523
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRttVar:I

    .line 524
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRcvRtt:I

    .line 525
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->minRtt:I

    .line 526
    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->tcpCount:I

    .line 527
    return-void
.end method


# virtual methods
.method accumulate(Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;)V
    .locals 2
    .param p1, "stat"    # Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;

    .line 529
    if-nez p1, :cond_0

    return-void

    .line 530
    :cond_0
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I

    iget v1, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I

    .line 531
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I

    iget v1, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I

    .line 532
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I

    iget v1, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I

    .line 533
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I

    iget v1, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I

    .line 534
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I

    iget v1, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I

    .line 535
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I

    iget v1, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I

    .line 536
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I

    iget v1, p1, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I

    .line 537
    return-void
.end method

.method avg(Lcom/android/server/wifi/MiuiTcpInfo;)V
    .locals 3
    .param p1, "tcpInfo"    # Lcom/android/server/wifi/MiuiTcpInfo;

    .line 540
    if-nez p1, :cond_0

    return-void

    .line 541
    :cond_0
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRtt:I

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->tcpCount:I

    mul-int/2addr v0, v1

    iget v1, p1, Lcom/android/server/wifi/MiuiTcpInfo;->mRtt:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->tcpCount:I

    add-int/lit8 v2, v1, 0x1

    div-int/2addr v0, v2

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRtt:I

    .line 542
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRttVar:I

    mul-int/2addr v0, v1

    iget v1, p1, Lcom/android/server/wifi/MiuiTcpInfo;->mRttVar:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->tcpCount:I

    add-int/lit8 v2, v1, 0x1

    div-int/2addr v0, v2

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRttVar:I

    .line 543
    iget v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRtt:I

    mul-int/2addr v0, v1

    iget v1, p1, Lcom/android/server/wifi/MiuiTcpInfo;->mRcvRtt:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->tcpCount:I

    add-int/lit8 v2, v1, 0x1

    div-int/2addr v0, v2

    iput v0, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->avgRcvRtt:I

    .line 545
    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->tcpCount:I

    .line 546
    return-void
.end method

.method public getFailPercent()I
    .locals 4

    .line 555
    const/4 v0, -0x1

    .line 556
    .local v0, "failpercent":I
    iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I

    if-lez v1, :cond_1

    .line 557
    iget v2, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I

    iget v3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I

    add-int/2addr v2, v3

    const/16 v3, 0x64

    mul-int/2addr v2, v3

    div-int/2addr v2, v1

    .line 558
    .end local v0    # "failpercent":I
    .local v2, "failpercent":I
    if-le v2, v3, :cond_0

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    move v0, v3

    .line 560
    .end local v2    # "failpercent":I
    .restart local v0    # "failpercent":I
    :cond_1
    return v0
.end method

.method public printMsg()Ljava/lang/String;
    .locals 2

    .line 549
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " failpercent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->this$0:Lcom/android/server/wifi/MiuiTcpSocketTracker;

    invoke-static {v1}, Lcom/android/server/wifi/MiuiTcpSocketTracker;->-$$Nest$fgetmLatestPacketFailPercentage(Lcom/android/server/wifi/MiuiTcpSocketTracker;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " { out="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->sentCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " in="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->recvCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->lostCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " inretrans="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retans:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " total retrans="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->totalretrans:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " tmo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->retransmit:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " inflight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wifi/MiuiTcpSocketTracker$TcpStat;->unacked:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
