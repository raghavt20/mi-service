.class public Lcom/android/server/wifi/ArpDetect;
.super Ljava/lang/Object;
.source "ArpDetect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wifi/ArpDetect$MessageHandler;
    }
.end annotation


# static fields
.field private static final ARP_DETECT_TIMEOUT_MS:I = 0x1f40

.field private static final ARP_GATEWAY_DETECT_TIMES:I = 0x6

.field private static final ARP_PACKET_INTERVAL_NORMAL_MS:I = 0x1f4

.field private static final ARP_PACKET_INTERVAL_QUICK_MS:I = 0x32

.field private static final CLOUD_MULTI_GW_DETECT_ENABLED:Ljava/lang/String; = "cloud_multi_gateway_detect_enabled"

.field private static final DEBUG:Z = false

.field private static final EXTRA_ARP_DETECT_GATEWAY_SIZE:Ljava/lang/String; = "extra_arp_detect_gateway_size"

.field private static final FD_EVENTS:I = 0x5

.field private static final IPV4_ADDR_ANY:Ljava/net/Inet4Address;

.field private static final MAX_GATEWAY_LIST_LEN:I = 0x4

.field private static final MAX_PACKET_LEN:I = 0x5dc

.field public static final MULTI_GATEWAY_DETECT_STATE_CHANGED:Ljava/lang/String; = "android.net.wifi.GATEWAY_DETECT_STATE_CHANGED"

.field private static final MULTI_GW_RECOVERY_TIMEOUT_MS:I = 0x1d4c0

.field public static STATE_MULTI_GW_RECOVERY_STARTED:I = 0x0

.field public static STATE_MULTI_GW_RECOVERY_STOPED:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ArpDetect"

.field private static final UNREGISTER_THIS_FD:I

.field private static sIntance:Lcom/android/server/wifi/ArpDetect;


# instance fields
.field private final CMD_MULTI_GW_RECOVERY_STOP:I

.field private final CMD_STOP_RECV_ARP_PACKET:I

.field private isMultiGwFind:Z

.field private mArpRecvSock:Ljava/io/FileDescriptor;

.field private mArpSendSock:Ljava/io/FileDescriptor;

.field private mContext:Landroid/content/Context;

.field private mCurRecoveryState:I

.field private mGatewayAddress:Ljava/net/Inet4Address;

.field private mGatewayChangeCount:I

.field private mGatewayMacList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "[B>;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mIfaceName:Ljava/lang/String;

.field private mInterfaceBroadcastAddr:Ljava/net/SocketAddress;

.field private mLocalIpAddress:Ljava/net/Inet4Address;

.field private mLocalMacAddress:[B

.field private mNetworkId:I

.field private mQueue:Landroid/os/MessageQueue;

.field private msgHandler:Lcom/android/server/wifi/ArpDetect$MessageHandler;

.field private stopDetect:Z


# direct methods
.method public static synthetic $r8$lambda$1dmcpER0oVUAqBGZYlT-XOrbP8I(Lcom/android/server/wifi/ArpDetect;Ljava/io/FileDescriptor;I)I
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wifi/ArpDetect;->lambda$registerArpRecvQueue$0(Ljava/io/FileDescriptor;I)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mhidenPrivateInfo(Lcom/android/server/wifi/ArpDetect;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mlogd(Lcom/android/server/wifi/ArpDetect;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wifi/ArpDetect;->logd(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msendArpPacket(Lcom/android/server/wifi/ArpDetect;[BLjava/net/Inet4Address;[BLjava/net/Inet4Address;II)Z
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/android/server/wifi/ArpDetect;->sendArpPacket([BLjava/net/Inet4Address;[BLjava/net/Inet4Address;II)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msetMultiGwRecoveryState(Lcom/android/server/wifi/ArpDetect;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wifi/ArpDetect;->setMultiGwRecoveryState(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$munregisterArpRecvQueue(Lcom/android/server/wifi/ArpDetect;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->unregisterArpRecvQueue()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 70
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/net/module/util/Inet4AddressUtils;->intToInet4AddressHTH(I)Ljava/net/Inet4Address;

    move-result-object v1

    sput-object v1, Lcom/android/server/wifi/ArpDetect;->IPV4_ADDR_ANY:Ljava/net/Inet4Address;

    .line 81
    sput v0, Lcom/android/server/wifi/ArpDetect;->STATE_MULTI_GW_RECOVERY_STOPED:I

    .line 82
    const/4 v0, 0x1

    sput v0, Lcom/android/server/wifi/ArpDetect;->STATE_MULTI_GW_RECOVERY_STARTED:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/16 v0, 0x64

    iput v0, p0, Lcom/android/server/wifi/ArpDetect;->CMD_STOP_RECV_ARP_PACKET:I

    .line 79
    const/16 v0, 0x65

    iput v0, p0, Lcom/android/server/wifi/ArpDetect;->CMD_MULTI_GW_RECOVERY_STOP:I

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->isMultiGwFind:Z

    .line 91
    iput-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->stopDetect:Z

    .line 92
    iput v0, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayChangeCount:I

    .line 93
    sget v0, Lcom/android/server/wifi/ArpDetect;->STATE_MULTI_GW_RECOVERY_STOPED:I

    iput v0, p0, Lcom/android/server/wifi/ArpDetect;->mCurRecoveryState:I

    .line 99
    iput-object p1, p0, Lcom/android/server/wifi/ArpDetect;->mContext:Landroid/content/Context;

    .line 100
    return-void
.end method

.method private checkIsMultiGwFind()V
    .locals 5

    .line 554
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    .line 555
    const-string v0, "MultiGateway detected!"

    const-string v1, "ArpDetect"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->isMultiGwFind:Z

    .line 557
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->msgHandler:Lcom/android/server/wifi/ArpDetect$MessageHandler;

    const/16 v2, 0x65

    const-wide/32 v3, 0x1d4c0

    invoke-virtual {v0, v2, v3, v4}, Lcom/android/server/wifi/ArpDetect$MessageHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 560
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 561
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mGatewayMacList["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    .line 562
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    invoke-static {v3}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ,size:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    .line 563
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 561
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 565
    .end local v0    # "i":I
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.net.wifi.GATEWAY_DETECT_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 566
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const-string v2, "extra_arp_detect_gateway_size"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 567
    iget-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->SYSTEM:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 569
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method private closeSocket(Ljava/io/FileDescriptor;)V
    .locals 3
    .param p1, "fd"    # Ljava/io/FileDescriptor;

    .line 645
    if-eqz p1, :cond_0

    .line 646
    :try_start_0
    invoke-static {p1}, Landroid/net/util/SocketUtils;->closeSocket(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 648
    :catch_0
    move-exception v0

    .line 649
    .local v0, "ignored":Ljava/io/IOException;
    const-string v1, "ArpDetect"

    const-string v2, "close socket fail"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 650
    .end local v0    # "ignored":Ljava/io/IOException;
    :cond_0
    :goto_0
    nop

    .line 651
    :goto_1
    return-void
.end method

.method private getCurGatewayMacFromRoute()[B
    .locals 2

    .line 419
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayAddress:Ljava/net/Inet4Address;

    invoke-virtual {v0}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mIfaceName:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/server/wifi/ArpDetect;->getMacAddrFromRoute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 420
    .local v0, "curGatewayMac":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 421
    sget-object v1, Llibcore/util/EmptyArray;->BYTE:[B

    return-object v1

    .line 423
    :cond_0
    invoke-static {v0}, Landroid/net/MacAddress;->byteAddrFromStringAddr(Ljava/lang/String;)[B

    move-result-object v1

    return-object v1
.end method

.method public static getInstance()Lcom/android/server/wifi/ArpDetect;
    .locals 1

    .line 108
    sget-object v0, Lcom/android/server/wifi/ArpDetect;->sIntance:Lcom/android/server/wifi/ArpDetect;

    return-object v0
.end method

.method private getMacAddrFromRoute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "ipAddress"    # Ljava/lang/String;
    .param p2, "ifaceName"    # Ljava/lang/String;

    .line 508
    const-string v0, "ArpDetect"

    if-eqz p1, :cond_7

    if-nez p2, :cond_0

    goto/16 :goto_7

    .line 512
    :cond_0
    const/4 v1, 0x0

    .line 513
    .local v1, "macAddress":Ljava/lang/String;
    const/4 v2, 0x0

    .line 515
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    const-string v5, "/proc/net/arp"

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v2, v3

    .line 517
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 518
    .local v3, "line":Ljava/lang/String;
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    if-eqz v4, :cond_3

    .line 519
    const-string v4, "[ ]+"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 520
    .local v4, "tokens":[Ljava/lang/String;
    array-length v5, v4

    const/4 v6, 0x6

    if-ge v5, v6, :cond_1

    .line 521
    goto :goto_0

    .line 525
    :cond_1
    const/4 v5, 0x0

    aget-object v5, v4, v5

    .line 526
    .local v5, "ip":Ljava/lang/String;
    const/4 v6, 0x3

    aget-object v6, v4, v6

    .line 527
    .local v6, "mac":Ljava/lang/String;
    const/4 v7, 0x5

    aget-object v7, v4, v7

    .line 529
    .local v7, "curIfaceName":Ljava/lang/String;
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 530
    move-object v1, v6

    .line 531
    goto :goto_1

    .line 533
    .end local v4    # "tokens":[Ljava/lang/String;
    .end local v5    # "ip":Ljava/lang/String;
    .end local v6    # "mac":Ljava/lang/String;
    .end local v7    # "curIfaceName":Ljava/lang/String;
    :cond_2
    goto :goto_0

    .line 534
    :cond_3
    :goto_1
    if-nez v1, :cond_4

    .line 535
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Did not find remoteAddress {"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "} in /proc/net/arp"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 543
    .end local v3    # "line":Ljava/lang/String;
    :cond_4
    nop

    .line 544
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 548
    :cond_5
    :goto_2
    goto :goto_3

    .line 546
    :catch_0
    move-exception v0

    .line 549
    goto :goto_3

    .line 542
    :catchall_0
    move-exception v0

    goto :goto_4

    .line 539
    :catch_1
    move-exception v3

    .line 540
    .local v3, "e":Ljava/io/IOException;
    :try_start_2
    const-string v4, "Could not read /proc/net/arp to lookup mac address"

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 543
    .end local v3    # "e":Ljava/io/IOException;
    if-eqz v2, :cond_5

    .line 544
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 537
    :catch_2
    move-exception v3

    .line 538
    .local v3, "e":Ljava/io/FileNotFoundException;
    :try_start_4
    const-string v4, "Could not open /proc/net/arp to lookup mac address"

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 543
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    if-eqz v2, :cond_5

    .line 544
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_2

    .line 550
    :goto_3
    return-object v1

    .line 543
    :goto_4
    if-eqz v2, :cond_6

    .line 544
    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_5

    .line 546
    :catch_3
    move-exception v3

    goto :goto_6

    .line 548
    :cond_6
    :goto_5
    nop

    .line 549
    :goto_6
    throw v0

    .line 509
    .end local v1    # "macAddress":Ljava/lang/String;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    :cond_7
    :goto_7
    const/4 v0, 0x0

    return-object v0
.end method

.method private getNetworkInterfaceByName(Ljava/lang/String;)Ljava/net/NetworkInterface;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .line 125
    :try_start_0
    invoke-static {p1}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ArpDetect"

    const-string v2, "get NetworkInterface faile:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 128
    const/4 v1, 0x0

    return-object v1
.end method

.method private getNextGatewayIndex()I
    .locals 5

    .line 427
    const/4 v0, -0x1

    .line 428
    .local v0, "curGatewayIndex":I
    const/4 v1, -0x1

    .line 430
    .local v1, "nextGatewayIndex":I
    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->getCurGatewayMacFromRoute()[B

    move-result-object v2

    .line 431
    .local v2, "curGatewayMac":[B
    if-eqz v2, :cond_4

    array-length v3, v2

    if-lez v3, :cond_4

    iget-object v3, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    if-eqz v3, :cond_4

    .line 432
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_0

    goto :goto_2

    .line 436
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 437
    iget-object v4, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    invoke-static {v4, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 438
    move v0, v3

    .line 436
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 441
    .end local v3    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 442
    add-int/lit8 v1, v0, 0x1

    goto :goto_1

    .line 444
    :cond_3
    const/4 v1, 0x0

    .line 446
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Current gateway index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,mac "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 447
    invoke-static {v2}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 446
    const-string v4, "ArpDetect"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    return v1

    .line 433
    :cond_4
    :goto_2
    const/4 v3, -0x1

    return v3
.end method

.method private handlePacket()Z
    .locals 8

    .line 572
    const-string v0, ", mac "

    const-string v1, "ArpDetect"

    const/16 v2, 0x5dc

    new-array v2, v2, [B

    .line 574
    .local v2, "mPacket":[B
    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/android/server/wifi/ArpDetect;->mArpRecvSock:Ljava/io/FileDescriptor;

    array-length v5, v2

    invoke-static {v4, v2, v3, v5}, Landroid/system/Os;->read(Ljava/io/FileDescriptor;[BII)I

    move-result v4

    .line 575
    .local v4, "length":I
    invoke-static {v2, v4}, Lcom/android/server/wifi/ArpPacket;->parseArpPacket([BI)Lcom/android/server/wifi/ArpPacket;

    move-result-object v5

    .line 576
    .local v5, "packet":Lcom/android/server/wifi/ArpPacket;
    if-eqz v5, :cond_0

    iget-short v6, v5, Lcom/android/server/wifi/ArpPacket;->mOpCode:S

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 577
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Received Arp response from IP "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v5, Lcom/android/server/wifi/ArpPacket;->mSenderIp:Ljava/net/Inet4Address;

    .line 578
    invoke-virtual {v7}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v5, Lcom/android/server/wifi/ArpPacket;->mSenderHwAddress:[B

    .line 579
    invoke-static {v7}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " --> target IP "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v5, Lcom/android/server/wifi/ArpPacket;->mTargetIp:Ljava/net/Inet4Address;

    .line 580
    invoke-virtual {v7}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, v5, Lcom/android/server/wifi/ArpPacket;->mTtargetHwAddress:[B

    .line 582
    invoke-static {v6}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 577
    invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->logd(Ljava/lang/String;)V

    .line 584
    iget-object v0, v5, Lcom/android/server/wifi/ArpPacket;->mSenderIp:Ljava/net/Inet4Address;

    iget-object v6, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayAddress:Ljava/net/Inet4Address;

    invoke-virtual {v0, v6}, Ljava/net/Inet4Address;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v5, Lcom/android/server/wifi/ArpPacket;->mSenderHwAddress:[B

    iget-object v6, p0, Lcom/android/server/wifi/ArpDetect;->mLocalMacAddress:[B

    .line 585
    invoke-static {v0, v6}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v5, Lcom/android/server/wifi/ArpPacket;->mSenderHwAddress:[B

    .line 586
    invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->isGatewayMacSaved([B)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 588
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v6, 0x4

    if-ge v0, v6, :cond_0

    .line 589
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    iget-object v6, v5, Lcom/android/server/wifi/ArpPacket;->mSenderHwAddress:[B

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 590
    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->checkIsMultiGwFind()V
    :try_end_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/server/wifi/ArpPacket$ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 599
    .end local v4    # "length":I
    .end local v5    # "packet":Lcom/android/server/wifi/ArpPacket;
    :cond_0
    goto :goto_0

    .line 597
    :catch_0
    move-exception v0

    .line 598
    .local v0, "e":Lcom/android/server/wifi/ArpPacket$ParseException;
    const-string v3, "Can\'t parse ARP packet: "

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 601
    .end local v0    # "e":Lcom/android/server/wifi/ArpPacket$ParseException;
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 593
    :catch_1
    move-exception v0

    .line 594
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 595
    const-string v4, "handlePacket error"

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    return v3
.end method

.method private hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .line 117
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    goto :goto_0

    .line 119
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "****"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "newStr":Ljava/lang/String;
    return-object v0

    .line 118
    .end local v0    # "newStr":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p1
.end method

.method private initArpRecvSocket()V
    .locals 5

    .line 151
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mIfaceName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->getNetworkInterfaceByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v0

    .line 152
    .local v0, "iface":Ljava/net/NetworkInterface;
    if-nez v0, :cond_0

    .line 153
    return-void

    .line 155
    :cond_0
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getIndex()I

    move-result v1

    .line 156
    .local v1, "ifaceIndex":I
    sget v2, Landroid/system/OsConstants;->AF_PACKET:I

    sget v3, Landroid/system/OsConstants;->SOCK_RAW:I

    sget v4, Landroid/system/OsConstants;->SOCK_NONBLOCK:I

    or-int/2addr v3, v4

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/system/Os;->socket(III)Ljava/io/FileDescriptor;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/wifi/ArpDetect;->mArpRecvSock:Ljava/io/FileDescriptor;

    .line 157
    sget v2, Landroid/system/OsConstants;->ETH_P_ARP:I

    invoke-static {v2, v1}, Landroid/net/util/SocketUtils;->makePacketSocketAddress(II)Ljava/net/SocketAddress;

    move-result-object v2

    .line 158
    .local v2, "addr":Ljava/net/SocketAddress;
    iget-object v3, p0, Lcom/android/server/wifi/ArpDetect;->mArpRecvSock:Ljava/io/FileDescriptor;

    invoke-static {v3, v2}, Landroid/system/Os;->bind(Ljava/io/FileDescriptor;Ljava/net/SocketAddress;)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    .end local v0    # "iface":Ljava/net/NetworkInterface;
    .end local v1    # "ifaceIndex":I
    .end local v2    # "addr":Ljava/net/SocketAddress;
    goto :goto_0

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ArpDetect"

    const-string v2, "Error creating ARP recv socket"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 161
    iget-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mArpRecvSock:Ljava/io/FileDescriptor;

    invoke-direct {p0, v1}, Lcom/android/server/wifi/ArpDetect;->closeSocket(Ljava/io/FileDescriptor;)V

    .line 162
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mArpRecvSock:Ljava/io/FileDescriptor;

    .line 164
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private initArpSendSocket()V
    .locals 5

    .line 134
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mIfaceName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->getNetworkInterfaceByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v0

    .line 135
    .local v0, "iface":Ljava/net/NetworkInterface;
    if-nez v0, :cond_0

    .line 136
    return-void

    .line 138
    :cond_0
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getIndex()I

    move-result v1

    .line 139
    .local v1, "ifaceIndex":I
    sget v2, Landroid/system/OsConstants;->AF_PACKET:I

    sget v3, Landroid/system/OsConstants;->SOCK_RAW:I

    sget v4, Landroid/system/OsConstants;->SOCK_NONBLOCK:I

    or-int/2addr v3, v4

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/system/Os;->socket(III)Ljava/io/FileDescriptor;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/wifi/ArpDetect;->mArpSendSock:Ljava/io/FileDescriptor;

    .line 140
    sget v2, Landroid/system/OsConstants;->ETH_P_ARP:I

    invoke-static {v2, v1}, Landroid/net/util/SocketUtils;->makePacketSocketAddress(II)Ljava/net/SocketAddress;

    move-result-object v2

    .line 141
    .local v2, "addr":Ljava/net/SocketAddress;
    iget-object v3, p0, Lcom/android/server/wifi/ArpDetect;->mArpSendSock:Ljava/io/FileDescriptor;

    invoke-static {v3, v2}, Landroid/system/Os;->bind(Ljava/io/FileDescriptor;Ljava/net/SocketAddress;)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    .end local v0    # "iface":Ljava/net/NetworkInterface;
    .end local v1    # "ifaceIndex":I
    .end local v2    # "addr":Ljava/net/SocketAddress;
    goto :goto_0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ArpDetect"

    const-string v2, "Error creating ARP send socket"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 144
    iget-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mArpSendSock:Ljava/io/FileDescriptor;

    invoke-direct {p0, v1}, Lcom/android/server/wifi/ArpDetect;->closeSocket(Ljava/io/FileDescriptor;)V

    .line 145
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mArpSendSock:Ljava/io/FileDescriptor;

    .line 147
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private initSocket()Z
    .locals 4

    .line 167
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mArpSendSock:Ljava/io/FileDescriptor;

    if-nez v0, :cond_0

    .line 168
    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->initArpSendSocket()V

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mArpRecvSock:Ljava/io/FileDescriptor;

    if-nez v0, :cond_1

    .line 170
    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->initArpRecvSocket()V

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mInterfaceBroadcastAddr:Ljava/net/SocketAddress;

    if-nez v0, :cond_2

    .line 172
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mIfaceName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->getNetworkInterfaceByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v0

    .line 173
    .local v0, "iface":Ljava/net/NetworkInterface;
    if-eqz v0, :cond_2

    .line 174
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getIndex()I

    move-result v1

    .line 175
    .local v1, "ifaceIndex":I
    sget v2, Landroid/system/OsConstants;->ETH_P_IP:I

    sget-object v3, Lcom/android/server/wifi/ArpPacket;->ETHER_BROADCAST:[B

    invoke-static {v2, v1, v3}, Landroid/net/util/SocketUtils;->makePacketSocketAddress(II[B)Ljava/net/SocketAddress;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/wifi/ArpDetect;->mInterfaceBroadcastAddr:Ljava/net/SocketAddress;

    .line 179
    .end local v0    # "iface":Ljava/net/NetworkInterface;
    .end local v1    # "ifaceIndex":I
    :cond_2
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mArpSendSock:Ljava/io/FileDescriptor;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mArpRecvSock:Ljava/io/FileDescriptor;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mInterfaceBroadcastAddr:Ljava/net/SocketAddress;

    if-nez v0, :cond_3

    goto :goto_0

    .line 183
    :cond_3
    const/4 v0, 0x1

    return v0

    .line 180
    :cond_4
    :goto_0
    const-string v0, "ArpDetect"

    const-string v1, "initArpSocket fail"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    const/4 v0, 0x0

    return v0
.end method

.method private isGatewayMacSaved([B)Z
    .locals 3
    .param p1, "macAddr"    # [B

    .line 453
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 454
    return v1

    .line 455
    :cond_0
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 456
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 457
    iget-object v2, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    invoke-static {v2, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 458
    const/4 v1, 0x1

    return v1

    .line 456
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 461
    .end local v0    # "i":I
    :cond_2
    return v1
.end method

.method private isMultiGatewayExist()Z
    .locals 1

    .line 345
    iget-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->isMultiGwFind:Z

    return v0
.end method

.method private isPermArpRoute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p1, "ipAddress"    # Ljava/lang/String;
    .param p2, "macAddress"    # Ljava/lang/String;
    .param p3, "ifaceName"    # Ljava/lang/String;

    .line 465
    const-string v0, "ArpDetect"

    const/4 v1, 0x0

    if-eqz p1, :cond_6

    if-eqz p2, :cond_6

    if-nez p3, :cond_0

    goto/16 :goto_7

    .line 469
    :cond_0
    const/4 v2, 0x0

    .line 471
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    const-string v5, "/proc/net/arp"

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v2, v3

    .line 473
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 474
    .local v3, "line":Ljava/lang/String;
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    if-eqz v4, :cond_3

    .line 475
    const-string v4, "[ ]+"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 476
    .local v4, "tokens":[Ljava/lang/String;
    array-length v5, v4

    const/4 v6, 0x6

    if-ge v5, v6, :cond_1

    .line 477
    goto :goto_0

    .line 481
    :cond_1
    aget-object v5, v4, v1

    .line 482
    .local v5, "ip":Ljava/lang/String;
    const/4 v6, 0x2

    aget-object v6, v4, v6

    .line 483
    .local v6, "flags":Ljava/lang/String;
    const/4 v7, 0x3

    aget-object v7, v4, v7

    .line 484
    .local v7, "mac":Ljava/lang/String;
    const/4 v8, 0x5

    aget-object v8, v4, v8

    .line 485
    .local v8, "curIfaceName":Ljava/lang/String;
    const-string v9, "0x6"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 486
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 487
    const-string v9, "neigh state is permanent"

    invoke-static {v0, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 488
    nop

    .line 497
    nop

    .line 498
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 502
    goto :goto_1

    .line 500
    :catch_0
    move-exception v0

    .line 488
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 490
    .end local v4    # "tokens":[Ljava/lang/String;
    .end local v5    # "ip":Ljava/lang/String;
    .end local v6    # "flags":Ljava/lang/String;
    .end local v7    # "mac":Ljava/lang/String;
    .end local v8    # "curIfaceName":Ljava/lang/String;
    :cond_2
    goto :goto_0

    .line 497
    .end local v3    # "line":Ljava/lang/String;
    :cond_3
    nop

    .line 498
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 502
    :cond_4
    :goto_2
    goto :goto_3

    .line 500
    :catch_1
    move-exception v0

    .line 503
    goto :goto_3

    .line 496
    :catchall_0
    move-exception v0

    goto :goto_4

    .line 493
    :catch_2
    move-exception v3

    .line 494
    .local v3, "e":Ljava/io/IOException;
    :try_start_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not read /proc/net/arp "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 497
    .end local v3    # "e":Ljava/io/IOException;
    if-eqz v2, :cond_4

    .line 498
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 491
    :catch_3
    move-exception v3

    .line 492
    .local v3, "e":Ljava/io/FileNotFoundException;
    :try_start_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not open /proc/net/arp "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 497
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    if-eqz v2, :cond_4

    .line 498
    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2

    .line 504
    :goto_3
    return v1

    .line 497
    :goto_4
    if-eqz v2, :cond_5

    .line 498
    :try_start_7
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_5

    .line 500
    :catch_4
    move-exception v1

    goto :goto_6

    .line 502
    :cond_5
    :goto_5
    nop

    .line 503
    :goto_6
    throw v0

    .line 466
    .end local v2    # "reader":Ljava/io/BufferedReader;
    :cond_6
    :goto_7
    return v1
.end method

.method private synthetic lambda$registerArpRecvQueue$0(Ljava/io/FileDescriptor;I)I
    .locals 1
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "events"    # I

    .line 618
    iget-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->stopDetect:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->handlePacket()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 622
    :cond_0
    const/4 v0, 0x5

    return v0

    .line 619
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->unregisterArpRecvQueue()V

    .line 620
    const/4 v0, 0x0

    return v0
.end method

.method private logd(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .line 114
    return-void
.end method

.method public static makeInstance(Landroid/content/Context;)Lcom/android/server/wifi/ArpDetect;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 103
    new-instance v0, Lcom/android/server/wifi/ArpDetect;

    invoke-direct {v0, p0}, Lcom/android/server/wifi/ArpDetect;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/server/wifi/ArpDetect;->sIntance:Lcom/android/server/wifi/ArpDetect;

    .line 104
    return-object v0
.end method

.method private registerArpRecvQueue()Z
    .locals 4

    .line 605
    const-string v0, "ArpDetect"

    const-string v1, "enter registerArpRecvQueue"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->stopDetect:Z

    .line 607
    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->initSocket()Z

    move-result v1

    if-nez v1, :cond_0

    .line 608
    return v0

    .line 611
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mHandler:Landroid/os/Handler;

    .line 612
    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getQueue()Landroid/os/MessageQueue;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mQueue:Landroid/os/MessageQueue;

    .line 614
    iget-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mArpRecvSock:Ljava/io/FileDescriptor;

    new-instance v2, Lcom/android/server/wifi/ArpDetect$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/wifi/ArpDetect$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wifi/ArpDetect;)V

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v3, v2}, Landroid/os/MessageQueue;->addOnFileDescriptorEventListener(Ljava/io/FileDescriptor;ILandroid/os/MessageQueue$OnFileDescriptorEventListener;)V

    .line 624
    const/4 v0, 0x1

    return v0
.end method

.method private sendArpPacket([BLjava/net/Inet4Address;[BLjava/net/Inet4Address;II)Z
    .locals 7
    .param p1, "senderMac"    # [B
    .param p2, "senderIp"    # Ljava/net/Inet4Address;
    .param p3, "targetMac"    # [B
    .param p4, "targetIp"    # Ljava/net/Inet4Address;
    .param p5, "retryTimes"    # I
    .param p6, "interval"    # I

    .line 395
    sget-object v0, Lcom/android/server/wifi/ArpPacket;->ETHER_BROADCAST:[B

    .line 396
    invoke-virtual {p2}, Ljava/net/Inet4Address;->getAddress()[B

    move-result-object v3

    .line 397
    invoke-virtual {p4}, Ljava/net/Inet4Address;->getAddress()[B

    move-result-object v5

    const/4 v6, 0x1

    .line 395
    move-object v1, p1

    move-object v2, p1

    move-object v4, p3

    invoke-static/range {v0 .. v6}, Lcom/android/server/wifi/ArpPacket;->buildArpPacket([B[B[B[B[B[BS)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 398
    .local v0, "packet":Ljava/nio/ByteBuffer;
    invoke-direct {p0, v0, p5, p6}, Lcom/android/server/wifi/ArpDetect;->transmitPacket(Ljava/nio/ByteBuffer;II)Z

    move-result v1

    return v1
.end method

.method private setMultiGwRecoveryState(I)V
    .locals 2
    .param p1, "state"    # I

    .line 654
    iget v0, p0, Lcom/android/server/wifi/ArpDetect;->mCurRecoveryState:I

    if-ne p1, v0, :cond_0

    .line 655
    return-void

    .line 657
    :cond_0
    iput p1, p0, Lcom/android/server/wifi/ArpDetect;->mCurRecoveryState:I

    .line 658
    sget v0, Lcom/android/server/wifi/ArpDetect;->STATE_MULTI_GW_RECOVERY_STOPED:I

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->msgHandler:Lcom/android/server/wifi/ArpDetect$MessageHandler;

    if-eqz v0, :cond_1

    .line 659
    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Lcom/android/server/wifi/ArpDetect$MessageHandler;->removeMessages(I)V

    .line 661
    :cond_1
    return-void
.end method

.method private startMultiGatewayDetect([BLjava/net/Inet4Address;Ljava/net/Inet4Address;I)V
    .locals 8
    .param p1, "senderMac"    # [B
    .param p2, "senderIp"    # Ljava/net/Inet4Address;
    .param p3, "targetIp"    # Ljava/net/Inet4Address;
    .param p4, "retryTimes"    # I

    .line 372
    const-string v0, "ArpDetect"

    const-string v1, "Start multiple Gatway Detect.."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    new-instance v0, Ljava/lang/Thread;

    new-instance v7, Lcom/android/server/wifi/ArpDetect$1;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wifi/ArpDetect$1;-><init>(Lcom/android/server/wifi/ArpDetect;Ljava/net/Inet4Address;[BLjava/net/Inet4Address;I)V

    invoke-direct {v0, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 389
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 390
    return-void
.end method

.method private transmitPacket(Ljava/nio/ByteBuffer;II)Z
    .locals 8
    .param p1, "buf"    # Ljava/nio/ByteBuffer;
    .param p2, "retryTimes"    # I
    .param p3, "interval"    # I

    .line 403
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-gt v0, p2, :cond_1

    .line 404
    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/wifi/ArpDetect;->stopDetect:Z

    if-eqz v1, :cond_0

    .line 405
    goto :goto_1

    .line 407
    :cond_0
    iget-object v2, p0, Lcom/android/server/wifi/ArpDetect;->mArpSendSock:Ljava/io/FileDescriptor;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v5

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/server/wifi/ArpDetect;->mInterfaceBroadcastAddr:Ljava/net/SocketAddress;

    invoke-static/range {v2 .. v7}, Landroid/system/Os;->sendto(Ljava/io/FileDescriptor;[BIIILjava/net/SocketAddress;)I

    .line 408
    int-to-long v1, p3

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 403
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 410
    .end local v0    # "i":I
    :catch_0
    move-exception v0

    .line 411
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ArpDetect"

    const-string/jumbo v2, "send packet error: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 412
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 413
    const/4 v1, 0x0

    return v1

    .line 414
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    nop

    .line 415
    const/4 v0, 0x1

    return v0
.end method

.method private unregisterArpRecvQueue()V
    .locals 2

    .line 628
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->msgHandler:Lcom/android/server/wifi/ArpDetect$MessageHandler;

    if-eqz v0, :cond_0

    .line 629
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/android/server/wifi/ArpDetect$MessageHandler;->removeMessages(I)V

    .line 632
    :cond_0
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mQueue:Landroid/os/MessageQueue;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mArpRecvSock:Ljava/io/FileDescriptor;

    if-eqz v1, :cond_1

    .line 633
    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->removeOnFileDescriptorEventListener(Ljava/io/FileDescriptor;)V

    .line 634
    const-string v0, "ArpDetect"

    const-string v1, "removed socket listen"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    :cond_1
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mArpRecvSock:Ljava/io/FileDescriptor;

    invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->closeSocket(Ljava/io/FileDescriptor;)V

    .line 637
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mArpRecvSock:Ljava/io/FileDescriptor;

    .line 638
    iput-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mQueue:Landroid/os/MessageQueue;

    .line 639
    iput-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mHandler:Landroid/os/Handler;

    .line 640
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->stopDetect:Z

    .line 641
    return-void
.end method

.method private static updateNeighbor(Ljava/net/Inet4Address;SI[B)Z
    .locals 6
    .param p0, "ipAddr"    # Ljava/net/Inet4Address;
    .param p1, "nudState"    # S
    .param p2, "ifIndex"    # I
    .param p3, "macAddr"    # [B

    .line 350
    const/4 v0, 0x0

    const-string v1, "ArpDetect"

    if-eqz p0, :cond_3

    if-nez p3, :cond_0

    goto :goto_1

    .line 353
    :cond_0
    invoke-virtual {p0}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/android/server/wifi/ArpDetect;->IPV4_ADDR_ANY:Ljava/net/Inet4Address;

    invoke-virtual {v3}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/android/server/wifi/ArpPacket;->ETHER_ANY:[B

    .line 354
    invoke-static {p3, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 359
    :cond_1
    const/4 v2, 0x1

    invoke-static {v2, p0, p1, p2, p3}, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->newNewNeighborMessage(ILjava/net/InetAddress;SI[B)[B

    move-result-object v3

    .line 362
    .local v3, "msg":[B
    :try_start_0
    sget v4, Landroid/system/OsConstants;->NETLINK_ROUTE:I

    invoke-static {v4, v3}, Lcom/android/net/module/util/netlink/NetlinkUtils;->sendOneShotKernelMessage(I[B)V
    :try_end_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    nop

    .line 367
    return v2

    .line 363
    :catch_0
    move-exception v2

    .line 364
    .local v2, "e":Landroid/system/ErrnoException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "sendOneShotKernelMessage error:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    return v0

    .line 355
    .end local v2    # "e":Landroid/system/ErrnoException;
    .end local v3    # "msg":[B
    :cond_2
    :goto_0
    const-string/jumbo v2, "update neigh fail,invaild ip or mac addr"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    return v0

    .line 351
    :cond_3
    :goto_1
    const-string/jumbo v2, "update neigh fail,param is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    return v0
.end method


# virtual methods
.method public isMultiGatewayNetwork(I)Z
    .locals 1
    .param p1, "netId"    # I

    .line 240
    iget v0, p0, Lcom/android/server/wifi/ArpDetect;->mNetworkId:I

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->isMultiGatewayExist()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isRecoveredOneRound(I)Z
    .locals 3
    .param p1, "netId"    # I

    .line 252
    invoke-virtual {p0, p1}, Lcom/android/server/wifi/ArpDetect;->isMultiGatewayNetwork(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayChangeCount:I

    .line 253
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x1

    sub-int/2addr v0, v2

    if-lt v1, v0, :cond_0

    .line 254
    return v2

    .line 256
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isRecoveryOngoing(I)Z
    .locals 2
    .param p1, "netId"    # I

    .line 244
    invoke-virtual {p0, p1}, Lcom/android/server/wifi/ArpDetect;->isMultiGatewayNetwork(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/wifi/ArpDetect;->mCurRecoveryState:I

    sget v1, Lcom/android/server/wifi/ArpDetect;->STATE_MULTI_GW_RECOVERY_STOPED:I

    if-eq v0, v1, :cond_0

    .line 246
    const/4 v0, 0x1

    return v0

    .line 248
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public resetGatewayNeighState()Z
    .locals 8

    .line 322
    const/4 v0, 0x0

    .line 323
    .local v0, "status":Z
    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->isMultiGatewayExist()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 324
    return v2

    .line 327
    :cond_0
    iget-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mIfaceName:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/server/wifi/ArpDetect;->getNetworkInterfaceByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v1

    .line 328
    .local v1, "iface":Ljava/net/NetworkInterface;
    const-string v3, "ArpDetect"

    if-nez v1, :cond_1

    .line 329
    const-string v4, "get iface fail"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    return v2

    .line 332
    :cond_1
    invoke-virtual {v1}, Ljava/net/NetworkInterface;->getIndex()I

    move-result v2

    .line 333
    .local v2, "ifaceIndex":I
    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->getCurGatewayMacFromRoute()[B

    move-result-object v4

    .line 334
    .local v4, "curGatewayMac":[B
    if-eqz v4, :cond_2

    array-length v5, v4

    if-lez v5, :cond_2

    iget-object v5, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayAddress:Ljava/net/Inet4Address;

    .line 335
    invoke-virtual {v5}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    .line 336
    invoke-static {v4}, Landroid/net/MacAddress;->stringAddrFromByteAddr([B)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/wifi/ArpDetect;->mIfaceName:Ljava/lang/String;

    .line 335
    invoke-direct {p0, v5, v6, v7}, Lcom/android/server/wifi/ArpDetect;->isPermArpRoute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 337
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "reset current gateway neigh state for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/wifi/ArpDetect;->mIfaceName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    iget-object v3, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayAddress:Ljava/net/Inet4Address;

    const/4 v5, 0x2

    invoke-static {v3, v5, v2, v4}, Lcom/android/server/wifi/ArpDetect;->updateNeighbor(Ljava/net/Inet4Address;SI[B)Z

    move-result v0

    .line 341
    :cond_2
    return v0
.end method

.method public setCurGatewayPerm()Z
    .locals 7

    .line 260
    const/4 v0, 0x0

    .line 261
    .local v0, "status":Z
    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->isMultiGatewayExist()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/android/server/wifi/ArpDetect;->mNetworkId:I

    invoke-virtual {p0, v1}, Lcom/android/server/wifi/ArpDetect;->isRecoveryOngoing(I)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 265
    :cond_0
    iget-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mIfaceName:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/server/wifi/ArpDetect;->getNetworkInterfaceByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v1

    .line 266
    .local v1, "iface":Ljava/net/NetworkInterface;
    const-string v3, "ArpDetect"

    if-nez v1, :cond_1

    .line 267
    const-string v4, "get iface fail"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    return v2

    .line 270
    :cond_1
    invoke-virtual {v1}, Ljava/net/NetworkInterface;->getIndex()I

    move-result v2

    .line 271
    .local v2, "ifaceIndex":I
    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->getCurGatewayMacFromRoute()[B

    move-result-object v4

    .line 272
    .local v4, "curGatewayMac":[B
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "set current gateway "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 273
    invoke-static {v4}, Landroid/net/MacAddress;->stringAddrFromByteAddr([B)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " permanent for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/wifi/ArpDetect;->mIfaceName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 272
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    if-eqz v4, :cond_2

    array-length v3, v4

    if-lez v3, :cond_2

    iget-object v3, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayAddress:Ljava/net/Inet4Address;

    .line 276
    invoke-virtual {v3}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v3

    .line 277
    invoke-static {v4}, Landroid/net/MacAddress;->stringAddrFromByteAddr([B)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/wifi/ArpDetect;->mIfaceName:Ljava/lang/String;

    .line 276
    invoke-direct {p0, v3, v5, v6}, Lcom/android/server/wifi/ArpDetect;->isPermArpRoute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 278
    iget-object v3, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayAddress:Ljava/net/Inet4Address;

    const/16 v5, 0x80

    invoke-static {v3, v5, v2, v4}, Lcom/android/server/wifi/ArpDetect;->updateNeighbor(Ljava/net/Inet4Address;SI[B)Z

    move-result v0

    .line 281
    :cond_2
    sget v3, Lcom/android/server/wifi/ArpDetect;->STATE_MULTI_GW_RECOVERY_STOPED:I

    invoke-direct {p0, v3}, Lcom/android/server/wifi/ArpDetect;->setMultiGwRecoveryState(I)V

    .line 282
    return v0

    .line 262
    .end local v1    # "iface":Ljava/net/NetworkInterface;
    .end local v2    # "ifaceIndex":I
    .end local v4    # "curGatewayMac":[B
    :cond_3
    :goto_0
    return v2
.end method

.method public startGatewayDetect(ILjava/lang/String;Ljava/net/Inet4Address;Ljava/net/Inet4Address;[B)V
    .locals 4
    .param p1, "networkId"    # I
    .param p2, "ifaceName"    # Ljava/lang/String;
    .param p3, "localIpAddress"    # Ljava/net/Inet4Address;
    .param p4, "gatewayeIpAddress"    # Ljava/net/Inet4Address;
    .param p5, "localMacAddress"    # [B

    .line 188
    const-string v0, "ArpDetect"

    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    if-eqz p4, :cond_1

    if-eqz p5, :cond_1

    array-length v1, p5

    if-nez v1, :cond_0

    goto :goto_0

    .line 193
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wifi/ArpDetect;->stopGatewayDetect()V

    .line 195
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start ArpDetect on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for netId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iput p1, p0, Lcom/android/server/wifi/ArpDetect;->mNetworkId:I

    .line 197
    iput-object p2, p0, Lcom/android/server/wifi/ArpDetect;->mIfaceName:Ljava/lang/String;

    .line 198
    iput-object p3, p0, Lcom/android/server/wifi/ArpDetect;->mLocalIpAddress:Ljava/net/Inet4Address;

    .line 199
    iput-object p4, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayAddress:Ljava/net/Inet4Address;

    .line 200
    iput-object p5, p0, Lcom/android/server/wifi/ArpDetect;->mLocalMacAddress:[B

    .line 202
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    .line 203
    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->registerArpRecvQueue()Z

    .line 205
    new-instance v0, Lcom/android/server/wifi/ArpDetect$MessageHandler;

    invoke-direct {v0, p0}, Lcom/android/server/wifi/ArpDetect$MessageHandler;-><init>(Lcom/android/server/wifi/ArpDetect;)V

    iput-object v0, p0, Lcom/android/server/wifi/ArpDetect;->msgHandler:Lcom/android/server/wifi/ArpDetect$MessageHandler;

    .line 206
    const/16 v1, 0x64

    const-wide/16 v2, 0x1f40

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/wifi/ArpDetect$MessageHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 208
    sget v0, Lcom/android/server/wifi/ArpDetect;->STATE_MULTI_GW_RECOVERY_STARTED:I

    invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->setMultiGwRecoveryState(I)V

    .line 209
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mLocalMacAddress:[B

    iget-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mLocalIpAddress:Ljava/net/Inet4Address;

    iget-object v2, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayAddress:Ljava/net/Inet4Address;

    const/4 v3, 0x6

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/server/wifi/ArpDetect;->startMultiGatewayDetect([BLjava/net/Inet4Address;Ljava/net/Inet4Address;I)V

    .line 211
    return-void

    .line 190
    :cond_1
    :goto_0
    const-string v1, "Invalid param"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    return-void
.end method

.method public stopGatewayDetect()V
    .locals 2

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Stop ArpDetect on "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mIfaceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ArpDetect"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    iget-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->stopDetect:Z

    if-nez v0, :cond_0

    .line 217
    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->unregisterArpRecvQueue()V

    .line 220
    :cond_0
    sget v0, Lcom/android/server/wifi/ArpDetect;->STATE_MULTI_GW_RECOVERY_STOPED:I

    invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->setMultiGwRecoveryState(I)V

    .line 221
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mArpSendSock:Ljava/io/FileDescriptor;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 222
    invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->closeSocket(Ljava/io/FileDescriptor;)V

    .line 223
    iput-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mArpSendSock:Ljava/io/FileDescriptor;

    .line 225
    :cond_1
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mArpRecvSock:Ljava/io/FileDescriptor;

    if-eqz v0, :cond_2

    .line 226
    invoke-direct {p0, v0}, Lcom/android/server/wifi/ArpDetect;->closeSocket(Ljava/io/FileDescriptor;)V

    .line 227
    iput-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mArpRecvSock:Ljava/io/FileDescriptor;

    .line 229
    :cond_2
    iget-object v0, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 230
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 231
    iput-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    .line 233
    :cond_3
    iput-object v1, p0, Lcom/android/server/wifi/ArpDetect;->msgHandler:Lcom/android/server/wifi/ArpDetect$MessageHandler;

    .line 234
    iput-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mInterfaceBroadcastAddr:Ljava/net/SocketAddress;

    .line 235
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wifi/ArpDetect;->isMultiGwFind:Z

    .line 236
    iput v0, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayChangeCount:I

    .line 237
    return-void
.end method

.method public tryOtherGateway()Z
    .locals 8

    .line 286
    const/4 v0, 0x0

    .line 287
    .local v0, "status":Z
    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->isMultiGatewayExist()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/android/server/wifi/ArpDetect;->mNetworkId:I

    invoke-virtual {p0, v1}, Lcom/android/server/wifi/ArpDetect;->isRecoveryOngoing(I)Z

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_0

    .line 290
    :cond_0
    iget-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget v3, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayChangeCount:I

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v3, v1, :cond_1

    .line 291
    invoke-virtual {p0}, Lcom/android/server/wifi/ArpDetect;->resetGatewayNeighState()Z

    .line 292
    sget v1, Lcom/android/server/wifi/ArpDetect;->STATE_MULTI_GW_RECOVERY_STOPED:I

    invoke-direct {p0, v1}, Lcom/android/server/wifi/ArpDetect;->setMultiGwRecoveryState(I)V

    .line 293
    return v2

    .line 296
    :cond_1
    iget-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    const-string v3, "ArpDetect"

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v4, 0x2

    if-ge v1, v4, :cond_2

    .line 297
    const-string v1, "Only one gateway device"

    invoke-static {v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    return v2

    .line 301
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "try other gateway for "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/wifi/ArpDetect;->mIfaceName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    iget-object v1, p0, Lcom/android/server/wifi/ArpDetect;->mIfaceName:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/server/wifi/ArpDetect;->getNetworkInterfaceByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v1

    .line 303
    .local v1, "iface":Ljava/net/NetworkInterface;
    if-nez v1, :cond_3

    .line 304
    const-string v4, "get iface fail"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    return v2

    .line 307
    :cond_3
    invoke-virtual {v1}, Ljava/net/NetworkInterface;->getIndex()I

    move-result v2

    .line 308
    .local v2, "ifaceIndex":I
    invoke-direct {p0}, Lcom/android/server/wifi/ArpDetect;->getNextGatewayIndex()I

    move-result v4

    .line 309
    .local v4, "nextGatewayIndex":I
    if-ltz v4, :cond_4

    iget-object v5, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayMacList:Ljava/util/ArrayList;

    if-eqz v5, :cond_4

    .line 310
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    .line 311
    .local v5, "nextGatewayMac":[B
    iget-object v6, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayAddress:Ljava/net/Inet4Address;

    const/16 v7, 0x80

    invoke-static {v6, v7, v2, v5}, Lcom/android/server/wifi/ArpDetect;->updateNeighbor(Ljava/net/Inet4Address;SI[B)Z

    move-result v0

    .line 313
    iget v6, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayChangeCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/android/server/wifi/ArpDetect;->mGatewayChangeCount:I

    .line 314
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "New gateway index "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ,mac "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 315
    invoke-static {v5}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/server/wifi/ArpDetect;->hidenPrivateInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 314
    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    .end local v5    # "nextGatewayMac":[B
    :cond_4
    return v0

    .line 288
    .end local v1    # "iface":Ljava/net/NetworkInterface;
    .end local v2    # "ifaceIndex":I
    .end local v4    # "nextGatewayIndex":I
    :cond_5
    :goto_0
    return v2
.end method
