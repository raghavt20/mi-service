.class public Lcom/android/server/uri/UriGrantsManagerServiceImpl;
.super Ljava/lang/Object;
.source "UriGrantsManagerServiceImpl.java"

# interfaces
.implements Lcom/android/server/uri/UriGrantsManagerServiceStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getPerms(Landroid/util/SparseArray;ILcom/android/server/uri/GrantUri;)Landroid/util/ArrayMap;
    .locals 4
    .param p2, "uid"    # I
    .param p3, "grantUri"    # Lcom/android/server/uri/GrantUri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Landroid/util/ArrayMap<",
            "Lcom/android/server/uri/GrantUri;",
            "Lcom/android/server/uri/UriPermission;",
            ">;>;I",
            "Lcom/android/server/uri/GrantUri;",
            ")",
            "Landroid/util/ArrayMap<",
            "Lcom/android/server/uri/GrantUri;",
            "Lcom/android/server/uri/UriPermission;",
            ">;"
        }
    .end annotation

    .line 14
    .local p1, "mGrantedUriPermissions":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/ArrayMap<Lcom/android/server/uri/GrantUri;Lcom/android/server/uri/UriPermission;>;>;"
    invoke-virtual {p1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/ArrayMap;

    .line 15
    .local v0, "perms":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Lcom/android/server/uri/GrantUri;Lcom/android/server/uri/UriPermission;>;"
    invoke-static {}, Lcom/miui/xspace/XSpaceManagerStub;->getInstance()Lcom/miui/xspace/XSpaceManagerStub;

    move-result-object v1

    iget v2, p3, Lcom/android/server/uri/GrantUri;->sourceUserId:I

    .line 16
    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    .line 15
    invoke-virtual {v1, v2, v3}, Lcom/miui/xspace/XSpaceManagerStub;->canCrossUser(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17
    iget v1, p3, Lcom/android/server/uri/GrantUri;->sourceUserId:I

    invoke-static {v1, p2}, Landroid/os/UserHandle;->getUid(II)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/ArrayMap;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 18
    .local v1, "userPerms":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Lcom/android/server/uri/GrantUri;Lcom/android/server/uri/UriPermission;>;"
    :goto_0
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 19
    invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->putAll(Landroid/util/ArrayMap;)V

    goto :goto_1

    .line 20
    :cond_1
    if-eqz v1, :cond_2

    .line 21
    move-object v0, v1

    .line 23
    :cond_2
    :goto_1
    return-object v0
.end method
