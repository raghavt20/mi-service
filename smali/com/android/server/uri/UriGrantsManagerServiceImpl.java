public class com.android.server.uri.UriGrantsManagerServiceImpl implements com.android.server.uri.UriGrantsManagerServiceStub {
	 /* .source "UriGrantsManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public com.android.server.uri.UriGrantsManagerServiceImpl ( ) {
		 /* .locals 0 */
		 /* .line 10 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public android.util.ArrayMap getPerms ( android.util.SparseArray p0, Integer p1, com.android.server.uri.GrantUri p2 ) {
		 /* .locals 4 */
		 /* .param p2, "uid" # I */
		 /* .param p3, "grantUri" # Lcom/android/server/uri/GrantUri; */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Landroid/util/SparseArray<", */
		 /* "Landroid/util/ArrayMap<", */
		 /* "Lcom/android/server/uri/GrantUri;", */
		 /* "Lcom/android/server/uri/UriPermission;", */
		 /* ">;>;I", */
		 /* "Lcom/android/server/uri/GrantUri;", */
		 /* ")", */
		 /* "Landroid/util/ArrayMap<", */
		 /* "Lcom/android/server/uri/GrantUri;", */
		 /* "Lcom/android/server/uri/UriPermission;", */
		 /* ">;" */
		 /* } */
	 } // .end annotation
	 /* .line 14 */
	 /* .local p1, "mGrantedUriPermissions":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/ArrayMap<Lcom/android/server/uri/GrantUri;Lcom/android/server/uri/UriPermission;>;>;" */
	 (( android.util.SparseArray ) p1 ).get ( p2 ); // invoke-virtual {p1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
	 /* check-cast v0, Landroid/util/ArrayMap; */
	 /* .line 15 */
	 /* .local v0, "perms":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Lcom/android/server/uri/GrantUri;Lcom/android/server/uri/UriPermission;>;" */
	 com.miui.xspace.XSpaceManagerStub .getInstance ( );
	 /* iget v2, p3, Lcom/android/server/uri/GrantUri;->sourceUserId:I */
	 /* .line 16 */
	 v3 = 	 android.os.UserHandle .getUserId ( p2 );
	 /* .line 15 */
	 v1 = 	 (( com.miui.xspace.XSpaceManagerStub ) v1 ).canCrossUser ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/miui/xspace/XSpaceManagerStub;->canCrossUser(II)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 17 */
		 /* iget v1, p3, Lcom/android/server/uri/GrantUri;->sourceUserId:I */
		 v1 = 		 android.os.UserHandle .getUid ( v1,p2 );
		 (( android.util.SparseArray ) p1 ).get ( v1 ); // invoke-virtual {p1, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
		 /* check-cast v1, Landroid/util/ArrayMap; */
	 } // :cond_0
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 18 */
	 /* .local v1, "userPerms":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Lcom/android/server/uri/GrantUri;Lcom/android/server/uri/UriPermission;>;" */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 19 */
		 (( android.util.ArrayMap ) v0 ).putAll ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->putAll(Landroid/util/ArrayMap;)V
		 /* .line 20 */
	 } // :cond_1
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 21 */
		 /* move-object v0, v1 */
		 /* .line 23 */
	 } // :cond_2
} // :goto_1
} // .end method
