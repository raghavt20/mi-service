.class public Lcom/android/server/BootKeeperStubImpl;
.super Ljava/lang/Object;
.source "BootKeeperStubImpl.java"

# interfaces
.implements Lcom/android/server/BootKeeperStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;,
        Lcom/android/server/BootKeeperStubImpl$BootKeeperOneTrack;
    }
.end annotation


# static fields
.field private static final ENABLE_BOOTKEEPER:Z

.field private static INIT_PLACEHOLDER_SIZE:J = 0x0L

.field private static MIN_BOOT_BYTES:J = 0x0L

.field private static PLACE_HOLDER_FILE:Ljava/lang/String; = null

.field private static SPACE_FREE_EACH:J = 0x0L

.field private static final TAG:Ljava/lang/String; = "BootKeeper"


# instance fields
.field private mBootStrategy:Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

.field private mReleaseSpaceTrigger:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 36
    nop

    .line 37
    const-string v0, "persist.sys.bootkeeper.support"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/BootKeeperStubImpl;->ENABLE_BOOTKEEPER:Z

    .line 38
    const-wide/32 v0, 0xa00000

    sput-wide v0, Lcom/android/server/BootKeeperStubImpl;->MIN_BOOT_BYTES:J

    .line 39
    sput-wide v0, Lcom/android/server/BootKeeperStubImpl;->SPACE_FREE_EACH:J

    .line 40
    const-wide/32 v0, 0x3c00000

    sput-wide v0, Lcom/android/server/BootKeeperStubImpl;->INIT_PLACEHOLDER_SIZE:J

    .line 41
    const-string v0, "/data/system/placeholder"

    sput-object v0, Lcom/android/server/BootKeeperStubImpl;->PLACE_HOLDER_FILE:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/BootKeeperStubImpl;->mReleaseSpaceTrigger:Z

    .line 52
    invoke-direct {p0}, Lcom/android/server/BootKeeperStubImpl;->initialize()V

    .line 53
    return-void
.end method

.method private getAvailableSpace()J
    .locals 3

    .line 97
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    .line 98
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 99
    .local v0, "statFs":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBytes()J

    move-result-wide v1

    .line 100
    .local v1, "availableSpaceBytes":J
    return-wide v1
.end method

.method private getFreeSpace()J
    .locals 3

    .line 104
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    .line 105
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 106
    .local v0, "statFs":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getFreeBytes()J

    move-result-wide v1

    .line 107
    .local v1, "freeSpaceBytes":J
    return-wide v1
.end method

.method private getTotalSpace()J
    .locals 3

    .line 111
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    .line 112
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 113
    .local v0, "statFs":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getTotalBytes()J

    move-result-wide v1

    .line 114
    .local v1, "totalBytes":J
    return-wide v1
.end method

.method private initialize()V
    .locals 1

    .line 57
    sget-object v0, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;->CLIP:Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    iput-object v0, p0, Lcom/android/server/BootKeeperStubImpl;->mBootStrategy:Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    .line 58
    return-void
.end method

.method private onBootStart()V
    .locals 5

    .line 81
    invoke-direct {p0}, Lcom/android/server/BootKeeperStubImpl;->getAvailableSpace()J

    move-result-wide v0

    .line 82
    .local v0, "availableSpaceBytes":J
    sget-wide v2, Lcom/android/server/BootKeeperStubImpl;->MIN_BOOT_BYTES:J

    cmp-long v2, v0, v2

    const/4 v3, 0x0

    if-gez v2, :cond_1

    .line 83
    invoke-direct {p0}, Lcom/android/server/BootKeeperStubImpl;->releaseSpace()Z

    move-result v2

    const-string v4, "BootKeeper"

    if-eqz v2, :cond_0

    .line 84
    const-string v2, "release space success:"

    invoke-static {v4, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/BootKeeperStubImpl;->mReleaseSpaceTrigger:Z

    goto :goto_0

    .line 87
    :cond_0
    iput-boolean v3, p0, Lcom/android/server/BootKeeperStubImpl;->mReleaseSpaceTrigger:Z

    .line 88
    const-string v2, " The placeholder file does not exist, so space cannot be freed"

    invoke-static {v4, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 91
    :cond_1
    iput-boolean v3, p0, Lcom/android/server/BootKeeperStubImpl;->mReleaseSpaceTrigger:Z

    .line 94
    :goto_0
    return-void
.end method

.method private onBootSuccess()V
    .locals 9

    .line 119
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/android/server/BootKeeperStubImpl;->PLACE_HOLDER_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 120
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "placeholder file doesn\'t exists, now start to create"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/server/BootKeeperStubImpl;->PLACE_HOLDER_FILE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BootKeeper"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    invoke-direct {p0}, Lcom/android/server/BootKeeperStubImpl;->getAvailableSpace()J

    move-result-wide v3

    .line 124
    .local v3, "availableSpaceBytes":J
    sget-wide v5, Lcom/android/server/BootKeeperStubImpl;->INIT_PLACEHOLDER_SIZE:J

    cmp-long v1, v3, v5

    if-gez v1, :cond_0

    .line 125
    const-string v1, "available space is not enough to create placeholder file"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    return-void

    .line 128
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "start create "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v5, Lcom/android/server/BootKeeperStubImpl;->PLACE_HOLDER_FILE:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :try_start_0
    sget-object v1, Lcom/android/server/BootKeeperStubImpl;->PLACE_HOLDER_FILE:Ljava/lang/String;

    sget v5, Landroid/system/OsConstants;->O_CREAT:I

    sget v6, Landroid/system/OsConstants;->O_RDWR:I

    or-int/2addr v5, v6

    const/16 v6, 0x1b6

    invoke-static {v1, v5, v6}, Landroid/system/Os;->open(Ljava/lang/String;II)Ljava/io/FileDescriptor;

    move-result-object v1

    .line 133
    .local v1, "fd":Ljava/io/FileDescriptor;
    sget-wide v5, Lcom/android/server/BootKeeperStubImpl;->INIT_PLACEHOLDER_SIZE:J

    const-wide/16 v7, 0x0

    invoke-static {v1, v7, v8, v5, v6}, Landroid/system/Os;->posix_fallocate(Ljava/io/FileDescriptor;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    .end local v1    # "fd":Ljava/io/FileDescriptor;
    goto :goto_0

    .line 134
    :catch_0
    move-exception v1

    .line 135
    .local v1, "ex":Ljava/lang/Exception;
    const-string/jumbo v5, "write file fail:"

    invoke-static {v2, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 137
    .end local v1    # "ex":Ljava/lang/Exception;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "create PlaceHolderFile size="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v5

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    .end local v3    # "availableSpaceBytes":J
    goto :goto_1

    .line 140
    :cond_1
    invoke-virtual {p0, v0}, Lcom/android/server/BootKeeperStubImpl;->takeUpSpace(Ljava/io/File;)V

    .line 142
    :goto_1
    return-void
.end method

.method private releaseSpace()Z
    .locals 6

    .line 146
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/android/server/BootKeeperStubImpl;->PLACE_HOLDER_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 147
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 148
    return v2

    .line 149
    :cond_0
    sget-object v1, Lcom/android/server/BootKeeperStubImpl$1;->$SwitchMap$com$android$server$BootKeeperStubImpl$BOOTKEEPER_STRATEGY:[I

    iget-object v3, p0, Lcom/android/server/BootKeeperStubImpl;->mBootStrategy:Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    invoke-virtual {v3}, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;->ordinal()I

    move-result v3

    aget v1, v1, v3

    const-string v3, "BootKeeper"

    packed-switch v1, :pswitch_data_0

    .line 172
    const-string v1, "releaseSpace: unKnow Boot keeper strategy"

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    return v2

    .line 165
    :pswitch_0
    sget-wide v4, Lcom/android/server/BootKeeperStubImpl;->SPACE_FREE_EACH:J

    neg-long v4, v4

    invoke-direct {p0, v0, v4, v5}, Lcom/android/server/BootKeeperStubImpl;->scale(Ljava/io/File;J)Z

    move-result v1

    if-nez v1, :cond_1

    .line 166
    const-string v1, "releaseSpace fail:"

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    return v2

    .line 169
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "releaseSpace: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-wide v4, Lcom/android/server/BootKeeperStubImpl;->SPACE_FREE_EACH:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    goto :goto_1

    .line 151
    :pswitch_1
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v1

    const-wide/16 v4, 0x0

    cmp-long v1, v1, v4

    if-gtz v1, :cond_2

    .line 152
    const-string v1, "canot release space,placeholder size is 0"

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :cond_2
    :try_start_0
    new-instance v1, Ljava/io/FileWriter;

    invoke-direct {v1, v0}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    .line 156
    .local v1, "fileWriter":Ljava/io/FileWriter;
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 157
    invoke-virtual {v1}, Ljava/io/FileWriter;->flush()V

    .line 158
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    .end local v1    # "fileWriter":Ljava/io/FileWriter;
    goto :goto_0

    .line 159
    :catch_0
    move-exception v1

    .line 160
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 162
    .end local v1    # "e":Ljava/io/IOException;
    :goto_0
    const-string v1, "releaseSpace: clear placeholder content"

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    nop

    .line 175
    :goto_1
    const/4 v1, 0x1

    return v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private scale(Ljava/io/File;J)Z
    .locals 12
    .param p1, "file"    # Ljava/io/File;
    .param p2, "deltaSpace"    # J

    .line 179
    const/4 v0, 0x1

    .line 180
    .local v0, "status":Z
    const/4 v1, 0x0

    .line 181
    .local v1, "inChannel":Ljava/nio/channels/FileChannel;
    const/4 v2, 0x0

    .line 183
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    const/4 v4, 0x1

    invoke-direct {v3, p1, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    move-object v2, v3

    .line 184
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    move-object v1, v3

    .line 185
    const-wide/16 v3, 0x0

    cmp-long v5, p2, v3

    if-gez v5, :cond_0

    .line 186
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v3

    add-long/2addr v3, p2

    invoke-virtual {v1, v3, v4}, Ljava/nio/channels/FileChannel;->truncate(J)Ljava/nio/channels/FileChannel;

    goto :goto_1

    .line 189
    :cond_0
    const/16 v5, 0x1000

    new-array v6, v5, [B

    .line 190
    .local v6, "buffer":[B
    array-length v7, v6

    int-to-long v7, v7

    div-long v7, p2, v7

    long-to-int v7, v7

    .line 191
    .local v7, "n":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    const/4 v9, 0x0

    if-ge v8, v7, :cond_1

    .line 192
    array-length v10, v6

    invoke-virtual {v2, v6, v9, v10}, Ljava/io/FileOutputStream;->write([BII)V

    .line 191
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 194
    .end local v8    # "i":I
    :cond_1
    array-length v8, v6

    mul-int/2addr v8, v7

    int-to-long v10, v8

    sub-long v10, p2, v10

    .line 195
    .local v10, "diff":J
    cmp-long v3, v10, v3

    if-lez v3, :cond_2

    .line 196
    new-array v3, v5, [B

    .line 197
    .local v3, "extraBuffer":[B
    array-length v4, v3

    invoke-virtual {v2, v3, v9, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    .end local v3    # "extraBuffer":[B
    .end local v6    # "buffer":[B
    .end local v7    # "n":I
    .end local v10    # "diff":J
    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    .line 206
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    .line 207
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/FileDescriptor;->sync()V

    .line 208
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 204
    :catchall_0
    move-exception v3

    goto :goto_3

    .line 200
    :catch_0
    move-exception v3

    .line 201
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "BootKeeper"

    const-string v5, "scale file fail"

    invoke-static {v4, v5, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 202
    const/4 v0, 0x0

    .line 204
    .end local v3    # "e":Ljava/lang/Exception;
    if-eqz v1, :cond_3

    .line 206
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    .line 207
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/FileDescriptor;->sync()V

    .line 208
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 209
    :catch_1
    move-exception v3

    .line 210
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 211
    const/4 v0, 0x0

    .line 212
    .end local v3    # "e":Ljava/io/IOException;
    :goto_2
    nop

    .line 216
    :cond_3
    return v0

    .line 204
    :goto_3
    if-eqz v1, :cond_4

    .line 206
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    .line 207
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/FileDescriptor;->sync()V

    .line 208
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 212
    goto :goto_4

    .line 209
    :catch_2
    move-exception v4

    .line 210
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 211
    const/4 v0, 0x0

    .line 215
    .end local v4    # "e":Ljava/io/IOException;
    :cond_4
    :goto_4
    throw v3
.end method


# virtual methods
.method public afterBoot(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 71
    sget-boolean v0, Lcom/android/server/BootKeeperStubImpl;->ENABLE_BOOTKEEPER:Z

    if-nez v0, :cond_0

    .line 72
    return-void

    .line 74
    :cond_0
    invoke-direct {p0}, Lcom/android/server/BootKeeperStubImpl;->onBootSuccess()V

    .line 75
    iget-boolean v0, p0, Lcom/android/server/BootKeeperStubImpl;->mReleaseSpaceTrigger:Z

    if-eqz v0, :cond_1

    .line 76
    invoke-static {p1, v0}, Lcom/android/server/BootKeeperStubImpl$BootKeeperOneTrack;->reportReleaseSpaceOneTrack(Landroid/content/Context;Z)V

    .line 78
    :cond_1
    return-void
.end method

.method public beforeBoot()V
    .locals 1

    .line 62
    sget-boolean v0, Lcom/android/server/BootKeeperStubImpl;->ENABLE_BOOTKEEPER:Z

    if-nez v0, :cond_0

    .line 63
    return-void

    .line 65
    :cond_0
    invoke-direct {p0}, Lcom/android/server/BootKeeperStubImpl;->onBootStart()V

    .line 67
    return-void
.end method

.method takeUpSpace(Ljava/io/File;)V
    .locals 9
    .param p1, "placeholder"    # Ljava/io/File;

    .line 220
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 221
    .local v0, "placeHolderSize":J
    sget-wide v2, Lcom/android/server/BootKeeperStubImpl;->INIT_PLACEHOLDER_SIZE:J

    sub-long/2addr v2, v0

    .line 222
    .local v2, "diff":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    .line 223
    invoke-direct {p0}, Lcom/android/server/BootKeeperStubImpl;->getAvailableSpace()J

    move-result-wide v4

    .line 224
    .local v4, "availableSpaceBytes":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "availableSpaceBytes:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "BootKeeper"

    invoke-static {v7, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    sget-object v6, Lcom/android/server/BootKeeperStubImpl$1;->$SwitchMap$com$android$server$BootKeeperStubImpl$BOOTKEEPER_STRATEGY:[I

    iget-object v8, p0, Lcom/android/server/BootKeeperStubImpl;->mBootStrategy:Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    invoke-virtual {v8}, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;->ordinal()I

    move-result v8

    aget v6, v6, v8

    const-string/jumbo v8, "take up space:"

    packed-switch v6, :pswitch_data_0

    .line 243
    const-string v6, "releaseSpace: unKnow Boot keeper strategy"

    invoke-static {v7, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 237
    :pswitch_0
    cmp-long v6, v4, v2

    if-lez v6, :cond_1

    .line 238
    invoke-direct {p0, p1, v2, v3}, Lcom/android/server/BootKeeperStubImpl;->scale(Ljava/io/File;J)Z

    .line 239
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 227
    :pswitch_1
    cmp-long v6, v4, v2

    if-lez v6, :cond_0

    .line 228
    invoke-direct {p0, p1, v2, v3}, Lcom/android/server/BootKeeperStubImpl;->scale(Ljava/io/File;J)Z

    .line 229
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 231
    :cond_0
    invoke-direct {p0, p1, v4, v5}, Lcom/android/server/BootKeeperStubImpl;->scale(Ljava/io/File;J)Z

    .line 232
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    nop

    .line 246
    .end local v4    # "availableSpaceBytes":J
    :cond_1
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
