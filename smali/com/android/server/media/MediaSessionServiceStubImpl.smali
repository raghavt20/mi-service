.class public Lcom/android/server/media/MediaSessionServiceStubImpl;
.super Lcom/android/server/media/MediaSessionServiceStub;
.source "MediaSessionServiceStubImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.media.MediaServiceStub$$"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MediaSessionServiceStubImpl"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/android/server/media/MediaSessionServiceStub;-><init>()V

    return-void
.end method


# virtual methods
.method public notifyPowerKeeperKeyEvent(ILandroid/view/KeyEvent;)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .line 54
    const/16 v0, 0x3ea

    if-ne p1, v0, :cond_1

    .line 55
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 56
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "eventcode"

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 57
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "down"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 58
    invoke-static {}, Lcom/miui/whetstone/PowerKeeperPolicy;->getInstance()Lcom/miui/whetstone/PowerKeeperPolicy;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifyKeyEvent(Landroid/os/Bundle;)V

    .line 60
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_1
    return-void
.end method

.method public startVoiceAssistant(Landroid/content/Context;)Z
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .line 28
    const-string v0, "com.xiaomi.voiceassistant.CTAAlertActivity"

    const-string v1, "MediaSessionServiceStubImpl"

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.ASSIST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 29
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "com.miui.voiceassist"

    .line 30
    .local v3, "MIUI_VOICE_ASSIST_PKG":Ljava/lang/String;
    const-string v10, "com.miui.voiceassist"

    invoke-virtual {v2, v10}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 32
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v4

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v6

    const-wide/32 v7, 0x10000

    const/4 v9, 0x0

    .line 32
    move-object v5, v2

    invoke-interface/range {v4 .. v9}, Landroid/content/pm/IPackageManager;->resolveIntent(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ResolveInfo;

    move-result-object v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    .local v4, "info":Landroid/content/pm/ResolveInfo;
    move-object v5, v0

    .line 36
    .local v5, "MIUI_VOICE_ASSIST_ACTIVITY":Ljava/lang/String;
    const-string v6, "com.xiaomi.voiceassistant.VoiceService"

    move-object v7, v6

    .line 37
    .local v7, "MIUI_VOICE_ASSIST_SERVICE":Ljava/lang/String;
    if-eqz v4, :cond_0

    :try_start_1
    iget-object v8, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v8, :cond_0

    iget-object v8, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 38
    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 39
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    const-string/jumbo v0, "voice_assist_start_from_key"

    const-string v8, "headset"

    invoke-virtual {v2, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    invoke-virtual {v2, v10, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    sget-object v0, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p1, v2, v0}, Landroid/content/Context;->startForegroundServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    .line 43
    const/4 v0, 0x1

    return v0

    .line 45
    :cond_0
    const-string/jumbo v0, "startVoiceAssistant can\'t find service"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 49
    nop

    .end local v4    # "info":Landroid/content/pm/ResolveInfo;
    .end local v5    # "MIUI_VOICE_ASSIST_ACTIVITY":Ljava/lang/String;
    .end local v7    # "MIUI_VOICE_ASSIST_SERVICE":Ljava/lang/String;
    goto :goto_0

    .line 47
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "RemoteException"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 50
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    const/4 v0, 0x0

    return v0
.end method
