.class public final Lcom/android/server/media/MediaServiceStub$$;
.super Ljava/lang/Object;
.source "MediaServiceStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 19
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/media/MediaSessionServiceStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/media/MediaSessionServiceStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.media.MediaSessionServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    new-instance v0, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.media.projection.MediaProjectionManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    return-void
.end method
