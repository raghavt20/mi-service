public class com.android.server.media.MediaSessionServiceStubImpl extends com.android.server.media.MediaSessionServiceStub {
	 /* .source "MediaSessionServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.media.MediaServiceStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # direct methods */
public com.android.server.media.MediaSessionServiceStubImpl ( ) {
	 /* .locals 0 */
	 /* .line 22 */
	 /* invoke-direct {p0}, Lcom/android/server/media/MediaSessionServiceStub;-><init>()V */
	 return;
} // .end method
/* # virtual methods */
public void notifyPowerKeeperKeyEvent ( Integer p0, android.view.KeyEvent p1 ) {
	 /* .locals 3 */
	 /* .param p1, "uid" # I */
	 /* .param p2, "event" # Landroid/view/KeyEvent; */
	 /* .line 54 */
	 /* const/16 v0, 0x3ea */
	 /* if-ne p1, v0, :cond_1 */
	 /* .line 55 */
	 /* new-instance v0, Landroid/os/Bundle; */
	 /* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
	 /* .line 56 */
	 /* .local v0, "b":Landroid/os/Bundle; */
	 final String v1 = "eventcode"; // const-string v1, "eventcode"
	 v2 = 	 (( android.view.KeyEvent ) p2 ).getKeyCode ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I
	 (( android.os.Bundle ) v0 ).putInt ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
	 /* .line 57 */
	 v1 = 	 (( android.view.KeyEvent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I
	 /* if-nez v1, :cond_0 */
	 int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
final String v2 = "down"; // const-string v2, "down"
(( android.os.Bundle ) v0 ).putBoolean ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 58 */
com.miui.whetstone.PowerKeeperPolicy .getInstance ( );
(( com.miui.whetstone.PowerKeeperPolicy ) v1 ).notifyKeyEvent ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifyKeyEvent(Landroid/os/Bundle;)V
/* .line 60 */
} // .end local v0 # "b":Landroid/os/Bundle;
} // :cond_1
return;
} // .end method
public Boolean startVoiceAssistant ( android.content.Context p0 ) {
/* .locals 11 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 28 */
final String v0 = "com.xiaomi.voiceassistant.CTAAlertActivity"; // const-string v0, "com.xiaomi.voiceassistant.CTAAlertActivity"
final String v1 = "MediaSessionServiceStubImpl"; // const-string v1, "MediaSessionServiceStubImpl"
/* new-instance v2, Landroid/content/Intent; */
final String v3 = "android.intent.action.ASSIST"; // const-string v3, "android.intent.action.ASSIST"
/* invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 29 */
/* .local v2, "intent":Landroid/content/Intent; */
final String v3 = "com.miui.voiceassist"; // const-string v3, "com.miui.voiceassist"
/* .line 30 */
/* .local v3, "MIUI_VOICE_ASSIST_PKG":Ljava/lang/String; */
final String v10 = "com.miui.voiceassist"; // const-string v10, "com.miui.voiceassist"
(( android.content.Intent ) v2 ).setPackage ( v10 ); // invoke-virtual {v2, v10}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 32 */
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* .line 33 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
(( android.content.Intent ) v2 ).resolveTypeIfNeeded ( v5 ); // invoke-virtual {v2, v5}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;
/* const-wide/32 v7, 0x10000 */
int v9 = 0; // const/4 v9, 0x0
/* .line 32 */
/* move-object v5, v2 */
/* invoke-interface/range {v4 ..v9}, Landroid/content/pm/IPackageManager;->resolveIntent(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ResolveInfo; */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 35 */
/* .local v4, "info":Landroid/content/pm/ResolveInfo; */
/* move-object v5, v0 */
/* .line 36 */
/* .local v5, "MIUI_VOICE_ASSIST_ACTIVITY":Ljava/lang/String; */
final String v6 = "com.xiaomi.voiceassistant.VoiceService"; // const-string v6, "com.xiaomi.voiceassistant.VoiceService"
/* move-object v7, v6 */
/* .line 37 */
/* .local v7, "MIUI_VOICE_ASSIST_SERVICE":Ljava/lang/String; */
if ( v4 != null) { // if-eqz v4, :cond_0
try { // :try_start_1
v8 = this.activityInfo;
if ( v8 != null) { // if-eqz v8, :cond_0
	 v8 = this.activityInfo;
	 v8 = this.packageName;
	 /* .line 38 */
	 v8 = 	 (( java.lang.String ) v10 ).equals ( v8 ); // invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v8 != null) { // if-eqz v8, :cond_0
		 v8 = this.activityInfo;
		 v8 = this.name;
		 /* .line 39 */
		 v0 = 		 (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 40 */
			 /* const-string/jumbo v0, "voice_assist_start_from_key" */
			 final String v8 = "headset"; // const-string v8, "headset"
			 (( android.content.Intent ) v2 ).putExtra ( v0, v8 ); // invoke-virtual {v2, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 41 */
			 (( android.content.Intent ) v2 ).setClassName ( v10, v6 ); // invoke-virtual {v2, v10, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 42 */
			 v0 = android.os.UserHandle.CURRENT;
			 (( android.content.Context ) p1 ).startForegroundServiceAsUser ( v2, v0 ); // invoke-virtual {p1, v2, v0}, Landroid/content/Context;->startForegroundServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
			 /* .line 43 */
			 int v0 = 1; // const/4 v0, 0x1
			 /* .line 45 */
		 } // :cond_0
		 /* const-string/jumbo v0, "startVoiceAssistant can\'t find service" */
		 android.util.Log .i ( v1,v0 );
		 /* :try_end_1 */
		 /* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
		 /* .line 49 */
		 /* nop */
	 } // .end local v4 # "info":Landroid/content/pm/ResolveInfo;
} // .end local v5 # "MIUI_VOICE_ASSIST_ACTIVITY":Ljava/lang/String;
} // .end local v7 # "MIUI_VOICE_ASSIST_SERVICE":Ljava/lang/String;
/* .line 47 */
/* :catch_0 */
/* move-exception v0 */
/* .line 48 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v4 = "RemoteException"; // const-string v4, "RemoteException"
android.util.Log .e ( v1,v4,v0 );
/* .line 50 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
