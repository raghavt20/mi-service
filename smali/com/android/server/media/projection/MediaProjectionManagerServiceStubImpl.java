public class com.android.server.media.projection.MediaProjectionManagerServiceStubImpl implements com.android.server.media.projection.MediaProjectionManagerServiceStub {
	 /* .source "MediaProjectionManagerServiceStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final java.util.LinkedList mMediaProjectionClients;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/LinkedList<", */
	 /* "Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.media.projection.MediaProjectionManagerServiceStubImpl ( ) {
/* .locals 1 */
/* .line 17 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 21 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.mMediaProjectionClients = v0;
return;
} // .end method
private Boolean isScreenRecorder ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 63 */
final String v0 = "com.miui.screenrecorder"; // const-string v0, "com.miui.screenrecorder"
v0 = android.text.TextUtils .equals ( p1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 64 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* .line 66 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isUcar ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 70 */
final String v0 = "com.miui.carlink"; // const-string v0, "com.miui.carlink"
v0 = android.text.TextUtils .equals ( p1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 71 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* .line 73 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
/* # virtual methods */
public void addMediaProjection ( com.android.server.media.projection.MediaProjectionManagerService$MediaProjection p0 ) {
/* .locals 2 */
/* .param p1, "mp" # Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection; */
/* .line 78 */
final String v0 = "MediaProjectionManagerServiceStubImpl"; // const-string v0, "MediaProjectionManagerServiceStubImpl"
final String v1 = "addMediaProjection"; // const-string v1, "addMediaProjection"
android.util.Log .d ( v0,v1 );
/* .line 79 */
(( com.android.server.media.projection.MediaProjectionManagerServiceStubImpl ) p0 ).removeMediaProjection ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->removeMediaProjection(Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;)V
/* .line 80 */
v0 = this.mMediaProjectionClients;
int v1 = 0; // const/4 v1, 0x0
(( java.util.LinkedList ) v0 ).add ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V
/* .line 81 */
return;
} // .end method
public android.media.projection.MediaProjectionInfo getActiveProjectionInfo ( ) {
/* .locals 2 */
/* .line 101 */
v0 = this.mMediaProjectionClients;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
/* if-nez v0, :cond_0 */
/* .line 102 */
int v0 = 0; // const/4 v0, 0x0
/* .line 104 */
} // :cond_0
v0 = this.mMediaProjectionClients;
int v1 = 0; // const/4 v1, 0x0
(( java.util.LinkedList ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection; */
(( com.android.server.media.projection.MediaProjectionManagerService$MediaProjection ) v0 ).getProjectionInfo ( ); // invoke-virtual {v0}, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->getProjectionInfo()Landroid/media/projection/MediaProjectionInfo;
} // .end method
public com.android.server.media.projection.MediaProjectionManagerService$MediaProjection getTopProject ( ) {
/* .locals 2 */
/* .line 130 */
v0 = this.mMediaProjectionClients;
v0 = (( java.util.LinkedList ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 131 */
int v0 = 0; // const/4 v0, 0x0
/* .line 133 */
} // :cond_0
v0 = this.mMediaProjectionClients;
int v1 = 0; // const/4 v1, 0x0
(( java.util.LinkedList ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection; */
} // .end method
public void handleForegroundServicesChanged ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "serviceTypes" # I */
/* .line 32 */
final String v0 = "handleForegroundServicesChanged"; // const-string v0, "handleForegroundServicesChanged"
final String v1 = "MediaProjectionManagerServiceStubImpl"; // const-string v1, "MediaProjectionManagerServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 33 */
v0 = this.mMediaProjectionClients;
(( java.util.LinkedList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
/* .line 34 */
/* .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;>;" */
} // :cond_0
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 35 */
/* check-cast v2, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection; */
/* .line 36 */
/* .local v2, "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* iget v3, v2, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->uid:I */
/* if-ne v3, p2, :cond_0 */
/* iget v3, v2, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->pid:I */
/* if-eq v3, p1, :cond_1 */
/* .line 38 */
/* .line 41 */
} // :cond_1
v3 = (( com.android.server.media.projection.MediaProjectionManagerService$MediaProjection ) v2 ).requiresForegroundService ( ); // invoke-virtual {v2}, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->requiresForegroundService()Z
/* if-nez v3, :cond_2 */
/* .line 42 */
/* .line 45 */
} // :cond_2
/* and-int/lit8 v3, p3, 0x20 */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 46 */
/* .line 49 */
} // :cond_3
v3 = this.packageName;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->isUcar(Ljava/lang/String;)Z */
/* if-nez v3, :cond_5 */
v3 = this.packageName;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->isScreenRecorder(Ljava/lang/String;)Z */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 55 */
} // :cond_4
/* .line 56 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "handleForegroundServicesChanged stop projection for "; // const-string v4, "handleForegroundServicesChanged stop projection for "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.packageName;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v3 );
/* .line 58 */
(( com.android.server.media.projection.MediaProjectionManagerService$MediaProjection ) v2 ).stop ( ); // invoke-virtual {v2}, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->stop()V
/* .line 59 */
} // .end local v2 # "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;
/* .line 50 */
/* .restart local v2 # "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection; */
} // :cond_5
} // :goto_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "handleForegroundServicesChanged don\'t stop projection for "; // const-string v4, "handleForegroundServicesChanged don\'t stop projection for "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.packageName;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v3 );
/* .line 52 */
/* .line 60 */
} // .end local v2 # "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;
} // :cond_6
return;
} // .end method
public Boolean isSupportMutilMediaProjection ( ) {
/* .locals 1 */
/* .line 27 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean isValidMediaProjection ( android.os.IBinder p0 ) {
/* .locals 3 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .line 91 */
v0 = this.mMediaProjectionClients;
(( java.util.LinkedList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection; */
/* .line 92 */
/* .local v1, "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection; */
(( com.android.server.media.projection.MediaProjectionManagerService$MediaProjection ) v1 ).asBinder ( ); // invoke-virtual {v1}, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->asBinder()Landroid/os/IBinder;
/* if-ne v2, p1, :cond_0 */
/* .line 93 */
int v0 = 1; // const/4 v0, 0x1
/* .line 95 */
} // .end local v1 # "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;
} // :cond_0
/* .line 96 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void removeMediaProjection ( com.android.server.media.projection.MediaProjectionManagerService$MediaProjection p0 ) {
/* .locals 2 */
/* .param p1, "mp" # Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection; */
/* .line 85 */
final String v0 = "MediaProjectionManagerServiceStubImpl"; // const-string v0, "MediaProjectionManagerServiceStubImpl"
final String v1 = "removeMediaProjection"; // const-string v1, "removeMediaProjection"
android.util.Log .d ( v0,v1 );
/* .line 86 */
v0 = this.mMediaProjectionClients;
(( java.util.LinkedList ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
/* .line 87 */
return;
} // .end method
public void stopActiveProjection ( ) {
/* .locals 2 */
/* .line 109 */
final String v0 = "MediaProjectionManagerServiceStubImpl"; // const-string v0, "MediaProjectionManagerServiceStubImpl"
/* const-string/jumbo v1, "stopActiveProjection" */
android.util.Log .d ( v0,v1 );
/* .line 110 */
v0 = this.mMediaProjectionClients;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
/* if-lez v0, :cond_0 */
/* .line 111 */
v0 = this.mMediaProjectionClients;
int v1 = 0; // const/4 v1, 0x0
(( java.util.LinkedList ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection; */
(( com.android.server.media.projection.MediaProjectionManagerService$MediaProjection ) v0 ).stop ( ); // invoke-virtual {v0}, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->stop()V
/* .line 113 */
} // :cond_0
return;
} // .end method
public void stopProjections ( ) {
/* .locals 2 */
/* .line 117 */
final String v0 = "MediaProjectionManagerServiceStubImpl"; // const-string v0, "MediaProjectionManagerServiceStubImpl"
/* const-string/jumbo v1, "stopProjections" */
android.util.Log .d ( v0,v1 );
/* .line 118 */
v0 = this.mMediaProjectionClients;
(( java.util.LinkedList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
/* .line 119 */
/* .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 120 */
/* check-cast v1, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection; */
/* .line 121 */
/* .local v1, "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection; */
/* .line 122 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 123 */
(( com.android.server.media.projection.MediaProjectionManagerService$MediaProjection ) v1 ).stop ( ); // invoke-virtual {v1}, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->stop()V
/* .line 125 */
} // .end local v1 # "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;
} // :cond_0
/* .line 126 */
} // :cond_1
return;
} // .end method
