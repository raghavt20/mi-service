.class public Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;
.super Ljava/lang/Object;
.source "MediaProjectionManagerServiceStubImpl.java"

# interfaces
.implements Lcom/android/server/media/projection/MediaProjectionManagerServiceStub;


# static fields
.field private static final TAG:Ljava/lang/String; = "MediaProjectionManagerServiceStubImpl"


# instance fields
.field private final mMediaProjectionClients:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->mMediaProjectionClients:Ljava/util/LinkedList;

    return-void
.end method

.method private isScreenRecorder(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 63
    const-string v0, "com.miui.screenrecorder"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const/4 v0, 0x1

    return v0

    .line 66
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isUcar(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 70
    const-string v0, "com.miui.carlink"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    const/4 v0, 0x1

    return v0

    .line 73
    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public addMediaProjection(Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;)V
    .locals 2
    .param p1, "mp"    # Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;

    .line 78
    const-string v0, "MediaProjectionManagerServiceStubImpl"

    const-string v1, "addMediaProjection"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-virtual {p0, p1}, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->removeMediaProjection(Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;)V

    .line 80
    iget-object v0, p0, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->mMediaProjectionClients:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 81
    return-void
.end method

.method public getActiveProjectionInfo()Landroid/media/projection/MediaProjectionInfo;
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->mMediaProjectionClients:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 102
    const/4 v0, 0x0

    return-object v0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->mMediaProjectionClients:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;

    invoke-virtual {v0}, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->getProjectionInfo()Landroid/media/projection/MediaProjectionInfo;

    move-result-object v0

    return-object v0
.end method

.method public getTopProject()Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->mMediaProjectionClients:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    const/4 v0, 0x0

    return-object v0

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->mMediaProjectionClients:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;

    return-object v0
.end method

.method public handleForegroundServicesChanged(III)V
    .locals 5
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "serviceTypes"    # I

    .line 32
    const-string v0, "handleForegroundServicesChanged"

    const-string v1, "MediaProjectionManagerServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    iget-object v0, p0, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->mMediaProjectionClients:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 34
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 35
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;

    .line 36
    .local v2, "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;
    if-eqz v2, :cond_0

    iget v3, v2, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->uid:I

    if-ne v3, p2, :cond_0

    iget v3, v2, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->pid:I

    if-eq v3, p1, :cond_1

    .line 38
    goto :goto_0

    .line 41
    :cond_1
    invoke-virtual {v2}, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->requiresForegroundService()Z

    move-result v3

    if-nez v3, :cond_2

    .line 42
    goto :goto_0

    .line 45
    :cond_2
    and-int/lit8 v3, p3, 0x20

    if-eqz v3, :cond_3

    .line 46
    goto :goto_0

    .line 49
    :cond_3
    iget-object v3, v2, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->packageName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->isUcar(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v2, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->packageName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->isScreenRecorder(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    goto :goto_1

    .line 55
    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 56
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleForegroundServicesChanged stop projection for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-virtual {v2}, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->stop()V

    .line 59
    .end local v2    # "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;
    goto :goto_0

    .line 50
    .restart local v2    # "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;
    :cond_5
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleForegroundServicesChanged don\'t stop projection for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    goto :goto_0

    .line 60
    .end local v2    # "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;
    :cond_6
    return-void
.end method

.method public isSupportMutilMediaProjection()Z
    .locals 1

    .line 27
    const/4 v0, 0x1

    return v0
.end method

.method public isValidMediaProjection(Landroid/os/IBinder;)Z
    .locals 3
    .param p1, "token"    # Landroid/os/IBinder;

    .line 91
    iget-object v0, p0, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->mMediaProjectionClients:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;

    .line 92
    .local v1, "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;
    invoke-virtual {v1}, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 93
    const/4 v0, 0x1

    return v0

    .line 95
    .end local v1    # "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;
    :cond_0
    goto :goto_0

    .line 96
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public removeMediaProjection(Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;)V
    .locals 2
    .param p1, "mp"    # Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;

    .line 85
    const-string v0, "MediaProjectionManagerServiceStubImpl"

    const-string v1, "removeMediaProjection"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v0, p0, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->mMediaProjectionClients:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 87
    return-void
.end method

.method public stopActiveProjection()V
    .locals 2

    .line 109
    const-string v0, "MediaProjectionManagerServiceStubImpl"

    const-string/jumbo v1, "stopActiveProjection"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v0, p0, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->mMediaProjectionClients:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->mMediaProjectionClients:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;

    invoke-virtual {v0}, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->stop()V

    .line 113
    :cond_0
    return-void
.end method

.method public stopProjections()V
    .locals 2

    .line 117
    const-string v0, "MediaProjectionManagerServiceStubImpl"

    const-string/jumbo v1, "stopProjections"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iget-object v0, p0, Lcom/android/server/media/projection/MediaProjectionManagerServiceStubImpl;->mMediaProjectionClients:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 119
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 120
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;

    .line 121
    .local v1, "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 122
    if-eqz v1, :cond_0

    .line 123
    invoke-virtual {v1}, Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;->stop()V

    .line 125
    .end local v1    # "projection":Lcom/android/server/media/projection/MediaProjectionManagerService$MediaProjection;
    :cond_0
    goto :goto_0

    .line 126
    :cond_1
    return-void
.end method
