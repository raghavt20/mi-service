class com.android.server.MiuiBatteryServiceImpl$HandleInfo {
	 /* .source "MiuiBatteryServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "HandleInfo" */
} // .end annotation
/* # instance fields */
Integer mBatteryLevel;
java.lang.String mBatteryStats;
java.lang.String mColorNumber;
Integer mConnectState;
java.lang.String mFwVersion;
java.lang.String mHidName;
Integer mPid;
Integer mVid;
final com.android.server.MiuiBatteryServiceImpl this$0; //synthetic
/* # direct methods */
public com.android.server.MiuiBatteryServiceImpl$HandleInfo ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryServiceImpl; */
/* .param p2, "usbDevice" # Landroid/hardware/usb/UsbDevice; */
/* .line 1635 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1636 */
v0 = (( android.hardware.usb.UsbDevice ) p2 ).getProductId ( ); // invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getProductId()I
/* iput v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mPid:I */
/* .line 1637 */
v0 = (( android.hardware.usb.UsbDevice ) p2 ).getVendorId ( ); // invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getVendorId()I
/* iput v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mVid:I */
/* .line 1638 */
(( android.hardware.usb.UsbDevice ) p2 ).getVersion ( ); // invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getVersion()Ljava/lang/String;
this.mFwVersion = v0;
/* .line 1639 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "hid-"; // const-string v1, "hid-"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.hardware.usb.UsbDevice ) p2 ).getSerialNumber ( ); // invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "-battery"; // const-string v1, "-battery"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
this.mHidName = v0;
/* .line 1640 */
return;
} // .end method
/* # virtual methods */
void setBatteryLevel ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "level" # I */
/* .line 1647 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mBatteryLevel:I */
/* .line 1648 */
return;
} // .end method
void setBatteryStats ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "batteryStats" # Ljava/lang/String; */
/* .line 1651 */
this.mBatteryStats = p1;
/* .line 1652 */
return;
} // .end method
void setColorNumber ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "color" # Ljava/lang/String; */
/* .line 1655 */
this.mColorNumber = p1;
/* .line 1656 */
return;
} // .end method
void setConnectState ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "connectState" # I */
/* .line 1643 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mConnectState:I */
/* .line 1644 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 1660 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "HandleInfo{mConnectState="; // const-string v1, "HandleInfo{mConnectState="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mConnectState:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mPid="; // const-string v1, ", mPid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mVid="; // const-string v1, ", mVid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mVid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mBatteryLevel="; // const-string v1, ", mBatteryLevel="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mBatteryLevel:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mHidName=\'"; // const-string v1, ", mHidName=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mHidName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mBatteryStats=\'"; // const-string v2, ", mBatteryStats=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mBatteryStats;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mFwVersion=\'"; // const-string v2, ", mFwVersion=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mFwVersion;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mColorNumber=\'"; // const-string v2, ", mColorNumber=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mColorNumber;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
