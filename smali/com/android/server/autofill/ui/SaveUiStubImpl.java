public class com.android.server.autofill.ui.SaveUiStubImpl extends com.android.server.autofill.ui.SaveUiStub {
	 /* .source "SaveUiStubImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.autofill.ui.SaveUiStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String AUTOFILL_ACTIVITY;
private static final java.lang.String AUTOFILL_PACKAGE;
private static final java.lang.String AUTO_CANCEL;
private static final java.lang.String AUTO_SAVE;
private static Integer MIUI_VERSION_CODE;
private static final java.lang.String NEVER_SHOW_SAVE_UI;
private static final java.lang.String SAVEUI_ACTION;
private static final java.lang.String SERVICE_SP_NAME;
private static final java.lang.String TAG;
private static miuix.appcompat.app.AlertDialog mDialog;
/* # instance fields */
private android.content.Context mContext;
/* # direct methods */
static com.android.server.autofill.ui.SaveUiStubImpl ( ) {
	 /* .locals 2 */
	 /* .line 51 */
	 final String v0 = "ro.miui.ui.version.code"; // const-string v0, "ro.miui.ui.version.code"
	 /* const/16 v1, 0xd */
	 v0 = 	 android.os.SystemProperties .getInt ( v0,v1 );
	 return;
} // .end method
public com.android.server.autofill.ui.SaveUiStubImpl ( ) {
	 /* .locals 0 */
	 /* .line 41 */
	 /* invoke-direct {p0}, Lcom/android/server/autofill/ui/SaveUiStub;-><init>()V */
	 return;
} // .end method
static void lambda$showDialog$0 ( android.content.DialogInterface$OnClickListener p0, android.content.DialogInterface p1, Integer p2 ) { //synthethic
	 /* .locals 2 */
	 /* .param p0, "okListener" # Landroid/content/DialogInterface$OnClickListener; */
	 /* .param p1, "v" # Landroid/content/DialogInterface; */
	 /* .param p2, "w" # I */
	 /* .line 102 */
	 final String v0 = "AutofillSaveUi"; // const-string v0, "AutofillSaveUi"
	 /* const-string/jumbo v1, "showDialog save" */
	 android.util.Log .d ( v0,v1 );
	 /* .line 103 */
	 int v0 = 0; // const/4 v0, 0x0
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 104 */
	 com.android.server.autofill.ui.SaveUiStubImpl .setDialog ( v1 );
	 /* .line 105 */
	 return;
} // .end method
static void lambda$showDialog$1 ( android.content.DialogInterface$OnClickListener p0, android.content.DialogInterface p1, Integer p2 ) { //synthethic
	 /* .locals 2 */
	 /* .param p0, "cancelListener" # Landroid/content/DialogInterface$OnClickListener; */
	 /* .param p1, "v" # Landroid/content/DialogInterface; */
	 /* .param p2, "w" # I */
	 /* .line 108 */
	 final String v0 = "AutofillSaveUi"; // const-string v0, "AutofillSaveUi"
	 /* const-string/jumbo v1, "showDialog cancel" */
	 android.util.Log .d ( v0,v1 );
	 /* .line 109 */
	 int v0 = 0; // const/4 v0, 0x0
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 110 */
	 com.android.server.autofill.ui.SaveUiStubImpl .setDialog ( v1 );
	 /* .line 111 */
	 return;
} // .end method
static void lambda$showDialog$2 ( android.content.DialogInterface$OnDismissListener p0, android.content.DialogInterface p1 ) { //synthethic
	 /* .locals 2 */
	 /* .param p0, "onDismissListener" # Landroid/content/DialogInterface$OnDismissListener; */
	 /* .param p1, "v" # Landroid/content/DialogInterface; */
	 /* .line 115 */
	 final String v0 = "AutofillSaveUi"; // const-string v0, "AutofillSaveUi"
	 /* const-string/jumbo v1, "showDialog dismiss" */
	 android.util.Log .d ( v0,v1 );
	 /* .line 116 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 117 */
	 com.android.server.autofill.ui.SaveUiStubImpl .setDialog ( v0 );
	 /* .line 118 */
	 return;
} // .end method
public static synchronized void setDialog ( miuix.appcompat.app.AlertDialog p0 ) {
	 /* .locals 1 */
	 /* .param p0, "dialog" # Lmiuix/appcompat/app/AlertDialog; */
	 /* const-class v0, Lcom/android/server/autofill/ui/SaveUiStubImpl; */
	 /* monitor-enter v0 */
	 /* .line 239 */
	 try { // :try_start_0
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* .line 240 */
		 /* monitor-exit v0 */
		 return;
		 /* .line 238 */
	 } // .end local p0 # "dialog":Lmiuix/appcompat/app/AlertDialog;
	 /* :catchall_0 */
	 /* move-exception p0 */
	 /* monitor-exit v0 */
	 /* throw p0 */
} // .end method
/* # virtual methods */
public void autoCancel ( ) {
	 /* .locals 5 */
	 /* .line 211 */
	 v0 = com.android.server.autofill.ui.SaveUiStubImpl.mDialog;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 215 */
		 final String v0 = "AutofillSaveUi"; // const-string v0, "AutofillSaveUi"
		 final String v1 = "autoCancel checked=true"; // const-string v1, "autoCancel checked=true"
		 android.util.Log .d ( v0,v1 );
		 /* .line 217 */
		 try { // :try_start_0
			 v0 = com.android.server.autofill.ui.SaveUiStubImpl.mDialog;
			 (( miuix.appcompat.app.AlertDialog ) v0 ).getContext ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->getContext()Landroid/content/Context;
			 final String v1 = "com.miui.contentcatcher"; // const-string v1, "com.miui.contentcatcher"
			 int v2 = 3; // const/4 v2, 0x3
			 (( android.content.Context ) v0 ).createPackageContext ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
			 /* .line 219 */
			 /* .local v0, "serviceContext":Landroid/content/Context; */
			 final String v1 = "multi_process"; // const-string v1, "multi_process"
			 int v2 = 4; // const/4 v2, 0x4
			 (( android.content.Context ) v0 ).getSharedPreferences ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
			 /* .line 221 */
			 /* .local v1, "sharedPreferences":Landroid/content/SharedPreferences; */
			 final String v3 = "never_show_save_ui"; // const-string v3, "never_show_save_ui"
			 int v4 = 1; // const/4 v4, 0x1
			 /* .line 222 */
			 final String v3 = "auto_cancel"; // const-string v3, "auto_cancel"
			 /* :try_end_0 */
			 /* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 225 */
		 } // .end local v0 # "serviceContext":Landroid/content/Context;
	 } // .end local v1 # "sharedPreferences":Landroid/content/SharedPreferences;
	 /* .line 223 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 224 */
	 /* .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
	 (( android.content.pm.PackageManager$NameNotFoundException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
	 /* .line 227 */
} // .end local v0 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :cond_0
} // :goto_0
return;
} // .end method
public void autoSave ( ) {
/* .locals 5 */
/* .line 191 */
v0 = com.android.server.autofill.ui.SaveUiStubImpl.mDialog;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 195 */
final String v0 = "AutofillSaveUi"; // const-string v0, "AutofillSaveUi"
final String v1 = "autoSave checked=true"; // const-string v1, "autoSave checked=true"
android.util.Log .d ( v0,v1 );
/* .line 197 */
try { // :try_start_0
v0 = com.android.server.autofill.ui.SaveUiStubImpl.mDialog;
(( miuix.appcompat.app.AlertDialog ) v0 ).getContext ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->getContext()Landroid/content/Context;
final String v1 = "com.miui.contentcatcher"; // const-string v1, "com.miui.contentcatcher"
int v2 = 3; // const/4 v2, 0x3
(( android.content.Context ) v0 ).createPackageContext ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
/* .line 199 */
/* .local v0, "serviceContext":Landroid/content/Context; */
final String v1 = "multi_process"; // const-string v1, "multi_process"
int v2 = 4; // const/4 v2, 0x4
(( android.content.Context ) v0 ).getSharedPreferences ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
/* .line 201 */
/* .local v1, "sharedPreferences":Landroid/content/SharedPreferences; */
final String v3 = "never_show_save_ui"; // const-string v3, "never_show_save_ui"
int v4 = 1; // const/4 v4, 0x1
/* .line 202 */
final String v3 = "auto_save"; // const-string v3, "auto_save"
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 205 */
} // .end local v0 # "serviceContext":Landroid/content/Context;
} // .end local v1 # "sharedPreferences":Landroid/content/SharedPreferences;
/* .line 203 */
/* :catch_0 */
/* move-exception v0 */
/* .line 204 */
/* .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
(( android.content.pm.PackageManager$NameNotFoundException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
/* .line 207 */
} // .end local v0 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :cond_0
} // :goto_0
return;
} // .end method
public void changeBackground ( android.view.View p0, android.view.WindowManager$LayoutParams p1 ) {
/* .locals 4 */
/* .param p1, "decor" # Landroid/view/View; */
/* .param p2, "params" # Landroid/view/WindowManager$LayoutParams; */
/* .line 165 */
if ( p1 != null) { // if-eqz p1, :cond_3
/* if-nez p2, :cond_0 */
/* .line 168 */
} // :cond_0
(( android.view.View ) p1 ).getContext ( ); // invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 169 */
v1 = android.os.UserHandle .myUserId ( );
/* .line 168 */
final String v2 = "autofill_service"; // const-string v2, "autofill_service"
android.provider.Settings$Secure .getStringForUser ( v0,v2,v1 );
/* .line 170 */
/* .local v0, "autofillService":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_2 */
/* .line 171 */
final String v1 = "/"; // const-string v1, "/"
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
int v2 = 0; // const/4 v2, 0x0
/* aget-object v1, v1, v2 */
/* .line 172 */
/* .local v1, "packageName":Ljava/lang/String; */
final String v2 = "com.miui.contentcatcher"; // const-string v2, "com.miui.contentcatcher"
v2 = android.text.TextUtils .equals ( v1,v2 );
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 173 */
/* const v2, 0x11080362 */
(( android.view.View ) p1 ).setBackgroundResource ( v2 ); // invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V
/* .line 174 */
/* const/16 v3, 0xa */
/* if-le v2, v3, :cond_1 */
/* .line 175 */
/* iget v2, p2, Landroid/view/WindowManager$LayoutParams;->x:I */
/* add-int/lit8 v2, v2, -0x28 */
/* iput v2, p2, Landroid/view/WindowManager$LayoutParams;->x:I */
/* .line 176 */
/* iget v2, p2, Landroid/view/WindowManager$LayoutParams;->y:I */
/* add-int/lit8 v2, v2, -0x50 */
/* iput v2, p2, Landroid/view/WindowManager$LayoutParams;->y:I */
/* .line 177 */
/* iget v2, p2, Landroid/view/WindowManager$LayoutParams;->width:I */
/* add-int/lit8 v2, v2, 0x64 */
/* iput v2, p2, Landroid/view/WindowManager$LayoutParams;->width:I */
/* .line 178 */
/* iget v2, p2, Landroid/view/WindowManager$LayoutParams;->height:I */
/* add-int/lit16 v2, v2, 0xa0 */
/* iput v2, p2, Landroid/view/WindowManager$LayoutParams;->height:I */
/* .line 180 */
} // :cond_1
/* iget v2, p2, Landroid/view/WindowManager$LayoutParams;->x:I */
/* add-int/lit8 v2, v2, -0x3c */
/* iput v2, p2, Landroid/view/WindowManager$LayoutParams;->x:I */
/* .line 181 */
/* iget v2, p2, Landroid/view/WindowManager$LayoutParams;->y:I */
/* add-int/lit8 v2, v2, -0x3c */
/* iput v2, p2, Landroid/view/WindowManager$LayoutParams;->y:I */
/* .line 182 */
/* iget v2, p2, Landroid/view/WindowManager$LayoutParams;->width:I */
/* add-int/lit8 v2, v2, 0x78 */
/* iput v2, p2, Landroid/view/WindowManager$LayoutParams;->width:I */
/* .line 183 */
/* iget v2, p2, Landroid/view/WindowManager$LayoutParams;->height:I */
/* add-int/lit8 v2, v2, 0x78 */
/* iput v2, p2, Landroid/view/WindowManager$LayoutParams;->height:I */
/* .line 187 */
} // .end local v1 # "packageName":Ljava/lang/String;
} // :cond_2
} // :goto_0
return;
/* .line 166 */
} // .end local v0 # "autofillService":Ljava/lang/String;
} // :cond_3
} // :goto_1
return;
} // .end method
public void setViewPadding ( android.app.Dialog p0 ) {
/* .locals 5 */
/* .param p1, "dialog" # Landroid/app/Dialog; */
/* .line 230 */
(( android.app.Dialog ) p1 ).getWindow ( ); // invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;
/* .line 231 */
/* .local v0, "window":Landroid/view/Window; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v2 = "id"; // const-string v2, "id"
final String v3 = "miuix.stub"; // const-string v3, "miuix.stub"
final String v4 = "customPanel"; // const-string v4, "customPanel"
v1 = (( android.content.res.Resources ) v1 ).getIdentifier ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 232 */
/* .local v1, "id":I */
(( android.view.Window ) v0 ).findViewById ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;
/* check-cast v2, Landroid/view/ViewGroup; */
/* .line 233 */
/* .local v2, "customPanel":Landroid/view/ViewGroup; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 234 */
/* const/16 v3, 0x50 */
int v4 = 0; // const/4 v4, 0x0
(( android.view.ViewGroup ) v2 ).setPadding ( v3, v4, v3, v4 ); // invoke-virtual {v2, v3, v4, v3, v4}, Landroid/view/ViewGroup;->setPadding(IIII)V
/* .line 236 */
} // :cond_0
return;
} // .end method
public android.app.Dialog showDialog ( android.content.Context p0, com.android.server.autofill.ui.OverlayControl p1, android.content.DialogInterface$OnClickListener p2, android.content.DialogInterface$OnClickListener p3, android.content.DialogInterface$OnDismissListener p4 ) {
/* .locals 23 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "overlayControl" # Lcom/android/server/autofill/ui/OverlayControl; */
/* .param p3, "cancelListener" # Landroid/content/DialogInterface$OnClickListener; */
/* .param p4, "okListener" # Landroid/content/DialogInterface$OnClickListener; */
/* .param p5, "onDismissListener" # Landroid/content/DialogInterface$OnDismissListener; */
/* .line 60 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v3, p3 */
/* move-object/from16 v4, p4 */
final String v5 = "AutofillSaveUi"; // const-string v5, "AutofillSaveUi"
this.mContext = v2;
/* .line 61 */
/* invoke-static/range {p1 ..p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater; */
/* const v6, 0x110c0009 */
int v7 = 0; // const/4 v7, 0x0
(( android.view.LayoutInflater ) v0 ).inflate ( v6, v7 ); // invoke-virtual {v0, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
/* .line 64 */
/* .local v6, "view":Landroid/view/View; */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources; */
/* const v8, 0x110f006a */
(( android.content.res.Resources ) v0 ).getString ( v8 ); // invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 65 */
/* .local v8, "linkMessage":Ljava/lang/String; */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources; */
/* const v9, 0x110f0068 */
/* filled-new-array {v8}, [Ljava/lang/Object; */
(( android.content.res.Resources ) v0 ).getString ( v9, v10 ); // invoke-virtual {v0, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
/* .line 66 */
/* .local v9, "message":Ljava/lang/String; */
v10 = (( java.lang.String ) v9 ).indexOf ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* .line 67 */
/* .local v10, "start":I */
v0 = (( java.lang.String ) v8 ).length ( ); // invoke-virtual {v8}, Ljava/lang/String;->length()I
/* add-int v11, v10, v0 */
/* .line 68 */
/* .local v11, "end":I */
/* new-instance v0, Landroid/text/SpannableString; */
/* invoke-direct {v0, v9}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V */
/* move-object v12, v0 */
/* .line 70 */
/* .local v12, "spannableString":Landroid/text/SpannableString; */
/* new-instance v0, Landroid/text/style/UnderlineSpan; */
/* invoke-direct {v0}, Landroid/text/style/UnderlineSpan;-><init>()V */
/* const/16 v13, 0x21 */
(( android.text.SpannableString ) v12 ).setSpan ( v0, v10, v11, v13 ); // invoke-virtual {v12, v0, v10, v11, v13}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V
/* .line 72 */
/* new-instance v0, Lcom/android/server/autofill/ui/SaveUiStubImpl$1; */
/* invoke-direct {v0, v1, v3}, Lcom/android/server/autofill/ui/SaveUiStubImpl$1;-><init>(Lcom/android/server/autofill/ui/SaveUiStubImpl;Landroid/content/DialogInterface$OnClickListener;)V */
(( android.text.SpannableString ) v12 ).setSpan ( v0, v10, v11, v13 ); // invoke-virtual {v12, v0, v10, v11, v13}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V
/* .line 87 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources; */
/* const v14, 0x11060007 */
v14 = (( android.content.res.Resources ) v0 ).getColor ( v14 ); // invoke-virtual {v0, v14}, Landroid/content/res/Resources;->getColor(I)I
/* .line 88 */
/* .local v14, "color":I */
/* new-instance v0, Landroid/text/style/ForegroundColorSpan; */
/* invoke-direct {v0, v14}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V */
(( android.text.SpannableString ) v12 ).setSpan ( v0, v10, v11, v13 ); // invoke-virtual {v12, v0, v10, v11, v13}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V
/* .line 89 */
/* const v0, 0x110a001c */
(( android.view.View ) v6 ).findViewById ( v0 ); // invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* move-object v13, v0 */
/* check-cast v13, Landroid/widget/TextView; */
/* .line 90 */
/* .local v13, "messageView":Landroid/widget/TextView; */
(( android.widget.TextView ) v13 ).setText ( v12 ); // invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
/* .line 91 */
android.text.method.LinkMovementMethod .getInstance ( );
(( android.widget.TextView ) v13 ).setMovementMethod ( v0 ); // invoke-virtual {v13, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V
/* .line 92 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources; */
/* const-string/jumbo v15, "style" */
final String v7 = "miuix.stub"; // const-string v7, "miuix.stub"
final String v1 = "AlertDialog.Theme.DayNight"; // const-string v1, "AlertDialog.Theme.DayNight"
v1 = (( android.content.res.Resources ) v0 ).getIdentifier ( v1, v15, v7 ); // invoke-virtual {v0, v1, v15, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 94 */
/* .local v1, "themeId":I */
/* new-instance v0, Lmiuix/appcompat/app/AlertDialog$Builder; */
/* invoke-direct {v0, v2, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V */
/* .line 95 */
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).setView ( v6 ); // invoke-virtual {v0, v6}, Lmiuix/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 96 */
/* const v7, 0x110f0069 */
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).setTitle ( v7 ); // invoke-virtual {v0, v7}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;
/* new-instance v7, Lcom/android/server/autofill/ui/SaveUiStubImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v7, v4}, Lcom/android/server/autofill/ui/SaveUiStubImpl$$ExternalSyntheticLambda0;-><init>(Landroid/content/DialogInterface$OnClickListener;)V */
/* .line 100 */
/* const v15, 0x110f0067 */
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).setPositiveButton ( v15, v7 ); // invoke-virtual {v0, v15, v7}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;
/* new-instance v7, Lcom/android/server/autofill/ui/SaveUiStubImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v7, v3}, Lcom/android/server/autofill/ui/SaveUiStubImpl$$ExternalSyntheticLambda1;-><init>(Landroid/content/DialogInterface$OnClickListener;)V */
/* .line 106 */
/* const/high16 v15, 0x1040000 */
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).setNegativeButton ( v15, v7 ); // invoke-virtual {v0, v15, v7}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 112 */
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).create ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;
/* .line 113 */
/* .local v7, "dialog":Lmiuix/appcompat/app/AlertDialog; */
/* new-instance v0, Lcom/android/server/autofill/ui/SaveUiStubImpl$$ExternalSyntheticLambda2; */
/* move-object/from16 v15, p5 */
/* invoke-direct {v0, v15}, Lcom/android/server/autofill/ui/SaveUiStubImpl$$ExternalSyntheticLambda2;-><init>(Landroid/content/DialogInterface$OnDismissListener;)V */
(( miuix.appcompat.app.AlertDialog ) v7 ).setOnDismissListener ( v0 ); // invoke-virtual {v7, v0}, Lmiuix/appcompat/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
/* .line 120 */
/* move/from16 v16, v1 */
} // .end local v1 # "themeId":I
/* .local v16, "themeId":I */
(( miuix.appcompat.app.AlertDialog ) v7 ).getWindow ( ); // invoke-virtual {v7}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;
/* .line 121 */
/* .local v1, "window":Landroid/view/Window; */
/* const/16 v0, 0x7f6 */
(( android.view.Window ) v1 ).setType ( v0 ); // invoke-virtual {v1, v0}, Landroid/view/Window;->setType(I)V
/* .line 122 */
/* const v0, 0x60020 */
(( android.view.Window ) v1 ).addFlags ( v0 ); // invoke-virtual {v1, v0}, Landroid/view/Window;->addFlags(I)V
/* .line 125 */
/* const/16 v0, 0x10 */
(( android.view.Window ) v1 ).addPrivateFlags ( v0 ); // invoke-virtual {v1, v0}, Landroid/view/Window;->addPrivateFlags(I)V
/* .line 126 */
/* const/16 v0, 0x20 */
(( android.view.Window ) v1 ).setSoftInputMode ( v0 ); // invoke-virtual {v1, v0}, Landroid/view/Window;->setSoftInputMode(I)V
/* .line 127 */
int v0 = 1; // const/4 v0, 0x1
(( android.view.Window ) v1 ).setCloseOnTouchOutside ( v0 ); // invoke-virtual {v1, v0}, Landroid/view/Window;->setCloseOnTouchOutside(Z)V
/* .line 129 */
/* const/16 v17, 0x0 */
/* .line 130 */
/* .local v17, "isNeverShowSaveUI":Z */
/* const/16 v18, 0x0 */
/* .line 131 */
/* .local v18, "isAutoSave":Z */
/* const/16 v19, 0x0 */
/* .line 133 */
/* .local v19, "isAutoCancel":Z */
/* move-object/from16 v20, v1 */
} // .end local v1 # "window":Landroid/view/Window;
/* .local v20, "window":Landroid/view/Window; */
try { // :try_start_0
final String v0 = "com.miui.contentcatcher"; // const-string v0, "com.miui.contentcatcher"
int v1 = 3; // const/4 v1, 0x3
(( android.content.Context ) v2 ).createPackageContext ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
/* .line 135 */
/* .local v0, "serviceContext":Landroid/content/Context; */
final String v1 = "multi_process"; // const-string v1, "multi_process"
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_2 */
/* move-object/from16 v21, v6 */
} // .end local v6 # "view":Landroid/view/View;
/* .local v21, "view":Landroid/view/View; */
int v6 = 4; // const/4 v6, 0x4
try { // :try_start_1
(( android.content.Context ) v0 ).getSharedPreferences ( v1, v6 ); // invoke-virtual {v0, v1, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
/* .line 137 */
/* .local v1, "sharedPreferences":Landroid/content/SharedPreferences; */
final String v6 = "never_show_save_ui"; // const-string v6, "never_show_save_ui"
/* :try_end_1 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 ..:try_end_1} :catch_1 */
/* move-object/from16 v22, v8 */
int v8 = 0; // const/4 v8, 0x0
} // .end local v8 # "linkMessage":Ljava/lang/String;
/* .local v22, "linkMessage":Ljava/lang/String; */
v6 = try { // :try_start_2
/* move/from16 v17, v6 */
/* .line 138 */
v6 = final String v6 = "auto_save"; // const-string v6, "auto_save"
/* move/from16 v18, v6 */
/* .line 139 */
v6 = final String v6 = "auto_cancel"; // const-string v6, "auto_cancel"
/* :try_end_2 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 ..:try_end_2} :catch_0 */
/* move/from16 v19, v6 */
/* .line 143 */
} // .end local v0 # "serviceContext":Landroid/content/Context;
} // .end local v1 # "sharedPreferences":Landroid/content/SharedPreferences;
/* move/from16 v0, v17 */
/* move/from16 v1, v18 */
/* .line 140 */
/* :catch_0 */
/* move-exception v0 */
} // .end local v22 # "linkMessage":Ljava/lang/String;
/* .restart local v8 # "linkMessage":Ljava/lang/String; */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v22, v8 */
} // .end local v8 # "linkMessage":Ljava/lang/String;
/* .restart local v22 # "linkMessage":Ljava/lang/String; */
} // .end local v21 # "view":Landroid/view/View;
} // .end local v22 # "linkMessage":Ljava/lang/String;
/* .restart local v6 # "view":Landroid/view/View; */
/* .restart local v8 # "linkMessage":Ljava/lang/String; */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v21, v6 */
/* move-object/from16 v22, v8 */
/* .line 141 */
} // .end local v6 # "view":Landroid/view/View;
} // .end local v8 # "linkMessage":Ljava/lang/String;
/* .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
/* .restart local v21 # "view":Landroid/view/View; */
/* .restart local v22 # "linkMessage":Ljava/lang/String; */
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "SaveUi e="; // const-string v6, "SaveUi e="
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v5,v1 );
/* .line 142 */
(( android.content.pm.PackageManager$NameNotFoundException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
/* move/from16 v0, v17 */
/* move/from16 v1, v18 */
/* move/from16 v6, v19 */
/* .line 145 */
} // .end local v17 # "isNeverShowSaveUI":Z
} // .end local v18 # "isAutoSave":Z
} // .end local v19 # "isAutoCancel":Z
/* .local v0, "isNeverShowSaveUI":Z */
/* .local v1, "isAutoSave":Z */
/* .local v6, "isAutoCancel":Z */
} // :goto_1
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v17, v9 */
} // .end local v9 # "message":Ljava/lang/String;
/* .local v17, "message":Ljava/lang/String; */
final String v9 = "SaveUi neverShow="; // const-string v9, "SaveUi neverShow="
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v9 = ", isAutoSave="; // const-string v9, ", isAutoSave="
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v1 ); // invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v9 = ", isAutoCancel="; // const-string v9, ", isAutoCancel="
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v5,v8 );
/* .line 148 */
/* if-nez v0, :cond_0 */
/* .line 149 */
com.android.server.autofill.ui.SaveUiStubImpl .setDialog ( v7 );
/* .line 150 */
(( miuix.appcompat.app.AlertDialog ) v7 ).show ( ); // invoke-virtual {v7}, Lmiuix/appcompat/app/AlertDialog;->show()V
/* .line 153 */
/* new-instance v5, Landroid/content/Intent; */
final String v8 = "intent.action.saveui"; // const-string v8, "intent.action.saveui"
/* invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
(( android.content.Context ) v2 ).sendBroadcast ( v5 ); // invoke-virtual {v2, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 154 */
/* invoke-virtual/range {p2 ..p2}, Lcom/android/server/autofill/ui/OverlayControl;->hideOverlays()V */
/* .line 155 */
} // :cond_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 156 */
int v5 = 0; // const/4 v5, 0x0
int v8 = 0; // const/4 v8, 0x0
/* .line 157 */
} // :cond_1
int v5 = 0; // const/4 v5, 0x0
int v8 = 0; // const/4 v8, 0x0
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 158 */
/* .line 160 */
} // :cond_2
} // :goto_2
} // .end method
