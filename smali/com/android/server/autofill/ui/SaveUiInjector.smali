.class final Lcom/android/server/autofill/ui/SaveUiInjector;
.super Ljava/lang/Object;
.source "SaveUiInjector.java"


# static fields
.field private static final AUTOFILL_ACTIVITY:Ljava/lang/String; = "com.miui.contentcatcher.autofill.activitys.AutofillSettingActivity"

.field private static final AUTOFILL_PACKAGE:Ljava/lang/String; = "com.miui.contentcatcher"

.field private static final AUTO_CANCEL:Ljava/lang/String; = "auto_cancel"

.field private static final AUTO_SAVE:Ljava/lang/String; = "auto_save"

.field private static MIUI_VERSION_CODE:I = 0x0

.field private static final NEVER_SHOW_SAVE_UI:Ljava/lang/String; = "never_show_save_ui"

.field private static final SAVEUI_ACTION:Ljava/lang/String; = "intent.action.saveui"

.field private static final SERVICE_SP_NAME:Ljava/lang/String; = "multi_process"

.field private static final TAG:Ljava/lang/String; = "AutofillSaveUi"

.field private static mDialog:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 42
    const-string v0, "ro.miui.ui.version.code"

    const/16 v1, 0xd

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/autofill/ui/SaveUiInjector;->MIUI_VERSION_CODE:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    return-void
.end method

.method public static autoCancel()V
    .locals 5

    .line 197
    sget-object v0, Lcom/android/server/autofill/ui/SaveUiInjector;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 201
    const-string v0, "AutofillSaveUi"

    const-string v1, "autoCancel  checked=true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :try_start_0
    sget-object v0, Lcom/android/server/autofill/ui/SaveUiInjector;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.miui.contentcatcher"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    .line 205
    .local v0, "serviceContext":Landroid/content/Context;
    const-string v1, "multi_process"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 207
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "never_show_save_ui"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 208
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "auto_cancel"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    .end local v0    # "serviceContext":Landroid/content/Context;
    .end local v1    # "sharedPreferences":Landroid/content/SharedPreferences;
    goto :goto_0

    .line 209
    :catch_0
    move-exception v0

    .line 210
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 213
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    :goto_0
    return-void
.end method

.method public static autoSave()V
    .locals 5

    .line 178
    sget-object v0, Lcom/android/server/autofill/ui/SaveUiInjector;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 182
    const-string v0, "AutofillSaveUi"

    const-string v1, "autoSave  checked=true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :try_start_0
    sget-object v0, Lcom/android/server/autofill/ui/SaveUiInjector;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.miui.contentcatcher"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    .line 186
    .local v0, "serviceContext":Landroid/content/Context;
    const-string v1, "multi_process"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 188
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "never_show_save_ui"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 189
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "auto_save"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    .end local v0    # "serviceContext":Landroid/content/Context;
    .end local v1    # "sharedPreferences":Landroid/content/SharedPreferences;
    goto :goto_0

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 194
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    :goto_0
    return-void
.end method

.method public static changeBackground(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;)V
    .locals 4
    .param p0, "decor"    # Landroid/view/View;
    .param p1, "params"    # Landroid/view/WindowManager$LayoutParams;

    .line 153
    if-eqz p0, :cond_3

    if-nez p1, :cond_0

    goto :goto_1

    .line 156
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 157
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    .line 156
    const-string v2, "autofill_service"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "autofillService":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 159
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    .line 160
    .local v1, "packageName":Ljava/lang/String;
    const-string v2, "com.miui.contentcatcher"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 161
    const v2, 0x11080362

    invoke-virtual {p0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 162
    sget v2, Lcom/android/server/autofill/ui/SaveUiInjector;->MIUI_VERSION_CODE:I

    const/16 v3, 0xa

    if-le v2, v3, :cond_1

    .line 163
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/lit8 v2, v2, -0x28

    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 164
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    add-int/lit8 v2, v2, -0x50

    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 165
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/lit8 v2, v2, 0x64

    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 166
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/lit16 v2, v2, 0xa0

    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    goto :goto_0

    .line 168
    :cond_1
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/lit8 v2, v2, -0x3c

    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 169
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    add-int/lit8 v2, v2, -0x3c

    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 170
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/lit8 v2, v2, 0x78

    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 171
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/lit8 v2, v2, 0x78

    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 175
    .end local v1    # "packageName":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 154
    .end local v0    # "autofillService":Ljava/lang/String;
    :cond_3
    :goto_1
    return-void
.end method

.method static synthetic lambda$showDialog$0(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface;I)V
    .locals 2
    .param p0, "okListener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p1, "v"    # Landroid/content/DialogInterface;
    .param p2, "w"    # I

    .line 92
    const-string v0, "AutofillSaveUi"

    const-string/jumbo v1, "showDialog  save"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-interface {p0, v1, v0}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 94
    invoke-static {}, Lcom/android/server/autofill/ui/SaveUiInjector;->autoSave()V

    .line 95
    sput-object v1, Lcom/android/server/autofill/ui/SaveUiInjector;->mDialog:Landroid/app/AlertDialog;

    .line 96
    return-void
.end method

.method static synthetic lambda$showDialog$1(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface;I)V
    .locals 2
    .param p0, "cancelListener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p1, "v"    # Landroid/content/DialogInterface;
    .param p2, "w"    # I

    .line 99
    const-string v0, "AutofillSaveUi"

    const-string/jumbo v1, "showDialog  cancel"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-interface {p0, v1, v0}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 101
    invoke-static {}, Lcom/android/server/autofill/ui/SaveUiInjector;->autoCancel()V

    .line 102
    sput-object v1, Lcom/android/server/autofill/ui/SaveUiInjector;->mDialog:Landroid/app/AlertDialog;

    .line 103
    return-void
.end method

.method static synthetic lambda$showDialog$2(Landroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface;)V
    .locals 2
    .param p0, "onDismissListener"    # Landroid/content/DialogInterface$OnDismissListener;
    .param p1, "v"    # Landroid/content/DialogInterface;

    .line 107
    const-string v0, "AutofillSaveUi"

    const-string/jumbo v1, "showDialog  dismiss"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    .line 109
    sput-object v0, Lcom/android/server/autofill/ui/SaveUiInjector;->mDialog:Landroid/app/AlertDialog;

    .line 110
    return-void
.end method

.method public static showDialog(Landroid/content/Context;Lcom/android/server/autofill/ui/OverlayControl;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/Dialog;
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "overlayControl"    # Lcom/android/server/autofill/ui/OverlayControl;
    .param p2, "cancelListener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p3, "okListener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p4, "onDismissListener"    # Landroid/content/DialogInterface$OnDismissListener;

    .line 52
    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    const-string v4, "AutofillSaveUi"

    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v5, 0x110c0009

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 55
    .local v5, "view":Landroid/view/View;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v7, 0x110f006a

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 56
    .local v7, "linkMessage":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v8, 0x110f0068

    filled-new-array {v7}, [Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 57
    .local v8, "message":Ljava/lang/String;
    invoke-virtual {v8, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 58
    .local v9, "start":I
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v0

    add-int v10, v9, v0

    .line 59
    .local v10, "end":I
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v11, v0

    .line 61
    .local v11, "spannableString":Landroid/text/SpannableString;
    new-instance v0, Landroid/text/style/UnderlineSpan;

    invoke-direct {v0}, Landroid/text/style/UnderlineSpan;-><init>()V

    const/16 v12, 0x21

    invoke-virtual {v11, v0, v9, v10, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 63
    new-instance v0, Lcom/android/server/autofill/ui/SaveUiInjector$1;

    invoke-direct {v0, v2}, Lcom/android/server/autofill/ui/SaveUiInjector$1;-><init>(Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v11, v0, v9, v10, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 78
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v13, 0x11060007

    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    .line 79
    .local v13, "color":I
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, v13}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v11, v0, v9, v10, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 80
    const v0, 0x110a001c

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Landroid/widget/TextView;

    .line 81
    .local v12, "messageView":Landroid/widget/TextView;
    invoke-virtual {v12, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 84
    new-instance v0, Landroid/app/AlertDialog$Builder;

    const v14, 0x66110191

    invoke-direct {v0, v1, v14}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 85
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 86
    const v14, 0x110f0069

    invoke-virtual {v0, v14}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v14, Lcom/android/server/autofill/ui/SaveUiInjector$$ExternalSyntheticLambda0;

    invoke-direct {v14, v3}, Lcom/android/server/autofill/ui/SaveUiInjector$$ExternalSyntheticLambda0;-><init>(Landroid/content/DialogInterface$OnClickListener;)V

    .line 90
    const v15, 0x110f0067

    invoke-virtual {v0, v15, v14}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v14, Lcom/android/server/autofill/ui/SaveUiInjector$$ExternalSyntheticLambda1;

    invoke-direct {v14, v2}, Lcom/android/server/autofill/ui/SaveUiInjector$$ExternalSyntheticLambda1;-><init>(Landroid/content/DialogInterface$OnClickListener;)V

    .line 97
    const/high16 v15, 0x1040000

    invoke-virtual {v0, v15, v14}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    .line 105
    .local v14, "dialog":Landroid/app/AlertDialog;
    new-instance v0, Lcom/android/server/autofill/ui/SaveUiInjector$$ExternalSyntheticLambda2;

    move-object/from16 v15, p4

    invoke-direct {v0, v15}, Lcom/android/server/autofill/ui/SaveUiInjector$$ExternalSyntheticLambda2;-><init>(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v14, v0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 112
    invoke-virtual {v14}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    .line 113
    .local v6, "window":Landroid/view/Window;
    const/16 v0, 0x7f6

    invoke-virtual {v6, v0}, Landroid/view/Window;->setType(I)V

    .line 114
    const v0, 0x60020

    invoke-virtual {v6, v0}, Landroid/view/Window;->addFlags(I)V

    .line 117
    const/16 v0, 0x10

    invoke-virtual {v6, v0}, Landroid/view/Window;->addPrivateFlags(I)V

    .line 118
    const/16 v0, 0x20

    invoke-virtual {v6, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 119
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/view/Window;->setCloseOnTouchOutside(Z)V

    .line 121
    const/16 v16, 0x0

    .line 122
    .local v16, "isNeverShowSaveUI":Z
    const/16 v17, 0x0

    .line 123
    .local v17, "isAutoSave":Z
    const/16 v18, 0x0

    .line 125
    .local v18, "isAutoCancel":Z
    move-object/from16 v19, v5

    .end local v5    # "view":Landroid/view/View;
    .local v19, "view":Landroid/view/View;
    :try_start_0
    const-string v0, "com.miui.contentcatcher"

    const/4 v5, 0x3

    invoke-virtual {v1, v0, v5}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    .line 127
    .local v0, "serviceContext":Landroid/content/Context;
    const-string v5, "multi_process"
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    move-object/from16 v20, v6

    .end local v6    # "window":Landroid/view/Window;
    .local v20, "window":Landroid/view/Window;
    const/4 v6, 0x4

    :try_start_1
    invoke-virtual {v0, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 129
    .local v5, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v6, "never_show_save_ui"
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v21, v7

    const/4 v7, 0x0

    .end local v7    # "linkMessage":Ljava/lang/String;
    .local v21, "linkMessage":Ljava/lang/String;
    :try_start_2
    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    move/from16 v16, v6

    .line 130
    const-string v6, "auto_save"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    move/from16 v17, v6

    .line 131
    const-string v6, "auto_cancel"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    move/from16 v18, v6

    .line 135
    .end local v0    # "serviceContext":Landroid/content/Context;
    .end local v5    # "sharedPreferences":Landroid/content/SharedPreferences;
    move/from16 v0, v16

    move/from16 v5, v17

    goto :goto_1

    .line 132
    :catch_0
    move-exception v0

    goto :goto_0

    .end local v21    # "linkMessage":Ljava/lang/String;
    .restart local v7    # "linkMessage":Ljava/lang/String;
    :catch_1
    move-exception v0

    move-object/from16 v21, v7

    .end local v7    # "linkMessage":Ljava/lang/String;
    .restart local v21    # "linkMessage":Ljava/lang/String;
    goto :goto_0

    .end local v20    # "window":Landroid/view/Window;
    .end local v21    # "linkMessage":Ljava/lang/String;
    .restart local v6    # "window":Landroid/view/Window;
    .restart local v7    # "linkMessage":Ljava/lang/String;
    :catch_2
    move-exception v0

    move-object/from16 v20, v6

    move-object/from16 v21, v7

    .line 133
    .end local v6    # "window":Landroid/view/Window;
    .end local v7    # "linkMessage":Ljava/lang/String;
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v20    # "window":Landroid/view/Window;
    .restart local v21    # "linkMessage":Ljava/lang/String;
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SaveUi  e="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move/from16 v0, v16

    move/from16 v5, v17

    move/from16 v6, v18

    .line 136
    .end local v16    # "isNeverShowSaveUI":Z
    .end local v17    # "isAutoSave":Z
    .end local v18    # "isAutoCancel":Z
    .local v0, "isNeverShowSaveUI":Z
    .local v5, "isAutoSave":Z
    .local v6, "isAutoCancel":Z
    :goto_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v16, v8

    .end local v8    # "message":Ljava/lang/String;
    .local v16, "message":Ljava/lang/String;
    const-string v8, "SaveUi  neverShow="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",  isAutoSave="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",  isAutoCancel="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    if-nez v0, :cond_0

    .line 140
    sput-object v14, Lcom/android/server/autofill/ui/SaveUiInjector;->mDialog:Landroid/app/AlertDialog;

    .line 141
    invoke-virtual {v14}, Landroid/app/AlertDialog;->show()V

    .line 142
    new-instance v4, Landroid/content/Intent;

    const-string v7, "intent.action.saveui"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 143
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/autofill/ui/OverlayControl;->hideOverlays()V

    goto :goto_2

    .line 144
    :cond_0
    if-eqz v5, :cond_1

    .line 145
    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-interface {v3, v7, v4}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    goto :goto_2

    .line 146
    :cond_1
    const/4 v4, 0x0

    const/4 v7, 0x0

    if-eqz v6, :cond_2

    .line 147
    invoke-interface {v2, v7, v4}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 149
    :cond_2
    :goto_2
    return-object v14
.end method
