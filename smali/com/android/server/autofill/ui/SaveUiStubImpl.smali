.class public Lcom/android/server/autofill/ui/SaveUiStubImpl;
.super Lcom/android/server/autofill/ui/SaveUiStub;
.source "SaveUiStubImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.autofill.ui.SaveUiStub$$"
.end annotation


# static fields
.field private static final AUTOFILL_ACTIVITY:Ljava/lang/String; = "com.miui.contentcatcher.autofill.activitys.AutofillSettingActivity"

.field private static final AUTOFILL_PACKAGE:Ljava/lang/String; = "com.miui.contentcatcher"

.field private static final AUTO_CANCEL:Ljava/lang/String; = "auto_cancel"

.field private static final AUTO_SAVE:Ljava/lang/String; = "auto_save"

.field private static MIUI_VERSION_CODE:I = 0x0

.field private static final NEVER_SHOW_SAVE_UI:Ljava/lang/String; = "never_show_save_ui"

.field private static final SAVEUI_ACTION:Ljava/lang/String; = "intent.action.saveui"

.field private static final SERVICE_SP_NAME:Ljava/lang/String; = "multi_process"

.field private static final TAG:Ljava/lang/String; = "AutofillSaveUi"

.field private static mDialog:Lmiuix/appcompat/app/AlertDialog;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 51
    const-string v0, "ro.miui.ui.version.code"

    const/16 v1, 0xd

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/autofill/ui/SaveUiStubImpl;->MIUI_VERSION_CODE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/android/server/autofill/ui/SaveUiStub;-><init>()V

    return-void
.end method

.method static synthetic lambda$showDialog$0(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface;I)V
    .locals 2
    .param p0, "okListener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p1, "v"    # Landroid/content/DialogInterface;
    .param p2, "w"    # I

    .line 102
    const-string v0, "AutofillSaveUi"

    const-string/jumbo v1, "showDialog  save"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-interface {p0, v1, v0}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 104
    invoke-static {v1}, Lcom/android/server/autofill/ui/SaveUiStubImpl;->setDialog(Lmiuix/appcompat/app/AlertDialog;)V

    .line 105
    return-void
.end method

.method static synthetic lambda$showDialog$1(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface;I)V
    .locals 2
    .param p0, "cancelListener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p1, "v"    # Landroid/content/DialogInterface;
    .param p2, "w"    # I

    .line 108
    const-string v0, "AutofillSaveUi"

    const-string/jumbo v1, "showDialog  cancel"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-interface {p0, v1, v0}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 110
    invoke-static {v1}, Lcom/android/server/autofill/ui/SaveUiStubImpl;->setDialog(Lmiuix/appcompat/app/AlertDialog;)V

    .line 111
    return-void
.end method

.method static synthetic lambda$showDialog$2(Landroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface;)V
    .locals 2
    .param p0, "onDismissListener"    # Landroid/content/DialogInterface$OnDismissListener;
    .param p1, "v"    # Landroid/content/DialogInterface;

    .line 115
    const-string v0, "AutofillSaveUi"

    const-string/jumbo v1, "showDialog  dismiss"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    .line 117
    invoke-static {v0}, Lcom/android/server/autofill/ui/SaveUiStubImpl;->setDialog(Lmiuix/appcompat/app/AlertDialog;)V

    .line 118
    return-void
.end method

.method public static declared-synchronized setDialog(Lmiuix/appcompat/app/AlertDialog;)V
    .locals 1
    .param p0, "dialog"    # Lmiuix/appcompat/app/AlertDialog;

    const-class v0, Lcom/android/server/autofill/ui/SaveUiStubImpl;

    monitor-enter v0

    .line 239
    :try_start_0
    sput-object p0, Lcom/android/server/autofill/ui/SaveUiStubImpl;->mDialog:Lmiuix/appcompat/app/AlertDialog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    monitor-exit v0

    return-void

    .line 238
    .end local p0    # "dialog":Lmiuix/appcompat/app/AlertDialog;
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public autoCancel()V
    .locals 5

    .line 211
    sget-object v0, Lcom/android/server/autofill/ui/SaveUiStubImpl;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 215
    const-string v0, "AutofillSaveUi"

    const-string v1, "autoCancel  checked=true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :try_start_0
    sget-object v0, Lcom/android/server/autofill/ui/SaveUiStubImpl;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.miui.contentcatcher"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    .line 219
    .local v0, "serviceContext":Landroid/content/Context;
    const-string v1, "multi_process"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 221
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "never_show_save_ui"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 222
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "auto_cancel"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    .end local v0    # "serviceContext":Landroid/content/Context;
    .end local v1    # "sharedPreferences":Landroid/content/SharedPreferences;
    goto :goto_0

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 227
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    :goto_0
    return-void
.end method

.method public autoSave()V
    .locals 5

    .line 191
    sget-object v0, Lcom/android/server/autofill/ui/SaveUiStubImpl;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 195
    const-string v0, "AutofillSaveUi"

    const-string v1, "autoSave  checked=true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :try_start_0
    sget-object v0, Lcom/android/server/autofill/ui/SaveUiStubImpl;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.miui.contentcatcher"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    .line 199
    .local v0, "serviceContext":Landroid/content/Context;
    const-string v1, "multi_process"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 201
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "never_show_save_ui"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 202
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "auto_save"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    .end local v0    # "serviceContext":Landroid/content/Context;
    .end local v1    # "sharedPreferences":Landroid/content/SharedPreferences;
    goto :goto_0

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 207
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    :goto_0
    return-void
.end method

.method public changeBackground(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;)V
    .locals 4
    .param p1, "decor"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/WindowManager$LayoutParams;

    .line 165
    if-eqz p1, :cond_3

    if-nez p2, :cond_0

    goto :goto_1

    .line 168
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 169
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    .line 168
    const-string v2, "autofill_service"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, "autofillService":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 171
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    .line 172
    .local v1, "packageName":Ljava/lang/String;
    const-string v2, "com.miui.contentcatcher"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 173
    const v2, 0x11080362

    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 174
    sget v2, Lcom/android/server/autofill/ui/SaveUiStubImpl;->MIUI_VERSION_CODE:I

    const/16 v3, 0xa

    if-le v2, v3, :cond_1

    .line 175
    iget v2, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/lit8 v2, v2, -0x28

    iput v2, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 176
    iget v2, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    add-int/lit8 v2, v2, -0x50

    iput v2, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 177
    iget v2, p2, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/lit8 v2, v2, 0x64

    iput v2, p2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 178
    iget v2, p2, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/lit16 v2, v2, 0xa0

    iput v2, p2, Landroid/view/WindowManager$LayoutParams;->height:I

    goto :goto_0

    .line 180
    :cond_1
    iget v2, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/lit8 v2, v2, -0x3c

    iput v2, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 181
    iget v2, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    add-int/lit8 v2, v2, -0x3c

    iput v2, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 182
    iget v2, p2, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/lit8 v2, v2, 0x78

    iput v2, p2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 183
    iget v2, p2, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/lit8 v2, v2, 0x78

    iput v2, p2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 187
    .end local v1    # "packageName":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 166
    .end local v0    # "autofillService":Ljava/lang/String;
    :cond_3
    :goto_1
    return-void
.end method

.method public setViewPadding(Landroid/app/Dialog;)V
    .locals 5
    .param p1, "dialog"    # Landroid/app/Dialog;

    .line 230
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 231
    .local v0, "window":Landroid/view/Window;
    iget-object v1, p0, Lcom/android/server/autofill/ui/SaveUiStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "id"

    const-string v3, "miuix.stub"

    const-string v4, "customPanel"

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 232
    .local v1, "id":I
    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 233
    .local v2, "customPanel":Landroid/view/ViewGroup;
    if-eqz v2, :cond_0

    .line 234
    const/16 v3, 0x50

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v3, v4}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 236
    :cond_0
    return-void
.end method

.method public showDialog(Landroid/content/Context;Lcom/android/server/autofill/ui/OverlayControl;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/Dialog;
    .locals 23
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "overlayControl"    # Lcom/android/server/autofill/ui/OverlayControl;
    .param p3, "cancelListener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p4, "okListener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p5, "onDismissListener"    # Landroid/content/DialogInterface$OnDismissListener;

    .line 60
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    const-string v5, "AutofillSaveUi"

    iput-object v2, v1, Lcom/android/server/autofill/ui/SaveUiStubImpl;->mContext:Landroid/content/Context;

    .line 61
    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v6, 0x110c0009

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 64
    .local v6, "view":Landroid/view/View;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v8, 0x110f006a

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 65
    .local v8, "linkMessage":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v9, 0x110f0068

    filled-new-array {v8}, [Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 66
    .local v9, "message":Ljava/lang/String;
    invoke-virtual {v9, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    .line 67
    .local v10, "start":I
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    add-int v11, v10, v0

    .line 68
    .local v11, "end":I
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v9}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v12, v0

    .line 70
    .local v12, "spannableString":Landroid/text/SpannableString;
    new-instance v0, Landroid/text/style/UnderlineSpan;

    invoke-direct {v0}, Landroid/text/style/UnderlineSpan;-><init>()V

    const/16 v13, 0x21

    invoke-virtual {v12, v0, v10, v11, v13}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 72
    new-instance v0, Lcom/android/server/autofill/ui/SaveUiStubImpl$1;

    invoke-direct {v0, v1, v3}, Lcom/android/server/autofill/ui/SaveUiStubImpl$1;-><init>(Lcom/android/server/autofill/ui/SaveUiStubImpl;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v12, v0, v10, v11, v13}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 87
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v14, 0x11060007

    invoke-virtual {v0, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    .line 88
    .local v14, "color":I
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, v14}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v12, v0, v10, v11, v13}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 89
    const v0, 0x110a001c

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Landroid/widget/TextView;

    .line 90
    .local v13, "messageView":Landroid/widget/TextView;
    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 92
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v15, "style"

    const-string v7, "miuix.stub"

    const-string v1, "AlertDialog.Theme.DayNight"

    invoke-virtual {v0, v1, v15, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 94
    .local v1, "themeId":I
    new-instance v0, Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-direct {v0, v2, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 95
    invoke-virtual {v0, v6}, Lmiuix/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 96
    const v7, 0x110f0069

    invoke-virtual {v0, v7}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v7, Lcom/android/server/autofill/ui/SaveUiStubImpl$$ExternalSyntheticLambda0;

    invoke-direct {v7, v4}, Lcom/android/server/autofill/ui/SaveUiStubImpl$$ExternalSyntheticLambda0;-><init>(Landroid/content/DialogInterface$OnClickListener;)V

    .line 100
    const v15, 0x110f0067

    invoke-virtual {v0, v15, v7}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v7, Lcom/android/server/autofill/ui/SaveUiStubImpl$$ExternalSyntheticLambda1;

    invoke-direct {v7, v3}, Lcom/android/server/autofill/ui/SaveUiStubImpl$$ExternalSyntheticLambda1;-><init>(Landroid/content/DialogInterface$OnClickListener;)V

    .line 106
    const/high16 v15, 0x1040000

    invoke-virtual {v0, v15, v7}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object v7

    .line 113
    .local v7, "dialog":Lmiuix/appcompat/app/AlertDialog;
    new-instance v0, Lcom/android/server/autofill/ui/SaveUiStubImpl$$ExternalSyntheticLambda2;

    move-object/from16 v15, p5

    invoke-direct {v0, v15}, Lcom/android/server/autofill/ui/SaveUiStubImpl$$ExternalSyntheticLambda2;-><init>(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v7, v0}, Lmiuix/appcompat/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 120
    move/from16 v16, v1

    .end local v1    # "themeId":I
    .local v16, "themeId":I
    invoke-virtual {v7}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 121
    .local v1, "window":Landroid/view/Window;
    const/16 v0, 0x7f6

    invoke-virtual {v1, v0}, Landroid/view/Window;->setType(I)V

    .line 122
    const v0, 0x60020

    invoke-virtual {v1, v0}, Landroid/view/Window;->addFlags(I)V

    .line 125
    const/16 v0, 0x10

    invoke-virtual {v1, v0}, Landroid/view/Window;->addPrivateFlags(I)V

    .line 126
    const/16 v0, 0x20

    invoke-virtual {v1, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 127
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setCloseOnTouchOutside(Z)V

    .line 129
    const/16 v17, 0x0

    .line 130
    .local v17, "isNeverShowSaveUI":Z
    const/16 v18, 0x0

    .line 131
    .local v18, "isAutoSave":Z
    const/16 v19, 0x0

    .line 133
    .local v19, "isAutoCancel":Z
    move-object/from16 v20, v1

    .end local v1    # "window":Landroid/view/Window;
    .local v20, "window":Landroid/view/Window;
    :try_start_0
    const-string v0, "com.miui.contentcatcher"

    const/4 v1, 0x3

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    .line 135
    .local v0, "serviceContext":Landroid/content/Context;
    const-string v1, "multi_process"
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    move-object/from16 v21, v6

    .end local v6    # "view":Landroid/view/View;
    .local v21, "view":Landroid/view/View;
    const/4 v6, 0x4

    :try_start_1
    invoke-virtual {v0, v1, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 137
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v6, "never_show_save_ui"
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v22, v8

    const/4 v8, 0x0

    .end local v8    # "linkMessage":Ljava/lang/String;
    .local v22, "linkMessage":Ljava/lang/String;
    :try_start_2
    invoke-interface {v1, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    move/from16 v17, v6

    .line 138
    const-string v6, "auto_save"

    invoke-interface {v1, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    move/from16 v18, v6

    .line 139
    const-string v6, "auto_cancel"

    invoke-interface {v1, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    move/from16 v19, v6

    .line 143
    .end local v0    # "serviceContext":Landroid/content/Context;
    .end local v1    # "sharedPreferences":Landroid/content/SharedPreferences;
    move/from16 v0, v17

    move/from16 v1, v18

    goto :goto_1

    .line 140
    :catch_0
    move-exception v0

    goto :goto_0

    .end local v22    # "linkMessage":Ljava/lang/String;
    .restart local v8    # "linkMessage":Ljava/lang/String;
    :catch_1
    move-exception v0

    move-object/from16 v22, v8

    .end local v8    # "linkMessage":Ljava/lang/String;
    .restart local v22    # "linkMessage":Ljava/lang/String;
    goto :goto_0

    .end local v21    # "view":Landroid/view/View;
    .end local v22    # "linkMessage":Ljava/lang/String;
    .restart local v6    # "view":Landroid/view/View;
    .restart local v8    # "linkMessage":Ljava/lang/String;
    :catch_2
    move-exception v0

    move-object/from16 v21, v6

    move-object/from16 v22, v8

    .line 141
    .end local v6    # "view":Landroid/view/View;
    .end local v8    # "linkMessage":Ljava/lang/String;
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v21    # "view":Landroid/view/View;
    .restart local v22    # "linkMessage":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SaveUi  e="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v6, v19

    .line 145
    .end local v17    # "isNeverShowSaveUI":Z
    .end local v18    # "isAutoSave":Z
    .end local v19    # "isAutoCancel":Z
    .local v0, "isNeverShowSaveUI":Z
    .local v1, "isAutoSave":Z
    .local v6, "isAutoCancel":Z
    :goto_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v17, v9

    .end local v9    # "message":Ljava/lang/String;
    .local v17, "message":Ljava/lang/String;
    const-string v9, "SaveUi  neverShow="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",  isAutoSave="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",  isAutoCancel="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    if-nez v0, :cond_0

    .line 149
    invoke-static {v7}, Lcom/android/server/autofill/ui/SaveUiStubImpl;->setDialog(Lmiuix/appcompat/app/AlertDialog;)V

    .line 150
    invoke-virtual {v7}, Lmiuix/appcompat/app/AlertDialog;->show()V

    .line 153
    new-instance v5, Landroid/content/Intent;

    const-string v8, "intent.action.saveui"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 154
    invoke-virtual/range {p2 .. p2}, Lcom/android/server/autofill/ui/OverlayControl;->hideOverlays()V

    goto :goto_2

    .line 155
    :cond_0
    if-eqz v1, :cond_1

    .line 156
    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-interface {v4, v8, v5}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    goto :goto_2

    .line 157
    :cond_1
    const/4 v5, 0x0

    const/4 v8, 0x0

    if-eqz v6, :cond_2

    .line 158
    invoke-interface {v3, v8, v5}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 160
    :cond_2
    :goto_2
    return-object v7
.end method
