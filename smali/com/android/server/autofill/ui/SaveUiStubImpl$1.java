class com.android.server.autofill.ui.SaveUiStubImpl$1 extends android.text.style.ClickableSpan {
	 /* .source "SaveUiStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/autofill/ui/SaveUiStubImpl;->showDialog(Landroid/content/Context;Lcom/android/server/autofill/ui/OverlayControl;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/Dialog; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.autofill.ui.SaveUiStubImpl this$0; //synthetic
final android.content.DialogInterface$OnClickListener val$cancelListener; //synthetic
/* # direct methods */
 com.android.server.autofill.ui.SaveUiStubImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/autofill/ui/SaveUiStubImpl; */
/* .line 72 */
this.this$0 = p1;
this.val$cancelListener = p2;
/* invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onClick ( android.view.View p0 ) {
/* .locals 4 */
/* .param p1, "widget" # Landroid/view/View; */
/* .line 75 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 76 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.contentcatcher"; // const-string v1, "com.miui.contentcatcher"
final String v2 = "com.miui.contentcatcher.autofill.activitys.AutofillSettingActivity"; // const-string v2, "com.miui.contentcatcher.autofill.activitys.AutofillSettingActivity"
(( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 77 */
/* const v1, 0x10008000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 79 */
try { // :try_start_0
	 (( android.view.View ) p1 ).getContext ( ); // invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;
	 v2 = android.os.UserHandle.CURRENT;
	 (( android.content.Context ) v1 ).startActivityAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
	 /* .line 80 */
	 v1 = this.val$cancelListener;
	 int v2 = 0; // const/4 v2, 0x0
	 int v3 = 0; // const/4 v3, 0x0
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 83 */
	 /* .line 81 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 82 */
	 /* .local v1, "e":Ljava/lang/Exception; */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "e: "; // const-string v3, "e: "
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v3 = "SaveUiInjector"; // const-string v3, "SaveUiInjector"
	 android.util.Log .d ( v3,v2 );
	 /* .line 84 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
