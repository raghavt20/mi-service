public class com.android.server.accounts.AccountManagerServiceImpl extends com.android.server.accounts.AccountManagerServiceStub {
	 /* .source "AccountManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.accounts.AccountManagerServiceStub$$" */
} // .end annotation
/* # direct methods */
public com.android.server.accounts.AccountManagerServiceImpl ( ) {
	 /* .locals 0 */
	 /* .line 15 */
	 /* invoke-direct {p0}, Lcom/android/server/accounts/AccountManagerServiceStub;-><init>()V */
	 return;
} // .end method
/* # virtual methods */
public Boolean canBindService ( android.content.Context p0, android.content.Intent p1, Integer p2 ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "service" # Landroid/content/Intent; */
	 /* .param p3, "userId" # I */
	 /* .line 28 */
	 v0 = 	 com.android.server.accounts.AccountManagerServiceInjector .canBindService ( p1,p2,p3 );
} // .end method
public Boolean isForceRemove ( Boolean p0 ) {
	 /* .locals 1 */
	 /* .param p1, "removalAllowed" # Z */
	 /* .line 19 */
	 v0 = 	 com.android.server.accounts.AccountManagerServiceInjector .isForceRemove ( p1 );
} // .end method
public Boolean isTrustedAccountSignature ( android.content.pm.PackageManager p0, java.lang.String p1, Integer p2, Integer p3 ) {
	 /* .locals 1 */
	 /* .param p1, "pm" # Landroid/content/pm/PackageManager; */
	 /* .param p2, "accountType" # Ljava/lang/String; */
	 /* .param p3, "serviceUid" # I */
	 /* .param p4, "callingUid" # I */
	 /* .line 24 */
	 v0 = 	 com.android.server.accounts.AccountManagerServiceInjector .isTrustedAccountSignature ( p1,p2,p3,p4 );
} // .end method
