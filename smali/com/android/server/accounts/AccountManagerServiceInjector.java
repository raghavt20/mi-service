public class com.android.server.accounts.AccountManagerServiceInjector {
	 /* .source "AccountManagerServiceInjector.java" */
	 /* # static fields */
	 public static final java.lang.String ACCOUNT_CHANGED_ACTION_ADDED;
	 public static final java.lang.String ACCOUNT_CHANGED_ACTION_ALTERED;
	 public static final java.lang.String ACCOUNT_CHANGED_ACTION_REMOVED;
	 public static final java.lang.String KEY_ACCOUNT_CHANGED_ACTION;
	 public static final java.lang.String LOGIN_ACCOUNTS_CHANGED_SYS_ACTION;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 public com.android.server.accounts.AccountManagerServiceInjector ( ) {
		 /* .locals 0 */
		 /* .line 22 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 static Boolean canBindService ( android.content.Context p0, android.content.Intent p1, Integer p2 ) {
		 /* .locals 1 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "service" # Landroid/content/Intent; */
		 /* .param p2, "userId" # I */
		 /* .line 80 */
		 v0 = 		 com.android.server.am.AutoStartManagerServiceStub .getInstance ( );
	 } // .end method
	 static Boolean isForceRemove ( Boolean p0 ) {
		 /* .locals 8 */
		 /* .param p0, "removalAllowed" # Z */
		 /* .line 40 */
		 final String v0 = "AccountManagerServiceInjector"; // const-string v0, "AccountManagerServiceInjector"
		 int v1 = 0; // const/4 v1, 0x0
		 try { // :try_start_0
			 android.app.AppGlobals .getPackageManager ( );
			 /* .line 41 */
			 /* .local v2, "pm":Landroid/content/pm/IPackageManager; */
			 v3 = 			 android.os.Binder .getCallingUid ( );
			 /* .line 42 */
			 /* .local v3, "packages":[Ljava/lang/String; */
			 if ( v3 != null) { // if-eqz v3, :cond_1
				 /* array-length v4, v3 */
				 /* if-lez v4, :cond_1 */
				 /* .line 43 */
				 /* aget-object v4, v3, v1 */
				 v5 = 				 android.os.UserHandle .getCallingUserId ( );
				 /* const-wide/16 v6, 0x0 */
				 /* .line 44 */
				 /* .local v4, "info":Landroid/content/pm/ApplicationInfo; */
				 if ( v4 != null) { // if-eqz v4, :cond_1
					 /* iget v5, v4, Landroid/content/pm/ApplicationInfo;->flags:I */
					 /* and-int/lit8 v5, v5, 0x1 */
					 /* if-nez v5, :cond_1 */
					 /* .line 45 */
					 /* if-nez p0, :cond_0 */
					 /* .line 46 */
					 final String v5 = "force remove account"; // const-string v5, "force remove account"
					 android.util.Slog .d ( v0,v5 );
					 /* .line 48 */
				 } // :cond_0
				 v0 = 				 android.miui.AppOpsUtils .isXOptMode ( );
				 /* :try_end_0 */
				 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* xor-int/lit8 v0, v0, 0x1 */
				 /* .line 53 */
			 } // .end local v2 # "pm":Landroid/content/pm/IPackageManager;
		 } // .end local v3 # "packages":[Ljava/lang/String;
	 } // .end local v4 # "info":Landroid/content/pm/ApplicationInfo;
} // :cond_1
/* .line 51 */
/* :catch_0 */
/* move-exception v2 */
/* .line 52 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "isForceRemove"; // const-string v3, "isForceRemove"
android.util.Log .e ( v0,v3,v2 );
/* .line 54 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
static Boolean isTrustedAccountSignature ( android.content.pm.PackageManager p0, java.lang.String p1, Integer p2, Integer p3 ) {
/* .locals 3 */
/* .param p0, "pm" # Landroid/content/pm/PackageManager; */
/* .param p1, "accountType" # Ljava/lang/String; */
/* .param p2, "serviceUid" # I */
/* .param p3, "callingUid" # I */
/* .line 28 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 30 */
/* .local v0, "identityToken":J */
try { // :try_start_0
v2 = miui.content.pm.ExtraPackageManager .isTrustedAccountSignature ( p0,p1,p2,p3 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 33 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 30 */
/* .line 33 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 34 */
/* throw v2 */
} // .end method
static void sendAccountsChangedSysBroadcast ( android.content.Context p0, Integer p1, java.lang.String p2, android.accounts.Account[] p3 ) {
/* .locals 3 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .param p1, "userId" # I */
/* .param p2, "action" # Ljava/lang/String; */
/* .param p3, "accounts" # [Landroid/accounts/Account; */
/* .line 71 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "android.accounts.LOGIN_ACCOUNTS_CHANGED_SYS"; // const-string v1, "android.accounts.LOGIN_ACCOUNTS_CHANGED_SYS"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 72 */
/* .local v0, "i":Landroid/content/Intent; */
final String v1 = "account_changed_action"; // const-string v1, "account_changed_action"
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 73 */
final String v1 = "accounts"; // const-string v1, "accounts"
(( android.content.Intent ) v0 ).putExtra ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;
/* .line 74 */
/* const/high16 v1, 0x4000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 75 */
/* new-instance v1, Landroid/os/UserHandle; */
/* invoke-direct {v1, p1}, Landroid/os/UserHandle;-><init>(I)V */
final String v2 = "android.permission.GET_ACCOUNTS"; // const-string v2, "android.permission.GET_ACCOUNTS"
(( android.content.Context ) p0 ).sendBroadcastAsUser ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V
/* .line 77 */
return;
} // .end method
