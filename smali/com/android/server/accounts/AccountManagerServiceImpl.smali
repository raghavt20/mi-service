.class public Lcom/android/server/accounts/AccountManagerServiceImpl;
.super Lcom/android/server/accounts/AccountManagerServiceStub;
.source "AccountManagerServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.accounts.AccountManagerServiceStub$$"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/android/server/accounts/AccountManagerServiceStub;-><init>()V

    return-void
.end method


# virtual methods
.method public canBindService(Landroid/content/Context;Landroid/content/Intent;I)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Landroid/content/Intent;
    .param p3, "userId"    # I

    .line 28
    invoke-static {p1, p2, p3}, Lcom/android/server/accounts/AccountManagerServiceInjector;->canBindService(Landroid/content/Context;Landroid/content/Intent;I)Z

    move-result v0

    return v0
.end method

.method public isForceRemove(Z)Z
    .locals 1
    .param p1, "removalAllowed"    # Z

    .line 19
    invoke-static {p1}, Lcom/android/server/accounts/AccountManagerServiceInjector;->isForceRemove(Z)Z

    move-result v0

    return v0
.end method

.method public isTrustedAccountSignature(Landroid/content/pm/PackageManager;Ljava/lang/String;II)Z
    .locals 1
    .param p1, "pm"    # Landroid/content/pm/PackageManager;
    .param p2, "accountType"    # Ljava/lang/String;
    .param p3, "serviceUid"    # I
    .param p4, "callingUid"    # I

    .line 24
    invoke-static {p1, p2, p3, p4}, Lcom/android/server/accounts/AccountManagerServiceInjector;->isTrustedAccountSignature(Landroid/content/pm/PackageManager;Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method
