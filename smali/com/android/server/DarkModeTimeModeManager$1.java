class com.android.server.DarkModeTimeModeManager$1 extends android.content.BroadcastReceiver {
	 /* .source "DarkModeTimeModeManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/DarkModeTimeModeManager;-><init>(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.DarkModeTimeModeManager this$0; //synthetic
/* # direct methods */
 com.android.server.DarkModeTimeModeManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/DarkModeTimeModeManager; */
/* .line 73 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 76 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 77 */
/* .local v0, "action":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " onReceive: action = "; // const-string v2, " onReceive: action = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "DarkModeTimeModeManager"; // const-string v2, "DarkModeTimeModeManager"
android.util.Slog .i ( v2,v1 );
/* .line 78 */
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v1 = "android.intent.action.USER_PRESENT"; // const-string v1, "android.intent.action.USER_PRESENT"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 0; // const/4 v1, 0x0
/* :sswitch_1 */
final String v1 = "miui.action.intent.DARK_MODE_SUGGEST_MESSAGE"; // const-string v1, "miui.action.intent.DARK_MODE_SUGGEST_MESSAGE"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 int v1 = 2; // const/4 v1, 0x2
	 /* :sswitch_2 */
	 final String v1 = "miui.action.intent.DARK_MODE_SUGGEST_ENABLE"; // const-string v1, "miui.action.intent.DARK_MODE_SUGGEST_ENABLE"
	 v1 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 int v1 = 1; // const/4 v1, 0x1
	 } // :goto_0
	 int v1 = -1; // const/4 v1, -0x1
} // :goto_1
/* packed-switch v1, :pswitch_data_0 */
/* .line 88 */
/* :pswitch_0 */
v1 = this.this$0;
(( com.android.server.DarkModeTimeModeManager ) v1 ).enterSettingsFromNotification ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/DarkModeTimeModeManager;->enterSettingsFromNotification(Landroid/content/Context;)V
/* .line 89 */
/* .line 85 */
/* :pswitch_1 */
v1 = this.this$0;
(( com.android.server.DarkModeTimeModeManager ) v1 ).showDarkModeSuggestToast ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/DarkModeTimeModeManager;->showDarkModeSuggestToast(Landroid/content/Context;)V
/* .line 86 */
/* .line 80 */
/* :pswitch_2 */
v1 = this.this$0;
v1 = (( com.android.server.DarkModeTimeModeManager ) v1 ).canShowDarkModeSuggestNotifocation ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/DarkModeTimeModeManager;->canShowDarkModeSuggestNotifocation(Landroid/content/Context;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 81 */
	 v1 = this.this$0;
	 (( com.android.server.DarkModeTimeModeManager ) v1 ).showDarkModeSuggestNotification ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/DarkModeTimeModeManager;->showDarkModeSuggestNotification(Landroid/content/Context;)V
	 /* .line 93 */
} // :cond_1
} // :goto_2
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x7957c397 -> :sswitch_2 */
/* -0x18c53a3f -> :sswitch_1 */
/* 0x311a1d6c -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
