public abstract class com.android.server.policy.MiuiShortcutObserver extends android.database.ContentObserver {
	 /* .source "MiuiShortcutObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String CURRENT_DEVICE_REGION;
private static final java.lang.String DEVICE_REGION_RUSSIA;
public static final Integer DOUBLE_VOLUME_DOWN_KEY_TYPE_LAUNCH_CAMERA_AND_TAKE_PHOTO;
public static final java.lang.Boolean SUPPORT_VARIABLE_APERTURE;
private static final java.lang.String TAG;
/* # instance fields */
private final android.content.ContentResolver mContentResolver;
private final android.content.Context mContext;
protected Integer mCurrentUserId;
private final android.os.Handler mHandler;
private final java.util.HashSet mMiuiShortcutListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager mMiuiShortcutOperatorCustomManager;
/* # direct methods */
static com.android.server.policy.MiuiShortcutObserver ( ) {
/* .locals 2 */
/* .line 32 */
/* nop */
/* .line 33 */
final String v0 = "ro.miui.build.region"; // const-string v0, "ro.miui.build.region"
final String v1 = "CN"; // const-string v1, "CN"
android.os.SystemProperties .get ( v0,v1 );
/* .line 37 */
final String v0 = "persist.vendor.camera.IsVariableApertureSupported"; // const-string v0, "persist.vendor.camera.IsVariableApertureSupported"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
java.lang.Boolean .valueOf ( v0 );
return;
} // .end method
public com.android.server.policy.MiuiShortcutObserver ( ) {
/* .locals 1 */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "currentUserId" # I */
/* .line 49 */
/* invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 40 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mMiuiShortcutListeners = v0;
/* .line 50 */
this.mContext = p2;
/* .line 51 */
this.mHandler = p1;
/* .line 52 */
(( android.content.Context ) p2 ).getContentResolver ( ); // invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mContentResolver = v0;
/* .line 53 */
/* iput p3, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mCurrentUserId:I */
/* .line 54 */
com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager .getInstance ( p2 );
this.mMiuiShortcutOperatorCustomManager = v0;
/* .line 56 */
return;
} // .end method
private Boolean enableLongPressPowerLaunchAssistant ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "function" # Ljava/lang/String; */
/* .line 191 */
/* sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 192 */
final String v0 = "launch_google_search"; // const-string v0, "launch_google_search"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 194 */
/* .local v0, "enableLongPressPowerLaunchAssistant":Z */
v1 = /* invoke-direct {p0}, Lcom/android/server/policy/MiuiShortcutObserver;->supportRSARegion()Z */
/* if-nez v1, :cond_1 */
/* .line 195 */
int v0 = 0; // const/4 v0, 0x0
/* .line 198 */
} // .end local v0 # "enableLongPressPowerLaunchAssistant":Z
} // :cond_0
/* nop */
/* .line 199 */
final String v0 = "launch_voice_assistant"; // const-string v0, "launch_voice_assistant"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 201 */
/* .restart local v0 # "enableLongPressPowerLaunchAssistant":Z */
} // :cond_1
} // :goto_0
} // .end method
private Boolean supportRSARegion ( ) {
/* .locals 2 */
/* .line 214 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = com.android.server.policy.MiuiShortcutObserver.CURRENT_DEVICE_REGION;
/* .line 215 */
final String v1 = "ru"; // const-string v1, "ru"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 214 */
} // :goto_0
} // .end method
/* # virtual methods */
public Boolean currentFunctionValueIsInt ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 97 */
/* const-string/jumbo v0, "volumekey_launch_camera" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 0 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 220 */
return;
} // .end method
protected java.util.Map getActionAndFunctionMap ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 131 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String getFunction ( ) {
/* .locals 1 */
/* .line 118 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String getFunction ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 122 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected Boolean hasCustomizedFunction ( java.lang.String p0, java.lang.String p1, Boolean p2 ) {
/* .locals 5 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .param p3, "isNeedReset" # Z */
/* .line 157 */
v0 = this.mMiuiShortcutOperatorCustomManager;
v0 = (( com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager ) v0 ).hasOperatorCustomFunctionForMiuiRule ( p2, p1 ); // invoke-virtual {v0, p2, p1}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->hasOperatorCustomFunctionForMiuiRule(Ljava/lang/String;Ljava/lang/String;)Z
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 159 */
/* .line 162 */
} // :cond_0
final String v0 = "long_press_power_key"; // const-string v0, "long_press_power_key"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 164 */
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 165 */
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/policy/MiuiShortcutObserver;->enableLongPressPowerLaunchAssistant(Ljava/lang/String;)Z */
/* .line 167 */
/* .local v0, "enableLongPressPowerLaunchAssistant":Z */
v2 = this.mContentResolver;
/* .line 169 */
/* iget v3, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mCurrentUserId:I */
/* .line 167 */
final String v4 = "long_press_power_launch_xiaoai"; // const-string v4, "long_press_power_launch_xiaoai"
android.provider.Settings$System .putIntForUser ( v2,v4,v0,v3 );
/* .line 170 */
} // .end local v0 # "enableLongPressPowerLaunchAssistant":Z
/* .line 173 */
} // :cond_1
final String v0 = "launch_google_search"; // const-string v0, "launch_google_search"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = /* invoke-direct {p0}, Lcom/android/server/policy/MiuiShortcutObserver;->supportRSARegion()Z */
/* if-nez v0, :cond_2 */
/* .line 174 */
/* .line 179 */
} // :cond_2
} // :goto_0
v0 = com.android.server.policy.MiuiShortcutObserver.SUPPORT_VARIABLE_APERTURE;
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* const-string/jumbo v0, "volumekey_launch_camera" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 180 */
v0 = android.text.TextUtils .isEmpty ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 181 */
v0 = this.mContentResolver;
int v2 = 2; // const/4 v2, 0x2
/* iget v3, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mCurrentUserId:I */
android.provider.Settings$System .putIntForUser ( v0,p1,v2,v3 );
/* .line 184 */
/* .line 186 */
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected Boolean isFeasibleFunction ( java.lang.String p0, android.content.Context p1 ) {
/* .locals 2 */
/* .param p1, "function" # Ljava/lang/String; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 142 */
final String v0 = "mi_pay"; // const-string v0, "mi_pay"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 143 */
v0 = android.provider.MiuiSettings$Key .isTSMClientInstalled ( p2 );
/* .line 144 */
} // :cond_0
final String v0 = "launch_voice_assistant"; // const-string v0, "launch_voice_assistant"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 145 */
v0 = android.provider.MiuiSettings$System .isXiaoAiExist ( p2 );
/* .line 146 */
} // :cond_1
final String v0 = "partial_screen_shot"; // const-string v0, "partial_screen_shot"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 147 */
final String v0 = "persist.sys.partial.screenshot.support"; // const-string v0, "persist.sys.partial.screenshot.support"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* .line 150 */
} // :cond_2
} // .end method
protected void notifyCombinationRuleChanged ( com.android.server.policy.MiuiCombinationRule p0 ) {
/* .locals 3 */
/* .param p1, "rule" # Lcom/android/server/policy/MiuiCombinationRule; */
/* .line 75 */
v0 = this.mMiuiShortcutListeners;
/* monitor-enter v0 */
/* .line 76 */
try { // :try_start_0
v1 = this.mMiuiShortcutListeners;
(( java.util.HashSet ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener; */
/* .line 77 */
/* .local v2, "listener":Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener; */
/* .line 78 */
} // .end local v2 # "listener":Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;
/* .line 79 */
} // :cond_0
/* monitor-exit v0 */
/* .line 80 */
return;
/* .line 79 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
protected void notifyGestureRuleChanged ( com.android.server.policy.MiuiGestureRule p0 ) {
/* .locals 3 */
/* .param p1, "rule" # Lcom/android/server/policy/MiuiGestureRule; */
/* .line 83 */
v0 = this.mMiuiShortcutListeners;
/* monitor-enter v0 */
/* .line 84 */
try { // :try_start_0
v1 = this.mMiuiShortcutListeners;
(( java.util.HashSet ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener; */
/* .line 85 */
/* .local v2, "listener":Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener; */
/* .line 86 */
} // .end local v2 # "listener":Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;
/* .line 87 */
} // :cond_0
/* monitor-exit v0 */
/* .line 88 */
return;
/* .line 87 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
protected void notifySingleRuleChanged ( com.android.server.policy.MiuiSingleKeyRule p0, android.net.Uri p1 ) {
/* .locals 3 */
/* .param p1, "rule" # Lcom/android/server/policy/MiuiSingleKeyRule; */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 67 */
v0 = this.mMiuiShortcutListeners;
/* monitor-enter v0 */
/* .line 68 */
try { // :try_start_0
v1 = this.mMiuiShortcutListeners;
(( java.util.HashSet ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener; */
/* .line 69 */
/* .local v2, "listener":Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener; */
/* .line 70 */
} // .end local v2 # "listener":Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;
/* .line 71 */
} // :cond_0
/* monitor-exit v0 */
/* .line 72 */
return;
/* .line 71 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void onDestroy ( ) {
/* .locals 1 */
/* .line 135 */
v0 = this.mContentResolver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 136 */
(( android.content.ContentResolver ) v0 ).unregisterContentObserver ( p0 ); // invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
/* .line 138 */
} // :cond_0
return;
} // .end method
public void onUserSwitch ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "currentUserId" # I */
/* .param p2, "isNewUser" # Z */
/* .line 205 */
int v0 = -2; // const/4 v0, -0x2
/* iput v0, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mCurrentUserId:I */
/* .line 206 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 207 */
int v0 = 1; // const/4 v0, 0x1
(( com.android.server.policy.MiuiShortcutObserver ) p0 ).setDefaultFunction ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V
/* .line 209 */
} // :cond_0
(( com.android.server.policy.MiuiShortcutObserver ) p0 ).updateRuleInfo ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutObserver;->updateRuleInfo()V
/* .line 211 */
} // :goto_0
return;
} // .end method
public void registerShortcutListener ( com.android.server.policy.MiuiShortcutObserver$MiuiShortcutListener p0 ) {
/* .locals 2 */
/* .param p1, "miuiShortcutListener" # Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener; */
/* .line 59 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 60 */
v0 = this.mMiuiShortcutListeners;
/* monitor-enter v0 */
/* .line 61 */
try { // :try_start_0
v1 = this.mMiuiShortcutListeners;
(( java.util.HashSet ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 62 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 64 */
} // :cond_0
} // :goto_0
return;
} // .end method
public void setDefaultFunction ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isNeedReset" # Z */
/* .line 113 */
(( com.android.server.policy.MiuiShortcutObserver ) p0 ).updateRuleInfo ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutObserver;->updateRuleInfo()V
/* .line 114 */
return;
} // .end method
void setRuleForObserver ( com.android.server.policy.MiuiCombinationRule p0 ) {
/* .locals 0 */
/* .param p1, "miuiCombinationRule" # Lcom/android/server/policy/MiuiCombinationRule; */
/* .line 224 */
return;
} // .end method
void setRuleForObserver ( com.android.server.policy.MiuiGestureRule p0 ) {
/* .locals 0 */
/* .param p1, "miuiGestureRule" # Lcom/android/server/policy/MiuiGestureRule; */
/* .line 228 */
return;
} // .end method
void setRuleForObserver ( com.android.server.policy.MiuiSingleKeyRule p0 ) {
/* .locals 0 */
/* .param p1, "miuiSingleKeyRule" # Lcom/android/server/policy/MiuiSingleKeyRule; */
/* .line 232 */
return;
} // .end method
protected void unRegisterShortcutListener ( com.android.server.policy.MiuiShortcutObserver$MiuiShortcutListener p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "listener" # Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener; */
/* .param p2, "action" # Ljava/lang/String; */
/* .line 101 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 102 */
v0 = this.mMiuiShortcutListeners;
/* monitor-enter v0 */
/* .line 103 */
try { // :try_start_0
v1 = this.mMiuiShortcutListeners;
(( java.util.HashSet ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
/* .line 104 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 106 */
} // :cond_0
} // :goto_0
return;
} // .end method
abstract void updateRuleInfo ( ) {
} // .end method
