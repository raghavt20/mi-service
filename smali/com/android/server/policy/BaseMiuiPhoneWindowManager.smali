.class public abstract Lcom/android/server/policy/BaseMiuiPhoneWindowManager;
.super Lcom/android/server/policy/PhoneWindowManager;
.source "BaseMiuiPhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;,
        Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;,
        Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;,
        Lcom/android/server/policy/BaseMiuiPhoneWindowManager$StatusBarPointEventTracker;
    }
.end annotation


# static fields
.field private static final ACCESSIBLE_MODE_LARGE_DENSITY:I = 0x1cd

.field private static final ACCESSIBLE_MODE_SMALL_DENSITY:I = 0x160

.field private static final ACTION_DOUBLE_CLICK:I = 0x1

.field private static final ACTION_DUMPSYS:Ljava/lang/String; = "dumpsys_by_power"

.field private static final ACTION_FACTORY_RESET:Ljava/lang/String; = "key_long_press_volume_up"

.field private static final ACTION_LONGPRESS:I = 0x2

.field private static final ACTION_PARTIAL_SCREENSHOT:Ljava/lang/String; = "android.intent.action.CAPTURE_PARTIAL_SCREENSHOT"

.field private static final ACTION_SINGLE_CLICK:I = 0x0

.field private static final BTN_MOUSE:I = 0x110

.field private static final COMBINE_VOLUME_KEY_DELAY_TIME:I = 0x96

.field private static final DEBUG:Z = false

.field private static final DOUBLE_CLICK_AI_KEY_TIME:I = 0x12c

.field private static final ENABLE_HOME_KEY_DOUBLE_TAP_INTERVAL:I = 0x12c

.field private static final ENABLE_VOLUME_KEY_PRESS_COUNTS:I = 0x2

.field private static final ENABLE_VOLUME_KEY_PRESS_INTERVAL:I = 0x12c

.field public static final FLAG_INJECTED_FROM_SHORTCUT:I = 0x1000000

.field private static final HOME_PACKAGE_NAME:Ljava/lang/String; = "com.miui.home"

.field protected static final INTERCEPT_EXPECTED_RESULT_GO_TO_SLEEP:I = -0x1

.field protected static final INTERCEPT_EXPECTED_RESULT_NONE:I = 0x0

.field protected static final INTERCEPT_EXPECTED_RESULT_WAKE_UP:I = 0x1

.field private static final IS_CETUS:Z

.field public static final IS_CUSTOM_SHORTCUTS_EFFECTIVE:Ljava/lang/String; = "is_custom_shortcut_effective"

.field private static final IS_FLIP_DEVICE:Z

.field private static final IS_FOLD_DEVICE:Z

.field private static final IS_MI_INPUT_EVENT_TIME_LINE_ENABLE:Ljava/lang/String; = "is_mi_input_event_time_line_enable"

.field private static final KEYCODE_AI:I = 0x2b1

.field private static final KEY_GAME_BOOSTER:Ljava/lang/String; = "gb_boosting"

.field private static final KEY_LONG_PRESS_TIMEOUT_DELAY_FOR_CAMERA_KEY:I = 0x12c

.field private static final KEY_LONG_PRESS_TIMEOUT_DELAY_FOR_SHOW_MENU:I = 0x32

.field private static final KID_MODE:Ljava/lang/String; = "kid_mode_status"

.field private static final KID_MODE_STATUE_EXIT:I = 0x0

.field private static final KID_MODE_STATUS_IN:I = 0x1

.field private static final KID_SPACE_ID:Ljava/lang/String; = "kid_user_id"

.field private static final LAST_POWER_UP_EVENT:Ljava/lang/String; = "power_up_event"

.field private static final LAST_POWER_UP_POLICY:Ljava/lang/String; = "power_up_policy"

.field private static final LAST_POWER_UP_SCREEN_STATE:Ljava/lang/String; = "power_up_screen_state"

.field private static final LONG_LONG_PRESS_POWER_KEY:Ljava/lang/String; = "long_press_power_key_four_second"

.field private static final LONG_PRESS_AI_KEY_TIME:I = 0x1f4

.field private static final LONG_PRESS_VOLUME_DOWN_ACTION_NONE:I = 0x0

.field private static final LONG_PRESS_VOLUME_DOWN_ACTION_PAY:I = 0x2

.field private static final LONG_PRESS_VOLUME_DOWN_ACTION_STREET_SNAP:I = 0x1

.field private static final META_KEY_PRESS_INTERVAL:I = 0x12c

.field private static final MSG_COMBINE_VOLUME_KEY_DELAY_TIME:I = 0xbb8

.field private static final PERMISSION_INTERNAL_GENERAL_API:Ljava/lang/String; = "miui.permission.USE_INTERNAL_GENERAL_API"

.field protected static final REASON_FP_DPAD_CENTER_WAKEUP:Ljava/lang/String; = "miui.policy:FINGERPRINT_DPAD_CENTER"

.field private static final SHORTCUT_BACK_POWER:I

.field private static final SHORTCUT_HOME_POWER:I

.field private static final SHORTCUT_MENU_POWER:I

.field private static final SHORTCUT_SCREENSHOT_ANDROID:I

.field private static final SHORTCUT_SCREENSHOT_MIUI:I

.field private static final SHORTCUT_SCREENSHOT_SINGLE_KEY:I

.field private static final SHORTCUT_UNLOCK:I

.field protected static final SUPPORT_EDGE_TOUCH_VOLUME:Z

.field protected static final SUPPORT_POWERFP:Z

.field private static final SYNERGY_MODE:Ljava/lang/String; = "synergy_mode"

.field private static final SYSTEM_SETTINGS_VR_MODE:Ljava/lang/String; = "vr_mode"

.field private static final TALK_BACK_SERVICE_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private static final mPhoneProduct:Ljava/lang/String;

.field private static phoneWindowManagerFeature:Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;

.field static final sScreenRecorderKeyEventList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final sVoiceAssistKeyEventList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mAccessibilityShortcutOnLockScreen:Z

.field private mAccessibilityShortcutSetting:Lmiui/provider/SettingsStringUtil$SettingStringHelper;

.field private mAiKeyWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAutoDisableScreenButtonsManager:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

.field private mBinder:Landroid/os/Binder;

.field mBootCompleteReceiver:Landroid/content/BroadcastReceiver;

.field private mBootProgress:Landroid/widget/ProgressBar;

.field private mBootText:[Ljava/lang/String;

.field private mBootTextView:Landroid/widget/TextView;

.field mCameraKeyWakeScreen:Z

.field private mCurrentUserId:I

.field private mDoubleClickAiKeyCount:I

.field private mDoubleClickAiKeyIsConsumed:Z

.field private mDoubleClickAiKeyRunnable:Ljava/lang/Runnable;

.field mDpadCenterDown:Z

.field private mEdgeSuppressionManager:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

.field protected mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

.field private mFolded:Z

.field private mForbidFullScreen:Z

.field protected mFpNavEventNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mFrontFingerprintSensor:Z

.field protected mHandler:Landroid/os/Handler;

.field private mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

.field private mHasWatchedRotation:Z

.field mHaveBankCard:Z

.field mHaveTranksCard:Z

.field private mHelpKeyWakeLock:Landroid/os/PowerManager$WakeLock;

.field mHomeConsumed:Z

.field mHomeDoubleClickPending:Z

.field private final mHomeDoubleClickTimeoutRunnable:Ljava/lang/Runnable;

.field mHomeDoubleTapPending:Z

.field private final mHomeDoubleTapTimeoutRunnable:Ljava/lang/Runnable;

.field mHomeDownAfterDpCenter:Z

.field private final mIDisplayFoldListener:Landroid/view/IDisplayFoldListener;

.field private mImperceptiblePowerKey:Ljava/lang/String;

.field private mInputFeature:Lmiui/hardware/input/InputFeature;

.field private mInputMethodWindowVisibleHeight:I

.field private mInternalBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mIsFoldChanged:Z

.field private mIsKidMode:Z

.field protected mIsStatusBarVisibleInFullscreen:Z

.field private mIsSupportGloablTounchDirection:Z

.field private mIsSynergyMode:Z

.field private mIsVRMode:Z

.field private mKeyBoardDeviceId:I

.field protected mKeyLongPressTimeout:I

.field private mKeyguardOnWhenHomeDown:Z

.field private mLastClickAiKeyTime:J

.field private mLongPressAiKeyIsConsumed:Z

.field private mLongPressDownAiKeyRunnable:Ljava/lang/Runnable;

.field private mLongPressUpAiKeyRunnable:Ljava/lang/Runnable;

.field private mLongPressVolumeDownBehavior:I

.field private mMetaKeyAction:I

.field private mMetaKeyConsume:Z

.field private final mMetaKeyRunnable:Ljava/lang/Runnable;

.field mMikeymodeEnabled:Z

.field private mMiuiBackTapGestureService:Lcom/miui/server/input/MiuiBackTapGestureService;

.field private mMiuiBootMsgDialog:Landroid/app/Dialog;

.field private mMiuiFingerPrintTapListener:Lcom/miui/server/input/MiuiFingerPrintTapListener;

.field private mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

.field private mMiuiKeyInterceptExtend:Lcom/android/server/policy/MiuiKeyInterceptExtend;

.field protected mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

.field protected mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

.field private mMiuiKnockGestureService:Lcom/miui/server/input/knock/MiuiKnockGestureService;

.field private mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

.field private mMiuiPocketModeManager:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

.field protected mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

.field protected mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

.field protected mMiuiThreeGestureListener:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

.field private mMiuiTimeFloatingWindow:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

.field mNavBarHeight:I

.field mNavBarHeightLand:I

.field mNavBarWidth:I

.field mPartialScreenshotReceiver:Landroid/content/BroadcastReceiver;

.field mPowerLongPressOriginal:Ljava/lang/Runnable;

.field private mProximitySensor:Lcom/android/server/policy/MiuiScreenOnProximityLock;

.field private mRotationWatcher:Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;

.field protected mScreenOffReason:I

.field mScreenRecordeEnablekKeyEventReceiver:Landroid/content/BroadcastReceiver;

.field private mScreenRecorderEnabled:Z

.field mScreenshotReceiver:Landroid/content/BroadcastReceiver;

.field private mSettingsObserver:Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;

.field private mShortcutOneTrackHelper:Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

.field mShortcutServiceIsTalkBack:Z

.field private mShoulderKeyManagerInternal:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

.field private mSingleClickAiKeyRunnable:Ljava/lang/Runnable;

.field private mSmartCoverManager:Lmiui/util/SmartCoverManager;

.field private mStabilityLocalServiceInternal:Lcom/miui/server/stability/StabilityLocalServiceInternal;

.field mStatusBarExitFullscreenReceiver:Landroid/content/BroadcastReceiver;

.field protected mSupportTapFingerprintSensorToHome:Z

.field private mSystemKeyPackages:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSystemShortcutsMenuShown:Z

.field mTalkBackIsOpened:Z

.field mTestModeEnabled:Z

.field private mTofManagerInternal:Lcom/android/server/tof/TofManagerInternal;

.field private mTrackDumpLogKeyCodeLastKeyCode:I

.field private mTrackDumpLogKeyCodePengding:Z

.field private mTrackDumpLogKeyCodeStartTime:J

.field private mTrackDumpLogKeyCodeTimeOut:I

.field private mTrackDumpLogKeyCodeVolumeDownTimes:I

.field mTrackballWakeScreen:Z

.field private volatile mUpdateWakeUpDetailTime:J

.field private mVoiceAssistEnabled:Z

.field private mVolumeButtonPrePressedTime:J

.field private mVolumeButtonPressedCount:J

.field private mVolumeDownKeyConsumed:Z

.field private mVolumeDownKeyPressed:Z

.field private mVolumeDownKeyTime:J

.field private mVolumeKeyUpWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mVolumeKeyWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mVolumeUpKeyConsumed:Z

.field private mVolumeUpKeyPressed:Z

.field private mVolumeUpKeyTime:J

.field private mWakeUpContentCatcherManager:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

.field private volatile mWakeUpDetail:Ljava/lang/String;

.field private final mWakeUpKeySensorListener:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;

.field private volatile mWakeUpReason:Ljava/lang/String;

.field private mWifiOnly:Z

.field private mWin:Lcom/android/server/policy/WindowManagerPolicy$WindowState;


# direct methods
.method public static synthetic $r8$lambda$91AwqgtuG8ejgavYkJ8k3OPNGG0(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->lambda$finishLayoutLw$4(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$BaDR-tBbngxKLWmgGeE4hvFzcsE(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->lambda$trackDumpLogKeyCode$5()V

    return-void
.end method

.method public static synthetic $r8$lambda$WVFdnZ_k2bzoGN3LX5sPVDN_K4I(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->lambda$sendAsyncBroadcast$3(Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic $r8$lambda$sWY5LyklPrVG68MiImrvvG9iAV4(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->lambda$setCurrentUserLw$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$t0GcjyAaHKRtE5bMyu-Xbz_4chg(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->lambda$takeScreenshot$0(Ljava/lang/String;Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic $r8$lambda$wZoOyBvQVQ_WcuRzH1eGqDhXKB0(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->lambda$sendAsyncBroadcast$2(Landroid/content/Intent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmAccessibilityShortcutSetting(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lmiui/provider/SettingsStringUtil$SettingStringHelper;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAccessibilityShortcutSetting:Lmiui/provider/SettingsStringUtil$SettingStringHelper;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBootProgress(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Landroid/widget/ProgressBar;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mBootProgress:Landroid/widget/ProgressBar;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBootText(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)[Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mBootText:[Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBootTextView(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mBootTextView:Landroid/widget/TextView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I
    .locals 0

    iget p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCurrentUserId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmEdgeSuppressionManager(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mEdgeSuppressionManager:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFolded(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFolded:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHapticFeedbackUtil(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lmiui/util/HapticFeedbackUtil;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsFoldChanged(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsFoldChanged:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsKidMode(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsKidMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsSupportGloablTounchDirection(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsSupportGloablTounchDirection:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsSynergyMode(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsSynergyMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmKeyBoardDeviceId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I
    .locals 0

    iget p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyBoardDeviceId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMetaKeyAction(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I
    .locals 0

    iget p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyAction:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMetaKeyConsume(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiBackTapGestureService(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/miui/server/input/MiuiBackTapGestureService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiBackTapGestureService:Lcom/miui/server/input/MiuiBackTapGestureService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiBootMsgDialog(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Landroid/app/Dialog;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiBootMsgDialog:Landroid/app/Dialog;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiPocketModeManager(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/input/pocketmode/MiuiPocketModeManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiPocketModeManager:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmProximitySensor(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/policy/MiuiScreenOnProximityLock;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mProximitySensor:Lcom/android/server/policy/MiuiScreenOnProximityLock;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmShortcutOneTrackHelper(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mShortcutOneTrackHelper:Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmStabilityLocalServiceInternal(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/miui/server/stability/StabilityLocalServiceInternal;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mStabilityLocalServiceInternal:Lcom/miui/server/stability/StabilityLocalServiceInternal;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWakeUpKeySensorListener(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWakeUpKeySensorListener:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWifiOnly(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWifiOnly:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmAccessibilityShortcutSetting(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Lmiui/provider/SettingsStringUtil$SettingStringHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAccessibilityShortcutSetting:Lmiui/provider/SettingsStringUtil$SettingStringHelper;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmBootProgress(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/widget/ProgressBar;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mBootProgress:Landroid/widget/ProgressBar;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmBootText(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;[Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mBootText:[Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmBootTextView(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mBootTextView:Landroid/widget/TextView;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDoubleClickAiKeyCount(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyCount:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFolded(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFolded:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmForbidFullScreen(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mForbidFullScreen:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmImperceptiblePowerKey(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mImperceptiblePowerKey:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsFoldChanged(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsFoldChanged:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsSynergyMode(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsSynergyMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsVRMode(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsVRMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLongPressAiKeyIsConsumed(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressAiKeyIsConsumed:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLongPressVolumeDownBehavior(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressVolumeDownBehavior:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMetaKeyConsume(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMiuiBootMsgDialog(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/app/Dialog;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiBootMsgDialog:Landroid/app/Dialog;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMiuiPocketModeManager(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Lcom/android/server/input/pocketmode/MiuiPocketModeManager;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiPocketModeManager:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmVoiceAssistEnabled(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVoiceAssistEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mcloseTalkBack(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->closeTalkBack()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleTouchFeatureRotationWatcher(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->handleTouchFeatureRotationWatcher()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhasTalkbackService(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->hasTalkbackService(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misGameMode(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isGameMode()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misTalkBackService(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isTalkBackService(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mmarkShortcutTriggered(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->markShortcutTriggered()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyKidSpaceChanged(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->notifyKidSpaceChanged(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreleaseScreenOnProximitySensor(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->releaseScreenOnProximitySensor(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetScreenRecorderEnabled(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->setScreenRecorderEnabled(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetStatusBarInFullscreen(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->setStatusBarInFullscreen(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetTouchFeatureRotation(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->setTouchFeatureRotation()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshowKeyboardShortcutsMenu(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;IZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showKeyboardShortcutsMenu(IZZ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshowMenu(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showMenu()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mshowRecentApps(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showRecentApps(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartAiKeyService(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->startAiKeyService(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mtakeScreenshot(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->takeScreenshot(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetphoneWindowManagerFeature()Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;
    .locals 1

    sget-object v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->phoneWindowManagerFeature:Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 11

    .line 145
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFoldDevice()Z

    move-result v0

    sput-boolean v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->IS_FOLD_DEVICE:Z

    .line 146
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFlipDevice()Z

    move-result v0

    sput-boolean v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->IS_FLIP_DEVICE:Z

    .line 171
    new-instance v0, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;

    invoke-direct {v0}, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;-><init>()V

    sput-object v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->phoneWindowManagerFeature:Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;

    .line 193
    nop

    .line 194
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v1

    const/16 v2, 0x1a

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v3

    or-int/2addr v1, v3

    sput v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SHORTCUT_HOME_POWER:I

    .line 195
    nop

    .line 196
    const/4 v1, 0x4

    .line 2870
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 196
    invoke-static {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v4

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v5

    or-int/2addr v4, v5

    sput v4, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SHORTCUT_BACK_POWER:I

    .line 197
    nop

    .line 198
    const/16 v4, 0xbb

    invoke-static {v4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v5

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v6

    or-int/2addr v5, v6

    sput v5, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SHORTCUT_MENU_POWER:I

    .line 199
    nop

    .line 200
    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v5

    const/16 v6, 0x19

    invoke-static {v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v7

    or-int/2addr v5, v7

    sput v5, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SHORTCUT_SCREENSHOT_ANDROID:I

    .line 201
    nop

    .line 202
    invoke-static {v4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v5

    invoke-static {v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v7

    or-int/2addr v5, v7

    sput v5, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SHORTCUT_SCREENSHOT_MIUI:I

    .line 203
    nop

    .line 204
    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v5

    invoke-static {v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v7

    or-int/2addr v5, v7

    sput v5, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SHORTCUT_SCREENSHOT_SINGLE_KEY:I

    .line 205
    nop

    .line 206
    invoke-static {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v1

    const/16 v5, 0x18

    invoke-static {v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v7

    or-int/2addr v1, v7

    sput v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SHORTCUT_UNLOCK:I

    .line 242
    const-string/jumbo v1, "support_edge_touch_volume"

    const/4 v7, 0x0

    invoke-static {v1, v7}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SUPPORT_EDGE_TOUCH_VOLUME:Z

    .line 243
    const-string v1, "ro.hardware.fp.sideCap"

    invoke-static {v1, v7}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SUPPORT_POWERFP:Z

    .line 244
    const-string v1, "cetus"

    sget-object v7, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->IS_CETUS:Z

    .line 272
    new-instance v1, Ljava/util/ArrayList;

    new-instance v7, Landroid/content/ComponentName;

    const-string v8, "com.google.android.marvin.talkback.TalkBackService"

    const-string v9, "com.google.android.marvin.talkback"

    invoke-direct {v7, v9, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, Landroid/content/ComponentName;

    const-string v10, ".TalkBackService"

    invoke-direct {v8, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    filled-new-array {v7, v8}, [Landroid/content/ComponentName;

    move-result-object v7

    .line 273
    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->TALK_BACK_SERVICE_LIST:Ljava/util/List;

    .line 296
    const-string v1, "ro.product.device"

    const-string v7, "null"

    invoke-static {v1, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPhoneProduct:Ljava/lang/String;

    .line 2867
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sScreenRecorderKeyEventList:Ljava/util/ArrayList;

    .line 2869
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2870
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2871
    const/16 v0, 0x52

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2872
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2873
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2874
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2875
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2963
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sVoiceAssistKeyEventList:Ljava/util/ArrayList;

    .line 2965
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2966
    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .line 137
    invoke-direct {p0}, Lcom/android/server/policy/PhoneWindowManager;-><init>()V

    .line 227
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyCount:I

    .line 230
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLastClickAiKeyTime:J

    .line 270
    iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressVolumeDownBehavior:I

    .line 298
    sget-object v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->phoneWindowManagerFeature:Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;

    invoke-virtual {v3, p0}, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;->getPowerLongPress(Lcom/android/server/policy/PhoneWindowManager;)Ljava/lang/Runnable;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPowerLongPressOriginal:Ljava/lang/Runnable;

    .line 614
    new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$2;

    invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$2;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mBootCompleteReceiver:Landroid/content/BroadcastReceiver;

    .line 623
    new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;

    invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIDisplayFoldListener:Landroid/view/IDisplayFoldListener;

    .line 703
    new-instance v3, Lmiui/util/SmartCoverManager;

    invoke-direct {v3}, Lmiui/util/SmartCoverManager;-><init>()V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSmartCoverManager:Lmiui/util/SmartCoverManager;

    .line 981
    new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$4;

    invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$4;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSingleClickAiKeyRunnable:Ljava/lang/Runnable;

    .line 990
    new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$5;

    invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$5;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyRunnable:Ljava/lang/Runnable;

    .line 998
    new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$6;

    invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$6;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressDownAiKeyRunnable:Ljava/lang/Runnable;

    .line 1008
    new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$7;

    invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$7;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressUpAiKeyRunnable:Ljava/lang/Runnable;

    .line 1058
    new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$8;

    invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$8;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWakeUpKeySensorListener:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;

    .line 1130
    iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyAction:I

    .line 1134
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z

    .line 1135
    iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemShortcutsMenuShown:Z

    .line 1136
    const/4 v3, -0x1

    iput v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyBoardDeviceId:I

    .line 1138
    new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;

    invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyRunnable:Ljava/lang/Runnable;

    .line 1235
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFpNavEventNameList:Ljava/util/List;

    .line 1236
    iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyguardOnWhenHomeDown:Z

    .line 1745
    new-instance v3, Landroid/os/Binder;

    invoke-direct {v3}, Landroid/os/Binder;-><init>()V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mBinder:Landroid/os/Binder;

    .line 1747
    new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$11;

    invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$11;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mStatusBarExitFullscreenReceiver:Landroid/content/BroadcastReceiver;

    .line 1826
    new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$12;

    invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$12;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenshotReceiver:Landroid/content/BroadcastReceiver;

    .line 1832
    new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$13;

    invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$13;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPartialScreenshotReceiver:Landroid/content/BroadcastReceiver;

    .line 1847
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemKeyPackages:Ljava/util/HashSet;

    .line 1849
    const-string v4, "android"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1850
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemKeyPackages:Ljava/util/HashSet;

    const-string v4, "com.android.systemui"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1851
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemKeyPackages:Ljava/util/HashSet;

    const-string v4, "com.android.phone"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1852
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemKeyPackages:Ljava/util/HashSet;

    const-string v4, "com.android.mms"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1853
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemKeyPackages:Ljava/util/HashSet;

    const-string v4, "com.android.contacts"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1854
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemKeyPackages:Ljava/util/HashSet;

    const-string v4, "com.miui.home"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1855
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemKeyPackages:Ljava/util/HashSet;

    const-string v4, "com.jeejen.family.miui"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1856
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemKeyPackages:Ljava/util/HashSet;

    const-string v4, "com.android.incallui"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1857
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemKeyPackages:Ljava/util/HashSet;

    const-string v4, "com.miui.backup"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1858
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemKeyPackages:Ljava/util/HashSet;

    const-string v4, "com.miui.securitycenter"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1859
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemKeyPackages:Ljava/util/HashSet;

    const-string v4, "com.xiaomi.mihomemanager"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1860
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemKeyPackages:Ljava/util/HashSet;

    const-string v4, "com.miui.securityadd"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2787
    new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$18;

    invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$18;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDoubleTapTimeoutRunnable:Ljava/lang/Runnable;

    .line 2797
    new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$19;

    invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$19;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDoubleClickTimeoutRunnable:Ljava/lang/Runnable;

    .line 2878
    iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenRecorderEnabled:Z

    .line 2884
    new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$20;

    invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$20;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenRecordeEnablekKeyEventReceiver:Landroid/content/BroadcastReceiver;

    .line 2909
    iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z

    .line 2910
    iput-wide v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeStartTime:J

    .line 2911
    const/16 v1, 0x19

    iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeLastKeyCode:I

    .line 2912
    const/16 v1, 0x7d0

    iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeTimeOut:I

    .line 2913
    iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeVolumeDownTimes:I

    .line 2968
    iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVoiceAssistEnabled:Z

    .line 3015
    new-instance v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$21;

    invoke-direct {v0, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$21;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mInternalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private cancelEventAndCallSuperQueueing(Landroid/view/KeyEvent;IZ)V
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "policyFlags"    # I
    .param p3, "isScreenOn"    # Z

    .line 1119
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v0

    or-int/lit8 v0, v0, 0x20

    invoke-static {p1, v0}, Landroid/view/KeyEvent;->changeFlags(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    move-result-object p1

    .line 1120
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->callSuperInterceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I

    .line 1121
    return-void
.end method

.method private cancelPendingAccessibilityShortcutAction()V
    .locals 3

    .line 1607
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-string v2, "close_talkback"

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 1608
    return-void
.end method

.method private closeApp()Z
    .locals 14

    .line 2015
    const-string v0, "NameNotFoundException"

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 2016
    return v2

    .line 2018
    :cond_0
    new-array v3, v2, [Ljava/lang/Object;

    .line 2019
    const-string v4, "getAttrs"

    invoke-static {v1, v4, v3}, Lcom/android/server/input/ReflectionUtils;->callPrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager$LayoutParams;

    .line 2020
    .local v1, "attrs":Landroid/view/WindowManager$LayoutParams;
    if-nez v1, :cond_1

    .line 2021
    return v2

    .line 2024
    :cond_1
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 2025
    .local v3, "type":I
    const/4 v4, 0x1

    if-lt v3, v4, :cond_2

    const/16 v5, 0x63

    if-le v3, v5, :cond_3

    :cond_2
    const/16 v5, 0x3e8

    if-lt v3, v5, :cond_9

    const/16 v5, 0x7cf

    if-le v3, v5, :cond_3

    goto/16 :goto_3

    .line 2031
    :cond_3
    const/4 v5, 0x0

    .line 2032
    .local v5, "title":Ljava/lang/String;
    iget-object v6, v1, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    .line 2033
    .local v6, "packageName":Ljava/lang/String;
    iget-object v7, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 2034
    .local v7, "pm":Landroid/content/pm/PackageManager;
    const/4 v8, -0x2

    .line 2037
    .local v8, "OwningUserId":I
    :try_start_0
    invoke-virtual {v1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2038
    .local v9, "className":Ljava/lang/String;
    const/16 v10, 0x2f

    invoke-virtual {v9, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v10

    .line 2039
    .local v10, "index":I
    if-ltz v10, :cond_4

    .line 2040
    new-instance v11, Landroid/content/ComponentName;

    add-int/lit8 v12, v10, 0x1

    .line 2042
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v13

    invoke-virtual {v9, v12, v13}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-direct {v11, v6, v12}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2045
    .local v11, "componentName":Landroid/content/ComponentName;
    invoke-virtual {v7, v11, v2}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v12

    .line 2046
    .local v12, "activityInfo":Landroid/content/pm/ActivityInfo;
    invoke-virtual {v12, v7}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v13

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v5, v13

    .line 2050
    .end local v9    # "className":Ljava/lang/String;
    .end local v10    # "index":I
    .end local v11    # "componentName":Landroid/content/ComponentName;
    .end local v12    # "activityInfo":Landroid/content/pm/ActivityInfo;
    :cond_4
    goto :goto_0

    .line 2048
    :catch_0
    move-exception v9

    .line 2049
    .local v9, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-static {v0, v9}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2053
    .end local v9    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_0
    :try_start_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2055
    invoke-virtual {v7, v6, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v9

    .line 2056
    .local v9, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v9, v7}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v5, v0

    .line 2060
    .end local v9    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :cond_5
    goto :goto_1

    .line 2058
    :catch_1
    move-exception v9

    .line 2059
    .local v9, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-static {v0, v9}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2062
    .end local v9    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2063
    move-object v5, v6

    .line 2066
    :cond_6
    const-string v0, "com.miui.home"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2067
    const-string v0, "The current window is the home,not need to close app"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 2068
    return v4

    .line 2071
    :cond_7
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemKeyPackages:Ljava/util/HashSet;

    invoke-virtual {v0, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2073
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    filled-new-array {v5}, [Ljava/lang/Object;

    move-result-object v9

    .line 2074
    const v10, 0x110f01f8

    invoke-virtual {v0, v10, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2073
    invoke-direct {p0, v0, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->makeAllUserToastAndShow(Ljava/lang/String;I)Landroid/widget/Toast;

    .line 2076
    const-string v0, "The current window is the system window,not need to close app"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 2077
    return v4

    .line 2082
    :cond_8
    :try_start_2
    iget-object v0, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    const/4 v9, 0x0

    invoke-virtual {p0, v0, v2, v9}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->finishActivityInternal(Landroid/os/IBinder;ILandroid/content/Intent;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 2085
    goto :goto_2

    .line 2083
    :catch_2
    move-exception v0

    .line 2084
    .local v0, "e":Landroid/os/RemoteException;
    const-string v9, "RemoteException"

    invoke-static {v9, v0}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2088
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_2
    const-string v0, "key shortcut"

    invoke-virtual {p0, v6, v8, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 2089
    const-string v0, "The \'close app\' interface was called successfully"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 2092
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    filled-new-array {v5}, [Ljava/lang/Object;

    move-result-object v9

    .line 2093
    const v10, 0x110f01f7

    invoke-virtual {v0, v10, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2092
    invoke-direct {p0, v0, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->makeAllUserToastAndShow(Ljava/lang/String;I)Landroid/widget/Toast;

    .line 2095
    return v4

    .line 2028
    .end local v5    # "title":Ljava/lang/String;
    .end local v6    # "packageName":Ljava/lang/String;
    .end local v7    # "pm":Landroid/content/pm/PackageManager;
    .end local v8    # "OwningUserId":I
    :cond_9
    :goto_3
    return v2
.end method

.method private closeTalkBack()V
    .locals 5

    .line 1974
    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTalkBackIsOpened:Z

    if-eqz v0, :cond_0

    .line 1975
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "miui_talkback"

    const-string v4, "key_combination_volume_up_volume_down"

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 1979
    :cond_0
    return-void
.end method

.method private getAudioManager()Landroid/media/AudioManager;
    .locals 2

    .line 2196
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 2197
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAudioManager:Landroid/media/AudioManager;

    .line 2199
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method private getCodeByKeyPressed(I)I
    .locals 1
    .param p1, "keyPressed"    # I

    .line 173
    sparse-switch p1, :sswitch_data_0

    .line 189
    const/4 v0, 0x0

    return v0

    .line 187
    :sswitch_0
    const/16 v0, 0x18

    return v0

    .line 185
    :sswitch_1
    const/16 v0, 0x19

    return v0

    .line 183
    :sswitch_2
    const/16 v0, 0x1a

    return v0

    .line 181
    :sswitch_3
    const/4 v0, 0x4

    return v0

    .line 179
    :sswitch_4
    const/4 v0, 0x3

    return v0

    .line 177
    :sswitch_5
    const/16 v0, 0xbb

    return v0

    .line 175
    :sswitch_6
    const/16 v0, 0x52

    return v0

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_6
        0x4 -> :sswitch_5
        0x8 -> :sswitch_4
        0x10 -> :sswitch_3
        0x20 -> :sswitch_2
        0x40 -> :sswitch_1
        0x80 -> :sswitch_0
    .end sparse-switch
.end method

.method private getImperceptiblePowerKeyTimeOut()J
    .locals 3

    .line 1567
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    .line 1568
    const/16 v1, 0x1a

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getSingleKeyRule(I)Lcom/android/server/policy/MiuiSingleKeyRule;

    move-result-object v0

    .line 1569
    invoke-virtual {v0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getMiuiLongPressTimeoutMs()J

    move-result-wide v0

    .line 1567
    const-string v2, "ro.miui.imp_power_time"

    invoke-static {v2, v0, v1}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static getKeyBitmask(I)I
    .locals 1
    .param p0, "keycode"    # I

    .line 151
    sparse-switch p0, :sswitch_data_0

    .line 167
    const/4 v0, 0x1

    return v0

    .line 155
    :sswitch_0
    const/4 v0, 0x4

    return v0

    .line 153
    :sswitch_1
    const/4 v0, 0x2

    return v0

    .line 161
    :sswitch_2
    const/16 v0, 0x20

    return v0

    .line 163
    :sswitch_3
    const/16 v0, 0x40

    return v0

    .line 165
    :sswitch_4
    const/16 v0, 0x80

    return v0

    .line 159
    :sswitch_5
    const/16 v0, 0x10

    return v0

    .line 157
    :sswitch_6
    const/16 v0, 0x8

    return v0

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_6
        0x4 -> :sswitch_5
        0x18 -> :sswitch_4
        0x19 -> :sswitch_3
        0x1a -> :sswitch_2
        0x52 -> :sswitch_1
        0xbb -> :sswitch_0
    .end sparse-switch
.end method

.method private handleAiKeyEvent(Landroid/view/KeyEvent;Z)V
    .locals 10
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "down"    # Z

    .line 1018
    const-wide/16 v0, 0x12c

    if-eqz p2, :cond_2

    .line 1019
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 1020
    .local v2, "keyDownTime":J
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-nez v4, :cond_0

    .line 1021
    iput-boolean v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyIsConsumed:Z

    .line 1022
    iput-boolean v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressAiKeyIsConsumed:Z

    .line 1023
    iget v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyCount:I

    add-int/2addr v4, v5

    iput v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyCount:I

    .line 1024
    iget-object v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressDownAiKeyRunnable:Ljava/lang/Runnable;

    const-wide/16 v8, 0x1f4

    invoke-virtual {v4, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1026
    :cond_0
    iget-wide v7, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLastClickAiKeyTime:J

    sub-long v7, v2, v7

    cmp-long v0, v7, v0

    if-gez v0, :cond_1

    iget v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyCount:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1028
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1029
    iput-boolean v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyIsConsumed:Z

    .line 1030
    iput v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyCount:I

    .line 1031
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSingleClickAiKeyRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1032
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressDownAiKeyRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1034
    :cond_1
    iput-wide v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLastClickAiKeyTime:J

    .line 1035
    .end local v2    # "keyDownTime":J
    goto :goto_0

    .line 1036
    :cond_2
    iget-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressAiKeyIsConsumed:Z

    if-eqz v2, :cond_3

    .line 1037
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressUpAiKeyRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1038
    :cond_3
    iget-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyIsConsumed:Z

    if-nez v2, :cond_4

    .line 1039
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSingleClickAiKeyRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1040
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressDownAiKeyRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1043
    :cond_4
    :goto_0
    return-void
.end method

.method private handleDoubleTapOnHome()V
    .locals 2

    .line 2807
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    iget v0, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDoubleTapOnHomeBehavior:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2809
    iput-boolean v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeConsumed:Z

    .line 2810
    invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchRecentPanel()Z

    .line 2812
    :cond_0
    return-void
.end method

.method private handleMetaKey(Landroid/view/KeyEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 1168
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    .line 1169
    .local v0, "down":Z
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    .line 1171
    .local v3, "keyCode":I
    invoke-static {v3}, Landroid/view/KeyEvent;->isMetaKey(I)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1172
    invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isUserSetupComplete()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/miui/server/input/PadManager;->isPad()Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_1

    .line 1175
    :cond_1
    if-eqz v0, :cond_3

    .line 1176
    iget-object v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1177
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v4

    iput v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyBoardDeviceId:I

    .line 1178
    iget-boolean v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z

    if-eqz v4, :cond_2

    .line 1179
    iput-boolean v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z

    .line 1180
    const/4 v1, 0x2

    iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyAction:I

    .line 1181
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x12c

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    .line 1183
    :cond_2
    iput v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyAction:I

    .line 1184
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 1187
    :cond_3
    iget-boolean v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z

    if-nez v4, :cond_4

    iget v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyAction:I

    if-eq v4, v2, :cond_4

    .line 1188
    iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyAction:I

    goto :goto_2

    .line 1189
    :cond_4
    iget-boolean v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemShortcutsMenuShown:Z

    if-eqz v4, :cond_8

    .line 1190
    iget v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyBoardDeviceId:I

    invoke-direct {p0, v4, v2, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showKeyboardShortcutsMenu(IZZ)V

    goto :goto_2

    .line 1173
    :cond_5
    :goto_1
    return-void

    .line 1194
    :cond_6
    iget-boolean v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z

    if-nez v4, :cond_7

    .line 1195
    iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z

    goto :goto_2

    .line 1196
    :cond_7
    iget-boolean v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemShortcutsMenuShown:Z

    if-eqz v4, :cond_8

    .line 1197
    iget v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyBoardDeviceId:I

    invoke-direct {p0, v4, v2, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showKeyboardShortcutsMenu(IZZ)V

    .line 1200
    :cond_8
    :goto_2
    return-void
.end method

.method private handleTouchFeatureRotationWatcher()V
    .locals 3

    .line 2257
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mRotationWatcher:Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;

    if-nez v0, :cond_0

    .line 2258
    new-instance v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;

    invoke-direct {v0, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    iput-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mRotationWatcher:Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;

    .line 2260
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsSupportGloablTounchDirection:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isGameMode()Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z

    if-eqz v0, :cond_1

    goto :goto_0

    .line 2274
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWindowManager:Landroid/view/IWindowManager;

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mRotationWatcher:Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;

    invoke-interface {v0, v1}, Landroid/view/IWindowManager;->removeRotationWatcher(Landroid/view/IRotationWatcher;)V

    .line 2275
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHasWatchedRotation:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2278
    goto :goto_2

    .line 2276
    :catch_0
    move-exception v0

    .line 2277
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 2262
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->setTouchFeatureRotation()V

    .line 2263
    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHasWatchedRotation:Z

    if-nez v0, :cond_3

    .line 2265
    :try_start_1
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWindowManager:Landroid/view/IWindowManager;

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mRotationWatcher:Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;

    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    .line 2266
    invoke-virtual {v2}, Landroid/content/Context;->getDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getDisplayId()I

    move-result v2

    .line 2265
    invoke-interface {v0, v1, v2}, Landroid/view/IWindowManager;->watchRotation(Landroid/view/IRotationWatcher;I)I

    .line 2267
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHasWatchedRotation:Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 2268
    :catch_1
    move-exception v0

    .line 2269
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2270
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    nop

    .line 2280
    :cond_3
    :goto_2
    return-void
.end method

.method private hasTalkbackService(Ljava/lang/String;)Z
    .locals 4
    .param p1, "accessibilityShortcut"    # Ljava/lang/String;

    .line 2513
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2514
    return v1

    .line 2516
    :cond_0
    sget-object v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->TALK_BACK_SERVICE_LIST:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 2517
    .local v2, "componentName":Landroid/content/ComponentName;
    invoke-static {p1, v2}, Lmiui/provider/SettingsStringUtil$ComponentNameSet;->contains(Ljava/lang/String;Landroid/content/ComponentName;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2518
    const/4 v0, 0x1

    return v0

    .line 2520
    .end local v2    # "componentName":Landroid/content/ComponentName;
    :cond_1
    goto :goto_0

    .line 2521
    :cond_2
    return v1
.end method

.method private inFingerprintEnrolling()Z
    .locals 5

    .line 1728
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1730
    .local v0, "am":Landroid/app/ActivityManager;
    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    .line 1731
    .local v3, "topClassName":Ljava/lang/String;
    const-string v4, "com.android.settings.NewFingerprintInternalActivity"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v4, :cond_0

    .line 1732
    return v2

    .line 1736
    .end local v3    # "topClassName":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 1734
    :catch_0
    move-exception v2

    .line 1735
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "Exception"

    invoke-static {v3, v2}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1737
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    return v1
.end method

.method private injectEvent(I)V
    .locals 15
    .param p1, "injectKeyCode"    # I

    .line 2160
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v11

    .line 2161
    .local v11, "now":J
    new-instance v13, Landroid/view/KeyEvent;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, -0x1

    const/4 v10, 0x0

    move-object v0, v13

    move-wide v1, v11

    move-wide v3, v11

    move/from16 v6, p1

    invoke-direct/range {v0 .. v10}, Landroid/view/KeyEvent;-><init>(JJIIIIII)V

    .line 2163
    .local v13, "homeDown":Landroid/view/KeyEvent;
    new-instance v14, Landroid/view/KeyEvent;

    const/4 v5, 0x1

    move-object v0, v14

    invoke-direct/range {v0 .. v10}, Landroid/view/KeyEvent;-><init>(JJIIIIII)V

    .line 2165
    .local v0, "homeUp":Landroid/view/KeyEvent;
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v13, v2}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    .line 2167
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    .line 2169
    return-void
.end method

.method private injectEventFromShortcut(I)V
    .locals 16
    .param p1, "injectKeyCode"    # I

    .line 2171
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v11

    .line 2172
    .local v11, "now":J
    new-instance v13, Landroid/view/KeyEvent;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, -0x1

    const/4 v10, 0x0

    move-object v0, v13

    move-wide v1, v11

    move-wide v3, v11

    move/from16 v6, p1

    invoke-direct/range {v0 .. v10}, Landroid/view/KeyEvent;-><init>(JJIIIIII)V

    .line 2174
    .local v13, "down":Landroid/view/KeyEvent;
    const/high16 v14, 0x1000000

    invoke-virtual {v13, v14}, Landroid/view/KeyEvent;->setFlags(I)V

    .line 2175
    new-instance v15, Landroid/view/KeyEvent;

    const/4 v5, 0x1

    move-object v0, v15

    invoke-direct/range {v0 .. v10}, Landroid/view/KeyEvent;-><init>(JJIIIIII)V

    .line 2177
    .local v0, "up":Landroid/view/KeyEvent;
    invoke-virtual {v0, v14}, Landroid/view/KeyEvent;->setFlags(I)V

    .line 2178
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v13, v2}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    .line 2180
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    .line 2182
    return-void
.end method

.method private interceptAccessibilityShortcutChord(Z)V
    .locals 6
    .param p1, "keyguardActive"    # Z

    .line 1592
    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyPressed:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyPressed:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTalkBackIsOpened:Z

    if-eqz v0, :cond_1

    .line 1593
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1594
    .local v0, "now":J
    iget-wide v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyTime:J

    const-wide/16 v4, 0x96

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    iget-wide v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyTime:J

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    .line 1596
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyConsumed:Z

    .line 1597
    iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyConsumed:Z

    .line 1598
    iget-boolean v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mShortcutServiceIsTalkBack:Z

    if-eqz v3, :cond_0

    if-eqz p1, :cond_1

    iget-boolean v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAccessibilityShortcutOnLockScreen:Z

    if-nez v3, :cond_1

    .line 1599
    :cond_0
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    const-string v4, "close_talkback"

    invoke-virtual {v3, v2, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0xbb8

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1604
    .end local v0    # "now":J
    :cond_1
    return-void
.end method

.method private isAudioActive()Z
    .locals 6

    .line 2842
    const/4 v0, 0x0

    .line 2843
    .local v0, "active":Z
    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioManager;->getMode()I

    move-result v1

    .line 2844
    .local v1, "mode":I
    const-string v2, "isAudioActive():"

    if-lez v1, :cond_0

    const/4 v3, 0x7

    if-ge v1, v3, :cond_0

    .line 2845
    const/4 v0, 0x1

    .line 2846
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 2847
    return v0

    .line 2849
    :cond_0
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    move-result v3

    .line 2850
    .local v3, "size":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_3

    .line 2851
    const/4 v5, 0x1

    if-ne v5, v4, :cond_1

    .line 2853
    goto :goto_1

    .line 2855
    :cond_1
    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v0

    .line 2856
    if-eqz v0, :cond_2

    .line 2857
    goto :goto_2

    .line 2850
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2860
    .end local v4    # "i":I
    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    .line 2861
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 2863
    :cond_4
    return v0
.end method

.method private isGameMode()Z
    .locals 3

    .line 2237
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gb_boosting"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    return v2
.end method

.method private isInCallScreenShowing()Z
    .locals 5

    .line 927
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    .line 928
    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 929
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 930
    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 931
    .local v2, "runningActivity":Ljava/lang/String;
    const-string v4, "com.android.phone.MiuiInCallScreen"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 932
    const-string v4, "com.android.incallui.InCallActivity"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_0
    move v1, v3

    goto :goto_1

    :cond_1
    :goto_0
    nop

    .line 931
    :goto_1
    return v1
.end method

.method public static isLargeScreen(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .line 3029
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 3030
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    .line 3031
    .local v1, "smallestScreenWidthDp":I
    iget v2, v0, Landroid/content/res/Configuration;->densityDpi:I

    const/16 v3, 0x160

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-ne v2, v3, :cond_1

    .line 3033
    const/16 v2, 0x190

    if-le v1, v2, :cond_0

    goto :goto_0

    :cond_0
    move v4, v5

    :goto_0
    return v4

    .line 3034
    :cond_1
    iget v2, v0, Landroid/content/res/Configuration;->densityDpi:I

    const/16 v3, 0x1cd

    if-ne v2, v3, :cond_3

    .line 3036
    const/16 v2, 0x131

    if-le v1, v2, :cond_2

    goto :goto_1

    :cond_2
    move v4, v5

    :goto_1
    return v4

    .line 3038
    :cond_3
    const/16 v2, 0x140

    if-le v1, v2, :cond_4

    goto :goto_2

    :cond_4
    move v4, v5

    :goto_2
    return v4
.end method

.method private isLockDeviceWindow(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)Z
    .locals 3
    .param p1, "win"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 3009
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 3010
    :cond_0
    const-string v1, "getAttrs"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {p1, v1, v2}, Lcom/android/server/input/ReflectionUtils;->callPrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager$LayoutParams;

    .line 3011
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    if-nez v1, :cond_1

    return v0

    .line 3012
    :cond_1
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    and-int/lit16 v2, v2, 0x800

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method private isMiPad()Z
    .locals 1

    .line 1203
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z

    move-result v0

    return v0
.end method

.method private isNfcEnable(Z)Z
    .locals 4
    .param p1, "ishomeclick"    # Z

    .line 2815
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_4

    .line 2816
    const-string v2, "sagit"

    sget-object v3, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "jason"

    sget-object v3, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 2819
    :cond_0
    iget-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveBankCard:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveTranksCard:Z

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v0, v1

    :cond_2
    :goto_0
    return v0

    .line 2817
    :cond_3
    :goto_1
    return v1

    .line 2822
    :cond_4
    iget v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressVolumeDownBehavior:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    iget-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveTranksCard:Z

    if-eqz v2, :cond_5

    goto :goto_2

    :cond_5
    move v0, v1

    :goto_2
    return v0
.end method

.method private isTalkBackService(Ljava/lang/String;)Z
    .locals 3
    .param p1, "accessibilityShortcut"    # Ljava/lang/String;

    .line 2504
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2505
    return v1

    .line 2507
    :cond_0
    invoke-static {p1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 2508
    .local v0, "componentName":Landroid/content/ComponentName;
    if-eqz v0, :cond_1

    sget-object v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->TALK_BACK_SERVICE_LIST:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private isTrackInputEvenForScreenRecorder(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 2893
    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenRecorderEnabled:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sScreenRecorderKeyEventList:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2894
    const/4 v0, 0x1

    return v0

    .line 2896
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isTrackInputEventForVoiceAssist(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 2971
    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVoiceAssistEnabled:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sVoiceAssistKeyEventList:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2972
    const/4 v0, 0x1

    return v0

    .line 2974
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private synthetic lambda$finishLayoutLw$4(I)V
    .locals 0
    .param p1, "inputMethodHeight"    # I

    .line 2756
    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->setInputMethodModeToTouch(I)V

    return-void
.end method

.method private synthetic lambda$sendAsyncBroadcast$2(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .line 2710
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    sget-object v1, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    .line 2711
    invoke-static {}, Landroid/app/BroadcastOptions;->makeBasic()Landroid/app/BroadcastOptions;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/BroadcastOptions;->setInteractive(Z)Landroid/app/BroadcastOptions;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/BroadcastOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 2710
    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v3, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method private synthetic lambda$sendAsyncBroadcast$3(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .line 2713
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    sget-object v1, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    return-void
.end method

.method private synthetic lambda$setCurrentUserLw$1()V
    .locals 4

    .line 2548
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCurrentUserId:I

    const-string v3, "is_mi_input_event_time_line_enable"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 2550
    return-void
.end method

.method private synthetic lambda$takeScreenshot$0(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 4
    .param p1, "function"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1840
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    .line 1841
    const-string/jumbo v1, "shortcut"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1840
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 1843
    return-void
.end method

.method private synthetic lambda$trackDumpLogKeyCode$5()V
    .locals 5

    .line 2953
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string v3, "dump_log_or_secret_code"

    const-string/jumbo v4, "volume_down_up_three_time"

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 2956
    return-void
.end method

.method private makeAllUserToastAndShow(Ljava/lang/String;I)Landroid/widget/Toast;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .line 956
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 957
    .local v0, "toast":Landroid/widget/Toast;
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 958
    return-object v0
.end method

.method private markShortcutTriggered()V
    .locals 2

    .line 936
    sget-object v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->phoneWindowManagerFeature:Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;->callInterceptPowerKeyUp(Lcom/android/server/policy/PhoneWindowManager;Z)V

    .line 937
    return-void
.end method

.method private notifyKidSpaceChanged(Z)V
    .locals 1
    .param p1, "isInKidMode"    # Z

    .line 2283
    iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsKidMode:Z

    .line 2284
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyInterceptExtend:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setKidSpaceMode(Z)V

    .line 2285
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->notifyKidSpaceChanged(Z)V

    .line 2286
    return-void
.end method

.method private notifyPowerKeeperKeyEvent(Landroid/view/KeyEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 1128
    return-void
.end method

.method private playSoundEffect(IIZI)V
    .locals 1
    .param p1, "policyFlags"    # I
    .param p2, "keyCode"    # I
    .param p3, "down"    # Z
    .param p4, "repeatCount"    # I

    .line 2203
    if-eqz p3, :cond_0

    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    if-nez p4, :cond_0

    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVibrator:Landroid/os/Vibrator;

    .line 2204
    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->hasNavigationBar()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2205
    sparse-switch p2, :sswitch_data_0

    goto :goto_0

    .line 2211
    :sswitch_0
    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->playSoundEffect()Z

    .line 2216
    :cond_0
    :goto_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x52 -> :sswitch_0
        0x54 -> :sswitch_0
        0xbb -> :sswitch_0
    .end sparse-switch
.end method

.method private playSoundEffect()Z
    .locals 2

    .line 2219
    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    .line 2220
    .local v0, "audioManager":Landroid/media/AudioManager;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 2221
    return v1

    .line 2223
    :cond_0
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 2224
    const/4 v1, 0x1

    return v1
.end method

.method private postKeyFunction(Ljava/lang/String;ILjava/lang/String;)V
    .locals 5
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "delay"    # I
    .param p3, "shortcut"    # Ljava/lang/String;

    .line 1573
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1574
    const-string v0, "action is null"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 1575
    return-void

    .line 1577
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1578
    .local v0, "message":Landroid/os/Message;
    const-string v2, "dumpsys_by_power"

    if-ne p1, v2, :cond_1

    .line 1579
    invoke-virtual {v0, v1}, Landroid/os/Message;->setAsynchronous(Z)V

    .line 1581
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1582
    .local v1, "bundle":Landroid/os/Bundle;
    const-string/jumbo v2, "shortcut"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1583
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1584
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    int-to-long v3, p2

    invoke-virtual {v2, v0, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1585
    return-void
.end method

.method private postPowerLongPress()V
    .locals 4

    .line 1545
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    const-string v1, "long_press_power_key"

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1547
    .local v0, "longPressPowerFunction":Ljava/lang/String;
    const/16 v1, 0xfa0

    const-string v2, "long_press_power_key_four_second"

    const-string v3, "dumpsys_by_power"

    invoke-direct {p0, v3, v1, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->postKeyFunction(Ljava/lang/String;ILjava/lang/String;)V

    .line 1549
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "none"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1551
    :cond_0
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mImperceptiblePowerKey:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getImperceptiblePowerKeyTimeOut()J

    move-result-wide v2

    long-to-int v2, v2

    const-string v3, "imperceptible_press_power_key"

    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->postKeyFunction(Ljava/lang/String;ILjava/lang/String;)V

    .line 1554
    :cond_1
    return-void
.end method

.method private postVolumeUpLongPress()V
    .locals 3

    .line 1563
    const/16 v0, 0x1388

    const-string v1, ""

    const-string v2, "key_long_press_volume_up"

    invoke-direct {p0, v2, v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->postKeyFunction(Ljava/lang/String;ILjava/lang/String;)V

    .line 1564
    return-void
.end method

.method private preloadRecentApps()V
    .locals 0

    .line 2137
    invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->preloadRecentAppsInternal()V

    .line 2138
    return-void
.end method

.method private releaseScreenOnProximitySensor(Z)V
    .locals 1
    .param p1, "isNowRelease"    # Z

    .line 718
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mProximitySensor:Lcom/android/server/policy/MiuiScreenOnProximityLock;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->release(Z)Z

    .line 719
    :cond_0
    return-void
.end method

.method private removeKeyFunction(Ljava/lang/String;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/String;

    .line 1588
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 1589
    return-void
.end method

.method private removePowerLongPress()V
    .locals 1

    .line 1540
    const-string v0, "dumpsys_by_power"

    invoke-direct {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->removeKeyFunction(Ljava/lang/String;)V

    .line 1541
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mImperceptiblePowerKey:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->removeKeyFunction(Ljava/lang/String;)V

    .line 1542
    return-void
.end method

.method private removeVolumeUpLongPress()V
    .locals 2

    .line 1557
    const-string/jumbo v0, "zhuque"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1558
    const-string v0, "key_long_press_volume_up"

    invoke-direct {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->removeKeyFunction(Ljava/lang/String;)V

    .line 1560
    :cond_0
    return-void
.end method

.method private saveWindowTypeLayer(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .line 428
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 429
    .local v0, "typeLayers":Lorg/json/JSONObject;
    const/16 v1, 0x7d1

    const/16 v2, 0x7dd

    const/16 v3, 0x7d0

    filled-new-array {v3, v1, v2}, [I

    move-result-object v1

    .line 432
    .local v1, "types":[I
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget v4, v1, v3

    .line 433
    .local v4, "type":I
    invoke-virtual {p0, v4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getWindowLayerFromTypeLw(I)I

    move-result v5

    .line 434
    .local v5, "layer":I
    const/4 v6, 0x2

    if-eq v5, v6, :cond_0

    .line 436
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 439
    goto :goto_1

    .line 437
    :catch_0
    move-exception v6

    .line 438
    .local v6, "ex":Lorg/json/JSONException;
    const-string v7, "WindowManager"

    const-string v8, "JSONException"

    invoke-static {v7, v8, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 432
    .end local v4    # "type":I
    .end local v5    # "layer":I
    .end local v6    # "ex":Lorg/json/JSONException;
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 442
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "window_type_layer"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/provider/MiuiSettings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 443
    return-void
.end method

.method private sendBackKeyEventBroadcast(Landroid/view/KeyEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 3003
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.KEYCODE_BACK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3004
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.extra.KEY_EVENT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3005
    invoke-virtual {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;)V

    .line 3006
    return-void
.end method

.method private sendFullScreenStateToHome()V
    .locals 3

    .line 2996
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.fullscreen_state_change"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2997
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "state"

    const-string/jumbo v2, "toHome"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2998
    invoke-virtual {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcastForAllUser(Landroid/content/Intent;)V

    .line 2999
    return-void
.end method

.method private sendFullScreenStateToTaskSnapshot()V
    .locals 3

    .line 2988
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.fullscreen_state_change"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2989
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "state"

    const-string/jumbo v2, "taskSnapshot"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2990
    invoke-virtual {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;)V

    .line 2991
    return-void
.end method

.method private sendKeyEventBroadcast(Landroid/view/KeyEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 2900
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.SCREEN_RECORDER_TRACK_KEYEVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2901
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.screenrecorder"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2902
    const-string v1, "keycode"

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2903
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "isdown"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2904
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 2905
    return-void
.end method

.method private sendOthersBroadcast(ZZZILandroid/view/KeyEvent;)Z
    .locals 16
    .param p1, "down"    # Z
    .param p2, "isScreenOn"    # Z
    .param p3, "keyguardActive"    # Z
    .param p4, "keyCode"    # I
    .param p5, "event"    # Landroid/view/KeyEvent;

    .line 1661
    move-object/from16 v0, p0

    move/from16 v1, p4

    move-object/from16 v2, p5

    const/4 v3, 0x0

    const/16 v4, 0x1a

    if-eqz p1, :cond_9

    .line 1665
    const/16 v5, 0xa4

    const/high16 v6, 0x40000000    # 2.0f

    const-string v7, "android.intent.extra.KEY_EVENT"

    const/16 v8, 0x18

    const/16 v9, 0x19

    const/4 v10, 0x1

    if-eqz p2, :cond_1

    if-nez p3, :cond_1

    if-eq v1, v4, :cond_0

    if-eq v1, v9, :cond_0

    if-eq v1, v8, :cond_0

    if-eq v1, v5, :cond_0

    const/16 v11, 0x55

    if-eq v1, v11, :cond_0

    const/16 v11, 0x4f

    if-ne v1, v11, :cond_1

    .line 1671
    :cond_0
    new-instance v11, Landroid/content/Intent;

    const-string v12, "miui.intent.action.KEYCODE_EXTERNAL"

    invoke-direct {v11, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1672
    .local v11, "i":Landroid/content/Intent;
    invoke-virtual {v11, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1673
    invoke-virtual {v11, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1674
    invoke-virtual {v0, v11, v10}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;Z)V

    .line 1676
    .end local v11    # "i":Landroid/content/Intent;
    :cond_1
    if-ne v1, v4, :cond_2

    move v4, v10

    goto :goto_0

    :cond_2
    move v4, v3

    .line 1677
    .local v4, "stopNotification":Z
    :goto_0
    if-nez v4, :cond_4

    if-eqz p3, :cond_4

    .line 1678
    if-eq v1, v9, :cond_3

    if-eq v1, v8, :cond_3

    if-ne v1, v5, :cond_4

    .line 1681
    :cond_3
    const/4 v4, 0x1

    .line 1685
    :cond_4
    if-eqz v4, :cond_5

    iget-boolean v5, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemReady:Z

    if-eqz v5, :cond_5

    .line 1686
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    move-result-object v5

    .line 1687
    .local v5, "statusBarService":Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v5, :cond_5

    .line 1688
    invoke-virtual {v0, v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->onStatusBarPanelRevealed(Lcom/android/internal/statusbar/IStatusBarService;)V

    .line 1692
    .end local v5    # "statusBarService":Lcom/android/internal/statusbar/IStatusBarService;
    :cond_5
    if-eq v1, v9, :cond_6

    if-ne v1, v8, :cond_8

    .line 1694
    :cond_6
    iget-object v5, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 1695
    .local v5, "cr":Landroid/content/ContentResolver;
    const-string v8, "remote_control_proc_name"

    invoke-static {v5, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1696
    .local v9, "proc":Ljava/lang/String;
    const-string v11, "remote_control_pkg_name"

    invoke-static {v5, v11}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1698
    .local v12, "pkg":Ljava/lang/String;
    if-eqz v9, :cond_8

    if-eqz v12, :cond_8

    .line 1699
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v13

    .line 1700
    .local v13, "c":J
    invoke-virtual {v0, v9}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->checkProcessRunning(Ljava/lang/String;)Z

    move-result v15

    .line 1704
    .local v15, "running":Z
    if-eqz v15, :cond_7

    .line 1705
    new-instance v3, Landroid/content/Intent;

    const-string v8, "miui.intent.action.REMOTE_CONTROL"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1706
    .local v3, "i":Landroid/content/Intent;
    invoke-virtual {v3, v12}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1707
    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1708
    invoke-virtual {v3, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1709
    invoke-virtual {v0, v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;)V

    .line 1710
    return v10

    .line 1712
    .end local v3    # "i":Landroid/content/Intent;
    :cond_7
    const/4 v6, 0x0

    invoke-static {v5, v8, v6}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1713
    invoke-static {v5, v11, v6}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1717
    .end local v4    # "stopNotification":Z
    .end local v5    # "cr":Landroid/content/ContentResolver;
    .end local v9    # "proc":Ljava/lang/String;
    .end local v12    # "pkg":Ljava/lang/String;
    .end local v13    # "c":J
    .end local v15    # "running":Z
    :cond_8
    goto :goto_1

    .line 1718
    :cond_9
    if-ne v1, v4, :cond_a

    .line 1719
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.KEYCODE_POWER_UP"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;)V

    .line 1722
    :cond_a
    :goto_1
    return v3
.end method

.method public static sendRecordCountEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "event"    # Ljava/lang/String;

    .line 940
    const-string v0, "count_event"

    .line 941
    .local v0, "STAT_TYPE_COUNT_EVENT":Ljava/lang/String;
    const-string v1, "com.miui.gallery"

    .line 942
    .local v1, "GALLERY_PKG_NAME":Ljava/lang/String;
    const-string v2, "com.miui.gallery.intent.action.SEND_STAT"

    .line 943
    .local v2, "ACTION_SEND_STAT":Ljava/lang/String;
    const-string/jumbo v3, "stat_type"

    .line 944
    .local v3, "EXTRA_STAT_TYPE":Ljava/lang/String;
    const-string v4, "category"

    .line 945
    .local v4, "EXTRA_CATEGORY":Ljava/lang/String;
    const-string v5, "event"

    .line 947
    .local v5, "EXTRA_EVENT":Ljava/lang/String;
    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.miui.gallery.intent.action.SEND_STAT"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 948
    .local v6, "intent":Landroid/content/Intent;
    const-string v7, "com.miui.gallery"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 949
    const-string/jumbo v7, "stat_type"

    const-string v8, "count_event"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 950
    const-string v7, "category"

    invoke-virtual {v6, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 951
    const-string v7, "event"

    invoke-virtual {v6, v7, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 952
    invoke-virtual {p0, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 953
    return-void
.end method

.method private sendVoiceAssistKeyEventBroadcast(Landroid/view/KeyEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 2978
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.VOICE_ASSIST_TRACK_KEYEVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2979
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.voiceassist"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2980
    const-string v1, "keycode"

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2981
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "isdown"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2982
    invoke-virtual {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;)V

    .line 2983
    return-void
.end method

.method private setInputMethodModeToTouch(I)V
    .locals 4
    .param p1, "inputMethodHeight"    # I

    .line 2774
    const/16 v0, 0x19

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 2775
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v0, v3}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z

    goto :goto_0

    .line 2778
    :cond_0
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v2

    invoke-virtual {v2, v1, v0, v1}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z

    .line 2781
    :goto_0
    return-void
.end method

.method private setScreenRecorderEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 2881
    iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenRecorderEnabled:Z

    .line 2882
    return-void
.end method

.method private setStatusBarInFullscreen(Z)V
    .locals 4
    .param p1, "show"    # Z

    .line 1754
    iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsStatusBarVisibleInFullscreen:Z

    .line 1756
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    move-result-object v0

    .line 1757
    .local v0, "statusbar":Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v0, :cond_1

    .line 1758
    nop

    .line 1759
    if-eqz p1, :cond_0

    const/high16 v1, -0x80000000

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mBinder:Landroid/os/Binder;

    const-string v3, "android"

    .line 1758
    invoke-interface {v0, v1, v2, v3}, Lcom/android/internal/statusbar/IStatusBarService;->disable(ILandroid/os/IBinder;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1767
    .end local v0    # "statusbar":Lcom/android/internal/statusbar/IStatusBarService;
    :cond_1
    goto :goto_1

    .line 1763
    :catch_0
    move-exception v0

    .line 1765
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "WindowManager"

    const-string v2, "RemoteException"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1766
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    .line 1775
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void
.end method

.method private setTouchFeatureRotation()V
    .locals 4

    .line 2241
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 2242
    .local v0, "rotation":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set rotation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "WindowManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2243
    const/4 v1, 0x0

    .line 2244
    .local v1, "targetId":I
    sget-boolean v2, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFolded:Z

    if-eqz v2, :cond_0

    .line 2245
    const/4 v1, 0x1

    .line 2247
    :cond_0
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v1, v3, v0}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z

    .line 2249
    return-void
.end method

.method private shouldInterceptHeadSetHookKey(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .line 1649
    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMikeymodeEnabled:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4f

    if-ne p1, v0, :cond_0

    .line 1650
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.MIKEY_BUTTON"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1651
    .local v0, "mikeyIntent":Landroid/content/Intent;
    const-string v1, "com.xiaomi.miclick"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1652
    const-string v1, "key_action"

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1653
    const-string v1, "key_event_time"

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1654
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1655
    const/4 v1, 0x1

    return v1

    .line 1657
    .end local v0    # "mikeyIntent":Landroid/content/Intent;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private shouldInterceptKey(Landroid/view/KeyEvent;IZ)Z
    .locals 6
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "policyFlags"    # I
    .param p3, "isScreenOn"    # Z

    .line 1089
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 1090
    .local v0, "keyCode":I
    iget-boolean v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsVRMode:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 1091
    const-string v1, "VR mode drop all keys."

    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 1092
    return v2

    .line 1094
    :cond_0
    const-string/jumbo v1, "sys.in_shutdown_progress"

    const/4 v3, 0x0

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 1096
    const-string/jumbo v1, "this device is being shut down, ignore key event."

    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 1097
    return v2

    .line 1099
    :cond_1
    if-nez p3, :cond_3

    .line 1100
    const/4 v1, 0x4

    if-eq v1, v0, :cond_2

    const/16 v1, 0x52

    if-ne v1, v0, :cond_3

    .line 1101
    :cond_2
    const-string v1, "Cancel back or menu key when screen is off"

    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 1102
    invoke-direct {p0, p1, p2, v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->cancelEventAndCallSuperQueueing(Landroid/view/KeyEvent;IZ)V

    .line 1103
    return v2

    .line 1107
    :cond_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    .line 1108
    .local v1, "inputDevice":Landroid/view/InputDevice;
    if-eqz v1, :cond_5

    .line 1109
    invoke-virtual {v1}, Landroid/view/InputDevice;->getProductId()I

    move-result v4

    const/16 v5, 0x628

    if-ne v4, v5, :cond_5

    .line 1110
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mShoulderKeyManagerInternal:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

    if-eqz v3, :cond_4

    .line 1111
    invoke-interface {v3, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;->handleShoulderKeyEvent(Landroid/view/KeyEvent;)V

    .line 1113
    :cond_4
    return v2

    .line 1115
    :cond_5
    return v3
.end method

.method private showKeyboardShortcutsMenu(IZZ)V
    .locals 3
    .param p1, "deviceId"    # I
    .param p2, "system"    # Z
    .param p3, "show"    # Z

    .line 1216
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "toggleKeyboardShortcutsMenu ,deviceId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  system:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " show:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 1219
    if-eqz p2, :cond_0

    .line 1220
    iput-boolean p3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemShortcutsMenuShown:Z

    .line 1222
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.systemui.action.KEYBOARD_SHORTCUTS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1223
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.systemui"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1224
    const-string/jumbo v1, "system"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1225
    const-string/jumbo v1, "show"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1226
    const-string v1, "deviceId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1227
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1228
    return-void
.end method

.method private showMenu()Z
    .locals 3

    .line 2152
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    const-string/jumbo v1, "virtual_key_longpress"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmiui/util/HapticFeedbackUtil;->performHapticFeedback(Ljava/lang/String;Z)Z

    .line 2153
    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->markShortcutTriggered()V

    .line 2154
    const/16 v0, 0x52

    invoke-direct {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->injectEventFromShortcut(I)V

    .line 2155
    const-string/jumbo v0, "show menu,the inject event is KEYCODE_MENU "

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 2156
    return v2
.end method

.method private showRecentApps(Z)V
    .locals 1
    .param p1, "triggeredFromAltTab"    # Z

    .line 1207
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPreloadedRecentApps:Z

    .line 1208
    invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;

    move-result-object v0

    .line 1209
    .local v0, "statusbar":Lcom/android/server/statusbar/StatusBarManagerInternal;
    if-eqz v0, :cond_0

    .line 1210
    invoke-interface {v0, p1}, Lcom/android/server/statusbar/StatusBarManagerInternal;->showRecentApps(Z)V

    .line 1212
    :cond_0
    return-void
.end method

.method private skipPocketModeAquireByWakeUpInfo(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "detail"    # Ljava/lang/String;
    .param p2, "wakeUpReason"    # Ljava/lang/String;

    .line 569
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 570
    .local v0, "currentTime":J
    iget-wide v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mUpdateWakeUpDetailTime:J

    cmp-long v2, v0, v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-lez v2, :cond_0

    iget-wide v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mUpdateWakeUpDetailTime:J

    sub-long v5, v0, v5

    const-wide/16 v7, 0x64

    cmp-long v2, v5, v7

    if-gez v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v4

    .line 573
    .local v2, "validData":Z
    :goto_0
    const-string v5, "android.policy:FINGERPRINT"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 575
    return v2

    .line 576
    :cond_1
    const-string/jumbo v5, "server.display:unfold"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 578
    if-eqz v2, :cond_2

    const/16 v5, 0xc

    invoke-static {v5}, Landroid/os/PowerManager;->wakeReasonToString(I)Ljava/lang/String;

    move-result-object v5

    .line 579
    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_1

    :cond_2
    move v3, v4

    .line 578
    :goto_1
    return v3

    .line 581
    :cond_3
    return v4
.end method

.method private startAiKeyService(Ljava/lang/String;)V
    .locals 4
    .param p1, "pressType"    # Ljava/lang/String;

    .line 971
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 972
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.ai.AidaemonService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 974
    const-string v1, "key_ai_button_settings"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 975
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 978
    nop

    .end local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 976
    :catch_0
    move-exception v0

    .line 977
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "WindowManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 979
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private startCameraProcess()V
    .locals 3

    .line 650
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.action.CAMERA_EMPTY_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 651
    .local v0, "cameraIntent":Landroid/content/Intent;
    const-string v1, "com.android.camera"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 652
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 655
    nop

    .end local v0    # "cameraIntent":Landroid/content/Intent;
    goto :goto_0

    .line 653
    :catch_0
    move-exception v0

    .line 654
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "WindowManager"

    const-string v2, "IllegalAccessException"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 656
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private streetSnap(ZIZLandroid/view/KeyEvent;)V
    .locals 4
    .param p1, "isScreenOn"    # Z
    .param p2, "keyCode"    # I
    .param p3, "down"    # Z
    .param p4, "event"    # Landroid/view/KeyEvent;

    .line 1624
    if-nez p1, :cond_3

    iget v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressVolumeDownBehavior:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1627
    const/4 v0, 0x0

    .line 1628
    .local v0, "keyIntent":Landroid/content/Intent;
    const/16 v1, 0x18

    if-eq p2, v1, :cond_1

    const/16 v1, 0x19

    if-ne p2, v1, :cond_0

    goto :goto_0

    .line 1632
    :cond_0
    if-eqz p3, :cond_2

    const/16 v1, 0x1a

    if-ne p2, v1, :cond_2

    .line 1634
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.KEYCODE_POWER_UP"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    .line 1631
    :cond_1
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "miui.intent.action.CAMERA_KEY_BUTTON"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 1636
    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    .line 1638
    const-string v1, "com.android.camera"

    const-string v2, "com.android.camera.snap.SnapKeyReceiver"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1640
    const-string v1, "key_code"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1641
    const-string v1, "key_action"

    invoke-virtual {p4}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1642
    const-string v1, "key_event_time"

    invoke-virtual {p4}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1643
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1646
    .end local v0    # "keyIntent":Landroid/content/Intent;
    :cond_3
    return-void
.end method

.method private takeScreenshot(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "function"    # Ljava/lang/String;

    .line 1839
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0, p2, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/String;Landroid/content/Intent;)V

    .line 1843
    const-string v2, "capture_delay"

    const-wide/16 v3, 0x0

    invoke-virtual {p1, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1839
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1845
    return-void
.end method

.method private trackDumpLogKeyCode(Landroid/view/KeyEvent;)V
    .locals 11
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 2916
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 2917
    .local v0, "code":I
    const/16 v1, 0x18

    const/4 v2, 0x0

    const/16 v3, 0x19

    if-eq v0, v3, :cond_0

    if-eq v0, v1, :cond_0

    .line 2918
    iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z

    .line 2919
    return-void

    .line 2922
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v4

    .line 2923
    .local v4, "inputDevice":Landroid/view/InputDevice;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/view/InputDevice;->isExternal()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2924
    iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z

    .line 2925
    return-void

    .line 2928
    :cond_1
    iget-boolean v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z

    const/4 v6, 0x1

    if-nez v5, :cond_2

    if-ne v0, v1, :cond_2

    .line 2929
    iput-boolean v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z

    .line 2930
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeStartTime:J

    .line 2931
    iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeLastKeyCode:I

    .line 2932
    iput v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeVolumeDownTimes:I

    goto :goto_1

    .line 2933
    :cond_2
    if-eqz v5, :cond_7

    .line 2934
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v7

    iget-wide v9, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeStartTime:J

    sub-long/2addr v7, v9

    .line 2935
    .local v7, "timeDelta":J
    iget v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeTimeOut:I

    int-to-long v9, v5

    cmp-long v5, v7, v9

    if-gez v5, :cond_5

    iget v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeLastKeyCode:I

    if-ne v0, v5, :cond_3

    goto :goto_0

    .line 2945
    :cond_3
    iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeLastKeyCode:I

    .line 2946
    if-ne v0, v3, :cond_4

    .line 2947
    iget v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeVolumeDownTimes:I

    add-int/2addr v1, v6

    iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeVolumeDownTimes:I

    .line 2949
    :cond_4
    iget v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeVolumeDownTimes:I

    const/4 v3, 0x3

    if-ne v1, v3, :cond_7

    .line 2950
    iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z

    .line 2951
    const-string v1, "DumpLog triggered"

    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 2952
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 2936
    :cond_5
    :goto_0
    iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z

    .line 2937
    if-ne v0, v1, :cond_6

    .line 2938
    iput-boolean v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z

    .line 2939
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeStartTime:J

    .line 2940
    iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeLastKeyCode:I

    .line 2941
    iput v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeVolumeDownTimes:I

    .line 2943
    :cond_6
    return-void

    .line 2959
    .end local v7    # "timeDelta":J
    :cond_7
    :goto_1
    return-void
.end method


# virtual methods
.method protected abstract callSuperInterceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I
.end method

.method protected abstract cancelPreloadRecentAppsInternal()V
.end method

.method checkProcessRunning(Ljava/lang/String;)Z
    .locals 6
    .param p1, "processName"    # Ljava/lang/String;

    .line 2683
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 2684
    .local v0, "am":Landroid/app/ActivityManager;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 2685
    return v1

    .line 2688
    :cond_0
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    .line 2690
    .local v2, "procs":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-nez v2, :cond_1

    .line 2691
    return v1

    .line 2694
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 2695
    .local v4, "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v5, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2696
    const/4 v1, 0x1

    return v1

    .line 2698
    .end local v4    # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_2
    goto :goto_0

    .line 2700
    :cond_3
    return v1
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 3043
    invoke-super {p0, p1, p2, p3}, Lcom/android/server/policy/PhoneWindowManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 3044
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "BaseMiuiPhoneWindowManager"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3045
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 3046
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mInputMethodWindowVisibleHeight="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mInputMethodWindowVisibleHeight:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 3047
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mFrontFingerprintSensor="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFrontFingerprintSensor:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3048
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSupportTapFingerprintSensorToHome="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSupportTapFingerprintSensorToHome:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3049
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mScreenOffReason="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenOffReason:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 3050
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mIsStatusBarVisibleInFullscreen="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsStatusBarVisibleInFullscreen:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3051
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mScreenRecorderEnabled="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenRecorderEnabled:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3052
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mVoiceAssistEnabled="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVoiceAssistEnabled:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3053
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mWifiOnly="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWifiOnly:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3054
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "KEYCODE_MENU KeyBitmask="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const/16 v0, 0x52

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3055
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "KEYCODE_APP_SWITCH KeyBitmask="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const/16 v0, 0xbb

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3056
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "KEYCODE_HOME KeyBitmask="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3057
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "KEYCODE_BACK KeyBitmask="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3058
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "KEYCODE_POWER KeyBitmask="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const/16 v0, 0x1a

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3059
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "KEYCODE_VOLUME_DOWN KeyBitmask="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const/16 v0, 0x19

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3060
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "KEYCODE_VOLUME_UP KeyBitmask="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const/16 v0, 0x18

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyBitmask(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3061
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "ElSE KEYCODE KeyBitmask="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3062
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "SHORTCUT_HOME_POWER="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    sget v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SHORTCUT_HOME_POWER:I

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3063
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "SHORTCUT_BACK_POWER="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    sget v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SHORTCUT_BACK_POWER:I

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3064
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "SHORTCUT_MENU_POWER="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    sget v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SHORTCUT_MENU_POWER:I

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3065
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "SHORTCUT_SCREENSHOT_ANDROID="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    sget v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SHORTCUT_SCREENSHOT_ANDROID:I

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3066
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "SHORTCUT_SCREENSHOT_MIUI="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    sget v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SHORTCUT_SCREENSHOT_MIUI:I

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3067
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "SHORTCUT_UNLOCK="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    sget v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->SHORTCUT_UNLOCK:I

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3068
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mDpadCenterDown="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDpadCenterDown:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3069
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHomeDownAfterDpCenter="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDownAfterDpCenter:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3070
    const-string v0, "    "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "KeyResponseSetting"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3071
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCurrentUserId="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCurrentUserId:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 3072
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mMikeymodeEnabled="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMikeymodeEnabled:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3073
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCameraKeyWakeScreen="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCameraKeyWakeScreen:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3074
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTrackballWakeScreen="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackballWakeScreen:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3075
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTestModeEnabled="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3077
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mScreenButtonsDisabled="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAutoDisableScreenButtonsManager:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    invoke-virtual {v0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->isScreenButtonsDisabled()Z

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3078
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mVolumeButtonPrePressedTime="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeButtonPrePressedTime:J

    invoke-virtual {p2, v0, v1}, Ljava/io/PrintWriter;->println(J)V

    .line 3079
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mVolumeButtonPressedCount="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeButtonPressedCount:J

    invoke-virtual {p2, v0, v1}, Ljava/io/PrintWriter;->println(J)V

    .line 3080
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHaveBankCard="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveBankCard:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3081
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHaveTranksCard="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveTranksCard:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3082
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mLongPressVolumeDownBehavior="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressVolumeDownBehavior:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 3083
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mIsVRMode="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsVRMode:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3084
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTalkBackIsOpened="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTalkBackIsOpened:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3085
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mShortcutServiceIsTalkBack="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mShortcutServiceIsTalkBack:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 3086
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mImperceptiblePowerKey="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mImperceptiblePowerKey:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3087
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSmartCoverManager:Lmiui/util/SmartCoverManager;

    invoke-virtual {v0, p1, p2}, Lmiui/util/SmartCoverManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 3088
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiThreeGestureListener:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 3089
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKnockGestureService:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 3090
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiBackTapGestureService:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/MiuiBackTapGestureService;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 3091
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 3092
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiFingerPrintTapListener:Lcom/miui/server/input/MiuiFingerPrintTapListener;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 3093
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiTimeFloatingWindow:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 3094
    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isMiPad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3095
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 3096
    invoke-static {}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->getInstance()Lcom/android/server/input/KeyboardCombinationManagerStubImpl;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 3098
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    if-eqz v0, :cond_1

    .line 3099
    invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 3100
    invoke-static {}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->getInstance()Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 3102
    :cond_1
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    if-eqz v0, :cond_2

    .line 3103
    invoke-interface {v0, p1, p2}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 3105
    :cond_2
    invoke-static {}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getInstance()Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 3106
    return-void
.end method

.method public enableScreenAfterBoot()V
    .locals 2

    .line 2744
    invoke-super {p0}, Lcom/android/server/policy/PhoneWindowManager;->enableScreenAfterBoot()V

    .line 2745
    const-string v0, "ro.radio.noril"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWifiOnly:Z

    .line 2746
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWifiOnly:Z

    invoke-virtual {v0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->setWifiOnly(Z)V

    .line 2747
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSmartCoverManager:Lmiui/util/SmartCoverManager;

    invoke-virtual {v0}, Lmiui/util/SmartCoverManager;->enableLidAfterBoot()V

    .line 2748
    return-void
.end method

.method protected abstract finishActivityInternal(Landroid/os/IBinder;ILandroid/content/Intent;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public finishLayoutLw(Lcom/android/server/wm/DisplayFrames;Landroid/graphics/Rect;I)V
    .locals 5
    .param p1, "displayFrames"    # Lcom/android/server/wm/DisplayFrames;
    .param p2, "inputMethodRegion"    # Landroid/graphics/Rect;
    .param p3, "displayId"    # I

    .line 2751
    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    iget v1, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    .line 2752
    .local v0, "inputMethodHeight":I
    iget v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mInputMethodWindowVisibleHeight:I

    if-eq v1, v0, :cond_0

    .line 2753
    iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mInputMethodWindowVisibleHeight:I

    .line 2754
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "input method visible height changed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "WindowManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2755
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKnockGestureService:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-virtual {v1, p2}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setInputMethodRect(Landroid/graphics/Rect;)V

    .line 2756
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda6;

    invoke-direct {v2, p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2757
    new-instance v1, Landroid/content/Intent;

    const-string v2, "miui.intent.action.INPUT_METHOD_VISIBLE_HEIGHT_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2758
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "miui.intent.extra.input_method_visible_height"

    iget v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mInputMethodWindowVisibleHeight:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2760
    const-string v2, "miui.permission.USE_INTERNAL_GENERAL_API"

    invoke-virtual {p0, v1, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 2762
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    iget v1, p1, Lcom/android/server/wm/DisplayFrames;->mWidth:I

    iget v2, p1, Lcom/android/server/wm/DisplayFrames;->mHeight:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 2765
    .local v1, "displayWidth":I
    iget-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsFoldChanged:Z

    if-eqz v2, :cond_1

    if-nez p3, :cond_1

    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mEdgeSuppressionManager:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    .line 2766
    invoke-virtual {v2}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->getScreenWidth()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 2767
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsFoldChanged:Z

    .line 2768
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mEdgeSuppressionManager:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    const-string v3, "configuration"

    iget-boolean v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFolded:Z

    invoke-virtual {v2, v3, v4, p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->handleEdgeModeChange(Ljava/lang/String;ZLcom/android/server/wm/DisplayFrames;)V

    .line 2771
    :cond_1
    return-void
.end method

.method public finishedGoingToSleep(II)V
    .locals 1
    .param p1, "displayGroupId"    # I
    .param p2, "why"    # I

    .line 683
    invoke-virtual {p0, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->screenTurnedOffInternal(I)V

    .line 684
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiBackTapGestureService:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-virtual {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->notifyScreenOff()V

    .line 685
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mEdgeSuppressionManager:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->finishedGoingToSleep()V

    .line 686
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->releaseScreenOnProximitySensor(Z)V

    .line 687
    sget-boolean v0, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 688
    invoke-static {}, Lcom/android/server/display/DisplayManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/display/DisplayManagerServiceStub;->finishedGoingToSleep()V

    .line 690
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager;->finishedGoingToSleep(II)V

    .line 691
    return-void
.end method

.method public finishedWakingUp(II)V
    .locals 1
    .param p1, "displayGroupId"    # I
    .param p2, "why"    # I

    .line 695
    invoke-super {p0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager;->finishedWakingUp(II)V

    .line 696
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mEdgeSuppressionManager:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->finishedWakingUp()V

    .line 697
    return-void
.end method

.method protected abstract forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public getFocusedWindow()Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    .locals 1

    .line 2838
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    return-object v0
.end method

.method protected getForbidFullScreenFlag()Z
    .locals 1

    .line 2784
    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mForbidFullScreen:Z

    return v0
.end method

.method public getKeyguardActive()Z
    .locals 1

    .line 2827
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    .line 2828
    invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    .line 2829
    invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    .line 2827
    :goto_1
    return v0
.end method

.method protected abstract getKeyguardWindowState()Lcom/android/server/policy/WindowManagerPolicy$WindowState;
.end method

.method public getOriginalPowerKeyRule()Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;
    .locals 1

    .line 1611
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPowerKeyRule:Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;

    return-object v0
.end method

.method protected abstract getWakePolicyFlag()I
.end method

.method public hideBootMessages()V
    .locals 2

    .line 2669
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$15;

    invoke-direct {v1, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$15;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2680
    return-void
.end method

.method protected initInternal(Landroid/content/Context;Lcom/android/server/policy/WindowManagerPolicy$WindowManagerFuncs;Landroid/view/IWindowManager;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "windowManagerFuncs"    # Lcom/android/server/policy/WindowManagerPolicy$WindowManagerFuncs;
    .param p3, "windowManager"    # Landroid/view/IWindowManager;

    .line 345
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 346
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x10501fb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mNavBarWidth:I

    .line 347
    const v1, 0x10501f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mNavBarHeight:I

    .line 348
    const v1, 0x10501f8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mNavBarHeightLand:I

    .line 349
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/util/ITouchFeature;->hasSupportGlobalTouchDirection()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsSupportGloablTounchDirection:Z

    .line 350
    if-nez v1, :cond_0

    sget-boolean v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z

    if-eqz v1, :cond_1

    .line 352
    :cond_0
    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->handleTouchFeatureRotationWatcher()V

    .line 354
    :cond_1
    new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H-IA;)V

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    .line 355
    const-class v1, Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/input/MiuiInputManagerInternal;

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    .line 356
    invoke-static {p1}, Lmiui/hardware/input/InputFeature;->getInstance(Landroid/content/Context;)Lmiui/hardware/input/InputFeature;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mInputFeature:Lmiui/hardware/input/InputFeature;

    .line 357
    invoke-virtual {v1}, Lmiui/hardware/input/InputFeature;->init()V

    .line 358
    new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;

    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSettingsObserver:Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;

    .line 359
    invoke-static {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mEdgeSuppressionManager:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    .line 361
    invoke-static {p1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 362
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSingleKeyGestureDetector:Lcom/android/server/policy/SingleKeyGestureDetector;

    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyCombinationManager:Lcom/android/server/policy/KeyCombinationManager;

    iget-object v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1, v2, v3, v4}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getInstance(Landroid/content/Context;Lcom/android/server/policy/SingleKeyGestureDetector;Lcom/android/server/policy/KeyCombinationManager;Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    .line 364
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mShortcutOneTrackHelper:Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    .line 365
    invoke-static {}, Lcom/miui/server/input/stylus/MiuiStylusUtils;->supportStylusGesture()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 366
    invoke-static {}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->getInstance()Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    .line 368
    :cond_2
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getInstance(Landroid/content/Context;)Lcom/android/server/policy/MiuiKeyInterceptExtend;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyInterceptExtend:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    .line 369
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSettingsObserver:Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;

    invoke-virtual {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->observe()V

    .line 371
    sget-object v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->phoneWindowManagerFeature:Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;

    new-instance v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$1;

    invoke-direct {v2, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$1;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    invoke-virtual {v1, p0, v2}, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;->setPowerLongPress(Lcom/android/server/policy/PhoneWindowManager;Ljava/lang/Runnable;)V

    .line 377
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    const-string v2, "PhoneWindowManager.mVolumeKeyWakeLock"

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 379
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    const-string v2, "PhoneWindowManager.mAiKeyWakeLock"

    invoke-virtual {v1, v3, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAiKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 382
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    const-string v2, "PhoneWindowManager.mHelpKeyWakeLock"

    invoke-virtual {v1, v3, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHelpKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 384
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    const v2, 0x3000001a

    const-string v3, "Reset"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeKeyUpWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 386
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 387
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.CAPTURE_SCREENSHOT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 388
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenshotReceiver:Landroid/content/BroadcastReceiver;

    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string v6, "miui.permission.USE_INTERNAL_GENERAL_API"

    const/4 v7, 0x0

    const/4 v8, 0x2

    move-object v2, p1

    move-object v5, v1

    invoke-virtual/range {v2 .. v8}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent;

    .line 391
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    move-object v1, v2

    .line 392
    const-string v2, "android.intent.action.CAPTURE_PARTIAL_SCREENSHOT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 393
    iget-object v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPartialScreenshotReceiver:Landroid/content/BroadcastReceiver;

    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string v7, "miui.permission.USE_INTERNAL_GENERAL_API"

    const/4 v8, 0x0

    const/4 v9, 0x2

    move-object v3, p1

    move-object v6, v1

    invoke-virtual/range {v3 .. v9}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent;

    .line 396
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    move-object v1, v2

    .line 397
    const-string v2, "com.miui.app.ExtraStatusBarManager.EXIT_FULLSCREEN"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 398
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mStatusBarExitFullscreenReceiver:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x2

    invoke-virtual {p1, v2, v1, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 401
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    move-object v1, v2

    .line 402
    const-string v2, "miui.intent.SCREEN_RECORDER_ENABLE_KEYEVENT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 403
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenRecordeEnablekKeyEventReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v2, v1, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 406
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    move-object v1, v2

    .line 407
    const-string v2, "com.miui.app.ExtraStatusBarManager.action_enter_drive_mode"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 408
    const-string v2, "com.miui.app.ExtraStatusBarManager.action_leave_drive_mode"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 409
    iget-object v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mInternalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    sget-object v6, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string v8, "miui.permission.USE_INTERNAL_GENERAL_API"

    iget-object v9, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    const/4 v10, 0x2

    move-object v4, p1

    move-object v7, v1

    invoke-virtual/range {v4 .. v10}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent;

    .line 415
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    move-object v1, v2

    .line 416
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 417
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mBootCompleteReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v2, v1, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 419
    new-instance v2, Lmiui/util/HapticFeedbackUtil;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v3}, Lmiui/util/HapticFeedbackUtil;-><init>(Landroid/content/Context;Z)V

    iput-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    .line 420
    new-instance v2, Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p1, v3}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAutoDisableScreenButtonsManager:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    .line 421
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSmartCoverManager:Lmiui/util/SmartCoverManager;

    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    iget-object v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, p1, v3, v4}, Lmiui/util/SmartCoverManager;->init(Landroid/content/Context;Landroid/os/PowerManager;Landroid/os/Handler;)V

    .line 422
    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->saveWindowTypeLayer(Landroid/content/Context;)V

    .line 423
    new-instance v2, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiTimeFloatingWindow:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

    .line 424
    new-instance v2, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWakeUpContentCatcherManager:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    .line 425
    return-void
.end method

.method protected intercept(Landroid/view/KeyEvent;IZI)I
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "policyFlags"    # I
    .param p3, "isScreenOn"    # Z
    .param p4, "expectedResult"    # I

    .line 1050
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1052
    .local v0, "down":Z
    :goto_0
    if-nez v0, :cond_1

    .line 1053
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->cancelEventAndCallSuperQueueing(Landroid/view/KeyEvent;IZ)V

    .line 1055
    :cond_1
    return p4
.end method

.method public interceptKeyBeforeDispatching(Landroid/os/IBinder;Landroid/view/KeyEvent;I)J
    .locals 26
    .param p1, "focusedToken"    # Landroid/os/IBinder;
    .param p2, "event"    # Landroid/view/KeyEvent;
    .param p3, "policyFlags"    # I

    .line 729
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    const-wide/16 v2, -0x1

    .line 730
    .local v2, "key_consumed":J
    const-wide/16 v4, 0x0

    .line 732
    .local v4, "key_not_consumed":J
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v6

    .line 733
    .local v6, "repeatCount":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v7

    const/4 v8, 0x1

    if-nez v7, :cond_0

    move v7, v8

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    .line 734
    .local v7, "down":Z
    :goto_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v10

    .line 735
    .local v10, "canceled":Z
    iget-object v11, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    if-eqz v11, :cond_1

    invoke-virtual {v11}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z

    move-result v11

    if-eqz v11, :cond_1

    move v11, v8

    goto :goto_1

    :cond_1
    const/4 v11, 0x0

    .line 737
    .local v11, "keyguardActive":Z
    :goto_1
    iget-object v12, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyCombinationManager:Lcom/android/server/policy/KeyCombinationManager;

    invoke-virtual {v12, v1}, Lcom/android/server/policy/KeyCombinationManager;->isKeyConsumed(Landroid/view/KeyEvent;)Z

    move-result v12

    if-eqz v12, :cond_2

    iget-object v12, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 738
    invoke-virtual {v12, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->skipKeyGesutre(Landroid/view/KeyEvent;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 739
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "is key consumed,keyCode="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " downTime="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 740
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v13

    invoke-virtual {v8, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 739
    invoke-static {v8}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 741
    const-wide/16 v8, -0x1

    return-wide v8

    .line 744
    :cond_2
    iget-object v12, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyInterceptExtend:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    iget-object v13, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    move/from16 v14, p3

    invoke-virtual {v12, v1, v14, v13}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getKeyInterceptTypeBeforeDispatching(Landroid/view/KeyEvent;ILcom/android/server/policy/WindowManagerPolicy$WindowState;)I

    move-result v12

    .line 746
    .local v12, "interceptType":I
    const-wide/16 v17, 0x0

    if-ne v12, v8, :cond_3

    .line 747
    const-string v8, "pass the key to app!"

    invoke-static {v8}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 748
    return-wide v17

    .line 749
    :cond_3
    const/4 v13, 0x2

    if-ne v12, v13, :cond_4

    .line 750
    const-string/jumbo v8, "send the key to aosp!"

    invoke-static {v8}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 751
    invoke-super/range {p0 .. p3}, Lcom/android/server/policy/PhoneWindowManager;->interceptKeyBeforeDispatching(Landroid/os/IBinder;Landroid/view/KeyEvent;I)J

    move-result-wide v8

    return-wide v8

    .line 752
    :cond_4
    const/4 v13, 0x4

    if-ne v12, v13, :cond_5

    .line 753
    const-wide/16 v8, -0x1

    return-wide v8

    .line 757
    :cond_5
    iget-object v15, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWindowManager:Landroid/view/IWindowManager;

    check-cast v15, Lcom/android/server/wm/WindowManagerService;

    move-object/from16 v13, p1

    invoke-static {v15, v13}, Lcom/android/server/wm/WindowStateStubImpl;->getWinStateFromInputWinMap(Lcom/android/server/wm/WindowManagerService;Landroid/os/IBinder;)Lcom/android/server/wm/WindowState;

    move-result-object v15

    .line 758
    .local v15, "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    if-eqz v7, :cond_6

    if-nez v6, :cond_6

    .line 759
    iput-object v15, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWin:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 762
    :cond_6
    const/16 v16, 0x0

    .line 763
    .local v16, "cancelKeyFunction":Z
    iget-object v9, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v9, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->interceptKeyByLongPress(Landroid/view/KeyEvent;)Z

    move-result v9

    const/high16 v21, 0x1000000

    if-eqz v9, :cond_7

    .line 764
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v9

    and-int v9, v9, v21

    if-eqz v9, :cond_8

    :cond_7
    if-eqz v10, :cond_9

    .line 765
    :cond_8
    const/16 v16, 0x1

    move/from16 v9, v16

    goto :goto_2

    .line 764
    :cond_9
    move/from16 v9, v16

    .line 767
    .end local v16    # "cancelKeyFunction":Z
    .local v9, "cancelKeyFunction":Z
    :goto_2
    move-wide/from16 v22, v2

    .end local v2    # "key_consumed":J
    .local v22, "key_consumed":J
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v8, :cond_a

    .line 768
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v2

    and-int v2, v2, v21

    if-nez v2, :cond_a

    .line 769
    iget-object v2, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v2}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->resetKeyCodeByLongPress()V

    .line 772
    :cond_a
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    .line 774
    .local v2, "keyCode":I
    const/16 v3, 0x52

    if-ne v2, v3, :cond_15

    .line 775
    iget-boolean v3, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z

    if-eqz v3, :cond_b

    .line 776
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Ignoring MENU because mTestModeEnabled = "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v8, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 777
    return-wide v17

    .line 780
    :cond_b
    invoke-direct {v0, v15}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isLockDeviceWindow(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 781
    const-string v3, "device locked, pass MENU to lock window"

    invoke-static {v3}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 782
    return-wide v17

    .line 785
    :cond_c
    iget-object v3, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isPressToAppSwitch()Z

    move-result v3

    const-string v8, "is show menu"

    if-nez v3, :cond_d

    .line 786
    invoke-static {v8}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 787
    return-wide v17

    .line 790
    :cond_d
    if-eqz v9, :cond_f

    .line 791
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->keyguardOn()Z

    move-result v3

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isPressToAppSwitch()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 792
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->cancelPreloadRecentAppsInternal()V

    .line 794
    :cond_e
    const-string v3, "Ignoring MENU; event canceled or intercepted by long press"

    invoke-static {v3}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;)V

    .line 795
    const-wide/16 v16, -0x1

    return-wide v16

    .line 799
    :cond_f
    move-object v3, v15

    .end local v15    # "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    .local v3, "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    iget-object v15, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v15}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isPressToAppSwitch()Z

    move-result v15

    if-eqz v15, :cond_14

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v15

    and-int v15, v15, v21

    if-eqz v15, :cond_10

    move-wide/from16 v24, v4

    goto :goto_4

    .line 805
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->keyguardOn()Z

    move-result v8

    if-nez v8, :cond_13

    .line 806
    if-eqz v7, :cond_11

    .line 807
    invoke-direct/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendFullScreenStateToTaskSnapshot()V

    .line 808
    invoke-direct/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->preloadRecentApps()V

    move-wide/from16 v24, v4

    goto :goto_3

    .line 809
    :cond_11
    if-nez v6, :cond_12

    .line 811
    iget-object v8, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mShortcutOneTrackHelper:Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    const-string v15, "screen_key_press_app_switch"

    move-wide/from16 v24, v4

    .end local v4    # "key_not_consumed":J
    .local v24, "key_not_consumed":J
    const-string v4, "launch_recents"

    invoke-virtual {v8, v15, v4}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchRecentPanel()Z

    goto :goto_3

    .line 817
    .end local v24    # "key_not_consumed":J
    .restart local v4    # "key_not_consumed":J
    :cond_12
    move-wide/from16 v24, v4

    .end local v4    # "key_not_consumed":J
    .restart local v24    # "key_not_consumed":J
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->cancelPreloadRecentAppsInternal()V

    goto :goto_3

    .line 805
    .end local v24    # "key_not_consumed":J
    .restart local v4    # "key_not_consumed":J
    :cond_13
    move-wide/from16 v24, v4

    .line 820
    .end local v4    # "key_not_consumed":J
    .restart local v24    # "key_not_consumed":J
    :goto_3
    const-wide/16 v4, -0x1

    return-wide v4

    .line 799
    .end local v24    # "key_not_consumed":J
    .restart local v4    # "key_not_consumed":J
    :cond_14
    move-wide/from16 v24, v4

    .line 801
    .end local v4    # "key_not_consumed":J
    .restart local v24    # "key_not_consumed":J
    :goto_4
    invoke-static {v8}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 802
    return-wide v17

    .line 824
    .end local v3    # "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    .end local v24    # "key_not_consumed":J
    .restart local v4    # "key_not_consumed":J
    .restart local v15    # "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    :cond_15
    move-wide/from16 v24, v4

    move-object v3, v15

    .end local v4    # "key_not_consumed":J
    .end local v15    # "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    .restart local v3    # "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    .restart local v24    # "key_not_consumed":J
    const/4 v4, 0x3

    if-ne v2, v4, :cond_24

    .line 825
    iget-boolean v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z

    if-eqz v4, :cond_16

    .line 826
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Ignoring HOME because mTestModeEnabled = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 827
    return-wide v17

    .line 830
    :cond_16
    invoke-direct {v0, v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isLockDeviceWindow(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 831
    const-string v4, "device locked, pass HOME to lock window"

    invoke-static {v4}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 832
    return-wide v17

    .line 835
    :cond_17
    if-eqz v9, :cond_18

    .line 836
    const-string v4, "Ignoring HOME; event canceled or intercepted by long press."

    invoke-static {v4}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;)V

    .line 837
    const-wide/16 v4, -0x1

    return-wide v4

    .line 840
    :cond_18
    const-wide/16 v4, -0x1

    if-nez v7, :cond_1e

    .line 841
    iget-boolean v15, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeConsumed:Z

    if-eqz v15, :cond_19

    .line 842
    const/4 v8, 0x0

    iput-boolean v8, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeConsumed:Z

    .line 843
    return-wide v4

    .line 845
    :cond_19
    sget-object v4, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->phoneWindowManagerFeature:Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;

    invoke-virtual {v4, v0}, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;->isScreenOnFully(Lcom/android/server/policy/PhoneWindowManager;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 846
    if-eqz v11, :cond_1a

    .line 848
    iget-object v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "statusbar"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/StatusBarManager;

    .line 849
    .local v4, "sbm":Landroid/app/StatusBarManager;
    invoke-virtual {v4}, Landroid/app/StatusBarManager;->collapsePanels()V

    .line 851
    .end local v4    # "sbm":Landroid/app/StatusBarManager;
    :cond_1a
    iget-boolean v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyguardOnWhenHomeDown:Z

    if-nez v4, :cond_1c

    .line 852
    iget-object v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    iget v4, v4, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDoubleTapOnHomeBehavior:I

    if-eqz v4, :cond_1b

    if-nez v11, :cond_1b

    .line 853
    iget-object v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v5, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDoubleTapTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 854
    iput-boolean v8, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDoubleTapPending:Z

    .line 855
    iget-object v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v5, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDoubleTapTimeoutRunnable:Ljava/lang/Runnable;

    move/from16 v19, v10

    move/from16 v20, v11

    .end local v10    # "canceled":Z
    .end local v11    # "keyguardActive":Z
    .local v19, "canceled":Z
    .local v20, "keyguardActive":Z
    const-wide/16 v10, 0x12c

    invoke-virtual {v4, v5, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 856
    const-wide/16 v4, -0x1

    return-wide v4

    .line 852
    .end local v19    # "canceled":Z
    .end local v20    # "keyguardActive":Z
    .restart local v10    # "canceled":Z
    .restart local v11    # "keyguardActive":Z
    :cond_1b
    move/from16 v19, v10

    move/from16 v20, v11

    .line 858
    .end local v10    # "canceled":Z
    .end local v11    # "keyguardActive":Z
    .restart local v19    # "canceled":Z
    .restart local v20    # "keyguardActive":Z
    const-string v4, "before go to home"

    invoke-static {v4}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;)V

    .line 859
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.miui.launch_home_from_hotkey"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 860
    .local v4, "intent":Landroid/content/Intent;
    const-string v5, "miui.permission.USE_INTERNAL_GENERAL_API"

    invoke-virtual {v0, v4, v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 861
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchHomeFromHotKey(I)V

    .line 862
    .end local v4    # "intent":Landroid/content/Intent;
    goto :goto_6

    .line 863
    .end local v19    # "canceled":Z
    .end local v20    # "keyguardActive":Z
    .restart local v10    # "canceled":Z
    .restart local v11    # "keyguardActive":Z
    :cond_1c
    move/from16 v19, v10

    move/from16 v20, v11

    .end local v10    # "canceled":Z
    .end local v11    # "keyguardActive":Z
    .restart local v19    # "canceled":Z
    .restart local v20    # "keyguardActive":Z
    const-string v4, "Ignoring HOME; keyguard is on when first Home down"

    invoke-static {v4}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    goto :goto_6

    .line 845
    .end local v19    # "canceled":Z
    .end local v20    # "keyguardActive":Z
    .restart local v10    # "canceled":Z
    .restart local v11    # "keyguardActive":Z
    :cond_1d
    move/from16 v19, v10

    move/from16 v20, v11

    .end local v10    # "canceled":Z
    .end local v11    # "keyguardActive":Z
    .restart local v19    # "canceled":Z
    .restart local v20    # "keyguardActive":Z
    goto :goto_6

    .line 868
    .end local v19    # "canceled":Z
    .end local v20    # "keyguardActive":Z
    .restart local v10    # "canceled":Z
    .restart local v11    # "keyguardActive":Z
    :cond_1e
    move/from16 v19, v10

    move/from16 v20, v11

    .end local v10    # "canceled":Z
    .end local v11    # "keyguardActive":Z
    .restart local v19    # "canceled":Z
    .restart local v20    # "keyguardActive":Z
    invoke-direct/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendFullScreenStateToHome()V

    .line 871
    if-eqz v3, :cond_1f

    const-string v4, "getAttrs"

    const/4 v5, 0x0

    new-array v10, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v10}, Lcom/android/server/input/ReflectionUtils;->callPrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager$LayoutParams;

    goto :goto_5

    :cond_1f
    const/4 v4, 0x0

    .line 872
    .local v4, "attrs":Landroid/view/WindowManager$LayoutParams;
    :goto_5
    if-eqz v4, :cond_21

    .line 873
    iget v5, v4, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 874
    .local v5, "type":I
    const/16 v10, 0x7d4

    if-eq v5, v10, :cond_20

    const/16 v10, 0x7d9

    if-ne v5, v10, :cond_21

    .line 877
    :cond_20
    return-wide v17

    .line 882
    .end local v4    # "attrs":Landroid/view/WindowManager$LayoutParams;
    .end local v5    # "type":I
    :cond_21
    :goto_6
    if-nez v6, :cond_23

    .line 883
    iget-boolean v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDoubleTapPending:Z

    if-eqz v4, :cond_22

    .line 884
    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDoubleTapPending:Z

    .line 885
    iget-object v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v5, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDoubleTapTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 886
    invoke-direct/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->handleDoubleTapOnHome()V

    goto :goto_7

    .line 887
    :cond_22
    iget-object v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    iget v4, v4, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDoubleTapOnHomeBehavior:I

    if-ne v4, v8, :cond_23

    .line 889
    invoke-direct/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->preloadRecentApps()V

    .line 892
    :cond_23
    :goto_7
    const-wide/16 v10, -0x1

    return-wide v10

    .line 894
    .end local v19    # "canceled":Z
    .end local v20    # "keyguardActive":Z
    .restart local v10    # "canceled":Z
    .restart local v11    # "keyguardActive":Z
    :cond_24
    move/from16 v19, v10

    move/from16 v20, v11

    const-wide/16 v10, -0x1

    .end local v10    # "canceled":Z
    .end local v11    # "keyguardActive":Z
    .restart local v19    # "canceled":Z
    .restart local v20    # "keyguardActive":Z
    const/4 v5, 0x4

    if-ne v2, v5, :cond_25

    .line 895
    if-eqz v9, :cond_25

    .line 896
    const-string v4, "Ignoring BACK; event canceled or intercepted by long press."

    invoke-static {v4}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;)V

    .line 897
    return-wide v10

    .line 900
    :cond_25
    const/16 v5, 0x19

    if-ne v2, v5, :cond_27

    iget-boolean v5, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyConsumed:Z

    if-eqz v5, :cond_27

    .line 901
    if-nez v7, :cond_26

    .line 902
    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyConsumed:Z

    .line 904
    :cond_26
    const-wide/16 v4, -0x1

    return-wide v4

    .line 907
    :cond_27
    const/16 v5, 0x18

    if-ne v2, v5, :cond_29

    iget-boolean v5, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyConsumed:Z

    if-eqz v5, :cond_29

    .line 908
    if-nez v7, :cond_28

    .line 909
    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyConsumed:Z

    .line 911
    :cond_28
    const-wide/16 v4, -0x1

    return-wide v4

    .line 914
    :cond_29
    iget-object v5, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    if-eqz v5, :cond_2a

    .line 915
    invoke-virtual {v5, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->needInterceptBeforeDispatching(Landroid/view/KeyEvent;)Z

    move-result v5

    if-eqz v5, :cond_2a

    .line 916
    iget-object v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-virtual {v4, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->getDelayTime(Landroid/view/KeyEvent;)J

    move-result-wide v4

    return-wide v4

    .line 919
    :cond_2a
    if-ne v12, v4, :cond_2b

    .line 920
    const-string v4, "Skip aosp key\'s logic"

    invoke-static {v4}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 921
    return-wide v17

    .line 923
    :cond_2b
    invoke-super/range {p0 .. p3}, Lcom/android/server/policy/PhoneWindowManager;->interceptKeyBeforeDispatching(Landroid/os/IBinder;Landroid/view/KeyEvent;I)J

    move-result-wide v4

    return-wide v4
.end method

.method protected interceptKeyBeforeQueueingInternal(Landroid/view/KeyEvent;IZ)I
    .locals 22
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "policyFlags"    # I
    .param p3, "isScreenOn"    # Z

    .line 1238
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move/from16 v9, p2

    move/from16 v10, p3

    iget-boolean v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemBooted:Z

    const/4 v11, 0x0

    if-nez v0, :cond_0

    .line 1240
    return v11

    .line 1243
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v12

    .line 1244
    .local v12, "keyCode":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v13, 0x1

    if-nez v0, :cond_1

    move v0, v13

    goto :goto_0

    :cond_1
    move v0, v11

    :goto_0
    move v14, v0

    .line 1245
    .local v14, "down":Z
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v15

    .line 1246
    .local v15, "repeatCount":I
    const/high16 v0, 0x1000000

    and-int/2addr v0, v9

    if-eqz v0, :cond_2

    move v0, v13

    goto :goto_1

    :cond_2
    move v0, v11

    :goto_1
    move/from16 v16, v0

    .line 1247
    .local v16, "isInjected":Z
    iget-object v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    if-eqz v0, :cond_4

    if-eqz v10, :cond_3

    .line 1248
    invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_2

    :cond_3
    nop

    .line 1249
    invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    :goto_2
    move v0, v13

    goto :goto_3

    :cond_4
    move v0, v11

    :goto_3
    move v6, v0

    .line 1251
    .local v6, "keyguardActive":Z
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "keyCode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " down:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " eventTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1252
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " downTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " policyFlags:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1253
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " flags:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1254
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " metaState:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1255
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " deviceId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1256
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isScreenOn:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " keyguardActive:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " repeatCount:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1251
    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 1258
    iget-object v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    const/16 v5, 0x1a

    invoke-virtual {v0, v5}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getSingleKeyRule(I)Lcom/android/server/policy/MiuiSingleKeyRule;

    move-result-object v17

    .line 1260
    .local v17, "miuiSingleKeyRule":Lcom/android/server/policy/MiuiSingleKeyRule;
    if-eqz v17, :cond_5

    .line 1261
    invoke-virtual/range {v17 .. v17}, Lcom/android/server/policy/MiuiSingleKeyRule;->getLongPressTimeoutMs()J

    move-result-wide v0

    invoke-static {v12, v14, v0, v1}, Landroid/os/AnrMonitor;->screenHangMonitor(IZJ)V

    .line 1266
    :cond_5
    iget-object v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyInterceptExtend:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    invoke-virtual {v0, v8, v10}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->interceptMiuiKeyboard(Landroid/view/KeyEvent;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1267
    return v11

    .line 1270
    :cond_6
    iget-object v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyInterceptExtend:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    iget-object v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    invoke-virtual {v0, v8, v9, v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getKeyInterceptTypeBeforeQueueing(Landroid/view/KeyEvent;ILcom/android/server/policy/WindowManagerPolicy$WindowState;)I

    move-result v4

    .line 1271
    .local v4, "interceptType":I
    if-ne v4, v13, :cond_7

    .line 1272
    const-string v0, "pass the key to app!"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 1273
    return v13

    .line 1274
    :cond_7
    const/4 v0, 0x2

    if-ne v4, v0, :cond_8

    .line 1275
    const-string/jumbo v0, "send the key to aosp!"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 1276
    invoke-virtual/range {p0 .. p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->callSuperInterceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I

    move-result v0

    return v0

    .line 1279
    :cond_8
    const/16 v0, 0x52

    const/4 v1, 0x4

    const/4 v3, 0x3

    if-eq v12, v0, :cond_9

    if-eq v12, v1, :cond_9

    if-eq v12, v3, :cond_9

    if-eq v12, v5, :cond_9

    .line 1281
    invoke-direct/range {p0 .. p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->notifyPowerKeeperKeyEvent(Landroid/view/KeyEvent;)V

    .line 1283
    :cond_9
    if-eqz v14, :cond_a

    .line 1284
    invoke-direct/range {p0 .. p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->trackDumpLogKeyCode(Landroid/view/KeyEvent;)V

    .line 1288
    :cond_a
    invoke-static {}, Lmiui/enterprise/ApplicationHelperStub;->getInstance()Lmiui/enterprise/IApplicationHelper;

    move-result-object v0

    invoke-interface {v0}, Lmiui/enterprise/IApplicationHelper;->isSetKeyEvent()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1289
    invoke-static {}, Lmiui/enterprise/ApplicationHelperStub;->getInstance()Lmiui/enterprise/IApplicationHelper;

    move-result-object v0

    invoke-interface {v0, v8}, Lmiui/enterprise/IApplicationHelper;->sendKeyEvent(Landroid/view/KeyEvent;)V

    .line 1293
    :cond_b
    if-nez v15, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->keyguardOn()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-direct/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isMiPad()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1294
    invoke-direct/range {p0 .. p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->handleMetaKey(Landroid/view/KeyEvent;)V

    .line 1297
    :cond_c
    invoke-direct/range {p0 .. p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isTrackInputEvenForScreenRecorder(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1298
    invoke-direct/range {p0 .. p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendKeyEventBroadcast(Landroid/view/KeyEvent;)V

    .line 1301
    :cond_d
    invoke-direct/range {p0 .. p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isTrackInputEventForVoiceAssist(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1302
    invoke-direct/range {p0 .. p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendVoiceAssistKeyEventBroadcast(Landroid/view/KeyEvent;)V

    .line 1305
    :cond_e
    if-ne v12, v1, :cond_f

    .line 1306
    invoke-direct/range {p0 .. p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendBackKeyEventBroadcast(Landroid/view/KeyEvent;)V

    .line 1309
    :cond_f
    invoke-direct/range {p0 .. p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->shouldInterceptKey(Landroid/view/KeyEvent;IZ)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1310
    return v11

    .line 1313
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->interceptPowerKeyByFingerPrintKey()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1314
    return v11

    .line 1317
    :cond_11
    invoke-virtual/range {p0 .. p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isFingerPrintKey(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1318
    invoke-virtual {v7, v8, v10}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->processFingerprintNavigationEvent(Landroid/view/KeyEvent;Z)I

    move-result v0

    return v0

    .line 1321
    :cond_12
    const/16 v0, 0x18

    if-ne v12, v0, :cond_13

    if-nez v14, :cond_13

    iget-object v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mProximitySensor:Lcom/android/server/policy/MiuiScreenOnProximityLock;

    if-eqz v1, :cond_13

    .line 1322
    invoke-virtual {v1}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1323
    invoke-direct {v7, v11}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->releaseScreenOnProximitySensor(Z)V

    .line 1326
    :cond_13
    if-ne v12, v5, :cond_16

    .line 1327
    iget-boolean v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z

    if-eqz v1, :cond_14

    .line 1328
    const-string v0, "Ignoring POWER because mTestModeEnabled is true"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 1329
    return v13

    .line 1332
    :cond_14
    if-eqz v14, :cond_15

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_15

    .line 1333
    invoke-direct/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->postPowerLongPress()V

    goto :goto_4

    .line 1335
    :cond_15
    invoke-direct/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->removePowerLongPress()V

    goto :goto_4

    .line 1338
    :cond_16
    invoke-direct/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->removePowerLongPress()V

    .line 1341
    :goto_4
    const/16 v1, 0x2b1

    move/from16 v18, v6

    .end local v6    # "keyguardActive":Z
    .local v18, "keyguardActive":Z
    const-wide/16 v5, 0x1388

    if-ne v12, v1, :cond_18

    iget-boolean v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z

    if-nez v1, :cond_18

    .line 1342
    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v0, :cond_17

    .line 1343
    invoke-direct {v7, v8, v14}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->handleAiKeyEvent(Landroid/view/KeyEvent;Z)V

    .line 1344
    invoke-virtual/range {p0 .. p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->callSuperInterceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I

    goto :goto_5

    .line 1346
    :cond_17
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1347
    .local v0, "voiceBundle":Landroid/os/Bundle;
    const-string v1, "extra_key_action"

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1348
    nop

    .line 1349
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v1

    .line 1348
    const-string v3, "extra_key_event_time"

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1350
    iget-object v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v1

    const-string v2, "launch_voice_assistant"

    const-string v3, "ai_key"

    invoke-virtual {v1, v2, v3, v0, v13}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 1353
    iget-object v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAiKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v5, v6}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 1355
    .end local v0    # "voiceBundle":Landroid/os/Bundle;
    :goto_5
    return v11

    .line 1359
    :cond_18
    const/16 v1, 0x103

    if-ne v12, v1, :cond_19

    move v1, v13

    goto :goto_6

    :cond_19
    move v1, v11

    :goto_6
    and-int/2addr v1, v14

    if-eqz v1, :cond_1a

    .line 1360
    iget-object v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    invoke-interface {v1}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.android.camera"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1a

    .line 1362
    :try_start_0
    iget-object v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHelpKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v5, v6}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 1363
    iget-object v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    const-string v1, "launch_camera"

    const-string/jumbo v2, "stabilizer"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, v13}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1368
    goto :goto_7

    .line 1366
    :catch_0
    move-exception v0

    .line 1367
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v1, "mCameraIntent problem"

    invoke-static {v1, v0}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1369
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :goto_7
    return v11

    .line 1373
    :cond_1a
    if-ne v12, v3, :cond_1c

    .line 1374
    if-eqz v14, :cond_1b

    if-nez v15, :cond_1b

    .line 1375
    iget-object v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    invoke-virtual {v1}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z

    move-result v1

    iput-boolean v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyguardOnWhenHomeDown:Z

    .line 1376
    iget-boolean v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDpadCenterDown:Z

    if-eqz v1, :cond_1b

    .line 1377
    iput-boolean v13, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDownAfterDpCenter:Z

    .line 1380
    :cond_1b
    iget-boolean v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFrontFingerprintSensor:Z

    if-eqz v1, :cond_1c

    invoke-direct/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->inFingerprintEnrolling()Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 1381
    return v11

    .line 1385
    :cond_1c
    iget-object v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyCombinationManager:Lcom/android/server/policy/KeyCombinationManager;

    invoke-virtual {v1, v8}, Lcom/android/server/policy/KeyCombinationManager;->shouldConsumedKey(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 1386
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "comsumed key,keycode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dowmTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1387
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1386
    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 1388
    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I

    move-result v0

    return v0

    .line 1391
    :cond_1d
    const/16 v1, 0x19

    const-string/jumbo v6, "zhuque"

    if-ne v12, v1, :cond_21

    .line 1392
    if-eqz v14, :cond_20

    .line 1393
    if-eqz v10, :cond_1f

    iget-boolean v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyPressed:Z

    if-nez v0, :cond_1f

    .line 1394
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v0

    and-int/lit16 v0, v0, 0x400

    if-nez v0, :cond_1e

    .line 1395
    iput-boolean v13, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyPressed:Z

    .line 1396
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v0

    iput-wide v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyTime:J

    .line 1397
    iput-boolean v11, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyConsumed:Z

    .line 1398
    move/from16 v5, v18

    .end local v18    # "keyguardActive":Z
    .local v5, "keyguardActive":Z
    invoke-direct {v7, v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->interceptAccessibilityShortcutChord(Z)V

    goto :goto_8

    .line 1394
    .end local v5    # "keyguardActive":Z
    .restart local v18    # "keyguardActive":Z
    :cond_1e
    move/from16 v5, v18

    .end local v18    # "keyguardActive":Z
    .restart local v5    # "keyguardActive":Z
    goto :goto_8

    .line 1393
    .end local v5    # "keyguardActive":Z
    .restart local v18    # "keyguardActive":Z
    :cond_1f
    move/from16 v5, v18

    .end local v18    # "keyguardActive":Z
    .restart local v5    # "keyguardActive":Z
    goto :goto_8

    .line 1401
    .end local v5    # "keyguardActive":Z
    .restart local v18    # "keyguardActive":Z
    :cond_20
    move/from16 v5, v18

    .end local v18    # "keyguardActive":Z
    .restart local v5    # "keyguardActive":Z
    iput-boolean v11, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyPressed:Z

    .line 1402
    invoke-direct/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->cancelPendingAccessibilityShortcutAction()V

    goto :goto_8

    .line 1404
    .end local v5    # "keyguardActive":Z
    .restart local v18    # "keyguardActive":Z
    :cond_21
    move/from16 v5, v18

    .end local v18    # "keyguardActive":Z
    .restart local v5    # "keyguardActive":Z
    if-ne v12, v0, :cond_25

    .line 1405
    if-eqz v14, :cond_24

    .line 1406
    if-eqz v10, :cond_22

    iget-boolean v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyPressed:Z

    if-nez v0, :cond_22

    .line 1407
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v0

    and-int/lit16 v0, v0, 0x400

    if-nez v0, :cond_22

    .line 1408
    iput-boolean v13, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyPressed:Z

    .line 1409
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v0

    iput-wide v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyTime:J

    .line 1410
    iput-boolean v11, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyConsumed:Z

    .line 1411
    invoke-direct {v7, v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->interceptAccessibilityShortcutChord(Z)V

    .line 1413
    :cond_22
    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 1414
    iget-object v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeKeyUpWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 1415
    iget-object v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeKeyUpWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1417
    :cond_23
    iget-object v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeKeyUpWakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v1, 0x2710

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 1418
    invoke-direct/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->postVolumeUpLongPress()V

    goto :goto_8

    .line 1421
    :cond_24
    iput-boolean v11, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyPressed:Z

    .line 1422
    invoke-direct/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->cancelPendingAccessibilityShortcutAction()V

    .line 1423
    invoke-direct/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->removeVolumeUpLongPress()V

    .line 1427
    :cond_25
    :goto_8
    iget-object v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mProximitySensor:Lcom/android/server/policy/MiuiScreenOnProximityLock;

    if-eqz v0, :cond_26

    sget-object v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->phoneWindowManagerFeature:Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;

    invoke-virtual {v1, v7}, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;->isScreenOnFully(Lcom/android/server/policy/PhoneWindowManager;)Z

    move-result v1

    invoke-virtual {v0, v1, v8}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->shouldBeBlocked(ZLandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 1428
    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I

    move-result v0

    return v0

    .line 1431
    :cond_26
    if-eqz v10, :cond_28

    iget-object v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAutoDisableScreenButtonsManager:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    iget-object v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 1432
    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isSingleKeyUse()Z

    move-result v0

    iget-object v2, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSmartCoverManager:Lmiui/util/SmartCoverManager;

    .line 1433
    invoke-virtual {v2}, Lmiui/util/SmartCoverManager;->getSmartCoverLidOpen()Z

    move-result v2

    if-nez v2, :cond_27

    iget-object v2, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDefaultDisplayPolicy:Lcom/android/server/wm/DisplayPolicy;

    .line 1434
    invoke-virtual {v2}, Lcom/android/server/wm/DisplayPolicy;->getLidState()I

    move-result v2

    if-nez v2, :cond_27

    move/from16 v18, v13

    goto :goto_9

    :cond_27
    move/from16 v18, v11

    .line 1431
    :goto_9
    move v2, v12

    move v3, v14

    move/from16 v20, v4

    .end local v4    # "interceptType":I
    .local v20, "interceptType":I
    move v4, v0

    move/from16 v19, v5

    const/16 v0, 0x1a

    .end local v5    # "keyguardActive":Z
    .local v19, "keyguardActive":Z
    move/from16 v5, v18

    move-object/from16 v21, v6

    move/from16 v13, v19

    .end local v19    # "keyguardActive":Z
    .local v13, "keyguardActive":Z
    move-object/from16 v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->handleDisableButtons(IZZZLandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_29

    .line 1436
    return v11

    .line 1431
    .end local v13    # "keyguardActive":Z
    .end local v20    # "interceptType":I
    .restart local v4    # "interceptType":I
    .restart local v5    # "keyguardActive":Z
    :cond_28
    move/from16 v20, v4

    move v13, v5

    move-object/from16 v21, v6

    const/16 v0, 0x1a

    .line 1439
    .end local v4    # "interceptType":I
    .end local v5    # "keyguardActive":Z
    .restart local v13    # "keyguardActive":Z
    .restart local v20    # "interceptType":I
    :cond_29
    const/16 v1, 0xe0

    if-ne v12, v1, :cond_2a

    if-nez v10, :cond_2a

    if-eqz v13, :cond_2a

    iget-object v2, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mProximitySensor:Lcom/android/server/policy/MiuiScreenOnProximityLock;

    if-eqz v2, :cond_2a

    .line 1442
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->registerProximitySensor()V

    .line 1445
    :cond_2a
    if-ne v12, v1, :cond_2b

    if-nez v10, :cond_2b

    .line 1446
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/miui/server/input/PadManager;->padLidInterceptWakeKey(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 1448
    return v11

    .line 1452
    :cond_2b
    iget-object v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    invoke-virtual {v1, v9}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->updatePolicyFlag(I)V

    .line 1454
    invoke-direct {v7, v10, v12, v14, v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->streetSnap(ZIZLandroid/view/KeyEvent;)V

    .line 1457
    iget-object v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiFingerPrintTapListener:Lcom/miui/server/input/MiuiFingerPrintTapListener;

    invoke-virtual {v1, v8, v10, v13}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->shouldInterceptSlideFpTapKey(Landroid/view/KeyEvent;ZZ)Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 1458
    return v11

    .line 1461
    :cond_2c
    iget-object v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    if-eqz v1, :cond_2d

    invoke-virtual {v1, v8}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->shouldInterceptKey(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 1463
    invoke-direct/range {p0 .. p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->cancelEventAndCallSuperQueueing(Landroid/view/KeyEvent;IZ)V

    .line 1464
    return v11

    .line 1467
    :cond_2d
    if-nez v10, :cond_32

    if-eqz v16, :cond_2e

    goto :goto_b

    .line 1473
    :cond_2e
    const/4 v1, 0x1

    .line 1474
    .local v1, "isWakeKey":Z
    const/4 v2, 0x0

    .line 1477
    .local v2, "allowToWake":Z
    sparse-switch v12, :sswitch_data_0

    .line 1487
    const/4 v1, 0x0

    goto :goto_a

    .line 1479
    :sswitch_0
    iget-boolean v2, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackballWakeScreen:Z

    .line 1480
    goto :goto_a

    .line 1483
    :sswitch_1
    iget-boolean v2, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCameraKeyWakeScreen:Z

    .line 1484
    nop

    .line 1490
    :goto_a
    if-eqz v1, :cond_33

    .line 1491
    if-eqz v2, :cond_31

    .line 1492
    if-eqz v14, :cond_2f

    .line 1494
    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I

    move-result v0

    return v0

    .line 1497
    :cond_2f
    if-eqz v13, :cond_30

    .line 1498
    iget-object v3, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    invoke-virtual {v3, v0, v11}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->onWakeKeyWhenKeyguardShowingTq(IZ)Z

    .line 1500
    :cond_30
    const/4 v0, 0x1

    invoke-virtual {v7, v8, v9, v10, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I

    move-result v0

    return v0

    .line 1503
    :cond_31
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getWakePolicyFlag()I

    move-result v0

    not-int v0, v0

    and-int/2addr v0, v9

    .end local p2    # "policyFlags":I
    .local v0, "policyFlags":I
    goto :goto_c

    .line 1469
    .end local v0    # "policyFlags":I
    .end local v1    # "isWakeKey":Z
    .end local v2    # "allowToWake":Z
    .restart local p2    # "policyFlags":I
    :cond_32
    :goto_b
    iget-boolean v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCameraKeyWakeScreen:Z

    if-eqz v0, :cond_33

    if-eqz v13, :cond_33

    const/16 v0, 0x1b

    if-ne v12, v0, :cond_33

    if-nez v14, :cond_33

    .line 1470
    const/4 v0, -0x1

    invoke-virtual {v7, v8, v9, v10, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I

    move-result v0

    return v0

    .line 1507
    :cond_33
    move v0, v9

    .end local p2    # "policyFlags":I
    .restart local v0    # "policyFlags":I
    :goto_c
    invoke-direct {v7, v12, v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->shouldInterceptHeadSetHookKey(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 1508
    return v11

    .line 1513
    :cond_34
    sget-object v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPhoneProduct:Ljava/lang/String;

    move-object/from16 v2, v21

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_35

    .line 1514
    const-string v1, "WindowManager"

    const-string v2, " sendOthersBroadcast keyevent"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1515
    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object/from16 v1, p0

    move v5, v12

    move-object/from16 v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendOthersBroadcast(ZZZILandroid/view/KeyEvent;)Z

    goto :goto_d

    .line 1518
    :cond_35
    move-object/from16 v1, p0

    move v2, v14

    move/from16 v3, p3

    move v4, v13

    move v5, v12

    move-object/from16 v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendOthersBroadcast(ZZZILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_36

    .line 1519
    invoke-virtual {v7, v8, v0, v10, v11}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I

    move-result v1

    return v1

    .line 1524
    :cond_36
    :goto_d
    invoke-direct {v7, v0, v12, v14, v15}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->playSoundEffect(IIZI)V

    .line 1525
    move/from16 v1, v20

    const/4 v2, 0x3

    .end local v20    # "interceptType":I
    .local v1, "interceptType":I
    if-ne v1, v2, :cond_39

    .line 1526
    const-string v2, "Skip aosp key\'s logic"

    invoke-static {v2}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 1527
    iget-object v2, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDefaultDisplay:Landroid/view/Display;

    invoke-virtual {v2}, Landroid/view/Display;->getState()I

    move-result v2

    invoke-static {v2}, Landroid/view/Display;->isOnState(I)Z

    move-result v2

    .line 1528
    .local v2, "isDefaultDisplayOn":Z
    const/high16 v3, 0x20000000

    and-int/2addr v3, v0

    if-eqz v3, :cond_37

    if-eqz v2, :cond_37

    const/4 v11, 0x1

    :cond_37
    move v3, v11

    .line 1530
    .local v3, "interactiveAndOn":Z
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v4

    and-int/lit16 v4, v4, 0x400

    if-nez v4, :cond_38

    iget-object v4, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyCombinationManager:Lcom/android/server/policy/KeyCombinationManager;

    .line 1531
    invoke-virtual {v4, v8, v3}, Lcom/android/server/policy/KeyCombinationManager;->interceptKey(Landroid/view/KeyEvent;Z)Z

    move-result v4

    if-eqz v4, :cond_38

    .line 1532
    iget-object v4, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSingleKeyGestureDetector:Lcom/android/server/policy/SingleKeyGestureDetector;

    invoke-virtual {v4}, Lcom/android/server/policy/SingleKeyGestureDetector;->reset()V

    .line 1534
    :cond_38
    const/4 v4, 0x1

    return v4

    .line 1536
    .end local v2    # "isDefaultDisplayOn":Z
    .end local v3    # "interactiveAndOn":Z
    :cond_39
    invoke-virtual {v7, v8, v0, v10}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->callSuperInterceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I

    move-result v2

    return v2

    nop

    :sswitch_data_0
    .sparse-switch
        0x1b -> :sswitch_1
        0x110 -> :sswitch_0
    .end sparse-switch
.end method

.method protected abstract interceptPowerKeyByFingerPrintKey()Z
.end method

.method protected abstract isFingerPrintKey(Landroid/view/KeyEvent;)Z
.end method

.method protected isInLockTaskMode()Z
    .locals 1

    .line 966
    const/4 v0, 0x0

    return v0
.end method

.method public isKeyGuardNotActive()Z
    .locals 1

    .line 2833
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    if-eqz v0, :cond_0

    .line 2834
    invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 2833
    :goto_0
    return v0
.end method

.method protected abstract isScreenOnInternal()Z
.end method

.method public launchAssistAction(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "hint"    # Ljava/lang/String;
    .param p2, "args"    # Landroid/os/Bundle;

    .line 2099
    const-string v0, "assist"

    invoke-virtual {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendCloseSystemWindows(Ljava/lang/String;)V

    .line 2100
    invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isUserSetupComplete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2102
    const/4 v0, 0x0

    return v0

    .line 2104
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 2107
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "search"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 2108
    invoke-virtual {v0, p2}, Landroid/app/SearchManager;->launchAssist(Landroid/os/Bundle;)V

    goto :goto_0

    .line 2110
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchAssistActionInternal(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2112
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method protected abstract launchAssistActionInternal(Ljava/lang/String;Landroid/os/Bundle;)V
.end method

.method public launchHome()Z
    .locals 1

    .line 2120
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchHomeFromHotKey(I)V

    .line 2121
    const/4 v0, 0x1

    return v0
.end method

.method public launchRecentPanel()Z
    .locals 1

    .line 2125
    const-string v0, "recentapps"

    invoke-virtual {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendCloseSystemWindows(Ljava/lang/String;)V

    .line 2127
    invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->keyguardOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2129
    const/4 v0, 0x0

    return v0

    .line 2132
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchRecentPanelInternal()V

    .line 2133
    const/4 v0, 0x1

    return v0
.end method

.method protected abstract launchRecentPanelInternal()V
.end method

.method public launchRecents()Z
    .locals 1

    .line 2116
    invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchRecentPanel()Z

    move-result v0

    return v0
.end method

.method public notifyLidSwitchChanged(JZ)V
    .locals 2
    .param p1, "whenNanos"    # J
    .param p3, "lidOpen"    # Z

    .line 707
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSmartCoverManager:Lmiui/util/SmartCoverManager;

    iget-boolean v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFolded:Z

    invoke-virtual {v0, p3, v1}, Lmiui/util/SmartCoverManager;->notifyLidSwitchChanged(ZZ)V

    .line 708
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    if-eqz v0, :cond_0

    .line 709
    invoke-interface {v0, p3}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->notifyLidSwitchChanged(Z)V

    .line 711
    :cond_0
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/miui/server/input/PadManager;->setIsLidOpen(Z)V

    .line 712
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    if-eqz v0, :cond_1

    .line 713
    invoke-virtual {v0, p3}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->notifyLidSwitchChanged(Z)V

    .line 715
    :cond_1
    return-void
.end method

.method public onDefaultDisplayFocusChangedLw(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V
    .locals 3
    .param p1, "newFocus"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 1983
    invoke-super {p0, p1}, Lcom/android/server/policy/PhoneWindowManager;->onDefaultDisplayFocusChangedLw(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V

    .line 1984
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1985
    .local v0, "packageName":Ljava/lang/String;
    :goto_0
    iget-boolean v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemReady:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTofManagerInternal:Lcom/android/server/tof/TofManagerInternal;

    if-eqz v1, :cond_1

    .line 1986
    invoke-virtual {v1, v0}, Lcom/android/server/tof/TofManagerInternal;->onDefaultDisplayFocusChanged(Ljava/lang/String;)V

    .line 1988
    :cond_1
    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 1989
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    if-eqz v1, :cond_2

    .line 1990
    invoke-virtual {v1, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->onDefaultDisplayFocusChangedLw(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V

    .line 1991
    invoke-static {}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->getInstance()Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->onFocusedWindowChanged(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V

    .line 1993
    :cond_2
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiThreeGestureListener:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    if-eqz v1, :cond_3

    .line 1994
    invoke-virtual {v1, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->onFocusedWindowChanged(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V

    .line 1996
    :cond_3
    if-eqz p1, :cond_4

    sget-boolean v1, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z

    if-eqz v1, :cond_4

    .line 1997
    invoke-interface {p1}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v1

    .line 1998
    .local v1, "focusedPackageName":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/display/DisplayPowerControllerStub;->getInstance()Lcom/android/server/display/DisplayPowerControllerStub;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1999
    invoke-static {}, Lcom/android/server/display/DisplayPowerControllerStub;->getInstance()Lcom/android/server/display/DisplayPowerControllerStub;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/android/server/display/DisplayPowerControllerStub;->notifyFocusedWindowChanged(Ljava/lang/String;)V

    .line 2002
    .end local v1    # "focusedPackageName":Ljava/lang/String;
    :cond_4
    const-class v1, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    .line 2003
    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    .line 2004
    .local v1, "service":Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;
    if-eqz v1, :cond_5

    .line 2005
    invoke-virtual {v1, p1}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->onFocusedWindowChanged(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V

    .line 2007
    :cond_5
    return-void
.end method

.method protected abstract onStatusBarPanelRevealed(Lcom/android/internal/statusbar/IStatusBarService;)V
.end method

.method public performHapticFeedback(ILjava/lang/String;IZLjava/lang/String;)Z
    .locals 1
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "effectId"    # I
    .param p4, "always"    # Z
    .param p5, "reason"    # Ljava/lang/String;

    .line 2229
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    invoke-virtual {v0, p3}, Lmiui/util/HapticFeedbackUtil;->isSupportedEffect(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2230
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    invoke-virtual {v0, p2, p3, p4}, Lmiui/util/HapticFeedbackUtil;->performHapticFeedback(Ljava/lang/String;IZ)Z

    move-result v0

    return v0

    .line 2233
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/android/server/policy/PhoneWindowManager;->performHapticFeedback(ILjava/lang/String;IZLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected abstract preloadRecentAppsInternal()V
.end method

.method protected abstract processFingerprintNavigationEvent(Landroid/view/KeyEvent;Z)I
.end method

.method protected registerProximitySensor()V
    .locals 2

    .line 1077
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$9;

    invoke-direct {v1, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$9;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1086
    return-void
.end method

.method protected registerStatusBarInputEventReceiver()V
    .locals 0

    .line 1777
    return-void
.end method

.method protected abstract screenOffBecauseOfProxSensor()Z
.end method

.method public screenTurnedOff(IZ)V
    .locals 0
    .param p1, "displayId"    # I
    .param p2, "isSwappingDisplay"    # Z

    .line 606
    invoke-super {p0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager;->screenTurnedOff(IZ)V

    .line 607
    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->startCameraProcess()V

    .line 608
    return-void
.end method

.method protected screenTurnedOffInternal(I)V
    .locals 0
    .param p1, "why"    # I

    .line 700
    iput p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenOffReason:I

    .line 701
    return-void
.end method

.method public screenTurningOff(ILcom/android/server/policy/WindowManagerPolicy$ScreenOffListener;)V
    .locals 1
    .param p1, "displayId"    # I
    .param p2, "screenOffListener"    # Lcom/android/server/policy/WindowManagerPolicy$ScreenOffListener;

    .line 598
    invoke-super {p0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager;->screenTurningOff(ILcom/android/server/policy/WindowManagerPolicy$ScreenOffListener;)V

    .line 599
    sget-boolean v0, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 600
    invoke-static {}, Lcom/android/server/display/DisplayManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/display/DisplayManagerServiceStub;->screenTurningOff()V

    .line 602
    :cond_0
    return-void
.end method

.method public screenTurningOn(ILcom/android/server/policy/WindowManagerPolicy$ScreenOnListener;)V
    .locals 1
    .param p1, "displayId"    # I
    .param p2, "screenOnListener"    # Lcom/android/server/policy/WindowManagerPolicy$ScreenOnListener;

    .line 586
    invoke-super {p0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager;->screenTurningOn(ILcom/android/server/policy/WindowManagerPolicy$ScreenOnListener;)V

    .line 587
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    if-eqz v0, :cond_0

    .line 588
    invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->onScreenTurnedOnWithoutListener()V

    .line 590
    :cond_0
    sget-boolean v0, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 591
    invoke-static {}, Lcom/android/server/display/DisplayManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/display/DisplayManagerServiceStub;->screenTurningOn()V

    .line 594
    :cond_1
    return-void
.end method

.method sendAsyncBroadcast(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .line 2704
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;Z)V

    .line 2705
    return-void
.end method

.method sendAsyncBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "receiverPermission"    # Ljava/lang/String;

    .line 2719
    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 2720
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$16;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$16;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/content/Intent;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2727
    :cond_0
    return-void
.end method

.method sendAsyncBroadcast(Landroid/content/Intent;Z)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isInteractive"    # Z

    .line 2708
    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemReady:Z

    if-eqz v0, :cond_1

    .line 2709
    if-eqz p2, :cond_0

    .line 2710
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 2713
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2716
    :cond_1
    :goto_0
    return-void
.end method

.method sendAsyncBroadcastForAllUser(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .line 2730
    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 2731
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$17;

    invoke-direct {v1, p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$17;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2738
    :cond_0
    return-void
.end method

.method public setCurrentUserLw(I)V
    .locals 2
    .param p1, "newUserId"    # I

    .line 2528
    invoke-super {p0, p1}, Lcom/android/server/policy/PhoneWindowManager;->setCurrentUserLw(I)V

    .line 2529
    iput p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCurrentUserId:I

    .line 2530
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAutoDisableScreenButtonsManager:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->onUserSwitch(I)V

    .line 2531
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSmartCoverManager:Lmiui/util/SmartCoverManager;

    invoke-virtual {v0, p1}, Lmiui/util/SmartCoverManager;->onUserSwitch(I)V

    .line 2532
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAccessibilityShortcutSetting:Lmiui/provider/SettingsStringUtil$SettingStringHelper;

    invoke-virtual {v0, p1}, Lmiui/provider/SettingsStringUtil$SettingStringHelper;->setUserId(I)V

    .line 2533
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSettingsObserver:Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->onChange(Z)V

    .line 2534
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiBackTapGestureService:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService;->onUserSwitch(I)V

    .line 2535
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKnockGestureService:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->onUserSwitch(I)V

    .line 2536
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiThreeGestureListener:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->onUserSwitch(I)V

    .line 2537
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->onUserSwitch(I)V

    .line 2538
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiFingerPrintTapListener:Lcom/miui/server/input/MiuiFingerPrintTapListener;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->onUserSwitch(I)V

    .line 2539
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mShoulderKeyManagerInternal:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

    if-eqz v0, :cond_0

    .line 2540
    invoke-interface {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;->onUserSwitch()V

    .line 2542
    :cond_0
    invoke-static {}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->getInstance()Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->onUserSwitch(I)V

    .line 2543
    invoke-static {}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getInstance()Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->onUserSwitch(I)V

    .line 2544
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    if-eqz v0, :cond_1

    .line 2545
    invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->onUserSwitch(I)V

    .line 2547
    :cond_1
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2551
    const-class v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    .line 2552
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    .line 2553
    .local v0, "service":Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;
    if-eqz v0, :cond_2

    .line 2554
    invoke-virtual {v0, p1}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->onUserChanged(I)V

    .line 2556
    :cond_2
    invoke-static {}, Lcom/android/server/input/TouchWakeUpFeatureManager;->getInstance()Lcom/android/server/input/TouchWakeUpFeatureManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/server/input/TouchWakeUpFeatureManager;->onUserSwitch(I)V

    .line 2557
    invoke-static {}, Lcom/android/server/input/InputManagerServiceStub;->getInstance()Lcom/android/server/input/InputManagerServiceStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/input/InputManagerServiceStub;->updateFromUserSwitch()V

    .line 2558
    return-void
.end method

.method public setWakeUpInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "detail"    # Ljava/lang/String;
    .param p2, "reason"    # Ljava/lang/String;

    .line 557
    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWakeUpDetail:Ljava/lang/String;

    .line 558
    iput-object p2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWakeUpReason:Ljava/lang/String;

    .line 559
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mUpdateWakeUpDetailTime:J

    .line 560
    return-void
.end method

.method public showBootMessage(Ljava/lang/CharSequence;Z)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/CharSequence;
    .param p2, "always"    # Z

    .line 2567
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$14;

    invoke-direct {v1, p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$14;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2665
    return-void
.end method

.method public startedGoingToSleep(II)V
    .locals 2
    .param p1, "displayGroupId"    # I
    .param p2, "why"    # I

    .line 660
    invoke-super {p0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager;->startedGoingToSleep(II)V

    .line 661
    if-eqz p1, :cond_0

    .line 662
    return-void

    .line 664
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKnockGestureService:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->updateScreenState(Z)V

    .line 665
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiThreeGestureListener:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->updateScreenState(Z)V

    .line 667
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mShoulderKeyManagerInternal:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

    if-eqz v0, :cond_1

    .line 668
    invoke-interface {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;->updateScreenState(Z)V

    .line 670
    :cond_1
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    if-eqz v0, :cond_2

    .line 671
    invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->updateScreenState(Z)V

    .line 673
    :cond_2
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiTimeFloatingWindow:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateScreenState(Z)V

    .line 674
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    if-eqz v0, :cond_3

    .line 675
    invoke-interface {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->notifyScreenState(Z)V

    .line 677
    :cond_3
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-virtual {v0, v1}, Lcom/android/server/input/MiuiInputManagerInternal;->setScreenState(Z)V

    .line 678
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyInterceptExtend:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setScreenState(Z)V

    .line 679
    return-void
.end method

.method public startedWakingUp(II)V
    .locals 2
    .param p1, "displayGroupId"    # I
    .param p2, "why"    # I

    .line 521
    invoke-super {p0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager;->startedWakingUp(II)V

    .line 522
    if-eqz p1, :cond_0

    .line 523
    return-void

    .line 525
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiPocketModeManager:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    if-nez v0, :cond_1

    .line 526
    new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiPocketModeManager:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    .line 528
    :cond_1
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mProximitySensor:Lcom/android/server/policy/MiuiScreenOnProximityLock;

    if-eqz v0, :cond_2

    .line 529
    invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isDeviceProvisioned()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    .line 530
    invoke-static {v0}, Landroid/provider/MiuiSettings$System;->isInSmallWindowMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiPocketModeManager:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    .line 531
    invoke-virtual {v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->getPocketModeEnableSettings()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsVRMode:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsSynergyMode:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWakeUpDetail:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWakeUpReason:Ljava/lang/String;

    .line 534
    invoke-direct {p0, v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->skipPocketModeAquireByWakeUpInfo(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 535
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mProximitySensor:Lcom/android/server/policy/MiuiScreenOnProximityLock;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->aquire()V

    .line 537
    :cond_2
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKnockGestureService:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->updateScreenState(Z)V

    .line 538
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiThreeGestureListener:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->updateScreenState(Z)V

    .line 539
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiTimeFloatingWindow:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateScreenState(Z)V

    .line 540
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiBackTapGestureService:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-virtual {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->notifyScreenOn()V

    .line 541
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    if-eqz v0, :cond_3

    .line 542
    invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->updateScreenState(Z)V

    .line 545
    :cond_3
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mShoulderKeyManagerInternal:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

    if-eqz v0, :cond_4

    .line 546
    invoke-interface {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;->updateScreenState(Z)V

    .line 549
    :cond_4
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    if-eqz v0, :cond_5

    .line 550
    invoke-interface {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->notifyScreenState(Z)V

    .line 552
    :cond_5
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-virtual {v0, v1}, Lcom/android/server/input/MiuiInputManagerInternal;->setScreenState(Z)V

    .line 553
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyInterceptExtend:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setScreenState(Z)V

    .line 554
    return-void
.end method

.method protected stopLockTaskMode()Z
    .locals 1

    .line 962
    const/4 v0, 0x0

    return v0
.end method

.method protected systemReadyInternal()V
    .locals 8

    .line 446
    new-instance v0, Lcom/miui/server/input/MiuiFingerPrintTapListener;

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/miui/server/input/MiuiFingerPrintTapListener;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiFingerPrintTapListener:Lcom/miui/server/input/MiuiFingerPrintTapListener;

    .line 447
    new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiThreeGestureListener:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    .line 448
    new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->initKeyguardActiveFunction(Ljava/util/function/BooleanSupplier;)V

    .line 449
    new-instance v0, Lcom/miui/server/input/knock/MiuiKnockGestureService;

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKnockGestureService:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    .line 450
    new-instance v0, Lcom/miui/server/input/MiuiBackTapGestureService;

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/miui/server/input/MiuiBackTapGestureService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiBackTapGestureService:Lcom/miui/server/input/MiuiBackTapGestureService;

    .line 451
    const-class v0, Lcom/miui/server/stability/StabilityLocalServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/stability/StabilityLocalServiceInternal;

    iput-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mStabilityLocalServiceInternal:Lcom/miui/server/stability/StabilityLocalServiceInternal;

    .line 452
    const-class v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

    iput-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mShoulderKeyManagerInternal:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

    .line 453
    if-eqz v0, :cond_0

    .line 454
    invoke-interface {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;->systemReady()V

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 458
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const-string v1, "config_antiMistouchDisabled"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 460
    .local v1, "configAntiMisTouchDisabled":Z
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->isSupportInertialAndLightSensor(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v0, :cond_2

    .line 461
    const-string v3, "android.hardware.sensor.proximity"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 462
    invoke-static {}, Lmiui/os/DeviceFeature;->hasSupportAudioPromity()Z

    move-result v3

    if-nez v3, :cond_2

    if-nez v1, :cond_2

    .line 463
    :cond_1
    new-instance v3, Lcom/android/server/policy/MiuiScreenOnProximityLock;

    iget-object v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    iget-object v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    .line 464
    invoke-virtual {v6}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/android/server/policy/MiuiScreenOnProximityLock;-><init>(Landroid/content/Context;Lcom/android/server/policy/MiuiKeyguardServiceDelegate;Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mProximitySensor:Lcom/android/server/policy/MiuiScreenOnProximityLock;

    .line 467
    :cond_2
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "torch_state"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 468
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "auto_test_mode_on"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 470
    iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsVRMode:Z

    .line 471
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "vr_mode"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 473
    nop

    .line 474
    const-string v3, "front_fingerprint_sensor"

    invoke-static {v3, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFrontFingerprintSensor:Z

    .line 475
    nop

    .line 476
    const-string/jumbo v3, "support_tap_fingerprint_sensor_to_home"

    invoke-static {v3, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSupportTapFingerprintSensorToHome:Z

    .line 477
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFpNavEventNameList:Ljava/util/List;

    .line 478
    const-string v3, "fp_nav_event_name_list"

    invoke-static {v3}, Lmiui/util/FeatureParser;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 479
    .local v3, "strArray":[Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 480
    array-length v4, v3

    move v5, v2

    :goto_0
    if-ge v5, v4, :cond_3

    aget-object v6, v3, v5

    .line 481
    .local v6, "str":Ljava/lang/String;
    iget-object v7, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFpNavEventNameList:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 480
    .end local v6    # "str":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 486
    :cond_3
    iget-object v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "immersive.preconfirms=*"

    const/4 v6, -0x2

    const-string v7, "policy_control"

    invoke-static {v4, v7, v5, v6}, Landroid/provider/Settings$Global;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 489
    iget-object v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "persist.camera.snap.enable"

    invoke-static {v4, v5, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_5

    .line 490
    iget-object v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v5, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 491
    iget-boolean v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveTranksCard:Z

    const-string v5, "key_long_press_volume_down"

    if-nez v4, :cond_4

    .line 492
    iget-object v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v6, "Street-snap"

    iget v7, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCurrentUserId:I

    invoke-static {v4, v5, v6, v7}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    goto :goto_1

    .line 496
    :cond_4
    iget-object v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v6, "none"

    iget v7, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCurrentUserId:I

    invoke-static {v4, v5, v6, v7}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 501
    :cond_5
    :goto_1
    iget-object v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSettingsObserver:Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;

    invoke-virtual {v4, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->onChange(Z)V

    .line 502
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mEdgeSuppressionManager:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    const-string v4, "configuration"

    invoke-virtual {v2, v4}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->handleEdgeModeChange(Ljava/lang/String;)V

    .line 504
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->getKeyboardManager(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    .line 505
    if-eqz v2, :cond_6

    .line 506
    const-class v4, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    invoke-static {v4, v2}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 507
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/server/input/PadManager;->registerPointerEventListener()V

    .line 509
    :cond_6
    sget-boolean v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->IS_FOLD_DEVICE:Z

    if-nez v2, :cond_7

    sget-boolean v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->IS_FLIP_DEVICE:Z

    if-eqz v2, :cond_8

    .line 510
    :cond_7
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIDisplayFoldListener:Landroid/view/IDisplayFoldListener;

    invoke-virtual {p0, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->registerDisplayFoldListener(Landroid/view/IDisplayFoldListener;)V

    .line 512
    :cond_8
    const-class v2, Lcom/android/server/tof/TofManagerInternal;

    invoke-static {v2}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/tof/TofManagerInternal;

    iput-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTofManagerInternal:Lcom/android/server/tof/TofManagerInternal;

    .line 513
    invoke-static {}, Lmiui/hardware/input/InputFeature;->supportPhotoHandle()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 514
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/input/MiInputPhotoHandleManager;->getInstance(Landroid/content/Context;)Lcom/android/server/input/MiInputPhotoHandleManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/input/MiInputPhotoHandleManager;->init()V

    .line 516
    :cond_9
    return-void
.end method

.method protected abstract toggleSplitScreenInternal()V
.end method

.method public triggerCloseApp(Ljava/lang/String;)Z
    .locals 2
    .param p1, "action"    # Ljava/lang/String;

    .line 2010
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "trigger close app reason:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 2011
    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->closeApp()Z

    move-result v0

    return v0
.end method

.method public triggerLaunchRecent(Ljava/lang/String;)Z
    .locals 2
    .param p1, "action"    # Ljava/lang/String;

    .line 2141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "trigger launch recent,reason:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 2142
    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->preloadRecentApps()V

    .line 2143
    invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchRecentPanel()Z

    move-result v0

    return v0
.end method

.method public triggerShowMenu(Ljava/lang/String;)Z
    .locals 2
    .param p1, "action"    # Ljava/lang/String;

    .line 2147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "trigger show menu,reason:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 2148
    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showMenu()Z

    move-result v0

    return v0
.end method

.method protected unregisterStatusBarInputEventReceiver()V
    .locals 0

    .line 1778
    return-void
.end method
