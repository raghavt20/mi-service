.class public Lcom/android/server/policy/PhoneWindowManagerStubImpl;
.super Ljava/lang/Object;
.source "PhoneWindowManagerStubImpl.java"

# interfaces
.implements Lcom/android/server/policy/PhoneWindowManagerStub;


# static fields
.field private static final DELIVE_META_APPS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SUPPORT_FOD:Z

.field static final TAG:Ljava/lang/String; = "PhoneWindowManagerStubImpl"


# instance fields
.field private SUB_DISPLAY_ID:I

.field private mDisplayTurnoverManager:Lcom/android/server/policy/DisplayTurnoverManager;

.field private mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

.field mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

.field private mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

.field private mPhotoHandleConnection:Z

.field private mPhotoHandleEventDeviceId:I

.field private mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

.field private mSupportAOD:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 43
    nop

    .line 44
    const-string v0, "ro.hardware.fp.fod"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->SUPPORT_FOD:Z

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->DELIVE_META_APPS:Ljava/util/List;

    .line 58
    const-string v1, "com.ss.android.lark.kami"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->SUB_DISPLAY_ID:I

    return-void
.end method


# virtual methods
.method public convertToMiuiHapticFeedback(I)Landroid/os/VibrationEffect;
    .locals 1
    .param p1, "hapticFeedbackConstantId"    # I

    .line 123
    iget-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    invoke-virtual {v0, p1}, Lmiui/util/HapticFeedbackUtil;->convertToMiuiHapticFeedback(I)Landroid/os/VibrationEffect;

    move-result-object v0

    return-object v0
.end method

.method public hasMiuiPowerMultiClick()Z
    .locals 2

    .line 151
    iget-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    if-nez v0, :cond_0

    .line 152
    const-class v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    iput-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    const-string v1, "double_click_power_key"

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "doubleClick":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "none"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x111012b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mSupportAOD:Z

    .line 64
    invoke-static {}, Lcom/android/server/power/PowerManagerServiceStub;->get()Lcom/android/server/power/PowerManagerServiceStub;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

    .line 65
    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v0, :cond_0

    .line 66
    new-instance v0, Lcom/android/server/policy/DisplayTurnoverManager;

    invoke-direct {v0, p1}, Lcom/android/server/policy/DisplayTurnoverManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mDisplayTurnoverManager:Lcom/android/server/policy/DisplayTurnoverManager;

    .line 68
    :cond_0
    new-instance v0, Lmiui/util/HapticFeedbackUtil;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lmiui/util/HapticFeedbackUtil;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    .line 69
    return-void
.end method

.method public interceptKeyBeforeQueueing(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 111
    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mDisplayTurnoverManager:Lcom/android/server/policy/DisplayTurnoverManager;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 112
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isWakeKey()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDisplayId()I

    move-result v0

    iget v1, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->SUB_DISPLAY_ID:I

    if-ne v0, v1, :cond_1

    .line 113
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mDisplayTurnoverManager:Lcom/android/server/policy/DisplayTurnoverManager;

    const-string v2, "DOUBLE_CLICK"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/policy/DisplayTurnoverManager;->switchSubDisplayPowerState(ZLjava/lang/String;)V

    .line 116
    :cond_0
    return v1

    .line 118
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public interceptKeyWithMeta()Z
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    if-eqz v0, :cond_1

    sget-object v1, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->DELIVE_META_APPS:Ljava/util/List;

    .line 145
    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v0

    .line 144
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public interceptUnhandledKey(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 217
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 218
    .local v0, "keyCode":I
    const/16 v1, 0xc1

    if-ne v0, v1, :cond_0

    .line 219
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    .line 220
    .local v1, "device":Landroid/view/InputDevice;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/InputDevice;->isXiaomiStylus()I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    .line 221
    const/4 v2, 0x1

    return v2

    .line 224
    .end local v1    # "device":Landroid/view/InputDevice;
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public interceptWakeKey(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 128
    invoke-static {}, Lcom/xiaomi/mirror/MirrorManager;->get()Lcom/xiaomi/mirror/MirrorManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mirror/MirrorManager;->isWorking()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isWakeKey()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x52

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 132
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    return v0
.end method

.method public isEnableCombinationPowerVolumeDownScreenShot()Z
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    if-nez v0, :cond_0

    .line 169
    const-class v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    iput-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    const-string v1, "key_combination_power_volume_down"

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, "powerVolumeDownFunction":Ljava/lang/String;
    const-string/jumbo v1, "screen_shot"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public isInHangUpState()Z
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

    invoke-virtual {v0}, Lcom/android/server/power/PowerManagerServiceStub;->isInHangUpState()Z

    move-result v0

    return v0
.end method

.method public isPad()Z
    .locals 1

    .line 140
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z

    move-result v0

    return v0
.end method

.method public logMessageRemoved(Landroid/os/Handler;ILjava/lang/String;)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "what"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 201
    invoke-virtual {p1, p2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "remove message what= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " reason= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 204
    :cond_0
    return-void
.end method

.method public noConfirmForShutdown()Z
    .locals 2

    .line 194
    const-string/jumbo v0, "zhuque"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    const/4 v0, 0x1

    return v0

    .line 197
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public notifyPhotoHandleConnectionStatus(ZI)V
    .locals 0
    .param p1, "connection"    # Z
    .param p2, "deviceId"    # I

    .line 229
    iput-boolean p1, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mPhotoHandleConnection:Z

    .line 230
    iput p2, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mPhotoHandleEventDeviceId:I

    .line 231
    return-void
.end method

.method public setFocusedWindow(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V
    .locals 0
    .param p1, "focusedWindow"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 136
    iput-object p1, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 137
    return-void
.end method

.method public setForcedDisplayDensityForUser(Landroid/view/IWindowManager;)V
    .locals 3
    .param p1, "windowManager"    # Landroid/view/IWindowManager;

    .line 93
    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v0, :cond_0

    .line 96
    :try_start_0
    iget v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->SUB_DISPLAY_ID:I

    const/16 v1, 0xf0

    const/4 v2, -0x2

    invoke-interface {p1, v0, v1, v2}, Landroid/view/IWindowManager;->setForcedDisplayDensityForUser(III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    goto :goto_0

    .line 97
    :catch_0
    move-exception v0

    .line 100
    :cond_0
    :goto_0
    return-void
.end method

.method public shouldDispatchInputWhenNonInteractive(I)Z
    .locals 3
    .param p1, "keyCode"    # I

    .line 73
    iget-boolean v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mSupportAOD:Z

    const/4 v1, 0x1

    const/16 v2, 0x162

    if-eqz v0, :cond_0

    if-ne p1, v2, :cond_0

    .line 74
    return v1

    .line 76
    :cond_0
    sget-boolean v0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->SUPPORT_FOD:Z

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    if-ne p1, v2, :cond_2

    :cond_1
    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public shouldMoveDisplayToTop(I)Z
    .locals 1
    .param p1, "keyCode"    # I

    .line 80
    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_FOLD_DEVICE:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mSupportAOD:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x162

    if-ne p1, v0, :cond_0

    .line 81
    const/4 v0, 0x0

    return v0

    .line 83
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public skipInterceptMacroEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 234
    const/16 v0, 0x139

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    .line 236
    iget-boolean v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mPhotoHandleConnection:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v0

    iget v1, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mPhotoHandleEventDeviceId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    if-eqz v0, :cond_0

    .line 238
    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v0

    .line 237
    const-string v1, "com.android.camera"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    nop

    .line 236
    :goto_0
    return v2

    .line 240
    :cond_1
    return v2
.end method

.method public skipKeyGesutre(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 207
    iget-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    if-nez v0, :cond_0

    .line 208
    const-class v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    iput-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->skipKeyGesutre(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public systemBooted()V
    .locals 1

    .line 104
    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mDisplayTurnoverManager:Lcom/android/server/policy/DisplayTurnoverManager;

    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {v0}, Lcom/android/server/policy/DisplayTurnoverManager;->systemReady()V

    .line 107
    :cond_0
    return-void
.end method

.method public trackGlobalActions(Ljava/lang/String;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/String;

    .line 179
    const-string v0, "key_combination_power_volume_down"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    const-string/jumbo v0, "screen_shot"

    .local v0, "trackFunction":Ljava/lang/String;
    goto :goto_0

    .line 182
    .end local v0    # "trackFunction":Ljava/lang/String;
    :cond_0
    const-string/jumbo v0, "show_global_action"

    .line 185
    .restart local v0    # "trackFunction":Ljava/lang/String;
    :goto_0
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    move-result-object v1

    .line 184
    invoke-static {v1}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    move-result-object v1

    .line 185
    invoke-virtual {v1, p1, v0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    return-void
.end method

.method public triggerModifierShortcut(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 161
    invoke-static {p1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getInstance(Landroid/content/Context;)Lcom/android/server/policy/MiuiKeyInterceptExtend;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getKeyboardShortcutEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    invoke-static {p1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getInstance(Landroid/content/Context;)Lcom/android/server/policy/MiuiKeyInterceptExtend;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getAospKeyboardShortcutEnable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 163
    :cond_0
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 161
    :goto_0
    return v0
.end method
