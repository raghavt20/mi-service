class com.android.server.policy.BaseMiuiPhoneWindowManager$20 extends android.content.BroadcastReceiver {
	 /* .source "BaseMiuiPhoneWindowManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.policy.BaseMiuiPhoneWindowManager this$0; //synthetic
/* # direct methods */
 com.android.server.policy.BaseMiuiPhoneWindowManager$20 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
/* .line 2884 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 2886 */
final String v0 = "enable"; // const-string v0, "enable"
int v1 = 0; // const/4 v1, 0x0
v0 = (( android.content.Intent ) p2 ).getBooleanExtra ( v0, v1 ); // invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
/* .line 2887 */
/* .local v0, "enable":Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mScreenRecordeEnablekKeyEventReceiver enable="; // const-string v2, "mScreenRecordeEnablekKeyEventReceiver enable="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "WindowManager"; // const-string v2, "WindowManager"
android.util.Slog .i ( v2,v1 );
/* .line 2888 */
v1 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$msetScreenRecorderEnabled ( v1,v0 );
/* .line 2889 */
return;
} // .end method
