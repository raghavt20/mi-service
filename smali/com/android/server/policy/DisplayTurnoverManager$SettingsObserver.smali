.class Lcom/android/server/policy/DisplayTurnoverManager$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "DisplayTurnoverManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/policy/DisplayTurnoverManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/DisplayTurnoverManager;


# direct methods
.method public constructor <init>(Lcom/android/server/policy/DisplayTurnoverManager;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 264
    iput-object p1, p0, Lcom/android/server/policy/DisplayTurnoverManager$SettingsObserver;->this$0:Lcom/android/server/policy/DisplayTurnoverManager;

    .line 265
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 266
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .line 270
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 271
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager$SettingsObserver;->this$0:Lcom/android/server/policy/DisplayTurnoverManager;

    invoke-static {v0}, Lcom/android/server/policy/DisplayTurnoverManager;->-$$Nest$mupdateSubSwitchMode(Lcom/android/server/policy/DisplayTurnoverManager;)V

    .line 272
    return-void
.end method
