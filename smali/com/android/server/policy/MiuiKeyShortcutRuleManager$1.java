class com.android.server.policy.MiuiKeyShortcutRuleManager$1 extends com.android.internal.content.PackageMonitor {
	 /* .source "MiuiKeyShortcutRuleManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->registerPackageChangeReceivers()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.policy.MiuiKeyShortcutRuleManager this$0; //synthetic
/* # direct methods */
 com.android.server.policy.MiuiKeyShortcutRuleManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/policy/MiuiKeyShortcutRuleManager; */
/* .line 141 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/android/internal/content/PackageMonitor;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onPackageDataCleared ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 144 */
final String v0 = "com.android.settings"; // const-string v0, "com.android.settings"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 145 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "onPackageDataCleared uid="; // const-string v1, "onPackageDataCleared uid="
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "MiuiKeyShortcutRuleManager"; // const-string v1, "MiuiKeyShortcutRuleManager"
	 android.util.Slog .i ( v1,v0 );
	 /* .line 146 */
	 v0 = this.this$0;
	 com.android.server.policy.MiuiKeyShortcutRuleManager .-$$Nest$mresetShortcutSettings ( v0 );
	 /* .line 148 */
} // :cond_0
/* invoke-super {p0, p1, p2}, Lcom/android/internal/content/PackageMonitor;->onPackageDataCleared(Ljava/lang/String;I)V */
/* .line 149 */
return;
} // .end method
