class com.android.server.policy.MiuiPhoneWindowManager$1 implements com.android.server.wm.AccountHelper$AccountCallback {
	 /* .source "MiuiPhoneWindowManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/policy/MiuiPhoneWindowManager;->systemReady()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.policy.MiuiPhoneWindowManager this$0; //synthetic
/* # direct methods */
 com.android.server.policy.MiuiPhoneWindowManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/policy/MiuiPhoneWindowManager; */
/* .line 160 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onWifiSettingFinish ( ) {
/* .locals 1 */
/* .line 177 */
v0 = this.this$0;
com.android.server.policy.MiuiPhoneWindowManager .-$$Nest$fgetmMiuiSecurityPermissionHandler ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 178 */
	 v0 = this.this$0;
	 com.android.server.policy.MiuiPhoneWindowManager .-$$Nest$fgetmMiuiSecurityPermissionHandler ( v0 );
	 (( miui.view.MiuiSecurityPermissionHandler ) v0 ).handleWifiSettingFinish ( ); // invoke-virtual {v0}, Lmiui/view/MiuiSecurityPermissionHandler;->handleWifiSettingFinish()V
	 /* .line 180 */
} // :cond_0
return;
} // .end method
public void onXiaomiAccountLogin ( ) {
/* .locals 1 */
/* .line 163 */
v0 = this.this$0;
com.android.server.policy.MiuiPhoneWindowManager .-$$Nest$fgetmMiuiSecurityPermissionHandler ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 164 */
	 v0 = this.this$0;
	 com.android.server.policy.MiuiPhoneWindowManager .-$$Nest$fgetmMiuiSecurityPermissionHandler ( v0 );
	 (( miui.view.MiuiSecurityPermissionHandler ) v0 ).handleAccountLogin ( ); // invoke-virtual {v0}, Lmiui/view/MiuiSecurityPermissionHandler;->handleAccountLogin()V
	 /* .line 166 */
} // :cond_0
return;
} // .end method
public void onXiaomiAccountLogout ( ) {
/* .locals 1 */
/* .line 170 */
v0 = this.this$0;
com.android.server.policy.MiuiPhoneWindowManager .-$$Nest$fgetmMiuiSecurityPermissionHandler ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 171 */
	 v0 = this.this$0;
	 com.android.server.policy.MiuiPhoneWindowManager .-$$Nest$fgetmMiuiSecurityPermissionHandler ( v0 );
	 (( miui.view.MiuiSecurityPermissionHandler ) v0 ).handleAccountLogout ( ); // invoke-virtual {v0}, Lmiui/view/MiuiSecurityPermissionHandler;->handleAccountLogout()V
	 /* .line 173 */
} // :cond_0
return;
} // .end method
