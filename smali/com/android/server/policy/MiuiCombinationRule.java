public abstract class com.android.server.policy.MiuiCombinationRule extends com.android.server.policy.KeyCombinationManager$TwoKeysCombinationRule {
	 /* .source "MiuiCombinationRule.java" */
	 /* # instance fields */
	 private final java.lang.String mAction;
	 private final Integer mCombinationKey;
	 private final android.content.Context mContext;
	 private Integer mCurrentUserId;
	 private final java.lang.String mDefaultFunction;
	 private final android.os.Handler mHandler;
	 private com.android.server.policy.MiuiShortcutObserver mMiuiShortcutObserver;
	 private final Integer mPrimaryKey;
	 /* # direct methods */
	 public com.android.server.policy.MiuiCombinationRule ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .param p3, "primaryKey" # I */
		 /* .param p4, "combinationKey" # I */
		 /* .param p5, "action" # Ljava/lang/String; */
		 /* .param p6, "defaultFunction" # Ljava/lang/String; */
		 /* .param p7, "currentUserId" # I */
		 /* .line 22 */
		 /* invoke-direct {p0, p3, p4}, Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;-><init>(II)V */
		 /* .line 23 */
		 this.mContext = p1;
		 /* .line 24 */
		 this.mHandler = p2;
		 /* .line 25 */
		 this.mAction = p5;
		 /* .line 26 */
		 /* iput p3, p0, Lcom/android/server/policy/MiuiCombinationRule;->mPrimaryKey:I */
		 /* .line 27 */
		 /* iput p4, p0, Lcom/android/server/policy/MiuiCombinationRule;->mCombinationKey:I */
		 /* .line 28 */
		 this.mDefaultFunction = p6;
		 /* .line 29 */
		 /* iput p7, p0, Lcom/android/server/policy/MiuiCombinationRule;->mCurrentUserId:I */
		 /* .line 31 */
		 return;
	 } // .end method
	 private com.android.server.policy.MiuiCombinationRule getInstance ( ) {
		 /* .locals 0 */
		 /* .line 41 */
	 } // .end method
	 /* # virtual methods */
	 void cancel ( ) {
		 /* .locals 0 */
		 /* .line 79 */
		 (( com.android.server.policy.MiuiCombinationRule ) p0 ).cancelMiui ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiCombinationRule;->cancelMiui()V
		 /* .line 80 */
		 return;
	 } // .end method
	 protected void cancelMiui ( ) {
		 /* .locals 0 */
		 /* .line 84 */
		 return;
	 } // .end method
	 public Boolean equals ( java.lang.Object p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* .line 8 */
		 p1 = 		 /* invoke-super {p0, p1}, Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;->equals(Ljava/lang/Object;)Z */
	 } // .end method
	 void execute ( ) {
		 /* .locals 0 */
		 /* .line 70 */
		 (( com.android.server.policy.MiuiCombinationRule ) p0 ).miuiExecute ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiCombinationRule;->miuiExecute()V
		 /* .line 71 */
		 return;
	 } // .end method
	 public java.lang.String getAction ( ) {
		 /* .locals 1 */
		 /* .line 57 */
		 v0 = this.mAction;
	 } // .end method
	 public Integer getCombinationKey ( ) {
		 /* .locals 1 */
		 /* .line 49 */
		 /* iget v0, p0, Lcom/android/server/policy/MiuiCombinationRule;->mCombinationKey:I */
	 } // .end method
	 public java.lang.String getFunction ( ) {
		 /* .locals 1 */
		 /* .line 61 */
		 v0 = this.mMiuiShortcutObserver;
		 (( com.android.server.policy.MiuiShortcutObserver ) v0 ).getFunction ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutObserver;->getFunction()Ljava/lang/String;
	 } // .end method
	 public com.android.server.policy.MiuiShortcutObserver getObserver ( ) {
		 /* .locals 1 */
		 /* .line 53 */
		 v0 = this.mMiuiShortcutObserver;
	 } // .end method
	 public Integer getPrimaryKey ( ) {
		 /* .locals 1 */
		 /* .line 45 */
		 /* iget v0, p0, Lcom/android/server/policy/MiuiCombinationRule;->mPrimaryKey:I */
	 } // .end method
	 public Integer hashCode ( ) { //bridge//synthethic
		 /* .locals 1 */
		 /* .line 8 */
		 v0 = 		 /* invoke-super {p0}, Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;->hashCode()I */
	 } // .end method
	 public void init ( ) {
		 /* .locals 7 */
		 /* .line 34 */
		 /* new-instance v6, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver; */
		 v1 = this.mContext;
		 v2 = this.mHandler;
		 v3 = this.mAction;
		 v4 = this.mDefaultFunction;
		 /* iget v5, p0, Lcom/android/server/policy/MiuiCombinationRule;->mCurrentUserId:I */
		 /* move-object v0, v6 */
		 /* invoke-direct/range {v0 ..v5}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;I)V */
		 this.mMiuiShortcutObserver = v6;
		 /* .line 36 */
		 /* invoke-direct {p0}, Lcom/android/server/policy/MiuiCombinationRule;->getInstance()Lcom/android/server/policy/MiuiCombinationRule; */
		 (( com.android.server.policy.MiuiShortcutObserver ) v6 ).setRuleForObserver ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/server/policy/MiuiShortcutObserver;->setRuleForObserver(Lcom/android/server/policy/MiuiCombinationRule;)V
		 /* .line 37 */
		 v0 = this.mMiuiShortcutObserver;
		 int v1 = 0; // const/4 v1, 0x0
		 (( com.android.server.policy.MiuiShortcutObserver ) v0 ).setDefaultFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V
		 /* .line 38 */
		 return;
	 } // .end method
	 protected void miuiExecute ( ) {
		 /* .locals 0 */
		 /* .line 75 */
		 return;
	 } // .end method
	 protected Boolean miuiPreCondition ( ) {
		 /* .locals 1 */
		 /* .line 102 */
		 int v0 = 0; // const/4 v0, 0x0
	 } // .end method
	 public void onUserSwitch ( Integer p0, Boolean p1 ) {
		 /* .locals 1 */
		 /* .param p1, "currentUserId" # I */
		 /* .param p2, "isNewUser" # Z */
		 /* .line 65 */
		 v0 = this.mMiuiShortcutObserver;
		 (( com.android.server.policy.MiuiShortcutObserver ) v0 ).onUserSwitch ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/MiuiShortcutObserver;->onUserSwitch(IZ)V
		 /* .line 66 */
		 return;
	 } // .end method
	 public Boolean preCondition ( ) {
		 /* .locals 1 */
		 /* .line 93 */
		 v0 = 		 (( com.android.server.policy.MiuiCombinationRule ) p0 ).miuiPreCondition ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiCombinationRule;->miuiPreCondition()Z
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 2 */
		 /* .line 107 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 /* iget v1, p0, Lcom/android/server/policy/MiuiCombinationRule;->mPrimaryKey:I */
		 android.view.KeyEvent .keyCodeToString ( v1 );
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v1 = " + "; // const-string v1, " + "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/android/server/policy/MiuiCombinationRule;->mCombinationKey:I */
		 /* .line 108 */
		 android.view.KeyEvent .keyCodeToString ( v1 );
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v1 = " preCondition="; // const-string v1, " preCondition="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 109 */
		 v1 = 		 (( com.android.server.policy.MiuiCombinationRule ) p0 ).miuiPreCondition ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiCombinationRule;->miuiPreCondition()Z
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 final String v1 = " action="; // const-string v1, " action="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v1 = this.mAction;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v1 = " function="; // const-string v1, " function="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 111 */
		 (( com.android.server.policy.MiuiCombinationRule ) p0 ).getFunction ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiCombinationRule;->getFunction()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* .line 107 */
	 } // .end method
