.class Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;
.super Landroid/view/IRotationWatcher$Stub;
.source "BaseMiuiPhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RotationWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    .line 2288
    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-direct {p0}, Landroid/view/IRotationWatcher$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onRotationChanged(I)V
    .locals 3
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2292
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "rotation changed = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "WindowManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2293
    const/4 v0, 0x0

    .line 2294
    .local v0, "targetId":I
    sget-boolean v1, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmFolded(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2295
    const/4 v0, 0x1

    .line 2297
    :cond_0
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2, p1}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z

    .line 2299
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$misGameMode(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z

    if-eqz v1, :cond_1

    .line 2300
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmEdgeSuppressionManager(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    move-result-object v1

    const-string v2, "rotation"

    invoke-virtual {v1, v2}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->handleEdgeModeChange(Ljava/lang/String;)V

    .line 2303
    :cond_1
    return-void
.end method
