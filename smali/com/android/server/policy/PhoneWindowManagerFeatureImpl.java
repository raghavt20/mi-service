public class com.android.server.policy.PhoneWindowManagerFeatureImpl {
	 /* .source "PhoneWindowManagerFeatureImpl.java" */
	 /* # direct methods */
	 public com.android.server.policy.PhoneWindowManagerFeatureImpl ( ) {
		 /* .locals 0 */
		 /* .line 7 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void callInterceptPowerKeyUp ( com.android.server.policy.PhoneWindowManager p0, Boolean p1 ) {
		 /* .locals 0 */
		 /* .param p1, "manager" # Lcom/android/server/policy/PhoneWindowManager; */
		 /* .param p2, "canceled" # Z */
		 /* .line 29 */
		 return;
	 } // .end method
	 public java.lang.Object getLock ( com.android.server.policy.PhoneWindowManager p0 ) {
		 /* .locals 2 */
		 /* .param p1, "phoneWindowManager" # Lcom/android/server/policy/PhoneWindowManager; */
		 /* .line 9 */
		 final String v0 = "mLock"; // const-string v0, "mLock"
		 /* const-class v1, Ljava/lang/Object; */
		 miui.util.ReflectionUtils .tryGetObjectField ( p1,v0,v1 );
		 /* .line 10 */
		 /* .local v0, "ref":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Object;>;" */
		 /* if-nez v0, :cond_0 */
		 int v1 = 0; // const/4 v1, 0x0
	 } // :cond_0
	 (( miui.util.ObjectReference ) v0 ).get ( ); // invoke-virtual {v0}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;
} // :goto_0
} // .end method
public java.lang.Runnable getPowerLongPress ( com.android.server.policy.PhoneWindowManager p0 ) {
/* .locals 2 */
/* .param p1, "manager" # Lcom/android/server/policy/PhoneWindowManager; */
/* .line 19 */
final String v0 = "mEndCallLongPress"; // const-string v0, "mEndCallLongPress"
/* const-class v1, Ljava/lang/Runnable; */
miui.util.ReflectionUtils .tryGetObjectField ( p1,v0,v1 );
/* .line 20 */
/* .local v0, "ref":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Runnable;>;" */
/* if-nez v0, :cond_0 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_0
(( miui.util.ObjectReference ) v0 ).get ( ); // invoke-virtual {v0}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Runnable; */
} // :goto_0
} // .end method
public java.lang.Runnable getScreenshotChordLongPress ( com.android.server.policy.PhoneWindowManager p0 ) {
/* .locals 2 */
/* .param p1, "manager" # Lcom/android/server/policy/PhoneWindowManager; */
/* .line 14 */
final String v0 = "mScreenshotRunnable"; // const-string v0, "mScreenshotRunnable"
/* const-class v1, Ljava/lang/Runnable; */
miui.util.ReflectionUtils .tryGetObjectField ( p1,v0,v1 );
/* .line 15 */
/* .local v0, "ref":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Runnable;>;" */
/* if-nez v0, :cond_0 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_0
(( miui.util.ObjectReference ) v0 ).get ( ); // invoke-virtual {v0}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Runnable; */
} // :goto_0
} // .end method
public Boolean isScreenOnFully ( com.android.server.policy.PhoneWindowManager p0 ) {
/* .locals 1 */
/* .param p1, "manager" # Lcom/android/server/policy/PhoneWindowManager; */
/* .line 32 */
v0 = this.mDefaultDisplayPolicy;
/* if-nez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 33 */
} // :cond_0
v0 = this.mDefaultDisplayPolicy;
v0 = (( com.android.server.wm.DisplayPolicy ) v0 ).isScreenOnFully ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayPolicy;->isScreenOnFully()Z
} // .end method
public void setPowerLongPress ( com.android.server.policy.PhoneWindowManager p0, java.lang.Runnable p1 ) {
/* .locals 1 */
/* .param p1, "manager" # Lcom/android/server/policy/PhoneWindowManager; */
/* .param p2, "value" # Ljava/lang/Runnable; */
/* .line 24 */
final String v0 = "mEndCallLongPress"; // const-string v0, "mEndCallLongPress"
miui.util.ReflectionUtils .trySetObjectField ( p1,v0,p2 );
/* .line 25 */
return;
} // .end method
