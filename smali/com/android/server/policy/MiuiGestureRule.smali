.class public Lcom/android/server/policy/MiuiGestureRule;
.super Ljava/lang/Object;
.source "MiuiGestureRule.java"


# instance fields
.field private final mAction:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private mFunction:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "function"    # Ljava/lang/String;
    .param p5, "currentUserId"    # I

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/android/server/policy/MiuiGestureRule;->mContext:Landroid/content/Context;

    .line 19
    iput-object p2, p0, Lcom/android/server/policy/MiuiGestureRule;->mHandler:Landroid/os/Handler;

    .line 20
    iput-object p3, p0, Lcom/android/server/policy/MiuiGestureRule;->mAction:Ljava/lang/String;

    .line 21
    iput-object p4, p0, Lcom/android/server/policy/MiuiGestureRule;->mFunction:Ljava/lang/String;

    .line 22
    iput p5, p0, Lcom/android/server/policy/MiuiGestureRule;->mCurrentUserId:I

    .line 23
    return-void
.end method

.method private getInstance()Lcom/android/server/policy/MiuiGestureRule;
    .locals 0

    .line 33
    return-object p0
.end method


# virtual methods
.method public getFunction()Ljava/lang/String;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/android/server/policy/MiuiGestureRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutObserver;->getFunction()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObserver()Lcom/android/server/policy/MiuiShortcutObserver;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/android/server/policy/MiuiGestureRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    return-object v0
.end method

.method public init()V
    .locals 7

    .line 26
    new-instance v6, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;

    iget-object v1, p0, Lcom/android/server/policy/MiuiGestureRule;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/policy/MiuiGestureRule;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/server/policy/MiuiGestureRule;->mAction:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/server/policy/MiuiGestureRule;->mFunction:Ljava/lang/String;

    iget v5, p0, Lcom/android/server/policy/MiuiGestureRule;->mCurrentUserId:I

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v6, p0, Lcom/android/server/policy/MiuiGestureRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    .line 28
    invoke-direct {p0}, Lcom/android/server/policy/MiuiGestureRule;->getInstance()Lcom/android/server/policy/MiuiGestureRule;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/android/server/policy/MiuiShortcutObserver;->setRuleForObserver(Lcom/android/server/policy/MiuiGestureRule;)V

    .line 29
    iget-object v0, p0, Lcom/android/server/policy/MiuiGestureRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V

    .line 30
    return-void
.end method

.method public onUserSwitch(IZ)V
    .locals 1
    .param p1, "currentUserId"    # I
    .param p2, "isNewUser"    # Z

    .line 45
    iget-object v0, p0, Lcom/android/server/policy/MiuiGestureRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/MiuiShortcutObserver;->onUserSwitch(IZ)V

    .line 46
    return-void
.end method
