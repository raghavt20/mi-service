public class com.android.server.policy.KeyboardCombinationRule extends com.android.server.policy.KeyCombinationManager$TwoKeysCombinationRule {
	 /* .source "KeyboardCombinationRule.java" */
	 /* # static fields */
	 static final java.lang.String CLASS_NAME;
	 static final java.lang.String PACKAGE_NAME;
	 static final java.lang.String TAG;
	 /* # instance fields */
	 public android.content.Context mContext;
	 private java.lang.String mFunction;
	 public android.os.Handler mHandler;
	 public com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo mKeyboardShortcutInfo;
	 private Integer mMetaState;
	 private final Integer mPrimaryKey;
	 /* # direct methods */
	 public static void $r8$lambda$mrIOXlkYZFfpQtAWIMvVvAtw_SM ( com.android.server.policy.KeyboardCombinationRule p0 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/policy/KeyboardCombinationRule;->lambda$execute$0()V */
		 return;
	 } // .end method
	 public com.android.server.policy.KeyboardCombinationRule ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .param p3, "info" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
		 /* .line 35 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* invoke-direct {p0, v0, v0}, Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;-><init>(II)V */
		 /* .line 30 */
		 /* iput v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
		 /* .line 36 */
		 this.mContext = p1;
		 /* .line 37 */
		 this.mHandler = p2;
		 /* .line 38 */
		 this.mKeyboardShortcutInfo = p3;
		 /* .line 39 */
		 v0 = 		 (( com.android.server.policy.KeyboardCombinationRule ) p0 ).getPrimaryKey ( ); // invoke-virtual {p0}, Lcom/android/server/policy/KeyboardCombinationRule;->getPrimaryKey()I
		 /* iput v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mPrimaryKey:I */
		 /* .line 40 */
		 /* invoke-direct {p0}, Lcom/android/server/policy/KeyboardCombinationRule;->setFunction()V */
		 /* .line 41 */
		 return;
	 } // .end method
	 private void lambda$execute$0 ( ) { //synthethic
		 /* .locals 0 */
		 /* .line 54 */
		 (( com.android.server.policy.KeyboardCombinationRule ) p0 ).triggerFunction ( ); // invoke-virtual {p0}, Lcom/android/server/policy/KeyboardCombinationRule;->triggerFunction()V
		 /* .line 55 */
		 return;
	 } // .end method
	 private void setFunction ( ) {
		 /* .locals 1 */
		 /* .line 113 */
		 v0 = this.mKeyboardShortcutInfo;
		 v0 = 		 (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v0 ).getType ( ); // invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I
		 /* packed-switch v0, :pswitch_data_0 */
		 /* .line 154 */
		 final String v0 = ""; // const-string v0, ""
		 this.mFunction = v0;
		 /* .line 148 */
		 /* :pswitch_0 */
		 final String v0 = "launch_split_screen_to_right"; // const-string v0, "launch_split_screen_to_right"
		 this.mFunction = v0;
		 /* .line 149 */
		 /* .line 145 */
		 /* :pswitch_1 */
		 final String v0 = "launch_split_screen_to_left"; // const-string v0, "launch_split_screen_to_left"
		 this.mFunction = v0;
		 /* .line 146 */
		 /* .line 142 */
		 /* :pswitch_2 */
		 final String v0 = "launch_app_full_window"; // const-string v0, "launch_app_full_window"
		 this.mFunction = v0;
		 /* .line 143 */
		 /* .line 139 */
		 /* :pswitch_3 */
		 final String v0 = "launch_app_small_window"; // const-string v0, "launch_app_small_window"
		 this.mFunction = v0;
		 /* .line 140 */
		 /* .line 136 */
		 /* :pswitch_4 */
		 final String v0 = "close_app"; // const-string v0, "close_app"
		 this.mFunction = v0;
		 /* .line 137 */
		 /* .line 133 */
		 /* :pswitch_5 */
		 final String v0 = "partial_screen_shot"; // const-string v0, "partial_screen_shot"
		 this.mFunction = v0;
		 /* .line 134 */
		 /* .line 130 */
		 /* :pswitch_6 */
		 /* const-string/jumbo v0, "screen_shot" */
		 this.mFunction = v0;
		 /* .line 131 */
		 /* .line 127 */
		 /* :pswitch_7 */
		 final String v0 = "go_to_sleep"; // const-string v0, "go_to_sleep"
		 this.mFunction = v0;
		 /* .line 128 */
		 /* .line 124 */
		 /* :pswitch_8 */
		 final String v0 = "launch_home"; // const-string v0, "launch_home"
		 this.mFunction = v0;
		 /* .line 125 */
		 /* .line 121 */
		 /* :pswitch_9 */
		 final String v0 = "launch_recents"; // const-string v0, "launch_recents"
		 this.mFunction = v0;
		 /* .line 122 */
		 /* .line 118 */
		 /* :pswitch_a */
		 final String v0 = "launch_notification_center"; // const-string v0, "launch_notification_center"
		 this.mFunction = v0;
		 /* .line 119 */
		 /* .line 115 */
		 /* :pswitch_b */
		 final String v0 = "launch_control_center"; // const-string v0, "launch_control_center"
		 this.mFunction = v0;
		 /* .line 116 */
		 /* .line 151 */
		 /* :pswitch_c */
		 final String v0 = "launch_app"; // const-string v0, "launch_app"
		 this.mFunction = v0;
		 /* .line 152 */
		 /* nop */
		 /* .line 157 */
	 } // :goto_0
	 return;
	 /* nop */
	 /* :pswitch_data_0 */
	 /* .packed-switch 0x0 */
	 /* :pswitch_c */
	 /* :pswitch_b */
	 /* :pswitch_a */
	 /* :pswitch_9 */
	 /* :pswitch_8 */
	 /* :pswitch_7 */
	 /* :pswitch_6 */
	 /* :pswitch_5 */
	 /* :pswitch_4 */
	 /* :pswitch_3 */
	 /* :pswitch_2 */
	 /* :pswitch_1 */
	 /* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
void cancel ( ) {
/* .locals 0 */
/* .line 60 */
return;
} // .end method
public Boolean equals ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 22 */
p1 = /* invoke-super {p0, p1}, Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;->equals(Ljava/lang/Object;)Z */
} // .end method
public void execute ( ) {
/* .locals 2 */
/* .line 53 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/policy/KeyboardCombinationRule$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/policy/KeyboardCombinationRule$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/policy/KeyboardCombinationRule;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 56 */
return;
} // .end method
public Integer getMetaState ( ) {
/* .locals 1 */
/* .line 108 */
/* iget v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
} // .end method
public Integer getPrimaryKey ( ) {
/* .locals 6 */
/* .line 81 */
v0 = this.mKeyboardShortcutInfo;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v0 ).getShortcutKeyCode ( ); // invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v0 */
/* .line 82 */
/* .local v0, "keyCode":J */
/* const-wide/high16 v2, 0x1000000000000L */
/* and-long/2addr v2, v0 */
/* const-wide/16 v4, 0x0 */
/* cmp-long v2, v2, v4 */
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 83 */
	 /* const-wide v2, -0x1000000000001L */
	 /* and-long/2addr v0, v2 */
	 /* .line 84 */
	 /* iget v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
	 /* const/high16 v3, 0x10000 */
	 /* or-int/2addr v2, v3 */
	 /* iput v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
	 /* .line 86 */
} // :cond_0
/* const-wide v2, 0x200000000L */
/* and-long/2addr v2, v0 */
/* cmp-long v2, v2, v4 */
if ( v2 != null) { // if-eqz v2, :cond_2
	 /* .line 87 */
	 /* const-wide v2, -0x200000001L */
	 /* and-long/2addr v0, v2 */
	 /* .line 88 */
	 /* const-wide v2, 0x2000000000L */
	 /* and-long/2addr v2, v0 */
	 /* cmp-long v2, v2, v4 */
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* .line 89 */
		 /* const-wide v2, -0x2000000001L */
		 /* and-long/2addr v0, v2 */
		 /* .line 90 */
		 /* iget v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
		 /* or-int/lit8 v2, v2, 0x22 */
		 /* iput v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
		 /* .line 91 */
	 } // :cond_1
	 /* const-wide v2, 0x1000000000L */
	 /* and-long/2addr v2, v0 */
	 /* cmp-long v2, v2, v4 */
	 if ( v2 != null) { // if-eqz v2, :cond_2
		 /* .line 92 */
		 /* const-wide v2, -0x1000000001L */
		 /* and-long/2addr v0, v2 */
		 /* .line 93 */
		 /* iget v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
		 /* or-int/lit8 v2, v2, 0x12 */
		 /* iput v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
		 /* .line 96 */
	 } // :cond_2
} // :goto_0
/* const-wide v2, 0x100000000000L */
/* and-long/2addr v2, v0 */
/* cmp-long v2, v2, v4 */
if ( v2 != null) { // if-eqz v2, :cond_3
	 /* .line 97 */
	 /* const-wide v2, -0x100000000001L */
	 /* and-long/2addr v0, v2 */
	 /* .line 98 */
	 /* iget v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
	 /* or-int/lit16 v2, v2, 0x1000 */
	 /* iput v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
	 /* .line 100 */
} // :cond_3
/* const-wide v2, 0x100000000L */
/* and-long/2addr v2, v0 */
/* cmp-long v2, v2, v4 */
if ( v2 != null) { // if-eqz v2, :cond_4
	 /* .line 101 */
	 /* const-wide v2, -0x100000001L */
	 /* and-long/2addr v0, v2 */
	 /* .line 102 */
	 /* iget v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
	 /* or-int/lit8 v2, v2, 0x1 */
	 /* iput v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
	 /* .line 104 */
} // :cond_4
/* long-to-int v2, v0 */
} // .end method
public Integer hashCode ( ) { //bridge//synthethic
/* .locals 1 */
/* .line 22 */
v0 = /* invoke-super {p0}, Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;->hashCode()I */
} // .end method
public Boolean shouldInterceptKeys ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "keyCode" # I */
/* .param p2, "metaState" # I */
/* .line 44 */
/* iget v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mPrimaryKey:I */
int v1 = 0; // const/4 v1, 0x0
/* if-ne v0, p1, :cond_0 */
/* iget v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
/* if-ne v0, p2, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
/* move v0, v1 */
/* .line 45 */
/* .local v0, "isMatch":Z */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_1
v2 = this.mContext;
v2 = com.miui.server.input.custom.InputMiuiDesktopMode .shouldInterceptKeyboardCombinationRule ( v2,p2,p1 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 46 */
/* .line 48 */
} // :cond_1
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 161 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "KeyboardCombinationRule{mPrimaryKey="; // const-string v1, "KeyboardCombinationRule{mPrimaryKey="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mPrimaryKey:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mKeyboardShortcutInfo="; // const-string v1, ", mKeyboardShortcutInfo="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mKeyboardShortcutInfo;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", mMetaState="; // const-string v1, ", mMetaState="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mFunction=\'"; // const-string v1, ", mFunction=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mFunction;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public void triggerFunction ( ) {
/* .locals 5 */
/* .line 64 */
v0 = this.mFunction;
v0 = android.text.TextUtils .isEmpty ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 65 */
final String v0 = "KeyboardCombinationRule"; // const-string v0, "KeyboardCombinationRule"
final String v1 = "function is null"; // const-string v1, "function is null"
android.util.Slog .i ( v0,v1 );
/* .line 66 */
return;
/* .line 68 */
} // :cond_0
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 69 */
/* .local v0, "extras":Landroid/os/Bundle; */
final String v1 = "launch_app"; // const-string v1, "launch_app"
v2 = this.mFunction;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 70 */
v1 = this.mKeyboardShortcutInfo;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getPackageName()Ljava/lang/String;
final String v2 = "packageName"; // const-string v2, "packageName"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 71 */
v1 = this.mKeyboardShortcutInfo;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v1 ).getClassName ( ); // invoke-virtual {v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getClassName()Ljava/lang/String;
final String v2 = "className"; // const-string v2, "className"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 73 */
} // :cond_1
v1 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v1 );
v2 = this.mKeyboardShortcutInfo;
/* .line 74 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil$KeyBoardShortcut .getShortCutNameByType ( v2 );
/* .line 73 */
(( com.android.server.input.InputOneTrackUtil ) v1 ).trackKeyboardShortcut ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardShortcut(Ljava/lang/String;)V
/* .line 76 */
v1 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v1 );
v2 = this.mFunction;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v4, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "+"; // const-string v4, "+"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mPrimaryKey:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v4 = 0; // const/4 v4, 0x0
(( com.miui.server.input.util.ShortCutActionsUtils ) v1 ).triggerFunction ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 78 */
return;
} // .end method
