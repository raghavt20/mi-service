.class Lcom/android/server/policy/BaseMiuiPhoneWindowManager$6;
.super Ljava/lang/Object;
.source "BaseMiuiPhoneWindowManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    .line 998
    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$6;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 1001
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$6;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmDoubleClickAiKeyCount(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;I)V

    .line 1002
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$6;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmLongPressAiKeyIsConsumed(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V

    .line 1003
    const-string v0, "long press down ai key"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->detail(Ljava/lang/String;)V

    .line 1004
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$6;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const-string v1, "key_long_press_down_ai_button_settings"

    invoke-static {v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$mstartAiKeyService(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/String;)V

    .line 1005
    return-void
.end method
