.class public Lcom/android/server/policy/MiuiInputLog;
.super Ljava/lang/Object;
.source "MiuiInputLog.java"


# static fields
.field private static final DETAIL:I = 0x1

.field private static final MAJOR:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MiuiInputKeyEventLog"

.field private static volatile sInstance:Lcom/android/server/policy/MiuiInputLog;


# instance fields
.field private mInputDebugLevel:I

.field private mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/policy/MiuiInputLog;->mInputDebugLevel:I

    .line 22
    const-class v0, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v0, p0, Lcom/android/server/policy/MiuiInputLog;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 23
    return-void
.end method

.method public static defaults(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .line 64
    const-string v0, "MiuiInputKeyEventLog"

    invoke-static {v0, p0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    return-void
.end method

.method public static defaults(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .line 75
    const-string v0, "MiuiInputKeyEventLog"

    invoke-static {v0, p0, p1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 76
    return-void
.end method

.method public static detail(Ljava/lang/String;)V
    .locals 2
    .param p0, "msg"    # Ljava/lang/String;

    .line 106
    invoke-static {}, Lcom/android/server/policy/MiuiInputLog;->getLogLevel()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    .line 107
    const-string v0, "MiuiInputKeyEventLog"

    invoke-static {v0, p0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :cond_0
    return-void
.end method

.method public static detail(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .line 119
    invoke-static {}, Lcom/android/server/policy/MiuiInputLog;->getLogLevel()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    .line 120
    const-string v0, "MiuiInputKeyEventLog"

    invoke-static {v0, p0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 122
    :cond_0
    return-void
.end method

.method public static error(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .line 43
    const-string v0, "MiuiInputKeyEventLog"

    invoke-static {v0, p0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    return-void
.end method

.method public static error(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .line 54
    const-string v0, "MiuiInputKeyEventLog"

    invoke-static {v0, p0, p1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 55
    return-void
.end method

.method public static getInstance()Lcom/android/server/policy/MiuiInputLog;
    .locals 2

    .line 26
    sget-object v0, Lcom/android/server/policy/MiuiInputLog;->sInstance:Lcom/android/server/policy/MiuiInputLog;

    if-nez v0, :cond_1

    .line 27
    const-class v0, Lcom/android/server/policy/MiuiInputLog;

    monitor-enter v0

    .line 28
    :try_start_0
    sget-object v1, Lcom/android/server/policy/MiuiInputLog;->sInstance:Lcom/android/server/policy/MiuiInputLog;

    if-nez v1, :cond_0

    .line 29
    new-instance v1, Lcom/android/server/policy/MiuiInputLog;

    invoke-direct {v1}, Lcom/android/server/policy/MiuiInputLog;-><init>()V

    sput-object v1, Lcom/android/server/policy/MiuiInputLog;->sInstance:Lcom/android/server/policy/MiuiInputLog;

    .line 31
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 33
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/policy/MiuiInputLog;->sInstance:Lcom/android/server/policy/MiuiInputLog;

    return-object v0
.end method

.method private static getLogLevel()I
    .locals 1

    .line 125
    invoke-static {}, Lcom/android/server/policy/MiuiInputLog;->getInstance()Lcom/android/server/policy/MiuiInputLog;

    move-result-object v0

    iget v0, v0, Lcom/android/server/policy/MiuiInputLog;->mInputDebugLevel:I

    return v0
.end method

.method public static major(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .line 85
    const-string v0, "MiuiInputKeyEventLog"

    invoke-static {v0, p0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    return-void
.end method

.method public static major(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .line 96
    const-string v0, "MiuiInputKeyEventLog"

    invoke-static {v0, p0, p1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 97
    return-void
.end method


# virtual methods
.method public setLogLevel(I)V
    .locals 5
    .param p1, "logLevel"    # I

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setLogLevel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 135
    iput p1, p0, Lcom/android/server/policy/MiuiInputLog;->mInputDebugLevel:I

    .line 136
    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 137
    .local v0, "flag":Z
    :goto_0
    iget-object v1, p0, Lcom/android/server/policy/MiuiInputLog;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    instance-of v2, v1, Lcom/android/server/policy/PhoneWindowManager;

    if-eqz v2, :cond_1

    .line 138
    const-class v2, Lcom/android/server/policy/PhoneWindowManager;

    check-cast v1, Lcom/android/server/policy/PhoneWindowManager;

    .line 139
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    .line 138
    const-string/jumbo v4, "setDebugSwitch"

    invoke-static {v2, v1, v4, v3}, Lcom/android/server/input/ReflectionUtils;->callPrivateMethod(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    :cond_1
    return-void
.end method
