class com.android.server.policy.BaseMiuiPhoneWindowManager$MiuiSettingsObserver extends android.database.ContentObserver {
	 /* .source "BaseMiuiPhoneWindowManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MiuiSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.policy.BaseMiuiPhoneWindowManager this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$D7FzyKjm9t67_452lQvoL2oMm20 ( com.android.server.policy.BaseMiuiPhoneWindowManager$MiuiSettingsObserver p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->lambda$observe$0()V */
return;
} // .end method
public static void $r8$lambda$e97DkC8pDKRtFq6Glj2fbRlVbno ( com.android.server.policy.BaseMiuiPhoneWindowManager$MiuiSettingsObserver p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->lambda$onChange$1()V */
return;
} // .end method
 com.android.server.policy.BaseMiuiPhoneWindowManager$MiuiSettingsObserver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 2308 */
this.this$0 = p1;
/* .line 2309 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 2310 */
return;
} // .end method
private void lambda$observe$0 ( ) { //synthethic
/* .locals 4 */
/* .line 2366 */
v0 = this.this$0;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.this$0;
v1 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v1 );
final String v2 = "is_mi_input_event_time_line_enable"; // const-string v2, "is_mi_input_event_time_line_enable"
int v3 = 0; // const/4 v3, 0x0
android.provider.Settings$System .putIntForUser ( v0,v2,v3,v1 );
/* .line 2368 */
return;
} // .end method
private void lambda$onChange$1 ( ) { //synthethic
/* .locals 4 */
/* .line 2421 */
v0 = this.this$0;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.this$0;
v1 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v1 );
final String v2 = "is_mi_input_event_time_line_enable"; // const-string v2, "is_mi_input_event_time_line_enable"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v3, v1 */
} // :cond_0
/* move v0, v3 */
/* .line 2423 */
/* .local v0, "enable":Z */
com.android.server.input.config.InputCommonConfig .getInstance ( );
/* .line 2424 */
/* .local v1, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig; */
(( com.android.server.input.config.InputCommonConfig ) v1 ).setMiInputEventTimeLineMode ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/config/InputCommonConfig;->setMiInputEventTimeLineMode(Z)V
/* .line 2425 */
(( com.android.server.input.config.InputCommonConfig ) v1 ).flushToNative ( ); // invoke-virtual {v1}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 2426 */
return;
} // .end method
/* # virtual methods */
void observe ( ) {
/* .locals 7 */
/* .line 2313 */
v0 = this.this$0;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 2314 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
/* const-string/jumbo v1, "trackball_wake_screen" */
android.provider.Settings$System .getUriFor ( v1 );
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2316 */
final String v1 = "camera_key_preferred_action_type"; // const-string v1, "camera_key_preferred_action_type"
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2318 */
final String v1 = "camera_key_preferred_action_shortcut_id"; // const-string v1, "camera_key_preferred_action_shortcut_id"
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2320 */
/* const-string/jumbo v1, "volumekey_wake_screen" */
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2322 */
final String v1 = "key_long_press_volume_down"; // const-string v1, "key_long_press_volume_down"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2324 */
final String v1 = "auto_test_mode_on"; // const-string v1, "auto_test_mode_on"
android.provider.Settings$Global .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2326 */
final String v1 = "key_bank_card_in_ese"; // const-string v1, "key_bank_card_in_ese"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2328 */
final String v1 = "key_trans_card_in_ese"; // const-string v1, "key_trans_card_in_ese"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2330 */
/* const-string/jumbo v1, "vr_mode" */
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2332 */
final String v1 = "enable_mikey_mode"; // const-string v1, "enable_mikey_mode"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2334 */
final String v1 = "enabled_accessibility_services"; // const-string v1, "enabled_accessibility_services"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2336 */
final String v1 = "accessibility_shortcut_target_service"; // const-string v1, "accessibility_shortcut_target_service"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2338 */
final String v1 = "accessibility_shortcut_on_lock_screen"; // const-string v1, "accessibility_shortcut_on_lock_screen"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2340 */
/* const-string/jumbo v1, "three_gesture_down" */
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2342 */
/* const-string/jumbo v1, "three_gesture_long_press" */
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2344 */
/* const-string/jumbo v1, "send_back_when_xiaoai_appear" */
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2346 */
final String v1 = "long_press_timeout"; // const-string v1, "long_press_timeout"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v4, v2, p0, v3 ); // invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2349 */
final String v4 = "kid_mode_status"; // const-string v4, "kid_mode_status"
android.provider.Settings$Global .getUriFor ( v4 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v4, v2, p0, v3 ); // invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2351 */
final String v4 = "kid_user_id"; // const-string v4, "kid_user_id"
android.provider.Settings$Secure .getUriFor ( v4 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v4, v2, p0, v3 ); // invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2354 */
v4 = this.this$0;
v4 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmIsSupportGloablTounchDirection ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_0
/* sget-boolean v4, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
} // :cond_0
/* sget-boolean v4, Lmiui/os/DeviceFeature;->SUPPORT_GAME_MODE:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
	 /* .line 2356 */
	 final String v4 = "gb_boosting"; // const-string v4, "gb_boosting"
	 android.provider.Settings$Secure .getUriFor ( v4 );
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v4, v2, p0, v3 ); // invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 2359 */
} // :cond_1
/* nop */
/* .line 2360 */
final String v4 = "is_custom_shortcut_effective"; // const-string v4, "is_custom_shortcut_effective"
android.provider.Settings$System .getUriFor ( v4 );
/* .line 2359 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v5, v2, p0, v3 ); // invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2362 */
/* const-string/jumbo v5, "synergy_mode" */
android.provider.Settings$Secure .getUriFor ( v5 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v5, v2, p0, v3 ); // invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2365 */
com.android.server.input.MiuiInputThread .getHandler ( );
/* new-instance v6, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver$$ExternalSyntheticLambda1; */
/* invoke-direct {v6, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;)V */
(( android.os.Handler ) v5 ).post ( v6 ); // invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2369 */
/* nop */
/* .line 2370 */
final String v5 = "is_mi_input_event_time_line_enable"; // const-string v5, "is_mi_input_event_time_line_enable"
android.provider.Settings$System .getUriFor ( v5 );
/* .line 2369 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v5, v2, p0, v3 ); // invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2372 */
/* nop */
/* .line 2373 */
final String v3 = "imperceptible_press_power_key"; // const-string v3, "imperceptible_press_power_key"
android.provider.Settings$Global .getUriFor ( v3 );
/* .line 2372 */
(( com.android.server.policy.BaseMiuiPhoneWindowManager$MiuiSettingsObserver ) p0 ).onChange ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 2374 */
android.provider.Settings$Secure .getUriFor ( v1 );
(( com.android.server.policy.BaseMiuiPhoneWindowManager$MiuiSettingsObserver ) p0 ).onChange ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 2375 */
android.provider.Settings$System .getUriFor ( v4 );
(( com.android.server.policy.BaseMiuiPhoneWindowManager$MiuiSettingsObserver ) p0 ).onChange ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 2376 */
return;
} // .end method
public void onChange ( Boolean p0 ) {
/* .locals 12 */
/* .param p1, "selfChange" # Z */
/* .line 2434 */
v0 = this.this$0;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 2435 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$sfgetphoneWindowManagerFeature ( );
v2 = this.this$0;
(( com.android.server.policy.PhoneWindowManagerFeatureImpl ) v1 ).getLock ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;->getLock(Lcom/android/server/policy/PhoneWindowManager;)Ljava/lang/Object;
/* .line 2436 */
/* .local v1, "lock":Ljava/lang/Object; */
/* monitor-enter v1 */
/* .line 2437 */
try { // :try_start_0
	 v2 = this.this$0;
	 v2 = this.mContext;
	 android.provider.MiuiSettings$Key .updateOldKeyFunctionToNew ( v2 );
	 /* .line 2438 */
	 v2 = this.this$0;
	 v2 = this.mMiuiShortcutTriggerHelper;
	 final String v3 = "screen_key_press_app_switch"; // const-string v3, "screen_key_press_app_switch"
	 v4 = this.this$0;
	 v4 = 	 com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v4 );
	 int v5 = 1; // const/4 v5, 0x1
	 v3 = 	 android.provider.Settings$System .getIntForUser ( v0,v3,v5,v4 );
	 int v4 = 0; // const/4 v4, 0x0
	 if ( v3 != null) { // if-eqz v3, :cond_0
		 /* move v3, v5 */
	 } // :cond_0
	 /* move v3, v4 */
} // :goto_0
(( com.android.server.policy.MiuiShortcutTriggerHelper ) v2 ).setPressToAppSwitch ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setPressToAppSwitch(Z)V
/* .line 2442 */
v2 = this.this$0;
/* const-string/jumbo v3, "trackball_wake_screen" */
v6 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v2 );
v3 = android.provider.Settings$System .getIntForUser ( v0,v3,v4,v6 );
/* if-ne v3, v5, :cond_1 */
/* move v3, v5 */
} // :cond_1
/* move v3, v4 */
} // :goto_1
/* iput-boolean v3, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackballWakeScreen:Z */
/* .line 2444 */
v2 = this.this$0;
final String v3 = "enable_mikey_mode"; // const-string v3, "enable_mikey_mode"
v6 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v2 );
v3 = android.provider.Settings$Secure .getIntForUser ( v0,v3,v4,v6 );
if ( v3 != null) { // if-eqz v3, :cond_2
/* move v3, v5 */
} // :cond_2
/* move v3, v4 */
} // :goto_2
/* iput-boolean v3, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMikeymodeEnabled:Z */
/* .line 2446 */
final String v2 = "camera_key_preferred_action_type"; // const-string v2, "camera_key_preferred_action_type"
v3 = this.this$0;
v3 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v3 );
v2 = android.provider.Settings$System .getIntForUser ( v0,v2,v4,v3 );
/* .line 2449 */
/* .local v2, "cameraKeyActionType":I */
v3 = this.this$0;
/* if-ne v5, v2, :cond_3 */
final String v6 = "camera_key_preferred_action_shortcut_id"; // const-string v6, "camera_key_preferred_action_shortcut_id"
v7 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v3 );
/* .line 2450 */
int v8 = -1; // const/4 v8, -0x1
v6 = android.provider.Settings$System .getIntForUser ( v0,v6,v8,v7 );
int v7 = 4; // const/4 v7, 0x4
/* if-ne v7, v6, :cond_3 */
/* move v6, v5 */
} // :cond_3
/* move v6, v4 */
} // :goto_3
/* iput-boolean v6, v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCameraKeyWakeScreen:Z */
/* .line 2452 */
v3 = this.this$0;
final String v6 = "auto_test_mode_on"; // const-string v6, "auto_test_mode_on"
v6 = android.provider.Settings$Global .getInt ( v0,v6,v4 );
if ( v6 != null) { // if-eqz v6, :cond_4
/* move v6, v5 */
} // :cond_4
/* move v6, v4 */
} // :goto_4
/* iput-boolean v6, v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z */
/* .line 2454 */
v3 = this.this$0;
/* const-string/jumbo v6, "send_back_when_xiaoai_appear" */
v7 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v3 );
v6 = android.provider.Settings$System .getIntForUser ( v0,v6,v4,v7 );
if ( v6 != null) { // if-eqz v6, :cond_5
/* move v6, v5 */
} // :cond_5
/* move v6, v4 */
} // :goto_5
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmVoiceAssistEnabled ( v3,v6 );
/* .line 2457 */
v3 = this.this$0;
final String v6 = "key_bank_card_in_ese"; // const-string v6, "key_bank_card_in_ese"
v7 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v3 );
v6 = android.provider.Settings$Secure .getIntForUser ( v0,v6,v4,v7 );
/* if-lez v6, :cond_6 */
/* move v6, v5 */
} // :cond_6
/* move v6, v4 */
} // :goto_6
/* iput-boolean v6, v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveBankCard:Z */
/* .line 2459 */
v3 = this.this$0;
final String v6 = "key_trans_card_in_ese"; // const-string v6, "key_trans_card_in_ese"
v7 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v3 );
v6 = android.provider.Settings$Secure .getIntForUser ( v0,v6,v4,v7 );
/* if-lez v6, :cond_7 */
/* move v6, v5 */
} // :cond_7
/* move v6, v4 */
} // :goto_7
/* iput-boolean v6, v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveTranksCard:Z */
/* .line 2461 */
final String v3 = "key_long_press_volume_down"; // const-string v3, "key_long_press_volume_down"
v6 = this.this$0;
v6 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v6 );
android.provider.Settings$Secure .getStringForUser ( v0,v3,v6 );
/* .line 2463 */
/* .local v3, "action":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_b
/* .line 2464 */
final String v6 = "Street-snap"; // const-string v6, "Street-snap"
v6 = (( java.lang.String ) v6 ).equals ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v6, :cond_a */
final String v6 = "Street-snap-picture"; // const-string v6, "Street-snap-picture"
/* .line 2465 */
v6 = (( java.lang.String ) v6 ).equals ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v6, :cond_a */
final String v6 = "Street-snap-movie"; // const-string v6, "Street-snap-movie"
/* .line 2466 */
v6 = (( java.lang.String ) v6 ).equals ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_8
/* .line 2468 */
} // :cond_8
final String v6 = "public_transportation_shortcuts"; // const-string v6, "public_transportation_shortcuts"
v6 = (( java.lang.String ) v6 ).equals ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_9
/* .line 2469 */
v6 = this.this$0;
int v7 = 2; // const/4 v7, 0x2
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmLongPressVolumeDownBehavior ( v6,v7 );
/* .line 2471 */
} // :cond_9
v6 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmLongPressVolumeDownBehavior ( v6,v4 );
/* .line 2467 */
} // :cond_a
} // :goto_8
v6 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmLongPressVolumeDownBehavior ( v6,v5 );
/* .line 2474 */
} // :cond_b
final String v6 = "key_long_press_volume_down"; // const-string v6, "key_long_press_volume_down"
final String v7 = "none"; // const-string v7, "none"
v8 = this.this$0;
v8 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v8 );
android.provider.Settings$Secure .putStringForUser ( v0,v6,v7,v8 );
/* .line 2477 */
} // :goto_9
v6 = this.this$0;
/* const-string/jumbo v7, "vr_mode" */
v8 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v6 );
v7 = android.provider.Settings$System .getIntForUser ( v0,v7,v4,v8 );
/* if-ne v7, v5, :cond_c */
/* move v7, v5 */
} // :cond_c
/* move v7, v4 */
} // :goto_a
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmIsVRMode ( v6,v7 );
/* .line 2479 */
v6 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmAccessibilityShortcutSetting ( v6 );
/* if-nez v6, :cond_d */
/* .line 2480 */
v6 = this.this$0;
/* new-instance v7, Lmiui/provider/SettingsStringUtil$SettingStringHelper; */
v8 = this.this$0;
v8 = this.mContext;
(( android.content.Context ) v8 ).getContentResolver ( ); // invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v9 = "enabled_accessibility_services"; // const-string v9, "enabled_accessibility_services"
v10 = this.this$0;
v10 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v10 );
/* invoke-direct {v7, v8, v9, v10}, Lmiui/provider/SettingsStringUtil$SettingStringHelper;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;I)V */
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmAccessibilityShortcutSetting ( v6,v7 );
/* .line 2483 */
} // :cond_d
v6 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmAccessibilityShortcutSetting ( v6 );
(( miui.provider.SettingsStringUtil$SettingStringHelper ) v6 ).read ( ); // invoke-virtual {v6}, Lmiui/provider/SettingsStringUtil$SettingStringHelper;->read()Ljava/lang/String;
/* .line 2484 */
/* .local v6, "shortcutService":Ljava/lang/String; */
v7 = this.this$0;
v8 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$mhasTalkbackService ( v7,v6 );
/* iput-boolean v8, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTalkBackIsOpened:Z */
/* .line 2486 */
int v7 = 0; // const/4 v7, 0x0
/* .line 2488 */
/* .local v7, "accessibilityShortcutEnabled":Z */
final String v8 = "accessibility_shortcut_target_service"; // const-string v8, "accessibility_shortcut_target_service"
v9 = this.this$0;
v9 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v9 );
android.provider.Settings$Secure .getStringForUser ( v0,v8,v9 );
/* .line 2490 */
/* .local v8, "accessibilityShortcut":Ljava/lang/String; */
/* if-nez v8, :cond_e */
/* .line 2491 */
v9 = this.this$0;
v9 = this.mContext;
/* const v10, 0x1040256 */
(( android.content.Context ) v9 ).getString ( v10 ); // invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* move-object v8, v9 */
/* .line 2494 */
} // :cond_e
v9 = this.this$0;
if ( v7 != null) { // if-eqz v7, :cond_f
v10 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$misTalkBackService ( v9,v8 );
if ( v10 != null) { // if-eqz v10, :cond_f
/* move v10, v5 */
} // :cond_f
/* move v10, v4 */
} // :goto_b
/* iput-boolean v10, v9, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mShortcutServiceIsTalkBack:Z */
/* .line 2497 */
v9 = this.this$0;
final String v10 = "accessibility_shortcut_on_lock_screen"; // const-string v10, "accessibility_shortcut_on_lock_screen"
v11 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v9 );
v10 = android.provider.Settings$Secure .getIntForUser ( v0,v10,v4,v11 );
/* if-ne v10, v5, :cond_10 */
} // :cond_10
/* move v5, v4 */
} // :goto_c
/* iput-boolean v5, v9, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAccessibilityShortcutOnLockScreen:Z */
/* .line 2499 */
} // .end local v2 # "cameraKeyActionType":I
} // .end local v3 # "action":Ljava/lang/String;
} // .end local v6 # "shortcutService":Ljava/lang/String;
} // .end local v7 # "accessibilityShortcutEnabled":Z
} // .end local v8 # "accessibilityShortcut":Ljava/lang/String;
/* monitor-exit v1 */
/* .line 2500 */
return;
/* .line 2499 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 2380 */
v0 = this.this$0;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 2381 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
final String v1 = "gb_boosting"; // const-string v1, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v1 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2382 */
/* sget-boolean v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.this$0;
v1 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$misGameMode ( v1 );
/* if-nez v1, :cond_0 */
/* .line 2383 */
v1 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmEdgeSuppressionManager ( v1 );
final String v2 = "gameBooster"; // const-string v2, "gameBooster"
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager ) v1 ).handleEdgeModeChange ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->handleEdgeModeChange(Ljava/lang/String;)V
/* goto/16 :goto_2 */
/* .line 2386 */
} // :cond_0
v1 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$mhandleTouchFeatureRotationWatcher ( v1 );
/* goto/16 :goto_2 */
/* .line 2388 */
} // :cond_1
final String v1 = "imperceptible_press_power_key"; // const-string v1, "imperceptible_press_power_key"
android.provider.Settings$Global .getUriFor ( v1 );
/* .line 2389 */
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 2390 */
v2 = this.this$0;
v3 = this.mContext;
android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v3,v1 );
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmImperceptiblePowerKey ( v2,v1 );
/* goto/16 :goto_2 */
/* .line 2392 */
} // :cond_2
final String v1 = "long_press_timeout"; // const-string v1, "long_press_timeout"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 2393 */
v2 = this.this$0;
v4 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v2 );
v1 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v3,v4 );
/* iput v1, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyLongPressTimeout:I */
/* goto/16 :goto_2 */
/* .line 2395 */
} // :cond_3
final String v1 = "kid_mode_status"; // const-string v1, "kid_mode_status"
android.provider.Settings$Global .getUriFor ( v1 );
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
final String v4 = "kid_user_id"; // const-string v4, "kid_user_id"
int v5 = 1; // const/4 v5, 0x1
/* if-nez v2, :cond_a */
android.provider.Settings$Secure .getUriFor ( v4 );
/* .line 2396 */
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* goto/16 :goto_0 */
/* .line 2409 */
} // :cond_4
final String v1 = "is_custom_shortcut_effective"; // const-string v1, "is_custom_shortcut_effective"
android.provider.Settings$System .getUriFor ( v1 );
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 2410 */
v2 = this.this$0;
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v4 = -2; // const/4 v4, -0x2
v1 = android.provider.Settings$System .getIntForUser ( v2,v1,v5,v4 );
/* if-ne v1, v5, :cond_5 */
/* move v3, v5 */
} // :cond_5
/* move v1, v3 */
/* .line 2412 */
/* .local v1, "enable":Z */
v2 = this.this$0;
v2 = this.mContext;
com.android.server.policy.MiuiKeyInterceptExtend .getInstance ( v2 );
(( com.android.server.policy.MiuiKeyInterceptExtend ) v2 ).setKeyboardShortcutEnable ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setKeyboardShortcutEnable(Z)V
/* .line 2413 */
} // .end local v1 # "enable":Z
/* goto/16 :goto_2 */
} // :cond_6
/* const-string/jumbo v1, "synergy_mode" */
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 2414 */
v2 = this.this$0;
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = android.provider.Settings$Secure .getInt ( v4,v1,v3 );
/* if-ne v1, v5, :cond_7 */
/* move v3, v5 */
} // :cond_7
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmIsSynergyMode ( v2,v3 );
/* .line 2416 */
v1 = this.this$0;
v1 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmIsSynergyMode ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_d
v1 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmProximitySensor ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_d
v1 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmProximitySensor ( v1 );
v1 = (( com.android.server.policy.MiuiScreenOnProximityLock ) v1 ).isHeld ( ); // invoke-virtual {v1}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->isHeld()Z
if ( v1 != null) { // if-eqz v1, :cond_d
/* .line 2417 */
v1 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$mreleaseScreenOnProximitySensor ( v1,v5 );
/* .line 2419 */
} // :cond_8
final String v1 = "is_mi_input_event_time_line_enable"; // const-string v1, "is_mi_input_event_time_line_enable"
android.provider.Settings$System .getUriFor ( v1 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 2420 */
com.android.server.input.MiuiInputThread .getHandler ( );
/* new-instance v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2428 */
} // :cond_9
/* invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V */
/* .line 2397 */
} // :cond_a
} // :goto_0
v1 = android.provider.Settings$Global .getInt ( v0,v1,v3 );
/* if-ne v1, v5, :cond_b */
} // :cond_b
/* move v5, v3 */
} // :goto_1
/* move v1, v5 */
/* .line 2399 */
/* .local v1, "isKidMode":Z */
/* if-nez v1, :cond_c */
/* .line 2400 */
/* const/16 v2, -0x2710 */
v2 = android.provider.Settings$Secure .getIntForUser ( v0,v4,v2,v3 );
/* .line 2404 */
/* .local v2, "kidSpaceId":I */
v3 = this.this$0;
v3 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmCurrentUserId ( v3 );
/* if-ne v2, v3, :cond_c */
/* .line 2405 */
int v1 = 1; // const/4 v1, 0x1
/* .line 2408 */
} // .end local v2 # "kidSpaceId":I
} // :cond_c
v2 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$mnotifyKidSpaceChanged ( v2,v1 );
/* .line 2409 */
} // .end local v1 # "isKidMode":Z
/* nop */
/* .line 2430 */
} // :cond_d
} // :goto_2
return;
} // .end method
