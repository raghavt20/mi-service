public class com.android.server.policy.MiuiKeyInterceptExtend {
	 /* .source "MiuiKeyInterceptExtend.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;, */
	 /* Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ALIYUN_PACKAGE_NAME;
private static volatile com.android.server.policy.MiuiKeyInterceptExtend INSTANCE;
private static final java.lang.String TAG;
public static final Integer TYPE_INTERCEPT_AOSP;
public static final Integer TYPE_INTERCEPT_MIUI;
public static final Integer TYPE_INTERCEPT_MIUI_AND_AOSP;
public static final Integer TYPE_INTERCEPT_MIUI_AND_AOSP_NO_PASS_TO_USER;
public static final Integer TYPE_INTERCEPT_NULL;
/* # instance fields */
private final android.media.AudioManager mAudioManager;
private final android.content.Context mContext;
private android.os.Handler mHandler;
private volatile Boolean mIsAospKeyboardShortcutEnable;
private volatile Boolean mIsKeyboardShortcutEnable;
private volatile Boolean mIsKidMode;
private Boolean mIsScreenOn;
private java.lang.Runnable mPartialScreenShot;
private final android.os.PowerManager mPowerManager;
private Boolean mScreenOnWhenDown;
private java.lang.Runnable mScreenShot;
private final com.android.server.policy.MiuiKeyInterceptExtend$SettingsObserver mSettingsObserver;
private final java.util.Map mSkipInterceptWindows;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/ArrayList<", */
/* "Landroid/view/KeyboardShortcutInfo;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private Boolean mTriggerLongPress;
/* # direct methods */
public static void $r8$lambda$eg_4qyiJV-YCpjWbzRo2lrXSZaI ( com.android.server.policy.MiuiKeyInterceptExtend p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->lambda$interceptMiuiKeyboard$0()V */
return;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.policy.MiuiKeyInterceptExtend p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Boolean -$$Nest$fgetmTriggerLongPress ( com.android.server.policy.MiuiKeyInterceptExtend p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mTriggerLongPress:Z */
} // .end method
static void -$$Nest$fputmTriggerLongPress ( com.android.server.policy.MiuiKeyInterceptExtend p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mTriggerLongPress:Z */
return;
} // .end method
static com.android.server.policy.MiuiKeyInterceptExtend ( ) {
/* .locals 1 */
/* .line 45 */
int v0 = 0; // const/4 v0, 0x0
return;
} // .end method
private com.android.server.policy.MiuiKeyInterceptExtend ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 102 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 61 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mSkipInterceptWindows = v0;
/* .line 64 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mScreenOnWhenDown:Z */
/* .line 66 */
/* iput-boolean v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsKeyboardShortcutEnable:Z */
/* .line 75 */
/* new-instance v0, Lcom/android/server/policy/MiuiKeyInterceptExtend$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend$1;-><init>(Lcom/android/server/policy/MiuiKeyInterceptExtend;)V */
this.mPartialScreenShot = v0;
/* .line 88 */
/* new-instance v0, Lcom/android/server/policy/MiuiKeyInterceptExtend$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend$2;-><init>(Lcom/android/server/policy/MiuiKeyInterceptExtend;)V */
this.mScreenShot = v0;
/* .line 103 */
com.android.server.input.MiuiInputThread .getHandler ( );
this.mHandler = v0;
/* .line 104 */
this.mContext = p1;
/* .line 105 */
/* invoke-direct {p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->initSystemSpecialWindow()V */
/* .line 106 */
final String v0 = "audio"; // const-string v0, "audio"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
this.mAudioManager = v0;
/* .line 107 */
/* new-instance v0, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;-><init>(Lcom/android/server/policy/MiuiKeyInterceptExtend;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 108 */
(( com.android.server.policy.MiuiKeyInterceptExtend$SettingsObserver ) v0 ).registerObserver ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;->registerObserver()V
/* .line 109 */
final String v0 = "power"; // const-string v0, "power"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/PowerManager; */
this.mPowerManager = v0;
/* .line 110 */
return;
} // .end method
public static com.android.server.policy.MiuiKeyInterceptExtend getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 121 */
v0 = com.android.server.policy.MiuiKeyInterceptExtend.INSTANCE;
/* if-nez v0, :cond_1 */
/* .line 122 */
/* const-class v0, Lcom/android/server/policy/MiuiKeyInterceptExtend; */
/* monitor-enter v0 */
/* .line 123 */
try { // :try_start_0
v1 = com.android.server.policy.MiuiKeyInterceptExtend.INSTANCE;
/* if-nez v1, :cond_0 */
/* .line 124 */
/* new-instance v1, Lcom/android/server/policy/MiuiKeyInterceptExtend; */
/* invoke-direct {v1, p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;-><init>(Landroid/content/Context;)V */
/* .line 126 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 128 */
} // :cond_1
} // :goto_0
v0 = com.android.server.policy.MiuiKeyInterceptExtend.INSTANCE;
} // .end method
private Integer getKeyInterceptType ( com.android.server.policy.MiuiKeyInterceptExtend$INTERCEPT_STAGE p0, android.view.KeyEvent p1, Integer p2, com.android.server.policy.WindowManagerPolicy$WindowState p3 ) {
/* .locals 10 */
/* .param p1, "interceptStage" # Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE; */
/* .param p2, "event" # Landroid/view/KeyEvent; */
/* .param p3, "policyFlags" # I */
/* .param p4, "focusedWin" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .line 273 */
int v0 = 0; // const/4 v0, 0x0
if ( p2 != null) { // if-eqz p2, :cond_e
v1 = (( android.view.KeyEvent ) p2 ).getDeviceId ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I
/* if-ltz v1, :cond_e */
(( android.view.KeyEvent ) p2 ).getDevice ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
if ( v1 != null) { // if-eqz v1, :cond_e
/* .line 274 */
(( android.view.KeyEvent ) p2 ).getDevice ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
v1 = (( android.view.InputDevice ) v1 ).isFullKeyboard ( ); // invoke-virtual {v1}, Landroid/view/InputDevice;->isFullKeyboard()Z
if ( v1 != null) { // if-eqz v1, :cond_e
/* sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z */
/* if-nez v1, :cond_0 */
/* goto/16 :goto_3 */
/* .line 279 */
} // :cond_0
v1 = (( com.android.server.policy.MiuiKeyInterceptExtend ) p0 ).getAospKeyboardShortcutEnable ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getAospKeyboardShortcutEnable()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 281 */
int v0 = 2; // const/4 v0, 0x2
/* .line 284 */
} // :cond_1
/* iget-boolean v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsKidMode:Z */
int v2 = 1; // const/4 v2, 0x1
/* if-nez v1, :cond_b */
v1 = (( com.android.server.policy.MiuiKeyInterceptExtend ) p0 ).getKeyboardShortcutEnable ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getKeyboardShortcutEnable()Z
/* if-nez v1, :cond_2 */
/* goto/16 :goto_2 */
/* .line 299 */
} // :cond_2
/* if-nez p4, :cond_3 */
/* .line 300 */
/* .line 304 */
} // :cond_3
v1 = this.mSkipInterceptWindows;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_8
/* check-cast v3, Ljava/util/Map$Entry; */
/* .line 305 */
/* .local v3, "entry":Ljava/util/Map$Entry; */
/* check-cast v4, Ljava/lang/String; */
/* .line 306 */
/* .local v4, "packageName":Ljava/lang/String; */
/* nop */
/* .line 307 */
/* check-cast v5, Ljava/util/ArrayList; */
/* .line 308 */
/* .local v5, "currentShortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/KeyboardShortcutInfo;>;" */
v6 = (( java.lang.String ) v4 ).equals ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_7
/* .line 309 */
v6 = (( java.util.ArrayList ) v5 ).size ( ); // invoke-virtual {v5}, Ljava/util/ArrayList;->size()I
/* if-nez v6, :cond_4 */
/* .line 311 */
/* .line 313 */
} // :cond_4
final String v6 = "MiuiKeyIntercept"; // const-string v6, "MiuiKeyIntercept"
final String v7 = "Ready intercept meta"; // const-string v7, "Ready intercept meta"
android.util.Slog .i ( v6,v7 );
/* .line 314 */
(( java.util.ArrayList ) v5 ).iterator ( ); // invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v7 = } // :goto_1
if ( v7 != null) { // if-eqz v7, :cond_7
/* check-cast v7, Landroid/view/KeyboardShortcutInfo; */
/* .line 315 */
/* .local v7, "info":Landroid/view/KeyboardShortcutInfo; */
v8 = (( android.view.KeyEvent ) p2 ).getKeyCode ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I
v9 = (( android.view.KeyboardShortcutInfo ) v7 ).getKeycode ( ); // invoke-virtual {v7}, Landroid/view/KeyboardShortcutInfo;->getKeycode()I
/* if-ne v8, v9, :cond_5 */
/* .line 317 */
v8 = (( android.view.KeyEvent ) p2 ).getMetaState ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I
v9 = (( android.view.KeyboardShortcutInfo ) v7 ).getModifiers ( ); // invoke-virtual {v7}, Landroid/view/KeyboardShortcutInfo;->getModifiers()I
/* .line 316 */
v8 = android.view.KeyEvent .metaStateHasModifiers ( v8,v9 );
if ( v8 != null) { // if-eqz v8, :cond_5
/* .line 318 */
/* .line 319 */
} // :cond_5
v8 = (( android.view.KeyboardShortcutInfo ) v7 ).getKeycode ( ); // invoke-virtual {v7}, Landroid/view/KeyboardShortcutInfo;->getKeycode()I
/* const/16 v9, 0x75 */
/* if-ne v9, v8, :cond_6 */
/* .line 320 */
v8 = (( android.view.KeyEvent ) p2 ).getKeyCode ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I
/* if-ne v8, v9, :cond_6 */
/* .line 322 */
/* .line 324 */
} // .end local v7 # "info":Landroid/view/KeyboardShortcutInfo;
} // :cond_6
/* .line 327 */
} // .end local v3 # "entry":Ljava/util/Map$Entry;
} // .end local v4 # "packageName":Ljava/lang/String;
} // .end local v5 # "currentShortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/KeyboardShortcutInfo;>;"
} // :cond_7
/* .line 329 */
} // :cond_8
v1 = this.mContext;
v1 = com.miui.server.input.custom.InputMiuiDesktopMode .getKeyInterceptType ( p1,v1,p2 );
/* .line 331 */
/* .local v1, "desktopModeKeyInterceptType":I */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 332 */
/* .line 335 */
} // :cond_9
v2 = this.mContext;
v2 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .interceptShortCutKeyIfCustomDefined ( v2,p2,p1 );
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 336 */
int v0 = 4; // const/4 v0, 0x4
/* .line 339 */
} // :cond_a
/* .line 287 */
} // .end local v1 # "desktopModeKeyInterceptType":I
} // :cond_b
} // :goto_2
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsScreenOn:Z */
/* if-nez v0, :cond_d */
v0 = com.android.server.policy.MiuiKeyInterceptExtend$INTERCEPT_STAGE.BEFORE_QUEUEING;
/* if-ne p1, v0, :cond_d */
/* .line 289 */
/* and-int/lit8 v0, p3, 0x1 */
/* if-nez v0, :cond_c */
v0 = (( android.view.KeyEvent ) p2 ).isWakeKey ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->isWakeKey()Z
if ( v0 != null) { // if-eqz v0, :cond_d
/* .line 290 */
} // :cond_c
v0 = this.mPowerManager;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
int v1 = 6; // const/4 v1, 0x6
final String v5 = "android.policy:KEY"; // const-string v5, "android.policy:KEY"
(( android.os.PowerManager ) v0 ).wakeUp ( v3, v4, v1, v5 ); // invoke-virtual {v0, v3, v4, v1, v5}, Landroid/os/PowerManager;->wakeUp(JILjava/lang/String;)V
/* .line 296 */
} // :cond_d
/* .line 276 */
} // :cond_e
} // :goto_3
} // .end method
private void initSystemSpecialWindow ( ) {
/* .locals 6 */
/* .line 113 */
/* new-instance v0, Ljava/util/ArrayList; */
/* new-instance v1, Landroid/view/KeyboardShortcutInfo; */
/* const/16 v2, 0x3d */
int v3 = 2; // const/4 v3, 0x2
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {v1, v4, v2, v3}, Landroid/view/KeyboardShortcutInfo;-><init>(Ljava/lang/CharSequence;II)V */
/* new-instance v2, Landroid/view/KeyboardShortcutInfo; */
/* const/16 v3, 0x75 */
/* const/high16 v5, 0x10000 */
/* invoke-direct {v2, v4, v3, v5}, Landroid/view/KeyboardShortcutInfo;-><init>(Ljava/lang/CharSequence;II)V */
/* .line 114 */
java.util.List .of ( v1,v2 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 113 */
final String v1 = "com.aliyun.wuying.enterprise"; // const-string v1, "com.aliyun.wuying.enterprise"
/* const-string/jumbo v2, "system" */
/* invoke-direct {p0, v1, v2, v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->updateSkipInterceptWindowList(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V */
/* .line 118 */
return;
} // .end method
private void lambda$interceptMiuiKeyboard$0 ( ) { //synthethic
/* .locals 1 */
/* .line 216 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mTriggerLongPress:Z */
return;
} // .end method
private void updateSkipInterceptWindowList ( java.lang.String p0, java.lang.String p1, java.util.ArrayList p2 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/ArrayList<", */
/* "Landroid/view/KeyboardShortcutInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 344 */
/* .local p3, "newShortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/KeyboardShortcutInfo;>;" */
if ( p1 != null) { // if-eqz p1, :cond_4
/* if-nez p3, :cond_0 */
/* .line 347 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "skip policy intercept because: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " for "; // const-string v1, " for "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 351 */
v0 = this.mSkipInterceptWindows;
/* check-cast v0, Ljava/util/ArrayList; */
/* .line 353 */
/* .local v0, "currentInterceptShortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/KeyboardShortcutInfo;>;" */
v1 = (( java.util.ArrayList ) p3 ).size ( ); // invoke-virtual {p3}, Ljava/util/ArrayList;->size()I
/* if-nez v1, :cond_1 */
/* .line 355 */
v1 = this.mSkipInterceptWindows;
/* .line 356 */
return;
/* .line 359 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 360 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Recover history special shortcut for "; // const-string v2, "Recover history special shortcut for "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .defaults ( v1 );
/* .line 361 */
v1 = (( java.util.ArrayList ) p3 ).size ( ); // invoke-virtual {p3}, Ljava/util/ArrayList;->size()I
/* if-nez v1, :cond_2 */
/* .line 363 */
v1 = this.mSkipInterceptWindows;
/* .line 364 */
return;
/* .line 366 */
} // :cond_2
(( java.util.ArrayList ) v0 ).addAll ( p3 ); // invoke-virtual {v0, p3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 368 */
} // :cond_3
v1 = this.mSkipInterceptWindows;
/* .line 370 */
} // :goto_0
return;
/* .line 345 */
} // .end local v0 # "currentInterceptShortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/KeyboardShortcutInfo;>;"
} // :cond_4
} // :goto_1
return;
} // .end method
/* # virtual methods */
public Boolean getAospKeyboardShortcutEnable ( ) {
/* .locals 1 */
/* .line 245 */
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsAospKeyboardShortcutEnable:Z */
} // .end method
public Integer getKeyInterceptTypeBeforeDispatching ( android.view.KeyEvent p0, Integer p1, com.android.server.policy.WindowManagerPolicy$WindowState p2 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "policyFlags" # I */
/* .param p3, "focusedWin" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .line 255 */
v0 = com.android.server.policy.MiuiKeyInterceptExtend$INTERCEPT_STAGE.BEFORE_DISPATCHING;
v0 = /* invoke-direct {p0, v0, p1, p2, p3}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getKeyInterceptType(Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;Landroid/view/KeyEvent;ILcom/android/server/policy/WindowManagerPolicy$WindowState;)I */
} // .end method
public Integer getKeyInterceptTypeBeforeQueueing ( android.view.KeyEvent p0, Integer p1, com.android.server.policy.WindowManagerPolicy$WindowState p2 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "policyFlags" # I */
/* .param p3, "focusedWin" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .line 250 */
v0 = com.android.server.policy.MiuiKeyInterceptExtend$INTERCEPT_STAGE.BEFORE_QUEUEING;
v0 = /* invoke-direct {p0, v0, p1, p2, p3}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getKeyInterceptType(Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;Landroid/view/KeyEvent;ILcom/android/server/policy/WindowManagerPolicy$WindowState;)I */
} // .end method
public Boolean getKeyboardShortcutEnable ( ) {
/* .locals 1 */
/* .line 236 */
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsKeyboardShortcutEnable:Z */
} // .end method
public Boolean interceptMiuiKeyboard ( android.view.KeyEvent p0, Boolean p1 ) {
/* .locals 10 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "isScreenOn" # Z */
/* .line 138 */
com.miui.server.input.PadManager .getInstance ( );
v0 = (( com.miui.server.input.PadManager ) v0 ).isPad ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 140 */
v0 = com.android.server.input.InputOneTrackUtil .shouldCountDevice ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 141 */
v0 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v0 );
(( com.android.server.input.InputOneTrackUtil ) v0 ).trackExternalDevice ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/InputOneTrackUtil;->trackExternalDevice(Landroid/view/InputEvent;)V
/* .line 144 */
} // :cond_0
(( android.view.KeyEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
/* .line 145 */
/* .local v0, "device":Landroid/view/InputDevice; */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_b
v2 = (( android.view.InputDevice ) v0 ).getProductId ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->getProductId()I
/* .line 146 */
v3 = (( android.view.InputDevice ) v0 ).getVendorId ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->getVendorId()I
/* .line 145 */
v2 = miui.hardware.input.MiuiKeyboardHelper .isXiaomiKeyboard ( v2,v3 );
/* if-nez v2, :cond_1 */
/* goto/16 :goto_3 */
/* .line 150 */
} // :cond_1
v2 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 151 */
/* .local v2, "keyCode":I */
v3 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
int v4 = 1; // const/4 v4, 0x1
/* if-nez v3, :cond_2 */
/* move v3, v4 */
} // :cond_2
/* move v3, v1 */
/* .line 153 */
/* .local v3, "down":Z */
} // :goto_0
v5 = miui.hardware.input.MiuiKeyEventUtil .matchMiuiKey ( p1 );
/* .line 154 */
/* .local v5, "miuiKeyCode":I */
/* const/16 v6, 0x270b */
/* if-ne v5, v6, :cond_4 */
/* .line 156 */
v4 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v4, :cond_3 */
/* .line 157 */
v4 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v4 );
/* const-string/jumbo v6, "\u5feb\u6377\u8bed\u97f3" */
(( com.android.server.input.InputOneTrackUtil ) v4 ).trackVoice2Word ( v6 ); // invoke-virtual {v4, v6}, Lcom/android/server/input/InputOneTrackUtil;->trackVoice2Word(Ljava/lang/String;)V
/* .line 160 */
} // :cond_3
/* .line 163 */
} // :cond_4
com.miui.server.input.PadManager .getInstance ( );
v6 = (( com.miui.server.input.PadManager ) v6 ).adjustBrightnessFromKeycode ( p1 ); // invoke-virtual {v6, p1}, Lcom/miui/server/input/PadManager;->adjustBrightnessFromKeycode(Landroid/view/KeyEvent;)Z
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 165 */
/* .line 169 */
} // :cond_5
/* if-nez v3, :cond_8 */
/* iget-boolean v6, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mScreenOnWhenDown:Z */
if ( v6 != null) { // if-eqz v6, :cond_8
/* .line 170 */
v6 = miui.hardware.input.MiuiKeyboardHelper .supportFnKeyboard ( );
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 171 */
v6 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v6 );
/* .line 173 */
v7 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 172 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil$KeyBoardShortcut .getShortcutNameByKeyCode ( v7 );
/* .line 171 */
(( com.android.server.input.InputOneTrackUtil ) v6 ).trackKeyboardShortcut ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardShortcut(Ljava/lang/String;)V
/* .line 174 */
} // :cond_6
v6 = miui.hardware.input.MiuiKeyboardHelper .support6FKeyboard ( );
if ( v6 != null) { // if-eqz v6, :cond_7
/* .line 175 */
v6 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v6 );
/* .line 176 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil$KeyBoardShortcut .getShortcutNameByKeyCode ( v5 );
/* .line 175 */
(( com.android.server.input.InputOneTrackUtil ) v6 ).track6FShortcut ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/input/InputOneTrackUtil;->track6FShortcut(Ljava/lang/String;)V
/* .line 179 */
} // :cond_7
} // :goto_1
int v6 = 0; // const/4 v6, 0x0
final String v7 = "keyboard"; // const-string v7, "keyboard"
/* sparse-switch v5, :sswitch_data_0 */
/* .line 209 */
/* .line 186 */
/* :sswitch_0 */
v8 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v8 );
final String v9 = "go_to_sleep"; // const-string v9, "go_to_sleep"
(( com.miui.server.input.util.ShortCutActionsUtils ) v8 ).triggerFunction ( v9, v7, v6, v1 ); // invoke-virtual {v8, v9, v7, v6, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 189 */
/* .line 191 */
/* :sswitch_1 */
v1 = this.mContext;
v1 = android.provider.MiuiSettings$SoundMode .isZenModeOn ( v1 );
/* .line 192 */
/* .local v1, "currentZenMode":Z */
v6 = this.mContext;
/* xor-int/lit8 v8, v1, 0x1 */
android.provider.MiuiSettings$SoundMode .setZenModeOn ( v6,v8,v7 );
/* .line 194 */
/* .line 196 */
} // .end local v1 # "currentZenMode":Z
/* :sswitch_2 */
v1 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v1 );
final String v8 = "launch_voice_assistant"; // const-string v8, "launch_voice_assistant"
(( com.miui.server.input.util.ShortCutActionsUtils ) v1 ).triggerFunction ( v8, v7, v6, v4 ); // invoke-virtual {v1, v8, v7, v6, v4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 199 */
/* .line 201 */
/* :sswitch_3 */
v1 = this.mAudioManager;
v1 = (( android.media.AudioManager ) v1 ).isMicrophoneMute ( ); // invoke-virtual {v1}, Landroid/media/AudioManager;->isMicrophoneMute()Z
/* xor-int/2addr v1, v4 */
/* .line 202 */
/* .local v1, "result":Z */
v6 = this.mAudioManager;
(( android.media.AudioManager ) v6 ).setMicrophoneMute ( v1 ); // invoke-virtual {v6, v1}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V
/* .line 203 */
/* .line 205 */
} // .end local v1 # "result":Z
/* :sswitch_4 */
v1 = this.mHandler;
v6 = this.mPartialScreenShot;
(( android.os.Handler ) v1 ).removeCallbacks ( v6 ); // invoke-virtual {v1, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 206 */
v1 = this.mHandler;
v6 = this.mScreenShot;
(( android.os.Handler ) v1 ).post ( v6 ); // invoke-virtual {v1, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 207 */
/* .line 181 */
/* :sswitch_5 */
com.miui.server.input.PadManager .getInstance ( );
v6 = (( android.view.KeyEvent ) p1 ).isCapsLockOn ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->isCapsLockOn()Z
(( com.miui.server.input.PadManager ) v4 ).setCapsLockStatus ( v6 ); // invoke-virtual {v4, v6}, Lcom/miui/server/input/PadManager;->setCapsLockStatus(Z)V
/* .line 182 */
v4 = this.mContext;
com.android.server.input.padkeyboard.MiuiPadKeyboardManager .getKeyboardManager ( v4 );
/* .line 183 */
v6 = (( android.view.KeyEvent ) p1 ).isCapsLockOn ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->isCapsLockOn()Z
/* .line 182 */
/* .line 184 */
/* .line 212 */
} // :cond_8
/* iput-boolean p2, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mScreenOnWhenDown:Z */
/* .line 213 */
/* if-eq v5, v2, :cond_a */
if ( p2 != null) { // if-eqz p2, :cond_a
/* .line 215 */
/* const/16 v1, 0x270a */
/* if-ne v5, v1, :cond_9 */
/* .line 216 */
v1 = this.mHandler;
/* new-instance v6, Lcom/android/server/policy/MiuiKeyInterceptExtend$$ExternalSyntheticLambda0; */
/* invoke-direct {v6, p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/policy/MiuiKeyInterceptExtend;)V */
(( android.os.Handler ) v1 ).post ( v6 ); // invoke-virtual {v1, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 217 */
v1 = this.mHandler;
v6 = this.mPartialScreenShot;
/* const-wide/16 v7, 0x12c */
(( android.os.Handler ) v1 ).postDelayed ( v6, v7, v8 ); // invoke-virtual {v1, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 219 */
} // :cond_9
/* .line 222 */
} // :cond_a
} // :goto_2
/* .line 148 */
} // .end local v2 # "keyCode":I
} // .end local v3 # "down":Z
} // .end local v5 # "miuiKeyCode":I
} // :cond_b
} // :goto_3
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x73 -> :sswitch_5 */
/* 0x270a -> :sswitch_4 */
/* 0x270c -> :sswitch_3 */
/* 0x270d -> :sswitch_2 */
/* 0x270e -> :sswitch_1 */
/* 0x270f -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public void onSystemBooted ( ) {
/* .locals 1 */
/* .line 373 */
v0 = this.mSettingsObserver;
com.android.server.policy.MiuiKeyInterceptExtend$SettingsObserver .-$$Nest$mprocessCloudData ( v0 );
/* .line 374 */
return;
} // .end method
public void setAospKeyboardShortcutEnable ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isEnable" # Z */
/* .line 240 */
/* iput-boolean p1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsAospKeyboardShortcutEnable:Z */
/* .line 241 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AOSP Keyboard Shortcut status is:"; // const-string v1, "AOSP Keyboard Shortcut status is:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsAospKeyboardShortcutEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiKeyIntercept"; // const-string v1, "MiuiKeyIntercept"
android.util.Slog .i ( v1,v0 );
/* .line 242 */
return;
} // .end method
public void setKeyboardShortcutEnable ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isEnable" # Z */
/* .line 231 */
/* iput-boolean p1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsKeyboardShortcutEnable:Z */
/* .line 232 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "MIUI Keyboard Shortcut status is:"; // const-string v1, "MIUI Keyboard Shortcut status is:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsKeyboardShortcutEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiKeyIntercept"; // const-string v1, "MiuiKeyIntercept"
android.util.Slog .i ( v1,v0 );
/* .line 233 */
return;
} // .end method
public void setKidSpaceMode ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isKidMode" # Z */
/* .line 226 */
/* iput-boolean p1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsKidMode:Z */
/* .line 227 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "KidMode status is:"; // const-string v1, "KidMode status is:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiKeyIntercept"; // const-string v1, "MiuiKeyIntercept"
android.util.Slog .i ( v1,v0 );
/* .line 228 */
return;
} // .end method
public void setScreenState ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isScreenOn" # Z */
/* .line 259 */
/* iput-boolean p1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsScreenOn:Z */
/* .line 260 */
return;
} // .end method
