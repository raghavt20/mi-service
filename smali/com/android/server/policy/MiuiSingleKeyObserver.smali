.class public Lcom/android/server/policy/MiuiSingleKeyObserver;
.super Lcom/android/server/policy/MiuiShortcutObserver;
.source "MiuiSingleKeyObserver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiSingKeyShortcutObserver"


# instance fields
.field private mActionAndDefaultFunction:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mActionAndFunctionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private mMiuiSingleKeyRule:Lcom/android/server/policy/MiuiSingleKeyRule;


# direct methods
.method public static synthetic $r8$lambda$-CUo51W_LnOnKhLb2_ykrm7RGRQ(Lcom/android/server/policy/MiuiSingleKeyObserver;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyObserver;->lambda$registerShortcutAction$1(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$3sFE00JR7ATSeD0bHIbVUTK_3fA(Lcom/android/server/policy/MiuiSingleKeyObserver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/policy/MiuiSingleKeyObserver;->lambda$onChange$2(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$FGfc5UFqbXu9N0-aAf_OjjOoq6Y(Lcom/android/server/policy/MiuiSingleKeyObserver;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyObserver;->lambda$updateRuleInfo$3(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$rTInd3nrtKfWmLRJcIHjTUxmdmQ(Lcom/android/server/policy/MiuiSingleKeyObserver;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyObserver;->lambda$new$0(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Landroid/content/Context;Ljava/util/Map;I)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "context"    # Landroid/content/Context;
    .param p4, "currentUserId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 26
    .local p3, "actionAndDefaultFunctionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2, p4}, Lcom/android/server/policy/MiuiShortcutObserver;-><init>(Landroid/os/Handler;Landroid/content/Context;I)V

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mActionAndFunctionMap:Ljava/util/Map;

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mActionAndDefaultFunction:Ljava/util/Map;

    .line 27
    iput-object p3, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mActionAndDefaultFunction:Ljava/util/Map;

    .line 28
    new-instance v0, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/policy/MiuiSingleKeyObserver;)V

    invoke-interface {p3, v0}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 30
    iput-object p2, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mContext:Landroid/content/Context;

    .line 31
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mContentResolver:Landroid/content/ContentResolver;

    .line 32
    invoke-direct {p0}, Lcom/android/server/policy/MiuiSingleKeyObserver;->registerShortcutAction()V

    .line 33
    return-void
.end method

.method private synthetic lambda$new$0(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;

    .line 29
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mActionAndFunctionMap:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private synthetic lambda$onChange$2(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "function"    # Ljava/lang/String;

    .line 45
    invoke-static {p2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-direct {p0, p2}, Lcom/android/server/policy/MiuiSingleKeyObserver;->updateFunction(Ljava/lang/String;)V

    .line 48
    :cond_0
    return-void
.end method

.method private synthetic lambda$registerShortcutAction$1(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;

    .line 37
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mContentResolver:Landroid/content/ContentResolver;

    .line 38
    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 37
    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    return-void
.end method

.method private synthetic lambda$updateRuleInfo$3(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;

    .line 94
    const/4 v0, 0x0

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/policy/MiuiSingleKeyObserver;->onChange(ZLandroid/net/Uri;)V

    return-void
.end method

.method private registerShortcutAction()V
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mActionAndFunctionMap:Ljava/util/Map;

    new-instance v1, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/policy/MiuiSingleKeyObserver;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 40
    return-void
.end method

.method private updateFunction(Ljava/lang/String;)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;

    .line 99
    invoke-virtual {p0, p1}, Lcom/android/server/policy/MiuiSingleKeyObserver;->currentFunctionValueIsInt(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v1, -0x1

    iget v2, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mCurrentUserId:I

    invoke-static {v0, p1, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 102
    .local v0, "functionStatus":I
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "function":Ljava/lang/String;
    goto :goto_0

    .line 104
    .end local v0    # "function":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mContentResolver:Landroid/content/ContentResolver;

    iget v1, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mCurrentUserId:I

    invoke-static {v0, p1, v1}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 106
    .restart local v0    # "function":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mActionAndFunctionMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 127
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mActionAndFunctionMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 128
    .local v1, "actionAndFunctionEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 129
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 130
    const-string v2, "="

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 131
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 132
    .end local v1    # "actionAndFunctionEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    goto :goto_0

    .line 133
    :cond_0
    return-void
.end method

.method public getActionAndFunctionMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mActionAndFunctionMap:Ljava/util/Map;

    return-object v0
.end method

.method public getFunction(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 116
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mActionAndFunctionMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 44
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mActionAndFunctionMap:Ljava/util/Map;

    new-instance v1, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, p2}, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/policy/MiuiSingleKeyObserver;Landroid/net/Uri;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 49
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mMiuiSingleKeyRule:Lcom/android/server/policy/MiuiSingleKeyRule;

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p0, v0, p2}, Lcom/android/server/policy/MiuiSingleKeyObserver;->notifySingleRuleChanged(Lcom/android/server/policy/MiuiSingleKeyRule;Landroid/net/Uri;)V

    .line 52
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/server/policy/MiuiShortcutObserver;->onChange(ZLandroid/net/Uri;)V

    .line 53
    return-void
.end method

.method public setDefaultFunction(Z)V
    .locals 8
    .param p1, "isNeedReset"    # Z

    .line 58
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mActionAndFunctionMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 59
    .local v1, "actionAndFunctionEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 60
    .local v2, "action":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mActionAndDefaultFunction:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 62
    .local v3, "defaultFunction":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/android/server/policy/MiuiSingleKeyObserver;->currentFunctionValueIsInt(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_1

    .line 63
    iget-object v4, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mContentResolver:Landroid/content/ContentResolver;

    iget v6, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mCurrentUserId:I

    const/4 v7, -0x1

    invoke-static {v4, v2, v7, v6}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v4

    .line 65
    .local v4, "functionStatus":I
    if-ne v4, v7, :cond_0

    move-object v6, v5

    goto :goto_1

    :cond_0
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    :goto_1
    move-object v4, v6

    .line 66
    .local v4, "currentFunction":Ljava/lang/String;
    goto :goto_3

    .line 67
    .end local v4    # "currentFunction":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mContentResolver:Landroid/content/ContentResolver;

    iget v6, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mCurrentUserId:I

    invoke-static {v4, v2, v6}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 69
    .restart local v4    # "currentFunction":Ljava/lang/String;
    if-nez v4, :cond_2

    move-object v6, v3

    goto :goto_2

    :cond_2
    move-object v6, v4

    :goto_2
    move-object v4, v6

    .line 72
    :goto_3
    if-eqz p1, :cond_3

    .line 73
    move-object v4, v3

    .line 75
    :cond_3
    invoke-virtual {p0, v2, v4, p1}, Lcom/android/server/policy/MiuiSingleKeyObserver;->hasCustomizedFunction(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v6

    if-nez v6, :cond_5

    .line 76
    iget-object v6, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v4, v6}, Lcom/android/server/policy/MiuiSingleKeyObserver;->isFeasibleFunction(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 77
    iget-object v5, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mContentResolver:Landroid/content/ContentResolver;

    iget v6, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mCurrentUserId:I

    invoke-static {v5, v2, v4, v6}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    goto :goto_4

    .line 80
    :cond_4
    if-eqz p1, :cond_5

    .line 82
    iget-object v6, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mContentResolver:Landroid/content/ContentResolver;

    iget v7, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mCurrentUserId:I

    invoke-static {v6, v2, v5, v7}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 87
    .end local v1    # "actionAndFunctionEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_5
    :goto_4
    goto :goto_0

    .line 88
    .end local v2    # "action":Ljava/lang/String;
    .end local v3    # "defaultFunction":Ljava/lang/String;
    .end local v4    # "currentFunction":Ljava/lang/String;
    :cond_6
    invoke-super {p0, p1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V

    .line 89
    return-void
.end method

.method public setRuleForObserver(Lcom/android/server/policy/MiuiSingleKeyRule;)V
    .locals 0
    .param p1, "miuiSingleKeyRule"    # Lcom/android/server/policy/MiuiSingleKeyRule;

    .line 121
    iput-object p1, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mMiuiSingleKeyRule:Lcom/android/server/policy/MiuiSingleKeyRule;

    .line 122
    return-void
.end method

.method updateRuleInfo()V
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mActionAndFunctionMap:Ljava/util/Map;

    new-instance v1, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/policy/MiuiSingleKeyObserver;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 95
    return-void
.end method
