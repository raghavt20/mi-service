public class com.android.server.policy.MiuiScreenOnProximityLock {
	 /* .source "MiuiScreenOnProximityLock.java" */
	 /* # static fields */
	 private static final Boolean DEBUG;
	 private static final Integer EVENT_PREPARE_VIEW;
	 private static final Integer EVENT_RELEASE;
	 private static final Integer EVENT_RELEASE_VIEW;
	 private static final Integer EVENT_SET_HINT_CONTAINER;
	 private static final Integer EVENT_SHOW_VIEW;
	 private static final Integer FIRST_CHANGE_TIMEOUT;
	 public static final Boolean IS_JP_KDDI;
	 private static final java.lang.String LOG_TAG;
	 public static final java.lang.String SKIP_AQUIRE_FINGER_WAKE_UP_DETAIL;
	 public static final java.lang.String SKIP_AQUIRE_UNFOLD_WAKE_UP_DETAIL;
	 /* # instance fields */
	 private Long mAquiredTime;
	 private android.content.Context mContext;
	 protected Boolean mFrontFingerprintSensor;
	 private android.os.Handler mHandler;
	 private Boolean mHideNavigationBarWhenForceShow;
	 protected android.view.ViewGroup mHintContainer;
	 protected android.view.View mHintView;
	 protected com.android.server.policy.MiuiKeyguardServiceDelegate mKeyguardDelegate;
	 private com.android.server.input.pocketmode.MiuiPocketModeManager mMiuiPocketModeManager;
	 private final com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper$ProximitySensorChangeListener mSensorListener;
	 /* # direct methods */
	 static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.policy.MiuiScreenOnProximityLock p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mHandler;
	 } // .end method
	 static void -$$Nest$mforceShow ( com.android.server.policy.MiuiScreenOnProximityLock p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->forceShow()V */
		 return;
	 } // .end method
	 static void -$$Nest$mprepareHintWindow ( com.android.server.policy.MiuiScreenOnProximityLock p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->prepareHintWindow()V */
		 return;
	 } // .end method
	 static void -$$Nest$mreleaseHintWindow ( com.android.server.policy.MiuiScreenOnProximityLock p0, Boolean p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->releaseHintWindow(Z)V */
		 return;
	 } // .end method
	 static void -$$Nest$mreleaseReset ( com.android.server.policy.MiuiScreenOnProximityLock p0, android.view.View p1, android.view.View p2 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->releaseReset(Landroid/view/View;Landroid/view/View;)V */
		 return;
	 } // .end method
	 static void -$$Nest$msetHintContainer ( com.android.server.policy.MiuiScreenOnProximityLock p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->setHintContainer()V */
		 return;
	 } // .end method
	 static void -$$Nest$mshowHint ( com.android.server.policy.MiuiScreenOnProximityLock p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->showHint()V */
		 return;
	 } // .end method
	 static com.android.server.policy.MiuiScreenOnProximityLock ( ) {
		 /* .locals 1 */
		 /* .line 57 */
		 /* sget-boolean v0, Lmiui/hardware/input/InputFeature;->IS_SUPPORT_KDDI:Z */
		 com.android.server.policy.MiuiScreenOnProximityLock.IS_JP_KDDI = (v0!= 0);
		 return;
	 } // .end method
	 public com.android.server.policy.MiuiScreenOnProximityLock ( ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "keyguardDelegate" # Lcom/android/server/policy/MiuiKeyguardServiceDelegate; */
		 /* .param p3, "looper" # Landroid/os/Looper; */
		 /* .line 83 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 68 */
		 /* const-wide/16 v0, 0x0 */
		 /* iput-wide v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mAquiredTime:J */
		 /* .line 72 */
		 /* new-instance v0, Lcom/android/server/policy/MiuiScreenOnProximityLock$1; */
		 /* invoke-direct {v0, p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock$1;-><init>(Lcom/android/server/policy/MiuiScreenOnProximityLock;)V */
		 this.mSensorListener = v0;
		 /* .line 84 */
		 this.mContext = p1;
		 /* .line 85 */
		 this.mKeyguardDelegate = p2;
		 /* .line 86 */
		 /* new-instance v0, Lcom/android/server/policy/MiuiScreenOnProximityLock$2; */
		 /* invoke-direct {v0, p0, p3}, Lcom/android/server/policy/MiuiScreenOnProximityLock$2;-><init>(Lcom/android/server/policy/MiuiScreenOnProximityLock;Landroid/os/Looper;)V */
		 this.mHandler = v0;
		 /* .line 115 */
		 /* nop */
		 /* .line 116 */
		 final String v0 = "front_fingerprint_sensor"; // const-string v0, "front_fingerprint_sensor"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 miui.util.FeatureParser .getBoolean ( v0,v1 );
		 /* iput-boolean v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mFrontFingerprintSensor:Z */
		 /* .line 117 */
		 /* new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager; */
		 v1 = this.mContext;
		 /* invoke-direct {v0, v1}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;-><init>(Landroid/content/Context;)V */
		 this.mMiuiPocketModeManager = v0;
		 /* .line 118 */
		 return;
	 } // .end method
	 private void forceShow ( ) {
		 /* .locals 2 */
		 /* .line 199 */
		 v0 = this.mHandler;
		 int v1 = 4; // const/4 v1, 0x4
		 (( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
		 (( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
		 /* .line 200 */
		 return;
	 } // .end method
	 private void modTipsForKddi ( ) {
		 /* .locals 3 */
		 /* .line 323 */
		 /* sget-boolean v0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->IS_JP_KDDI:Z */
		 /* if-nez v0, :cond_0 */
		 /* .line 324 */
		 return;
		 /* .line 326 */
	 } // :cond_0
	 v0 = this.mHintView;
	 /* .line 327 */
	 /* const v1, 0x110a00c0 */
	 (( android.view.View ) v0 ).findViewById ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;
	 /* check-cast v0, Landroid/widget/TextView; */
	 /* .line 328 */
	 /* .local v0, "summaryTextView":Landroid/widget/TextView; */
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 329 */
		 /* nop */
		 /* .line 330 */
		 /* const v1, 0x110f0336 */
		 (( android.widget.TextView ) v0 ).setText ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V
		 /* .line 332 */
	 } // :cond_1
	 v1 = this.mHintView;
	 /* .line 333 */
	 /* const v2, 0x110a00bf */
	 (( android.view.View ) v1 ).findViewById ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;
	 /* check-cast v1, Landroid/widget/TextView; */
	 /* .line 334 */
	 /* .local v1, "hintHasNavigationBarTextView":Landroid/widget/TextView; */
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 335 */
		 /* nop */
		 /* .line 336 */
		 /* const v2, 0x110f0334 */
		 (( android.widget.TextView ) v1 ).setText ( v2 ); // invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V
		 /* .line 338 */
	 } // :cond_2
	 return;
} // .end method
private void prepareHintWindow ( ) {
	 /* .locals 8 */
	 /* .line 203 */
	 /* new-instance v0, Landroid/widget/FrameLayout; */
	 /* new-instance v1, Landroid/view/ContextThemeWrapper; */
	 v2 = this.mContext;
	 /* const v3, 0x103006b */
	 /* invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V */
	 /* invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V */
	 this.mHintContainer = v0;
	 /* .line 204 */
	 /* new-instance v1, Lcom/android/server/policy/MiuiScreenOnProximityLock$3; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock$3;-><init>(Lcom/android/server/policy/MiuiScreenOnProximityLock;)V */
	 (( android.view.ViewGroup ) v0 ).setOnTouchListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V
	 /* .line 212 */
	 /* new-instance v0, Landroid/view/WindowManager$LayoutParams; */
	 int v3 = -1; // const/4 v3, -0x1
	 int v4 = -1; // const/4 v4, -0x1
	 /* const/16 v5, 0x7e2 */
	 /* const v6, 0x1831100 */
	 int v7 = -3; // const/4 v7, -0x3
	 /* move-object v2, v0 */
	 /* invoke-direct/range {v2 ..v7}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V */
	 /* .line 224 */
	 /* .local v0, "lp":Landroid/view/WindowManager$LayoutParams; */
	 /* iget v1, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I */
	 /* or-int/lit8 v1, v1, 0x2 */
	 /* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I */
	 /* .line 225 */
	 int v1 = 1; // const/4 v1, 0x1
	 /* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
	 /* .line 226 */
	 /* const/16 v1, 0x11 */
	 /* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I */
	 /* .line 227 */
	 final String v1 = "ScreenOnProximitySensorGuide"; // const-string v1, "ScreenOnProximitySensorGuide"
	 (( android.view.WindowManager$LayoutParams ) v0 ).setTitle ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
	 /* .line 228 */
	 v1 = this.mContext;
	 /* const-string/jumbo v2, "window" */
	 (( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v1, Landroid/view/WindowManager; */
	 /* .line 229 */
	 /* .local v1, "wm":Landroid/view/WindowManager; */
	 v2 = this.mHintContainer;
	 /* .line 231 */
	 v2 = this.mKeyguardDelegate;
	 int v3 = 0; // const/4 v3, 0x0
	 (( com.android.server.policy.MiuiKeyguardServiceDelegate ) v2 ).enableUserActivity ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->enableUserActivity(Z)V
	 /* .line 232 */
	 return;
} // .end method
private void releaseHintWindow ( Boolean p0 ) {
	 /* .locals 5 */
	 /* .param p1, "isNowRelease" # Z */
	 /* .line 235 */
	 v0 = this.mHintContainer;
	 /* .line 236 */
	 /* .local v0, "container":Landroid/view/View; */
	 /* if-nez v0, :cond_0 */
	 return;
	 /* .line 238 */
} // :cond_0
v1 = this.mHintView;
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_1 */
/* .line 239 */
v1 = this.mContext;
/* const-string/jumbo v3, "window" */
(( android.content.Context ) v1 ).getSystemService ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/view/WindowManager; */
/* .line 240 */
/* .local v1, "wm":Landroid/view/WindowManager; */
/* .line 241 */
} // .end local v1 # "wm":Landroid/view/WindowManager;
/* .line 242 */
} // :cond_1
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 243 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->releaseReset(Landroid/view/View;Landroid/view/View;)V */
/* .line 244 */
this.mHintView = v2;
/* .line 246 */
} // :cond_2
v3 = android.view.View.ALPHA;
int v4 = 2; // const/4 v4, 0x2
/* new-array v4, v4, [F */
/* fill-array-data v4, :array_0 */
android.animation.ObjectAnimator .ofFloat ( v1,v3,v4 );
/* .line 247 */
/* .local v1, "animator":Landroid/animation/ObjectAnimator; */
/* const-wide/16 v3, 0x1f4 */
(( android.animation.ObjectAnimator ) v1 ).setDuration ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;
/* .line 248 */
/* new-instance v3, Lcom/android/server/policy/MiuiScreenOnProximityLock$4; */
/* invoke-direct {v3, p0, v0}, Lcom/android/server/policy/MiuiScreenOnProximityLock$4;-><init>(Lcom/android/server/policy/MiuiScreenOnProximityLock;Landroid/view/View;)V */
(( android.animation.ObjectAnimator ) v1 ).addListener ( v3 ); // invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V
/* .line 267 */
(( android.animation.ObjectAnimator ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V
/* .line 268 */
this.mHintView = v2;
/* .line 272 */
} // .end local v1 # "animator":Landroid/animation/ObjectAnimator;
} // :goto_0
v1 = this.mKeyguardDelegate;
v1 = (( com.android.server.policy.MiuiKeyguardServiceDelegate ) v1 ).isShowingAndNotHidden ( ); // invoke-virtual {v1}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z
/* if-nez v1, :cond_3 */
/* .line 273 */
v1 = this.mKeyguardDelegate;
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.policy.MiuiKeyguardServiceDelegate ) v1 ).enableUserActivity ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->enableUserActivity(Z)V
/* .line 275 */
} // :cond_3
this.mHintContainer = v2;
/* .line 276 */
return;
/* :array_0 */
/* .array-data 4 */
/* 0x3f800000 # 1.0f */
/* 0x0 */
} // .end array-data
} // .end method
private void releaseReset ( android.view.View p0, android.view.View p1 ) {
/* .locals 2 */
/* .param p1, "container" # Landroid/view/View; */
/* .param p2, "hintView" # Landroid/view/View; */
/* .line 279 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 280 */
/* const/16 v0, 0x8 */
(( android.view.View ) p2 ).setVisibility ( v0 ); // invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V
/* .line 281 */
(( android.view.View ) p2 ).clearAnimation ( ); // invoke-virtual {p2}, Landroid/view/View;->clearAnimation()V
/* .line 284 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHideNavigationBarWhenForceShow:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 285 */
/* const/16 v0, 0xf00 */
(( android.view.View ) p1 ).setSystemUiVisibility ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V
/* .line 290 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHideNavigationBarWhenForceShow:Z */
/* .line 292 */
} // :cond_1
v0 = this.mContext;
/* const-string/jumbo v1, "window" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/view/WindowManager; */
/* .line 293 */
/* .local v0, "wm":Landroid/view/WindowManager; */
/* .line 294 */
return;
} // .end method
private void setHintContainer ( ) {
/* .locals 4 */
/* .line 185 */
v0 = this.mHintContainer;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( android.view.ViewGroup ) v0 ).getSystemUiVisibility ( ); // invoke-virtual {v0}, Landroid/view/ViewGroup;->getSystemUiVisibility()I
/* and-int/lit8 v0, v0, 0x4 */
/* if-nez v0, :cond_0 */
/* .line 186 */
v0 = this.mHintContainer;
/* const/16 v1, 0xf02 */
(( android.view.ViewGroup ) v0 ).setSystemUiVisibility ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V
/* .line 192 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHideNavigationBarWhenForceShow:Z */
/* .line 195 */
} // :cond_0
v0 = this.mHandler;
v1 = this.mMiuiPocketModeManager;
v1 = (( com.android.server.input.pocketmode.MiuiPocketModeManager ) v1 ).getStateStableDelay ( ); // invoke-virtual {v1}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->getStateStableDelay()I
/* int-to-long v1, v1 */
int v3 = 2; // const/4 v3, 0x2
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 196 */
return;
} // .end method
private Boolean shouldBeBlockedInternal ( android.view.KeyEvent p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "ScreenOnFully" # Z */
/* .line 158 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_1
v1 = (( com.android.server.policy.MiuiScreenOnProximityLock ) p0 ).isHeld ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->isHeld()Z
if ( v1 != null) { // if-eqz v1, :cond_1
if ( p2 != null) { // if-eqz p2, :cond_1
v1 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
/* .line 162 */
} // :cond_0
v1 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* sparse-switch v1, :sswitch_data_0 */
/* .line 181 */
/* .line 175 */
/* :sswitch_0 */
/* .line 165 */
/* :sswitch_1 */
v0 = this.mContext;
final String v1 = "audio"; // const-string v1, "audio"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
/* .line 166 */
/* .local v0, "audioManager":Landroid/media/AudioManager; */
v1 = (( android.media.AudioManager ) v0 ).isMusicActive ( ); // invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z
/* xor-int/2addr v1, v2 */
/* .line 177 */
} // .end local v0 # "audioManager":Landroid/media/AudioManager;
/* :sswitch_2 */
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mFrontFingerprintSensor:Z */
/* xor-int/2addr v0, v2 */
/* .line 159 */
} // :cond_1
} // :goto_0
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x3 -> :sswitch_2 */
/* 0x18 -> :sswitch_1 */
/* 0x19 -> :sswitch_1 */
/* 0x1a -> :sswitch_0 */
/* 0x4f -> :sswitch_0 */
/* 0x55 -> :sswitch_0 */
/* 0x56 -> :sswitch_0 */
/* 0x57 -> :sswitch_0 */
/* 0x7e -> :sswitch_0 */
/* 0x7f -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private void showHint ( ) {
/* .locals 7 */
/* .line 297 */
v0 = (( com.android.server.policy.MiuiScreenOnProximityLock ) p0 ).isHeld ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->isHeld()Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mHintView;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 301 */
} // :cond_0
final String v0 = "MiuiScreenOnProximityLock"; // const-string v0, "MiuiScreenOnProximityLock"
/* const-string/jumbo v1, "show hint..." */
android.util.Slog .d ( v0,v1 );
/* .line 302 */
/* new-instance v0, Landroid/view/ContextThemeWrapper; */
v1 = this.mContext;
/* const v2, 0x103006b */
/* invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V */
/* const v1, 0x110c0037 */
v2 = this.mHintContainer;
android.view.View .inflate ( v0,v1,v2 );
this.mHintView = v0;
/* .line 306 */
/* invoke-direct {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->modTipsForKddi()V */
/* .line 307 */
v0 = this.mHintView;
v1 = android.view.View.ALPHA;
int v2 = 2; // const/4 v2, 0x2
/* new-array v3, v2, [F */
/* fill-array-data v3, :array_0 */
android.animation.ObjectAnimator .ofFloat ( v0,v1,v3 );
/* .line 308 */
/* .local v0, "animator":Landroid/animation/ObjectAnimator; */
/* const-wide/16 v3, 0x1f4 */
(( android.animation.ObjectAnimator ) v0 ).setDuration ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;
/* .line 309 */
(( android.animation.ObjectAnimator ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V
/* .line 311 */
/* new-instance v1, Landroid/view/animation/AlphaAnimation; */
/* const/high16 v5, 0x3f800000 # 1.0f */
int v6 = 0; // const/4 v6, 0x0
/* invoke-direct {v1, v5, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* .line 312 */
/* .local v1, "animation":Landroid/view/animation/Animation; */
(( android.view.animation.Animation ) v1 ).setDuration ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 313 */
int v5 = -1; // const/4 v5, -0x1
(( android.view.animation.Animation ) v1 ).setRepeatCount ( v5 ); // invoke-virtual {v1, v5}, Landroid/view/animation/Animation;->setRepeatCount(I)V
/* .line 314 */
(( android.view.animation.Animation ) v1 ).setRepeatMode ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setRepeatMode(I)V
/* .line 315 */
(( android.view.animation.Animation ) v1 ).setStartOffset ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/view/animation/Animation;->setStartOffset(J)V
/* .line 317 */
v2 = this.mHintView;
/* const v3, 0x110a00be */
(( android.view.View ) v2 ).findViewById ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* .line 318 */
/* .local v2, "animationView":Landroid/view/View; */
(( android.view.View ) v2 ).startAnimation ( v1 ); // invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V
/* .line 319 */
return;
/* .line 298 */
} // .end local v0 # "animator":Landroid/animation/ObjectAnimator;
} // .end local v1 # "animation":Landroid/view/animation/Animation;
} // .end local v2 # "animationView":Landroid/view/View;
} // :cond_1
} // :goto_0
return;
/* :array_0 */
/* .array-data 4 */
/* 0x0 */
/* 0x3f800000 # 1.0f */
} // .end array-data
} // .end method
/* # virtual methods */
public synchronized void aquire ( ) {
/* .locals 4 */
/* monitor-enter p0 */
/* .line 125 */
try { // :try_start_0
v0 = (( com.android.server.policy.MiuiScreenOnProximityLock ) p0 ).isHeld ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->isHeld()Z
/* if-nez v0, :cond_0 */
/* .line 126 */
final String v0 = "MiuiScreenOnProximityLock"; // const-string v0, "MiuiScreenOnProximityLock"
final String v1 = "aquire"; // const-string v1, "aquire"
android.util.Slog .d ( v0,v1 );
/* .line 127 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mAquiredTime:J */
/* .line 128 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 129 */
v0 = this.mHandler;
int v1 = 0; // const/4 v1, 0x0
/* const-wide/16 v2, 0x5dc */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 130 */
v0 = this.mMiuiPocketModeManager;
v1 = this.mSensorListener;
(( com.android.server.input.pocketmode.MiuiPocketModeManager ) v0 ).registerListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->registerListener(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 132 */
} // .end local p0 # "this":Lcom/android/server/policy/MiuiScreenOnProximityLock;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 124 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public synchronized Boolean isHeld ( ) {
/* .locals 4 */
/* monitor-enter p0 */
/* .line 121 */
try { // :try_start_0
/* iget-wide v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mAquiredTime:J */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* monitor-exit p0 */
/* .line 121 */
} // .end local p0 # "this":Lcom/android/server/policy/MiuiScreenOnProximityLock;
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public synchronized Boolean release ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "isNowRelease" # Z */
/* monitor-enter p0 */
/* .line 135 */
try { // :try_start_0
v0 = (( com.android.server.policy.MiuiScreenOnProximityLock ) p0 ).isHeld ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->isHeld()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 136 */
final String v0 = "MiuiScreenOnProximityLock"; // const-string v0, "MiuiScreenOnProximityLock"
final String v1 = "release"; // const-string v1, "release"
android.util.Slog .d ( v0,v1 );
/* .line 137 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mAquiredTime:J */
/* .line 138 */
v0 = this.mMiuiPocketModeManager;
(( com.android.server.input.pocketmode.MiuiPocketModeManager ) v0 ).unregisterListener ( ); // invoke-virtual {v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->unregisterListener()V
/* .line 139 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 140 */
v0 = this.mHandler;
int v2 = 2; // const/4 v2, 0x2
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 141 */
v0 = this.mHandler;
int v2 = 3; // const/4 v2, 0x3
(( android.os.Handler ) v0 ).obtainMessage ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 142 */
/* .local v0, "releaseViewMessage":Landroid/os/Message; */
java.lang.Boolean .valueOf ( p1 );
this.obj = v2;
/* .line 143 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).sendMessage ( v0 ); // invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 144 */
/* monitor-exit p0 */
/* .line 146 */
} // .end local v0 # "releaseViewMessage":Landroid/os/Message;
} // .end local p0 # "this":Lcom/android/server/policy/MiuiScreenOnProximityLock;
} // :cond_0
/* monitor-exit p0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 134 */
} // .end local p1 # "isNowRelease":Z
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public Boolean shouldBeBlocked ( Boolean p0, android.view.KeyEvent p1 ) {
/* .locals 1 */
/* .param p1, "ScreenOnFully" # Z */
/* .param p2, "event" # Landroid/view/KeyEvent; */
/* .line 150 */
v0 = /* invoke-direct {p0, p2, p1}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->shouldBeBlockedInternal(Landroid/view/KeyEvent;Z)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 151 */
/* invoke-direct {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->forceShow()V */
/* .line 152 */
int v0 = 1; // const/4 v0, 0x1
/* .line 154 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
