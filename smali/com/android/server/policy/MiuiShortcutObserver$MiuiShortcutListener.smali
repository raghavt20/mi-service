.class public interface abstract Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;
.super Ljava/lang/Object;
.source "MiuiShortcutObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/policy/MiuiShortcutObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MiuiShortcutListener"
.end annotation


# virtual methods
.method public onCombinationChanged(Lcom/android/server/policy/MiuiCombinationRule;)V
    .locals 0
    .param p1, "rule"    # Lcom/android/server/policy/MiuiCombinationRule;

    .line 237
    return-void
.end method

.method public onGestureChanged(Lcom/android/server/policy/MiuiGestureRule;)V
    .locals 0
    .param p1, "rule"    # Lcom/android/server/policy/MiuiGestureRule;

    .line 245
    return-void
.end method

.method public onSingleChanged(Lcom/android/server/policy/MiuiSingleKeyRule;Landroid/net/Uri;)V
    .locals 0
    .param p1, "rule"    # Lcom/android/server/policy/MiuiSingleKeyRule;
    .param p2, "uri"    # Landroid/net/Uri;

    .line 241
    return-void
.end method
