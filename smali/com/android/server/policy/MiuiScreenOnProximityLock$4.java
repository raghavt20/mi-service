class com.android.server.policy.MiuiScreenOnProximityLock$4 implements android.animation.Animator$AnimatorListener {
	 /* .source "MiuiScreenOnProximityLock.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/policy/MiuiScreenOnProximityLock;->releaseHintWindow(Z)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.policy.MiuiScreenOnProximityLock this$0; //synthetic
final android.view.View val$container; //synthetic
/* # direct methods */
 com.android.server.policy.MiuiScreenOnProximityLock$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/policy/MiuiScreenOnProximityLock; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 248 */
this.this$0 = p1;
this.val$container = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAnimationCancel ( android.animation.Animator p0 ) {
/* .locals 0 */
/* .param p1, "animation" # Landroid/animation/Animator; */
/* .line 265 */
return;
} // .end method
public void onAnimationEnd ( android.animation.Animator p0 ) {
/* .locals 3 */
/* .param p1, "animation" # Landroid/animation/Animator; */
/* .line 259 */
/* move-object v0, p1 */
/* check-cast v0, Landroid/animation/ObjectAnimator; */
(( android.animation.ObjectAnimator ) v0 ).getTarget ( ); // invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getTarget()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
/* .line 260 */
/* .local v0, "hintView":Landroid/view/View; */
v1 = this.this$0;
v2 = this.val$container;
com.android.server.policy.MiuiScreenOnProximityLock .-$$Nest$mreleaseReset ( v1,v2,v0 );
/* .line 261 */
return;
} // .end method
public void onAnimationRepeat ( android.animation.Animator p0 ) {
/* .locals 0 */
/* .param p1, "animation" # Landroid/animation/Animator; */
/* .line 255 */
return;
} // .end method
public void onAnimationStart ( android.animation.Animator p0 ) {
/* .locals 0 */
/* .param p1, "animation" # Landroid/animation/Animator; */
/* .line 251 */
return;
} // .end method
