.class Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;
.super Landroid/view/IDisplayFoldListener$Stub;
.source "BaseMiuiPhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    .line 623
    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-direct {p0}, Landroid/view/IDisplayFoldListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onDisplayFoldChanged(IZ)V
    .locals 3
    .param p1, "displayId"    # I
    .param p2, "folded"    # Z

    .line 626
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmFolded(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, p2, :cond_0

    .line 627
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmFolded(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V

    .line 628
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmIsFoldChanged(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V

    .line 629
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$msetTouchFeatureRotation(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    .line 631
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "display changed,display="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " fold="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " mIsFoldChanged="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmIsFoldChanged(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 634
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmMiuiBackTapGestureService(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/miui/server/input/MiuiBackTapGestureService;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 635
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmMiuiBackTapGestureService(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/miui/server/input/MiuiBackTapGestureService;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/miui/server/input/MiuiBackTapGestureService;->notifyFoldStatus(Z)V

    .line 637
    :cond_1
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmProximitySensor(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/policy/MiuiScreenOnProximityLock;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 638
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmProximitySensor(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/policy/MiuiScreenOnProximityLock;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->release(Z)Z

    .line 640
    :cond_2
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v0, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiThreeGestureListener:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    if-eqz v0, :cond_3

    .line 641
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v0, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiThreeGestureListener:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-virtual {v0, p2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->notifyFoldStatus(Z)V

    .line 643
    :cond_3
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v0, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->notifyFoldStatus(Z)V

    .line 644
    return-void
.end method
