.class Lcom/android/server/policy/MiuiPhoneWindowManager$2;
.super Ljava/lang/Object;
.source "MiuiPhoneWindowManager.java"

# interfaces
.implements Lmiui/view/MiuiSecurityPermissionHandler$PermissionViewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/policy/MiuiPhoneWindowManager;->systemReady()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/server/policy/MiuiPhoneWindowManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/policy/MiuiPhoneWindowManager;

    .line 183
    iput-object p1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$2;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAddAccount()V
    .locals 2

    .line 193
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$2;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->-$$Nest$fgetmAccountHelper(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lcom/android/server/wm/AccountHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$2;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    iget-object v1, v1, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/AccountHelper;->addAccount(Landroid/content/Context;)V

    .line 194
    return-void
.end method

.method public onHideWaterMarker()V
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$2;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->-$$Nest$fgetmPhoneWindowCallback(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$2;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->-$$Nest$fgetmPhoneWindowCallback(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;->onHideWatermark()V

    .line 212
    :cond_0
    return-void
.end method

.method public onListenAccount(I)V
    .locals 1
    .param p1, "mode"    # I

    .line 199
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$2;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->-$$Nest$fgetmAccountHelper(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lcom/android/server/wm/AccountHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/AccountHelper;->ListenAccount(I)V

    .line 200
    return-void
.end method

.method public onListenPermission()V
    .locals 0

    .line 217
    return-void
.end method

.method public onShowWaterMarker()V
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$2;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->-$$Nest$fgetmPhoneWindowCallback(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$2;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->-$$Nest$fgetmPhoneWindowCallback(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;->onShowWatermark()V

    .line 189
    :cond_0
    return-void
.end method

.method public onUnListenAccount(I)V
    .locals 1
    .param p1, "mode"    # I

    .line 204
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$2;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->-$$Nest$fgetmAccountHelper(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lcom/android/server/wm/AccountHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/AccountHelper;->UnListenAccount(I)V

    .line 205
    return-void
.end method
