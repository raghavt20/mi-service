.class Lcom/android/server/policy/MiuiPhoneWindowManager$1;
.super Ljava/lang/Object;
.source "MiuiPhoneWindowManager.java"

# interfaces
.implements Lcom/android/server/wm/AccountHelper$AccountCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/policy/MiuiPhoneWindowManager;->systemReady()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/server/policy/MiuiPhoneWindowManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/policy/MiuiPhoneWindowManager;

    .line 160
    iput-object p1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$1;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onWifiSettingFinish()V
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$1;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->-$$Nest$fgetmMiuiSecurityPermissionHandler(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lmiui/view/MiuiSecurityPermissionHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$1;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->-$$Nest$fgetmMiuiSecurityPermissionHandler(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lmiui/view/MiuiSecurityPermissionHandler;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/view/MiuiSecurityPermissionHandler;->handleWifiSettingFinish()V

    .line 180
    :cond_0
    return-void
.end method

.method public onXiaomiAccountLogin()V
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$1;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->-$$Nest$fgetmMiuiSecurityPermissionHandler(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lmiui/view/MiuiSecurityPermissionHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$1;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->-$$Nest$fgetmMiuiSecurityPermissionHandler(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lmiui/view/MiuiSecurityPermissionHandler;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/view/MiuiSecurityPermissionHandler;->handleAccountLogin()V

    .line 166
    :cond_0
    return-void
.end method

.method public onXiaomiAccountLogout()V
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$1;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->-$$Nest$fgetmMiuiSecurityPermissionHandler(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lmiui/view/MiuiSecurityPermissionHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager$1;->this$0:Lcom/android/server/policy/MiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->-$$Nest$fgetmMiuiSecurityPermissionHandler(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lmiui/view/MiuiSecurityPermissionHandler;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/view/MiuiSecurityPermissionHandler;->handleAccountLogout()V

    .line 173
    :cond_0
    return-void
.end method
