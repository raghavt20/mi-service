.class Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;
.super Ljava/lang/Object;
.source "BaseMiuiPhoneWindowManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    .line 1138
    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1141
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmMetaKeyConsume(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1142
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmMetaKeyConsume(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V

    .line 1143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mMetaKeyAction : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmMetaKeyAction(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 1144
    const/4 v0, 0x0

    .line 1145
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmMetaKeyAction(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v2

    const-string v3, "com.miui.home"

    if-nez v2, :cond_0

    .line 1146
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.miui.home.action.SINGLE_CLICK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 1147
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1148
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v1, v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v1

    const-string/jumbo v2, "\u663e\u793adock"

    invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardShortcut(Ljava/lang/String;)V

    goto :goto_0

    .line 1150
    :cond_0
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmMetaKeyAction(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v2

    if-ne v2, v1, :cond_1

    .line 1151
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.miui.home.action.DOUBLE_CLICK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 1152
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1153
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v1, v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v1

    const-string/jumbo v2, "\u5feb\u5207\u6700\u8fd1\u5e94\u7528"

    invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardShortcut(Ljava/lang/String;)V

    goto :goto_0

    .line 1155
    :cond_1
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmMetaKeyAction(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 1156
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmKeyBoardDeviceId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v3

    invoke-static {v2, v3, v1, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$mshowKeyboardShortcutsMenu(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;IZZ)V

    .line 1157
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v1, v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v1

    const-string/jumbo v2, "\u663e\u793a\u5feb\u6377\u952e\u5217\u8868"

    invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardShortcut(Ljava/lang/String;)V

    .line 1160
    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    .line 1161
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const-string v2, "miui.permission.USE_INTERNAL_GENERAL_API"

    invoke-virtual {v1, v0, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 1164
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    return-void
.end method
