public class com.android.server.policy.MiuiPhoneWindowManager extends com.android.server.policy.BaseMiuiPhoneWindowManager {
	 /* .source "MiuiPhoneWindowManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ACTION_NOT_PASS_TO_USER;
private static final Integer ACTION_PASS_TO_USER;
private static final java.lang.String CAMERA_COVERED_SERVICE;
private static final Integer FINGERPRINT_NAV_ACTION_DEFAULT;
private static final Integer FINGERPRINT_NAV_ACTION_HOME;
private static final Integer FINGERPRINT_NAV_ACTION_NONE;
protected static final Integer NAV_BAR_BOTTOM;
protected static final Integer NAV_BAR_LEFT;
protected static final Integer NAV_BAR_RIGHT;
private static final java.lang.String PERMISSION_INTERNAL_GENERAL_API;
private static final Boolean SUPPORT_POWERFP;
private static final java.lang.String XIAOMI_MIRROR_APP;
/* # instance fields */
private Long interceptPowerKeyTimeByDpadCenter;
private com.android.server.wm.AccountHelper mAccountHelper;
private android.hardware.biometrics.BiometricManager mBiometricManager;
private com.miui.app.MiuiCameraCoveredManagerServiceInternal mCameraCoveredService;
private Integer mDisplayHeight;
private Integer mDisplayRotation;
private Integer mDisplayWidth;
private android.hardware.fingerprint.IFingerprintService mFingerprintService;
private android.app.AlertDialog mFpNavCenterActionChooseDialog;
private java.lang.reflect.Method mGetFpLockoutModeMethod;
private miui.view.MiuiSecurityPermissionHandler mMiuiSecurityPermissionHandler;
private com.android.server.policy.MiuiPhoneWindowManager$MIUIWatermarkCallback mPhoneWindowCallback;
private final java.lang.Object mPowerLock;
/* # direct methods */
static com.android.server.wm.AccountHelper -$$Nest$fgetmAccountHelper ( com.android.server.policy.MiuiPhoneWindowManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mAccountHelper;
} // .end method
static android.app.AlertDialog -$$Nest$fgetmFpNavCenterActionChooseDialog ( com.android.server.policy.MiuiPhoneWindowManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mFpNavCenterActionChooseDialog;
} // .end method
static miui.view.MiuiSecurityPermissionHandler -$$Nest$fgetmMiuiSecurityPermissionHandler ( com.android.server.policy.MiuiPhoneWindowManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mMiuiSecurityPermissionHandler;
} // .end method
static com.android.server.policy.MiuiPhoneWindowManager$MIUIWatermarkCallback -$$Nest$fgetmPhoneWindowCallback ( com.android.server.policy.MiuiPhoneWindowManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mPhoneWindowCallback;
} // .end method
static void -$$Nest$fputmFpNavCenterActionChooseDialog ( com.android.server.policy.MiuiPhoneWindowManager p0, android.app.AlertDialog p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mFpNavCenterActionChooseDialog = p1;
	 return;
} // .end method
static void -$$Nest$mbringUpActionChooseDlg ( com.android.server.policy.MiuiPhoneWindowManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->bringUpActionChooseDlg()V */
	 return;
} // .end method
static com.android.server.policy.MiuiPhoneWindowManager ( ) {
	 /* .locals 2 */
	 /* .line 137 */
	 /* nop */
	 /* .line 138 */
	 final String v0 = "ro.hardware.fp.sideCap"; // const-string v0, "ro.hardware.fp.sideCap"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.android.server.policy.MiuiPhoneWindowManager.SUPPORT_POWERFP = (v0!= 0);
	 /* .line 137 */
	 return;
} // .end method
public com.android.server.policy.MiuiPhoneWindowManager ( ) {
	 /* .locals 2 */
	 /* .line 114 */
	 /* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;-><init>()V */
	 /* .line 134 */
	 /* const-wide/16 v0, -0x1 */
	 /* iput-wide v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->interceptPowerKeyTimeByDpadCenter:J */
	 /* .line 136 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mCameraCoveredService = v0;
	 /* .line 139 */
	 /* new-instance v1, Ljava/lang/Object; */
	 /* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
	 this.mPowerLock = v1;
	 /* .line 552 */
	 this.mGetFpLockoutModeMethod = v0;
	 /* .line 567 */
	 this.mFpNavCenterActionChooseDialog = v0;
	 return;
} // .end method
private void bringUpActionChooseDlg ( ) {
	 /* .locals 3 */
	 /* .line 569 */
	 v0 = this.mFpNavCenterActionChooseDialog;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 570 */
		 return;
		 /* .line 573 */
	 } // :cond_0
	 /* new-instance v0, Lcom/android/server/policy/MiuiPhoneWindowManager$4; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/policy/MiuiPhoneWindowManager$4;-><init>(Lcom/android/server/policy/MiuiPhoneWindowManager;)V */
	 /* .line 587 */
	 /* .local v0, "listener":Landroid/content/DialogInterface$OnClickListener; */
	 /* new-instance v1, Landroid/app/AlertDialog$Builder; */
	 v2 = this.mContext;
	 /* invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V */
	 /* .line 588 */
	 /* const v2, 0x110f01fd */
	 (( android.app.AlertDialog$Builder ) v1 ).setTitle ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;
	 /* .line 589 */
	 /* const v2, 0x110f01fa */
	 (( android.app.AlertDialog$Builder ) v1 ).setMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;
	 /* .line 590 */
	 /* const v2, 0x110f01fc */
	 (( android.app.AlertDialog$Builder ) v1 ).setPositiveButton ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
	 /* .line 591 */
	 /* const v2, 0x110f01fb */
	 (( android.app.AlertDialog$Builder ) v1 ).setNegativeButton ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
	 /* .line 592 */
	 int v2 = 0; // const/4 v2, 0x0
	 (( android.app.AlertDialog$Builder ) v1 ).setCancelable ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;
	 (( android.app.AlertDialog$Builder ) v1 ).create ( ); // invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;
	 this.mFpNavCenterActionChooseDialog = v1;
	 /* .line 593 */
	 (( android.app.AlertDialog ) v1 ).getWindow ( ); // invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;
	 (( android.view.Window ) v1 ).getAttributes ( ); // invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;
	 /* .line 594 */
	 /* .local v1, "lp":Landroid/view/WindowManager$LayoutParams; */
	 /* const/16 v2, 0x7d8 */
	 /* iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I */
	 /* .line 595 */
	 v2 = this.mFpNavCenterActionChooseDialog;
	 (( android.app.AlertDialog ) v2 ).getWindow ( ); // invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;
	 (( android.view.Window ) v2 ).setAttributes ( v1 ); // invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
	 /* .line 596 */
	 v2 = this.mFpNavCenterActionChooseDialog;
	 (( android.app.AlertDialog ) v2 ).show ( ); // invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V
	 /* .line 597 */
	 return;
} // .end method
private Boolean drawsSystemBarBackground ( com.android.server.policy.WindowManagerPolicy$WindowState p0 ) {
	 /* .locals 5 */
	 /* .param p1, "win" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
	 /* .line 639 */
	 int v0 = 1; // const/4 v0, 0x1
	 if ( p1 != null) { // if-eqz p1, :cond_1
		 /* .line 640 */
		 final String v1 = "getAttrs"; // const-string v1, "getAttrs"
		 int v2 = 0; // const/4 v2, 0x0
		 /* new-array v3, v2, [Ljava/lang/Object; */
		 com.android.server.input.ReflectionUtils .callPrivateMethod ( p1,v1,v3 );
		 /* check-cast v1, Landroid/view/WindowManager$LayoutParams; */
		 /* .line 641 */
		 /* .local v1, "attrs":Landroid/view/WindowManager$LayoutParams; */
		 if ( v1 != null) { // if-eqz v1, :cond_1
			 /* .line 642 */
			 /* iget v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I */
			 /* const/high16 v4, -0x80000000 */
			 /* and-int/2addr v3, v4 */
			 if ( v3 != null) { // if-eqz v3, :cond_0
			 } // :cond_0
			 /* move v0, v2 */
		 } // :goto_0
		 /* .line 645 */
	 } // .end local v1 # "attrs":Landroid/view/WindowManager$LayoutParams;
} // :cond_1
} // .end method
private Boolean forcesDrawStatusBarBackground ( com.android.server.policy.WindowManagerPolicy$WindowState p0 ) {
/* .locals 5 */
/* .param p1, "win" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .line 649 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_1
	 /* .line 650 */
	 final String v1 = "getAttrs"; // const-string v1, "getAttrs"
	 int v2 = 0; // const/4 v2, 0x0
	 /* new-array v3, v2, [Ljava/lang/Object; */
	 com.android.server.input.ReflectionUtils .callPrivateMethod ( p1,v1,v3 );
	 /* check-cast v1, Landroid/view/WindowManager$LayoutParams; */
	 /* .line 651 */
	 /* .local v1, "attrs":Landroid/view/WindowManager$LayoutParams; */
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 652 */
		 /* iget v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* const v4, 0x8000 */
		 /* and-int/2addr v3, v4 */
		 if ( v3 != null) { // if-eqz v3, :cond_0
		 } // :cond_0
		 /* move v0, v2 */
	 } // :goto_0
	 /* .line 655 */
} // .end local v1 # "attrs":Landroid/view/WindowManager$LayoutParams;
} // :cond_1
} // .end method
private Integer getExtraWindowSystemUiVis ( com.android.server.policy.WindowManagerPolicy$WindowState p0 ) {
/* .locals 4 */
/* .param p1, "transWin" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .line 624 */
int v0 = 0; // const/4 v0, 0x0
/* .line 625 */
/* .local v0, "vis":I */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 626 */
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Ljava/lang/Object; */
final String v2 = "getAttrs"; // const-string v2, "getAttrs"
com.android.server.input.ReflectionUtils .callPrivateMethod ( p1,v2,v1 );
/* check-cast v1, Landroid/view/WindowManager$LayoutParams; */
/* .line 627 */
/* .local v1, "attrs":Landroid/view/WindowManager$LayoutParams; */
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 628 */
	 /* iget v2, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
	 /* or-int/2addr v0, v2 */
	 /* .line 629 */
	 /* iget v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I */
	 int v3 = 3; // const/4 v3, 0x3
	 /* if-ne v2, v3, :cond_0 */
	 /* .line 630 */
	 /* or-int/lit8 v0, v0, 0x1 */
	 /* .line 634 */
} // .end local v1 # "attrs":Landroid/view/WindowManager$LayoutParams;
} // :cond_0
v0 = android.app.MiuiStatusBarManager .getSystemUIVisibilityFlags ( v0 );
/* .line 635 */
} // .end method
private Boolean hideNavBar ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "flag" # I */
/* .param p2, "sys" # I */
/* .line 616 */
/* and-int/lit8 v0, p2, 0x2 */
/* if-nez v0, :cond_1 */
/* and-int/lit16 v0, p2, 0x1800 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 620 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 618 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean hideStatusBar ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "flag" # I */
/* .param p2, "sys" # I */
/* .line 609 */
/* and-int/lit16 v0, p1, 0x400 */
/* if-nez v0, :cond_1 */
/* and-int/lit8 v0, p2, 0x4 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 612 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 610 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void injectEvent ( android.view.KeyEvent p0, Integer p1, Integer p2 ) {
/* .locals 17 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "injectKeyCode" # I */
/* .param p3, "deviceId" # I */
/* .line 600 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v13 */
/* .line 601 */
/* .local v13, "now":J */
/* new-instance v15, Landroid/view/KeyEvent; */
int v5 = 0; // const/4 v5, 0x0
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
int v10 = 0; // const/4 v10, 0x0
v11 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getFlags()I */
v12 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getSource()I */
/* move-object v0, v15 */
/* move-wide v1, v13 */
/* move-wide v3, v13 */
/* move/from16 v6, p2 */
/* move/from16 v9, p3 */
/* invoke-direct/range {v0 ..v12}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V */
/* .line 602 */
/* .local v15, "homeDown":Landroid/view/KeyEvent; */
/* new-instance v16, Landroid/view/KeyEvent; */
int v5 = 1; // const/4 v5, 0x1
v11 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getFlags()I */
v12 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getSource()I */
/* move-object/from16 v0, v16 */
/* invoke-direct/range {v0 ..v12}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V */
/* .line 603 */
/* .local v0, "homeUp":Landroid/view/KeyEvent; */
android.hardware.input.InputManager .getInstance ( );
int v2 = 0; // const/4 v2, 0x0
(( android.hardware.input.InputManager ) v1 ).injectInputEvent ( v15, v2 ); // invoke-virtual {v1, v15, v2}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z
/* .line 604 */
android.hardware.input.InputManager .getInstance ( );
(( android.hardware.input.InputManager ) v1 ).injectInputEvent ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z
/* .line 606 */
return;
} // .end method
private void processBackFingerprintDpcenterEvent ( android.view.KeyEvent p0, Boolean p1 ) {
/* .locals 7 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "isScreenOn" # Z */
/* .line 494 */
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v0, :cond_5 */
v0 = (( com.android.server.policy.MiuiPhoneWindowManager ) p0 ).isDeviceProvisioned ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->isDeviceProvisioned()Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 495 */
int v0 = 0; // const/4 v0, 0x0
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 497 */
v1 = this.mPowerManager;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
(( android.os.PowerManager ) v1 ).userActivity ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/os/PowerManager;->userActivity(JZ)V
/* .line 499 */
} // :cond_0
/* sget-boolean v1, Lcom/android/server/policy/MiuiPhoneWindowManager;->SUPPORT_POWERFP:Z */
final String v2 = "miui.policy:FINGERPRINT_DPAD_CENTER"; // const-string v2, "miui.policy:FINGERPRINT_DPAD_CENTER"
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 500 */
int v1 = 0; // const/4 v1, 0x0
/* .line 501 */
/* .local v1, "lockout":Z */
v3 = this.mFingerprintService;
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 503 */
v3 = try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v3 != null) { // if-eqz v3, :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* move v1, v0 */
/* .line 507 */
/* .line 505 */
/* :catch_0 */
/* move-exception v0 */
/* .line 506 */
/* .local v0, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v0 ).rethrowFromSystemServer ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;
/* throw v2 */
/* .line 509 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_2
} // :goto_0
v0 = (( com.android.server.policy.MiuiPhoneWindowManager ) p0 ).hasEnrolledFingerpirntForAuthentication ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->hasEnrolledFingerpirntForAuthentication()I
/* const/16 v3, 0xb */
/* if-eq v0, v3, :cond_3 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 510 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "fingerprint lockoutmode: "; // const-string v3, "fingerprint lockoutmode: "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "BaseMiuiPhoneWindowManager"; // const-string v3, "BaseMiuiPhoneWindowManager"
android.util.Slog .d ( v3,v0 );
/* .line 511 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* const-wide/16 v5, 0x12c */
/* add-long/2addr v3, v5 */
/* iput-wide v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->interceptPowerKeyTimeByDpadCenter:J */
/* .line 512 */
v0 = this.mPowerManager;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
(( android.os.PowerManager ) v0 ).wakeUp ( v3, v4, v2 ); // invoke-virtual {v0, v3, v4, v2}, Landroid/os/PowerManager;->wakeUp(JLjava/lang/String;)V
/* .line 514 */
} // .end local v1 # "lockout":Z
} // :cond_3
/* .line 515 */
} // :cond_4
v0 = this.mPowerManager;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
(( android.os.PowerManager ) v0 ).wakeUp ( v3, v4, v2 ); // invoke-virtual {v0, v3, v4, v2}, Landroid/os/PowerManager;->wakeUp(JLjava/lang/String;)V
/* .line 519 */
} // :cond_5
} // :goto_1
return;
} // .end method
private void processFrontFingerprintDpcenterEvent ( android.view.KeyEvent p0 ) {
/* .locals 6 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 452 */
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_4 */
/* .line 453 */
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDpadCenterDown:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 454 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDpadCenterDown:Z */
/* .line 455 */
/* iget-boolean v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mHomeDownAfterDpCenter:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 456 */
/* iput-boolean v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mHomeDownAfterDpCenter:Z */
/* .line 457 */
final String v0 = "BaseMiuiPhoneWindowManager"; // const-string v0, "BaseMiuiPhoneWindowManager"
final String v1 = "After dpcenter & home down, ignore tap fingerprint"; // const-string v1, "After dpcenter & home down, ignore tap fingerprint"
android.util.Slog .w ( v0,v1 );
/* .line 458 */
return;
/* .line 461 */
} // :cond_0
v0 = (( com.android.server.policy.MiuiPhoneWindowManager ) p0 ).isDeviceProvisioned ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->isDeviceProvisioned()Z
if ( v0 != null) { // if-eqz v0, :cond_5
v0 = this.mMiuiKeyguardDelegate;
v0 = (( com.android.server.policy.MiuiKeyguardServiceDelegate ) v0 ).isShowingAndNotHidden ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z
/* if-nez v0, :cond_5 */
/* .line 462 */
(( android.view.KeyEvent ) p1 ).getEventTime ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J
/* move-result-wide v2 */
(( android.view.KeyEvent ) p1 ).getDownTime ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDownTime()J
/* move-result-wide v4 */
/* sub-long/2addr v2, v4 */
/* const-wide/16 v4, 0x12c */
/* cmp-long v0, v2, v4 */
/* if-gez v0, :cond_5 */
/* .line 463 */
v0 = this.mMiuiShortcutTriggerHelper;
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).isSingleKeyUse ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isSingleKeyUse()Z
int v2 = -1; // const/4 v2, -0x1
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 464 */
int v0 = 4; // const/4 v0, 0x4
/* invoke-direct {p0, p1, v0, v2}, Lcom/android/server/policy/MiuiPhoneWindowManager;->injectEvent(Landroid/view/KeyEvent;II)V */
/* .line 466 */
} // :cond_1
v0 = this.mMiuiShortcutTriggerHelper;
/* .line 467 */
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).getFingerPrintNavCenterAction ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getFingerPrintNavCenterAction()I
/* if-ne v2, v0, :cond_2 */
/* .line 468 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/policy/MiuiPhoneWindowManager$3; */
/* invoke-direct {v1, p0}, Lcom/android/server/policy/MiuiPhoneWindowManager$3;-><init>(Lcom/android/server/policy/MiuiPhoneWindowManager;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 474 */
} // :cond_2
v0 = this.mMiuiShortcutTriggerHelper;
/* .line 475 */
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).getFingerPrintNavCenterAction ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getFingerPrintNavCenterAction()I
/* if-ne v1, v0, :cond_3 */
/* .line 476 */
int v0 = 3; // const/4 v0, 0x3
/* invoke-direct {p0, p1, v0, v2}, Lcom/android/server/policy/MiuiPhoneWindowManager;->injectEvent(Landroid/view/KeyEvent;II)V */
/* .line 477 */
} // :cond_3
v0 = this.mMiuiShortcutTriggerHelper;
/* .line 478 */
(( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).getFingerPrintNavCenterAction ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getFingerPrintNavCenterAction()I
/* .line 479 */
return;
/* .line 483 */
} // :cond_4
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v0, :cond_5 */
/* .line 484 */
/* iput-boolean v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDpadCenterDown:Z */
/* .line 486 */
} // :cond_5
} // :goto_0
return;
} // .end method
private void processFrontFingerprintDprightEvent ( android.view.KeyEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 490 */
final String v0 = "BaseMiuiPhoneWindowManager"; // const-string v0, "BaseMiuiPhoneWindowManager"
final String v1 = "processFrontFingerprintDprightEvent"; // const-string v1, "processFrontFingerprintDprightEvent"
android.util.Slog .d ( v0,v1 );
/* .line 491 */
return;
} // .end method
private Boolean stopGoogleAssistantVoiceMonitoring ( ) {
/* .locals 2 */
/* .line 312 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 313 */
v0 = this.mMiuiKeyShortcutRuleManager;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mMiuiKeyShortcutRuleManager;
/* .line 314 */
final String v1 = "long_press_power_key"; // const-string v1, "long_press_power_key"
(( com.android.server.policy.MiuiKeyShortcutRuleManager ) v0 ).getFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 313 */
final String v1 = "launch_google_search"; // const-string v1, "launch_google_search"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 315 */
int v0 = 1; // const/4 v0, 0x1
/* .line 318 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
/* # virtual methods */
protected Integer callSuperInterceptKeyBeforeQueueing ( android.view.KeyEvent p0, Integer p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "policyFlags" # I */
/* .param p3, "isScreenOn" # Z */
/* .line 365 */
v0 = this.mPowerLock;
/* monitor-enter v0 */
/* .line 366 */
try { // :try_start_0
v1 = /* invoke-super {p0, p1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->interceptKeyBeforeQueueing(Landroid/view/KeyEvent;I)I */
/* monitor-exit v0 */
/* .line 367 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
protected void cancelPreloadRecentAppsInternal ( ) {
/* .locals 2 */
/* .line 276 */
(( com.android.server.policy.MiuiPhoneWindowManager ) p0 ).getStatusBarManagerInternal ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;
/* .line 277 */
/* .local v0, "statusbar":Lcom/android/server/statusbar/StatusBarManagerInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 278 */
final String v1 = "execute cancelPreloadRecentAppsInternal"; // const-string v1, "execute cancelPreloadRecentAppsInternal"
com.android.server.policy.MiuiInputLog .defaults ( v1 );
/* .line 279 */
/* .line 281 */
} // :cond_0
return;
} // .end method
protected void finishActivityInternal ( android.os.IBinder p0, Integer p1, android.content.Intent p2 ) {
/* .locals 2 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "code" # I */
/* .param p3, "data" # Landroid/content/Intent; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 340 */
android.app.ActivityManagerNative .getDefault ( );
int v1 = 0; // const/4 v1, 0x0
/* .line 341 */
return;
} // .end method
protected void forceStopPackage ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "OwningUserId" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 345 */
/* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
/* .line 346 */
(( com.miui.server.process.ProcessManagerInternal ) v0 ).forceStopPackage ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/process/ProcessManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
/* .line 347 */
return;
} // .end method
protected Integer getFingerprintLockoutMode ( java.lang.Object p0 ) {
/* .locals 4 */
/* .param p1, "bm" # Ljava/lang/Object; */
/* .line 555 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mGetFpLockoutModeMethod;
/* if-nez v1, :cond_0 */
/* .line 556 */
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
final String v2 = "getLockoutMode"; // const-string v2, "getLockoutMode"
/* new-array v3, v0, [Ljava/lang/Class; */
(( java.lang.Class ) v1 ).getDeclaredMethod ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mGetFpLockoutModeMethod = v1;
/* .line 558 */
} // :cond_0
v1 = this.mGetFpLockoutModeMethod;
/* new-array v2, v0, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v1 ).invoke ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 559 */
/* .local v0, "res":I */
/* .line 560 */
} // .end local v0 # "res":I
/* :catch_0 */
/* move-exception v1 */
/* .line 561 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "BaseMiuiPhoneWindowManager"; // const-string v2, "BaseMiuiPhoneWindowManager"
final String v3 = "getFingerprintLockoutMode function exception"; // const-string v3, "getFingerprintLockoutMode function exception"
android.util.Slog .e ( v2,v3 );
/* .line 562 */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 564 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // .end method
protected com.android.server.policy.WindowManagerPolicy$WindowState getKeyguardWindowState ( ) {
/* .locals 1 */
/* .line 371 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected Integer getWakePolicyFlag ( ) {
/* .locals 1 */
/* .line 392 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
protected Integer hasEnrolledFingerpirntForAuthentication ( ) {
/* .locals 1 */
/* .line 549 */
v0 = this.mBiometricManager;
v0 = (( android.hardware.biometrics.BiometricManager ) v0 ).canAuthenticate ( ); // invoke-virtual {v0}, Landroid/hardware/biometrics/BiometricManager;->canAuthenticate()I
} // .end method
public void init ( android.content.Context p0, com.android.server.policy.WindowManagerPolicy$WindowManagerFuncs p1, android.view.IWindowManager p2 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "windowManagerFuncs" # Lcom/android/server/policy/WindowManagerPolicy$WindowManagerFuncs; */
/* .param p3, "windowManager" # Landroid/view/IWindowManager; */
/* .line 144 */
/* invoke-super {p0, p1, p2, p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->init(Landroid/content/Context;Lcom/android/server/policy/WindowManagerPolicy$WindowManagerFuncs;Landroid/view/IWindowManager;)V */
/* .line 145 */
(( com.android.server.policy.MiuiPhoneWindowManager ) p0 ).initInternal ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/policy/MiuiPhoneWindowManager;->initInternal(Landroid/content/Context;Lcom/android/server/policy/WindowManagerPolicy$WindowManagerFuncs;Landroid/view/IWindowManager;)V
/* .line 146 */
return;
} // .end method
protected Integer intercept ( android.view.KeyEvent p0, Integer p1, Boolean p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "policyFlags" # I */
/* .param p3, "isScreenOn" # Z */
/* .param p4, "expectedResult" # I */
/* .line 380 */
/* invoke-super {p0, p1, p2, p3, p4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I */
/* .line 381 */
v0 = this.mContext;
final String v1 = "power"; // const-string v1, "power"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/PowerManager; */
/* .line 382 */
/* .local v0, "pm":Landroid/os/PowerManager; */
int v1 = -1; // const/4 v1, -0x1
/* if-ne p4, v1, :cond_0 */
/* .line 383 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
(( android.os.PowerManager ) v0 ).goToSleep ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->goToSleep(J)V
/* .line 384 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* if-ne p4, v1, :cond_1 */
/* .line 385 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
(( android.os.PowerManager ) v0 ).wakeUp ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->wakeUp(J)V
/* .line 387 */
} // :cond_1
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Integer interceptKeyBeforeQueueing ( android.view.KeyEvent p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "policyFlags" # I */
/* .line 351 */
/* const/high16 v0, 0x20000000 */
/* and-int/2addr v0, p2 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
v0 = (( com.android.server.policy.MiuiPhoneWindowManager ) p0 ).interceptKeyBeforeQueueingInternal ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->interceptKeyBeforeQueueingInternal(Landroid/view/KeyEvent;IZ)I
} // .end method
protected Boolean interceptPowerKeyByFingerPrintKey ( ) {
/* .locals 4 */
/* .line 545 */
/* iget-wide v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->interceptPowerKeyTimeByDpadCenter:J */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
protected Boolean isFingerPrintKey ( android.view.KeyEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 437 */
(( android.view.KeyEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mFpNavEventNameList;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mFpNavEventNameList;
/* .line 438 */
(( android.view.KeyEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
v0 = (( android.view.InputDevice ) v2 ).getName ( ); // invoke-virtual {v2}, Landroid/view/InputDevice;->getName()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 439 */
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 440 */
/* .local v0, "keyCode":I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 445 */
/* .line 443 */
/* :pswitch_0 */
int v1 = 1; // const/4 v1, 0x1
/* .line 448 */
} // .end local v0 # "keyCode":I
} // :cond_0
/* :pswitch_data_0 */
/* .packed-switch 0x16 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
protected Boolean isInLockTaskMode ( ) {
/* .locals 2 */
/* .line 425 */
try { // :try_start_0
android.app.ActivityTaskManager .getService ( );
/* .line 426 */
/* .local v0, "activityTaskManager":Landroid/app/IActivityTaskManager; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = /* .line 427 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 431 */
} // .end local v0 # "activityTaskManager":Landroid/app/IActivityTaskManager;
} // :cond_0
/* .line 429 */
/* :catch_0 */
/* move-exception v0 */
/* .line 430 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 433 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected Boolean isScreenOnInternal ( ) {
/* .locals 1 */
/* .line 335 */
v0 = (( com.android.server.policy.MiuiPhoneWindowManager ) p0 ).isScreenOn ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->isScreenOn()Z
} // .end method
protected void launchAssistActionInternal ( java.lang.String p0, android.os.Bundle p1 ) {
/* .locals 3 */
/* .param p1, "hint" # Ljava/lang/String; */
/* .param p2, "args" # Landroid/os/Bundle; */
/* .line 323 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 324 */
int v0 = 1; // const/4 v0, 0x1
(( android.os.Bundle ) p2 ).putBoolean ( p1, v0 ); // invoke-virtual {p2, p1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 326 */
} // :cond_0
(( com.android.server.policy.MiuiPhoneWindowManager ) p0 ).getStatusBarManagerInternal ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;
/* .line 327 */
/* .local v0, "statusbar":Lcom/android/server/statusbar/StatusBarManagerInternal; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 328 */
final String v1 = "WindowManager"; // const-string v1, "WindowManager"
final String v2 = "launch Google Assist"; // const-string v2, "launch Google Assist"
android.util.Slog .i ( v1,v2 );
/* .line 329 */
/* .line 331 */
} // :cond_1
return;
} // .end method
protected void launchRecentPanelInternal ( ) {
/* .locals 2 */
/* .line 258 */
(( com.android.server.policy.MiuiPhoneWindowManager ) p0 ).getStatusBarManagerInternal ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;
/* .line 259 */
/* .local v0, "statusbar":Lcom/android/server/statusbar/StatusBarManagerInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 260 */
final String v1 = "execute launchRecentPanelInternal"; // const-string v1, "execute launchRecentPanelInternal"
com.android.server.policy.MiuiInputLog .defaults ( v1 );
/* .line 261 */
/* .line 263 */
} // :cond_0
return;
} // .end method
protected void onStatusBarPanelRevealed ( com.android.internal.statusbar.IStatusBarService p0 ) {
/* .locals 2 */
/* .param p1, "statusBarService" # Lcom/android/internal/statusbar/IStatusBarService; */
/* .line 403 */
int v0 = 1; // const/4 v0, 0x1
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 406 */
/* .line 404 */
/* :catch_0 */
/* move-exception v0 */
/* .line 405 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 407 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
protected void preloadRecentAppsInternal ( ) {
/* .locals 2 */
/* .line 267 */
(( com.android.server.policy.MiuiPhoneWindowManager ) p0 ).getStatusBarManagerInternal ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;
/* .line 268 */
/* .local v0, "statusbar":Lcom/android/server/statusbar/StatusBarManagerInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 269 */
final String v1 = "execute preloadRecentAppsInternal"; // const-string v1, "execute preloadRecentAppsInternal"
com.android.server.policy.MiuiInputLog .defaults ( v1 );
/* .line 270 */
/* .line 272 */
} // :cond_0
return;
} // .end method
protected Integer processFingerprintNavigationEvent ( android.view.KeyEvent p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "isScreenOn" # Z */
/* .line 522 */
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 523 */
/* .local v0, "keyCode":I */
int v1 = 0; // const/4 v1, 0x0
/* packed-switch v0, :pswitch_data_0 */
/* .line 540 */
/* .line 525 */
/* :pswitch_0 */
/* iget-boolean v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFrontFingerprintSensor:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 526 */
/* iget-boolean v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mSupportTapFingerprintSensorToHome:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 527 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/MiuiPhoneWindowManager;->processFrontFingerprintDpcenterEvent(Landroid/view/KeyEvent;)V */
/* .line 528 */
/* .line 530 */
} // :cond_0
/* .line 533 */
} // :cond_1
/* invoke-direct {p0, p1, p2}, Lcom/android/server/policy/MiuiPhoneWindowManager;->processBackFingerprintDpcenterEvent(Landroid/view/KeyEvent;Z)V */
/* .line 534 */
/* .line 537 */
/* :pswitch_1 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/MiuiPhoneWindowManager;->processFrontFingerprintDprightEvent(Landroid/view/KeyEvent;)V */
/* .line 538 */
/* :pswitch_data_0 */
/* .packed-switch 0x16 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void registerMIUIWatermarkCallback ( com.android.server.policy.MiuiPhoneWindowManager$MIUIWatermarkCallback p0 ) {
/* .locals 0 */
/* .param p1, "callback" # Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback; */
/* .line 659 */
this.mPhoneWindowCallback = p1;
/* .line 660 */
return;
} // .end method
protected Boolean screenOffBecauseOfProxSensor ( ) {
/* .locals 1 */
/* .line 397 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
void showGlobalActionsInternal ( ) {
/* .locals 3 */
/* .line 294 */
final String v0 = "execute showGlobalActionsInternal"; // const-string v0, "execute showGlobalActionsInternal"
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 296 */
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 297 */
/* new-instance v0, Landroid/content/Intent; */
/* const-string/jumbo v1, "show_shutdown_dialog" */
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 298 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.xiaomi.mirror"; // const-string v1, "com.xiaomi.mirror"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 299 */
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 300 */
final String v1 = "miui.permission.USE_INTERNAL_GENERAL_API"; // const-string v1, "miui.permission.USE_INTERNAL_GENERAL_API"
(( com.android.server.policy.MiuiPhoneWindowManager ) p0 ).sendAsyncBroadcast ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/policy/MiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
/* .line 303 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->stopGoogleAssistantVoiceMonitoring()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 304 */
v0 = this.mContext;
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "close_asssistant_ui"; // const-string v2, "close_asssistant_ui"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
(( android.content.Context ) v0 ).sendBroadcast ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 305 */
final String v0 = "close asssistant ui"; // const-string v0, "close asssistant ui"
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 307 */
} // :cond_1
/* invoke-super {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showGlobalActionsInternal()V */
/* .line 308 */
return;
} // .end method
protected Boolean stopLockTaskMode ( ) {
/* .locals 2 */
/* .line 411 */
try { // :try_start_0
android.app.ActivityTaskManager .getService ( );
/* .line 412 */
/* .local v0, "activityTaskManager":Landroid/app/IActivityTaskManager; */
v1 = if ( v0 != null) { // if-eqz v0, :cond_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 413 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 414 */
int v1 = 1; // const/4 v1, 0x1
/* .line 418 */
} // .end local v0 # "activityTaskManager":Landroid/app/IActivityTaskManager;
} // :cond_0
/* .line 416 */
/* :catch_0 */
/* move-exception v0 */
/* .line 417 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 420 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void systemBooted ( ) {
/* .locals 2 */
/* .line 231 */
/* invoke-super {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->systemBooted()V */
/* .line 232 */
com.miui.server.input.PadManager .getInstance ( );
(( com.miui.server.input.PadManager ) v0 ).notifySystemBooted ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->notifySystemBooted()V
/* .line 233 */
com.miui.server.input.stylus.blocker.MiuiEventBlockerManager .getInstance ( );
(( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager ) v0 ).onSystemBooted ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->onSystemBooted()V
/* .line 234 */
com.android.server.input.overscroller.ScrollerOptimizationConfigProvider .getInstance ( );
v1 = this.mContext;
(( com.android.server.input.overscroller.ScrollerOptimizationConfigProvider ) v0 ).systemBooted ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->systemBooted(Landroid/content/Context;)V
/* .line 235 */
v0 = this.mContext;
com.android.server.policy.MiuiKeyInterceptExtend .getInstance ( v0 );
(( com.android.server.policy.MiuiKeyInterceptExtend ) v0 ).onSystemBooted ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->onSystemBooted()V
/* .line 236 */
v0 = this.mMiuiStylusShortcutManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 237 */
v0 = this.mMiuiStylusShortcutManager;
(( com.miui.server.input.stylus.MiuiStylusShortcutManager ) v0 ).onSystemBooted ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->onSystemBooted()V
/* .line 239 */
} // :cond_0
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_FRONTCAMERA_CIRCLE_BLACK:Z */
final String v1 = "WindowManager"; // const-string v1, "WindowManager"
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mCameraCoveredService;
/* if-nez v0, :cond_2 */
/* .line 240 */
/* const-class v0, Lcom/miui/app/MiuiCameraCoveredManagerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/MiuiCameraCoveredManagerServiceInternal; */
this.mCameraCoveredService = v0;
/* .line 241 */
/* if-nez v0, :cond_1 */
/* .line 242 */
final String v0 = "camera_covered_service not start!"; // const-string v0, "camera_covered_service not start!"
android.util.Slog .e ( v1,v0 );
/* .line 243 */
return;
/* .line 246 */
} // :cond_1
try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 247 */
/* :catch_0 */
/* move-exception v0 */
/* .line 248 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 249 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* .line 251 */
} // :cond_2
final String v0 = "camera_covered_service start again!"; // const-string v0, "camera_covered_service start again!"
android.util.Slog .e ( v1,v0 );
/* .line 253 */
} // :goto_1
com.android.server.input.TouchWakeUpFeatureManager .getInstance ( );
(( com.android.server.input.TouchWakeUpFeatureManager ) v0 ).onSystemBooted ( ); // invoke-virtual {v0}, Lcom/android/server/input/TouchWakeUpFeatureManager;->onSystemBooted()V
/* .line 254 */
return;
} // .end method
public void systemReady ( ) {
/* .locals 4 */
/* .line 150 */
/* invoke-super {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->systemReady()V */
/* .line 151 */
/* new-instance v0, Lcom/android/server/policy/MiuiKeyguardServiceDelegate; */
v1 = this.mKeyguardDelegate;
v2 = this.mPowerManager;
/* invoke-direct {v0, p0, v1, v2}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;-><init>(Lcom/android/server/policy/PhoneWindowManager;Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;Landroid/os/PowerManager;)V */
this.mMiuiKeyguardDelegate = v0;
/* .line 152 */
v0 = this.mContext;
/* const-class v1, Landroid/hardware/biometrics/BiometricManager; */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/biometrics/BiometricManager; */
this.mBiometricManager = v0;
/* .line 153 */
final String v0 = "fingerprint"; // const-string v0, "fingerprint"
android.os.ServiceManager .getService ( v0 );
/* .line 154 */
/* .local v0, "binder":Landroid/os/IBinder; */
android.hardware.fingerprint.IFingerprintService$Stub .asInterface ( v0 );
this.mFingerprintService = v1;
/* .line 155 */
(( com.android.server.policy.MiuiPhoneWindowManager ) p0 ).systemReadyInternal ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->systemReadyInternal()V
/* .line 156 */
/* sget-boolean v1, Lmiui/os/Build;->IS_PRIVATE_BUILD:Z */
/* if-nez v1, :cond_0 */
/* sget-boolean v1, Lmiui/os/Build;->IS_PRIVATE_WATER_MARKER:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 159 */
} // :cond_0
com.android.server.wm.AccountHelper .getInstance ( );
this.mAccountHelper = v1;
/* .line 160 */
v2 = this.mContext;
/* new-instance v3, Lcom/android/server/policy/MiuiPhoneWindowManager$1; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/MiuiPhoneWindowManager$1;-><init>(Lcom/android/server/policy/MiuiPhoneWindowManager;)V */
(( com.android.server.wm.AccountHelper ) v1 ).registerAccountListener ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/AccountHelper;->registerAccountListener(Landroid/content/Context;Lcom/android/server/wm/AccountHelper$AccountCallback;)V
/* .line 183 */
/* new-instance v1, Lmiui/view/MiuiSecurityPermissionHandler; */
v2 = this.mContext;
/* new-instance v3, Lcom/android/server/policy/MiuiPhoneWindowManager$2; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/MiuiPhoneWindowManager$2;-><init>(Lcom/android/server/policy/MiuiPhoneWindowManager;)V */
/* invoke-direct {v1, v2, v3}, Lmiui/view/MiuiSecurityPermissionHandler;-><init>(Landroid/content/Context;Lmiui/view/MiuiSecurityPermissionHandler$PermissionViewCallback;)V */
this.mMiuiSecurityPermissionHandler = v1;
/* .line 220 */
} // :cond_1
/* sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 221 */
v1 = this.mContext;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .getInstance ( v1 );
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) v1 ).enableAutoRemoveShortCutWhenAppRemove ( ); // invoke-virtual {v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->enableAutoRemoveShortCutWhenAppRemove()V
/* .line 222 */
com.android.server.input.KeyboardCombinationManagerStub .get ( );
v2 = this.mContext;
/* .line 224 */
} // :cond_2
v1 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v1 );
(( com.miui.server.input.util.ShortCutActionsUtils ) v1 ).systemReady ( ); // invoke-virtual {v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->systemReady()V
/* .line 225 */
/* const-class v1, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
java.util.Optional .ofNullable ( v1 );
/* new-instance v2, Lcom/android/server/policy/MiuiPhoneWindowManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v2}, Lcom/android/server/policy/MiuiPhoneWindowManager$$ExternalSyntheticLambda0;-><init>()V */
/* .line 226 */
(( java.util.Optional ) v1 ).ifPresent ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 227 */
return;
} // .end method
protected void toggleSplitScreenInternal ( ) {
/* .locals 2 */
/* .line 285 */
(( com.android.server.policy.MiuiPhoneWindowManager ) p0 ).getStatusBarManagerInternal ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;
/* .line 286 */
/* .local v0, "statusbar":Lcom/android/server/statusbar/StatusBarManagerInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 287 */
final String v1 = "execute toggleSplitScreenInternal"; // const-string v1, "execute toggleSplitScreenInternal"
com.android.server.policy.MiuiInputLog .defaults ( v1 );
/* .line 288 */
/* .line 290 */
} // :cond_0
return;
} // .end method
