class com.android.server.policy.BaseMiuiPhoneWindowManager$8 implements com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper$ProximitySensorChangeListener {
	 /* .source "BaseMiuiPhoneWindowManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.policy.BaseMiuiPhoneWindowManager this$0; //synthetic
/* # direct methods */
 com.android.server.policy.BaseMiuiPhoneWindowManager$8 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
/* .line 1059 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onSensorChanged ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "tooClose" # Z */
/* .line 1062 */
v0 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMiuiPocketModeManager ( v0 );
(( com.android.server.input.pocketmode.MiuiPocketModeManager ) v0 ).unregisterListener ( ); // invoke-virtual {v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->unregisterListener()V
/* .line 1063 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 1064 */
	 final String v0 = "BaseMiuiPhoneWindowManager"; // const-string v0, "BaseMiuiPhoneWindowManager"
	 final String v1 = "Going to sleep due to KEYCODE_WAKEUP/KEYCODE_DPAD_CENTER: proximity sensor too close"; // const-string v1, "Going to sleep due to KEYCODE_WAKEUP/KEYCODE_DPAD_CENTER: proximity sensor too close"
	 android.util.Slog .w ( v0,v1 );
	 /* .line 1066 */
	 v0 = this.this$0;
	 v0 = this.mContext;
	 com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
	 int v1 = 0; // const/4 v1, 0x0
	 int v2 = 0; // const/4 v2, 0x0
	 final String v3 = "go_to_sleep"; // const-string v3, "go_to_sleep"
	 final String v4 = "proximity_sensor"; // const-string v4, "proximity_sensor"
	 (( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( v3, v4, v1, v2 ); // invoke-virtual {v0, v3, v4, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
	 /* .line 1071 */
} // :cond_0
return;
} // .end method
