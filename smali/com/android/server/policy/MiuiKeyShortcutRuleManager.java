public class com.android.server.policy.MiuiKeyShortcutRuleManager {
	 /* .source "MiuiKeyShortcutRuleManager.java" */
	 /* # static fields */
	 private static final java.lang.String ATTRIBUTE_ACTION;
	 private static final java.lang.String ATTRIBUTE_COMBINATION_KEY;
	 private static final java.lang.String ATTRIBUTE_FUNCTION;
	 private static final java.lang.String ATTRIBUTE_FUNCTIONS;
	 private static final java.lang.String ATTRIBUTE_GLOBAL;
	 private static final java.lang.String ATTRIBUTE_PRIMARY_KEY;
	 private static final java.lang.String ATTRIBUTE_REGION;
	 public static final Integer ATTRIBUTE_REGION_TYPE_ALL;
	 public static final Integer ATTRIBUTE_REGION_TYPE_CN;
	 public static final Integer ATTRIBUTE_REGION_TYPE_GLOBAL;
	 private static final java.lang.String ATTRIBUTE_TYPE;
	 private static final java.lang.String MIUI_SETTINGS_PACKAGE;
	 private static final java.lang.String SHORTCUT_CONFIG_NAME;
	 private static final java.lang.String SHORTCUT_CONFIG_PATH_DEFAULT;
	 private static final java.lang.String SHORTCUT_CONFIG_PATH_EXTRA;
	 private static final java.lang.String SHORTCUT_TYPE_COMBINATION_KEY_RULE;
	 private static final java.lang.String SHORTCUT_TYPE_GESTURE_RULE;
	 private static final java.lang.String SHORTCUT_TYPE_SINGLE_KEY_RULE;
	 private static final java.lang.String SINGLE_KEY_RULE_ATTRIBUTE_LONG_PRESS_TIME_OUT;
	 private static final java.lang.String SINGLE_KEY_RULE_ATTRIBUTE_MAX_COUNT;
	 private static final java.lang.String SINGLE_KEY_RULE_TYPE_MULTI_PRESS;
	 private static final java.lang.String SINGLE_KEY_RULE_TYPE_SINGLE_PRESS;
	 private static final java.lang.String TAG;
	 private static com.android.server.policy.MiuiKeyShortcutRuleManager sMiuiKeyShortcutRuleManager;
	 /* # instance fields */
	 private final android.content.ContentResolver mContentResolver;
	 private final android.content.Context mContext;
	 private Integer mCurrentUserId;
	 private final android.os.Handler mHandler;
	 private final java.util.Set mInitForUsers;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final com.android.server.policy.KeyCombinationManager mKeyCombinationManager;
private final com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager mMiuiCombinationRuleManager;
private final com.android.server.input.shortcut.MiuiGestureRuleManager mMiuiGestureManager;
private final com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager mMiuiShortcutOperatorCustomManager;
private final com.android.server.policy.MiuiShortcutTriggerHelper mMiuiShortcutTriggerHelper;
private final com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager mMiuiSingleKeyRuleManager;
private final com.android.server.policy.SingleKeyGestureDetector mSingleKeyGestureDetector;
/* # direct methods */
static void -$$Nest$mresetShortcutSettings ( com.android.server.policy.MiuiKeyShortcutRuleManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->resetShortcutSettings()V */
return;
} // .end method
private com.android.server.policy.MiuiKeyShortcutRuleManager ( ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "singleKeyGestureDetector" # Lcom/android/server/policy/SingleKeyGestureDetector; */
/* .param p3, "combinationManager" # Lcom/android/server/policy/KeyCombinationManager; */
/* .param p4, "miuiShortcutTriggerHelper" # Lcom/android/server/policy/MiuiShortcutTriggerHelper; */
/* .line 108 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 109 */
this.mContext = p1;
/* .line 110 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mContentResolver = v0;
/* .line 111 */
com.android.server.input.MiuiInputThread .getHandler ( );
this.mHandler = v0;
/* .line 113 */
/* const-class v1, Lcom/android/server/pm/UserManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/android/server/pm/UserManagerInternal; */
(( com.android.server.pm.UserManagerInternal ) v1 ).getUserIds ( ); // invoke-virtual {v1}, Lcom/android/server/pm/UserManagerInternal;->getUserIds()[I
/* .line 114 */
/* .local v1, "userIds":[I */
/* new-instance v2, Ljava/util/HashSet; */
java.util.Arrays .stream ( v1 );
/* new-instance v4, Lcom/android/server/policy/MiuiKeyShortcutRuleManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v4}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager$$ExternalSyntheticLambda0;-><init>()V */
/* check-cast v3, [Ljava/lang/Integer; */
java.util.Arrays .asList ( v3 );
/* invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
this.mInitForUsers = v2;
/* .line 116 */
v2 = android.os.UserHandle .myUserId ( );
/* iput v2, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mCurrentUserId:I */
/* .line 117 */
this.mSingleKeyGestureDetector = p2;
/* .line 118 */
this.mKeyCombinationManager = p3;
/* .line 120 */
/* const-class v2, Lcom/android/server/policy/MiuiKeyShortcutRuleManager; */
com.android.server.LocalServices .addService ( v2,p0 );
/* .line 122 */
this.mMiuiShortcutTriggerHelper = p4;
/* .line 123 */
com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager .getInstance ( p1,v0,p4,p2 );
this.mMiuiSingleKeyRuleManager = v2;
/* .line 125 */
com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager .getInstance ( p1,v0,p3 );
this.mMiuiCombinationRuleManager = v2;
/* .line 127 */
com.android.server.input.shortcut.MiuiGestureRuleManager .getInstance ( p1,v0 );
this.mMiuiGestureManager = v0;
/* .line 128 */
com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager .getInstance ( p1 );
this.mMiuiShortcutOperatorCustomManager = v0;
/* .line 131 */
/* invoke-direct {p0}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->initMiuiKeyShortcut()V */
/* .line 133 */
(( com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager ) v0 ).initShortcut ( ); // invoke-virtual {v0}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->initShortcut()V
/* .line 135 */
com.android.server.input.shortcut.ShortcutOneTrackHelper .getInstance ( p1 );
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.input.shortcut.ShortcutOneTrackHelper ) v0 ).setUploadShortcutAlarm ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->setUploadShortcutAlarm(Z)V
/* .line 137 */
/* invoke-direct {p0}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->registerPackageChangeReceivers()V */
/* .line 138 */
return;
} // .end method
private org.json.JSONObject getFunctionJSONObject ( org.json.JSONObject p0 ) {
/* .locals 5 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .line 386 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_4
	 v1 = 	 (( org.json.JSONObject ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONObject;->length()I
	 /* if-nez v1, :cond_0 */
	 /* .line 389 */
} // :cond_0
final String v1 = "functions"; // const-string v1, "functions"
v2 = (( org.json.JSONObject ) p1 ).has ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
	 /* .line 391 */
	 try { // :try_start_0
		 (( org.json.JSONObject ) p1 ).getJSONArray ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
		 /* .line 392 */
		 /* .local v1, "functionsArray":Lorg/json/JSONArray; */
		 int v2 = 0; // const/4 v2, 0x0
		 /* .local v2, "i":I */
	 } // :goto_0
	 v3 = 	 (( org.json.JSONArray ) v1 ).length ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
	 /* if-ge v2, v3, :cond_2 */
	 /* .line 393 */
	 (( org.json.JSONArray ) v1 ).getJSONObject ( v2 ); // invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
	 /* .line 394 */
	 /* .local v3, "functionJSONObject":Lorg/json/JSONObject; */
	 v4 = 	 /* invoke-direct {p0, v3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->isLegaDataInCurrentRule(Lorg/json/JSONObject;)Z */
	 /* :try_end_0 */
	 /* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
	 if ( v4 != null) { // if-eqz v4, :cond_1
		 /* .line 395 */
		 /* .line 392 */
	 } // .end local v3 # "functionJSONObject":Lorg/json/JSONObject;
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 400 */
} // .end local v1 # "functionsArray":Lorg/json/JSONArray;
} // .end local v2 # "i":I
} // :cond_2
/* .line 398 */
/* :catch_0 */
/* move-exception v1 */
/* .line 399 */
/* .local v1, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
/* .line 402 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :cond_3
} // :goto_1
/* .line 387 */
} // :cond_4
} // :goto_2
} // .end method
public static synchronized com.android.server.policy.MiuiKeyShortcutRuleManager getInstance ( android.content.Context p0, com.android.server.policy.SingleKeyGestureDetector p1, com.android.server.policy.KeyCombinationManager p2, com.android.server.policy.MiuiShortcutTriggerHelper p3 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "singleKeyGestureDetector" # Lcom/android/server/policy/SingleKeyGestureDetector; */
/* .param p2, "keyCombinationManager" # Lcom/android/server/policy/KeyCombinationManager; */
/* .param p3, "miuiShortcutTriggerHelper" # Lcom/android/server/policy/MiuiShortcutTriggerHelper; */
/* const-class v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager; */
/* monitor-enter v0 */
/* .line 159 */
try { // :try_start_0
v1 = com.android.server.policy.MiuiKeyShortcutRuleManager.sMiuiKeyShortcutRuleManager;
/* if-nez v1, :cond_0 */
/* .line 160 */
/* new-instance v1, Lcom/android/server/policy/MiuiKeyShortcutRuleManager; */
/* invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;-><init>(Landroid/content/Context;Lcom/android/server/policy/SingleKeyGestureDetector;Lcom/android/server/policy/KeyCombinationManager;Lcom/android/server/policy/MiuiShortcutTriggerHelper;)V */
/* .line 163 */
} // :cond_0
v1 = com.android.server.policy.MiuiKeyShortcutRuleManager.sMiuiKeyShortcutRuleManager;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit v0 */
/* .line 158 */
} // .end local p0 # "context":Landroid/content/Context;
} // .end local p1 # "singleKeyGestureDetector":Lcom/android/server/policy/SingleKeyGestureDetector;
} // .end local p2 # "keyCombinationManager":Lcom/android/server/policy/KeyCombinationManager;
} // .end local p3 # "miuiShortcutTriggerHelper":Lcom/android/server/policy/MiuiShortcutTriggerHelper;
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
} // .end method
private Integer getIntAttributeValueByJsonObject ( org.json.JSONObject p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .param p2, "attribute" # Ljava/lang/String; */
/* .line 453 */
int v0 = -1; // const/4 v0, -0x1
/* .line 455 */
/* .local v0, "invalidAttributeValue":I */
final String v1 = "MiuiKeyShortcutRuleManager"; // const-string v1, "MiuiKeyShortcutRuleManager"
if ( p1 != null) { // if-eqz p1, :cond_5
v2 = (( org.json.JSONObject ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONObject;->length()I
if ( v2 != null) { // if-eqz v2, :cond_5
v2 = android.text.TextUtils .isEmpty ( p2 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* goto/16 :goto_5 */
/* .line 460 */
} // :cond_0
try { // :try_start_0
v2 = (( java.lang.String ) p2 ).hashCode ( ); // invoke-virtual {p2}, Ljava/lang/String;->hashCode()I
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* sparse-switch v2, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v2 = "maxCount"; // const-string v2, "maxCount"
v2 = (( java.lang.String ) p2 ).equals ( v2 ); // invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 3; // const/4 v2, 0x3
/* :sswitch_1 */
final String v2 = "combinationKey"; // const-string v2, "combinationKey"
v2 = (( java.lang.String ) p2 ).equals ( v2 ); // invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v3 */
/* :sswitch_2 */
final String v2 = "global"; // const-string v2, "global"
v2 = (( java.lang.String ) p2 ).equals ( v2 ); // invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 2; // const/4 v2, 0x2
/* :sswitch_3 */
final String v2 = "primaryKey"; // const-string v2, "primaryKey"
v2 = (( java.lang.String ) p2 ).equals ( v2 ); // invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v4 */
} // :goto_0
int v2 = -1; // const/4 v2, -0x1
} // :goto_1
/* packed-switch v2, :pswitch_data_0 */
/* .line 471 */
/* .line 469 */
/* :pswitch_0 */
v2 = (( org.json.JSONObject ) p1 ).has ( p2 ); // invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
v3 = (( org.json.JSONObject ) p1 ).getInt ( p2 ); // invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
} // :cond_2
/* .line 466 */
/* :pswitch_1 */
v2 = (( org.json.JSONObject ) p1 ).has ( p2 ); // invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
v4 = (( org.json.JSONObject ) p1 ).getInt ( p2 ); // invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 467 */
} // :cond_3
/* nop */
/* .line 466 */
} // :goto_2
/* .line 463 */
/* :pswitch_2 */
v2 = (( org.json.JSONObject ) p1 ).has ( p2 ); // invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
v1 = (( org.json.JSONObject ) p1 ).getInt ( p2 ); // invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 464 */
} // :cond_4
/* move v1, v0 */
/* .line 463 */
} // :goto_3
/* .line 471 */
} // :goto_4
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "current attribute is invalid,attribute="; // const-string v3, "current attribute is invalid,attribute="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 472 */
/* .line 474 */
/* :catch_0 */
/* move-exception v2 */
/* .line 475 */
/* .local v2, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v2 ).toString ( ); // invoke-virtual {v2}, Lorg/json/JSONException;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v3 );
/* .line 477 */
} // .end local v2 # "e":Lorg/json/JSONException;
/* .line 456 */
} // :cond_5
} // :goto_5
final String v2 = "get int attribute fail,jsonObject is null or attribute is null"; // const-string v2, "get int attribute fail,jsonObject is null or attribute is null"
android.util.Slog .i ( v1,v2 );
/* .line 457 */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x4bfdbf03 -> :sswitch_3 */
/* -0x4a16fc5d -> :sswitch_2 */
/* -0x933af0 -> :sswitch_1 */
/* 0x16c67a0b -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Long getLongAttributeByFunctionJSONObject ( org.json.JSONObject p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .param p2, "attribute" # Ljava/lang/String; */
/* .line 406 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getFunctionJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject; */
/* invoke-direct {p0, v0, p2}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getLongAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)J */
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
private Long getLongAttributeValueByJsonObject ( org.json.JSONObject p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .param p2, "attribute" # Ljava/lang/String; */
/* .line 430 */
/* const-wide/16 v0, -0x1 */
/* .line 432 */
/* .local v0, "invalidAttributeValue":J */
final String v2 = "MiuiKeyShortcutRuleManager"; // const-string v2, "MiuiKeyShortcutRuleManager"
if ( p1 != null) { // if-eqz p1, :cond_3
v3 = (( org.json.JSONObject ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONObject;->length()I
if ( v3 != null) { // if-eqz v3, :cond_3
v3 = android.text.TextUtils .isEmpty ( p2 );
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 437 */
} // :cond_0
try { // :try_start_0
v3 = (( java.lang.String ) p2 ).hashCode ( ); // invoke-virtual {p2}, Ljava/lang/String;->hashCode()I
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
final String v4 = "longPressTimeOut"; // const-string v4, "longPressTimeOut"
/* packed-switch v3, :pswitch_data_0 */
} // :cond_1
/* :pswitch_0 */
try { // :try_start_1
v3 = (( java.lang.String ) p2 ).equals ( v4 ); // invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
int v3 = -1; // const/4 v3, -0x1
} // :goto_1
/* packed-switch v3, :pswitch_data_1 */
/* .line 444 */
/* .line 440 */
/* :pswitch_1 */
v3 = (( org.json.JSONObject ) p1 ).has ( p2 ); // invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
(( org.json.JSONObject ) p1 ).getLong ( v4 ); // invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J
/* move-result-wide v2 */
/* .line 442 */
} // :cond_2
/* move-wide v2, v0 */
/* .line 440 */
} // :goto_2
/* return-wide v2 */
/* .line 444 */
} // :goto_3
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "current attribute is invalid,attribute="; // const-string v4, "current attribute is invalid,attribute="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 448 */
/* .line 446 */
/* :catch_0 */
/* move-exception v3 */
/* .line 447 */
/* .local v3, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v3 ).toString ( ); // invoke-virtual {v3}, Lorg/json/JSONException;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v4 );
/* .line 449 */
} // .end local v3 # "e":Lorg/json/JSONException;
} // :goto_4
/* return-wide v0 */
/* .line 433 */
} // :cond_3
} // :goto_5
final String v3 = "get int attribute fail,jsonObject is null or attribute is null"; // const-string v3, "get int attribute fail,jsonObject is null or attribute is null"
android.util.Slog .i ( v2,v3 );
/* .line 434 */
/* return-wide v0 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch -0x6c2db7e6 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo getMiuiSingleKeyInfo ( org.json.JSONObject p0 ) {
/* .locals 23 */
/* .param p1, "singleKeyObject" # Lorg/json/JSONObject; */
/* .line 316 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
final String v0 = "multiPress"; // const-string v0, "multiPress"
/* const-string/jumbo v3, "singlePress" */
/* new-instance v4, Ljava/util/HashMap; */
/* invoke-direct {v4}, Ljava/util/HashMap;-><init>()V */
/* .line 317 */
/* .local v4, "actionAndDefaultFunctionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* new-instance v5, Ljava/util/HashMap; */
/* invoke-direct {v5}, Ljava/util/HashMap;-><init>()V */
/* move-object v12, v5 */
/* .line 318 */
/* .local v12, "actionMaxCountMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* new-instance v5, Ljava/util/HashMap; */
/* invoke-direct {v5}, Ljava/util/HashMap;-><init>()V */
/* move-object v13, v5 */
/* .line 319 */
/* .local v13, "actionMapForType":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
int v5 = 0; // const/4 v5, 0x0
/* .line 320 */
/* .local v5, "primaryKey":I */
v6 = this.mMiuiShortcutTriggerHelper;
v6 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v6 ).getDefaultLongPressTimeOut ( ); // invoke-virtual {v6}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getDefaultLongPressTimeOut()I
/* int-to-long v6, v6 */
/* .line 322 */
/* .local v6, "longPressTimeOut":J */
try { // :try_start_0
final String v8 = "primaryKey"; // const-string v8, "primaryKey"
v8 = /* invoke-direct {v1, v2, v8}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I */
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_2 */
/* move v5, v8 */
/* .line 324 */
try { // :try_start_1
v8 = (( org.json.JSONObject ) v2 ).has ( v3 ); // invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_1 */
/* const-string/jumbo v9, "type" */
final String v10 = "maxCount"; // const-string v10, "maxCount"
final String v11 = "function"; // const-string v11, "function"
final String v14 = "action"; // const-string v14, "action"
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 325 */
try { // :try_start_2
(( org.json.JSONObject ) v2 ).getJSONArray ( v3 ); // invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 328 */
/* .local v3, "singlePressArray":Lorg/json/JSONArray; */
int v8 = 0; // const/4 v8, 0x0
/* .local v8, "i":I */
} // :goto_0
v15 = (( org.json.JSONArray ) v3 ).length ( ); // invoke-virtual {v3}, Lorg/json/JSONArray;->length()I
/* if-ge v8, v15, :cond_1 */
/* .line 329 */
(( org.json.JSONArray ) v3 ).getJSONObject ( v8 ); // invoke-virtual {v3, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 330 */
/* .local v15, "singlePressJSONObject":Lorg/json/JSONObject; */
/* invoke-direct {v1, v15, v14}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String; */
/* move-object/from16 v17, v16 */
/* .line 333 */
/* .local v17, "action":Ljava/lang/String; */
/* move-object/from16 v16, v3 */
} // .end local v3 # "singlePressArray":Lorg/json/JSONArray;
/* .local v16, "singlePressArray":Lorg/json/JSONArray; */
/* invoke-direct {v1, v15, v11}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeByFunctionJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String; */
/* :try_end_2 */
/* .catch Lorg/json/JSONException; {:try_start_2 ..:try_end_2} :catch_1 */
/* move/from16 v18, v5 */
/* move-object/from16 v5, v17 */
} // .end local v17 # "action":Ljava/lang/String;
/* .local v5, "action":Ljava/lang/String; */
/* .local v18, "primaryKey":I */
try { // :try_start_3
/* .line 335 */
final String v3 = "longPressTimeOut"; // const-string v3, "longPressTimeOut"
/* .line 336 */
/* invoke-direct {v1, v15, v3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getLongAttributeByFunctionJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)J */
/* move-result-wide v19 */
/* .line 338 */
/* .local v19, "configLongPressTimeOut":J */
/* const-wide/16 v21, 0x0 */
/* cmp-long v3, v19, v21 */
/* if-lez v3, :cond_0 */
/* .line 339 */
/* move-wide/from16 v21, v19 */
} // :cond_0
/* move-wide/from16 v21, v6 */
} // :goto_1
/* move-wide/from16 v6, v21 */
/* .line 340 */
/* invoke-direct {v1, v15, v9}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String; */
/* .line 342 */
/* nop */
/* .line 343 */
v3 = /* invoke-direct {v1, v15, v10}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I */
java.lang.Integer .valueOf ( v3 );
/* .line 342 */
/* .line 328 */
/* nop */
} // .end local v5 # "action":Ljava/lang/String;
} // .end local v15 # "singlePressJSONObject":Lorg/json/JSONObject;
} // .end local v19 # "configLongPressTimeOut":J
/* add-int/lit8 v8, v8, 0x1 */
/* move-object/from16 v3, v16 */
/* move/from16 v5, v18 */
} // .end local v16 # "singlePressArray":Lorg/json/JSONArray;
} // .end local v18 # "primaryKey":I
/* .restart local v3 # "singlePressArray":Lorg/json/JSONArray; */
/* .local v5, "primaryKey":I */
} // :cond_1
/* move-object/from16 v16, v3 */
/* move/from16 v18, v5 */
} // .end local v3 # "singlePressArray":Lorg/json/JSONArray;
} // .end local v5 # "primaryKey":I
/* .restart local v16 # "singlePressArray":Lorg/json/JSONArray; */
/* .restart local v18 # "primaryKey":I */
/* .line 324 */
} // .end local v8 # "i":I
} // .end local v16 # "singlePressArray":Lorg/json/JSONArray;
} // .end local v18 # "primaryKey":I
/* .restart local v5 # "primaryKey":I */
} // :cond_2
/* move/from16 v18, v5 */
/* .line 350 */
} // .end local v5 # "primaryKey":I
/* .restart local v18 # "primaryKey":I */
} // :goto_2
v3 = (( org.json.JSONObject ) v2 ).has ( v0 ); // invoke-virtual {v2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 351 */
(( org.json.JSONObject ) v2 ).getJSONArray ( v0 ); // invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 354 */
/* .local v0, "multiPressArray":Lorg/json/JSONArray; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_3
v5 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* if-ge v3, v5, :cond_3 */
/* .line 355 */
(( org.json.JSONArray ) v0 ).getJSONObject ( v3 ); // invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 356 */
/* .local v5, "multiPressJSONObject":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v5 ).getString ( v14 ); // invoke-virtual {v5, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 358 */
/* .local v8, "action":Ljava/lang/String; */
/* nop */
/* .line 359 */
/* invoke-direct {v1, v5, v11}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeByFunctionJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String; */
/* .line 358 */
/* .line 361 */
/* nop */
/* .line 362 */
v15 = /* invoke-direct {v1, v5, v10}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I */
java.lang.Integer .valueOf ( v15 );
/* .line 361 */
/* .line 364 */
/* nop */
/* .line 365 */
/* invoke-direct {v1, v5, v9}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String; */
/* .line 364 */
/* :try_end_3 */
/* .catch Lorg/json/JSONException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 354 */
/* nop */
} // .end local v5 # "multiPressJSONObject":Lorg/json/JSONObject;
} // .end local v8 # "action":Ljava/lang/String;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 373 */
} // .end local v0 # "multiPressArray":Lorg/json/JSONArray;
} // .end local v3 # "i":I
} // :cond_3
/* move-wide v14, v6 */
/* .line 371 */
/* :catch_0 */
/* move-exception v0 */
/* move/from16 v5, v18 */
} // .end local v18 # "primaryKey":I
/* .local v5, "primaryKey":I */
/* :catch_1 */
/* move-exception v0 */
/* move/from16 v18, v5 */
} // .end local v5 # "primaryKey":I
/* .restart local v18 # "primaryKey":I */
} // .end local v18 # "primaryKey":I
/* .restart local v5 # "primaryKey":I */
/* :catch_2 */
/* move-exception v0 */
/* .line 372 */
/* .local v0, "e":Lorg/json/JSONException; */
} // :goto_4
(( org.json.JSONException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
/* move/from16 v18, v5 */
/* move-wide v14, v6 */
/* .line 375 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // .end local v5 # "primaryKey":I
} // .end local v6 # "longPressTimeOut":J
/* .local v14, "longPressTimeOut":J */
/* .restart local v18 # "primaryKey":I */
} // :goto_5
/* new-instance v0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo; */
/* move-object v5, v0 */
/* move/from16 v6, v18 */
/* move-object v7, v4 */
/* move-wide v8, v14 */
/* move-object v10, v12 */
/* move-object v11, v13 */
/* invoke-direct/range {v5 ..v11}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;-><init>(ILjava/util/Map;JLjava/util/Map;Ljava/util/Map;)V */
} // .end method
private java.lang.String getShortcutConfig ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "filePtah" # Ljava/lang/String; */
/* .param p2, "fileName" # Ljava/lang/String; */
/* .line 501 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 502 */
/* .local v0, "localFile":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 503 */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
/* invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 504 */
/* .local v1, "inputStream":Ljava/io/FileInputStream; */
try { // :try_start_1
/* invoke-direct {p0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->readFully(Ljava/io/FileInputStream;)Ljava/lang/String; */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 505 */
try { // :try_start_2
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 504 */
/* .line 503 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "localFile":Ljava/io/File;
} // .end local p0 # "this":Lcom/android/server/policy/MiuiKeyShortcutRuleManager;
} // .end local p1 # "filePtah":Ljava/lang/String;
} // .end local p2 # "fileName":Ljava/lang/String;
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 505 */
} // .end local v1 # "inputStream":Ljava/io/FileInputStream;
/* .restart local v0 # "localFile":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/android/server/policy/MiuiKeyShortcutRuleManager; */
/* .restart local p1 # "filePtah":Ljava/lang/String; */
/* .restart local p2 # "fileName":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v1 */
/* .line 506 */
/* .local v1, "e":Ljava/io/IOException; */
(( java.io.IOException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
/* .line 509 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private java.util.Map getShortcutJsonObjectMap ( java.util.Map p0, java.util.List p1 ) {
/* .locals 13 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Lorg/json/JSONObject;", */
/* ">;>;>;" */
/* } */
} // .end annotation
/* .line 214 */
/* .local p1, "shortcutConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* .local p2, "shortcutTypeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = if ( p1 != null) { // if-eqz p1, :cond_6
if ( v0 != null) { // if-eqz v0, :cond_6
if ( p2 != null) { // if-eqz p2, :cond_6
v0 = /* .line 215 */
/* if-nez v0, :cond_0 */
/* goto/16 :goto_4 */
/* .line 220 */
} // :cond_0
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 221 */
/* .local v0, "shortcutJsonObjectMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lorg/json/JSONObject;>;>;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_5
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 222 */
/* .local v2, "shortcutConfigMapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;" */
/* check-cast v3, Ljava/lang/String; */
/* .line 223 */
/* .local v3, "configType":Ljava/lang/String; */
/* check-cast v4, Ljava/lang/String; */
/* .line 224 */
/* .local v4, "config":Ljava/lang/String; */
/* new-instance v5, Ljava/util/HashMap; */
/* invoke-direct {v5}, Ljava/util/HashMap;-><init>()V */
/* .line 225 */
/* .local v5, "shortcutJsonObjectMapForType":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lorg/json/JSONObject;>;>;" */
v6 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v6, :cond_4 */
v6 = android.text.TextUtils .isEmpty ( v4 );
/* if-nez v6, :cond_4 */
/* .line 226 */
v6 = (( java.lang.String ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/lang/String;->length()I
if ( v6 != null) { // if-eqz v6, :cond_4
v6 = (( java.lang.String ) v4 ).length ( ); // invoke-virtual {v4}, Ljava/lang/String;->length()I
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 229 */
try { // :try_start_0
/* new-instance v6, Lorg/json/JSONObject; */
/* invoke-direct {v6, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 231 */
/* .local v6, "jsonObject":Lorg/json/JSONObject; */
v8 = } // :goto_1
if ( v8 != null) { // if-eqz v8, :cond_3
/* check-cast v8, Ljava/lang/String; */
/* .line 233 */
/* .local v8, "shortcutType":Ljava/lang/String; */
v9 = (( org.json.JSONObject ) v6 ).has ( v8 ); // invoke-virtual {v6, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v9 != null) { // if-eqz v9, :cond_2
/* .line 234 */
(( org.json.JSONObject ) v6 ).getJSONArray ( v8 ); // invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 235 */
/* .local v9, "jsonArray":Lorg/json/JSONArray; */
/* new-instance v10, Ljava/util/ArrayList; */
/* invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V */
/* .line 236 */
/* .local v10, "jsonObjectList":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;" */
int v11 = 0; // const/4 v11, 0x0
/* .local v11, "i":I */
} // :goto_2
v12 = (( org.json.JSONArray ) v9 ).length ( ); // invoke-virtual {v9}, Lorg/json/JSONArray;->length()I
/* if-ge v11, v12, :cond_1 */
/* .line 237 */
(( org.json.JSONArray ) v9 ).getJSONObject ( v11 ); // invoke-virtual {v9, v11}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 236 */
/* add-int/lit8 v11, v11, 0x1 */
/* .line 239 */
} // .end local v11 # "i":I
} // :cond_1
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 241 */
} // .end local v8 # "shortcutType":Ljava/lang/String;
} // .end local v9 # "jsonArray":Lorg/json/JSONArray;
} // .end local v10 # "jsonObjectList":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
} // :cond_2
/* .line 244 */
} // .end local v6 # "jsonObject":Lorg/json/JSONObject;
} // :cond_3
/* .line 242 */
/* :catch_0 */
/* move-exception v6 */
/* .line 243 */
/* .local v6, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v6 ).printStackTrace ( ); // invoke-virtual {v6}, Lorg/json/JSONException;->printStackTrace()V
/* .line 246 */
} // .end local v6 # "e":Lorg/json/JSONException;
} // :cond_4
} // :goto_3
/* .line 247 */
} // .end local v2 # "shortcutConfigMapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
} // .end local v3 # "configType":Ljava/lang/String;
} // .end local v4 # "config":Ljava/lang/String;
} // .end local v5 # "shortcutJsonObjectMapForType":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lorg/json/JSONObject;>;>;"
/* .line 248 */
} // :cond_5
/* .line 216 */
} // .end local v0 # "shortcutJsonObjectMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lorg/json/JSONObject;>;>;>;"
} // :cond_6
} // :goto_4
/* const-string/jumbo v0, "shortcut config is null" */
com.android.server.policy.MiuiInputLog .major ( v0 );
/* .line 217 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private java.lang.String getStringAttributeByFunctionJSONObject ( org.json.JSONObject p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .param p2, "attribute" # Ljava/lang/String; */
/* .line 410 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getFunctionJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject; */
/* invoke-direct {p0, v0, p2}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String; */
} // .end method
private java.lang.String getStringAttributeValueByJsonObject ( org.json.JSONObject p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .param p2, "attribute" # Ljava/lang/String; */
/* .line 481 */
final String v0 = "MiuiKeyShortcutRuleManager"; // const-string v0, "MiuiKeyShortcutRuleManager"
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
v2 = (( org.json.JSONObject ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONObject;->length()I
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = android.text.TextUtils .isEmpty ( p2 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 486 */
} // :cond_0
try { // :try_start_0
v2 = (( org.json.JSONObject ) p1 ).has ( p2 ); // invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
(( org.json.JSONObject ) p1 ).getString ( p2 ); // invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
} // :cond_1
/* .line 487 */
/* :catch_0 */
/* move-exception v2 */
/* .line 488 */
/* .local v2, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v2 ).toString ( ); // invoke-virtual {v2}, Lorg/json/JSONException;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v3 );
/* .line 490 */
} // .end local v2 # "e":Lorg/json/JSONException;
/* .line 482 */
} // :cond_2
} // :goto_0
final String v2 = "get string attribute fail,jsonObject is null or attribute is null"; // const-string v2, "get string attribute fail,jsonObject is null or attribute is null"
android.util.Slog .i ( v0,v2 );
/* .line 483 */
} // .end method
private void initCombinationKeyRule ( java.util.List p0 ) {
/* .locals 12 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lorg/json/JSONObject;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 270 */
/* .local p1, "combinationJsonObjectList":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;" */
v0 = if ( p1 != null) { // if-eqz p1, :cond_3
/* if-nez v0, :cond_0 */
/* .line 274 */
} // :cond_0
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Lorg/json/JSONObject; */
/* .line 278 */
/* .local v1, "combinationJSONObject":Lorg/json/JSONObject; */
final String v2 = "primaryKey"; // const-string v2, "primaryKey"
v2 = /* invoke-direct {p0, v1, v2}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I */
/* .line 280 */
/* .local v2, "primaryKey":I */
final String v3 = "combinationKey"; // const-string v3, "combinationKey"
v9 = /* invoke-direct {p0, v1, v3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I */
/* .line 282 */
/* .local v9, "combinationKey":I */
final String v3 = "action"; // const-string v3, "action"
/* invoke-direct {p0, v1, v3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String; */
/* .line 283 */
/* .local v10, "action":Ljava/lang/String; */
final String v3 = "function"; // const-string v3, "function"
/* invoke-direct {p0, v1, v3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeByFunctionJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String; */
/* .line 285 */
/* .local v11, "function":Ljava/lang/String; */
v3 = this.mMiuiCombinationRuleManager;
/* iget v8, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mCurrentUserId:I */
/* .line 286 */
/* move v4, v2 */
/* move v5, v9 */
/* move-object v6, v10 */
/* move-object v7, v11 */
/* invoke-virtual/range {v3 ..v8}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->getMiuiCombinationRule(IILjava/lang/String;Ljava/lang/String;I)Lcom/android/server/policy/MiuiCombinationRule; */
/* .line 288 */
/* .local v3, "combinationRule":Lcom/android/server/policy/MiuiCombinationRule; */
v4 = this.mMiuiCombinationRuleManager;
(( com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager ) v4 ).addRule ( v10, v3 ); // invoke-virtual {v4, v10, v3}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->addRule(Ljava/lang/String;Lcom/android/server/policy/MiuiCombinationRule;)V
/* .line 289 */
v4 = this.mMiuiCombinationRuleManager;
v4 = (( com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager ) v4 ).shouldHoldOnAOSPLogic ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->shouldHoldOnAOSPLogic(Lcom/android/server/policy/MiuiCombinationRule;)Z
/* if-nez v4, :cond_1 */
/* .line 290 */
v4 = this.mKeyCombinationManager;
(( com.android.server.policy.KeyCombinationManager ) v4 ).addRule ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/policy/KeyCombinationManager;->addRule(Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;)V
/* .line 292 */
} // .end local v1 # "combinationJSONObject":Lorg/json/JSONObject;
} // .end local v2 # "primaryKey":I
} // .end local v3 # "combinationRule":Lcom/android/server/policy/MiuiCombinationRule;
} // .end local v9 # "combinationKey":I
} // .end local v10 # "action":Ljava/lang/String;
} // .end local v11 # "function":Ljava/lang/String;
} // :cond_1
/* .line 293 */
} // :cond_2
return;
/* .line 271 */
} // :cond_3
} // :goto_1
return;
} // .end method
private void initGestureRule ( java.util.List p0 ) {
/* .locals 11 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lorg/json/JSONObject;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 252 */
/* .local p1, "gestureJsonObjectList":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;" */
v0 = if ( p1 != null) { // if-eqz p1, :cond_2
/* if-nez v0, :cond_0 */
/* .line 256 */
} // :cond_0
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lorg/json/JSONObject; */
/* .line 258 */
/* .local v1, "gestureJSONObject":Lorg/json/JSONObject; */
final String v2 = "action"; // const-string v2, "action"
/* invoke-direct {p0, v1, v2}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String; */
/* .line 259 */
/* .local v2, "action":Ljava/lang/String; */
final String v3 = "function"; // const-string v3, "function"
/* invoke-direct {p0, v1, v3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeByFunctionJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String; */
/* .line 261 */
/* .local v9, "function":Ljava/lang/String; */
/* new-instance v10, Lcom/android/server/policy/MiuiGestureRule; */
v4 = this.mContext;
v5 = this.mHandler;
/* iget v8, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mCurrentUserId:I */
/* move-object v3, v10 */
/* move-object v6, v2 */
/* move-object v7, v9 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/policy/MiuiGestureRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;I)V */
/* .line 263 */
/* .local v3, "miuiGestureRule":Lcom/android/server/policy/MiuiGestureRule; */
v4 = this.mMiuiGestureManager;
(( com.android.server.input.shortcut.MiuiGestureRuleManager ) v4 ).addRule ( v2, v3 ); // invoke-virtual {v4, v2, v3}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->addRule(Ljava/lang/String;Lcom/android/server/policy/MiuiGestureRule;)V
/* .line 266 */
} // .end local v1 # "gestureJSONObject":Lorg/json/JSONObject;
} // .end local v2 # "action":Ljava/lang/String;
} // .end local v3 # "miuiGestureRule":Lcom/android/server/policy/MiuiGestureRule;
} // .end local v9 # "function":Ljava/lang/String;
/* .line 267 */
} // :cond_1
return;
/* .line 253 */
} // :cond_2
} // :goto_1
return;
} // .end method
private void initMiuiKeyShortcut ( ) {
/* .locals 13 */
/* .line 168 */
/* const-string/jumbo v0, "system_ext/etc/input/" */
final String v1 = "miui_shortcut_config.json"; // const-string v1, "miui_shortcut_config.json"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getShortcutConfig(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
/* .line 170 */
/* .local v0, "shortcutConfigString":Ljava/lang/String; */
final String v2 = "product/etc/input/"; // const-string v2, "product/etc/input/"
/* invoke-direct {p0, v2, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getShortcutConfig(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
/* .line 173 */
/* .local v1, "shortcutConfigStringExtra":Ljava/lang/String; */
final String v8 = "defaultShortcutMapConfig"; // const-string v8, "defaultShortcutMapConfig"
/* .line 174 */
/* .local v8, "defaultShortcutConfigType":Ljava/lang/String; */
final String v9 = "extraShortcutMapConfig"; // const-string v9, "extraShortcutMapConfig"
/* .line 175 */
/* .local v9, "extraShortcutConfigType":Ljava/lang/String; */
/* new-instance v10, Lcom/android/server/policy/MiuiKeyShortcutRuleManager$2; */
/* move-object v2, v10 */
/* move-object v3, p0 */
/* move-object v4, v8 */
/* move-object v5, v0 */
/* move-object v6, v9 */
/* move-object v7, v1 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager$2;-><init>(Lcom/android/server/policy/MiuiKeyShortcutRuleManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 179 */
/* .local v2, "shortcutConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* new-instance v3, Ljava/util/ArrayList; */
/* const-string/jumbo v4, "singleKeyRules" */
final String v5 = "combinationKeyRules"; // const-string v5, "combinationKeyRules"
final String v6 = "gestureRules"; // const-string v6, "gestureRules"
/* filled-new-array {v4, v5, v6}, [Ljava/lang/String; */
/* .line 180 */
java.util.Arrays .asList ( v7 );
/* invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 183 */
/* .local v3, "shortcutTypeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* invoke-direct {p0, v2, v3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getShortcutJsonObjectMap(Ljava/util/Map;Ljava/util/List;)Ljava/util/Map; */
/* .line 187 */
/* .local v7, "shortcutJsonObjectMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lorg/json/JSONObject;>;>;>;" */
/* check-cast v10, Ljava/util/Map; */
/* .line 189 */
/* .local v10, "defaultShortcutConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lorg/json/JSONObject;>;>;" */
v11 = if ( v10 != null) { // if-eqz v10, :cond_0
if ( v11 != null) { // if-eqz v11, :cond_0
/* .line 190 */
/* check-cast v11, Ljava/util/List; */
/* invoke-direct {p0, v11}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->initSingleKeyRule(Ljava/util/List;)V */
/* .line 191 */
/* nop */
/* .line 192 */
/* check-cast v11, Ljava/util/List; */
/* .line 191 */
/* invoke-direct {p0, v11}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->initCombinationKeyRule(Ljava/util/List;)V */
/* .line 193 */
/* check-cast v11, Ljava/util/List; */
/* invoke-direct {p0, v11}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->initGestureRule(Ljava/util/List;)V */
/* .line 197 */
} // :cond_0
/* check-cast v11, Ljava/util/Map; */
/* .line 199 */
/* .local v11, "extraShortcutConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lorg/json/JSONObject;>;>;" */
v12 = if ( v11 != null) { // if-eqz v11, :cond_1
if ( v12 != null) { // if-eqz v12, :cond_1
/* .line 200 */
/* check-cast v4, Ljava/util/List; */
/* invoke-direct {p0, v4}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->initSingleKeyRule(Ljava/util/List;)V */
/* .line 201 */
/* check-cast v4, Ljava/util/List; */
/* invoke-direct {p0, v4}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->initCombinationKeyRule(Ljava/util/List;)V */
/* .line 202 */
/* check-cast v4, Ljava/util/List; */
/* invoke-direct {p0, v4}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->initGestureRule(Ljava/util/List;)V */
/* .line 205 */
} // :cond_1
v4 = this.mMiuiSingleKeyRuleManager;
(( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager ) v4 ).initSingleKeyRule ( ); // invoke-virtual {v4}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->initSingleKeyRule()V
/* .line 206 */
v4 = this.mMiuiCombinationRuleManager;
(( com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager ) v4 ).initCombinationKeyRule ( ); // invoke-virtual {v4}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->initCombinationKeyRule()V
/* .line 207 */
v4 = this.mMiuiGestureManager;
(( com.android.server.input.shortcut.MiuiGestureRuleManager ) v4 ).initGestureRule ( ); // invoke-virtual {v4}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->initGestureRule()V
/* .line 209 */
return;
} // .end method
private void initSingleKeyRule ( java.util.List p0 ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lorg/json/JSONObject;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 296 */
/* .local p1, "singleKeyJsonObjectList":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;" */
v0 = if ( p1 != null) { // if-eqz p1, :cond_3
/* if-nez v0, :cond_0 */
/* .line 300 */
} // :cond_0
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Lorg/json/JSONObject; */
/* .line 301 */
/* .local v1, "singleKeyObject":Lorg/json/JSONObject; */
final String v2 = "primaryKey"; // const-string v2, "primaryKey"
v2 = /* invoke-direct {p0, v1, v2}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I */
/* .line 302 */
/* .local v2, "primaryKey":I */
/* invoke-direct {p0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getMiuiSingleKeyInfo(Lorg/json/JSONObject;)Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo; */
/* .line 303 */
/* .local v3, "miuiSingleKeyInfo":Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo; */
v4 = this.mMiuiSingleKeyRuleManager;
/* iget v5, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mCurrentUserId:I */
/* .line 304 */
(( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager ) v4 ).getMiuiSingleKeyRule ( v2, v3, v5 ); // invoke-virtual {v4, v2, v3, v5}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->getMiuiSingleKeyRule(ILcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)Lcom/android/server/policy/MiuiSingleKeyRule;
/* .line 306 */
/* .local v4, "miuiSingleKeyRule":Lcom/android/server/policy/MiuiSingleKeyRule; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 307 */
v5 = this.mMiuiSingleKeyRuleManager;
(( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager ) v5 ).addRule ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->addRule(Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;Lcom/android/server/policy/MiuiSingleKeyRule;)V
/* .line 308 */
v5 = this.mSingleKeyGestureDetector;
(( com.android.server.policy.SingleKeyGestureDetector ) v5 ).addRule ( v4 ); // invoke-virtual {v5, v4}, Lcom/android/server/policy/SingleKeyGestureDetector;->addRule(Lcom/android/server/policy/SingleKeyGestureDetector$SingleKeyRule;)V
/* .line 310 */
} // .end local v1 # "singleKeyObject":Lorg/json/JSONObject;
} // .end local v3 # "miuiSingleKeyInfo":Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;
} // .end local v4 # "miuiSingleKeyRule":Lcom/android/server/policy/MiuiSingleKeyRule;
} // :cond_1
/* .line 311 */
} // .end local v2 # "primaryKey":I
} // :cond_2
return;
/* .line 297 */
} // :cond_3
} // :goto_1
return;
} // .end method
private Boolean isLegaDataInCurrentRule ( org.json.JSONObject p0 ) {
/* .locals 6 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .line 414 */
final String v0 = "global"; // const-string v0, "global"
v0 = /* invoke-direct {p0, p1, v0}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I */
/* .line 415 */
/* .local v0, "global":I */
final String v1 = "region"; // const-string v1, "region"
/* invoke-direct {p0, p1, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String; */
/* .line 417 */
/* .local v1, "regions":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 418 */
/* sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v2, :cond_0 */
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_0
/* if-ne v4, v0, :cond_1 */
/* sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v2, :cond_2 */
} // :cond_1
int v2 = 2; // const/4 v2, 0x2
/* if-ne v2, v0, :cond_3 */
} // :cond_2
/* move v3, v4 */
} // :cond_3
/* .line 423 */
} // :cond_4
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v2 );
/* .line 424 */
/* .local v2, "regionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v5 = v5 = com.android.server.policy.MiuiShortcutTriggerHelper.CURRENT_DEVICE_REGION;
/* if-nez v5, :cond_5 */
v5 = com.android.server.policy.MiuiShortcutTriggerHelper.CURRENT_DEVICE_CUSTOMIZED_REGION;
v5 = /* .line 425 */
if ( v5 != null) { // if-eqz v5, :cond_6
} // :cond_5
/* move v3, v4 */
/* .line 424 */
} // :cond_6
} // .end method
static java.lang.Integer lambda$new$0 ( Integer p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "x$0" # I */
/* .line 115 */
/* new-array v0, p0, [Ljava/lang/Integer; */
} // .end method
private java.lang.String readFully ( java.io.FileInputStream p0 ) {
/* .locals 4 */
/* .param p1, "inputStream" # Ljava/io/FileInputStream; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 513 */
/* new-instance v0, Ljava/io/ByteArrayOutputStream; */
/* invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V */
/* .line 514 */
/* .local v0, "baos":Ljava/io/OutputStream; */
/* const/16 v1, 0x400 */
/* new-array v1, v1, [B */
/* .line 515 */
/* .local v1, "buffer":[B */
v2 = (( java.io.FileInputStream ) p1 ).read ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/FileInputStream;->read([B)I
/* .line 516 */
/* .local v2, "read":I */
} // :goto_0
/* if-ltz v2, :cond_0 */
/* .line 517 */
int v3 = 0; // const/4 v3, 0x0
(( java.io.OutputStream ) v0 ).write ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V
/* .line 518 */
v2 = (( java.io.FileInputStream ) p1 ).read ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/FileInputStream;->read([B)I
/* .line 520 */
} // :cond_0
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
private void registerPackageChangeReceivers ( ) {
/* .locals 5 */
/* .line 141 */
/* new-instance v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager$1;-><init>(Lcom/android/server/policy/MiuiKeyShortcutRuleManager;)V */
/* .line 152 */
/* .local v0, "packageMonitor":Lcom/android/internal/content/PackageMonitor; */
v1 = this.mContext;
v2 = android.os.UserHandle.ALL;
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
(( com.android.internal.content.PackageMonitor ) v0 ).register ( v1, v4, v2, v3 ); // invoke-virtual {v0, v1, v4, v2, v3}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Landroid/os/UserHandle;Z)V
/* .line 153 */
return;
} // .end method
private void resetShortcutSettings ( ) {
/* .locals 1 */
/* .line 542 */
v0 = this.mMiuiSingleKeyRuleManager;
(( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager ) v0 ).resetShortcutSettings ( ); // invoke-virtual {v0}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->resetShortcutSettings()V
/* .line 543 */
v0 = this.mMiuiCombinationRuleManager;
(( com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager ) v0 ).resetDefaultFunction ( ); // invoke-virtual {v0}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->resetDefaultFunction()V
/* .line 544 */
v0 = this.mMiuiGestureManager;
(( com.android.server.input.shortcut.MiuiGestureRuleManager ) v0 ).resetDefaultFunction ( ); // invoke-virtual {v0}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->resetDefaultFunction()V
/* .line 545 */
v0 = this.mMiuiShortcutTriggerHelper;
(( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).resetMiuiShortcutSettings ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->resetMiuiShortcutSettings()V
/* .line 546 */
v0 = this.mMiuiShortcutOperatorCustomManager;
(( com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager ) v0 ).initShortcut ( ); // invoke-virtual {v0}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->initShortcut()V
/* .line 547 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 2 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 585 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 586 */
final String v0 = "MiuiKeyShortcutRuleManager"; // const-string v0, "MiuiKeyShortcutRuleManager"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 587 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 588 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 589 */
v0 = this.mMiuiSingleKeyRuleManager;
(( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 590 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 591 */
v0 = this.mMiuiCombinationRuleManager;
(( com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 592 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 593 */
v0 = this.mMiuiGestureManager;
(( com.android.server.input.shortcut.MiuiGestureRuleManager ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 594 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 595 */
v0 = this.mMiuiShortcutTriggerHelper;
(( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 596 */
return;
} // .end method
public com.android.server.policy.MiuiCombinationRule getCombinationRule ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 550 */
v0 = this.mMiuiCombinationRuleManager;
(( com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager ) v0 ).getCombinationRule ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->getCombinationRule(Ljava/lang/String;)Lcom/android/server/policy/MiuiCombinationRule;
} // .end method
public java.lang.String getFunction ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 569 */
v0 = this.mMiuiSingleKeyRuleManager;
v0 = (( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager ) v0 ).hasActionInSingleKeyMap ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->hasActionInSingleKeyMap(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 570 */
v0 = this.mMiuiSingleKeyRuleManager;
(( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager ) v0 ).getFunction ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 573 */
} // :cond_0
v0 = this.mMiuiCombinationRuleManager;
v0 = (( com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager ) v0 ).hasActionInCombinationKeyMap ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->hasActionInCombinationKeyMap(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 574 */
v0 = this.mMiuiCombinationRuleManager;
(( com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager ) v0 ).getFunction ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 578 */
} // :cond_1
v0 = this.mMiuiGestureManager;
v0 = (( com.android.server.input.shortcut.MiuiGestureRuleManager ) v0 ).hasActionInGestureRuleMap ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->hasActionInGestureRuleMap(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 579 */
v0 = this.mMiuiGestureManager;
(( com.android.server.input.shortcut.MiuiGestureRuleManager ) v0 ).getFunction ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 581 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
public com.android.server.policy.MiuiGestureRule getGestureRule ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 558 */
v0 = this.mMiuiGestureManager;
(( com.android.server.input.shortcut.MiuiGestureRuleManager ) v0 ).getGestureManager ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->getGestureManager(Ljava/lang/String;)Lcom/android/server/policy/MiuiGestureRule;
} // .end method
public com.android.server.policy.MiuiSingleKeyRule getSingleKeyRule ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "primaryKey" # I */
/* .line 554 */
v0 = this.mMiuiSingleKeyRuleManager;
(( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager ) v0 ).getSingleKeyRuleForPrimaryKey ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->getSingleKeyRuleForPrimaryKey(I)Lcom/android/server/policy/MiuiSingleKeyRule;
} // .end method
public void onUserSwitch ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "currentUserId" # I */
/* .line 529 */
/* iput p1, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mCurrentUserId:I */
/* .line 530 */
v0 = this.mInitForUsers;
v0 = java.lang.Integer .valueOf ( p1 );
/* xor-int/lit8 v0, v0, 0x1 */
/* .line 531 */
/* .local v0, "isNewUser":Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 532 */
v1 = this.mInitForUsers;
java.lang.Integer .valueOf ( p1 );
/* .line 534 */
} // :cond_0
v1 = this.mMiuiShortcutTriggerHelper;
(( com.android.server.policy.MiuiShortcutTriggerHelper ) v1 ).onUserSwitch ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->onUserSwitch(IZ)V
/* .line 535 */
v1 = this.mMiuiSingleKeyRuleManager;
(( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager ) v1 ).onUserSwitch ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->onUserSwitch(IZ)V
/* .line 536 */
v1 = this.mMiuiCombinationRuleManager;
(( com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager ) v1 ).onUserSwitch ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->onUserSwitch(IZ)V
/* .line 537 */
v1 = this.mMiuiGestureManager;
(( com.android.server.input.shortcut.MiuiGestureRuleManager ) v1 ).onUserSwitch ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->onUserSwitch(IZ)V
/* .line 538 */
v1 = this.mMiuiShortcutOperatorCustomManager;
(( com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager ) v1 ).onUserSwitch ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->onUserSwitch(I)V
/* .line 539 */
return;
} // .end method
Boolean skipKeyGesutre ( android.view.KeyEvent p0 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 604 */
v0 = this.mMiuiShortcutTriggerHelper;
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).skipKeyGesutre ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->skipKeyGesutre(Landroid/view/KeyEvent;)Z
} // .end method
public void updatePolicyFlag ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "policyFlags" # I */
/* .line 601 */
v0 = this.mMiuiSingleKeyRuleManager;
(( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager ) v0 ).updatePolicyFlag ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->updatePolicyFlag(I)V
/* .line 602 */
return;
} // .end method
