.class public Lcom/android/server/policy/OriginalPowerKeyRuleBridge;
.super Ljava/lang/Object;
.source "OriginalPowerKeyRuleBridge.java"


# instance fields
.field private mOriginalPowerKeyRule:Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-class v0, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/WindowManagerPolicy;

    .line 11
    .local v0, "windowManagerPolicy":Lcom/android/server/policy/WindowManagerPolicy;
    instance-of v1, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    if-eqz v1, :cond_0

    .line 12
    move-object v1, v0

    check-cast v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    .line 13
    invoke-virtual {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getOriginalPowerKeyRule()Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;->mOriginalPowerKeyRule:Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;

    .line 15
    :cond_0
    return-void
.end method


# virtual methods
.method public onLongPress(J)V
    .locals 1
    .param p1, "eventTime"    # J

    .line 25
    iget-object v0, p0, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;->mOriginalPowerKeyRule:Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;

    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;->onLongPress(J)V

    .line 28
    :cond_0
    return-void
.end method

.method public onMultiPress(JI)V
    .locals 1
    .param p1, "downTime"    # J
    .param p3, "count"    # I

    .line 37
    iget-object v0, p0, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;->mOriginalPowerKeyRule:Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;

    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;->onMultiPress(JI)V

    .line 40
    :cond_0
    return-void
.end method

.method public onPress(J)V
    .locals 1
    .param p1, "downTime"    # J

    .line 18
    iget-object v0, p0, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;->mOriginalPowerKeyRule:Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;

    if-eqz v0, :cond_0

    .line 19
    invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;->onPress(J)V

    .line 22
    :cond_0
    return-void
.end method

.method public onVeryLongPress(J)V
    .locals 1
    .param p1, "eventTime"    # J

    .line 31
    iget-object v0, p0, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;->mOriginalPowerKeyRule:Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;

    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;->onVeryLongPress(J)V

    .line 34
    :cond_0
    return-void
.end method
