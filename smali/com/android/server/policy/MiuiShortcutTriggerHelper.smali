.class public Lcom/android/server/policy/MiuiShortcutTriggerHelper;
.super Ljava/lang/Object;
.source "MiuiShortcutTriggerHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;
    }
.end annotation


# static fields
.field public static final ACTION_KEYBOARD:Ljava/lang/String; = "keyboard:"

.field public static final CURRENT_DEVICE_CUSTOMIZED_REGION:Ljava/lang/String;

.field public static final CURRENT_DEVICE_REGION:Ljava/lang/String;

.field private static final DEVICE_REGION_RUSSIA:Ljava/lang/String; = "ru"

.field public static final DOUBLE_VOLUME_DOWN_KEY_TYPE_CLOSE:I = 0x0

.field public static final DOUBLE_VOLUME_DOWN_KEY_TYPE_LAUNCH_CAMERA:I = 0x1

.field public static final DOUBLE_VOLUME_DOWN_KEY_TYPE_LAUNCH_CAMERA_AND_TAKE_PHOTO:I = 0x2

.field public static final FUNCTION_DEFAULT:I = -0x1

.field public static final FUNCTION_DISABLE:I = 0x0

.field public static final FUNCTION_ENABLE:I = 0x1

.field private static final KEY_IS_IN_MIUI_SOS_MODE:Ljava/lang/String; = "key_is_in_miui_sos_mode"

.field private static final KEY_MIUI_SOS_ENABLE:Ljava/lang/String; = "key_miui_sos_enable"

.field private static final LONG_PRESS_POWER_GLOBAL_ACTIONS:I = 0x1

.field private static final LONG_PRESS_POWER_NOTHING:I = 0x0

.field public static final PACKAGE_SMART_HOME:Ljava/lang/String; = "com.miui.smarthomeplus"

.field private static final TAG:Ljava/lang/String; = "MiuiShortcutTriggerHelp"

.field public static final TYPE_SOS_GOOGLE:I = 0x0

.field public static final TYPE_SOS_MI:I = 0x1

.field public static final VOICE_ASSIST_GUIDE_MAX_COUNT:I = 0x2

.field private static volatile sMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;


# instance fields
.field private mAOSPAssistantLongPressHomeEnabled:Z

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field public mDefaultLongPressTimeOut:I

.field public mDoubleTapOnHomeBehavior:I

.field private mFingerPrintNavCenterAction:I

.field private mFivePressPowerLaunchGoogleSos:Ljava/lang/String;

.field private mFivePressPowerLaunchSos:Z

.field private mHandledKeyCodeByLongPress:I

.field private final mHandler:Landroid/os/Handler;

.field private mIsCtsMode:Z

.field private mIsOnSosMode:Z

.field public mLongPressPowerKeyLaunchSmartHome:Z

.field public mLongPressPowerKeyLaunchXiaoAi:Z

.field private final mPowerManager:Landroid/os/PowerManager;

.field private mPressToAppSwitch:Z

.field private mRSAGuideStatus:I

.field private final mShortcutSettingsObserver:Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;

.field private mSingleKeyUse:Z

.field private final mSupportGoogleSos:Z

.field private mTorchEnabled:Z

.field private final mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

.field public mXiaoaiPowerGuideFlag:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mContentResolver:Landroid/content/ContentResolver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I
    .locals 0

    iget p0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mCurrentUserId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSingleKeyUse(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mSingleKeyUse:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmAOSPAssistantLongPressHomeEnabled(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mAOSPAssistantLongPressHomeEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFingerPrintNavCenterAction(Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mFingerPrintNavCenterAction:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFivePressPowerLaunchGoogleSos(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mFivePressPowerLaunchGoogleSos:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFivePressPowerLaunchSos(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mFivePressPowerLaunchSos:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsCtsMode(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mIsCtsMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsOnSosMode(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mIsOnSosMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmPressToAppSwitch(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mPressToAppSwitch:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRSAGuideStatus(Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mRSAGuideStatus:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSingleKeyUse(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mSingleKeyUse:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTorchEnabled(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mTorchEnabled:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 35
    nop

    .line 36
    const-string v0, "ro.miui.build.region"

    const-string v1, "CN"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->CURRENT_DEVICE_REGION:Ljava/lang/String;

    .line 38
    nop

    .line 39
    const-string v0, "ro.miui.customized.region"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->CURRENT_DEVICE_CUSTOMIZED_REGION:Ljava/lang/String;

    .line 38
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mXiaoaiPowerGuideFlag:I

    .line 65
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDoubleTapOnHomeBehavior:I

    .line 74
    iput v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mRSAGuideStatus:I

    .line 84
    iput v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mHandledKeyCodeByLongPress:I

    .line 86
    iput-object p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mContext:Landroid/content/Context;

    .line 87
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mHandler:Landroid/os/Handler;

    .line 88
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mContentResolver:Landroid/content/ContentResolver;

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 90
    const v3, 0x111013f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mSupportGoogleSos:Z

    .line 91
    const-class v2, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v2}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v2, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 92
    const-string v2, "power"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    iput-object v2, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mPowerManager:Landroid/os/PowerManager;

    .line 94
    const-string/jumbo v2, "support_gesture_shortcut_settings"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 96
    new-instance v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;

    invoke-direct {v1, p0, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;-><init>(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mShortcutSettingsObserver:Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;

    .line 97
    invoke-virtual {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->initShortcutSettingsObserver()V

    .line 99
    return-void
.end method

.method public static getDoubleVolumeDownKeyFunction(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "doubleVolumeDownStatus"    # Ljava/lang/String;

    .line 182
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    const/4 v0, 0x0

    return-object v0

    .line 185
    :cond_0
    const/4 v0, -0x1

    .line 187
    .local v0, "status":I
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 190
    goto :goto_0

    .line 188
    :catch_0
    move-exception v1

    .line 189
    .local v1, "e":Ljava/lang/NumberFormatException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status is invalid,status="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiShortcutTriggerHelp"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :goto_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 192
    const-string v1, "launch_camera"

    return-object v1

    .line 193
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 194
    const-string v1, "launch_camera_and_take_photo"

    return-object v1

    .line 196
    :cond_2
    const-string v1, "none"

    return-object v1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/policy/MiuiShortcutTriggerHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 102
    sget-object v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->sMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    if-nez v0, :cond_1

    .line 103
    const-class v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    monitor-enter v0

    .line 104
    :try_start_0
    sget-object v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->sMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    if-nez v1, :cond_0

    .line 105
    new-instance v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-direct {v1, p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->sMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 107
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 109
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->sMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    return-object v0
.end method

.method private getVeryLongPressPowerBehavior()I
    .locals 4

    .line 461
    const/4 v0, 0x0

    .line 462
    .local v0, "veryLongPressBehavior":I
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isUserSetUpComplete()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 463
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mContentResolver:Landroid/content/ContentResolver;

    const-string v2, "long_press_power_key"

    iget v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mCurrentUserId:I

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 465
    .local v1, "longPressPowerFunction":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isSupportLongPressPowerGuide()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 466
    const-string v2, "none"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 467
    :cond_0
    sget-boolean v2, Lmiui/hardware/input/InputFeature;->IS_SUPPORT_KDDI_POWER_GUIDE:Z

    xor-int/lit8 v2, v2, 0x1

    move v0, v2

    .line 470
    .end local v1    # "longPressPowerFunction":Ljava/lang/String;
    :cond_1
    return v0
.end method

.method private setupMenuFunction()V
    .locals 7

    .line 519
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mContentResolver:Landroid/content/ContentResolver;

    iget v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mCurrentUserId:I

    const-string v2, "screen_key_press_app_switch"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 521
    .local v0, "menuKeyCurrentFunction":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 523
    return-void

    .line 526
    :cond_0
    iget v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mCurrentUserId:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v1, :cond_1

    move v5, v3

    goto :goto_0

    :cond_1
    move v5, v4

    .line 527
    .local v5, "isAppSwitch":Z
    :goto_0
    iget-object v6, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mContentResolver:Landroid/content/ContentResolver;

    .line 529
    if-eqz v5, :cond_2

    goto :goto_1

    :cond_2
    move v3, v4

    .line 527
    :goto_1
    invoke-static {v6, v2, v3, v1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 530
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 551
    const-string v0, "MiuiShortcutTriggerHelp"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 552
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 553
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 554
    const-string v0, "mPressToAppSwitch="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 555
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mPressToAppSwitch:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 556
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 557
    const-string v0, "isMiuiSosCanBeTrigger="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 558
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->shouldLaunchSosByType(I)Z

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 559
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 560
    const-string v0, "isGoogleSosEnable="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 561
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->shouldLaunchSosByType(I)Z

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 562
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 563
    const-string v0, "mFingerPrintNavCenterAction="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 564
    iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mFingerPrintNavCenterAction:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 565
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 566
    const-string v0, "mXiaoaiPowerGuideFlag="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 567
    iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mXiaoaiPowerGuideFlag:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 568
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 569
    const-string v0, "mDefaultLongPressTimeOut="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 570
    iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDefaultLongPressTimeOut:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 571
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 572
    const-string v0, "mTorchEnabled="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 573
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mTorchEnabled:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 574
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 575
    const-string v0, "mAOSPAssistantLongPressHomeEnabled="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 576
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mAOSPAssistantLongPressHomeEnabled:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 577
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 578
    const-string v0, "mIsCtsMode="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 579
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mIsCtsMode:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 580
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 581
    const-string v0, "mRSAGuideStatus="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 582
    iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mRSAGuideStatus:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 583
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 584
    const-string v0, "mHandledKeyCodeByLongPress="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 585
    iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mHandledKeyCodeByLongPress:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 586
    return-void
.end method

.method public getDefaultLongPressTimeOut()I
    .locals 1

    .line 213
    iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDefaultLongPressTimeOut:I

    return v0
.end method

.method public getFingerPrintNavCenterAction()I
    .locals 1

    .line 147
    iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mFingerPrintNavCenterAction:I

    return v0
.end method

.method public getFivePressPowerLaunchGoogleSos()Ljava/lang/String;
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mFivePressPowerLaunchGoogleSos:Ljava/lang/String;

    return-object v0
.end method

.method public getPowerManager()Landroid/os/PowerManager;
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mPowerManager:Landroid/os/PowerManager;

    return-object v0
.end method

.method public getRSAGuideStatus()I
    .locals 1

    .line 131
    iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mRSAGuideStatus:I

    return v0
.end method

.method public interceptKeyByLongPress(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 601
    const/4 v0, 0x0

    .line 602
    .local v0, "intercepted":Z
    iget v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mHandledKeyCodeByLongPress:I

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 603
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v1

    and-int/lit16 v1, v1, 0x400

    if-nez v1, :cond_0

    .line 604
    const/4 v0, 0x1

    .line 607
    :cond_0
    return v0
.end method

.method public isCtsMode()Z
    .locals 1

    .line 205
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mIsCtsMode:Z

    return v0
.end method

.method public isLegalData(ILjava/lang/String;)Z
    .locals 2
    .param p1, "global"    # I
    .param p2, "region"    # Ljava/lang/String;

    .line 534
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 535
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    if-eqz p1, :cond_3

    :cond_0
    if-ne v1, p1, :cond_1

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_3

    :cond_1
    const/4 v0, 0x2

    if-ne v0, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :cond_3
    :goto_0
    return v1

    .line 541
    :cond_4
    const-string v0, ","

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 542
    .local v0, "regionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->CURRENT_DEVICE_REGION:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public isPressToAppSwitch()Z
    .locals 1

    .line 135
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mPressToAppSwitch:Z

    return v0
.end method

.method public isSingleKeyUse()Z
    .locals 1

    .line 143
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mSingleKeyUse:Z

    return v0
.end method

.method public isSupportLongPressPowerGuide()Z
    .locals 3

    .line 151
    const/4 v0, -0x1

    .line 153
    .local v0, "longPressPowerGuideStatus":I
    sget-object v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->CURRENT_DEVICE_REGION:Ljava/lang/String;

    const-string v2, "cn"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154
    iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mXiaoaiPowerGuideFlag:I

    goto :goto_0

    .line 155
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->supportRSARegion()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 156
    iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mRSAGuideStatus:I

    .line 158
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isUserSetUpComplete()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public isTorchEnabled()Z
    .locals 1

    .line 201
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mTorchEnabled:Z

    return v0
.end method

.method public isUserSetUpComplete()Z
    .locals 1

    .line 493
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isUserSetupComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public notifyLongPressed(I)V
    .locals 0
    .param p1, "keyCode"    # I

    .line 595
    iput p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mHandledKeyCodeByLongPress:I

    .line 596
    return-void
.end method

.method public onUserSwitch(IZ)V
    .locals 3
    .param p1, "currentUserId"    # I
    .param p2, "isNewUser"    # Z

    .line 498
    iput p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mCurrentUserId:I

    .line 499
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mShortcutSettingsObserver:Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->onChange(Z)V

    .line 500
    invoke-direct {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setupMenuFunction()V

    .line 501
    if-eqz p2, :cond_0

    .line 502
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->resetMiuiShortcutSettings()V

    .line 505
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "support_gesture_shortcut_settings"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, p1}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 507
    return-void
.end method

.method public resetKeyCodeByLongPress()V
    .locals 1

    .line 598
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mHandledKeyCodeByLongPress:I

    .line 599
    return-void
.end method

.method public resetMiuiShortcutSettings()V
    .locals 2

    .line 547
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mShortcutSettingsObserver:Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->onChange(Z)V

    .line 548
    return-void
.end method

.method public setLongPressPowerBehavior(Ljava/lang/String;)V
    .locals 3
    .param p1, "function"    # Ljava/lang/String;

    .line 481
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isUserSetUpComplete()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 483
    sget-boolean v0, Lmiui/hardware/input/InputFeature;->IS_SUPPORT_KDDI_POWER_GUIDE:Z

    if-eqz v0, :cond_1

    .line 484
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 486
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "none"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 487
    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    .line 484
    :goto_0
    const-string v2, "power_button_long_press"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 490
    :cond_1
    return-void
.end method

.method public setPressToAppSwitch(Z)V
    .locals 0
    .param p1, "pressToAppSwitch"    # Z

    .line 139
    iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mPressToAppSwitch:Z

    .line 140
    return-void
.end method

.method public setVeryLongPressPowerBehavior()V
    .locals 3

    .line 474
    invoke-direct {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getVeryLongPressPowerBehavior()I

    move-result v0

    .line 475
    .local v0, "behavior":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set veryLongPressBehavior="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiShortcutTriggerHelp"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mContentResolver:Landroid/content/ContentResolver;

    const-string v2, "power_button_very_long_press"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 478
    return-void
.end method

.method public shouldLaunchSos()Z
    .locals 3

    .line 222
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->shouldLaunchSosByType(I)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->shouldLaunchSosByType(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move v0, v2

    :cond_1
    return v0
.end method

.method public shouldLaunchSosByType(I)Z
    .locals 4
    .param p1, "type"    # I

    .line 166
    const/4 v0, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    .line 173
    return v1

    .line 168
    :pswitch_0
    iget-boolean v2, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mFivePressPowerLaunchSos:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mIsOnSosMode:Z

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    return v0

    .line 170
    :pswitch_1
    iget-boolean v2, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mSupportGoogleSos:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mFivePressPowerLaunchGoogleSos:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 171
    const-string v3, "1"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    goto :goto_1

    :cond_2
    move v0, v1

    .line 170
    :goto_1
    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public skipKeyGesutre(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 589
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v0

    const/high16 v1, 0x1000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    .line 592
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 590
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public supportAOSPTriggerFunction(I)Z
    .locals 1
    .param p1, "keyCode"    # I

    .line 124
    const/4 v0, 0x3

    if-ne v0, p1, :cond_0

    .line 125
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mAOSPAssistantLongPressHomeEnabled:Z

    return v0

    .line 127
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public supportRSARegion()Z
    .locals 2

    .line 113
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->CURRENT_DEVICE_REGION:Ljava/lang/String;

    .line 114
    const-string v1, "ru"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 113
    :goto_0
    return v0
.end method
