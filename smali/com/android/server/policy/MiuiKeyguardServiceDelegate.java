public class com.android.server.policy.MiuiKeyguardServiceDelegate extends com.android.server.policy.AbstractKeyguardServiceDelegate {
	 /* .source "MiuiKeyguardServiceDelegate.java" */
	 /* # instance fields */
	 protected com.android.server.policy.keyguard.KeyguardServiceDelegate mKeyguardDelegate;
	 protected com.android.server.policy.PhoneWindowManager mPhoneWindowManager;
	 protected android.os.PowerManager mPowerManager;
	 /* # direct methods */
	 public com.android.server.policy.MiuiKeyguardServiceDelegate ( ) {
		 /* .locals 0 */
		 /* .param p1, "phoneWindowManager" # Lcom/android/server/policy/PhoneWindowManager; */
		 /* .param p2, "keyguardDelegate" # Lcom/android/server/policy/keyguard/KeyguardServiceDelegate; */
		 /* .param p3, "powerManager" # Landroid/os/PowerManager; */
		 /* .line 15 */
		 /* invoke-direct {p0}, Lcom/android/server/policy/AbstractKeyguardServiceDelegate;-><init>()V */
		 /* .line 16 */
		 this.mPhoneWindowManager = p1;
		 /* .line 17 */
		 this.mKeyguardDelegate = p2;
		 /* .line 18 */
		 this.mPowerManager = p3;
		 /* .line 19 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void OnDoubleClickHome ( ) {
		 /* .locals 0 */
		 /* .line 51 */
		 /* nop */
		 /* .line 55 */
		 return;
	 } // .end method
	 protected void enableUserActivity ( Boolean p0 ) {
		 /* .locals 0 */
		 /* .param p1, "value" # Z */
		 /* .line 22 */
		 return;
	 } // .end method
	 public Boolean isShowing ( ) {
		 /* .locals 1 */
		 /* .line 25 */
		 v0 = this.mKeyguardDelegate;
		 v0 = 		 (( com.android.server.policy.keyguard.KeyguardServiceDelegate ) v0 ).isShowing ( ); // invoke-virtual {v0}, Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;->isShowing()Z
	 } // .end method
	 public Boolean isShowingAndNotHidden ( ) {
		 /* .locals 1 */
		 /* .line 29 */
		 v0 = 		 (( com.android.server.policy.MiuiKeyguardServiceDelegate ) p0 ).isShowing ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowing()Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 v0 = this.mPhoneWindowManager;
			 v0 = 			 (( com.android.server.policy.PhoneWindowManager ) v0 ).isKeyguardOccluded ( ); // invoke-virtual {v0}, Lcom/android/server/policy/PhoneWindowManager;->isKeyguardOccluded()Z
			 /* if-nez v0, :cond_0 */
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :goto_0
} // .end method
public void keyguardDone ( ) {
	 /* .locals 0 */
	 /* .line 35 */
	 return;
} // .end method
public void onScreenTurnedOnWithoutListener ( ) {
	 /* .locals 1 */
	 /* .line 42 */
	 v0 = this.mKeyguardDelegate;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 43 */
		 (( com.android.server.policy.keyguard.KeyguardServiceDelegate ) v0 ).onScreenTurnedOn ( ); // invoke-virtual {v0}, Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;->onScreenTurnedOn()V
		 /* .line 45 */
	 } // :cond_0
	 return;
} // .end method
public Boolean onWakeKeyWhenKeyguardShowingTq ( Integer p0, Boolean p1 ) {
	 /* .locals 1 */
	 /* .param p1, "keyCode" # I */
	 /* .param p2, "isDocked" # Z */
	 /* .line 38 */
	 int v0 = 0; // const/4 v0, 0x0
} // .end method
public void pokeWakelock ( ) {
	 /* .locals 4 */
	 /* .line 48 */
	 v0 = this.mPowerManager;
	 android.os.SystemClock .uptimeMillis ( );
	 /* move-result-wide v1 */
	 int v3 = 1; // const/4 v3, 0x1
	 (( android.os.PowerManager ) v0 ).userActivity ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/PowerManager;->userActivity(JZ)V
	 /* .line 49 */
	 return;
} // .end method
