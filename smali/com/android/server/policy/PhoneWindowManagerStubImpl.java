public class com.android.server.policy.PhoneWindowManagerStubImpl implements com.android.server.policy.PhoneWindowManagerStub {
	 /* .source "PhoneWindowManagerStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.util.List DELIVE_META_APPS;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final Boolean SUPPORT_FOD;
static final java.lang.String TAG;
/* # instance fields */
private Integer SUB_DISPLAY_ID;
private com.android.server.policy.DisplayTurnoverManager mDisplayTurnoverManager;
private com.android.server.policy.WindowManagerPolicy$WindowState mFocusedWindow;
miui.util.HapticFeedbackUtil mHapticFeedbackUtil;
private com.android.server.policy.MiuiKeyShortcutRuleManager mMiuiKeyShortcutRuleManager;
private Boolean mPhotoHandleConnection;
private Integer mPhotoHandleEventDeviceId;
private com.android.server.power.PowerManagerServiceStub mPowerManagerServiceImpl;
private Boolean mSupportAOD;
/* # direct methods */
static com.android.server.policy.PhoneWindowManagerStubImpl ( ) {
/* .locals 2 */
/* .line 43 */
/* nop */
/* .line 44 */
final String v0 = "ro.hardware.fp.fod"; // const-string v0, "ro.hardware.fp.fod"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.policy.PhoneWindowManagerStubImpl.SUPPORT_FOD = (v0!= 0);
/* .line 45 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 58 */
final String v1 = "com.ss.android.lark.kami"; // const-string v1, "com.ss.android.lark.kami"
/* .line 59 */
return;
} // .end method
public com.android.server.policy.PhoneWindowManagerStubImpl ( ) {
/* .locals 1 */
/* .line 41 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 49 */
int v0 = 2; // const/4 v0, 0x2
/* iput v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->SUB_DISPLAY_ID:I */
return;
} // .end method
/* # virtual methods */
public android.os.VibrationEffect convertToMiuiHapticFeedback ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "hapticFeedbackConstantId" # I */
/* .line 123 */
v0 = this.mHapticFeedbackUtil;
(( miui.util.HapticFeedbackUtil ) v0 ).convertToMiuiHapticFeedback ( p1 ); // invoke-virtual {v0, p1}, Lmiui/util/HapticFeedbackUtil;->convertToMiuiHapticFeedback(I)Landroid/os/VibrationEffect;
} // .end method
public Boolean hasMiuiPowerMultiClick ( ) {
/* .locals 2 */
/* .line 151 */
v0 = this.mMiuiKeyShortcutRuleManager;
/* if-nez v0, :cond_0 */
/* .line 152 */
/* const-class v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager; */
this.mMiuiKeyShortcutRuleManager = v0;
/* .line 155 */
} // :cond_0
v0 = this.mMiuiKeyShortcutRuleManager;
final String v1 = "double_click_power_key"; // const-string v1, "double_click_power_key"
(( com.android.server.policy.MiuiKeyShortcutRuleManager ) v0 ).getFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 157 */
/* .local v0, "doubleClick":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_1 */
final String v1 = "none"; // const-string v1, "none"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 63 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x111012b */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mSupportAOD:Z */
/* .line 64 */
com.android.server.power.PowerManagerServiceStub .get ( );
this.mPowerManagerServiceImpl = v0;
/* .line 65 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 66 */
/* new-instance v0, Lcom/android/server/policy/DisplayTurnoverManager; */
/* invoke-direct {v0, p1}, Lcom/android/server/policy/DisplayTurnoverManager;-><init>(Landroid/content/Context;)V */
this.mDisplayTurnoverManager = v0;
/* .line 68 */
} // :cond_0
/* new-instance v0, Lmiui/util/HapticFeedbackUtil; */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {v0, p1, v1}, Lmiui/util/HapticFeedbackUtil;-><init>(Landroid/content/Context;Z)V */
this.mHapticFeedbackUtil = v0;
/* .line 69 */
return;
} // .end method
public Boolean interceptKeyBeforeQueueing ( android.view.KeyEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 111 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mDisplayTurnoverManager;
if ( v0 != null) { // if-eqz v0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 112 */
v0 = (( android.view.KeyEvent ) p1 ).isWakeKey ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->isWakeKey()Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 v0 = 	 (( android.view.KeyEvent ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDisplayId()I
	 /* iget v1, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->SUB_DISPLAY_ID:I */
	 /* if-ne v0, v1, :cond_1 */
	 /* .line 113 */
	 v0 = 	 (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
	 int v1 = 1; // const/4 v1, 0x1
	 /* if-nez v0, :cond_0 */
	 /* .line 114 */
	 v0 = this.mDisplayTurnoverManager;
	 final String v2 = "DOUBLE_CLICK"; // const-string v2, "DOUBLE_CLICK"
	 (( com.android.server.policy.DisplayTurnoverManager ) v0 ).switchSubDisplayPowerState ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/policy/DisplayTurnoverManager;->switchSubDisplayPowerState(ZLjava/lang/String;)V
	 /* .line 116 */
} // :cond_0
/* .line 118 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean interceptKeyWithMeta ( ) {
/* .locals 2 */
/* .line 144 */
v0 = this.mFocusedWindow;
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = com.android.server.policy.PhoneWindowManagerStubImpl.DELIVE_META_APPS;
/* .line 145 */
v0 = /* .line 144 */
/* if-nez v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public Boolean interceptUnhandledKey ( android.view.KeyEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 217 */
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 218 */
/* .local v0, "keyCode":I */
/* const/16 v1, 0xc1 */
/* if-ne v0, v1, :cond_0 */
/* .line 219 */
(( android.view.KeyEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
/* .line 220 */
/* .local v1, "device":Landroid/view/InputDevice; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = (( android.view.InputDevice ) v1 ).isXiaomiStylus ( ); // invoke-virtual {v1}, Landroid/view/InputDevice;->isXiaomiStylus()I
int v3 = 3; // const/4 v3, 0x3
/* if-lt v2, v3, :cond_0 */
/* .line 221 */
int v2 = 1; // const/4 v2, 0x1
/* .line 224 */
} // .end local v1 # "device":Landroid/view/InputDevice;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean interceptWakeKey ( android.view.KeyEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 128 */
com.xiaomi.mirror.MirrorManager .get ( );
v0 = (( com.xiaomi.mirror.MirrorManager ) v0 ).isWorking ( ); // invoke-virtual {v0}, Lcom/xiaomi/mirror/MirrorManager;->isWorking()Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( android.view.KeyEvent ) p1 ).isWakeKey ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->isWakeKey()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 129 */
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* const/16 v1, 0x52 */
/* if-ne v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* .line 132 */
} // :cond_1
v0 = (( android.view.KeyEvent ) p1 ).isCanceled ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z
} // .end method
public Boolean isEnableCombinationPowerVolumeDownScreenShot ( ) {
/* .locals 2 */
/* .line 168 */
v0 = this.mMiuiKeyShortcutRuleManager;
/* if-nez v0, :cond_0 */
/* .line 169 */
/* const-class v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager; */
this.mMiuiKeyShortcutRuleManager = v0;
/* .line 172 */
} // :cond_0
v0 = this.mMiuiKeyShortcutRuleManager;
final String v1 = "key_combination_power_volume_down"; // const-string v1, "key_combination_power_volume_down"
(( com.android.server.policy.MiuiKeyShortcutRuleManager ) v0 ).getFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 174 */
/* .local v0, "powerVolumeDownFunction":Ljava/lang/String; */
/* const-string/jumbo v1, "screen_shot" */
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
public Boolean isInHangUpState ( ) {
/* .locals 1 */
/* .line 88 */
v0 = this.mPowerManagerServiceImpl;
v0 = (( com.android.server.power.PowerManagerServiceStub ) v0 ).isInHangUpState ( ); // invoke-virtual {v0}, Lcom/android/server/power/PowerManagerServiceStub;->isInHangUpState()Z
} // .end method
public Boolean isPad ( ) {
/* .locals 1 */
/* .line 140 */
com.miui.server.input.PadManager .getInstance ( );
v0 = (( com.miui.server.input.PadManager ) v0 ).isPad ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z
} // .end method
public void logMessageRemoved ( android.os.Handler p0, Integer p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .param p2, "what" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 201 */
v0 = (( android.os.Handler ) p1 ).hasMessages ( p2 ); // invoke-virtual {p1, p2}, Landroid/os/Handler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 202 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "remove message what= "; // const-string v1, "remove message what= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " reason= "; // const-string v1, " reason= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 204 */
} // :cond_0
return;
} // .end method
public Boolean noConfirmForShutdown ( ) {
/* .locals 2 */
/* .line 194 */
/* const-string/jumbo v0, "zhuque" */
v1 = miui.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 195 */
int v0 = 1; // const/4 v0, 0x1
/* .line 197 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void notifyPhotoHandleConnectionStatus ( Boolean p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "connection" # Z */
/* .param p2, "deviceId" # I */
/* .line 229 */
/* iput-boolean p1, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mPhotoHandleConnection:Z */
/* .line 230 */
/* iput p2, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mPhotoHandleEventDeviceId:I */
/* .line 231 */
return;
} // .end method
public void setFocusedWindow ( com.android.server.policy.WindowManagerPolicy$WindowState p0 ) {
/* .locals 0 */
/* .param p1, "focusedWindow" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .line 136 */
this.mFocusedWindow = p1;
/* .line 137 */
return;
} // .end method
public void setForcedDisplayDensityForUser ( android.view.IWindowManager p0 ) {
/* .locals 3 */
/* .param p1, "windowManager" # Landroid/view/IWindowManager; */
/* .line 93 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 96 */
try { // :try_start_0
/* iget v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->SUB_DISPLAY_ID:I */
/* const/16 v1, 0xf0 */
int v2 = -2; // const/4 v2, -0x2
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 98 */
/* .line 97 */
/* :catch_0 */
/* move-exception v0 */
/* .line 100 */
} // :cond_0
} // :goto_0
return;
} // .end method
public Boolean shouldDispatchInputWhenNonInteractive ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "keyCode" # I */
/* .line 73 */
/* iget-boolean v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mSupportAOD:Z */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v2, 0x162 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* if-ne p1, v2, :cond_0 */
/* .line 74 */
/* .line 76 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->SUPPORT_FOD:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
if ( p1 != null) { // if-eqz p1, :cond_1
/* if-ne p1, v2, :cond_2 */
} // :cond_1
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public Boolean shouldMoveDisplayToTop ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "keyCode" # I */
/* .line 80 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_FOLD_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mSupportAOD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x162 */
/* if-ne p1, v0, :cond_0 */
/* .line 81 */
int v0 = 0; // const/4 v0, 0x0
/* .line 83 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean skipInterceptMacroEvent ( android.view.KeyEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 234 */
/* const/16 v0, 0x139 */
v1 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, v1, :cond_1 */
/* .line 236 */
/* iget-boolean v0, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mPhotoHandleConnection:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( android.view.KeyEvent ) p1 ).getDeviceId ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDeviceId()I
/* iget v1, p0, Lcom/android/server/policy/PhoneWindowManagerStubImpl;->mPhotoHandleEventDeviceId:I */
/* if-ne v0, v1, :cond_0 */
v0 = this.mFocusedWindow;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 238 */
/* .line 237 */
final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
/* nop */
/* .line 236 */
} // :goto_0
/* .line 240 */
} // :cond_1
} // .end method
public Boolean skipKeyGesutre ( android.view.KeyEvent p0 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 207 */
v0 = this.mMiuiKeyShortcutRuleManager;
/* if-nez v0, :cond_0 */
/* .line 208 */
/* const-class v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager; */
this.mMiuiKeyShortcutRuleManager = v0;
/* .line 211 */
} // :cond_0
v0 = this.mMiuiKeyShortcutRuleManager;
v0 = (( com.android.server.policy.MiuiKeyShortcutRuleManager ) v0 ).skipKeyGesutre ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->skipKeyGesutre(Landroid/view/KeyEvent;)Z
} // .end method
public void systemBooted ( ) {
/* .locals 1 */
/* .line 104 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mDisplayTurnoverManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 105 */
(( com.android.server.policy.DisplayTurnoverManager ) v0 ).systemReady ( ); // invoke-virtual {v0}, Lcom/android/server/policy/DisplayTurnoverManager;->systemReady()V
/* .line 107 */
} // :cond_0
return;
} // .end method
public void trackGlobalActions ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 179 */
final String v0 = "key_combination_power_volume_down"; // const-string v0, "key_combination_power_volume_down"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 180 */
/* const-string/jumbo v0, "screen_shot" */
/* .local v0, "trackFunction":Ljava/lang/String; */
/* .line 182 */
} // .end local v0 # "trackFunction":Ljava/lang/String;
} // :cond_0
/* const-string/jumbo v0, "show_global_action" */
/* .line 185 */
/* .restart local v0 # "trackFunction":Ljava/lang/String; */
} // :goto_0
android.app.ActivityThread .currentActivityThread ( );
(( android.app.ActivityThread ) v1 ).getSystemContext ( ); // invoke-virtual {v1}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;
/* .line 184 */
com.android.server.input.shortcut.ShortcutOneTrackHelper .getInstance ( v1 );
/* .line 185 */
(( com.android.server.input.shortcut.ShortcutOneTrackHelper ) v1 ).trackShortcutEventTrigger ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V
/* .line 187 */
return;
} // .end method
public Boolean triggerModifierShortcut ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 161 */
com.android.server.policy.MiuiKeyInterceptExtend .getInstance ( p1 );
v0 = (( com.android.server.policy.MiuiKeyInterceptExtend ) v0 ).getKeyboardShortcutEnable ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getKeyboardShortcutEnable()Z
/* if-nez v0, :cond_0 */
/* .line 162 */
com.android.server.policy.MiuiKeyInterceptExtend .getInstance ( p1 );
v0 = (( com.android.server.policy.MiuiKeyInterceptExtend ) v0 ).getAospKeyboardShortcutEnable ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getAospKeyboardShortcutEnable()Z
/* if-nez v0, :cond_1 */
/* .line 163 */
} // :cond_0
com.miui.server.input.PadManager .getInstance ( );
v0 = (( com.miui.server.input.PadManager ) v0 ).isPad ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z
/* if-nez v0, :cond_2 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 161 */
} // :goto_0
} // .end method
