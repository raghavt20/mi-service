public class com.android.server.policy.FindDevicePowerOffLocateManager {
	 /* .source "FindDevicePowerOffLocateManager.java" */
	 /* # static fields */
	 public static final java.lang.String GLOBAL_ACTIONS;
	 public static final java.lang.String IMPERCEPTIBLE_POWER_PRESS;
	 /* # direct methods */
	 public com.android.server.policy.FindDevicePowerOffLocateManager ( ) {
		 /* .locals 0 */
		 /* .line 7 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static void sendFindDeviceLocateBroadcast ( android.content.Context p0, java.lang.String p1 ) {
		 /* .locals 2 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "cause" # Ljava/lang/String; */
		 /* .line 13 */
		 /* new-instance v0, Landroid/content/Intent; */
		 final String v1 = "miui.intent.action.FIND_DEVICE_LOCATE"; // const-string v1, "miui.intent.action.FIND_DEVICE_LOCATE"
		 /* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
		 /* .line 14 */
		 /* .local v0, "intent":Landroid/content/Intent; */
		 final String v1 = "com.xiaomi.finddevice"; // const-string v1, "com.xiaomi.finddevice"
		 (( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 15 */
		 final String v1 = "cause"; // const-string v1, "cause"
		 (( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 16 */
		 v1 = android.os.UserHandle.OWNER;
		 (( android.content.Context ) p0 ).sendBroadcastAsUser ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
		 /* .line 17 */
		 return;
	 } // .end method
