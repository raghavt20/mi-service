public abstract class com.android.server.policy.MiuiShortcutObserver$MiuiShortcutListener {
	 /* .source "MiuiShortcutObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/policy/MiuiShortcutObserver; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "MiuiShortcutListener" */
} // .end annotation
/* # virtual methods */
public void onCombinationChanged ( com.android.server.policy.MiuiCombinationRule p0 ) {
/* .locals 0 */
/* .param p1, "rule" # Lcom/android/server/policy/MiuiCombinationRule; */
/* .line 237 */
return;
} // .end method
public void onGestureChanged ( com.android.server.policy.MiuiGestureRule p0 ) {
/* .locals 0 */
/* .param p1, "rule" # Lcom/android/server/policy/MiuiGestureRule; */
/* .line 245 */
return;
} // .end method
public void onSingleChanged ( com.android.server.policy.MiuiSingleKeyRule p0, android.net.Uri p1 ) {
/* .locals 0 */
/* .param p1, "rule" # Lcom/android/server/policy/MiuiSingleKeyRule; */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 241 */
return;
} // .end method
