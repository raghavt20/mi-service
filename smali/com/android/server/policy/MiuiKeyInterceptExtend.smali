.class public Lcom/android/server/policy/MiuiKeyInterceptExtend;
.super Ljava/lang/Object;
.source "MiuiKeyInterceptExtend.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;,
        Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;
    }
.end annotation


# static fields
.field private static final ALIYUN_PACKAGE_NAME:Ljava/lang/String; = "com.aliyun.wuying.enterprise"

.field private static volatile INSTANCE:Lcom/android/server/policy/MiuiKeyInterceptExtend; = null

.field private static final TAG:Ljava/lang/String; = "MiuiKeyIntercept"

.field public static final TYPE_INTERCEPT_AOSP:I = 0x3

.field public static final TYPE_INTERCEPT_MIUI:I = 0x2

.field public static final TYPE_INTERCEPT_MIUI_AND_AOSP:I = 0x1

.field public static final TYPE_INTERCEPT_MIUI_AND_AOSP_NO_PASS_TO_USER:I = 0x4

.field public static final TYPE_INTERCEPT_NULL:I


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private volatile mIsAospKeyboardShortcutEnable:Z

.field private volatile mIsKeyboardShortcutEnable:Z

.field private volatile mIsKidMode:Z

.field private mIsScreenOn:Z

.field private mPartialScreenShot:Ljava/lang/Runnable;

.field private final mPowerManager:Landroid/os/PowerManager;

.field private mScreenOnWhenDown:Z

.field private mScreenShot:Ljava/lang/Runnable;

.field private final mSettingsObserver:Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;

.field private final mSkipInterceptWindows:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Landroid/view/KeyboardShortcutInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private mTriggerLongPress:Z


# direct methods
.method public static synthetic $r8$lambda$eg_4qyiJV-YCpjWbzRo2lrXSZaI(Lcom/android/server/policy/MiuiKeyInterceptExtend;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->lambda$interceptMiuiKeyboard$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/policy/MiuiKeyInterceptExtend;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTriggerLongPress(Lcom/android/server/policy/MiuiKeyInterceptExtend;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mTriggerLongPress:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmTriggerLongPress(Lcom/android/server/policy/MiuiKeyInterceptExtend;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mTriggerLongPress:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->INSTANCE:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mSkipInterceptWindows:Ljava/util/Map;

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mScreenOnWhenDown:Z

    .line 66
    iput-boolean v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsKeyboardShortcutEnable:Z

    .line 75
    new-instance v0, Lcom/android/server/policy/MiuiKeyInterceptExtend$1;

    invoke-direct {v0, p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend$1;-><init>(Lcom/android/server/policy/MiuiKeyInterceptExtend;)V

    iput-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mPartialScreenShot:Ljava/lang/Runnable;

    .line 88
    new-instance v0, Lcom/android/server/policy/MiuiKeyInterceptExtend$2;

    invoke-direct {v0, p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend$2;-><init>(Lcom/android/server/policy/MiuiKeyInterceptExtend;)V

    iput-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mScreenShot:Ljava/lang/Runnable;

    .line 103
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mHandler:Landroid/os/Handler;

    .line 104
    iput-object p1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mContext:Landroid/content/Context;

    .line 105
    invoke-direct {p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->initSystemSpecialWindow()V

    .line 106
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mAudioManager:Landroid/media/AudioManager;

    .line 107
    new-instance v0, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;

    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;-><init>(Lcom/android/server/policy/MiuiKeyInterceptExtend;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mSettingsObserver:Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;

    .line 108
    invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;->registerObserver()V

    .line 109
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mPowerManager:Landroid/os/PowerManager;

    .line 110
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/policy/MiuiKeyInterceptExtend;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 121
    sget-object v0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->INSTANCE:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    if-nez v0, :cond_1

    .line 122
    const-class v0, Lcom/android/server/policy/MiuiKeyInterceptExtend;

    monitor-enter v0

    .line 123
    :try_start_0
    sget-object v1, Lcom/android/server/policy/MiuiKeyInterceptExtend;->INSTANCE:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    if-nez v1, :cond_0

    .line 124
    new-instance v1, Lcom/android/server/policy/MiuiKeyInterceptExtend;

    invoke-direct {v1, p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/policy/MiuiKeyInterceptExtend;->INSTANCE:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    .line 126
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 128
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->INSTANCE:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    return-object v0
.end method

.method private getKeyInterceptType(Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;Landroid/view/KeyEvent;ILcom/android/server/policy/WindowManagerPolicy$WindowState;)I
    .locals 10
    .param p1, "interceptStage"    # Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;
    .param p2, "event"    # Landroid/view/KeyEvent;
    .param p3, "policyFlags"    # I
    .param p4, "focusedWin"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 273
    const/4 v0, 0x0

    if-eqz p2, :cond_e

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v1

    if-ltz v1, :cond_e

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 274
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/InputDevice;->isFullKeyboard()Z

    move-result v1

    if-eqz v1, :cond_e

    sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v1, :cond_0

    goto/16 :goto_3

    .line 279
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getAospKeyboardShortcutEnable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 281
    const/4 v0, 0x2

    return v0

    .line 284
    :cond_1
    iget-boolean v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsKidMode:Z

    const/4 v2, 0x1

    if-nez v1, :cond_b

    invoke-virtual {p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getKeyboardShortcutEnable()Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_2

    .line 299
    :cond_2
    if-nez p4, :cond_3

    .line 300
    return v0

    .line 304
    :cond_3
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mSkipInterceptWindows:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 305
    .local v3, "entry":Ljava/util/Map$Entry;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 306
    .local v4, "packageName":Ljava/lang/String;
    nop

    .line 307
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 308
    .local v5, "currentShortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/KeyboardShortcutInfo;>;"
    invoke-interface {p4}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 309
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_4

    .line 311
    return v2

    .line 313
    :cond_4
    const-string v6, "MiuiKeyIntercept"

    const-string v7, "Ready intercept meta"

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/KeyboardShortcutInfo;

    .line 315
    .local v7, "info":Landroid/view/KeyboardShortcutInfo;
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    invoke-virtual {v7}, Landroid/view/KeyboardShortcutInfo;->getKeycode()I

    move-result v9

    if-ne v8, v9, :cond_5

    .line 317
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v8

    invoke-virtual {v7}, Landroid/view/KeyboardShortcutInfo;->getModifiers()I

    move-result v9

    .line 316
    invoke-static {v8, v9}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 318
    return v2

    .line 319
    :cond_5
    invoke-virtual {v7}, Landroid/view/KeyboardShortcutInfo;->getKeycode()I

    move-result v8

    const/16 v9, 0x75

    if-ne v9, v8, :cond_6

    .line 320
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    if-ne v8, v9, :cond_6

    .line 322
    return v2

    .line 324
    .end local v7    # "info":Landroid/view/KeyboardShortcutInfo;
    :cond_6
    goto :goto_1

    .line 327
    .end local v3    # "entry":Ljava/util/Map$Entry;
    .end local v4    # "packageName":Ljava/lang/String;
    .end local v5    # "currentShortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/KeyboardShortcutInfo;>;"
    :cond_7
    goto :goto_0

    .line 329
    :cond_8
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mContext:Landroid/content/Context;

    invoke-static {p1, v1, p2}, Lcom/miui/server/input/custom/InputMiuiDesktopMode;->getKeyInterceptType(Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;Landroid/content/Context;Landroid/view/KeyEvent;)I

    move-result v1

    .line 331
    .local v1, "desktopModeKeyInterceptType":I
    if-eqz v1, :cond_9

    .line 332
    return v1

    .line 335
    :cond_9
    iget-object v2, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mContext:Landroid/content/Context;

    invoke-static {v2, p2, p1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->interceptShortCutKeyIfCustomDefined(Landroid/content/Context;Landroid/view/KeyEvent;Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 336
    const/4 v0, 0x4

    return v0

    .line 339
    :cond_a
    return v0

    .line 287
    .end local v1    # "desktopModeKeyInterceptType":I
    :cond_b
    :goto_2
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsScreenOn:Z

    if-nez v0, :cond_d

    sget-object v0, Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;->BEFORE_QUEUEING:Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;

    if-ne p1, v0, :cond_d

    .line 289
    and-int/lit8 v0, p3, 0x1

    if-nez v0, :cond_c

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isWakeKey()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 290
    :cond_c
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    const/4 v1, 0x6

    const-string v5, "android.policy:KEY"

    invoke-virtual {v0, v3, v4, v1, v5}, Landroid/os/PowerManager;->wakeUp(JILjava/lang/String;)V

    .line 296
    :cond_d
    return v2

    .line 276
    :cond_e
    :goto_3
    return v0
.end method

.method private initSystemSpecialWindow()V
    .locals 6

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    new-instance v1, Landroid/view/KeyboardShortcutInfo;

    const/16 v2, 0x3d

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {v1, v4, v2, v3}, Landroid/view/KeyboardShortcutInfo;-><init>(Ljava/lang/CharSequence;II)V

    new-instance v2, Landroid/view/KeyboardShortcutInfo;

    const/16 v3, 0x75

    const/high16 v5, 0x10000

    invoke-direct {v2, v4, v3, v5}, Landroid/view/KeyboardShortcutInfo;-><init>(Ljava/lang/CharSequence;II)V

    .line 114
    invoke-static {v1, v2}, Ljava/util/List;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 113
    const-string v1, "com.aliyun.wuying.enterprise"

    const-string/jumbo v2, "system"

    invoke-direct {p0, v1, v2, v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->updateSkipInterceptWindowList(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 118
    return-void
.end method

.method private synthetic lambda$interceptMiuiKeyboard$0()V
    .locals 1

    .line 216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mTriggerLongPress:Z

    return-void
.end method

.method private updateSkipInterceptWindowList(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "reason"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Landroid/view/KeyboardShortcutInfo;",
            ">;)V"
        }
    .end annotation

    .line 344
    .local p3, "newShortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/KeyboardShortcutInfo;>;"
    if-eqz p1, :cond_4

    if-nez p3, :cond_0

    goto :goto_1

    .line 347
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "skip policy intercept because: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 351
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mSkipInterceptWindows:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 353
    .local v0, "currentInterceptShortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/KeyboardShortcutInfo;>;"
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 355
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mSkipInterceptWindows:Ljava/util/Map;

    invoke-interface {v1, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    return-void

    .line 359
    :cond_1
    if-eqz v0, :cond_3

    .line 360
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Recover history special shortcut for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 361
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 363
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mSkipInterceptWindows:Ljava/util/Map;

    invoke-interface {v1, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    return-void

    .line 366
    :cond_2
    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 368
    :cond_3
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mSkipInterceptWindows:Ljava/util/Map;

    invoke-interface {v1, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    :goto_0
    return-void

    .line 345
    .end local v0    # "currentInterceptShortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/KeyboardShortcutInfo;>;"
    :cond_4
    :goto_1
    return-void
.end method


# virtual methods
.method public getAospKeyboardShortcutEnable()Z
    .locals 1

    .line 245
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsAospKeyboardShortcutEnable:Z

    return v0
.end method

.method public getKeyInterceptTypeBeforeDispatching(Landroid/view/KeyEvent;ILcom/android/server/policy/WindowManagerPolicy$WindowState;)I
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "policyFlags"    # I
    .param p3, "focusedWin"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 255
    sget-object v0, Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;->BEFORE_DISPATCHING:Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getKeyInterceptType(Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;Landroid/view/KeyEvent;ILcom/android/server/policy/WindowManagerPolicy$WindowState;)I

    move-result v0

    return v0
.end method

.method public getKeyInterceptTypeBeforeQueueing(Landroid/view/KeyEvent;ILcom/android/server/policy/WindowManagerPolicy$WindowState;)I
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "policyFlags"    # I
    .param p3, "focusedWin"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 250
    sget-object v0, Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;->BEFORE_QUEUEING:Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getKeyInterceptType(Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;Landroid/view/KeyEvent;ILcom/android/server/policy/WindowManagerPolicy$WindowState;)I

    move-result v0

    return v0
.end method

.method public getKeyboardShortcutEnable()Z
    .locals 1

    .line 236
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsKeyboardShortcutEnable:Z

    return v0
.end method

.method public interceptMiuiKeyboard(Landroid/view/KeyEvent;Z)Z
    .locals 10
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "isScreenOn"    # Z

    .line 138
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-static {p1}, Lcom/android/server/input/InputOneTrackUtil;->shouldCountDevice(Landroid/view/InputEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/InputOneTrackUtil;->trackExternalDevice(Landroid/view/InputEvent;)V

    .line 144
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    .line 145
    .local v0, "device":Landroid/view/InputDevice;
    const/4 v1, 0x0

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Landroid/view/InputDevice;->getProductId()I

    move-result v2

    .line 146
    invoke-virtual {v0}, Landroid/view/InputDevice;->getVendorId()I

    move-result v3

    .line 145
    invoke-static {v2, v3}, Lmiui/hardware/input/MiuiKeyboardHelper;->isXiaomiKeyboard(II)Z

    move-result v2

    if-nez v2, :cond_1

    goto/16 :goto_3

    .line 150
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    .line 151
    .local v2, "keyCode":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    const/4 v4, 0x1

    if-nez v3, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    move v3, v1

    .line 153
    .local v3, "down":Z
    :goto_0
    invoke-static {p1}, Lmiui/hardware/input/MiuiKeyEventUtil;->matchMiuiKey(Landroid/view/KeyEvent;)I

    move-result v5

    .line 154
    .local v5, "miuiKeyCode":I
    const/16 v6, 0x270b

    if-ne v5, v6, :cond_4

    .line 156
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_3

    .line 157
    iget-object v4, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v4

    const-string/jumbo v6, "\u5feb\u6377\u8bed\u97f3"

    invoke-virtual {v4, v6}, Lcom/android/server/input/InputOneTrackUtil;->trackVoice2Word(Ljava/lang/String;)V

    .line 160
    :cond_3
    return v1

    .line 163
    :cond_4
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/miui/server/input/PadManager;->adjustBrightnessFromKeycode(Landroid/view/KeyEvent;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 165
    return v4

    .line 169
    :cond_5
    if-nez v3, :cond_8

    iget-boolean v6, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mScreenOnWhenDown:Z

    if-eqz v6, :cond_8

    .line 170
    invoke-static {}, Lmiui/hardware/input/MiuiKeyboardHelper;->supportFnKeyboard()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 171
    iget-object v6, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v6

    .line 173
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v7

    .line 172
    invoke-static {v7}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil$KeyBoardShortcut;->getShortcutNameByKeyCode(I)Ljava/lang/String;

    move-result-object v7

    .line 171
    invoke-virtual {v6, v7}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardShortcut(Ljava/lang/String;)V

    goto :goto_1

    .line 174
    :cond_6
    invoke-static {}, Lmiui/hardware/input/MiuiKeyboardHelper;->support6FKeyboard()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 175
    iget-object v6, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v6

    .line 176
    invoke-static {v5}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil$KeyBoardShortcut;->getShortcutNameByKeyCode(I)Ljava/lang/String;

    move-result-object v7

    .line 175
    invoke-virtual {v6, v7}, Lcom/android/server/input/InputOneTrackUtil;->track6FShortcut(Ljava/lang/String;)V

    .line 179
    :cond_7
    :goto_1
    const/4 v6, 0x0

    const-string v7, "keyboard"

    sparse-switch v5, :sswitch_data_0

    .line 209
    goto :goto_2

    .line 186
    :sswitch_0
    iget-object v8, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v8

    const-string v9, "go_to_sleep"

    invoke-virtual {v8, v9, v7, v6, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 189
    return v4

    .line 191
    :sswitch_1
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/provider/MiuiSettings$SoundMode;->isZenModeOn(Landroid/content/Context;)Z

    move-result v1

    .line 192
    .local v1, "currentZenMode":Z
    iget-object v6, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mContext:Landroid/content/Context;

    xor-int/lit8 v8, v1, 0x1

    invoke-static {v6, v8, v7}, Landroid/provider/MiuiSettings$SoundMode;->setZenModeOn(Landroid/content/Context;ZLjava/lang/String;)V

    .line 194
    return v4

    .line 196
    .end local v1    # "currentZenMode":Z
    :sswitch_2
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v1

    const-string v8, "launch_voice_assistant"

    invoke-virtual {v1, v8, v7, v6, v4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 199
    return v4

    .line 201
    :sswitch_3
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v1

    xor-int/2addr v1, v4

    .line 202
    .local v1, "result":Z
    iget-object v6, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v6, v1}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    .line 203
    return v4

    .line 205
    .end local v1    # "result":Z
    :sswitch_4
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mPartialScreenShot:Ljava/lang/Runnable;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 206
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mScreenShot:Ljava/lang/Runnable;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 207
    return v4

    .line 181
    :sswitch_5
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCapsLockOn()Z

    move-result v6

    invoke-virtual {v4, v6}, Lcom/miui/server/input/PadManager;->setCapsLockStatus(Z)V

    .line 182
    iget-object v4, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->getKeyboardManager(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    move-result-object v4

    .line 183
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCapsLockOn()Z

    move-result v6

    .line 182
    invoke-interface {v4, v6}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->setCapsLockLight(Z)V

    .line 184
    return v1

    .line 212
    :cond_8
    iput-boolean p2, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mScreenOnWhenDown:Z

    .line 213
    if-eq v5, v2, :cond_a

    if-eqz p2, :cond_a

    .line 215
    const/16 v1, 0x270a

    if-ne v5, v1, :cond_9

    .line 216
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mHandler:Landroid/os/Handler;

    new-instance v6, Lcom/android/server/policy/MiuiKeyInterceptExtend$$ExternalSyntheticLambda0;

    invoke-direct {v6, p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/policy/MiuiKeyInterceptExtend;)V

    invoke-virtual {v1, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 217
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mPartialScreenShot:Ljava/lang/Runnable;

    const-wide/16 v7, 0x12c

    invoke-virtual {v1, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 219
    :cond_9
    return v4

    .line 222
    :cond_a
    :goto_2
    return v1

    .line 148
    .end local v2    # "keyCode":I
    .end local v3    # "down":Z
    .end local v5    # "miuiKeyCode":I
    :cond_b
    :goto_3
    return v1

    nop

    :sswitch_data_0
    .sparse-switch
        0x73 -> :sswitch_5
        0x270a -> :sswitch_4
        0x270c -> :sswitch_3
        0x270d -> :sswitch_2
        0x270e -> :sswitch_1
        0x270f -> :sswitch_0
    .end sparse-switch
.end method

.method public onSystemBooted()V
    .locals 1

    .line 373
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mSettingsObserver:Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;

    invoke-static {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;->-$$Nest$mprocessCloudData(Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;)V

    .line 374
    return-void
.end method

.method public setAospKeyboardShortcutEnable(Z)V
    .locals 2
    .param p1, "isEnable"    # Z

    .line 240
    iput-boolean p1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsAospKeyboardShortcutEnable:Z

    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AOSP Keyboard Shortcut status is:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsAospKeyboardShortcutEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiKeyIntercept"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    return-void
.end method

.method public setKeyboardShortcutEnable(Z)V
    .locals 2
    .param p1, "isEnable"    # Z

    .line 231
    iput-boolean p1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsKeyboardShortcutEnable:Z

    .line 232
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MIUI Keyboard Shortcut status is:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsKeyboardShortcutEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiKeyIntercept"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    return-void
.end method

.method public setKidSpaceMode(Z)V
    .locals 2
    .param p1, "isKidMode"    # Z

    .line 226
    iput-boolean p1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsKidMode:Z

    .line 227
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KidMode status is:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiKeyIntercept"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    return-void
.end method

.method public setScreenState(Z)V
    .locals 0
    .param p1, "isScreenOn"    # Z

    .line 259
    iput-boolean p1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend;->mIsScreenOn:Z

    .line 260
    return-void
.end method
