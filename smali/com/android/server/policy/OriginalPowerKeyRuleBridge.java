public class com.android.server.policy.OriginalPowerKeyRuleBridge {
	 /* .source "OriginalPowerKeyRuleBridge.java" */
	 /* # instance fields */
	 private com.android.server.policy.PhoneWindowManager$PowerKeyRule mOriginalPowerKeyRule;
	 /* # direct methods */
	 public com.android.server.policy.OriginalPowerKeyRuleBridge ( ) {
		 /* .locals 2 */
		 /* .line 8 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 9 */
		 /* const-class v0, Lcom/android/server/policy/WindowManagerPolicy; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Lcom/android/server/policy/WindowManagerPolicy; */
		 /* .line 11 */
		 /* .local v0, "windowManagerPolicy":Lcom/android/server/policy/WindowManagerPolicy; */
		 /* instance-of v1, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 12 */
			 /* move-object v1, v0 */
			 /* check-cast v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
			 /* .line 13 */
			 (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v1 ).getOriginalPowerKeyRule ( ); // invoke-virtual {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getOriginalPowerKeyRule()Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;
			 this.mOriginalPowerKeyRule = v1;
			 /* .line 15 */
		 } // :cond_0
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void onLongPress ( Long p0 ) {
		 /* .locals 1 */
		 /* .param p1, "eventTime" # J */
		 /* .line 25 */
		 v0 = this.mOriginalPowerKeyRule;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 26 */
			 (( com.android.server.policy.PhoneWindowManager$PowerKeyRule ) v0 ).onLongPress ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;->onLongPress(J)V
			 /* .line 28 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void onMultiPress ( Long p0, Integer p1 ) {
		 /* .locals 1 */
		 /* .param p1, "downTime" # J */
		 /* .param p3, "count" # I */
		 /* .line 37 */
		 v0 = this.mOriginalPowerKeyRule;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 38 */
			 (( com.android.server.policy.PhoneWindowManager$PowerKeyRule ) v0 ).onMultiPress ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;->onMultiPress(JI)V
			 /* .line 40 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void onPress ( Long p0 ) {
		 /* .locals 1 */
		 /* .param p1, "downTime" # J */
		 /* .line 18 */
		 v0 = this.mOriginalPowerKeyRule;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 19 */
			 (( com.android.server.policy.PhoneWindowManager$PowerKeyRule ) v0 ).onPress ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;->onPress(J)V
			 /* .line 22 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void onVeryLongPress ( Long p0 ) {
		 /* .locals 1 */
		 /* .param p1, "eventTime" # J */
		 /* .line 31 */
		 v0 = this.mOriginalPowerKeyRule;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 32 */
			 (( com.android.server.policy.PhoneWindowManager$PowerKeyRule ) v0 ).onVeryLongPress ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager$PowerKeyRule;->onVeryLongPress(J)V
			 /* .line 34 */
		 } // :cond_0
		 return;
	 } // .end method
