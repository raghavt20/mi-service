public abstract class com.android.server.policy.BaseMiuiPhoneWindowManager extends com.android.server.policy.PhoneWindowManager {
	 /* .source "BaseMiuiPhoneWindowManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;, */
	 /* Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;, */
	 /* Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;, */
	 /* Lcom/android/server/policy/BaseMiuiPhoneWindowManager$StatusBarPointEventTracker; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ACCESSIBLE_MODE_LARGE_DENSITY;
private static final Integer ACCESSIBLE_MODE_SMALL_DENSITY;
private static final Integer ACTION_DOUBLE_CLICK;
private static final java.lang.String ACTION_DUMPSYS;
private static final java.lang.String ACTION_FACTORY_RESET;
private static final Integer ACTION_LONGPRESS;
private static final java.lang.String ACTION_PARTIAL_SCREENSHOT;
private static final Integer ACTION_SINGLE_CLICK;
private static final Integer BTN_MOUSE;
private static final Integer COMBINE_VOLUME_KEY_DELAY_TIME;
private static final Boolean DEBUG;
private static final Integer DOUBLE_CLICK_AI_KEY_TIME;
private static final Integer ENABLE_HOME_KEY_DOUBLE_TAP_INTERVAL;
private static final Integer ENABLE_VOLUME_KEY_PRESS_COUNTS;
private static final Integer ENABLE_VOLUME_KEY_PRESS_INTERVAL;
public static final Integer FLAG_INJECTED_FROM_SHORTCUT;
private static final java.lang.String HOME_PACKAGE_NAME;
protected static final Integer INTERCEPT_EXPECTED_RESULT_GO_TO_SLEEP;
protected static final Integer INTERCEPT_EXPECTED_RESULT_NONE;
protected static final Integer INTERCEPT_EXPECTED_RESULT_WAKE_UP;
private static final Boolean IS_CETUS;
public static final java.lang.String IS_CUSTOM_SHORTCUTS_EFFECTIVE;
private static final Boolean IS_FLIP_DEVICE;
private static final Boolean IS_FOLD_DEVICE;
private static final java.lang.String IS_MI_INPUT_EVENT_TIME_LINE_ENABLE;
private static final Integer KEYCODE_AI;
private static final java.lang.String KEY_GAME_BOOSTER;
private static final Integer KEY_LONG_PRESS_TIMEOUT_DELAY_FOR_CAMERA_KEY;
private static final Integer KEY_LONG_PRESS_TIMEOUT_DELAY_FOR_SHOW_MENU;
private static final java.lang.String KID_MODE;
private static final Integer KID_MODE_STATUE_EXIT;
private static final Integer KID_MODE_STATUS_IN;
private static final java.lang.String KID_SPACE_ID;
private static final java.lang.String LAST_POWER_UP_EVENT;
private static final java.lang.String LAST_POWER_UP_POLICY;
private static final java.lang.String LAST_POWER_UP_SCREEN_STATE;
private static final java.lang.String LONG_LONG_PRESS_POWER_KEY;
private static final Integer LONG_PRESS_AI_KEY_TIME;
private static final Integer LONG_PRESS_VOLUME_DOWN_ACTION_NONE;
private static final Integer LONG_PRESS_VOLUME_DOWN_ACTION_PAY;
private static final Integer LONG_PRESS_VOLUME_DOWN_ACTION_STREET_SNAP;
private static final Integer META_KEY_PRESS_INTERVAL;
private static final Integer MSG_COMBINE_VOLUME_KEY_DELAY_TIME;
private static final java.lang.String PERMISSION_INTERNAL_GENERAL_API;
protected static final java.lang.String REASON_FP_DPAD_CENTER_WAKEUP;
private static final Integer SHORTCUT_BACK_POWER;
private static final Integer SHORTCUT_HOME_POWER;
private static final Integer SHORTCUT_MENU_POWER;
private static final Integer SHORTCUT_SCREENSHOT_ANDROID;
private static final Integer SHORTCUT_SCREENSHOT_MIUI;
private static final Integer SHORTCUT_SCREENSHOT_SINGLE_KEY;
private static final Integer SHORTCUT_UNLOCK;
protected static final Boolean SUPPORT_EDGE_TOUCH_VOLUME;
protected static final Boolean SUPPORT_POWERFP;
private static final java.lang.String SYNERGY_MODE;
private static final java.lang.String SYSTEM_SETTINGS_VR_MODE;
private static final java.util.List TALK_BACK_SERVICE_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Landroid/content/ComponentName;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String mPhoneProduct;
private static com.android.server.policy.PhoneWindowManagerFeatureImpl phoneWindowManagerFeature;
static final java.util.ArrayList sScreenRecorderKeyEventList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
static final java.util.ArrayList sVoiceAssistKeyEventList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
Boolean mAccessibilityShortcutOnLockScreen;
private miui.provider.SettingsStringUtil$SettingStringHelper mAccessibilityShortcutSetting;
private android.os.PowerManager$WakeLock mAiKeyWakeLock;
private android.media.AudioManager mAudioManager;
private com.miui.server.input.AutoDisableScreenButtonsManager mAutoDisableScreenButtonsManager;
private android.os.Binder mBinder;
android.content.BroadcastReceiver mBootCompleteReceiver;
private android.widget.ProgressBar mBootProgress;
private java.lang.String mBootText;
private android.widget.TextView mBootTextView;
Boolean mCameraKeyWakeScreen;
private Integer mCurrentUserId;
private Integer mDoubleClickAiKeyCount;
private Boolean mDoubleClickAiKeyIsConsumed;
private java.lang.Runnable mDoubleClickAiKeyRunnable;
Boolean mDpadCenterDown;
private com.miui.server.input.edgesuppression.EdgeSuppressionManager mEdgeSuppressionManager;
protected com.android.server.policy.WindowManagerPolicy$WindowState mFocusedWindow;
private Boolean mFolded;
private Boolean mForbidFullScreen;
protected java.util.List mFpNavEventNameList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
protected Boolean mFrontFingerprintSensor;
protected android.os.Handler mHandler;
private miui.util.HapticFeedbackUtil mHapticFeedbackUtil;
private Boolean mHasWatchedRotation;
Boolean mHaveBankCard;
Boolean mHaveTranksCard;
private android.os.PowerManager$WakeLock mHelpKeyWakeLock;
Boolean mHomeConsumed;
Boolean mHomeDoubleClickPending;
private final java.lang.Runnable mHomeDoubleClickTimeoutRunnable;
Boolean mHomeDoubleTapPending;
private final java.lang.Runnable mHomeDoubleTapTimeoutRunnable;
Boolean mHomeDownAfterDpCenter;
private final android.view.IDisplayFoldListener mIDisplayFoldListener;
private java.lang.String mImperceptiblePowerKey;
private miui.hardware.input.InputFeature mInputFeature;
private Integer mInputMethodWindowVisibleHeight;
private android.content.BroadcastReceiver mInternalBroadcastReceiver;
private Boolean mIsFoldChanged;
private Boolean mIsKidMode;
protected Boolean mIsStatusBarVisibleInFullscreen;
private Boolean mIsSupportGloablTounchDirection;
private Boolean mIsSynergyMode;
private Boolean mIsVRMode;
private Integer mKeyBoardDeviceId;
protected Integer mKeyLongPressTimeout;
private Boolean mKeyguardOnWhenHomeDown;
private Long mLastClickAiKeyTime;
private Boolean mLongPressAiKeyIsConsumed;
private java.lang.Runnable mLongPressDownAiKeyRunnable;
private java.lang.Runnable mLongPressUpAiKeyRunnable;
private Integer mLongPressVolumeDownBehavior;
private Integer mMetaKeyAction;
private Boolean mMetaKeyConsume;
private final java.lang.Runnable mMetaKeyRunnable;
Boolean mMikeymodeEnabled;
private com.miui.server.input.MiuiBackTapGestureService mMiuiBackTapGestureService;
private android.app.Dialog mMiuiBootMsgDialog;
private com.miui.server.input.MiuiFingerPrintTapListener mMiuiFingerPrintTapListener;
private com.android.server.input.MiuiInputManagerInternal mMiuiInputManagerInternal;
private com.android.server.policy.MiuiKeyInterceptExtend mMiuiKeyInterceptExtend;
protected com.android.server.policy.MiuiKeyShortcutRuleManager mMiuiKeyShortcutRuleManager;
protected com.android.server.policy.MiuiKeyguardServiceDelegate mMiuiKeyguardDelegate;
private com.miui.server.input.knock.MiuiKnockGestureService mMiuiKnockGestureService;
private com.android.server.input.padkeyboard.MiuiPadKeyboardManager mMiuiPadKeyboardManager;
private com.android.server.input.pocketmode.MiuiPocketModeManager mMiuiPocketModeManager;
protected com.android.server.policy.MiuiShortcutTriggerHelper mMiuiShortcutTriggerHelper;
protected com.miui.server.input.stylus.MiuiStylusShortcutManager mMiuiStylusShortcutManager;
protected com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager mMiuiThreeGestureListener;
private com.miui.server.input.time.MiuiTimeFloatingWindow mMiuiTimeFloatingWindow;
Integer mNavBarHeight;
Integer mNavBarHeightLand;
Integer mNavBarWidth;
android.content.BroadcastReceiver mPartialScreenshotReceiver;
java.lang.Runnable mPowerLongPressOriginal;
private com.android.server.policy.MiuiScreenOnProximityLock mProximitySensor;
private com.android.server.policy.BaseMiuiPhoneWindowManager$RotationWatcher mRotationWatcher;
protected Integer mScreenOffReason;
android.content.BroadcastReceiver mScreenRecordeEnablekKeyEventReceiver;
private Boolean mScreenRecorderEnabled;
android.content.BroadcastReceiver mScreenshotReceiver;
private com.android.server.policy.BaseMiuiPhoneWindowManager$MiuiSettingsObserver mSettingsObserver;
private com.android.server.input.shortcut.ShortcutOneTrackHelper mShortcutOneTrackHelper;
Boolean mShortcutServiceIsTalkBack;
private com.android.server.input.shoulderkey.ShoulderKeyManagerInternal mShoulderKeyManagerInternal;
private java.lang.Runnable mSingleClickAiKeyRunnable;
private miui.util.SmartCoverManager mSmartCoverManager;
private com.miui.server.stability.StabilityLocalServiceInternal mStabilityLocalServiceInternal;
android.content.BroadcastReceiver mStatusBarExitFullscreenReceiver;
protected Boolean mSupportTapFingerprintSensorToHome;
private java.util.HashSet mSystemKeyPackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mSystemShortcutsMenuShown;
Boolean mTalkBackIsOpened;
Boolean mTestModeEnabled;
private com.android.server.tof.TofManagerInternal mTofManagerInternal;
private Integer mTrackDumpLogKeyCodeLastKeyCode;
private Boolean mTrackDumpLogKeyCodePengding;
private Long mTrackDumpLogKeyCodeStartTime;
private Integer mTrackDumpLogKeyCodeTimeOut;
private Integer mTrackDumpLogKeyCodeVolumeDownTimes;
Boolean mTrackballWakeScreen;
private volatile Long mUpdateWakeUpDetailTime;
private Boolean mVoiceAssistEnabled;
private Long mVolumeButtonPrePressedTime;
private Long mVolumeButtonPressedCount;
private Boolean mVolumeDownKeyConsumed;
private Boolean mVolumeDownKeyPressed;
private Long mVolumeDownKeyTime;
private android.os.PowerManager$WakeLock mVolumeKeyUpWakeLock;
private android.os.PowerManager$WakeLock mVolumeKeyWakeLock;
private Boolean mVolumeUpKeyConsumed;
private Boolean mVolumeUpKeyPressed;
private Long mVolumeUpKeyTime;
private com.android.server.contentcatcher.WakeUpContentCatcherManager mWakeUpContentCatcherManager;
private volatile java.lang.String mWakeUpDetail;
private final com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper$ProximitySensorChangeListener mWakeUpKeySensorListener;
private volatile java.lang.String mWakeUpReason;
private Boolean mWifiOnly;
private com.android.server.policy.WindowManagerPolicy$WindowState mWin;
/* # direct methods */
public static void $r8$lambda$91AwqgtuG8ejgavYkJ8k3OPNGG0 ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->lambda$finishLayoutLw$4(I)V */
return;
} // .end method
public static void $r8$lambda$BaDR-tBbngxKLWmgGeE4hvFzcsE ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->lambda$trackDumpLogKeyCode$5()V */
return;
} // .end method
public static void $r8$lambda$WVFdnZ_k2bzoGN3LX5sPVDN_K4I ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, android.content.Intent p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->lambda$sendAsyncBroadcast$3(Landroid/content/Intent;)V */
return;
} // .end method
public static void $r8$lambda$sWY5LyklPrVG68MiImrvvG9iAV4 ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->lambda$setCurrentUserLw$1()V */
return;
} // .end method
public static void $r8$lambda$t0GcjyAaHKRtE5bMyu-Xbz_4chg ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, java.lang.String p1, android.content.Intent p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->lambda$takeScreenshot$0(Ljava/lang/String;Landroid/content/Intent;)V */
return;
} // .end method
public static void $r8$lambda$wZoOyBvQVQ_WcuRzH1eGqDhXKB0 ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, android.content.Intent p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->lambda$sendAsyncBroadcast$2(Landroid/content/Intent;)V */
return;
} // .end method
static miui.provider.SettingsStringUtil$SettingStringHelper -$$Nest$fgetmAccessibilityShortcutSetting ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAccessibilityShortcutSetting;
} // .end method
static android.widget.ProgressBar -$$Nest$fgetmBootProgress ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBootProgress;
} // .end method
static java.lang.String -$$Nest$fgetmBootText ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBootText;
} // .end method
static android.widget.TextView -$$Nest$fgetmBootTextView ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBootTextView;
} // .end method
static Integer -$$Nest$fgetmCurrentUserId ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCurrentUserId:I */
} // .end method
static com.miui.server.input.edgesuppression.EdgeSuppressionManager -$$Nest$fgetmEdgeSuppressionManager ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mEdgeSuppressionManager;
} // .end method
static Boolean -$$Nest$fgetmFolded ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFolded:Z */
} // .end method
static miui.util.HapticFeedbackUtil -$$Nest$fgetmHapticFeedbackUtil ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHapticFeedbackUtil;
} // .end method
static Boolean -$$Nest$fgetmIsFoldChanged ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsFoldChanged:Z */
} // .end method
static Boolean -$$Nest$fgetmIsKidMode ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsKidMode:Z */
} // .end method
static Boolean -$$Nest$fgetmIsSupportGloablTounchDirection ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsSupportGloablTounchDirection:Z */
} // .end method
static Boolean -$$Nest$fgetmIsSynergyMode ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsSynergyMode:Z */
} // .end method
static Integer -$$Nest$fgetmKeyBoardDeviceId ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyBoardDeviceId:I */
} // .end method
static Integer -$$Nest$fgetmMetaKeyAction ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyAction:I */
} // .end method
static Boolean -$$Nest$fgetmMetaKeyConsume ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z */
} // .end method
static com.miui.server.input.MiuiBackTapGestureService -$$Nest$fgetmMiuiBackTapGestureService ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiuiBackTapGestureService;
} // .end method
static android.app.Dialog -$$Nest$fgetmMiuiBootMsgDialog ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiuiBootMsgDialog;
} // .end method
static com.android.server.input.pocketmode.MiuiPocketModeManager -$$Nest$fgetmMiuiPocketModeManager ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiuiPocketModeManager;
} // .end method
static com.android.server.policy.MiuiScreenOnProximityLock -$$Nest$fgetmProximitySensor ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProximitySensor;
} // .end method
static com.android.server.input.shortcut.ShortcutOneTrackHelper -$$Nest$fgetmShortcutOneTrackHelper ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mShortcutOneTrackHelper;
} // .end method
static com.miui.server.stability.StabilityLocalServiceInternal -$$Nest$fgetmStabilityLocalServiceInternal ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mStabilityLocalServiceInternal;
} // .end method
static com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper$ProximitySensorChangeListener -$$Nest$fgetmWakeUpKeySensorListener ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mWakeUpKeySensorListener;
} // .end method
static Boolean -$$Nest$fgetmWifiOnly ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWifiOnly:Z */
} // .end method
static void -$$Nest$fputmAccessibilityShortcutSetting ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, miui.provider.SettingsStringUtil$SettingStringHelper p1 ) { //bridge//synthethic
/* .locals 0 */
this.mAccessibilityShortcutSetting = p1;
return;
} // .end method
static void -$$Nest$fputmBootProgress ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, android.widget.ProgressBar p1 ) { //bridge//synthethic
/* .locals 0 */
this.mBootProgress = p1;
return;
} // .end method
static void -$$Nest$fputmBootText ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, java.lang.String[] p1 ) { //bridge//synthethic
/* .locals 0 */
this.mBootText = p1;
return;
} // .end method
static void -$$Nest$fputmBootTextView ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, android.widget.TextView p1 ) { //bridge//synthethic
/* .locals 0 */
this.mBootTextView = p1;
return;
} // .end method
static void -$$Nest$fputmDoubleClickAiKeyCount ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyCount:I */
return;
} // .end method
static void -$$Nest$fputmFolded ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFolded:Z */
return;
} // .end method
static void -$$Nest$fputmForbidFullScreen ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mForbidFullScreen:Z */
return;
} // .end method
static void -$$Nest$fputmImperceptiblePowerKey ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mImperceptiblePowerKey = p1;
return;
} // .end method
static void -$$Nest$fputmIsFoldChanged ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsFoldChanged:Z */
return;
} // .end method
static void -$$Nest$fputmIsSynergyMode ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsSynergyMode:Z */
return;
} // .end method
static void -$$Nest$fputmIsVRMode ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsVRMode:Z */
return;
} // .end method
static void -$$Nest$fputmLongPressAiKeyIsConsumed ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressAiKeyIsConsumed:Z */
return;
} // .end method
static void -$$Nest$fputmLongPressVolumeDownBehavior ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressVolumeDownBehavior:I */
return;
} // .end method
static void -$$Nest$fputmMetaKeyConsume ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z */
return;
} // .end method
static void -$$Nest$fputmMiuiBootMsgDialog ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, android.app.Dialog p1 ) { //bridge//synthethic
/* .locals 0 */
this.mMiuiBootMsgDialog = p1;
return;
} // .end method
static void -$$Nest$fputmMiuiPocketModeManager ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, com.android.server.input.pocketmode.MiuiPocketModeManager p1 ) { //bridge//synthethic
/* .locals 0 */
this.mMiuiPocketModeManager = p1;
return;
} // .end method
static void -$$Nest$fputmVoiceAssistEnabled ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVoiceAssistEnabled:Z */
return;
} // .end method
static void -$$Nest$mcloseTalkBack ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->closeTalkBack()V */
return;
} // .end method
static void -$$Nest$mhandleTouchFeatureRotationWatcher ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->handleTouchFeatureRotationWatcher()V */
return;
} // .end method
static Boolean -$$Nest$mhasTalkbackService ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->hasTalkbackService(Ljava/lang/String;)Z */
} // .end method
static Boolean -$$Nest$misGameMode ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isGameMode()Z */
} // .end method
static Boolean -$$Nest$misTalkBackService ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isTalkBackService(Ljava/lang/String;)Z */
} // .end method
static void -$$Nest$mmarkShortcutTriggered ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->markShortcutTriggered()V */
return;
} // .end method
static void -$$Nest$mnotifyKidSpaceChanged ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->notifyKidSpaceChanged(Z)V */
return;
} // .end method
static void -$$Nest$mreleaseScreenOnProximitySensor ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->releaseScreenOnProximitySensor(Z)V */
return;
} // .end method
static void -$$Nest$msetScreenRecorderEnabled ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->setScreenRecorderEnabled(Z)V */
return;
} // .end method
static void -$$Nest$msetStatusBarInFullscreen ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->setStatusBarInFullscreen(Z)V */
return;
} // .end method
static void -$$Nest$msetTouchFeatureRotation ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->setTouchFeatureRotation()V */
return;
} // .end method
static void -$$Nest$mshowKeyboardShortcutsMenu ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Integer p1, Boolean p2, Boolean p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showKeyboardShortcutsMenu(IZZ)V */
return;
} // .end method
static Boolean -$$Nest$mshowMenu ( com.android.server.policy.BaseMiuiPhoneWindowManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showMenu()Z */
} // .end method
static void -$$Nest$mshowRecentApps ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showRecentApps(Z)V */
return;
} // .end method
static void -$$Nest$mstartAiKeyService ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->startAiKeyService(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mtakeScreenshot ( com.android.server.policy.BaseMiuiPhoneWindowManager p0, android.content.Intent p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->takeScreenshot(Landroid/content/Intent;Ljava/lang/String;)V */
return;
} // .end method
static com.android.server.policy.PhoneWindowManagerFeatureImpl -$$Nest$sfgetphoneWindowManagerFeature ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager.phoneWindowManagerFeature;
} // .end method
static com.android.server.policy.BaseMiuiPhoneWindowManager ( ) {
/* .locals 11 */
/* .line 145 */
v0 = miui.util.MiuiMultiDisplayTypeInfo .isFoldDevice ( );
com.android.server.policy.BaseMiuiPhoneWindowManager.IS_FOLD_DEVICE = (v0!= 0);
/* .line 146 */
v0 = miui.util.MiuiMultiDisplayTypeInfo .isFlipDevice ( );
com.android.server.policy.BaseMiuiPhoneWindowManager.IS_FLIP_DEVICE = (v0!= 0);
/* .line 171 */
/* new-instance v0, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl; */
/* invoke-direct {v0}, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;-><init>()V */
/* .line 193 */
/* nop */
/* .line 194 */
int v0 = 3; // const/4 v0, 0x3
v1 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v0 );
/* const/16 v2, 0x1a */
v3 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v2 );
/* or-int/2addr v1, v3 */
/* .line 195 */
/* nop */
/* .line 196 */
int v1 = 4; // const/4 v1, 0x4
/* .line 2870 */
java.lang.Integer .valueOf ( v1 );
/* .line 196 */
v4 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v1 );
v5 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v2 );
/* or-int/2addr v4, v5 */
/* .line 197 */
/* nop */
/* .line 198 */
/* const/16 v4, 0xbb */
v5 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v4 );
v6 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v2 );
/* or-int/2addr v5, v6 */
/* .line 199 */
/* nop */
/* .line 200 */
v5 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v2 );
/* const/16 v6, 0x19 */
v7 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v6 );
/* or-int/2addr v5, v7 */
/* .line 201 */
/* nop */
/* .line 202 */
v5 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v4 );
v7 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v6 );
/* or-int/2addr v5, v7 */
/* .line 203 */
/* nop */
/* .line 204 */
v5 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v0 );
v7 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v6 );
/* or-int/2addr v5, v7 */
/* .line 205 */
/* nop */
/* .line 206 */
v1 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v1 );
/* const/16 v5, 0x18 */
v7 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v5 );
/* or-int/2addr v1, v7 */
/* .line 242 */
/* const-string/jumbo v1, "support_edge_touch_volume" */
int v7 = 0; // const/4 v7, 0x0
v1 = miui.util.FeatureParser .getBoolean ( v1,v7 );
com.android.server.policy.BaseMiuiPhoneWindowManager.SUPPORT_EDGE_TOUCH_VOLUME = (v1!= 0);
/* .line 243 */
final String v1 = "ro.hardware.fp.sideCap"; // const-string v1, "ro.hardware.fp.sideCap"
v1 = android.os.SystemProperties .getBoolean ( v1,v7 );
com.android.server.policy.BaseMiuiPhoneWindowManager.SUPPORT_POWERFP = (v1!= 0);
/* .line 244 */
final String v1 = "cetus"; // const-string v1, "cetus"
v7 = miui.os.Build.DEVICE;
v1 = (( java.lang.String ) v1 ).equals ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
com.android.server.policy.BaseMiuiPhoneWindowManager.IS_CETUS = (v1!= 0);
/* .line 272 */
/* new-instance v1, Ljava/util/ArrayList; */
/* new-instance v7, Landroid/content/ComponentName; */
final String v8 = "com.google.android.marvin.talkback.TalkBackService"; // const-string v8, "com.google.android.marvin.talkback.TalkBackService"
final String v9 = "com.google.android.marvin.talkback"; // const-string v9, "com.google.android.marvin.talkback"
/* invoke-direct {v7, v9, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* new-instance v8, Landroid/content/ComponentName; */
final String v10 = ".TalkBackService"; // const-string v10, ".TalkBackService"
/* invoke-direct {v8, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* filled-new-array {v7, v8}, [Landroid/content/ComponentName; */
/* .line 273 */
java.util.Arrays .asList ( v7 );
/* invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 296 */
final String v1 = "ro.product.device"; // const-string v1, "ro.product.device"
final String v7 = "null"; // const-string v7, "null"
android.os.SystemProperties .get ( v1,v7 );
/* .line 2867 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 2869 */
java.lang.Integer .valueOf ( v0 );
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 2870 */
(( java.util.ArrayList ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 2871 */
/* const/16 v0, 0x52 */
java.lang.Integer .valueOf ( v0 );
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 2872 */
java.lang.Integer .valueOf ( v4 );
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 2873 */
java.lang.Integer .valueOf ( v5 );
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 2874 */
java.lang.Integer .valueOf ( v6 );
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 2875 */
java.lang.Integer .valueOf ( v2 );
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 2963 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 2965 */
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 2966 */
return;
} // .end method
public com.android.server.policy.BaseMiuiPhoneWindowManager ( ) {
/* .locals 5 */
/* .line 137 */
/* invoke-direct {p0}, Lcom/android/server/policy/PhoneWindowManager;-><init>()V */
/* .line 227 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyCount:I */
/* .line 230 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLastClickAiKeyTime:J */
/* .line 270 */
/* iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressVolumeDownBehavior:I */
/* .line 298 */
v3 = com.android.server.policy.BaseMiuiPhoneWindowManager.phoneWindowManagerFeature;
(( com.android.server.policy.PhoneWindowManagerFeatureImpl ) v3 ).getPowerLongPress ( p0 ); // invoke-virtual {v3, p0}, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;->getPowerLongPress(Lcom/android/server/policy/PhoneWindowManager;)Ljava/lang/Runnable;
this.mPowerLongPressOriginal = v3;
/* .line 614 */
/* new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$2; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$2;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mBootCompleteReceiver = v3;
/* .line 623 */
/* new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$3;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mIDisplayFoldListener = v3;
/* .line 703 */
/* new-instance v3, Lmiui/util/SmartCoverManager; */
/* invoke-direct {v3}, Lmiui/util/SmartCoverManager;-><init>()V */
this.mSmartCoverManager = v3;
/* .line 981 */
/* new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$4; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$4;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mSingleClickAiKeyRunnable = v3;
/* .line 990 */
/* new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$5; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$5;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mDoubleClickAiKeyRunnable = v3;
/* .line 998 */
/* new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$6; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$6;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mLongPressDownAiKeyRunnable = v3;
/* .line 1008 */
/* new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$7; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$7;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mLongPressUpAiKeyRunnable = v3;
/* .line 1058 */
/* new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$8; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$8;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mWakeUpKeySensorListener = v3;
/* .line 1130 */
/* iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyAction:I */
/* .line 1134 */
int v3 = 1; // const/4 v3, 0x1
/* iput-boolean v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z */
/* .line 1135 */
/* iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemShortcutsMenuShown:Z */
/* .line 1136 */
int v3 = -1; // const/4 v3, -0x1
/* iput v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyBoardDeviceId:I */
/* .line 1138 */
/* new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$10;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mMetaKeyRunnable = v3;
/* .line 1235 */
int v3 = 0; // const/4 v3, 0x0
this.mFpNavEventNameList = v3;
/* .line 1236 */
/* iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyguardOnWhenHomeDown:Z */
/* .line 1745 */
/* new-instance v3, Landroid/os/Binder; */
/* invoke-direct {v3}, Landroid/os/Binder;-><init>()V */
this.mBinder = v3;
/* .line 1747 */
/* new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$11; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$11;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mStatusBarExitFullscreenReceiver = v3;
/* .line 1826 */
/* new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$12; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$12;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mScreenshotReceiver = v3;
/* .line 1832 */
/* new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$13; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$13;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mPartialScreenshotReceiver = v3;
/* .line 1847 */
/* new-instance v3, Ljava/util/HashSet; */
/* invoke-direct {v3}, Ljava/util/HashSet;-><init>()V */
this.mSystemKeyPackages = v3;
/* .line 1849 */
final String v4 = "android"; // const-string v4, "android"
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1850 */
v3 = this.mSystemKeyPackages;
final String v4 = "com.android.systemui"; // const-string v4, "com.android.systemui"
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1851 */
v3 = this.mSystemKeyPackages;
final String v4 = "com.android.phone"; // const-string v4, "com.android.phone"
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1852 */
v3 = this.mSystemKeyPackages;
final String v4 = "com.android.mms"; // const-string v4, "com.android.mms"
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1853 */
v3 = this.mSystemKeyPackages;
final String v4 = "com.android.contacts"; // const-string v4, "com.android.contacts"
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1854 */
v3 = this.mSystemKeyPackages;
final String v4 = "com.miui.home"; // const-string v4, "com.miui.home"
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1855 */
v3 = this.mSystemKeyPackages;
final String v4 = "com.jeejen.family.miui"; // const-string v4, "com.jeejen.family.miui"
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1856 */
v3 = this.mSystemKeyPackages;
final String v4 = "com.android.incallui"; // const-string v4, "com.android.incallui"
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1857 */
v3 = this.mSystemKeyPackages;
final String v4 = "com.miui.backup"; // const-string v4, "com.miui.backup"
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1858 */
v3 = this.mSystemKeyPackages;
final String v4 = "com.miui.securitycenter"; // const-string v4, "com.miui.securitycenter"
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1859 */
v3 = this.mSystemKeyPackages;
final String v4 = "com.xiaomi.mihomemanager"; // const-string v4, "com.xiaomi.mihomemanager"
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1860 */
v3 = this.mSystemKeyPackages;
final String v4 = "com.miui.securityadd"; // const-string v4, "com.miui.securityadd"
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 2787 */
/* new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$18; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$18;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mHomeDoubleTapTimeoutRunnable = v3;
/* .line 2797 */
/* new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$19; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$19;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mHomeDoubleClickTimeoutRunnable = v3;
/* .line 2878 */
/* iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenRecorderEnabled:Z */
/* .line 2884 */
/* new-instance v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$20; */
/* invoke-direct {v3, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$20;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mScreenRecordeEnablekKeyEventReceiver = v3;
/* .line 2909 */
/* iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z */
/* .line 2910 */
/* iput-wide v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeStartTime:J */
/* .line 2911 */
/* const/16 v1, 0x19 */
/* iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeLastKeyCode:I */
/* .line 2912 */
/* const/16 v1, 0x7d0 */
/* iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeTimeOut:I */
/* .line 2913 */
/* iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeVolumeDownTimes:I */
/* .line 2968 */
/* iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVoiceAssistEnabled:Z */
/* .line 3015 */
/* new-instance v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$21; */
/* invoke-direct {v0, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$21;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mInternalBroadcastReceiver = v0;
return;
} // .end method
private void cancelEventAndCallSuperQueueing ( android.view.KeyEvent p0, Integer p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "policyFlags" # I */
/* .param p3, "isScreenOn" # Z */
/* .line 1119 */
v0 = (( android.view.KeyEvent ) p1 ).getFlags ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I
/* or-int/lit8 v0, v0, 0x20 */
android.view.KeyEvent .changeFlags ( p1,v0 );
/* .line 1120 */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).callSuperInterceptKeyBeforeQueueing ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->callSuperInterceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I
/* .line 1121 */
return;
} // .end method
private void cancelPendingAccessibilityShortcutAction ( ) {
/* .locals 3 */
/* .line 1607 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
final String v2 = "close_talkback"; // const-string v2, "close_talkback"
(( android.os.Handler ) v0 ).removeMessages ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V
/* .line 1608 */
return;
} // .end method
private Boolean closeApp ( ) {
/* .locals 14 */
/* .line 2015 */
final String v0 = "NameNotFoundException"; // const-string v0, "NameNotFoundException"
v1 = this.mFocusedWindow;
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 2016 */
/* .line 2018 */
} // :cond_0
/* new-array v3, v2, [Ljava/lang/Object; */
/* .line 2019 */
final String v4 = "getAttrs"; // const-string v4, "getAttrs"
com.android.server.input.ReflectionUtils .callPrivateMethod ( v1,v4,v3 );
/* check-cast v1, Landroid/view/WindowManager$LayoutParams; */
/* .line 2020 */
/* .local v1, "attrs":Landroid/view/WindowManager$LayoutParams; */
/* if-nez v1, :cond_1 */
/* .line 2021 */
/* .line 2024 */
} // :cond_1
/* iget v3, v1, Landroid/view/WindowManager$LayoutParams;->type:I */
/* .line 2025 */
/* .local v3, "type":I */
int v4 = 1; // const/4 v4, 0x1
/* if-lt v3, v4, :cond_2 */
/* const/16 v5, 0x63 */
/* if-le v3, v5, :cond_3 */
} // :cond_2
/* const/16 v5, 0x3e8 */
/* if-lt v3, v5, :cond_9 */
/* const/16 v5, 0x7cf */
/* if-le v3, v5, :cond_3 */
/* goto/16 :goto_3 */
/* .line 2031 */
} // :cond_3
int v5 = 0; // const/4 v5, 0x0
/* .line 2032 */
/* .local v5, "title":Ljava/lang/String; */
v6 = this.packageName;
/* .line 2033 */
/* .local v6, "packageName":Ljava/lang/String; */
v7 = this.mContext;
(( android.content.Context ) v7 ).getPackageManager ( ); // invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 2034 */
/* .local v7, "pm":Landroid/content/pm/PackageManager; */
int v8 = -2; // const/4 v8, -0x2
/* .line 2037 */
/* .local v8, "OwningUserId":I */
try { // :try_start_0
(( android.view.WindowManager$LayoutParams ) v1 ).getTitle ( ); // invoke-virtual {v1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;
/* .line 2038 */
/* .local v9, "className":Ljava/lang/String; */
/* const/16 v10, 0x2f */
v10 = (( java.lang.String ) v9 ).lastIndexOf ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/String;->lastIndexOf(I)I
/* .line 2039 */
/* .local v10, "index":I */
/* if-ltz v10, :cond_4 */
/* .line 2040 */
/* new-instance v11, Landroid/content/ComponentName; */
/* add-int/lit8 v12, v10, 0x1 */
/* .line 2042 */
v13 = (( java.lang.String ) v9 ).length ( ); // invoke-virtual {v9}, Ljava/lang/String;->length()I
(( java.lang.String ) v9 ).subSequence ( v12, v13 ); // invoke-virtual {v9, v12, v13}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;
/* check-cast v12, Ljava/lang/String; */
/* invoke-direct {v11, v6, v12}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 2045 */
/* .local v11, "componentName":Landroid/content/ComponentName; */
(( android.content.pm.PackageManager ) v7 ).getActivityInfo ( v11, v2 ); // invoke-virtual {v7, v11, v2}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
/* .line 2046 */
/* .local v12, "activityInfo":Landroid/content/pm/ActivityInfo; */
(( android.content.pm.ActivityInfo ) v12 ).loadLabel ( v7 ); // invoke-virtual {v12, v7}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v5, v13 */
/* .line 2050 */
} // .end local v9 # "className":Ljava/lang/String;
} // .end local v10 # "index":I
} // .end local v11 # "componentName":Landroid/content/ComponentName;
} // .end local v12 # "activityInfo":Landroid/content/pm/ActivityInfo;
} // :cond_4
/* .line 2048 */
/* :catch_0 */
/* move-exception v9 */
/* .line 2049 */
/* .local v9, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
com.android.server.policy.MiuiInputLog .error ( v0,v9 );
/* .line 2053 */
} // .end local v9 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_0
try { // :try_start_1
v9 = android.text.TextUtils .isEmpty ( v5 );
if ( v9 != null) { // if-eqz v9, :cond_5
/* .line 2055 */
(( android.content.pm.PackageManager ) v7 ).getApplicationInfo ( v6, v2 ); // invoke-virtual {v7, v6, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* .line 2056 */
/* .local v9, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
(( android.content.pm.ApplicationInfo ) v9 ).loadLabel ( v7 ); // invoke-virtual {v9, v7}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
/* :try_end_1 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 ..:try_end_1} :catch_1 */
/* move-object v5, v0 */
/* .line 2060 */
} // .end local v9 # "applicationInfo":Landroid/content/pm/ApplicationInfo;
} // :cond_5
/* .line 2058 */
/* :catch_1 */
/* move-exception v9 */
/* .line 2059 */
/* .local v9, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
com.android.server.policy.MiuiInputLog .error ( v0,v9 );
/* .line 2062 */
} // .end local v9 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_1
v0 = android.text.TextUtils .isEmpty ( v5 );
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 2063 */
/* move-object v5, v6 */
/* .line 2066 */
} // :cond_6
final String v0 = "com.miui.home"; // const-string v0, "com.miui.home"
v0 = (( java.lang.String ) v6 ).equals ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 2067 */
final String v0 = "The current window is the home,not need to close app"; // const-string v0, "The current window is the home,not need to close app"
com.android.server.policy.MiuiInputLog .major ( v0 );
/* .line 2068 */
/* .line 2071 */
} // :cond_7
v0 = this.mSystemKeyPackages;
v0 = (( java.util.HashSet ) v0 ).contains ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 2073 */
v0 = this.mContext;
/* filled-new-array {v5}, [Ljava/lang/Object; */
/* .line 2074 */
/* const v10, 0x110f01f8 */
(( android.content.Context ) v0 ).getString ( v10, v9 ); // invoke-virtual {v0, v10, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
/* .line 2073 */
/* invoke-direct {p0, v0, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->makeAllUserToastAndShow(Ljava/lang/String;I)Landroid/widget/Toast; */
/* .line 2076 */
final String v0 = "The current window is the system window,not need to close app"; // const-string v0, "The current window is the system window,not need to close app"
com.android.server.policy.MiuiInputLog .major ( v0 );
/* .line 2077 */
/* .line 2082 */
} // :cond_8
try { // :try_start_2
v0 = this.token;
int v9 = 0; // const/4 v9, 0x0
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).finishActivityInternal ( v0, v2, v9 ); // invoke-virtual {p0, v0, v2, v9}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->finishActivityInternal(Landroid/os/IBinder;ILandroid/content/Intent;)V
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_2 */
/* .line 2085 */
/* .line 2083 */
/* :catch_2 */
/* move-exception v0 */
/* .line 2084 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v9 = "RemoteException"; // const-string v9, "RemoteException"
com.android.server.policy.MiuiInputLog .error ( v9,v0 );
/* .line 2088 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_2
final String v0 = "key shortcut"; // const-string v0, "key shortcut"
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).forceStopPackage ( v6, v8, v0 ); // invoke-virtual {p0, v6, v8, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
/* .line 2089 */
final String v0 = "The \'close app\' interface was called successfully"; // const-string v0, "The \'close app\' interface was called successfully"
com.android.server.policy.MiuiInputLog .major ( v0 );
/* .line 2092 */
v0 = this.mContext;
/* filled-new-array {v5}, [Ljava/lang/Object; */
/* .line 2093 */
/* const v10, 0x110f01f7 */
(( android.content.Context ) v0 ).getString ( v10, v9 ); // invoke-virtual {v0, v10, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
/* .line 2092 */
/* invoke-direct {p0, v0, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->makeAllUserToastAndShow(Ljava/lang/String;I)Landroid/widget/Toast; */
/* .line 2095 */
/* .line 2028 */
} // .end local v5 # "title":Ljava/lang/String;
} // .end local v6 # "packageName":Ljava/lang/String;
} // .end local v7 # "pm":Landroid/content/pm/PackageManager;
} // .end local v8 # "OwningUserId":I
} // :cond_9
} // :goto_3
} // .end method
private void closeTalkBack ( ) {
/* .locals 5 */
/* .line 1974 */
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTalkBackIsOpened:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1975 */
v0 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
final String v3 = "miui_talkback"; // const-string v3, "miui_talkback"
final String v4 = "key_combination_volume_up_volume_down"; // const-string v4, "key_combination_volume_up_volume_down"
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( v3, v4, v1, v2 ); // invoke-virtual {v0, v3, v4, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 1979 */
} // :cond_0
return;
} // .end method
private android.media.AudioManager getAudioManager ( ) {
/* .locals 2 */
/* .line 2196 */
v0 = this.mAudioManager;
/* if-nez v0, :cond_0 */
/* .line 2197 */
v0 = this.mContext;
final String v1 = "audio"; // const-string v1, "audio"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
this.mAudioManager = v0;
/* .line 2199 */
} // :cond_0
v0 = this.mAudioManager;
} // .end method
private Integer getCodeByKeyPressed ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "keyPressed" # I */
/* .line 173 */
/* sparse-switch p1, :sswitch_data_0 */
/* .line 189 */
int v0 = 0; // const/4 v0, 0x0
/* .line 187 */
/* :sswitch_0 */
/* const/16 v0, 0x18 */
/* .line 185 */
/* :sswitch_1 */
/* const/16 v0, 0x19 */
/* .line 183 */
/* :sswitch_2 */
/* const/16 v0, 0x1a */
/* .line 181 */
/* :sswitch_3 */
int v0 = 4; // const/4 v0, 0x4
/* .line 179 */
/* :sswitch_4 */
int v0 = 3; // const/4 v0, 0x3
/* .line 177 */
/* :sswitch_5 */
/* const/16 v0, 0xbb */
/* .line 175 */
/* :sswitch_6 */
/* const/16 v0, 0x52 */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x2 -> :sswitch_6 */
/* 0x4 -> :sswitch_5 */
/* 0x8 -> :sswitch_4 */
/* 0x10 -> :sswitch_3 */
/* 0x20 -> :sswitch_2 */
/* 0x40 -> :sswitch_1 */
/* 0x80 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private Long getImperceptiblePowerKeyTimeOut ( ) {
/* .locals 3 */
/* .line 1567 */
v0 = this.mMiuiKeyShortcutRuleManager;
/* .line 1568 */
/* const/16 v1, 0x1a */
(( com.android.server.policy.MiuiKeyShortcutRuleManager ) v0 ).getSingleKeyRule ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getSingleKeyRule(I)Lcom/android/server/policy/MiuiSingleKeyRule;
/* .line 1569 */
(( com.android.server.policy.MiuiSingleKeyRule ) v0 ).getMiuiLongPressTimeoutMs ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getMiuiLongPressTimeoutMs()J
/* move-result-wide v0 */
/* .line 1567 */
final String v2 = "ro.miui.imp_power_time"; // const-string v2, "ro.miui.imp_power_time"
android.os.SystemProperties .getLong ( v2,v0,v1 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
private static Integer getKeyBitmask ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "keycode" # I */
/* .line 151 */
/* sparse-switch p0, :sswitch_data_0 */
/* .line 167 */
int v0 = 1; // const/4 v0, 0x1
/* .line 155 */
/* :sswitch_0 */
int v0 = 4; // const/4 v0, 0x4
/* .line 153 */
/* :sswitch_1 */
int v0 = 2; // const/4 v0, 0x2
/* .line 161 */
/* :sswitch_2 */
/* const/16 v0, 0x20 */
/* .line 163 */
/* :sswitch_3 */
/* const/16 v0, 0x40 */
/* .line 165 */
/* :sswitch_4 */
/* const/16 v0, 0x80 */
/* .line 159 */
/* :sswitch_5 */
/* const/16 v0, 0x10 */
/* .line 157 */
/* :sswitch_6 */
/* const/16 v0, 0x8 */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x3 -> :sswitch_6 */
/* 0x4 -> :sswitch_5 */
/* 0x18 -> :sswitch_4 */
/* 0x19 -> :sswitch_3 */
/* 0x1a -> :sswitch_2 */
/* 0x52 -> :sswitch_1 */
/* 0xbb -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private void handleAiKeyEvent ( android.view.KeyEvent p0, Boolean p1 ) {
/* .locals 10 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "down" # Z */
/* .line 1018 */
/* const-wide/16 v0, 0x12c */
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 1019 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* .line 1020 */
/* .local v2, "keyDownTime":J */
v4 = (( android.view.KeyEvent ) p1 ).getRepeatCount ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I
int v5 = 1; // const/4 v5, 0x1
int v6 = 0; // const/4 v6, 0x0
/* if-nez v4, :cond_0 */
/* .line 1021 */
/* iput-boolean v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyIsConsumed:Z */
/* .line 1022 */
/* iput-boolean v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressAiKeyIsConsumed:Z */
/* .line 1023 */
/* iget v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyCount:I */
/* add-int/2addr v4, v5 */
/* iput v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyCount:I */
/* .line 1024 */
v4 = this.mHandler;
v7 = this.mLongPressDownAiKeyRunnable;
/* const-wide/16 v8, 0x1f4 */
(( android.os.Handler ) v4 ).postDelayed ( v7, v8, v9 ); // invoke-virtual {v4, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 1026 */
} // :cond_0
/* iget-wide v7, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLastClickAiKeyTime:J */
/* sub-long v7, v2, v7 */
/* cmp-long v0, v7, v0 */
/* if-gez v0, :cond_1 */
/* iget v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyCount:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_1 */
/* .line 1028 */
v0 = this.mHandler;
v1 = this.mDoubleClickAiKeyRunnable;
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1029 */
/* iput-boolean v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyIsConsumed:Z */
/* .line 1030 */
/* iput v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyCount:I */
/* .line 1031 */
v0 = this.mHandler;
v1 = this.mSingleClickAiKeyRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 1032 */
v0 = this.mHandler;
v1 = this.mLongPressDownAiKeyRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 1034 */
} // :cond_1
/* iput-wide v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLastClickAiKeyTime:J */
/* .line 1035 */
} // .end local v2 # "keyDownTime":J
/* .line 1036 */
} // :cond_2
/* iget-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressAiKeyIsConsumed:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1037 */
v0 = this.mHandler;
v1 = this.mLongPressUpAiKeyRunnable;
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1038 */
} // :cond_3
/* iget-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDoubleClickAiKeyIsConsumed:Z */
/* if-nez v2, :cond_4 */
/* .line 1039 */
v2 = this.mHandler;
v3 = this.mSingleClickAiKeyRunnable;
(( android.os.Handler ) v2 ).postDelayed ( v3, v0, v1 ); // invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 1040 */
v0 = this.mHandler;
v1 = this.mLongPressDownAiKeyRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 1043 */
} // :cond_4
} // :goto_0
return;
} // .end method
private void handleDoubleTapOnHome ( ) {
/* .locals 2 */
/* .line 2807 */
v0 = this.mMiuiShortcutTriggerHelper;
/* iget v0, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDoubleTapOnHomeBehavior:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 2809 */
/* iput-boolean v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeConsumed:Z */
/* .line 2810 */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).launchRecentPanel ( ); // invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchRecentPanel()Z
/* .line 2812 */
} // :cond_0
return;
} // .end method
private void handleMetaKey ( android.view.KeyEvent p0 ) {
/* .locals 6 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 1168 */
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* if-nez v0, :cond_0 */
/* move v0, v2 */
} // :cond_0
/* move v0, v1 */
/* .line 1169 */
/* .local v0, "down":Z */
} // :goto_0
v3 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 1171 */
/* .local v3, "keyCode":I */
v4 = android.view.KeyEvent .isMetaKey ( v3 );
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 1172 */
v4 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).isUserSetupComplete ( ); // invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isUserSetupComplete()Z
if ( v4 != null) { // if-eqz v4, :cond_5
com.miui.server.input.PadManager .getInstance ( );
v4 = (( com.miui.server.input.PadManager ) v4 ).isPad ( ); // invoke-virtual {v4}, Lcom/miui/server/input/PadManager;->isPad()Z
/* if-nez v4, :cond_1 */
/* .line 1175 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1176 */
v4 = this.mHandler;
v5 = this.mMetaKeyRunnable;
(( android.os.Handler ) v4 ).removeCallbacks ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 1177 */
v4 = (( android.view.KeyEvent ) p1 ).getDeviceId ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDeviceId()I
/* iput v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyBoardDeviceId:I */
/* .line 1178 */
/* iget-boolean v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 1179 */
/* iput-boolean v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z */
/* .line 1180 */
int v1 = 2; // const/4 v1, 0x2
/* iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyAction:I */
/* .line 1181 */
v1 = this.mHandler;
v2 = this.mMetaKeyRunnable;
/* const-wide/16 v4, 0x12c */
(( android.os.Handler ) v1 ).postDelayed ( v2, v4, v5 ); // invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 1183 */
} // :cond_2
/* iput v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyAction:I */
/* .line 1184 */
v1 = this.mHandler;
v2 = this.mMetaKeyRunnable;
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1187 */
} // :cond_3
/* iget-boolean v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z */
/* if-nez v4, :cond_4 */
/* iget v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyAction:I */
/* if-eq v4, v2, :cond_4 */
/* .line 1188 */
/* iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyAction:I */
/* .line 1189 */
} // :cond_4
/* iget-boolean v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemShortcutsMenuShown:Z */
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 1190 */
/* iget v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyBoardDeviceId:I */
/* invoke-direct {p0, v4, v2, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showKeyboardShortcutsMenu(IZZ)V */
/* .line 1173 */
} // :cond_5
} // :goto_1
return;
/* .line 1194 */
} // :cond_6
/* iget-boolean v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z */
/* if-nez v4, :cond_7 */
/* .line 1195 */
/* iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMetaKeyConsume:Z */
/* .line 1196 */
} // :cond_7
/* iget-boolean v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemShortcutsMenuShown:Z */
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 1197 */
/* iget v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyBoardDeviceId:I */
/* invoke-direct {p0, v4, v2, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showKeyboardShortcutsMenu(IZZ)V */
/* .line 1200 */
} // :cond_8
} // :goto_2
return;
} // .end method
private void handleTouchFeatureRotationWatcher ( ) {
/* .locals 3 */
/* .line 2257 */
v0 = this.mRotationWatcher;
/* if-nez v0, :cond_0 */
/* .line 2258 */
/* new-instance v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher; */
/* invoke-direct {v0, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$RotationWatcher;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
this.mRotationWatcher = v0;
/* .line 2260 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsSupportGloablTounchDirection:Z */
/* if-nez v0, :cond_2 */
v0 = /* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isGameMode()Z */
/* if-nez v0, :cond_2 */
/* sget-boolean v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2274 */
} // :cond_1
try { // :try_start_0
v0 = this.mWindowManager;
v1 = this.mRotationWatcher;
/* .line 2275 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHasWatchedRotation:Z */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 2278 */
/* .line 2276 */
/* :catch_0 */
/* move-exception v0 */
/* .line 2277 */
/* .local v0, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 2262 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_2
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->setTouchFeatureRotation()V */
/* .line 2263 */
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHasWatchedRotation:Z */
/* if-nez v0, :cond_3 */
/* .line 2265 */
try { // :try_start_1
v0 = this.mWindowManager;
v1 = this.mRotationWatcher;
v2 = this.mContext;
/* .line 2266 */
(( android.content.Context ) v2 ).getDisplay ( ); // invoke-virtual {v2}, Landroid/content/Context;->getDisplay()Landroid/view/Display;
v2 = (( android.view.Display ) v2 ).getDisplayId ( ); // invoke-virtual {v2}, Landroid/view/Display;->getDisplayId()I
/* .line 2265 */
/* .line 2267 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHasWatchedRotation:Z */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 2268 */
/* :catch_1 */
/* move-exception v0 */
/* .line 2269 */
/* .restart local v0 # "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 2270 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_1
/* nop */
/* .line 2280 */
} // :cond_3
} // :goto_2
return;
} // .end method
private Boolean hasTalkbackService ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "accessibilityShortcut" # Ljava/lang/String; */
/* .line 2513 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2514 */
/* .line 2516 */
} // :cond_0
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager.TALK_BACK_SERVICE_LIST;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Landroid/content/ComponentName; */
/* .line 2517 */
/* .local v2, "componentName":Landroid/content/ComponentName; */
v3 = miui.provider.SettingsStringUtil$ComponentNameSet .contains ( p1,v2 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 2518 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2520 */
} // .end local v2 # "componentName":Landroid/content/ComponentName;
} // :cond_1
/* .line 2521 */
} // :cond_2
} // .end method
private Boolean inFingerprintEnrolling ( ) {
/* .locals 5 */
/* .line 1728 */
v0 = this.mContext;
final String v1 = "activity"; // const-string v1, "activity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
/* .line 1730 */
/* .local v0, "am":Landroid/app/ActivityManager; */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
try { // :try_start_0
(( android.app.ActivityManager ) v0 ).getRunningTasks ( v2 ); // invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;
/* check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo; */
v3 = this.topActivity;
(( android.content.ComponentName ) v3 ).getClassName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* .line 1731 */
/* .local v3, "topClassName":Ljava/lang/String; */
final String v4 = "com.android.settings.NewFingerprintInternalActivity"; // const-string v4, "com.android.settings.NewFingerprintInternalActivity"
v4 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1732 */
/* .line 1736 */
} // .end local v3 # "topClassName":Ljava/lang/String;
} // :cond_0
/* .line 1734 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1735 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "Exception"; // const-string v3, "Exception"
com.android.server.policy.MiuiInputLog .error ( v3,v2 );
/* .line 1737 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
private void injectEvent ( Integer p0 ) {
/* .locals 15 */
/* .param p1, "injectKeyCode" # I */
/* .line 2160 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v11 */
/* .line 2161 */
/* .local v11, "now":J */
/* new-instance v13, Landroid/view/KeyEvent; */
int v5 = 0; // const/4 v5, 0x0
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
int v9 = -1; // const/4 v9, -0x1
int v10 = 0; // const/4 v10, 0x0
/* move-object v0, v13 */
/* move-wide v1, v11 */
/* move-wide v3, v11 */
/* move/from16 v6, p1 */
/* invoke-direct/range {v0 ..v10}, Landroid/view/KeyEvent;-><init>(JJIIIIII)V */
/* .line 2163 */
/* .local v13, "homeDown":Landroid/view/KeyEvent; */
/* new-instance v14, Landroid/view/KeyEvent; */
int v5 = 1; // const/4 v5, 0x1
/* move-object v0, v14 */
/* invoke-direct/range {v0 ..v10}, Landroid/view/KeyEvent;-><init>(JJIIIIII)V */
/* .line 2165 */
/* .local v0, "homeUp":Landroid/view/KeyEvent; */
android.hardware.input.InputManager .getInstance ( );
int v2 = 0; // const/4 v2, 0x0
(( android.hardware.input.InputManager ) v1 ).injectInputEvent ( v13, v2 ); // invoke-virtual {v1, v13, v2}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z
/* .line 2167 */
android.hardware.input.InputManager .getInstance ( );
(( android.hardware.input.InputManager ) v1 ).injectInputEvent ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z
/* .line 2169 */
return;
} // .end method
private void injectEventFromShortcut ( Integer p0 ) {
/* .locals 16 */
/* .param p1, "injectKeyCode" # I */
/* .line 2171 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v11 */
/* .line 2172 */
/* .local v11, "now":J */
/* new-instance v13, Landroid/view/KeyEvent; */
int v5 = 0; // const/4 v5, 0x0
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
int v9 = -1; // const/4 v9, -0x1
int v10 = 0; // const/4 v10, 0x0
/* move-object v0, v13 */
/* move-wide v1, v11 */
/* move-wide v3, v11 */
/* move/from16 v6, p1 */
/* invoke-direct/range {v0 ..v10}, Landroid/view/KeyEvent;-><init>(JJIIIIII)V */
/* .line 2174 */
/* .local v13, "down":Landroid/view/KeyEvent; */
/* const/high16 v14, 0x1000000 */
(( android.view.KeyEvent ) v13 ).setFlags ( v14 ); // invoke-virtual {v13, v14}, Landroid/view/KeyEvent;->setFlags(I)V
/* .line 2175 */
/* new-instance v15, Landroid/view/KeyEvent; */
int v5 = 1; // const/4 v5, 0x1
/* move-object v0, v15 */
/* invoke-direct/range {v0 ..v10}, Landroid/view/KeyEvent;-><init>(JJIIIIII)V */
/* .line 2177 */
/* .local v0, "up":Landroid/view/KeyEvent; */
(( android.view.KeyEvent ) v0 ).setFlags ( v14 ); // invoke-virtual {v0, v14}, Landroid/view/KeyEvent;->setFlags(I)V
/* .line 2178 */
android.hardware.input.InputManager .getInstance ( );
int v2 = 0; // const/4 v2, 0x0
(( android.hardware.input.InputManager ) v1 ).injectInputEvent ( v13, v2 ); // invoke-virtual {v1, v13, v2}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z
/* .line 2180 */
android.hardware.input.InputManager .getInstance ( );
(( android.hardware.input.InputManager ) v1 ).injectInputEvent ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z
/* .line 2182 */
return;
} // .end method
private void interceptAccessibilityShortcutChord ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "keyguardActive" # Z */
/* .line 1592 */
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyPressed:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyPressed:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTalkBackIsOpened:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1593 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 1594 */
/* .local v0, "now":J */
/* iget-wide v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyTime:J */
/* const-wide/16 v4, 0x96 */
/* add-long/2addr v2, v4 */
/* cmp-long v2, v0, v2 */
/* if-gtz v2, :cond_1 */
/* iget-wide v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyTime:J */
/* add-long/2addr v2, v4 */
/* cmp-long v2, v0, v2 */
/* if-gtz v2, :cond_1 */
/* .line 1596 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyConsumed:Z */
/* .line 1597 */
/* iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyConsumed:Z */
/* .line 1598 */
/* iget-boolean v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mShortcutServiceIsTalkBack:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* iget-boolean v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAccessibilityShortcutOnLockScreen:Z */
/* if-nez v3, :cond_1 */
/* .line 1599 */
} // :cond_0
v3 = this.mHandler;
final String v4 = "close_talkback"; // const-string v4, "close_talkback"
(( android.os.Handler ) v3 ).obtainMessage ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* const-wide/16 v4, 0xbb8 */
(( android.os.Handler ) v3 ).sendMessageDelayed ( v2, v4, v5 ); // invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1604 */
} // .end local v0 # "now":J
} // :cond_1
return;
} // .end method
private Boolean isAudioActive ( ) {
/* .locals 6 */
/* .line 2842 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2843 */
/* .local v0, "active":Z */
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getAudioManager()Landroid/media/AudioManager; */
v1 = (( android.media.AudioManager ) v1 ).getMode ( ); // invoke-virtual {v1}, Landroid/media/AudioManager;->getMode()I
/* .line 2844 */
/* .local v1, "mode":I */
final String v2 = "isAudioActive():"; // const-string v2, "isAudioActive():"
/* if-lez v1, :cond_0 */
int v3 = 7; // const/4 v3, 0x7
/* if-ge v1, v3, :cond_0 */
/* .line 2845 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2846 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .major ( v2 );
/* .line 2847 */
/* .line 2849 */
} // :cond_0
v3 = android.media.AudioSystem .getNumStreamTypes ( );
/* .line 2850 */
/* .local v3, "size":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* if-ge v4, v3, :cond_3 */
/* .line 2851 */
int v5 = 1; // const/4 v5, 0x1
/* if-ne v5, v4, :cond_1 */
/* .line 2853 */
/* .line 2855 */
} // :cond_1
int v5 = 0; // const/4 v5, 0x0
v0 = android.media.AudioSystem .isStreamActive ( v4,v5 );
/* .line 2856 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 2857 */
/* .line 2850 */
} // :cond_2
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 2860 */
} // .end local v4 # "i":I
} // :cond_3
} // :goto_2
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 2861 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .major ( v2 );
/* .line 2863 */
} // :cond_4
} // .end method
private Boolean isGameMode ( ) {
/* .locals 3 */
/* .line 2237 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "gb_boosting"; // const-string v1, "gb_boosting"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
} // .end method
private Boolean isInCallScreenShowing ( ) {
/* .locals 5 */
/* .line 927 */
v0 = this.mContext;
/* .line 928 */
final String v1 = "activity"; // const-string v1, "activity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
/* .line 929 */
/* .local v0, "activityManager":Landroid/app/ActivityManager; */
int v1 = 1; // const/4 v1, 0x1
(( android.app.ActivityManager ) v0 ).getRunningTasks ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;
int v3 = 0; // const/4 v3, 0x0
/* check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo; */
v2 = this.topActivity;
/* .line 930 */
(( android.content.ComponentName ) v2 ).getClassName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* .line 931 */
/* .local v2, "runningActivity":Ljava/lang/String; */
final String v4 = "com.android.phone.MiuiInCallScreen"; // const-string v4, "com.android.phone.MiuiInCallScreen"
v4 = (( java.lang.String ) v4 ).equals ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_1 */
/* .line 932 */
final String v4 = "com.android.incallui.InCallActivity"; // const-string v4, "com.android.incallui.InCallActivity"
v4 = (( java.lang.String ) v4 ).equals ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
} // :cond_0
/* move v1, v3 */
} // :cond_1
} // :goto_0
/* nop */
/* .line 931 */
} // :goto_1
} // .end method
public static Boolean isLargeScreen ( android.content.Context p0 ) {
/* .locals 6 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 3029 */
(( android.content.Context ) p0 ).getResources ( ); // invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v0 ).getConfiguration ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
/* .line 3030 */
/* .local v0, "configuration":Landroid/content/res/Configuration; */
/* iget v1, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I */
/* .line 3031 */
/* .local v1, "smallestScreenWidthDp":I */
/* iget v2, v0, Landroid/content/res/Configuration;->densityDpi:I */
/* const/16 v3, 0x160 */
int v4 = 1; // const/4 v4, 0x1
int v5 = 0; // const/4 v5, 0x0
/* if-ne v2, v3, :cond_1 */
/* .line 3033 */
/* const/16 v2, 0x190 */
/* if-le v1, v2, :cond_0 */
} // :cond_0
/* move v4, v5 */
} // :goto_0
/* .line 3034 */
} // :cond_1
/* iget v2, v0, Landroid/content/res/Configuration;->densityDpi:I */
/* const/16 v3, 0x1cd */
/* if-ne v2, v3, :cond_3 */
/* .line 3036 */
/* const/16 v2, 0x131 */
/* if-le v1, v2, :cond_2 */
} // :cond_2
/* move v4, v5 */
} // :goto_1
/* .line 3038 */
} // :cond_3
/* const/16 v2, 0x140 */
/* if-le v1, v2, :cond_4 */
} // :cond_4
/* move v4, v5 */
} // :goto_2
} // .end method
private Boolean isLockDeviceWindow ( com.android.server.policy.WindowManagerPolicy$WindowState p0 ) {
/* .locals 3 */
/* .param p1, "win" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .line 3009 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 3010 */
} // :cond_0
final String v1 = "getAttrs"; // const-string v1, "getAttrs"
/* new-array v2, v0, [Ljava/lang/Object; */
com.android.server.input.ReflectionUtils .callPrivateMethod ( p1,v1,v2 );
/* check-cast v1, Landroid/view/WindowManager$LayoutParams; */
/* .line 3011 */
/* .local v1, "lp":Landroid/view/WindowManager$LayoutParams; */
/* if-nez v1, :cond_1 */
/* .line 3012 */
} // :cond_1
/* iget v2, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* and-int/lit16 v2, v2, 0x800 */
if ( v2 != null) { // if-eqz v2, :cond_2
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
} // .end method
private Boolean isMiPad ( ) {
/* .locals 1 */
/* .line 1203 */
com.miui.server.input.PadManager .getInstance ( );
v0 = (( com.miui.server.input.PadManager ) v0 ).isPad ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z
} // .end method
private Boolean isNfcEnable ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "ishomeclick" # Z */
/* .line 2815 */
int v0 = 1; // const/4 v0, 0x1
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_4
/* .line 2816 */
final String v2 = "sagit"; // const-string v2, "sagit"
v3 = miui.os.Build.DEVICE;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_3 */
final String v2 = "jason"; // const-string v2, "jason"
v3 = miui.os.Build.DEVICE;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 2819 */
} // :cond_0
/* iget-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveBankCard:Z */
/* if-nez v2, :cond_2 */
/* iget-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveTranksCard:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
} // :cond_1
/* move v0, v1 */
} // :cond_2
} // :goto_0
/* .line 2817 */
} // :cond_3
} // :goto_1
/* .line 2822 */
} // :cond_4
/* iget v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressVolumeDownBehavior:I */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_5 */
/* iget-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveTranksCard:Z */
if ( v2 != null) { // if-eqz v2, :cond_5
} // :cond_5
/* move v0, v1 */
} // :goto_2
} // .end method
private Boolean isTalkBackService ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "accessibilityShortcut" # Ljava/lang/String; */
/* .line 2504 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2505 */
/* .line 2507 */
} // :cond_0
android.content.ComponentName .unflattenFromString ( p1 );
/* .line 2508 */
/* .local v0, "componentName":Landroid/content/ComponentName; */
if ( v0 != null) { // if-eqz v0, :cond_1
v2 = v2 = com.android.server.policy.BaseMiuiPhoneWindowManager.TALK_BACK_SERVICE_LIST;
if ( v2 != null) { // if-eqz v2, :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
private Boolean isTrackInputEvenForScreenRecorder ( android.view.KeyEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 2893 */
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenRecorderEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager.sScreenRecorderKeyEventList;
v1 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
java.lang.Integer .valueOf ( v1 );
v0 = (( java.util.ArrayList ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2894 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2896 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isTrackInputEventForVoiceAssist ( android.view.KeyEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 2971 */
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVoiceAssistEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager.sVoiceAssistKeyEventList;
v1 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
java.lang.Integer .valueOf ( v1 );
v0 = (( java.util.ArrayList ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2972 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2974 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void lambda$finishLayoutLw$4 ( Integer p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "inputMethodHeight" # I */
/* .line 2756 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->setInputMethodModeToTouch(I)V */
return;
} // .end method
private void lambda$sendAsyncBroadcast$2 ( android.content.Intent p0 ) { //synthethic
/* .locals 4 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 2710 */
v0 = this.mContext;
v1 = android.os.UserHandle.CURRENT;
/* .line 2711 */
android.app.BroadcastOptions .makeBasic ( );
int v3 = 1; // const/4 v3, 0x1
(( android.app.BroadcastOptions ) v2 ).setInteractive ( v3 ); // invoke-virtual {v2, v3}, Landroid/app/BroadcastOptions;->setInteractive(Z)Landroid/app/BroadcastOptions;
(( android.app.BroadcastOptions ) v2 ).toBundle ( ); // invoke-virtual {v2}, Landroid/app/BroadcastOptions;->toBundle()Landroid/os/Bundle;
/* .line 2710 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.Context ) v0 ).sendBroadcastAsUser ( p1, v1, v3, v2 ); // invoke-virtual {v0, p1, v1, v3, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/os/Bundle;)V
return;
} // .end method
private void lambda$sendAsyncBroadcast$3 ( android.content.Intent p0 ) { //synthethic
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 2713 */
v0 = this.mContext;
v1 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v0 ).sendBroadcastAsUser ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
return;
} // .end method
private void lambda$setCurrentUserLw$1 ( ) { //synthethic
/* .locals 4 */
/* .line 2548 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = 0; // const/4 v1, 0x0
/* iget v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCurrentUserId:I */
final String v3 = "is_mi_input_event_time_line_enable"; // const-string v3, "is_mi_input_event_time_line_enable"
android.provider.Settings$System .putIntForUser ( v0,v3,v1,v2 );
/* .line 2550 */
return;
} // .end method
private void lambda$takeScreenshot$0 ( java.lang.String p0, android.content.Intent p1 ) { //synthethic
/* .locals 4 */
/* .param p1, "function" # Ljava/lang/String; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 1840 */
v0 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
/* .line 1841 */
/* const-string/jumbo v1, "shortcut" */
(( android.content.Intent ) p2 ).getStringExtra ( v1 ); // invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
/* .line 1840 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( p1, v1, v2, v3 ); // invoke-virtual {v0, p1, v1, v2, v3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 1843 */
return;
} // .end method
private void lambda$trackDumpLogKeyCode$5 ( ) { //synthethic
/* .locals 5 */
/* .line 2953 */
v0 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
int v1 = 0; // const/4 v1, 0x0
int v2 = 0; // const/4 v2, 0x0
final String v3 = "dump_log_or_secret_code"; // const-string v3, "dump_log_or_secret_code"
/* const-string/jumbo v4, "volume_down_up_three_time" */
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( v3, v4, v1, v2 ); // invoke-virtual {v0, v3, v4, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 2956 */
return;
} // .end method
private android.widget.Toast makeAllUserToastAndShow ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "text" # Ljava/lang/String; */
/* .param p2, "duration" # I */
/* .line 956 */
v0 = this.mContext;
android.widget.Toast .makeText ( v0,p1,p2 );
/* .line 957 */
/* .local v0, "toast":Landroid/widget/Toast; */
(( android.widget.Toast ) v0 ).show ( ); // invoke-virtual {v0}, Landroid/widget/Toast;->show()V
/* .line 958 */
} // .end method
private void markShortcutTriggered ( ) {
/* .locals 2 */
/* .line 936 */
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager.phoneWindowManagerFeature;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.policy.PhoneWindowManagerFeatureImpl ) v0 ).callInterceptPowerKeyUp ( p0, v1 ); // invoke-virtual {v0, p0, v1}, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;->callInterceptPowerKeyUp(Lcom/android/server/policy/PhoneWindowManager;Z)V
/* .line 937 */
return;
} // .end method
private void notifyKidSpaceChanged ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isInKidMode" # Z */
/* .line 2283 */
/* iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsKidMode:Z */
/* .line 2284 */
v0 = this.mMiuiKeyInterceptExtend;
(( com.android.server.policy.MiuiKeyInterceptExtend ) v0 ).setKidSpaceMode ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setKidSpaceMode(Z)V
/* .line 2285 */
v0 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).notifyKidSpaceChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->notifyKidSpaceChanged(Z)V
/* .line 2286 */
return;
} // .end method
private void notifyPowerKeeperKeyEvent ( android.view.KeyEvent p0 ) {
/* .locals 0 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 1128 */
return;
} // .end method
private void playSoundEffect ( Integer p0, Integer p1, Boolean p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "policyFlags" # I */
/* .param p2, "keyCode" # I */
/* .param p3, "down" # Z */
/* .param p4, "repeatCount" # I */
/* .line 2203 */
if ( p3 != null) { // if-eqz p3, :cond_0
/* and-int/lit8 v0, p1, 0x2 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* if-nez p4, :cond_0 */
v0 = this.mVibrator;
/* .line 2204 */
v0 = (( android.os.Vibrator ) v0 ).hasVibrator ( ); // invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z
/* if-nez v0, :cond_0 */
v0 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).hasNavigationBar ( ); // invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->hasNavigationBar()Z
/* if-nez v0, :cond_0 */
/* .line 2205 */
/* sparse-switch p2, :sswitch_data_0 */
/* .line 2211 */
/* :sswitch_0 */
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->playSoundEffect()Z */
/* .line 2216 */
} // :cond_0
} // :goto_0
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x3 -> :sswitch_0 */
/* 0x4 -> :sswitch_0 */
/* 0x52 -> :sswitch_0 */
/* 0x54 -> :sswitch_0 */
/* 0xbb -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private Boolean playSoundEffect ( ) {
/* .locals 2 */
/* .line 2219 */
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getAudioManager()Landroid/media/AudioManager; */
/* .line 2220 */
/* .local v0, "audioManager":Landroid/media/AudioManager; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 2221 */
/* .line 2223 */
} // :cond_0
(( android.media.AudioManager ) v0 ).playSoundEffect ( v1 ); // invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V
/* .line 2224 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
private void postKeyFunction ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
/* .locals 5 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "delay" # I */
/* .param p3, "shortcut" # Ljava/lang/String; */
/* .line 1573 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1574 */
final String v0 = "action is null"; // const-string v0, "action is null"
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 1575 */
return;
/* .line 1577 */
} // :cond_0
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1578 */
/* .local v0, "message":Landroid/os/Message; */
final String v2 = "dumpsys_by_power"; // const-string v2, "dumpsys_by_power"
/* if-ne p1, v2, :cond_1 */
/* .line 1579 */
(( android.os.Message ) v0 ).setAsynchronous ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Message;->setAsynchronous(Z)V
/* .line 1581 */
} // :cond_1
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 1582 */
/* .local v1, "bundle":Landroid/os/Bundle; */
/* const-string/jumbo v2, "shortcut" */
(( android.os.Bundle ) v1 ).putString ( v2, p3 ); // invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1583 */
(( android.os.Message ) v0 ).setData ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 1584 */
v2 = this.mHandler;
/* int-to-long v3, p2 */
(( android.os.Handler ) v2 ).sendMessageDelayed ( v0, v3, v4 ); // invoke-virtual {v2, v0, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1585 */
return;
} // .end method
private void postPowerLongPress ( ) {
/* .locals 4 */
/* .line 1545 */
v0 = this.mMiuiKeyShortcutRuleManager;
final String v1 = "long_press_power_key"; // const-string v1, "long_press_power_key"
(( com.android.server.policy.MiuiKeyShortcutRuleManager ) v0 ).getFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 1547 */
/* .local v0, "longPressPowerFunction":Ljava/lang/String; */
/* const/16 v1, 0xfa0 */
final String v2 = "long_press_power_key_four_second"; // const-string v2, "long_press_power_key_four_second"
final String v3 = "dumpsys_by_power"; // const-string v3, "dumpsys_by_power"
/* invoke-direct {p0, v3, v1, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->postKeyFunction(Ljava/lang/String;ILjava/lang/String;)V */
/* .line 1549 */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
final String v1 = "none"; // const-string v1, "none"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1551 */
} // :cond_0
v1 = this.mImperceptiblePowerKey;
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getImperceptiblePowerKeyTimeOut()J */
/* move-result-wide v2 */
/* long-to-int v2, v2 */
final String v3 = "imperceptible_press_power_key"; // const-string v3, "imperceptible_press_power_key"
/* invoke-direct {p0, v1, v2, v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->postKeyFunction(Ljava/lang/String;ILjava/lang/String;)V */
/* .line 1554 */
} // :cond_1
return;
} // .end method
private void postVolumeUpLongPress ( ) {
/* .locals 3 */
/* .line 1563 */
/* const/16 v0, 0x1388 */
final String v1 = ""; // const-string v1, ""
final String v2 = "key_long_press_volume_up"; // const-string v2, "key_long_press_volume_up"
/* invoke-direct {p0, v2, v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->postKeyFunction(Ljava/lang/String;ILjava/lang/String;)V */
/* .line 1564 */
return;
} // .end method
private void preloadRecentApps ( ) {
/* .locals 0 */
/* .line 2137 */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).preloadRecentAppsInternal ( ); // invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->preloadRecentAppsInternal()V
/* .line 2138 */
return;
} // .end method
private void releaseScreenOnProximitySensor ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isNowRelease" # Z */
/* .line 718 */
v0 = this.mProximitySensor;
if ( v0 != null) { // if-eqz v0, :cond_0
(( com.android.server.policy.MiuiScreenOnProximityLock ) v0 ).release ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->release(Z)Z
/* .line 719 */
} // :cond_0
return;
} // .end method
private void removeKeyFunction ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 1588 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).removeMessages ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V
/* .line 1589 */
return;
} // .end method
private void removePowerLongPress ( ) {
/* .locals 1 */
/* .line 1540 */
final String v0 = "dumpsys_by_power"; // const-string v0, "dumpsys_by_power"
/* invoke-direct {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->removeKeyFunction(Ljava/lang/String;)V */
/* .line 1541 */
v0 = this.mImperceptiblePowerKey;
/* invoke-direct {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->removeKeyFunction(Ljava/lang/String;)V */
/* .line 1542 */
return;
} // .end method
private void removeVolumeUpLongPress ( ) {
/* .locals 2 */
/* .line 1557 */
/* const-string/jumbo v0, "zhuque" */
v1 = miui.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1558 */
final String v0 = "key_long_press_volume_up"; // const-string v0, "key_long_press_volume_up"
/* invoke-direct {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->removeKeyFunction(Ljava/lang/String;)V */
/* .line 1560 */
} // :cond_0
return;
} // .end method
private void saveWindowTypeLayer ( android.content.Context p0 ) {
/* .locals 9 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 428 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 429 */
/* .local v0, "typeLayers":Lorg/json/JSONObject; */
/* const/16 v1, 0x7d1 */
/* const/16 v2, 0x7dd */
/* const/16 v3, 0x7d0 */
/* filled-new-array {v3, v1, v2}, [I */
/* .line 432 */
/* .local v1, "types":[I */
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_1 */
/* aget v4, v1, v3 */
/* .line 433 */
/* .local v4, "type":I */
v5 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).getWindowLayerFromTypeLw ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getWindowLayerFromTypeLw(I)I
/* .line 434 */
/* .local v5, "layer":I */
int v6 = 2; // const/4 v6, 0x2
/* if-eq v5, v6, :cond_0 */
/* .line 436 */
try { // :try_start_0
java.lang.Integer .toString ( v4 );
(( org.json.JSONObject ) v0 ).put ( v6, v5 ); // invoke-virtual {v0, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 439 */
/* .line 437 */
/* :catch_0 */
/* move-exception v6 */
/* .line 438 */
/* .local v6, "ex":Lorg/json/JSONException; */
final String v7 = "WindowManager"; // const-string v7, "WindowManager"
final String v8 = "JSONException"; // const-string v8, "JSONException"
android.util.Slog .e ( v7,v8,v6 );
/* .line 432 */
} // .end local v4 # "type":I
} // .end local v5 # "layer":I
} // .end local v6 # "ex":Lorg/json/JSONException;
} // :cond_0
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 442 */
} // :cond_1
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v3, "window_type_layer" */
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
android.provider.MiuiSettings$System .putString ( v2,v3,v4 );
/* .line 443 */
return;
} // .end method
private void sendBackKeyEventBroadcast ( android.view.KeyEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 3003 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.KEYCODE_BACK"; // const-string v1, "miui.intent.KEYCODE_BACK"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 3004 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "android.intent.extra.KEY_EVENT"; // const-string v1, "android.intent.extra.KEY_EVENT"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
/* .line 3005 */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).sendAsyncBroadcast ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;)V
/* .line 3006 */
return;
} // .end method
private void sendFullScreenStateToHome ( ) {
/* .locals 3 */
/* .line 2996 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.miui.fullscreen_state_change"; // const-string v1, "com.miui.fullscreen_state_change"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 2997 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const-string/jumbo v1, "state" */
/* const-string/jumbo v2, "toHome" */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 2998 */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).sendAsyncBroadcastForAllUser ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcastForAllUser(Landroid/content/Intent;)V
/* .line 2999 */
return;
} // .end method
private void sendFullScreenStateToTaskSnapshot ( ) {
/* .locals 3 */
/* .line 2988 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.miui.fullscreen_state_change"; // const-string v1, "com.miui.fullscreen_state_change"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 2989 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const-string/jumbo v1, "state" */
/* const-string/jumbo v2, "taskSnapshot" */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 2990 */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).sendAsyncBroadcast ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;)V
/* .line 2991 */
return;
} // .end method
private void sendKeyEventBroadcast ( android.view.KeyEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 2900 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.SCREEN_RECORDER_TRACK_KEYEVENT"; // const-string v1, "miui.intent.SCREEN_RECORDER_TRACK_KEYEVENT"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 2901 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.screenrecorder"; // const-string v1, "com.miui.screenrecorder"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 2902 */
final String v1 = "keycode"; // const-string v1, "keycode"
v2 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 2903 */
v1 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
final String v2 = "isdown"; // const-string v2, "isdown"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 2904 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 2905 */
return;
} // .end method
private Boolean sendOthersBroadcast ( Boolean p0, Boolean p1, Boolean p2, Integer p3, android.view.KeyEvent p4 ) {
/* .locals 16 */
/* .param p1, "down" # Z */
/* .param p2, "isScreenOn" # Z */
/* .param p3, "keyguardActive" # Z */
/* .param p4, "keyCode" # I */
/* .param p5, "event" # Landroid/view/KeyEvent; */
/* .line 1661 */
/* move-object/from16 v0, p0 */
/* move/from16 v1, p4 */
/* move-object/from16 v2, p5 */
int v3 = 0; // const/4 v3, 0x0
/* const/16 v4, 0x1a */
if ( p1 != null) { // if-eqz p1, :cond_9
/* .line 1665 */
/* const/16 v5, 0xa4 */
/* const/high16 v6, 0x40000000 # 2.0f */
final String v7 = "android.intent.extra.KEY_EVENT"; // const-string v7, "android.intent.extra.KEY_EVENT"
/* const/16 v8, 0x18 */
/* const/16 v9, 0x19 */
int v10 = 1; // const/4 v10, 0x1
if ( p2 != null) { // if-eqz p2, :cond_1
/* if-nez p3, :cond_1 */
/* if-eq v1, v4, :cond_0 */
/* if-eq v1, v9, :cond_0 */
/* if-eq v1, v8, :cond_0 */
/* if-eq v1, v5, :cond_0 */
/* const/16 v11, 0x55 */
/* if-eq v1, v11, :cond_0 */
/* const/16 v11, 0x4f */
/* if-ne v1, v11, :cond_1 */
/* .line 1671 */
} // :cond_0
/* new-instance v11, Landroid/content/Intent; */
final String v12 = "miui.intent.action.KEYCODE_EXTERNAL"; // const-string v12, "miui.intent.action.KEYCODE_EXTERNAL"
/* invoke-direct {v11, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1672 */
/* .local v11, "i":Landroid/content/Intent; */
(( android.content.Intent ) v11 ).putExtra ( v7, v2 ); // invoke-virtual {v11, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
/* .line 1673 */
(( android.content.Intent ) v11 ).addFlags ( v6 ); // invoke-virtual {v11, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1674 */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) v0 ).sendAsyncBroadcast ( v11, v10 ); // invoke-virtual {v0, v11, v10}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;Z)V
/* .line 1676 */
} // .end local v11 # "i":Landroid/content/Intent;
} // :cond_1
/* if-ne v1, v4, :cond_2 */
/* move v4, v10 */
} // :cond_2
/* move v4, v3 */
/* .line 1677 */
/* .local v4, "stopNotification":Z */
} // :goto_0
/* if-nez v4, :cond_4 */
if ( p3 != null) { // if-eqz p3, :cond_4
/* .line 1678 */
/* if-eq v1, v9, :cond_3 */
/* if-eq v1, v8, :cond_3 */
/* if-ne v1, v5, :cond_4 */
/* .line 1681 */
} // :cond_3
int v4 = 1; // const/4 v4, 0x1
/* .line 1685 */
} // :cond_4
if ( v4 != null) { // if-eqz v4, :cond_5
/* iget-boolean v5, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemReady:Z */
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 1686 */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService; */
/* .line 1687 */
/* .local v5, "statusBarService":Lcom/android/internal/statusbar/IStatusBarService; */
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 1688 */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) v0 ).onStatusBarPanelRevealed ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->onStatusBarPanelRevealed(Lcom/android/internal/statusbar/IStatusBarService;)V
/* .line 1692 */
} // .end local v5 # "statusBarService":Lcom/android/internal/statusbar/IStatusBarService;
} // :cond_5
/* if-eq v1, v9, :cond_6 */
/* if-ne v1, v8, :cond_8 */
/* .line 1694 */
} // :cond_6
v5 = this.mContext;
(( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1695 */
/* .local v5, "cr":Landroid/content/ContentResolver; */
final String v8 = "remote_control_proc_name"; // const-string v8, "remote_control_proc_name"
android.provider.Settings$System .getString ( v5,v8 );
/* .line 1696 */
/* .local v9, "proc":Ljava/lang/String; */
final String v11 = "remote_control_pkg_name"; // const-string v11, "remote_control_pkg_name"
android.provider.Settings$System .getString ( v5,v11 );
/* .line 1698 */
/* .local v12, "pkg":Ljava/lang/String; */
if ( v9 != null) { // if-eqz v9, :cond_8
if ( v12 != null) { // if-eqz v12, :cond_8
/* .line 1699 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v13 */
/* .line 1700 */
/* .local v13, "c":J */
v15 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v0 ).checkProcessRunning ( v9 ); // invoke-virtual {v0, v9}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->checkProcessRunning(Ljava/lang/String;)Z
/* .line 1704 */
/* .local v15, "running":Z */
if ( v15 != null) { // if-eqz v15, :cond_7
/* .line 1705 */
/* new-instance v3, Landroid/content/Intent; */
final String v8 = "miui.intent.action.REMOTE_CONTROL"; // const-string v8, "miui.intent.action.REMOTE_CONTROL"
/* invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1706 */
/* .local v3, "i":Landroid/content/Intent; */
(( android.content.Intent ) v3 ).setPackage ( v12 ); // invoke-virtual {v3, v12}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1707 */
(( android.content.Intent ) v3 ).addFlags ( v6 ); // invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1708 */
(( android.content.Intent ) v3 ).putExtra ( v7, v2 ); // invoke-virtual {v3, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
/* .line 1709 */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) v0 ).sendAsyncBroadcast ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;)V
/* .line 1710 */
/* .line 1712 */
} // .end local v3 # "i":Landroid/content/Intent;
} // :cond_7
int v6 = 0; // const/4 v6, 0x0
android.provider.Settings$System .putString ( v5,v8,v6 );
/* .line 1713 */
android.provider.Settings$System .putString ( v5,v11,v6 );
/* .line 1717 */
} // .end local v4 # "stopNotification":Z
} // .end local v5 # "cr":Landroid/content/ContentResolver;
} // .end local v9 # "proc":Ljava/lang/String;
} // .end local v12 # "pkg":Ljava/lang/String;
} // .end local v13 # "c":J
} // .end local v15 # "running":Z
} // :cond_8
/* .line 1718 */
} // :cond_9
/* if-ne v1, v4, :cond_a */
/* .line 1719 */
/* new-instance v4, Landroid/content/Intent; */
final String v5 = "android.intent.action.KEYCODE_POWER_UP"; // const-string v5, "android.intent.action.KEYCODE_POWER_UP"
/* invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) v0 ).sendAsyncBroadcast ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;)V
/* .line 1722 */
} // :cond_a
} // :goto_1
} // .end method
public static void sendRecordCountEvent ( android.content.Context p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 9 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "category" # Ljava/lang/String; */
/* .param p2, "event" # Ljava/lang/String; */
/* .line 940 */
final String v0 = "count_event"; // const-string v0, "count_event"
/* .line 941 */
/* .local v0, "STAT_TYPE_COUNT_EVENT":Ljava/lang/String; */
final String v1 = "com.miui.gallery"; // const-string v1, "com.miui.gallery"
/* .line 942 */
/* .local v1, "GALLERY_PKG_NAME":Ljava/lang/String; */
final String v2 = "com.miui.gallery.intent.action.SEND_STAT"; // const-string v2, "com.miui.gallery.intent.action.SEND_STAT"
/* .line 943 */
/* .local v2, "ACTION_SEND_STAT":Ljava/lang/String; */
/* const-string/jumbo v3, "stat_type" */
/* .line 944 */
/* .local v3, "EXTRA_STAT_TYPE":Ljava/lang/String; */
final String v4 = "category"; // const-string v4, "category"
/* .line 945 */
/* .local v4, "EXTRA_CATEGORY":Ljava/lang/String; */
final String v5 = "event"; // const-string v5, "event"
/* .line 947 */
/* .local v5, "EXTRA_EVENT":Ljava/lang/String; */
/* new-instance v6, Landroid/content/Intent; */
final String v7 = "com.miui.gallery.intent.action.SEND_STAT"; // const-string v7, "com.miui.gallery.intent.action.SEND_STAT"
/* invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 948 */
/* .local v6, "intent":Landroid/content/Intent; */
final String v7 = "com.miui.gallery"; // const-string v7, "com.miui.gallery"
(( android.content.Intent ) v6 ).setPackage ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 949 */
/* const-string/jumbo v7, "stat_type" */
final String v8 = "count_event"; // const-string v8, "count_event"
(( android.content.Intent ) v6 ).putExtra ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 950 */
final String v7 = "category"; // const-string v7, "category"
(( android.content.Intent ) v6 ).putExtra ( v7, p1 ); // invoke-virtual {v6, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 951 */
final String v7 = "event"; // const-string v7, "event"
(( android.content.Intent ) v6 ).putExtra ( v7, p2 ); // invoke-virtual {v6, v7, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 952 */
(( android.content.Context ) p0 ).sendBroadcast ( v6 ); // invoke-virtual {p0, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 953 */
return;
} // .end method
private void sendVoiceAssistKeyEventBroadcast ( android.view.KeyEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 2978 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.VOICE_ASSIST_TRACK_KEYEVENT"; // const-string v1, "miui.intent.VOICE_ASSIST_TRACK_KEYEVENT"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 2979 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.voiceassist"; // const-string v1, "com.miui.voiceassist"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 2980 */
final String v1 = "keycode"; // const-string v1, "keycode"
v2 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 2981 */
v1 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
final String v2 = "isdown"; // const-string v2, "isdown"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 2982 */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).sendAsyncBroadcast ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;)V
/* .line 2983 */
return;
} // .end method
private void setInputMethodModeToTouch ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "inputMethodHeight" # I */
/* .line 2774 */
/* const/16 v0, 0x19 */
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 2775 */
miui.util.ITouchFeature .getInstance ( );
int v3 = 1; // const/4 v3, 0x1
(( miui.util.ITouchFeature ) v2 ).setTouchMode ( v1, v0, v3 ); // invoke-virtual {v2, v1, v0, v3}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z
/* .line 2778 */
} // :cond_0
miui.util.ITouchFeature .getInstance ( );
(( miui.util.ITouchFeature ) v2 ).setTouchMode ( v1, v0, v1 ); // invoke-virtual {v2, v1, v0, v1}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z
/* .line 2781 */
} // :goto_0
return;
} // .end method
private void setScreenRecorderEnabled ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 2881 */
/* iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenRecorderEnabled:Z */
/* .line 2882 */
return;
} // .end method
private void setStatusBarInFullscreen ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "show" # Z */
/* .line 1754 */
/* iput-boolean p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsStatusBarVisibleInFullscreen:Z */
/* .line 1756 */
try { // :try_start_0
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).getStatusBarService ( ); // invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;
/* .line 1757 */
/* .local v0, "statusbar":Lcom/android/internal/statusbar/IStatusBarService; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1758 */
/* nop */
/* .line 1759 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* const/high16 v1, -0x80000000 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
v2 = this.mBinder;
final String v3 = "android"; // const-string v3, "android"
/* .line 1758 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1767 */
} // .end local v0 # "statusbar":Lcom/android/internal/statusbar/IStatusBarService;
} // :cond_1
/* .line 1763 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1765 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "WindowManager"; // const-string v1, "WindowManager"
final String v2 = "RemoteException"; // const-string v2, "RemoteException"
android.util.Slog .e ( v1,v2,v0 );
/* .line 1766 */
int v1 = 0; // const/4 v1, 0x0
this.mStatusBarService = v1;
/* .line 1775 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_1
return;
} // .end method
private void setTouchFeatureRotation ( ) {
/* .locals 4 */
/* .line 2241 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getDisplay ( ); // invoke-virtual {v0}, Landroid/content/Context;->getDisplay()Landroid/view/Display;
v0 = (( android.view.Display ) v0 ).getRotation ( ); // invoke-virtual {v0}, Landroid/view/Display;->getRotation()I
/* .line 2242 */
/* .local v0, "rotation":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "set rotation = " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "WindowManager"; // const-string v2, "WindowManager"
android.util.Slog .d ( v2,v1 );
/* .line 2243 */
int v1 = 0; // const/4 v1, 0x0
/* .line 2244 */
/* .local v1, "targetId":I */
/* sget-boolean v2, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* iget-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFolded:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 2245 */
int v1 = 1; // const/4 v1, 0x1
/* .line 2247 */
} // :cond_0
miui.util.ITouchFeature .getInstance ( );
/* const/16 v3, 0x8 */
(( miui.util.ITouchFeature ) v2 ).setTouchMode ( v1, v3, v0 ); // invoke-virtual {v2, v1, v3, v0}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z
/* .line 2249 */
return;
} // .end method
private Boolean shouldInterceptHeadSetHookKey ( Integer p0, android.view.KeyEvent p1 ) {
/* .locals 4 */
/* .param p1, "keyCode" # I */
/* .param p2, "event" # Landroid/view/KeyEvent; */
/* .line 1649 */
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMikeymodeEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x4f */
/* if-ne p1, v0, :cond_0 */
/* .line 1650 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.MIKEY_BUTTON"; // const-string v1, "miui.intent.action.MIKEY_BUTTON"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1651 */
/* .local v0, "mikeyIntent":Landroid/content/Intent; */
final String v1 = "com.xiaomi.miclick"; // const-string v1, "com.xiaomi.miclick"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1652 */
final String v1 = "key_action"; // const-string v1, "key_action"
v2 = (( android.view.KeyEvent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1653 */
final String v1 = "key_event_time"; // const-string v1, "key_event_time"
(( android.view.KeyEvent ) p2 ).getEventTime ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J
/* move-result-wide v2 */
(( android.content.Intent ) v0 ).putExtra ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 1654 */
v1 = this.mContext;
(( android.content.Context ) v1 ).sendBroadcast ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 1655 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1657 */
} // .end local v0 # "mikeyIntent":Landroid/content/Intent;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean shouldInterceptKey ( android.view.KeyEvent p0, Integer p1, Boolean p2 ) {
/* .locals 6 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "policyFlags" # I */
/* .param p3, "isScreenOn" # Z */
/* .line 1089 */
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 1090 */
/* .local v0, "keyCode":I */
/* iget-boolean v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsVRMode:Z */
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1091 */
final String v1 = "VR mode drop all keys."; // const-string v1, "VR mode drop all keys."
com.android.server.policy.MiuiInputLog .major ( v1 );
/* .line 1092 */
/* .line 1094 */
} // :cond_0
/* const-string/jumbo v1, "sys.in_shutdown_progress" */
int v3 = 0; // const/4 v3, 0x0
v1 = android.os.SystemProperties .getInt ( v1,v3 );
/* if-ne v1, v2, :cond_1 */
/* .line 1096 */
/* const-string/jumbo v1, "this device is being shut down, ignore key event." */
com.android.server.policy.MiuiInputLog .major ( v1 );
/* .line 1097 */
/* .line 1099 */
} // :cond_1
/* if-nez p3, :cond_3 */
/* .line 1100 */
int v1 = 4; // const/4 v1, 0x4
/* if-eq v1, v0, :cond_2 */
/* const/16 v1, 0x52 */
/* if-ne v1, v0, :cond_3 */
/* .line 1101 */
} // :cond_2
final String v1 = "Cancel back or menu key when screen is off"; // const-string v1, "Cancel back or menu key when screen is off"
com.android.server.policy.MiuiInputLog .major ( v1 );
/* .line 1102 */
/* invoke-direct {p0, p1, p2, v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->cancelEventAndCallSuperQueueing(Landroid/view/KeyEvent;IZ)V */
/* .line 1103 */
/* .line 1107 */
} // :cond_3
(( android.view.KeyEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
/* .line 1108 */
/* .local v1, "inputDevice":Landroid/view/InputDevice; */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1109 */
v4 = (( android.view.InputDevice ) v1 ).getProductId ( ); // invoke-virtual {v1}, Landroid/view/InputDevice;->getProductId()I
/* const/16 v5, 0x628 */
/* if-ne v4, v5, :cond_5 */
/* .line 1110 */
v3 = this.mShoulderKeyManagerInternal;
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1111 */
/* .line 1113 */
} // :cond_4
/* .line 1115 */
} // :cond_5
} // .end method
private void showKeyboardShortcutsMenu ( Integer p0, Boolean p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "deviceId" # I */
/* .param p2, "system" # Z */
/* .param p3, "show" # Z */
/* .line 1216 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "toggleKeyboardShortcutsMenu ,deviceId: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " system:"; // const-string v1, " system:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " show:"; // const-string v1, " show:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .major ( v0 );
/* .line 1219 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 1220 */
/* iput-boolean p3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemShortcutsMenuShown:Z */
/* .line 1222 */
} // :cond_0
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.miui.systemui.action.KEYBOARD_SHORTCUTS"; // const-string v1, "com.miui.systemui.action.KEYBOARD_SHORTCUTS"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1223 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.android.systemui"; // const-string v1, "com.android.systemui"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1224 */
/* const-string/jumbo v1, "system" */
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1225 */
/* const-string/jumbo v1, "show" */
(( android.content.Intent ) v0 ).putExtra ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1226 */
final String v1 = "deviceId"; // const-string v1, "deviceId"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1227 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 1228 */
return;
} // .end method
private Boolean showMenu ( ) {
/* .locals 3 */
/* .line 2152 */
v0 = this.mHapticFeedbackUtil;
/* const-string/jumbo v1, "virtual_key_longpress" */
int v2 = 0; // const/4 v2, 0x0
(( miui.util.HapticFeedbackUtil ) v0 ).performHapticFeedback ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/util/HapticFeedbackUtil;->performHapticFeedback(Ljava/lang/String;Z)Z
/* .line 2153 */
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->markShortcutTriggered()V */
/* .line 2154 */
/* const/16 v0, 0x52 */
/* invoke-direct {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->injectEventFromShortcut(I)V */
/* .line 2155 */
/* const-string/jumbo v0, "show menu,the inject event is KEYCODE_MENU " */
com.android.server.policy.MiuiInputLog .major ( v0 );
/* .line 2156 */
} // .end method
private void showRecentApps ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "triggeredFromAltTab" # Z */
/* .line 1207 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPreloadedRecentApps:Z */
/* .line 1208 */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).getStatusBarManagerInternal ( ); // invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;
/* .line 1209 */
/* .local v0, "statusbar":Lcom/android/server/statusbar/StatusBarManagerInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1210 */
/* .line 1212 */
} // :cond_0
return;
} // .end method
private Boolean skipPocketModeAquireByWakeUpInfo ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 9 */
/* .param p1, "detail" # Ljava/lang/String; */
/* .param p2, "wakeUpReason" # Ljava/lang/String; */
/* .line 569 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 570 */
/* .local v0, "currentTime":J */
/* iget-wide v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mUpdateWakeUpDetailTime:J */
/* cmp-long v2, v0, v2 */
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* if-lez v2, :cond_0 */
/* iget-wide v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mUpdateWakeUpDetailTime:J */
/* sub-long v5, v0, v5 */
/* const-wide/16 v7, 0x64 */
/* cmp-long v2, v5, v7 */
/* if-gez v2, :cond_0 */
/* move v2, v3 */
} // :cond_0
/* move v2, v4 */
/* .line 573 */
/* .local v2, "validData":Z */
} // :goto_0
final String v5 = "android.policy:FINGERPRINT"; // const-string v5, "android.policy:FINGERPRINT"
v5 = (( java.lang.String ) v5 ).equals ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 575 */
/* .line 576 */
} // :cond_1
/* const-string/jumbo v5, "server.display:unfold" */
v5 = (( java.lang.String ) v5 ).equals ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 578 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v5, 0xc */
android.os.PowerManager .wakeReasonToString ( v5 );
/* .line 579 */
v5 = (( java.lang.String ) v5 ).equals ( p2 ); // invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
} // :cond_2
/* move v3, v4 */
/* .line 578 */
} // :goto_1
/* .line 581 */
} // :cond_3
} // .end method
private void startAiKeyService ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "pressType" # Ljava/lang/String; */
/* .line 971 */
try { // :try_start_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 972 */
/* .local v0, "intent":Landroid/content/Intent; */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.android.settings"; // const-string v2, "com.android.settings"
final String v3 = "com.android.settings.ai.AidaemonService"; // const-string v3, "com.android.settings.ai.AidaemonService"
/* invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 974 */
final String v1 = "key_ai_button_settings"; // const-string v1, "key_ai_button_settings"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 975 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).startServiceAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 978 */
/* nop */
} // .end local v0 # "intent":Landroid/content/Intent;
/* .line 976 */
/* :catch_0 */
/* move-exception v0 */
/* .line 977 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "WindowManager"; // const-string v1, "WindowManager"
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 979 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void startCameraProcess ( ) {
/* .locals 3 */
/* .line 650 */
try { // :try_start_0
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.action.CAMERA_EMPTY_SERVICE"; // const-string v1, "miui.action.CAMERA_EMPTY_SERVICE"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 651 */
/* .local v0, "cameraIntent":Landroid/content/Intent; */
final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 652 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).startServiceAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 655 */
/* nop */
} // .end local v0 # "cameraIntent":Landroid/content/Intent;
/* .line 653 */
/* :catch_0 */
/* move-exception v0 */
/* .line 654 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "WindowManager"; // const-string v1, "WindowManager"
final String v2 = "IllegalAccessException"; // const-string v2, "IllegalAccessException"
android.util.Slog .e ( v1,v2,v0 );
/* .line 656 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void streetSnap ( Boolean p0, Integer p1, Boolean p2, android.view.KeyEvent p3 ) {
/* .locals 4 */
/* .param p1, "isScreenOn" # Z */
/* .param p2, "keyCode" # I */
/* .param p3, "down" # Z */
/* .param p4, "event" # Landroid/view/KeyEvent; */
/* .line 1624 */
/* if-nez p1, :cond_3 */
/* iget v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressVolumeDownBehavior:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_3 */
/* .line 1627 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1628 */
/* .local v0, "keyIntent":Landroid/content/Intent; */
/* const/16 v1, 0x18 */
/* if-eq p2, v1, :cond_1 */
/* const/16 v1, 0x19 */
/* if-ne p2, v1, :cond_0 */
/* .line 1632 */
} // :cond_0
if ( p3 != null) { // if-eqz p3, :cond_2
/* const/16 v1, 0x1a */
/* if-ne p2, v1, :cond_2 */
/* .line 1634 */
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "android.intent.action.KEYCODE_POWER_UP"; // const-string v2, "android.intent.action.KEYCODE_POWER_UP"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* move-object v0, v1 */
/* .line 1631 */
} // :cond_1
} // :goto_0
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "miui.intent.action.CAMERA_KEY_BUTTON"; // const-string v2, "miui.intent.action.CAMERA_KEY_BUTTON"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* move-object v0, v1 */
/* .line 1636 */
} // :cond_2
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1638 */
final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
final String v2 = "com.android.camera.snap.SnapKeyReceiver"; // const-string v2, "com.android.camera.snap.SnapKeyReceiver"
(( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1640 */
final String v1 = "key_code"; // const-string v1, "key_code"
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1641 */
final String v1 = "key_action"; // const-string v1, "key_action"
v2 = (( android.view.KeyEvent ) p4 ).getAction ( ); // invoke-virtual {p4}, Landroid/view/KeyEvent;->getAction()I
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1642 */
final String v1 = "key_event_time"; // const-string v1, "key_event_time"
(( android.view.KeyEvent ) p4 ).getEventTime ( ); // invoke-virtual {p4}, Landroid/view/KeyEvent;->getEventTime()J
/* move-result-wide v2 */
(( android.content.Intent ) v0 ).putExtra ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 1643 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 1646 */
} // .end local v0 # "keyIntent":Landroid/content/Intent;
} // :cond_3
return;
} // .end method
private void takeScreenshot ( android.content.Intent p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "function" # Ljava/lang/String; */
/* .line 1839 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p0, p2, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/String;Landroid/content/Intent;)V */
/* .line 1843 */
final String v2 = "capture_delay"; // const-string v2, "capture_delay"
/* const-wide/16 v3, 0x0 */
(( android.content.Intent ) p1 ).getLongExtra ( v2, v3, v4 ); // invoke-virtual {p1, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J
/* move-result-wide v2 */
/* .line 1839 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 1845 */
return;
} // .end method
private void trackDumpLogKeyCode ( android.view.KeyEvent p0 ) {
/* .locals 11 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 2916 */
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 2917 */
/* .local v0, "code":I */
/* const/16 v1, 0x18 */
int v2 = 0; // const/4 v2, 0x0
/* const/16 v3, 0x19 */
/* if-eq v0, v3, :cond_0 */
/* if-eq v0, v1, :cond_0 */
/* .line 2918 */
/* iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z */
/* .line 2919 */
return;
/* .line 2922 */
} // :cond_0
(( android.view.KeyEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
/* .line 2923 */
/* .local v4, "inputDevice":Landroid/view/InputDevice; */
if ( v4 != null) { // if-eqz v4, :cond_1
v5 = (( android.view.InputDevice ) v4 ).isExternal ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->isExternal()Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 2924 */
/* iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z */
/* .line 2925 */
return;
/* .line 2928 */
} // :cond_1
/* iget-boolean v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z */
int v6 = 1; // const/4 v6, 0x1
/* if-nez v5, :cond_2 */
/* if-ne v0, v1, :cond_2 */
/* .line 2929 */
/* iput-boolean v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z */
/* .line 2930 */
(( android.view.KeyEvent ) p1 ).getEventTime ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J
/* move-result-wide v5 */
/* iput-wide v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeStartTime:J */
/* .line 2931 */
/* iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeLastKeyCode:I */
/* .line 2932 */
/* iput v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeVolumeDownTimes:I */
/* .line 2933 */
} // :cond_2
if ( v5 != null) { // if-eqz v5, :cond_7
/* .line 2934 */
(( android.view.KeyEvent ) p1 ).getEventTime ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J
/* move-result-wide v7 */
/* iget-wide v9, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeStartTime:J */
/* sub-long/2addr v7, v9 */
/* .line 2935 */
/* .local v7, "timeDelta":J */
/* iget v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeTimeOut:I */
/* int-to-long v9, v5 */
/* cmp-long v5, v7, v9 */
/* if-gez v5, :cond_5 */
/* iget v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeLastKeyCode:I */
/* if-ne v0, v5, :cond_3 */
/* .line 2945 */
} // :cond_3
/* iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeLastKeyCode:I */
/* .line 2946 */
/* if-ne v0, v3, :cond_4 */
/* .line 2947 */
/* iget v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeVolumeDownTimes:I */
/* add-int/2addr v1, v6 */
/* iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeVolumeDownTimes:I */
/* .line 2949 */
} // :cond_4
/* iget v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeVolumeDownTimes:I */
int v3 = 3; // const/4 v3, 0x3
/* if-ne v1, v3, :cond_7 */
/* .line 2950 */
/* iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z */
/* .line 2951 */
final String v1 = "DumpLog triggered"; // const-string v1, "DumpLog triggered"
com.android.server.policy.MiuiInputLog .defaults ( v1 );
/* .line 2952 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2936 */
} // :cond_5
} // :goto_0
/* iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z */
/* .line 2937 */
/* if-ne v0, v1, :cond_6 */
/* .line 2938 */
/* iput-boolean v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodePengding:Z */
/* .line 2939 */
(( android.view.KeyEvent ) p1 ).getEventTime ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J
/* move-result-wide v5 */
/* iput-wide v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeStartTime:J */
/* .line 2940 */
/* iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeLastKeyCode:I */
/* .line 2941 */
/* iput v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackDumpLogKeyCodeVolumeDownTimes:I */
/* .line 2943 */
} // :cond_6
return;
/* .line 2959 */
} // .end local v7 # "timeDelta":J
} // :cond_7
} // :goto_1
return;
} // .end method
/* # virtual methods */
protected abstract Integer callSuperInterceptKeyBeforeQueueing ( android.view.KeyEvent p0, Integer p1, Boolean p2 ) {
} // .end method
protected abstract void cancelPreloadRecentAppsInternal ( ) {
} // .end method
Boolean checkProcessRunning ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 2683 */
v0 = this.mContext;
final String v1 = "activity"; // const-string v1, "activity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
/* .line 2684 */
/* .local v0, "am":Landroid/app/ActivityManager; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 2685 */
/* .line 2688 */
} // :cond_0
(( android.app.ActivityManager ) v0 ).getRunningAppProcesses ( ); // invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
/* .line 2690 */
/* .local v2, "procs":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
/* if-nez v2, :cond_1 */
/* .line 2691 */
/* .line 2694 */
} // :cond_1
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* .line 2695 */
/* .local v4, "info":Landroid/app/ActivityManager$RunningAppProcessInfo; */
v5 = this.processName;
v5 = (( java.lang.String ) p1 ).equalsIgnoreCase ( v5 ); // invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 2696 */
int v1 = 1; // const/4 v1, 0x1
/* .line 2698 */
} // .end local v4 # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
} // :cond_2
/* .line 2700 */
} // :cond_3
} // .end method
public void dump ( java.lang.String p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 2 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 3043 */
/* invoke-super {p0, p1, p2, p3}, Lcom/android/server/policy/PhoneWindowManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;[Ljava/lang/String;)V */
/* .line 3044 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "BaseMiuiPhoneWindowManager"; // const-string v0, "BaseMiuiPhoneWindowManager"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3045 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 3046 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mInputMethodWindowVisibleHeight="; // const-string v0, "mInputMethodWindowVisibleHeight="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mInputMethodWindowVisibleHeight:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 3047 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mFrontFingerprintSensor="; // const-string v0, "mFrontFingerprintSensor="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFrontFingerprintSensor:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3048 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mSupportTapFingerprintSensorToHome="; // const-string v0, "mSupportTapFingerprintSensorToHome="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSupportTapFingerprintSensorToHome:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3049 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mScreenOffReason="; // const-string v0, "mScreenOffReason="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenOffReason:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 3050 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mIsStatusBarVisibleInFullscreen="; // const-string v0, "mIsStatusBarVisibleInFullscreen="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsStatusBarVisibleInFullscreen:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3051 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mScreenRecorderEnabled="; // const-string v0, "mScreenRecorderEnabled="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenRecorderEnabled:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3052 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mVoiceAssistEnabled="; // const-string v0, "mVoiceAssistEnabled="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVoiceAssistEnabled:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3053 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mWifiOnly="; // const-string v0, "mWifiOnly="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWifiOnly:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3054 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "KEYCODE_MENU KeyBitmask="; // const-string v0, "KEYCODE_MENU KeyBitmask="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* const/16 v0, 0x52 */
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v0 );
java.lang.Integer .toBinaryString ( v0 );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3055 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "KEYCODE_APP_SWITCH KeyBitmask="; // const-string v0, "KEYCODE_APP_SWITCH KeyBitmask="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* const/16 v0, 0xbb */
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v0 );
java.lang.Integer .toBinaryString ( v0 );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3056 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "KEYCODE_HOME KeyBitmask="; // const-string v0, "KEYCODE_HOME KeyBitmask="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
int v0 = 3; // const/4 v0, 0x3
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v0 );
java.lang.Integer .toBinaryString ( v0 );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3057 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "KEYCODE_BACK KeyBitmask="; // const-string v0, "KEYCODE_BACK KeyBitmask="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
int v0 = 4; // const/4 v0, 0x4
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v0 );
java.lang.Integer .toBinaryString ( v0 );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3058 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "KEYCODE_POWER KeyBitmask="; // const-string v0, "KEYCODE_POWER KeyBitmask="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* const/16 v0, 0x1a */
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v0 );
java.lang.Integer .toBinaryString ( v0 );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3059 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "KEYCODE_VOLUME_DOWN KeyBitmask="; // const-string v0, "KEYCODE_VOLUME_DOWN KeyBitmask="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* const/16 v0, 0x19 */
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v0 );
java.lang.Integer .toBinaryString ( v0 );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3060 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "KEYCODE_VOLUME_UP KeyBitmask="; // const-string v0, "KEYCODE_VOLUME_UP KeyBitmask="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* const/16 v0, 0x18 */
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager .getKeyBitmask ( v0 );
java.lang.Integer .toBinaryString ( v0 );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3061 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "ElSE KEYCODE KeyBitmask="; // const-string v0, "ElSE KEYCODE KeyBitmask="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
int v0 = 1; // const/4 v0, 0x1
java.lang.Integer .toBinaryString ( v0 );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3062 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "SHORTCUT_HOME_POWER="; // const-string v0, "SHORTCUT_HOME_POWER="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
java.lang.Integer .toBinaryString ( v0 );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3063 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "SHORTCUT_BACK_POWER="; // const-string v0, "SHORTCUT_BACK_POWER="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
java.lang.Integer .toBinaryString ( v0 );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3064 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "SHORTCUT_MENU_POWER="; // const-string v0, "SHORTCUT_MENU_POWER="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
java.lang.Integer .toBinaryString ( v0 );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3065 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "SHORTCUT_SCREENSHOT_ANDROID="; // const-string v0, "SHORTCUT_SCREENSHOT_ANDROID="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
java.lang.Integer .toBinaryString ( v0 );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3066 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "SHORTCUT_SCREENSHOT_MIUI="; // const-string v0, "SHORTCUT_SCREENSHOT_MIUI="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
java.lang.Integer .toBinaryString ( v0 );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3067 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "SHORTCUT_UNLOCK="; // const-string v0, "SHORTCUT_UNLOCK="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
java.lang.Integer .toBinaryString ( v0 );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3068 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mDpadCenterDown="; // const-string v0, "mDpadCenterDown="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDpadCenterDown:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3069 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mHomeDownAfterDpCenter="; // const-string v0, "mHomeDownAfterDpCenter="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDownAfterDpCenter:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3070 */
final String v0 = " "; // const-string v0, " "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "KeyResponseSetting"; // const-string v0, "KeyResponseSetting"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3071 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mCurrentUserId="; // const-string v0, "mCurrentUserId="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCurrentUserId:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 3072 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mMikeymodeEnabled="; // const-string v0, "mMikeymodeEnabled="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMikeymodeEnabled:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3073 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mCameraKeyWakeScreen="; // const-string v0, "mCameraKeyWakeScreen="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCameraKeyWakeScreen:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3074 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mTrackballWakeScreen="; // const-string v0, "mTrackballWakeScreen="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackballWakeScreen:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3075 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mTestModeEnabled="; // const-string v0, "mTestModeEnabled="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3077 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mScreenButtonsDisabled="; // const-string v0, "mScreenButtonsDisabled="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
v0 = this.mAutoDisableScreenButtonsManager;
v0 = (( com.miui.server.input.AutoDisableScreenButtonsManager ) v0 ).isScreenButtonsDisabled ( ); // invoke-virtual {v0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->isScreenButtonsDisabled()Z
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3078 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mVolumeButtonPrePressedTime="; // const-string v0, "mVolumeButtonPrePressedTime="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-wide v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeButtonPrePressedTime:J */
(( java.io.PrintWriter ) p2 ).println ( v0, v1 ); // invoke-virtual {p2, v0, v1}, Ljava/io/PrintWriter;->println(J)V
/* .line 3079 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mVolumeButtonPressedCount="; // const-string v0, "mVolumeButtonPressedCount="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-wide v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeButtonPressedCount:J */
(( java.io.PrintWriter ) p2 ).println ( v0, v1 ); // invoke-virtual {p2, v0, v1}, Ljava/io/PrintWriter;->println(J)V
/* .line 3080 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mHaveBankCard="; // const-string v0, "mHaveBankCard="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveBankCard:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3081 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mHaveTranksCard="; // const-string v0, "mHaveTranksCard="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveTranksCard:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3082 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mLongPressVolumeDownBehavior="; // const-string v0, "mLongPressVolumeDownBehavior="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mLongPressVolumeDownBehavior:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 3083 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mIsVRMode="; // const-string v0, "mIsVRMode="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsVRMode:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3084 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mTalkBackIsOpened="; // const-string v0, "mTalkBackIsOpened="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTalkBackIsOpened:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3085 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mShortcutServiceIsTalkBack="; // const-string v0, "mShortcutServiceIsTalkBack="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mShortcutServiceIsTalkBack:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 3086 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "mImperceptiblePowerKey="; // const-string v0, "mImperceptiblePowerKey="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
v0 = this.mImperceptiblePowerKey;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3087 */
v0 = this.mSmartCoverManager;
(( miui.util.SmartCoverManager ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/util/SmartCoverManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 3088 */
v0 = this.mMiuiThreeGestureListener;
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 3089 */
v0 = this.mMiuiKnockGestureService;
(( com.miui.server.input.knock.MiuiKnockGestureService ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 3090 */
v0 = this.mMiuiBackTapGestureService;
(( com.miui.server.input.MiuiBackTapGestureService ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/MiuiBackTapGestureService;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 3091 */
v0 = this.mMiuiKeyShortcutRuleManager;
(( com.android.server.policy.MiuiKeyShortcutRuleManager ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 3092 */
v0 = this.mMiuiFingerPrintTapListener;
(( com.miui.server.input.MiuiFingerPrintTapListener ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 3093 */
v0 = this.mMiuiTimeFloatingWindow;
(( com.miui.server.input.time.MiuiTimeFloatingWindow ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 3094 */
v0 = /* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isMiPad()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 3095 */
v0 = this.mContext;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .getInstance ( v0 );
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 3096 */
com.android.server.input.KeyboardCombinationManagerStubImpl .getInstance ( );
(( com.android.server.input.KeyboardCombinationManagerStubImpl ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 3098 */
} // :cond_0
v0 = this.mMiuiStylusShortcutManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 3099 */
(( com.miui.server.input.stylus.MiuiStylusShortcutManager ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 3100 */
com.miui.server.input.stylus.blocker.MiuiEventBlockerManager .getInstance ( );
(( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 3102 */
} // :cond_1
v0 = this.mMiuiPadKeyboardManager;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 3103 */
/* .line 3105 */
} // :cond_2
com.android.server.input.overscroller.ScrollerOptimizationConfigProvider .getInstance ( );
(( com.android.server.input.overscroller.ScrollerOptimizationConfigProvider ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 3106 */
return;
} // .end method
public void enableScreenAfterBoot ( ) {
/* .locals 2 */
/* .line 2744 */
/* invoke-super {p0}, Lcom/android/server/policy/PhoneWindowManager;->enableScreenAfterBoot()V */
/* .line 2745 */
final String v0 = "ro.radio.noril"; // const-string v0, "ro.radio.noril"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWifiOnly:Z */
/* .line 2746 */
v0 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
/* iget-boolean v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mWifiOnly:Z */
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).setWifiOnly ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->setWifiOnly(Z)V
/* .line 2747 */
v0 = this.mSmartCoverManager;
(( miui.util.SmartCoverManager ) v0 ).enableLidAfterBoot ( ); // invoke-virtual {v0}, Lmiui/util/SmartCoverManager;->enableLidAfterBoot()V
/* .line 2748 */
return;
} // .end method
protected abstract void finishActivityInternal ( android.os.IBinder p0, Integer p1, android.content.Intent p2 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public void finishLayoutLw ( com.android.server.wm.DisplayFrames p0, android.graphics.Rect p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "displayFrames" # Lcom/android/server/wm/DisplayFrames; */
/* .param p2, "inputMethodRegion" # Landroid/graphics/Rect; */
/* .param p3, "displayId" # I */
/* .line 2751 */
/* iget v0, p2, Landroid/graphics/Rect;->bottom:I */
/* iget v1, p2, Landroid/graphics/Rect;->top:I */
/* sub-int/2addr v0, v1 */
/* .line 2752 */
/* .local v0, "inputMethodHeight":I */
/* iget v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mInputMethodWindowVisibleHeight:I */
/* if-eq v1, v0, :cond_0 */
/* .line 2753 */
/* iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mInputMethodWindowVisibleHeight:I */
/* .line 2754 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "input method visible height changed "; // const-string v2, "input method visible height changed "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "WindowManager"; // const-string v2, "WindowManager"
android.util.Slog .i ( v2,v1 );
/* .line 2755 */
v1 = this.mMiuiKnockGestureService;
(( com.miui.server.input.knock.MiuiKnockGestureService ) v1 ).setInputMethodRect ( p2 ); // invoke-virtual {v1, p2}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setInputMethodRect(Landroid/graphics/Rect;)V
/* .line 2756 */
com.android.server.input.MiuiInputThread .getHandler ( );
/* new-instance v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda6; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;I)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2757 */
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "miui.intent.action.INPUT_METHOD_VISIBLE_HEIGHT_CHANGED"; // const-string v2, "miui.intent.action.INPUT_METHOD_VISIBLE_HEIGHT_CHANGED"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 2758 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v2 = "miui.intent.extra.input_method_visible_height"; // const-string v2, "miui.intent.extra.input_method_visible_height"
/* iget v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mInputMethodWindowVisibleHeight:I */
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 2760 */
final String v2 = "miui.permission.USE_INTERNAL_GENERAL_API"; // const-string v2, "miui.permission.USE_INTERNAL_GENERAL_API"
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).sendAsyncBroadcast ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
/* .line 2762 */
} // .end local v1 # "intent":Landroid/content/Intent;
} // :cond_0
/* iget v1, p1, Lcom/android/server/wm/DisplayFrames;->mWidth:I */
/* iget v2, p1, Lcom/android/server/wm/DisplayFrames;->mHeight:I */
v1 = java.lang.Math .min ( v1,v2 );
/* add-int/lit8 v1, v1, -0x1 */
/* .line 2765 */
/* .local v1, "displayWidth":I */
/* iget-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsFoldChanged:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* if-nez p3, :cond_1 */
v2 = this.mEdgeSuppressionManager;
/* .line 2766 */
v2 = (( com.miui.server.input.edgesuppression.EdgeSuppressionManager ) v2 ).getScreenWidth ( ); // invoke-virtual {v2}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->getScreenWidth()I
/* if-eq v1, v2, :cond_1 */
/* .line 2767 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsFoldChanged:Z */
/* .line 2768 */
v2 = this.mEdgeSuppressionManager;
final String v3 = "configuration"; // const-string v3, "configuration"
/* iget-boolean v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFolded:Z */
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager ) v2 ).handleEdgeModeChange ( v3, v4, p1 ); // invoke-virtual {v2, v3, v4, p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->handleEdgeModeChange(Ljava/lang/String;ZLcom/android/server/wm/DisplayFrames;)V
/* .line 2771 */
} // :cond_1
return;
} // .end method
public void finishedGoingToSleep ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "displayGroupId" # I */
/* .param p2, "why" # I */
/* .line 683 */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).screenTurnedOffInternal ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->screenTurnedOffInternal(I)V
/* .line 684 */
v0 = this.mMiuiBackTapGestureService;
(( com.miui.server.input.MiuiBackTapGestureService ) v0 ).notifyScreenOff ( ); // invoke-virtual {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->notifyScreenOff()V
/* .line 685 */
v0 = this.mEdgeSuppressionManager;
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager ) v0 ).finishedGoingToSleep ( ); // invoke-virtual {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->finishedGoingToSleep()V
/* .line 686 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->releaseScreenOnProximitySensor(Z)V */
/* .line 687 */
/* sget-boolean v0, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* if-nez p1, :cond_0 */
/* .line 688 */
com.android.server.display.DisplayManagerServiceStub .getInstance ( );
(( com.android.server.display.DisplayManagerServiceStub ) v0 ).finishedGoingToSleep ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayManagerServiceStub;->finishedGoingToSleep()V
/* .line 690 */
} // :cond_0
/* invoke-super {p0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager;->finishedGoingToSleep(II)V */
/* .line 691 */
return;
} // .end method
public void finishedWakingUp ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "displayGroupId" # I */
/* .param p2, "why" # I */
/* .line 695 */
/* invoke-super {p0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager;->finishedWakingUp(II)V */
/* .line 696 */
v0 = this.mEdgeSuppressionManager;
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager ) v0 ).finishedWakingUp ( ); // invoke-virtual {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->finishedWakingUp()V
/* .line 697 */
return;
} // .end method
protected abstract void forceStopPackage ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
} // .end method
public com.android.server.policy.WindowManagerPolicy$WindowState getFocusedWindow ( ) {
/* .locals 1 */
/* .line 2838 */
v0 = this.mFocusedWindow;
} // .end method
protected Boolean getForbidFullScreenFlag ( ) {
/* .locals 1 */
/* .line 2784 */
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mForbidFullScreen:Z */
} // .end method
public Boolean getKeyguardActive ( ) {
/* .locals 1 */
/* .line 2827 */
v0 = this.mMiuiKeyguardDelegate;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mPowerManager;
v0 = (( android.os.PowerManager ) v0 ).isScreenOn ( ); // invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mMiuiKeyguardDelegate;
/* .line 2828 */
v0 = (( com.android.server.policy.MiuiKeyguardServiceDelegate ) v0 ).isShowingAndNotHidden ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
v0 = this.mMiuiKeyguardDelegate;
/* .line 2829 */
v0 = (( com.android.server.policy.MiuiKeyguardServiceDelegate ) v0 ).isShowing ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowing()Z
if ( v0 != null) { // if-eqz v0, :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 2827 */
} // :goto_1
} // .end method
protected abstract com.android.server.policy.WindowManagerPolicy$WindowState getKeyguardWindowState ( ) {
} // .end method
public com.android.server.policy.PhoneWindowManager$PowerKeyRule getOriginalPowerKeyRule ( ) {
/* .locals 1 */
/* .line 1611 */
v0 = this.mPowerKeyRule;
} // .end method
protected abstract Integer getWakePolicyFlag ( ) {
} // .end method
public void hideBootMessages ( ) {
/* .locals 2 */
/* .line 2669 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$15; */
/* invoke-direct {v1, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$15;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2680 */
return;
} // .end method
protected void initInternal ( android.content.Context p0, com.android.server.policy.WindowManagerPolicy$WindowManagerFuncs p1, android.view.IWindowManager p2 ) {
/* .locals 11 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "windowManagerFuncs" # Lcom/android/server/policy/WindowManagerPolicy$WindowManagerFuncs; */
/* .param p3, "windowManager" # Landroid/view/IWindowManager; */
/* .line 345 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 346 */
/* .local v0, "res":Landroid/content/res/Resources; */
/* const v1, 0x10501fb */
v1 = (( android.content.res.Resources ) v0 ).getDimensionPixelSize ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I
/* iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mNavBarWidth:I */
/* .line 347 */
/* const v1, 0x10501f6 */
v1 = (( android.content.res.Resources ) v0 ).getDimensionPixelSize ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I
/* iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mNavBarHeight:I */
/* .line 348 */
/* const v1, 0x10501f8 */
v1 = (( android.content.res.Resources ) v0 ).getDimensionPixelSize ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I
/* iput v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mNavBarHeightLand:I */
/* .line 349 */
miui.util.ITouchFeature .getInstance ( );
v1 = (( miui.util.ITouchFeature ) v1 ).hasSupportGlobalTouchDirection ( ); // invoke-virtual {v1}, Lmiui/util/ITouchFeature;->hasSupportGlobalTouchDirection()Z
/* iput-boolean v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsSupportGloablTounchDirection:Z */
/* .line 350 */
/* if-nez v1, :cond_0 */
/* sget-boolean v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 352 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->handleTouchFeatureRotationWatcher()V */
/* .line 354 */
} // :cond_1
/* new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H-IA;)V */
this.mHandler = v1;
/* .line 355 */
/* const-class v1, Lcom/android/server/input/MiuiInputManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/android/server/input/MiuiInputManagerInternal; */
this.mMiuiInputManagerInternal = v1;
/* .line 356 */
miui.hardware.input.InputFeature .getInstance ( p1 );
this.mInputFeature = v1;
/* .line 357 */
(( miui.hardware.input.InputFeature ) v1 ).init ( ); // invoke-virtual {v1}, Lmiui/hardware/input/InputFeature;->init()V
/* .line 358 */
/* new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver; */
v2 = this.mHandler;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/os/Handler;)V */
this.mSettingsObserver = v1;
/* .line 359 */
com.miui.server.input.edgesuppression.EdgeSuppressionManager .getInstance ( p1 );
this.mEdgeSuppressionManager = v1;
/* .line 361 */
com.android.server.policy.MiuiShortcutTriggerHelper .getInstance ( p1 );
this.mMiuiShortcutTriggerHelper = v1;
/* .line 362 */
v1 = this.mContext;
v2 = this.mSingleKeyGestureDetector;
v3 = this.mKeyCombinationManager;
v4 = this.mMiuiShortcutTriggerHelper;
com.android.server.policy.MiuiKeyShortcutRuleManager .getInstance ( v1,v2,v3,v4 );
this.mMiuiKeyShortcutRuleManager = v1;
/* .line 364 */
v1 = this.mContext;
com.android.server.input.shortcut.ShortcutOneTrackHelper .getInstance ( v1 );
this.mShortcutOneTrackHelper = v1;
/* .line 365 */
v1 = com.miui.server.input.stylus.MiuiStylusUtils .supportStylusGesture ( );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 366 */
com.miui.server.input.stylus.MiuiStylusShortcutManager .getInstance ( );
this.mMiuiStylusShortcutManager = v1;
/* .line 368 */
} // :cond_2
v1 = this.mContext;
com.android.server.policy.MiuiKeyInterceptExtend .getInstance ( v1 );
this.mMiuiKeyInterceptExtend = v1;
/* .line 369 */
v1 = this.mSettingsObserver;
(( com.android.server.policy.BaseMiuiPhoneWindowManager$MiuiSettingsObserver ) v1 ).observe ( ); // invoke-virtual {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->observe()V
/* .line 371 */
v1 = com.android.server.policy.BaseMiuiPhoneWindowManager.phoneWindowManagerFeature;
/* new-instance v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$1; */
/* invoke-direct {v2, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$1;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
(( com.android.server.policy.PhoneWindowManagerFeatureImpl ) v1 ).setPowerLongPress ( p0, v2 ); // invoke-virtual {v1, p0, v2}, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;->setPowerLongPress(Lcom/android/server/policy/PhoneWindowManager;Ljava/lang/Runnable;)V
/* .line 377 */
v1 = this.mPowerManager;
final String v2 = "PhoneWindowManager.mVolumeKeyWakeLock"; // const-string v2, "PhoneWindowManager.mVolumeKeyWakeLock"
int v3 = 1; // const/4 v3, 0x1
(( android.os.PowerManager ) v1 ).newWakeLock ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
this.mVolumeKeyWakeLock = v1;
/* .line 379 */
v1 = this.mPowerManager;
final String v2 = "PhoneWindowManager.mAiKeyWakeLock"; // const-string v2, "PhoneWindowManager.mAiKeyWakeLock"
(( android.os.PowerManager ) v1 ).newWakeLock ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
this.mAiKeyWakeLock = v1;
/* .line 382 */
v1 = this.mPowerManager;
final String v2 = "PhoneWindowManager.mHelpKeyWakeLock"; // const-string v2, "PhoneWindowManager.mHelpKeyWakeLock"
(( android.os.PowerManager ) v1 ).newWakeLock ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
this.mHelpKeyWakeLock = v1;
/* .line 384 */
v1 = this.mPowerManager;
/* const v2, 0x3000001a */
final String v3 = "Reset"; // const-string v3, "Reset"
(( android.os.PowerManager ) v1 ).newWakeLock ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
this.mVolumeKeyUpWakeLock = v1;
/* .line 386 */
/* new-instance v1, Landroid/content/IntentFilter; */
/* invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V */
/* .line 387 */
/* .local v1, "filter":Landroid/content/IntentFilter; */
final String v2 = "android.intent.action.CAPTURE_SCREENSHOT"; // const-string v2, "android.intent.action.CAPTURE_SCREENSHOT"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 388 */
v3 = this.mScreenshotReceiver;
v4 = android.os.UserHandle.ALL;
final String v6 = "miui.permission.USE_INTERNAL_GENERAL_API"; // const-string v6, "miui.permission.USE_INTERNAL_GENERAL_API"
int v7 = 0; // const/4 v7, 0x0
int v8 = 2; // const/4 v8, 0x2
/* move-object v2, p1 */
/* move-object v5, v1 */
/* invoke-virtual/range {v2 ..v8}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent; */
/* .line 391 */
/* new-instance v2, Landroid/content/IntentFilter; */
/* invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V */
/* move-object v1, v2 */
/* .line 392 */
final String v2 = "android.intent.action.CAPTURE_PARTIAL_SCREENSHOT"; // const-string v2, "android.intent.action.CAPTURE_PARTIAL_SCREENSHOT"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 393 */
v4 = this.mPartialScreenshotReceiver;
v5 = android.os.UserHandle.ALL;
final String v7 = "miui.permission.USE_INTERNAL_GENERAL_API"; // const-string v7, "miui.permission.USE_INTERNAL_GENERAL_API"
int v8 = 0; // const/4 v8, 0x0
int v9 = 2; // const/4 v9, 0x2
/* move-object v3, p1 */
/* move-object v6, v1 */
/* invoke-virtual/range {v3 ..v9}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent; */
/* .line 396 */
/* new-instance v2, Landroid/content/IntentFilter; */
/* invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V */
/* move-object v1, v2 */
/* .line 397 */
final String v2 = "com.miui.app.ExtraStatusBarManager.EXIT_FULLSCREEN"; // const-string v2, "com.miui.app.ExtraStatusBarManager.EXIT_FULLSCREEN"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 398 */
v2 = this.mStatusBarExitFullscreenReceiver;
int v3 = 2; // const/4 v3, 0x2
(( android.content.Context ) p1 ).registerReceiver ( v2, v1, v3 ); // invoke-virtual {p1, v2, v1, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 401 */
/* new-instance v2, Landroid/content/IntentFilter; */
/* invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V */
/* move-object v1, v2 */
/* .line 402 */
final String v2 = "miui.intent.SCREEN_RECORDER_ENABLE_KEYEVENT"; // const-string v2, "miui.intent.SCREEN_RECORDER_ENABLE_KEYEVENT"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 403 */
v2 = this.mScreenRecordeEnablekKeyEventReceiver;
(( android.content.Context ) p1 ).registerReceiver ( v2, v1, v3 ); // invoke-virtual {p1, v2, v1, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 406 */
/* new-instance v2, Landroid/content/IntentFilter; */
/* invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V */
/* move-object v1, v2 */
/* .line 407 */
final String v2 = "com.miui.app.ExtraStatusBarManager.action_enter_drive_mode"; // const-string v2, "com.miui.app.ExtraStatusBarManager.action_enter_drive_mode"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 408 */
final String v2 = "com.miui.app.ExtraStatusBarManager.action_leave_drive_mode"; // const-string v2, "com.miui.app.ExtraStatusBarManager.action_leave_drive_mode"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 409 */
v5 = this.mInternalBroadcastReceiver;
v6 = android.os.UserHandle.ALL;
final String v8 = "miui.permission.USE_INTERNAL_GENERAL_API"; // const-string v8, "miui.permission.USE_INTERNAL_GENERAL_API"
v9 = this.mHandler;
int v10 = 2; // const/4 v10, 0x2
/* move-object v4, p1 */
/* move-object v7, v1 */
/* invoke-virtual/range {v4 ..v10}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent; */
/* .line 415 */
/* new-instance v2, Landroid/content/IntentFilter; */
/* invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V */
/* move-object v1, v2 */
/* .line 416 */
final String v2 = "android.intent.action.BOOT_COMPLETED"; // const-string v2, "android.intent.action.BOOT_COMPLETED"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 417 */
v2 = this.mBootCompleteReceiver;
(( android.content.Context ) p1 ).registerReceiver ( v2, v1, v3 ); // invoke-virtual {p1, v2, v1, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 419 */
/* new-instance v2, Lmiui/util/HapticFeedbackUtil; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, p1, v3}, Lmiui/util/HapticFeedbackUtil;-><init>(Landroid/content/Context;Z)V */
this.mHapticFeedbackUtil = v2;
/* .line 420 */
/* new-instance v2, Lcom/miui/server/input/AutoDisableScreenButtonsManager; */
v3 = this.mHandler;
/* invoke-direct {v2, p1, v3}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;-><init>(Landroid/content/Context;Landroid/os/Handler;)V */
this.mAutoDisableScreenButtonsManager = v2;
/* .line 421 */
v2 = this.mSmartCoverManager;
v3 = this.mPowerManager;
v4 = this.mHandler;
(( miui.util.SmartCoverManager ) v2 ).init ( p1, v3, v4 ); // invoke-virtual {v2, p1, v3, v4}, Lmiui/util/SmartCoverManager;->init(Landroid/content/Context;Landroid/os/PowerManager;Landroid/os/Handler;)V
/* .line 422 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->saveWindowTypeLayer(Landroid/content/Context;)V */
/* .line 423 */
/* new-instance v2, Lcom/miui/server/input/time/MiuiTimeFloatingWindow; */
v3 = this.mContext;
/* invoke-direct {v2, v3}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;-><init>(Landroid/content/Context;)V */
this.mMiuiTimeFloatingWindow = v2;
/* .line 424 */
/* new-instance v2, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager; */
v3 = this.mContext;
/* invoke-direct {v2, v3}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;-><init>(Landroid/content/Context;)V */
this.mWakeUpContentCatcherManager = v2;
/* .line 425 */
return;
} // .end method
protected Integer intercept ( android.view.KeyEvent p0, Integer p1, Boolean p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "policyFlags" # I */
/* .param p3, "isScreenOn" # Z */
/* .param p4, "expectedResult" # I */
/* .line 1050 */
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1052 */
/* .local v0, "down":Z */
} // :goto_0
/* if-nez v0, :cond_1 */
/* .line 1053 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->cancelEventAndCallSuperQueueing(Landroid/view/KeyEvent;IZ)V */
/* .line 1055 */
} // :cond_1
} // .end method
public Long interceptKeyBeforeDispatching ( android.os.IBinder p0, android.view.KeyEvent p1, Integer p2 ) {
/* .locals 26 */
/* .param p1, "focusedToken" # Landroid/os/IBinder; */
/* .param p2, "event" # Landroid/view/KeyEvent; */
/* .param p3, "policyFlags" # I */
/* .line 729 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p2 */
/* const-wide/16 v2, -0x1 */
/* .line 730 */
/* .local v2, "key_consumed":J */
/* const-wide/16 v4, 0x0 */
/* .line 732 */
/* .local v4, "key_not_consumed":J */
v6 = /* invoke-virtual/range {p2 ..p2}, Landroid/view/KeyEvent;->getRepeatCount()I */
/* .line 733 */
/* .local v6, "repeatCount":I */
v7 = /* invoke-virtual/range {p2 ..p2}, Landroid/view/KeyEvent;->getAction()I */
int v8 = 1; // const/4 v8, 0x1
/* if-nez v7, :cond_0 */
/* move v7, v8 */
} // :cond_0
int v7 = 0; // const/4 v7, 0x0
/* .line 734 */
/* .local v7, "down":Z */
} // :goto_0
v10 = /* invoke-virtual/range {p2 ..p2}, Landroid/view/KeyEvent;->isCanceled()Z */
/* .line 735 */
/* .local v10, "canceled":Z */
v11 = this.mMiuiKeyguardDelegate;
if ( v11 != null) { // if-eqz v11, :cond_1
v11 = (( com.android.server.policy.MiuiKeyguardServiceDelegate ) v11 ).isShowingAndNotHidden ( ); // invoke-virtual {v11}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z
if ( v11 != null) { // if-eqz v11, :cond_1
/* move v11, v8 */
} // :cond_1
int v11 = 0; // const/4 v11, 0x0
/* .line 737 */
/* .local v11, "keyguardActive":Z */
} // :goto_1
v12 = this.mKeyCombinationManager;
v12 = (( com.android.server.policy.KeyCombinationManager ) v12 ).isKeyConsumed ( v1 ); // invoke-virtual {v12, v1}, Lcom/android/server/policy/KeyCombinationManager;->isKeyConsumed(Landroid/view/KeyEvent;)Z
if ( v12 != null) { // if-eqz v12, :cond_2
v12 = this.mMiuiShortcutTriggerHelper;
/* .line 738 */
v12 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v12 ).skipKeyGesutre ( v1 ); // invoke-virtual {v12, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->skipKeyGesutre(Landroid/view/KeyEvent;)Z
/* if-nez v12, :cond_2 */
/* .line 739 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "is key consumed,keyCode="; // const-string v9, "is key consumed,keyCode="
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = /* invoke-virtual/range {p2 ..p2}, Landroid/view/KeyEvent;->getKeyCode()I */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = " downTime="; // const-string v9, " downTime="
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 740 */
/* invoke-virtual/range {p2 ..p2}, Landroid/view/KeyEvent;->getDownTime()J */
/* move-result-wide v13 */
(( java.lang.StringBuilder ) v8 ).append ( v13, v14 ); // invoke-virtual {v8, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 739 */
com.android.server.policy.MiuiInputLog .defaults ( v8 );
/* .line 741 */
/* const-wide/16 v8, -0x1 */
/* return-wide v8 */
/* .line 744 */
} // :cond_2
v12 = this.mMiuiKeyInterceptExtend;
v13 = this.mFocusedWindow;
/* move/from16 v14, p3 */
v12 = (( com.android.server.policy.MiuiKeyInterceptExtend ) v12 ).getKeyInterceptTypeBeforeDispatching ( v1, v14, v13 ); // invoke-virtual {v12, v1, v14, v13}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getKeyInterceptTypeBeforeDispatching(Landroid/view/KeyEvent;ILcom/android/server/policy/WindowManagerPolicy$WindowState;)I
/* .line 746 */
/* .local v12, "interceptType":I */
/* const-wide/16 v17, 0x0 */
/* if-ne v12, v8, :cond_3 */
/* .line 747 */
final String v8 = "pass the key to app!"; // const-string v8, "pass the key to app!"
com.android.server.policy.MiuiInputLog .defaults ( v8 );
/* .line 748 */
/* return-wide v17 */
/* .line 749 */
} // :cond_3
int v13 = 2; // const/4 v13, 0x2
/* if-ne v12, v13, :cond_4 */
/* .line 750 */
/* const-string/jumbo v8, "send the key to aosp!" */
com.android.server.policy.MiuiInputLog .defaults ( v8 );
/* .line 751 */
/* invoke-super/range {p0 ..p3}, Lcom/android/server/policy/PhoneWindowManager;->interceptKeyBeforeDispatching(Landroid/os/IBinder;Landroid/view/KeyEvent;I)J */
/* move-result-wide v8 */
/* return-wide v8 */
/* .line 752 */
} // :cond_4
int v13 = 4; // const/4 v13, 0x4
/* if-ne v12, v13, :cond_5 */
/* .line 753 */
/* const-wide/16 v8, -0x1 */
/* return-wide v8 */
/* .line 757 */
} // :cond_5
v15 = this.mWindowManager;
/* check-cast v15, Lcom/android/server/wm/WindowManagerService; */
/* move-object/from16 v13, p1 */
com.android.server.wm.WindowStateStubImpl .getWinStateFromInputWinMap ( v15,v13 );
/* .line 758 */
/* .local v15, "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
if ( v7 != null) { // if-eqz v7, :cond_6
/* if-nez v6, :cond_6 */
/* .line 759 */
this.mWin = v15;
/* .line 762 */
} // :cond_6
/* const/16 v16, 0x0 */
/* .line 763 */
/* .local v16, "cancelKeyFunction":Z */
v9 = this.mMiuiShortcutTriggerHelper;
v9 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v9 ).interceptKeyByLongPress ( v1 ); // invoke-virtual {v9, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->interceptKeyByLongPress(Landroid/view/KeyEvent;)Z
/* const/high16 v21, 0x1000000 */
if ( v9 != null) { // if-eqz v9, :cond_7
/* .line 764 */
v9 = /* invoke-virtual/range {p2 ..p2}, Landroid/view/KeyEvent;->getFlags()I */
/* and-int v9, v9, v21 */
if ( v9 != null) { // if-eqz v9, :cond_8
} // :cond_7
if ( v10 != null) { // if-eqz v10, :cond_9
/* .line 765 */
} // :cond_8
/* const/16 v16, 0x1 */
/* move/from16 v9, v16 */
/* .line 764 */
} // :cond_9
/* move/from16 v9, v16 */
/* .line 767 */
} // .end local v16 # "cancelKeyFunction":Z
/* .local v9, "cancelKeyFunction":Z */
} // :goto_2
/* move-wide/from16 v22, v2 */
} // .end local v2 # "key_consumed":J
/* .local v22, "key_consumed":J */
v2 = /* invoke-virtual/range {p2 ..p2}, Landroid/view/KeyEvent;->getAction()I */
/* if-ne v2, v8, :cond_a */
/* .line 768 */
v2 = /* invoke-virtual/range {p2 ..p2}, Landroid/view/KeyEvent;->getFlags()I */
/* and-int v2, v2, v21 */
/* if-nez v2, :cond_a */
/* .line 769 */
v2 = this.mMiuiShortcutTriggerHelper;
(( com.android.server.policy.MiuiShortcutTriggerHelper ) v2 ).resetKeyCodeByLongPress ( ); // invoke-virtual {v2}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->resetKeyCodeByLongPress()V
/* .line 772 */
} // :cond_a
v2 = /* invoke-virtual/range {p2 ..p2}, Landroid/view/KeyEvent;->getKeyCode()I */
/* .line 774 */
/* .local v2, "keyCode":I */
/* const/16 v3, 0x52 */
/* if-ne v2, v3, :cond_15 */
/* .line 775 */
/* iget-boolean v3, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z */
if ( v3 != null) { // if-eqz v3, :cond_b
/* .line 776 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Ignoring MENU because mTestModeEnabled = "; // const-string v8, "Ignoring MENU because mTestModeEnabled = "
(( java.lang.StringBuilder ) v3 ).append ( v8 ); // invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v8, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z */
(( java.lang.StringBuilder ) v3 ).append ( v8 ); // invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .major ( v3 );
/* .line 777 */
/* return-wide v17 */
/* .line 780 */
} // :cond_b
v3 = /* invoke-direct {v0, v15}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isLockDeviceWindow(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)Z */
if ( v3 != null) { // if-eqz v3, :cond_c
/* .line 781 */
final String v3 = "device locked, pass MENU to lock window"; // const-string v3, "device locked, pass MENU to lock window"
com.android.server.policy.MiuiInputLog .major ( v3 );
/* .line 782 */
/* return-wide v17 */
/* .line 785 */
} // :cond_c
v3 = this.mMiuiShortcutTriggerHelper;
v3 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v3 ).isPressToAppSwitch ( ); // invoke-virtual {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isPressToAppSwitch()Z
final String v8 = "is show menu"; // const-string v8, "is show menu"
/* if-nez v3, :cond_d */
/* .line 786 */
com.android.server.policy.MiuiInputLog .major ( v8 );
/* .line 787 */
/* return-wide v17 */
/* .line 790 */
} // :cond_d
if ( v9 != null) { // if-eqz v9, :cond_f
/* .line 791 */
v3 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->keyguardOn()Z */
/* if-nez v3, :cond_e */
v3 = this.mMiuiShortcutTriggerHelper;
v3 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v3 ).isPressToAppSwitch ( ); // invoke-virtual {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isPressToAppSwitch()Z
if ( v3 != null) { // if-eqz v3, :cond_e
/* .line 792 */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->cancelPreloadRecentAppsInternal()V */
/* .line 794 */
} // :cond_e
final String v3 = "Ignoring MENU; event canceled or intercepted by long press"; // const-string v3, "Ignoring MENU; event canceled or intercepted by long press"
com.android.server.policy.MiuiInputLog .error ( v3 );
/* .line 795 */
/* const-wide/16 v16, -0x1 */
/* return-wide v16 */
/* .line 799 */
} // :cond_f
/* move-object v3, v15 */
} // .end local v15 # "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
/* .local v3, "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
v15 = this.mMiuiShortcutTriggerHelper;
v15 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v15 ).isPressToAppSwitch ( ); // invoke-virtual {v15}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isPressToAppSwitch()Z
if ( v15 != null) { // if-eqz v15, :cond_14
v15 = /* invoke-virtual/range {p2 ..p2}, Landroid/view/KeyEvent;->getFlags()I */
/* and-int v15, v15, v21 */
if ( v15 != null) { // if-eqz v15, :cond_10
/* move-wide/from16 v24, v4 */
/* .line 805 */
} // :cond_10
v8 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->keyguardOn()Z */
/* if-nez v8, :cond_13 */
/* .line 806 */
if ( v7 != null) { // if-eqz v7, :cond_11
/* .line 807 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendFullScreenStateToTaskSnapshot()V */
/* .line 808 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->preloadRecentApps()V */
/* move-wide/from16 v24, v4 */
/* .line 809 */
} // :cond_11
/* if-nez v6, :cond_12 */
/* .line 811 */
v8 = this.mShortcutOneTrackHelper;
final String v15 = "screen_key_press_app_switch"; // const-string v15, "screen_key_press_app_switch"
/* move-wide/from16 v24, v4 */
} // .end local v4 # "key_not_consumed":J
/* .local v24, "key_not_consumed":J */
final String v4 = "launch_recents"; // const-string v4, "launch_recents"
(( com.android.server.input.shortcut.ShortcutOneTrackHelper ) v8 ).trackShortcutEventTrigger ( v15, v4 ); // invoke-virtual {v8, v15, v4}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V
/* .line 814 */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchRecentPanel()Z */
/* .line 817 */
} // .end local v24 # "key_not_consumed":J
/* .restart local v4 # "key_not_consumed":J */
} // :cond_12
/* move-wide/from16 v24, v4 */
} // .end local v4 # "key_not_consumed":J
/* .restart local v24 # "key_not_consumed":J */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->cancelPreloadRecentAppsInternal()V */
/* .line 805 */
} // .end local v24 # "key_not_consumed":J
/* .restart local v4 # "key_not_consumed":J */
} // :cond_13
/* move-wide/from16 v24, v4 */
/* .line 820 */
} // .end local v4 # "key_not_consumed":J
/* .restart local v24 # "key_not_consumed":J */
} // :goto_3
/* const-wide/16 v4, -0x1 */
/* return-wide v4 */
/* .line 799 */
} // .end local v24 # "key_not_consumed":J
/* .restart local v4 # "key_not_consumed":J */
} // :cond_14
/* move-wide/from16 v24, v4 */
/* .line 801 */
} // .end local v4 # "key_not_consumed":J
/* .restart local v24 # "key_not_consumed":J */
} // :goto_4
com.android.server.policy.MiuiInputLog .major ( v8 );
/* .line 802 */
/* return-wide v17 */
/* .line 824 */
} // .end local v3 # "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
} // .end local v24 # "key_not_consumed":J
/* .restart local v4 # "key_not_consumed":J */
/* .restart local v15 # "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
} // :cond_15
/* move-wide/from16 v24, v4 */
/* move-object v3, v15 */
} // .end local v4 # "key_not_consumed":J
} // .end local v15 # "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
/* .restart local v3 # "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .restart local v24 # "key_not_consumed":J */
int v4 = 3; // const/4 v4, 0x3
/* if-ne v2, v4, :cond_24 */
/* .line 825 */
/* iget-boolean v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z */
if ( v4 != null) { // if-eqz v4, :cond_16
/* .line 826 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Ignoring HOME because mTestModeEnabled = "; // const-string v5, "Ignoring HOME because mTestModeEnabled = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v5, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .major ( v4 );
/* .line 827 */
/* return-wide v17 */
/* .line 830 */
} // :cond_16
v4 = /* invoke-direct {v0, v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isLockDeviceWindow(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)Z */
if ( v4 != null) { // if-eqz v4, :cond_17
/* .line 831 */
final String v4 = "device locked, pass HOME to lock window"; // const-string v4, "device locked, pass HOME to lock window"
com.android.server.policy.MiuiInputLog .major ( v4 );
/* .line 832 */
/* return-wide v17 */
/* .line 835 */
} // :cond_17
if ( v9 != null) { // if-eqz v9, :cond_18
/* .line 836 */
final String v4 = "Ignoring HOME; event canceled or intercepted by long press."; // const-string v4, "Ignoring HOME; event canceled or intercepted by long press."
com.android.server.policy.MiuiInputLog .error ( v4 );
/* .line 837 */
/* const-wide/16 v4, -0x1 */
/* return-wide v4 */
/* .line 840 */
} // :cond_18
/* const-wide/16 v4, -0x1 */
/* if-nez v7, :cond_1e */
/* .line 841 */
/* iget-boolean v15, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeConsumed:Z */
if ( v15 != null) { // if-eqz v15, :cond_19
/* .line 842 */
int v8 = 0; // const/4 v8, 0x0
/* iput-boolean v8, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeConsumed:Z */
/* .line 843 */
/* return-wide v4 */
/* .line 845 */
} // :cond_19
v4 = com.android.server.policy.BaseMiuiPhoneWindowManager.phoneWindowManagerFeature;
v4 = (( com.android.server.policy.PhoneWindowManagerFeatureImpl ) v4 ).isScreenOnFully ( v0 ); // invoke-virtual {v4, v0}, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;->isScreenOnFully(Lcom/android/server/policy/PhoneWindowManager;)Z
if ( v4 != null) { // if-eqz v4, :cond_1d
/* .line 846 */
if ( v11 != null) { // if-eqz v11, :cond_1a
/* .line 848 */
v4 = this.mContext;
/* const-string/jumbo v5, "statusbar" */
(( android.content.Context ) v4 ).getSystemService ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v4, Landroid/app/StatusBarManager; */
/* .line 849 */
/* .local v4, "sbm":Landroid/app/StatusBarManager; */
(( android.app.StatusBarManager ) v4 ).collapsePanels ( ); // invoke-virtual {v4}, Landroid/app/StatusBarManager;->collapsePanels()V
/* .line 851 */
} // .end local v4 # "sbm":Landroid/app/StatusBarManager;
} // :cond_1a
/* iget-boolean v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyguardOnWhenHomeDown:Z */
/* if-nez v4, :cond_1c */
/* .line 852 */
v4 = this.mMiuiShortcutTriggerHelper;
/* iget v4, v4, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDoubleTapOnHomeBehavior:I */
if ( v4 != null) { // if-eqz v4, :cond_1b
/* if-nez v11, :cond_1b */
/* .line 853 */
v4 = this.mHandler;
v5 = this.mHomeDoubleTapTimeoutRunnable;
(( android.os.Handler ) v4 ).removeCallbacks ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 854 */
/* iput-boolean v8, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDoubleTapPending:Z */
/* .line 855 */
v4 = this.mHandler;
v5 = this.mHomeDoubleTapTimeoutRunnable;
/* move/from16 v19, v10 */
/* move/from16 v20, v11 */
} // .end local v10 # "canceled":Z
} // .end local v11 # "keyguardActive":Z
/* .local v19, "canceled":Z */
/* .local v20, "keyguardActive":Z */
/* const-wide/16 v10, 0x12c */
(( android.os.Handler ) v4 ).postDelayed ( v5, v10, v11 ); // invoke-virtual {v4, v5, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 856 */
/* const-wide/16 v4, -0x1 */
/* return-wide v4 */
/* .line 852 */
} // .end local v19 # "canceled":Z
} // .end local v20 # "keyguardActive":Z
/* .restart local v10 # "canceled":Z */
/* .restart local v11 # "keyguardActive":Z */
} // :cond_1b
/* move/from16 v19, v10 */
/* move/from16 v20, v11 */
/* .line 858 */
} // .end local v10 # "canceled":Z
} // .end local v11 # "keyguardActive":Z
/* .restart local v19 # "canceled":Z */
/* .restart local v20 # "keyguardActive":Z */
final String v4 = "before go to home"; // const-string v4, "before go to home"
com.android.server.policy.MiuiInputLog .error ( v4 );
/* .line 859 */
/* new-instance v4, Landroid/content/Intent; */
final String v5 = "com.miui.launch_home_from_hotkey"; // const-string v5, "com.miui.launch_home_from_hotkey"
/* invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 860 */
/* .local v4, "intent":Landroid/content/Intent; */
final String v5 = "miui.permission.USE_INTERNAL_GENERAL_API"; // const-string v5, "miui.permission.USE_INTERNAL_GENERAL_API"
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) v0 ).sendAsyncBroadcast ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
/* .line 861 */
int v5 = 0; // const/4 v5, 0x0
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) v0 ).launchHomeFromHotKey ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchHomeFromHotKey(I)V
/* .line 862 */
} // .end local v4 # "intent":Landroid/content/Intent;
/* .line 863 */
} // .end local v19 # "canceled":Z
} // .end local v20 # "keyguardActive":Z
/* .restart local v10 # "canceled":Z */
/* .restart local v11 # "keyguardActive":Z */
} // :cond_1c
/* move/from16 v19, v10 */
/* move/from16 v20, v11 */
} // .end local v10 # "canceled":Z
} // .end local v11 # "keyguardActive":Z
/* .restart local v19 # "canceled":Z */
/* .restart local v20 # "keyguardActive":Z */
final String v4 = "Ignoring HOME; keyguard is on when first Home down"; // const-string v4, "Ignoring HOME; keyguard is on when first Home down"
com.android.server.policy.MiuiInputLog .major ( v4 );
/* .line 845 */
} // .end local v19 # "canceled":Z
} // .end local v20 # "keyguardActive":Z
/* .restart local v10 # "canceled":Z */
/* .restart local v11 # "keyguardActive":Z */
} // :cond_1d
/* move/from16 v19, v10 */
/* move/from16 v20, v11 */
} // .end local v10 # "canceled":Z
} // .end local v11 # "keyguardActive":Z
/* .restart local v19 # "canceled":Z */
/* .restart local v20 # "keyguardActive":Z */
/* .line 868 */
} // .end local v19 # "canceled":Z
} // .end local v20 # "keyguardActive":Z
/* .restart local v10 # "canceled":Z */
/* .restart local v11 # "keyguardActive":Z */
} // :cond_1e
/* move/from16 v19, v10 */
/* move/from16 v20, v11 */
} // .end local v10 # "canceled":Z
} // .end local v11 # "keyguardActive":Z
/* .restart local v19 # "canceled":Z */
/* .restart local v20 # "keyguardActive":Z */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendFullScreenStateToHome()V */
/* .line 871 */
if ( v3 != null) { // if-eqz v3, :cond_1f
final String v4 = "getAttrs"; // const-string v4, "getAttrs"
int v5 = 0; // const/4 v5, 0x0
/* new-array v10, v5, [Ljava/lang/Object; */
com.android.server.input.ReflectionUtils .callPrivateMethod ( v3,v4,v10 );
/* check-cast v4, Landroid/view/WindowManager$LayoutParams; */
} // :cond_1f
int v4 = 0; // const/4 v4, 0x0
/* .line 872 */
/* .local v4, "attrs":Landroid/view/WindowManager$LayoutParams; */
} // :goto_5
if ( v4 != null) { // if-eqz v4, :cond_21
/* .line 873 */
/* iget v5, v4, Landroid/view/WindowManager$LayoutParams;->type:I */
/* .line 874 */
/* .local v5, "type":I */
/* const/16 v10, 0x7d4 */
/* if-eq v5, v10, :cond_20 */
/* const/16 v10, 0x7d9 */
/* if-ne v5, v10, :cond_21 */
/* .line 877 */
} // :cond_20
/* return-wide v17 */
/* .line 882 */
} // .end local v4 # "attrs":Landroid/view/WindowManager$LayoutParams;
} // .end local v5 # "type":I
} // :cond_21
} // :goto_6
/* if-nez v6, :cond_23 */
/* .line 883 */
/* iget-boolean v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDoubleTapPending:Z */
if ( v4 != null) { // if-eqz v4, :cond_22
/* .line 884 */
int v4 = 0; // const/4 v4, 0x0
/* iput-boolean v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDoubleTapPending:Z */
/* .line 885 */
v4 = this.mHandler;
v5 = this.mHomeDoubleTapTimeoutRunnable;
(( android.os.Handler ) v4 ).removeCallbacks ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 886 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->handleDoubleTapOnHome()V */
/* .line 887 */
} // :cond_22
v4 = this.mMiuiShortcutTriggerHelper;
/* iget v4, v4, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDoubleTapOnHomeBehavior:I */
/* if-ne v4, v8, :cond_23 */
/* .line 889 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->preloadRecentApps()V */
/* .line 892 */
} // :cond_23
} // :goto_7
/* const-wide/16 v10, -0x1 */
/* return-wide v10 */
/* .line 894 */
} // .end local v19 # "canceled":Z
} // .end local v20 # "keyguardActive":Z
/* .restart local v10 # "canceled":Z */
/* .restart local v11 # "keyguardActive":Z */
} // :cond_24
/* move/from16 v19, v10 */
/* move/from16 v20, v11 */
/* const-wide/16 v10, -0x1 */
} // .end local v10 # "canceled":Z
} // .end local v11 # "keyguardActive":Z
/* .restart local v19 # "canceled":Z */
/* .restart local v20 # "keyguardActive":Z */
int v5 = 4; // const/4 v5, 0x4
/* if-ne v2, v5, :cond_25 */
/* .line 895 */
if ( v9 != null) { // if-eqz v9, :cond_25
/* .line 896 */
final String v4 = "Ignoring BACK; event canceled or intercepted by long press."; // const-string v4, "Ignoring BACK; event canceled or intercepted by long press."
com.android.server.policy.MiuiInputLog .error ( v4 );
/* .line 897 */
/* return-wide v10 */
/* .line 900 */
} // :cond_25
/* const/16 v5, 0x19 */
/* if-ne v2, v5, :cond_27 */
/* iget-boolean v5, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyConsumed:Z */
if ( v5 != null) { // if-eqz v5, :cond_27
/* .line 901 */
/* if-nez v7, :cond_26 */
/* .line 902 */
int v4 = 0; // const/4 v4, 0x0
/* iput-boolean v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyConsumed:Z */
/* .line 904 */
} // :cond_26
/* const-wide/16 v4, -0x1 */
/* return-wide v4 */
/* .line 907 */
} // :cond_27
/* const/16 v5, 0x18 */
/* if-ne v2, v5, :cond_29 */
/* iget-boolean v5, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyConsumed:Z */
if ( v5 != null) { // if-eqz v5, :cond_29
/* .line 908 */
/* if-nez v7, :cond_28 */
/* .line 909 */
int v4 = 0; // const/4 v4, 0x0
/* iput-boolean v4, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyConsumed:Z */
/* .line 911 */
} // :cond_28
/* const-wide/16 v4, -0x1 */
/* return-wide v4 */
/* .line 914 */
} // :cond_29
v5 = this.mMiuiStylusShortcutManager;
if ( v5 != null) { // if-eqz v5, :cond_2a
/* .line 915 */
v5 = (( com.miui.server.input.stylus.MiuiStylusShortcutManager ) v5 ).needInterceptBeforeDispatching ( v1 ); // invoke-virtual {v5, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->needInterceptBeforeDispatching(Landroid/view/KeyEvent;)Z
if ( v5 != null) { // if-eqz v5, :cond_2a
/* .line 916 */
v4 = this.mMiuiStylusShortcutManager;
(( com.miui.server.input.stylus.MiuiStylusShortcutManager ) v4 ).getDelayTime ( v1 ); // invoke-virtual {v4, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->getDelayTime(Landroid/view/KeyEvent;)J
/* move-result-wide v4 */
/* return-wide v4 */
/* .line 919 */
} // :cond_2a
/* if-ne v12, v4, :cond_2b */
/* .line 920 */
final String v4 = "Skip aosp key\'s logic"; // const-string v4, "Skip aosp key\'s logic"
com.android.server.policy.MiuiInputLog .defaults ( v4 );
/* .line 921 */
/* return-wide v17 */
/* .line 923 */
} // :cond_2b
/* invoke-super/range {p0 ..p3}, Lcom/android/server/policy/PhoneWindowManager;->interceptKeyBeforeDispatching(Landroid/os/IBinder;Landroid/view/KeyEvent;I)J */
/* move-result-wide v4 */
/* return-wide v4 */
} // .end method
protected Integer interceptKeyBeforeQueueingInternal ( android.view.KeyEvent p0, Integer p1, Boolean p2 ) {
/* .locals 22 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "policyFlags" # I */
/* .param p3, "isScreenOn" # Z */
/* .line 1238 */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p1 */
/* move/from16 v9, p2 */
/* move/from16 v10, p3 */
/* iget-boolean v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemBooted:Z */
int v11 = 0; // const/4 v11, 0x0
/* if-nez v0, :cond_0 */
/* .line 1240 */
/* .line 1243 */
} // :cond_0
v12 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getKeyCode()I */
/* .line 1244 */
/* .local v12, "keyCode":I */
v0 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getAction()I */
int v13 = 1; // const/4 v13, 0x1
/* if-nez v0, :cond_1 */
/* move v0, v13 */
} // :cond_1
/* move v0, v11 */
} // :goto_0
/* move v14, v0 */
/* .line 1245 */
/* .local v14, "down":Z */
v15 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getRepeatCount()I */
/* .line 1246 */
/* .local v15, "repeatCount":I */
/* const/high16 v0, 0x1000000 */
/* and-int/2addr v0, v9 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* move v0, v13 */
} // :cond_2
/* move v0, v11 */
} // :goto_1
/* move/from16 v16, v0 */
/* .line 1247 */
/* .local v16, "isInjected":Z */
v0 = this.mMiuiKeyguardDelegate;
if ( v0 != null) { // if-eqz v0, :cond_4
if ( v10 != null) { // if-eqz v10, :cond_3
/* .line 1248 */
v0 = (( com.android.server.policy.MiuiKeyguardServiceDelegate ) v0 ).isShowingAndNotHidden ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z
if ( v0 != null) { // if-eqz v0, :cond_4
} // :cond_3
/* nop */
/* .line 1249 */
v0 = (( com.android.server.policy.MiuiKeyguardServiceDelegate ) v0 ).isShowing ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowing()Z
if ( v0 != null) { // if-eqz v0, :cond_4
} // :goto_2
/* move v0, v13 */
} // :cond_4
/* move v0, v11 */
} // :goto_3
/* move v6, v0 */
/* .line 1251 */
/* .local v6, "keyguardActive":Z */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "keyCode:"; // const-string v1, "keyCode:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " down:"; // const-string v1, " down:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " eventTime:"; // const-string v1, " eventTime:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1252 */
/* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getEventTime()J */
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " downTime:"; // const-string v1, " downTime:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getDownTime()J */
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " policyFlags:"; // const-string v1, " policyFlags:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1253 */
/* invoke-static/range {p2 ..p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " flags:"; // const-string v1, " flags:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1254 */
v1 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getFlags()I */
java.lang.Integer .toHexString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " metaState:"; // const-string v1, " metaState:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1255 */
v1 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getMetaState()I */
java.lang.Integer .toHexString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " deviceId:"; // const-string v1, " deviceId:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1256 */
v1 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getDeviceId()I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " isScreenOn:"; // const-string v1, " isScreenOn:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " keyguardActive:"; // const-string v1, " keyguardActive:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " repeatCount:"; // const-string v1, " repeatCount:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1251 */
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 1258 */
v0 = this.mMiuiKeyShortcutRuleManager;
/* const/16 v5, 0x1a */
(( com.android.server.policy.MiuiKeyShortcutRuleManager ) v0 ).getSingleKeyRule ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getSingleKeyRule(I)Lcom/android/server/policy/MiuiSingleKeyRule;
/* .line 1260 */
/* .local v17, "miuiSingleKeyRule":Lcom/android/server/policy/MiuiSingleKeyRule; */
if ( v17 != null) { // if-eqz v17, :cond_5
/* .line 1261 */
/* invoke-virtual/range {v17 ..v17}, Lcom/android/server/policy/MiuiSingleKeyRule;->getLongPressTimeoutMs()J */
/* move-result-wide v0 */
android.os.AnrMonitor .screenHangMonitor ( v12,v14,v0,v1 );
/* .line 1266 */
} // :cond_5
v0 = this.mMiuiKeyInterceptExtend;
v0 = (( com.android.server.policy.MiuiKeyInterceptExtend ) v0 ).interceptMiuiKeyboard ( v8, v10 ); // invoke-virtual {v0, v8, v10}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->interceptMiuiKeyboard(Landroid/view/KeyEvent;Z)Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 1267 */
/* .line 1270 */
} // :cond_6
v0 = this.mMiuiKeyInterceptExtend;
v1 = this.mFocusedWindow;
v4 = (( com.android.server.policy.MiuiKeyInterceptExtend ) v0 ).getKeyInterceptTypeBeforeQueueing ( v8, v9, v1 ); // invoke-virtual {v0, v8, v9, v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getKeyInterceptTypeBeforeQueueing(Landroid/view/KeyEvent;ILcom/android/server/policy/WindowManagerPolicy$WindowState;)I
/* .line 1271 */
/* .local v4, "interceptType":I */
/* if-ne v4, v13, :cond_7 */
/* .line 1272 */
final String v0 = "pass the key to app!"; // const-string v0, "pass the key to app!"
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 1273 */
/* .line 1274 */
} // :cond_7
int v0 = 2; // const/4 v0, 0x2
/* if-ne v4, v0, :cond_8 */
/* .line 1275 */
/* const-string/jumbo v0, "send the key to aosp!" */
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 1276 */
v0 = /* invoke-virtual/range {p0 ..p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->callSuperInterceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I */
/* .line 1279 */
} // :cond_8
/* const/16 v0, 0x52 */
int v1 = 4; // const/4 v1, 0x4
int v3 = 3; // const/4 v3, 0x3
/* if-eq v12, v0, :cond_9 */
/* if-eq v12, v1, :cond_9 */
/* if-eq v12, v3, :cond_9 */
/* if-eq v12, v5, :cond_9 */
/* .line 1281 */
/* invoke-direct/range {p0 ..p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->notifyPowerKeeperKeyEvent(Landroid/view/KeyEvent;)V */
/* .line 1283 */
} // :cond_9
if ( v14 != null) { // if-eqz v14, :cond_a
/* .line 1284 */
/* invoke-direct/range {p0 ..p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->trackDumpLogKeyCode(Landroid/view/KeyEvent;)V */
/* .line 1288 */
} // :cond_a
v0 = miui.enterprise.ApplicationHelperStub .getInstance ( );
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 1289 */
miui.enterprise.ApplicationHelperStub .getInstance ( );
/* .line 1293 */
} // :cond_b
/* if-nez v15, :cond_c */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->keyguardOn()Z */
/* if-nez v0, :cond_c */
v0 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isMiPad()Z */
if ( v0 != null) { // if-eqz v0, :cond_c
/* .line 1294 */
/* invoke-direct/range {p0 ..p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->handleMetaKey(Landroid/view/KeyEvent;)V */
/* .line 1297 */
} // :cond_c
v0 = /* invoke-direct/range {p0 ..p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isTrackInputEvenForScreenRecorder(Landroid/view/KeyEvent;)Z */
if ( v0 != null) { // if-eqz v0, :cond_d
/* .line 1298 */
/* invoke-direct/range {p0 ..p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendKeyEventBroadcast(Landroid/view/KeyEvent;)V */
/* .line 1301 */
} // :cond_d
v0 = /* invoke-direct/range {p0 ..p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isTrackInputEventForVoiceAssist(Landroid/view/KeyEvent;)Z */
if ( v0 != null) { // if-eqz v0, :cond_e
/* .line 1302 */
/* invoke-direct/range {p0 ..p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendVoiceAssistKeyEventBroadcast(Landroid/view/KeyEvent;)V */
/* .line 1305 */
} // :cond_e
/* if-ne v12, v1, :cond_f */
/* .line 1306 */
/* invoke-direct/range {p0 ..p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendBackKeyEventBroadcast(Landroid/view/KeyEvent;)V */
/* .line 1309 */
} // :cond_f
v0 = /* invoke-direct/range {p0 ..p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->shouldInterceptKey(Landroid/view/KeyEvent;IZ)Z */
if ( v0 != null) { // if-eqz v0, :cond_10
/* .line 1310 */
/* .line 1313 */
} // :cond_10
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->interceptPowerKeyByFingerPrintKey()Z */
if ( v0 != null) { // if-eqz v0, :cond_11
/* .line 1314 */
/* .line 1317 */
} // :cond_11
v0 = /* invoke-virtual/range {p0 ..p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isFingerPrintKey(Landroid/view/KeyEvent;)Z */
if ( v0 != null) { // if-eqz v0, :cond_12
/* .line 1318 */
v0 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v7 ).processFingerprintNavigationEvent ( v8, v10 ); // invoke-virtual {v7, v8, v10}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->processFingerprintNavigationEvent(Landroid/view/KeyEvent;Z)I
/* .line 1321 */
} // :cond_12
/* const/16 v0, 0x18 */
/* if-ne v12, v0, :cond_13 */
/* if-nez v14, :cond_13 */
v1 = this.mProximitySensor;
if ( v1 != null) { // if-eqz v1, :cond_13
/* .line 1322 */
v1 = (( com.android.server.policy.MiuiScreenOnProximityLock ) v1 ).isHeld ( ); // invoke-virtual {v1}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->isHeld()Z
if ( v1 != null) { // if-eqz v1, :cond_13
/* .line 1323 */
/* invoke-direct {v7, v11}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->releaseScreenOnProximitySensor(Z)V */
/* .line 1326 */
} // :cond_13
/* if-ne v12, v5, :cond_16 */
/* .line 1327 */
/* iget-boolean v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_14
/* .line 1328 */
final String v0 = "Ignoring POWER because mTestModeEnabled is true"; // const-string v0, "Ignoring POWER because mTestModeEnabled is true"
com.android.server.policy.MiuiInputLog .major ( v0 );
/* .line 1329 */
/* .line 1332 */
} // :cond_14
if ( v14 != null) { // if-eqz v14, :cond_15
v1 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getRepeatCount()I */
/* if-nez v1, :cond_15 */
/* .line 1333 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->postPowerLongPress()V */
/* .line 1335 */
} // :cond_15
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->removePowerLongPress()V */
/* .line 1338 */
} // :cond_16
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->removePowerLongPress()V */
/* .line 1341 */
} // :goto_4
/* const/16 v1, 0x2b1 */
/* move/from16 v18, v6 */
} // .end local v6 # "keyguardActive":Z
/* .local v18, "keyguardActive":Z */
/* const-wide/16 v5, 0x1388 */
/* if-ne v12, v1, :cond_18 */
/* iget-boolean v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z */
/* if-nez v1, :cond_18 */
/* .line 1342 */
/* sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_17
/* .line 1343 */
/* invoke-direct {v7, v8, v14}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->handleAiKeyEvent(Landroid/view/KeyEvent;Z)V */
/* .line 1344 */
/* invoke-virtual/range {p0 ..p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->callSuperInterceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I */
/* .line 1346 */
} // :cond_17
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1347 */
/* .local v0, "voiceBundle":Landroid/os/Bundle; */
final String v1 = "extra_key_action"; // const-string v1, "extra_key_action"
v2 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getAction()I */
(( android.os.Bundle ) v0 ).putInt ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1348 */
/* nop */
/* .line 1349 */
/* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getEventTime()J */
/* move-result-wide v1 */
/* .line 1348 */
final String v3 = "extra_key_event_time"; // const-string v3, "extra_key_event_time"
(( android.os.Bundle ) v0 ).putLong ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 1350 */
v1 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v1 );
final String v2 = "launch_voice_assistant"; // const-string v2, "launch_voice_assistant"
final String v3 = "ai_key"; // const-string v3, "ai_key"
(( com.miui.server.input.util.ShortCutActionsUtils ) v1 ).triggerFunction ( v2, v3, v0, v13 ); // invoke-virtual {v1, v2, v3, v0, v13}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 1353 */
v1 = this.mAiKeyWakeLock;
(( android.os.PowerManager$WakeLock ) v1 ).acquire ( v5, v6 ); // invoke-virtual {v1, v5, v6}, Landroid/os/PowerManager$WakeLock;->acquire(J)V
/* .line 1355 */
} // .end local v0 # "voiceBundle":Landroid/os/Bundle;
} // :goto_5
/* .line 1359 */
} // :cond_18
/* const/16 v1, 0x103 */
/* if-ne v12, v1, :cond_19 */
/* move v1, v13 */
} // :cond_19
/* move v1, v11 */
} // :goto_6
/* and-int/2addr v1, v14 */
if ( v1 != null) { // if-eqz v1, :cond_1a
/* .line 1360 */
v1 = this.mFocusedWindow;
final String v2 = "com.android.camera"; // const-string v2, "com.android.camera"
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1a */
/* .line 1362 */
try { // :try_start_0
v0 = this.mHelpKeyWakeLock;
(( android.os.PowerManager$WakeLock ) v0 ).acquire ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Landroid/os/PowerManager$WakeLock;->acquire(J)V
/* .line 1363 */
v0 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
final String v1 = "launch_camera"; // const-string v1, "launch_camera"
/* const-string/jumbo v2, "stabilizer" */
int v3 = 0; // const/4 v3, 0x0
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( v1, v2, v3, v13 ); // invoke-virtual {v0, v1, v2, v3, v13}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* :try_end_0 */
/* .catch Landroid/content/ActivityNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1368 */
/* .line 1366 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1367 */
/* .local v0, "e":Landroid/content/ActivityNotFoundException; */
final String v1 = "mCameraIntent problem"; // const-string v1, "mCameraIntent problem"
com.android.server.policy.MiuiInputLog .error ( v1,v0 );
/* .line 1369 */
} // .end local v0 # "e":Landroid/content/ActivityNotFoundException;
} // :goto_7
/* .line 1373 */
} // :cond_1a
/* if-ne v12, v3, :cond_1c */
/* .line 1374 */
if ( v14 != null) { // if-eqz v14, :cond_1b
/* if-nez v15, :cond_1b */
/* .line 1375 */
v1 = this.mMiuiKeyguardDelegate;
v1 = (( com.android.server.policy.MiuiKeyguardServiceDelegate ) v1 ).isShowingAndNotHidden ( ); // invoke-virtual {v1}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z
/* iput-boolean v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyguardOnWhenHomeDown:Z */
/* .line 1376 */
/* iget-boolean v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mDpadCenterDown:Z */
if ( v1 != null) { // if-eqz v1, :cond_1b
/* .line 1377 */
/* iput-boolean v13, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHomeDownAfterDpCenter:Z */
/* .line 1380 */
} // :cond_1b
/* iget-boolean v1, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFrontFingerprintSensor:Z */
if ( v1 != null) { // if-eqz v1, :cond_1c
v1 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->inFingerprintEnrolling()Z */
if ( v1 != null) { // if-eqz v1, :cond_1c
/* .line 1381 */
/* .line 1385 */
} // :cond_1c
v1 = this.mKeyCombinationManager;
v1 = (( com.android.server.policy.KeyCombinationManager ) v1 ).shouldConsumedKey ( v8 ); // invoke-virtual {v1, v8}, Lcom/android/server/policy/KeyCombinationManager;->shouldConsumedKey(Landroid/view/KeyEvent;)Z
if ( v1 != null) { // if-eqz v1, :cond_1d
/* .line 1386 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "comsumed key,keycode="; // const-string v1, "comsumed key,keycode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getKeyCode()I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " dowmTime="; // const-string v1, " dowmTime="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1387 */
/* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getDownTime()J */
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1386 */
com.android.server.policy.MiuiInputLog .major ( v0 );
/* .line 1388 */
v0 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v7 ).intercept ( v8, v9, v10, v11 ); // invoke-virtual {v7, v8, v9, v10, v11}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I
/* .line 1391 */
} // :cond_1d
/* const/16 v1, 0x19 */
/* const-string/jumbo v6, "zhuque" */
/* if-ne v12, v1, :cond_21 */
/* .line 1392 */
if ( v14 != null) { // if-eqz v14, :cond_20
/* .line 1393 */
if ( v10 != null) { // if-eqz v10, :cond_1f
/* iget-boolean v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyPressed:Z */
/* if-nez v0, :cond_1f */
/* .line 1394 */
v0 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getFlags()I */
/* and-int/lit16 v0, v0, 0x400 */
/* if-nez v0, :cond_1e */
/* .line 1395 */
/* iput-boolean v13, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyPressed:Z */
/* .line 1396 */
/* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getDownTime()J */
/* move-result-wide v0 */
/* iput-wide v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyTime:J */
/* .line 1397 */
/* iput-boolean v11, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyConsumed:Z */
/* .line 1398 */
/* move/from16 v5, v18 */
} // .end local v18 # "keyguardActive":Z
/* .local v5, "keyguardActive":Z */
/* invoke-direct {v7, v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->interceptAccessibilityShortcutChord(Z)V */
/* .line 1394 */
} // .end local v5 # "keyguardActive":Z
/* .restart local v18 # "keyguardActive":Z */
} // :cond_1e
/* move/from16 v5, v18 */
} // .end local v18 # "keyguardActive":Z
/* .restart local v5 # "keyguardActive":Z */
/* .line 1393 */
} // .end local v5 # "keyguardActive":Z
/* .restart local v18 # "keyguardActive":Z */
} // :cond_1f
/* move/from16 v5, v18 */
} // .end local v18 # "keyguardActive":Z
/* .restart local v5 # "keyguardActive":Z */
/* .line 1401 */
} // .end local v5 # "keyguardActive":Z
/* .restart local v18 # "keyguardActive":Z */
} // :cond_20
/* move/from16 v5, v18 */
} // .end local v18 # "keyguardActive":Z
/* .restart local v5 # "keyguardActive":Z */
/* iput-boolean v11, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeDownKeyPressed:Z */
/* .line 1402 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->cancelPendingAccessibilityShortcutAction()V */
/* .line 1404 */
} // .end local v5 # "keyguardActive":Z
/* .restart local v18 # "keyguardActive":Z */
} // :cond_21
/* move/from16 v5, v18 */
} // .end local v18 # "keyguardActive":Z
/* .restart local v5 # "keyguardActive":Z */
/* if-ne v12, v0, :cond_25 */
/* .line 1405 */
if ( v14 != null) { // if-eqz v14, :cond_24
/* .line 1406 */
if ( v10 != null) { // if-eqz v10, :cond_22
/* iget-boolean v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyPressed:Z */
/* if-nez v0, :cond_22 */
/* .line 1407 */
v0 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getFlags()I */
/* and-int/lit16 v0, v0, 0x400 */
/* if-nez v0, :cond_22 */
/* .line 1408 */
/* iput-boolean v13, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyPressed:Z */
/* .line 1409 */
/* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getDownTime()J */
/* move-result-wide v0 */
/* iput-wide v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyTime:J */
/* .line 1410 */
/* iput-boolean v11, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyConsumed:Z */
/* .line 1411 */
/* invoke-direct {v7, v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->interceptAccessibilityShortcutChord(Z)V */
/* .line 1413 */
} // :cond_22
v0 = miui.os.Build.DEVICE;
v0 = (( java.lang.String ) v6 ).equals ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_25
/* .line 1414 */
v0 = this.mVolumeKeyUpWakeLock;
v0 = (( android.os.PowerManager$WakeLock ) v0 ).isHeld ( ); // invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z
if ( v0 != null) { // if-eqz v0, :cond_23
/* .line 1415 */
v0 = this.mVolumeKeyUpWakeLock;
(( android.os.PowerManager$WakeLock ) v0 ).release ( ); // invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
/* .line 1417 */
} // :cond_23
v0 = this.mVolumeKeyUpWakeLock;
/* const-wide/16 v1, 0x2710 */
(( android.os.PowerManager$WakeLock ) v0 ).acquire ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V
/* .line 1418 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->postVolumeUpLongPress()V */
/* .line 1421 */
} // :cond_24
/* iput-boolean v11, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mVolumeUpKeyPressed:Z */
/* .line 1422 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->cancelPendingAccessibilityShortcutAction()V */
/* .line 1423 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->removeVolumeUpLongPress()V */
/* .line 1427 */
} // :cond_25
} // :goto_8
v0 = this.mProximitySensor;
if ( v0 != null) { // if-eqz v0, :cond_26
v1 = com.android.server.policy.BaseMiuiPhoneWindowManager.phoneWindowManagerFeature;
v1 = (( com.android.server.policy.PhoneWindowManagerFeatureImpl ) v1 ).isScreenOnFully ( v7 ); // invoke-virtual {v1, v7}, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;->isScreenOnFully(Lcom/android/server/policy/PhoneWindowManager;)Z
v0 = (( com.android.server.policy.MiuiScreenOnProximityLock ) v0 ).shouldBeBlocked ( v1, v8 ); // invoke-virtual {v0, v1, v8}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->shouldBeBlocked(ZLandroid/view/KeyEvent;)Z
if ( v0 != null) { // if-eqz v0, :cond_26
/* .line 1428 */
v0 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v7 ).intercept ( v8, v9, v10, v11 ); // invoke-virtual {v7, v8, v9, v10, v11}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I
/* .line 1431 */
} // :cond_26
if ( v10 != null) { // if-eqz v10, :cond_28
v1 = this.mAutoDisableScreenButtonsManager;
v0 = this.mMiuiShortcutTriggerHelper;
/* .line 1432 */
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).isSingleKeyUse ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isSingleKeyUse()Z
v2 = this.mSmartCoverManager;
/* .line 1433 */
v2 = (( miui.util.SmartCoverManager ) v2 ).getSmartCoverLidOpen ( ); // invoke-virtual {v2}, Lmiui/util/SmartCoverManager;->getSmartCoverLidOpen()Z
/* if-nez v2, :cond_27 */
v2 = this.mDefaultDisplayPolicy;
/* .line 1434 */
v2 = (( com.android.server.wm.DisplayPolicy ) v2 ).getLidState ( ); // invoke-virtual {v2}, Lcom/android/server/wm/DisplayPolicy;->getLidState()I
/* if-nez v2, :cond_27 */
/* move/from16 v18, v13 */
} // :cond_27
/* move/from16 v18, v11 */
/* .line 1431 */
} // :goto_9
/* move v2, v12 */
/* move v3, v14 */
/* move/from16 v20, v4 */
} // .end local v4 # "interceptType":I
/* .local v20, "interceptType":I */
/* move v4, v0 */
/* move/from16 v19, v5 */
/* const/16 v0, 0x1a */
} // .end local v5 # "keyguardActive":Z
/* .local v19, "keyguardActive":Z */
/* move/from16 v5, v18 */
/* move-object/from16 v21, v6 */
/* move/from16 v13, v19 */
} // .end local v19 # "keyguardActive":Z
/* .local v13, "keyguardActive":Z */
/* move-object/from16 v6, p1 */
v1 = /* invoke-virtual/range {v1 ..v6}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->handleDisableButtons(IZZZLandroid/view/KeyEvent;)Z */
if ( v1 != null) { // if-eqz v1, :cond_29
/* .line 1436 */
/* .line 1431 */
} // .end local v13 # "keyguardActive":Z
} // .end local v20 # "interceptType":I
/* .restart local v4 # "interceptType":I */
/* .restart local v5 # "keyguardActive":Z */
} // :cond_28
/* move/from16 v20, v4 */
/* move v13, v5 */
/* move-object/from16 v21, v6 */
/* const/16 v0, 0x1a */
/* .line 1439 */
} // .end local v4 # "interceptType":I
} // .end local v5 # "keyguardActive":Z
/* .restart local v13 # "keyguardActive":Z */
/* .restart local v20 # "interceptType":I */
} // :cond_29
/* const/16 v1, 0xe0 */
/* if-ne v12, v1, :cond_2a */
/* if-nez v10, :cond_2a */
if ( v13 != null) { // if-eqz v13, :cond_2a
v2 = this.mProximitySensor;
if ( v2 != null) { // if-eqz v2, :cond_2a
/* .line 1442 */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->registerProximitySensor()V */
/* .line 1445 */
} // :cond_2a
/* if-ne v12, v1, :cond_2b */
/* if-nez v10, :cond_2b */
/* .line 1446 */
com.miui.server.input.PadManager .getInstance ( );
v1 = (( com.miui.server.input.PadManager ) v1 ).padLidInterceptWakeKey ( v8 ); // invoke-virtual {v1, v8}, Lcom/miui/server/input/PadManager;->padLidInterceptWakeKey(Landroid/view/KeyEvent;)Z
if ( v1 != null) { // if-eqz v1, :cond_2b
/* .line 1448 */
/* .line 1452 */
} // :cond_2b
v1 = this.mMiuiKeyShortcutRuleManager;
(( com.android.server.policy.MiuiKeyShortcutRuleManager ) v1 ).updatePolicyFlag ( v9 ); // invoke-virtual {v1, v9}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->updatePolicyFlag(I)V
/* .line 1454 */
/* invoke-direct {v7, v10, v12, v14, v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->streetSnap(ZIZLandroid/view/KeyEvent;)V */
/* .line 1457 */
v1 = this.mMiuiFingerPrintTapListener;
v1 = (( com.miui.server.input.MiuiFingerPrintTapListener ) v1 ).shouldInterceptSlideFpTapKey ( v8, v10, v13 ); // invoke-virtual {v1, v8, v10, v13}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->shouldInterceptSlideFpTapKey(Landroid/view/KeyEvent;ZZ)Z
if ( v1 != null) { // if-eqz v1, :cond_2c
/* .line 1458 */
/* .line 1461 */
} // :cond_2c
v1 = this.mMiuiStylusShortcutManager;
if ( v1 != null) { // if-eqz v1, :cond_2d
v1 = (( com.miui.server.input.stylus.MiuiStylusShortcutManager ) v1 ).shouldInterceptKey ( v8 ); // invoke-virtual {v1, v8}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->shouldInterceptKey(Landroid/view/KeyEvent;)Z
if ( v1 != null) { // if-eqz v1, :cond_2d
/* .line 1463 */
/* invoke-direct/range {p0 ..p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->cancelEventAndCallSuperQueueing(Landroid/view/KeyEvent;IZ)V */
/* .line 1464 */
/* .line 1467 */
} // :cond_2d
/* if-nez v10, :cond_32 */
if ( v16 != null) { // if-eqz v16, :cond_2e
/* .line 1473 */
} // :cond_2e
int v1 = 1; // const/4 v1, 0x1
/* .line 1474 */
/* .local v1, "isWakeKey":Z */
int v2 = 0; // const/4 v2, 0x0
/* .line 1477 */
/* .local v2, "allowToWake":Z */
/* sparse-switch v12, :sswitch_data_0 */
/* .line 1487 */
int v1 = 0; // const/4 v1, 0x0
/* .line 1479 */
/* :sswitch_0 */
/* iget-boolean v2, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackballWakeScreen:Z */
/* .line 1480 */
/* .line 1483 */
/* :sswitch_1 */
/* iget-boolean v2, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCameraKeyWakeScreen:Z */
/* .line 1484 */
/* nop */
/* .line 1490 */
} // :goto_a
if ( v1 != null) { // if-eqz v1, :cond_33
/* .line 1491 */
if ( v2 != null) { // if-eqz v2, :cond_31
/* .line 1492 */
if ( v14 != null) { // if-eqz v14, :cond_2f
/* .line 1494 */
v0 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v7 ).intercept ( v8, v9, v10, v11 ); // invoke-virtual {v7, v8, v9, v10, v11}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I
/* .line 1497 */
} // :cond_2f
if ( v13 != null) { // if-eqz v13, :cond_30
/* .line 1498 */
v3 = this.mMiuiKeyguardDelegate;
(( com.android.server.policy.MiuiKeyguardServiceDelegate ) v3 ).onWakeKeyWhenKeyguardShowingTq ( v0, v11 ); // invoke-virtual {v3, v0, v11}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->onWakeKeyWhenKeyguardShowingTq(IZ)Z
/* .line 1500 */
} // :cond_30
int v0 = 1; // const/4 v0, 0x1
v0 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v7 ).intercept ( v8, v9, v10, v0 ); // invoke-virtual {v7, v8, v9, v10, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I
/* .line 1503 */
} // :cond_31
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getWakePolicyFlag()I */
/* not-int v0, v0 */
/* and-int/2addr v0, v9 */
} // .end local p2 # "policyFlags":I
/* .local v0, "policyFlags":I */
/* .line 1469 */
} // .end local v0 # "policyFlags":I
} // .end local v1 # "isWakeKey":Z
} // .end local v2 # "allowToWake":Z
/* .restart local p2 # "policyFlags":I */
} // :cond_32
} // :goto_b
/* iget-boolean v0, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCameraKeyWakeScreen:Z */
if ( v0 != null) { // if-eqz v0, :cond_33
if ( v13 != null) { // if-eqz v13, :cond_33
/* const/16 v0, 0x1b */
/* if-ne v12, v0, :cond_33 */
/* if-nez v14, :cond_33 */
/* .line 1470 */
int v0 = -1; // const/4 v0, -0x1
v0 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v7 ).intercept ( v8, v9, v10, v0 ); // invoke-virtual {v7, v8, v9, v10, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I
/* .line 1507 */
} // :cond_33
/* move v0, v9 */
} // .end local p2 # "policyFlags":I
/* .restart local v0 # "policyFlags":I */
} // :goto_c
v1 = /* invoke-direct {v7, v12, v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->shouldInterceptHeadSetHookKey(ILandroid/view/KeyEvent;)Z */
if ( v1 != null) { // if-eqz v1, :cond_34
/* .line 1508 */
/* .line 1513 */
} // :cond_34
v1 = com.android.server.policy.BaseMiuiPhoneWindowManager.mPhoneProduct;
/* move-object/from16 v2, v21 */
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_35
/* .line 1514 */
final String v1 = "WindowManager"; // const-string v1, "WindowManager"
final String v2 = " sendOthersBroadcast keyevent"; // const-string v2, " sendOthersBroadcast keyevent"
android.util.Log .d ( v1,v2 );
/* .line 1515 */
int v2 = 1; // const/4 v2, 0x1
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* move-object/from16 v1, p0 */
/* move v5, v12 */
/* move-object/from16 v6, p1 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendOthersBroadcast(ZZZILandroid/view/KeyEvent;)Z */
/* .line 1518 */
} // :cond_35
/* move-object/from16 v1, p0 */
/* move v2, v14 */
/* move/from16 v3, p3 */
/* move v4, v13 */
/* move v5, v12 */
/* move-object/from16 v6, p1 */
v1 = /* invoke-direct/range {v1 ..v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendOthersBroadcast(ZZZILandroid/view/KeyEvent;)Z */
if ( v1 != null) { // if-eqz v1, :cond_36
/* .line 1519 */
v1 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v7 ).intercept ( v8, v0, v10, v11 ); // invoke-virtual {v7, v8, v0, v10, v11}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I
/* .line 1524 */
} // :cond_36
} // :goto_d
/* invoke-direct {v7, v0, v12, v14, v15}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->playSoundEffect(IIZI)V */
/* .line 1525 */
/* move/from16 v1, v20 */
int v2 = 3; // const/4 v2, 0x3
} // .end local v20 # "interceptType":I
/* .local v1, "interceptType":I */
/* if-ne v1, v2, :cond_39 */
/* .line 1526 */
final String v2 = "Skip aosp key\'s logic"; // const-string v2, "Skip aosp key\'s logic"
com.android.server.policy.MiuiInputLog .defaults ( v2 );
/* .line 1527 */
v2 = this.mDefaultDisplay;
v2 = (( android.view.Display ) v2 ).getState ( ); // invoke-virtual {v2}, Landroid/view/Display;->getState()I
v2 = android.view.Display .isOnState ( v2 );
/* .line 1528 */
/* .local v2, "isDefaultDisplayOn":Z */
/* const/high16 v3, 0x20000000 */
/* and-int/2addr v3, v0 */
if ( v3 != null) { // if-eqz v3, :cond_37
if ( v2 != null) { // if-eqz v2, :cond_37
int v11 = 1; // const/4 v11, 0x1
} // :cond_37
/* move v3, v11 */
/* .line 1530 */
/* .local v3, "interactiveAndOn":Z */
v4 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getFlags()I */
/* and-int/lit16 v4, v4, 0x400 */
/* if-nez v4, :cond_38 */
v4 = this.mKeyCombinationManager;
/* .line 1531 */
v4 = (( com.android.server.policy.KeyCombinationManager ) v4 ).interceptKey ( v8, v3 ); // invoke-virtual {v4, v8, v3}, Lcom/android/server/policy/KeyCombinationManager;->interceptKey(Landroid/view/KeyEvent;Z)Z
if ( v4 != null) { // if-eqz v4, :cond_38
/* .line 1532 */
v4 = this.mSingleKeyGestureDetector;
(( com.android.server.policy.SingleKeyGestureDetector ) v4 ).reset ( ); // invoke-virtual {v4}, Lcom/android/server/policy/SingleKeyGestureDetector;->reset()V
/* .line 1534 */
} // :cond_38
int v4 = 1; // const/4 v4, 0x1
/* .line 1536 */
} // .end local v2 # "isDefaultDisplayOn":Z
} // .end local v3 # "interactiveAndOn":Z
} // :cond_39
v2 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v7 ).callSuperInterceptKeyBeforeQueueing ( v8, v0, v10 ); // invoke-virtual {v7, v8, v0, v10}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->callSuperInterceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1b -> :sswitch_1 */
/* 0x110 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
protected abstract Boolean interceptPowerKeyByFingerPrintKey ( ) {
} // .end method
protected abstract Boolean isFingerPrintKey ( android.view.KeyEvent p0 ) {
} // .end method
protected Boolean isInLockTaskMode ( ) {
/* .locals 1 */
/* .line 966 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isKeyGuardNotActive ( ) {
/* .locals 1 */
/* .line 2833 */
v0 = this.mMiuiKeyguardDelegate;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2834 */
v0 = (( com.android.server.policy.MiuiKeyguardServiceDelegate ) v0 ).isShowingAndNotHidden ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 2833 */
} // :goto_0
} // .end method
protected abstract Boolean isScreenOnInternal ( ) {
} // .end method
public Boolean launchAssistAction ( java.lang.String p0, android.os.Bundle p1 ) {
/* .locals 2 */
/* .param p1, "hint" # Ljava/lang/String; */
/* .param p2, "args" # Landroid/os/Bundle; */
/* .line 2099 */
final String v0 = "assist"; // const-string v0, "assist"
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).sendCloseSystemWindows ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendCloseSystemWindows(Ljava/lang/String;)V
/* .line 2100 */
v0 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).isUserSetupComplete ( ); // invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isUserSetupComplete()Z
/* if-nez v0, :cond_0 */
/* .line 2102 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2104 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v0 ).getConfiguration ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
/* iget v0, v0, Landroid/content/res/Configuration;->uiMode:I */
/* and-int/lit8 v0, v0, 0xf */
int v1 = 4; // const/4 v1, 0x4
/* if-ne v0, v1, :cond_1 */
/* .line 2107 */
v0 = this.mContext;
/* const-string/jumbo v1, "search" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/SearchManager; */
/* .line 2108 */
(( android.app.SearchManager ) v0 ).launchAssist ( p2 ); // invoke-virtual {v0, p2}, Landroid/app/SearchManager;->launchAssist(Landroid/os/Bundle;)V
/* .line 2110 */
} // :cond_1
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).launchAssistActionInternal ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchAssistActionInternal(Ljava/lang/String;Landroid/os/Bundle;)V
/* .line 2112 */
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
protected abstract void launchAssistActionInternal ( java.lang.String p0, android.os.Bundle p1 ) {
} // .end method
public Boolean launchHome ( ) {
/* .locals 1 */
/* .line 2120 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).launchHomeFromHotKey ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchHomeFromHotKey(I)V
/* .line 2121 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean launchRecentPanel ( ) {
/* .locals 1 */
/* .line 2125 */
final String v0 = "recentapps"; // const-string v0, "recentapps"
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).sendCloseSystemWindows ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendCloseSystemWindows(Ljava/lang/String;)V
/* .line 2127 */
v0 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).keyguardOn ( ); // invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->keyguardOn()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2129 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2132 */
} // :cond_0
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).launchRecentPanelInternal ( ); // invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchRecentPanelInternal()V
/* .line 2133 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
protected abstract void launchRecentPanelInternal ( ) {
} // .end method
public Boolean launchRecents ( ) {
/* .locals 1 */
/* .line 2116 */
v0 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).launchRecentPanel ( ); // invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchRecentPanel()Z
} // .end method
public void notifyLidSwitchChanged ( Long p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "whenNanos" # J */
/* .param p3, "lidOpen" # Z */
/* .line 707 */
v0 = this.mSmartCoverManager;
/* iget-boolean v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFolded:Z */
(( miui.util.SmartCoverManager ) v0 ).notifyLidSwitchChanged ( p3, v1 ); // invoke-virtual {v0, p3, v1}, Lmiui/util/SmartCoverManager;->notifyLidSwitchChanged(ZZ)V
/* .line 708 */
v0 = this.mMiuiPadKeyboardManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 709 */
/* .line 711 */
} // :cond_0
com.miui.server.input.PadManager .getInstance ( );
(( com.miui.server.input.PadManager ) v0 ).setIsLidOpen ( p3 ); // invoke-virtual {v0, p3}, Lcom/miui/server/input/PadManager;->setIsLidOpen(Z)V
/* .line 712 */
v0 = this.mMiuiStylusShortcutManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 713 */
(( com.miui.server.input.stylus.MiuiStylusShortcutManager ) v0 ).notifyLidSwitchChanged ( p3 ); // invoke-virtual {v0, p3}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->notifyLidSwitchChanged(Z)V
/* .line 715 */
} // :cond_1
return;
} // .end method
public void onDefaultDisplayFocusChangedLw ( com.android.server.policy.WindowManagerPolicy$WindowState p0 ) {
/* .locals 3 */
/* .param p1, "newFocus" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .line 1983 */
/* invoke-super {p0, p1}, Lcom/android/server/policy/PhoneWindowManager;->onDefaultDisplayFocusChangedLw(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V */
/* .line 1984 */
if ( p1 != null) { // if-eqz p1, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1985 */
/* .local v0, "packageName":Ljava/lang/String; */
} // :goto_0
/* iget-boolean v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemReady:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = this.mTofManagerInternal;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1986 */
(( com.android.server.tof.TofManagerInternal ) v1 ).onDefaultDisplayFocusChanged ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/tof/TofManagerInternal;->onDefaultDisplayFocusChanged(Ljava/lang/String;)V
/* .line 1988 */
} // :cond_1
this.mFocusedWindow = p1;
/* .line 1989 */
v1 = this.mMiuiStylusShortcutManager;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1990 */
(( com.miui.server.input.stylus.MiuiStylusShortcutManager ) v1 ).onDefaultDisplayFocusChangedLw ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->onDefaultDisplayFocusChangedLw(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V
/* .line 1991 */
com.miui.server.input.stylus.blocker.MiuiEventBlockerManager .getInstance ( );
(( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager ) v1 ).onFocusedWindowChanged ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->onFocusedWindowChanged(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V
/* .line 1993 */
} // :cond_2
v1 = this.mMiuiThreeGestureListener;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1994 */
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager ) v1 ).onFocusedWindowChanged ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->onFocusedWindowChanged(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V
/* .line 1996 */
} // :cond_3
if ( p1 != null) { // if-eqz p1, :cond_4
/* sget-boolean v1, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1997 */
/* .line 1998 */
/* .local v1, "focusedPackageName":Ljava/lang/String; */
com.android.server.display.DisplayPowerControllerStub .getInstance ( );
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1999 */
com.android.server.display.DisplayPowerControllerStub .getInstance ( );
/* .line 2002 */
} // .end local v1 # "focusedPackageName":Ljava/lang/String;
} // :cond_4
/* const-class v1, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
/* .line 2003 */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
/* .line 2004 */
/* .local v1, "service":Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 2005 */
(( com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal ) v1 ).onFocusedWindowChanged ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->onFocusedWindowChanged(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V
/* .line 2007 */
} // :cond_5
return;
} // .end method
protected abstract void onStatusBarPanelRevealed ( com.android.internal.statusbar.IStatusBarService p0 ) {
} // .end method
public Boolean performHapticFeedback ( Integer p0, java.lang.String p1, Integer p2, Boolean p3, java.lang.String p4 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "effectId" # I */
/* .param p4, "always" # Z */
/* .param p5, "reason" # Ljava/lang/String; */
/* .line 2229 */
v0 = this.mHapticFeedbackUtil;
v0 = (( miui.util.HapticFeedbackUtil ) v0 ).isSupportedEffect ( p3 ); // invoke-virtual {v0, p3}, Lmiui/util/HapticFeedbackUtil;->isSupportedEffect(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2230 */
v0 = this.mHapticFeedbackUtil;
v0 = (( miui.util.HapticFeedbackUtil ) v0 ).performHapticFeedback ( p2, p3, p4 ); // invoke-virtual {v0, p2, p3, p4}, Lmiui/util/HapticFeedbackUtil;->performHapticFeedback(Ljava/lang/String;IZ)Z
/* .line 2233 */
} // :cond_0
v0 = /* invoke-super/range {p0 ..p5}, Lcom/android/server/policy/PhoneWindowManager;->performHapticFeedback(ILjava/lang/String;IZLjava/lang/String;)Z */
} // .end method
protected abstract void preloadRecentAppsInternal ( ) {
} // .end method
protected abstract Integer processFingerprintNavigationEvent ( android.view.KeyEvent p0, Boolean p1 ) {
} // .end method
protected void registerProximitySensor ( ) {
/* .locals 2 */
/* .line 1077 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$9; */
/* invoke-direct {v1, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$9;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1086 */
return;
} // .end method
protected void registerStatusBarInputEventReceiver ( ) {
/* .locals 0 */
/* .line 1777 */
return;
} // .end method
protected abstract Boolean screenOffBecauseOfProxSensor ( ) {
} // .end method
public void screenTurnedOff ( Integer p0, Boolean p1 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .param p2, "isSwappingDisplay" # Z */
/* .line 606 */
/* invoke-super {p0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager;->screenTurnedOff(IZ)V */
/* .line 607 */
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->startCameraProcess()V */
/* .line 608 */
return;
} // .end method
protected void screenTurnedOffInternal ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "why" # I */
/* .line 700 */
/* iput p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mScreenOffReason:I */
/* .line 701 */
return;
} // .end method
public void screenTurningOff ( Integer p0, com.android.server.policy.WindowManagerPolicy$ScreenOffListener p1 ) {
/* .locals 1 */
/* .param p1, "displayId" # I */
/* .param p2, "screenOffListener" # Lcom/android/server/policy/WindowManagerPolicy$ScreenOffListener; */
/* .line 598 */
/* invoke-super {p0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager;->screenTurningOff(ILcom/android/server/policy/WindowManagerPolicy$ScreenOffListener;)V */
/* .line 599 */
/* sget-boolean v0, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* if-nez p1, :cond_0 */
/* .line 600 */
com.android.server.display.DisplayManagerServiceStub .getInstance ( );
(( com.android.server.display.DisplayManagerServiceStub ) v0 ).screenTurningOff ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayManagerServiceStub;->screenTurningOff()V
/* .line 602 */
} // :cond_0
return;
} // .end method
public void screenTurningOn ( Integer p0, com.android.server.policy.WindowManagerPolicy$ScreenOnListener p1 ) {
/* .locals 1 */
/* .param p1, "displayId" # I */
/* .param p2, "screenOnListener" # Lcom/android/server/policy/WindowManagerPolicy$ScreenOnListener; */
/* .line 586 */
/* invoke-super {p0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager;->screenTurningOn(ILcom/android/server/policy/WindowManagerPolicy$ScreenOnListener;)V */
/* .line 587 */
/* if-nez p2, :cond_0 */
v0 = this.mMiuiKeyguardDelegate;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 588 */
(( com.android.server.policy.MiuiKeyguardServiceDelegate ) v0 ).onScreenTurnedOnWithoutListener ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->onScreenTurnedOnWithoutListener()V
/* .line 590 */
} // :cond_0
/* sget-boolean v0, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez p1, :cond_1 */
/* .line 591 */
com.android.server.display.DisplayManagerServiceStub .getInstance ( );
(( com.android.server.display.DisplayManagerServiceStub ) v0 ).screenTurningOn ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayManagerServiceStub;->screenTurningOn()V
/* .line 594 */
} // :cond_1
return;
} // .end method
void sendAsyncBroadcast ( android.content.Intent p0 ) {
/* .locals 1 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 2704 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).sendAsyncBroadcast ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;Z)V
/* .line 2705 */
return;
} // .end method
void sendAsyncBroadcast ( android.content.Intent p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "receiverPermission" # Ljava/lang/String; */
/* .line 2719 */
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2720 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$16; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$16;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/content/Intent;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2727 */
} // :cond_0
return;
} // .end method
void sendAsyncBroadcast ( android.content.Intent p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "isInteractive" # Z */
/* .line 2708 */
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2709 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 2710 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/content/Intent;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2713 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/content/Intent;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2716 */
} // :cond_1
} // :goto_0
return;
} // .end method
void sendAsyncBroadcastForAllUser ( android.content.Intent p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 2730 */
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2731 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$17; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$17;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/content/Intent;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2738 */
} // :cond_0
return;
} // .end method
public void setCurrentUserLw ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "newUserId" # I */
/* .line 2528 */
/* invoke-super {p0, p1}, Lcom/android/server/policy/PhoneWindowManager;->setCurrentUserLw(I)V */
/* .line 2529 */
/* iput p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCurrentUserId:I */
/* .line 2530 */
v0 = this.mAutoDisableScreenButtonsManager;
(( com.miui.server.input.AutoDisableScreenButtonsManager ) v0 ).onUserSwitch ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->onUserSwitch(I)V
/* .line 2531 */
v0 = this.mSmartCoverManager;
(( miui.util.SmartCoverManager ) v0 ).onUserSwitch ( p1 ); // invoke-virtual {v0, p1}, Lmiui/util/SmartCoverManager;->onUserSwitch(I)V
/* .line 2532 */
v0 = this.mAccessibilityShortcutSetting;
(( miui.provider.SettingsStringUtil$SettingStringHelper ) v0 ).setUserId ( p1 ); // invoke-virtual {v0, p1}, Lmiui/provider/SettingsStringUtil$SettingStringHelper;->setUserId(I)V
/* .line 2533 */
v0 = this.mSettingsObserver;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.policy.BaseMiuiPhoneWindowManager$MiuiSettingsObserver ) v0 ).onChange ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->onChange(Z)V
/* .line 2534 */
v0 = this.mMiuiBackTapGestureService;
(( com.miui.server.input.MiuiBackTapGestureService ) v0 ).onUserSwitch ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService;->onUserSwitch(I)V
/* .line 2535 */
v0 = this.mMiuiKnockGestureService;
(( com.miui.server.input.knock.MiuiKnockGestureService ) v0 ).onUserSwitch ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->onUserSwitch(I)V
/* .line 2536 */
v0 = this.mMiuiThreeGestureListener;
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager ) v0 ).onUserSwitch ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->onUserSwitch(I)V
/* .line 2537 */
v0 = this.mMiuiKeyShortcutRuleManager;
(( com.android.server.policy.MiuiKeyShortcutRuleManager ) v0 ).onUserSwitch ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->onUserSwitch(I)V
/* .line 2538 */
v0 = this.mMiuiFingerPrintTapListener;
(( com.miui.server.input.MiuiFingerPrintTapListener ) v0 ).onUserSwitch ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->onUserSwitch(I)V
/* .line 2539 */
v0 = this.mShoulderKeyManagerInternal;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2540 */
/* .line 2542 */
} // :cond_0
com.miui.server.input.stylus.blocker.MiuiEventBlockerManager .getInstance ( );
(( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager ) v0 ).onUserSwitch ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->onUserSwitch(I)V
/* .line 2543 */
com.android.server.input.overscroller.ScrollerOptimizationConfigProvider .getInstance ( );
(( com.android.server.input.overscroller.ScrollerOptimizationConfigProvider ) v0 ).onUserSwitch ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->onUserSwitch(I)V
/* .line 2544 */
v0 = this.mMiuiStylusShortcutManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2545 */
(( com.miui.server.input.stylus.MiuiStylusShortcutManager ) v0 ).onUserSwitch ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->onUserSwitch(I)V
/* .line 2547 */
} // :cond_1
com.android.server.input.MiuiInputThread .getHandler ( );
/* new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda5; */
/* invoke-direct {v1, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2551 */
/* const-class v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
/* .line 2552 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
/* .line 2553 */
/* .local v0, "service":Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 2554 */
(( com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal ) v0 ).onUserChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->onUserChanged(I)V
/* .line 2556 */
} // :cond_2
com.android.server.input.TouchWakeUpFeatureManager .getInstance ( );
(( com.android.server.input.TouchWakeUpFeatureManager ) v1 ).onUserSwitch ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/input/TouchWakeUpFeatureManager;->onUserSwitch(I)V
/* .line 2557 */
com.android.server.input.InputManagerServiceStub .getInstance ( );
/* .line 2558 */
return;
} // .end method
public void setWakeUpInfo ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "detail" # Ljava/lang/String; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 557 */
this.mWakeUpDetail = p1;
/* .line 558 */
this.mWakeUpReason = p2;
/* .line 559 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mUpdateWakeUpDetailTime:J */
/* .line 560 */
return;
} // .end method
public void showBootMessage ( java.lang.CharSequence p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "msg" # Ljava/lang/CharSequence; */
/* .param p2, "always" # Z */
/* .line 2567 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$14; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$14;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/CharSequence;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2665 */
return;
} // .end method
public void startedGoingToSleep ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "displayGroupId" # I */
/* .param p2, "why" # I */
/* .line 660 */
/* invoke-super {p0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager;->startedGoingToSleep(II)V */
/* .line 661 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 662 */
return;
/* .line 664 */
} // :cond_0
v0 = this.mMiuiKnockGestureService;
int v1 = 0; // const/4 v1, 0x0
(( com.miui.server.input.knock.MiuiKnockGestureService ) v0 ).updateScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->updateScreenState(Z)V
/* .line 665 */
v0 = this.mMiuiThreeGestureListener;
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager ) v0 ).updateScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->updateScreenState(Z)V
/* .line 667 */
v0 = this.mShoulderKeyManagerInternal;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 668 */
/* .line 670 */
} // :cond_1
v0 = this.mMiuiStylusShortcutManager;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 671 */
(( com.miui.server.input.stylus.MiuiStylusShortcutManager ) v0 ).updateScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->updateScreenState(Z)V
/* .line 673 */
} // :cond_2
v0 = this.mMiuiTimeFloatingWindow;
(( com.miui.server.input.time.MiuiTimeFloatingWindow ) v0 ).updateScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateScreenState(Z)V
/* .line 674 */
v0 = this.mMiuiPadKeyboardManager;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 675 */
/* .line 677 */
} // :cond_3
v0 = this.mMiuiInputManagerInternal;
(( com.android.server.input.MiuiInputManagerInternal ) v0 ).setScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/MiuiInputManagerInternal;->setScreenState(Z)V
/* .line 678 */
v0 = this.mMiuiKeyInterceptExtend;
(( com.android.server.policy.MiuiKeyInterceptExtend ) v0 ).setScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setScreenState(Z)V
/* .line 679 */
return;
} // .end method
public void startedWakingUp ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "displayGroupId" # I */
/* .param p2, "why" # I */
/* .line 521 */
/* invoke-super {p0, p1, p2}, Lcom/android/server/policy/PhoneWindowManager;->startedWakingUp(II)V */
/* .line 522 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 523 */
return;
/* .line 525 */
} // :cond_0
v0 = this.mMiuiPocketModeManager;
/* if-nez v0, :cond_1 */
/* .line 526 */
/* new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;-><init>(Landroid/content/Context;)V */
this.mMiuiPocketModeManager = v0;
/* .line 528 */
} // :cond_1
v0 = this.mProximitySensor;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 529 */
v0 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).isDeviceProvisioned ( ); // invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isDeviceProvisioned()Z
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mContext;
/* .line 530 */
v0 = android.provider.MiuiSettings$System .isInSmallWindowMode ( v0 );
/* if-nez v0, :cond_2 */
v0 = this.mMiuiPocketModeManager;
/* .line 531 */
v0 = (( com.android.server.input.pocketmode.MiuiPocketModeManager ) v0 ).getPocketModeEnableSettings ( ); // invoke-virtual {v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->getPocketModeEnableSettings()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsVRMode:Z */
/* if-nez v0, :cond_2 */
/* iget-boolean v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsSynergyMode:Z */
/* if-nez v0, :cond_2 */
v0 = this.mWakeUpDetail;
v1 = this.mWakeUpReason;
/* .line 534 */
v0 = /* invoke-direct {p0, v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->skipPocketModeAquireByWakeUpInfo(Ljava/lang/String;Ljava/lang/String;)Z */
/* if-nez v0, :cond_2 */
/* .line 535 */
v0 = this.mProximitySensor;
(( com.android.server.policy.MiuiScreenOnProximityLock ) v0 ).aquire ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->aquire()V
/* .line 537 */
} // :cond_2
v0 = this.mMiuiKnockGestureService;
int v1 = 1; // const/4 v1, 0x1
(( com.miui.server.input.knock.MiuiKnockGestureService ) v0 ).updateScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->updateScreenState(Z)V
/* .line 538 */
v0 = this.mMiuiThreeGestureListener;
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager ) v0 ).updateScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->updateScreenState(Z)V
/* .line 539 */
v0 = this.mMiuiTimeFloatingWindow;
(( com.miui.server.input.time.MiuiTimeFloatingWindow ) v0 ).updateScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateScreenState(Z)V
/* .line 540 */
v0 = this.mMiuiBackTapGestureService;
(( com.miui.server.input.MiuiBackTapGestureService ) v0 ).notifyScreenOn ( ); // invoke-virtual {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->notifyScreenOn()V
/* .line 541 */
v0 = this.mMiuiStylusShortcutManager;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 542 */
(( com.miui.server.input.stylus.MiuiStylusShortcutManager ) v0 ).updateScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->updateScreenState(Z)V
/* .line 545 */
} // :cond_3
v0 = this.mShoulderKeyManagerInternal;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 546 */
/* .line 549 */
} // :cond_4
v0 = this.mMiuiPadKeyboardManager;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 550 */
/* .line 552 */
} // :cond_5
v0 = this.mMiuiInputManagerInternal;
(( com.android.server.input.MiuiInputManagerInternal ) v0 ).setScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/MiuiInputManagerInternal;->setScreenState(Z)V
/* .line 553 */
v0 = this.mMiuiKeyInterceptExtend;
(( com.android.server.policy.MiuiKeyInterceptExtend ) v0 ).setScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setScreenState(Z)V
/* .line 554 */
return;
} // .end method
protected Boolean stopLockTaskMode ( ) {
/* .locals 1 */
/* .line 962 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected void systemReadyInternal ( ) {
/* .locals 8 */
/* .line 446 */
/* new-instance v0, Lcom/miui/server/input/MiuiFingerPrintTapListener; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/miui/server/input/MiuiFingerPrintTapListener;-><init>(Landroid/content/Context;)V */
this.mMiuiFingerPrintTapListener = v0;
/* .line 447 */
/* new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager; */
v1 = this.mContext;
v2 = this.mHandler;
/* invoke-direct {v0, v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;-><init>(Landroid/content/Context;Landroid/os/Handler;)V */
this.mMiuiThreeGestureListener = v0;
/* .line 448 */
/* new-instance v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager ) v0 ).initKeyguardActiveFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->initKeyguardActiveFunction(Ljava/util/function/BooleanSupplier;)V
/* .line 449 */
/* new-instance v0, Lcom/miui/server/input/knock/MiuiKnockGestureService; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;-><init>(Landroid/content/Context;)V */
this.mMiuiKnockGestureService = v0;
/* .line 450 */
/* new-instance v0, Lcom/miui/server/input/MiuiBackTapGestureService; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/miui/server/input/MiuiBackTapGestureService;-><init>(Landroid/content/Context;)V */
this.mMiuiBackTapGestureService = v0;
/* .line 451 */
/* const-class v0, Lcom/miui/server/stability/StabilityLocalServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/stability/StabilityLocalServiceInternal; */
this.mStabilityLocalServiceInternal = v0;
/* .line 452 */
/* const-class v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal; */
this.mShoulderKeyManagerInternal = v0;
/* .line 453 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 454 */
/* .line 456 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 458 */
/* .local v0, "pm":Landroid/content/pm/PackageManager; */
final String v1 = "config_antiMistouchDisabled"; // const-string v1, "config_antiMistouchDisabled"
int v2 = 0; // const/4 v2, 0x0
v1 = miui.util.FeatureParser .getBoolean ( v1,v2 );
/* .line 460 */
/* .local v1, "configAntiMisTouchDisabled":Z */
v3 = this.mContext;
v3 = com.android.server.input.pocketmode.MiuiPocketModeManager .isSupportInertialAndLightSensor ( v3 );
/* if-nez v3, :cond_1 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 461 */
final String v3 = "android.hardware.sensor.proximity"; // const-string v3, "android.hardware.sensor.proximity"
v3 = (( android.content.pm.PackageManager ) v0 ).hasSystemFeature ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 462 */
v3 = miui.os.DeviceFeature .hasSupportAudioPromity ( );
/* if-nez v3, :cond_2 */
/* if-nez v1, :cond_2 */
/* .line 463 */
} // :cond_1
/* new-instance v3, Lcom/android/server/policy/MiuiScreenOnProximityLock; */
v4 = this.mContext;
v5 = this.mMiuiKeyguardDelegate;
v6 = this.mHandler;
/* .line 464 */
(( android.os.Handler ) v6 ).getLooper ( ); // invoke-virtual {v6}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v3, v4, v5, v6}, Lcom/android/server/policy/MiuiScreenOnProximityLock;-><init>(Landroid/content/Context;Lcom/android/server/policy/MiuiKeyguardServiceDelegate;Landroid/os/Looper;)V */
this.mProximitySensor = v3;
/* .line 467 */
} // :cond_2
v3 = this.mContext;
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v4, "torch_state" */
android.provider.Settings$Global .putInt ( v3,v4,v2 );
/* .line 468 */
v3 = this.mContext;
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v4 = "auto_test_mode_on"; // const-string v4, "auto_test_mode_on"
android.provider.Settings$Global .putInt ( v3,v4,v2 );
/* .line 470 */
/* iput-boolean v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mIsVRMode:Z */
/* .line 471 */
v3 = this.mContext;
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v4, "vr_mode" */
android.provider.Settings$System .putInt ( v3,v4,v2 );
/* .line 473 */
/* nop */
/* .line 474 */
final String v3 = "front_fingerprint_sensor"; // const-string v3, "front_fingerprint_sensor"
v3 = miui.util.FeatureParser .getBoolean ( v3,v2 );
/* iput-boolean v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mFrontFingerprintSensor:Z */
/* .line 475 */
/* nop */
/* .line 476 */
/* const-string/jumbo v3, "support_tap_fingerprint_sensor_to_home" */
v3 = miui.util.FeatureParser .getBoolean ( v3,v2 );
/* iput-boolean v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mSupportTapFingerprintSensorToHome:Z */
/* .line 477 */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
this.mFpNavEventNameList = v3;
/* .line 478 */
final String v3 = "fp_nav_event_name_list"; // const-string v3, "fp_nav_event_name_list"
miui.util.FeatureParser .getStringArray ( v3 );
/* .line 479 */
/* .local v3, "strArray":[Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 480 */
/* array-length v4, v3 */
/* move v5, v2 */
} // :goto_0
/* if-ge v5, v4, :cond_3 */
/* aget-object v6, v3, v5 */
/* .line 481 */
/* .local v6, "str":Ljava/lang/String; */
v7 = this.mFpNavEventNameList;
/* .line 480 */
} // .end local v6 # "str":Ljava/lang/String;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 486 */
} // :cond_3
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v5 = "immersive.preconfirms=*"; // const-string v5, "immersive.preconfirms=*"
int v6 = -2; // const/4 v6, -0x2
final String v7 = "policy_control"; // const-string v7, "policy_control"
android.provider.Settings$Global .putStringForUser ( v4,v7,v5,v6 );
/* .line 489 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v5 = "persist.camera.snap.enable"; // const-string v5, "persist.camera.snap.enable"
v4 = android.provider.Settings$System .getInt ( v4,v5,v2 );
int v6 = 1; // const/4 v6, 0x1
/* if-ne v4, v6, :cond_5 */
/* .line 490 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putInt ( v4,v5,v2 );
/* .line 491 */
/* iget-boolean v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveTranksCard:Z */
final String v5 = "key_long_press_volume_down"; // const-string v5, "key_long_press_volume_down"
/* if-nez v4, :cond_4 */
/* .line 492 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "Street-snap"; // const-string v6, "Street-snap"
/* iget v7, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCurrentUserId:I */
android.provider.Settings$Secure .putStringForUser ( v4,v5,v6,v7 );
/* .line 496 */
} // :cond_4
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "none"; // const-string v6, "none"
/* iget v7, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCurrentUserId:I */
android.provider.Settings$Secure .putStringForUser ( v4,v5,v6,v7 );
/* .line 501 */
} // :cond_5
} // :goto_1
v4 = this.mSettingsObserver;
(( com.android.server.policy.BaseMiuiPhoneWindowManager$MiuiSettingsObserver ) v4 ).onChange ( v2 ); // invoke-virtual {v4, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->onChange(Z)V
/* .line 502 */
v2 = this.mEdgeSuppressionManager;
final String v4 = "configuration"; // const-string v4, "configuration"
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager ) v2 ).handleEdgeModeChange ( v4 ); // invoke-virtual {v2, v4}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->handleEdgeModeChange(Ljava/lang/String;)V
/* .line 504 */
v2 = this.mContext;
com.android.server.input.padkeyboard.MiuiPadKeyboardManager .getKeyboardManager ( v2 );
this.mMiuiPadKeyboardManager = v2;
/* .line 505 */
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 506 */
/* const-class v4, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager; */
com.android.server.LocalServices .addService ( v4,v2 );
/* .line 507 */
com.miui.server.input.PadManager .getInstance ( );
(( com.miui.server.input.PadManager ) v2 ).registerPointerEventListener ( ); // invoke-virtual {v2}, Lcom/miui/server/input/PadManager;->registerPointerEventListener()V
/* .line 509 */
} // :cond_6
/* sget-boolean v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->IS_FOLD_DEVICE:Z */
/* if-nez v2, :cond_7 */
/* sget-boolean v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->IS_FLIP_DEVICE:Z */
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 510 */
} // :cond_7
v2 = this.mIDisplayFoldListener;
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).registerDisplayFoldListener ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->registerDisplayFoldListener(Landroid/view/IDisplayFoldListener;)V
/* .line 512 */
} // :cond_8
/* const-class v2, Lcom/android/server/tof/TofManagerInternal; */
com.android.server.LocalServices .getService ( v2 );
/* check-cast v2, Lcom/android/server/tof/TofManagerInternal; */
this.mTofManagerInternal = v2;
/* .line 513 */
v2 = miui.hardware.input.InputFeature .supportPhotoHandle ( );
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 514 */
v2 = this.mContext;
com.android.server.input.MiInputPhotoHandleManager .getInstance ( v2 );
(( com.android.server.input.MiInputPhotoHandleManager ) v2 ).init ( ); // invoke-virtual {v2}, Lcom/android/server/input/MiInputPhotoHandleManager;->init()V
/* .line 516 */
} // :cond_9
return;
} // .end method
protected abstract void toggleSplitScreenInternal ( ) {
} // .end method
public Boolean triggerCloseApp ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 2010 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "trigger close app reason:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 2011 */
v0 = /* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->closeApp()Z */
} // .end method
public Boolean triggerLaunchRecent ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 2141 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "trigger launch recent,reason:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 2142 */
/* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->preloadRecentApps()V */
/* .line 2143 */
v0 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) p0 ).launchRecentPanel ( ); // invoke-virtual {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchRecentPanel()Z
} // .end method
public Boolean triggerShowMenu ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 2147 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "trigger show menu,reason:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 2148 */
v0 = /* invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showMenu()Z */
} // .end method
protected void unregisterStatusBarInputEventReceiver ( ) {
/* .locals 0 */
/* .line 1778 */
return;
} // .end method
