public class com.android.server.policy.MiuiCombinationKeyAndGestureObserver extends com.android.server.policy.MiuiShortcutObserver {
	 /* .source "MiuiCombinationKeyAndGestureObserver.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final java.lang.String mAction;
	 private final android.content.ContentResolver mContentResolver;
	 private final android.content.Context mContext;
	 private final java.lang.String mDefaultFunction;
	 private java.lang.String mFunction;
	 private com.android.server.policy.MiuiCombinationRule mMiuiCombinationRule;
	 private com.android.server.policy.MiuiGestureRule mMiuiGestureRule;
	 /* # direct methods */
	 public com.android.server.policy.MiuiCombinationKeyAndGestureObserver ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .param p3, "action" # Ljava/lang/String; */
		 /* .param p4, "defaultFunction" # Ljava/lang/String; */
		 /* .param p5, "currentUserId" # I */
		 /* .line 23 */
		 /* invoke-direct {p0, p2, p1, p5}, Lcom/android/server/policy/MiuiShortcutObserver;-><init>(Landroid/os/Handler;Landroid/content/Context;I)V */
		 /* .line 24 */
		 this.mAction = p3;
		 /* .line 25 */
		 this.mDefaultFunction = p4;
		 /* .line 26 */
		 this.mContext = p1;
		 /* .line 27 */
		 (( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 this.mContentResolver = v0;
		 /* .line 28 */
		 /* invoke-direct {p0, p3}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->registerShortcutAction(Ljava/lang/String;)V */
		 /* .line 29 */
		 return;
	 } // .end method
	 private void registerShortcutAction ( java.lang.String p0 ) {
		 /* .locals 4 */
		 /* .param p1, "action" # Ljava/lang/String; */
		 /* .line 37 */
		 v0 = this.mContentResolver;
		 /* .line 38 */
		 android.provider.Settings$System .getUriFor ( p1 );
		 /* .line 37 */
		 int v2 = 0; // const/4 v2, 0x0
		 int v3 = -1; // const/4 v3, -0x1
		 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
		 /* .line 40 */
		 return;
	 } // .end method
	 private void updateFunction ( ) {
		 /* .locals 4 */
		 /* .line 92 */
		 v0 = this.mAction;
		 v0 = 		 (( com.android.server.policy.MiuiCombinationKeyAndGestureObserver ) p0 ).currentFunctionValueIsInt ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->currentFunctionValueIsInt(Ljava/lang/String;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 93 */
			 v0 = this.mContentResolver;
			 v1 = this.mAction;
			 /* iget v2, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mCurrentUserId:I */
			 int v3 = -1; // const/4 v3, -0x1
			 v0 = 			 android.provider.Settings$System .getIntForUser ( v0,v1,v3,v2 );
			 /* .line 95 */
			 /* .local v0, "functionStatus":I */
			 /* if-ne v0, v3, :cond_0 */
			 int v1 = 0; // const/4 v1, 0x0
		 } // :cond_0
		 java.lang.String .valueOf ( v0 );
	 } // :goto_0
	 /* move-object v0, v1 */
	 /* .line 96 */
	 /* .local v0, "function":Ljava/lang/String; */
	 /* .line 97 */
} // .end local v0 # "function":Ljava/lang/String;
} // :cond_1
v0 = this.mContentResolver;
v1 = this.mAction;
/* iget v2, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mCurrentUserId:I */
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 99 */
/* .restart local v0 # "function":Ljava/lang/String; */
} // :goto_1
this.mFunction = v0;
/* .line 100 */
return;
} // .end method
/* # virtual methods */
public java.lang.String getFunction ( ) {
/* .locals 1 */
/* .line 114 */
v0 = this.mFunction;
} // .end method
public void onChange ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .line 44 */
/* invoke-direct {p0}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->updateFunction()V */
/* .line 45 */
v0 = this.mMiuiGestureRule;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 46 */
(( com.android.server.policy.MiuiCombinationKeyAndGestureObserver ) p0 ).notifyGestureRuleChanged ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->notifyGestureRuleChanged(Lcom/android/server/policy/MiuiGestureRule;)V
/* .line 48 */
} // :cond_0
v0 = this.mMiuiCombinationRule;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 49 */
(( com.android.server.policy.MiuiCombinationKeyAndGestureObserver ) p0 ).notifyCombinationRuleChanged ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->notifyCombinationRuleChanged(Lcom/android/server/policy/MiuiCombinationRule;)V
/* .line 51 */
} // :cond_1
/* invoke-super {p0, p1}, Lcom/android/server/policy/MiuiShortcutObserver;->onChange(Z)V */
/* .line 52 */
return;
} // .end method
public void setDefaultFunction ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "isNeedReset" # Z */
/* .line 57 */
v0 = this.mAction;
v0 = (( com.android.server.policy.MiuiCombinationKeyAndGestureObserver ) p0 ).currentFunctionValueIsInt ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->currentFunctionValueIsInt(Ljava/lang/String;)Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 58 */
v0 = this.mContentResolver;
v2 = this.mAction;
/* iget v3, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mCurrentUserId:I */
int v4 = -1; // const/4 v4, -0x1
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v4,v3 );
/* .line 60 */
/* .local v0, "functionStatus":I */
/* if-ne v0, v4, :cond_0 */
/* move-object v2, v1 */
} // :cond_0
java.lang.String .valueOf ( v0 );
} // :goto_0
/* move-object v0, v2 */
/* .line 61 */
/* .local v0, "currentFunction":Ljava/lang/String; */
/* .line 63 */
} // .end local v0 # "currentFunction":Ljava/lang/String;
} // :cond_1
v0 = this.mContentResolver;
v2 = this.mAction;
/* iget v3, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mCurrentUserId:I */
android.provider.Settings$System .getStringForUser ( v0,v2,v3 );
/* .line 65 */
/* .restart local v0 # "currentFunction":Ljava/lang/String; */
/* if-nez v0, :cond_2 */
v2 = this.mDefaultFunction;
} // :cond_2
/* move-object v2, v0 */
} // :goto_1
/* move-object v0, v2 */
/* .line 67 */
} // :goto_2
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 68 */
v0 = this.mDefaultFunction;
/* .line 70 */
} // :cond_3
v2 = this.mAction;
v2 = (( com.android.server.policy.MiuiCombinationKeyAndGestureObserver ) p0 ).hasCustomizedFunction ( v2, v0, p1 ); // invoke-virtual {p0, v2, v0, p1}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->hasCustomizedFunction(Ljava/lang/String;Ljava/lang/String;Z)Z
/* if-nez v2, :cond_5 */
/* .line 71 */
v2 = this.mContext;
v2 = (( com.android.server.policy.MiuiCombinationKeyAndGestureObserver ) p0 ).isFeasibleFunction ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->isFeasibleFunction(Ljava/lang/String;Landroid/content/Context;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 72 */
v1 = this.mContentResolver;
v2 = this.mAction;
/* iget v3, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mCurrentUserId:I */
android.provider.Settings$System .putStringForUser ( v1,v2,v0,v3 );
/* .line 75 */
} // :cond_4
if ( p1 != null) { // if-eqz p1, :cond_5
/* .line 77 */
v2 = this.mContentResolver;
v3 = this.mAction;
/* iget v4, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mCurrentUserId:I */
android.provider.Settings$System .putStringForUser ( v2,v3,v1,v4 );
/* .line 82 */
} // :cond_5
} // :goto_3
/* invoke-super {p0, p1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V */
/* .line 83 */
return;
} // .end method
public void setRuleForObserver ( com.android.server.policy.MiuiCombinationRule p0 ) {
/* .locals 0 */
/* .param p1, "miuiCombinationRule" # Lcom/android/server/policy/MiuiCombinationRule; */
/* .line 104 */
this.mMiuiCombinationRule = p1;
/* .line 105 */
return;
} // .end method
public void setRuleForObserver ( com.android.server.policy.MiuiGestureRule p0 ) {
/* .locals 0 */
/* .param p1, "miuiGestureRule" # Lcom/android/server/policy/MiuiGestureRule; */
/* .line 109 */
this.mMiuiGestureRule = p1;
/* .line 110 */
return;
} // .end method
void updateRuleInfo ( ) {
/* .locals 1 */
/* .line 87 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.policy.MiuiCombinationKeyAndGestureObserver ) p0 ).onChange ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->onChange(Z)V
/* .line 88 */
return;
} // .end method
