class com.android.server.policy.BaseMiuiPhoneWindowManager$RotationWatcher extends android.view.IRotationWatcher$Stub {
	 /* .source "BaseMiuiPhoneWindowManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "RotationWatcher" */
} // .end annotation
/* # instance fields */
final com.android.server.policy.BaseMiuiPhoneWindowManager this$0; //synthetic
/* # direct methods */
 com.android.server.policy.BaseMiuiPhoneWindowManager$RotationWatcher ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
/* .line 2288 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/view/IRotationWatcher$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onRotationChanged ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "i" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 2292 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "rotation changed = "; // const-string v1, "rotation changed = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "WindowManager"; // const-string v1, "WindowManager"
android.util.Slog .d ( v1,v0 );
/* .line 2293 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2294 */
/* .local v0, "targetId":I */
/* sget-boolean v1, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.this$0;
v1 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmFolded ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 2295 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* .line 2297 */
} // :cond_0
miui.util.ITouchFeature .getInstance ( );
/* const/16 v2, 0x8 */
(( miui.util.ITouchFeature ) v1 ).setTouchMode ( v0, v2, p1 ); // invoke-virtual {v1, v0, v2, p1}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z
/* .line 2299 */
v1 = this.this$0;
v1 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$misGameMode ( v1 );
/* if-nez v1, :cond_1 */
/* sget-boolean v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 2300 */
	 v1 = this.this$0;
	 com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmEdgeSuppressionManager ( v1 );
	 final String v2 = "rotation"; // const-string v2, "rotation"
	 (( com.miui.server.input.edgesuppression.EdgeSuppressionManager ) v1 ).handleEdgeModeChange ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->handleEdgeModeChange(Ljava/lang/String;)V
	 /* .line 2303 */
} // :cond_1
return;
} // .end method
