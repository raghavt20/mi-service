class com.android.server.policy.BaseMiuiPhoneWindowManager$14 implements java.lang.Runnable {
	 /* .source "BaseMiuiPhoneWindowManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showBootMessage(Ljava/lang/CharSequence;Z)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.policy.BaseMiuiPhoneWindowManager this$0; //synthetic
final java.lang.CharSequence val$msg; //synthetic
/* # direct methods */
 com.android.server.policy.BaseMiuiPhoneWindowManager$14 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 2567 */
this.this$0 = p1;
this.val$msg = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 8 */
/* .line 2569 */
v0 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMiuiBootMsgDialog ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_2 */
/* .line 2570 */
v0 = this.this$0;
/* new-instance v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$14$1; */
v3 = this.this$0;
v3 = this.mContext;
/* const v4, 0x1110000a */
/* invoke-direct {v2, p0, v3, v4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$14$1;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager$14;Landroid/content/Context;I)V */
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmMiuiBootMsgDialog ( v0,v2 );
/* .line 2593 */
v0 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMiuiBootMsgDialog ( v0 );
(( android.app.Dialog ) v0 ).getContext ( ); // invoke-virtual {v0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;
android.view.LayoutInflater .from ( v0 );
/* const v2, 0x110c000a */
int v3 = 0; // const/4 v3, 0x0
(( android.view.LayoutInflater ) v0 ).inflate ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
/* .line 2594 */
/* .local v0, "view":Landroid/view/View; */
v2 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMiuiBootMsgDialog ( v2 );
(( android.app.Dialog ) v2 ).setContentView ( v0 ); // invoke-virtual {v2, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V
/* .line 2595 */
v2 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMiuiBootMsgDialog ( v2 );
(( android.app.Dialog ) v2 ).getWindow ( ); // invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;
/* const/16 v3, 0x7e5 */
(( android.view.Window ) v2 ).setType ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V
/* .line 2597 */
v2 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMiuiBootMsgDialog ( v2 );
(( android.app.Dialog ) v2 ).getWindow ( ); // invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;
/* const/16 v3, 0x502 */
(( android.view.Window ) v2 ).addFlags ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V
/* .line 2601 */
v2 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMiuiBootMsgDialog ( v2 );
(( android.app.Dialog ) v2 ).getWindow ( ); // invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;
/* const/high16 v3, 0x3f800000 # 1.0f */
(( android.view.Window ) v2 ).setDimAmount ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/Window;->setDimAmount(F)V
/* .line 2602 */
v2 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMiuiBootMsgDialog ( v2 );
(( android.app.Dialog ) v2 ).getWindow ( ); // invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;
(( android.view.Window ) v2 ).getAttributes ( ); // invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;
/* .line 2603 */
/* .local v2, "lp":Landroid/view/WindowManager$LayoutParams; */
int v3 = 5; // const/4 v3, 0x5
/* iput v3, v2, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I */
/* .line 2604 */
v3 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMiuiBootMsgDialog ( v3 );
(( android.app.Dialog ) v3 ).getWindow ( ); // invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;
(( android.view.Window ) v3 ).setAttributes ( v2 ); // invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
/* .line 2605 */
v3 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMiuiBootMsgDialog ( v3 );
(( android.app.Dialog ) v3 ).setCancelable ( v1 ); // invoke-virtual {v3, v1}, Landroid/app/Dialog;->setCancelable(Z)V
/* .line 2606 */
v3 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMiuiBootMsgDialog ( v3 );
(( android.app.Dialog ) v3 ).show ( ); // invoke-virtual {v3}, Landroid/app/Dialog;->show()V
/* .line 2608 */
/* const v3, 0x110a001e */
(( android.view.View ) v0 ).findViewById ( v3 ); // invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* check-cast v3, Landroid/widget/ImageView; */
/* .line 2609 */
/* .local v3, "bootLogo":Landroid/widget/ImageView; */
(( android.widget.ImageView ) v3 ).setVisibility ( v1 ); // invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setVisibility(I)V
/* .line 2610 */
final String v4 = "beryllium"; // const-string v4, "beryllium"
v5 = miui.os.Build.DEVICE;
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 2611 */
final String v4 = "ro.boot.hwc"; // const-string v4, "ro.boot.hwc"
final String v5 = ""; // const-string v5, ""
android.os.SystemProperties .get ( v4,v5 );
/* .line 2612 */
/* .local v4, "hwc":Ljava/lang/String; */
final String v5 = "INDIA"; // const-string v5, "INDIA"
v5 = (( java.lang.String ) v4 ).contains ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
	 /* .line 2613 */
	 /* const v5, 0x11080168 */
	 (( android.widget.ImageView ) v3 ).setImageResource ( v5 ); // invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V
	 /* .line 2614 */
} // :cond_0
final String v5 = "GLOBAL"; // const-string v5, "GLOBAL"
v5 = (( java.lang.String ) v4 ).contains ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
	 /* .line 2615 */
	 /* const v5, 0x11080167 */
	 (( android.widget.ImageView ) v3 ).setImageResource ( v5 ); // invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V
	 /* .line 2618 */
} // .end local v4 # "hwc":Ljava/lang/String;
} // :cond_1
} // :goto_0
v4 = this.this$0;
/* const v5, 0x110a001f */
(( android.view.View ) v0 ).findViewById ( v5 ); // invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* check-cast v5, Landroid/widget/ProgressBar; */
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmBootProgress ( v4,v5 );
/* .line 2619 */
v4 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootProgress ( v4 );
int v5 = 4; // const/4 v5, 0x4
(( android.widget.ProgressBar ) v4 ).setVisibility ( v5 ); // invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V
/* .line 2621 */
v4 = this.this$0;
v6 = this.mContext;
(( android.content.Context ) v6 ).getResources ( ); // invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v7, 0x11030019 */
(( android.content.res.Resources ) v6 ).getStringArray ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmBootText ( v4,v6 );
/* .line 2622 */
v4 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootText ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_2
v4 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootText ( v4 );
/* array-length v4, v4 */
/* if-lez v4, :cond_2 */
/* .line 2623 */
v4 = this.this$0;
/* const v6, 0x110a0020 */
(( android.view.View ) v0 ).findViewById ( v6 ); // invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* check-cast v6, Landroid/widget/TextView; */
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmBootTextView ( v4,v6 );
/* .line 2624 */
v4 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootTextView ( v4 );
(( android.widget.TextView ) v4 ).setVisibility ( v5 ); // invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V
/* .line 2632 */
} // .end local v0 # "view":Landroid/view/View;
} // .end local v2 # "lp":Landroid/view/WindowManager$LayoutParams;
} // .end local v3 # "bootLogo":Landroid/widget/ImageView;
} // :cond_2
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 2633 */
/* .local v0, "parseList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v2 = this.val$msg;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 2634 */
java.lang.String .valueOf ( v2 );
final String v3 = "[^0-9]"; // const-string v3, "[^0-9]"
final String v4 = ","; // const-string v4, ","
(( java.lang.String ) v2 ).replaceAll ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
(( java.lang.String ) v2 ).split ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* array-length v3, v2 */
/* move v4, v1 */
} // :goto_1
/* if-ge v4, v3, :cond_4 */
/* aget-object v5, v2, v4 */
/* .line 2635 */
/* .local v5, "sp":Ljava/lang/String; */
v6 = (( java.lang.String ) v5 ).length ( ); // invoke-virtual {v5}, Ljava/lang/String;->length()I
/* if-lez v6, :cond_3 */
/* .line 2636 */
/* .line 2634 */
} // .end local v5 # "sp":Ljava/lang/String;
} // :cond_3
/* add-int/lit8 v4, v4, 0x1 */
/* .line 2640 */
v2 = } // :cond_4
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_7 */
/* .line 2641 */
/* check-cast v2, Ljava/lang/String; */
v2 = java.lang.Integer .parseInt ( v2 );
/* .line 2642 */
/* .local v2, "progress":I */
int v3 = 1; // const/4 v3, 0x1
/* check-cast v4, Ljava/lang/String; */
v4 = java.lang.Integer .parseInt ( v4 );
/* .line 2643 */
/* .local v4, "total":I */
/* if-le v2, v4, :cond_5 */
/* .line 2644 */
/* move v5, v2 */
/* .line 2645 */
/* .local v5, "tmp":I */
/* move v2, v4 */
/* .line 2646 */
/* move v4, v5 */
/* .line 2648 */
} // .end local v5 # "tmp":I
} // :cond_5
int v5 = 3; // const/4 v5, 0x3
/* if-le v4, v5, :cond_7 */
/* .line 2649 */
v5 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootProgress ( v5 );
(( android.widget.ProgressBar ) v5 ).setVisibility ( v1 ); // invoke-virtual {v5, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V
/* .line 2650 */
v5 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootProgress ( v5 );
(( android.widget.ProgressBar ) v5 ).setMax ( v4 ); // invoke-virtual {v5, v4}, Landroid/widget/ProgressBar;->setMax(I)V
/* .line 2651 */
v5 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootProgress ( v5 );
(( android.widget.ProgressBar ) v5 ).setProgress ( v2 ); // invoke-virtual {v5, v2}, Landroid/widget/ProgressBar;->setProgress(I)V
/* .line 2653 */
v5 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootTextView ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_7
v5 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootText ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_7
/* .line 2654 */
v5 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootTextView ( v5 );
(( android.widget.TextView ) v5 ).setVisibility ( v1 ); // invoke-virtual {v5, v1}, Landroid/widget/TextView;->setVisibility(I)V
/* .line 2655 */
v1 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootText ( v1 );
/* array-length v1, v1 */
/* mul-int/2addr v1, v2 */
/* div-int/2addr v1, v4 */
/* .line 2656 */
/* .local v1, "pos":I */
v5 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootText ( v5 );
/* array-length v5, v5 */
/* if-lt v1, v5, :cond_6 */
/* .line 2657 */
v5 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootText ( v5 );
/* array-length v5, v5 */
/* add-int/lit8 v1, v5, -0x1 */
/* .line 2659 */
} // :cond_6
v3 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootTextView ( v3 );
v5 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmBootText ( v5 );
/* aget-object v5, v5, v1 */
(( android.widget.TextView ) v3 ).setText ( v5 ); // invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
/* .line 2663 */
} // .end local v1 # "pos":I
} // .end local v2 # "progress":I
} // .end local v4 # "total":I
} // :cond_7
return;
} // .end method
