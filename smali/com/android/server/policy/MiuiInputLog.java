public class com.android.server.policy.MiuiInputLog {
	 /* .source "MiuiInputLog.java" */
	 /* # static fields */
	 private static final Integer DETAIL;
	 private static final Integer MAJOR;
	 private static final java.lang.String TAG;
	 private static volatile com.android.server.policy.MiuiInputLog sInstance;
	 /* # instance fields */
	 private Integer mInputDebugLevel;
	 private com.android.server.policy.WindowManagerPolicy mWindowManagerPolicy;
	 /* # direct methods */
	 private com.android.server.policy.MiuiInputLog ( ) {
		 /* .locals 1 */
		 /* .line 21 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 18 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/server/policy/MiuiInputLog;->mInputDebugLevel:I */
		 /* .line 22 */
		 /* const-class v0, Lcom/android/server/policy/WindowManagerPolicy; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Lcom/android/server/policy/WindowManagerPolicy; */
		 this.mWindowManagerPolicy = v0;
		 /* .line 23 */
		 return;
	 } // .end method
	 public static void defaults ( java.lang.String p0 ) {
		 /* .locals 1 */
		 /* .param p0, "msg" # Ljava/lang/String; */
		 /* .line 64 */
		 final String v0 = "MiuiInputKeyEventLog"; // const-string v0, "MiuiInputKeyEventLog"
		 android.util.Slog .w ( v0,p0 );
		 /* .line 65 */
		 return;
	 } // .end method
	 public static void defaults ( java.lang.String p0, java.lang.Throwable p1 ) {
		 /* .locals 1 */
		 /* .param p0, "msg" # Ljava/lang/String; */
		 /* .param p1, "throwable" # Ljava/lang/Throwable; */
		 /* .line 75 */
		 final String v0 = "MiuiInputKeyEventLog"; // const-string v0, "MiuiInputKeyEventLog"
		 android.util.Slog .w ( v0,p0,p1 );
		 /* .line 76 */
		 return;
	 } // .end method
	 public static void detail ( java.lang.String p0 ) {
		 /* .locals 2 */
		 /* .param p0, "msg" # Ljava/lang/String; */
		 /* .line 106 */
		 v0 = 		 com.android.server.policy.MiuiInputLog .getLogLevel ( );
		 int v1 = 1; // const/4 v1, 0x1
		 /* if-lt v0, v1, :cond_0 */
		 /* .line 107 */
		 final String v0 = "MiuiInputKeyEventLog"; // const-string v0, "MiuiInputKeyEventLog"
		 android.util.Slog .d ( v0,p0 );
		 /* .line 109 */
	 } // :cond_0
	 return;
} // .end method
public static void detail ( java.lang.String p0, java.lang.Throwable p1 ) {
	 /* .locals 2 */
	 /* .param p0, "msg" # Ljava/lang/String; */
	 /* .param p1, "throwable" # Ljava/lang/Throwable; */
	 /* .line 119 */
	 v0 = 	 com.android.server.policy.MiuiInputLog .getLogLevel ( );
	 int v1 = 1; // const/4 v1, 0x1
	 /* if-lt v0, v1, :cond_0 */
	 /* .line 120 */
	 final String v0 = "MiuiInputKeyEventLog"; // const-string v0, "MiuiInputKeyEventLog"
	 android.util.Slog .d ( v0,p0,p1 );
	 /* .line 122 */
} // :cond_0
return;
} // .end method
public static void error ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "msg" # Ljava/lang/String; */
/* .line 43 */
final String v0 = "MiuiInputKeyEventLog"; // const-string v0, "MiuiInputKeyEventLog"
android.util.Slog .e ( v0,p0 );
/* .line 44 */
return;
} // .end method
public static void error ( java.lang.String p0, java.lang.Throwable p1 ) {
/* .locals 1 */
/* .param p0, "msg" # Ljava/lang/String; */
/* .param p1, "throwable" # Ljava/lang/Throwable; */
/* .line 54 */
final String v0 = "MiuiInputKeyEventLog"; // const-string v0, "MiuiInputKeyEventLog"
android.util.Slog .e ( v0,p0,p1 );
/* .line 55 */
return;
} // .end method
public static com.android.server.policy.MiuiInputLog getInstance ( ) {
/* .locals 2 */
/* .line 26 */
v0 = com.android.server.policy.MiuiInputLog.sInstance;
/* if-nez v0, :cond_1 */
/* .line 27 */
/* const-class v0, Lcom/android/server/policy/MiuiInputLog; */
/* monitor-enter v0 */
/* .line 28 */
try { // :try_start_0
	 v1 = com.android.server.policy.MiuiInputLog.sInstance;
	 /* if-nez v1, :cond_0 */
	 /* .line 29 */
	 /* new-instance v1, Lcom/android/server/policy/MiuiInputLog; */
	 /* invoke-direct {v1}, Lcom/android/server/policy/MiuiInputLog;-><init>()V */
	 /* .line 31 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 33 */
} // :cond_1
} // :goto_0
v0 = com.android.server.policy.MiuiInputLog.sInstance;
} // .end method
private static Integer getLogLevel ( ) {
/* .locals 1 */
/* .line 125 */
com.android.server.policy.MiuiInputLog .getInstance ( );
/* iget v0, v0, Lcom/android/server/policy/MiuiInputLog;->mInputDebugLevel:I */
} // .end method
public static void major ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "msg" # Ljava/lang/String; */
/* .line 85 */
final String v0 = "MiuiInputKeyEventLog"; // const-string v0, "MiuiInputKeyEventLog"
android.util.Slog .i ( v0,p0 );
/* .line 86 */
return;
} // .end method
public static void major ( java.lang.String p0, java.lang.Throwable p1 ) {
/* .locals 1 */
/* .param p0, "msg" # Ljava/lang/String; */
/* .param p1, "throwable" # Ljava/lang/Throwable; */
/* .line 96 */
final String v0 = "MiuiInputKeyEventLog"; // const-string v0, "MiuiInputKeyEventLog"
android.util.Slog .i ( v0,p0,p1 );
/* .line 97 */
return;
} // .end method
/* # virtual methods */
public void setLogLevel ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "logLevel" # I */
/* .line 134 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setLogLevel:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 135 */
/* iput p1, p0, Lcom/android/server/policy/MiuiInputLog;->mInputDebugLevel:I */
/* .line 136 */
int v0 = 1; // const/4 v0, 0x1
/* if-lt p1, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 137 */
/* .local v0, "flag":Z */
} // :goto_0
v1 = this.mWindowManagerPolicy;
/* instance-of v2, v1, Lcom/android/server/policy/PhoneWindowManager; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 138 */
/* const-class v2, Lcom/android/server/policy/PhoneWindowManager; */
/* check-cast v1, Lcom/android/server/policy/PhoneWindowManager; */
/* .line 139 */
java.lang.Boolean .valueOf ( v0 );
/* filled-new-array {v3}, [Ljava/lang/Object; */
/* .line 138 */
/* const-string/jumbo v4, "setDebugSwitch" */
com.android.server.input.ReflectionUtils .callPrivateMethod ( v2,v1,v4,v3 );
/* .line 141 */
} // :cond_1
return;
} // .end method
