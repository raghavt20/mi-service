.class public Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;
.super Ljava/lang/Object;
.source "PhoneWindowManagerFeatureImpl.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callInterceptPowerKeyUp(Lcom/android/server/policy/PhoneWindowManager;Z)V
    .locals 0
    .param p1, "manager"    # Lcom/android/server/policy/PhoneWindowManager;
    .param p2, "canceled"    # Z

    .line 29
    return-void
.end method

.method public getLock(Lcom/android/server/policy/PhoneWindowManager;)Ljava/lang/Object;
    .locals 2
    .param p1, "phoneWindowManager"    # Lcom/android/server/policy/PhoneWindowManager;

    .line 9
    const-string v0, "mLock"

    const-class v1, Ljava/lang/Object;

    invoke-static {p1, v0, v1}, Lmiui/util/ReflectionUtils;->tryGetObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Lmiui/util/ObjectReference;

    move-result-object v0

    .line 10
    .local v0, "ref":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Object;>;"
    if-nez v0, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;

    move-result-object v1

    :goto_0
    return-object v1
.end method

.method public getPowerLongPress(Lcom/android/server/policy/PhoneWindowManager;)Ljava/lang/Runnable;
    .locals 2
    .param p1, "manager"    # Lcom/android/server/policy/PhoneWindowManager;

    .line 19
    const-string v0, "mEndCallLongPress"

    const-class v1, Ljava/lang/Runnable;

    invoke-static {p1, v0, v1}, Lmiui/util/ReflectionUtils;->tryGetObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Lmiui/util/ObjectReference;

    move-result-object v0

    .line 20
    .local v0, "ref":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Runnable;>;"
    if-nez v0, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    :goto_0
    return-object v1
.end method

.method public getScreenshotChordLongPress(Lcom/android/server/policy/PhoneWindowManager;)Ljava/lang/Runnable;
    .locals 2
    .param p1, "manager"    # Lcom/android/server/policy/PhoneWindowManager;

    .line 14
    const-string v0, "mScreenshotRunnable"

    const-class v1, Ljava/lang/Runnable;

    invoke-static {p1, v0, v1}, Lmiui/util/ReflectionUtils;->tryGetObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Lmiui/util/ObjectReference;

    move-result-object v0

    .line 15
    .local v0, "ref":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Runnable;>;"
    if-nez v0, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    :goto_0
    return-object v1
.end method

.method public isScreenOnFully(Lcom/android/server/policy/PhoneWindowManager;)Z
    .locals 1
    .param p1, "manager"    # Lcom/android/server/policy/PhoneWindowManager;

    .line 32
    iget-object v0, p1, Lcom/android/server/policy/PhoneWindowManager;->mDefaultDisplayPolicy:Lcom/android/server/wm/DisplayPolicy;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 33
    :cond_0
    iget-object v0, p1, Lcom/android/server/policy/PhoneWindowManager;->mDefaultDisplayPolicy:Lcom/android/server/wm/DisplayPolicy;

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayPolicy;->isScreenOnFully()Z

    move-result v0

    return v0
.end method

.method public setPowerLongPress(Lcom/android/server/policy/PhoneWindowManager;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "manager"    # Lcom/android/server/policy/PhoneWindowManager;
    .param p2, "value"    # Ljava/lang/Runnable;

    .line 24
    const-string v0, "mEndCallLongPress"

    invoke-static {p1, v0, p2}, Lmiui/util/ReflectionUtils;->trySetObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 25
    return-void
.end method
