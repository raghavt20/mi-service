.class Lcom/android/server/policy/BaseMiuiPhoneWindowManager$21;
.super Landroid/content/BroadcastReceiver;
.source "BaseMiuiPhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    .line 3015
    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$21;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 3017
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 3018
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.miui.app.ExtraStatusBarManager.action_enter_drive_mode"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3019
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$21;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmForbidFullScreen(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V

    goto :goto_0

    .line 3020
    :cond_0
    const-string v1, "com.miui.app.ExtraStatusBarManager.action_leave_drive_mode"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3021
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$21;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmForbidFullScreen(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V

    .line 3023
    :cond_1
    :goto_0
    return-void
.end method
