class com.android.server.policy.BaseMiuiPhoneWindowManager$3 extends android.view.IDisplayFoldListener$Stub {
	 /* .source "BaseMiuiPhoneWindowManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.policy.BaseMiuiPhoneWindowManager this$0; //synthetic
/* # direct methods */
 com.android.server.policy.BaseMiuiPhoneWindowManager$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
/* .line 623 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/view/IDisplayFoldListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onDisplayFoldChanged ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "displayId" # I */
/* .param p2, "folded" # Z */
/* .line 626 */
v0 = this.this$0;
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmFolded ( v0 );
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, p2, :cond_0 */
/* .line 627 */
v0 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmFolded ( v0,p2 );
/* .line 628 */
v0 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmIsFoldChanged ( v0,v1 );
/* .line 629 */
v0 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$msetTouchFeatureRotation ( v0 );
/* .line 631 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "display changed,display="; // const-string v2, "display changed,display="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " fold="; // const-string v2, " fold="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " mIsFoldChanged="; // const-string v2, " mIsFoldChanged="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmIsFoldChanged ( v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 634 */
v0 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMiuiBackTapGestureService ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 635 */
v0 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMiuiBackTapGestureService ( v0 );
(( com.miui.server.input.MiuiBackTapGestureService ) v0 ).notifyFoldStatus ( p2 ); // invoke-virtual {v0, p2}, Lcom/miui/server/input/MiuiBackTapGestureService;->notifyFoldStatus(Z)V
/* .line 637 */
} // :cond_1
v0 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmProximitySensor ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 638 */
v0 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmProximitySensor ( v0 );
(( com.android.server.policy.MiuiScreenOnProximityLock ) v0 ).release ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->release(Z)Z
/* .line 640 */
} // :cond_2
v0 = this.this$0;
v0 = this.mMiuiThreeGestureListener;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 641 */
v0 = this.this$0;
v0 = this.mMiuiThreeGestureListener;
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager ) v0 ).notifyFoldStatus ( p2 ); // invoke-virtual {v0, p2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->notifyFoldStatus(Z)V
/* .line 643 */
} // :cond_3
v0 = this.this$0;
v0 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).notifyFoldStatus ( p2 ); // invoke-virtual {v0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->notifyFoldStatus(Z)V
/* .line 644 */
return;
} // .end method
