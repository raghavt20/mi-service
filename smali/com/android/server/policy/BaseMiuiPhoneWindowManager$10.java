class com.android.server.policy.BaseMiuiPhoneWindowManager$10 implements java.lang.Runnable {
	 /* .source "BaseMiuiPhoneWindowManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.policy.BaseMiuiPhoneWindowManager this$0; //synthetic
/* # direct methods */
 com.android.server.policy.BaseMiuiPhoneWindowManager$10 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
/* .line 1138 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 1141 */
v0 = this.this$0;
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMetaKeyConsume ( v0 );
/* if-nez v0, :cond_3 */
/* .line 1142 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmMetaKeyConsume ( v0,v1 );
/* .line 1143 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mMetaKeyAction : "; // const-string v2, " mMetaKeyAction : "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMetaKeyAction ( v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .major ( v0 );
/* .line 1144 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1145 */
/* .local v0, "intent":Landroid/content/Intent; */
v2 = this.this$0;
v2 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMetaKeyAction ( v2 );
final String v3 = "com.miui.home"; // const-string v3, "com.miui.home"
/* if-nez v2, :cond_0 */
/* .line 1146 */
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "com.miui.home.action.SINGLE_CLICK"; // const-string v2, "com.miui.home.action.SINGLE_CLICK"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* move-object v0, v1 */
/* .line 1147 */
(( android.content.Intent ) v0 ).setPackage ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1148 */
v1 = this.this$0;
v1 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v1 );
/* const-string/jumbo v2, "\u663e\u793adock" */
(( com.android.server.input.InputOneTrackUtil ) v1 ).trackKeyboardShortcut ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardShortcut(Ljava/lang/String;)V
/* .line 1150 */
} // :cond_0
v2 = this.this$0;
v2 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMetaKeyAction ( v2 );
/* if-ne v2, v1, :cond_1 */
/* .line 1151 */
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "com.miui.home.action.DOUBLE_CLICK"; // const-string v2, "com.miui.home.action.DOUBLE_CLICK"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* move-object v0, v1 */
/* .line 1152 */
(( android.content.Intent ) v0 ).setPackage ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1153 */
v1 = this.this$0;
v1 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v1 );
/* const-string/jumbo v2, "\u5feb\u5207\u6700\u8fd1\u5e94\u7528" */
(( com.android.server.input.InputOneTrackUtil ) v1 ).trackKeyboardShortcut ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardShortcut(Ljava/lang/String;)V
/* .line 1155 */
} // :cond_1
v2 = this.this$0;
v2 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmMetaKeyAction ( v2 );
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_2 */
/* .line 1156 */
v2 = this.this$0;
v3 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmKeyBoardDeviceId ( v2 );
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$mshowKeyboardShortcutsMenu ( v2,v3,v1,v1 );
/* .line 1157 */
v1 = this.this$0;
v1 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v1 );
/* const-string/jumbo v2, "\u663e\u793a\u5feb\u6377\u952e\u5217\u8868" */
(( com.android.server.input.InputOneTrackUtil ) v1 ).trackKeyboardShortcut ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardShortcut(Ljava/lang/String;)V
/* .line 1160 */
} // :cond_2
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1161 */
v1 = this.this$0;
final String v2 = "miui.permission.USE_INTERNAL_GENERAL_API"; // const-string v2, "miui.permission.USE_INTERNAL_GENERAL_API"
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) v1 ).sendAsyncBroadcast ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
/* .line 1164 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_3
return;
} // .end method
