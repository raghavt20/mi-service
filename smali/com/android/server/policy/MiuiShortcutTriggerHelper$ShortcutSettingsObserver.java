class com.android.server.policy.MiuiShortcutTriggerHelper$ShortcutSettingsObserver extends android.database.ContentObserver {
	 /* .source "MiuiShortcutTriggerHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/policy/MiuiShortcutTriggerHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ShortcutSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.policy.MiuiShortcutTriggerHelper this$0; //synthetic
/* # direct methods */
public com.android.server.policy.MiuiShortcutTriggerHelper$ShortcutSettingsObserver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/policy/MiuiShortcutTriggerHelper; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 232 */
this.this$0 = p1;
/* .line 233 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 234 */
return;
} // .end method
/* # virtual methods */
public void initShortcutSettingsObserver ( ) {
/* .locals 7 */
/* .line 239 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 240 */
final String v1 = "long_press_power_launch_xiaoai"; // const-string v1, "long_press_power_launch_xiaoai"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 239 */
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 243 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 244 */
final String v1 = "long_press_power_launch_smarthome"; // const-string v1, "long_press_power_launch_smarthome"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 243 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v4, v2, p0, v3 ); // invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 247 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 248 */
final String v4 = "screen_key_press_app_switch"; // const-string v4, "screen_key_press_app_switch"
android.provider.Settings$System .getUriFor ( v4 );
/* .line 247 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v4, v2, p0, v3 ); // invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 250 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 251 */
final String v4 = "fingerprint_nav_center_action"; // const-string v4, "fingerprint_nav_center_action"
android.provider.Settings$System .getUriFor ( v4 );
/* .line 250 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v4, v2, p0, v3 ); // invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 253 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 254 */
/* const-string/jumbo v4, "single_key_use_enable" */
android.provider.Settings$System .getUriFor ( v4 );
/* .line 253 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v4, v2, p0, v3 ); // invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 257 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 258 */
final String v4 = "long_press_menu_key_when_lock"; // const-string v4, "long_press_menu_key_when_lock"
android.provider.Settings$System .getUriFor ( v4 );
/* .line 257 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v4, v2, p0, v3 ); // invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 261 */
/* sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z */
/* if-nez v0, :cond_0 */
/* .line 262 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* const-string/jumbo v4, "xiaoai_power_guide" */
android.provider.Settings$System .getUriFor ( v4 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v5, v2, p0, v3 ); // invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 266 */
android.provider.Settings$System .getUriFor ( v4 );
(( com.android.server.policy.MiuiShortcutTriggerHelper$ShortcutSettingsObserver ) p0 ).onChange ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 269 */
} // :cond_0
v0 = this.this$0;
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).supportRSARegion ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->supportRSARegion()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 270 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
final String v4 = "global_power_guide"; // const-string v4, "global_power_guide"
android.provider.Settings$System .getUriFor ( v4 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v5, v2, p0, v3 ); // invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 273 */
android.provider.Settings$System .getUriFor ( v4 );
(( com.android.server.policy.MiuiShortcutTriggerHelper$ShortcutSettingsObserver ) p0 ).onChange ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 276 */
} // :cond_1
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 277 */
/* const-string/jumbo v4, "user_setup_complete" */
android.provider.Settings$Secure .getUriFor ( v4 );
/* .line 276 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v5, v2, p0, v3 ); // invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 280 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 281 */
final String v5 = "pc_mode_open"; // const-string v5, "pc_mode_open"
android.provider.Settings$System .getUriFor ( v5 );
/* .line 280 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v5, v2, p0, v3 ); // invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 283 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 284 */
android.provider.Settings$System .getUriFor ( v1 );
/* .line 283 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 287 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 288 */
final String v1 = "emergency_gesture_enabled"; // const-string v1, "emergency_gesture_enabled"
android.provider.Settings$Secure .getUriFor ( v1 );
/* .line 287 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 290 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 291 */
final String v1 = "key_miui_sos_enable"; // const-string v1, "key_miui_sos_enable"
android.provider.Settings$Secure .getUriFor ( v1 );
/* .line 290 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v5, v2, p0, v3 ); // invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 293 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 294 */
final String v5 = "key_is_in_miui_sos_mode"; // const-string v5, "key_is_in_miui_sos_mode"
android.provider.Settings$Secure .getUriFor ( v5 );
/* .line 293 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v6, v2, p0, v3 ); // invoke-virtual {v0, v6, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 297 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 298 */
final String v6 = "imperceptible_press_power_key"; // const-string v6, "imperceptible_press_power_key"
android.provider.Settings$Global .getUriFor ( v6 );
/* .line 297 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v6, v2, p0 ); // invoke-virtual {v0, v6, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 300 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 301 */
android.provider.Settings$Secure .getUriFor ( v1 );
/* .line 300 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0 ); // invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 302 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 303 */
android.provider.Settings$Secure .getUriFor ( v5 );
/* .line 302 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0 ); // invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 304 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
final String v1 = "long_press_timeout"; // const-string v1, "long_press_timeout"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 306 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* const-string/jumbo v1, "torch_state" */
android.provider.Settings$Global .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0 ); // invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 308 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 309 */
final String v1 = "assist_long_press_home_enabled"; // const-string v1, "assist_long_press_home_enabled"
android.provider.Settings$Secure .getUriFor ( v1 );
/* .line 308 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 311 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
v1 = android.provider.MiuiSettings$Secure.MIUI_OPTIMIZATION;
/* .line 312 */
android.provider.Settings$Secure .getUriFor ( v1 );
/* .line 311 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 314 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 315 */
android.provider.Settings$Secure .getUriFor ( v4 );
/* .line 314 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 317 */
(( com.android.server.policy.MiuiShortcutTriggerHelper$ShortcutSettingsObserver ) p0 ).onChange ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->onChange(Z)V
/* .line 318 */
return;
} // .end method
public void onChange ( Boolean p0 ) {
/* .locals 7 */
/* .param p1, "selfChange" # Z */
/* .line 322 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
v2 = this.this$0;
v2 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v2 );
final String v3 = "long_press_power_launch_xiaoai"; // const-string v3, "long_press_power_launch_xiaoai"
int v4 = 0; // const/4 v4, 0x0
v1 = android.provider.Settings$System .getIntForUser ( v1,v3,v4,v2 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
/* move v1, v2 */
} // :cond_0
/* move v1, v4 */
} // :goto_0
/* iput-boolean v1, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mLongPressPowerKeyLaunchXiaoAi:Z */
/* .line 328 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
v3 = this.this$0;
v3 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v3 );
final String v5 = "screen_key_press_app_switch"; // const-string v5, "screen_key_press_app_switch"
v1 = android.provider.Settings$System .getIntForUser ( v1,v5,v2,v3 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* move v1, v2 */
} // :cond_1
/* move v1, v4 */
} // :goto_1
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmPressToAppSwitch ( v0,v1 );
/* .line 332 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
v3 = this.this$0;
v3 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v3 );
final String v5 = "fingerprint_nav_center_action"; // const-string v5, "fingerprint_nav_center_action"
int v6 = -1; // const/4 v6, -0x1
v1 = android.provider.Settings$System .getIntForUser ( v1,v5,v6,v3 );
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmFingerPrintNavCenterAction ( v0,v1 );
/* .line 335 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
v3 = this.this$0;
v3 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v3 );
/* const-string/jumbo v5, "single_key_use_enable" */
v1 = android.provider.Settings$System .getIntForUser ( v1,v5,v4,v3 );
/* if-ne v1, v2, :cond_2 */
/* move v1, v2 */
} // :cond_2
/* move v1, v4 */
} // :goto_2
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmSingleKeyUse ( v0,v1 );
/* .line 338 */
v0 = this.this$0;
/* .line 339 */
v1 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmSingleKeyUse ( v0 );
/* iput v1, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDoubleTapOnHomeBehavior:I */
/* .line 340 */
/* sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z */
/* if-nez v0, :cond_3 */
/* .line 341 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
v3 = this.this$0;
v3 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v3 );
/* const-string/jumbo v5, "xiaoai_power_guide" */
v1 = android.provider.Settings$System .getIntForUser ( v1,v5,v2,v3 );
/* iput v1, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mXiaoaiPowerGuideFlag:I */
/* .line 347 */
} // :cond_3
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
v3 = this.this$0;
v3 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v3 );
final String v5 = "global_power_guide"; // const-string v5, "global_power_guide"
v1 = android.provider.Settings$System .getIntForUser ( v1,v5,v2,v3 );
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmRSAGuideStatus ( v0,v1 );
/* .line 351 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
v3 = this.this$0;
v3 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v3 );
final String v5 = "emergency_gesture_enabled"; // const-string v5, "emergency_gesture_enabled"
android.provider.Settings$Secure .getStringForUser ( v1,v5,v3 );
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmFivePressPowerLaunchGoogleSos ( v0,v1 );
/* .line 353 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
v3 = this.this$0;
v3 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v3 );
final String v5 = "key_miui_sos_enable"; // const-string v5, "key_miui_sos_enable"
v1 = android.provider.Settings$Secure .getIntForUser ( v1,v5,v4,v3 );
/* if-ne v1, v2, :cond_4 */
/* move v1, v2 */
} // :cond_4
/* move v1, v4 */
} // :goto_3
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmFivePressPowerLaunchSos ( v0,v1 );
/* .line 355 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
v3 = this.this$0;
v3 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v3 );
final String v5 = "key_is_in_miui_sos_mode"; // const-string v5, "key_is_in_miui_sos_mode"
v1 = android.provider.Settings$Secure .getIntForUser ( v1,v5,v4,v3 );
/* if-ne v1, v2, :cond_5 */
/* move v1, v2 */
} // :cond_5
/* move v1, v4 */
} // :goto_4
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmIsOnSosMode ( v0,v1 );
/* .line 357 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
v3 = this.this$0;
v3 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v3 );
final String v5 = "long_press_timeout"; // const-string v5, "long_press_timeout"
v1 = android.provider.Settings$Secure .getIntForUser ( v1,v5,v4,v3 );
/* iput v1, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDefaultLongPressTimeOut:I */
/* .line 359 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* const-string/jumbo v3, "torch_state" */
v1 = android.provider.Settings$Global .getInt ( v1,v3,v4 );
if ( v1 != null) { // if-eqz v1, :cond_6
/* move v1, v2 */
} // :cond_6
/* move v1, v4 */
} // :goto_5
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmTorchEnabled ( v0,v1 );
/* .line 361 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
final String v3 = "assist_long_press_home_enabled"; // const-string v3, "assist_long_press_home_enabled"
v1 = android.provider.Settings$Secure .getInt ( v1,v3,v2 );
/* if-ne v1, v2, :cond_7 */
/* move v4, v2 */
} // :cond_7
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmAOSPAssistantLongPressHomeEnabled ( v0,v4 );
/* .line 363 */
v0 = this.this$0;
/* .line 364 */
final String v1 = "ro.miui.cts"; // const-string v1, "ro.miui.cts"
android.os.SystemProperties .get ( v1 );
final String v3 = "1"; // const-string v3, "1"
v1 = (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 363 */
/* xor-int/2addr v1, v2 */
final String v3 = "persist.sys.miui_optimization"; // const-string v3, "persist.sys.miui_optimization"
v1 = android.os.SystemProperties .getBoolean ( v3,v1 );
/* xor-int/2addr v1, v2 */
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmIsCtsMode ( v0,v1 );
/* .line 365 */
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 7 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 369 */
final String v0 = "long_press_power_launch_xiaoai"; // const-string v0, "long_press_power_launch_xiaoai"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
final String v2 = "long_press_power_key"; // const-string v2, "long_press_power_key"
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 371 */
v1 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v1 );
v6 = this.this$0;
v6 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v6 );
v0 = android.provider.Settings$System .getIntForUser ( v5,v0,v3,v6 );
/* if-ne v0, v4, :cond_0 */
/* move v3, v4 */
} // :cond_0
/* iput-boolean v3, v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mLongPressPowerKeyLaunchXiaoAi:Z */
/* .line 377 */
/* sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 378 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
/* .line 380 */
v1 = this.this$0;
/* iget-boolean v1, v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mLongPressPowerKeyLaunchXiaoAi:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
final String v1 = "launch_google_search"; // const-string v1, "launch_google_search"
/* .line 381 */
} // :cond_1
final String v1 = "none"; // const-string v1, "none"
} // :goto_0
v3 = this.this$0;
v3 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v3 );
/* .line 378 */
android.provider.Settings$System .putStringForUser ( v0,v2,v1,v3 );
/* goto/16 :goto_1 */
/* .line 383 */
} // :cond_2
v0 = this.this$0;
/* iget-boolean v0, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mLongPressPowerKeyLaunchXiaoAi:Z */
if ( v0 != null) { // if-eqz v0, :cond_16
/* .line 384 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
v1 = this.this$0;
v1 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v1 );
final String v3 = "launch_voice_assistant"; // const-string v3, "launch_voice_assistant"
android.provider.Settings$System .putStringForUser ( v0,v2,v3,v1 );
/* goto/16 :goto_1 */
/* .line 388 */
} // :cond_3
final String v0 = "long_press_power_launch_smarthome"; // const-string v0, "long_press_power_launch_smarthome"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 389 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 391 */
v1 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v1 );
v6 = this.this$0;
v6 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v6 );
v0 = android.provider.Settings$System .getIntForUser ( v5,v0,v3,v6 );
/* if-ne v0, v4, :cond_4 */
/* move v3, v4 */
} // :cond_4
/* iput-boolean v3, v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mLongPressPowerKeyLaunchSmartHome:Z */
/* .line 395 */
v0 = this.this$0;
/* iget-boolean v0, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mLongPressPowerKeyLaunchSmartHome:Z */
if ( v0 != null) { // if-eqz v0, :cond_16
/* .line 396 */
v0 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v0 );
v1 = this.this$0;
v1 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v1 );
final String v3 = "launch_smarthome"; // const-string v3, "launch_smarthome"
android.provider.Settings$System .putStringForUser ( v0,v2,v3,v1 );
/* goto/16 :goto_1 */
/* .line 400 */
} // :cond_5
final String v0 = "screen_key_press_app_switch"; // const-string v0, "screen_key_press_app_switch"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 401 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 402 */
v1 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v1 );
v5 = this.this$0;
v5 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v5 );
v0 = android.provider.Settings$System .getIntForUser ( v2,v0,v4,v5 );
if ( v0 != null) { // if-eqz v0, :cond_6
/* move v3, v4 */
} // :cond_6
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmPressToAppSwitch ( v1,v3 );
/* goto/16 :goto_1 */
/* .line 406 */
} // :cond_7
final String v0 = "fingerprint_nav_center_action"; // const-string v0, "fingerprint_nav_center_action"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 407 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
int v2 = -1; // const/4 v2, -0x1
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 408 */
v1 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v1 );
v4 = this.this$0;
v4 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v4 );
v0 = android.provider.Settings$System .getIntForUser ( v3,v0,v2,v4 );
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmFingerPrintNavCenterAction ( v1,v0 );
/* goto/16 :goto_1 */
/* .line 412 */
} // :cond_8
/* const-string/jumbo v0, "single_key_use_enable" */
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 414 */
v1 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v1 );
v5 = this.this$0;
v5 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v5 );
v0 = android.provider.Settings$System .getIntForUser ( v2,v0,v3,v5 );
/* if-ne v0, v4, :cond_9 */
/* move v3, v4 */
} // :cond_9
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmSingleKeyUse ( v1,v3 );
/* .line 417 */
v0 = this.this$0;
/* .line 418 */
v1 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmSingleKeyUse ( v0 );
/* iput v1, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDoubleTapOnHomeBehavior:I */
/* goto/16 :goto_1 */
/* .line 419 */
} // :cond_a
/* const-string/jumbo v0, "xiaoai_power_guide" */
android.provider.Settings$System .getUriFor ( v0 );
/* .line 420 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_b
/* .line 421 */
v1 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v3 = this.this$0;
v3 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v3 );
v0 = android.provider.Settings$System .getIntForUser ( v2,v0,v4,v3 );
/* iput v0, v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mXiaoaiPowerGuideFlag:I */
/* goto/16 :goto_1 */
/* .line 425 */
} // :cond_b
final String v0 = "emergency_gesture_enabled"; // const-string v0, "emergency_gesture_enabled"
android.provider.Settings$Secure .getUriFor ( v0 );
/* .line 426 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_c
/* .line 427 */
v1 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v1 );
v3 = this.this$0;
v3 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v3 );
android.provider.Settings$Secure .getStringForUser ( v2,v0,v3 );
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmFivePressPowerLaunchGoogleSos ( v1,v0 );
/* goto/16 :goto_1 */
/* .line 429 */
} // :cond_c
final String v0 = "key_miui_sos_enable"; // const-string v0, "key_miui_sos_enable"
android.provider.Settings$Secure .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_e
/* .line 430 */
v1 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v1 );
v5 = this.this$0;
v5 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v5 );
v0 = android.provider.Settings$Secure .getIntForUser ( v2,v0,v3,v5 );
/* if-ne v0, v4, :cond_d */
/* move v3, v4 */
} // :cond_d
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmFivePressPowerLaunchSos ( v1,v3 );
/* goto/16 :goto_1 */
/* .line 432 */
} // :cond_e
final String v0 = "key_is_in_miui_sos_mode"; // const-string v0, "key_is_in_miui_sos_mode"
android.provider.Settings$Secure .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_10
/* .line 433 */
v1 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v1 );
v5 = this.this$0;
v5 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v5 );
v0 = android.provider.Settings$Secure .getIntForUser ( v2,v0,v3,v5 );
/* if-ne v0, v4, :cond_f */
/* move v3, v4 */
} // :cond_f
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmIsOnSosMode ( v1,v3 );
/* goto/16 :goto_1 */
/* .line 435 */
} // :cond_10
final String v0 = "global_power_guide"; // const-string v0, "global_power_guide"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 436 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_11
/* .line 437 */
v1 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v1 );
v3 = this.this$0;
v3 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v3 );
v0 = android.provider.Settings$System .getIntForUser ( v2,v0,v4,v3 );
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmRSAGuideStatus ( v1,v0 );
/* goto/16 :goto_1 */
/* .line 442 */
} // :cond_11
final String v0 = "long_press_timeout"; // const-string v0, "long_press_timeout"
android.provider.Settings$Secure .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_12
/* .line 443 */
v1 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v1 );
v4 = this.this$0;
v4 = com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmCurrentUserId ( v4 );
v0 = android.provider.Settings$Secure .getIntForUser ( v2,v0,v3,v4 );
/* iput v0, v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDefaultLongPressTimeOut:I */
/* .line 445 */
} // :cond_12
final String v0 = "assist_long_press_home_enabled"; // const-string v0, "assist_long_press_home_enabled"
android.provider.Settings$Secure .getUriFor ( v0 );
/* .line 446 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_14
/* .line 447 */
v1 = this.this$0;
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fgetmContentResolver ( v1 );
v0 = android.provider.Settings$Secure .getInt ( v5,v0,v2 );
/* if-ne v0, v4, :cond_13 */
/* move v3, v4 */
} // :cond_13
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmAOSPAssistantLongPressHomeEnabled ( v1,v3 );
/* .line 449 */
} // :cond_14
v0 = android.provider.MiuiSettings$Secure.MIUI_OPTIMIZATION;
android.provider.Settings$Secure .getUriFor ( v0 );
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_15
/* .line 451 */
v0 = this.this$0;
/* .line 452 */
final String v1 = "ro.miui.cts"; // const-string v1, "ro.miui.cts"
android.os.SystemProperties .get ( v1 );
final String v2 = "1"; // const-string v2, "1"
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 451 */
/* xor-int/2addr v1, v4 */
final String v2 = "persist.sys.miui_optimization"; // const-string v2, "persist.sys.miui_optimization"
v1 = android.os.SystemProperties .getBoolean ( v2,v1 );
/* xor-int/2addr v1, v4 */
com.android.server.policy.MiuiShortcutTriggerHelper .-$$Nest$fputmIsCtsMode ( v0,v1 );
/* .line 453 */
} // :cond_15
/* const-string/jumbo v0, "user_setup_complete" */
android.provider.Settings$Secure .getUriFor ( v0 );
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_16
/* .line 454 */
v0 = this.this$0;
(( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).setVeryLongPressPowerBehavior ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setVeryLongPressPowerBehavior()V
/* .line 456 */
} // :cond_16
} // :goto_1
/* invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V */
/* .line 457 */
return;
} // .end method
