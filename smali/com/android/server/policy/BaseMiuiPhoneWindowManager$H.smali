.class Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;
.super Landroid/os/Handler;
.source "BaseMiuiPhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# static fields
.field static final MSG_DISPATCH_SHOW_RECENTS:I = 0x3

.field static final MSG_KEY_DELAY_POWER:I = 0x2

.field static final MSG_KEY_FUNCTION:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;


# direct methods
.method private constructor <init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0

    .line 1863
    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .line 1870
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "shortcut"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "long_press_power_key_four_second"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1871
    const-string/jumbo v0, "trigger dump power log"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 1872
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmStabilityLocalServiceInternal(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/miui/server/stability/StabilityLocalServiceInternal;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1873
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmStabilityLocalServiceInternal(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/miui/server/stability/StabilityLocalServiceInternal;

    move-result-object v0

    invoke-interface {v0}, Lcom/miui/server/stability/StabilityLocalServiceInternal;->crawlLogsByPower()V

    .line 1875
    :cond_0
    return-void

    .line 1878
    :cond_1
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmIsKidMode(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1879
    const-string v0, "Children\'s space does not trigger shortcut gestures"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 1880
    return-void

    .line 1883
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 1884
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "power_up_event"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/view/KeyEvent;

    .line 1885
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "power_up_policy"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 1886
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "power_up_screen_state"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 1884
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->callSuperInterceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I

    goto/16 :goto_4

    .line 1888
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_16

    .line 1890
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v0, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_4

    return-void

    .line 1892
    :cond_4
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1893
    .local v0, "shortcut":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1894
    .local v1, "triggered":Z
    const-string/jumbo v3, "virtual_key_longpress"

    .line 1895
    .local v3, "effectKey":Ljava/lang/String;
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    .line 1896
    .local v4, "action":Ljava/lang/String;
    if-nez v4, :cond_5

    .line 1897
    return-void

    .line 1899
    :cond_5
    iget-object v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-virtual {v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isUserSetupComplete()Z

    move-result v5

    const-string v6, "close_talkback"

    const-string v7, "find_device_locate"

    const-string v8, "dump_log"

    if-nez v5, :cond_6

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 1900
    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 1901
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 1902
    const-string/jumbo v2, "user setup not complete, does not trigger shortcut"

    invoke-static {v2}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 1903
    return-void

    .line 1905
    :cond_6
    const-string v5, "launch_camera"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    const/4 v10, 0x0

    if-eqz v9, :cond_7

    .line 1906
    iget-object v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v6, v6, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v6

    invoke-virtual {v6, v5, v0, v10, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v1

    goto/16 :goto_2

    .line 1908
    :cond_7
    const-string/jumbo v5, "screen_shot"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    const/4 v11, 0x0

    if-eqz v9, :cond_8

    .line 1909
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v2, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v2

    invoke-virtual {v2, v5, v0, v10, v11}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v1

    .line 1911
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v2, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "screenshot"

    const-string v6, "key_shortcut"

    invoke-static {v2, v5, v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->sendRecordCountEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1912
    :cond_8
    const-string v5, "partial_screen_shot"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1913
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v2, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v2

    invoke-virtual {v2, v5, v0, v10, v11}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v1

    goto/16 :goto_2

    .line 1915
    :cond_9
    const-string v5, "go_to_sleep"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 1916
    iget-object v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v6, v6, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v6

    invoke-virtual {v6, v5, v0, v10, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v1

    goto/16 :goto_2

    .line 1918
    :cond_a
    const-string/jumbo v5, "turn_on_torch"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1919
    iget-object v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-virtual {v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getTelecommService()Landroid/telecom/TelecomManager;

    move-result-object v6

    .line 1920
    .local v6, "telecomManager":Landroid/telecom/TelecomManager;
    iget-object v7, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v7}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmWifiOnly(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v7

    if-nez v7, :cond_c

    if-eqz v6, :cond_b

    .line 1921
    invoke-virtual {v6}, Landroid/telecom/TelecomManager;->getCallState()I

    move-result v7

    if-nez v7, :cond_b

    goto :goto_0

    :cond_b
    goto :goto_1

    :cond_c
    :goto_0
    move v11, v2

    :goto_1
    move v7, v11

    .line 1922
    .local v7, "phoneIdle":Z
    if-eqz v7, :cond_d

    .line 1923
    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v8, v8, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v8

    invoke-virtual {v8, v5, v0, v10, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v1

    .line 1926
    .end local v6    # "telecomManager":Landroid/telecom/TelecomManager;
    .end local v7    # "phoneIdle":Z
    :cond_d
    goto/16 :goto_2

    :cond_e
    const-string v5, "close_app"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 1927
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v2, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    .line 1928
    const-string v6, "long_press_back_key"

    invoke-virtual {v2, v6}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1927
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 1930
    .local v2, "isTriggeredByBack":Z
    iget-object v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmShortcutOneTrackHelper(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    move-result-object v6

    invoke-virtual {v6, v0, v5}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V

    .line 1932
    iget-object v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-virtual {v5, v4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->triggerCloseApp(Ljava/lang/String;)Z

    move-result v1

    .line 1934
    .end local v2    # "isTriggeredByBack":Z
    goto/16 :goto_2

    :cond_f
    const-string/jumbo v5, "show_menu"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 1935
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmShortcutOneTrackHelper(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    move-result-object v2

    invoke-virtual {v2, v0, v5}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V

    .line 1937
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$mshowMenu(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v1

    goto/16 :goto_2

    .line 1938
    :cond_10
    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 1939
    iget-object v5, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v5, v5, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v5

    invoke-virtual {v5, v8, v0, v10, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v1

    goto/16 :goto_2

    .line 1941
    :cond_11
    const-string v5, "launch_recents"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_12

    .line 1942
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmShortcutOneTrackHelper(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    move-result-object v2

    invoke-virtual {v2, v0, v5}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V

    .line 1944
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-virtual {v2, v4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->triggerLaunchRecent(Ljava/lang/String;)Z

    move-result v1

    .line 1945
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmHapticFeedbackUtil(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lmiui/util/HapticFeedbackUtil;

    move-result-object v2

    invoke-virtual {v2, v3, v11}, Lmiui/util/HapticFeedbackUtil;->performHapticFeedback(Ljava/lang/String;Z)Z

    goto :goto_2

    .line 1946
    :cond_12
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 1947
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmShortcutOneTrackHelper(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    move-result-object v2

    invoke-virtual {v2, v0, v6}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V

    .line 1949
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$mcloseTalkBack(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    .line 1950
    const/4 v1, 0x1

    goto :goto_2

    .line 1951
    :cond_13
    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 1952
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v2, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v2

    invoke-virtual {v2, v7, v0, v10, v11}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v1

    goto :goto_2

    .line 1954
    :cond_14
    const-string v5, "key_long_press_volume_up"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 1955
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.FACTORY_RESET"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1956
    .local v5, "intent":Landroid/content/Intent;
    const-string v6, "android"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1957
    const/high16 v6, 0x10000000

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1958
    const-string v6, "android.intent.extra.WIPE_EXTERNAL_STORAGE"

    invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1959
    const-string v6, "com.android.internal.intent.extra.WIPE_ESIMS"

    invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1960
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v2, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1962
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_15
    :goto_2
    if-eqz v1, :cond_17

    .line 1963
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$mmarkShortcutTriggered(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    goto :goto_3

    .line 1965
    .end local v0    # "shortcut":Ljava/lang/String;
    .end local v1    # "triggered":Z
    .end local v3    # "effectKey":Ljava/lang/String;
    .end local v4    # "action":Ljava/lang/String;
    :cond_16
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_17

    .line 1966
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1967
    .local v0, "triggeredFromAltTab":Z
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v1, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$mshowRecentApps(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V

    goto :goto_4

    .line 1965
    .end local v0    # "triggeredFromAltTab":Z
    :cond_17
    :goto_3
    nop

    .line 1969
    :goto_4
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 1970
    return-void
.end method
