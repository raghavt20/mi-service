.class public Lcom/android/server/policy/MiuiScreenOnProximityLock;
.super Ljava/lang/Object;
.source "MiuiScreenOnProximityLock.java"


# static fields
.field private static final DEBUG:Z = true

.field private static final EVENT_PREPARE_VIEW:I = 0x1

.field private static final EVENT_RELEASE:I = 0x0

.field private static final EVENT_RELEASE_VIEW:I = 0x3

.field private static final EVENT_SET_HINT_CONTAINER:I = 0x4

.field private static final EVENT_SHOW_VIEW:I = 0x2

.field private static final FIRST_CHANGE_TIMEOUT:I = 0x5dc

.field public static final IS_JP_KDDI:Z

.field private static final LOG_TAG:Ljava/lang/String; = "MiuiScreenOnProximityLock"

.field public static final SKIP_AQUIRE_FINGER_WAKE_UP_DETAIL:Ljava/lang/String; = "android.policy:FINGERPRINT"

.field public static final SKIP_AQUIRE_UNFOLD_WAKE_UP_DETAIL:Ljava/lang/String; = "server.display:unfold"


# instance fields
.field private mAquiredTime:J

.field private mContext:Landroid/content/Context;

.field protected mFrontFingerprintSensor:Z

.field private mHandler:Landroid/os/Handler;

.field private mHideNavigationBarWhenForceShow:Z

.field protected mHintContainer:Landroid/view/ViewGroup;

.field protected mHintView:Landroid/view/View;

.field protected mKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

.field private mMiuiPocketModeManager:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

.field private final mSensorListener:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;


# direct methods
.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/policy/MiuiScreenOnProximityLock;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mforceShow(Lcom/android/server/policy/MiuiScreenOnProximityLock;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->forceShow()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mprepareHintWindow(Lcom/android/server/policy/MiuiScreenOnProximityLock;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->prepareHintWindow()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreleaseHintWindow(Lcom/android/server/policy/MiuiScreenOnProximityLock;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->releaseHintWindow(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreleaseReset(Lcom/android/server/policy/MiuiScreenOnProximityLock;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->releaseReset(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetHintContainer(Lcom/android/server/policy/MiuiScreenOnProximityLock;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->setHintContainer()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshowHint(Lcom/android/server/policy/MiuiScreenOnProximityLock;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->showHint()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 57
    sget-boolean v0, Lmiui/hardware/input/InputFeature;->IS_SUPPORT_KDDI:Z

    sput-boolean v0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->IS_JP_KDDI:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/policy/MiuiKeyguardServiceDelegate;Landroid/os/Looper;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keyguardDelegate"    # Lcom/android/server/policy/MiuiKeyguardServiceDelegate;
    .param p3, "looper"    # Landroid/os/Looper;

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mAquiredTime:J

    .line 72
    new-instance v0, Lcom/android/server/policy/MiuiScreenOnProximityLock$1;

    invoke-direct {v0, p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock$1;-><init>(Lcom/android/server/policy/MiuiScreenOnProximityLock;)V

    iput-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mSensorListener:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;

    .line 84
    iput-object p1, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mContext:Landroid/content/Context;

    .line 85
    iput-object p2, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    .line 86
    new-instance v0, Lcom/android/server/policy/MiuiScreenOnProximityLock$2;

    invoke-direct {v0, p0, p3}, Lcom/android/server/policy/MiuiScreenOnProximityLock$2;-><init>(Lcom/android/server/policy/MiuiScreenOnProximityLock;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHandler:Landroid/os/Handler;

    .line 115
    nop

    .line 116
    const-string v0, "front_fingerprint_sensor"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mFrontFingerprintSensor:Z

    .line 117
    new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    iget-object v1, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mMiuiPocketModeManager:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    .line 118
    return-void
.end method

.method private forceShow()V
    .locals 2

    .line 199
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 200
    return-void
.end method

.method private modTipsForKddi()V
    .locals 3

    .line 323
    sget-boolean v0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->IS_JP_KDDI:Z

    if-nez v0, :cond_0

    .line 324
    return-void

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintView:Landroid/view/View;

    .line 327
    const v1, 0x110a00c0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 328
    .local v0, "summaryTextView":Landroid/widget/TextView;
    if-eqz v0, :cond_1

    .line 329
    nop

    .line 330
    const v1, 0x110f0336

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 332
    :cond_1
    iget-object v1, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintView:Landroid/view/View;

    .line 333
    const v2, 0x110a00bf

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 334
    .local v1, "hintHasNavigationBarTextView":Landroid/widget/TextView;
    if-eqz v1, :cond_2

    .line 335
    nop

    .line 336
    const v2, 0x110f0334

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 338
    :cond_2
    return-void
.end method

.method private prepareHintWindow()V
    .locals 8

    .line 203
    new-instance v0, Landroid/widget/FrameLayout;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mContext:Landroid/content/Context;

    const v3, 0x103006b

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintContainer:Landroid/view/ViewGroup;

    .line 204
    new-instance v1, Lcom/android/server/policy/MiuiScreenOnProximityLock$3;

    invoke-direct {v1, p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock$3;-><init>(Lcom/android/server/policy/MiuiScreenOnProximityLock;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 212
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/16 v5, 0x7e2

    const v6, 0x1831100

    const/4 v7, -0x3

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 224
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    .line 225
    const/4 v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    .line 226
    const/16 v1, 0x11

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 227
    const-string v1, "ScreenOnProximitySensorGuide"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v1, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 229
    .local v1, "wm":Landroid/view/WindowManager;
    iget-object v2, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintContainer:Landroid/view/ViewGroup;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 231
    iget-object v2, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->enableUserActivity(Z)V

    .line 232
    return-void
.end method

.method private releaseHintWindow(Z)V
    .locals 5
    .param p1, "isNowRelease"    # Z

    .line 235
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintContainer:Landroid/view/ViewGroup;

    .line 236
    .local v0, "container":Landroid/view/View;
    if-nez v0, :cond_0

    return-void

    .line 238
    :cond_0
    iget-object v1, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintView:Landroid/view/View;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    .line 239
    iget-object v1, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "window"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 240
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1, v0}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 241
    .end local v1    # "wm":Landroid/view/WindowManager;
    goto :goto_0

    .line 242
    :cond_1
    if-eqz p1, :cond_2

    .line 243
    invoke-direct {p0, v0, v1}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->releaseReset(Landroid/view/View;Landroid/view/View;)V

    .line 244
    iput-object v2, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintView:Landroid/view/View;

    goto :goto_0

    .line 246
    :cond_2
    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v4, 0x2

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    invoke-static {v1, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 247
    .local v1, "animator":Landroid/animation/ObjectAnimator;
    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 248
    new-instance v3, Lcom/android/server/policy/MiuiScreenOnProximityLock$4;

    invoke-direct {v3, p0, v0}, Lcom/android/server/policy/MiuiScreenOnProximityLock$4;-><init>(Lcom/android/server/policy/MiuiScreenOnProximityLock;Landroid/view/View;)V

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 267
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 268
    iput-object v2, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintView:Landroid/view/View;

    .line 272
    .end local v1    # "animator":Landroid/animation/ObjectAnimator;
    :goto_0
    iget-object v1, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    invoke-virtual {v1}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z

    move-result v1

    if-nez v1, :cond_3

    .line 273
    iget-object v1, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->enableUserActivity(Z)V

    .line 275
    :cond_3
    iput-object v2, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintContainer:Landroid/view/ViewGroup;

    .line 276
    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private releaseReset(Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p1, "container"    # Landroid/view/View;
    .param p2, "hintView"    # Landroid/view/View;

    .line 279
    if-eqz p2, :cond_0

    .line 280
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 281
    invoke-virtual {p2}, Landroid/view/View;->clearAnimation()V

    .line 284
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHideNavigationBarWhenForceShow:Z

    if-eqz v0, :cond_1

    .line 285
    const/16 v0, 0xf00

    invoke-virtual {p1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 290
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHideNavigationBarWhenForceShow:Z

    .line 292
    :cond_1
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 293
    .local v0, "wm":Landroid/view/WindowManager;
    invoke-interface {v0, p1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 294
    return-void
.end method

.method private setHintContainer()V
    .locals 4

    .line 185
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getSystemUiVisibility()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintContainer:Landroid/view/ViewGroup;

    const/16 v1, 0xf02

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V

    .line 192
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHideNavigationBarWhenForceShow:Z

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mMiuiPocketModeManager:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    invoke-virtual {v1}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->getStateStableDelay()I

    move-result v1

    int-to-long v1, v1

    const/4 v3, 0x2

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 196
    return-void
.end method

.method private shouldBeBlockedInternal(Landroid/view/KeyEvent;Z)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "ScreenOnFully"    # Z

    .line 158
    const/4 v0, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 162
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 181
    return v2

    .line 175
    :sswitch_0
    return v0

    .line 165
    :sswitch_1
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 166
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v1

    xor-int/2addr v1, v2

    return v1

    .line 177
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    :sswitch_2
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mFrontFingerprintSensor:Z

    xor-int/2addr v0, v2

    return v0

    .line 159
    :cond_1
    :goto_0
    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_2
        0x18 -> :sswitch_1
        0x19 -> :sswitch_1
        0x1a -> :sswitch_0
        0x4f -> :sswitch_0
        0x55 -> :sswitch_0
        0x56 -> :sswitch_0
        0x57 -> :sswitch_0
        0x7e -> :sswitch_0
        0x7f -> :sswitch_0
    .end sparse-switch
.end method

.method private showHint()V
    .locals 7

    .line 297
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintView:Landroid/view/View;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 301
    :cond_0
    const-string v0, "MiuiScreenOnProximityLock"

    const-string/jumbo v1, "show hint..."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mContext:Landroid/content/Context;

    const v2, 0x103006b

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    const v1, 0x110c0037

    iget-object v2, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintContainer:Landroid/view/ViewGroup;

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintView:Landroid/view/View;

    .line 306
    invoke-direct {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->modTipsForKddi()V

    .line 307
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintView:Landroid/view/View;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v3, v2, [F

    fill-array-data v3, :array_0

    invoke-static {v0, v1, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 308
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    const-wide/16 v3, 0x1f4

    invoke-virtual {v0, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 309
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 311
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct {v1, v5, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 312
    .local v1, "animation":Landroid/view/animation/Animation;
    invoke-virtual {v1, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 313
    const/4 v5, -0x1

    invoke-virtual {v1, v5}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 314
    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    .line 315
    invoke-virtual {v1, v3, v4}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 317
    iget-object v2, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHintView:Landroid/view/View;

    const v3, 0x110a00be

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 318
    .local v2, "animationView":Landroid/view/View;
    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 319
    return-void

    .line 298
    .end local v0    # "animator":Landroid/animation/ObjectAnimator;
    .end local v1    # "animation":Landroid/view/animation/Animation;
    .end local v2    # "animationView":Landroid/view/View;
    :cond_1
    :goto_0
    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public declared-synchronized aquire()V
    .locals 4

    monitor-enter p0

    .line 125
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    const-string v0, "MiuiScreenOnProximityLock"

    const-string v1, "aquire"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mAquiredTime:J

    .line 128
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 129
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 130
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mMiuiPocketModeManager:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    iget-object v1, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mSensorListener:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->registerListener(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    .end local p0    # "this":Lcom/android/server/policy/MiuiScreenOnProximityLock;
    :cond_0
    monitor-exit p0

    return-void

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isHeld()Z
    .locals 4

    monitor-enter p0

    .line 121
    :try_start_0
    iget-wide v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mAquiredTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    .line 121
    .end local p0    # "this":Lcom/android/server/policy/MiuiScreenOnProximityLock;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized release(Z)Z
    .locals 3
    .param p1, "isNowRelease"    # Z

    monitor-enter p0

    .line 135
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    const-string v0, "MiuiScreenOnProximityLock"

    const-string v1, "release"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mAquiredTime:J

    .line 138
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mMiuiPocketModeManager:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    invoke-virtual {v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->unregisterListener()V

    .line 139
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 140
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 141
    iget-object v0, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 142
    .local v0, "releaseViewMessage":Landroid/os/Message;
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 143
    iget-object v2, p0, Lcom/android/server/policy/MiuiScreenOnProximityLock;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    monitor-exit p0

    return v1

    .line 146
    .end local v0    # "releaseViewMessage":Landroid/os/Message;
    .end local p0    # "this":Lcom/android/server/policy/MiuiScreenOnProximityLock;
    :cond_0
    monitor-exit p0

    const/4 v0, 0x0

    return v0

    .line 134
    .end local p1    # "isNowRelease":Z
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public shouldBeBlocked(ZLandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "ScreenOnFully"    # Z
    .param p2, "event"    # Landroid/view/KeyEvent;

    .line 150
    invoke-direct {p0, p2, p1}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->shouldBeBlockedInternal(Landroid/view/KeyEvent;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    invoke-direct {p0}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->forceShow()V

    .line 152
    const/4 v0, 0x1

    return v0

    .line 154
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
