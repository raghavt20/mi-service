.class Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiShortcutTriggerHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/policy/MiuiShortcutTriggerHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ShortcutSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;


# direct methods
.method public constructor <init>(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/policy/MiuiShortcutTriggerHelper;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 232
    iput-object p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 233
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 234
    return-void
.end method


# virtual methods
.method public initShortcutSettingsObserver()V
    .locals 7

    .line 239
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 240
    const-string v1, "long_press_power_launch_xiaoai"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 239
    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 243
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 244
    const-string v1, "long_press_power_launch_smarthome"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 243
    invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 247
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 248
    const-string v4, "screen_key_press_app_switch"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 247
    invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 250
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 251
    const-string v4, "fingerprint_nav_center_action"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 250
    invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 253
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 254
    const-string/jumbo v4, "single_key_use_enable"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 253
    invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 257
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 258
    const-string v4, "long_press_menu_key_when_lock"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 257
    invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 261
    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-nez v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "xiaoai_power_guide"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 266
    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->supportRSARegion()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 270
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    const-string v4, "global_power_guide"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 273
    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 276
    :cond_1
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 277
    const-string/jumbo v4, "user_setup_complete"

    invoke-static {v4}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 276
    invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 280
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 281
    const-string v5, "pc_mode_open"

    invoke-static {v5}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 280
    invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 283
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 284
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 283
    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 287
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 288
    const-string v1, "emergency_gesture_enabled"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 287
    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 290
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 291
    const-string v1, "key_miui_sos_enable"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 290
    invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 293
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 294
    const-string v5, "key_is_in_miui_sos_mode"

    invoke-static {v5}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 293
    invoke-virtual {v0, v6, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 297
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 298
    const-string v6, "imperceptible_press_power_key"

    invoke-static {v6}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 297
    invoke-virtual {v0, v6, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 300
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 301
    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 300
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 302
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 303
    invoke-static {v5}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 302
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 304
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "long_press_timeout"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 306
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "torch_state"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 308
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 309
    const-string v1, "assist_long_press_home_enabled"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 308
    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 311
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MiuiSettings$Secure;->MIUI_OPTIMIZATION:Ljava/lang/String;

    .line 312
    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 311
    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 314
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 315
    invoke-static {v4}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 314
    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 317
    invoke-virtual {p0, v2}, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->onChange(Z)V

    .line 318
    return-void
.end method

.method public onChange(Z)V
    .locals 7
    .param p1, "selfChange"    # Z

    .line 322
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v2}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v2

    const-string v3, "long_press_power_launch_xiaoai"

    const/4 v4, 0x0

    invoke-static {v1, v3, v4, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v4

    :goto_0
    iput-boolean v1, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mLongPressPowerKeyLaunchXiaoAi:Z

    .line 328
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v3

    const-string v5, "screen_key_press_app_switch"

    invoke-static {v1, v5, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    goto :goto_1

    :cond_1
    move v1, v4

    :goto_1
    invoke-static {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmPressToAppSwitch(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V

    .line 332
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v3

    const-string v5, "fingerprint_nav_center_action"

    const/4 v6, -0x1

    invoke-static {v1, v5, v6, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmFingerPrintNavCenterAction(Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V

    .line 335
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v3

    const-string/jumbo v5, "single_key_use_enable"

    invoke-static {v1, v5, v4, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-ne v1, v2, :cond_2

    move v1, v2

    goto :goto_2

    :cond_2
    move v1, v4

    :goto_2
    invoke-static {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmSingleKeyUse(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V

    .line 338
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 339
    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmSingleKeyUse(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Z

    move-result v1

    iput v1, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDoubleTapOnHomeBehavior:I

    .line 340
    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-nez v0, :cond_3

    .line 341
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v3

    const-string/jumbo v5, "xiaoai_power_guide"

    invoke-static {v1, v5, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iput v1, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mXiaoaiPowerGuideFlag:I

    .line 347
    :cond_3
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v3

    const-string v5, "global_power_guide"

    invoke-static {v1, v5, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmRSAGuideStatus(Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V

    .line 351
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v3

    const-string v5, "emergency_gesture_enabled"

    invoke-static {v1, v5, v3}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmFivePressPowerLaunchGoogleSos(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Ljava/lang/String;)V

    .line 353
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v3

    const-string v5, "key_miui_sos_enable"

    invoke-static {v1, v5, v4, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-ne v1, v2, :cond_4

    move v1, v2

    goto :goto_3

    :cond_4
    move v1, v4

    :goto_3
    invoke-static {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmFivePressPowerLaunchSos(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V

    .line 355
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v3

    const-string v5, "key_is_in_miui_sos_mode"

    invoke-static {v1, v5, v4, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-ne v1, v2, :cond_5

    move v1, v2

    goto :goto_4

    :cond_5
    move v1, v4

    :goto_4
    invoke-static {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmIsOnSosMode(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V

    .line 357
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v3

    const-string v5, "long_press_timeout"

    invoke-static {v1, v5, v4, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iput v1, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDefaultLongPressTimeOut:I

    .line 359
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v3, "torch_state"

    invoke-static {v1, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_6

    move v1, v2

    goto :goto_5

    :cond_6
    move v1, v4

    :goto_5
    invoke-static {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmTorchEnabled(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V

    .line 361
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "assist_long_press_home_enabled"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v2, :cond_7

    move v4, v2

    :cond_7
    invoke-static {v0, v4}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmAOSPAssistantLongPressHomeEnabled(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V

    .line 363
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 364
    const-string v1, "ro.miui.cts"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "1"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 363
    xor-int/2addr v1, v2

    const-string v3, "persist.sys.miui_optimization"

    invoke-static {v3, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    xor-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmIsCtsMode(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V

    .line 365
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 7
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 369
    const-string v0, "long_press_power_launch_xiaoai"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "long_press_power_key"

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_3

    .line 371
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v6}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v6

    invoke-static {v5, v0, v3, v6}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v4, :cond_0

    move v3, v4

    :cond_0
    iput-boolean v3, v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mLongPressPowerKeyLaunchXiaoAi:Z

    .line 377
    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v0, :cond_2

    .line 378
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 380
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    iget-boolean v1, v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mLongPressPowerKeyLaunchXiaoAi:Z

    if-eqz v1, :cond_1

    const-string v1, "launch_google_search"

    goto :goto_0

    .line 381
    :cond_1
    const-string v1, "none"

    :goto_0
    iget-object v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v3

    .line 378
    invoke-static {v0, v2, v1, v3}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 383
    :cond_2
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    iget-boolean v0, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mLongPressPowerKeyLaunchXiaoAi:Z

    if-eqz v0, :cond_16

    .line 384
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v1

    const-string v3, "launch_voice_assistant"

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 388
    :cond_3
    const-string v0, "long_press_power_launch_smarthome"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 389
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 391
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v6}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v6

    invoke-static {v5, v0, v3, v6}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v4, :cond_4

    move v3, v4

    :cond_4
    iput-boolean v3, v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mLongPressPowerKeyLaunchSmartHome:Z

    .line 395
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    iget-boolean v0, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mLongPressPowerKeyLaunchSmartHome:Z

    if-eqz v0, :cond_16

    .line 396
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v1

    const-string v3, "launch_smarthome"

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 400
    :cond_5
    const-string v0, "screen_key_press_app_switch"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 401
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 402
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v5, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v5}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v5

    invoke-static {v2, v0, v4, v5}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_6

    move v3, v4

    :cond_6
    invoke-static {v1, v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmPressToAppSwitch(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V

    goto/16 :goto_1

    .line 406
    :cond_7
    const-string v0, "fingerprint_nav_center_action"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 407
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, -0x1

    if-eqz v1, :cond_8

    .line 408
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v4}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v4

    invoke-static {v3, v0, v2, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    invoke-static {v1, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmFingerPrintNavCenterAction(Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V

    goto/16 :goto_1

    .line 412
    :cond_8
    const-string/jumbo v0, "single_key_use_enable"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 414
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v5, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v5}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v5

    invoke-static {v2, v0, v3, v5}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v4, :cond_9

    move v3, v4

    :cond_9
    invoke-static {v1, v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmSingleKeyUse(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V

    .line 417
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 418
    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmSingleKeyUse(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Z

    move-result v1

    iput v1, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDoubleTapOnHomeBehavior:I

    goto/16 :goto_1

    .line 419
    :cond_a
    const-string/jumbo v0, "xiaoai_power_guide"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 420
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 421
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContext(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v3

    invoke-static {v2, v0, v4, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mXiaoaiPowerGuideFlag:I

    goto/16 :goto_1

    .line 425
    :cond_b
    const-string v0, "emergency_gesture_enabled"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 426
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 427
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v3

    invoke-static {v2, v0, v3}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmFivePressPowerLaunchGoogleSos(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 429
    :cond_c
    const-string v0, "key_miui_sos_enable"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 430
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v5, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v5}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v5

    invoke-static {v2, v0, v3, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v4, :cond_d

    move v3, v4

    :cond_d
    invoke-static {v1, v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmFivePressPowerLaunchSos(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V

    goto/16 :goto_1

    .line 432
    :cond_e
    const-string v0, "key_is_in_miui_sos_mode"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 433
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v5, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v5}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v5

    invoke-static {v2, v0, v3, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v4, :cond_f

    move v3, v4

    :cond_f
    invoke-static {v1, v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmIsOnSosMode(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V

    goto/16 :goto_1

    .line 435
    :cond_10
    const-string v0, "global_power_guide"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 436
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 437
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v3

    invoke-static {v2, v0, v4, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    invoke-static {v1, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmRSAGuideStatus(Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V

    goto/16 :goto_1

    .line 442
    :cond_11
    const-string v0, "long_press_timeout"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 443
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v4}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)I

    move-result v4

    invoke-static {v2, v0, v3, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDefaultLongPressTimeOut:I

    goto :goto_1

    .line 445
    :cond_12
    const-string v0, "assist_long_press_home_enabled"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 446
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 447
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-static {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fgetmContentResolver(Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v5, v0, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_13

    move v3, v4

    :cond_13
    invoke-static {v1, v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmAOSPAssistantLongPressHomeEnabled(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V

    goto :goto_1

    .line 449
    :cond_14
    sget-object v0, Landroid/provider/MiuiSettings$Secure;->MIUI_OPTIMIZATION:Ljava/lang/String;

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 451
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 452
    const-string v1, "ro.miui.cts"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 451
    xor-int/2addr v1, v4

    const-string v2, "persist.sys.miui_optimization"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    xor-int/2addr v1, v4

    invoke-static {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->-$$Nest$fputmIsCtsMode(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Z)V

    goto :goto_1

    .line 453
    :cond_15
    const-string/jumbo v0, "user_setup_complete"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 454
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->this$0:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setVeryLongPressPowerBehavior()V

    .line 456
    :cond_16
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 457
    return-void
.end method
