public class com.android.server.policy.MiuiSingleKeyObserver extends com.android.server.policy.MiuiShortcutObserver {
	 /* .source "MiuiSingleKeyObserver.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private java.util.Map mActionAndDefaultFunction;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final java.util.Map mActionAndFunctionMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.content.ContentResolver mContentResolver;
private final android.content.Context mContext;
private com.android.server.policy.MiuiSingleKeyRule mMiuiSingleKeyRule;
/* # direct methods */
public static void $r8$lambda$-CUo51W_LnOnKhLb2_ykrm7RGRQ ( com.android.server.policy.MiuiSingleKeyObserver p0, java.lang.String p1, java.lang.String p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyObserver;->lambda$registerShortcutAction$1(Ljava/lang/String;Ljava/lang/String;)V */
return;
} // .end method
public static void $r8$lambda$3sFE00JR7ATSeD0bHIbVUTK_3fA ( com.android.server.policy.MiuiSingleKeyObserver p0, android.net.Uri p1, java.lang.String p2, java.lang.String p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/policy/MiuiSingleKeyObserver;->lambda$onChange$2(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V */
return;
} // .end method
public static void $r8$lambda$FGfc5UFqbXu9N0-aAf_OjjOoq6Y ( com.android.server.policy.MiuiSingleKeyObserver p0, java.lang.String p1, java.lang.String p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyObserver;->lambda$updateRuleInfo$3(Ljava/lang/String;Ljava/lang/String;)V */
return;
} // .end method
public static void $r8$lambda$rTInd3nrtKfWmLRJcIHjTUxmdmQ ( com.android.server.policy.MiuiSingleKeyObserver p0, java.lang.String p1, java.lang.String p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyObserver;->lambda$new$0(Ljava/lang/String;Ljava/lang/String;)V */
return;
} // .end method
public com.android.server.policy.MiuiSingleKeyObserver ( ) {
/* .locals 1 */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p4, "currentUserId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/Handler;", */
/* "Landroid/content/Context;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 26 */
/* .local p3, "actionAndDefaultFunctionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* invoke-direct {p0, p1, p2, p4}, Lcom/android/server/policy/MiuiShortcutObserver;-><init>(Landroid/os/Handler;Landroid/content/Context;I)V */
/* .line 20 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mActionAndFunctionMap = v0;
/* .line 21 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mActionAndDefaultFunction = v0;
/* .line 27 */
this.mActionAndDefaultFunction = p3;
/* .line 28 */
/* new-instance v0, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda2; */
/* invoke-direct {v0, p0}, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/policy/MiuiSingleKeyObserver;)V */
/* .line 30 */
this.mContext = p2;
/* .line 31 */
(( android.content.Context ) p2 ).getContentResolver ( ); // invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mContentResolver = v0;
/* .line 32 */
/* invoke-direct {p0}, Lcom/android/server/policy/MiuiSingleKeyObserver;->registerShortcutAction()V */
/* .line 33 */
return;
} // .end method
private void lambda$new$0 ( java.lang.String p0, java.lang.String p1 ) { //synthethic
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .line 29 */
v0 = this.mActionAndFunctionMap;
int v1 = 0; // const/4 v1, 0x0
return;
} // .end method
private void lambda$onChange$2 ( android.net.Uri p0, java.lang.String p1, java.lang.String p2 ) { //synthethic
/* .locals 1 */
/* .param p1, "uri" # Landroid/net/Uri; */
/* .param p2, "action" # Ljava/lang/String; */
/* .param p3, "function" # Ljava/lang/String; */
/* .line 45 */
android.provider.Settings$System .getUriFor ( p2 );
v0 = (( android.net.Uri ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 46 */
/* invoke-direct {p0, p2}, Lcom/android/server/policy/MiuiSingleKeyObserver;->updateFunction(Ljava/lang/String;)V */
/* .line 48 */
} // :cond_0
return;
} // .end method
private void lambda$registerShortcutAction$1 ( java.lang.String p0, java.lang.String p1 ) { //synthethic
/* .locals 4 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .line 37 */
v0 = this.mContentResolver;
/* .line 38 */
android.provider.Settings$System .getUriFor ( p1 );
/* .line 37 */
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
return;
} // .end method
private void lambda$updateRuleInfo$3 ( java.lang.String p0, java.lang.String p1 ) { //synthethic
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .line 94 */
int v0 = 0; // const/4 v0, 0x0
android.provider.Settings$System .getUriFor ( p1 );
(( com.android.server.policy.MiuiSingleKeyObserver ) p0 ).onChange ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/policy/MiuiSingleKeyObserver;->onChange(ZLandroid/net/Uri;)V
return;
} // .end method
private void registerShortcutAction ( ) {
/* .locals 2 */
/* .line 36 */
v0 = this.mActionAndFunctionMap;
/* new-instance v1, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/policy/MiuiSingleKeyObserver;)V */
/* .line 40 */
return;
} // .end method
private void updateFunction ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 99 */
v0 = (( com.android.server.policy.MiuiSingleKeyObserver ) p0 ).currentFunctionValueIsInt ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/policy/MiuiSingleKeyObserver;->currentFunctionValueIsInt(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 100 */
v0 = this.mContentResolver;
int v1 = -1; // const/4 v1, -0x1
/* iget v2, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mCurrentUserId:I */
v0 = android.provider.Settings$System .getIntForUser ( v0,p1,v1,v2 );
/* .line 102 */
/* .local v0, "functionStatus":I */
java.lang.String .valueOf ( v0 );
/* .line 103 */
/* .local v0, "function":Ljava/lang/String; */
/* .line 104 */
} // .end local v0 # "function":Ljava/lang/String;
} // :cond_0
v0 = this.mContentResolver;
/* iget v1, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mCurrentUserId:I */
android.provider.Settings$System .getStringForUser ( v0,p1,v1 );
/* .line 106 */
/* .restart local v0 # "function":Ljava/lang/String; */
} // :goto_0
v1 = this.mActionAndFunctionMap;
/* .line 107 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 3 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 126 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 127 */
v0 = this.mActionAndFunctionMap;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 128 */
/* .local v1, "actionAndFunctionEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;" */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 129 */
/* check-cast v2, Ljava/lang/String; */
(( java.io.PrintWriter ) p2 ).print ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 130 */
final String v2 = "="; // const-string v2, "="
(( java.io.PrintWriter ) p2 ).print ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 131 */
/* check-cast v2, Ljava/lang/String; */
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 132 */
} // .end local v1 # "actionAndFunctionEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
/* .line 133 */
} // :cond_0
return;
} // .end method
public java.util.Map getActionAndFunctionMap ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 111 */
v0 = this.mActionAndFunctionMap;
} // .end method
public java.lang.String getFunction ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 116 */
v0 = this.mActionAndFunctionMap;
/* check-cast v0, Ljava/lang/String; */
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 44 */
v0 = this.mActionAndFunctionMap;
/* new-instance v1, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0, p2}, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/policy/MiuiSingleKeyObserver;Landroid/net/Uri;)V */
/* .line 49 */
v0 = this.mMiuiSingleKeyRule;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 50 */
(( com.android.server.policy.MiuiSingleKeyObserver ) p0 ).notifySingleRuleChanged ( v0, p2 ); // invoke-virtual {p0, v0, p2}, Lcom/android/server/policy/MiuiSingleKeyObserver;->notifySingleRuleChanged(Lcom/android/server/policy/MiuiSingleKeyRule;Landroid/net/Uri;)V
/* .line 52 */
} // :cond_0
/* invoke-super {p0, p1, p2}, Lcom/android/server/policy/MiuiShortcutObserver;->onChange(ZLandroid/net/Uri;)V */
/* .line 53 */
return;
} // .end method
public void setDefaultFunction ( Boolean p0 ) {
/* .locals 8 */
/* .param p1, "isNeedReset" # Z */
/* .line 58 */
v0 = this.mActionAndFunctionMap;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_6
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 59 */
/* .local v1, "actionAndFunctionEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;" */
/* check-cast v2, Ljava/lang/String; */
/* .line 60 */
/* .local v2, "action":Ljava/lang/String; */
v3 = this.mActionAndDefaultFunction;
/* check-cast v3, Ljava/lang/String; */
/* .line 62 */
/* .local v3, "defaultFunction":Ljava/lang/String; */
v4 = (( com.android.server.policy.MiuiSingleKeyObserver ) p0 ).currentFunctionValueIsInt ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/policy/MiuiSingleKeyObserver;->currentFunctionValueIsInt(Ljava/lang/String;)Z
int v5 = 0; // const/4 v5, 0x0
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 63 */
v4 = this.mContentResolver;
/* iget v6, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mCurrentUserId:I */
int v7 = -1; // const/4 v7, -0x1
v4 = android.provider.Settings$System .getIntForUser ( v4,v2,v7,v6 );
/* .line 65 */
/* .local v4, "functionStatus":I */
/* if-ne v4, v7, :cond_0 */
/* move-object v6, v5 */
} // :cond_0
java.lang.String .valueOf ( v4 );
} // :goto_1
/* move-object v4, v6 */
/* .line 66 */
/* .local v4, "currentFunction":Ljava/lang/String; */
/* .line 67 */
} // .end local v4 # "currentFunction":Ljava/lang/String;
} // :cond_1
v4 = this.mContentResolver;
/* iget v6, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mCurrentUserId:I */
android.provider.Settings$System .getStringForUser ( v4,v2,v6 );
/* .line 69 */
/* .restart local v4 # "currentFunction":Ljava/lang/String; */
/* if-nez v4, :cond_2 */
/* move-object v6, v3 */
} // :cond_2
/* move-object v6, v4 */
} // :goto_2
/* move-object v4, v6 */
/* .line 72 */
} // :goto_3
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 73 */
/* move-object v4, v3 */
/* .line 75 */
} // :cond_3
v6 = (( com.android.server.policy.MiuiSingleKeyObserver ) p0 ).hasCustomizedFunction ( v2, v4, p1 ); // invoke-virtual {p0, v2, v4, p1}, Lcom/android/server/policy/MiuiSingleKeyObserver;->hasCustomizedFunction(Ljava/lang/String;Ljava/lang/String;Z)Z
/* if-nez v6, :cond_5 */
/* .line 76 */
v6 = this.mContext;
v6 = (( com.android.server.policy.MiuiSingleKeyObserver ) p0 ).isFeasibleFunction ( v4, v6 ); // invoke-virtual {p0, v4, v6}, Lcom/android/server/policy/MiuiSingleKeyObserver;->isFeasibleFunction(Ljava/lang/String;Landroid/content/Context;)Z
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 77 */
v5 = this.mContentResolver;
/* iget v6, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mCurrentUserId:I */
android.provider.Settings$System .putStringForUser ( v5,v2,v4,v6 );
/* .line 80 */
} // :cond_4
if ( p1 != null) { // if-eqz p1, :cond_5
/* .line 82 */
v6 = this.mContentResolver;
/* iget v7, p0, Lcom/android/server/policy/MiuiSingleKeyObserver;->mCurrentUserId:I */
android.provider.Settings$System .putStringForUser ( v6,v2,v5,v7 );
/* .line 87 */
} // .end local v1 # "actionAndFunctionEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
} // :cond_5
} // :goto_4
/* .line 88 */
} // .end local v2 # "action":Ljava/lang/String;
} // .end local v3 # "defaultFunction":Ljava/lang/String;
} // .end local v4 # "currentFunction":Ljava/lang/String;
} // :cond_6
/* invoke-super {p0, p1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V */
/* .line 89 */
return;
} // .end method
public void setRuleForObserver ( com.android.server.policy.MiuiSingleKeyRule p0 ) {
/* .locals 0 */
/* .param p1, "miuiSingleKeyRule" # Lcom/android/server/policy/MiuiSingleKeyRule; */
/* .line 121 */
this.mMiuiSingleKeyRule = p1;
/* .line 122 */
return;
} // .end method
void updateRuleInfo ( ) {
/* .locals 2 */
/* .line 93 */
v0 = this.mActionAndFunctionMap;
/* new-instance v1, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/policy/MiuiSingleKeyObserver$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/policy/MiuiSingleKeyObserver;)V */
/* .line 95 */
return;
} // .end method
