.class public Lcom/android/server/policy/DisplayTurnoverManager;
.super Ljava/lang/Object;
.source "DisplayTurnoverManager.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/policy/DisplayTurnoverManager$SettingsObserver;
    }
.end annotation


# static fields
.field private static final ACCURATE_THRESHOLD:I = 0x2

.field public static final CODE_TURN_OFF_SUB_DISPLAY:I = 0xfffffb

.field public static final CODE_TURN_ON_SUB_DISPLAY:I = 0xfffffc

.field private static final CRESCENDO_RINGER_MILLI_TIME_DELAY_ANTISPAM_STRANGER:I = 0x1194

.field private static final CRESCENDO_RINGER_MILLI_TIME_DELAY_MARK_RINGING:I = 0x5dc

.field private static final CRESCENDO_RINGER_SECONDS_TIME:I = 0x4

.field private static final CRESCENDO_RINGER_SECONDS_TIME_WITH_HEADSET:I = 0xa

.field private static final DECRESCENDO_DELAY_TIME:I = 0x190

.field private static final DECRESCENDO_INTERVAL_VOLUME:F = 0.08f

.field private static final DECRESCENDO_MIN_VOLUME:F = 0.52f

.field private static final HORIZONTAL_Z_ACCELERATION:F = 9.0f

.field private static final KEY_SUB_SCREEN_FACE_UP:Ljava/lang/String; = "sub_screen_face_up"

.field private static final NS2S:F = 1.0E-9f

.field private static final SENSOR_SCREEN_DOWN:I = 0x1fa265d

.field private static final SUB_SCREEN_OFF:I = 0x1

.field private static final SUB_SCREEN_ON:I = 0x2

.field private static final TAG:Ljava/lang/String; = "DisplayTurnoverManager"

.field private static final TILT_Z_ACCELERATION:F = 8.9f

.field private static final TRIGGER_THRESHOLD:I = 0x1


# instance fields
.field private mAccValues:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair<",
            "Ljava/lang/Float;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mAccelerometerSensor:Landroid/hardware/Sensor;

.field private mContext:Landroid/content/Context;

.field private mGravitySensor:Landroid/hardware/Sensor;

.field private mGravityValues:[F

.field private mGyroscopeSensor:Landroid/hardware/Sensor;

.field private mGyroscopeTimestamp:F

.field private mGyroscopeValues:[F

.field private mHandler:Landroid/os/Handler;

.field private mIPowerManager:Landroid/os/IBinder;

.field private mIsLastStateOn:Z

.field private mIsTurnOverMuteEnabled:Z

.field private mIsUserSetupComplete:Z

.field private mLinearAccSensor:Landroid/hardware/Sensor;

.field private mLinearAccValues:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair<",
            "Ljava/lang/Float;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mListeningSensor:Z

.field private mRadians:[F

.field private mScreendownSensor:Landroid/hardware/Sensor;

.field private mSensorDownValues:[F

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSettingsObserver:Lcom/android/server/policy/DisplayTurnoverManager$SettingsObserver;

.field private mTurnoverTriggered:Z


# direct methods
.method static bridge synthetic -$$Nest$mupdateSubSwitchMode(Lcom/android/server/policy/DisplayTurnoverManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/DisplayTurnoverManager;->updateSubSwitchMode()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mListeningSensor:Z

    .line 91
    iput-object p1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mContext:Landroid/content/Context;

    .line 92
    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 93
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mHandler:Landroid/os/Handler;

    .line 94
    const-string v0, "power"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIPowerManager:Landroid/os/IBinder;

    .line 95
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/android/server/policy/DisplayTurnoverManager$1;

    invoke-direct {v1, p0}, Lcom/android/server/policy/DisplayTurnoverManager$1;-><init>(Lcom/android/server/policy/DisplayTurnoverManager;)V

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.USER_SWITCHED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 101
    return-void
.end method

.method private isUserSetupComplete()Z
    .locals 5

    .line 246
    iget-boolean v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsUserSetupComplete:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "user_setup_complete"

    const/4 v3, -0x2

    const/4 v4, 0x0

    invoke-static {v0, v2, v4, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    move v1, v4

    :goto_0
    iput-boolean v1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsUserSetupComplete:Z

    .line 249
    return v1
.end method

.method private readyToTurnoverOrByScreenDownSensor()V
    .locals 6

    .line 149
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorDownValues:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 150
    .local v0, "z":F
    iget-object v2, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    float-to-int v3, v0

    const/4 v4, -0x2

    const-string/jumbo v5, "sub_screen_face_up"

    invoke-static {v2, v5, v3, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 153
    iget-boolean v2, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsTurnOverMuteEnabled:Z

    if-eqz v2, :cond_1

    .line 154
    float-to-double v2, v0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsLastStateOn:Z

    if-nez v2, :cond_0

    .line 155
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/server/policy/DisplayTurnoverManager;->turnOverMute(Z)V

    goto :goto_0

    .line 156
    :cond_0
    float-to-double v2, v0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsLastStateOn:Z

    if-eqz v2, :cond_1

    .line 157
    invoke-direct {p0, v1}, Lcom/android/server/policy/DisplayTurnoverManager;->turnOverMute(Z)V

    .line 160
    :cond_1
    :goto_0
    return-void
.end method

.method private readyToTurnoverOrHandonByGravity()V
    .locals 5

    .line 163
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mGravityValues:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    .line 166
    .local v0, "z":F
    iget-boolean v1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsTurnOverMuteEnabled:Z

    if-eqz v1, :cond_1

    .line 167
    float-to-double v1, v0

    const-wide/high16 v3, -0x3fe8000000000000L    # -6.0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsLastStateOn:Z

    if-nez v1, :cond_0

    .line 168
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/server/policy/DisplayTurnoverManager;->turnOverMute(Z)V

    goto :goto_0

    .line 169
    :cond_0
    float-to-double v1, v0

    const-wide/high16 v3, 0x4010000000000000L    # 4.0

    cmpl-double v1, v1, v3

    if-lez v1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsLastStateOn:Z

    if-eqz v1, :cond_1

    .line 170
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/server/policy/DisplayTurnoverManager;->turnOverMute(Z)V

    .line 173
    :cond_1
    :goto_0
    return-void
.end method

.method private startSensor()V
    .locals 5

    .line 181
    iget-boolean v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsTurnOverMuteEnabled:Z

    if-eqz v0, :cond_2

    .line 183
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1fa265d

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mScreendownSensor:Landroid/hardware/Sensor;

    .line 184
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mGravitySensor:Landroid/hardware/Sensor;

    .line 185
    iget-object v1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mScreendownSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 186
    const-string v0, "DisplayTurnoverManager"

    const-string v1, "mScreendownSensor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mScreendownSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0

    .line 189
    :cond_0
    const/4 v1, 0x3

    if-eqz v0, :cond_1

    .line 190
    iget-object v3, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v3, p0, v0, v1}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0

    .line 193
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mGyroscopeTimestamp:F

    .line 194
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mRadians:[F

    .line 195
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    .line 196
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v4, 0xa

    invoke-virtual {v0, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mLinearAccSensor:Landroid/hardware/Sensor;

    .line 197
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mGyroscopeSensor:Landroid/hardware/Sensor;

    .line 198
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v4, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v4, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 200
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v4, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mLinearAccSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v4, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 202
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mGyroscopeSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v3, v1}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 205
    :goto_0
    iput-boolean v2, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mListeningSensor:Z

    .line 207
    :cond_2
    return-void
.end method

.method private stopSensor()V
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 211
    return-void
.end method

.method private turnOnOrOFFSubDisplay(ILjava/lang/String;)V
    .locals 5
    .param p1, "code"    # I
    .param p2, "reason"    # Ljava/lang/String;

    .line 225
    invoke-direct {p0}, Lcom/android/server/policy/DisplayTurnoverManager;->isUserSetupComplete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIPowerManager:Landroid/os/IBinder;

    if-eqz v0, :cond_1

    .line 229
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 230
    .local v0, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 231
    .local v1, "reply":Landroid/os/Parcel;
    const-string v2, "android.os.IPowerManager"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 232
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 233
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 235
    :try_start_0
    iget-object v2, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIPowerManager:Landroid/os/IBinder;

    const/4 v3, 0x1

    invoke-interface {v2, p1, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    nop

    :goto_0
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 240
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 241
    goto :goto_2

    .line 239
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 236
    :catch_0
    move-exception v2

    .line 237
    .local v2, "ex":Landroid/os/RemoteException;
    :try_start_1
    const-string v3, "DisplayTurnoverManager"

    const-string v4, "Failed to wake up secondary display"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 239
    nop

    .end local v2    # "ex":Landroid/os/RemoteException;
    goto :goto_0

    :goto_1
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 240
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 241
    throw v2

    .line 243
    .end local v0    # "data":Landroid/os/Parcel;
    .end local v1    # "reply":Landroid/os/Parcel;
    :cond_1
    :goto_2
    return-void
.end method

.method private turnOverMute(Z)V
    .locals 3
    .param p1, "isOnSubScreen"    # Z

    .line 215
    const-string v0, "DisplayTurnoverManager"

    :try_start_0
    iput-boolean p1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsLastStateOn:Z

    .line 216
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set SubScreen On: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    if-eqz p1, :cond_0

    const v1, 0xfffffc

    goto :goto_0

    :cond_0
    const v1, 0xfffffb

    .line 218
    .local v1, "code":I
    :goto_0
    const-string v2, "TURN_OVER"

    invoke-direct {p0, v1, v2}, Lcom/android/server/policy/DisplayTurnoverManager;->turnOnOrOFFSubDisplay(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    .end local v1    # "code":I
    goto :goto_1

    .line 219
    :catch_0
    move-exception v1

    .line 220
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "close sub display manager service connect fail!"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private updateSubSwitchMode()V
    .locals 4

    .line 253
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string/jumbo v2, "subscreen_switch"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    iput-boolean v1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsTurnOverMuteEnabled:Z

    .line 255
    if-eqz v1, :cond_1

    .line 256
    invoke-direct {p0}, Lcom/android/server/policy/DisplayTurnoverManager;->startSensor()V

    goto :goto_1

    .line 258
    :cond_1
    invoke-direct {p0}, Lcom/android/server/policy/DisplayTurnoverManager;->stopSensor()V

    .line 259
    invoke-direct {p0, v3}, Lcom/android/server/policy/DisplayTurnoverManager;->turnOverMute(Z)V

    .line 261
    :goto_1
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .line 178
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 116
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x2

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 130
    :sswitch_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorDownValues:[F

    .line 131
    goto :goto_0

    .line 124
    :sswitch_1
    new-instance v0, Landroid/util/Pair;

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v2, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mLinearAccValues:Landroid/util/Pair;

    .line 125
    goto :goto_0

    .line 118
    :sswitch_2
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mGravityValues:[F

    .line 119
    goto :goto_0

    .line 127
    :sswitch_3
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mGyroscopeValues:[F

    .line 128
    goto :goto_0

    .line 121
    :sswitch_4
    new-instance v0, Landroid/util/Pair;

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v2, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mAccValues:Landroid/util/Pair;

    .line 122
    nop

    .line 136
    :goto_0
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mGravityValues:[F

    if-eqz v0, :cond_0

    .line 137
    invoke-direct {p0}, Lcom/android/server/policy/DisplayTurnoverManager;->readyToTurnoverOrHandonByGravity()V

    goto :goto_1

    .line 139
    :cond_0
    const-string v0, "Do not support Gravity sensor!!!"

    const-string v1, "DisplayTurnoverManager"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSensorDownValues:[F

    if-eqz v0, :cond_1

    .line 141
    invoke-direct {p0}, Lcom/android/server/policy/DisplayTurnoverManager;->readyToTurnoverOrByScreenDownSensor()V

    goto :goto_1

    .line 143
    :cond_1
    const-string v0, "Do not support Screen Down sensor!!!"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :goto_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x4 -> :sswitch_3
        0x9 -> :sswitch_2
        0xa -> :sswitch_1
        0x1fa265d -> :sswitch_0
    .end sparse-switch
.end method

.method public switchSubDisplayPowerState(ZLjava/lang/String;)V
    .locals 1
    .param p1, "isOnSubScreen"    # Z
    .param p2, "reason"    # Ljava/lang/String;

    .line 110
    if-eqz p1, :cond_0

    const v0, 0xfffffc

    goto :goto_0

    :cond_0
    const v0, 0xfffffb

    .line 111
    .local v0, "code":I
    :goto_0
    invoke-direct {p0, v0, p2}, Lcom/android/server/policy/DisplayTurnoverManager;->turnOnOrOFFSubDisplay(ILjava/lang/String;)V

    .line 112
    return-void
.end method

.method public systemReady()V
    .locals 5

    .line 104
    new-instance v0, Lcom/android/server/policy/DisplayTurnoverManager$SettingsObserver;

    iget-object v1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/policy/DisplayTurnoverManager$SettingsObserver;-><init>(Lcom/android/server/policy/DisplayTurnoverManager;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSettingsObserver:Lcom/android/server/policy/DisplayTurnoverManager$SettingsObserver;

    .line 105
    iget-object v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "subscreen_switch"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mSettingsObserver:Lcom/android/server/policy/DisplayTurnoverManager$SettingsObserver;

    const/4 v3, -0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 106
    invoke-direct {p0}, Lcom/android/server/policy/DisplayTurnoverManager;->updateSubSwitchMode()V

    .line 107
    return-void
.end method
