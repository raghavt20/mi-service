class com.android.server.policy.MiuiPhoneWindowManager$2 implements miui.view.MiuiSecurityPermissionHandler$PermissionViewCallback {
	 /* .source "MiuiPhoneWindowManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/policy/MiuiPhoneWindowManager;->systemReady()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.policy.MiuiPhoneWindowManager this$0; //synthetic
/* # direct methods */
 com.android.server.policy.MiuiPhoneWindowManager$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/policy/MiuiPhoneWindowManager; */
/* .line 183 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAddAccount ( ) {
/* .locals 2 */
/* .line 193 */
v0 = this.this$0;
com.android.server.policy.MiuiPhoneWindowManager .-$$Nest$fgetmAccountHelper ( v0 );
v1 = this.this$0;
v1 = this.mContext;
(( com.android.server.wm.AccountHelper ) v0 ).addAccount ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/AccountHelper;->addAccount(Landroid/content/Context;)V
/* .line 194 */
return;
} // .end method
public void onHideWaterMarker ( ) {
/* .locals 1 */
/* .line 209 */
v0 = this.this$0;
com.android.server.policy.MiuiPhoneWindowManager .-$$Nest$fgetmPhoneWindowCallback ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 210 */
	 v0 = this.this$0;
	 com.android.server.policy.MiuiPhoneWindowManager .-$$Nest$fgetmPhoneWindowCallback ( v0 );
	 /* .line 212 */
} // :cond_0
return;
} // .end method
public void onListenAccount ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "mode" # I */
/* .line 199 */
v0 = this.this$0;
com.android.server.policy.MiuiPhoneWindowManager .-$$Nest$fgetmAccountHelper ( v0 );
(( com.android.server.wm.AccountHelper ) v0 ).ListenAccount ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/AccountHelper;->ListenAccount(I)V
/* .line 200 */
return;
} // .end method
public void onListenPermission ( ) {
/* .locals 0 */
/* .line 217 */
return;
} // .end method
public void onShowWaterMarker ( ) {
/* .locals 1 */
/* .line 186 */
v0 = this.this$0;
com.android.server.policy.MiuiPhoneWindowManager .-$$Nest$fgetmPhoneWindowCallback ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 187 */
	 v0 = this.this$0;
	 com.android.server.policy.MiuiPhoneWindowManager .-$$Nest$fgetmPhoneWindowCallback ( v0 );
	 /* .line 189 */
} // :cond_0
return;
} // .end method
public void onUnListenAccount ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "mode" # I */
/* .line 204 */
v0 = this.this$0;
com.android.server.policy.MiuiPhoneWindowManager .-$$Nest$fgetmAccountHelper ( v0 );
(( com.android.server.wm.AccountHelper ) v0 ).UnListenAccount ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/AccountHelper;->UnListenAccount(I)V
/* .line 205 */
return;
} // .end method
