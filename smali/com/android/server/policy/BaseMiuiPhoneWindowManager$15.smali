.class Lcom/android/server/policy/BaseMiuiPhoneWindowManager$15;
.super Ljava/lang/Object;
.source "BaseMiuiPhoneWindowManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->hideBootMessages()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    .line 2669
    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$15;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 2671
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$15;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmMiuiBootMsgDialog(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2672
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$15;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmMiuiBootMsgDialog(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 2673
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$15;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmMiuiBootMsgDialog(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/app/Dialog;)V

    .line 2674
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$15;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmBootProgress(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/widget/ProgressBar;)V

    .line 2675
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$15;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmBootTextView(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/widget/TextView;)V

    .line 2676
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$15;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmBootText(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;[Ljava/lang/String;)V

    .line 2678
    :cond_0
    return-void
.end method
