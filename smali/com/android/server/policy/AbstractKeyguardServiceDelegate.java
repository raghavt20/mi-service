public abstract class com.android.server.policy.AbstractKeyguardServiceDelegate {
	 /* .source "AbstractKeyguardServiceDelegate.java" */
	 /* # direct methods */
	 public com.android.server.policy.AbstractKeyguardServiceDelegate ( ) {
		 /* .locals 0 */
		 /* .line 3 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public abstract void OnDoubleClickHome ( ) {
	 } // .end method
	 protected abstract void enableUserActivity ( Boolean p0 ) {
	 } // .end method
	 public abstract Boolean isShowing ( ) {
	 } // .end method
	 public abstract Boolean isShowingAndNotHidden ( ) {
	 } // .end method
	 public abstract void keyguardDone ( ) {
	 } // .end method
	 public abstract void onScreenTurnedOnWithoutListener ( ) {
	 } // .end method
	 public abstract Boolean onWakeKeyWhenKeyguardShowingTq ( Integer p0, Boolean p1 ) {
	 } // .end method
	 public abstract void pokeWakelock ( ) {
	 } // .end method
