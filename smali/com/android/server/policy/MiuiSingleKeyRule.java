public abstract class com.android.server.policy.MiuiSingleKeyRule extends com.android.server.policy.SingleKeyGestureDetector$SingleKeyRule {
	 /* .source "MiuiSingleKeyRule.java" */
	 /* # static fields */
	 public static final java.lang.String ACTION_TYPE_DOUBLE_CLICK;
	 public static final java.lang.String ACTION_TYPE_FIVE_CLICK;
	 public static final java.lang.String ACTION_TYPE_LONG_PRESS;
	 public static final Long DEFAULT_VERY_LONG_PRESS_TIME_OUT;
	 public static final Integer DOUBLE_TIMES_TAP;
	 public static final Integer FIVE_TIMES_TAP;
	 public static final Integer SINGLE_TAP;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final android.content.Context mContext;
	 private Integer mCurrentUserId;
	 private final android.os.Handler mHandler;
	 private final Integer mKeyCode;
	 private com.android.server.policy.MiuiShortcutObserver mMiuiShortcutObserver;
	 private com.android.server.policy.MiuiShortcutTriggerHelper mMiuiShortcutTriggerHelper;
	 private final com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo mMiuiSingleKeyInfo;
	 /* # direct methods */
	 public com.android.server.policy.MiuiSingleKeyRule ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .param p3, "miuiSingleKeyInfo" # Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo; */
		 /* .param p4, "currentUserId" # I */
		 /* .line 33 */
		 v0 = 		 (( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo ) p3 ).getPrimaryKey ( ); // invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getPrimaryKey()I
		 /* invoke-direct {p0, v0}, Lcom/android/server/policy/SingleKeyGestureDetector$SingleKeyRule;-><init>(I)V */
		 /* .line 34 */
		 v0 = 		 (( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo ) p3 ).getPrimaryKey ( ); // invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getPrimaryKey()I
		 /* iput v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mKeyCode:I */
		 /* .line 35 */
		 this.mContext = p1;
		 /* .line 36 */
		 this.mHandler = p2;
		 /* .line 37 */
		 /* iput p4, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mCurrentUserId:I */
		 /* .line 38 */
		 this.mMiuiSingleKeyInfo = p3;
		 /* .line 39 */
		 return;
	 } // .end method
	 private com.android.server.policy.MiuiSingleKeyRule getInstance ( ) {
		 /* .locals 0 */
		 /* .line 55 */
	 } // .end method
	 /* # virtual methods */
	 public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
		 /* .locals 1 */
		 /* .param p1, "prefix" # Ljava/lang/String; */
		 /* .param p2, "pw" # Ljava/io/PrintWriter; */
		 /* .line 83 */
		 v0 = this.mMiuiShortcutObserver;
		 (( com.android.server.policy.MiuiShortcutObserver ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/MiuiShortcutObserver;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
		 /* .line 85 */
		 return;
	 } // .end method
	 public Boolean equals ( java.lang.Object p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* .line 13 */
		 p1 = 		 /* invoke-super {p0, p1}, Lcom/android/server/policy/SingleKeyGestureDetector$SingleKeyRule;->equals(Ljava/lang/Object;)Z */
	 } // .end method
	 public java.util.Map getActionAndFunctionMap ( ) {
		 /* .locals 1 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "()", */
		 /* "Ljava/util/Map<", */
		 /* "Ljava/lang/String;", */
		 /* "Ljava/lang/String;", */
		 /* ">;" */
		 /* } */
	 } // .end annotation
	 /* .line 75 */
	 v0 = this.mMiuiShortcutObserver;
	 (( com.android.server.policy.MiuiShortcutObserver ) v0 ).getActionAndFunctionMap ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutObserver;->getActionAndFunctionMap()Ljava/util/Map;
} // .end method
public java.lang.String getFunction ( java.lang.String p0 ) {
	 /* .locals 1 */
	 /* .param p1, "action" # Ljava/lang/String; */
	 /* .line 67 */
	 v0 = this.mMiuiShortcutObserver;
	 (( com.android.server.policy.MiuiShortcutObserver ) v0 ).getFunction ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiShortcutObserver;->getFunction(Ljava/lang/String;)Ljava/lang/String;
} // .end method
Long getLongPressTimeoutMs ( ) {
	 /* .locals 2 */
	 /* .line 134 */
	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).getMiuiLongPressTimeoutMs ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getMiuiLongPressTimeoutMs()J
	 /* move-result-wide v0 */
	 /* return-wide v0 */
} // .end method
Integer getMaxMultiPressCount ( ) {
	 /* .locals 1 */
	 /* .line 187 */
	 v0 = 	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).getMiuiMaxMultiPressCount ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getMiuiMaxMultiPressCount()I
} // .end method
protected Long getMiuiLongPressTimeoutMs ( ) {
	 /* .locals 2 */
	 /* .line 143 */
	 v0 = this.mMiuiShortcutTriggerHelper;
	 v0 = 	 (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).getDefaultLongPressTimeOut ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getDefaultLongPressTimeOut()I
	 /* int-to-long v0, v0 */
	 /* return-wide v0 */
} // .end method
protected Integer getMiuiMaxMultiPressCount ( ) {
	 /* .locals 1 */
	 /* .line 191 */
	 int v0 = 1; // const/4 v0, 0x1
} // .end method
protected Long getMiuiVeryLongPressTimeoutMs ( ) {
	 /* .locals 2 */
	 /* .line 163 */
	 /* const-wide/16 v0, 0xbb8 */
	 /* return-wide v0 */
} // .end method
public com.android.server.policy.MiuiShortcutObserver getObserver ( ) {
	 /* .locals 1 */
	 /* .line 59 */
	 v0 = this.mMiuiShortcutObserver;
} // .end method
public Integer getPrimaryKey ( ) {
	 /* .locals 1 */
	 /* .line 63 */
	 /* iget v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mKeyCode:I */
} // .end method
Long getVeryLongPressTimeoutMs ( ) {
	 /* .locals 2 */
	 /* .line 159 */
	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).getMiuiVeryLongPressTimeoutMs ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getMiuiVeryLongPressTimeoutMs()J
	 /* move-result-wide v0 */
	 /* return-wide v0 */
} // .end method
public Integer hashCode ( ) { //bridge//synthethic
	 /* .locals 1 */
	 /* .line 13 */
	 v0 = 	 /* invoke-super {p0}, Lcom/android/server/policy/SingleKeyGestureDetector$SingleKeyRule;->hashCode()I */
} // .end method
public void init ( ) {
	 /* .locals 5 */
	 /* .line 42 */
	 v0 = this.mContext;
	 com.android.server.policy.MiuiShortcutTriggerHelper .getInstance ( v0 );
	 this.mMiuiShortcutTriggerHelper = v0;
	 /* .line 43 */
	 /* new-instance v0, Lcom/android/server/policy/MiuiSingleKeyObserver; */
	 v1 = this.mHandler;
	 v2 = this.mContext;
	 v3 = this.mMiuiSingleKeyInfo;
	 /* .line 44 */
	 (( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo ) v3 ).getActionAndDefaultFunctionMap ( ); // invoke-virtual {v3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getActionAndDefaultFunctionMap()Ljava/util/Map;
	 /* iget v4, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mCurrentUserId:I */
	 /* invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/policy/MiuiSingleKeyObserver;-><init>(Landroid/os/Handler;Landroid/content/Context;Ljava/util/Map;I)V */
	 this.mMiuiShortcutObserver = v0;
	 /* .line 45 */
	 /* invoke-direct {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getInstance()Lcom/android/server/policy/MiuiSingleKeyRule; */
	 (( com.android.server.policy.MiuiShortcutObserver ) v0 ).setRuleForObserver ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->setRuleForObserver(Lcom/android/server/policy/MiuiSingleKeyRule;)V
	 /* .line 46 */
	 v0 = this.mMiuiShortcutObserver;
	 int v1 = 0; // const/4 v1, 0x0
	 (( com.android.server.policy.MiuiShortcutObserver ) v0 ).setDefaultFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V
	 /* .line 47 */
	 return;
} // .end method
protected Boolean miuiSupportLongPress ( ) {
	 /* .locals 1 */
	 /* .line 93 */
	 int v0 = 1; // const/4 v0, 0x1
} // .end method
protected Boolean miuiSupportVeryLongPress ( ) {
	 /* .locals 1 */
	 /* .line 102 */
	 int v0 = 0; // const/4 v0, 0x0
} // .end method
void onKeyDown ( android.view.KeyEvent p0 ) {
	 /* .locals 0 */
	 /* .param p1, "event" # Landroid/view/KeyEvent; */
	 /* .line 112 */
	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).onMiuiKeyDown ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->onMiuiKeyDown(Landroid/view/KeyEvent;)V
	 /* .line 113 */
	 return;
} // .end method
void onLongPress ( Long p0 ) {
	 /* .locals 2 */
	 /* .param p1, "eventTime" # J */
	 /* .line 149 */
	 v0 = this.mMiuiShortcutTriggerHelper;
	 /* iget v1, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mKeyCode:I */
	 (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).notifyLongPressed ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->notifyLongPressed(I)V
	 /* .line 150 */
	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).onMiuiLongPress ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyRule;->onMiuiLongPress(J)V
	 /* .line 151 */
	 return;
} // .end method
void onLongPressKeyUp ( android.view.KeyEvent p0 ) {
	 /* .locals 0 */
	 /* .param p1, "event" # Landroid/view/KeyEvent; */
	 /* .line 178 */
	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).onMiuiLongPressKeyUp ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->onMiuiLongPressKeyUp(Landroid/view/KeyEvent;)V
	 /* .line 179 */
	 return;
} // .end method
protected void onMiuiKeyDown ( android.view.KeyEvent p0 ) {
	 /* .locals 0 */
	 /* .param p1, "event" # Landroid/view/KeyEvent; */
	 /* .line 117 */
	 return;
} // .end method
protected void onMiuiLongPress ( Long p0 ) {
	 /* .locals 0 */
	 /* .param p1, "eventTime" # J */
	 /* .line 155 */
	 return;
} // .end method
protected void onMiuiLongPressKeyUp ( android.view.KeyEvent p0 ) {
	 /* .locals 0 */
	 /* .param p1, "event" # Landroid/view/KeyEvent; */
	 /* .line 183 */
	 return;
} // .end method
protected void onMiuiMultiPress ( Long p0, Integer p1 ) {
	 /* .locals 0 */
	 /* .param p1, "downTime" # J */
	 /* .param p3, "count" # I */
	 /* .line 130 */
	 return;
} // .end method
protected void onMiuiPress ( Long p0 ) {
	 /* .locals 0 */
	 /* .param p1, "downTime" # J */
	 /* .line 121 */
	 return;
} // .end method
protected void onMiuiVeryLongPress ( Long p0 ) {
	 /* .locals 0 */
	 /* .param p1, "eventTime" # J */
	 /* .line 174 */
	 return;
} // .end method
void onMultiPress ( Long p0, Integer p1 ) {
	 /* .locals 0 */
	 /* .param p1, "downTime" # J */
	 /* .param p3, "count" # I */
	 /* .line 125 */
	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).onMiuiMultiPress ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/policy/MiuiSingleKeyRule;->onMiuiMultiPress(JI)V
	 /* .line 126 */
	 return;
} // .end method
void onPress ( Long p0 ) {
	 /* .locals 0 */
	 /* .param p1, "downTime" # J */
	 /* .line 107 */
	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).onMiuiPress ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyRule;->onMiuiPress(J)V
	 /* .line 108 */
	 return;
} // .end method
public void onUserSwitch ( Integer p0, Boolean p1 ) {
	 /* .locals 1 */
	 /* .param p1, "currentUserId" # I */
	 /* .param p2, "isNewUser" # Z */
	 /* .line 71 */
	 v0 = this.mMiuiShortcutObserver;
	 (( com.android.server.policy.MiuiShortcutObserver ) v0 ).onUserSwitch ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/MiuiShortcutObserver;->onUserSwitch(IZ)V
	 /* .line 72 */
	 return;
} // .end method
void onVeryLongPress ( Long p0 ) {
	 /* .locals 0 */
	 /* .param p1, "eventTime" # J */
	 /* .line 169 */
	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).onMiuiVeryLongPress ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyRule;->onMiuiVeryLongPress(J)V
	 /* .line 170 */
	 return;
} // .end method
public void registerShortcutListener ( com.android.server.policy.MiuiShortcutObserver$MiuiShortcutListener p0 ) {
	 /* .locals 1 */
	 /* .param p1, "miuiShortcutListener" # Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener; */
	 /* .line 51 */
	 v0 = this.mMiuiShortcutObserver;
	 (( com.android.server.policy.MiuiShortcutObserver ) v0 ).registerShortcutListener ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiShortcutObserver;->registerShortcutListener(Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;)V
	 /* .line 52 */
	 return;
} // .end method
Boolean supportLongPress ( ) {
	 /* .locals 1 */
	 /* .line 89 */
	 v0 = 	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).miuiSupportLongPress ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->miuiSupportLongPress()Z
} // .end method
Boolean supportVeryLongPress ( ) {
	 /* .locals 1 */
	 /* .line 98 */
	 v0 = 	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).miuiSupportVeryLongPress ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->miuiSupportVeryLongPress()Z
} // .end method
public java.lang.String toString ( ) {
	 /* .locals 3 */
	 /* .line 196 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "KeyCode="; // const-string v1, "KeyCode="
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mKeyCode:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v1 = ", SupportLongPress="; // const-string v1, ", SupportLongPress="
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 197 */
	 v1 = 	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).miuiSupportLongPress ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->miuiSupportLongPress()Z
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 final String v1 = ", SupportVeryLongPress="; // const-string v1, ", SupportVeryLongPress="
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 198 */
	 v1 = 	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).miuiSupportVeryLongPress ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->miuiSupportVeryLongPress()Z
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 final String v1 = ", LongPressTimeOut="; // const-string v1, ", LongPressTimeOut="
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 199 */
	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).getLongPressTimeoutMs ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getLongPressTimeoutMs()J
	 /* move-result-wide v1 */
	 (( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
	 final String v1 = ", VeryLongPressTimeOut="; // const-string v1, ", VeryLongPressTimeOut="
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 200 */
	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).getVeryLongPressTimeoutMs ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getVeryLongPressTimeoutMs()J
	 /* move-result-wide v1 */
	 (( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
	 final String v1 = ", MaxCount="; // const-string v1, ", MaxCount="
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 201 */
	 v1 = 	 (( com.android.server.policy.MiuiSingleKeyRule ) p0 ).getMaxMultiPressCount ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getMaxMultiPressCount()I
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 196 */
} // .end method
public void updatePolicyFlag ( Integer p0 ) {
	 /* .locals 0 */
	 /* .param p1, "policyFlags" # I */
	 /* .line 80 */
	 return;
} // .end method
