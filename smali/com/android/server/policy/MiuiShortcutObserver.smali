.class public abstract Lcom/android/server/policy/MiuiShortcutObserver;
.super Landroid/database/ContentObserver;
.source "MiuiShortcutObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;
    }
.end annotation


# static fields
.field public static final CURRENT_DEVICE_REGION:Ljava/lang/String;

.field private static final DEVICE_REGION_RUSSIA:Ljava/lang/String; = "ru"

.field public static final DOUBLE_VOLUME_DOWN_KEY_TYPE_LAUNCH_CAMERA_AND_TAKE_PHOTO:I = 0x2

.field public static final SUPPORT_VARIABLE_APERTURE:Ljava/lang/Boolean;

.field private static final TAG:Ljava/lang/String; = "MiuiShortcutObserver"


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field protected mCurrentUserId:I

.field private final mHandler:Landroid/os/Handler;

.field private final mMiuiShortcutListeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mMiuiShortcutOperatorCustomManager:Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 32
    nop

    .line 33
    const-string v0, "ro.miui.build.region"

    const-string v1, "CN"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/policy/MiuiShortcutObserver;->CURRENT_DEVICE_REGION:Ljava/lang/String;

    .line 37
    const-string v0, "persist.vendor.camera.IsVariableApertureSupported"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/android/server/policy/MiuiShortcutObserver;->SUPPORT_VARIABLE_APERTURE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Landroid/content/Context;I)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "currentUserId"    # I

    .line 49
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 40
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mMiuiShortcutListeners:Ljava/util/HashSet;

    .line 50
    iput-object p2, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mContext:Landroid/content/Context;

    .line 51
    iput-object p1, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mHandler:Landroid/os/Handler;

    .line 52
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mContentResolver:Landroid/content/ContentResolver;

    .line 53
    iput p3, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mCurrentUserId:I

    .line 54
    invoke-static {p2}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->getInstance(Landroid/content/Context;)Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mMiuiShortcutOperatorCustomManager:Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;

    .line 56
    return-void
.end method

.method private enableLongPressPowerLaunchAssistant(Ljava/lang/String;)Z
    .locals 2
    .param p1, "function"    # Ljava/lang/String;

    .line 191
    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v0, :cond_0

    .line 192
    const-string v0, "launch_google_search"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 194
    .local v0, "enableLongPressPowerLaunchAssistant":Z
    invoke-direct {p0}, Lcom/android/server/policy/MiuiShortcutObserver;->supportRSARegion()Z

    move-result v1

    if-nez v1, :cond_1

    .line 195
    const/4 v0, 0x0

    goto :goto_0

    .line 198
    .end local v0    # "enableLongPressPowerLaunchAssistant":Z
    :cond_0
    nop

    .line 199
    const-string v0, "launch_voice_assistant"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 201
    .restart local v0    # "enableLongPressPowerLaunchAssistant":Z
    :cond_1
    :goto_0
    return v0
.end method

.method private supportRSARegion()Z
    .locals 2

    .line 214
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/policy/MiuiShortcutObserver;->CURRENT_DEVICE_REGION:Ljava/lang/String;

    .line 215
    const-string v1, "ru"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 214
    :goto_0
    return v0
.end method


# virtual methods
.method public currentFunctionValueIsInt(Ljava/lang/String;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 97
    const-string/jumbo v0, "volumekey_launch_camera"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 220
    return-void
.end method

.method protected getActionAndFunctionMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 131
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFunction()Ljava/lang/String;
    .locals 1

    .line 118
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFunction(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 122
    const/4 v0, 0x0

    return-object v0
.end method

.method protected hasCustomizedFunction(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 5
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;
    .param p3, "isNeedReset"    # Z

    .line 157
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mMiuiShortcutOperatorCustomManager:Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;

    invoke-virtual {v0, p2, p1}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->hasOperatorCustomFunctionForMiuiRule(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 159
    return v1

    .line 162
    :cond_0
    const-string v0, "long_press_power_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164
    if-eqz p3, :cond_1

    .line 165
    invoke-direct {p0, p2}, Lcom/android/server/policy/MiuiShortcutObserver;->enableLongPressPowerLaunchAssistant(Ljava/lang/String;)Z

    move-result v0

    .line 167
    .local v0, "enableLongPressPowerLaunchAssistant":Z
    iget-object v2, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mContentResolver:Landroid/content/ContentResolver;

    .line 169
    iget v3, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mCurrentUserId:I

    .line 167
    const-string v4, "long_press_power_launch_xiaoai"

    invoke-static {v2, v4, v0, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 170
    .end local v0    # "enableLongPressPowerLaunchAssistant":Z
    goto :goto_0

    .line 173
    :cond_1
    const-string v0, "launch_google_search"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/server/policy/MiuiShortcutObserver;->supportRSARegion()Z

    move-result v0

    if-nez v0, :cond_2

    .line 174
    return v1

    .line 179
    :cond_2
    :goto_0
    sget-object v0, Lcom/android/server/policy/MiuiShortcutObserver;->SUPPORT_VARIABLE_APERTURE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "volumekey_launch_camera"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 180
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 181
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x2

    iget v3, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mCurrentUserId:I

    invoke-static {v0, p1, v2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 184
    return v1

    .line 186
    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method protected isFeasibleFunction(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 2
    .param p1, "function"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .line 142
    const-string v0, "mi_pay"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    invoke-static {p2}, Landroid/provider/MiuiSettings$Key;->isTSMClientInstalled(Landroid/content/Context;)Z

    move-result v0

    return v0

    .line 144
    :cond_0
    const-string v0, "launch_voice_assistant"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    invoke-static {p2}, Landroid/provider/MiuiSettings$System;->isXiaoAiExist(Landroid/content/Context;)Z

    move-result v0

    return v0

    .line 146
    :cond_1
    const-string v0, "partial_screen_shot"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    .line 147
    const-string v0, "persist.sys.partial.screenshot.support"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0

    .line 150
    :cond_2
    return v1
.end method

.method protected notifyCombinationRuleChanged(Lcom/android/server/policy/MiuiCombinationRule;)V
    .locals 3
    .param p1, "rule"    # Lcom/android/server/policy/MiuiCombinationRule;

    .line 75
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mMiuiShortcutListeners:Ljava/util/HashSet;

    monitor-enter v0

    .line 76
    :try_start_0
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mMiuiShortcutListeners:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;

    .line 77
    .local v2, "listener":Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;
    invoke-interface {v2, p1}, Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;->onCombinationChanged(Lcom/android/server/policy/MiuiCombinationRule;)V

    .line 78
    .end local v2    # "listener":Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;
    goto :goto_0

    .line 79
    :cond_0
    monitor-exit v0

    .line 80
    return-void

    .line 79
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected notifyGestureRuleChanged(Lcom/android/server/policy/MiuiGestureRule;)V
    .locals 3
    .param p1, "rule"    # Lcom/android/server/policy/MiuiGestureRule;

    .line 83
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mMiuiShortcutListeners:Ljava/util/HashSet;

    monitor-enter v0

    .line 84
    :try_start_0
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mMiuiShortcutListeners:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;

    .line 85
    .local v2, "listener":Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;
    invoke-interface {v2, p1}, Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;->onGestureChanged(Lcom/android/server/policy/MiuiGestureRule;)V

    .line 86
    .end local v2    # "listener":Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;
    goto :goto_0

    .line 87
    :cond_0
    monitor-exit v0

    .line 88
    return-void

    .line 87
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected notifySingleRuleChanged(Lcom/android/server/policy/MiuiSingleKeyRule;Landroid/net/Uri;)V
    .locals 3
    .param p1, "rule"    # Lcom/android/server/policy/MiuiSingleKeyRule;
    .param p2, "uri"    # Landroid/net/Uri;

    .line 67
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mMiuiShortcutListeners:Ljava/util/HashSet;

    monitor-enter v0

    .line 68
    :try_start_0
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mMiuiShortcutListeners:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;

    .line 69
    .local v2, "listener":Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;
    invoke-interface {v2, p1, p2}, Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;->onSingleChanged(Lcom/android/server/policy/MiuiSingleKeyRule;Landroid/net/Uri;)V

    .line 70
    .end local v2    # "listener":Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;
    goto :goto_0

    .line 71
    :cond_0
    monitor-exit v0

    .line 72
    return-void

    .line 71
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onDestroy()V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mContentResolver:Landroid/content/ContentResolver;

    if-eqz v0, :cond_0

    .line 136
    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 138
    :cond_0
    return-void
.end method

.method public onUserSwitch(IZ)V
    .locals 1
    .param p1, "currentUserId"    # I
    .param p2, "isNewUser"    # Z

    .line 205
    const/4 v0, -0x2

    iput v0, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mCurrentUserId:I

    .line 206
    if-eqz p2, :cond_0

    .line 207
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V

    goto :goto_0

    .line 209
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutObserver;->updateRuleInfo()V

    .line 211
    :goto_0
    return-void
.end method

.method public registerShortcutListener(Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;)V
    .locals 2
    .param p1, "miuiShortcutListener"    # Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;

    .line 59
    if-eqz p1, :cond_0

    .line 60
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mMiuiShortcutListeners:Ljava/util/HashSet;

    monitor-enter v0

    .line 61
    :try_start_0
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mMiuiShortcutListeners:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 62
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 64
    :cond_0
    :goto_0
    return-void
.end method

.method public setDefaultFunction(Z)V
    .locals 0
    .param p1, "isNeedReset"    # Z

    .line 113
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutObserver;->updateRuleInfo()V

    .line 114
    return-void
.end method

.method setRuleForObserver(Lcom/android/server/policy/MiuiCombinationRule;)V
    .locals 0
    .param p1, "miuiCombinationRule"    # Lcom/android/server/policy/MiuiCombinationRule;

    .line 224
    return-void
.end method

.method setRuleForObserver(Lcom/android/server/policy/MiuiGestureRule;)V
    .locals 0
    .param p1, "miuiGestureRule"    # Lcom/android/server/policy/MiuiGestureRule;

    .line 228
    return-void
.end method

.method setRuleForObserver(Lcom/android/server/policy/MiuiSingleKeyRule;)V
    .locals 0
    .param p1, "miuiSingleKeyRule"    # Lcom/android/server/policy/MiuiSingleKeyRule;

    .line 232
    return-void
.end method

.method protected unRegisterShortcutListener(Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;Ljava/lang/String;)V
    .locals 2
    .param p1, "listener"    # Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;
    .param p2, "action"    # Ljava/lang/String;

    .line 101
    if-eqz p1, :cond_0

    .line 102
    iget-object v0, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mMiuiShortcutListeners:Ljava/util/HashSet;

    monitor-enter v0

    .line 103
    :try_start_0
    iget-object v1, p0, Lcom/android/server/policy/MiuiShortcutObserver;->mMiuiShortcutListeners:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 104
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 106
    :cond_0
    :goto_0
    return-void
.end method

.method abstract updateRuleInfo()V
.end method
