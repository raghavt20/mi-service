.class public abstract Lcom/android/server/policy/MiuiSingleKeyRule;
.super Lcom/android/server/policy/SingleKeyGestureDetector$SingleKeyRule;
.source "MiuiSingleKeyRule.java"


# static fields
.field public static final ACTION_TYPE_DOUBLE_CLICK:Ljava/lang/String; = "doubleClick"

.field public static final ACTION_TYPE_FIVE_CLICK:Ljava/lang/String; = "fiveClick"

.field public static final ACTION_TYPE_LONG_PRESS:Ljava/lang/String; = "longPress"

.field public static final DEFAULT_VERY_LONG_PRESS_TIME_OUT:J = 0xbb8L

.field public static final DOUBLE_TIMES_TAP:I = 0x2

.field public static final FIVE_TIMES_TAP:I = 0x5

.field public static final SINGLE_TAP:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MiuiSingleKeyRule"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private final mHandler:Landroid/os/Handler;

.field private final mKeyCode:I

.field private mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

.field private mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

.field private final mMiuiSingleKeyInfo:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "miuiSingleKeyInfo"    # Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;
    .param p4, "currentUserId"    # I

    .line 33
    invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getPrimaryKey()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/policy/SingleKeyGestureDetector$SingleKeyRule;-><init>(I)V

    .line 34
    invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getPrimaryKey()I

    move-result v0

    iput v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mKeyCode:I

    .line 35
    iput-object p1, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mContext:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mHandler:Landroid/os/Handler;

    .line 37
    iput p4, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mCurrentUserId:I

    .line 38
    iput-object p3, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mMiuiSingleKeyInfo:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;

    .line 39
    return-void
.end method

.method private getInstance()Lcom/android/server/policy/MiuiSingleKeyRule;
    .locals 0

    .line 55
    return-object p0
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 83
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/MiuiShortcutObserver;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 85
    return-void
.end method

.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 0

    .line 13
    invoke-super {p0, p1}, Lcom/android/server/policy/SingleKeyGestureDetector$SingleKeyRule;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getActionAndFunctionMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutObserver;->getActionAndFunctionMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getFunction(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 67
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiShortcutObserver;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getLongPressTimeoutMs()J
    .locals 2

    .line 134
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getMiuiLongPressTimeoutMs()J

    move-result-wide v0

    return-wide v0
.end method

.method getMaxMultiPressCount()I
    .locals 1

    .line 187
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getMiuiMaxMultiPressCount()I

    move-result v0

    return v0
.end method

.method protected getMiuiLongPressTimeoutMs()J
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getDefaultLongPressTimeOut()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method protected getMiuiMaxMultiPressCount()I
    .locals 1

    .line 191
    const/4 v0, 0x1

    return v0
.end method

.method protected getMiuiVeryLongPressTimeoutMs()J
    .locals 2

    .line 163
    const-wide/16 v0, 0xbb8

    return-wide v0
.end method

.method public getObserver()Lcom/android/server/policy/MiuiShortcutObserver;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    return-object v0
.end method

.method public getPrimaryKey()I
    .locals 1

    .line 63
    iget v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mKeyCode:I

    return v0
.end method

.method getVeryLongPressTimeoutMs()J
    .locals 2

    .line 159
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getMiuiVeryLongPressTimeoutMs()J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic hashCode()I
    .locals 1

    .line 13
    invoke-super {p0}, Lcom/android/server/policy/SingleKeyGestureDetector$SingleKeyRule;->hashCode()I

    move-result v0

    return v0
.end method

.method public init()V
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 43
    new-instance v0, Lcom/android/server/policy/MiuiSingleKeyObserver;

    iget-object v1, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mMiuiSingleKeyInfo:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;

    .line 44
    invoke-virtual {v3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getActionAndDefaultFunctionMap()Ljava/util/Map;

    move-result-object v3

    iget v4, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mCurrentUserId:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/policy/MiuiSingleKeyObserver;-><init>(Landroid/os/Handler;Landroid/content/Context;Ljava/util/Map;I)V

    iput-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    .line 45
    invoke-direct {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getInstance()Lcom/android/server/policy/MiuiSingleKeyRule;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->setRuleForObserver(Lcom/android/server/policy/MiuiSingleKeyRule;)V

    .line 46
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V

    .line 47
    return-void
.end method

.method protected miuiSupportLongPress()Z
    .locals 1

    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method protected miuiSupportVeryLongPress()Z
    .locals 1

    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method onKeyDown(Landroid/view/KeyEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 112
    invoke-virtual {p0, p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->onMiuiKeyDown(Landroid/view/KeyEvent;)V

    .line 113
    return-void
.end method

.method onLongPress(J)V
    .locals 2
    .param p1, "eventTime"    # J

    .line 149
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    iget v1, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mKeyCode:I

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->notifyLongPressed(I)V

    .line 150
    invoke-virtual {p0, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyRule;->onMiuiLongPress(J)V

    .line 151
    return-void
.end method

.method onLongPressKeyUp(Landroid/view/KeyEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 178
    invoke-virtual {p0, p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->onMiuiLongPressKeyUp(Landroid/view/KeyEvent;)V

    .line 179
    return-void
.end method

.method protected onMiuiKeyDown(Landroid/view/KeyEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 117
    return-void
.end method

.method protected onMiuiLongPress(J)V
    .locals 0
    .param p1, "eventTime"    # J

    .line 155
    return-void
.end method

.method protected onMiuiLongPressKeyUp(Landroid/view/KeyEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 183
    return-void
.end method

.method protected onMiuiMultiPress(JI)V
    .locals 0
    .param p1, "downTime"    # J
    .param p3, "count"    # I

    .line 130
    return-void
.end method

.method protected onMiuiPress(J)V
    .locals 0
    .param p1, "downTime"    # J

    .line 121
    return-void
.end method

.method protected onMiuiVeryLongPress(J)V
    .locals 0
    .param p1, "eventTime"    # J

    .line 174
    return-void
.end method

.method onMultiPress(JI)V
    .locals 0
    .param p1, "downTime"    # J
    .param p3, "count"    # I

    .line 125
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/policy/MiuiSingleKeyRule;->onMiuiMultiPress(JI)V

    .line 126
    return-void
.end method

.method onPress(J)V
    .locals 0
    .param p1, "downTime"    # J

    .line 107
    invoke-virtual {p0, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyRule;->onMiuiPress(J)V

    .line 108
    return-void
.end method

.method public onUserSwitch(IZ)V
    .locals 1
    .param p1, "currentUserId"    # I
    .param p2, "isNewUser"    # Z

    .line 71
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/MiuiShortcutObserver;->onUserSwitch(IZ)V

    .line 72
    return-void
.end method

.method onVeryLongPress(J)V
    .locals 0
    .param p1, "eventTime"    # J

    .line 169
    invoke-virtual {p0, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyRule;->onMiuiVeryLongPress(J)V

    .line 170
    return-void
.end method

.method public registerShortcutListener(Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;)V
    .locals 1
    .param p1, "miuiShortcutListener"    # Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;

    .line 51
    iget-object v0, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiShortcutObserver;->registerShortcutListener(Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;)V

    .line 52
    return-void
.end method

.method supportLongPress()Z
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->miuiSupportLongPress()Z

    move-result v0

    return v0
.end method

.method supportVeryLongPress()Z
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->miuiSupportVeryLongPress()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 196
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KeyCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/policy/MiuiSingleKeyRule;->mKeyCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", SupportLongPress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 197
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->miuiSupportLongPress()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", SupportVeryLongPress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 198
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->miuiSupportVeryLongPress()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", LongPressTimeOut="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 199
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getLongPressTimeoutMs()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", VeryLongPressTimeOut="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 200
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getVeryLongPressTimeoutMs()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", MaxCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 201
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getMaxMultiPressCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 196
    return-object v0
.end method

.method public updatePolicyFlag(I)V
    .locals 0
    .param p1, "policyFlags"    # I

    .line 80
    return-void
.end method
