.class public Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;
.super Lcom/android/server/policy/MiuiShortcutObserver;
.source "MiuiCombinationKeyAndGestureObserver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiCombinationKeyAndGestureObserver"


# instance fields
.field private final mAction:Ljava/lang/String;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private final mDefaultFunction:Ljava/lang/String;

.field private mFunction:Ljava/lang/String;

.field private mMiuiCombinationRule:Lcom/android/server/policy/MiuiCombinationRule;

.field private mMiuiGestureRule:Lcom/android/server/policy/MiuiGestureRule;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "defaultFunction"    # Ljava/lang/String;
    .param p5, "currentUserId"    # I

    .line 23
    invoke-direct {p0, p2, p1, p5}, Lcom/android/server/policy/MiuiShortcutObserver;-><init>(Landroid/os/Handler;Landroid/content/Context;I)V

    .line 24
    iput-object p3, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mAction:Ljava/lang/String;

    .line 25
    iput-object p4, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mDefaultFunction:Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mContext:Landroid/content/Context;

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mContentResolver:Landroid/content/ContentResolver;

    .line 28
    invoke-direct {p0, p3}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->registerShortcutAction(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method private registerShortcutAction(Ljava/lang/String;)V
    .locals 4
    .param p1, "action"    # Ljava/lang/String;

    .line 37
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mContentResolver:Landroid/content/ContentResolver;

    .line 38
    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 37
    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 40
    return-void
.end method

.method private updateFunction()V
    .locals 4

    .line 92
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mAction:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->currentFunctionValueIsInt(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mAction:Ljava/lang/String;

    iget v2, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mCurrentUserId:I

    const/4 v3, -0x1

    invoke-static {v0, v1, v3, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 95
    .local v0, "functionStatus":I
    if-ne v0, v3, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 96
    .local v0, "function":Ljava/lang/String;
    goto :goto_1

    .line 97
    .end local v0    # "function":Ljava/lang/String;
    :cond_1
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mAction:Ljava/lang/String;

    iget v2, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mCurrentUserId:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 99
    .restart local v0    # "function":Ljava/lang/String;
    :goto_1
    iput-object v0, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mFunction:Ljava/lang/String;

    .line 100
    return-void
.end method


# virtual methods
.method public getFunction()Ljava/lang/String;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mFunction:Ljava/lang/String;

    return-object v0
.end method

.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .line 44
    invoke-direct {p0}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->updateFunction()V

    .line 45
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mMiuiGestureRule:Lcom/android/server/policy/MiuiGestureRule;

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->notifyGestureRuleChanged(Lcom/android/server/policy/MiuiGestureRule;)V

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mMiuiCombinationRule:Lcom/android/server/policy/MiuiCombinationRule;

    if-eqz v0, :cond_1

    .line 49
    invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->notifyCombinationRuleChanged(Lcom/android/server/policy/MiuiCombinationRule;)V

    .line 51
    :cond_1
    invoke-super {p0, p1}, Lcom/android/server/policy/MiuiShortcutObserver;->onChange(Z)V

    .line 52
    return-void
.end method

.method public setDefaultFunction(Z)V
    .locals 5
    .param p1, "isNeedReset"    # Z

    .line 57
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mAction:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->currentFunctionValueIsInt(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mAction:Ljava/lang/String;

    iget v3, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mCurrentUserId:I

    const/4 v4, -0x1

    invoke-static {v0, v2, v4, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 60
    .local v0, "functionStatus":I
    if-ne v0, v4, :cond_0

    move-object v2, v1

    goto :goto_0

    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object v0, v2

    .line 61
    .local v0, "currentFunction":Ljava/lang/String;
    goto :goto_2

    .line 63
    .end local v0    # "currentFunction":Ljava/lang/String;
    :cond_1
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mAction:Ljava/lang/String;

    iget v3, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mCurrentUserId:I

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 65
    .restart local v0    # "currentFunction":Ljava/lang/String;
    if-nez v0, :cond_2

    iget-object v2, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mDefaultFunction:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object v2, v0

    :goto_1
    move-object v0, v2

    .line 67
    :goto_2
    if-eqz p1, :cond_3

    .line 68
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mDefaultFunction:Ljava/lang/String;

    .line 70
    :cond_3
    iget-object v2, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mAction:Ljava/lang/String;

    invoke-virtual {p0, v2, v0, p1}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->hasCustomizedFunction(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_5

    .line 71
    iget-object v2, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0, v2}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->isFeasibleFunction(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 72
    iget-object v1, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mAction:Ljava/lang/String;

    iget v3, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mCurrentUserId:I

    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    goto :goto_3

    .line 75
    :cond_4
    if-eqz p1, :cond_5

    .line 77
    iget-object v2, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mAction:Ljava/lang/String;

    iget v4, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mCurrentUserId:I

    invoke-static {v2, v3, v1, v4}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 82
    :cond_5
    :goto_3
    invoke-super {p0, p1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V

    .line 83
    return-void
.end method

.method public setRuleForObserver(Lcom/android/server/policy/MiuiCombinationRule;)V
    .locals 0
    .param p1, "miuiCombinationRule"    # Lcom/android/server/policy/MiuiCombinationRule;

    .line 104
    iput-object p1, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mMiuiCombinationRule:Lcom/android/server/policy/MiuiCombinationRule;

    .line 105
    return-void
.end method

.method public setRuleForObserver(Lcom/android/server/policy/MiuiGestureRule;)V
    .locals 0
    .param p1, "miuiGestureRule"    # Lcom/android/server/policy/MiuiGestureRule;

    .line 109
    iput-object p1, p0, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->mMiuiGestureRule:Lcom/android/server/policy/MiuiGestureRule;

    .line 110
    return-void
.end method

.method updateRuleInfo()V
    .locals 1

    .line 87
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;->onChange(Z)V

    .line 88
    return-void
.end method
