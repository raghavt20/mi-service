.class Lcom/android/server/policy/MiuiKeyInterceptExtend$2;
.super Ljava/lang/Object;
.source "MiuiKeyInterceptExtend.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/policy/MiuiKeyInterceptExtend;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/MiuiKeyInterceptExtend;


# direct methods
.method constructor <init>(Lcom/android/server/policy/MiuiKeyInterceptExtend;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/policy/MiuiKeyInterceptExtend;

    .line 88
    iput-object p1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend$2;->this$0:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 91
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend$2;->this$0:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    invoke-static {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->-$$Nest$fgetmTriggerLongPress(Lcom/android/server/policy/MiuiKeyInterceptExtend;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend$2;->this$0:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    invoke-static {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->-$$Nest$fgetmContext(Lcom/android/server/policy/MiuiKeyInterceptExtend;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string/jumbo v3, "screen_shot"

    const-string v4, "keyboard"

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 95
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend$2;->this$0:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    invoke-static {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->-$$Nest$fgetmContext(Lcom/android/server/policy/MiuiKeyInterceptExtend;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v0

    .line 96
    const/16 v1, 0x270a

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil$KeyBoardShortcut;->getShortcutNameByKeyCodeWithAction(IZ)Ljava/lang/String;

    move-result-object v1

    .line 95
    invoke-virtual {v0, v1}, Lcom/android/server/input/InputOneTrackUtil;->track6FShortcut(Ljava/lang/String;)V

    .line 99
    :cond_0
    return-void
.end method
