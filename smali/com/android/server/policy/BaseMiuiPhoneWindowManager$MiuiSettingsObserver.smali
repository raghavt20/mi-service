.class Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;
.super Landroid/database/ContentObserver;
.source "BaseMiuiPhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MiuiSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;


# direct methods
.method public static synthetic $r8$lambda$D7FzyKjm9t67_452lQvoL2oMm20(Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->lambda$observe$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$e97DkC8pDKRtFq6Glj2fbRlVbno(Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->lambda$onChange$1()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/policy/BaseMiuiPhoneWindowManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 2308
    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    .line 2309
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 2310
    return-void
.end method

.method private synthetic lambda$observe$0()V
    .locals 4

    .line 2366
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v0, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v1

    const-string v2, "is_mi_input_event_time_line_enable"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 2368
    return-void
.end method

.method private synthetic lambda$onChange$1()V
    .locals 4

    .line 2421
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v0, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v1

    const-string v2, "is_mi_input_event_time_line_enable"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    move v0, v3

    .line 2423
    .local v0, "enable":Z
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v1

    .line 2424
    .local v1, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
    invoke-virtual {v1, v0}, Lcom/android/server/input/config/InputCommonConfig;->setMiInputEventTimeLineMode(Z)V

    .line 2425
    invoke-virtual {v1}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 2426
    return-void
.end method


# virtual methods
.method observe()V
    .locals 7

    .line 2313
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v0, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2314
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string/jumbo v1, "trackball_wake_screen"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2316
    const-string v1, "camera_key_preferred_action_type"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2318
    const-string v1, "camera_key_preferred_action_shortcut_id"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2320
    const-string/jumbo v1, "volumekey_wake_screen"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2322
    const-string v1, "key_long_press_volume_down"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2324
    const-string v1, "auto_test_mode_on"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2326
    const-string v1, "key_bank_card_in_ese"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2328
    const-string v1, "key_trans_card_in_ese"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2330
    const-string/jumbo v1, "vr_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2332
    const-string v1, "enable_mikey_mode"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2334
    const-string v1, "enabled_accessibility_services"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2336
    const-string v1, "accessibility_shortcut_target_service"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2338
    const-string v1, "accessibility_shortcut_on_lock_screen"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2340
    const-string/jumbo v1, "three_gesture_down"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2342
    const-string/jumbo v1, "three_gesture_long_press"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2344
    const-string/jumbo v1, "send_back_when_xiaoai_appear"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2346
    const-string v1, "long_press_timeout"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2349
    const-string v4, "kid_mode_status"

    invoke-static {v4}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2351
    const-string v4, "kid_user_id"

    invoke-static {v4}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2354
    iget-object v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmIsSupportGloablTounchDirection(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-boolean v4, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z

    if-eqz v4, :cond_1

    :cond_0
    sget-boolean v4, Lmiui/os/DeviceFeature;->SUPPORT_GAME_MODE:Z

    if-eqz v4, :cond_1

    .line 2356
    const-string v4, "gb_boosting"

    invoke-static {v4}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2359
    :cond_1
    nop

    .line 2360
    const-string v4, "is_custom_shortcut_effective"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 2359
    invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2362
    const-string/jumbo v5, "synergy_mode"

    invoke-static {v5}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2365
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v5

    new-instance v6, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver$$ExternalSyntheticLambda1;

    invoke-direct {v6, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2369
    nop

    .line 2370
    const-string v5, "is_mi_input_event_time_line_enable"

    invoke-static {v5}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 2369
    invoke-virtual {v0, v5, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2372
    nop

    .line 2373
    const-string v3, "imperceptible_press_power_key"

    invoke-static {v3}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2372
    invoke-virtual {p0, v2, v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 2374
    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 2375
    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 2376
    return-void
.end method

.method public onChange(Z)V
    .locals 12
    .param p1, "selfChange"    # Z

    .line 2434
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v0, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2435
    .local v0, "resolver":Landroid/content/ContentResolver;
    invoke-static {}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$sfgetphoneWindowManagerFeature()Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-virtual {v1, v2}, Lcom/android/server/policy/PhoneWindowManagerFeatureImpl;->getLock(Lcom/android/server/policy/PhoneWindowManager;)Ljava/lang/Object;

    move-result-object v1

    .line 2436
    .local v1, "lock":Ljava/lang/Object;
    monitor-enter v1

    .line 2437
    :try_start_0
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v2, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/provider/MiuiSettings$Key;->updateOldKeyFunctionToNew(Landroid/content/Context;)V

    .line 2438
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v2, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    const-string v3, "screen_key_press_app_switch"

    iget-object v4, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v4

    const/4 v5, 0x1

    invoke-static {v0, v3, v5, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    move v3, v5

    goto :goto_0

    :cond_0
    move v3, v4

    :goto_0
    invoke-virtual {v2, v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setPressToAppSwitch(Z)V

    .line 2442
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const-string/jumbo v3, "trackball_wake_screen"

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v6

    invoke-static {v0, v3, v4, v6}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-ne v3, v5, :cond_1

    move v3, v5

    goto :goto_1

    :cond_1
    move v3, v4

    :goto_1
    iput-boolean v3, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTrackballWakeScreen:Z

    .line 2444
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const-string v3, "enable_mikey_mode"

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v6

    invoke-static {v0, v3, v4, v6}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_2

    move v3, v5

    goto :goto_2

    :cond_2
    move v3, v4

    :goto_2
    iput-boolean v3, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mMikeymodeEnabled:Z

    .line 2446
    const-string v2, "camera_key_preferred_action_type"

    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v3

    invoke-static {v0, v2, v4, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    .line 2449
    .local v2, "cameraKeyActionType":I
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    if-ne v5, v2, :cond_3

    const-string v6, "camera_key_preferred_action_shortcut_id"

    invoke-static {v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v7

    .line 2450
    const/4 v8, -0x1

    invoke-static {v0, v6, v8, v7}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    const/4 v7, 0x4

    if-ne v7, v6, :cond_3

    move v6, v5

    goto :goto_3

    :cond_3
    move v6, v4

    :goto_3
    iput-boolean v6, v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mCameraKeyWakeScreen:Z

    .line 2452
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const-string v6, "auto_test_mode_on"

    invoke-static {v0, v6, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_4

    move v6, v5

    goto :goto_4

    :cond_4
    move v6, v4

    :goto_4
    iput-boolean v6, v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTestModeEnabled:Z

    .line 2454
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const-string/jumbo v6, "send_back_when_xiaoai_appear"

    invoke-static {v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v7

    invoke-static {v0, v6, v4, v7}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-eqz v6, :cond_5

    move v6, v5

    goto :goto_5

    :cond_5
    move v6, v4

    :goto_5
    invoke-static {v3, v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmVoiceAssistEnabled(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V

    .line 2457
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const-string v6, "key_bank_card_in_ese"

    invoke-static {v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v7

    invoke-static {v0, v6, v4, v7}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-lez v6, :cond_6

    move v6, v5

    goto :goto_6

    :cond_6
    move v6, v4

    :goto_6
    iput-boolean v6, v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveBankCard:Z

    .line 2459
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const-string v6, "key_trans_card_in_ese"

    invoke-static {v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v7

    invoke-static {v0, v6, v4, v7}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-lez v6, :cond_7

    move v6, v5

    goto :goto_7

    :cond_7
    move v6, v4

    :goto_7
    iput-boolean v6, v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mHaveTranksCard:Z

    .line 2461
    const-string v3, "key_long_press_volume_down"

    iget-object v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v6

    invoke-static {v0, v3, v6}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 2463
    .local v3, "action":Ljava/lang/String;
    if-eqz v3, :cond_b

    .line 2464
    const-string v6, "Street-snap"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    const-string v6, "Street-snap-picture"

    .line 2465
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    const-string v6, "Street-snap-movie"

    .line 2466
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    goto :goto_8

    .line 2468
    :cond_8
    const-string v6, "public_transportation_shortcuts"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2469
    iget-object v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const/4 v7, 0x2

    invoke-static {v6, v7}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmLongPressVolumeDownBehavior(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;I)V

    goto :goto_9

    .line 2471
    :cond_9
    iget-object v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v6, v4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmLongPressVolumeDownBehavior(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;I)V

    goto :goto_9

    .line 2467
    :cond_a
    :goto_8
    iget-object v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v6, v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmLongPressVolumeDownBehavior(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;I)V

    goto :goto_9

    .line 2474
    :cond_b
    const-string v6, "key_long_press_volume_down"

    const-string v7, "none"

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v8

    invoke-static {v0, v6, v7, v8}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 2477
    :goto_9
    iget-object v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const-string/jumbo v7, "vr_mode"

    invoke-static {v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v8

    invoke-static {v0, v7, v4, v8}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v7

    if-ne v7, v5, :cond_c

    move v7, v5

    goto :goto_a

    :cond_c
    move v7, v4

    :goto_a
    invoke-static {v6, v7}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmIsVRMode(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V

    .line 2479
    iget-object v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmAccessibilityShortcutSetting(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lmiui/provider/SettingsStringUtil$SettingStringHelper;

    move-result-object v6

    if-nez v6, :cond_d

    .line 2480
    iget-object v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    new-instance v7, Lmiui/provider/SettingsStringUtil$SettingStringHelper;

    iget-object v8, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v8, v8, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "enabled_accessibility_services"

    iget-object v10, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v10}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v10

    invoke-direct {v7, v8, v9, v10}, Lmiui/provider/SettingsStringUtil$SettingStringHelper;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;I)V

    invoke-static {v6, v7}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmAccessibilityShortcutSetting(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Lmiui/provider/SettingsStringUtil$SettingStringHelper;)V

    .line 2483
    :cond_d
    iget-object v6, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmAccessibilityShortcutSetting(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lmiui/provider/SettingsStringUtil$SettingStringHelper;

    move-result-object v6

    invoke-virtual {v6}, Lmiui/provider/SettingsStringUtil$SettingStringHelper;->read()Ljava/lang/String;

    move-result-object v6

    .line 2484
    .local v6, "shortcutService":Ljava/lang/String;
    iget-object v7, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v7, v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$mhasTalkbackService(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, v7, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mTalkBackIsOpened:Z

    .line 2486
    const/4 v7, 0x0

    .line 2488
    .local v7, "accessibilityShortcutEnabled":Z
    const-string v8, "accessibility_shortcut_target_service"

    iget-object v9, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v9}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v9

    invoke-static {v0, v8, v9}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 2490
    .local v8, "accessibilityShortcut":Ljava/lang/String;
    if-nez v8, :cond_e

    .line 2491
    iget-object v9, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v9, v9, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    const v10, 0x1040256

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object v8, v9

    .line 2494
    :cond_e
    iget-object v9, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    if-eqz v7, :cond_f

    invoke-static {v9, v8}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$misTalkBackService(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_f

    move v10, v5

    goto :goto_b

    :cond_f
    move v10, v4

    :goto_b
    iput-boolean v10, v9, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mShortcutServiceIsTalkBack:Z

    .line 2497
    iget-object v9, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const-string v10, "accessibility_shortcut_on_lock_screen"

    invoke-static {v9}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v11

    invoke-static {v0, v10, v4, v11}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v10

    if-ne v10, v5, :cond_10

    goto :goto_c

    :cond_10
    move v5, v4

    :goto_c
    iput-boolean v5, v9, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mAccessibilityShortcutOnLockScreen:Z

    .line 2499
    .end local v2    # "cameraKeyActionType":I
    .end local v3    # "action":Ljava/lang/String;
    .end local v6    # "shortcutService":Ljava/lang/String;
    .end local v7    # "accessibilityShortcutEnabled":Z
    .end local v8    # "accessibilityShortcut":Ljava/lang/String;
    monitor-exit v1

    .line 2500
    return-void

    .line 2499
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 6
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 2380
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v0, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2381
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "gb_boosting"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2382
    sget-boolean v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$misGameMode(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2383
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmEdgeSuppressionManager(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    move-result-object v1

    const-string v2, "gameBooster"

    invoke-virtual {v1, v2}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->handleEdgeModeChange(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2386
    :cond_0
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$mhandleTouchFeatureRotationWatcher(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V

    goto/16 :goto_2

    .line 2388
    :cond_1
    const-string v1, "imperceptible_press_power_key"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2389
    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2390
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v3, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v3, v1}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmImperceptiblePowerKey(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2392
    :cond_2
    const-string v1, "long_press_timeout"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    .line 2393
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v4

    invoke-static {v0, v1, v3, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iput v1, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mKeyLongPressTimeout:I

    goto/16 :goto_2

    .line 2395
    :cond_3
    const-string v1, "kid_mode_status"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string v4, "kid_user_id"

    const/4 v5, 0x1

    if-nez v2, :cond_a

    invoke-static {v4}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2396
    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    goto/16 :goto_0

    .line 2409
    :cond_4
    const-string v1, "is_custom_shortcut_effective"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2410
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v2, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, -0x2

    invoke-static {v2, v1, v5, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-ne v1, v5, :cond_5

    move v3, v5

    :cond_5
    move v1, v3

    .line 2412
    .local v1, "enable":Z
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v2, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getInstance(Landroid/content/Context;)Lcom/android/server/policy/MiuiKeyInterceptExtend;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setKeyboardShortcutEnable(Z)V

    .line 2413
    .end local v1    # "enable":Z
    goto/16 :goto_2

    :cond_6
    const-string/jumbo v1, "synergy_mode"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2414
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget-object v4, v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v1, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v5, :cond_7

    move v3, v5

    :cond_7
    invoke-static {v2, v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmIsSynergyMode(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V

    .line 2416
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmIsSynergyMode(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmProximitySensor(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/policy/MiuiScreenOnProximityLock;

    move-result-object v1

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmProximitySensor(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/policy/MiuiScreenOnProximityLock;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 2417
    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v1, v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$mreleaseScreenOnProximitySensor(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V

    goto :goto_2

    .line 2419
    :cond_8
    const-string v1, "is_mi_input_event_time_line_enable"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2420
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 2428
    :cond_9
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    goto :goto_2

    .line 2397
    :cond_a
    :goto_0
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v5, :cond_b

    goto :goto_1

    :cond_b
    move v5, v3

    :goto_1
    move v1, v5

    .line 2399
    .local v1, "isKidMode":Z
    if-nez v1, :cond_c

    .line 2400
    const/16 v2, -0x2710

    invoke-static {v0, v4, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    .line 2404
    .local v2, "kidSpaceId":I
    iget-object v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)I

    move-result v3

    if-ne v2, v3, :cond_c

    .line 2405
    const/4 v1, 0x1

    .line 2408
    .end local v2    # "kidSpaceId":I
    :cond_c
    iget-object v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$MiuiSettingsObserver;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v2, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$mnotifyKidSpaceChanged(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Z)V

    .line 2409
    .end local v1    # "isKidMode":Z
    nop

    .line 2430
    :cond_d
    :goto_2
    return-void
.end method
