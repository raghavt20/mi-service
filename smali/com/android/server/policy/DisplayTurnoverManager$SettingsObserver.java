class com.android.server.policy.DisplayTurnoverManager$SettingsObserver extends android.database.ContentObserver {
	 /* .source "DisplayTurnoverManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/policy/DisplayTurnoverManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "SettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.policy.DisplayTurnoverManager this$0; //synthetic
/* # direct methods */
public com.android.server.policy.DisplayTurnoverManager$SettingsObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 264 */
this.this$0 = p1;
/* .line 265 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 266 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .line 270 */
/* invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V */
/* .line 271 */
v0 = this.this$0;
com.android.server.policy.DisplayTurnoverManager .-$$Nest$mupdateSubSwitchMode ( v0 );
/* .line 272 */
return;
} // .end method
