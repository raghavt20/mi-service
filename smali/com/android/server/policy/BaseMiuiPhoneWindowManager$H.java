class com.android.server.policy.BaseMiuiPhoneWindowManager$H extends android.os.Handler {
	 /* .source "BaseMiuiPhoneWindowManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # static fields */
static final Integer MSG_DISPATCH_SHOW_RECENTS;
static final Integer MSG_KEY_DELAY_POWER;
static final Integer MSG_KEY_FUNCTION;
/* # instance fields */
final com.android.server.policy.BaseMiuiPhoneWindowManager this$0; //synthetic
/* # direct methods */
private com.android.server.policy.BaseMiuiPhoneWindowManager$H ( ) {
/* .locals 0 */
/* .line 1863 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/Handler;-><init>()V */
return;
} // .end method
 com.android.server.policy.BaseMiuiPhoneWindowManager$H ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$H;-><init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 12 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1870 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* const-string/jumbo v1, "shortcut" */
(( android.os.Bundle ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
final String v2 = "long_press_power_key_four_second"; // const-string v2, "long_press_power_key_four_second"
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 1871 */
	 /* const-string/jumbo v0, "trigger dump power log" */
	 com.android.server.policy.MiuiInputLog .defaults ( v0 );
	 /* .line 1872 */
	 v0 = this.this$0;
	 com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmStabilityLocalServiceInternal ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 1873 */
		 v0 = this.this$0;
		 com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmStabilityLocalServiceInternal ( v0 );
		 /* .line 1875 */
	 } // :cond_0
	 return;
	 /* .line 1878 */
} // :cond_1
v0 = this.this$0;
v0 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmIsKidMode ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 1879 */
	 final String v0 = "Children\'s space does not trigger shortcut gestures"; // const-string v0, "Children\'s space does not trigger shortcut gestures"
	 com.android.server.policy.MiuiInputLog .defaults ( v0 );
	 /* .line 1880 */
	 return;
	 /* .line 1883 */
} // :cond_2
/* iget v0, p1, Landroid/os/Message;->what:I */
int v2 = 2; // const/4 v2, 0x2
/* if-ne v0, v2, :cond_3 */
/* .line 1884 */
v0 = this.this$0;
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
final String v2 = "power_up_event"; // const-string v2, "power_up_event"
(( android.os.Bundle ) v1 ).getParcelable ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
/* check-cast v1, Landroid/view/KeyEvent; */
/* .line 1885 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
final String v3 = "power_up_policy"; // const-string v3, "power_up_policy"
v2 = (( android.os.Bundle ) v2 ).getInt ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 1886 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
final String v4 = "power_up_screen_state"; // const-string v4, "power_up_screen_state"
v3 = (( android.os.Bundle ) v3 ).getBoolean ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
/* .line 1884 */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) v0 ).callSuperInterceptKeyBeforeQueueing ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->callSuperInterceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I
/* goto/16 :goto_4 */
/* .line 1888 */
} // :cond_3
/* iget v0, p1, Landroid/os/Message;->what:I */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_16 */
/* .line 1890 */
v0 = this.this$0;
v0 = this.mPowerManager;
v0 = (( android.os.PowerManager ) v0 ).isScreenOn ( ); // invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z
/* if-nez v0, :cond_4 */
return;
/* .line 1892 */
} // :cond_4
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
(( android.os.Bundle ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 1893 */
/* .local v0, "shortcut":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1894 */
/* .local v1, "triggered":Z */
/* const-string/jumbo v3, "virtual_key_longpress" */
/* .line 1895 */
/* .local v3, "effectKey":Ljava/lang/String; */
v4 = this.obj;
/* check-cast v4, Ljava/lang/String; */
/* .line 1896 */
/* .local v4, "action":Ljava/lang/String; */
/* if-nez v4, :cond_5 */
/* .line 1897 */
return;
/* .line 1899 */
} // :cond_5
v5 = this.this$0;
v5 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v5 ).isUserSetupComplete ( ); // invoke-virtual {v5}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isUserSetupComplete()Z
final String v6 = "close_talkback"; // const-string v6, "close_talkback"
final String v7 = "find_device_locate"; // const-string v7, "find_device_locate"
final String v8 = "dump_log"; // const-string v8, "dump_log"
/* if-nez v5, :cond_6 */
v5 = (( java.lang.String ) v8 ).equals ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_6 */
/* .line 1900 */
v5 = (( java.lang.String ) v7 ).equals ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_6 */
/* .line 1901 */
v5 = (( java.lang.String ) v6 ).equals ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_6 */
/* .line 1902 */
/* const-string/jumbo v2, "user setup not complete, does not trigger shortcut" */
com.android.server.policy.MiuiInputLog .defaults ( v2 );
/* .line 1903 */
return;
/* .line 1905 */
} // :cond_6
final String v5 = "launch_camera"; // const-string v5, "launch_camera"
v9 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v10 = 0; // const/4 v10, 0x0
if ( v9 != null) { // if-eqz v9, :cond_7
/* .line 1906 */
v6 = this.this$0;
v6 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v6 );
v1 = (( com.miui.server.input.util.ShortCutActionsUtils ) v6 ).triggerFunction ( v5, v0, v10, v2 ); // invoke-virtual {v6, v5, v0, v10, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* goto/16 :goto_2 */
/* .line 1908 */
} // :cond_7
/* const-string/jumbo v5, "screen_shot" */
v9 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v11 = 0; // const/4 v11, 0x0
if ( v9 != null) { // if-eqz v9, :cond_8
/* .line 1909 */
v2 = this.this$0;
v2 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v2 );
v1 = (( com.miui.server.input.util.ShortCutActionsUtils ) v2 ).triggerFunction ( v5, v0, v10, v11 ); // invoke-virtual {v2, v5, v0, v10, v11}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 1911 */
v2 = this.this$0;
v2 = this.mContext;
/* const-string/jumbo v5, "screenshot" */
final String v6 = "key_shortcut"; // const-string v6, "key_shortcut"
com.android.server.policy.BaseMiuiPhoneWindowManager .sendRecordCountEvent ( v2,v5,v6 );
/* goto/16 :goto_2 */
/* .line 1912 */
} // :cond_8
final String v5 = "partial_screen_shot"; // const-string v5, "partial_screen_shot"
v9 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_9
/* .line 1913 */
v2 = this.this$0;
v2 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v2 );
v1 = (( com.miui.server.input.util.ShortCutActionsUtils ) v2 ).triggerFunction ( v5, v0, v10, v11 ); // invoke-virtual {v2, v5, v0, v10, v11}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* goto/16 :goto_2 */
/* .line 1915 */
} // :cond_9
final String v5 = "go_to_sleep"; // const-string v5, "go_to_sleep"
v9 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_a
/* .line 1916 */
v6 = this.this$0;
v6 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v6 );
v1 = (( com.miui.server.input.util.ShortCutActionsUtils ) v6 ).triggerFunction ( v5, v0, v10, v2 ); // invoke-virtual {v6, v5, v0, v10, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* goto/16 :goto_2 */
/* .line 1918 */
} // :cond_a
/* const-string/jumbo v5, "turn_on_torch" */
v9 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_e
/* .line 1919 */
v6 = this.this$0;
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) v6 ).getTelecommService ( ); // invoke-virtual {v6}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getTelecommService()Landroid/telecom/TelecomManager;
/* .line 1920 */
/* .local v6, "telecomManager":Landroid/telecom/TelecomManager; */
v7 = this.this$0;
v7 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmWifiOnly ( v7 );
/* if-nez v7, :cond_c */
if ( v6 != null) { // if-eqz v6, :cond_b
/* .line 1921 */
v7 = (( android.telecom.TelecomManager ) v6 ).getCallState ( ); // invoke-virtual {v6}, Landroid/telecom/TelecomManager;->getCallState()I
/* if-nez v7, :cond_b */
} // :cond_b
} // :cond_c
} // :goto_0
/* move v11, v2 */
} // :goto_1
/* move v7, v11 */
/* .line 1922 */
/* .local v7, "phoneIdle":Z */
if ( v7 != null) { // if-eqz v7, :cond_d
/* .line 1923 */
v8 = this.this$0;
v8 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v8 );
v1 = (( com.miui.server.input.util.ShortCutActionsUtils ) v8 ).triggerFunction ( v5, v0, v10, v2 ); // invoke-virtual {v8, v5, v0, v10, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 1926 */
} // .end local v6 # "telecomManager":Landroid/telecom/TelecomManager;
} // .end local v7 # "phoneIdle":Z
} // :cond_d
/* goto/16 :goto_2 */
} // :cond_e
final String v5 = "close_app"; // const-string v5, "close_app"
v9 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_f
/* .line 1927 */
v2 = this.this$0;
v2 = this.mMiuiKeyShortcutRuleManager;
/* .line 1928 */
final String v6 = "long_press_back_key"; // const-string v6, "long_press_back_key"
(( com.android.server.policy.MiuiKeyShortcutRuleManager ) v2 ).getFunction ( v6 ); // invoke-virtual {v2, v6}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 1927 */
v2 = (( java.lang.String ) v5 ).equals ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 1930 */
/* .local v2, "isTriggeredByBack":Z */
v6 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmShortcutOneTrackHelper ( v6 );
(( com.android.server.input.shortcut.ShortcutOneTrackHelper ) v6 ).trackShortcutEventTrigger ( v0, v5 ); // invoke-virtual {v6, v0, v5}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1932 */
v5 = this.this$0;
v1 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v5 ).triggerCloseApp ( v4 ); // invoke-virtual {v5, v4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->triggerCloseApp(Ljava/lang/String;)Z
/* .line 1934 */
} // .end local v2 # "isTriggeredByBack":Z
/* goto/16 :goto_2 */
} // :cond_f
/* const-string/jumbo v5, "show_menu" */
v9 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_10
/* .line 1935 */
v2 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmShortcutOneTrackHelper ( v2 );
(( com.android.server.input.shortcut.ShortcutOneTrackHelper ) v2 ).trackShortcutEventTrigger ( v0, v5 ); // invoke-virtual {v2, v0, v5}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1937 */
v2 = this.this$0;
v1 = com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$mshowMenu ( v2 );
/* goto/16 :goto_2 */
/* .line 1938 */
} // :cond_10
v5 = (( java.lang.String ) v8 ).equals ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_11
/* .line 1939 */
v5 = this.this$0;
v5 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v5 );
v1 = (( com.miui.server.input.util.ShortCutActionsUtils ) v5 ).triggerFunction ( v8, v0, v10, v2 ); // invoke-virtual {v5, v8, v0, v10, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* goto/16 :goto_2 */
/* .line 1941 */
} // :cond_11
final String v5 = "launch_recents"; // const-string v5, "launch_recents"
v8 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_12
/* .line 1942 */
v2 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmShortcutOneTrackHelper ( v2 );
(( com.android.server.input.shortcut.ShortcutOneTrackHelper ) v2 ).trackShortcutEventTrigger ( v0, v5 ); // invoke-virtual {v2, v0, v5}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1944 */
v2 = this.this$0;
v1 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v2 ).triggerLaunchRecent ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->triggerLaunchRecent(Ljava/lang/String;)Z
/* .line 1945 */
v2 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmHapticFeedbackUtil ( v2 );
(( miui.util.HapticFeedbackUtil ) v2 ).performHapticFeedback ( v3, v11 ); // invoke-virtual {v2, v3, v11}, Lmiui/util/HapticFeedbackUtil;->performHapticFeedback(Ljava/lang/String;Z)Z
/* .line 1946 */
} // :cond_12
v5 = (( java.lang.String ) v6 ).equals ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_13
/* .line 1947 */
v2 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fgetmShortcutOneTrackHelper ( v2 );
(( com.android.server.input.shortcut.ShortcutOneTrackHelper ) v2 ).trackShortcutEventTrigger ( v0, v6 ); // invoke-virtual {v2, v0, v6}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1949 */
v2 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$mcloseTalkBack ( v2 );
/* .line 1950 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1951 */
} // :cond_13
v5 = (( java.lang.String ) v7 ).equals ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_14
/* .line 1952 */
v2 = this.this$0;
v2 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v2 );
v1 = (( com.miui.server.input.util.ShortCutActionsUtils ) v2 ).triggerFunction ( v7, v0, v10, v11 ); // invoke-virtual {v2, v7, v0, v10, v11}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 1954 */
} // :cond_14
final String v5 = "key_long_press_volume_up"; // const-string v5, "key_long_press_volume_up"
v5 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_15
/* .line 1955 */
/* new-instance v5, Landroid/content/Intent; */
final String v6 = "android.intent.action.FACTORY_RESET"; // const-string v6, "android.intent.action.FACTORY_RESET"
/* invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1956 */
/* .local v5, "intent":Landroid/content/Intent; */
final String v6 = "android"; // const-string v6, "android"
(( android.content.Intent ) v5 ).setPackage ( v6 ); // invoke-virtual {v5, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1957 */
/* const/high16 v6, 0x10000000 */
(( android.content.Intent ) v5 ).addFlags ( v6 ); // invoke-virtual {v5, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1958 */
final String v6 = "android.intent.extra.WIPE_EXTERNAL_STORAGE"; // const-string v6, "android.intent.extra.WIPE_EXTERNAL_STORAGE"
(( android.content.Intent ) v5 ).putExtra ( v6, v2 ); // invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1959 */
final String v6 = "com.android.internal.intent.extra.WIPE_ESIMS"; // const-string v6, "com.android.internal.intent.extra.WIPE_ESIMS"
(( android.content.Intent ) v5 ).putExtra ( v6, v2 ); // invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1960 */
v2 = this.this$0;
v2 = this.mContext;
(( android.content.Context ) v2 ).sendBroadcast ( v5 ); // invoke-virtual {v2, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 1962 */
} // .end local v5 # "intent":Landroid/content/Intent;
} // :cond_15
} // :goto_2
if ( v1 != null) { // if-eqz v1, :cond_17
/* .line 1963 */
v2 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$mmarkShortcutTriggered ( v2 );
/* .line 1965 */
} // .end local v0 # "shortcut":Ljava/lang/String;
} // .end local v1 # "triggered":Z
} // .end local v3 # "effectKey":Ljava/lang/String;
} // .end local v4 # "action":Ljava/lang/String;
} // :cond_16
/* iget v0, p1, Landroid/os/Message;->what:I */
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_17 */
/* .line 1966 */
v0 = this.obj;
/* check-cast v0, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 1967 */
/* .local v0, "triggeredFromAltTab":Z */
v1 = this.this$0;
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$mshowRecentApps ( v1,v0 );
/* .line 1965 */
} // .end local v0 # "triggeredFromAltTab":Z
} // :cond_17
} // :goto_3
/* nop */
/* .line 1969 */
} // :goto_4
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 1970 */
return;
} // .end method
