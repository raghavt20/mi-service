.class public Lcom/android/server/policy/KeyboardCombinationRule;
.super Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;
.source "KeyboardCombinationRule.java"


# static fields
.field static final CLASS_NAME:Ljava/lang/String; = "className"

.field static final PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field static final TAG:Ljava/lang/String; = "KeyboardCombinationRule"


# instance fields
.field public mContext:Landroid/content/Context;

.field private mFunction:Ljava/lang/String;

.field public mHandler:Landroid/os/Handler;

.field public mKeyboardShortcutInfo:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

.field private mMetaState:I

.field private final mPrimaryKey:I


# direct methods
.method public static synthetic $r8$lambda$mrIOXlkYZFfpQtAWIMvVvAtw_SM(Lcom/android/server/policy/KeyboardCombinationRule;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/KeyboardCombinationRule;->lambda$execute$0()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "info"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;-><init>(II)V

    .line 30
    iput v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    .line 36
    iput-object p1, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mContext:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mHandler:Landroid/os/Handler;

    .line 38
    iput-object p3, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mKeyboardShortcutInfo:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 39
    invoke-virtual {p0}, Lcom/android/server/policy/KeyboardCombinationRule;->getPrimaryKey()I

    move-result v0

    iput v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mPrimaryKey:I

    .line 40
    invoke-direct {p0}, Lcom/android/server/policy/KeyboardCombinationRule;->setFunction()V

    .line 41
    return-void
.end method

.method private synthetic lambda$execute$0()V
    .locals 0

    .line 54
    invoke-virtual {p0}, Lcom/android/server/policy/KeyboardCombinationRule;->triggerFunction()V

    .line 55
    return-void
.end method

.method private setFunction()V
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mKeyboardShortcutInfo:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 154
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    goto :goto_0

    .line 148
    :pswitch_0
    const-string v0, "launch_split_screen_to_right"

    iput-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    .line 149
    goto :goto_0

    .line 145
    :pswitch_1
    const-string v0, "launch_split_screen_to_left"

    iput-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    .line 146
    goto :goto_0

    .line 142
    :pswitch_2
    const-string v0, "launch_app_full_window"

    iput-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    .line 143
    goto :goto_0

    .line 139
    :pswitch_3
    const-string v0, "launch_app_small_window"

    iput-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    .line 140
    goto :goto_0

    .line 136
    :pswitch_4
    const-string v0, "close_app"

    iput-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    .line 137
    goto :goto_0

    .line 133
    :pswitch_5
    const-string v0, "partial_screen_shot"

    iput-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    .line 134
    goto :goto_0

    .line 130
    :pswitch_6
    const-string/jumbo v0, "screen_shot"

    iput-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    .line 131
    goto :goto_0

    .line 127
    :pswitch_7
    const-string v0, "go_to_sleep"

    iput-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    .line 128
    goto :goto_0

    .line 124
    :pswitch_8
    const-string v0, "launch_home"

    iput-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    .line 125
    goto :goto_0

    .line 121
    :pswitch_9
    const-string v0, "launch_recents"

    iput-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    .line 122
    goto :goto_0

    .line 118
    :pswitch_a
    const-string v0, "launch_notification_center"

    iput-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    .line 119
    goto :goto_0

    .line 115
    :pswitch_b
    const-string v0, "launch_control_center"

    iput-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    .line 116
    goto :goto_0

    .line 151
    :pswitch_c
    const-string v0, "launch_app"

    iput-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    .line 152
    nop

    .line 157
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method cancel()V
    .locals 0

    .line 60
    return-void
.end method

.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 0

    .line 22
    invoke-super {p0, p1}, Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public execute()V
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/policy/KeyboardCombinationRule$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/policy/KeyboardCombinationRule$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/policy/KeyboardCombinationRule;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 56
    return-void
.end method

.method public getMetaState()I
    .locals 1

    .line 108
    iget v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    return v0
.end method

.method public getPrimaryKey()I
    .locals 6

    .line 81
    iget-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mKeyboardShortcutInfo:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v0

    .line 82
    .local v0, "keyCode":J
    const-wide/high16 v2, 0x1000000000000L

    and-long/2addr v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 83
    const-wide v2, -0x1000000000001L

    and-long/2addr v0, v2

    .line 84
    iget v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    const/high16 v3, 0x10000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    .line 86
    :cond_0
    const-wide v2, 0x200000000L

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 87
    const-wide v2, -0x200000001L

    and-long/2addr v0, v2

    .line 88
    const-wide v2, 0x2000000000L

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 89
    const-wide v2, -0x2000000001L

    and-long/2addr v0, v2

    .line 90
    iget v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    or-int/lit8 v2, v2, 0x22

    iput v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    goto :goto_0

    .line 91
    :cond_1
    const-wide v2, 0x1000000000L

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 92
    const-wide v2, -0x1000000001L

    and-long/2addr v0, v2

    .line 93
    iget v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    or-int/lit8 v2, v2, 0x12

    iput v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    .line 96
    :cond_2
    :goto_0
    const-wide v2, 0x100000000000L

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 97
    const-wide v2, -0x100000000001L

    and-long/2addr v0, v2

    .line 98
    iget v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    .line 100
    :cond_3
    const-wide v2, 0x100000000L

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 101
    const-wide v2, -0x100000001L

    and-long/2addr v0, v2

    .line 102
    iget v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    .line 104
    :cond_4
    long-to-int v2, v0

    return v2
.end method

.method public bridge synthetic hashCode()I
    .locals 1

    .line 22
    invoke-super {p0}, Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;->hashCode()I

    move-result v0

    return v0
.end method

.method public shouldInterceptKeys(II)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "metaState"    # I

    .line 44
    iget v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mPrimaryKey:I

    const/4 v1, 0x0

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 45
    .local v0, "isMatch":Z
    :goto_0
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mContext:Landroid/content/Context;

    invoke-static {v2, p2, p1}, Lcom/miui/server/input/custom/InputMiuiDesktopMode;->shouldInterceptKeyboardCombinationRule(Landroid/content/Context;II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 46
    return v1

    .line 48
    :cond_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KeyboardCombinationRule{mPrimaryKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mPrimaryKey:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mKeyboardShortcutInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mKeyboardShortcutInfo:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMetaState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mFunction=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public triggerFunction()V
    .locals 5

    .line 64
    iget-object v0, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "KeyboardCombinationRule"

    const-string v1, "function is null"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    return-void

    .line 68
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 69
    .local v0, "extras":Landroid/os/Bundle;
    const-string v1, "launch_app"

    iget-object v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    iget-object v1, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mKeyboardShortcutInfo:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    invoke-virtual {v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "packageName"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v1, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mKeyboardShortcutInfo:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    invoke-virtual {v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getClassName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "className"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_1
    iget-object v1, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mKeyboardShortcutInfo:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 74
    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil$KeyBoardShortcut;->getShortCutNameByType(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Ljava/lang/String;

    move-result-object v2

    .line 73
    invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardShortcut(Ljava/lang/String;)V

    .line 76
    iget-object v1, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mFunction:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mMetaState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/policy/KeyboardCombinationRule;->mPrimaryKey:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 78
    return-void
.end method
