.class public Lcom/android/server/policy/FindDevicePowerOffLocateManager;
.super Ljava/lang/Object;
.source "FindDevicePowerOffLocateManager.java"


# static fields
.field public static final GLOBAL_ACTIONS:Ljava/lang/String; = "global_actions"

.field public static final IMPERCEPTIBLE_POWER_PRESS:Ljava/lang/String; = "imperceptible_power_press"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static sendFindDeviceLocateBroadcast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cause"    # Ljava/lang/String;

    .line 13
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.FIND_DEVICE_LOCATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 14
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.xiaomi.finddevice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 15
    const-string v1, "cause"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 16
    sget-object v1, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 17
    return-void
.end method
