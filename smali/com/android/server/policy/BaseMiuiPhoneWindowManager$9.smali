.class Lcom/android/server/policy/BaseMiuiPhoneWindowManager$9;
.super Ljava/lang/Object;
.source "BaseMiuiPhoneWindowManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->registerProximitySensor()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    .line 1077
    iput-object p1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$9;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 1080
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$9;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmMiuiPocketModeManager(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1081
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$9;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    new-instance v1, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    iget-object v2, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fputmMiuiPocketModeManager(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;Lcom/android/server/input/pocketmode/MiuiPocketModeManager;)V

    .line 1083
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$9;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmMiuiPocketModeManager(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$9;->this$0:Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-static {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->-$$Nest$fgetmWakeUpKeySensorListener(Lcom/android/server/policy/BaseMiuiPhoneWindowManager;)Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->registerListener(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;)V

    .line 1084
    return-void
.end method
