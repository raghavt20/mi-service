.class public Lcom/android/server/policy/MiuiKeyShortcutRuleManager;
.super Ljava/lang/Object;
.source "MiuiKeyShortcutRuleManager.java"


# static fields
.field private static final ATTRIBUTE_ACTION:Ljava/lang/String; = "action"

.field private static final ATTRIBUTE_COMBINATION_KEY:Ljava/lang/String; = "combinationKey"

.field private static final ATTRIBUTE_FUNCTION:Ljava/lang/String; = "function"

.field private static final ATTRIBUTE_FUNCTIONS:Ljava/lang/String; = "functions"

.field private static final ATTRIBUTE_GLOBAL:Ljava/lang/String; = "global"

.field private static final ATTRIBUTE_PRIMARY_KEY:Ljava/lang/String; = "primaryKey"

.field private static final ATTRIBUTE_REGION:Ljava/lang/String; = "region"

.field public static final ATTRIBUTE_REGION_TYPE_ALL:I = 0x2

.field public static final ATTRIBUTE_REGION_TYPE_CN:I = 0x0

.field public static final ATTRIBUTE_REGION_TYPE_GLOBAL:I = 0x1

.field private static final ATTRIBUTE_TYPE:Ljava/lang/String; = "type"

.field private static final MIUI_SETTINGS_PACKAGE:Ljava/lang/String; = "com.android.settings"

.field private static final SHORTCUT_CONFIG_NAME:Ljava/lang/String; = "miui_shortcut_config.json"

.field private static final SHORTCUT_CONFIG_PATH_DEFAULT:Ljava/lang/String; = "system_ext/etc/input/"

.field private static final SHORTCUT_CONFIG_PATH_EXTRA:Ljava/lang/String; = "product/etc/input/"

.field private static final SHORTCUT_TYPE_COMBINATION_KEY_RULE:Ljava/lang/String; = "combinationKeyRules"

.field private static final SHORTCUT_TYPE_GESTURE_RULE:Ljava/lang/String; = "gestureRules"

.field private static final SHORTCUT_TYPE_SINGLE_KEY_RULE:Ljava/lang/String; = "singleKeyRules"

.field private static final SINGLE_KEY_RULE_ATTRIBUTE_LONG_PRESS_TIME_OUT:Ljava/lang/String; = "longPressTimeOut"

.field private static final SINGLE_KEY_RULE_ATTRIBUTE_MAX_COUNT:Ljava/lang/String; = "maxCount"

.field private static final SINGLE_KEY_RULE_TYPE_MULTI_PRESS:Ljava/lang/String; = "multiPress"

.field private static final SINGLE_KEY_RULE_TYPE_SINGLE_PRESS:Ljava/lang/String; = "singlePress"

.field private static final TAG:Ljava/lang/String; = "MiuiKeyShortcutRuleManager"

.field private static sMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private final mHandler:Landroid/os/Handler;

.field private final mInitForUsers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mKeyCombinationManager:Lcom/android/server/policy/KeyCombinationManager;

.field private final mMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

.field private final mMiuiGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

.field private final mMiuiShortcutOperatorCustomManager:Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;

.field private final mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

.field private final mMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

.field private final mSingleKeyGestureDetector:Lcom/android/server/policy/SingleKeyGestureDetector;


# direct methods
.method static bridge synthetic -$$Nest$mresetShortcutSettings(Lcom/android/server/policy/MiuiKeyShortcutRuleManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->resetShortcutSettings()V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/android/server/policy/SingleKeyGestureDetector;Lcom/android/server/policy/KeyCombinationManager;Lcom/android/server/policy/MiuiShortcutTriggerHelper;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "singleKeyGestureDetector"    # Lcom/android/server/policy/SingleKeyGestureDetector;
    .param p3, "combinationManager"    # Lcom/android/server/policy/KeyCombinationManager;
    .param p4, "miuiShortcutTriggerHelper"    # Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p1, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mContext:Landroid/content/Context;

    .line 110
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 111
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mHandler:Landroid/os/Handler;

    .line 113
    const-class v1, Lcom/android/server/pm/UserManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/pm/UserManagerInternal;

    invoke-virtual {v1}, Lcom/android/server/pm/UserManagerInternal;->getUserIds()[I

    move-result-object v1

    .line 114
    .local v1, "userIds":[I
    new-instance v2, Ljava/util/HashSet;

    invoke-static {v1}, Ljava/util/Arrays;->stream([I)Ljava/util/stream/IntStream;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/stream/IntStream;->boxed()Ljava/util/stream/Stream;

    move-result-object v3

    new-instance v4, Lcom/android/server/policy/MiuiKeyShortcutRuleManager$$ExternalSyntheticLambda0;

    invoke-direct {v4}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager$$ExternalSyntheticLambda0;-><init>()V

    invoke-interface {v3, v4}, Ljava/util/stream/Stream;->toArray(Ljava/util/function/IntFunction;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mInitForUsers:Ljava/util/Set;

    .line 116
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    iput v2, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mCurrentUserId:I

    .line 117
    iput-object p2, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mSingleKeyGestureDetector:Lcom/android/server/policy/SingleKeyGestureDetector;

    .line 118
    iput-object p3, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mKeyCombinationManager:Lcom/android/server/policy/KeyCombinationManager;

    .line 120
    const-class v2, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    invoke-static {v2, p0}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 122
    iput-object p4, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 123
    invoke-static {p1, v0, p4, p2}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->getInstance(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/policy/MiuiShortcutTriggerHelper;Lcom/android/server/policy/SingleKeyGestureDetector;)Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    .line 125
    invoke-static {p1, v0, p3}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->getInstance(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/policy/KeyCombinationManager;)Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    .line 127
    invoke-static {p1, v0}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->getInstance(Landroid/content/Context;Landroid/os/Handler;)Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    .line 128
    invoke-static {p1}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->getInstance(Landroid/content/Context;)Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiShortcutOperatorCustomManager:Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;

    .line 131
    invoke-direct {p0}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->initMiuiKeyShortcut()V

    .line 133
    invoke-virtual {v0}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->initShortcut()V

    .line 135
    invoke-static {p1}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->setUploadShortcutAlarm(Z)V

    .line 137
    invoke-direct {p0}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->registerPackageChangeReceivers()V

    .line 138
    return-void
.end method

.method private getFunctionJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 5
    .param p1, "jsonObject"    # Lorg/json/JSONObject;

    .line 386
    const/4 v0, 0x0

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    .line 389
    :cond_0
    const-string v1, "functions"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 391
    :try_start_0
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 392
    .local v1, "functionsArray":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 393
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 394
    .local v3, "functionJSONObject":Lorg/json/JSONObject;
    invoke-direct {p0, v3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->isLegaDataInCurrentRule(Lorg/json/JSONObject;)Z

    move-result v4
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v4, :cond_1

    .line 395
    return-object v3

    .line 392
    .end local v3    # "functionJSONObject":Lorg/json/JSONObject;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 400
    .end local v1    # "functionsArray":Lorg/json/JSONArray;
    .end local v2    # "i":I
    :cond_2
    goto :goto_1

    .line 398
    :catch_0
    move-exception v1

    .line 399
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 402
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_3
    :goto_1
    return-object v0

    .line 387
    :cond_4
    :goto_2
    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;Lcom/android/server/policy/SingleKeyGestureDetector;Lcom/android/server/policy/KeyCombinationManager;Lcom/android/server/policy/MiuiShortcutTriggerHelper;)Lcom/android/server/policy/MiuiKeyShortcutRuleManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "singleKeyGestureDetector"    # Lcom/android/server/policy/SingleKeyGestureDetector;
    .param p2, "keyCombinationManager"    # Lcom/android/server/policy/KeyCombinationManager;
    .param p3, "miuiShortcutTriggerHelper"    # Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    const-class v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    monitor-enter v0

    .line 159
    :try_start_0
    sget-object v1, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->sMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    if-nez v1, :cond_0

    .line 160
    new-instance v1, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;-><init>(Landroid/content/Context;Lcom/android/server/policy/SingleKeyGestureDetector;Lcom/android/server/policy/KeyCombinationManager;Lcom/android/server/policy/MiuiShortcutTriggerHelper;)V

    sput-object v1, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->sMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    .line 163
    :cond_0
    sget-object v1, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->sMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 158
    .end local p0    # "context":Landroid/content/Context;
    .end local p1    # "singleKeyGestureDetector":Lcom/android/server/policy/SingleKeyGestureDetector;
    .end local p2    # "keyCombinationManager":Lcom/android/server/policy/KeyCombinationManager;
    .end local p3    # "miuiShortcutTriggerHelper":Lcom/android/server/policy/MiuiShortcutTriggerHelper;
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I
    .locals 5
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "attribute"    # Ljava/lang/String;

    .line 453
    const/4 v0, -0x1

    .line 455
    .local v0, "invalidAttributeValue":I
    const-string v1, "MiuiKeyShortcutRuleManager"

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto/16 :goto_5

    .line 460
    :cond_0
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    sparse-switch v2, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v2, "maxCount"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    goto :goto_1

    :sswitch_1
    const-string v2, "combinationKey"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    goto :goto_1

    :sswitch_2
    const-string v2, "global"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    goto :goto_1

    :sswitch_3
    const-string v2, "primaryKey"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v4

    goto :goto_1

    :goto_0
    const/4 v2, -0x1

    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 471
    goto :goto_4

    .line 469
    :pswitch_0
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    :cond_2
    return v3

    .line 466
    :pswitch_1
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    goto :goto_2

    .line 467
    :cond_3
    nop

    .line 466
    :goto_2
    return v4

    .line 463
    :pswitch_2
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_3

    .line 464
    :cond_4
    move v1, v0

    .line 463
    :goto_3
    return v1

    .line 471
    :goto_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "current attribute is invalid,attribute="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 472
    return v0

    .line 474
    :catch_0
    move-exception v2

    .line 475
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    .end local v2    # "e":Lorg/json/JSONException;
    return v0

    .line 456
    :cond_5
    :goto_5
    const-string v2, "get int attribute fail,jsonObject is null or attribute is null"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4bfdbf03 -> :sswitch_3
        -0x4a16fc5d -> :sswitch_2
        -0x933af0 -> :sswitch_1
        0x16c67a0b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getLongAttributeByFunctionJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)J
    .locals 2
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "attribute"    # Ljava/lang/String;

    .line 406
    invoke-direct {p0, p1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getFunctionJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getLongAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private getLongAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)J
    .locals 5
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "attribute"    # Ljava/lang/String;

    .line 430
    const-wide/16 v0, -0x1

    .line 432
    .local v0, "invalidAttributeValue":J
    const-string v2, "MiuiKeyShortcutRuleManager"

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_5

    .line 437
    :cond_0
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v3
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v4, "longPressTimeOut"

    packed-switch v3, :pswitch_data_0

    :cond_1
    goto :goto_0

    :pswitch_0
    :try_start_1
    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :goto_0
    const/4 v3, -0x1

    :goto_1
    packed-switch v3, :pswitch_data_1

    .line 444
    goto :goto_3

    .line 440
    :pswitch_1
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_2

    .line 442
    :cond_2
    move-wide v2, v0

    .line 440
    :goto_2
    return-wide v2

    .line 444
    :goto_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "current attribute is invalid,attribute="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 448
    goto :goto_4

    .line 446
    :catch_0
    move-exception v3

    .line 447
    .local v3, "e":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    .end local v3    # "e":Lorg/json/JSONException;
    :goto_4
    return-wide v0

    .line 433
    :cond_3
    :goto_5
    const-string v3, "get int attribute fail,jsonObject is null or attribute is null"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    return-wide v0

    nop

    :pswitch_data_0
    .packed-switch -0x6c2db7e6
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method private getMiuiSingleKeyInfo(Lorg/json/JSONObject;)Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;
    .locals 23
    .param p1, "singleKeyObject"    # Lorg/json/JSONObject;

    .line 316
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string v0, "multiPress"

    const-string/jumbo v3, "singlePress"

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 317
    .local v4, "actionAndDefaultFunctionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    move-object v12, v5

    .line 318
    .local v12, "actionMaxCountMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    move-object v13, v5

    .line 319
    .local v13, "actionMapForType":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 320
    .local v5, "primaryKey":I
    iget-object v6, v1, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v6}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getDefaultLongPressTimeOut()I

    move-result v6

    int-to-long v6, v6

    .line 322
    .local v6, "longPressTimeOut":J
    :try_start_0
    const-string v8, "primaryKey"

    invoke-direct {v1, v2, v8}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I

    move-result v8
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    move v5, v8

    .line 324
    :try_start_1
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    const-string/jumbo v9, "type"

    const-string v10, "maxCount"

    const-string v11, "function"

    const-string v14, "action"

    if-eqz v8, :cond_2

    .line 325
    :try_start_2
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 328
    .local v3, "singlePressArray":Lorg/json/JSONArray;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v15

    if-ge v8, v15, :cond_1

    .line 329
    invoke-virtual {v3, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v15

    .line 330
    .local v15, "singlePressJSONObject":Lorg/json/JSONObject;
    invoke-direct {v1, v15, v14}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v17, v16

    .line 333
    .local v17, "action":Ljava/lang/String;
    move-object/from16 v16, v3

    .end local v3    # "singlePressArray":Lorg/json/JSONArray;
    .local v16, "singlePressArray":Lorg/json/JSONArray;
    invoke-direct {v1, v15, v11}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeByFunctionJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    move/from16 v18, v5

    move-object/from16 v5, v17

    .end local v17    # "action":Ljava/lang/String;
    .local v5, "action":Ljava/lang/String;
    .local v18, "primaryKey":I
    :try_start_3
    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    const-string v3, "longPressTimeOut"

    .line 336
    invoke-direct {v1, v15, v3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getLongAttributeByFunctionJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v19

    .line 338
    .local v19, "configLongPressTimeOut":J
    const-wide/16 v21, 0x0

    cmp-long v3, v19, v21

    if-lez v3, :cond_0

    .line 339
    move-wide/from16 v21, v19

    goto :goto_1

    :cond_0
    move-wide/from16 v21, v6

    :goto_1
    move-wide/from16 v6, v21

    .line 340
    invoke-direct {v1, v15, v9}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v13, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    nop

    .line 343
    invoke-direct {v1, v15, v10}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 342
    invoke-interface {v12, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    nop

    .end local v5    # "action":Ljava/lang/String;
    .end local v15    # "singlePressJSONObject":Lorg/json/JSONObject;
    .end local v19    # "configLongPressTimeOut":J
    add-int/lit8 v8, v8, 0x1

    move-object/from16 v3, v16

    move/from16 v5, v18

    goto :goto_0

    .end local v16    # "singlePressArray":Lorg/json/JSONArray;
    .end local v18    # "primaryKey":I
    .restart local v3    # "singlePressArray":Lorg/json/JSONArray;
    .local v5, "primaryKey":I
    :cond_1
    move-object/from16 v16, v3

    move/from16 v18, v5

    .end local v3    # "singlePressArray":Lorg/json/JSONArray;
    .end local v5    # "primaryKey":I
    .restart local v16    # "singlePressArray":Lorg/json/JSONArray;
    .restart local v18    # "primaryKey":I
    goto :goto_2

    .line 324
    .end local v8    # "i":I
    .end local v16    # "singlePressArray":Lorg/json/JSONArray;
    .end local v18    # "primaryKey":I
    .restart local v5    # "primaryKey":I
    :cond_2
    move/from16 v18, v5

    .line 350
    .end local v5    # "primaryKey":I
    .restart local v18    # "primaryKey":I
    :goto_2
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 351
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 354
    .local v0, "multiPressArray":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_3
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v3, v5, :cond_3

    .line 355
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 356
    .local v5, "multiPressJSONObject":Lorg/json/JSONObject;
    invoke-virtual {v5, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 358
    .local v8, "action":Ljava/lang/String;
    nop

    .line 359
    invoke-direct {v1, v5, v11}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeByFunctionJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 358
    invoke-interface {v4, v8, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    nop

    .line 362
    invoke-direct {v1, v5, v10}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    .line 361
    invoke-interface {v12, v8, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    nop

    .line 365
    invoke-direct {v1, v5, v9}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 364
    invoke-interface {v13, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 354
    nop

    .end local v5    # "multiPressJSONObject":Lorg/json/JSONObject;
    .end local v8    # "action":Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 373
    .end local v0    # "multiPressArray":Lorg/json/JSONArray;
    .end local v3    # "i":I
    :cond_3
    move-wide v14, v6

    goto :goto_5

    .line 371
    :catch_0
    move-exception v0

    move/from16 v5, v18

    goto :goto_4

    .end local v18    # "primaryKey":I
    .local v5, "primaryKey":I
    :catch_1
    move-exception v0

    move/from16 v18, v5

    .end local v5    # "primaryKey":I
    .restart local v18    # "primaryKey":I
    goto :goto_4

    .end local v18    # "primaryKey":I
    .restart local v5    # "primaryKey":I
    :catch_2
    move-exception v0

    .line 372
    .local v0, "e":Lorg/json/JSONException;
    :goto_4
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    move/from16 v18, v5

    move-wide v14, v6

    .line 375
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v5    # "primaryKey":I
    .end local v6    # "longPressTimeOut":J
    .local v14, "longPressTimeOut":J
    .restart local v18    # "primaryKey":I
    :goto_5
    new-instance v0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;

    move-object v5, v0

    move/from16 v6, v18

    move-object v7, v4

    move-wide v8, v14

    move-object v10, v12

    move-object v11, v13

    invoke-direct/range {v5 .. v11}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;-><init>(ILjava/util/Map;JLjava/util/Map;Ljava/util/Map;)V

    return-object v0
.end method

.method private getShortcutConfig(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "filePtah"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;

    .line 501
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 502
    .local v0, "localFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 503
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 504
    .local v1, "inputStream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-direct {p0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->readFully(Ljava/io/FileInputStream;)Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 505
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 504
    return-object v2

    .line 503
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "localFile":Ljava/io/File;
    .end local p0    # "this":Lcom/android/server/policy/MiuiKeyShortcutRuleManager;
    .end local p1    # "filePtah":Ljava/lang/String;
    .end local p2    # "fileName":Ljava/lang/String;
    :goto_0
    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 505
    .end local v1    # "inputStream":Ljava/io/FileInputStream;
    .restart local v0    # "localFile":Ljava/io/File;
    .restart local p0    # "this":Lcom/android/server/policy/MiuiKeyShortcutRuleManager;
    .restart local p1    # "filePtah":Ljava/lang/String;
    .restart local p2    # "fileName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 506
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 509
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method private getShortcutJsonObjectMap(Ljava/util/Map;Ljava/util/List;)Ljava/util/Map;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lorg/json/JSONObject;",
            ">;>;>;"
        }
    .end annotation

    .line 214
    .local p1, "shortcutConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p2, "shortcutTypeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_6

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-eqz v0, :cond_6

    if-eqz p2, :cond_6

    .line 215
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 220
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 221
    .local v0, "shortcutJsonObjectMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lorg/json/JSONObject;>;>;>;"
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 222
    .local v2, "shortcutConfigMapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 223
    .local v3, "configType":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 224
    .local v4, "config":Ljava/lang/String;
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 225
    .local v5, "shortcutJsonObjectMapForType":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lorg/json/JSONObject;>;>;"
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 226
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_4

    .line 229
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 231
    .local v6, "jsonObject":Lorg/json/JSONObject;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 233
    .local v8, "shortcutType":Ljava/lang/String;
    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 234
    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 235
    .local v9, "jsonArray":Lorg/json/JSONArray;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 236
    .local v10, "jsonObjectList":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_1

    .line 237
    invoke-virtual {v9, v11}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v12

    invoke-interface {v10, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 239
    .end local v11    # "i":I
    :cond_1
    invoke-interface {v5, v8, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    .end local v8    # "shortcutType":Ljava/lang/String;
    .end local v9    # "jsonArray":Lorg/json/JSONArray;
    .end local v10    # "jsonObjectList":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
    :cond_2
    goto :goto_1

    .line 244
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    :cond_3
    goto :goto_3

    .line 242
    :catch_0
    move-exception v6

    .line 243
    .local v6, "e":Lorg/json/JSONException;
    invoke-virtual {v6}, Lorg/json/JSONException;->printStackTrace()V

    .line 246
    .end local v6    # "e":Lorg/json/JSONException;
    :cond_4
    :goto_3
    invoke-interface {v0, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    .end local v2    # "shortcutConfigMapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "configType":Ljava/lang/String;
    .end local v4    # "config":Ljava/lang/String;
    .end local v5    # "shortcutJsonObjectMapForType":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lorg/json/JSONObject;>;>;"
    goto :goto_0

    .line 248
    :cond_5
    return-object v0

    .line 216
    .end local v0    # "shortcutJsonObjectMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lorg/json/JSONObject;>;>;>;"
    :cond_6
    :goto_4
    const-string/jumbo v0, "shortcut config is null"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 217
    const/4 v0, 0x0

    return-object v0
.end method

.method private getStringAttributeByFunctionJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "attribute"    # Ljava/lang/String;

    .line 410
    invoke-direct {p0, p1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getFunctionJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "attribute"    # Ljava/lang/String;

    .line 481
    const-string v0, "MiuiKeyShortcutRuleManager"

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 486
    :cond_0
    :try_start_0
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-object v1

    .line 487
    :catch_0
    move-exception v2

    .line 488
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    .end local v2    # "e":Lorg/json/JSONException;
    return-object v1

    .line 482
    :cond_2
    :goto_0
    const-string v2, "get string attribute fail,jsonObject is null or attribute is null"

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    return-object v1
.end method

.method private initCombinationKeyRule(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/json/JSONObject;",
            ">;)V"
        }
    .end annotation

    .line 270
    .local p1, "combinationJsonObjectList":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 274
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    .line 278
    .local v1, "combinationJSONObject":Lorg/json/JSONObject;
    const-string v2, "primaryKey"

    invoke-direct {p0, v1, v2}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I

    move-result v2

    .line 280
    .local v2, "primaryKey":I
    const-string v3, "combinationKey"

    invoke-direct {p0, v1, v3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I

    move-result v9

    .line 282
    .local v9, "combinationKey":I
    const-string v3, "action"

    invoke-direct {p0, v1, v3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 283
    .local v10, "action":Ljava/lang/String;
    const-string v3, "function"

    invoke-direct {p0, v1, v3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeByFunctionJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 285
    .local v11, "function":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    iget v8, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mCurrentUserId:I

    .line 286
    move v4, v2

    move v5, v9

    move-object v6, v10

    move-object v7, v11

    invoke-virtual/range {v3 .. v8}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->getMiuiCombinationRule(IILjava/lang/String;Ljava/lang/String;I)Lcom/android/server/policy/MiuiCombinationRule;

    move-result-object v3

    .line 288
    .local v3, "combinationRule":Lcom/android/server/policy/MiuiCombinationRule;
    iget-object v4, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    invoke-virtual {v4, v10, v3}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->addRule(Ljava/lang/String;Lcom/android/server/policy/MiuiCombinationRule;)V

    .line 289
    iget-object v4, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    invoke-virtual {v4, v3}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->shouldHoldOnAOSPLogic(Lcom/android/server/policy/MiuiCombinationRule;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 290
    iget-object v4, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mKeyCombinationManager:Lcom/android/server/policy/KeyCombinationManager;

    invoke-virtual {v4, v3}, Lcom/android/server/policy/KeyCombinationManager;->addRule(Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;)V

    .line 292
    .end local v1    # "combinationJSONObject":Lorg/json/JSONObject;
    .end local v2    # "primaryKey":I
    .end local v3    # "combinationRule":Lcom/android/server/policy/MiuiCombinationRule;
    .end local v9    # "combinationKey":I
    .end local v10    # "action":Ljava/lang/String;
    .end local v11    # "function":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 293
    :cond_2
    return-void

    .line 271
    :cond_3
    :goto_1
    return-void
.end method

.method private initGestureRule(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/json/JSONObject;",
            ">;)V"
        }
    .end annotation

    .line 252
    .local p1, "gestureJsonObjectList":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 256
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    .line 258
    .local v1, "gestureJSONObject":Lorg/json/JSONObject;
    const-string v2, "action"

    invoke-direct {p0, v1, v2}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 259
    .local v2, "action":Ljava/lang/String;
    const-string v3, "function"

    invoke-direct {p0, v1, v3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeByFunctionJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 261
    .local v9, "function":Ljava/lang/String;
    new-instance v10, Lcom/android/server/policy/MiuiGestureRule;

    iget-object v4, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mHandler:Landroid/os/Handler;

    iget v8, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mCurrentUserId:I

    move-object v3, v10

    move-object v6, v2

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/android/server/policy/MiuiGestureRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;I)V

    .line 263
    .local v3, "miuiGestureRule":Lcom/android/server/policy/MiuiGestureRule;
    iget-object v4, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    invoke-virtual {v4, v2, v3}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->addRule(Ljava/lang/String;Lcom/android/server/policy/MiuiGestureRule;)V

    .line 266
    .end local v1    # "gestureJSONObject":Lorg/json/JSONObject;
    .end local v2    # "action":Ljava/lang/String;
    .end local v3    # "miuiGestureRule":Lcom/android/server/policy/MiuiGestureRule;
    .end local v9    # "function":Ljava/lang/String;
    goto :goto_0

    .line 267
    :cond_1
    return-void

    .line 253
    :cond_2
    :goto_1
    return-void
.end method

.method private initMiuiKeyShortcut()V
    .locals 13

    .line 168
    const-string/jumbo v0, "system_ext/etc/input/"

    const-string v1, "miui_shortcut_config.json"

    invoke-direct {p0, v0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getShortcutConfig(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, "shortcutConfigString":Ljava/lang/String;
    const-string v2, "product/etc/input/"

    invoke-direct {p0, v2, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getShortcutConfig(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 173
    .local v1, "shortcutConfigStringExtra":Ljava/lang/String;
    const-string v8, "defaultShortcutMapConfig"

    .line 174
    .local v8, "defaultShortcutConfigType":Ljava/lang/String;
    const-string v9, "extraShortcutMapConfig"

    .line 175
    .local v9, "extraShortcutConfigType":Ljava/lang/String;
    new-instance v10, Lcom/android/server/policy/MiuiKeyShortcutRuleManager$2;

    move-object v2, v10

    move-object v3, p0

    move-object v4, v8

    move-object v5, v0

    move-object v6, v9

    move-object v7, v1

    invoke-direct/range {v2 .. v7}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager$2;-><init>(Lcom/android/server/policy/MiuiKeyShortcutRuleManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    .local v2, "shortcutConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    const-string/jumbo v4, "singleKeyRules"

    const-string v5, "combinationKeyRules"

    const-string v6, "gestureRules"

    filled-new-array {v4, v5, v6}, [Ljava/lang/String;

    move-result-object v7

    .line 180
    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 183
    .local v3, "shortcutTypeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, v2, v3}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getShortcutJsonObjectMap(Ljava/util/Map;Ljava/util/List;)Ljava/util/Map;

    move-result-object v7

    .line 187
    .local v7, "shortcutJsonObjectMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lorg/json/JSONObject;>;>;>;"
    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map;

    .line 189
    .local v10, "defaultShortcutConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lorg/json/JSONObject;>;>;"
    if-eqz v10, :cond_0

    invoke-interface {v10}, Ljava/util/Map;->size()I

    move-result v11

    if-eqz v11, :cond_0

    .line 190
    invoke-interface {v10, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    invoke-direct {p0, v11}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->initSingleKeyRule(Ljava/util/List;)V

    .line 191
    nop

    .line 192
    invoke-interface {v10, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    .line 191
    invoke-direct {p0, v11}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->initCombinationKeyRule(Ljava/util/List;)V

    .line 193
    invoke-interface {v10, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    invoke-direct {p0, v11}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->initGestureRule(Ljava/util/List;)V

    .line 197
    :cond_0
    invoke-interface {v7, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Map;

    .line 199
    .local v11, "extraShortcutConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lorg/json/JSONObject;>;>;"
    if-eqz v11, :cond_1

    invoke-interface {v11}, Ljava/util/Map;->size()I

    move-result v12

    if-eqz v12, :cond_1

    .line 200
    invoke-interface {v11, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-direct {p0, v4}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->initSingleKeyRule(Ljava/util/List;)V

    .line 201
    invoke-interface {v11, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-direct {p0, v4}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->initCombinationKeyRule(Ljava/util/List;)V

    .line 202
    invoke-interface {v11, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-direct {p0, v4}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->initGestureRule(Ljava/util/List;)V

    .line 205
    :cond_1
    iget-object v4, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    invoke-virtual {v4}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->initSingleKeyRule()V

    .line 206
    iget-object v4, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    invoke-virtual {v4}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->initCombinationKeyRule()V

    .line 207
    iget-object v4, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    invoke-virtual {v4}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->initGestureRule()V

    .line 209
    return-void
.end method

.method private initSingleKeyRule(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/json/JSONObject;",
            ">;)V"
        }
    .end annotation

    .line 296
    .local p1, "singleKeyJsonObjectList":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 300
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    .line 301
    .local v1, "singleKeyObject":Lorg/json/JSONObject;
    const-string v2, "primaryKey"

    invoke-direct {p0, v1, v2}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I

    move-result v2

    .line 302
    .local v2, "primaryKey":I
    invoke-direct {p0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getMiuiSingleKeyInfo(Lorg/json/JSONObject;)Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;

    move-result-object v3

    .line 303
    .local v3, "miuiSingleKeyInfo":Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;
    iget-object v4, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    iget v5, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mCurrentUserId:I

    .line 304
    invoke-virtual {v4, v2, v3, v5}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->getMiuiSingleKeyRule(ILcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)Lcom/android/server/policy/MiuiSingleKeyRule;

    move-result-object v4

    .line 306
    .local v4, "miuiSingleKeyRule":Lcom/android/server/policy/MiuiSingleKeyRule;
    if-eqz v4, :cond_1

    .line 307
    iget-object v5, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    invoke-virtual {v5, v3, v4}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->addRule(Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;Lcom/android/server/policy/MiuiSingleKeyRule;)V

    .line 308
    iget-object v5, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mSingleKeyGestureDetector:Lcom/android/server/policy/SingleKeyGestureDetector;

    invoke-virtual {v5, v4}, Lcom/android/server/policy/SingleKeyGestureDetector;->addRule(Lcom/android/server/policy/SingleKeyGestureDetector$SingleKeyRule;)V

    .line 310
    .end local v1    # "singleKeyObject":Lorg/json/JSONObject;
    .end local v3    # "miuiSingleKeyInfo":Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;
    .end local v4    # "miuiSingleKeyRule":Lcom/android/server/policy/MiuiSingleKeyRule;
    :cond_1
    goto :goto_0

    .line 311
    .end local v2    # "primaryKey":I
    :cond_2
    return-void

    .line 297
    :cond_3
    :goto_1
    return-void
.end method

.method private isLegaDataInCurrentRule(Lorg/json/JSONObject;)Z
    .locals 6
    .param p1, "jsonObject"    # Lorg/json/JSONObject;

    .line 414
    const-string v0, "global"

    invoke-direct {p0, p1, v0}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getIntAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)I

    move-result v0

    .line 415
    .local v0, "global":I
    const-string v1, "region"

    invoke-direct {p0, p1, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getStringAttributeValueByJsonObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 417
    .local v1, "regions":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_4

    .line 418
    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v2, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    if-ne v4, v0, :cond_1

    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v2, :cond_2

    :cond_1
    const/4 v2, 0x2

    if-ne v2, v0, :cond_3

    :cond_2
    move v3, v4

    :cond_3
    return v3

    .line 423
    :cond_4
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 424
    .local v2, "regionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v5, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->CURRENT_DEVICE_REGION:Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    sget-object v5, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->CURRENT_DEVICE_CUSTOMIZED_REGION:Ljava/lang/String;

    .line 425
    invoke-interface {v2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    :cond_5
    move v3, v4

    .line 424
    :cond_6
    return v3
.end method

.method static synthetic lambda$new$0(I)[Ljava/lang/Integer;
    .locals 1
    .param p0, "x$0"    # I

    .line 115
    new-array v0, p0, [Ljava/lang/Integer;

    return-object v0
.end method

.method private readFully(Ljava/io/FileInputStream;)Ljava/lang/String;
    .locals 4
    .param p1, "inputStream"    # Ljava/io/FileInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 513
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 514
    .local v0, "baos":Ljava/io/OutputStream;
    const/16 v1, 0x400

    new-array v1, v1, [B

    .line 515
    .local v1, "buffer":[B
    invoke-virtual {p1, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v2

    .line 516
    .local v2, "read":I
    :goto_0
    if-ltz v2, :cond_0

    .line 517
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 518
    invoke-virtual {p1, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v2

    goto :goto_0

    .line 520
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private registerPackageChangeReceivers()V
    .locals 5

    .line 141
    new-instance v0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager$1;

    invoke-direct {v0, p0}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager$1;-><init>(Lcom/android/server/policy/MiuiKeyShortcutRuleManager;)V

    .line 152
    .local v0, "packageMonitor":Lcom/android/internal/content/PackageMonitor;
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Landroid/os/UserHandle;Z)V

    .line 153
    return-void
.end method

.method private resetShortcutSettings()V
    .locals 1

    .line 542
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    invoke-virtual {v0}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->resetShortcutSettings()V

    .line 543
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    invoke-virtual {v0}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->resetDefaultFunction()V

    .line 544
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    invoke-virtual {v0}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->resetDefaultFunction()V

    .line 545
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->resetMiuiShortcutSettings()V

    .line 546
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiShortcutOperatorCustomManager:Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;

    invoke-virtual {v0}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->initShortcut()V

    .line 547
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 585
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 586
    const-string v0, "MiuiKeyShortcutRuleManager"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 587
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 588
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 589
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 590
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 591
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 592
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 593
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 594
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 595
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 596
    return-void
.end method

.method public getCombinationRule(Ljava/lang/String;)Lcom/android/server/policy/MiuiCombinationRule;
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 550
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->getCombinationRule(Ljava/lang/String;)Lcom/android/server/policy/MiuiCombinationRule;

    move-result-object v0

    return-object v0
.end method

.method public getFunction(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 569
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->hasActionInSingleKeyMap(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 570
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 573
    :cond_0
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->hasActionInCombinationKeyMap(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 574
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 578
    :cond_1
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->hasActionInGestureRuleMap(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 579
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 581
    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGestureRule(Ljava/lang/String;)Lcom/android/server/policy/MiuiGestureRule;
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 558
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->getGestureManager(Ljava/lang/String;)Lcom/android/server/policy/MiuiGestureRule;

    move-result-object v0

    return-object v0
.end method

.method public getSingleKeyRule(I)Lcom/android/server/policy/MiuiSingleKeyRule;
    .locals 1
    .param p1, "primaryKey"    # I

    .line 554
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->getSingleKeyRuleForPrimaryKey(I)Lcom/android/server/policy/MiuiSingleKeyRule;

    move-result-object v0

    return-object v0
.end method

.method public onUserSwitch(I)V
    .locals 3
    .param p1, "currentUserId"    # I

    .line 529
    iput p1, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mCurrentUserId:I

    .line 530
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mInitForUsers:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 531
    .local v0, "isNewUser":Z
    if-eqz v0, :cond_0

    .line 532
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mInitForUsers:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 534
    :cond_0
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v1, p1, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->onUserSwitch(IZ)V

    .line 535
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    invoke-virtual {v1, p1, v0}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->onUserSwitch(IZ)V

    .line 536
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    invoke-virtual {v1, p1, v0}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->onUserSwitch(IZ)V

    .line 537
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    invoke-virtual {v1, p1, v0}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->onUserSwitch(IZ)V

    .line 538
    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiShortcutOperatorCustomManager:Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;

    invoke-virtual {v1, p1}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->onUserSwitch(I)V

    .line 539
    return-void
.end method

.method skipKeyGesutre(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 604
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->skipKeyGesutre(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public updatePolicyFlag(I)V
    .locals 1
    .param p1, "policyFlags"    # I

    .line 601
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->mMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    invoke-virtual {v0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->updatePolicyFlag(I)V

    .line 602
    return-void
.end method
