public class com.android.server.policy.MiuiShortcutTriggerHelper {
	 /* .source "MiuiShortcutTriggerHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String ACTION_KEYBOARD;
public static final java.lang.String CURRENT_DEVICE_CUSTOMIZED_REGION;
public static final java.lang.String CURRENT_DEVICE_REGION;
private static final java.lang.String DEVICE_REGION_RUSSIA;
public static final Integer DOUBLE_VOLUME_DOWN_KEY_TYPE_CLOSE;
public static final Integer DOUBLE_VOLUME_DOWN_KEY_TYPE_LAUNCH_CAMERA;
public static final Integer DOUBLE_VOLUME_DOWN_KEY_TYPE_LAUNCH_CAMERA_AND_TAKE_PHOTO;
public static final Integer FUNCTION_DEFAULT;
public static final Integer FUNCTION_DISABLE;
public static final Integer FUNCTION_ENABLE;
private static final java.lang.String KEY_IS_IN_MIUI_SOS_MODE;
private static final java.lang.String KEY_MIUI_SOS_ENABLE;
private static final Integer LONG_PRESS_POWER_GLOBAL_ACTIONS;
private static final Integer LONG_PRESS_POWER_NOTHING;
public static final java.lang.String PACKAGE_SMART_HOME;
private static final java.lang.String TAG;
public static final Integer TYPE_SOS_GOOGLE;
public static final Integer TYPE_SOS_MI;
public static final Integer VOICE_ASSIST_GUIDE_MAX_COUNT;
private static volatile com.android.server.policy.MiuiShortcutTriggerHelper sMiuiShortcutTriggerHelper;
/* # instance fields */
private Boolean mAOSPAssistantLongPressHomeEnabled;
private final android.content.ContentResolver mContentResolver;
private final android.content.Context mContext;
private Integer mCurrentUserId;
public Integer mDefaultLongPressTimeOut;
public Integer mDoubleTapOnHomeBehavior;
private Integer mFingerPrintNavCenterAction;
private java.lang.String mFivePressPowerLaunchGoogleSos;
private Boolean mFivePressPowerLaunchSos;
private Integer mHandledKeyCodeByLongPress;
private final android.os.Handler mHandler;
private Boolean mIsCtsMode;
private Boolean mIsOnSosMode;
public Boolean mLongPressPowerKeyLaunchSmartHome;
public Boolean mLongPressPowerKeyLaunchXiaoAi;
private final android.os.PowerManager mPowerManager;
private Boolean mPressToAppSwitch;
private Integer mRSAGuideStatus;
private final com.android.server.policy.MiuiShortcutTriggerHelper$ShortcutSettingsObserver mShortcutSettingsObserver;
private Boolean mSingleKeyUse;
private final Boolean mSupportGoogleSos;
private Boolean mTorchEnabled;
private final com.android.server.policy.WindowManagerPolicy mWindowManagerPolicy;
public Integer mXiaoaiPowerGuideFlag;
/* # direct methods */
static android.content.ContentResolver -$$Nest$fgetmContentResolver ( com.android.server.policy.MiuiShortcutTriggerHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContentResolver;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.policy.MiuiShortcutTriggerHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static Integer -$$Nest$fgetmCurrentUserId ( com.android.server.policy.MiuiShortcutTriggerHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mCurrentUserId:I */
} // .end method
static Boolean -$$Nest$fgetmSingleKeyUse ( com.android.server.policy.MiuiShortcutTriggerHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mSingleKeyUse:Z */
} // .end method
static void -$$Nest$fputmAOSPAssistantLongPressHomeEnabled ( com.android.server.policy.MiuiShortcutTriggerHelper p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mAOSPAssistantLongPressHomeEnabled:Z */
	 return;
} // .end method
static void -$$Nest$fputmFingerPrintNavCenterAction ( com.android.server.policy.MiuiShortcutTriggerHelper p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mFingerPrintNavCenterAction:I */
	 return;
} // .end method
static void -$$Nest$fputmFivePressPowerLaunchGoogleSos ( com.android.server.policy.MiuiShortcutTriggerHelper p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mFivePressPowerLaunchGoogleSos = p1;
	 return;
} // .end method
static void -$$Nest$fputmFivePressPowerLaunchSos ( com.android.server.policy.MiuiShortcutTriggerHelper p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mFivePressPowerLaunchSos:Z */
	 return;
} // .end method
static void -$$Nest$fputmIsCtsMode ( com.android.server.policy.MiuiShortcutTriggerHelper p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mIsCtsMode:Z */
	 return;
} // .end method
static void -$$Nest$fputmIsOnSosMode ( com.android.server.policy.MiuiShortcutTriggerHelper p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mIsOnSosMode:Z */
	 return;
} // .end method
static void -$$Nest$fputmPressToAppSwitch ( com.android.server.policy.MiuiShortcutTriggerHelper p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mPressToAppSwitch:Z */
	 return;
} // .end method
static void -$$Nest$fputmRSAGuideStatus ( com.android.server.policy.MiuiShortcutTriggerHelper p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mRSAGuideStatus:I */
	 return;
} // .end method
static void -$$Nest$fputmSingleKeyUse ( com.android.server.policy.MiuiShortcutTriggerHelper p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mSingleKeyUse:Z */
	 return;
} // .end method
static void -$$Nest$fputmTorchEnabled ( com.android.server.policy.MiuiShortcutTriggerHelper p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mTorchEnabled:Z */
	 return;
} // .end method
static com.android.server.policy.MiuiShortcutTriggerHelper ( ) {
	 /* .locals 2 */
	 /* .line 35 */
	 /* nop */
	 /* .line 36 */
	 final String v0 = "ro.miui.build.region"; // const-string v0, "ro.miui.build.region"
	 final String v1 = "CN"; // const-string v1, "CN"
	 android.os.SystemProperties .get ( v0,v1 );
	 /* .line 38 */
	 /* nop */
	 /* .line 39 */
	 final String v0 = "ro.miui.customized.region"; // const-string v0, "ro.miui.customized.region"
	 final String v1 = ""; // const-string v1, ""
	 android.os.SystemProperties .get ( v0,v1 );
	 /* .line 38 */
	 return;
} // .end method
private com.android.server.policy.MiuiShortcutTriggerHelper ( ) {
	 /* .locals 4 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 85 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 64 */
	 int v0 = -1; // const/4 v0, -0x1
	 /* iput v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mXiaoaiPowerGuideFlag:I */
	 /* .line 65 */
	 int v1 = 0; // const/4 v1, 0x0
	 /* iput v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDoubleTapOnHomeBehavior:I */
	 /* .line 74 */
	 /* iput v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mRSAGuideStatus:I */
	 /* .line 84 */
	 /* iput v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mHandledKeyCodeByLongPress:I */
	 /* .line 86 */
	 this.mContext = p1;
	 /* .line 87 */
	 com.android.server.input.MiuiInputThread .getHandler ( );
	 this.mHandler = v0;
	 /* .line 88 */
	 (( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 this.mContentResolver = v1;
	 /* .line 89 */
	 (( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
	 /* .line 90 */
	 /* const v3, 0x111013f */
	 v2 = 	 (( android.content.res.Resources ) v2 ).getBoolean ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z
	 /* iput-boolean v2, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mSupportGoogleSos:Z */
	 /* .line 91 */
	 /* const-class v2, Lcom/android/server/policy/WindowManagerPolicy; */
	 com.android.server.LocalServices .getService ( v2 );
	 /* check-cast v2, Lcom/android/server/policy/WindowManagerPolicy; */
	 this.mWindowManagerPolicy = v2;
	 /* .line 92 */
	 final String v2 = "power"; // const-string v2, "power"
	 (( android.content.Context ) p1 ).getSystemService ( v2 ); // invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v2, Landroid/os/PowerManager; */
	 this.mPowerManager = v2;
	 /* .line 94 */
	 /* const-string/jumbo v2, "support_gesture_shortcut_settings" */
	 int v3 = 1; // const/4 v3, 0x1
	 android.provider.Settings$Secure .putInt ( v1,v2,v3 );
	 /* .line 96 */
	 /* new-instance v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver; */
	 /* invoke-direct {v1, p0, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;-><init>(Lcom/android/server/policy/MiuiShortcutTriggerHelper;Landroid/os/Handler;)V */
	 this.mShortcutSettingsObserver = v1;
	 /* .line 97 */
	 (( com.android.server.policy.MiuiShortcutTriggerHelper$ShortcutSettingsObserver ) v1 ).initShortcutSettingsObserver ( ); // invoke-virtual {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->initShortcutSettingsObserver()V
	 /* .line 99 */
	 return;
} // .end method
public static java.lang.String getDoubleVolumeDownKeyFunction ( java.lang.String p0 ) {
	 /* .locals 4 */
	 /* .param p0, "doubleVolumeDownStatus" # Ljava/lang/String; */
	 /* .line 182 */
	 v0 = 	 android.text.TextUtils .isEmpty ( p0 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 183 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 185 */
	 } // :cond_0
	 int v0 = -1; // const/4 v0, -0x1
	 /* .line 187 */
	 /* .local v0, "status":I */
	 try { // :try_start_0
		 v1 = 		 java.lang.Integer .parseInt ( p0 );
		 /* :try_end_0 */
		 /* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* move v0, v1 */
		 /* .line 190 */
		 /* .line 188 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 189 */
		 /* .local v1, "e":Ljava/lang/NumberFormatException; */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v3, "status is invalid,status=" */
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v3 = "MiuiShortcutTriggerHelp"; // const-string v3, "MiuiShortcutTriggerHelp"
		 android.util.Slog .d ( v3,v2 );
		 /* .line 191 */
	 } // .end local v1 # "e":Ljava/lang/NumberFormatException;
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 192 */
final String v1 = "launch_camera"; // const-string v1, "launch_camera"
/* .line 193 */
} // :cond_1
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_2 */
/* .line 194 */
final String v1 = "launch_camera_and_take_photo"; // const-string v1, "launch_camera_and_take_photo"
/* .line 196 */
} // :cond_2
final String v1 = "none"; // const-string v1, "none"
} // .end method
public static com.android.server.policy.MiuiShortcutTriggerHelper getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 102 */
v0 = com.android.server.policy.MiuiShortcutTriggerHelper.sMiuiShortcutTriggerHelper;
/* if-nez v0, :cond_1 */
/* .line 103 */
/* const-class v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper; */
/* monitor-enter v0 */
/* .line 104 */
try { // :try_start_0
v1 = com.android.server.policy.MiuiShortcutTriggerHelper.sMiuiShortcutTriggerHelper;
/* if-nez v1, :cond_0 */
/* .line 105 */
/* new-instance v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper; */
/* invoke-direct {v1, p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;-><init>(Landroid/content/Context;)V */
/* .line 107 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 109 */
} // :cond_1
} // :goto_0
v0 = com.android.server.policy.MiuiShortcutTriggerHelper.sMiuiShortcutTriggerHelper;
} // .end method
private Integer getVeryLongPressPowerBehavior ( ) {
/* .locals 4 */
/* .line 461 */
int v0 = 0; // const/4 v0, 0x0
/* .line 462 */
/* .local v0, "veryLongPressBehavior":I */
v1 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) p0 ).isUserSetUpComplete ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isUserSetUpComplete()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 463 */
v1 = this.mContentResolver;
final String v2 = "long_press_power_key"; // const-string v2, "long_press_power_key"
/* iget v3, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mCurrentUserId:I */
android.provider.Settings$System .getStringForUser ( v1,v2,v3 );
/* .line 465 */
/* .local v1, "longPressPowerFunction":Ljava/lang/String; */
v2 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) p0 ).isSupportLongPressPowerGuide ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isSupportLongPressPowerGuide()Z
/* if-nez v2, :cond_0 */
v2 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v2, :cond_1 */
/* .line 466 */
final String v2 = "none"; // const-string v2, "none"
v2 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* .line 467 */
} // :cond_0
/* sget-boolean v2, Lmiui/hardware/input/InputFeature;->IS_SUPPORT_KDDI_POWER_GUIDE:Z */
/* xor-int/lit8 v2, v2, 0x1 */
/* move v0, v2 */
/* .line 470 */
} // .end local v1 # "longPressPowerFunction":Ljava/lang/String;
} // :cond_1
} // .end method
private void setupMenuFunction ( ) {
/* .locals 7 */
/* .line 519 */
v0 = this.mContentResolver;
/* iget v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mCurrentUserId:I */
final String v2 = "screen_key_press_app_switch"; // const-string v2, "screen_key_press_app_switch"
android.provider.Settings$System .getStringForUser ( v0,v2,v1 );
/* .line 521 */
/* .local v0, "menuKeyCurrentFunction":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 523 */
return;
/* .line 526 */
} // :cond_0
/* iget v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mCurrentUserId:I */
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* if-nez v1, :cond_1 */
/* move v5, v3 */
} // :cond_1
/* move v5, v4 */
/* .line 527 */
/* .local v5, "isAppSwitch":Z */
} // :goto_0
v6 = this.mContentResolver;
/* .line 529 */
if ( v5 != null) { // if-eqz v5, :cond_2
} // :cond_2
/* move v3, v4 */
/* .line 527 */
} // :goto_1
android.provider.Settings$System .putIntForUser ( v6,v2,v3,v1 );
/* .line 530 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 2 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 551 */
final String v0 = "MiuiShortcutTriggerHelp"; // const-string v0, "MiuiShortcutTriggerHelp"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 552 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 553 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 554 */
final String v0 = "mPressToAppSwitch="; // const-string v0, "mPressToAppSwitch="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 555 */
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mPressToAppSwitch:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 556 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 557 */
final String v0 = "isMiuiSosCanBeTrigger="; // const-string v0, "isMiuiSosCanBeTrigger="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 558 */
int v0 = 1; // const/4 v0, 0x1
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) p0 ).shouldLaunchSosByType ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->shouldLaunchSosByType(I)Z
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 559 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 560 */
final String v0 = "isGoogleSosEnable="; // const-string v0, "isGoogleSosEnable="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 561 */
int v0 = 0; // const/4 v0, 0x0
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) p0 ).shouldLaunchSosByType ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->shouldLaunchSosByType(I)Z
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 562 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 563 */
final String v0 = "mFingerPrintNavCenterAction="; // const-string v0, "mFingerPrintNavCenterAction="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 564 */
/* iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mFingerPrintNavCenterAction:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 565 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 566 */
final String v0 = "mXiaoaiPowerGuideFlag="; // const-string v0, "mXiaoaiPowerGuideFlag="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 567 */
/* iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mXiaoaiPowerGuideFlag:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 568 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 569 */
final String v0 = "mDefaultLongPressTimeOut="; // const-string v0, "mDefaultLongPressTimeOut="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 570 */
/* iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDefaultLongPressTimeOut:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 571 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 572 */
final String v0 = "mTorchEnabled="; // const-string v0, "mTorchEnabled="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 573 */
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mTorchEnabled:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 574 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 575 */
final String v0 = "mAOSPAssistantLongPressHomeEnabled="; // const-string v0, "mAOSPAssistantLongPressHomeEnabled="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 576 */
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mAOSPAssistantLongPressHomeEnabled:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 577 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 578 */
final String v0 = "mIsCtsMode="; // const-string v0, "mIsCtsMode="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 579 */
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mIsCtsMode:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 580 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 581 */
final String v0 = "mRSAGuideStatus="; // const-string v0, "mRSAGuideStatus="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 582 */
/* iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mRSAGuideStatus:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 583 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 584 */
final String v0 = "mHandledKeyCodeByLongPress="; // const-string v0, "mHandledKeyCodeByLongPress="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 585 */
/* iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mHandledKeyCodeByLongPress:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 586 */
return;
} // .end method
public Integer getDefaultLongPressTimeOut ( ) {
/* .locals 1 */
/* .line 213 */
/* iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mDefaultLongPressTimeOut:I */
} // .end method
public Integer getFingerPrintNavCenterAction ( ) {
/* .locals 1 */
/* .line 147 */
/* iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mFingerPrintNavCenterAction:I */
} // .end method
public java.lang.String getFivePressPowerLaunchGoogleSos ( ) {
/* .locals 1 */
/* .line 162 */
v0 = this.mFivePressPowerLaunchGoogleSos;
} // .end method
public android.os.PowerManager getPowerManager ( ) {
/* .locals 1 */
/* .line 178 */
v0 = this.mPowerManager;
} // .end method
public Integer getRSAGuideStatus ( ) {
/* .locals 1 */
/* .line 131 */
/* iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mRSAGuideStatus:I */
} // .end method
public Boolean interceptKeyByLongPress ( android.view.KeyEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 601 */
int v0 = 0; // const/4 v0, 0x0
/* .line 602 */
/* .local v0, "intercepted":Z */
/* iget v1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mHandledKeyCodeByLongPress:I */
v2 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* if-ne v1, v2, :cond_0 */
/* .line 603 */
v1 = (( android.view.KeyEvent ) p1 ).getFlags ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I
/* and-int/lit16 v1, v1, 0x400 */
/* if-nez v1, :cond_0 */
/* .line 604 */
int v0 = 1; // const/4 v0, 0x1
/* .line 607 */
} // :cond_0
} // .end method
public Boolean isCtsMode ( ) {
/* .locals 1 */
/* .line 205 */
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mIsCtsMode:Z */
} // .end method
public Boolean isLegalData ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "global" # I */
/* .param p2, "region" # Ljava/lang/String; */
/* .line 534 */
v0 = android.text.TextUtils .isEmpty ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 535 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_3
} // :cond_0
/* if-ne v1, p1, :cond_1 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_3 */
} // :cond_1
int v0 = 2; // const/4 v0, 0x2
/* if-ne v0, p1, :cond_2 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // :cond_3
} // :goto_0
/* .line 541 */
} // :cond_4
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p2 ).split ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v0 );
/* .line 542 */
/* .local v0, "regionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = v1 = com.android.server.policy.MiuiShortcutTriggerHelper.CURRENT_DEVICE_REGION;
} // .end method
public Boolean isPressToAppSwitch ( ) {
/* .locals 1 */
/* .line 135 */
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mPressToAppSwitch:Z */
} // .end method
public Boolean isSingleKeyUse ( ) {
/* .locals 1 */
/* .line 143 */
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mSingleKeyUse:Z */
} // .end method
public Boolean isSupportLongPressPowerGuide ( ) {
/* .locals 3 */
/* .line 151 */
int v0 = -1; // const/4 v0, -0x1
/* .line 153 */
/* .local v0, "longPressPowerGuideStatus":I */
v1 = com.android.server.policy.MiuiShortcutTriggerHelper.CURRENT_DEVICE_REGION;
final String v2 = "cn"; // const-string v2, "cn"
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 154 */
/* iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mXiaoaiPowerGuideFlag:I */
/* .line 155 */
} // :cond_0
v1 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) p0 ).supportRSARegion ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->supportRSARegion()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 156 */
/* iget v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mRSAGuideStatus:I */
/* .line 158 */
} // :cond_1
} // :goto_0
v1 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) p0 ).isUserSetUpComplete ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isUserSetUpComplete()Z
if ( v1 != null) { // if-eqz v1, :cond_2
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_2 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // :goto_1
} // .end method
public Boolean isTorchEnabled ( ) {
/* .locals 1 */
/* .line 201 */
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mTorchEnabled:Z */
} // .end method
public Boolean isUserSetUpComplete ( ) {
/* .locals 1 */
/* .line 493 */
v0 = this.mWindowManagerPolicy;
v0 = if ( v0 != null) { // if-eqz v0, :cond_0
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void notifyLongPressed ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "keyCode" # I */
/* .line 595 */
/* iput p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mHandledKeyCodeByLongPress:I */
/* .line 596 */
return;
} // .end method
public void onUserSwitch ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "currentUserId" # I */
/* .param p2, "isNewUser" # Z */
/* .line 498 */
/* iput p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mCurrentUserId:I */
/* .line 499 */
v0 = this.mShortcutSettingsObserver;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.policy.MiuiShortcutTriggerHelper$ShortcutSettingsObserver ) v0 ).onChange ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->onChange(Z)V
/* .line 500 */
/* invoke-direct {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setupMenuFunction()V */
/* .line 501 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 502 */
(( com.android.server.policy.MiuiShortcutTriggerHelper ) p0 ).resetMiuiShortcutSettings ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->resetMiuiShortcutSettings()V
/* .line 505 */
} // :cond_0
v0 = this.mContentResolver;
/* const-string/jumbo v1, "support_gesture_shortcut_settings" */
int v2 = 1; // const/4 v2, 0x1
android.provider.Settings$Secure .putIntForUser ( v0,v1,v2,p1 );
/* .line 507 */
return;
} // .end method
public void resetKeyCodeByLongPress ( ) {
/* .locals 1 */
/* .line 598 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mHandledKeyCodeByLongPress:I */
/* .line 599 */
return;
} // .end method
public void resetMiuiShortcutSettings ( ) {
/* .locals 2 */
/* .line 547 */
v0 = this.mShortcutSettingsObserver;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.policy.MiuiShortcutTriggerHelper$ShortcutSettingsObserver ) v0 ).onChange ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper$ShortcutSettingsObserver;->onChange(Z)V
/* .line 548 */
return;
} // .end method
public void setLongPressPowerBehavior ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "function" # Ljava/lang/String; */
/* .line 481 */
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) p0 ).isUserSetUpComplete ( ); // invoke-virtual {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isUserSetUpComplete()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 483 */
/* sget-boolean v0, Lmiui/hardware/input/InputFeature;->IS_SUPPORT_KDDI_POWER_GUIDE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 484 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 486 */
v1 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v1, :cond_0 */
final String v1 = "none"; // const-string v1, "none"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 487 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* .line 484 */
} // :goto_0
final String v2 = "power_button_long_press"; // const-string v2, "power_button_long_press"
android.provider.Settings$Global .putInt ( v0,v2,v1 );
/* .line 490 */
} // :cond_1
return;
} // .end method
public void setPressToAppSwitch ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "pressToAppSwitch" # Z */
/* .line 139 */
/* iput-boolean p1, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mPressToAppSwitch:Z */
/* .line 140 */
return;
} // .end method
public void setVeryLongPressPowerBehavior ( ) {
/* .locals 3 */
/* .line 474 */
v0 = /* invoke-direct {p0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getVeryLongPressPowerBehavior()I */
/* .line 475 */
/* .local v0, "behavior":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "set veryLongPressBehavior=" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiShortcutTriggerHelp"; // const-string v2, "MiuiShortcutTriggerHelp"
android.util.Slog .d ( v2,v1 );
/* .line 476 */
v1 = this.mContentResolver;
final String v2 = "power_button_very_long_press"; // const-string v2, "power_button_very_long_press"
android.provider.Settings$Global .putInt ( v1,v2,v0 );
/* .line 478 */
return;
} // .end method
public Boolean shouldLaunchSos ( ) {
/* .locals 3 */
/* .line 222 */
int v0 = 0; // const/4 v0, 0x0
v1 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) p0 ).shouldLaunchSosByType ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->shouldLaunchSosByType(I)Z
int v2 = 1; // const/4 v2, 0x1
/* if-nez v1, :cond_0 */
v1 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) p0 ).shouldLaunchSosByType ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->shouldLaunchSosByType(I)Z
if ( v1 != null) { // if-eqz v1, :cond_1
} // :cond_0
/* move v0, v2 */
} // :cond_1
} // .end method
public Boolean shouldLaunchSosByType ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "type" # I */
/* .line 166 */
int v0 = 1; // const/4 v0, 0x1
int v1 = 0; // const/4 v1, 0x0
/* packed-switch p1, :pswitch_data_0 */
/* .line 173 */
/* .line 168 */
/* :pswitch_0 */
/* iget-boolean v2, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mFivePressPowerLaunchSos:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* iget-boolean v2, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mIsOnSosMode:Z */
/* if-nez v2, :cond_0 */
} // :cond_0
/* move v0, v1 */
} // :goto_0
/* .line 170 */
/* :pswitch_1 */
/* iget-boolean v2, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mSupportGoogleSos:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = this.mFivePressPowerLaunchGoogleSos;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 171 */
final String v3 = "1"; // const-string v3, "1"
v2 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
} // :cond_1
} // :cond_2
/* move v0, v1 */
/* .line 170 */
} // :goto_1
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean skipKeyGesutre ( android.view.KeyEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 589 */
v0 = (( android.view.KeyEvent ) p1 ).getRepeatCount ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I
/* if-nez v0, :cond_1 */
v0 = (( android.view.KeyEvent ) p1 ).getFlags ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I
/* const/high16 v1, 0x1000000 */
/* and-int/2addr v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 592 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 590 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean supportAOSPTriggerFunction ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "keyCode" # I */
/* .line 124 */
int v0 = 3; // const/4 v0, 0x3
/* if-ne v0, p1, :cond_0 */
/* .line 125 */
/* iget-boolean v0, p0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mAOSPAssistantLongPressHomeEnabled:Z */
/* .line 127 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean supportRSARegion ( ) {
/* .locals 2 */
/* .line 113 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = com.android.server.policy.MiuiShortcutTriggerHelper.CURRENT_DEVICE_REGION;
/* .line 114 */
final String v1 = "ru"; // const-string v1, "ru"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 113 */
} // :goto_0
} // .end method
