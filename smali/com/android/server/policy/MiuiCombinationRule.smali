.class public abstract Lcom/android/server/policy/MiuiCombinationRule;
.super Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;
.source "MiuiCombinationRule.java"


# instance fields
.field private final mAction:Ljava/lang/String;

.field private final mCombinationKey:I

.field private final mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private final mDefaultFunction:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

.field private final mPrimaryKey:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;IILjava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "primaryKey"    # I
    .param p4, "combinationKey"    # I
    .param p5, "action"    # Ljava/lang/String;
    .param p6, "defaultFunction"    # Ljava/lang/String;
    .param p7, "currentUserId"    # I

    .line 22
    invoke-direct {p0, p3, p4}, Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;-><init>(II)V

    .line 23
    iput-object p1, p0, Lcom/android/server/policy/MiuiCombinationRule;->mContext:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/android/server/policy/MiuiCombinationRule;->mHandler:Landroid/os/Handler;

    .line 25
    iput-object p5, p0, Lcom/android/server/policy/MiuiCombinationRule;->mAction:Ljava/lang/String;

    .line 26
    iput p3, p0, Lcom/android/server/policy/MiuiCombinationRule;->mPrimaryKey:I

    .line 27
    iput p4, p0, Lcom/android/server/policy/MiuiCombinationRule;->mCombinationKey:I

    .line 28
    iput-object p6, p0, Lcom/android/server/policy/MiuiCombinationRule;->mDefaultFunction:Ljava/lang/String;

    .line 29
    iput p7, p0, Lcom/android/server/policy/MiuiCombinationRule;->mCurrentUserId:I

    .line 31
    return-void
.end method

.method private getInstance()Lcom/android/server/policy/MiuiCombinationRule;
    .locals 0

    .line 41
    return-object p0
.end method


# virtual methods
.method cancel()V
    .locals 0

    .line 79
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiCombinationRule;->cancelMiui()V

    .line 80
    return-void
.end method

.method protected cancelMiui()V
    .locals 0

    .line 84
    return-void
.end method

.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 0

    .line 8
    invoke-super {p0, p1}, Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method execute()V
    .locals 0

    .line 70
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiCombinationRule;->miuiExecute()V

    .line 71
    return-void
.end method

.method public getAction()Ljava/lang/String;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationRule;->mAction:Ljava/lang/String;

    return-object v0
.end method

.method public getCombinationKey()I
    .locals 1

    .line 49
    iget v0, p0, Lcom/android/server/policy/MiuiCombinationRule;->mCombinationKey:I

    return v0
.end method

.method public getFunction()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutObserver;->getFunction()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObserver()Lcom/android/server/policy/MiuiShortcutObserver;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    return-object v0
.end method

.method public getPrimaryKey()I
    .locals 1

    .line 45
    iget v0, p0, Lcom/android/server/policy/MiuiCombinationRule;->mPrimaryKey:I

    return v0
.end method

.method public bridge synthetic hashCode()I
    .locals 1

    .line 8
    invoke-super {p0}, Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;->hashCode()I

    move-result v0

    return v0
.end method

.method public init()V
    .locals 7

    .line 34
    new-instance v6, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;

    iget-object v1, p0, Lcom/android/server/policy/MiuiCombinationRule;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/policy/MiuiCombinationRule;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/server/policy/MiuiCombinationRule;->mAction:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/server/policy/MiuiCombinationRule;->mDefaultFunction:Ljava/lang/String;

    iget v5, p0, Lcom/android/server/policy/MiuiCombinationRule;->mCurrentUserId:I

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v6, p0, Lcom/android/server/policy/MiuiCombinationRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    .line 36
    invoke-direct {p0}, Lcom/android/server/policy/MiuiCombinationRule;->getInstance()Lcom/android/server/policy/MiuiCombinationRule;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/android/server/policy/MiuiShortcutObserver;->setRuleForObserver(Lcom/android/server/policy/MiuiCombinationRule;)V

    .line 37
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V

    .line 38
    return-void
.end method

.method protected miuiExecute()V
    .locals 0

    .line 75
    return-void
.end method

.method protected miuiPreCondition()Z
    .locals 1

    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method public onUserSwitch(IZ)V
    .locals 1
    .param p1, "currentUserId"    # I
    .param p2, "isNewUser"    # Z

    .line 65
    iget-object v0, p0, Lcom/android/server/policy/MiuiCombinationRule;->mMiuiShortcutObserver:Lcom/android/server/policy/MiuiShortcutObserver;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/MiuiShortcutObserver;->onUserSwitch(IZ)V

    .line 66
    return-void
.end method

.method public preCondition()Z
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiCombinationRule;->miuiPreCondition()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/android/server/policy/MiuiCombinationRule;->mPrimaryKey:I

    invoke-static {v1}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " + "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/policy/MiuiCombinationRule;->mCombinationKey:I

    .line 108
    invoke-static {v1}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " preCondition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 109
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiCombinationRule;->miuiPreCondition()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/policy/MiuiCombinationRule;->mAction:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " function="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 111
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiCombinationRule;->getFunction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 107
    return-object v0
.end method
