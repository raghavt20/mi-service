.class final Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiKeyInterceptExtend.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/policy/MiuiKeyInterceptExtend;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SettingsObserver"
.end annotation


# static fields
.field public static final FILTER_INTERCEPT_CLOUD_NAME:Ljava/lang/String; = "MIUI_FILTER_INTERCEPT_KEY"

.field public static final FILTER_INTERCEPT_MODE:Ljava/lang/String; = "inputFilterKeyIntercept"


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mFilterInterceptMode:I

.field final synthetic this$0:Lcom/android/server/policy/MiuiKeyInterceptExtend;


# direct methods
.method static bridge synthetic -$$Nest$mprocessCloudData(Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;->processCloudData()V

    return-void
.end method

.method public constructor <init>(Lcom/android/server/policy/MiuiKeyInterceptExtend;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 390
    iput-object p1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;->this$0:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    .line 391
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 382
    const/4 p1, 0x1

    iput p1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;->mFilterInterceptMode:I

    .line 392
    return-void
.end method

.method private processCloudData()V
    .locals 8

    .line 410
    const-string v0, "MiuiKeyIntercept"

    iget-object v1, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;->this$0:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    invoke-static {v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->-$$Nest$fgetmContext(Lcom/android/server/policy/MiuiKeyInterceptExtend;)Landroid/content/Context;

    move-result-object v1

    .line 411
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "MIUI_FILTER_INTERCEPT_KEY"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v1

    .line 413
    .local v1, "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 416
    :cond_0
    invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v2

    .line 417
    .local v2, "jsonObject":Lorg/json/JSONObject;
    iget v3, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;->mFilterInterceptMode:I

    .line 420
    .local v3, "isFilterInterceptMode":I
    :try_start_0
    const-string v5, "inputFilterKeyIntercept"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    move v3, v5

    .line 421
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Update CloudData for MiInput Success for:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 424
    goto :goto_0

    .line 422
    :catch_0
    move-exception v5

    .line 423
    .local v5, "exception":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "process CloudData Exception:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    .end local v5    # "exception":Ljava/lang/Exception;
    :goto_0
    iget v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;->mFilterInterceptMode:I

    if-eq v3, v0, :cond_2

    .line 427
    iput v3, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;->mFilterInterceptMode:I

    .line 428
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v0

    .line 429
    .local v0, "commonConfig":Lcom/android/server/input/config/InputCommonConfig;
    iget v5, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;->mFilterInterceptMode:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    move v4, v6

    :cond_1
    invoke-virtual {v0, v4}, Lcom/android/server/input/config/InputCommonConfig;->setFilterInterceptMode(Z)V

    .line 430
    invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 432
    .end local v0    # "commonConfig":Lcom/android/server/input/config/InputCommonConfig;
    :cond_2
    return-void

    .line 414
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    .end local v3    # "isFilterInterceptMode":I
    :cond_3
    :goto_1
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 403
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 404
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    invoke-direct {p0}, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;->processCloudData()V

    .line 407
    :cond_0
    return-void
.end method

.method public registerObserver()V
    .locals 4

    .line 395
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;->this$0:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    invoke-static {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->-$$Nest$fgetmContext(Lcom/android/server/policy/MiuiKeyInterceptExtend;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/policy/MiuiKeyInterceptExtend$SettingsObserver;->mContentResolver:Landroid/content/ContentResolver;

    .line 396
    nop

    .line 397
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    .line 396
    const/4 v2, 0x1

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 399
    return-void
.end method
