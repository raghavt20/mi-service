public class com.android.server.policy.MiuiGestureRule {
	 /* .source "MiuiGestureRule.java" */
	 /* # instance fields */
	 private final java.lang.String mAction;
	 private final android.content.Context mContext;
	 private Integer mCurrentUserId;
	 private java.lang.String mFunction;
	 private final android.os.Handler mHandler;
	 private com.android.server.policy.MiuiShortcutObserver mMiuiShortcutObserver;
	 /* # direct methods */
	 public com.android.server.policy.MiuiGestureRule ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .param p3, "action" # Ljava/lang/String; */
		 /* .param p4, "function" # Ljava/lang/String; */
		 /* .param p5, "currentUserId" # I */
		 /* .line 17 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 18 */
		 this.mContext = p1;
		 /* .line 19 */
		 this.mHandler = p2;
		 /* .line 20 */
		 this.mAction = p3;
		 /* .line 21 */
		 this.mFunction = p4;
		 /* .line 22 */
		 /* iput p5, p0, Lcom/android/server/policy/MiuiGestureRule;->mCurrentUserId:I */
		 /* .line 23 */
		 return;
	 } // .end method
	 private com.android.server.policy.MiuiGestureRule getInstance ( ) {
		 /* .locals 0 */
		 /* .line 33 */
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String getFunction ( ) {
		 /* .locals 1 */
		 /* .line 41 */
		 v0 = this.mMiuiShortcutObserver;
		 (( com.android.server.policy.MiuiShortcutObserver ) v0 ).getFunction ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutObserver;->getFunction()Ljava/lang/String;
	 } // .end method
	 public com.android.server.policy.MiuiShortcutObserver getObserver ( ) {
		 /* .locals 1 */
		 /* .line 37 */
		 v0 = this.mMiuiShortcutObserver;
	 } // .end method
	 public void init ( ) {
		 /* .locals 7 */
		 /* .line 26 */
		 /* new-instance v6, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver; */
		 v1 = this.mContext;
		 v2 = this.mHandler;
		 v3 = this.mAction;
		 v4 = this.mFunction;
		 /* iget v5, p0, Lcom/android/server/policy/MiuiGestureRule;->mCurrentUserId:I */
		 /* move-object v0, v6 */
		 /* invoke-direct/range {v0 ..v5}, Lcom/android/server/policy/MiuiCombinationKeyAndGestureObserver;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;I)V */
		 this.mMiuiShortcutObserver = v6;
		 /* .line 28 */
		 /* invoke-direct {p0}, Lcom/android/server/policy/MiuiGestureRule;->getInstance()Lcom/android/server/policy/MiuiGestureRule; */
		 (( com.android.server.policy.MiuiShortcutObserver ) v6 ).setRuleForObserver ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/server/policy/MiuiShortcutObserver;->setRuleForObserver(Lcom/android/server/policy/MiuiGestureRule;)V
		 /* .line 29 */
		 v0 = this.mMiuiShortcutObserver;
		 int v1 = 0; // const/4 v1, 0x0
		 (( com.android.server.policy.MiuiShortcutObserver ) v0 ).setDefaultFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V
		 /* .line 30 */
		 return;
	 } // .end method
	 public void onUserSwitch ( Integer p0, Boolean p1 ) {
		 /* .locals 1 */
		 /* .param p1, "currentUserId" # I */
		 /* .param p2, "isNewUser" # Z */
		 /* .line 45 */
		 v0 = this.mMiuiShortcutObserver;
		 (( com.android.server.policy.MiuiShortcutObserver ) v0 ).onUserSwitch ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/MiuiShortcutObserver;->onUserSwitch(IZ)V
		 /* .line 46 */
		 return;
	 } // .end method
