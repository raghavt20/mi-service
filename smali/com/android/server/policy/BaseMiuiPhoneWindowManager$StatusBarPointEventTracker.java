public class com.android.server.policy.BaseMiuiPhoneWindowManager$StatusBarPointEventTracker {
	 /* .source "BaseMiuiPhoneWindowManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x4 */
/* name = "StatusBarPointEventTracker" */
} // .end annotation
/* # instance fields */
private Float mDownX;
private Float mDownY;
final com.android.server.policy.BaseMiuiPhoneWindowManager this$0; //synthetic
/* # direct methods */
public com.android.server.policy.BaseMiuiPhoneWindowManager$StatusBarPointEventTracker ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
/* .line 1784 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1781 */
/* const/high16 v0, -0x40800000 # -1.0f */
/* iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$StatusBarPointEventTracker;->mDownX:F */
/* .line 1782 */
/* iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$StatusBarPointEventTracker;->mDownY:F */
/* .line 1785 */
return;
} // .end method
/* # virtual methods */
protected void onTrack ( android.view.MotionEvent p0 ) {
/* .locals 6 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 1788 */
v0 = (( android.view.MotionEvent ) p1 ).getActionMasked ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I
/* packed-switch v0, :pswitch_data_0 */
/* .line 1802 */
/* :pswitch_0 */
v0 = this.this$0;
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const/high16 v1, 0x11090000 */
int v2 = 0; // const/4 v2, 0x0
v0 = (( android.content.res.Resources ) v0 ).getFraction ( v1, v2, v2 ); // invoke-virtual {v0, v1, v2, v2}, Landroid/content/res/Resources;->getFraction(III)F
/* .line 1807 */
/* .local v0, "statusBarExpandHeight":F */
/* iget v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$StatusBarPointEventTracker;->mDownY:F */
/* cmpg-float v2, v0, v1 */
/* if-ltz v2, :cond_2 */
/* const/high16 v2, -0x40800000 # -1.0f */
/* cmpl-float v1, v1, v2 */
/* if-nez v1, :cond_0 */
/* .line 1809 */
} // :cond_0
/* iget v1, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$StatusBarPointEventTracker;->mDownX:F */
v2 = (( android.view.MotionEvent ) p1 ).getRawX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F
/* sub-float/2addr v1, v2 */
v1 = java.lang.Math .abs ( v1 );
/* .line 1810 */
/* .local v1, "distanceX":F */
/* iget v2, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$StatusBarPointEventTracker;->mDownY:F */
v3 = (( android.view.MotionEvent ) p1 ).getRawY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F
/* sub-float/2addr v2, v3 */
v2 = java.lang.Math .abs ( v2 );
/* .line 1813 */
/* .local v2, "distanceY":F */
/* const/high16 v3, 0x40000000 # 2.0f */
/* mul-float/2addr v3, v1 */
/* cmpl-float v3, v3, v2 */
/* if-gtz v3, :cond_2 */
int v3 = 0; // const/4 v3, 0x0
/* cmpl-float v4, v3, v2 */
/* if-lez v4, :cond_1 */
/* .line 1815 */
} // :cond_1
v4 = this.this$0;
int v5 = 1; // const/4 v5, 0x1
com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$msetStatusBarInFullscreen ( v4,v5 );
/* .line 1817 */
/* iput v3, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$StatusBarPointEventTracker;->mDownY:F */
/* .line 1819 */
/* .line 1790 */
} // .end local v0 # "statusBarExpandHeight":F
} // .end local v1 # "distanceX":F
} // .end local v2 # "distanceY":F
/* :pswitch_1 */
v0 = (( android.view.MotionEvent ) p1 ).getRawX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F
/* iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$StatusBarPointEventTracker;->mDownX:F */
/* .line 1791 */
v0 = (( android.view.MotionEvent ) p1 ).getRawY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F
/* iput v0, p0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager$StatusBarPointEventTracker;->mDownY:F */
/* .line 1792 */
/* nop */
/* .line 1823 */
} // :cond_2
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
