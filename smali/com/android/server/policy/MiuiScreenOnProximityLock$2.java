class com.android.server.policy.MiuiScreenOnProximityLock$2 extends android.os.Handler {
	 /* .source "MiuiScreenOnProximityLock.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/policy/MiuiScreenOnProximityLock;-><init>(Landroid/content/Context;Lcom/android/server/policy/MiuiKeyguardServiceDelegate;Landroid/os/Looper;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.policy.MiuiScreenOnProximityLock this$0; //synthetic
/* # direct methods */
 com.android.server.policy.MiuiScreenOnProximityLock$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/policy/MiuiScreenOnProximityLock; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 86 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 89 */
v0 = this.this$0;
/* monitor-enter v0 */
/* .line 90 */
try { // :try_start_0
	 /* iget v1, p1, Landroid/os/Message;->what:I */
	 /* packed-switch v1, :pswitch_data_0 */
	 /* .line 108 */
	 /* :pswitch_0 */
	 v1 = this.this$0;
	 com.android.server.policy.MiuiScreenOnProximityLock .-$$Nest$msetHintContainer ( v1 );
	 /* .line 101 */
	 /* :pswitch_1 */
	 v1 = this.this$0;
	 v2 = this.obj;
	 /* check-cast v2, Ljava/lang/Boolean; */
	 v2 = 	 (( java.lang.Boolean ) v2 ).booleanValue ( ); // invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
	 com.android.server.policy.MiuiScreenOnProximityLock .-$$Nest$mreleaseHintWindow ( v1,v2 );
	 /* .line 102 */
	 /* .line 105 */
	 /* :pswitch_2 */
	 v1 = this.this$0;
	 com.android.server.policy.MiuiScreenOnProximityLock .-$$Nest$mshowHint ( v1 );
	 /* .line 106 */
	 /* .line 97 */
	 /* :pswitch_3 */
	 v1 = this.this$0;
	 com.android.server.policy.MiuiScreenOnProximityLock .-$$Nest$mprepareHintWindow ( v1 );
	 /* .line 98 */
	 /* .line 92 */
	 /* :pswitch_4 */
	 final String v1 = "MiuiScreenOnProximityLock"; // const-string v1, "MiuiScreenOnProximityLock"
	 final String v2 = "far from the screen for a certain time, release proximity sensor..."; // const-string v2, "far from the screen for a certain time, release proximity sensor..."
	 android.util.Slog .d ( v1,v2 );
	 /* .line 93 */
	 v1 = this.this$0;
	 int v2 = 0; // const/4 v2, 0x0
	 (( com.android.server.policy.MiuiScreenOnProximityLock ) v1 ).release ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/policy/MiuiScreenOnProximityLock;->release(Z)Z
	 /* .line 94 */
	 /* nop */
	 /* .line 111 */
} // :goto_0
/* monitor-exit v0 */
/* .line 112 */
return;
/* .line 111 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
