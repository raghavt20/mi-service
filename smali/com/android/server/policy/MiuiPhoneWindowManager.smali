.class public Lcom/android/server/policy/MiuiPhoneWindowManager;
.super Lcom/android/server/policy/BaseMiuiPhoneWindowManager;
.source "MiuiPhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;
    }
.end annotation


# static fields
.field private static final ACTION_NOT_PASS_TO_USER:I = 0x0

.field private static final ACTION_PASS_TO_USER:I = 0x1

.field private static final CAMERA_COVERED_SERVICE:Ljava/lang/String; = "camera_covered_service"

.field private static final FINGERPRINT_NAV_ACTION_DEFAULT:I = -0x1

.field private static final FINGERPRINT_NAV_ACTION_HOME:I = 0x1

.field private static final FINGERPRINT_NAV_ACTION_NONE:I = 0x0

.field protected static final NAV_BAR_BOTTOM:I = 0x0

.field protected static final NAV_BAR_LEFT:I = 0x2

.field protected static final NAV_BAR_RIGHT:I = 0x1

.field private static final PERMISSION_INTERNAL_GENERAL_API:Ljava/lang/String; = "miui.permission.USE_INTERNAL_GENERAL_API"

.field private static final SUPPORT_POWERFP:Z

.field private static final XIAOMI_MIRROR_APP:Ljava/lang/String; = "com.xiaomi.mirror"


# instance fields
.field private interceptPowerKeyTimeByDpadCenter:J

.field private mAccountHelper:Lcom/android/server/wm/AccountHelper;

.field private mBiometricManager:Landroid/hardware/biometrics/BiometricManager;

.field private mCameraCoveredService:Lcom/miui/app/MiuiCameraCoveredManagerServiceInternal;

.field private mDisplayHeight:I

.field private mDisplayRotation:I

.field private mDisplayWidth:I

.field private mFingerprintService:Landroid/hardware/fingerprint/IFingerprintService;

.field private mFpNavCenterActionChooseDialog:Landroid/app/AlertDialog;

.field private mGetFpLockoutModeMethod:Ljava/lang/reflect/Method;

.field private mMiuiSecurityPermissionHandler:Lmiui/view/MiuiSecurityPermissionHandler;

.field private mPhoneWindowCallback:Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;

.field private final mPowerLock:Ljava/lang/Object;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAccountHelper(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lcom/android/server/wm/AccountHelper;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mAccountHelper:Lcom/android/server/wm/AccountHelper;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFpNavCenterActionChooseDialog(Lcom/android/server/policy/MiuiPhoneWindowManager;)Landroid/app/AlertDialog;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Landroid/app/AlertDialog;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiSecurityPermissionHandler(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lmiui/view/MiuiSecurityPermissionHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiSecurityPermissionHandler:Lmiui/view/MiuiSecurityPermissionHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPhoneWindowCallback(Lcom/android/server/policy/MiuiPhoneWindowManager;)Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;
    .locals 0

    iget-object p0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mPhoneWindowCallback:Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmFpNavCenterActionChooseDialog(Lcom/android/server/policy/MiuiPhoneWindowManager;Landroid/app/AlertDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method static bridge synthetic -$$Nest$mbringUpActionChooseDlg(Lcom/android/server/policy/MiuiPhoneWindowManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->bringUpActionChooseDlg()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 137
    nop

    .line 138
    const-string v0, "ro.hardware.fp.sideCap"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/policy/MiuiPhoneWindowManager;->SUPPORT_POWERFP:Z

    .line 137
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 114
    invoke-direct {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;-><init>()V

    .line 134
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->interceptPowerKeyTimeByDpadCenter:J

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mCameraCoveredService:Lcom/miui/app/MiuiCameraCoveredManagerServiceInternal;

    .line 139
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mPowerLock:Ljava/lang/Object;

    .line 552
    iput-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mGetFpLockoutModeMethod:Ljava/lang/reflect/Method;

    .line 567
    iput-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method private bringUpActionChooseDlg()V
    .locals 3

    .line 569
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 570
    return-void

    .line 573
    :cond_0
    new-instance v0, Lcom/android/server/policy/MiuiPhoneWindowManager$4;

    invoke-direct {v0, p0}, Lcom/android/server/policy/MiuiPhoneWindowManager$4;-><init>(Lcom/android/server/policy/MiuiPhoneWindowManager;)V

    .line 587
    .local v0, "listener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 588
    const v2, 0x110f01fd

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 589
    const v2, 0x110f01fa

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 590
    const v2, 0x110f01fc

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 591
    const v2, 0x110f01fb

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 592
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Landroid/app/AlertDialog;

    .line 593
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 594
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    const/16 v2, 0x7d8

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 595
    iget-object v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 596
    iget-object v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavCenterActionChooseDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 597
    return-void
.end method

.method private drawsSystemBarBackground(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)Z
    .locals 5
    .param p1, "win"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 639
    const/4 v0, 0x1

    if-eqz p1, :cond_1

    .line 640
    const-string v1, "getAttrs"

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p1, v1, v3}, Lcom/android/server/input/ReflectionUtils;->callPrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager$LayoutParams;

    .line 641
    .local v1, "attrs":Landroid/view/WindowManager$LayoutParams;
    if-eqz v1, :cond_1

    .line 642
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v4, -0x80000000

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    return v0

    .line 645
    .end local v1    # "attrs":Landroid/view/WindowManager$LayoutParams;
    :cond_1
    return v0
.end method

.method private forcesDrawStatusBarBackground(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)Z
    .locals 5
    .param p1, "win"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 649
    const/4 v0, 0x1

    if-eqz p1, :cond_1

    .line 650
    const-string v1, "getAttrs"

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p1, v1, v3}, Lcom/android/server/input/ReflectionUtils;->callPrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager$LayoutParams;

    .line 651
    .local v1, "attrs":Landroid/view/WindowManager$LayoutParams;
    if-eqz v1, :cond_1

    .line 652
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    const v4, 0x8000

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    return v0

    .line 655
    .end local v1    # "attrs":Landroid/view/WindowManager$LayoutParams;
    :cond_1
    return v0
.end method

.method private getExtraWindowSystemUiVis(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)I
    .locals 4
    .param p1, "transWin"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 624
    const/4 v0, 0x0

    .line 625
    .local v0, "vis":I
    if-eqz p1, :cond_0

    .line 626
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "getAttrs"

    invoke-static {p1, v2, v1}, Lcom/android/server/input/ReflectionUtils;->callPrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager$LayoutParams;

    .line 627
    .local v1, "attrs":Landroid/view/WindowManager$LayoutParams;
    if-eqz v1, :cond_0

    .line 628
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    or-int/2addr v0, v2

    .line 629
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 630
    or-int/lit8 v0, v0, 0x1

    .line 634
    .end local v1    # "attrs":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    invoke-static {v0}, Landroid/app/MiuiStatusBarManager;->getSystemUIVisibilityFlags(I)I

    move-result v0

    .line 635
    return v0
.end method

.method private hideNavBar(II)Z
    .locals 1
    .param p1, "flag"    # I
    .param p2, "sys"    # I

    .line 616
    and-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_1

    and-int/lit16 v0, p2, 0x1800

    if-eqz v0, :cond_0

    goto :goto_0

    .line 620
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 618
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private hideStatusBar(II)Z
    .locals 1
    .param p1, "flag"    # I
    .param p2, "sys"    # I

    .line 609
    and-int/lit16 v0, p1, 0x400

    if-nez v0, :cond_1

    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_0

    goto :goto_0

    .line 612
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 610
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private injectEvent(Landroid/view/KeyEvent;II)V
    .locals 17
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "injectKeyCode"    # I
    .param p3, "deviceId"    # I

    .line 600
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v13

    .line 601
    .local v13, "now":J
    new-instance v15, Landroid/view/KeyEvent;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getSource()I

    move-result v12

    move-object v0, v15

    move-wide v1, v13

    move-wide v3, v13

    move/from16 v6, p2

    move/from16 v9, p3

    invoke-direct/range {v0 .. v12}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    .line 602
    .local v15, "homeDown":Landroid/view/KeyEvent;
    new-instance v16, Landroid/view/KeyEvent;

    const/4 v5, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getSource()I

    move-result v12

    move-object/from16 v0, v16

    invoke-direct/range {v0 .. v12}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    .line 603
    .local v0, "homeUp":Landroid/view/KeyEvent;
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v15, v2}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    .line 604
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    .line 606
    return-void
.end method

.method private processBackFingerprintDpcenterEvent(Landroid/view/KeyEvent;Z)V
    .locals 7
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "isScreenOn"    # Z

    .line 494
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->isDeviceProvisioned()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 495
    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 497
    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, Landroid/os/PowerManager;->userActivity(JZ)V

    goto :goto_1

    .line 499
    :cond_0
    sget-boolean v1, Lcom/android/server/policy/MiuiPhoneWindowManager;->SUPPORT_POWERFP:Z

    const-string v2, "miui.policy:FINGERPRINT_DPAD_CENTER"

    if-eqz v1, :cond_4

    .line 500
    const/4 v1, 0x0

    .line 501
    .local v1, "lockout":Z
    iget-object v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFingerprintService:Landroid/hardware/fingerprint/IFingerprintService;

    if-eqz v3, :cond_2

    .line 503
    :try_start_0
    invoke-interface {v3, v0, v0}, Landroid/hardware/fingerprint/IFingerprintService;->getLockoutModeForUser(II)I

    move-result v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    move v1, v0

    .line 507
    goto :goto_0

    .line 505
    :catch_0
    move-exception v0

    .line 506
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v2

    throw v2

    .line 509
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->hasEnrolledFingerpirntForAuthentication()I

    move-result v0

    const/16 v3, 0xb

    if-eq v0, v3, :cond_3

    if-eqz v1, :cond_3

    .line 510
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fingerprint lockoutmode: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "BaseMiuiPhoneWindowManager"

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x12c

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->interceptPowerKeyTimeByDpadCenter:J

    .line 512
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4, v2}, Landroid/os/PowerManager;->wakeUp(JLjava/lang/String;)V

    .line 514
    .end local v1    # "lockout":Z
    :cond_3
    goto :goto_1

    .line 515
    :cond_4
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4, v2}, Landroid/os/PowerManager;->wakeUp(JLjava/lang/String;)V

    .line 519
    :cond_5
    :goto_1
    return-void
.end method

.method private processFrontFingerprintDpcenterEvent(Landroid/view/KeyEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 452
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 453
    iget-boolean v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDpadCenterDown:Z

    if-eqz v0, :cond_0

    .line 454
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDpadCenterDown:Z

    .line 455
    iget-boolean v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mHomeDownAfterDpCenter:Z

    if-eqz v2, :cond_0

    .line 456
    iput-boolean v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mHomeDownAfterDpCenter:Z

    .line 457
    const-string v0, "BaseMiuiPhoneWindowManager"

    const-string v1, "After dpcenter & home down, ignore tap fingerprint"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    return-void

    .line 461
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->isDeviceProvisioned()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;->isShowingAndNotHidden()Z

    move-result v0

    if-nez v0, :cond_5

    .line 462
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x12c

    cmp-long v0, v2, v4

    if-gez v0, :cond_5

    .line 463
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isSingleKeyUse()Z

    move-result v0

    const/4 v2, -0x1

    if-eqz v0, :cond_1

    .line 464
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0, v2}, Lcom/android/server/policy/MiuiPhoneWindowManager;->injectEvent(Landroid/view/KeyEvent;II)V

    goto :goto_0

    .line 466
    :cond_1
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 467
    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getFingerPrintNavCenterAction()I

    move-result v0

    if-ne v2, v0, :cond_2

    .line 468
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/policy/MiuiPhoneWindowManager$3;

    invoke-direct {v1, p0}, Lcom/android/server/policy/MiuiPhoneWindowManager$3;-><init>(Lcom/android/server/policy/MiuiPhoneWindowManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 474
    :cond_2
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 475
    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getFingerPrintNavCenterAction()I

    move-result v0

    if-ne v1, v0, :cond_3

    .line 476
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0, v2}, Lcom/android/server/policy/MiuiPhoneWindowManager;->injectEvent(Landroid/view/KeyEvent;II)V

    goto :goto_0

    .line 477
    :cond_3
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 478
    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getFingerPrintNavCenterAction()I

    .line 479
    return-void

    .line 483
    :cond_4
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_5

    .line 484
    iput-boolean v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mDpadCenterDown:Z

    .line 486
    :cond_5
    :goto_0
    return-void
.end method

.method private processFrontFingerprintDprightEvent(Landroid/view/KeyEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 490
    const-string v0, "BaseMiuiPhoneWindowManager"

    const-string v1, "processFrontFingerprintDprightEvent"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    return-void
.end method

.method private stopGoogleAssistantVoiceMonitoring()Z
    .locals 2

    .line 312
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiKeyShortcutRuleManager:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    .line 314
    const-string v1, "long_press_power_key"

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 313
    const-string v1, "launch_google_search"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    const/4 v0, 0x1

    return v0

    .line 318
    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method protected callSuperInterceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "policyFlags"    # I
    .param p3, "isScreenOn"    # Z

    .line 365
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mPowerLock:Ljava/lang/Object;

    monitor-enter v0

    .line 366
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->interceptKeyBeforeQueueing(Landroid/view/KeyEvent;I)I

    move-result v1

    monitor-exit v0

    return v1

    .line 367
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected cancelPreloadRecentAppsInternal()V
    .locals 2

    .line 276
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;

    move-result-object v0

    .line 277
    .local v0, "statusbar":Lcom/android/server/statusbar/StatusBarManagerInternal;
    if-eqz v0, :cond_0

    .line 278
    const-string v1, "execute cancelPreloadRecentAppsInternal"

    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 279
    invoke-interface {v0}, Lcom/android/server/statusbar/StatusBarManagerInternal;->cancelPreloadRecentApps()V

    .line 281
    :cond_0
    return-void
.end method

.method protected finishActivityInternal(Landroid/os/IBinder;ILandroid/content/Intent;)V
    .locals 2
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "code"    # I
    .param p3, "data"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 340
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, p3, v1}, Landroid/app/IActivityManager;->finishActivity(Landroid/os/IBinder;ILandroid/content/Intent;I)Z

    .line 341
    return-void
.end method

.method protected forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "OwningUserId"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 345
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    .line 346
    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/process/ProcessManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 347
    return-void
.end method

.method protected getFingerprintLockoutMode(Ljava/lang/Object;)I
    .locals 4
    .param p1, "bm"    # Ljava/lang/Object;

    .line 555
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mGetFpLockoutModeMethod:Ljava/lang/reflect/Method;

    if-nez v1, :cond_0

    .line 556
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "getLockoutMode"

    new-array v3, v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mGetFpLockoutModeMethod:Ljava/lang/reflect/Method;

    .line 558
    :cond_0
    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mGetFpLockoutModeMethod:Ljava/lang/reflect/Method;

    new-array v2, v0, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 559
    .local v0, "res":I
    return v0

    .line 560
    .end local v0    # "res":I
    :catch_0
    move-exception v1

    .line 561
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "BaseMiuiPhoneWindowManager"

    const-string v3, "getFingerprintLockoutMode function exception"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 564
    .end local v1    # "e":Ljava/lang/Exception;
    return v0
.end method

.method protected getKeyguardWindowState()Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    .locals 1

    .line 371
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getWakePolicyFlag()I
    .locals 1

    .line 392
    const/4 v0, 0x1

    return v0
.end method

.method protected hasEnrolledFingerpirntForAuthentication()I
    .locals 1

    .line 549
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mBiometricManager:Landroid/hardware/biometrics/BiometricManager;

    invoke-virtual {v0}, Landroid/hardware/biometrics/BiometricManager;->canAuthenticate()I

    move-result v0

    return v0
.end method

.method public init(Landroid/content/Context;Lcom/android/server/policy/WindowManagerPolicy$WindowManagerFuncs;Landroid/view/IWindowManager;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "windowManagerFuncs"    # Lcom/android/server/policy/WindowManagerPolicy$WindowManagerFuncs;
    .param p3, "windowManager"    # Landroid/view/IWindowManager;

    .line 144
    invoke-super {p0, p1, p2, p3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->init(Landroid/content/Context;Lcom/android/server/policy/WindowManagerPolicy$WindowManagerFuncs;Landroid/view/IWindowManager;)V

    .line 145
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/policy/MiuiPhoneWindowManager;->initInternal(Landroid/content/Context;Lcom/android/server/policy/WindowManagerPolicy$WindowManagerFuncs;Landroid/view/IWindowManager;)V

    .line 146
    return-void
.end method

.method protected intercept(Landroid/view/KeyEvent;IZI)I
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "policyFlags"    # I
    .param p3, "isScreenOn"    # Z
    .param p4, "expectedResult"    # I

    .line 380
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->intercept(Landroid/view/KeyEvent;IZI)I

    .line 381
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 382
    .local v0, "pm":Landroid/os/PowerManager;
    const/4 v1, -0x1

    if-ne p4, v1, :cond_0

    .line 383
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->goToSleep(J)V

    goto :goto_0

    .line 384
    :cond_0
    const/4 v1, 0x1

    if-ne p4, v1, :cond_1

    .line 385
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->wakeUp(J)V

    .line 387
    :cond_1
    :goto_0
    const/4 v1, 0x0

    return v1
.end method

.method public interceptKeyBeforeQueueing(Landroid/view/KeyEvent;I)I
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "policyFlags"    # I

    .line 351
    const/high16 v0, 0x20000000

    and-int/2addr v0, p2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->interceptKeyBeforeQueueingInternal(Landroid/view/KeyEvent;IZ)I

    move-result v0

    return v0
.end method

.method protected interceptPowerKeyByFingerPrintKey()Z
    .locals 4

    .line 545
    iget-wide v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->interceptPowerKeyTimeByDpadCenter:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected isFingerPrintKey(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 437
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavEventNameList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFpNavEventNameList:Ljava/util/List;

    .line 438
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 440
    .local v0, "keyCode":I
    packed-switch v0, :pswitch_data_0

    .line 445
    return v1

    .line 443
    :pswitch_0
    const/4 v1, 0x1

    return v1

    .line 448
    .end local v0    # "keyCode":I
    :cond_0
    return v1

    :pswitch_data_0
    .packed-switch 0x16
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected isInLockTaskMode()Z
    .locals 2

    .line 425
    :try_start_0
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v0

    .line 426
    .local v0, "activityTaskManager":Landroid/app/IActivityTaskManager;
    if-eqz v0, :cond_0

    .line 427
    invoke-interface {v0}, Landroid/app/IActivityTaskManager;->isInLockTaskMode()Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    .line 431
    .end local v0    # "activityTaskManager":Landroid/app/IActivityTaskManager;
    :cond_0
    goto :goto_0

    .line 429
    :catch_0
    move-exception v0

    .line 430
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 433
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method protected isScreenOnInternal()Z
    .locals 1

    .line 335
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->isScreenOn()Z

    move-result v0

    return v0
.end method

.method protected launchAssistActionInternal(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "hint"    # Ljava/lang/String;
    .param p2, "args"    # Landroid/os/Bundle;

    .line 323
    if-eqz p1, :cond_0

    .line 324
    const/4 v0, 0x1

    invoke-virtual {p2, p1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 326
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;

    move-result-object v0

    .line 327
    .local v0, "statusbar":Lcom/android/server/statusbar/StatusBarManagerInternal;
    if-eqz v0, :cond_1

    .line 328
    const-string v1, "WindowManager"

    const-string v2, "launch Google Assist"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    invoke-interface {v0, p2}, Lcom/android/server/statusbar/StatusBarManagerInternal;->startAssist(Landroid/os/Bundle;)V

    .line 331
    :cond_1
    return-void
.end method

.method protected launchRecentPanelInternal()V
    .locals 2

    .line 258
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;

    move-result-object v0

    .line 259
    .local v0, "statusbar":Lcom/android/server/statusbar/StatusBarManagerInternal;
    if-eqz v0, :cond_0

    .line 260
    const-string v1, "execute launchRecentPanelInternal"

    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 261
    invoke-interface {v0}, Lcom/android/server/statusbar/StatusBarManagerInternal;->toggleRecentApps()V

    .line 263
    :cond_0
    return-void
.end method

.method protected onStatusBarPanelRevealed(Lcom/android/internal/statusbar/IStatusBarService;)V
    .locals 2
    .param p1, "statusBarService"    # Lcom/android/internal/statusbar/IStatusBarService;

    .line 403
    const/4 v0, 0x1

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {p1, v0, v1}, Lcom/android/internal/statusbar/IStatusBarService;->onPanelRevealed(ZI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 406
    goto :goto_0

    .line 404
    :catch_0
    move-exception v0

    .line 405
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 407
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method protected preloadRecentAppsInternal()V
    .locals 2

    .line 267
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;

    move-result-object v0

    .line 268
    .local v0, "statusbar":Lcom/android/server/statusbar/StatusBarManagerInternal;
    if-eqz v0, :cond_0

    .line 269
    const-string v1, "execute preloadRecentAppsInternal"

    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 270
    invoke-interface {v0}, Lcom/android/server/statusbar/StatusBarManagerInternal;->preloadRecentApps()V

    .line 272
    :cond_0
    return-void
.end method

.method protected processFingerprintNavigationEvent(Landroid/view/KeyEvent;Z)I
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "isScreenOn"    # Z

    .line 522
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 523
    .local v0, "keyCode":I
    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    .line 540
    return v1

    .line 525
    :pswitch_0
    iget-boolean v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFrontFingerprintSensor:Z

    if-eqz v2, :cond_1

    .line 526
    iget-boolean v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mSupportTapFingerprintSensorToHome:Z

    if-eqz v2, :cond_0

    .line 527
    invoke-direct {p0, p1}, Lcom/android/server/policy/MiuiPhoneWindowManager;->processFrontFingerprintDpcenterEvent(Landroid/view/KeyEvent;)V

    .line 528
    return v1

    .line 530
    :cond_0
    return v1

    .line 533
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/android/server/policy/MiuiPhoneWindowManager;->processBackFingerprintDpcenterEvent(Landroid/view/KeyEvent;Z)V

    .line 534
    return v1

    .line 537
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/server/policy/MiuiPhoneWindowManager;->processFrontFingerprintDprightEvent(Landroid/view/KeyEvent;)V

    .line 538
    return v1

    :pswitch_data_0
    .packed-switch 0x16
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public registerMIUIWatermarkCallback(Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;

    .line 659
    iput-object p1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mPhoneWindowCallback:Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;

    .line 660
    return-void
.end method

.method protected screenOffBecauseOfProxSensor()Z
    .locals 1

    .line 397
    const/4 v0, 0x0

    return v0
.end method

.method showGlobalActionsInternal()V
    .locals 3

    .line 294
    const-string v0, "execute showGlobalActionsInternal"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 296
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_0

    .line 297
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "show_shutdown_dialog"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 298
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.xiaomi.mirror"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 300
    const-string v1, "miui.permission.USE_INTERNAL_GENERAL_API"

    invoke-virtual {p0, v0, v1}, Lcom/android/server/policy/MiuiPhoneWindowManager;->sendAsyncBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 303
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-direct {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->stopGoogleAssistantVoiceMonitoring()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 304
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "close_asssistant_ui"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 305
    const-string v0, "close asssistant ui"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 307
    :cond_1
    invoke-super {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->showGlobalActionsInternal()V

    .line 308
    return-void
.end method

.method protected stopLockTaskMode()Z
    .locals 2

    .line 411
    :try_start_0
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v0

    .line 412
    .local v0, "activityTaskManager":Landroid/app/IActivityTaskManager;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/app/IActivityTaskManager;->isInLockTaskMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 413
    invoke-interface {v0}, Landroid/app/IActivityTaskManager;->stopSystemLockTaskMode()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 414
    const/4 v1, 0x1

    return v1

    .line 418
    .end local v0    # "activityTaskManager":Landroid/app/IActivityTaskManager;
    :cond_0
    goto :goto_0

    .line 416
    :catch_0
    move-exception v0

    .line 417
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 420
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public systemBooted()V
    .locals 2

    .line 231
    invoke-super {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->systemBooted()V

    .line 232
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->notifySystemBooted()V

    .line 233
    invoke-static {}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->getInstance()Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->onSystemBooted()V

    .line 234
    invoke-static {}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getInstance()Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->systemBooted(Landroid/content/Context;)V

    .line 235
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getInstance(Landroid/content/Context;)Lcom/android/server/policy/MiuiKeyInterceptExtend;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->onSystemBooted()V

    .line 236
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->onSystemBooted()V

    .line 239
    :cond_0
    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_FRONTCAMERA_CIRCLE_BLACK:Z

    const-string v1, "WindowManager"

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mCameraCoveredService:Lcom/miui/app/MiuiCameraCoveredManagerServiceInternal;

    if-nez v0, :cond_2

    .line 240
    const-class v0, Lcom/miui/app/MiuiCameraCoveredManagerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/MiuiCameraCoveredManagerServiceInternal;

    iput-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mCameraCoveredService:Lcom/miui/app/MiuiCameraCoveredManagerServiceInternal;

    .line 241
    if-nez v0, :cond_1

    .line 242
    const-string v0, "camera_covered_service not start!"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    return-void

    .line 246
    :cond_1
    :try_start_0
    invoke-interface {v0}, Lcom/miui/app/MiuiCameraCoveredManagerServiceInternal;->systemBooted()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 247
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 249
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    goto :goto_1

    .line 251
    :cond_2
    const-string v0, "camera_covered_service start again!"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    :goto_1
    invoke-static {}, Lcom/android/server/input/TouchWakeUpFeatureManager;->getInstance()Lcom/android/server/input/TouchWakeUpFeatureManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/TouchWakeUpFeatureManager;->onSystemBooted()V

    .line 254
    return-void
.end method

.method public systemReady()V
    .locals 4

    .line 150
    invoke-super {p0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->systemReady()V

    .line 151
    new-instance v0, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mKeyguardDelegate:Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;

    iget-object v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/server/policy/MiuiKeyguardServiceDelegate;-><init>(Lcom/android/server/policy/PhoneWindowManager;Lcom/android/server/policy/keyguard/KeyguardServiceDelegate;Landroid/os/PowerManager;)V

    iput-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiKeyguardDelegate:Lcom/android/server/policy/MiuiKeyguardServiceDelegate;

    .line 152
    iget-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    const-class v1, Landroid/hardware/biometrics/BiometricManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/biometrics/BiometricManager;

    iput-object v0, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mBiometricManager:Landroid/hardware/biometrics/BiometricManager;

    .line 153
    const-string v0, "fingerprint"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 154
    .local v0, "binder":Landroid/os/IBinder;
    invoke-static {v0}, Landroid/hardware/fingerprint/IFingerprintService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/hardware/fingerprint/IFingerprintService;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mFingerprintService:Landroid/hardware/fingerprint/IFingerprintService;

    .line 155
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->systemReadyInternal()V

    .line 156
    sget-boolean v1, Lmiui/os/Build;->IS_PRIVATE_BUILD:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lmiui/os/Build;->IS_PRIVATE_WATER_MARKER:Z

    if-eqz v1, :cond_1

    .line 159
    :cond_0
    invoke-static {}, Lcom/android/server/wm/AccountHelper;->getInstance()Lcom/android/server/wm/AccountHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mAccountHelper:Lcom/android/server/wm/AccountHelper;

    .line 160
    iget-object v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/android/server/policy/MiuiPhoneWindowManager$1;

    invoke-direct {v3, p0}, Lcom/android/server/policy/MiuiPhoneWindowManager$1;-><init>(Lcom/android/server/policy/MiuiPhoneWindowManager;)V

    invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/AccountHelper;->registerAccountListener(Landroid/content/Context;Lcom/android/server/wm/AccountHelper$AccountCallback;)V

    .line 183
    new-instance v1, Lmiui/view/MiuiSecurityPermissionHandler;

    iget-object v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/android/server/policy/MiuiPhoneWindowManager$2;

    invoke-direct {v3, p0}, Lcom/android/server/policy/MiuiPhoneWindowManager$2;-><init>(Lcom/android/server/policy/MiuiPhoneWindowManager;)V

    invoke-direct {v1, v2, v3}, Lmiui/view/MiuiSecurityPermissionHandler;-><init>(Landroid/content/Context;Lmiui/view/MiuiSecurityPermissionHandler$PermissionViewCallback;)V

    iput-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mMiuiSecurityPermissionHandler:Lmiui/view/MiuiSecurityPermissionHandler;

    .line 220
    :cond_1
    sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v1, :cond_2

    .line 221
    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->enableAutoRemoveShortCutWhenAppRemove()V

    .line 222
    invoke-static {}, Lcom/android/server/input/KeyboardCombinationManagerStub;->get()Lcom/android/server/input/KeyboardCombinationManagerStub;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-interface {v1, v2}, Lcom/android/server/input/KeyboardCombinationManagerStub;->init(Landroid/content/Context;)V

    .line 224
    :cond_2
    iget-object v1, p0, Lcom/android/server/policy/MiuiPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->systemReady()V

    .line 225
    const-class v1, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    invoke-static {v1}, Ljava/util/Optional;->ofNullable(Ljava/lang/Object;)Ljava/util/Optional;

    move-result-object v1

    new-instance v2, Lcom/android/server/policy/MiuiPhoneWindowManager$$ExternalSyntheticLambda0;

    invoke-direct {v2}, Lcom/android/server/policy/MiuiPhoneWindowManager$$ExternalSyntheticLambda0;-><init>()V

    .line 226
    invoke-virtual {v1, v2}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 227
    return-void
.end method

.method protected toggleSplitScreenInternal()V
    .locals 2

    .line 285
    invoke-virtual {p0}, Lcom/android/server/policy/MiuiPhoneWindowManager;->getStatusBarManagerInternal()Lcom/android/server/statusbar/StatusBarManagerInternal;

    move-result-object v0

    .line 286
    .local v0, "statusbar":Lcom/android/server/statusbar/StatusBarManagerInternal;
    if-eqz v0, :cond_0

    .line 287
    const-string v1, "execute toggleSplitScreenInternal"

    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 288
    invoke-interface {v0}, Lcom/android/server/statusbar/StatusBarManagerInternal;->toggleSplitScreen()V

    .line 290
    :cond_0
    return-void
.end method
