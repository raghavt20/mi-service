.class Lcom/android/server/policy/MiuiKeyShortcutRuleManager$1;
.super Lcom/android/internal/content/PackageMonitor;
.source "MiuiKeyShortcutRuleManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->registerPackageChangeReceivers()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;


# direct methods
.method constructor <init>(Lcom/android/server/policy/MiuiKeyShortcutRuleManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    .line 141
    iput-object p1, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager$1;->this$0:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    invoke-direct {p0}, Lcom/android/internal/content/PackageMonitor;-><init>()V

    return-void
.end method


# virtual methods
.method public onPackageDataCleared(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 144
    const-string v0, "com.android.settings"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPackageDataCleared uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiKeyShortcutRuleManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    iget-object v0, p0, Lcom/android/server/policy/MiuiKeyShortcutRuleManager$1;->this$0:Lcom/android/server/policy/MiuiKeyShortcutRuleManager;

    invoke-static {v0}, Lcom/android/server/policy/MiuiKeyShortcutRuleManager;->-$$Nest$mresetShortcutSettings(Lcom/android/server/policy/MiuiKeyShortcutRuleManager;)V

    .line 148
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/internal/content/PackageMonitor;->onPackageDataCleared(Ljava/lang/String;I)V

    .line 149
    return-void
.end method
