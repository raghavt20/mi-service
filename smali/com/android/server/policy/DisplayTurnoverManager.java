public class com.android.server.policy.DisplayTurnoverManager implements android.hardware.SensorEventListener {
	 /* .source "DisplayTurnoverManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/policy/DisplayTurnoverManager$SettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ACCURATE_THRESHOLD;
public static final Integer CODE_TURN_OFF_SUB_DISPLAY;
public static final Integer CODE_TURN_ON_SUB_DISPLAY;
private static final Integer CRESCENDO_RINGER_MILLI_TIME_DELAY_ANTISPAM_STRANGER;
private static final Integer CRESCENDO_RINGER_MILLI_TIME_DELAY_MARK_RINGING;
private static final Integer CRESCENDO_RINGER_SECONDS_TIME;
private static final Integer CRESCENDO_RINGER_SECONDS_TIME_WITH_HEADSET;
private static final Integer DECRESCENDO_DELAY_TIME;
private static final Float DECRESCENDO_INTERVAL_VOLUME;
private static final Float DECRESCENDO_MIN_VOLUME;
private static final Float HORIZONTAL_Z_ACCELERATION;
private static final java.lang.String KEY_SUB_SCREEN_FACE_UP;
private static final Float NS2S;
private static final Integer SENSOR_SCREEN_DOWN;
private static final Integer SUB_SCREEN_OFF;
private static final Integer SUB_SCREEN_ON;
private static final java.lang.String TAG;
private static final Float TILT_Z_ACCELERATION;
private static final Integer TRIGGER_THRESHOLD;
/* # instance fields */
private android.util.Pair mAccValues;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Float;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.hardware.Sensor mAccelerometerSensor;
private android.content.Context mContext;
private android.hardware.Sensor mGravitySensor;
private mGravityValues;
private android.hardware.Sensor mGyroscopeSensor;
private Float mGyroscopeTimestamp;
private mGyroscopeValues;
private android.os.Handler mHandler;
private android.os.IBinder mIPowerManager;
private Boolean mIsLastStateOn;
private Boolean mIsTurnOverMuteEnabled;
private Boolean mIsUserSetupComplete;
private android.hardware.Sensor mLinearAccSensor;
private android.util.Pair mLinearAccValues;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Float;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mListeningSensor;
private mRadians;
private android.hardware.Sensor mScreendownSensor;
private mSensorDownValues;
private android.hardware.SensorManager mSensorManager;
private com.android.server.policy.DisplayTurnoverManager$SettingsObserver mSettingsObserver;
private Boolean mTurnoverTriggered;
/* # direct methods */
static void -$$Nest$mupdateSubSwitchMode ( com.android.server.policy.DisplayTurnoverManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/policy/DisplayTurnoverManager;->updateSubSwitchMode()V */
return;
} // .end method
public com.android.server.policy.DisplayTurnoverManager ( ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 90 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 60 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mListeningSensor:Z */
/* .line 91 */
this.mContext = p1;
/* .line 92 */
/* const-string/jumbo v0, "sensor" */
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/SensorManager; */
this.mSensorManager = v0;
/* .line 93 */
/* new-instance v0, Landroid/os/Handler; */
/* invoke-direct {v0}, Landroid/os/Handler;-><init>()V */
this.mHandler = v0;
/* .line 94 */
final String v0 = "power"; // const-string v0, "power"
android.os.ServiceManager .getService ( v0 );
this.mIPowerManager = v0;
/* .line 95 */
v0 = this.mContext;
/* new-instance v1, Lcom/android/server/policy/DisplayTurnoverManager$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/policy/DisplayTurnoverManager$1;-><init>(Lcom/android/server/policy/DisplayTurnoverManager;)V */
/* new-instance v2, Landroid/content/IntentFilter; */
final String v3 = "android.intent.action.USER_SWITCHED"; // const-string v3, "android.intent.action.USER_SWITCHED"
/* invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
int v3 = 0; // const/4 v3, 0x0
v4 = this.mHandler;
(( android.content.Context ) v0 ).registerReceiver ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
/* .line 101 */
return;
} // .end method
private Boolean isUserSetupComplete ( ) {
/* .locals 5 */
/* .line 246 */
/* iget-boolean v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsUserSetupComplete:Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 247 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "user_setup_complete" */
int v3 = -2; // const/4 v3, -0x2
int v4 = 0; // const/4 v4, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v4,v3 );
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_1
/* move v1, v4 */
} // :goto_0
/* iput-boolean v1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsUserSetupComplete:Z */
/* .line 249 */
} // .end method
private void readyToTurnoverOrByScreenDownSensor ( ) {
/* .locals 6 */
/* .line 149 */
v0 = this.mSensorDownValues;
int v1 = 0; // const/4 v1, 0x0
/* aget v0, v0, v1 */
/* .line 150 */
/* .local v0, "z":F */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* float-to-int v3, v0 */
int v4 = -2; // const/4 v4, -0x2
/* const-string/jumbo v5, "sub_screen_face_up" */
android.provider.Settings$System .putIntForUser ( v2,v5,v3,v4 );
/* .line 153 */
/* iget-boolean v2, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsTurnOverMuteEnabled:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 154 */
/* float-to-double v2, v0 */
/* const-wide/high16 v4, 0x4000000000000000L # 2.0 */
/* cmpl-double v2, v2, v4 */
/* if-nez v2, :cond_0 */
/* iget-boolean v2, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsLastStateOn:Z */
/* if-nez v2, :cond_0 */
/* .line 155 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, v1}, Lcom/android/server/policy/DisplayTurnoverManager;->turnOverMute(Z)V */
/* .line 156 */
} // :cond_0
/* float-to-double v2, v0 */
/* const-wide/high16 v4, 0x3ff0000000000000L # 1.0 */
/* cmpl-double v2, v2, v4 */
/* if-nez v2, :cond_1 */
/* iget-boolean v2, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsLastStateOn:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 157 */
/* invoke-direct {p0, v1}, Lcom/android/server/policy/DisplayTurnoverManager;->turnOverMute(Z)V */
/* .line 160 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void readyToTurnoverOrHandonByGravity ( ) {
/* .locals 5 */
/* .line 163 */
v0 = this.mGravityValues;
int v1 = 2; // const/4 v1, 0x2
/* aget v0, v0, v1 */
/* .line 166 */
/* .local v0, "z":F */
/* iget-boolean v1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsTurnOverMuteEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 167 */
/* float-to-double v1, v0 */
/* const-wide/high16 v3, -0x3fe8000000000000L # -6.0 */
/* cmpg-double v1, v1, v3 */
/* if-gez v1, :cond_0 */
/* iget-boolean v1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsLastStateOn:Z */
/* if-nez v1, :cond_0 */
/* .line 168 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, v1}, Lcom/android/server/policy/DisplayTurnoverManager;->turnOverMute(Z)V */
/* .line 169 */
} // :cond_0
/* float-to-double v1, v0 */
/* const-wide/high16 v3, 0x4010000000000000L # 4.0 */
/* cmpl-double v1, v1, v3 */
/* if-lez v1, :cond_1 */
/* iget-boolean v1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsLastStateOn:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 170 */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v1}, Lcom/android/server/policy/DisplayTurnoverManager;->turnOverMute(Z)V */
/* .line 173 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void startSensor ( ) {
/* .locals 5 */
/* .line 181 */
/* iget-boolean v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsTurnOverMuteEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 183 */
v0 = this.mSensorManager;
/* const v1, 0x1fa265d */
int v2 = 1; // const/4 v2, 0x1
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;
this.mScreendownSensor = v0;
/* .line 184 */
v0 = this.mSensorManager;
/* const/16 v1, 0x9 */
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mGravitySensor = v0;
/* .line 185 */
v1 = this.mScreendownSensor;
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 186 */
final String v0 = "DisplayTurnoverManager"; // const-string v0, "DisplayTurnoverManager"
final String v1 = "mScreendownSensor"; // const-string v1, "mScreendownSensor"
android.util.Log .d ( v0,v1 );
/* .line 187 */
v0 = this.mSensorManager;
v1 = this.mScreendownSensor;
(( android.hardware.SensorManager ) v0 ).registerListener ( p0, v1, v3 ); // invoke-virtual {v0, p0, v1, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 189 */
} // :cond_0
int v1 = 3; // const/4 v1, 0x3
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 190 */
v3 = this.mSensorManager;
(( android.hardware.SensorManager ) v3 ).registerListener ( p0, v0, v1 ); // invoke-virtual {v3, p0, v0, v1}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 193 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mGyroscopeTimestamp:F */
/* .line 194 */
/* new-array v0, v1, [F */
this.mRadians = v0;
/* .line 195 */
v0 = this.mSensorManager;
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v2 ); // invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mAccelerometerSensor = v0;
/* .line 196 */
v0 = this.mSensorManager;
/* const/16 v4, 0xa */
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v4 ); // invoke-virtual {v0, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mLinearAccSensor = v0;
/* .line 197 */
v0 = this.mSensorManager;
int v4 = 4; // const/4 v4, 0x4
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v4 ); // invoke-virtual {v0, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mGyroscopeSensor = v0;
/* .line 198 */
v0 = this.mSensorManager;
v4 = this.mAccelerometerSensor;
(( android.hardware.SensorManager ) v0 ).registerListener ( p0, v4, v3 ); // invoke-virtual {v0, p0, v4, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 200 */
v0 = this.mSensorManager;
v4 = this.mLinearAccSensor;
(( android.hardware.SensorManager ) v0 ).registerListener ( p0, v4, v3 ); // invoke-virtual {v0, p0, v4, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 202 */
v0 = this.mSensorManager;
v3 = this.mGyroscopeSensor;
(( android.hardware.SensorManager ) v0 ).registerListener ( p0, v3, v1 ); // invoke-virtual {v0, p0, v3, v1}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 205 */
} // :goto_0
/* iput-boolean v2, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mListeningSensor:Z */
/* .line 207 */
} // :cond_2
return;
} // .end method
private void stopSensor ( ) {
/* .locals 1 */
/* .line 210 */
v0 = this.mSensorManager;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( p0 ); // invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 211 */
return;
} // .end method
private void turnOnOrOFFSubDisplay ( Integer p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "code" # I */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 225 */
v0 = /* invoke-direct {p0}, Lcom/android/server/policy/DisplayTurnoverManager;->isUserSetupComplete()Z */
/* if-nez v0, :cond_0 */
/* .line 226 */
return;
/* .line 228 */
} // :cond_0
v0 = this.mIPowerManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 229 */
android.os.Parcel .obtain ( );
/* .line 230 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 231 */
/* .local v1, "reply":Landroid/os/Parcel; */
final String v2 = "android.os.IPowerManager"; // const-string v2, "android.os.IPowerManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 232 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
(( android.os.Parcel ) v0 ).writeLong ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/os/Parcel;->writeLong(J)V
/* .line 233 */
(( android.os.Parcel ) v0 ).writeString ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 235 */
try { // :try_start_0
v2 = this.mIPowerManager;
int v3 = 1; // const/4 v3, 0x1
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 239 */
/* nop */
} // :goto_0
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 240 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 241 */
/* .line 239 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 236 */
/* :catch_0 */
/* move-exception v2 */
/* .line 237 */
/* .local v2, "ex":Landroid/os/RemoteException; */
try { // :try_start_1
final String v3 = "DisplayTurnoverManager"; // const-string v3, "DisplayTurnoverManager"
final String v4 = "Failed to wake up secondary display"; // const-string v4, "Failed to wake up secondary display"
android.util.Log .e ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 239 */
/* nop */
} // .end local v2 # "ex":Landroid/os/RemoteException;
} // :goto_1
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 240 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 241 */
/* throw v2 */
/* .line 243 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_1
} // :goto_2
return;
} // .end method
private void turnOverMute ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "isOnSubScreen" # Z */
/* .line 215 */
final String v0 = "DisplayTurnoverManager"; // const-string v0, "DisplayTurnoverManager"
try { // :try_start_0
/* iput-boolean p1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsLastStateOn:Z */
/* .line 216 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Set SubScreen On: "; // const-string v2, "Set SubScreen On: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 217 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* const v1, 0xfffffc */
} // :cond_0
/* const v1, 0xfffffb */
/* .line 218 */
/* .local v1, "code":I */
} // :goto_0
final String v2 = "TURN_OVER"; // const-string v2, "TURN_OVER"
/* invoke-direct {p0, v1, v2}, Lcom/android/server/policy/DisplayTurnoverManager;->turnOnOrOFFSubDisplay(ILjava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 221 */
} // .end local v1 # "code":I
/* .line 219 */
/* :catch_0 */
/* move-exception v1 */
/* .line 220 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "close sub display manager service connect fail!"; // const-string v2, "close sub display manager service connect fail!"
android.util.Log .d ( v0,v2 );
/* .line 222 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void updateSubSwitchMode ( ) {
/* .locals 4 */
/* .line 253 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
/* const-string/jumbo v2, "subscreen_switch" */
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
/* move v1, v3 */
} // :goto_0
/* iput-boolean v1, p0, Lcom/android/server/policy/DisplayTurnoverManager;->mIsTurnOverMuteEnabled:Z */
/* .line 255 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 256 */
/* invoke-direct {p0}, Lcom/android/server/policy/DisplayTurnoverManager;->startSensor()V */
/* .line 258 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/policy/DisplayTurnoverManager;->stopSensor()V */
/* .line 259 */
/* invoke-direct {p0, v3}, Lcom/android/server/policy/DisplayTurnoverManager;->turnOverMute(Z)V */
/* .line 261 */
} // :goto_1
return;
} // .end method
/* # virtual methods */
public void onAccuracyChanged ( android.hardware.Sensor p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .param p2, "accuracy" # I */
/* .line 178 */
return;
} // .end method
public void onSensorChanged ( android.hardware.SensorEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 116 */
v0 = this.sensor;
v0 = (( android.hardware.Sensor ) v0 ).getType ( ); // invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I
int v1 = 2; // const/4 v1, 0x2
/* sparse-switch v0, :sswitch_data_0 */
/* .line 130 */
/* :sswitch_0 */
v0 = this.values;
this.mSensorDownValues = v0;
/* .line 131 */
/* .line 124 */
/* :sswitch_1 */
/* new-instance v0, Landroid/util/Pair; */
v2 = this.values;
/* aget v1, v2, v1 */
java.lang.Float .valueOf ( v1 );
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
java.lang.Long .valueOf ( v2,v3 );
/* invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
this.mLinearAccValues = v0;
/* .line 125 */
/* .line 118 */
/* :sswitch_2 */
v0 = this.values;
this.mGravityValues = v0;
/* .line 119 */
/* .line 127 */
/* :sswitch_3 */
v0 = this.values;
this.mGyroscopeValues = v0;
/* .line 128 */
/* .line 121 */
/* :sswitch_4 */
/* new-instance v0, Landroid/util/Pair; */
v2 = this.values;
/* aget v1, v2, v1 */
java.lang.Float .valueOf ( v1 );
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
java.lang.Long .valueOf ( v2,v3 );
/* invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
this.mAccValues = v0;
/* .line 122 */
/* nop */
/* .line 136 */
} // :goto_0
v0 = this.mGravityValues;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 137 */
/* invoke-direct {p0}, Lcom/android/server/policy/DisplayTurnoverManager;->readyToTurnoverOrHandonByGravity()V */
/* .line 139 */
} // :cond_0
final String v0 = "Do not support Gravity sensor!!!"; // const-string v0, "Do not support Gravity sensor!!!"
final String v1 = "DisplayTurnoverManager"; // const-string v1, "DisplayTurnoverManager"
android.util.Log .w ( v1,v0 );
/* .line 140 */
v0 = this.mSensorDownValues;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 141 */
/* invoke-direct {p0}, Lcom/android/server/policy/DisplayTurnoverManager;->readyToTurnoverOrByScreenDownSensor()V */
/* .line 143 */
} // :cond_1
final String v0 = "Do not support Screen Down sensor!!!"; // const-string v0, "Do not support Screen Down sensor!!!"
android.util.Log .w ( v1,v0 );
/* .line 146 */
} // :goto_1
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_4 */
/* 0x4 -> :sswitch_3 */
/* 0x9 -> :sswitch_2 */
/* 0xa -> :sswitch_1 */
/* 0x1fa265d -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public void switchSubDisplayPowerState ( Boolean p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "isOnSubScreen" # Z */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 110 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* const v0, 0xfffffc */
} // :cond_0
/* const v0, 0xfffffb */
/* .line 111 */
/* .local v0, "code":I */
} // :goto_0
/* invoke-direct {p0, v0, p2}, Lcom/android/server/policy/DisplayTurnoverManager;->turnOnOrOFFSubDisplay(ILjava/lang/String;)V */
/* .line 112 */
return;
} // .end method
public void systemReady ( ) {
/* .locals 5 */
/* .line 104 */
/* new-instance v0, Lcom/android/server/policy/DisplayTurnoverManager$SettingsObserver; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/policy/DisplayTurnoverManager$SettingsObserver;-><init>(Lcom/android/server/policy/DisplayTurnoverManager;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 105 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "subscreen_switch" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
int v3 = -1; // const/4 v3, -0x1
int v4 = 0; // const/4 v4, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v4, v2, v3 ); // invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 106 */
/* invoke-direct {p0}, Lcom/android/server/policy/DisplayTurnoverManager;->updateSubSwitchMode()V */
/* .line 107 */
return;
} // .end method
