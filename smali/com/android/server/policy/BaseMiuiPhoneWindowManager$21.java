class com.android.server.policy.BaseMiuiPhoneWindowManager$21 extends android.content.BroadcastReceiver {
	 /* .source "BaseMiuiPhoneWindowManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.policy.BaseMiuiPhoneWindowManager this$0; //synthetic
/* # direct methods */
 com.android.server.policy.BaseMiuiPhoneWindowManager$21 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
/* .line 3015 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 3017 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 3018 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "com.miui.app.ExtraStatusBarManager.action_enter_drive_mode"; // const-string v1, "com.miui.app.ExtraStatusBarManager.action_enter_drive_mode"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 3019 */
	 v1 = this.this$0;
	 int v2 = 1; // const/4 v2, 0x1
	 com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmForbidFullScreen ( v1,v2 );
	 /* .line 3020 */
} // :cond_0
final String v1 = "com.miui.app.ExtraStatusBarManager.action_leave_drive_mode"; // const-string v1, "com.miui.app.ExtraStatusBarManager.action_leave_drive_mode"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 3021 */
	 v1 = this.this$0;
	 int v2 = 0; // const/4 v2, 0x0
	 com.android.server.policy.BaseMiuiPhoneWindowManager .-$$Nest$fputmForbidFullScreen ( v1,v2 );
	 /* .line 3023 */
} // :cond_1
} // :goto_0
return;
} // .end method
