.class Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;
.super Ljava/lang/Object;
.source "MiuiRestoreManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiRestoreManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TransferDataTask"
.end annotation


# instance fields
.field final callback:Lmiui/app/backup/ITransferDataCallback;

.field final copy:Z

.field final isExternal:Z

.field final src:Ljava/lang/String;

.field final targetBase:Ljava/lang/String;

.field final targetMode:I

.field final targetPkgName:Ljava/lang/String;

.field final targetRelative:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/MiuiRestoreManagerService;

.field final userId:I


# direct methods
.method public constructor <init>(Lcom/android/server/MiuiRestoreManagerService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIZLmiui/app/backup/ITransferDataCallback;)V
    .locals 0
    .param p2, "src"    # Ljava/lang/String;
    .param p3, "targetBase"    # Ljava/lang/String;
    .param p4, "targetRelative"    # Ljava/lang/String;
    .param p5, "targetPkgName"    # Ljava/lang/String;
    .param p6, "targetMode"    # I
    .param p7, "copy"    # Z
    .param p8, "userId"    # I
    .param p9, "isExternal"    # Z
    .param p10, "callback"    # Lmiui/app/backup/ITransferDataCallback;

    .line 492
    iput-object p1, p0, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->this$0:Lcom/android/server/MiuiRestoreManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 493
    iput-object p2, p0, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->src:Ljava/lang/String;

    .line 494
    iput-object p3, p0, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->targetBase:Ljava/lang/String;

    .line 495
    iput-object p4, p0, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->targetRelative:Ljava/lang/String;

    .line 496
    iput-object p5, p0, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->targetPkgName:Ljava/lang/String;

    .line 497
    iput p6, p0, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->targetMode:I

    .line 498
    iput-boolean p7, p0, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->copy:Z

    .line 499
    iput p8, p0, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->userId:I

    .line 500
    iput-boolean p9, p0, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->isExternal:Z

    .line 501
    iput-object p10, p0, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->callback:Lmiui/app/backup/ITransferDataCallback;

    .line 502
    return-void
.end method

.method private parseStructStatFromList(Ljava/util/List;)Landroid/system/StructStat;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/system/StructStat;"
        }
    .end annotation

    .line 573
    .local p1, "stat":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p1

    new-instance v25, Landroid/system/StructStat;

    move-object/from16 v1, v25

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 574
    const/4 v6, 0x2

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    long-to-int v6, v6

    const/4 v7, 0x3

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    const/4 v9, 0x4

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    long-to-int v9, v9

    .line 575
    const/4 v10, 0x5

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    long-to-int v10, v10

    const/4 v11, 0x6

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v11

    const/4 v13, 0x7

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v13

    .line 576
    const/16 v15, 0x8

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v15

    move-object/from16 v26, v1

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v17

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v19

    .line 577
    const/16 v1, 0xb

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v21

    const/16 v1, 0xc

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v23

    move-object/from16 v1, v26

    invoke-direct/range {v1 .. v24}, Landroid/system/StructStat;-><init>(JJIJIIJJJJJJJ)V

    .line 573
    return-object v25
.end method


# virtual methods
.method public run()V
    .locals 18

    .line 508
    move-object/from16 v1, p0

    const-string v2, "MiuiRestoreManagerService"

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 509
    .local v0, "tmpStatList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->this$0:Lcom/android/server/MiuiRestoreManagerService;

    invoke-static {v3}, Lcom/android/server/MiuiRestoreManagerService;->-$$Nest$fgetmInstaller(Lcom/android/server/MiuiRestoreManagerService;)Lcom/android/server/pm/Installer;

    move-result-object v3

    iget-object v4, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->src:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lcom/android/server/pm/Installer;->getDataFileStat(Ljava/lang/String;Ljava/util/List;)I

    move-result v3

    .line 510
    .local v3, "errorCode":I
    if-nez v3, :cond_7

    .line 511
    const/4 v4, 0x0

    .line 512
    .local v4, "preStat":Landroid/system/StructStat;
    iget-boolean v5, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->copy:Z

    if-nez v5, :cond_0

    iget-boolean v5, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->isExternal:Z

    if-eqz v5, :cond_1

    .line 513
    :cond_0
    invoke-direct {v1, v0}, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->parseStructStatFromList(Ljava/util/List;)Landroid/system/StructStat;

    move-result-object v5

    move-object v4, v5

    .line 516
    :cond_1
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v5

    iget-object v6, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->targetPkgName:Ljava/lang/String;

    iget v7, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->userId:I

    const-wide/16 v8, 0x0

    invoke-interface {v5, v6, v8, v9, v7}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    .line 519
    .local v5, "info":Landroid/content/pm/ApplicationInfo;
    if-eqz v5, :cond_6

    .line 522
    iget-object v6, v5, Landroid/content/pm/ApplicationInfo;->seInfo:Ljava/lang/String;

    if-eqz v6, :cond_5

    .line 526
    iget v12, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 529
    .local v12, "uid":I
    iget-boolean v6, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->isExternal:Z

    if-eqz v6, :cond_3

    .line 530
    if-nez v4, :cond_2

    const/4 v6, -0x1

    goto :goto_0

    :cond_2
    iget v6, v4, Landroid/system/StructStat;->st_gid:I

    .line 531
    .local v6, "gid":I
    :goto_0
    const-string v7, ""

    move-object/from16 v17, v7

    .local v7, "seInfo":Ljava/lang/String;
    goto :goto_1

    .line 533
    .end local v6    # "gid":I
    .end local v7    # "seInfo":Ljava/lang/String;
    :cond_3
    iget v6, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 534
    .restart local v6    # "gid":I
    iget-object v7, v5, Landroid/content/pm/ApplicationInfo;->seInfo:Ljava/lang/String;

    move-object/from16 v17, v7

    .line 537
    .local v17, "seInfo":Ljava/lang/String;
    :goto_1
    iget-object v7, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->this$0:Lcom/android/server/MiuiRestoreManagerService;

    invoke-static {v7}, Lcom/android/server/MiuiRestoreManagerService;->-$$Nest$fgetmInstaller(Lcom/android/server/MiuiRestoreManagerService;)Lcom/android/server/pm/Installer;

    move-result-object v7

    iget-object v8, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->src:Ljava/lang/String;

    iget-object v9, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->targetBase:Ljava/lang/String;

    iget-object v10, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->targetRelative:Ljava/lang/String;

    iget-boolean v11, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->copy:Z

    iget v14, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->targetMode:I

    iget-boolean v15, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->isExternal:Z

    move v13, v6

    move/from16 v16, v15

    move-object/from16 v15, v17

    invoke-virtual/range {v7 .. v16}, Lcom/android/server/pm/Installer;->transferData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIIILjava/lang/String;Z)I

    move-result v7

    move v3, v7

    .line 539
    const/4 v7, 0x0

    .line 540
    .local v7, "curStat":Landroid/system/StructStat;
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 541
    if-nez v3, :cond_7

    iget-boolean v8, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->copy:Z

    if-eqz v8, :cond_7

    if-eqz v4, :cond_7

    iget-object v8, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->this$0:Lcom/android/server/MiuiRestoreManagerService;

    invoke-static {v8}, Lcom/android/server/MiuiRestoreManagerService;->-$$Nest$fgetmInstaller(Lcom/android/server/MiuiRestoreManagerService;)Lcom/android/server/pm/Installer;

    move-result-object v8

    iget-object v9, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->src:Ljava/lang/String;

    .line 544
    invoke-virtual {v8, v9, v0}, Lcom/android/server/pm/Installer;->getDataFileStat(Ljava/lang/String;Ljava/util/List;)I

    move-result v8

    if-eqz v8, :cond_7

    .line 545
    invoke-direct {v1, v0}, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->parseStructStatFromList(Ljava/util/List;)Landroid/system/StructStat;

    move-result-object v8

    move-object v7, v8

    if-eqz v8, :cond_7

    iget-wide v8, v4, Landroid/system/StructStat;->st_mtime:J

    iget-wide v10, v7, Landroid/system/StructStat;->st_mtime:J

    cmp-long v8, v8, v10

    if-nez v8, :cond_4

    iget-wide v8, v4, Landroid/system/StructStat;->st_size:J

    iget-wide v10, v7, Landroid/system/StructStat;->st_size:J

    cmp-long v8, v8, v10

    if-nez v8, :cond_4

    goto :goto_2

    .line 547
    :cond_4
    new-instance v8, Lcom/android/server/MiuiRestoreManagerService$FileChangedException;

    const-string v9, "file has been changed"

    invoke-direct {v8, v9}, Lcom/android/server/MiuiRestoreManagerService$FileChangedException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;
    throw v8

    .line 523
    .end local v6    # "gid":I
    .end local v7    # "curStat":Landroid/system/StructStat;
    .end local v12    # "uid":I
    .end local v17    # "seInfo":Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;
    :cond_5
    new-instance v6, Lcom/android/server/MiuiRestoreManagerService$BadSeInfoException;

    const-string/jumbo v7, "se info is null"

    invoke-direct {v6, v7}, Lcom/android/server/MiuiRestoreManagerService$BadSeInfoException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;
    throw v6

    .line 520
    .restart local p0    # "this":Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;
    :cond_6
    new-instance v6, Lcom/android/server/MiuiRestoreManagerService$QueryAppInfoErrorException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "no app info with pkg: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->targetPkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/android/server/MiuiRestoreManagerService$QueryAppInfoErrorException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;
    throw v6
    :try_end_0
    .catch Lcom/android/server/MiuiRestoreManagerService$QueryAppInfoErrorException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/android/server/MiuiRestoreManagerService$BadSeInfoException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/android/server/pm/Installer$InstallerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/server/MiuiRestoreManagerService$FileChangedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 559
    .end local v0    # "tmpStatList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "errorCode":I
    .end local v4    # "preStat":Landroid/system/StructStat;
    .end local v5    # "info":Landroid/content/pm/ApplicationInfo;
    .restart local p0    # "this":Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;
    :catch_0
    move-exception v0

    .line 560
    .local v0, "e":Lcom/android/server/MiuiRestoreManagerService$FileChangedException;
    const/4 v3, -0x7

    .line 561
    .restart local v3    # "errorCode":I
    const-string v4, "file changed "

    invoke-static {v2, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 556
    .end local v0    # "e":Lcom/android/server/MiuiRestoreManagerService$FileChangedException;
    .end local v3    # "errorCode":I
    :catch_1
    move-exception v0

    .line 557
    .local v0, "e":Lcom/android/server/pm/Installer$InstallerException;
    const/4 v3, -0x1

    .line 558
    .restart local v3    # "errorCode":I
    const-string/jumbo v4, "transfer data dir error "

    invoke-static {v2, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Lcom/android/server/pm/Installer$InstallerException;
    goto :goto_2

    .line 553
    .end local v3    # "errorCode":I
    :catch_2
    move-exception v0

    .line 554
    .local v0, "e":Lcom/android/server/MiuiRestoreManagerService$BadSeInfoException;
    const/4 v3, -0x6

    .line 555
    .restart local v3    # "errorCode":I
    const-string/jumbo v4, "se info is null "

    invoke-static {v2, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Lcom/android/server/MiuiRestoreManagerService$BadSeInfoException;
    goto :goto_2

    .line 550
    .end local v3    # "errorCode":I
    :catch_3
    move-exception v0

    .line 551
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, -0x2

    .line 552
    .restart local v3    # "errorCode":I
    const-string v4, "query app info failed "

    invoke-static {v2, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 562
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_7
    :goto_2
    nop

    .line 565
    :goto_3
    :try_start_1
    iget-object v0, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->callback:Lmiui/app/backup/ITransferDataCallback;

    iget-object v4, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->src:Ljava/lang/String;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/CharSequence;

    iget-object v6, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->targetBase:Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    iget-object v6, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->targetRelative:Ljava/lang/String;

    const/4 v7, 0x1

    aput-object v6, v5, v7

    invoke-static {v5}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v3, v4, v5}, Lmiui/app/backup/ITransferDataCallback;->onTransferDataEnd(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_4

    .line 568
    goto :goto_4

    .line 566
    :catch_4
    move-exception v0

    .line 567
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "error when notify onTransferDataEnd "

    invoke-static {v2, v4, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 569
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_4
    return-void
.end method
