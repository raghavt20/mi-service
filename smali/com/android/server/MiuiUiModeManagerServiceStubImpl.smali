.class public Lcom/android/server/MiuiUiModeManagerServiceStubImpl;
.super Lcom/android/server/MiuiUiModeManagerServiceStub;
.source "MiuiUiModeManagerServiceStubImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.MiuiUiModeManagerServiceStub$$"
.end annotation


# static fields
.field private static A:F = 0.0f

.field private static final AUTO_NIGHT_DEFAULT_END_TIME:I = 0x168

.field private static final AUTO_NIGHT_DEFAULT_START_TIME:I = 0x438

.field private static final AUTO_NIGHT_END_TIME:Ljava/lang/String; = "auto_night_end_time"

.field private static final AUTO_NIGHT_START_TIME:Ljava/lang/String; = "auto_night_start_time"

.field private static B:F = 0.0f

.field private static C:F = 0.0f

.field private static final DARK_MODE_ENABLE:Ljava/lang/String; = "dark_mode_enable"

.field private static final DARK_MODE_ENABLE_BY_POWER_SAVE:Ljava/lang/String; = "dark_mode_enable_by_power_save"

.field private static final DARK_MODE_ENABLE_BY_SETTING:Ljava/lang/String; = "dark_mode_enable_by_setting"

.field private static final DARK_MODE_SWITCH_NOW:Ljava/lang/String; = "dark_mode_switch_now"

.field private static final GAMMA_SPACE_MAX:I

.field private static final GAMMA_SPACE_MIN:I = 0x0

.field private static final GET_SUN_TIME_FROM_CLOUD:Ljava/lang/String; = "get_sun_time_from_cloud"

.field private static final IS_JP_KDDI:Z

.field private static final IS_MEXICO_TELCEL:Z

.field private static R:F = 0.0f

.field private static final TAG:Ljava/lang/String; = "MiuiUiModeManagerServiceStubImpl"


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mForceDarkUiModeModeManager:Lcom/android/server/ForceDarkUiModeModeManager;

.field private mUiModeManagerService:Lcom/android/server/UiModeManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/MiuiUiModeManagerServiceStubImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUiModeManagerService(Lcom/android/server/MiuiUiModeManagerServiceStubImpl;)Lcom/android/server/UiModeManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mupdateAlpha(Lcom/android/server/MiuiUiModeManagerServiceStubImpl;Landroid/content/Context;FFF)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->updateAlpha(Landroid/content/Context;FFF)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 35
    nop

    .line 36
    const-string v0, "ro.miui.customized.region"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 35
    const-string v2, "mx_telcel"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->IS_MEXICO_TELCEL:Z

    .line 38
    nop

    .line 39
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 38
    const-string v1, "jp_kd"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->IS_JP_KDDI:Z

    .line 41
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e00f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->GAMMA_SPACE_MAX:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 32
    invoke-direct {p0}, Lcom/android/server/MiuiUiModeManagerServiceStub;-><init>()V

    .line 63
    new-instance v0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$1;-><init>(Lcom/android/server/MiuiUiModeManagerServiceStubImpl;)V

    iput-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    return-void
.end method

.method private calculateMinuteFormat(J)I
    .locals 3
    .param p1, "sunTimeMillis"    # J

    .line 272
    const-string v0, "HH"

    invoke-static {v0, p1, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 273
    .local v0, "sunHour":I
    nop

    .line 274
    const-string v1, "mm"

    invoke-static {v1, p1, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    .line 273
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 275
    .local v1, "sunMinute":I
    mul-int/lit8 v2, v0, 0x3c

    add-int/2addr v2, v1

    .line 276
    .local v2, "sunTime":I
    return v2
.end method

.method private convertLinearToGammaFloat(FFF)I
    .locals 4
    .param p1, "val"    # F
    .param p2, "min"    # F
    .param p3, "max"    # F

    .line 130
    invoke-static {p2, p3, p1}, Landroid/util/MathUtils;->norm(FFF)F

    move-result v0

    const/high16 v1, 0x41400000    # 12.0f

    mul-float/2addr v0, v1

    .line 132
    .local v0, "normalizedVal":F
    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    .line 133
    invoke-static {v0}, Landroid/util/MathUtils;->sqrt(F)F

    move-result v1

    sget v2, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->R:F

    mul-float/2addr v1, v2

    .local v1, "ret":F
    goto :goto_0

    .line 135
    .end local v1    # "ret":F
    :cond_0
    sget v1, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->A:F

    sget v2, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->B:F

    sub-float v2, v0, v2

    invoke-static {v2}, Landroid/util/MathUtils;->log(F)F

    move-result v2

    mul-float/2addr v1, v2

    sget v2, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->C:F

    add-float/2addr v1, v2

    .line 137
    .restart local v1    # "ret":F
    :goto_0
    const/4 v2, 0x0

    sget v3, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->GAMMA_SPACE_MAX:I

    invoke-static {v2, v3, v1}, Landroid/util/MathUtils;->lerp(IIF)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    return v2
.end method

.method private getAlarmInMills(IIZ)J
    .locals 10
    .param p1, "startTime"    # I
    .param p2, "endTime"    # I
    .param p3, "isSunRise"    # Z

    .line 223
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 224
    .local v0, "calendar":Ljava/util/Calendar;
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 225
    .local v2, "day":I
    const/16 v3, 0xb

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 226
    .local v4, "hour":I
    const/16 v5, 0xc

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 227
    .local v6, "minute":I
    mul-int/lit8 v7, v4, 0x3c

    add-int/2addr v7, v6

    .line 228
    .local v7, "currentTime":I
    if-eqz p3, :cond_1

    .line 229
    if-ge v7, p1, :cond_0

    .line 230
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    goto :goto_0

    .line 232
    :cond_0
    add-int/lit8 v8, v2, 0x1

    invoke-virtual {v0, v1, v8}, Ljava/util/Calendar;->set(II)V

    .line 234
    :goto_0
    div-int/lit8 v1, p2, 0x3c

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->set(II)V

    .line 235
    rem-int/lit8 v1, p2, 0x3c

    invoke-virtual {v0, v5, v1}, Ljava/util/Calendar;->set(II)V

    goto :goto_2

    .line 237
    :cond_1
    if-ge v7, p2, :cond_2

    .line 238
    add-int/lit8 v8, v2, -0x1

    invoke-virtual {v0, v1, v8}, Ljava/util/Calendar;->set(II)V

    goto :goto_1

    .line 240
    :cond_2
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 242
    :goto_1
    div-int/lit8 v1, p1, 0x3c

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->set(II)V

    .line 243
    rem-int/lit8 v1, p1, 0x3c

    invoke-virtual {v0, v5, v1}, Ljava/util/Calendar;->set(II)V

    .line 245
    :goto_2
    const/16 v1, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 246
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 247
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    return-wide v8
.end method

.method private getTimeInString(I)Ljava/lang/String;
    .locals 2
    .param p1, "time"    # I

    .line 280
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v1, p1, 0x3c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    rem-int/lit8 v1, p1, 0x3c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private registUIModeScaleChangeObserver(Lcom/android/server/UiModeManagerService;Landroid/content/Context;)V
    .locals 4
    .param p1, "service"    # Lcom/android/server/UiModeManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 151
    new-instance v0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1, p2, p1}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$2;-><init>(Lcom/android/server/MiuiUiModeManagerServiceStubImpl;Landroid/os/Handler;Landroid/content/Context;Lcom/android/server/UiModeManagerService;)V

    .line 164
    .local v0, "uiModeScaleChangedObserver":Landroid/database/ContentObserver;
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 165
    const-string/jumbo v2, "ui_mode_scale"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 164
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 166
    invoke-virtual {v0, v3}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 167
    return-void
.end method

.method private updateAlpha(Landroid/content/Context;FFF)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mBrightness"    # F
    .param p3, "mMaxBrightness"    # F
    .param p4, "mMinBrightness"    # F

    .line 116
    invoke-direct {p0, p2, p4, p3}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->convertLinearToGammaFloat(FFF)I

    move-result v0

    sget v1, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->GAMMA_SPACE_MAX:I

    div-int/2addr v0, v1

    int-to-float v0, v0

    .line 117
    .local v0, "ratio":F
    const-wide/16 v1, 0x0

    .line 118
    .local v1, "alpha":D
    float-to-double v3, v0

    const-wide v5, 0x3fd999999999999aL    # 0.4

    cmpg-double v3, v3, v5

    const-wide v7, 0x3fb999999999999aL    # 0.1

    if-gez v3, :cond_0

    float-to-double v3, v0

    cmpl-double v3, v3, v7

    if-lez v3, :cond_0

    .line 119
    float-to-double v3, v0

    sub-double/2addr v5, v3

    const-wide/high16 v3, 0x4004000000000000L    # 2.5

    div-double/2addr v5, v3

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    .line 121
    :cond_0
    float-to-double v3, v0

    cmpg-double v3, v3, v7

    if-gtz v3, :cond_1

    .line 122
    float-to-double v3, v0

    const-wide v5, 0x3fc17c1bda5119ceL    # 0.1366

    add-double/2addr v3, v5

    const-wide v5, 0x3fe5db22d0e56042L    # 0.683

    div-double v1, v3, v5

    .line 124
    :cond_1
    double-to-float v3, v1

    .line 125
    .local v3, "setalpha":F
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "contrast_alpha"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    .line 126
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 175
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mForceDarkUiModeModeManager:Lcom/android/server/ForceDarkUiModeModeManager;

    invoke-virtual {v0, p1}, Lcom/android/server/ForceDarkUiModeModeManager;->dump(Ljava/io/PrintWriter;)V

    .line 176
    return-void
.end method

.method public getDefaultTwilightState()Lcom/android/server/twilight/TwilightState;
    .locals 7

    .line 211
    const-string v0, "MiuiUiModeManagerServiceStubImpl"

    const-string v1, "cannot get real sunrise and sunset time, return default time or old time"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "auto_night_end_time"

    const/16 v2, 0x168

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 214
    .local v0, "sunRiseTime":I
    iget-object v1, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "auto_night_start_time"

    const/16 v3, 0x438

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 216
    .local v1, "sunSetTime":I
    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->getAlarmInMills(IIZ)J

    move-result-wide v2

    .line 217
    .local v2, "sunSetTimeMillis":J
    const/4 v4, 0x1

    invoke-direct {p0, v1, v0, v4}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->getAlarmInMills(IIZ)J

    move-result-wide v4

    .line 218
    .local v4, "sunRiseTimeMillis":J
    new-instance v6, Lcom/android/server/twilight/TwilightState;

    invoke-direct {v6, v4, v5, v2, v3}, Lcom/android/server/twilight/TwilightState;-><init>(JJ)V

    .line 219
    .local v6, "lastState":Lcom/android/server/twilight/TwilightState;
    return-object v6
.end method

.method public getForceDarkAppDefaultEnable(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 170
    invoke-static {}, Lcom/android/server/ForceDarkAppListProvider;->getInstance()Lcom/android/server/ForceDarkAppListProvider;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/ForceDarkAppListProvider;->getForceDarkAppDefaultEnable(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getSwitchValue()Z
    .locals 3

    .line 205
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "dark_mode_switch_now"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    return v2
.end method

.method public getTwilightState(JJ)Lcom/android/server/twilight/TwilightState;
    .locals 3
    .param p1, "sunriseTimeMillis"    # J
    .param p3, "sunsetTimeMillis"    # J

    .line 264
    invoke-direct {p0, p3, p4}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->calculateMinuteFormat(J)I

    move-result v0

    .line 265
    .local v0, "sunsetTime":I
    invoke-direct {p0, p1, p2}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->calculateMinuteFormat(J)I

    move-result v1

    .line 266
    .local v1, "sunriseTime":I
    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->getAlarmInMills(IIZ)J

    move-result-wide p3

    .line 267
    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->getAlarmInMills(IIZ)J

    move-result-wide p1

    .line 268
    new-instance v2, Lcom/android/server/twilight/TwilightState;

    invoke-direct {v2, p1, p2, p3, p4}, Lcom/android/server/twilight/TwilightState;-><init>(JJ)V

    return-object v2
.end method

.method public init(Lcom/android/server/UiModeManagerService;)V
    .locals 8
    .param p1, "uiModeManagerService"    # Lcom/android/server/UiModeManagerService;

    .line 87
    const-string v0, "MiuiUiModeManagerServiceStubImpl"

    iput-object p1, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    .line 88
    invoke-virtual {p1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    .line 89
    const-class v2, Landroid/hardware/display/DisplayManager;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    iput-object v1, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 90
    iget-object v1, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    .line 91
    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 92
    .local v1, "powerManager":Landroid/os/PowerManager;
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 93
    .local v2, "filter":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 95
    iget-object v3, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1, v3}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->registUIModeScaleChangeObserver(Lcom/android/server/UiModeManagerService;Landroid/content/Context;)V

    .line 96
    iget-object v3, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    iget-object v4, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    const-wide/16 v6, 0x8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;J)V

    .line 99
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mNightMode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/android/server/UiModeManagerService;->getService()Landroid/app/IUiModeManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/app/IUiModeManager;->getNightMode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    goto :goto_0

    .line 100
    :catch_0
    move-exception v3

    .line 101
    .local v3, "e":Landroid/os/RemoteException;
    const-string v4, "Failure communicating with uimode manager"

    invoke-static {v0, v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 104
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_0
    new-instance v0, Lcom/android/server/ForceDarkUiModeModeManager;

    invoke-direct {v0, p1}, Lcom/android/server/ForceDarkUiModeModeManager;-><init>(Lcom/android/server/UiModeManagerService;)V

    iput-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mForceDarkUiModeModeManager:Lcom/android/server/ForceDarkUiModeModeManager;

    .line 105
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x1107001d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v0

    sput v0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->R:F

    .line 107
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x1107001a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v0

    sput v0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->A:F

    .line 109
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x1107001b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v0

    sput v0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->B:F

    .line 111
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x1107001c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v0

    sput v0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->C:F

    .line 113
    return-void
.end method

.method public modifySettingValue(II)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "value"    # I

    .line 186
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 196
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "dark_mode_enable_by_power_save"

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 197
    goto :goto_0

    .line 191
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "dark_mode_switch_now"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 192
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v2, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 188
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "dark_mode_enable_by_setting"

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 189
    nop

    .line 201
    :cond_0
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onBootPhase(I)V
    .locals 1
    .param p1, "phase"    # I

    .line 141
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mForceDarkUiModeModeManager:Lcom/android/server/ForceDarkUiModeModeManager;

    invoke-virtual {v0, p1}, Lcom/android/server/ForceDarkUiModeModeManager;->onBootPhase(I)V

    .line 142
    return-void
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 1
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mForceDarkUiModeModeManager:Lcom/android/server/ForceDarkUiModeModeManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/ForceDarkUiModeModeManager;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0
.end method

.method public setDarkModeStatus(I)V
    .locals 4
    .param p1, "uiMode"    # I

    .line 180
    and-int/lit8 v0, p1, 0x30

    const/16 v1, 0x20

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v3

    .line 181
    .local v0, "isNightMode":Z
    :goto_0
    iget-object v1, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    const-string v3, "dark_mode_enable"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 182
    return-void
.end method

.method public updateSunRiseSetTime(Lcom/android/server/twilight/TwilightState;)V
    .locals 4
    .param p1, "state"    # Lcom/android/server/twilight/TwilightState;

    .line 252
    if-eqz p1, :cond_0

    .line 253
    invoke-virtual {p1}, Lcom/android/server/twilight/TwilightState;->sunsetTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->calculateMinuteFormat(J)I

    move-result v0

    .line 254
    .local v0, "sunsetTime":I
    invoke-virtual {p1}, Lcom/android/server/twilight/TwilightState;->sunriseTimeMillis()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->calculateMinuteFormat(J)I

    move-result v1

    .line 255
    .local v1, "sunriseTime":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "today state: sunrise\uff1a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v1}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->getTimeInString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  sunset\uff1a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 256
    invoke-direct {p0, v0}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->getTimeInString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 255
    const-string v3, "MiuiUiModeManagerServiceStubImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    iget-object v2, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "auto_night_start_time"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 258
    iget-object v2, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "auto_night_end_time"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 260
    .end local v0    # "sunsetTime":I
    .end local v1    # "sunriseTime":I
    :cond_0
    return-void
.end method
