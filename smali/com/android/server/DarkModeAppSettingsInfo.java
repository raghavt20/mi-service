public class com.android.server.DarkModeAppSettingsInfo {
	 /* .source "DarkModeAppSettingsInfo.java" */
	 /* # static fields */
	 public static final Integer OVERRIDE_ENABLE_CLOSE;
	 public static final Integer OVERRIDE_ENABLE_NO;
	 public static final Integer OVERRIDE_ENABLE_OPEN;
	 /* # instance fields */
	 private Integer adaptStat;
	 private Boolean defaultEnable;
	 private Boolean forceDarkOrigin;
	 private Integer forceDarkSplashScreen;
	 private Integer interceptRelaunch;
	 private Integer overrideEnableValue;
	 private java.lang.String packageName;
	 private Boolean showInSettings;
	 /* # direct methods */
	 public com.android.server.DarkModeAppSettingsInfo ( ) {
		 /* .locals 0 */
		 /* .line 7 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Integer getAdaptStat ( ) {
		 /* .locals 1 */
		 /* .line 56 */
		 /* iget v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->adaptStat:I */
	 } // .end method
	 public Integer getForceDarkSplashScreen ( ) {
		 /* .locals 1 */
		 /* .line 80 */
		 /* iget v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->forceDarkSplashScreen:I */
	 } // .end method
	 public Integer getInterceptRelaunch ( ) {
		 /* .locals 1 */
		 /* .line 88 */
		 /* iget v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->interceptRelaunch:I */
	 } // .end method
	 public Integer getOverrideEnableValue ( ) {
		 /* .locals 1 */
		 /* .line 64 */
		 /* iget v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->overrideEnableValue:I */
	 } // .end method
	 public java.lang.String getPackageName ( ) {
		 /* .locals 1 */
		 /* .line 32 */
		 v0 = this.packageName;
	 } // .end method
	 public Boolean isDefaultEnable ( ) {
		 /* .locals 1 */
		 /* .line 48 */
		 /* iget-boolean v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->defaultEnable:Z */
	 } // .end method
	 public Boolean isForceDarkOrigin ( ) {
		 /* .locals 1 */
		 /* .line 72 */
		 /* iget-boolean v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->forceDarkOrigin:Z */
	 } // .end method
	 public Boolean isShowInSettings ( ) {
		 /* .locals 1 */
		 /* .line 40 */
		 /* iget-boolean v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->showInSettings:Z */
	 } // .end method
	 public void setAdaptStat ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "adaptStat" # I */
		 /* .line 60 */
		 /* iput p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->adaptStat:I */
		 /* .line 61 */
		 return;
	 } // .end method
	 public void setDefaultEnable ( Boolean p0 ) {
		 /* .locals 0 */
		 /* .param p1, "defaultEnable" # Z */
		 /* .line 52 */
		 /* iput-boolean p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->defaultEnable:Z */
		 /* .line 53 */
		 return;
	 } // .end method
	 public void setForceDarkOrigin ( Boolean p0 ) {
		 /* .locals 0 */
		 /* .param p1, "forceDarkOrigin" # Z */
		 /* .line 76 */
		 /* iput-boolean p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->forceDarkOrigin:Z */
		 /* .line 77 */
		 return;
	 } // .end method
	 public void setForceDarkSplashScreen ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "forceDarkSplashScreen" # I */
		 /* .line 84 */
		 /* iput p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->forceDarkSplashScreen:I */
		 /* .line 85 */
		 return;
	 } // .end method
	 public void setInterceptRelaunch ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "interceptRelaunch" # I */
		 /* .line 92 */
		 /* iput p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->interceptRelaunch:I */
		 /* .line 93 */
		 return;
	 } // .end method
	 public void setOverrideEnableValue ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "overrideEnableValue" # I */
		 /* .line 68 */
		 /* iput p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->overrideEnableValue:I */
		 /* .line 69 */
		 return;
	 } // .end method
	 public void setPackageName ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "packageName" # Ljava/lang/String; */
		 /* .line 36 */
		 this.packageName = p1;
		 /* .line 37 */
		 return;
	 } // .end method
	 public void setShowInSettings ( Boolean p0 ) {
		 /* .locals 0 */
		 /* .param p1, "showInSettings" # Z */
		 /* .line 44 */
		 /* iput-boolean p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->showInSettings:Z */
		 /* .line 45 */
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 2 */
		 /* .line 97 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "DarkModeAppSettingsInfo{packageName=\'"; // const-string v1, "DarkModeAppSettingsInfo{packageName=\'"
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v1 = this.packageName;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x27 */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 final String v1 = ", showInSettings="; // const-string v1, ", showInSettings="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-boolean v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->showInSettings:Z */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 final String v1 = ", defaultEnable="; // const-string v1, ", defaultEnable="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-boolean v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->defaultEnable:Z */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 final String v1 = ", overrideEnableValue="; // const-string v1, ", overrideEnableValue="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->overrideEnableValue:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", forceDarkOrigin="; // const-string v1, ", forceDarkOrigin="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-boolean v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->forceDarkOrigin:Z */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 final String v1 = ", adaptStat="; // const-string v1, ", adaptStat="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->adaptStat:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", forceDarkSplashScreen="; // const-string v1, ", forceDarkSplashScreen="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->forceDarkSplashScreen:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", interceptRelaunch="; // const-string v1, ", interceptRelaunch="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->interceptRelaunch:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x7d */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
