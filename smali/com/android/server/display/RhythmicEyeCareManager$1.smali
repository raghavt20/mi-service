.class Lcom/android/server/display/RhythmicEyeCareManager$1;
.super Ljava/lang/Object;
.source "RhythmicEyeCareManager.java"

# interfaces
.implements Landroid/app/AlarmManager$OnAlarmListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/RhythmicEyeCareManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/RhythmicEyeCareManager;


# direct methods
.method public static synthetic $r8$lambda$3zmkR-0cMMkM7n9X9UZR-clevqg(Lcom/android/server/display/RhythmicEyeCareManager$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager$1;->lambda$onAlarm$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/display/RhythmicEyeCareManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/RhythmicEyeCareManager;

    .line 61
    iput-object p1, p0, Lcom/android/server/display/RhythmicEyeCareManager$1;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synthetic lambda$onAlarm$0()V
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager$1;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$mupdateState(Lcom/android/server/display/RhythmicEyeCareManager;Z)V

    return-void
.end method


# virtual methods
.method public onAlarm()V
    .locals 3

    .line 64
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager$1;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-static {v0}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$fgetmHandler(Lcom/android/server/display/RhythmicEyeCareManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/display/RhythmicEyeCareManager$1$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/display/RhythmicEyeCareManager$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/RhythmicEyeCareManager$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 65
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager$1;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-static {v0}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$fgetmAlarmIndex(Lcom/android/server/display/RhythmicEyeCareManager;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/android/server/display/RhythmicEyeCareManager$1;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-static {v2}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$fgetmAlarmTimePoints(Lcom/android/server/display/RhythmicEyeCareManager;)[I

    move-result-object v2

    array-length v2, v2

    rem-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$fputmAlarmIndex(Lcom/android/server/display/RhythmicEyeCareManager;I)V

    .line 66
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager$1;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-static {v0}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$msetAlarm(Lcom/android/server/display/RhythmicEyeCareManager;)V

    .line 67
    return-void
.end method
