.class public Lcom/android/server/display/SwipeUpWindow;
.super Ljava/lang/Object;
.source "SwipeUpWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/SwipeUpWindow$SwipeUpWindowHandler;,
        Lcom/android/server/display/SwipeUpWindow$AnimationState;,
        Lcom/android/server/display/SwipeUpWindow$SwipeUpFrameLayout;,
        Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;
    }
.end annotation


# static fields
.field private static final BG_INIT_ALPHA:F = 0.6f

.field private static final DEBUG:Z = false

.field private static final DEFAULT_DAMPING:F = 0.95f

.field private static final DEFAULT_STIFFNESS:F = 322.27f

.field private static final DISTANCE_DEBOUNCE:F = 200.0f

.field private static final ICON_HEIGHT:I = 0x1c

.field private static final ICON_OFFSET:F = 30.0f

.field private static final ICON_TIP_SHOW_DAMPING:F = 1.0f

.field private static final ICON_TIP_SHOW_STIFFNESS:F = 39.478416f

.field private static final ICON_VIEW_BOTTOM:F = 117.0f

.field private static final ICON_WIDTH:I = 0x1c

.field private static final LOCK_SATE_START_DELAY:I = 0x3e8

.field private static final LOCK_STATE_LONG_DAMPING:F = 1.0f

.field private static final LOCK_STATE_LONG_STIFFNESS:F = 6.86f

.field private static final MSG_ICON_LOCK_ANIMATION:I = 0x66

.field private static final MSG_ICON_TIP_HIDE_ANIMATION:I = 0x67

.field private static final MSG_LOCK_STATE_WITH_LONG_ANIMATION:I = 0x65

.field private static final MSG_RELEASE_WINDOW:I = 0x69

.field private static final MSG_SCREEN_OFF:I = 0x68

.field private static final SCREEN_HEIGHT:I = 0x9d8

.field private static final SCREEN_OFF_DELAY:I = 0x1770

.field private static final SCREEN_WIDTH:I = 0x438

.field private static final SCROLL_DAMPING:F = 0.9f

.field private static final SCROLL_STIFFNESS:F = 986.96045f

.field private static final SHOW_STATE_DAMPING:F = 1.0f

.field private static final SHOW_STATE_STIFFNESS:F = 157.91367f

.field public static final TAG:Ljava/lang/String; = "SwipeUpWindow"

.field private static final TIP_HEIGHT:I = 0x19

.field private static final TIP_INIT_ALPHA:F = 0.4f

.field private static final TIP_OFFSET:F = 50.0f

.field private static final TIP_TEXT_SIZE:I = 0x13

.field private static final TIP_VIEW_BOTTOM:F = 63.0f


# instance fields
.field private mAnimationState:Lcom/android/server/display/SwipeUpWindow$AnimationState;

.field private mBlackLinearGradientView:Lcom/android/server/display/BlackLinearGradientView;

.field private mContext:Landroid/content/Context;

.field private mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

.field private mHandler:Landroid/os/Handler;

.field private mIconDrawable:Landroid/graphics/drawable/AnimatedVectorDrawable;

.field private mIconSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

.field private mIconView:Landroid/widget/ImageView;

.field private mLockStateLongAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

.field private mLockStateShortAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

.field private mOnTouchListener:Landroid/view/View$OnTouchListener;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPreBlurLevel:I

.field private mPreBottomColor:I

.field private mPreOnAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

.field private mPreTopColor:I

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mScrollAnimationNeedInit:Z

.field private mStartPer:F

.field private mSwipeUpFrameLayout:Landroid/widget/FrameLayout;

.field private mSwipeUpLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mSwipeUpWindowShowing:Z

.field private mTipSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

.field private mTipView:Landroid/widget/TextView;

.field private mUnlockDistance:F

.field private mUnlockStateAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mWakeStateAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAnimationState(Lcom/android/server/display/SwipeUpWindow;)Lcom/android/server/display/SwipeUpWindow$AnimationState;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/SwipeUpWindow;->mAnimationState:Lcom/android/server/display/SwipeUpWindow$AnimationState;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmGradientShadowSpringAnimation(Lcom/android/server/display/SwipeUpWindow;)Lcom/android/server/display/animation/SpringAnimation;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/display/SwipeUpWindow;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/SwipeUpWindow;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIconSpringAnimation(Lcom/android/server/display/SwipeUpWindow;)Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIconView(Lcom/android/server/display/SwipeUpWindow;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmScreenHeight(Lcom/android/server/display/SwipeUpWindow;)I
    .locals 0

    iget p0, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmScrollAnimationNeedInit(Lcom/android/server/display/SwipeUpWindow;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/SwipeUpWindow;->mScrollAnimationNeedInit:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmStartPer(Lcom/android/server/display/SwipeUpWindow;)F
    .locals 0

    iget p0, p0, Lcom/android/server/display/SwipeUpWindow;->mStartPer:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTipSpringAnimation(Lcom/android/server/display/SwipeUpWindow;)Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/SwipeUpWindow;->mTipSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTipView(Lcom/android/server/display/SwipeUpWindow;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUnlockDistance(Lcom/android/server/display/SwipeUpWindow;)F
    .locals 0

    iget p0, p0, Lcom/android/server/display/SwipeUpWindow;->mUnlockDistance:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmVelocityTracker(Lcom/android/server/display/SwipeUpWindow;)Landroid/view/VelocityTracker;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/SwipeUpWindow;->mVelocityTracker:Landroid/view/VelocityTracker;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIconSpringAnimation(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/display/SwipeUpWindow;->mIconSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmScrollAnimationNeedInit(Lcom/android/server/display/SwipeUpWindow;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/SwipeUpWindow;->mScrollAnimationNeedInit:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmStartPer(Lcom/android/server/display/SwipeUpWindow;F)V
    .locals 0

    iput p1, p0, Lcom/android/server/display/SwipeUpWindow;->mStartPer:F

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTipSpringAnimation(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/display/SwipeUpWindow;->mTipSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmVelocityTracker(Lcom/android/server/display/SwipeUpWindow;Landroid/view/VelocityTracker;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/display/SwipeUpWindow;->mVelocityTracker:Landroid/view/VelocityTracker;

    return-void
.end method

.method static bridge synthetic -$$Nest$mcreatSpringAnimation(Lcom/android/server/display/SwipeUpWindow;Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation;
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleScreenOff(Lcom/android/server/display/SwipeUpWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->handleScreenOff()V

    return-void
.end method

.method static bridge synthetic -$$Nest$minitVelocityTrackerIfNotExists(Lcom/android/server/display/SwipeUpWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->initVelocityTrackerIfNotExists()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mplayIconAndTipHideAnimation(Lcom/android/server/display/SwipeUpWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->playIconAndTipHideAnimation()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mplayIconAnimation(Lcom/android/server/display/SwipeUpWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->playIconAnimation()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mrecycleVelocityTracker(Lcom/android/server/display/SwipeUpWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->recycleVelocityTracker()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mresetIconAnimation(Lcom/android/server/display/SwipeUpWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->resetIconAnimation()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetLockStateWithLongAnimation(Lcom/android/server/display/SwipeUpWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->setLockStateWithLongAnimation()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetLockStateWithShortAnimation(Lcom/android/server/display/SwipeUpWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->setLockStateWithShortAnimation()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetUnlockState(Lcom/android/server/display/SwipeUpWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->setUnlockState()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetWakeState(Lcom/android/server/display/SwipeUpWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->setWakeState()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartGradientShadowAnimation(Lcom/android/server/display/SwipeUpWindow;FFF)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/SwipeUpWindow;->startGradientShadowAnimation(FFF)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartSwipeUpAnimation(Lcom/android/server/display/SwipeUpWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->startSwipeUpAnimation()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateBlackView(Lcom/android/server/display/SwipeUpWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->updateBlackView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mPreTopColor:I

    .line 120
    iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBottomColor:I

    .line 121
    iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBlurLevel:I

    .line 122
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mStartPer:F

    .line 525
    new-instance v0, Lcom/android/server/display/SwipeUpWindow$3;

    invoke-direct {v0, p0}, Lcom/android/server/display/SwipeUpWindow$3;-><init>(Lcom/android/server/display/SwipeUpWindow;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mWakeStateAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

    .line 539
    new-instance v0, Lcom/android/server/display/SwipeUpWindow$4;

    invoke-direct {v0, p0}, Lcom/android/server/display/SwipeUpWindow$4;-><init>(Lcom/android/server/display/SwipeUpWindow;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mLockStateLongAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

    .line 554
    new-instance v0, Lcom/android/server/display/SwipeUpWindow$5;

    invoke-direct {v0, p0}, Lcom/android/server/display/SwipeUpWindow$5;-><init>(Lcom/android/server/display/SwipeUpWindow;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mLockStateShortAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

    .line 566
    new-instance v0, Lcom/android/server/display/SwipeUpWindow$6;

    invoke-direct {v0, p0}, Lcom/android/server/display/SwipeUpWindow$6;-><init>(Lcom/android/server/display/SwipeUpWindow;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mUnlockStateAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

    .line 578
    new-instance v0, Lcom/android/server/display/SwipeUpWindow$7;

    invoke-direct {v0, p0}, Lcom/android/server/display/SwipeUpWindow$7;-><init>(Lcom/android/server/display/SwipeUpWindow;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 130
    const v0, 0x7fffffff

    iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I

    .line 131
    iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenWidth:I

    .line 132
    iput-object p1, p0, Lcom/android/server/display/SwipeUpWindow;->mContext:Landroid/content/Context;

    .line 133
    new-instance v0, Lcom/android/server/display/SwipeUpWindow$SwipeUpWindowHandler;

    invoke-direct {v0, p0, p2}, Lcom/android/server/display/SwipeUpWindow$SwipeUpWindowHandler;-><init>(Lcom/android/server/display/SwipeUpWindow;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mHandler:Landroid/os/Handler;

    .line 134
    new-instance v0, Lcom/android/server/display/SwipeUpWindow$AnimationState;

    invoke-direct {v0, p0}, Lcom/android/server/display/SwipeUpWindow$AnimationState;-><init>(Lcom/android/server/display/SwipeUpWindow;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mAnimationState:Lcom/android/server/display/SwipeUpWindow$AnimationState;

    .line 136
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mWindowManager:Landroid/view/WindowManager;

    .line 137
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mPowerManager:Landroid/os/PowerManager;

    .line 139
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/display/SwipeUpWindow;->updateSettings(Z)V

    .line 140
    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->updateSizeInfo()V

    .line 141
    return-void
.end method

.method private calculateBlackAlpha(F)I
    .locals 2
    .param p1, "alpha"    # F

    .line 423
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 425
    .local v0, "blackAlpha":I
    shl-int/lit8 v1, v0, 0x18

    or-int/lit8 v1, v1, 0x0

    return v1
.end method

.method private constraintAlpha(F)F
    .locals 2
    .param p1, "alpha"    # F

    .line 455
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v0

    if-lez v1, :cond_0

    :goto_0
    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    cmpg-float v1, p1, v0

    if-gez v1, :cond_1

    goto :goto_0

    :cond_1
    move v0, p1

    :goto_1
    return v0
.end method

.method private constraintBlur(F)F
    .locals 2
    .param p1, "level"    # F

    .line 459
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v0

    if-lez v1, :cond_0

    :goto_0
    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    cmpg-float v1, p1, v0

    if-gez v1, :cond_1

    goto :goto_0

    :cond_1
    move v0, p1

    :goto_1
    return v0
.end method

.method private creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;F)Lcom/android/server/display/animation/SpringAnimation;
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "viewProperty"    # Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;
    .param p3, "finalPosition"    # F

    .line 782
    const v3, 0x43a1228f

    const v4, 0x3f733333    # 0.95f

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object v0

    return-object v0
.end method

.method private creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation;
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "viewProperty"    # Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;
    .param p3, "stiffness"    # F
    .param p4, "damping"    # F
    .param p5, "finalPosition"    # F

    .line 789
    new-instance v0, Lcom/android/server/display/animation/SpringAnimation;

    invoke-direct {v0, p1, p2}, Lcom/android/server/display/animation/SpringAnimation;-><init>(Ljava/lang/Object;Lcom/android/server/display/animation/FloatPropertyCompat;)V

    .line 790
    .local v0, "springAnimation":Lcom/android/server/display/animation/SpringAnimation;
    new-instance v1, Lcom/android/server/display/animation/SpringForce;

    invoke-direct {v1, p5}, Lcom/android/server/display/animation/SpringForce;-><init>(F)V

    .line 791
    .local v1, "springForce":Lcom/android/server/display/animation/SpringForce;
    invoke-virtual {v1, p3}, Lcom/android/server/display/animation/SpringForce;->setStiffness(F)Lcom/android/server/display/animation/SpringForce;

    .line 792
    invoke-virtual {v1, p4}, Lcom/android/server/display/animation/SpringForce;->setDampingRatio(F)Lcom/android/server/display/animation/SpringForce;

    .line 793
    invoke-virtual {v0, v1}, Lcom/android/server/display/animation/SpringAnimation;->setSpring(Lcom/android/server/display/animation/SpringForce;)Lcom/android/server/display/animation/SpringAnimation;

    .line 794
    const/high16 v2, 0x3b800000    # 0.00390625f

    invoke-virtual {v0, v2}, Lcom/android/server/display/animation/SpringAnimation;->setMinimumVisibleChange(F)Lcom/android/server/display/animation/DynamicAnimation;

    .line 795
    return-object v0
.end method

.method private handleScreenOff()V
    .locals 5

    .line 486
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 487
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/PowerManager;->goToSleep(JII)V

    .line 489
    return-void
.end method

.method private initBlackLinearGradientView()V
    .locals 3

    .line 239
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/16 v2, 0x11

    invoke-direct {v0, v1, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 243
    .local v0, "backViewParams":Landroid/widget/FrameLayout$LayoutParams;
    new-instance v1, Lcom/android/server/display/BlackLinearGradientView;

    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/android/server/display/BlackLinearGradientView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mBlackLinearGradientView:Lcom/android/server/display/BlackLinearGradientView;

    .line 244
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/server/display/BlackLinearGradientView;->setBackgroundResource(I)V

    .line 245
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpFrameLayout:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mBlackLinearGradientView:Lcom/android/server/display/BlackLinearGradientView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 246
    return-void
.end method

.method private initGradientShadowAnimation()V
    .locals 4

    .line 287
    new-instance v0, Lcom/android/server/display/animation/SpringAnimation;

    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mAnimationState:Lcom/android/server/display/SwipeUpWindow$AnimationState;

    new-instance v2, Lcom/android/server/display/SwipeUpWindow$1;

    const-string v3, "perState"

    invoke-direct {v2, p0, v3}, Lcom/android/server/display/SwipeUpWindow$1;-><init>(Lcom/android/server/display/SwipeUpWindow;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcom/android/server/display/animation/SpringAnimation;-><init>(Ljava/lang/Object;Lcom/android/server/display/animation/FloatPropertyCompat;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    .line 300
    new-instance v0, Lcom/android/server/display/animation/SpringForce;

    invoke-direct {v0}, Lcom/android/server/display/animation/SpringForce;-><init>()V

    .line 301
    .local v0, "springForce":Lcom/android/server/display/animation/SpringForce;
    const v1, 0x43a1228f

    invoke-virtual {v0, v1}, Lcom/android/server/display/animation/SpringForce;->setStiffness(F)Lcom/android/server/display/animation/SpringForce;

    .line 302
    const v1, 0x3f733333    # 0.95f

    invoke-virtual {v0, v1}, Lcom/android/server/display/animation/SpringForce;->setDampingRatio(F)Lcom/android/server/display/animation/SpringForce;

    .line 303
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v1, v0}, Lcom/android/server/display/animation/SpringAnimation;->setSpring(Lcom/android/server/display/animation/SpringForce;)Lcom/android/server/display/animation/SpringAnimation;

    .line 304
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    const/high16 v2, 0x3b800000    # 0.00390625f

    invoke-virtual {v1, v2}, Lcom/android/server/display/animation/SpringAnimation;->setMinimumVisibleChange(F)Lcom/android/server/display/animation/DynamicAnimation;

    .line 307
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    new-instance v2, Lcom/android/server/display/SwipeUpWindow$2;

    invoke-direct {v2, p0}, Lcom/android/server/display/SwipeUpWindow$2;-><init>(Lcom/android/server/display/SwipeUpWindow;)V

    invoke-virtual {v1, v2}, Lcom/android/server/display/animation/SpringAnimation;->addUpdateListener(Lcom/android/server/display/animation/DynamicAnimation$OnAnimationUpdateListener;)Lcom/android/server/display/animation/DynamicAnimation;

    .line 314
    return-void
.end method

.method private initIconView()V
    .locals 5

    .line 249
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    .line 250
    const v1, 0x1108019b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 251
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 252
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 253
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 254
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 255
    const/high16 v2, 0x41e00000    # 28.0f

    invoke-virtual {p0, v2}, Lcom/android/server/display/SwipeUpWindow;->transformDpToPx(F)I

    move-result v3

    .line 256
    invoke-virtual {p0, v2}, Lcom/android/server/display/SwipeUpWindow;->transformDpToPx(F)I

    move-result v2

    const/16 v4, 0x31

    invoke-direct {v0, v3, v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 258
    .local v0, "iconViewParams":Landroid/widget/FrameLayout$LayoutParams;
    iget v2, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I

    const/high16 v3, 0x43110000    # 145.0f

    invoke-virtual {p0, v3}, Lcom/android/server/display/SwipeUpWindow;->transformDpToPx(F)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 259
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpFrameLayout:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 261
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    instance-of v1, v1, Landroid/graphics/drawable/AnimatedVectorDrawable;

    if-eqz v1, :cond_0

    .line 262
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/AnimatedVectorDrawable;

    iput-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mIconDrawable:Landroid/graphics/drawable/AnimatedVectorDrawable;

    goto :goto_0

    .line 264
    :cond_0
    const-string v1, "SwipeUpWindow"

    const-string v2, "icon drawable get incompatible class"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :goto_0
    return-void
.end method

.method private initSwipeUpWindow()V
    .locals 10

    .line 210
    new-instance v0, Lcom/android/server/display/SwipeUpWindow$SwipeUpFrameLayout;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mContext:Landroid/content/Context;

    const v3, 0x1030007

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, p0, v1}, Lcom/android/server/display/SwipeUpWindow$SwipeUpFrameLayout;-><init>(Lcom/android/server/display/SwipeUpWindow;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpFrameLayout:Landroid/widget/FrameLayout;

    .line 212
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x1

    const/16 v7, 0x7ea

    const v8, 0x1820104

    const/4 v9, -0x3

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 222
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    .line 224
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, 0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    .line 225
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const v1, 0x800033

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 226
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const-string v1, "SwipeUpWindow"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 227
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    .line 228
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpFrameLayout:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 229
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLongClickable(Z)V

    .line 230
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    .line 232
    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->initBlackLinearGradientView()V

    .line 233
    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->initIconView()V

    .line 234
    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->initTipView()V

    .line 235
    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->initGradientShadowAnimation()V

    .line 236
    return-void
.end method

.method private initTipView()V
    .locals 5

    .line 269
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    .line 270
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 271
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    const v1, 0x110f03b1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 272
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v2, 0x41980000    # 19.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 273
    const-string v0, "mipro-medium"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 274
    .local v0, "typeface":Landroid/graphics/Typeface;
    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 275
    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    const v3, 0x66ffffff

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 276
    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 277
    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 278
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x2

    const/16 v4, 0x31

    invoke-direct {v2, v3, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 282
    .local v2, "tipViewParams":Landroid/widget/FrameLayout$LayoutParams;
    iget v3, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I

    const/high16 v4, 0x42b00000    # 88.0f

    invoke-virtual {p0, v4}, Lcom/android/server/display/SwipeUpWindow;->transformDpToPx(F)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v2, v1, v3, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 283
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpFrameLayout:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    invoke-virtual {v1, v3, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 284
    return-void
.end method

.method private initVelocityTrackerIfNotExists()V
    .locals 1

    .line 677
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 678
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 680
    :cond_0
    return-void
.end method

.method private playIconAndTipHideAnimation()V
    .locals 5

    .line 760
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    if-eqz v0, :cond_0

    .line 761
    invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->cancel()V

    .line 763
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mTipSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    if-eqz v0, :cond_1

    .line 764
    invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->cancel()V

    .line 767
    :cond_1
    new-instance v0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    sget-object v2, Lcom/android/server/display/animation/SpringAnimation;->Y:Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;

    iget-object v3, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    .line 768
    invoke-virtual {v3}, Landroid/widget/ImageView;->getTop()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;F)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    sget-object v3, Lcom/android/server/display/animation/SpringAnimation;->ALPHA:Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;

    .line 769
    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;F)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;-><init>(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/animation/SpringAnimation;Lcom/android/server/display/animation/SpringAnimation;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    .line 771
    new-instance v0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    sget-object v2, Lcom/android/server/display/animation/SpringAnimation;->Y:Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;

    iget-object v3, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    .line 772
    invoke-virtual {v3}, Landroid/widget/TextView;->getTop()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;F)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    sget-object v3, Lcom/android/server/display/animation/SpringAnimation;->ALPHA:Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;

    .line 773
    invoke-direct {p0, v2, v3, v4}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;F)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;-><init>(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/animation/SpringAnimation;Lcom/android/server/display/animation/SpringAnimation;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mTipSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    .line 775
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->start()V

    .line 776
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mTipSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->start()V

    .line 777
    return-void
.end method

.method private playIconAndTipShowAnimation()V
    .locals 8

    .line 736
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    if-eqz v0, :cond_0

    .line 737
    invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->cancel()V

    .line 739
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mTipSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    if-eqz v0, :cond_1

    .line 740
    invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->cancel()V

    .line 743
    :cond_1
    new-instance v0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    sget-object v3, Lcom/android/server/display/animation/SpringAnimation;->Y:Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;

    const v4, 0x421de9e6

    const/high16 v5, 0x3f800000    # 1.0f

    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    .line 745
    invoke-virtual {v1}, Landroid/widget/ImageView;->getTop()I

    move-result v1

    int-to-float v6, v1

    .line 744
    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    sget-object v4, Lcom/android/server/display/animation/SpringAnimation;->ALPHA:Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;

    const v5, 0x421de9e6

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v7, 0x3f800000    # 1.0f

    .line 746
    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;-><init>(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/animation/SpringAnimation;Lcom/android/server/display/animation/SpringAnimation;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    .line 749
    new-instance v0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    sget-object v3, Lcom/android/server/display/animation/SpringAnimation;->Y:Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;

    const v4, 0x421de9e6

    const/high16 v5, 0x3f800000    # 1.0f

    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    .line 751
    invoke-virtual {v1}, Landroid/widget/TextView;->getTop()I

    move-result v1

    int-to-float v6, v1

    .line 750
    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    sget-object v4, Lcom/android/server/display/animation/SpringAnimation;->ALPHA:Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;

    const v5, 0x421de9e6

    const/high16 v6, 0x3f800000    # 1.0f

    .line 752
    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;-><init>(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/animation/SpringAnimation;Lcom/android/server/display/animation/SpringAnimation;)V

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mTipSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    .line 755
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->start()V

    .line 756
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mTipSpringAnimation:Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->start()V

    .line 757
    return-void
.end method

.method private playIconAnimation()V
    .locals 2

    .line 702
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconDrawable:Landroid/graphics/drawable/AnimatedVectorDrawable;

    if-nez v0, :cond_0

    .line 703
    return-void

    .line 706
    :cond_0
    new-instance v1, Lcom/android/server/display/SwipeUpWindow$8;

    invoke-direct {v1, p0}, Lcom/android/server/display/SwipeUpWindow$8;-><init>(Lcom/android/server/display/SwipeUpWindow;)V

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/AnimatedVectorDrawable;->registerAnimationCallback(Landroid/graphics/drawable/Animatable2$AnimationCallback;)V

    .line 714
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconDrawable:Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->start()V

    .line 715
    return-void
.end method

.method private prepareIconAndTipAnimation()V
    .locals 4

    .line 728
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTop()I

    move-result v1

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {p0, v2}, Lcom/android/server/display/SwipeUpWindow;->transformDpToPx(F)I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setY(F)V

    .line 729
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 731
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTop()I

    move-result v2

    const/high16 v3, 0x42480000    # 50.0f

    invoke-virtual {p0, v3}, Lcom/android/server/display/SwipeUpWindow;->transformDpToPx(F)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setY(F)V

    .line 732
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mTipView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 733
    return-void
.end method

.method private recycleVelocityTracker()V
    .locals 1

    .line 683
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 684
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 685
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 687
    :cond_0
    return-void
.end method

.method private resetIconAnimation()V
    .locals 1

    .line 718
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconDrawable:Landroid/graphics/drawable/AnimatedVectorDrawable;

    if-nez v0, :cond_0

    .line 719
    return-void

    .line 722
    :cond_0
    nop

    .line 723
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconDrawable:Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->reset()V

    .line 725
    return-void
.end method

.method private setBackgroudBlur(I)V
    .locals 3
    .param p1, "blurRadius"    # I

    .line 433
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpFrameLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_2

    iget v1, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBlurLevel:I

    if-ne v1, p1, :cond_0

    goto :goto_0

    .line 440
    :cond_0
    iput p1, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBlurLevel:I

    .line 442
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getViewRootImpl()Landroid/view/ViewRootImpl;

    move-result-object v0

    .line 443
    .local v0, "viewRootImpl":Landroid/view/ViewRootImpl;
    if-nez v0, :cond_1

    .line 444
    const-string v1, "SwipeUpWindow"

    const-string v2, "mViewRootImpl is null"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    return-void

    .line 447
    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewRootImpl;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v1

    .line 448
    .local v1, "surfaceControl":Landroid/view/SurfaceControl;
    new-instance v2, Landroid/view/SurfaceControl$Transaction;

    invoke-direct {v2}, Landroid/view/SurfaceControl$Transaction;-><init>()V

    .line 449
    .local v2, "transaction":Landroid/view/SurfaceControl$Transaction;
    invoke-virtual {v2, v1, p1}, Landroid/view/SurfaceControl$Transaction;->setBackgroundBlurRadius(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;

    .line 450
    invoke-virtual {v2, v1}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 451
    invoke-virtual {v2}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 452
    return-void

    .line 434
    .end local v0    # "viewRootImpl":Landroid/view/ViewRootImpl;
    .end local v1    # "surfaceControl":Landroid/view/SurfaceControl;
    .end local v2    # "transaction":Landroid/view/SurfaceControl$Transaction;
    :cond_2
    :goto_0
    return-void
.end method

.method private setLockStateWithLongAnimation()V
    .locals 6

    .line 348
    const v1, 0x40db851f    # 6.86f

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v4, p0, Lcom/android/server/display/SwipeUpWindow;->mLockStateLongAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

    const/high16 v5, 0x3b800000    # 0.00390625f

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/SwipeUpWindow;->startGradientShadowAnimation(FFFLcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;F)V

    .line 351
    return-void
.end method

.method private setLockStateWithShortAnimation()V
    .locals 6

    .line 354
    const v1, 0x43a1228f

    const v2, 0x3f733333    # 0.95f

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v4, p0, Lcom/android/server/display/SwipeUpWindow;->mLockStateShortAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

    const/high16 v5, 0x3b800000    # 0.00390625f

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/SwipeUpWindow;->startGradientShadowAnimation(FFFLcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;F)V

    .line 357
    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->playIconAndTipHideAnimation()V

    .line 358
    return-void
.end method

.method private setUnlockState()V
    .locals 8

    .line 361
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 362
    const v3, 0x43a1228f

    const v4, 0x3f733333    # 0.95f

    const/high16 v5, -0x40800000    # -1.0f

    iget-object v6, p0, Lcom/android/server/display/SwipeUpWindow;->mUnlockStateAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

    const/high16 v7, 0x3f800000    # 1.0f

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/server/display/SwipeUpWindow;->startGradientShadowAnimation(FFFLcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;F)V

    .line 365
    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->playIconAndTipHideAnimation()V

    .line 366
    return-void
.end method

.method private setWakeState()V
    .locals 6

    .line 335
    const v1, 0x431de9e6

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/server/display/SwipeUpWindow;->mWakeStateAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

    const/high16 v5, 0x3b800000    # 0.00390625f

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/SwipeUpWindow;->startGradientShadowAnimation(FFFLcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;F)V

    .line 339
    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->resetIconAnimation()V

    .line 340
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAlpha()F

    move-result v0

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 341
    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->prepareIconAndTipAnimation()V

    .line 344
    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->playIconAndTipShowAnimation()V

    .line 345
    return-void
.end method

.method private startGradientShadowAnimation(F)V
    .locals 2
    .param p1, "finalPosition"    # F

    .line 369
    const v0, 0x43a1228f

    const v1, 0x3f733333    # 0.95f

    invoke-direct {p0, v0, v1, p1}, Lcom/android/server/display/SwipeUpWindow;->startGradientShadowAnimation(FFF)V

    .line 370
    return-void
.end method

.method private startGradientShadowAnimation(FFF)V
    .locals 6
    .param p1, "stiffness"    # F
    .param p2, "dampingRatio"    # F
    .param p3, "finalPosition"    # F

    .line 374
    const/4 v4, 0x0

    const/high16 v5, 0x3b800000    # 0.00390625f

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/SwipeUpWindow;->startGradientShadowAnimation(FFFLcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;F)V

    .line 376
    return-void
.end method

.method private startGradientShadowAnimation(FFFLcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;F)V
    .locals 3
    .param p1, "stiffness"    # F
    .param p2, "dampingRatio"    # F
    .param p3, "finalPosition"    # F
    .param p4, "onAnimationEndListener"    # Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;
    .param p5, "minimumVisibleChange"    # F

    .line 381
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v0}, Lcom/android/server/display/animation/SpringAnimation;->cancel()V

    .line 382
    new-instance v0, Lcom/android/server/display/animation/SpringForce;

    invoke-direct {v0}, Lcom/android/server/display/animation/SpringForce;-><init>()V

    .line 383
    .local v0, "springForce":Lcom/android/server/display/animation/SpringForce;
    invoke-virtual {v0, p1}, Lcom/android/server/display/animation/SpringForce;->setStiffness(F)Lcom/android/server/display/animation/SpringForce;

    .line 384
    invoke-virtual {v0, p2}, Lcom/android/server/display/animation/SpringForce;->setDampingRatio(F)Lcom/android/server/display/animation/SpringForce;

    .line 385
    invoke-virtual {v0, p3}, Lcom/android/server/display/animation/SpringForce;->setFinalPosition(F)Lcom/android/server/display/animation/SpringForce;

    .line 386
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v1, v0}, Lcom/android/server/display/animation/SpringAnimation;->setSpring(Lcom/android/server/display/animation/SpringForce;)Lcom/android/server/display/animation/SpringAnimation;

    .line 388
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mPreOnAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

    if-eqz v1, :cond_0

    .line 389
    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v2, v1}, Lcom/android/server/display/animation/SpringAnimation;->removeEndListener(Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;)V

    .line 391
    :cond_0
    if-eqz p4, :cond_1

    .line 392
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v1, p4}, Lcom/android/server/display/animation/SpringAnimation;->addEndListener(Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;)Lcom/android/server/display/animation/DynamicAnimation;

    .line 394
    :cond_1
    iput-object p4, p0, Lcom/android/server/display/SwipeUpWindow;->mPreOnAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

    .line 396
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v1, p5}, Lcom/android/server/display/animation/SpringAnimation;->setMinimumVisibleChange(F)Lcom/android/server/display/animation/DynamicAnimation;

    .line 398
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v1}, Lcom/android/server/display/animation/SpringAnimation;->start()V

    .line 399
    return-void
.end method

.method private startSwipeUpAnimation()V
    .locals 4

    .line 317
    iget-boolean v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpWindowShowing:Z

    if-nez v0, :cond_0

    .line 318
    return-void

    .line 321
    :cond_0
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mPreTopColor:I

    .line 322
    iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBottomColor:I

    .line 323
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBlurLevel:I

    .line 324
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mAnimationState:Lcom/android/server/display/SwipeUpWindow$AnimationState;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/android/server/display/SwipeUpWindow$AnimationState;->setPerState(F)V

    .line 325
    invoke-direct {p0, v1}, Lcom/android/server/display/SwipeUpWindow;->updateBlur(F)V

    .line 327
    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->setWakeState()V

    .line 329
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x68

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 330
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 331
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1770

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 332
    return-void
.end method

.method private updateBlackView()V
    .locals 8

    .line 402
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mAnimationState:Lcom/android/server/display/SwipeUpWindow$AnimationState;

    invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$AnimationState;->getPerState()F

    move-result v0

    .line 403
    .local v0, "per":F
    const/high16 v1, 0x3f800000    # 1.0f

    add-float v2, v0, v1

    invoke-direct {p0, v2}, Lcom/android/server/display/SwipeUpWindow;->constraintAlpha(F)F

    move-result v2

    .line 404
    .local v2, "topAlpha":F
    const v3, 0x3f19999a    # 0.6f

    add-float/2addr v3, v0

    invoke-direct {p0, v3}, Lcom/android/server/display/SwipeUpWindow;->constraintAlpha(F)F

    move-result v3

    .line 406
    .local v3, "bottomAlpha":F
    invoke-direct {p0, v2}, Lcom/android/server/display/SwipeUpWindow;->calculateBlackAlpha(F)I

    move-result v4

    .line 407
    .local v4, "topColor":I
    invoke-direct {p0, v3}, Lcom/android/server/display/SwipeUpWindow;->calculateBlackAlpha(F)I

    move-result v5

    .line 408
    .local v5, "bottomColor":I
    iget v6, p0, Lcom/android/server/display/SwipeUpWindow;->mPreTopColor:I

    if-ne v4, v6, :cond_0

    iget v6, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBottomColor:I

    if-eq v5, v6, :cond_1

    .line 409
    :cond_0
    iput v4, p0, Lcom/android/server/display/SwipeUpWindow;->mPreTopColor:I

    .line 410
    iput v5, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBottomColor:I

    .line 411
    iget-object v6, p0, Lcom/android/server/display/SwipeUpWindow;->mBlackLinearGradientView:Lcom/android/server/display/BlackLinearGradientView;

    filled-new-array {v4, v5}, [I

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/server/display/BlackLinearGradientView;->setLinearGradientColor([I)V

    .line 418
    :cond_1
    const/4 v6, 0x0

    invoke-static {v6, v0}, Landroid/util/MathUtils;->min(FF)F

    move-result v6

    add-float/2addr v6, v1

    invoke-direct {p0, v6}, Lcom/android/server/display/SwipeUpWindow;->constraintBlur(F)F

    move-result v1

    .line 419
    .local v1, "blurLevel":F
    invoke-direct {p0, v1}, Lcom/android/server/display/SwipeUpWindow;->updateBlur(F)V

    .line 420
    return-void
.end method

.method private updateBlur(F)V
    .locals 1
    .param p1, "level"    # F

    .line 429
    const/high16 v0, 0x42c80000    # 100.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/android/server/display/SwipeUpWindow;->setBackgroudBlur(I)V

    .line 430
    return-void
.end method

.method private updateSettings(Z)V
    .locals 3
    .param p1, "bool"    # Z

    .line 492
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 493
    nop

    .line 492
    const-string/jumbo v1, "swipe_up_is_showing"

    const/4 v2, -0x2

    invoke-static {v0, v1, p1, v2}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 494
    return-void
.end method

.method private updateSizeInfo()V
    .locals 8

    .line 144
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mContext:Landroid/content/Context;

    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 146
    .local v0, "displayManager":Landroid/hardware/display/DisplayManager;
    const-string v1, "android.hardware.display.category.ALL_INCLUDING_DISABLED"

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;

    move-result-object v1

    .line 147
    .local v1, "displays":[Landroid/view/Display;
    new-instance v2, Landroid/view/DisplayInfo;

    invoke-direct {v2}, Landroid/view/DisplayInfo;-><init>()V

    .line 148
    .local v2, "outDisplayInfo":Landroid/view/DisplayInfo;
    array-length v3, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    aget-object v5, v1, v4

    .line 149
    .local v5, "display":Landroid/view/Display;
    invoke-virtual {v5, v2}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z

    .line 150
    iget v6, v2, Landroid/view/DisplayInfo;->type:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 151
    iget v6, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenWidth:I

    iget v7, v2, Landroid/view/DisplayInfo;->logicalWidth:I

    if-le v6, v7, :cond_0

    .line 152
    iget v6, v2, Landroid/view/DisplayInfo;->logicalWidth:I

    iput v6, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenWidth:I

    .line 153
    iget v6, v2, Landroid/view/DisplayInfo;->logicalHeight:I

    iput v6, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I

    .line 148
    .end local v5    # "display":Landroid/view/Display;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 158
    :cond_1
    iget v3, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenWidth:I

    const v4, 0x7fffffff

    if-ne v3, v4, :cond_2

    .line 159
    const/16 v3, 0x438

    iput v3, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenWidth:I

    .line 160
    const/16 v3, 0x9d8

    iput v3, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I

    .line 162
    :cond_2
    iget v3, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I

    int-to-float v3, v3

    const v4, 0x3e4ccccd    # 0.2f

    mul-float/2addr v3, v4

    iput v3, p0, Lcom/android/server/display/SwipeUpWindow;->mUnlockDistance:F

    .line 163
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateSizeInfo: (h="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", w="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "SwipeUpWindow"

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    return-void
.end method


# virtual methods
.method public afterFrictionValue(FF)F
    .locals 4
    .param p1, "value"    # F
    .param p2, "range"    # F

    .line 691
    const/4 v0, 0x0

    cmpl-float v1, p2, v0

    if-nez v1, :cond_0

    .line 692
    return v0

    .line 694
    :cond_0
    cmpl-float v0, p1, v0

    const/high16 v1, 0x3f800000    # 1.0f

    if-ltz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/high16 v0, -0x40800000    # -1.0f

    .line 695
    .local v0, "t":F
    :goto_0
    invoke-static {p1}, Landroid/util/MathUtils;->abs(F)F

    move-result p1

    .line 696
    div-float v2, p1, p2

    invoke-static {v2, v1}, Landroid/util/MathUtils;->min(FF)F

    move-result v1

    .line 697
    .local v1, "per":F
    mul-float v2, v1, v1

    mul-float/2addr v2, v1

    const/high16 v3, 0x40400000    # 3.0f

    div-float/2addr v2, v3

    mul-float v3, v1, v1

    sub-float/2addr v2, v3

    add-float/2addr v2, v1

    mul-float/2addr v2, v0

    mul-float/2addr v2, p2

    return v2
.end method

.method public cancelScreenOffDelay()V
    .locals 3

    .line 475
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 476
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    if-eqz v0, :cond_1

    .line 477
    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mPreOnAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

    if-eqz v2, :cond_0

    .line 478
    invoke-virtual {v0, v2}, Lcom/android/server/display/animation/SpringAnimation;->removeEndListener(Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;)V

    .line 479
    iput-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mPreOnAnimationEndListener:Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;

    .line 481
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v0}, Lcom/android/server/display/animation/SpringAnimation;->cancel()V

    .line 483
    :cond_1
    return-void
.end method

.method public releaseSwipeWindow()V
    .locals 1

    .line 171
    const-string/jumbo v0, "unknow"

    invoke-virtual {p0, v0}, Lcom/android/server/display/SwipeUpWindow;->releaseSwipeWindow(Ljava/lang/String;)V

    .line 172
    return-void
.end method

.method public releaseSwipeWindow(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .line 175
    iget-boolean v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpWindowShowing:Z

    if-nez v0, :cond_0

    .line 176
    return-void

    .line 178
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "release swipe up window: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SwipeUpWindow"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpWindowShowing:Z

    .line 181
    invoke-direct {p0, v0}, Lcom/android/server/display/SwipeUpWindow;->updateSettings(Z)V

    .line 182
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 184
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    if-eqz v0, :cond_1

    .line 185
    invoke-virtual {v0}, Lcom/android/server/display/animation/SpringAnimation;->cancel()V

    .line 186
    iput-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mGradientShadowSpringAnimation:Lcom/android/server/display/animation/SpringAnimation;

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpFrameLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_2

    .line 190
    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2, v0}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 191
    iput-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpFrameLayout:Landroid/widget/FrameLayout;

    .line 193
    :cond_2
    return-void
.end method

.method public removeSwipeUpWindow()V
    .locals 0

    .line 168
    return-void
.end method

.method public showSwipeUpWindow()V
    .locals 3

    .line 196
    iget-boolean v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpWindowShowing:Z

    if-eqz v0, :cond_0

    .line 197
    return-void

    .line 200
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/server/display/SwipeUpWindow;->updateSettings(Z)V

    .line 201
    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpFrameLayout:Landroid/widget/FrameLayout;

    if-nez v1, :cond_1

    .line 202
    invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->initSwipeUpWindow()V

    .line 204
    :cond_1
    iput-boolean v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpWindowShowing:Z

    .line 205
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpFrameLayout:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 206
    const-string v0, "SwipeUpWindow"

    const-string/jumbo v1, "show swipe up window"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    return-void
.end method

.method public transformDpToPx(F)I
    .locals 2
    .param p1, "dp"    # F

    .line 468
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow;->mContext:Landroid/content/Context;

    .line 469
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 468
    const/4 v1, 0x1

    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public transformDpToPx(Landroid/content/Context;F)I
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "dp"    # F

    .line 463
    nop

    .line 464
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 463
    const/4 v1, 0x1

    invoke-static {v1, p2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method
