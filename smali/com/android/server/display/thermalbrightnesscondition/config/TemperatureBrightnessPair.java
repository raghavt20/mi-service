public class com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair {
	 /* .source "TemperatureBrightnessPair.java" */
	 /* # instance fields */
	 private java.lang.Float maxExclusive;
	 private java.lang.Float minInclusive;
	 private java.lang.Float nit;
	 /* # direct methods */
	 public com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ( ) {
		 /* .locals 0 */
		 /* .line 3 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 static com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair read ( org.xmlpull.v1.XmlPullParser p0 ) {
		 /* .locals 7 */
		 /* .param p0, "_parser" # Lorg/xmlpull/v1/XmlPullParser; */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Lorg/xmlpull/v1/XmlPullParserException;, */
		 /* Ljava/io/IOException;, */
		 /* Ljavax/xml/datatype/DatatypeConfigurationException; */
		 /* } */
	 } // .end annotation
	 /* .line 63 */
	 /* new-instance v0, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
	 /* invoke-direct {v0}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;-><init>()V */
	 /* .line 64 */
	 /* .local v0, "_instance":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 65 */
	 v2 = 	 /* .local v1, "_raw":Ljava/lang/String; */
	 /* .line 67 */
	 /* .local v2, "outerDepth":I */
v3 = } // :goto_0
/* move v4, v3 */
/* .local v4, "type":I */
int v5 = 1; // const/4 v5, 0x1
int v6 = 3; // const/4 v6, 0x3
/* if-eq v3, v5, :cond_4 */
/* if-eq v4, v6, :cond_4 */
v3 = /* .line 69 */
int v5 = 2; // const/4 v5, 0x2
/* if-eq v3, v5, :cond_0 */
/* .line 70 */
} // :cond_0
/* .line 71 */
/* .local v3, "_tagName":Ljava/lang/String; */
final String v5 = "min-inclusive"; // const-string v5, "min-inclusive"
v5 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 72 */
com.android.server.display.thermalbrightnesscondition.config.XmlParser .readText ( p0 );
/* .line 73 */
v5 = java.lang.Float .parseFloat ( v1 );
/* .line 74 */
/* .local v5, "_value":F */
(( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v0 ).setMinInclusive ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->setMinInclusive(F)V
/* .line 75 */
} // .end local v5 # "_value":F
} // :cond_1
final String v5 = "max-exclusive"; // const-string v5, "max-exclusive"
v5 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 76 */
com.android.server.display.thermalbrightnesscondition.config.XmlParser .readText ( p0 );
/* .line 77 */
v5 = java.lang.Float .parseFloat ( v1 );
/* .line 78 */
/* .restart local v5 # "_value":F */
(( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v0 ).setMaxExclusive ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->setMaxExclusive(F)V
/* .line 79 */
} // .end local v5 # "_value":F
} // :cond_2
final String v5 = "nit"; // const-string v5, "nit"
v5 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 80 */
com.android.server.display.thermalbrightnesscondition.config.XmlParser .readText ( p0 );
/* .line 81 */
v5 = java.lang.Float .parseFloat ( v1 );
/* .line 82 */
/* .restart local v5 # "_value":F */
(( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v0 ).setNit ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->setNit(F)V
/* .line 83 */
} // .end local v5 # "_value":F
/* .line 84 */
} // :cond_3
com.android.server.display.thermalbrightnesscondition.config.XmlParser .skip ( p0 );
/* .line 86 */
} // .end local v3 # "_tagName":Ljava/lang/String;
} // :goto_1
/* .line 87 */
} // :cond_4
/* if-ne v4, v6, :cond_5 */
/* .line 90 */
/* .line 88 */
} // :cond_5
/* new-instance v3, Ljavax/xml/datatype/DatatypeConfigurationException; */
final String v5 = "TemperatureBrightnessPair is not closed"; // const-string v5, "TemperatureBrightnessPair is not closed"
/* invoke-direct {v3, v5}, Ljavax/xml/datatype/DatatypeConfigurationException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
} // .end method
/* # virtual methods */
public Float getMaxExclusive ( ) {
/* .locals 1 */
/* .line 27 */
v0 = this.maxExclusive;
/* if-nez v0, :cond_0 */
/* .line 28 */
int v0 = 0; // const/4 v0, 0x0
/* .line 30 */
} // :cond_0
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
} // .end method
public Float getMinInclusive ( ) {
/* .locals 1 */
/* .line 9 */
v0 = this.minInclusive;
/* if-nez v0, :cond_0 */
/* .line 10 */
int v0 = 0; // const/4 v0, 0x0
/* .line 12 */
} // :cond_0
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
} // .end method
public Float getNit ( ) {
/* .locals 1 */
/* .line 45 */
v0 = this.nit;
/* if-nez v0, :cond_0 */
/* .line 46 */
int v0 = 0; // const/4 v0, 0x0
/* .line 48 */
} // :cond_0
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
} // .end method
Boolean hasMaxExclusive ( ) {
/* .locals 1 */
/* .line 34 */
v0 = this.maxExclusive;
/* if-nez v0, :cond_0 */
/* .line 35 */
int v0 = 0; // const/4 v0, 0x0
/* .line 37 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
Boolean hasMinInclusive ( ) {
/* .locals 1 */
/* .line 16 */
v0 = this.minInclusive;
/* if-nez v0, :cond_0 */
/* .line 17 */
int v0 = 0; // const/4 v0, 0x0
/* .line 19 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
Boolean hasNit ( ) {
/* .locals 1 */
/* .line 52 */
v0 = this.nit;
/* if-nez v0, :cond_0 */
/* .line 53 */
int v0 = 0; // const/4 v0, 0x0
/* .line 55 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void setMaxExclusive ( Float p0 ) {
/* .locals 1 */
/* .param p1, "maxExclusive" # F */
/* .line 41 */
java.lang.Float .valueOf ( p1 );
this.maxExclusive = v0;
/* .line 42 */
return;
} // .end method
public void setMinInclusive ( Float p0 ) {
/* .locals 1 */
/* .param p1, "minInclusive" # F */
/* .line 23 */
java.lang.Float .valueOf ( p1 );
this.minInclusive = v0;
/* .line 24 */
return;
} // .end method
public void setNit ( Float p0 ) {
/* .locals 1 */
/* .param p1, "nit" # F */
/* .line 59 */
java.lang.Float .valueOf ( p1 );
this.nit = v0;
/* .line 60 */
return;
} // .end method
