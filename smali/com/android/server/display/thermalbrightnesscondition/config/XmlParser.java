public class com.android.server.display.thermalbrightnesscondition.config.XmlParser {
	 /* .source "XmlParser.java" */
	 /* # direct methods */
	 public com.android.server.display.thermalbrightnesscondition.config.XmlParser ( ) {
		 /* .locals 0 */
		 /* .line 3 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static com.android.server.display.thermalbrightnesscondition.config.ThermalBrightnessConfig read ( java.io.InputStream p0 ) {
		 /* .locals 5 */
		 /* .param p0, "in" # Ljava/io/InputStream; */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Lorg/xmlpull/v1/XmlPullParserException;, */
		 /* Ljava/io/IOException;, */
		 /* Ljavax/xml/datatype/DatatypeConfigurationException; */
		 /* } */
	 } // .end annotation
	 /* .line 5 */
	 org.xmlpull.v1.XmlPullParserFactory .newInstance ( );
	 (( org.xmlpull.v1.XmlPullParserFactory ) v0 ).newPullParser ( ); // invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;
	 /* .line 6 */
	 /* .local v0, "_parser":Lorg/xmlpull/v1/XmlPullParser; */
	 final String v1 = "http://xmlpull.org/v1/doc/features.html#process-namespaces"; // const-string v1, "http://xmlpull.org/v1/doc/features.html#process-namespaces"
	 int v2 = 1; // const/4 v2, 0x1
	 /* .line 7 */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 8 */
	 /* .line 9 */
	 /* .line 10 */
	 /* .local v2, "_tagName":Ljava/lang/String; */
	 int v3 = 0; // const/4 v3, 0x0
	 /* .line 11 */
	 /* .local v3, "_raw":Ljava/lang/String; */
	 /* const-string/jumbo v4, "thermal-brightness-config" */
	 v4 = 	 (( java.lang.String ) v2 ).equals ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v4 != null) { // if-eqz v4, :cond_0
		 /* .line 12 */
		 com.android.server.display.thermalbrightnesscondition.config.ThermalBrightnessConfig .read ( v0 );
		 /* .line 13 */
		 /* .local v1, "_value":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig; */
		 /* .line 15 */
	 } // .end local v1 # "_value":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;
} // :cond_0
} // .end method
public static java.lang.String readText ( org.xmlpull.v1.XmlPullParser p0 ) {
/* .locals 3 */
/* .param p0, "_parser" # Lorg/xmlpull/v1/XmlPullParser; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/xmlpull/v1/XmlPullParserException;, */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 19 */
final String v0 = ""; // const-string v0, ""
/* .line 20 */
v1 = /* .local v0, "result":Ljava/lang/String; */
int v2 = 4; // const/4 v2, 0x4
/* if-ne v1, v2, :cond_0 */
/* .line 21 */
/* .line 22 */
/* .line 24 */
} // :cond_0
} // .end method
public static void skip ( org.xmlpull.v1.XmlPullParser p0 ) {
/* .locals 2 */
/* .param p0, "_parser" # Lorg/xmlpull/v1/XmlPullParser; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/xmlpull/v1/XmlPullParserException;, */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
v0 = /* .line 28 */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_1 */
/* .line 31 */
int v0 = 1; // const/4 v0, 0x1
/* .line 32 */
/* .local v0, "depth":I */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = /* .line 33 */
/* packed-switch v1, :pswitch_data_0 */
/* .line 35 */
/* :pswitch_0 */
/* add-int/lit8 v0, v0, -0x1 */
/* .line 36 */
/* .line 38 */
/* :pswitch_1 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 39 */
} // :goto_1
/* .line 42 */
} // :cond_0
return;
/* .line 29 */
} // .end local v0 # "depth":I
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalStateException; */
/* invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V */
/* throw v0 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
