public class com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem {
	 /* .source "ThermalConditionItem.java" */
	 /* # instance fields */
	 private java.lang.String description;
	 private java.lang.Integer identifier;
	 private java.util.List temperatureBrightnessPair;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ( ) {
/* .locals 0 */
/* .line 3 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem read ( org.xmlpull.v1.XmlPullParser p0 ) {
/* .locals 7 */
/* .param p0, "_parser" # Lorg/xmlpull/v1/XmlPullParser; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/xmlpull/v1/XmlPullParserException;, */
/* Ljava/io/IOException;, */
/* Ljavax/xml/datatype/DatatypeConfigurationException; */
/* } */
} // .end annotation
/* .line 49 */
/* new-instance v0, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
/* invoke-direct {v0}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;-><init>()V */
/* .line 50 */
/* .local v0, "_instance":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
int v1 = 0; // const/4 v1, 0x0
/* .line 51 */
v2 = /* .local v1, "_raw":Ljava/lang/String; */
/* .line 53 */
/* .local v2, "outerDepth":I */
v3 = } // :goto_0
/* move v4, v3 */
/* .local v4, "type":I */
int v5 = 1; // const/4 v5, 0x1
int v6 = 3; // const/4 v6, 0x3
/* if-eq v3, v5, :cond_4 */
/* if-eq v4, v6, :cond_4 */
v3 = /* .line 55 */
int v5 = 2; // const/4 v5, 0x2
/* if-eq v3, v5, :cond_0 */
/* .line 56 */
} // :cond_0
/* .line 57 */
/* .local v3, "_tagName":Ljava/lang/String; */
final String v5 = "identifier"; // const-string v5, "identifier"
v5 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 58 */
com.android.server.display.thermalbrightnesscondition.config.XmlParser .readText ( p0 );
/* .line 59 */
v5 = java.lang.Integer .parseInt ( v1 );
/* .line 60 */
/* .local v5, "_value":I */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v0 ).setIdentifier ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->setIdentifier(I)V
/* .line 61 */
} // .end local v5 # "_value":I
} // :cond_1
final String v5 = "description"; // const-string v5, "description"
v5 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 62 */
com.android.server.display.thermalbrightnesscondition.config.XmlParser .readText ( p0 );
/* .line 63 */
/* move-object v5, v1 */
/* .line 64 */
/* .local v5, "_value":Ljava/lang/String; */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v0 ).setDescription ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->setDescription(Ljava/lang/String;)V
/* .line 65 */
} // .end local v5 # "_value":Ljava/lang/String;
} // :cond_2
/* const-string/jumbo v5, "temperature-brightness-pair" */
v5 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 66 */
com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair .read ( p0 );
/* .line 67 */
/* .local v5, "_value":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v0 ).getTemperatureBrightnessPair ( ); // invoke-virtual {v0}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;
/* .line 68 */
} // .end local v5 # "_value":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
/* .line 69 */
} // :cond_3
com.android.server.display.thermalbrightnesscondition.config.XmlParser .skip ( p0 );
/* .line 71 */
} // .end local v3 # "_tagName":Ljava/lang/String;
} // :goto_1
/* .line 72 */
} // :cond_4
/* if-ne v4, v6, :cond_5 */
/* .line 75 */
/* .line 73 */
} // :cond_5
/* new-instance v3, Ljavax/xml/datatype/DatatypeConfigurationException; */
final String v5 = "ThermalConditionItem is not closed"; // const-string v5, "ThermalConditionItem is not closed"
/* invoke-direct {v3, v5}, Ljavax/xml/datatype/DatatypeConfigurationException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
} // .end method
/* # virtual methods */
public java.lang.String getDescription ( ) {
/* .locals 1 */
/* .line 27 */
v0 = this.description;
} // .end method
public Integer getIdentifier ( ) {
/* .locals 1 */
/* .line 9 */
v0 = this.identifier;
/* if-nez v0, :cond_0 */
/* .line 10 */
int v0 = 0; // const/4 v0, 0x0
/* .line 12 */
} // :cond_0
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
} // .end method
public java.util.List getTemperatureBrightnessPair ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 42 */
v0 = this.temperatureBrightnessPair;
/* if-nez v0, :cond_0 */
/* .line 43 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.temperatureBrightnessPair = v0;
/* .line 45 */
} // :cond_0
v0 = this.temperatureBrightnessPair;
} // .end method
Boolean hasDescription ( ) {
/* .locals 1 */
/* .line 31 */
v0 = this.description;
/* if-nez v0, :cond_0 */
/* .line 32 */
int v0 = 0; // const/4 v0, 0x0
/* .line 34 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
Boolean hasIdentifier ( ) {
/* .locals 1 */
/* .line 16 */
v0 = this.identifier;
/* if-nez v0, :cond_0 */
/* .line 17 */
int v0 = 0; // const/4 v0, 0x0
/* .line 19 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void setDescription ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "description" # Ljava/lang/String; */
/* .line 38 */
this.description = p1;
/* .line 39 */
return;
} // .end method
public void setIdentifier ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "identifier" # I */
/* .line 23 */
java.lang.Integer .valueOf ( p1 );
this.identifier = v0;
/* .line 24 */
return;
} // .end method
