class com.android.server.display.DaemonSensorPolicy$DaemonSensorHandle extends android.os.Handler {
	 /* .source "DaemonSensorPolicy.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/DaemonSensorPolicy; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "DaemonSensorHandle" */
} // .end annotation
/* # instance fields */
final com.android.server.display.DaemonSensorPolicy this$0; //synthetic
/* # direct methods */
public com.android.server.display.DaemonSensorPolicy$DaemonSensorHandle ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/DaemonSensorPolicy; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 183 */
this.this$0 = p1;
/* .line 184 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 185 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 189 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 191 */
/* :pswitch_0 */
v0 = this.this$0;
com.android.server.display.DaemonSensorPolicy .-$$Nest$fgetmPowerManager ( v0 );
v1 = (( android.os.PowerManager ) v1 ).isDeviceIdleMode ( ); // invoke-virtual {v1}, Landroid/os/PowerManager;->isDeviceIdleMode()Z
com.android.server.display.DaemonSensorPolicy .-$$Nest$fputmIsDeviceIdleMode ( v0,v1 );
/* .line 192 */
/* nop */
/* .line 196 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
