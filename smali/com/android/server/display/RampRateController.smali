.class public Lcom/android/server/display/RampRateController;
.super Ljava/lang/Object;
.source "RampRateController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/RampRateController$RateStateRecord;
    }
.end annotation


# static fields
.field private static final ANIMATION_RATE_TYPE_DEFAULT:I = 0x0

.field private static final ANIMATION_RATE_TYPE_DIM:I = 0x1

.field private static final ANIMATION_RATE_TYPE_HDR_BRIGHTNESS:I = 0x4

.field private static final ANIMATION_RATE_TYPE_MANUAL_BRIGHTNESS:I = 0x3

.field private static final ANIMATION_RATE_TYPE_TEMPORARY_DIMMING:I = 0x2

.field private static final BRIGHTNESS_12BIT:I = 0xfff

.field private static final IS_UMI_0B_DISPLAY_PANEL:Z

.field private static final NIT_LEVEL:F = 40.0f

.field private static final NIT_LEVEL1:F = 35.0f

.field private static final NIT_LEVEL2:F = 87.450005f

.field private static final NIT_LEVEL3:F = 265.0f

.field private static final OLED_PANEL_ID:Ljava/lang/String;

.field private static final RATE_LEVEL:I = 0x28

.field private static final REASON_AUTO_BRIGHTNESS_RATE:I = 0x8

.field private static final REASON_BCBC_BRIGHTNESS_RATE:I = 0xb

.field private static final REASON_DIM_BRIGHTNESS_RATE:I = 0x3

.field private static final REASON_DOZE_BRIGHTNESS_RATE:I = 0xc

.field private static final REASON_FAST_BRIGHTNESS_ADJUSTMENT_RATE:I = 0x7

.field private static final REASON_FAST_RATE:I = 0x2

.field private static final REASON_HDR_BRIGHTNESS_RATE:I = 0x6

.field private static final REASON_MANUAL_BRIGHTNESS_RATE:I = 0x9

.field private static final REASON_OVERRIDE_BRIGHTNESS_RATE:I = 0x5

.field private static final REASON_SLOW_FAST:I = 0x1

.field private static final REASON_TEMPORARY_BRIGHTNESS_RATE:I = 0x4

.field private static final REASON_THERMAL_BRIGHTNESS_RATE:I = 0xa

.field private static final REASON_ZERO_RATE:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field private static final TIME_1:F = 0.0f

.field private static final TIME_2:F = 0.8f

.field private static final TIME_3:F = 1.8f

.field private static final TIME_4:F = 4.0f

.field private static final TIME_5:F = 24.0f


# instance fields
.field private final mALevels:[F

.field private mAnimateValueChanged:Z

.field private final mAnimationDurationDim:I

.field private final mAnimationDurationDozeDimming:I

.field private final mAnimationDurationHdrBrightness:I

.field private final mAnimationDurationManualBrightness:I

.field private final mAnimationDurationTemporaryDimming:I

.field private mAppliedHdrRateDueToEntry:Z

.field private mAppliedHdrRateDueToExit:Z

.field private mAppliedOverrideRateDueToEntry:Z

.field private mAppliedOverrideRateDueToExit:Z

.field private final mBLevels:[F

.field private mBrightness:F

.field private mBrightnessChanged:Z

.field private mBrightnessMaxNit:F

.field private final mBrightnessRampRateFast:F

.field private final mBrightnessRampRateSlow:F

.field private final mBrightnessReason:Lcom/android/server/display/brightness/BrightnessReason;

.field private final mBrightnessReasonTemp:Lcom/android/server/display/brightness/BrightnessReason;

.field private mCurrentPolicy:I

.field private mDebug:Z

.field private final mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

.field private final mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

.field private final mHandler:Landroid/os/Handler;

.field private mHbmMaxNit:F

.field private mHbmTransitionPointNit:F

.field private final mHighBrightnessModeData:Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;

.field private final mMinimumAdjustRate:F

.field private final mNitsLevels:[F

.field private mPreviousDisplayState:I

.field private mPreviousPolicy:I

.field private mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

.field private final mScreenBrightnessMaximumInt:I

.field private mSdrBrightness:F

.field private mSlowChange:Z

.field private final mSlowLinearRampRate:F

.field private final mSupportManualDimming:Z

.field private mTargetBrightness:F

.field private mTargetSdrBrightness:F


# direct methods
.method static bridge synthetic -$$Nest$fgetmAnimationDurationDozeDimming(Lcom/android/server/display/RampRateController;)I
    .locals 0

    iget p0, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationDozeDimming:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmAnimationDurationTemporaryDimming(Lcom/android/server/display/RampRateController;)I
    .locals 0

    iget p0, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationTemporaryDimming:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSupportManualDimming(Lcom/android/server/display/RampRateController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/RampRateController;->mSupportManualDimming:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/display/RampRateController;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 45
    const-class v0, Lcom/android/server/display/RampRateController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/display/RampRateController;->TAG:Ljava/lang/String;

    .line 68
    nop

    .line 69
    const-string v0, "ro.boot.oled_panel_id"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/display/RampRateController;->OLED_PANEL_ID:Ljava/lang/String;

    .line 70
    const-string v1, "0B"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 71
    const-string/jumbo v1, "umi"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "umiin"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    sput-boolean v0, Lcom/android/server/display/RampRateController;->IS_UMI_0B_DISPLAY_PANEL:Z

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/display/DisplayPowerControllerImpl;Lcom/android/server/display/DisplayDeviceConfig;Landroid/os/Looper;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "impl"    # Lcom/android/server/display/DisplayPowerControllerImpl;
    .param p3, "deviceConfig"    # Lcom/android/server/display/DisplayDeviceConfig;
    .param p4, "looper"    # Landroid/os/Looper;

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const/16 v0, 0x9

    new-array v1, v0, [F

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/android/server/display/RampRateController;->mNitsLevels:[F

    .line 84
    new-array v1, v0, [F

    fill-array-data v1, :array_1

    iput-object v1, p0, Lcom/android/server/display/RampRateController;->mALevels:[F

    .line 86
    new-array v0, v0, [F

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/android/server/display/RampRateController;->mBLevels:[F

    .line 102
    new-instance v0, Lcom/android/server/display/brightness/BrightnessReason;

    invoke-direct {v0}, Lcom/android/server/display/brightness/BrightnessReason;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/RampRateController;->mBrightnessReason:Lcom/android/server/display/brightness/BrightnessReason;

    .line 103
    new-instance v0, Lcom/android/server/display/brightness/BrightnessReason;

    invoke-direct {v0}, Lcom/android/server/display/brightness/BrightnessReason;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/RampRateController;->mBrightnessReasonTemp:Lcom/android/server/display/brightness/BrightnessReason;

    .line 112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mDebug:Z

    .line 122
    const/high16 v1, 0x7fc00000    # Float.NaN

    iput v1, p0, Lcom/android/server/display/RampRateController;->mHbmTransitionPointNit:F

    .line 123
    iput v1, p0, Lcom/android/server/display/RampRateController;->mHbmMaxNit:F

    .line 124
    iput v1, p0, Lcom/android/server/display/RampRateController;->mBrightnessMaxNit:F

    .line 132
    iput-object p2, p0, Lcom/android/server/display/RampRateController;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    .line 133
    iput-object p3, p0, Lcom/android/server/display/RampRateController;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    .line 134
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, p4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/display/RampRateController;->mHandler:Landroid/os/Handler;

    .line 135
    invoke-virtual {p3}, Lcom/android/server/display/DisplayDeviceConfig;->getHighBrightnessModeData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/display/RampRateController;->mHighBrightnessModeData:Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;

    .line 136
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 137
    .local v1, "resources":Landroid/content/res/Resources;
    const v2, 0x10500b8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    iput v2, p0, Lcom/android/server/display/RampRateController;->mBrightnessRampRateSlow:F

    .line 139
    const v2, 0x10500b7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    iput v2, p0, Lcom/android/server/display/RampRateController;->mBrightnessRampRateFast:F

    .line 141
    const v2, 0x110b000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationDim:I

    .line 143
    const v2, 0x110b000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationTemporaryDimming:I

    .line 145
    const v2, 0x110b000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationManualBrightness:I

    .line 147
    const v2, 0x110b000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationHdrBrightness:I

    .line 149
    const v2, 0x110b000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationDozeDimming:I

    .line 151
    const v2, 0x11070036

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    iput v2, p0, Lcom/android/server/display/RampRateController;->mSlowLinearRampRate:F

    .line 152
    const v2, 0x10e00f3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/android/server/display/RampRateController;->mScreenBrightnessMaximumInt:I

    .line 154
    const/high16 v3, 0x3f800000    # 1.0f

    int-to-float v2, v2

    div-float/2addr v3, v2

    iput v3, p0, Lcom/android/server/display/RampRateController;->mMinimumAdjustRate:F

    .line 155
    const-string/jumbo v2, "support_manual_dimming"

    invoke-static {v2, v0}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mSupportManualDimming:Z

    .line 156
    new-instance v0, Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-direct {v0, p0}, Lcom/android/server/display/RampRateController$RateStateRecord;-><init>(Lcom/android/server/display/RampRateController;)V

    iput-object v0, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    .line 157
    invoke-direct {p0}, Lcom/android/server/display/RampRateController;->updateDeviceConfigData()V

    .line 158
    return-void

    nop

    :array_0
    .array-data 4
        0x44480000    # 800.0f
        0x437b0000    # 251.0f
        0x43160000    # 150.0f
        0x42c80000    # 100.0f
        0x428c0000    # 70.0f
        0x42480000    # 50.0f
        0x42200000    # 40.0f
        0x41f00000    # 30.0f
        0x41e40000    # 28.5f
    .end array-data

    :array_1
    .array-data 4
        0x44480000    # 800.0f
        0x440e5eb8
        0x43ac71ec
        0x436dc000    # 237.75f
        0x4333b5c3    # 179.71f
        0x430730a4    # 135.19f
        0x42e32e14    # 113.59f
        0x427b5c29    # 62.84f
        0x442937ae    # 676.87f
    .end array-data

    :array_2
    .array-data 4
        0x3f7d1b71    # 0.9887f
        0x3f7df3b6    # 0.992f
        0x3f7eb852    # 0.995f
        0x3f7f1aa0    # 0.9965f
        0x3f7f4f0e    # 0.9973f
        0x3f7f7660    # 0.9979f
        0x3f7f8a09    # 0.9982f
        0x3f7fbe77    # 0.999f
        0x3f7ef9db    # 0.996f
    .end array-data
.end method

.method private appliedFastRate(FF)Z
    .locals 1
    .param p1, "currentBrightness"    # F
    .param p2, "targetBrightness"    # F

    .line 607
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->appliedFastRate(FF)Z

    move-result v0

    return v0
.end method

.method private clearHdrRateModifier()V
    .locals 2

    .line 623
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 624
    return-void
.end method

.method private clearOverrideRateModifier()V
    .locals 2

    .line 627
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 628
    return-void
.end method

.method private convertToBrightness(F)F
    .locals 1
    .param p1, "nit"    # F

    .line 576
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    if-nez v0, :cond_0

    .line 577
    const/high16 v0, 0x7fc00000    # Float.NaN

    return v0

    .line 579
    :cond_0
    invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F

    move-result v0

    return v0
.end method

.method private convertToNit(F)F
    .locals 1
    .param p1, "brightness"    # F

    .line 569
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    if-nez v0, :cond_0

    .line 570
    const/high16 v0, 0x7fc00000    # Float.NaN

    return v0

    .line 572
    :cond_0
    invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v0

    return v0
.end method

.method private getBrighteningLogRate(FFFF)F
    .locals 5
    .param p1, "startBrightness"    # F
    .param p2, "currentBrightness"    # F
    .param p3, "targetBrightness"    # F
    .param p4, "duration"    # F

    .line 523
    nop

    .line 524
    invoke-static {p3}, Landroid/util/MathUtils;->exp(F)F

    move-result v0

    invoke-static {p1}, Landroid/util/MathUtils;->exp(F)F

    move-result v1

    sub-float/2addr v0, v1

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    div-float/2addr v0, p4

    .line 525
    .local v0, "coefficient":F
    invoke-static {p2}, Landroid/util/MathUtils;->exp(F)F

    move-result v1

    div-float v1, v0, v1

    .line 526
    .local v1, "rate":F
    iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mDebug:Z

    if-eqz v2, :cond_0

    .line 527
    sget-object v2, Lcom/android/server/display/RampRateController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getBrighteningLogRate: rate: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", startBrightness: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", currentBrightness: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", targetBrightness: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", coefficient: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    :cond_0
    return v1
.end method

.method private getBrighteningRate(FFF)F
    .locals 9
    .param p1, "brightness"    # F
    .param p2, "startBrightness"    # F
    .param p3, "tgtBrightness"    # F

    .line 382
    invoke-direct {p0, p1}, Lcom/android/server/display/RampRateController;->convertToNit(F)F

    move-result v6

    .line 383
    .local v6, "nit":F
    invoke-direct {p0, p2}, Lcom/android/server/display/RampRateController;->convertToNit(F)F

    move-result v7

    .line 384
    .local v7, "startNit":F
    invoke-direct {p0, p3}, Lcom/android/server/display/RampRateController;->convertToNit(F)F

    move-result v8

    .line 386
    .local v8, "targetNit":F
    const/high16 v0, 0x420c0000    # 35.0f

    cmpg-float v1, v7, v0

    const v2, 0x42aee667    # 87.450005f

    const v3, 0x43848000    # 265.0f

    if-gez v1, :cond_5

    .line 387
    cmpg-float v1, v8, v0

    if-gez v1, :cond_0

    .line 388
    sub-float v0, v8, v7

    invoke-direct {p0, v0}, Lcom/android/server/display/RampRateController;->convertToBrightness(F)F

    move-result v0

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    .local v0, "rate":F
    goto/16 :goto_0

    .line 389
    .end local v0    # "rate":F
    :cond_0
    cmpg-float v1, v8, v3

    if-gez v1, :cond_2

    .line 390
    cmpg-float v1, v6, v0

    if-gez v1, :cond_1

    .line 391
    invoke-direct {p0, v0}, Lcom/android/server/display/RampRateController;->convertToBrightness(F)F

    move-result v0

    sub-float/2addr v0, p2

    const v1, 0x3fe66666    # 1.8f

    div-float/2addr v0, v1

    .restart local v0    # "rate":F
    goto/16 :goto_0

    .line 393
    .end local v0    # "rate":F
    :cond_1
    const/high16 v1, 0x420c0000    # 35.0f

    const v4, 0x3fe66666    # 1.8f

    const/high16 v5, 0x40800000    # 4.0f

    move-object v0, p0

    move v2, v8

    move v3, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/RampRateController;->getExpRate(FFFFF)F

    move-result v0

    .restart local v0    # "rate":F
    goto/16 :goto_0

    .line 396
    .end local v0    # "rate":F
    :cond_2
    cmpg-float v1, v6, v0

    if-gez v1, :cond_3

    .line 397
    invoke-direct {p0, v0}, Lcom/android/server/display/RampRateController;->convertToBrightness(F)F

    move-result v0

    sub-float/2addr v0, p2

    const v1, 0x3f4ccccd    # 0.8f

    div-float/2addr v0, v1

    .restart local v0    # "rate":F
    goto/16 :goto_0

    .line 398
    .end local v0    # "rate":F
    :cond_3
    cmpg-float v0, v6, v2

    if-gez v0, :cond_4

    .line 399
    const/high16 v1, 0x420c0000    # 35.0f

    const v2, 0x42aee667    # 87.450005f

    const v4, 0x3f4ccccd    # 0.8f

    const v5, 0x3fe66666    # 1.8f

    move-object v0, p0

    move v3, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/RampRateController;->getExpRate(FFFFF)F

    move-result v0

    .restart local v0    # "rate":F
    goto/16 :goto_0

    .line 401
    .end local v0    # "rate":F
    :cond_4
    const v1, 0x42aee667    # 87.450005f

    const v4, 0x3fe66666    # 1.8f

    const/high16 v5, 0x40800000    # 4.0f

    move-object v0, p0

    move v2, v8

    move v3, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/RampRateController;->getExpRate(FFFFF)F

    move-result v0

    .restart local v0    # "rate":F
    goto :goto_0

    .line 404
    .end local v0    # "rate":F
    :cond_5
    cmpg-float v0, v7, v3

    if-gez v0, :cond_8

    .line 405
    cmpg-float v0, v8, v3

    if-gez v0, :cond_6

    .line 406
    const/4 v4, 0x0

    const/high16 v5, 0x40800000    # 4.0f

    move-object v0, p0

    move v1, v7

    move v2, v8

    move v3, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/RampRateController;->getExpRate(FFFFF)F

    move-result v0

    .restart local v0    # "rate":F
    goto :goto_0

    .line 408
    .end local v0    # "rate":F
    :cond_6
    cmpg-float v0, v6, v2

    if-gez v0, :cond_7

    .line 409
    const v2, 0x42aee667    # 87.450005f

    const/4 v4, 0x0

    const v5, 0x3fe66666    # 1.8f

    move-object v0, p0

    move v1, v7

    move v3, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/RampRateController;->getExpRate(FFFFF)F

    move-result v0

    .restart local v0    # "rate":F
    goto :goto_0

    .line 411
    .end local v0    # "rate":F
    :cond_7
    const v1, 0x42aee667    # 87.450005f

    const v4, 0x3fe66666    # 1.8f

    const/high16 v5, 0x40800000    # 4.0f

    move-object v0, p0

    move v2, v8

    move v3, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/RampRateController;->getExpRate(FFFFF)F

    move-result v0

    .restart local v0    # "rate":F
    goto :goto_0

    .line 415
    .end local v0    # "rate":F
    :cond_8
    const/4 v4, 0x0

    const/high16 v5, 0x40800000    # 4.0f

    move-object v0, p0

    move v1, v7

    move v2, v8

    move v3, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/RampRateController;->getExpRate(FFFFF)F

    move-result v0

    .line 417
    .restart local v0    # "rate":F
    :goto_0
    iget v1, p0, Lcom/android/server/display/RampRateController;->mMinimumAdjustRate:F

    invoke-static {v0, v1}, Landroid/util/MathUtils;->max(FF)F

    move-result v0

    .line 418
    iget-boolean v1, p0, Lcom/android/server/display/RampRateController;->mDebug:Z

    if-eqz v1, :cond_9

    .line 419
    sget-object v1, Lcom/android/server/display/RampRateController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getBrighteningRate: rate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", nit: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", startNit: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", targetNit: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    :cond_9
    return v0
.end method

.method private getDarkeningExpRate(FFFF)F
    .locals 5
    .param p1, "startBrightness"    # F
    .param p2, "currentBrightness"    # F
    .param p3, "targetBrightness"    # F
    .param p4, "duration"    # F

    .line 539
    div-float v0, p3, p1

    invoke-static {v0}, Landroid/util/MathUtils;->log(F)F

    move-result v0

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    div-float/2addr v0, p4

    .line 540
    .local v0, "coefficient":F
    mul-float v1, v0, p2

    invoke-static {v1}, Landroid/util/MathUtils;->abs(F)F

    move-result v1

    .line 541
    .local v1, "rate":F
    iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mDebug:Z

    if-eqz v2, :cond_0

    .line 542
    sget-object v2, Lcom/android/server/display/RampRateController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDarkeningExpRate: rate: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", startBrightness: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", currentBrightness: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", targetBrightness: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", coefficient: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    :cond_0
    return v1
.end method

.method private getDarkeningRate(F)F
    .locals 10
    .param p1, "brightness"    # F

    .line 437
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mHighBrightnessModeData:Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;

    const/high16 v1, 0x3f800000    # 1.0f

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    .line 438
    :cond_0
    iget v0, v0, Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;->transitionPoint:F

    :goto_0
    nop

    .line 439
    .local v0, "normalMaxBrightnessFloat":F
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v2, v0}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v2

    .line 440
    .local v2, "normalNit":F
    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v3, v2, v3

    if-nez v3, :cond_1

    .line 441
    const/high16 v2, 0x43fa0000    # 500.0f

    .line 443
    :cond_1
    invoke-static {v0}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v3

    .line 444
    .local v3, "normalMaxBrightnessInt":I
    invoke-direct {p0, p1}, Lcom/android/server/display/RampRateController;->convertToNit(F)F

    move-result v4

    .line 445
    .local v4, "nit":F
    invoke-direct {p0, v4}, Lcom/android/server/display/RampRateController;->getIndex(F)I

    move-result v5

    .line 446
    .local v5, "index":I
    iget-object v6, p0, Lcom/android/server/display/RampRateController;->mALevels:[F

    aget v6, v6, v5

    const/high16 v7, 0x41c00000    # 24.0f

    mul-float/2addr v6, v7

    iget-object v8, p0, Lcom/android/server/display/RampRateController;->mBLevels:[F

    aget v8, v8, v5

    .line 447
    invoke-direct {p0, v4, v5}, Lcom/android/server/display/RampRateController;->getTime(FI)F

    move-result v9

    mul-float/2addr v9, v7

    invoke-static {v8, v9}, Landroid/util/MathUtils;->pow(FF)F

    move-result v7

    mul-float/2addr v6, v7

    iget-object v7, p0, Lcom/android/server/display/RampRateController;->mBLevels:[F

    aget v7, v7, v5

    .line 448
    invoke-static {v7}, Landroid/util/MathUtils;->log(F)F

    move-result v7

    mul-float/2addr v6, v7

    .line 446
    invoke-static {v6}, Landroid/util/MathUtils;->abs(F)F

    move-result v6

    .line 449
    .local v6, "rate":F
    iget v7, p0, Lcom/android/server/display/RampRateController;->mHbmTransitionPointNit:F

    cmpl-float v7, v4, v7

    if-lez v7, :cond_2

    .line 450
    invoke-static {v1}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v1

    .line 451
    .local v1, "maxBrightnessInt":I
    sub-int v7, v1, v3

    invoke-static {v7}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessIntToFloat(I)F

    move-result v7

    mul-float/2addr v7, v6

    iget v8, p0, Lcom/android/server/display/RampRateController;->mHbmMaxNit:F

    iget v9, p0, Lcom/android/server/display/RampRateController;->mHbmTransitionPointNit:F

    sub-float/2addr v8, v9

    div-float/2addr v7, v8

    .line 453
    .end local v1    # "maxBrightnessInt":I
    .end local v6    # "rate":F
    .local v7, "rate":F
    goto :goto_1

    .line 454
    .end local v7    # "rate":F
    .restart local v6    # "rate":F
    :cond_2
    mul-float v1, v6, v0

    div-float v7, v1, v2

    .line 456
    .end local v6    # "rate":F
    .restart local v7    # "rate":F
    :goto_1
    const/16 v1, 0xfff

    const/high16 v6, 0x42200000    # 40.0f

    if-ge v3, v1, :cond_3

    cmpg-float v1, v4, v6

    if-gtz v1, :cond_3

    .line 457
    iget v1, p0, Lcom/android/server/display/RampRateController;->mScreenBrightnessMaximumInt:I

    int-to-float v1, v1

    div-float v7, v6, v1

    goto :goto_2

    .line 458
    :cond_3
    sget-boolean v1, Lcom/android/server/display/RampRateController;->IS_UMI_0B_DISPLAY_PANEL:Z

    if-eqz v1, :cond_4

    cmpg-float v1, v4, v6

    if-gtz v1, :cond_4

    .line 459
    iget v1, p0, Lcom/android/server/display/RampRateController;->mScreenBrightnessMaximumInt:I

    int-to-float v1, v1

    const/high16 v6, 0x42a00000    # 80.0f

    div-float v7, v6, v1

    .line 461
    :cond_4
    :goto_2
    iget v1, p0, Lcom/android/server/display/RampRateController;->mMinimumAdjustRate:F

    invoke-static {v7, v1}, Landroid/util/MathUtils;->max(FF)F

    move-result v1

    .line 462
    .end local v7    # "rate":F
    .local v1, "rate":F
    iget-boolean v6, p0, Lcom/android/server/display/RampRateController;->mDebug:Z

    if-eqz v6, :cond_5

    .line 463
    sget-object v6, Lcom/android/server/display/RampRateController;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDarkeningRate: rate: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", nit: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    :cond_5
    return v1
.end method

.method private getExpRate(FFFFF)F
    .locals 5
    .param p1, "startNit"    # F
    .param p2, "targetNit"    # F
    .param p3, "currentNit"    # F
    .param p4, "startTime"    # F
    .param p5, "targetTime"    # F

    .line 429
    invoke-direct {p0, p1}, Lcom/android/server/display/RampRateController;->convertToBrightness(F)F

    move-result v0

    .line 430
    .local v0, "beginDbv":F
    invoke-direct {p0, p2}, Lcom/android/server/display/RampRateController;->convertToBrightness(F)F

    move-result v1

    .line 431
    .local v1, "endDbv":F
    invoke-direct {p0, p3}, Lcom/android/server/display/RampRateController;->convertToBrightness(F)F

    move-result v2

    .line 432
    .local v2, "curDbv":F
    div-float v3, v1, v0

    invoke-static {v3}, Landroid/util/MathUtils;->log(F)F

    move-result v3

    sub-float v4, p5, p4

    div-float/2addr v3, v4

    .line 433
    .local v3, "a":F
    mul-float v4, v3, v2

    return v4
.end method

.method private getIndex(F)I
    .locals 4
    .param p1, "nit"    # F

    .line 469
    const/4 v0, 0x1

    .line 470
    .local v0, "index":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/display/RampRateController;->mNitsLevels:[F

    array-length v2, v1

    if-le v2, v0, :cond_0

    aget v1, v1, v0

    cmpg-float v1, p1, v1

    if-gez v1, :cond_0

    .line 471
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 473
    :cond_0
    iget-boolean v1, p0, Lcom/android/server/display/RampRateController;->mDebug:Z

    if-eqz v1, :cond_1

    .line 474
    sget-object v1, Lcom/android/server/display/RampRateController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getIndex: nit: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", index: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    :cond_1
    add-int/lit8 v1, v0, -0x1

    return v1
.end method

.method private getRampRate(IZFFFF)F
    .locals 4
    .param p1, "type"    # I
    .param p2, "increase"    # Z
    .param p3, "startBrightness"    # F
    .param p4, "currentBrightness"    # F
    .param p5, "targetBrightness"    # F
    .param p6, "rate"    # F

    .line 508
    invoke-direct {p0, p1}, Lcom/android/server/display/RampRateController;->getRateDuration(I)F

    move-result v0

    .line 509
    .local v0, "duration":F
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    cmpl-float v1, p5, v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 515
    :cond_0
    if-eqz p2, :cond_1

    .line 516
    invoke-direct {p0, p3, p4, p5, v0}, Lcom/android/server/display/RampRateController;->getBrighteningLogRate(FFFF)F

    move-result v1

    return v1

    .line 518
    :cond_1
    invoke-direct {p0, p3, p4, p5, v0}, Lcom/android/server/display/RampRateController;->getDarkeningExpRate(FFFF)F

    move-result v1

    return v1

    .line 510
    :cond_2
    :goto_0
    iget-boolean v1, p0, Lcom/android/server/display/RampRateController;->mDebug:Z

    if-eqz v1, :cond_3

    .line 511
    sget-object v1, Lcom/android/server/display/RampRateController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRampRate: rate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    :cond_3
    return p6
.end method

.method private getRampRate(ZFFF)F
    .locals 1
    .param p1, "increase"    # Z
    .param p2, "startBrightness"    # F
    .param p3, "currentBrightness"    # F
    .param p4, "targetBrightness"    # F

    .line 375
    if-eqz p1, :cond_0

    .line 376
    invoke-direct {p0, p3, p2, p4}, Lcom/android/server/display/RampRateController;->getBrighteningRate(FFF)F

    move-result v0

    return v0

    .line 378
    :cond_0
    invoke-direct {p0, p3}, Lcom/android/server/display/RampRateController;->getDarkeningRate(F)F

    move-result v0

    return v0
.end method

.method private getRateDuration(I)F
    .locals 1
    .param p1, "type"    # I

    .line 553
    packed-switch p1, :pswitch_data_0

    .line 564
    const/high16 v0, 0x7fc00000    # Float.NaN

    return v0

    .line 559
    :pswitch_0
    iget v0, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationHdrBrightness:I

    int-to-float v0, v0

    return v0

    .line 561
    :pswitch_1
    iget v0, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationManualBrightness:I

    int-to-float v0, v0

    return v0

    .line 557
    :pswitch_2
    iget v0, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationTemporaryDimming:I

    int-to-float v0, v0

    return v0

    .line 555
    :pswitch_3
    iget v0, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationDim:I

    int-to-float v0, v0

    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getTime(FI)F
    .locals 6
    .param p1, "nit"    # F
    .param p2, "index"    # I

    .line 480
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mALevels:[F

    aget v0, v0, p2

    .line 481
    .local v0, "a":F
    iget-object v1, p0, Lcom/android/server/display/RampRateController;->mBLevels:[F

    aget v1, v1, p2

    .line 482
    .local v1, "b":F
    div-float v2, p1, v0

    invoke-static {v2}, Landroid/util/MathUtils;->log(F)F

    move-result v2

    invoke-static {v1}, Landroid/util/MathUtils;->log(F)F

    move-result v3

    div-float/2addr v2, v3

    const/high16 v3, 0x41c00000    # 24.0f

    div-float/2addr v2, v3

    .line 483
    .local v2, "time":F
    iget-boolean v3, p0, Lcom/android/server/display/RampRateController;->mDebug:Z

    if-eqz v3, :cond_0

    .line 484
    sget-object v3, Lcom/android/server/display/RampRateController;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getTime: time: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", a: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", b: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    :cond_0
    invoke-static {v2}, Landroid/util/MathUtils;->abs(F)F

    move-result v3

    return v3
.end method

.method private reasonToString(I)Ljava/lang/String;
    .locals 1
    .param p1, "reason"    # I

    .line 655
    packed-switch p1, :pswitch_data_0

    .line 683
    const-string/jumbo v0, "unknown"

    return-object v0

    .line 681
    :pswitch_0
    const-string v0, "doze_rate"

    return-object v0

    .line 679
    :pswitch_1
    const-string v0, "bcbc_rate"

    return-object v0

    .line 677
    :pswitch_2
    const-string/jumbo v0, "thermal_rate"

    return-object v0

    .line 675
    :pswitch_3
    const-string v0, "manual_brightness_rate"

    return-object v0

    .line 673
    :pswitch_4
    const-string v0, "auto_brightness_rate"

    return-object v0

    .line 671
    :pswitch_5
    const-string v0, "fast_brightness_adj"

    return-object v0

    .line 669
    :pswitch_6
    const-string v0, "hdr_brightness_rate"

    return-object v0

    .line 667
    :pswitch_7
    const-string v0, "override_brightness_rate"

    return-object v0

    .line 665
    :pswitch_8
    const-string/jumbo v0, "temporary_brightness_rate"

    return-object v0

    .line 663
    :pswitch_9
    const-string v0, "dim_rate"

    return-object v0

    .line 661
    :pswitch_a
    const-string v0, "fast_rate"

    return-object v0

    .line 659
    :pswitch_b
    const-string/jumbo v0, "slow_fast"

    return-object v0

    .line 657
    :pswitch_c
    const-string/jumbo v0, "zero_rate"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateDeviceConfigData()V
    .locals 3

    .line 490
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mHighBrightnessModeData:Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/server/display/RampRateController;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v1, :cond_0

    .line 491
    iget v0, v0, Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;->transitionPoint:F

    invoke-direct {p0, v0}, Lcom/android/server/display/RampRateController;->convertToNit(F)F

    move-result v0

    iput v0, p0, Lcom/android/server/display/RampRateController;->mHbmTransitionPointNit:F

    .line 492
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lcom/android/server/display/RampRateController;->convertToNit(F)F

    move-result v0

    iput v0, p0, Lcom/android/server/display/RampRateController;->mBrightnessMaxNit:F

    .line 493
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getMaxHbmBrightnessForPeak()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/display/RampRateController;->convertToNit(F)F

    move-result v0

    iput v0, p0, Lcom/android/server/display/RampRateController;->mHbmMaxNit:F

    goto :goto_1

    .line 495
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v0}, Lcom/android/server/display/DisplayDeviceConfig;->getNits()[F

    move-result-object v0

    .line 496
    .local v0, "nits":[F
    if-eqz v0, :cond_1

    array-length v1, v0

    if-eqz v1, :cond_1

    .line 497
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget v1, v0, v1

    iput v1, p0, Lcom/android/server/display/RampRateController;->mBrightnessMaxNit:F

    goto :goto_0

    .line 499
    :cond_1
    const/high16 v1, 0x43fa0000    # 500.0f

    iput v1, p0, Lcom/android/server/display/RampRateController;->mBrightnessMaxNit:F

    .line 500
    sget-object v1, Lcom/android/server/display/RampRateController;->TAG:Ljava/lang/String;

    const-string v2, "The max nit of the device is not adapted."

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    :goto_0
    iget v1, p0, Lcom/android/server/display/RampRateController;->mBrightnessMaxNit:F

    iput v1, p0, Lcom/android/server/display/RampRateController;->mHbmMaxNit:F

    .line 504
    .end local v0    # "nits":[F
    :goto_1
    return-void
.end method

.method private updateRatePriority(Lcom/android/server/display/brightness/BrightnessReason;Lcom/android/server/display/brightness/BrightnessReason;FFZZ)V
    .locals 20
    .param p1, "reasonTemp"    # Lcom/android/server/display/brightness/BrightnessReason;
    .param p2, "reason"    # Lcom/android/server/display/brightness/BrightnessReason;
    .param p3, "targetBrightness"    # F
    .param p4, "targetSdrBrightness"    # F
    .param p5, "animating"    # Z
    .param p6, "readyToAnimate"    # Z

    .line 181
    move-object/from16 v0, p0

    move/from16 v1, p3

    move/from16 v2, p4

    iget v3, v0, Lcom/android/server/display/RampRateController;->mCurrentPolicy:I

    const/4 v5, 0x1

    const/4 v6, 0x3

    if-ne v3, v6, :cond_0

    move v7, v5

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    .line 182
    .local v7, "isPolicyBright":Z
    :goto_0
    const/4 v8, 0x2

    if-ne v3, v8, :cond_1

    move v3, v5

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    .line 183
    .local v3, "isPolicyDim":Z
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/display/brightness/BrightnessReason;->getReason()I

    move-result v9

    if-ne v9, v5, :cond_2

    move v9, v5

    goto :goto_2

    :cond_2
    const/4 v9, 0x0

    .line 184
    .local v9, "isReasonManual":Z
    :goto_2
    invoke-virtual/range {p2 .. p2}, Lcom/android/server/display/brightness/BrightnessReason;->getModifier()I

    move-result v10

    const/16 v11, 0x10

    and-int/2addr v10, v11

    if-eqz v10, :cond_3

    move v10, v5

    goto :goto_3

    :cond_3
    const/4 v10, 0x0

    .line 185
    .local v10, "isModifierBcbc":Z
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/display/brightness/BrightnessReason;->getModifier()I

    move-result v12

    and-int/2addr v12, v11

    if-eqz v12, :cond_4

    move v12, v5

    goto :goto_4

    :cond_4
    const/4 v12, 0x0

    .line 186
    .local v12, "isModifierTempBcbc":Z
    :goto_4
    invoke-virtual/range {p2 .. p2}, Lcom/android/server/display/brightness/BrightnessReason;->getModifier()I

    move-result v13

    const/16 v14, 0x40

    and-int/2addr v13, v14

    if-eqz v13, :cond_5

    move v13, v5

    goto :goto_5

    :cond_5
    const/4 v13, 0x0

    .line 187
    .local v13, "isModifierThermal":Z
    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/display/brightness/BrightnessReason;->getModifier()I

    move-result v15

    and-int/2addr v15, v14

    if-eqz v15, :cond_6

    move v15, v5

    goto :goto_6

    :cond_6
    const/4 v15, 0x0

    .line 188
    .local v15, "isModifierTempThermal":Z
    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/display/brightness/BrightnessReason;->getModifier()I

    move-result v16

    const/4 v4, 0x4

    and-int/lit8 v16, v16, 0x4

    if-eqz v16, :cond_7

    move/from16 v16, v5

    goto :goto_7

    :cond_7
    const/16 v16, 0x0

    .line 189
    .local v16, "isModifierTempHdr":Z
    :goto_7
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/display/brightness/BrightnessReason;->getReason()I

    move-result v14

    const/4 v4, 0x6

    if-ne v14, v4, :cond_8

    move v4, v5

    goto :goto_8

    :cond_8
    const/4 v4, 0x0

    .line 190
    .local v4, "isReasonTempOverride":Z
    :goto_8
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/display/brightness/BrightnessReason;->getReason()I

    move-result v14

    const/4 v11, 0x7

    if-ne v14, v11, :cond_9

    move/from16 v17, v5

    goto :goto_9

    :cond_9
    const/16 v17, 0x0

    :goto_9
    move/from16 v11, v17

    .line 192
    .local v11, "isReasonTemporary":Z
    if-eqz v7, :cond_a

    if-nez v12, :cond_b

    if-nez v10, :cond_b

    :cond_a
    iget-object v14, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    .line 194
    invoke-static {v14}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedBcbcDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v14

    if-eqz v14, :cond_e

    .line 195
    :cond_b
    iget-boolean v14, v0, Lcom/android/server/display/RampRateController;->mBrightnessChanged:Z

    if-nez v14, :cond_c

    if-nez p5, :cond_c

    .line 196
    iget-object v14, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v14, v5}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$maddRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    goto :goto_a

    .line 197
    :cond_c
    iget-object v14, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v14}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedBcbcDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v14

    if-eqz v14, :cond_e

    iget-boolean v14, v0, Lcom/android/server/display/RampRateController;->mBrightnessChanged:Z

    if-nez v14, :cond_d

    if-eqz v9, :cond_e

    .line 199
    :cond_d
    iget-object v14, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v14, v5}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 203
    :cond_e
    :goto_a
    if-eqz v7, :cond_f

    if-nez v15, :cond_10

    if-nez v13, :cond_10

    :cond_f
    iget-object v14, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    .line 205
    invoke-static {v14}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedThermalDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v14

    if-eqz v14, :cond_13

    .line 206
    :cond_10
    iget-boolean v14, v0, Lcom/android/server/display/RampRateController;->mBrightnessChanged:Z

    if-nez v14, :cond_11

    iget-boolean v14, v0, Lcom/android/server/display/RampRateController;->mAnimateValueChanged:Z

    if-nez v14, :cond_11

    if-nez p5, :cond_11

    .line 207
    iget-object v14, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v14, v5}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 208
    iget-object v5, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v5, v8}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$maddRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    goto :goto_b

    .line 209
    :cond_11
    iget-object v5, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v5}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedThermalDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v5

    if-eqz v5, :cond_13

    iget-boolean v5, v0, Lcom/android/server/display/RampRateController;->mBrightnessChanged:Z

    if-nez v5, :cond_12

    iget-boolean v5, v0, Lcom/android/server/display/RampRateController;->mAnimateValueChanged:Z

    if-eqz v5, :cond_13

    .line 211
    :cond_12
    iget-object v5, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v5, v8}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 215
    :cond_13
    :goto_b
    if-eqz v7, :cond_15

    iget-object v5, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    .line 216
    invoke-static {v5}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedHdrDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v5

    if-nez v5, :cond_14

    if-eqz v16, :cond_15

    iget-boolean v5, v0, Lcom/android/server/display/RampRateController;->mAnimateValueChanged:Z

    if-eqz v5, :cond_15

    .line 218
    :cond_14
    iget-object v5, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v5, v6}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 220
    iget-object v5, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v6, 0x8

    invoke-static {v5, v6}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$maddRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 223
    :cond_15
    if-eqz v7, :cond_17

    iget-object v5, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    .line 224
    invoke-static {v5}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedOverrideDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v5

    if-nez v5, :cond_16

    if-eqz v4, :cond_17

    iget-boolean v5, v0, Lcom/android/server/display/RampRateController;->mBrightnessChanged:Z

    if-eqz v5, :cond_17

    .line 226
    :cond_16
    iget-object v5, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v6, 0xb

    invoke-static {v5, v6}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 229
    iget-object v5, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v6, 0x10

    invoke-static {v5, v6}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$maddRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 232
    :cond_17
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    .line 233
    .local v5, "currentTime":J
    if-eqz v7, :cond_18

    iget v14, v0, Lcom/android/server/display/RampRateController;->mPreviousDisplayState:I

    .line 234
    invoke-static {v14}, Landroid/view/Display;->isDozeState(I)Z

    move-result v14

    if-eqz v14, :cond_18

    iget v14, v0, Lcom/android/server/display/RampRateController;->mBrightness:F

    cmpl-float v14, v1, v14

    if-gtz v14, :cond_19

    :cond_18
    iget-object v14, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    .line 236
    invoke-static {v14, v5, v6}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedDozeDimming(Lcom/android/server/display/RampRateController$RateStateRecord;J)Z

    move-result v14

    if-eqz v14, :cond_1a

    .line 237
    :cond_19
    iget-object v14, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v8, 0x1b

    invoke-static {v14, v8}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 241
    if-eqz p6, :cond_1b

    .line 242
    iget-object v8, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v8, v5, v6}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetDozeDimmingTimeMills(Lcom/android/server/display/RampRateController$RateStateRecord;J)V

    .line 243
    iget-object v8, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/4 v14, 0x4

    invoke-static {v8, v14}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$maddRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    goto :goto_c

    .line 245
    :cond_1a
    const/4 v14, 0x4

    iget-object v8, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v8}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedDozeDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v8

    if-eqz v8, :cond_1b

    .line 246
    iget-object v8, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v8, v14}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 249
    :cond_1b
    :goto_c
    if-eqz v7, :cond_1c

    if-nez v11, :cond_1d

    :cond_1c
    iget-object v8, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    .line 250
    invoke-static {v8}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedTemporaryDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v8

    if-eqz v8, :cond_20

    .line 251
    :cond_1d
    move v8, v4

    move-wide/from16 v18, v5

    .end local v4    # "isReasonTempOverride":Z
    .end local v5    # "currentTime":J
    .local v8, "isReasonTempOverride":Z
    .local v18, "currentTime":J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 252
    .local v4, "now":J
    iget-object v6, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v14, 0x1f

    invoke-static {v6, v14}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 257
    iget-boolean v6, v0, Lcom/android/server/display/RampRateController;->mSupportManualDimming:Z

    if-eqz v6, :cond_21

    .line 258
    const/16 v6, 0x20

    if-nez v11, :cond_1f

    .line 259
    iget-object v14, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v14, v4, v5, v1, v2}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedTemporaryDimming(Lcom/android/server/display/RampRateController$RateStateRecord;JFF)Z

    move-result v14

    if-eqz v14, :cond_1e

    .line 261
    iget-object v14, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v14, v6}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$maddRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    goto :goto_d

    .line 263
    :cond_1e
    iget-object v14, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v14, v6}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    goto :goto_d

    .line 266
    :cond_1f
    iget-object v14, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v14, v4, v5}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetStartTemporaryDimmingTimeMills(Lcom/android/server/display/RampRateController$RateStateRecord;J)V

    .line 267
    iget-object v14, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v14, v6}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$maddRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    goto :goto_d

    .line 250
    .end local v8    # "isReasonTempOverride":Z
    .end local v18    # "currentTime":J
    .local v4, "isReasonTempOverride":Z
    .restart local v5    # "currentTime":J
    :cond_20
    move v8, v4

    move-wide/from16 v18, v5

    .line 272
    .end local v4    # "isReasonTempOverride":Z
    .end local v5    # "currentTime":J
    .restart local v8    # "isReasonTempOverride":Z
    .restart local v18    # "currentTime":J
    :cond_21
    :goto_d
    if-nez v3, :cond_23

    iget v4, v0, Lcom/android/server/display/RampRateController;->mPreviousPolicy:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_22

    if-eqz v7, :cond_22

    goto :goto_e

    .line 281
    :cond_22
    iget-object v4, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v4}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedDimDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 282
    iget-object v4, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v5, 0x40

    invoke-static {v4, v5}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    goto :goto_f

    .line 274
    :cond_23
    :goto_e
    iget-object v4, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v5, 0x3f

    invoke-static {v4, v5}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 280
    iget-object v4, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v5, 0x40

    invoke-static {v4, v5}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$maddRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 285
    :cond_24
    :goto_f
    const/4 v4, 0x0

    cmpl-float v5, v1, v4

    if-eqz v5, :cond_25

    cmpl-float v5, v2, v4

    if-eqz v5, :cond_25

    iget v5, v0, Lcom/android/server/display/RampRateController;->mBrightness:F

    cmpl-float v5, v5, v4

    if-eqz v5, :cond_25

    iget v5, v0, Lcom/android/server/display/RampRateController;->mSdrBrightness:F

    cmpl-float v4, v5, v4

    if-nez v4, :cond_26

    .line 287
    :cond_25
    iget-object v4, v0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v5, 0x7f

    invoke-static {v4, v5}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 289
    :cond_26
    return-void
.end method


# virtual methods
.method protected addHdrRateModifier(Z)V
    .locals 2
    .param p1, "isEntry"    # Z

    .line 635
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v1, 0x7f

    invoke-static {v0, v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 636
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$maddRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 637
    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 638
    iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedHdrRateDueToEntry:Z

    goto :goto_0

    .line 640
    :cond_0
    iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedHdrRateDueToExit:Z

    .line 642
    :goto_0
    return-void
.end method

.method protected addOverrideRateModifier(Z)V
    .locals 2
    .param p1, "isEntry"    # Z

    .line 645
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v1, 0x7f

    invoke-static {v0, v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 646
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$maddRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 647
    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 648
    iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedOverrideRateDueToEntry:Z

    goto :goto_0

    .line 650
    :cond_0
    iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedOverrideRateDueToExit:Z

    .line 652
    :goto_0
    return-void
.end method

.method protected clearAllRateModifier()V
    .locals 2

    .line 631
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v1, 0x7f

    invoke-static {v0, v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 632
    return-void
.end method

.method protected clearBcbcModifier()V
    .locals 2

    .line 615
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 616
    return-void
.end method

.method protected clearThermalModifier()V
    .locals 2

    .line 619
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mclearRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 620
    return-void
.end method

.method protected dump(Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 688
    sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_DPC:Z

    iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mDebug:Z

    .line 689
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 690
    const-string v0, "Ramp Rate Controller:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 691
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mRateType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mgetRateType(Lcom/android/server/display/RampRateController$RateStateRecord;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 692
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCurrentRateReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$fgetSCREEN_BRIGHTNESS_FLOAT_NAME(Lcom/android/server/display/RampRateController$RateStateRecord;)Ljava/lang/String;

    move-result-object v2

    .line 693
    invoke-static {v1, v2}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mgetCurrentRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 692
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 694
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mPreviousRateReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$fgetSCREEN_BRIGHTNESS_FLOAT_NAME(Lcom/android/server/display/RampRateController$RateStateRecord;)Ljava/lang/String;

    move-result-object v2

    .line 695
    invoke-static {v1, v2}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mgetPreviousRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 694
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 696
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCurrentSdrRateReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$fgetSCREEN_SDR_BRIGHTNESS_FLOAT_NAME(Lcom/android/server/display/RampRateController$RateStateRecord;)Ljava/lang/String;

    move-result-object v2

    .line 697
    invoke-static {v1, v2}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mgetCurrentRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 696
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 698
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mPreviousSdrRateReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$fgetSCREEN_SDR_BRIGHTNESS_FLOAT_NAME(Lcom/android/server/display/RampRateController$RateStateRecord;)Ljava/lang/String;

    move-result-object v2

    .line 699
    invoke-static {v1, v2}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mgetPreviousRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 698
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 700
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mRateModifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mgetRateModifier(Lcom/android/server/display/RampRateController$RateStateRecord;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 701
    return-void
.end method

.method protected isTemporaryDimming()Z
    .locals 1

    .line 611
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v0}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedTemporaryDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v0

    return v0
.end method

.method protected onAnimateValueChanged(ZZ)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "isAnimating"    # Z

    .line 595
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    iget-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedHdrRateDueToEntry:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedHdrRateDueToExit:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    .line 598
    invoke-static {v0}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedHdrDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 599
    invoke-direct {p0}, Lcom/android/server/display/RampRateController;->clearHdrRateModifier()V

    .line 601
    :cond_1
    iput-boolean p1, p0, Lcom/android/server/display/RampRateController;->mAnimateValueChanged:Z

    .line 602
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedHdrRateDueToEntry:Z

    .line 603
    iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedHdrRateDueToExit:Z

    .line 604
    return-void
.end method

.method protected onBrightnessChanged(ZZ)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "isAnimating"    # Z

    .line 583
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    iget-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedOverrideRateDueToEntry:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedOverrideRateDueToExit:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    .line 586
    invoke-static {v0}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedOverrideDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 587
    invoke-direct {p0}, Lcom/android/server/display/RampRateController;->clearOverrideRateModifier()V

    .line 589
    :cond_1
    iput-boolean p1, p0, Lcom/android/server/display/RampRateController;->mBrightnessChanged:Z

    .line 590
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedOverrideRateDueToEntry:Z

    .line 591
    iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedOverrideRateDueToExit:Z

    .line 592
    return-void
.end method

.method protected updateBrightnessRate(Ljava/lang/String;FFF)F
    .locals 11
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "currentBrightness"    # F
    .param p3, "targetBrightness"    # F
    .param p4, "rate"    # F

    .line 293
    move v0, p4

    .line 294
    .local v0, "finalRate":F
    iget v1, p0, Lcom/android/server/display/RampRateController;->mBrightness:F

    .line 295
    .local v1, "startBrightness":F
    sget-object v2, Lcom/android/server/display/DisplayPowerState;->SCREEN_SDR_BRIGHTNESS_FLOAT:Landroid/util/FloatProperty;

    invoke-virtual {v2}, Landroid/util/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 296
    iget v1, p0, Lcom/android/server/display/RampRateController;->mSdrBrightness:F

    .line 298
    :cond_0
    cmpl-float v2, p3, v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-lez v2, :cond_1

    move v2, v3

    goto :goto_0

    :cond_1
    move v2, v4

    :goto_0
    move v9, v2

    .line 299
    .local v9, "increase":Z
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mresetRateType(Lcom/android/server/display/RampRateController$RateStateRecord;)V

    .line 300
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mgetCurrentRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;Ljava/lang/String;)I

    move-result v5

    invoke-static {v2, v5, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetPreviousRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    .line 301
    iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mSlowChange:Z

    const/4 v5, 0x2

    if-eqz v2, :cond_2

    .line 302
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2, v3, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetCurrentRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    goto :goto_1

    .line 304
    :cond_2
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2, v5, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetCurrentRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    .line 307
    :goto_1
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedDimDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v2

    const/4 v6, 0x0

    const/4 v7, 0x3

    if-eqz v2, :cond_3

    .line 308
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2, v3}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetRateTypeIfNeeded(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 309
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2, v7, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetRateReasonIfNeeded(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    goto/16 :goto_2

    .line 310
    :cond_3
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedDozeDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 311
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v3, 0xc

    invoke-static {v2, v3, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetCurrentRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    .line 312
    sub-float v2, p3, v1

    const/high16 v3, 0x447a0000    # 1000.0f

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationDozeDimming:I

    int-to-float v3, v3

    div-float v0, v2, v3

    goto/16 :goto_2

    .line 313
    :cond_4
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedTemporaryDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v2

    const/4 v3, 0x4

    if-eqz v2, :cond_5

    .line 314
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2, v4, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetCurrentRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    .line 315
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2, v5}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetRateTypeIfNeeded(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 316
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2, v3, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetRateReasonIfNeeded(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    goto/16 :goto_2

    .line 317
    :cond_5
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedOverrideDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 318
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2, v7}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetRateTypeIfNeeded(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 319
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/4 v3, 0x5

    invoke-static {v2, v3, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetRateReasonIfNeeded(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    goto/16 :goto_2

    .line 320
    :cond_6
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedHdrDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 321
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2, v3}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetRateType(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 322
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/4 v3, 0x6

    invoke-static {v2, v3, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetCurrentRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    goto :goto_2

    .line 323
    :cond_7
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedThermalDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 324
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v3, 0xa

    invoke-static {v2, v3, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetCurrentRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    .line 325
    iget v0, p0, Lcom/android/server/display/RampRateController;->mSlowLinearRampRate:F

    goto :goto_2

    .line 326
    :cond_8
    iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mSlowChange:Z

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mappliedBcbcDimming(Lcom/android/server/display/RampRateController$RateStateRecord;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 327
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v3, 0xb

    invoke-static {v2, v3, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetCurrentRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    .line 328
    iget v0, p0, Lcom/android/server/display/RampRateController;->mSlowLinearRampRate:F

    goto :goto_2

    .line 329
    :cond_9
    iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mSlowChange:Z

    if-eqz v2, :cond_a

    invoke-direct {p0, p2, p3}, Lcom/android/server/display/RampRateController;->appliedFastRate(FF)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 330
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2, v7}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetRateTypeIfNeeded(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 331
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/4 v3, 0x7

    invoke-static {v2, v3, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetCurrentRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    goto :goto_2

    .line 332
    :cond_a
    iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mSlowChange:Z

    if-eqz v2, :cond_b

    cmpl-float v2, v0, v6

    if-eqz v2, :cond_b

    .line 333
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v3, 0x8

    invoke-static {v2, v3, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetCurrentRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    .line 334
    invoke-direct {p0, v9, v1, p2, p3}, Lcom/android/server/display/RampRateController;->getRampRate(ZFFF)F

    move-result v0

    goto :goto_2

    .line 335
    :cond_b
    cmpl-float v2, v0, v6

    if-eqz v2, :cond_c

    .line 336
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2, v7}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetRateTypeIfNeeded(Lcom/android/server/display/RampRateController$RateStateRecord;I)V

    .line 337
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    const/16 v3, 0x9

    invoke-static {v2, v3, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetRateReasonIfNeeded(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    goto :goto_2

    .line 339
    :cond_c
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2, v4, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$msetCurrentRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;ILjava/lang/String;)V

    .line 344
    :goto_2
    invoke-static {p2}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v2

    .line 345
    invoke-static {p3}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v3

    const-string/jumbo v10, "updateBrightnessRate: "

    if-ne v2, v3, :cond_f

    .line 346
    cmpl-float v2, v0, v6

    if-eqz v2, :cond_f

    .line 347
    iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mSlowChange:Z

    if-eqz v2, :cond_d

    iget v2, p0, Lcom/android/server/display/RampRateController;->mBrightnessRampRateSlow:F

    goto :goto_3

    :cond_d
    iget v2, p0, Lcom/android/server/display/RampRateController;->mBrightnessRampRateFast:F

    :goto_3
    move v0, v2

    .line 348
    iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mDebug:Z

    if-eqz v2, :cond_e

    .line 349
    sget-object v2, Lcom/android/server/display/RampRateController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": using current rate to avoid frequent animation execution: rate: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", currentBrightness: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", targetBrightness: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    :cond_e
    return v0

    .line 359
    :cond_f
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mgetRateType(Lcom/android/server/display/RampRateController$RateStateRecord;)I

    move-result v3

    move-object v2, p0

    move v4, v9

    move v5, v1

    move v6, p2

    move v7, p3

    move v8, v0

    invoke-direct/range {v2 .. v8}, Lcom/android/server/display/RampRateController;->getRampRate(IZFFFF)F

    move-result v0

    .line 361
    iget-object v2, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v2, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mgetPreviousRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;Ljava/lang/String;)I

    move-result v2

    .line 362
    .local v2, "previousRateReason":I
    iget-object v3, p0, Lcom/android/server/display/RampRateController;->mRateRecord:Lcom/android/server/display/RampRateController$RateStateRecord;

    invoke-static {v3, p1}, Lcom/android/server/display/RampRateController$RateStateRecord;->-$$Nest$mgetCurrentRateReason(Lcom/android/server/display/RampRateController$RateStateRecord;Ljava/lang/String;)I

    move-result v3

    .line 363
    .local v3, "currentRateReason":I
    if-ne v3, v2, :cond_10

    iget-boolean v4, p0, Lcom/android/server/display/RampRateController;->mDebug:Z

    if-eqz v4, :cond_11

    .line 364
    :cond_10
    sget-object v4, Lcom/android/server/display/RampRateController;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": brightness rate changing from ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 366
    invoke-direct {p0, v2}, Lcom/android/server/display/RampRateController;->reasonToString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] to ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 367
    invoke-direct {p0, v3}, Lcom/android/server/display/RampRateController;->reasonToString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], rate: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 364
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    :cond_11
    return v0
.end method

.method protected updateBrightnessState(ZZZFFFFIIILcom/android/server/display/brightness/BrightnessReason;Lcom/android/server/display/brightness/BrightnessReason;)V
    .locals 16
    .param p1, "animating"    # Z
    .param p2, "readyToAnimate"    # Z
    .param p3, "slowChange"    # Z
    .param p4, "brightness"    # F
    .param p5, "sdrBrightness"    # F
    .param p6, "targetBrightness"    # F
    .param p7, "targetSdrBrightness"    # F
    .param p8, "currentPolicy"    # I
    .param p9, "previousPolicy"    # I
    .param p10, "oldDisplayState"    # I
    .param p11, "reasonTemp"    # Lcom/android/server/display/brightness/BrightnessReason;
    .param p12, "reason"    # Lcom/android/server/display/brightness/BrightnessReason;

    .line 164
    move-object/from16 v7, p0

    move/from16 v8, p3

    iput-boolean v8, v7, Lcom/android/server/display/RampRateController;->mSlowChange:Z

    .line 165
    move/from16 v9, p4

    iput v9, v7, Lcom/android/server/display/RampRateController;->mBrightness:F

    .line 166
    move/from16 v10, p5

    iput v10, v7, Lcom/android/server/display/RampRateController;->mSdrBrightness:F

    .line 167
    move/from16 v11, p6

    iput v11, v7, Lcom/android/server/display/RampRateController;->mTargetBrightness:F

    .line 168
    move/from16 v12, p7

    iput v12, v7, Lcom/android/server/display/RampRateController;->mTargetSdrBrightness:F

    .line 169
    move/from16 v13, p8

    iput v13, v7, Lcom/android/server/display/RampRateController;->mCurrentPolicy:I

    .line 170
    move/from16 v14, p9

    iput v14, v7, Lcom/android/server/display/RampRateController;->mPreviousPolicy:I

    .line 171
    move/from16 v15, p10

    iput v15, v7, Lcom/android/server/display/RampRateController;->mPreviousDisplayState:I

    .line 172
    iget-object v0, v7, Lcom/android/server/display/RampRateController;->mBrightnessReasonTemp:Lcom/android/server/display/brightness/BrightnessReason;

    move-object/from16 v6, p11

    invoke-virtual {v0, v6}, Lcom/android/server/display/brightness/BrightnessReason;->set(Lcom/android/server/display/brightness/BrightnessReason;)V

    .line 173
    iget-object v0, v7, Lcom/android/server/display/RampRateController;->mBrightnessReason:Lcom/android/server/display/brightness/BrightnessReason;

    move-object/from16 v5, p12

    invoke-virtual {v0, v5}, Lcom/android/server/display/brightness/BrightnessReason;->set(Lcom/android/server/display/brightness/BrightnessReason;)V

    .line 174
    move-object/from16 v0, p0

    move-object/from16 v1, p11

    move-object/from16 v2, p12

    move/from16 v3, p6

    move/from16 v4, p7

    move/from16 v5, p1

    move/from16 v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/android/server/display/RampRateController;->updateRatePriority(Lcom/android/server/display/brightness/BrightnessReason;Lcom/android/server/display/brightness/BrightnessReason;FFZZ)V

    .line 176
    return-void
.end method
