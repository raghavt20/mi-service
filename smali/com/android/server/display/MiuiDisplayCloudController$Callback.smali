.class public interface abstract Lcom/android/server/display/MiuiDisplayCloudController$Callback;
.super Ljava/lang/Object;
.source "MiuiDisplayCloudController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/MiuiDisplayCloudController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation


# virtual methods
.method public abstract notifyDisableResetShortTermModel(Z)V
.end method

.method public abstract notifyThresholdSunlightNitChanged(F)V
.end method

.method public abstract updateOutdoorThermalAppCategoryList(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method
