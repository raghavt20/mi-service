.class Lcom/android/server/display/DisplayPowerControllerImpl$4;
.super Ljava/lang/Object;
.source "DisplayPowerControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/display/DisplayPowerControllerImpl;->updateForegroundApp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/DisplayPowerControllerImpl;


# direct methods
.method constructor <init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/DisplayPowerControllerImpl;

    .line 1683
    iput-object p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl$4;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1687
    :try_start_0
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$4;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmActivityTaskManager(Lcom/android/server/display/DisplayPowerControllerImpl;)Landroid/app/IActivityTaskManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/app/IActivityTaskManager;->getFocusedRootTaskInfo()Landroid/app/ActivityTaskManager$RootTaskInfo;

    move-result-object v0

    .line 1688
    .local v0, "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    if-eqz v0, :cond_4

    iget-object v1, v0, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-nez v1, :cond_0

    goto :goto_1

    .line 1691
    :cond_0
    invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_3

    .line 1692
    invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I

    move-result v1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl$4;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmActivityTaskManager(Lcom/android/server/display/DisplayPowerControllerImpl;)Landroid/app/IActivityTaskManager;

    move-result-object v1

    .line 1693
    invoke-interface {v1}, Landroid/app/IActivityTaskManager;->isInSplitScreenWindowingMode()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 1696
    :cond_1
    iget-object v1, v0, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 1698
    .local v1, "packageName":Ljava/lang/String;
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl$4;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmForegroundAppPackageName(Lcom/android/server/display/DisplayPowerControllerImpl;)Ljava/lang/String;

    move-result-object v2

    .line 1699
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1700
    return-void

    .line 1702
    :cond_2
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl$4;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v2, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fputmPendingForegroundAppPackageName(Lcom/android/server/display/DisplayPowerControllerImpl;Ljava/lang/String;)V

    .line 1703
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl$4;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmHandler(Lcom/android/server/display/DisplayPowerControllerImpl;)Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1706
    nop

    .end local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    .end local v1    # "packageName":Ljava/lang/String;
    goto :goto_2

    .line 1694
    .restart local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :cond_3
    :goto_0
    return-void

    .line 1689
    :cond_4
    :goto_1
    return-void

    .line 1704
    .end local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :catch_0
    move-exception v0

    .line 1707
    :goto_2
    return-void
.end method
