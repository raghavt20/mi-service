public class com.android.server.display.DisplayPowerControllerImpl implements com.android.server.display.DisplayPowerControllerStub implements com.android.server.display.SunlightController$Callback implements com.android.server.display.MiuiDisplayCloudController$Callback {
	 /* .source "DisplayPowerControllerImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;, */
	 /* Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;, */
	 /* Lcom/android/server/display/DisplayPowerControllerImpl$TaskStackListenerImpl;, */
	 /* Lcom/android/server/display/DisplayPowerControllerImpl$RotationWatcher;, */
	 /* Lcom/android/server/display/DisplayPowerControllerImpl$Injector;, */
	 /* Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
private static Boolean BCBC_ENABLE;
private static final Integer BCBC_STATE_DISABLE;
private static final Integer BCBC_STATE_ENABLE;
private static final Float COEFFICIENT;
private static final Integer CURRENT_GRAYSCALE_UPDATE_DISABLED;
private static final Integer CURRENT_GRAYSCALE_UPDATE_ENABLED;
private static final DATA_D;
private static final Long DELAY_TIME;
private static final Integer DISPLAY_DIM_STATE;
private static final Float DOLBY_PREVIEW_DEFAULT_BOOST_RATIO;
private static final Float DOLBY_PREVIEW_MAX_BOOST_RATIO;
private static final Float DOZE_HBM_NIT_DEFAULT;
private static final Float DOZE_LBM_NIT_DEFAULT;
private static final Integer DOZE_LIGHT_LOW;
public static final Integer EPSILON;
private static final Float HBM_MINIMUM_LUX;
private static final Boolean IS_FOLDABLE_DEVICE;
private static final java.lang.String KEY_CURTAIN_ANIM_ENABLED;
private static final java.lang.String KEY_IS_DYNAMIC_LOCK_SCREEN_SHOW;
private static final java.lang.String KEY_SUNLIGHT_MODE_AVAILABLE;
private static final Float MAX_A;
private static final Float MAX_DIFF;
public static final Float MAX_GALLERY_HDR_FACTOR;
private static final Float MAX_HBM_BRIGHTNESS_FOR_PEAK;
private static final Float MAX_POWER_SAVE_MODE_NIT;
public static final Float MIN_GALLERY_HDR_FACTOR;
private static final Integer MSG_UPDATE_CURRENT_GRAY_SCALE;
private static final Integer MSG_UPDATE_DOLBY_STATE;
private static final Integer MSG_UPDATE_FOREGROUND_APP;
private static final Integer MSG_UPDATE_FOREGROUND_APP_SYNC;
private static final Integer MSG_UPDATE_GRAY_SCALE;
private static final Integer MSG_UPDATE_ROTATION;
private static final Integer MSG_UPDATE_THERMAL_MAX_BRIGHTNESS;
private static final java.lang.String PACKAGE_DIM_SYSTEM;
private static final Integer PEAK_BRIGHTNESS_AMBIENT_LUX_THRESHOLD;
private static final Integer PEAK_BRIGHTNESS_GRAY_SCALE_THRESHOLD;
private static final Boolean SUPPORT_BCBC_BY_AMBIENT_LUX;
private static final Boolean SUPPORT_IDLE_DIM;
private static final Boolean SUPPORT_MULTIPLE_AOD_BRIGHTNESS;
private static final android.content.res.Resources SYSTEM_RESOURCES;
private static final java.lang.String TAG;
private static final Integer TRANSACTION_NOTIFY_BRIGHTNESS;
private static final Integer TRANSACTION_NOTIFY_DIM;
private static final Float V1;
private static final Float V2;
private static final mBCBCLuxThreshold;
private static final mBCBCNitDecreaseThreshold;
private static final Boolean mSupportGalleryHdr;
/* # instance fields */
private final Boolean SUPPORT_DOLBY_VERSION_BRIGHTEN;
private final Boolean SUPPORT_HDR_HBM_BRIGHTEN;
private final Boolean SUPPORT_MANUAL_BRIGHTNESS_BOOST;
private final Boolean SUPPORT_MANUAL_DIMMING;
private android.app.IActivityTaskManager mActivityTaskManager;
private Float mActualScreenOnBrightness;
private Boolean mAppliedBcbc;
private Boolean mAppliedDimming;
private Boolean mAppliedLowPower;
private Float mAppliedMaxOprBrightness;
private Boolean mAppliedScreenBrightnessOverride;
private Boolean mAppliedSunlightMode;
private Boolean mAutoBrightnessEnable;
private com.android.server.display.AutomaticBrightnessControllerImpl mAutomaticBrightnessControllerImpl;
private Integer mBCBCState;
private Float mBasedBrightness;
private Float mBasedSdrBrightness;
private com.android.server.display.DisplayPowerControllerImpl$BatteryReceiver mBatteryReceiver;
private Float mBcbcBrightness;
private Boolean mBootCompleted;
private com.android.server.display.BrightnessABTester mBrightnessABTester;
private com.android.server.display.statistics.BrightnessDataProcessor mBrightnessDataProcessor;
private com.android.server.display.BrightnessMappingStrategy mBrightnessMapper;
private com.android.server.display.RampAnimator$DualRampAnimator mBrightnessRampAnimator;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/android/server/display/RampAnimator$DualRampAnimator<", */
/* "Lcom/android/server/display/DisplayPowerState;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.display.aiautobrt.CustomBrightnessModeController mCbmController;
private com.android.server.display.AutomaticBrightnessControllerImpl$CloudControllerListener mCloudListener;
private Boolean mColorInversionEnabled;
private android.content.Context mContext;
private Float mCurrentBrightness;
private Integer mCurrentConditionId;
private Integer mCurrentDisplayPolicy;
private Float mCurrentGalleryHdrBoostFactor;
private Float mCurrentGrayScale;
private Float mCurrentSdrBrightness;
private Float mCurrentTemperature;
private Boolean mCurtainAnimationAvailable;
private Boolean mCurtainAnimationEnabled;
private Boolean mDebug;
private Float mDesiredBrightness;
private Integer mDesiredBrightnessInt;
private com.android.server.display.DisplayDeviceConfig mDisplayDeviceConfig;
private com.android.server.display.DisplayFeatureManagerServiceImpl mDisplayFeatureManagerServicImpl;
private Integer mDisplayId;
private com.android.server.display.DisplayManagerServiceImpl mDisplayMangerServiceImpl;
private com.android.server.display.DisplayPowerController mDisplayPowerController;
private Boolean mDolbyPreviewBoostAvailable;
private Float mDolbyPreviewBoostRatio;
private Boolean mDolbyPreviewEnable;
private Boolean mDolbyStateEnable;
private Boolean mDozeInLowBrightness;
private Float mDozeScreenBrightness;
private java.lang.Boolean mFolded;
private java.lang.String mForegroundAppPackageName;
private final miui.process.IForegroundWindowListener mForegroundWindowListener;
private Boolean mGalleryHdrThrottled;
private Float mGrayBrightnessFactor;
private Float mGrayScale;
private com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler mHandler;
private com.android.server.display.HighBrightnessModeController mHbmController;
private com.android.server.display.HighBrightnessModeController$HdrStateListener mHdrStateListener;
private android.os.IBinder mISurfaceFlinger;
private Boolean mInitialBCBCParameters;
private com.android.server.display.DisplayPowerControllerImpl$Injector mInjector;
private Boolean mIsDynamicLockScreenShowing;
private Boolean mIsGalleryHdrEnable;
private Boolean mIsScreenOn;
private Boolean mIsSunlightModeEnable;
private Boolean mIsSupportManualBoostApp;
private Float mK1;
private Float mK2;
private Float mK3;
private Float mK4;
private Boolean mLastAnimating;
private Integer mLastDisplayState;
private Boolean mLastFoldedState;
private Float mLastManualBoostBrightness;
private Float mLastMaxBrightness;
private Float mLastSettingsBrightnessBeforeApplySunlight;
private Boolean mLastSlowChange;
private Float mLastTemperature;
private Float mLastTemporaryBrightness;
private com.android.server.display.LogicalDisplay mLogicalDisplay;
private mLowBatteryLevelBrightnessThreshold;
private mLowBatteryLevelThreshold;
private Float mLowBatteryLimitBrightness;
private Boolean mManualBrightnessBoostEnable;
private Float mMaxDozeBrightnessFloat;
private Float mMaxManualBoostBrightness;
private Float mMaxPowerSaveModeBrightness;
private Float mMinDozeBrightnessFloat;
protected com.android.server.display.MiuiDisplayCloudController mMiuiDisplayCloudController;
private Integer mOprAmbientLuxThreshold;
private Boolean mOprBrightnessControlAvailable;
private mOprGrayscaleThreshold;
private Integer mOprMaxNitThreshold;
private mOprNitThreshold;
private Integer mOrientation;
private Boolean mOutDoorHighTempState;
private java.lang.String mPendingForegroundAppPackageName;
private Boolean mPendingResetGrayscaleStateForOpr;
private Boolean mPendingShowCurtainAnimation;
private Boolean mPendingUpdateBrightness;
private com.android.server.policy.WindowManagerPolicy mPolicy;
private android.os.PowerManager mPowerManager;
private Integer mPreviousDisplayPolicy;
private com.android.server.display.RampRateController mRampRateController;
private mRealtimeArrayD;
private Float mRealtimeMaxA;
private Float mRealtimeMaxDiff;
private Boolean mRotationListenerEnabled;
private com.android.server.display.DisplayPowerControllerImpl$RotationWatcher mRotationWatcher;
private Float mScreenBrightnessRangeMaximum;
private Float mScreenBrightnessRangeMinimum;
private Boolean mScreenGrayscaleState;
private com.android.server.display.DisplayPowerControllerImpl$SettingsObserver mSettingsObserver;
private Boolean mShouldDimming;
private Boolean mShowOutdoorHighTempToast;
private com.android.server.display.SunlightController mSunlightController;
private Boolean mSunlightModeActive;
private Boolean mSunlightModeAvailable;
private com.android.server.display.DisplayPowerControllerStub$SunlightStateChangedListener mSunlightStateListener;
private Boolean mSupportCustomBrightness;
private Boolean mSupportIndividualBrightness;
private Float mTargetBrightness;
private Float mTargetSdrBrightness;
private com.android.server.display.DisplayPowerControllerImpl$TaskStackListenerImpl mTaskStackListener;
private com.android.server.display.ThermalBrightnessController$Callback mThermalBrightnessCallback;
private Boolean mThermalBrightnessControlAvailable;
private com.android.server.display.ThermalBrightnessController mThermalBrightnessController;
private Float mThermalMaxBrightness;
private com.android.server.display.ThermalObserver mThermalObserver;
private Boolean mUpdateBrightnessAnimInfoEnable;
private com.android.server.wm.WindowManagerService mWms;
/* # direct methods */
public static void $r8$lambda$9MdydEw6WUBFaa6WgULApqZMMv4 ( com.android.server.display.DisplayPowerControllerImpl p0, java.util.List p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->lambda$updateOutdoorThermalAppCategoryList$2(Ljava/util/List;)V */
return;
} // .end method
public static void $r8$lambda$Ffw3zG_X6ZkLvOU37Xk8nKYUfLc ( com.android.server.display.DisplayPowerControllerImpl p0, Boolean p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->lambda$notifyOutDoorHighTempState$4(Z)V */
return;
} // .end method
public static void $r8$lambda$Qx_JfyyAhlDFM0JaQj_pe5qb60E ( com.android.server.display.DisplayPowerControllerImpl p0, Integer p1, Float p2, Boolean p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->lambda$startDetailThermalUsageStatsOnThermalChanged$5(IFZ)V */
return;
} // .end method
public static void $r8$lambda$l09th6PYVtGhBJdqbyc0CsivaZk ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->lambda$init$0()V */
return;
} // .end method
public static void $r8$lambda$vRZRNl1DftxwNyhEMYS6FMy4lOo ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->lambda$stop$3()V */
return;
} // .end method
public static void $r8$lambda$wpIyozD0qhRbWzJcY53YXqpB4HI ( com.android.server.display.DisplayPowerControllerImpl p0, java.lang.Boolean p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->lambda$init$1(Ljava/lang/Boolean;)V */
return;
} // .end method
static android.app.IActivityTaskManager -$$Nest$fgetmActivityTaskManager ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mActivityTaskManager;
} // .end method
static Boolean -$$Nest$fgetmAutoBrightnessEnable ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z */
} // .end method
static com.android.server.display.aiautobrt.CustomBrightnessModeController -$$Nest$fgetmCbmController ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCbmController;
} // .end method
static java.lang.String -$$Nest$fgetmForegroundAppPackageName ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mForegroundAppPackageName;
} // .end method
static com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler -$$Nest$fgetmHandler ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Integer -$$Nest$fgetmOrientation ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOrientation:I */
} // .end method
static com.android.server.display.RampRateController -$$Nest$fgetmRampRateController ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mRampRateController;
} // .end method
static com.android.server.display.ThermalBrightnessController -$$Nest$fgetmThermalBrightnessController ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mThermalBrightnessController;
} // .end method
static void -$$Nest$fputmOrientation ( com.android.server.display.DisplayPowerControllerImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOrientation:I */
return;
} // .end method
static void -$$Nest$fputmPendingForegroundAppPackageName ( com.android.server.display.DisplayPowerControllerImpl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mPendingForegroundAppPackageName = p1;
return;
} // .end method
static void -$$Nest$mnotifyUpdateForegroundApp ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->notifyUpdateForegroundApp()V */
return;
} // .end method
static void -$$Nest$mresetBCBCState ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetBCBCState()V */
return;
} // .end method
static void -$$Nest$mresetScreenGrayscaleState ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetScreenGrayscaleState()V */
return;
} // .end method
static void -$$Nest$msetCurrentGrayScale ( com.android.server.display.DisplayPowerControllerImpl p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->setCurrentGrayScale(F)V */
return;
} // .end method
static void -$$Nest$msetGrayScale ( com.android.server.display.DisplayPowerControllerImpl p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->setGrayScale(F)V */
return;
} // .end method
static void -$$Nest$mupdateAutoBrightnessMode ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateAutoBrightnessMode()V */
return;
} // .end method
static void -$$Nest$mupdateBatteryBrightness ( com.android.server.display.DisplayPowerControllerImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateBatteryBrightness(I)V */
return;
} // .end method
static void -$$Nest$mupdateColorInversionEnabled ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateColorInversionEnabled()V */
return;
} // .end method
static void -$$Nest$mupdateCurtainAnimationEnabled ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateCurtainAnimationEnabled()V */
return;
} // .end method
static void -$$Nest$mupdateDolbyBrightnessIfNeeded ( com.android.server.display.DisplayPowerControllerImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateDolbyBrightnessIfNeeded(Z)V */
return;
} // .end method
static void -$$Nest$mupdateForegroundApp ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateForegroundApp()V */
return;
} // .end method
static void -$$Nest$mupdateForegroundAppSync ( com.android.server.display.DisplayPowerControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateForegroundAppSync()V */
return;
} // .end method
static void -$$Nest$mupdateThermalBrightness ( com.android.server.display.DisplayPowerControllerImpl p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateThermalBrightness(F)V */
return;
} // .end method
static com.android.server.display.DisplayPowerControllerImpl ( ) {
/* .locals 3 */
/* .line 81 */
/* nop */
/* .line 82 */
final String v0 = "ro.vendor.bcbc.enable"; // const-string v0, "ro.vendor.bcbc.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.display.DisplayPowerControllerImpl.BCBC_ENABLE = (v0!= 0);
/* .line 83 */
/* nop */
/* .line 84 */
final String v0 = "persist.sys.muiltdisplay_type"; // const-string v0, "persist.sys.muiltdisplay_type"
v0 = android.os.SystemProperties .getInt ( v0,v1 );
int v2 = 2; // const/4 v2, 0x2
/* if-ne v0, v2, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
/* move v0, v1 */
} // :goto_0
com.android.server.display.DisplayPowerControllerImpl.IS_FOLDABLE_DEVICE = (v0!= 0);
/* .line 87 */
/* nop */
/* .line 88 */
final String v0 = "ro.vendor.aod.brightness.cust"; // const-string v0, "ro.vendor.aod.brightness.cust"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.display.DisplayPowerControllerImpl.SUPPORT_MULTIPLE_AOD_BRIGHTNESS = (v0!= 0);
/* .line 176 */
/* new-array v0, v2, [F */
/* fill-array-data v0, :array_0 */
/* .line 178 */
/* new-array v0, v2, [F */
/* fill-array-data v0, :array_1 */
/* .line 180 */
android.content.res.Resources .getSystem ( );
/* .line 183 */
/* nop */
/* .line 184 */
/* const v2, 0x11030032 */
(( android.content.res.Resources ) v0 ).obtainTypedArray ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* .line 183 */
com.android.server.display.DisplayPowerControllerImpl .getFloatArray ( v2 );
/* .line 187 */
/* nop */
/* .line 188 */
/* const v2, 0x11070025 */
v2 = (( android.content.res.Resources ) v0 ).getFloat ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F
/* .line 189 */
/* nop */
/* .line 190 */
/* const v2, 0x11070024 */
v2 = (( android.content.res.Resources ) v0 ).getFloat ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F
/* .line 193 */
/* nop */
/* .line 194 */
/* const v2, 0x11070020 */
v2 = (( android.content.res.Resources ) v0 ).getFloat ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F
/* .line 197 */
/* nop */
/* .line 198 */
/* const v2, 0x11070022 */
v2 = (( android.content.res.Resources ) v0 ).getFloat ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F
/* .line 201 */
/* nop */
/* .line 202 */
/* const v2, 0x11070030 */
v2 = (( android.content.res.Resources ) v0 ).getFloat ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F
/* .line 205 */
/* const v2, 0x110b001a */
v2 = (( android.content.res.Resources ) v0 ).getInteger ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* .line 207 */
/* const v2, 0x110b0009 */
v2 = (( android.content.res.Resources ) v0 ).getInteger ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* .line 210 */
/* const v2, 0x1107002e */
v0 = (( android.content.res.Resources ) v0 ).getFloat ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F
/* .line 229 */
/* nop */
/* .line 230 */
/* const-string/jumbo v0, "support_bcbc_by_ambient_lux" */
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
com.android.server.display.DisplayPowerControllerImpl.SUPPORT_BCBC_BY_AMBIENT_LUX = (v0!= 0);
/* .line 281 */
/* nop */
/* .line 282 */
/* const-string/jumbo v0, "support_idle_dim" */
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
com.android.server.display.DisplayPowerControllerImpl.SUPPORT_IDLE_DIM = (v0!= 0);
/* .line 292 */
/* nop */
/* .line 293 */
/* const-string/jumbo v0, "support_gallery_hdr" */
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
com.android.server.display.DisplayPowerControllerImpl.mSupportGalleryHdr = (v0!= 0);
/* .line 292 */
return;
/* :array_0 */
/* .array-data 4 */
/* 0x41200000 # 10.0f */
/* 0x42c80000 # 100.0f */
} // .end array-data
/* :array_1 */
/* .array-data 4 */
/* 0x40a00000 # 5.0f */
/* 0x41400000 # 12.0f */
} // .end array-data
} // .end method
public com.android.server.display.DisplayPowerControllerImpl ( ) {
/* .locals 5 */
/* .line 77 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 168 */
/* const/high16 v0, -0x40800000 # -1.0f */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDozeScreenBrightness:F */
/* .line 225 */
v1 = com.android.server.display.DisplayPowerControllerImpl.DATA_D;
/* array-length v2, v1 */
java.util.Arrays .copyOf ( v1,v2 );
this.mRealtimeArrayD = v1;
/* .line 226 */
/* iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeMaxA:F */
/* .line 227 */
/* iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeMaxDiff:F */
/* .line 232 */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F */
/* .line 233 */
/* const/high16 v2, 0x7fc00000 # Float.NaN */
/* iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayScale:F */
/* .line 235 */
/* iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBcbcBrightness:F */
/* .line 243 */
/* nop */
/* .line 244 */
/* const-string/jumbo v3, "support_dolby_version_brighten" */
int v4 = 0; // const/4 v4, 0x0
v3 = miui.util.FeatureParser .getBoolean ( v3,v4 );
/* iput-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_DOLBY_VERSION_BRIGHTEN:Z */
/* .line 245 */
/* nop */
/* .line 246 */
/* const-string/jumbo v3, "support_hdr_hbm_brighten" */
v3 = miui.util.FeatureParser .getBoolean ( v3,v4 );
/* iput-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_HDR_HBM_BRIGHTEN:Z */
/* .line 248 */
/* nop */
/* .line 249 */
/* const-string/jumbo v3, "support_manual_dimming" */
v3 = miui.util.FeatureParser .getBoolean ( v3,v4 );
/* iput-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MANUAL_DIMMING:Z */
/* .line 251 */
/* nop */
/* .line 252 */
/* const-string/jumbo v3, "support_manual_brightness_boost" */
v3 = miui.util.FeatureParser .getBoolean ( v3,v4 );
/* iput-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MANUAL_BRIGHTNESS_BOOST:Z */
/* .line 270 */
int v3 = 2; // const/4 v3, 0x2
/* iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastDisplayState:I */
/* .line 287 */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastMaxBrightness:F */
/* .line 288 */
/* iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F */
/* .line 297 */
/* iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGalleryHdrBoostFactor:F */
/* .line 304 */
/* iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalMaxBrightness:F */
/* .line 305 */
/* iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastTemporaryBrightness:F */
/* .line 308 */
/* iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedSdrBrightness:F */
/* .line 309 */
/* iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedBrightness:F */
/* .line 313 */
/* iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLimitBrightness:F */
/* .line 334 */
/* iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F */
/* .line 375 */
/* iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxPowerSaveModeBrightness:F */
/* .line 381 */
/* new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/DisplayPowerControllerImpl$1;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V */
this.mForegroundWindowListener = v0;
/* .line 394 */
/* iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostRatio:F */
/* .line 1004 */
/* new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$3; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/DisplayPowerControllerImpl$3;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V */
this.mThermalBrightnessCallback = v0;
/* .line 2076 */
/* new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$5; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/DisplayPowerControllerImpl$5;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V */
this.mCloudListener = v0;
return;
} // .end method
private Float adjustBrightnessByBattery ( Float p0, com.android.server.display.brightness.BrightnessReason p1 ) {
/* .locals 2 */
/* .param p1, "brightness" # F */
/* .param p2, "reason" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .line 2136 */
/* move v0, p1 */
/* .line 2137 */
/* .local v0, "newBrightness":F */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLimitBrightness:F */
v0 = java.lang.Math .min ( v0,v1 );
/* .line 2138 */
/* cmpl-float v1, v0, p1 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2139 */
/* const/16 v1, 0x80 */
(( com.android.server.display.brightness.BrightnessReason ) p2 ).addModifier ( v1 ); // invoke-virtual {p2, v1}, Lcom/android/server/display/brightness/BrightnessReason;->addModifier(I)V
/* .line 2141 */
} // :cond_0
} // .end method
private Float adjustBrightnessByBcbc ( Float p0, com.android.server.display.brightness.BrightnessReason p1 ) {
/* .locals 4 */
/* .param p1, "brightness" # F */
/* .param p2, "reason" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .line 943 */
/* move v0, p1 */
/* .line 944 */
/* .local v0, "newBrightness":F */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayScale:F */
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGrayScaleLegal(F)Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 945 */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayScale:F */
v0 = /* invoke-direct {p0, p1, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->calculateBrightnessBCBC(FF)F */
/* .line 946 */
/* cmpl-float v1, p1, v0 */
final String v2 = "DisplayPowerControllerImpl"; // const-string v2, "DisplayPowerControllerImpl"
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 947 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedBcbc:Z */
/* .line 948 */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBcbcBrightness:F */
/* .line 949 */
/* const/16 v1, 0x10 */
(( com.android.server.display.brightness.BrightnessReason ) p2 ).addModifier ( v1 ); // invoke-virtual {p2, v1}, Lcom/android/server/display/brightness/BrightnessReason;->addModifier(I)V
/* .line 950 */
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 951 */
final String v1 = "Apply bcbc brightness."; // const-string v1, "Apply bcbc brightness."
android.util.Slog .d ( v2,v1 );
/* .line 953 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedBcbc:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* cmpl-float v1, p1, v0 */
/* if-nez v1, :cond_1 */
/* .line 954 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedBcbc:Z */
/* .line 955 */
/* const/high16 v3, 0x7fc00000 # Float.NaN */
/* iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBcbcBrightness:F */
/* .line 956 */
/* invoke-direct {p0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetBcbcRateModifier(Z)V */
/* .line 957 */
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 958 */
final String v1 = "Exit bcbc brightness."; // const-string v1, "Exit bcbc brightness."
android.util.Slog .d ( v2,v1 );
/* .line 962 */
} // :cond_1
} // :goto_0
} // .end method
private Float adjustBrightnessByLux ( Float p0, Float p1 ) {
/* .locals 8 */
/* .param p1, "preBrightness" # F */
/* .param p2, "currentBrightness" # F */
/* .line 1153 */
/* move v0, p2 */
/* .line 1154 */
/* .local v0, "outBrightness":F */
v1 = this.mBrightnessMapper;
if ( v1 != null) { // if-eqz v1, :cond_4
v2 = this.mAutomaticBrightnessControllerImpl;
/* if-nez v2, :cond_0 */
/* goto/16 :goto_1 */
/* .line 1157 */
} // :cond_0
v1 = (( com.android.server.display.BrightnessMappingStrategy ) v1 ).convertToNits ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/display/BrightnessMappingStrategy;->convertToNits(F)F
/* .line 1158 */
/* .local v1, "preNit":F */
v2 = this.mBrightnessMapper;
v2 = (( com.android.server.display.BrightnessMappingStrategy ) v2 ).convertToNits ( p2 ); // invoke-virtual {v2, p2}, Lcom/android/server/display/BrightnessMappingStrategy;->convertToNits(F)F
/* .line 1159 */
/* .local v2, "currentNit":F */
v3 = this.mAutomaticBrightnessControllerImpl;
v3 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v3 ).getCurrentAmbientLux ( ); // invoke-virtual {v3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getCurrentAmbientLux()F
/* .line 1161 */
/* .local v3, "currentLux":F */
v4 = com.android.server.display.DisplayPowerControllerImpl.mBCBCLuxThreshold;
int v5 = 0; // const/4 v5, 0x0
/* aget v6, v4, v5 */
/* cmpg-float v6, v3, v6 */
/* if-gez v6, :cond_1 */
/* cmpl-float v6, v1, v2 */
/* if-lez v6, :cond_1 */
/* sub-float v6, v1, v2 */
v7 = com.android.server.display.DisplayPowerControllerImpl.mBCBCNitDecreaseThreshold;
/* aget v5, v7, v5 */
/* cmpl-float v6, v6, v5 */
/* if-lez v6, :cond_1 */
/* .line 1163 */
v4 = this.mBrightnessMapper;
/* sub-float v5, v1, v5 */
v0 = (( com.android.server.display.BrightnessMappingStrategy ) v4 ).convertToBrightness ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/display/BrightnessMappingStrategy;->convertToBrightness(F)F
/* .line 1164 */
} // :cond_1
int v5 = 1; // const/4 v5, 0x1
/* aget v4, v4, v5 */
/* cmpg-float v4, v3, v4 */
/* if-gez v4, :cond_2 */
/* cmpl-float v4, v1, v2 */
/* if-lez v4, :cond_2 */
/* sub-float v4, v1, v2 */
v6 = com.android.server.display.DisplayPowerControllerImpl.mBCBCNitDecreaseThreshold;
/* aget v5, v6, v5 */
/* cmpl-float v4, v4, v5 */
/* if-lez v4, :cond_2 */
/* .line 1166 */
v4 = this.mBrightnessMapper;
/* sub-float v5, v1, v5 */
v0 = (( com.android.server.display.BrightnessMappingStrategy ) v4 ).convertToBrightness ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/display/BrightnessMappingStrategy;->convertToBrightness(F)F
/* .line 1169 */
} // :cond_2
} // :goto_0
/* iget-boolean v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 1170 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "adjustBrightnessByLux: currentLux: "; // const-string v5, "adjustBrightnessByLux: currentLux: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = ", preNit: "; // const-string v5, ", preNit: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = ", currentNit: "; // const-string v5, ", currentNit: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = ", currentBrightness: "; // const-string v5, ", currentBrightness: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "DisplayPowerControllerImpl"; // const-string v5, "DisplayPowerControllerImpl"
android.util.Slog .d ( v5,v4 );
/* .line 1174 */
} // :cond_3
/* .line 1155 */
} // .end local v1 # "preNit":F
} // .end local v2 # "currentNit":F
} // .end local v3 # "currentLux":F
} // :cond_4
} // :goto_1
} // .end method
private Float adjustBrightnessByOpr ( Float p0, com.android.server.display.brightness.BrightnessReason p1 ) {
/* .locals 7 */
/* .param p1, "brightness" # F */
/* .param p2, "reason" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .line 1827 */
v0 = this.mDisplayDeviceConfig;
final String v1 = "DisplayPowerControllerImpl"; // const-string v1, "DisplayPowerControllerImpl"
/* if-nez v0, :cond_0 */
/* .line 1828 */
final String v0 = "adjustBrightnessByOpr: no valid display device config!"; // const-string v0, "adjustBrightnessByOpr: no valid display device config!"
android.util.Slog .e ( v1,v0 );
/* .line 1829 */
/* .line 1833 */
} // :cond_0
v0 = this.mAutomaticBrightnessControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1834 */
v0 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).getCurrentAmbientLux ( ); // invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getCurrentAmbientLux()F
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprAmbientLuxThreshold:I */
/* int-to-float v2, v2 */
/* cmpl-float v0, v0, v2 */
/* if-lez v0, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 1835 */
/* .local v0, "shouldResetScreenGrayscaleState":Z */
} // :goto_0
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingResetGrayscaleStateForOpr:Z */
/* if-eq v2, v0, :cond_2 */
/* .line 1836 */
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingResetGrayscaleStateForOpr:Z */
/* .line 1837 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetScreenGrayscaleState()V */
/* .line 1841 */
} // :cond_2
v2 = this.mDisplayDeviceConfig;
/* iget v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprMaxNitThreshold:I */
/* int-to-float v3, v3 */
v2 = (( com.android.server.display.DisplayDeviceConfig ) v2 ).getBrightnessFromNit ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F
/* .line 1842 */
/* .local v2, "maxOprBrightness":F */
/* const/high16 v3, -0x40800000 # -1.0f */
/* cmpl-float v3, v2, v3 */
/* if-nez v3, :cond_3 */
/* .line 1843 */
/* .line 1845 */
} // :cond_3
/* iget-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingResetGrayscaleStateForOpr:Z */
/* const/16 v4, 0x200 */
/* if-nez v3, :cond_5 */
/* cmpl-float v3, p1, v2 */
/* if-lez v3, :cond_5 */
/* .line 1846 */
/* iget-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1847 */
final String v3 = "adjustBrightnessByOpr: constrain brightnesswhen current lux is below opr ambient lux threshold."; // const-string v3, "adjustBrightnessByOpr: constrain brightnesswhen current lux is below opr ambient lux threshold."
android.util.Slog .d ( v1,v3 );
/* .line 1850 */
} // :cond_4
(( com.android.server.display.brightness.BrightnessReason ) p2 ).addModifier ( v4 ); // invoke-virtual {p2, v4}, Lcom/android/server/display/brightness/BrightnessReason;->addModifier(I)V
/* .line 1851 */
/* .line 1855 */
} // :cond_5
/* iget v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F */
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/display/DisplayPowerControllerImpl;->getRestrictedOprBrightness(F)F */
/* .line 1856 */
/* .local v3, "restrictedOprBrightness":F */
v5 = java.lang.Float .isNaN ( v3 );
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 1857 */
/* .line 1859 */
} // :cond_6
/* iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F */
/* cmpl-float v5, v3, v5 */
if ( v5 != null) { // if-eqz v5, :cond_7
/* .line 1860 */
/* iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F */
/* .line 1862 */
} // :cond_7
/* iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F */
/* cmpl-float v5, p1, v5 */
/* if-lez v5, :cond_9 */
/* .line 1863 */
/* iget-boolean v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 1864 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "adjustBrightnessByOpr: current brightness: "; // const-string v6, "adjustBrightnessByOpr: current brightness: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v6 = " is constrained to target brightness: "; // const-string v6, " is constrained to target brightness: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v5 );
/* .line 1867 */
} // :cond_8
(( com.android.server.display.brightness.BrightnessReason ) p2 ).addModifier ( v4 ); // invoke-virtual {p2, v4}, Lcom/android/server/display/brightness/BrightnessReason;->addModifier(I)V
/* .line 1868 */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F */
/* .line 1870 */
} // :cond_9
} // .end method
private Float adjustBrightnessByPowerSaveMode ( Float p0, com.android.server.display.brightness.BrightnessReason p1 ) {
/* .locals 2 */
/* .param p1, "brightness" # F */
/* .param p2, "reason" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .line 1889 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxPowerSaveModeBrightness:F */
v0 = java.lang.Float .isNaN ( v0 );
/* if-nez v0, :cond_0 */
/* .line 1890 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxPowerSaveModeBrightness:F */
v0 = android.util.MathUtils .min ( v0,p1 );
/* .line 1891 */
/* .local v0, "newBrightness":F */
/* cmpl-float v1, v0, p1 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1892 */
/* const/16 v1, 0x400 */
(( com.android.server.display.brightness.BrightnessReason ) p2 ).addModifier ( v1 ); // invoke-virtual {p2, v1}, Lcom/android/server/display/brightness/BrightnessReason;->addModifier(I)V
/* .line 1893 */
/* .line 1896 */
} // .end local v0 # "newBrightness":F
} // :cond_0
} // .end method
private Float adjustBrightnessByThermal ( Float p0, Boolean p1, com.android.server.display.brightness.BrightnessReason p2 ) {
/* .locals 8 */
/* .param p1, "brightness" # F */
/* .param p2, "isHdr" # Z */
/* .param p3, "reason" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .line 919 */
/* move v0, p1 */
/* .line 921 */
/* .local v0, "newBrightness":F */
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessControlAvailable:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 922 */
/* if-nez p2, :cond_0 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrScene()Z */
if ( v1 != null) { // if-eqz v1, :cond_1
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_3
v1 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrScene()Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 923 */
} // :cond_1
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalMaxBrightness:F */
/* const/high16 v2, -0x40800000 # -1.0f */
/* cmpl-float v2, v1, v2 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 924 */
v1 = java.lang.Float .isNaN ( v1 );
/* if-nez v1, :cond_2 */
/* .line 925 */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalMaxBrightness:F */
v0 = android.util.MathUtils .min ( v1,p1 );
/* .line 928 */
} // :cond_2
/* cmpl-float v1, v0, p1 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 929 */
/* const/16 v1, 0x40 */
(( com.android.server.display.brightness.BrightnessReason ) p3 ).addModifier ( v1 ); // invoke-virtual {p3, v1}, Lcom/android/server/display/brightness/BrightnessReason;->addModifier(I)V
/* .line 934 */
} // :cond_3
int v1 = 0; // const/4 v1, 0x0
/* cmpl-float v1, p1, v1 */
/* if-lez v1, :cond_5 */
/* if-nez p2, :cond_4 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrScene()Z */
/* if-nez v1, :cond_5 */
/* .line 935 */
} // :cond_4
/* iget v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalMaxBrightness:F */
/* iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentConditionId:I */
/* iget v6, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentTemperature:F */
/* iget-boolean v7, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOutDoorHighTempState:Z */
/* move-object v2, p0 */
/* move v3, p1 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/display/DisplayPowerControllerImpl;->startFullSceneThermalUsageStats(FFIFZ)V */
/* .line 939 */
} // :cond_5
} // .end method
private Float adjustBrightnessToPeak ( Float p0, Boolean p1, com.android.server.display.brightness.BrightnessReason p2 ) {
/* .locals 7 */
/* .param p1, "brightness" # F */
/* .param p2, "isHdrBrightness" # Z */
/* .param p3, "reason" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .line 967 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 969 */
/* .local v0, "currentMaxBrightness":F */
v1 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).isSupportPeakBrightness ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isSupportPeakBrightness()Z
if ( v1 != null) { // if-eqz v1, :cond_4
v1 = this.mAutomaticBrightnessControllerImpl;
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 970 */
v1 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v1 ).getCurrentAmbientLux ( ); // invoke-virtual {v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getCurrentAmbientLux()F
/* .line 971 */
/* .local v1, "currentLux":F */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).getMaxHbmBrightnessForPeak ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getMaxHbmBrightnessForPeak()F
/* .line 973 */
v2 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrVideo()Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* int-to-float v2, v2 */
/* cmpl-float v2, v1, v2 */
/* if-lez v2, :cond_0 */
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F */
/* .line 976 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGrayScaleLegal(F)Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F */
/* int-to-float v3, v3 */
/* cmpg-float v2, v2, v3 */
/* if-gtz v2, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 978 */
/* .local v2, "shouldApplyPeakBrightness":Z */
} // :goto_0
final String v3 = ", current gray scale: "; // const-string v3, ", current gray scale: "
final String v4 = "DisplayPowerControllerImpl"; // const-string v4, "DisplayPowerControllerImpl"
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 980 */
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 981 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 982 */
/* const/16 v5, 0x100 */
(( com.android.server.display.brightness.BrightnessReason ) p3 ).addModifier ( v5 ); // invoke-virtual {p3, v5}, Lcom/android/server/display/brightness/BrightnessReason;->addModifier(I)V
/* .line 983 */
/* iget-boolean v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 984 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Apply peak brightness, currentLux: "; // const-string v6, "Apply peak brightness, currentLux: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v3 );
/* .line 990 */
} // :cond_1
/* iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastMaxBrightness:F */
/* cmpl-float v5, v5, v0 */
/* if-lez v5, :cond_3 */
/* .line 991 */
/* iget-boolean v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 992 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Exit peak brightness, currentLux: "; // const-string v6, "Exit peak brightness, currentLux: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v3 );
/* .line 995 */
} // :cond_2
/* const/high16 v3, 0x7fc00000 # Float.NaN */
/* iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F */
/* .line 998 */
} // :cond_3
} // :goto_1
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastMaxBrightness:F */
/* .line 1000 */
} // .end local v1 # "currentLux":F
} // .end local v2 # "shouldApplyPeakBrightness":Z
} // :cond_4
p1 = android.util.MathUtils .min ( p1,v0 );
/* .line 1001 */
} // .end method
private Float calculateBrightnessBCBC ( Float p0, Float p1 ) {
/* .locals 7 */
/* .param p1, "brightness" # F */
/* .param p2, "grayScale" # F */
/* .line 1112 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenBrightnessRangeMaximum:F */
/* div-float v0, p1, v0 */
/* .line 1114 */
/* .local v0, "ratio":F */
/* cmpl-float v2, p2, v1 */
/* const/high16 v3, 0x3f800000 # 1.0f */
/* if-lez v2, :cond_3 */
/* .line 1115 */
v2 = this.mRealtimeArrayD;
int v4 = 4; // const/4 v4, 0x4
/* aget v4, v2, v4 */
/* cmpl-float v5, v0, v4 */
int v6 = 5; // const/4 v6, 0x5
/* if-lez v5, :cond_0 */
/* aget v5, v2, v6 */
/* cmpg-float v5, v0, v5 */
/* if-gtz v5, :cond_0 */
/* .line 1116 */
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK3:F */
/* sub-float v4, v0, v4 */
/* mul-float/2addr v2, v4 */
/* sub-float v1, p2, v1 */
/* mul-float/2addr v2, v1 */
/* add-float/2addr v2, v3 */
/* iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F */
/* goto/16 :goto_0 */
/* .line 1117 */
} // :cond_0
/* aget v4, v2, v6 */
/* cmpl-float v4, v0, v4 */
int v5 = 6; // const/4 v5, 0x6
/* if-lez v4, :cond_1 */
/* aget v4, v2, v5 */
/* cmpg-float v4, v0, v4 */
/* if-gtz v4, :cond_1 */
/* .line 1118 */
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeMaxDiff:F */
/* sub-float v4, p2, v1 */
/* mul-float/2addr v2, v4 */
/* sub-float v1, v3, v1 */
/* mul-float/2addr v1, v0 */
/* div-float/2addr v2, v1 */
/* sub-float v1, v3, v2 */
/* iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F */
/* goto/16 :goto_0 */
/* .line 1119 */
} // :cond_1
/* aget v4, v2, v5 */
/* cmpl-float v4, v0, v4 */
/* if-lez v4, :cond_2 */
int v4 = 7; // const/4 v4, 0x7
/* aget v2, v2, v4 */
/* cmpg-float v4, v0, v2 */
/* if-gez v4, :cond_2 */
/* .line 1120 */
/* iget v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK4:F */
/* sub-float v2, v0, v2 */
/* mul-float/2addr v4, v2 */
/* sub-float v1, p2, v1 */
/* mul-float/2addr v4, v1 */
/* add-float/2addr v4, v3 */
/* iput v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F */
/* .line 1122 */
} // :cond_2
/* iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F */
/* .line 1124 */
} // :cond_3
int v1 = 0; // const/4 v1, 0x0
/* cmpl-float v1, p2, v1 */
/* if-lez v1, :cond_7 */
/* cmpg-float v2, p2, v1 */
/* if-gez v2, :cond_7 */
/* .line 1125 */
v2 = this.mRealtimeArrayD;
int v4 = 0; // const/4 v4, 0x0
/* aget v4, v2, v4 */
/* cmpl-float v5, v0, v4 */
int v6 = 1; // const/4 v6, 0x1
/* if-lez v5, :cond_4 */
/* aget v5, v2, v6 */
/* cmpg-float v5, v0, v5 */
/* if-gtz v5, :cond_4 */
/* .line 1126 */
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK1:F */
/* sub-float v4, v0, v4 */
/* mul-float/2addr v2, v4 */
/* sub-float v1, p2, v1 */
/* mul-float/2addr v2, v1 */
/* add-float/2addr v2, v3 */
/* iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F */
/* .line 1127 */
} // :cond_4
/* aget v4, v2, v6 */
/* cmpl-float v4, v0, v4 */
int v5 = 2; // const/4 v5, 0x2
/* if-lez v4, :cond_5 */
/* aget v4, v2, v5 */
/* cmpg-float v4, v0, v4 */
/* if-gtz v4, :cond_5 */
/* .line 1128 */
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeMaxA:F */
/* add-float v4, v2, v3 */
/* div-float/2addr v2, v1 */
/* mul-float/2addr v2, p2 */
/* sub-float/2addr v4, v2 */
/* iput v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F */
/* .line 1129 */
} // :cond_5
/* aget v4, v2, v5 */
/* cmpl-float v4, v0, v4 */
/* if-lez v4, :cond_6 */
int v4 = 3; // const/4 v4, 0x3
/* aget v2, v2, v4 */
/* cmpg-float v4, v0, v2 */
/* if-gez v4, :cond_6 */
/* .line 1130 */
/* iget v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK2:F */
/* sub-float v2, v0, v2 */
/* mul-float/2addr v4, v2 */
/* sub-float v1, p2, v1 */
/* mul-float/2addr v4, v1 */
/* add-float/2addr v4, v3 */
/* iput v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F */
/* .line 1133 */
} // :cond_6
/* iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F */
/* .line 1136 */
} // :cond_7
/* iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F */
/* .line 1139 */
} // :goto_0
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F */
/* mul-float v2, p1, v1 */
/* .line 1140 */
/* .local v2, "outBrightness":F */
/* sget-boolean v4, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_BCBC_BY_AMBIENT_LUX:Z */
if ( v4 != null) { // if-eqz v4, :cond_8
/* cmpg-float v1, v1, v3 */
/* if-gez v1, :cond_8 */
/* .line 1141 */
v2 = /* invoke-direct {p0, p1, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByLux(FF)F */
/* .line 1144 */
} // :cond_8
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 1145 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " grayScale = "; // const-string v3, " grayScale = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = " factor = "; // const-string v3, " factor = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = " inBrightness = "; // const-string v3, " inBrightness = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = " outBrightness = "; // const-string v3, " outBrightness = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "DisplayPowerControllerImpl"; // const-string v3, "DisplayPowerControllerImpl"
android.util.Slog .d ( v3,v1 );
/* .line 1149 */
} // :cond_9
} // .end method
private Float calculateBrightnessForManualBoost ( Float p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "brightness" # F */
/* .param p2, "isBoostEntering" # Z */
/* .line 770 */
v0 = this.mDisplayDeviceConfig;
(( com.android.server.display.DisplayDeviceConfig ) v0 ).getHighBrightnessModeData ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayDeviceConfig;->getHighBrightnessModeData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;
/* iget v0, v0, Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;->transitionPoint:F */
/* .line 771 */
/* .local v0, "transitionPoint":F */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 773 */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenBrightnessRangeMinimum:F */
/* sub-float v2, p1, v1 */
/* sub-float v1, v0, v1 */
/* div-float/2addr v2, v1 */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F */
/* sub-float/2addr v1, v0 */
/* mul-float/2addr v2, v1 */
/* add-float/2addr v2, p1 */
/* .local v2, "tempBrightness":F */
/* .line 779 */
} // .end local v2 # "tempBrightness":F
} // :cond_0
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastManualBoostBrightness:F */
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenBrightnessRangeMinimum:F */
/* sub-float v3, v0, v2 */
/* mul-float/2addr v1, v3 */
/* iget v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F */
/* sub-float v4, v3, v0 */
/* mul-float/2addr v4, v2 */
/* add-float/2addr v1, v4 */
/* sub-float/2addr v3, v2 */
/* div-float v2, v1, v3 */
/* .line 784 */
/* .restart local v2 # "tempBrightness":F */
} // :goto_0
} // .end method
private Float calculateGalleryHdrBoostFactor ( Float p0, Float p1 ) {
/* .locals 4 */
/* .param p1, "hdrNit" # F */
/* .param p2, "sdrNit" # F */
/* .line 1327 */
int v0 = 0; // const/4 v0, 0x0
/* cmpl-float v1, p1, v0 */
/* const/high16 v2, 0x3f800000 # 1.0f */
if ( v1 != null) { // if-eqz v1, :cond_1
/* cmpl-float v0, p2, v0 */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = java.lang.Float .isNaN ( p2 );
/* if-nez v0, :cond_1 */
v0 = java.lang.Float .isNaN ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1331 */
} // :cond_0
/* div-float v0, p1, p2 */
/* float-to-double v0, v0 */
java.math.BigDecimal .valueOf ( v0,v1 );
v1 = java.math.RoundingMode.HALF_UP;
/* .line 1332 */
int v3 = 3; // const/4 v3, 0x3
(( java.math.BigDecimal ) v0 ).setScale ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;
/* .line 1333 */
v0 = (( java.math.BigDecimal ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/math/BigDecimal;->floatValue()F
/* .line 1335 */
/* .local v0, "factor":F */
/* const/high16 v1, 0x40100000 # 2.25f */
v1 = android.util.MathUtils .constrain ( v0,v2,v1 );
/* .line 1328 */
} // .end local v0 # "factor":F
} // :cond_1
} // :goto_0
} // .end method
private void canApplyManualBrightnessBoost ( Float p0 ) {
/* .locals 3 */
/* .param p1, "brightness" # F */
/* .line 739 */
v0 = this.mDisplayDeviceConfig;
(( com.android.server.display.DisplayDeviceConfig ) v0 ).getHighBrightnessModeData ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayDeviceConfig;->getHighBrightnessModeData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;
final String v1 = "DisplayPowerControllerImpl"; // const-string v1, "DisplayPowerControllerImpl"
if ( v0 != null) { // if-eqz v0, :cond_4
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBootCompleted:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F */
/* const/high16 v2, -0x40800000 # -1.0f */
/* cmpl-float v0, v0, v2 */
/* if-nez v0, :cond_0 */
/* .line 745 */
} // :cond_0
/* move v0, p1 */
/* .line 746 */
/* .local v0, "tempBrightness":F */
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z */
/* if-nez v2, :cond_1 */
v2 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->shouldManualBoostForCurrentApp()Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 747 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z */
/* .line 749 */
v0 = /* invoke-direct {p0, v0, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->calculateBrightnessForManualBoost(FZ)F */
/* .line 750 */
v2 = this.mSunlightStateListener;
/* .line 751 */
final String v2 = "Enter manual brightness boost."; // const-string v2, "Enter manual brightness boost."
android.util.Slog .d ( v1,v2 );
/* .line 752 */
} // :cond_1
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->shouldManualBoostForCurrentApp()Z */
/* if-nez v2, :cond_2 */
/* .line 753 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z */
/* .line 755 */
v0 = /* invoke-direct {p0, v0, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->calculateBrightnessForManualBoost(FZ)F */
/* .line 756 */
v2 = this.mSunlightStateListener;
/* .line 757 */
final String v2 = "Exit manual brightness boost."; // const-string v2, "Exit manual brightness boost."
android.util.Slog .d ( v1,v2 );
/* .line 759 */
} // :cond_2
} // :goto_0
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSunlightModeEnable:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 761 */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastManualBoostBrightness:F */
/* .line 764 */
} // :cond_3
/* const/high16 v1, 0x7fc00000 # Float.NaN */
/* iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastManualBoostBrightness:F */
/* .line 766 */
} // :goto_1
return;
/* .line 742 */
} // .end local v0 # "tempBrightness":F
} // :cond_4
} // :goto_2
final String v0 = "Don\'t apply manual brightness boost because current device status is invalid."; // const-string v0, "Don\'t apply manual brightness boost because current device status is invalid."
android.util.Slog .w ( v1,v0 );
/* .line 743 */
return;
} // .end method
private void computeBCBCAdjustmentParams ( ) {
/* .locals 8 */
/* .line 1192 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeMaxA:F */
/* neg-float v1, v0 */
v3 = this.mRealtimeArrayD;
int v4 = 1; // const/4 v4, 0x1
/* aget v4, v3, v4 */
int v5 = 0; // const/4 v5, 0x0
/* aget v5, v3, v5 */
/* sub-float/2addr v4, v5 */
/* mul-float/2addr v4, v2 */
/* div-float/2addr v1, v4 */
/* iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK1:F */
/* .line 1193 */
int v1 = 3; // const/4 v1, 0x3
/* aget v1, v3, v1 */
int v4 = 2; // const/4 v4, 0x2
/* aget v4, v3, v4 */
/* sub-float/2addr v1, v4 */
/* mul-float/2addr v2, v1 */
/* div-float/2addr v0, v2 */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK2:F */
/* .line 1194 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeMaxDiff:F */
/* neg-float v1, v0 */
int v2 = 5; // const/4 v2, 0x5
/* aget v2, v3, v2 */
/* const/high16 v5, 0x3f800000 # 1.0f */
/* sub-float v6, v5, v4 */
/* mul-float/2addr v6, v2 */
int v7 = 4; // const/4 v7, 0x4
/* aget v7, v3, v7 */
/* sub-float/2addr v2, v7 */
/* mul-float/2addr v6, v2 */
/* div-float/2addr v1, v6 */
/* iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK3:F */
/* .line 1196 */
int v1 = 6; // const/4 v1, 0x6
/* aget v1, v3, v1 */
/* sub-float/2addr v5, v4 */
/* mul-float/2addr v5, v1 */
int v2 = 7; // const/4 v2, 0x7
/* aget v2, v3, v2 */
/* sub-float/2addr v2, v1 */
/* mul-float/2addr v5, v2 */
/* div-float/2addr v0, v5 */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK4:F */
/* .line 1198 */
return;
} // .end method
private static getFloatArray ( android.content.res.TypedArray p0 ) {
/* .locals 4 */
/* .param p0, "array" # Landroid/content/res/TypedArray; */
/* .line 1182 */
v0 = (( android.content.res.TypedArray ) p0 ).length ( ); // invoke-virtual {p0}, Landroid/content/res/TypedArray;->length()I
/* .line 1183 */
/* .local v0, "length":I */
/* new-array v1, v0, [F */
/* .line 1184 */
/* .local v1, "floatArray":[F */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v0, :cond_0 */
/* .line 1185 */
/* const/high16 v3, 0x7fc00000 # Float.NaN */
v3 = (( android.content.res.TypedArray ) p0 ).getFloat ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F
/* aput v3, v1, v2 */
/* .line 1184 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1187 */
} // .end local v2 # "i":I
} // :cond_0
(( android.content.res.TypedArray ) p0 ).recycle ( ); // invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V
/* .line 1188 */
} // .end method
private Float getRestrictedOprBrightness ( Float p0 ) {
/* .locals 3 */
/* .param p1, "grayScale" # F */
/* .line 1874 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGrayScaleLegal(F)Z */
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mDisplayDeviceConfig;
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mOprGrayscaleThreshold;
if ( v0 != null) { // if-eqz v0, :cond_3
v1 = this.mOprNitThreshold;
if ( v1 != null) { // if-eqz v1, :cond_3
/* array-length v0, v0 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* array-length v0, v1 */
/* if-nez v0, :cond_0 */
/* .line 1880 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "index":I */
} // :goto_0
v1 = this.mOprGrayscaleThreshold;
/* array-length v2, v1 */
/* if-ge v0, v2, :cond_2 */
/* .line 1881 */
/* aget v1, v1, v0 */
/* int-to-float v1, v1 */
/* cmpg-float v1, p1, v1 */
/* if-gtz v1, :cond_1 */
/* .line 1882 */
/* .line 1880 */
} // :cond_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1885 */
} // :cond_2
} // :goto_1
v1 = this.mDisplayDeviceConfig;
v2 = this.mOprNitThreshold;
/* aget v2, v2, v0 */
/* int-to-float v2, v2 */
v1 = (( com.android.server.display.DisplayDeviceConfig ) v1 ).getBrightnessFromNit ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F
/* .line 1877 */
} // .end local v0 # "index":I
} // :cond_3
} // :goto_2
/* const/high16 v0, 0x7fc00000 # Float.NaN */
} // .end method
private Integer getSceneState ( ) {
/* .locals 2 */
/* .line 2417 */
int v0 = -1; // const/4 v0, -0x1
/* .line 2418 */
/* .local v0, "state":I */
v1 = this.mCbmController;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2419 */
v0 = (( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v1 ).getCurrentSceneState ( ); // invoke-virtual {v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getCurrentSceneState()I
/* .line 2421 */
} // :cond_0
} // .end method
private Boolean isAllowedUseSunlightMode ( ) {
/* .locals 1 */
/* .line 607 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeActive:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isSunlightModeDisabledByUser()Z */
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isGrayScaleLegal ( Float p0 ) {
/* .locals 1 */
/* .param p1, "grayScale" # F */
/* .line 1031 */
v0 = java.lang.Float .isNaN ( p1 );
/* if-nez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* cmpl-float v0, p1, v0 */
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isHdrScene ( ) {
/* .locals 2 */
/* .line 1047 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyStateEnable:Z */
/* if-nez v0, :cond_1 */
v0 = this.mHbmController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1048 */
v0 = (( com.android.server.display.HighBrightnessModeController ) v0 ).getHighBrightnessMode ( ); // invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->getHighBrightnessMode()I
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 1047 */
} // :goto_1
} // .end method
private Boolean isHdrVideo ( ) {
/* .locals 1 */
/* .line 1038 */
v0 = this.mHbmController;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1039 */
v0 = (( com.android.server.display.HighBrightnessModeController ) v0 ).isHdrLayerPresent ( ); // invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->isHdrLayerPresent()Z
/* if-nez v0, :cond_0 */
v0 = this.mHbmController;
/* .line 1040 */
v0 = (( com.android.server.display.HighBrightnessModeController ) v0 ).isDolbyEnable ( ); // invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->isDolbyEnable()Z
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 1038 */
} // :goto_0
} // .end method
private Boolean isInOutdoorCriticalTemperature ( ) {
/* .locals 1 */
/* .line 1414 */
v0 = this.mThermalBrightnessController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1415 */
v0 = (( com.android.server.display.ThermalBrightnessController ) v0 ).isInOutdoorCriticalTemperature ( ); // invoke-virtual {v0}, Lcom/android/server/display/ThermalBrightnessController;->isInOutdoorCriticalTemperature()Z
/* .line 1417 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isKeyguardOn ( ) {
/* .locals 1 */
/* .line 1794 */
v0 = v0 = this.mPolicy;
/* if-nez v0, :cond_1 */
v0 = v0 = this.mPolicy;
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean isManualBrightnessBoostAppEnable ( ) {
/* .locals 1 */
/* .line 710 */
v0 = this.mMiuiDisplayCloudController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 711 */
v0 = (( com.android.server.display.MiuiDisplayCloudController ) v0 ).isManualBoostAppEnable ( ); // invoke-virtual {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->isManualBoostAppEnable()Z
/* .line 713 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isOverrideBrightnessPolicyEnable ( ) {
/* .locals 1 */
/* .line 1384 */
v0 = this.mMiuiDisplayCloudController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1385 */
v0 = (( com.android.server.display.MiuiDisplayCloudController ) v0 ).isOverrideBrightnessPolicyEnable ( ); // invoke-virtual {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->isOverrideBrightnessPolicyEnable()Z
/* .line 1387 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isSunlightModeDisabledByUser ( ) {
/* .locals 1 */
/* .line 661 */
v0 = this.mSunlightController;
v0 = (( com.android.server.display.SunlightController ) v0 ).isSunlightModeDisabledByUser ( ); // invoke-virtual {v0}, Lcom/android/server/display/SunlightController;->isSunlightModeDisabledByUser()Z
} // .end method
private void lambda$init$0 ( ) { //synthethic
/* .locals 0 */
/* .line 491 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->registerForegroundAppUpdater()V */
/* .line 492 */
return;
} // .end method
private void lambda$init$1 ( java.lang.Boolean p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "folded" # Ljava/lang/Boolean; */
/* .line 496 */
v0 = (( java.lang.Boolean ) p1 ).booleanValue ( ); // invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z
/* invoke-direct {p0, v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->setDeviceFolded(Z)V */
return;
} // .end method
private void lambda$notifyOutDoorHighTempState$4 ( Boolean p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "changed" # Z */
/* .line 2226 */
/* iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOutDoorHighTempState:Z */
return;
} // .end method
private void lambda$startDetailThermalUsageStatsOnThermalChanged$5 ( Integer p0, Float p1, Boolean p2 ) { //synthethic
/* .locals 1 */
/* .param p1, "conditionId" # I */
/* .param p2, "temperature" # F */
/* .param p3, "brightnessChanged" # Z */
/* .line 2258 */
/* iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentConditionId:I */
/* .line 2259 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentTemperature:F */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastTemperature:F */
/* .line 2260 */
/* iput p2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentTemperature:F */
/* .line 2261 */
/* cmpl-float v0, v0, p2 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* invoke-direct {p0, p2, v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->startAverageTemperatureStats(FZ)V */
/* .line 2262 */
/* if-nez p3, :cond_1 */
/* .line 2263 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->startDetailsThermalUsageStats(IF)V */
/* .line 2265 */
} // :cond_1
return;
} // .end method
private void lambda$stop$3 ( ) { //synthethic
/* .locals 1 */
/* .line 2020 */
v0 = this.mForegroundWindowListener;
miui.process.ProcessManager .unregisterForegroundWindowListener ( v0 );
return;
} // .end method
private void lambda$updateOutdoorThermalAppCategoryList$2 ( java.util.List p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "outdoorThermalAppList" # Ljava/util/List; */
/* .line 1807 */
v0 = this.mThermalBrightnessController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1808 */
(( com.android.server.display.ThermalBrightnessController ) v0 ).updateOutdoorThermalAppCategoryList ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/ThermalBrightnessController;->updateOutdoorThermalAppCategoryList(Ljava/util/List;)V
/* .line 1810 */
} // :cond_0
return;
} // .end method
private void loadSettings ( ) {
/* .locals 0 */
/* .line 1644 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateColorInversionEnabled()V */
/* .line 1645 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateAutoBrightnessMode()V */
/* .line 1646 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateCurtainAnimationEnabled()V */
/* .line 1647 */
return;
} // .end method
private void notifyUpdateForegroundApp ( ) {
/* .locals 1 */
/* .line 2432 */
v0 = this.mAutomaticBrightnessControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2433 */
(( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).notifyUpdateForegroundApp ( ); // invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->notifyUpdateForegroundApp()V
/* .line 2435 */
} // :cond_0
return;
} // .end method
private void onAnimateValueChanged ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "changed" # Z */
/* .line 2497 */
v0 = this.mRampRateController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2498 */
v1 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).isAnimating ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isAnimating()Z
(( com.android.server.display.RampRateController ) v0 ).onAnimateValueChanged ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/display/RampRateController;->onAnimateValueChanged(ZZ)V
/* .line 2500 */
} // :cond_0
return;
} // .end method
private void onBrightnessChanged ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 2491 */
v0 = this.mRampRateController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2492 */
v1 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).isAnimating ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isAnimating()Z
(( com.android.server.display.RampRateController ) v0 ).onBrightnessChanged ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/display/RampRateController;->onBrightnessChanged(ZZ)V
/* .line 2494 */
} // :cond_0
return;
} // .end method
private void recalculationForBCBC ( Float p0 ) {
/* .locals 7 */
/* .param p1, "coefficient" # F */
/* .line 1614 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mInitialBCBCParameters:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1615 */
return;
/* .line 1617 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mInitialBCBCParameters:Z */
/* .line 1619 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mRealtimeArrayD;
/* array-length v2, v1 */
int v3 = 4; // const/4 v3, 0x4
int v4 = 6; // const/4 v4, 0x6
/* if-ge v0, v2, :cond_1 */
/* .line 1620 */
v2 = com.android.server.display.DisplayPowerControllerImpl.DATA_D;
/* aget v2, v2, v0 */
/* mul-float/2addr v2, p1 */
/* float-to-double v5, v2 */
java.math.BigDecimal .valueOf ( v5,v6 );
/* .line 1621 */
(( java.math.BigDecimal ) v2 ).setScale ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;
v2 = (( java.math.BigDecimal ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/math/BigDecimal;->floatValue()F
/* aput v2, v1, v0 */
/* .line 1619 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1623 */
} // .end local v0 # "i":I
} // :cond_1
/* mul-float/2addr v0, p1 */
/* float-to-double v0, v0 */
java.math.BigDecimal .valueOf ( v0,v1 );
/* .line 1624 */
(( java.math.BigDecimal ) v0 ).setScale ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;
v0 = (( java.math.BigDecimal ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/math/BigDecimal;->floatValue()F
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeMaxDiff:F */
/* .line 1625 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->computeBCBCAdjustmentParams()V */
/* .line 1626 */
return;
} // .end method
private void registerBroadcastsReceiver ( ) {
/* .locals 5 */
/* .line 2145 */
v0 = this.mLowBatteryLevelThreshold;
/* array-length v0, v0 */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mLowBatteryLevelBrightnessThreshold;
/* array-length v0, v0 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2147 */
/* new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver-IA;)V */
this.mBatteryReceiver = v0;
/* .line 2148 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 2149 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v2 = "android.intent.action.BATTERY_CHANGED"; // const-string v2, "android.intent.action.BATTERY_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 2150 */
/* const/16 v2, 0x3e8 */
(( android.content.IntentFilter ) v0 ).setPriority ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->setPriority(I)V
/* .line 2151 */
v2 = this.mContext;
v3 = this.mBatteryReceiver;
v4 = this.mHandler;
(( android.content.Context ) v2 ).registerReceiver ( v3, v0, v1, v4 ); // invoke-virtual {v2, v3, v0, v1, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
/* .line 2153 */
} // .end local v0 # "filter":Landroid/content/IntentFilter;
} // :cond_0
return;
} // .end method
private void registerForegroundAppUpdater ( ) {
/* .locals 2 */
/* .line 1655 */
try { // :try_start_0
v0 = this.mActivityTaskManager;
v1 = this.mTaskStackListener;
/* .line 1656 */
v0 = this.mForegroundWindowListener;
miui.process.ProcessManager .registerForegroundWindowListener ( v0 );
/* .line 1659 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateForegroundApp()V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1662 */
/* .line 1660 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1663 */
} // :goto_0
return;
} // .end method
private void registerSettingsObserver ( ) {
/* .locals 5 */
/* .line 570 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 571 */
final String v1 = "accessibility_display_inversion_enabled"; // const-string v1, "accessibility_display_inversion_enabled"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 570 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 573 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 574 */
final String v1 = "screen_brightness_mode"; // const-string v1, "screen_brightness_mode"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 573 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 576 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 577 */
final String v1 = "curtain_anim_enabled"; // const-string v1, "curtain_anim_enabled"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 576 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 579 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 580 */
final String v1 = "is_dynamic_lockscreen_shown"; // const-string v1, "is_dynamic_lockscreen_shown"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 579 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 582 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->loadSettings()V */
/* .line 583 */
return;
} // .end method
private void resetBCBCState ( ) {
/* .locals 1 */
/* .line 1016 */
/* const/high16 v0, 0x7fc00000 # Float.NaN */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayScale:F */
/* .line 1017 */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBcbcBrightness:F */
/* .line 1018 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedBcbc:Z */
/* .line 1019 */
(( com.android.server.display.DisplayPowerControllerImpl ) p0 ).updateBCBCStateIfNeeded ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateBCBCStateIfNeeded()V
/* .line 1020 */
return;
} // .end method
private void resetBcbcRateModifier ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "appliedAutoBrightness" # Z */
/* .line 2520 */
v0 = this.mRampRateController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2521 */
/* if-nez p1, :cond_0 */
/* .line 2522 */
(( com.android.server.display.RampRateController ) v0 ).clearBcbcModifier ( ); // invoke-virtual {v0}, Lcom/android/server/display/RampRateController;->clearBcbcModifier()V
/* .line 2525 */
} // :cond_0
return;
} // .end method
private void resetRateModifierOnAnimateValueChanged ( ) {
/* .locals 1 */
/* .line 2534 */
v0 = this.mRampRateController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2535 */
(( com.android.server.display.RampRateController ) v0 ).clearBcbcModifier ( ); // invoke-virtual {v0}, Lcom/android/server/display/RampRateController;->clearBcbcModifier()V
/* .line 2536 */
v0 = this.mRampRateController;
(( com.android.server.display.RampRateController ) v0 ).clearThermalModifier ( ); // invoke-virtual {v0}, Lcom/android/server/display/RampRateController;->clearThermalModifier()V
/* .line 2538 */
} // :cond_0
return;
} // .end method
private void resetScreenGrayscaleState ( ) {
/* .locals 2 */
/* .line 1023 */
/* const/high16 v0, 0x7fc00000 # Float.NaN */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F */
/* .line 1024 */
/* const/high16 v1, -0x40800000 # -1.0f */
/* iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastMaxBrightness:F */
/* .line 1025 */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F */
/* .line 1026 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingUpdateBrightness:Z */
/* .line 1027 */
(( com.android.server.display.DisplayPowerControllerImpl ) p0 ).updateScreenGrayscaleStateIfNeeded ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateScreenGrayscaleStateIfNeeded()V
/* .line 1028 */
return;
} // .end method
private void resetThermalRateModifier ( ) {
/* .locals 1 */
/* .line 2528 */
v0 = this.mRampRateController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2529 */
(( com.android.server.display.RampRateController ) v0 ).clearThermalModifier ( ); // invoke-virtual {v0}, Lcom/android/server/display/RampRateController;->clearThermalModifier()V
/* .line 2531 */
} // :cond_0
return;
} // .end method
private void sendSurfaceFlingerActualBrightness ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "brightness" # I */
/* .line 788 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
final String v1 = "DisplayPowerControllerImpl"; // const-string v1, "DisplayPowerControllerImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 789 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "sendSurfaceFlingerActualBrightness, brightness = " */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 791 */
} // :cond_0
v0 = this.mISurfaceFlinger;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 792 */
android.os.Parcel .obtain ( );
/* .line 793 */
/* .local v0, "data":Landroid/os/Parcel; */
final String v2 = "android.ui.ISurfaceComposer"; // const-string v2, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 794 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 796 */
try { // :try_start_0
v2 = this.mISurfaceFlinger;
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
/* const/16 v5, 0x7980 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 801 */
/* nop */
} // :goto_0
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 802 */
/* .line 801 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 798 */
/* :catch_0 */
/* move-exception v2 */
/* .line 799 */
/* .local v2, "ex":Ljava/lang/Exception; */
try { // :try_start_1
final String v3 = "Failed to send brightness to SurfaceFlinger"; // const-string v3, "Failed to send brightness to SurfaceFlinger"
android.util.Slog .e ( v1,v3,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 801 */
/* nop */
} // .end local v2 # "ex":Ljava/lang/Exception;
} // :goto_1
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 802 */
/* throw v1 */
/* .line 804 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // :cond_1
} // :goto_2
return;
} // .end method
private void setCurrentGrayScale ( Float p0 ) {
/* .locals 1 */
/* .param p1, "grayScale" # F */
/* .line 1482 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGrayScaleLegal(F)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F */
/* cmpl-float v0, v0, p1 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1483 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateBrightnessForOpr(F)V */
/* .line 1484 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateBrightnessForPeak(F)V */
/* .line 1486 */
} // :cond_0
return;
} // .end method
private void setDeviceFolded ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "folded" # Z */
/* .line 532 */
v0 = this.mFolded;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* if-ne v0, p1, :cond_0 */
/* .line 533 */
return;
/* .line 535 */
} // :cond_0
java.lang.Boolean .valueOf ( p1 );
this.mFolded = v0;
/* .line 536 */
com.android.server.display.statistics.OneTrackFoldStateHelper .getInstance ( );
(( com.android.server.display.statistics.OneTrackFoldStateHelper ) v0 ).oneTrackFoldState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->oneTrackFoldState(Z)V
/* .line 537 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 538 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mFolded: "; // const-string v1, "mFolded: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mFolded;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "DisplayPowerControllerImpl"; // const-string v1, "DisplayPowerControllerImpl"
android.util.Slog .d ( v1,v0 );
/* .line 540 */
} // :cond_1
v0 = this.mPowerManager;
v0 = (( android.os.PowerManager ) v0 ).isInteractive ( ); // invoke-virtual {v0}, Landroid/os/PowerManager;->isInteractive()Z
/* .line 541 */
/* .local v0, "isInteractive":Z */
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurtainAnimationAvailable:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurtainAnimationEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsDynamicLockScreenShowing:Z */
/* if-nez v1, :cond_2 */
/* if-nez v0, :cond_2 */
/* .line 543 */
v1 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).isFolded ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isFolded()Z
/* if-nez v1, :cond_2 */
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingShowCurtainAnimation:Z */
/* if-nez v1, :cond_2 */
/* .line 545 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingShowCurtainAnimation:Z */
/* .line 547 */
} // :cond_2
return;
} // .end method
private void setGrayScale ( Float p0 ) {
/* .locals 1 */
/* .param p1, "grayScale" # F */
/* .line 1469 */
/* iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayScale:F */
/* .line 1470 */
v0 = this.mBrightnessDataProcessor;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).updateGrayScale ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateGrayScale(F)V
/* .line 1471 */
v0 = this.mDisplayPowerController;
(( com.android.server.display.DisplayPowerController ) v0 ).updateBrightness ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V
/* .line 1472 */
return;
} // .end method
private Boolean shouldManualBoostForCurrentApp ( ) {
/* .locals 1 */
/* .line 717 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isManualBrightnessBoostAppEnable()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 718 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSupportManualBoostApp:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSunlightModeEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* .line 720 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSunlightModeEnable:Z */
} // .end method
private void startAverageTemperatureStats ( Float p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "temperature" # F */
/* .param p2, "needComputed" # Z */
/* .line 2285 */
v0 = this.mBrightnessDataProcessor;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2286 */
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).noteAverageTemperature ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteAverageTemperature(FZ)V
/* .line 2288 */
} // :cond_0
return;
} // .end method
private void startDetailsThermalUsageStats ( Integer p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "conditionId" # I */
/* .param p2, "temperature" # F */
/* .line 2274 */
v0 = this.mBrightnessDataProcessor;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2275 */
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).noteDetailThermalUsage ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteDetailThermalUsage(IF)V
/* .line 2277 */
} // :cond_0
return;
} // .end method
private void startFullSceneThermalUsageStats ( Float p0, Float p1, Integer p2, Float p3, Boolean p4 ) {
/* .locals 6 */
/* .param p1, "brightness" # F */
/* .param p2, "thermalBrightness" # F */
/* .param p3, "currentConditionId" # I */
/* .param p4, "temperature" # F */
/* .param p5, "outdoorHighTempState" # Z */
/* .line 2243 */
v0 = this.mBrightnessDataProcessor;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2244 */
/* move v1, p1 */
/* move v2, p2 */
/* move v3, p3 */
/* move v4, p4 */
/* move v5, p5 */
/* invoke-virtual/range {v0 ..v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteFullSceneThermalUsageStats(FFIFZ)V */
/* .line 2247 */
} // :cond_0
return;
} // .end method
private void startUpdateThermalStats ( Float p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "brightnessState" # F */
/* .param p2, "isScreenOn" # Z */
/* .line 2215 */
v0 = this.mBrightnessDataProcessor;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2216 */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentTemperature:F */
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastTemperature:F */
/* cmpl-float v2, v2, v1 */
if ( v2 != null) { // if-eqz v2, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).updateThermalStats ( p1, p2, v1, v2 ); // invoke-virtual {v0, p1, p2, v1, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateThermalStats(FZFZ)V
/* .line 2219 */
} // :cond_1
return;
} // .end method
private void updateAmbientLightSensor ( android.hardware.Sensor p0 ) {
/* .locals 1 */
/* .param p1, "lightSensor" # Landroid/hardware/Sensor; */
/* .line 596 */
v0 = this.mSunlightController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 597 */
(( com.android.server.display.SunlightController ) v0 ).updateAmbientLightSensor ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/SunlightController;->updateAmbientLightSensor(Landroid/hardware/Sensor;)V
/* .line 599 */
} // :cond_0
return;
} // .end method
private void updateAutoBrightnessMode ( ) {
/* .locals 4 */
/* .line 1629 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "screen_brightness_mode"; // const-string v2, "screen_brightness_mode"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
/* move v1, v3 */
} // :goto_0
/* iput-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z */
/* .line 1633 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetBCBCState()V */
/* .line 1634 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetScreenGrayscaleState()V */
/* .line 1636 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z */
/* if-nez v0, :cond_1 */
/* .line 1637 */
(( com.android.server.display.DisplayPowerControllerImpl ) p0 ).updateBrightnessAnimInfoIfNeeded ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateBrightnessAnimInfoIfNeeded(Z)V
/* .line 1639 */
} // :cond_1
/* iput-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z */
/* .line 1641 */
} // :goto_1
return;
} // .end method
private void updateBatteryBrightness ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "batteryLevel" # I */
/* .line 2159 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2160 */
/* .local v0, "index":I */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* .line 2161 */
/* .local v1, "lowBatteryBrightness":F */
} // :goto_0
v2 = this.mLowBatteryLevelThreshold;
/* array-length v3, v2 */
/* if-ge v0, v3, :cond_0 */
/* aget v2, v2, v0 */
/* if-le p1, v2, :cond_0 */
/* .line 2163 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 2165 */
} // :cond_0
v2 = this.mLowBatteryLevelBrightnessThreshold;
/* array-length v3, v2 */
/* if-ge v0, v3, :cond_1 */
/* .line 2166 */
v3 = this.mDisplayDeviceConfig;
/* aget v2, v2, v0 */
/* int-to-float v2, v2 */
v1 = (( com.android.server.display.DisplayDeviceConfig ) v3 ).getBrightnessFromNit ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F
/* .line 2169 */
} // :cond_1
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLimitBrightness:F */
/* cmpl-float v2, v1, v2 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 2170 */
/* iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLimitBrightness:F */
/* .line 2171 */
v2 = this.mDisplayPowerController;
(( com.android.server.display.DisplayPowerController ) v2 ).updateBrightness ( ); // invoke-virtual {v2}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V
/* .line 2173 */
} // :cond_2
return;
} // .end method
private void updateBrightnessForOpr ( Float p0 ) {
/* .locals 4 */
/* .param p1, "grayScale" # F */
/* .line 1498 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrScene()Z */
/* if-nez v0, :cond_0 */
v0 = this.mAutomaticBrightnessControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1500 */
v0 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).getCurrentAmbientLux ( ); // invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getCurrentAmbientLux()F
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprAmbientLuxThreshold:I */
/* int-to-float v1, v1 */
/* cmpl-float v0, v0, v1 */
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1501 */
/* .local v0, "updateBrightnessForOpr":Z */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1502 */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F */
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->getRestrictedOprBrightness(F)F */
/* .line 1503 */
/* .local v1, "currentRestrictedBrightness":F */
v2 = /* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->getRestrictedOprBrightness(F)F */
/* .line 1504 */
/* .local v2, "pendingRestrictedOprBrightness":F */
/* iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F */
/* .line 1505 */
/* cmpl-float v3, v1, v2 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1506 */
v3 = this.mDisplayPowerController;
(( com.android.server.display.DisplayPowerController ) v3 ).updateBrightness ( ); // invoke-virtual {v3}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V
/* .line 1509 */
} // .end local v1 # "currentRestrictedBrightness":F
} // .end local v2 # "pendingRestrictedOprBrightness":F
} // :cond_1
return;
} // .end method
private void updateBrightnessForPeak ( Float p0 ) {
/* .locals 2 */
/* .param p1, "grayScale" # F */
/* .line 1489 */
/* iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F */
/* .line 1490 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrVideo()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* int-to-float v0, v0 */
/* cmpg-float v0, p1, v0 */
/* if-gtz v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1491 */
/* .local v0, "updateBrightnessForPeak":Z */
} // :goto_0
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingUpdateBrightness:Z */
/* if-eq v1, v0, :cond_1 */
/* .line 1492 */
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingUpdateBrightness:Z */
/* .line 1493 */
v1 = this.mDisplayPowerController;
(( com.android.server.display.DisplayPowerController ) v1 ).updateBrightness ( ); // invoke-virtual {v1}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V
/* .line 1495 */
} // :cond_1
return;
} // .end method
private void updateColorInversionEnabled ( ) {
/* .locals 4 */
/* .line 1205 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "accessibility_display_inversion_enabled"; // const-string v2, "accessibility_display_inversion_enabled"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mColorInversionEnabled:Z */
/* .line 1208 */
return;
} // .end method
private void updateCurtainAnimationEnabled ( ) {
/* .locals 5 */
/* .line 1817 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "curtain_anim_enabled"; // const-string v1, "curtain_anim_enabled"
int v2 = 1; // const/4 v2, 0x1
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$System .getIntForUser ( v0,v1,v2,v3 );
int v1 = 0; // const/4 v1, 0x0
/* if-ne v0, v2, :cond_0 */
/* move v0, v2 */
} // :cond_0
/* move v0, v1 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurtainAnimationEnabled:Z */
/* .line 1819 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v4 = "is_dynamic_lockscreen_shown"; // const-string v4, "is_dynamic_lockscreen_shown"
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v4,v1,v3 );
/* if-ne v0, v2, :cond_1 */
} // :cond_1
/* move v2, v1 */
} // :goto_1
/* iput-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsDynamicLockScreenShowing:Z */
/* .line 1821 */
return;
} // .end method
private void updateDolbyBrightnessIfNeeded ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 1996 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_DOLBY_VERSION_BRIGHTEN:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mHbmController;
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyStateEnable:Z */
/* if-eq v0, p1, :cond_2 */
/* .line 1997 */
/* iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyStateEnable:Z */
/* .line 1998 */
v0 = this.mRampRateController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1999 */
(( com.android.server.display.RampRateController ) v0 ).addHdrRateModifier ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/RampRateController;->addHdrRateModifier(Z)V
/* .line 2001 */
} // :cond_0
v0 = this.mHbmController;
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyStateEnable:Z */
(( com.android.server.display.HighBrightnessModeController ) v0 ).setDolbyEnable ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/HighBrightnessModeController;->setDolbyEnable(Z)V
/* .line 2002 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2003 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetBCBCState()V */
/* .line 2004 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetScreenGrayscaleState()V */
/* .line 2006 */
} // :cond_1
v0 = this.mDisplayPowerController;
(( com.android.server.display.DisplayPowerController ) v0 ).updateBrightness ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V
/* .line 2007 */
v0 = this.mThermalBrightnessController;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 2008 */
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyStateEnable:Z */
(( com.android.server.display.ThermalBrightnessController ) v0 ).setDolbyEnabled ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/ThermalBrightnessController;->setDolbyEnabled(Z)V
/* .line 2011 */
} // :cond_2
return;
} // .end method
private void updateDolbyState ( android.os.Bundle p0 ) {
/* .locals 2 */
/* .param p1, "bundle" # Landroid/os/Bundle; */
/* .line 1512 */
v0 = this.mHandler;
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 1513 */
/* .local v0, "msg":Landroid/os/Message; */
(( android.os.Message ) v0 ).setData ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 1514 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 1515 */
return;
} // .end method
private void updateForegroundApp ( ) {
/* .locals 2 */
/* .line 1683 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* new-instance v1, Lcom/android/server/display/DisplayPowerControllerImpl$4; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/DisplayPowerControllerImpl$4;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1709 */
return;
} // .end method
private void updateForegroundAppSync ( ) {
/* .locals 3 */
/* .line 1715 */
v0 = this.mPendingForegroundAppPackageName;
this.mForegroundAppPackageName = v0;
/* .line 1716 */
int v1 = 0; // const/4 v1, 0x0
this.mPendingForegroundAppPackageName = v1;
/* .line 1717 */
v1 = this.mCbmController;
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1718 */
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOrientation:I */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v1 ).updateCustomSceneState ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCustomSceneState(Ljava/lang/String;I)V
/* .line 1720 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1721 */
(( com.android.server.display.DisplayPowerControllerImpl ) p0 ).updateBCBCStateIfNeeded ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateBCBCStateIfNeeded()V
/* .line 1722 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateGameSceneEnable()V */
/* .line 1724 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateManualBrightnessBoostState()V */
/* .line 1726 */
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateThermalBrightnessBoostState()V */
/* .line 1727 */
return;
} // .end method
private void updateGameSceneEnable ( ) {
/* .locals 3 */
/* .line 2089 */
v0 = this.mAutomaticBrightnessControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2090 */
v1 = this.mMiuiDisplayCloudController;
/* .line 2091 */
(( com.android.server.display.MiuiDisplayCloudController ) v1 ).getTouchCoverProtectionGameList ( ); // invoke-virtual {v1}, Lcom/android/server/display/MiuiDisplayCloudController;->getTouchCoverProtectionGameList()Ljava/util/List;
v1 = v2 = this.mForegroundAppPackageName;
/* .line 2090 */
(( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).updateGameSceneEnable ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->updateGameSceneEnable(Z)V
/* .line 2093 */
} // :cond_0
return;
} // .end method
private void updateManualBrightnessBoostState ( ) {
/* .locals 2 */
/* .line 724 */
v0 = this.mMiuiDisplayCloudController;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isManualBrightnessBoostAppEnable()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 725 */
v0 = this.mMiuiDisplayCloudController;
/* .line 726 */
(( com.android.server.display.MiuiDisplayCloudController ) v0 ).getManualBoostDisableAppList ( ); // invoke-virtual {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->getManualBoostDisableAppList()Ljava/util/List;
v0 = v1 = this.mForegroundAppPackageName;
/* xor-int/lit8 v0, v0, 0x1 */
/* .line 727 */
/* .local v0, "isSupportManualBoostApp":Z */
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSupportManualBoostApp:Z */
/* if-eq v0, v1, :cond_0 */
/* .line 728 */
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSupportManualBoostApp:Z */
/* .line 729 */
v1 = this.mDisplayPowerController;
(( com.android.server.display.DisplayPowerController ) v1 ).updateBrightness ( ); // invoke-virtual {v1}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V
/* .line 732 */
} // .end local v0 # "isSupportManualBoostApp":Z
} // :cond_0
return;
} // .end method
private void updateThermalBrightness ( Float p0 ) {
/* .locals 2 */
/* .param p1, "thermalNit" # F */
/* .line 1557 */
v0 = this.mDisplayDeviceConfig;
/* if-nez v0, :cond_0 */
/* .line 1558 */
final String v0 = "DisplayPowerControllerImpl"; // const-string v0, "DisplayPowerControllerImpl"
/* const-string/jumbo v1, "updateThermalBrightness: no valid display device config!" */
android.util.Slog .e ( v0,v1 );
/* .line 1559 */
return;
/* .line 1561 */
} // :cond_0
v0 = (( com.android.server.display.DisplayDeviceConfig ) v0 ).getBrightnessFromNit ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F
/* .line 1562 */
/* .local v0, "thermalBrightness":F */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalMaxBrightness:F */
/* cmpl-float v1, v0, v1 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1563 */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalMaxBrightness:F */
/* .line 1564 */
v1 = java.lang.Float .isNaN ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1565 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetThermalRateModifier()V */
/* .line 1567 */
} // :cond_1
v1 = this.mDisplayPowerController;
(( com.android.server.display.DisplayPowerController ) v1 ).updateBrightness ( ); // invoke-virtual {v1}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V
/* .line 1569 */
} // :cond_2
return;
} // .end method
private void updateThermalBrightnessBoostState ( ) {
/* .locals 2 */
/* .line 1900 */
v0 = this.mThermalBrightnessController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1901 */
v1 = this.mForegroundAppPackageName;
(( com.android.server.display.ThermalBrightnessController ) v0 ).updateForegroundApp ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/ThermalBrightnessController;->updateForegroundApp(Ljava/lang/String;)V
/* .line 1903 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public Float adjustBrightness ( Float p0, com.android.server.display.brightness.BrightnessReason p1 ) {
/* .locals 3 */
/* .param p1, "brightness" # F */
/* .param p2, "reason" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .line 908 */
/* move v0, p1 */
/* .line 909 */
/* .local v0, "newBrightness":F */
int v1 = 1; // const/4 v1, 0x1
v0 = /* invoke-direct {p0, v0, v1, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessToPeak(FZLcom/android/server/display/brightness/BrightnessReason;)F */
/* .line 910 */
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessControlAvailable:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 911 */
v0 = /* invoke-direct {p0, v0, v1, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByThermal(FZLcom/android/server/display/brightness/BrightnessReason;)F */
/* .line 913 */
} // :cond_0
v0 = /* invoke-direct {p0, v0, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByBattery(FLcom/android/server/display/brightness/BrightnessReason;)F */
/* .line 914 */
} // .end method
public Float adjustSdrBrightness ( Float p0, Boolean p1, com.android.server.display.brightness.BrightnessReason p2, Boolean p3 ) {
/* .locals 4 */
/* .param p1, "brightness" # F */
/* .param p2, "useAutoBrightness" # Z */
/* .param p3, "reason" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .param p4, "appliedAutoBrightness" # Z */
/* .line 865 */
/* move v0, p1 */
/* .line 866 */
/* .local v0, "newBrightness":F */
v1 = this.mDisplayPowerController;
(( com.android.server.display.DisplayPowerController ) v1 ).getDisplayPowerState ( ); // invoke-virtual {v1}, Lcom/android/server/display/DisplayPowerController;->getDisplayPowerState()Lcom/android/server/display/DisplayPowerState;
v1 = (( com.android.server.display.DisplayPowerState ) v1 ).getScreenState ( ); // invoke-virtual {v1}, Lcom/android/server/display/DisplayPowerState;->getScreenState()I
int v2 = 0; // const/4 v2, 0x0
int v3 = 2; // const/4 v3, 0x2
/* if-ne v1, v3, :cond_5 */
/* .line 867 */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 869 */
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprBrightnessControlAvailable:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrScene()Z */
/* if-nez v1, :cond_0 */
/* .line 870 */
v0 = /* invoke-direct {p0, v0, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByOpr(FLcom/android/server/display/brightness/BrightnessReason;)F */
/* .line 873 */
} // :cond_0
v1 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isKeyguardOn()Z */
/* if-nez v1, :cond_2 */
/* .line 874 */
v0 = /* invoke-direct {p0, v0, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByBcbc(FLcom/android/server/display/brightness/BrightnessReason;)F */
/* .line 878 */
} // :cond_1
v1 = this.mDisplayPowerController;
/* .line 879 */
v1 = (( com.android.server.display.DisplayPowerController ) v1 ).getScreenBrightnessSetting ( ); // invoke-virtual {v1}, Lcom/android/server/display/DisplayPowerController;->getScreenBrightnessSetting()F
/* .line 878 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).canApplyingSunlightBrightness ( v1, p1, p3 ); // invoke-virtual {p0, v1, p1, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->canApplyingSunlightBrightness(FFLcom/android/server/display/brightness/BrightnessReason;)F
/* .line 882 */
} // :cond_2
} // :goto_0
v1 = (( com.android.server.display.brightness.BrightnessReason ) p3 ).getModifier ( ); // invoke-virtual {p3}, Lcom/android/server/display/brightness/BrightnessReason;->getModifier()I
/* and-int/2addr v1, v3 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 883 */
v0 = /* invoke-direct {p0, v0, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByPowerSaveMode(FLcom/android/server/display/brightness/BrightnessReason;)F */
/* .line 885 */
} // :cond_3
v0 = /* invoke-direct {p0, v0, v2, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessToPeak(FZLcom/android/server/display/brightness/BrightnessReason;)F */
/* .line 886 */
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessControlAvailable:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 887 */
v0 = /* invoke-direct {p0, v0, v2, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByThermal(FZLcom/android/server/display/brightness/BrightnessReason;)F */
/* .line 888 */
if ( p4 != null) { // if-eqz p4, :cond_4
/* cmpl-float v1, v0, p1 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 889 */
(( com.android.server.display.DisplayPowerControllerImpl ) p0 ).updateFastRateStatus ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateFastRateStatus(F)V
/* .line 892 */
} // :cond_4
v0 = /* invoke-direct {p0, v0, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByBattery(FLcom/android/server/display/brightness/BrightnessReason;)F */
/* .line 895 */
} // :cond_5
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedSdrBrightness:F */
v1 = com.android.internal.display.BrightnessSynchronizer .floatEquals ( p1,v1 );
/* if-nez v1, :cond_6 */
/* .line 896 */
/* iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedSdrBrightness:F */
/* .line 897 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetRateModifierOnAnimateValueChanged()V */
/* .line 898 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->onBrightnessChanged(Z)V */
/* .line 900 */
} // :cond_6
/* invoke-direct {p0, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->onBrightnessChanged(Z)V */
/* .line 902 */
} // :goto_1
/* invoke-direct {p0, p4}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetBcbcRateModifier(Z)V */
/* .line 903 */
} // .end method
protected Boolean appliedFastRate ( Float p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "currentBrightness" # F */
/* .param p2, "targetBrightness" # F */
/* .line 2513 */
v0 = this.mAutomaticBrightnessControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2514 */
v0 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).shouldUseFastRate ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->shouldUseFastRate(FF)Z
/* .line 2516 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Float canApplyingSunlightBrightness ( Float p0, Float p1, com.android.server.display.brightness.BrightnessReason p2 ) {
/* .locals 5 */
/* .param p1, "currentScreenBrightness" # F */
/* .param p2, "brightness" # F */
/* .param p3, "reason" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .line 613 */
v0 = (( com.android.server.display.brightness.BrightnessReason ) p3 ).getModifier ( ); // invoke-virtual {p3}, Lcom/android/server/display/brightness/BrightnessReason;->getModifier()I
/* .line 614 */
/* .local v0, "reasonModifier":I */
/* and-int/lit8 v1, v0, 0x1 */
/* if-nez v1, :cond_9 */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayId:I */
if ( v1 != null) { // if-eqz v1, :cond_0
/* goto/16 :goto_2 */
/* .line 618 */
} // :cond_0
/* move v1, p2 */
/* .line 619 */
/* .local v1, "tempBrightness":F */
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MANUAL_BRIGHTNESS_BOOST:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 620 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->canApplyManualBrightnessBoost(F)V */
/* .line 622 */
} // :cond_1
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeAvailable:Z */
final String v3 = "DisplayPowerControllerImpl"; // const-string v3, "DisplayPowerControllerImpl"
if ( v2 != null) { // if-eqz v2, :cond_6
v2 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isAllowedUseSunlightMode()Z */
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 623 */
v2 = this.mDisplayDeviceConfig;
(( com.android.server.display.DisplayDeviceConfig ) v2 ).getHighBrightnessModeData ( ); // invoke-virtual {v2}, Lcom/android/server/display/DisplayDeviceConfig;->getHighBrightnessModeData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 624 */
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MANUAL_BRIGHTNESS_BOOST:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->shouldManualBoostForCurrentApp()Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F */
/* const/high16 v4, -0x40800000 # -1.0f */
/* cmpl-float v2, v2, v4 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 626 */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F */
/* .line 628 */
} // :cond_2
v2 = this.mDisplayDeviceConfig;
(( com.android.server.display.DisplayDeviceConfig ) v2 ).getHighBrightnessModeData ( ); // invoke-virtual {v2}, Lcom/android/server/display/DisplayDeviceConfig;->getHighBrightnessModeData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;
/* iget v1, v2, Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;->transitionPoint:F */
/* .line 631 */
} // :cond_3
/* const/high16 v1, 0x3f800000 # 1.0f */
/* .line 633 */
} // :goto_0
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedSunlightMode:Z */
/* if-nez v2, :cond_4 */
/* .line 634 */
/* iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSettingsBrightnessBeforeApplySunlight:F */
/* .line 636 */
} // :cond_4
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedSunlightMode:Z */
/* .line 637 */
v2 = this.mSunlightStateListener;
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 638 */
/* .line 640 */
} // :cond_5
/* const/16 v2, 0xb */
(( com.android.server.display.brightness.BrightnessReason ) p3 ).setReason ( v2 ); // invoke-virtual {p3, v2}, Lcom/android/server/display/brightness/BrightnessReason;->setReason(I)V
/* .line 641 */
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 642 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "updatePowerState: appling sunlight mode brightness.last brightness:" */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSettingsBrightnessBeforeApplySunlight:F */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 645 */
} // :cond_6
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedSunlightMode:Z */
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 646 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedSunlightMode:Z */
/* .line 648 */
v2 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isSunlightModeDisabledByUser()Z */
/* if-nez v2, :cond_7 */
v2 = this.mSunlightStateListener;
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 649 */
/* iget v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSettingsBrightnessBeforeApplySunlight:F */
/* .line 652 */
} // :cond_7
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 653 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "updatePowerState: exit sunlight mode brightness.reset brightness: " */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSettingsBrightnessBeforeApplySunlight:F */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 657 */
} // :cond_8
} // :goto_1
/* .line 616 */
} // .end local v1 # "tempBrightness":F
} // :cond_9
} // :goto_2
} // .end method
public Float clampDozeBrightness ( Float p0 ) {
/* .locals 3 */
/* .param p1, "dozeBrightness" # F */
/* .line 2464 */
v0 = java.lang.Float .isNaN ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2465 */
/* .line 2468 */
} // :cond_0
/* move v0, p1 */
/* .line 2469 */
/* .local v0, "newDozeBrightness":F */
/* sget-boolean v1, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MULTIPLE_AOD_BRIGHTNESS:Z */
/* if-nez v1, :cond_2 */
/* .line 2470 */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenBrightnessRangeMinimum:F */
/* cmpl-float v1, p1, v1 */
/* if-lez v1, :cond_1 */
/* .line 2471 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxDozeBrightnessFloat:F */
/* .line 2473 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMinDozeBrightnessFloat:F */
/* .line 2476 */
} // :cond_2
} // :goto_0
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 2477 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "clamp doze brightness: "; // const-string v2, "clamp doze brightness: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = "->"; // const-string v2, "->"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "DisplayPowerControllerImpl"; // const-string v2, "DisplayPowerControllerImpl"
android.util.Slog .i ( v2,v1 );
/* .line 2480 */
} // :cond_3
} // .end method
public Float convertBrightnessToNit ( Float p0 ) {
/* .locals 1 */
/* .param p1, "brightness" # F */
/* .line 680 */
v0 = this.mDisplayDeviceConfig;
/* if-nez v0, :cond_0 */
/* .line 681 */
/* const/high16 v0, 0x7fc00000 # Float.NaN */
/* .line 683 */
} // :cond_0
v0 = (( com.android.server.display.DisplayDeviceConfig ) v0 ).getNitsFromBacklight ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 1907 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeAvailable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1908 */
v0 = this.mSunlightController;
(( com.android.server.display.SunlightController ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/SunlightController;->dump(Ljava/io/PrintWriter;)V
/* .line 1909 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAppliedSunlightMode="; // const-string v1, " mAppliedSunlightMode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedSunlightMode:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1910 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mLastSettingsBrightnessBeforeApplySunlight="; // const-string v1, " mLastSettingsBrightnessBeforeApplySunlight="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSettingsBrightnessBeforeApplySunlight:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1911 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " SUPPORT_MANUAL_BRIGHTNESS_BOOST="; // const-string v1, " SUPPORT_MANUAL_BRIGHTNESS_BOOST="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MANUAL_BRIGHTNESS_BOOST:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1912 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mMaxManualBoostBrightness="; // const-string v1, " mMaxManualBoostBrightness="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1913 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mManualBrightnessBoostEnable="; // const-string v1, " mManualBrightnessBoostEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1914 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mLastManualBoostBrightness="; // const-string v1, " mLastManualBoostBrightness="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastManualBoostBrightness:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1915 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mIsSupportManualBoostApp="; // const-string v1, " mIsSupportManualBoostApp="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSupportManualBoostApp:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1917 */
} // :cond_0
final String v0 = ""; // const-string v0, ""
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1918 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "BCBC_ENABLE="; // const-string v2, "BCBC_ENABLE="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v2, Lcom/android/server/display/DisplayPowerControllerImpl;->BCBC_ENABLE:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1919 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mGrayScale="; // const-string v2, " mGrayScale="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayScale:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1920 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mAppliedBcbc="; // const-string v2, " mAppliedBcbc="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedBcbc:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1921 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mBcbcBrightness="; // const-string v2, " mBcbcBrightness="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBcbcBrightness:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1922 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mCurrentGrayScale="; // const-string v2, " mCurrentGrayScale="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1923 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mColorInversionEnabled="; // const-string v2, " mColorInversionEnabled="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mColorInversionEnabled:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1924 */
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurtainAnimationAvailable:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1925 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1926 */
final String v1 = "Curtain animation state: "; // const-string v1, "Curtain animation state: "
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1927 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mCurtainAnimationAvailable="; // const-string v2, " mCurtainAnimationAvailable="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurtainAnimationAvailable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1928 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mCurtainAnimationEnabled="; // const-string v2, " mCurtainAnimationEnabled="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurtainAnimationEnabled:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1929 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mIsDynamicLockScreenShowing="; // const-string v2, " mIsDynamicLockScreenShowing="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsDynamicLockScreenShowing:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1930 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mPendingShowCurtainAnimation="; // const-string v2, " mPendingShowCurtainAnimation="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingShowCurtainAnimation:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1933 */
} // :cond_1
/* sget-boolean v1, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportGalleryHdr:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1934 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1935 */
final String v2 = "Gallery Hdr Boost:"; // const-string v2, "Gallery Hdr Boost:"
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1936 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " mSupportGalleryHdr="; // const-string v3, " mSupportGalleryHdr="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1937 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mIsGalleryHdrEnable="; // const-string v2, " mIsGalleryHdrEnable="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsGalleryHdrEnable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1938 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mGalleryHdrThrottled="; // const-string v2, " mGalleryHdrThrottled="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGalleryHdrThrottled:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1939 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mCurrentGalleryHdrBoostFactor="; // const-string v2, " mCurrentGalleryHdrBoostFactor="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGalleryHdrBoostFactor:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1941 */
v1 = this.mThermalObserver;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1942 */
(( com.android.server.display.ThermalObserver ) v1 ).dump ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/display/ThermalObserver;->dump(Ljava/io/PrintWriter;)V
/* .line 1946 */
} // :cond_2
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostAvailable:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1947 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1948 */
final String v1 = "Dolby Preview Brightness Boost:"; // const-string v1, "Dolby Preview Brightness Boost:"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1949 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mDolbyPreviewBoostAvailable="; // const-string v2, " mDolbyPreviewBoostAvailable="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostAvailable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1950 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mDolbyPreviewEnable="; // const-string v2, " mDolbyPreviewEnable="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewEnable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1951 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mDolbyPreviewBoostRatio="; // const-string v2, " mDolbyPreviewBoostRatio="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostRatio:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1954 */
} // :cond_3
v1 = this.mThermalBrightnessController;
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1955 */
(( com.android.server.display.ThermalBrightnessController ) v1 ).dump ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/display/ThermalBrightnessController;->dump(Ljava/io/PrintWriter;)V
/* .line 1956 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mThermalMaxBrightness="; // const-string v2, "mThermalMaxBrightness="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalMaxBrightness:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1957 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mBasedSdrBrightness="; // const-string v2, "mBasedSdrBrightness="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedSdrBrightness:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1958 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mBasedBrightness="; // const-string v2, "mBasedBrightness="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedBrightness:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1961 */
} // :cond_4
v1 = this.mLowBatteryLevelThreshold;
/* array-length v1, v1 */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1962 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1963 */
final String v1 = "Low Battery Level:"; // const-string v1, "Low Battery Level:"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1964 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mLowBatteryLevelThreshold="; // const-string v2, " mLowBatteryLevelThreshold="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mLowBatteryLevelThreshold;
java.util.Arrays .toString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1965 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mLowBatteryLevelBrightnessThreshold="; // const-string v2, " mLowBatteryLevelBrightnessThreshold="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mLowBatteryLevelBrightnessThreshold;
java.util.Arrays .toString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1968 */
} // :cond_5
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprBrightnessControlAvailable:Z */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 1969 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1970 */
final String v1 = "Opr Brightness Control:"; // const-string v1, "Opr Brightness Control:"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1971 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mOprBrightnessControlAvailable="; // const-string v2, " mOprBrightnessControlAvailable="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprBrightnessControlAvailable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1972 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mOprGrayscaleThreshold="; // const-string v2, " mOprGrayscaleThreshold="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mOprGrayscaleThreshold;
java.util.Arrays .toString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1973 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mOprNitThreshold="; // const-string v2, " mOprNitThreshold="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mOprNitThreshold;
java.util.Arrays .toString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1974 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mOprAmbientLuxThreshold="; // const-string v2, " mOprAmbientLuxThreshold="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprAmbientLuxThreshold:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1975 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mAppliedMaxOprBrightness="; // const-string v2, " mAppliedMaxOprBrightness="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1976 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mOprMaxNitThreshold="; // const-string v2, " mOprMaxNitThreshold="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprMaxNitThreshold:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1979 */
} // :cond_6
v1 = this.mCbmController;
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 1980 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1981 */
final String v0 = "Cbm Config: "; // const-string v0, "Cbm Config: "
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1982 */
v0 = this.mCbmController;
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->dump(Ljava/io/PrintWriter;)V
/* .line 1985 */
} // :cond_7
v0 = this.mRampRateController;
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 1986 */
(( com.android.server.display.RampRateController ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/RampRateController;->dump(Ljava/io/PrintWriter;)V
/* .line 1989 */
} // :cond_8
/* sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_DPC:Z */
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
/* .line 1990 */
v0 = this.mMiuiDisplayCloudController;
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 1991 */
(( com.android.server.display.MiuiDisplayCloudController ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/MiuiDisplayCloudController;->dump(Ljava/io/PrintWriter;)V
/* .line 1993 */
} // :cond_9
return;
} // .end method
public Float getClampedBrightnessForPeak ( Float p0 ) {
/* .locals 1 */
/* .param p1, "brightnessValue" # F */
/* .line 1783 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).isSupportPeakBrightness ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isSupportPeakBrightness()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mHbmController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1784 */
v0 = (( com.android.server.display.HighBrightnessModeController ) v0 ).getCurrentBrightnessMax ( ); // invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->getCurrentBrightnessMax()F
p1 = android.util.MathUtils .min ( p1,v0 );
/* .line 1786 */
} // :cond_0
} // .end method
protected Float getCustomBrightness ( Float p0, java.lang.String p1, Integer p2, Float p3, Float p4, Boolean p5 ) {
/* .locals 8 */
/* .param p1, "lux" # F */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "category" # I */
/* .param p4, "oldAutoBrightness" # F */
/* .param p5, "newAutoBrightness" # F */
/* .param p6, "isManuallySet" # Z */
/* .line 2338 */
v0 = this.mCbmController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2339 */
/* iget v6, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOrientation:I */
/* move v1, p1 */
/* move-object v2, p2 */
/* move v3, p3 */
/* move v4, p4 */
/* move v5, p5 */
/* move v7, p6 */
p5 = /* invoke-virtual/range {v0 ..v7}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getCustomBrightness(FLjava/lang/String;IFFIZ)F */
/* .line 2344 */
} // :cond_0
} // .end method
public Float getDolbyPreviewBoostRatio ( ) {
/* .locals 2 */
/* .line 1235 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostRatio:F */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* cmpg-float v1, v0, v1 */
/* if-ltz v1, :cond_1 */
/* const/high16 v1, 0x41200000 # 10.0f */
/* cmpl-float v1, v0, v1 */
/* if-lez v1, :cond_0 */
/* .line 1240 */
} // :cond_0
/* .line 1237 */
} // :cond_1
} // :goto_0
final String v0 = "DisplayPowerControllerImpl"; // const-string v0, "DisplayPowerControllerImpl"
final String v1 = "getDolbyPreviewBoostRatio: current dolby preview boost ratio is invalid."; // const-string v1, "getDolbyPreviewBoostRatio: current dolby preview boost ratio is invalid."
android.util.Slog .e ( v0,v1 );
/* .line 1238 */
/* const/high16 v0, 0x7fc00000 # Float.NaN */
} // .end method
public getDozeBrightnessThreshold ( ) {
/* .locals 3 */
/* .line 2487 */
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [F */
int v1 = 0; // const/4 v1, 0x0
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxDozeBrightnessFloat:F */
/* aput v2, v0, v1 */
int v1 = 1; // const/4 v1, 0x1
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMinDozeBrightnessFloat:F */
/* aput v2, v0, v1 */
} // .end method
protected Float getGalleryHdrBoostFactor ( Float p0, Float p1 ) {
/* .locals 6 */
/* .param p1, "sdrBacklight" # F */
/* .param p2, "hdrBacklight" # F */
/* .line 1293 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).isGalleryHdrEnable ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGalleryHdrEnable()Z
/* if-nez v0, :cond_0 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGalleryHdrBoostFactor:F */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* cmpl-float v0, v0, v1 */
/* if-nez v0, :cond_0 */
/* .line 1295 */
/* .line 1297 */
} // :cond_0
final String v0 = "null"; // const-string v0, "null"
/* .line 1298 */
/* .local v0, "tempReason":Ljava/lang/String; */
v1 = this.mDisplayDeviceConfig;
v1 = (( com.android.server.display.DisplayDeviceConfig ) v1 ).getNitsFromBacklight ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
/* .line 1299 */
/* .local v1, "hdrNit":F */
v2 = this.mDisplayDeviceConfig;
v2 = (( com.android.server.display.DisplayDeviceConfig ) v2 ).getNitsFromBacklight ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
/* .line 1300 */
/* .local v2, "sdrNit":F */
v3 = /* invoke-direct {p0, v1, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->calculateGalleryHdrBoostFactor(FF)F */
/* .line 1302 */
/* .local v3, "factor":F */
v4 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).isEnterGalleryHdr ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isEnterGalleryHdr()Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1304 */
final String v0 = "enter_gallery_hdr_boost"; // const-string v0, "enter_gallery_hdr_boost"
/* .line 1305 */
} // :cond_1
v4 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).isExitGalleryHdr ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isExitGalleryHdr()Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 1307 */
final String v0 = "exit_gallery_hdr_boost"; // const-string v0, "exit_gallery_hdr_boost"
/* .line 1308 */
} // :cond_2
/* iget-boolean v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedDimming:Z */
if ( v4 != null) { // if-eqz v4, :cond_3
v4 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).isGalleryHdrEnable ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGalleryHdrEnable()Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 1310 */
final String v0 = "enter_dim_state"; // const-string v0, "enter_dim_state"
/* .line 1312 */
} // :cond_3
} // :goto_0
/* iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGalleryHdrBoostFactor:F */
/* .line 1313 */
/* iget-boolean v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 1314 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "getGalleryHdrBoostFactor: reason:"; // const-string v5, "getGalleryHdrBoostFactor: reason:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", hdrBrightness: "; // const-string v5, ", hdrBrightness: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = ", sdrBrightness: "; // const-string v5, ", sdrBrightness: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = ", mCurrentBrightness: "; // const-string v5, ", mCurrentBrightness: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentBrightness:F */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = ", mCurrentSdrBrightness: "; // const-string v5, ", mCurrentSdrBrightness: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentSdrBrightness:F */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = ", hdrNit: "; // const-string v5, ", hdrNit: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = ", sdrNit: "; // const-string v5, ", sdrNit: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = ", factor: "; // const-string v5, ", factor: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "DisplayPowerControllerImpl"; // const-string v5, "DisplayPowerControllerImpl"
android.util.Slog .d ( v5,v4 );
/* .line 1323 */
} // :cond_4
} // .end method
Float getGrayBrightnessFactor ( ) {
/* .locals 1 */
/* .line 1178 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F */
} // .end method
public Float getHbmDataMinimumLux ( ) {
/* .locals 2 */
/* .line 2367 */
/* const v0, 0x45bb8000 # 6000.0f */
/* .line 2368 */
/* .local v0, "minimumLux":F */
v1 = this.mHbmController;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = (( com.android.server.display.HighBrightnessModeController ) v1 ).deviceSupportsHbm ( ); // invoke-virtual {v1}, Lcom/android/server/display/HighBrightnessModeController;->deviceSupportsHbm()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2370 */
v1 = this.mHbmController;
(( com.android.server.display.HighBrightnessModeController ) v1 ).getHbmData ( ); // invoke-virtual {v1}, Lcom/android/server/display/HighBrightnessModeController;->getHbmData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;
/* iget v0, v1, Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;->minimumLux:F */
/* .line 2372 */
} // :cond_0
} // .end method
public Float getMaxHbmBrightnessForPeak ( ) {
/* .locals 1 */
/* .line 1775 */
} // .end method
public Float getMaxManualBrightnessBoost ( ) {
/* .locals 3 */
/* .line 702 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z */
/* const/high16 v1, -0x40800000 # -1.0f */
/* if-nez v0, :cond_0 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->shouldManualBoostForCurrentApp()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F */
/* cmpl-float v2, v0, v1 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 704 */
/* .line 706 */
} // :cond_0
} // .end method
public Float getMinBrightness ( ) {
/* .locals 2 */
/* .line 2383 */
v0 = this.mHbmController;
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = this.mDisplayDeviceConfig;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2384 */
v0 = (( com.android.server.display.HighBrightnessModeController ) v0 ).getCurrentBrightnessMin ( ); // invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->getCurrentBrightnessMin()F
/* .line 2386 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Float getNormalMaxBrightness ( ) {
/* .locals 2 */
/* .line 2376 */
v0 = this.mHbmController;
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = this.mDisplayDeviceConfig;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2377 */
v0 = (( com.android.server.display.HighBrightnessModeController ) v0 ).getNormalBrightnessMax ( ); // invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->getNormalBrightnessMax()F
/* .line 2379 */
} // :cond_0
/* const/high16 v0, 0x3f800000 # 1.0f */
} // .end method
public Integer getProximityNegativeDebounceDelay ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "delay" # I */
/* .line 2573 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void init ( com.android.server.display.DisplayPowerController p0, android.content.Context p1, android.os.Looper p2, Integer p3, Float p4, Float p5, com.android.server.display.DisplayDeviceConfig p6, com.android.server.display.LogicalDisplay p7, com.android.server.display.HighBrightnessModeController p8 ) {
/* .locals 19 */
/* .param p1, "displayPowerController" # Lcom/android/server/display/DisplayPowerController; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "looper" # Landroid/os/Looper; */
/* .param p4, "displayId" # I */
/* .param p5, "brightnessMin" # F */
/* .param p6, "brightnessMax" # F */
/* .param p7, "displayDeviceConfig" # Lcom/android/server/display/DisplayDeviceConfig; */
/* .param p8, "logicalDisplay" # Lcom/android/server/display/LogicalDisplay; */
/* .param p9, "hbmController" # Lcom/android/server/display/HighBrightnessModeController; */
/* .line 403 */
/* move-object/from16 v9, p0 */
/* move-object/from16 v10, p2 */
/* move-object/from16 v11, p3 */
/* move/from16 v12, p4 */
this.mContext = v10;
/* .line 404 */
/* move-object/from16 v13, p1 */
this.mDisplayPowerController = v13;
/* .line 405 */
/* iput v12, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayId:I */
/* .line 406 */
/* move-object/from16 v14, p7 */
this.mDisplayDeviceConfig = v14;
/* .line 407 */
/* move-object/from16 v15, p8 */
this.mLogicalDisplay = v15;
/* .line 408 */
/* move-object/from16 v8, p9 */
this.mHbmController = v8;
/* .line 409 */
com.android.server.display.DisplayManagerServiceStub .getInstance ( );
/* instance-of v0, v0, Lcom/android/server/display/DisplayManagerServiceImpl; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 410 */
com.android.server.display.DisplayManagerServiceStub .getInstance ( );
/* check-cast v0, Lcom/android/server/display/DisplayManagerServiceImpl; */
this.mDisplayMangerServiceImpl = v0;
/* .line 412 */
} // :cond_0
v0 = this.mDisplayMangerServiceImpl;
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v1, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayId:I */
/* if-nez v1, :cond_1 */
/* .line 413 */
(( com.android.server.display.DisplayManagerServiceImpl ) v0 ).setUpDisplayPowerControllerImpl ( v9 ); // invoke-virtual {v0, v9}, Lcom/android/server/display/DisplayManagerServiceImpl;->setUpDisplayPowerControllerImpl(Lcom/android/server/display/DisplayPowerControllerImpl;)V
/* .line 415 */
} // :cond_1
/* nop */
/* .line 416 */
com.android.server.display.DisplayFeatureManagerServiceStub .getInstance ( );
/* check-cast v0, Lcom/android/server/display/DisplayFeatureManagerServiceImpl; */
this.mDisplayFeatureManagerServicImpl = v0;
/* .line 417 */
/* iget v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayId:I */
/* if-nez v0, :cond_2 */
/* .line 418 */
final String v0 = "config_sunlight_mode_available"; // const-string v0, "config_sunlight_mode_available"
int v1 = 1; // const/4 v1, 0x1
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* iput-boolean v1, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeAvailable:Z */
/* .line 419 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 420 */
/* const v1, 0x11050016 */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurtainAnimationAvailable:Z */
/* .line 421 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 422 */
/* const v1, 0x11050087 */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessControlAvailable:Z */
/* .line 423 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 424 */
/* const v1, 0x1105006c */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprBrightnessControlAvailable:Z */
/* .line 425 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 426 */
/* const v1, 0x110b001d */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprAmbientLuxThreshold:I */
/* .line 427 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x1103004a */
(( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
this.mOprGrayscaleThreshold = v0;
/* .line 429 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x1103004b */
(( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
this.mOprNitThreshold = v0;
/* .line 431 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 432 */
/* const v1, 0x110b001e */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprMaxNitThreshold:I */
/* .line 433 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 434 */
/* const v1, 0x1107002f */
v0 = (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F */
/* .line 436 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x11030040 */
(( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
this.mLowBatteryLevelThreshold = v0;
/* .line 438 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x1103003f */
(( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
this.mLowBatteryLevelBrightnessThreshold = v0;
/* .line 441 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x1105001e */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostAvailable:Z */
/* .line 444 */
/* new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler; */
/* invoke-direct {v0, v9, v11}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 445 */
/* iget-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeAvailable:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 446 */
/* new-instance v0, Lcom/android/server/display/SunlightController; */
/* invoke-direct {v0, v10, v9, v11, v12}, Lcom/android/server/display/SunlightController;-><init>(Landroid/content/Context;Lcom/android/server/display/SunlightController$Callback;Landroid/os/Looper;I)V */
this.mSunlightController = v0;
/* .line 448 */
} // :cond_3
final String v0 = "SurfaceFlinger"; // const-string v0, "SurfaceFlinger"
android.os.ServiceManager .getService ( v0 );
this.mISurfaceFlinger = v0;
/* .line 449 */
/* move/from16 v7, p5 */
/* iput v7, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenBrightnessRangeMinimum:F */
/* .line 450 */
/* move/from16 v6, p6 */
/* iput v6, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenBrightnessRangeMaximum:F */
/* .line 451 */
v0 = this.mDisplayDeviceConfig;
/* const/high16 v1, 0x42700000 # 60.0f */
v0 = (( com.android.server.display.DisplayDeviceConfig ) v0 ).getBrightnessFromNit ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F
/* iput v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxDozeBrightnessFloat:F */
/* .line 452 */
v0 = this.mDisplayDeviceConfig;
/* const/high16 v1, 0x40a00000 # 5.0f */
v0 = (( com.android.server.display.DisplayDeviceConfig ) v0 ).getBrightnessFromNit ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F
/* iput v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mMinDozeBrightnessFloat:F */
/* .line 454 */
/* new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver; */
v1 = this.mHandler;
/* invoke-direct {v0, v9, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 455 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->registerSettingsObserver()V */
/* .line 456 */
android.app.ActivityTaskManager .getService ( );
this.mActivityTaskManager = v0;
/* .line 457 */
/* new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$TaskStackListenerImpl; */
/* invoke-direct {v0, v9}, Lcom/android/server/display/DisplayPowerControllerImpl$TaskStackListenerImpl;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V */
this.mTaskStackListener = v0;
/* .line 458 */
/* const-class v0, Lcom/android/server/policy/WindowManagerPolicy; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/policy/WindowManagerPolicy; */
this.mPolicy = v0;
/* .line 459 */
v0 = this.mContext;
final String v1 = "power"; // const-string v1, "power"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/PowerManager; */
this.mPowerManager = v0;
/* .line 460 */
/* const-string/jumbo v0, "window" */
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
this.mWms = v0;
/* .line 461 */
/* new-instance v0, Lcom/android/server/display/RampRateController; */
v1 = this.mDisplayDeviceConfig;
/* invoke-direct {v0, v10, v9, v1, v11}, Lcom/android/server/display/RampRateController;-><init>(Landroid/content/Context;Lcom/android/server/display/DisplayPowerControllerImpl;Lcom/android/server/display/DisplayDeviceConfig;Landroid/os/Looper;)V */
this.mRampRateController = v0;
/* .line 462 */
/* if-nez v12, :cond_8 */
/* .line 463 */
/* new-instance v5, Lcom/android/server/display/statistics/BrightnessDataProcessor; */
v0 = this.mHbmController;
/* .line 464 */
v3 = (( com.android.server.display.HighBrightnessModeController ) v0 ).getCurrentBrightnessMin ( ); // invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->getCurrentBrightnessMin()F
v0 = this.mHbmController;
/* .line 465 */
v4 = (( com.android.server.display.HighBrightnessModeController ) v0 ).getNormalBrightnessMax ( ); // invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->getNormalBrightnessMax()F
/* .line 466 */
v16 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getHbmDataMinimumLux()F */
/* move-object v0, v5 */
/* move-object/from16 v1, p2 */
/* move-object/from16 v2, p7 */
/* move-object v6, v5 */
/* move/from16 v5, v16 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;-><init>(Landroid/content/Context;Lcom/android/server/display/DisplayDeviceConfig;FFF)V */
this.mBrightnessDataProcessor = v6;
/* .line 467 */
/* new-instance v0, Lcom/android/server/display/MiuiDisplayCloudController; */
/* invoke-direct {v0, v11, v9, v10}, Lcom/android/server/display/MiuiDisplayCloudController;-><init>(Landroid/os/Looper;Lcom/android/server/display/MiuiDisplayCloudController$Callback;Landroid/content/Context;)V */
this.mMiuiDisplayCloudController = v0;
/* .line 469 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x11050033 */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportIndividualBrightness:Z */
/* .line 471 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x11050017 */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportCustomBrightness:Z */
/* .line 473 */
/* iget-boolean v1, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportIndividualBrightness:Z */
/* if-nez v1, :cond_4 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 474 */
} // :cond_4
/* new-instance v6, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController; */
v5 = this.mBrightnessDataProcessor;
/* .line 476 */
v16 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getHbmDataMinimumLux()F */
v17 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getNormalMaxBrightness()F */
v18 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getMinBrightness()F */
/* move-object v0, v6 */
/* move-object/from16 v1, p2 */
/* move-object/from16 v2, p3 */
/* move-object/from16 v3, p7 */
/* move-object/from16 v4, p0 */
/* move-object v11, v6 */
/* move/from16 v6, v16 */
/* move/from16 v7, v17 */
/* move/from16 v8, v18 */
/* invoke-direct/range {v0 ..v8}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/display/DisplayDeviceConfig;Lcom/android/server/display/DisplayPowerControllerImpl;Lcom/android/server/display/statistics/BrightnessDataProcessor;FFF)V */
this.mCbmController = v11;
/* .line 477 */
v0 = this.mMiuiDisplayCloudController;
(( com.android.server.display.MiuiDisplayCloudController ) v0 ).addCloudListener ( v11 ); // invoke-virtual {v0, v11}, Lcom/android/server/display/MiuiDisplayCloudController;->addCloudListener(Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;)V
/* .line 478 */
/* new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$RotationWatcher; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v9, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$RotationWatcher;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;Lcom/android/server/display/DisplayPowerControllerImpl$RotationWatcher-IA;)V */
this.mRotationWatcher = v0;
/* .line 481 */
} // :cond_5
/* new-instance v0, Lcom/android/server/display/ThermalObserver; */
com.android.internal.os.BackgroundThread .get ( );
(( com.android.internal.os.BackgroundThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1, v9}, Lcom/android/server/display/ThermalObserver;-><init>(Landroid/os/Looper;Lcom/android/server/display/DisplayPowerControllerImpl;)V */
this.mThermalObserver = v0;
/* .line 482 */
/* iget-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessControlAvailable:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 483 */
/* new-instance v0, Lcom/android/server/display/ThermalBrightnessController; */
v1 = this.mContext;
v2 = this.mHandler;
(( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v2 ).getLooper ( ); // invoke-virtual {v2}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->getLooper()Landroid/os/Looper;
v3 = this.mThermalBrightnessCallback;
/* invoke-direct {v0, v1, v2, v3, v9}, Lcom/android/server/display/ThermalBrightnessController;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/display/ThermalBrightnessController$Callback;Lcom/android/server/display/DisplayPowerControllerImpl;)V */
this.mThermalBrightnessController = v0;
/* .line 484 */
v1 = this.mBrightnessDataProcessor;
(( com.android.server.display.ThermalBrightnessController ) v0 ).addThermalListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/ThermalBrightnessController;->addThermalListener(Lcom/android/server/display/ThermalBrightnessController$ThermalListener;)V
/* .line 485 */
v0 = this.mMiuiDisplayCloudController;
v1 = this.mThermalBrightnessController;
(( com.android.server.display.MiuiDisplayCloudController ) v0 ).addCloudListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/MiuiDisplayCloudController;->addCloudListener(Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;)V
/* .line 486 */
v0 = this.mMiuiDisplayCloudController;
v1 = this.mBrightnessDataProcessor;
(( com.android.server.display.MiuiDisplayCloudController ) v0 ).addCloudListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/MiuiDisplayCloudController;->addCloudListener(Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;)V
/* .line 487 */
/* new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$Injector; */
/* invoke-direct {v0, v10}, Lcom/android/server/display/DisplayPowerControllerImpl$Injector;-><init>(Landroid/content/Context;)V */
this.mInjector = v0;
/* .line 490 */
} // :cond_6
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, v9}, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V */
(( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->post(Ljava/lang/Runnable;)Z
/* .line 493 */
/* sget-boolean v0, Lcom/android/server/display/DisplayPowerControllerImpl;->IS_FOLDABLE_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 494 */
/* const-class v0, Landroid/hardware/devicestate/DeviceStateManager; */
(( android.content.Context ) v10 ).getSystemService ( v0 ); // invoke-virtual {v10, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/devicestate/DeviceStateManager; */
/* .line 495 */
/* .local v0, "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager; */
/* new-instance v1, Landroid/os/HandlerExecutor; */
v2 = this.mHandler;
/* invoke-direct {v1, v2}, Landroid/os/HandlerExecutor;-><init>(Landroid/os/Handler;)V */
/* new-instance v2, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener; */
/* new-instance v3, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v3, v9}, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V */
/* invoke-direct {v2, v10, v3}, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;-><init>(Landroid/content/Context;Ljava/util/function/Consumer;)V */
(( android.hardware.devicestate.DeviceStateManager ) v0 ).registerCallback ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/devicestate/DeviceStateManager;->registerCallback(Ljava/util/concurrent/Executor;Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;)V
/* .line 498 */
} // .end local v0 # "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager;
} // :cond_7
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->registerBroadcastsReceiver()V */
/* .line 499 */
/* new-instance v0, Lcom/android/server/display/BrightnessABTester; */
com.android.internal.os.BackgroundThread .get ( );
(( com.android.internal.os.BackgroundThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;
v2 = this.mContext;
v3 = this.mBrightnessDataProcessor;
/* invoke-direct {v0, v1, v2, v3}, Lcom/android/server/display/BrightnessABTester;-><init>(Landroid/os/Looper;Landroid/content/Context;Lcom/android/server/display/statistics/BrightnessDataProcessor;)V */
this.mBrightnessABTester = v0;
/* .line 503 */
} // :cond_8
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->computeBCBCAdjustmentParams()V */
/* .line 506 */
/* new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$2; */
/* invoke-direct {v0, v9}, Lcom/android/server/display/DisplayPowerControllerImpl$2;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V */
this.mHdrStateListener = v0;
/* .line 529 */
return;
} // .end method
public Boolean isAnimating ( ) {
/* .locals 1 */
/* .line 2425 */
v0 = this.mBrightnessRampAnimator;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2426 */
v0 = (( com.android.server.display.RampAnimator$DualRampAnimator ) v0 ).isAnimating ( ); // invoke-virtual {v0}, Lcom/android/server/display/RampAnimator$DualRampAnimator;->isAnimating()Z
/* .line 2428 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isBrightnessCurveOptimizePolicyDisable ( ) {
/* .locals 1 */
/* .line 2454 */
v0 = this.mMiuiDisplayCloudController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2455 */
v0 = (( com.android.server.display.MiuiDisplayCloudController ) v0 ).isBrightnessCurveOptimizePolicyDisable ( ); // invoke-virtual {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->isBrightnessCurveOptimizePolicyDisable()Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 2454 */
} // :goto_0
} // .end method
public Boolean isColorInversionEnabled ( ) {
/* .locals 1 */
/* .line 1201 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mColorInversionEnabled:Z */
} // .end method
public Boolean isCurtainAnimationNeeded ( ) {
/* .locals 1 */
/* .line 551 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingShowCurtainAnimation:Z */
} // .end method
public Boolean isDolbyPreviewEnable ( ) {
/* .locals 1 */
/* .line 1226 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewEnable:Z */
} // .end method
protected Boolean isEnterGalleryHdr ( ) {
/* .locals 2 */
/* .line 1274 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).isGalleryHdrEnable ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGalleryHdrEnable()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSlowChange:Z */
/* if-nez v0, :cond_0 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mTargetBrightness:F */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mTargetSdrBrightness:F */
/* cmpl-float v1, v0, v1 */
/* if-lez v1, :cond_0 */
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedDimming:Z */
/* if-nez v1, :cond_0 */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentBrightness:F */
/* cmpl-float v0, v0, v1 */
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
protected Boolean isExitGalleryHdr ( ) {
/* .locals 2 */
/* .line 1283 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentBrightness:F */
int v1 = 0; // const/4 v1, 0x0
/* cmpl-float v0, v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1284 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).isGalleryHdrEnable ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGalleryHdrEnable()Z
/* if-nez v0, :cond_0 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGalleryHdrBoostFactor:F */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* cmpl-float v0, v0, v1 */
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1283 */
} // :goto_0
} // .end method
public Boolean isFolded ( ) {
/* .locals 1 */
/* .line 562 */
v0 = this.mFolded;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 563 */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 566 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isGalleryHdrEnable ( ) {
/* .locals 1 */
/* .line 1268 */
/* sget-boolean v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportGalleryHdr:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsGalleryHdrEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGalleryHdrThrottled:Z */
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isSupportManualBrightnessBoost ( ) {
/* .locals 1 */
/* .line 698 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MANUAL_BRIGHTNESS_BOOST:Z */
} // .end method
public Boolean isSupportPeakBrightness ( ) {
/* .locals 2 */
/* .line 1771 */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* cmpl-float v0, v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
protected Boolean isTemporaryDimmingEnabled ( ) {
/* .locals 2 */
/* .line 2295 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2296 */
/* .local v0, "enable":Z */
v1 = this.mRampRateController;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2297 */
v0 = (( com.android.server.display.RampRateController ) v1 ).isTemporaryDimming ( ); // invoke-virtual {v1}, Lcom/android/server/display/RampRateController;->isTemporaryDimming()Z
/* .line 2299 */
} // :cond_0
} // .end method
public void mayBeReportUserDisableSunlightTemporary ( Float p0 ) {
/* .locals 1 */
/* .param p1, "tempBrightness" # F */
/* .line 587 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeAvailable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedSunlightMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = java.lang.Float .isNaN ( p1 );
/* if-nez v0, :cond_0 */
/* .line 588 */
v0 = this.mSunlightController;
(( com.android.server.display.SunlightController ) v0 ).setSunlightModeDisabledByUserTemporary ( ); // invoke-virtual {v0}, Lcom/android/server/display/SunlightController;->setSunlightModeDisabledByUserTemporary()V
/* .line 590 */
} // :cond_0
return;
} // .end method
public void notifyAonFlareEvents ( Integer p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "preLux" # F */
/* .line 2560 */
v0 = this.mBrightnessDataProcessor;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2561 */
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).notifyAonFlareEvents ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->notifyAonFlareEvents(IF)V
/* .line 2563 */
} // :cond_0
return;
} // .end method
public void notifyBrightnessChangeIfNeeded ( Boolean p0, Float p1, Boolean p2, Boolean p3, Float p4, Boolean p5, Float p6, Float p7, Boolean p8, Float p9 ) {
/* .locals 22 */
/* .param p1, "screenOn" # Z */
/* .param p2, "brightness" # F */
/* .param p3, "userInitiatedChange" # Z */
/* .param p4, "useAutoBrightness" # Z */
/* .param p5, "brightnessOverrideFromWindow" # F */
/* .param p6, "lowPowerMode" # Z */
/* .param p7, "ambientLux" # F */
/* .param p8, "userDataPoint" # F */
/* .param p9, "defaultConfig" # Z */
/* .param p10, "actualBrightness" # F */
/* .line 845 */
/* move-object/from16 v0, p0 */
v1 = this.mBrightnessDataProcessor;
if ( v1 != null) { // if-eqz v1, :cond_4
v1 = this.mAutomaticBrightnessControllerImpl;
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 846 */
v1 = this.mHbmController;
int v2 = 1; // const/4 v2, 0x1
int v3 = 2; // const/4 v3, 0x2
int v4 = 0; // const/4 v4, 0x0
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = (( com.android.server.display.HighBrightnessModeController ) v1 ).getHighBrightnessMode ( ); // invoke-virtual {v1}, Lcom/android/server/display/HighBrightnessModeController;->getHighBrightnessMode()I
/* if-eq v1, v3, :cond_0 */
v1 = this.mHbmController;
/* .line 847 */
v1 = (( com.android.server.display.HighBrightnessModeController ) v1 ).isDolbyEnable ( ); // invoke-virtual {v1}, Lcom/android/server/display/HighBrightnessModeController;->isDolbyEnable()Z
if ( v1 != null) { // if-eqz v1, :cond_1
} // :cond_0
/* move/from16 v17, v2 */
} // :cond_1
/* move/from16 v17, v4 */
/* .line 848 */
/* .local v17, "isHdrLayer":Z */
} // :goto_0
/* iget-boolean v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedDimming:Z */
/* if-nez v1, :cond_3 */
/* iget v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPreviousDisplayPolicy:I */
/* if-ne v1, v3, :cond_2 */
/* iget v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentDisplayPolicy:I */
int v3 = 3; // const/4 v3, 0x3
/* if-ne v1, v3, :cond_2 */
} // :cond_2
/* move/from16 v18, v4 */
} // :cond_3
} // :goto_1
/* move/from16 v18, v2 */
/* .line 852 */
/* .local v18, "isDimmingChanged":Z */
} // :goto_2
v5 = this.mBrightnessDataProcessor;
/* iget-boolean v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeActive:Z */
/* move/from16 v16, v1 */
v1 = this.mAutomaticBrightnessControllerImpl;
/* .line 856 */
v19 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v1 ).getMainFastAmbientLux ( ); // invoke-virtual {v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getMainFastAmbientLux()F
v1 = this.mAutomaticBrightnessControllerImpl;
/* .line 857 */
v20 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v1 ).getAssistFastAmbientLux ( ); // invoke-virtual {v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getAssistFastAmbientLux()F
/* .line 858 */
v21 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getSceneState()I */
/* .line 852 */
/* move/from16 v6, p1 */
/* move/from16 v7, p2 */
/* move/from16 v8, p10 */
/* move/from16 v9, p3 */
/* move/from16 v10, p4 */
/* move/from16 v11, p5 */
/* move/from16 v12, p6 */
/* move/from16 v13, p7 */
/* move/from16 v14, p8 */
/* move/from16 v15, p9 */
/* invoke-virtual/range {v5 ..v21}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->notifyBrightnessEventIfNeeded(ZFFZZFZFFZZZZFFI)V */
/* .line 860 */
} // .end local v17 # "isHdrLayer":Z
} // .end local v18 # "isDimmingChanged":Z
} // :cond_4
return;
} // .end method
public void notifyDisableResetShortTermModel ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 1393 */
v0 = this.mAutomaticBrightnessControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1394 */
(( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).notifyDisableResetShortTermModel ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->notifyDisableResetShortTermModel(Z)V
/* .line 1396 */
} // :cond_0
return;
} // .end method
public void notifyDisplaySwapFinished ( ) {
/* .locals 2 */
/* .line 2584 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).isFolded ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isFolded()Z
/* .line 2585 */
/* .local v0, "foldedState":Z */
/* sget-boolean v1, Lcom/android/server/display/DisplayPowerControllerImpl;->IS_FOLDABLE_DEVICE:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastFoldedState:Z */
/* if-eq v0, v1, :cond_0 */
/* .line 2586 */
com.android.server.display.statistics.OneTrackFoldStateHelper .getInstance ( );
(( com.android.server.display.statistics.OneTrackFoldStateHelper ) v1 ).notifyDisplaySwapFinished ( ); // invoke-virtual {v1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->notifyDisplaySwapFinished()V
/* .line 2587 */
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastFoldedState:Z */
/* .line 2589 */
} // :cond_0
return;
} // .end method
public void notifyFocusedWindowChanged ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "focusedPackageName" # Ljava/lang/String; */
/* .line 2579 */
com.android.server.display.statistics.OneTrackFoldStateHelper .getInstance ( );
(( com.android.server.display.statistics.OneTrackFoldStateHelper ) v0 ).notifyFocusedWindowChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->notifyFocusedWindowChanged(Ljava/lang/String;)V
/* .line 2580 */
return;
} // .end method
protected void notifyOutDoorHighTempState ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "changed" # Z */
/* .line 2226 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda5; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;Z)V */
(( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->post(Ljava/lang/Runnable;)Z
/* .line 2227 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2228 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "notifyOutDoorState: mOutDoorHighTempState: "; // const-string v1, "notifyOutDoorState: mOutDoorHighTempState: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "DisplayPowerControllerImpl"; // const-string v1, "DisplayPowerControllerImpl"
android.util.Slog .d ( v1,v0 );
/* .line 2230 */
} // :cond_0
return;
} // .end method
public void notifySunlightModeChanged ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isSunlightModeEnable" # Z */
/* .line 687 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MANUAL_BRIGHTNESS_BOOST:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSunlightModeEnable:Z */
/* if-eq v0, p1, :cond_1 */
/* .line 689 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBootCompleted:Z */
/* if-nez v0, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 690 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z */
/* .line 692 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSunlightModeEnable:Z */
/* .line 693 */
v0 = this.mDisplayPowerController;
(( com.android.server.display.DisplayPowerController ) v0 ).updateBrightness ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V
/* .line 695 */
} // :cond_1
return;
} // .end method
public void notifySunlightStateChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "active" # Z */
/* .line 666 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 667 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "notifySunlightStateChange: "; // const-string v1, "notifySunlightStateChange: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "DisplayPowerControllerImpl"; // const-string v1, "DisplayPowerControllerImpl"
android.util.Slog .d ( v1,v0 );
/* .line 669 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeActive:Z */
/* .line 670 */
v0 = this.mSunlightStateListener;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 671 */
/* .line 673 */
} // :cond_1
v0 = this.mThermalBrightnessController;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 674 */
(( com.android.server.display.ThermalBrightnessController ) v0 ).setSunlightState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/ThermalBrightnessController;->setSunlightState(Z)V
/* .line 676 */
} // :cond_2
return;
} // .end method
public void notifyThresholdSunlightNitChanged ( Float p0 ) {
/* .locals 2 */
/* .param p1, "thresholdSunlightNit" # F */
/* .line 1400 */
v0 = this.mSunlightController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1401 */
java.lang.Float .valueOf ( p1 );
(( com.android.server.display.SunlightController ) v0 ).updateThresholdSunlightNit ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/SunlightController;->updateThresholdSunlightNit(Ljava/lang/Float;)V
/* .line 1403 */
} // :cond_0
return;
} // .end method
public void notifyUpdateBrightness ( ) {
/* .locals 1 */
/* .line 2566 */
v0 = this.mBrightnessDataProcessor;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2567 */
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).notifyUpdateBrightness ( ); // invoke-virtual {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->notifyUpdateBrightness()V
/* .line 2569 */
} // :cond_0
return;
} // .end method
public void notifyUpdateBrightnessAnimInfo ( Float p0, Float p1, Float p2 ) {
/* .locals 1 */
/* .param p1, "currentBrightnessAnim" # F */
/* .param p2, "brightnessAnim" # F */
/* .param p3, "targetBrightnessAnim" # F */
/* .line 2038 */
v0 = this.mBrightnessDataProcessor;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mUpdateBrightnessAnimInfoEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2040 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).isTemporaryDimmingEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isTemporaryDimmingEnabled()Z
/* if-nez v0, :cond_0 */
/* .line 2041 */
v0 = this.mBrightnessDataProcessor;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).notifyUpdateBrightnessAnimInfo ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->notifyUpdateBrightnessAnimInfo(FFF)V
/* .line 2044 */
} // :cond_0
return;
} // .end method
public void notifyUpdateTempBrightnessTimeStamp ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 2053 */
v0 = this.mBrightnessDataProcessor;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2054 */
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).notifyUpdateTempBrightnessTimeStampIfNeeded ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->notifyUpdateTempBrightnessTimeStampIfNeeded(Z)V
/* .line 2056 */
} // :cond_0
return;
} // .end method
public void onAnimateValueChanged ( Float p0 ) {
/* .locals 1 */
/* .param p1, "animateValue" # F */
/* .line 2550 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedBrightness:F */
v0 = com.android.internal.display.BrightnessSynchronizer .floatEquals ( p1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2551 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->onAnimateValueChanged(Z)V */
/* .line 2552 */
return;
/* .line 2554 */
} // :cond_0
/* iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedBrightness:F */
/* .line 2555 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetRateModifierOnAnimateValueChanged()V */
/* .line 2556 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->onAnimateValueChanged(Z)V */
/* .line 2557 */
return;
} // .end method
public void onAnimationEnd ( ) {
/* .locals 1 */
/* .line 2542 */
v0 = this.mRampRateController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2543 */
(( com.android.server.display.RampRateController ) v0 ).clearAllRateModifier ( ); // invoke-virtual {v0}, Lcom/android/server/display/RampRateController;->clearAllRateModifier()V
/* .line 2545 */
} // :cond_0
return;
} // .end method
public void onBootCompleted ( ) {
/* .locals 1 */
/* .line 2097 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBootCompleted:Z */
/* .line 2098 */
v0 = this.mCbmController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2099 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).onBootCompleted ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->onBootCompleted()V
/* .line 2101 */
} // :cond_0
return;
} // .end method
public void onCurtainAnimationFinished ( ) {
/* .locals 1 */
/* .line 556 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingShowCurtainAnimation:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 557 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingShowCurtainAnimation:Z */
/* .line 559 */
} // :cond_0
return;
} // .end method
public void receiveNoticeFromDisplayPowerController ( Integer p0, android.os.Bundle p1 ) {
/* .locals 0 */
/* .param p1, "code" # I */
/* .param p2, "bundle" # Landroid/os/Bundle; */
/* .line 1447 */
/* packed-switch p1, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 1455 */
/* :pswitch_1 */
(( com.android.server.display.DisplayPowerControllerImpl ) p0 ).updateCurrentGrayScale ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateCurrentGrayScale(Landroid/os/Bundle;)V
/* .line 1456 */
/* .line 1452 */
/* :pswitch_2 */
/* invoke-direct {p0, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateDolbyState(Landroid/os/Bundle;)V */
/* .line 1453 */
/* .line 1449 */
/* :pswitch_3 */
(( com.android.server.display.DisplayPowerControllerImpl ) p0 ).updateGrayScale ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateGrayScale(Landroid/os/Bundle;)V
/* .line 1450 */
/* nop */
/* .line 1460 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
protected void registerRotationWatcher ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "enable" # Z */
/* .line 2312 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportIndividualBrightness:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportCustomBrightness:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_0
v0 = this.mRotationWatcher;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 2314 */
final String v1 = "DisplayPowerControllerImpl"; // const-string v1, "DisplayPowerControllerImpl"
int v2 = 0; // const/4 v2, 0x0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 2315 */
/* iget-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRotationListenerEnabled:Z */
/* if-nez v3, :cond_2 */
/* .line 2316 */
int v3 = 1; // const/4 v3, 0x1
/* iput-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRotationListenerEnabled:Z */
/* .line 2317 */
v3 = this.mWms;
(( com.android.server.wm.WindowManagerService ) v3 ).watchRotation ( v0, v2 ); // invoke-virtual {v3, v0, v2}, Lcom/android/server/wm/WindowManagerService;->watchRotation(Landroid/view/IRotationWatcher;I)I
/* .line 2318 */
final String v0 = "Register rotation listener."; // const-string v0, "Register rotation listener."
android.util.Slog .d ( v1,v0 );
/* .line 2321 */
} // :cond_1
/* iget-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRotationListenerEnabled:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 2322 */
/* iput-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRotationListenerEnabled:Z */
/* .line 2323 */
v2 = this.mWms;
(( com.android.server.wm.WindowManagerService ) v2 ).removeRotationWatcher ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/wm/WindowManagerService;->removeRotationWatcher(Landroid/view/IRotationWatcher;)V
/* .line 2324 */
final String v0 = "Unregister rotation listener."; // const-string v0, "Unregister rotation listener."
android.util.Slog .d ( v1,v0 );
/* .line 2328 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void resetShortTermModel ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "manually" # Z */
/* .line 2357 */
v0 = this.mCbmController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2358 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).resetShortTermModel ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->resetShortTermModel(Z)V
/* .line 2360 */
} // :cond_0
return;
} // .end method
public void sendBrightnessToSurfaceFlingerIfNeeded ( Float p0, Float p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "target" # F */
/* .param p2, "dozeBrightness" # F */
/* .param p3, "isDimming" # Z */
/* .line 809 */
v0 = this.mDisplayPowerController;
(( com.android.server.display.DisplayPowerController ) v0 ).getDisplayPowerState ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->getDisplayPowerState()Lcom/android/server/display/DisplayPowerState;
v0 = (( com.android.server.display.DisplayPowerState ) v0 ).getScreenState ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerState;->getScreenState()I
/* packed-switch v0, :pswitch_data_0 */
/* .line 811 */
/* :pswitch_0 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mActualScreenOnBrightness:F */
/* cmpl-float v0, p1, v0 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* if-nez p3, :cond_4 */
/* .line 813 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeActive:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 814 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSettingsBrightnessBeforeApplySunlight:F */
v0 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( v0 );
/* int-to-float v0, v0 */
/* .line 815 */
} // :cond_0
/* move v0, p1 */
} // :goto_0
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mActualScreenOnBrightness:F */
/* .line 820 */
/* :pswitch_1 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDozeScreenBrightness:F */
v0 = com.android.internal.display.BrightnessSynchronizer .floatEquals ( p2,v0 );
/* if-nez v0, :cond_2 */
/* .line 821 */
/* iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDozeScreenBrightness:F */
/* .line 822 */
v0 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( p2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :goto_1
/* iput-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDozeInLowBrightness:Z */
/* .line 825 */
} // :cond_2
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDozeInLowBrightness:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 826 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDozeScreenBrightness:F */
} // :cond_3
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mActualScreenOnBrightness:F */
/* .line 827 */
/* .local v0, "pendingBrightness":F */
} // :goto_2
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDesiredBrightness:F */
/* cmpl-float v1, v0, v1 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 828 */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDesiredBrightness:F */
/* .line 829 */
/* nop */
/* .line 830 */
v1 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( v0 );
/* iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDesiredBrightnessInt:I */
/* .line 831 */
/* invoke-direct {p0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->sendSurfaceFlingerActualBrightness(I)V */
/* .line 837 */
} // .end local v0 # "pendingBrightness":F
} // :cond_4
} // :goto_3
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public void sendDimStateToSurfaceFlinger ( Boolean p0 ) {
/* .locals 7 */
/* .param p1, "isDim" # Z */
/* .line 2108 */
/* sget-boolean v0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_IDLE_DIM:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayId:I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2111 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
final String v1 = "DisplayPowerControllerImpl"; // const-string v1, "DisplayPowerControllerImpl"
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2112 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "sendDimStateToSurfaceFlinger is dim " */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 2114 */
} // :cond_1
v0 = this.mISurfaceFlinger;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 2115 */
android.os.Parcel .obtain ( );
/* .line 2116 */
/* .local v0, "data":Landroid/os/Parcel; */
final String v2 = "android.ui.ISurfaceComposer"; // const-string v2, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 2117 */
/* move v2, p1 */
/* .line 2118 */
/* .local v2, "val":I */
/* const/16 v3, 0x106 */
(( android.os.Parcel ) v0 ).writeInt ( v3 ); // invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 2119 */
(( android.os.Parcel ) v0 ).writeInt ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 2120 */
/* const-string/jumbo v3, "system" */
(( android.os.Parcel ) v0 ).writeString ( v3 ); // invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 2122 */
try { // :try_start_0
v3 = this.mISurfaceFlinger;
int v4 = 0; // const/4 v4, 0x0
int v5 = 1; // const/4 v5, 0x1
/* const/16 v6, 0x7983 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 2127 */
/* nop */
} // :goto_0
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 2128 */
/* .line 2127 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 2124 */
/* :catch_0 */
/* move-exception v3 */
/* .line 2125 */
/* .local v3, "ex":Ljava/lang/Exception; */
try { // :try_start_1
final String v4 = "Failed to send brightness to SurfaceFlinger"; // const-string v4, "Failed to send brightness to SurfaceFlinger"
android.util.Slog .e ( v1,v4,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 2127 */
/* nop */
} // .end local v3 # "ex":Ljava/lang/Exception;
} // :goto_1
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 2128 */
/* throw v1 */
/* .line 2130 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v2 # "val":I
} // :cond_2
} // :goto_2
return;
/* .line 2109 */
} // :cond_3
} // :goto_3
return;
} // .end method
public void setAppliedScreenBrightnessOverride ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isApplied" # Z */
/* .line 1369 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedScreenBrightnessOverride:Z */
/* if-eq v0, p1, :cond_1 */
/* .line 1370 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* if-nez p1, :cond_0 */
/* .line 1371 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetBCBCState()V */
/* .line 1373 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedScreenBrightnessOverride:Z */
/* .line 1374 */
v0 = this.mRampRateController;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1375 */
(( com.android.server.display.RampRateController ) v0 ).addOverrideRateModifier ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/RampRateController;->addOverrideRateModifier(Z)V
/* .line 1378 */
} // :cond_1
return;
} // .end method
public void setBrightnessConfiguration ( android.hardware.display.BrightnessConfiguration p0 ) {
/* .locals 2 */
/* .param p1, "config" # Landroid/hardware/display/BrightnessConfiguration; */
/* .line 2363 */
v0 = this.mDisplayPowerController;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.display.DisplayPowerController ) v0 ).setBrightnessConfiguration ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/display/DisplayPowerController;->setBrightnessConfiguration(Landroid/hardware/display/BrightnessConfiguration;Z)V
/* .line 2364 */
return;
} // .end method
public void setCustomCurveEnabledOnCommand ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 2397 */
v0 = this.mCbmController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2398 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).setCustomCurveEnabledOnCommand ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setCustomCurveEnabledOnCommand(Z)V
/* .line 2400 */
} // :cond_0
return;
} // .end method
public void setForceTrainEnabledOnCommand ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 2411 */
v0 = this.mCbmController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2412 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).setForceTrainEnabledOnCommand ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setForceTrainEnabledOnCommand(Z)V
/* .line 2414 */
} // :cond_0
return;
} // .end method
public void setIndividualModelEnabledOnCommand ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 2404 */
v0 = this.mCbmController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2405 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).setIndividualModelEnabledOnCommand ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setIndividualModelEnabledOnCommand(Z)V
/* .line 2407 */
} // :cond_0
return;
} // .end method
public void setRampAnimator ( com.android.server.display.RampAnimator$DualRampAnimator p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/display/RampAnimator$DualRampAnimator<", */
/* "Lcom/android/server/display/DisplayPowerState;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1610 */
/* .local p1, "rampAnimator":Lcom/android/server/display/RampAnimator$DualRampAnimator;, "Lcom/android/server/display/RampAnimator$DualRampAnimator<Lcom/android/server/display/DisplayPowerState;>;" */
this.mBrightnessRampAnimator = p1;
/* .line 1611 */
return;
} // .end method
public void setScreenBrightnessByUser ( Float p0, Float p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "lux" # F */
/* .param p2, "brightness" # F */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 2348 */
v0 = this.mBrightnessMapper;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.display.BrightnessMappingStrategy ) v0 ).getBrightness ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/BrightnessMappingStrategy;->getBrightness(F)F
} // :cond_0
/* const/high16 v0, -0x40800000 # -1.0f */
/* .line 2349 */
/* .local v0, "unAdjustedBrightness":F */
} // :goto_0
/* cmpg-float v1, v0, p2 */
/* if-gez v1, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 2350 */
/* .local v1, "isBrightening":Z */
} // :goto_1
v2 = this.mCbmController;
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 2351 */
/* iget v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOrientation:I */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v2 ).setScreenBrightnessByUser ( p1, v1, p3, v3 ); // invoke-virtual {v2, p1, v1, p3, v3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setScreenBrightnessByUser(FZLjava/lang/String;I)V
/* .line 2353 */
} // :cond_2
return;
} // .end method
public void setSunlightListener ( com.android.server.display.DisplayPowerControllerStub$SunlightStateChangedListener p0 ) {
/* .locals 0 */
/* .param p1, "listener" # Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener; */
/* .line 603 */
this.mSunlightStateListener = p1;
/* .line 604 */
return;
} // .end method
public void setUpAutoBrightness ( com.android.server.display.BrightnessMappingStrategy p0, com.android.server.display.AutomaticBrightnessControllerStub p1, com.android.server.display.DisplayDeviceConfig p2, android.hardware.Sensor p3 ) {
/* .locals 4 */
/* .param p1, "brightnessMapper" # Lcom/android/server/display/BrightnessMappingStrategy; */
/* .param p2, "stub" # Lcom/android/server/display/AutomaticBrightnessControllerStub; */
/* .param p3, "displayDeviceConfig" # Lcom/android/server/display/DisplayDeviceConfig; */
/* .param p4, "lightSensor" # Landroid/hardware/Sensor; */
/* .line 1575 */
this.mBrightnessMapper = p1;
/* .line 1576 */
this.mDisplayDeviceConfig = p3;
/* .line 1577 */
/* move-object v0, p2 */
/* check-cast v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl; */
this.mAutomaticBrightnessControllerImpl = v0;
/* .line 1578 */
(( com.android.server.display.BrightnessMappingStrategy ) p1 ).setDisplayPowerControllerImpl ( p0 ); // invoke-virtual {p1, p0}, Lcom/android/server/display/BrightnessMappingStrategy;->setDisplayPowerControllerImpl(Lcom/android/server/display/DisplayPowerControllerStub;)V
/* .line 1579 */
v0 = this.mAutomaticBrightnessControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1580 */
v1 = this.mBrightnessMapper;
v2 = this.mDisplayDeviceConfig;
v3 = this.mLogicalDisplay;
(( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).setUpAutoBrightness ( p0, v1, v2, v3 ); // invoke-virtual {v0, p0, v1, v2, v3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setUpAutoBrightness(Lcom/android/server/display/DisplayPowerControllerImpl;Lcom/android/server/display/BrightnessMappingStrategy;Lcom/android/server/display/DisplayDeviceConfig;Lcom/android/server/display/LogicalDisplay;)V
/* .line 1582 */
v0 = this.mBrightnessDataProcessor;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1583 */
v1 = this.mCloudListener;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).setUpCloudControllerListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setUpCloudControllerListener(Lcom/android/server/display/AutomaticBrightnessControllerImpl$CloudControllerListener;)V
/* .line 1584 */
v0 = this.mBrightnessDataProcessor;
v1 = this.mDisplayDeviceConfig;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).setDisplayDeviceConfig ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setDisplayDeviceConfig(Lcom/android/server/display/DisplayDeviceConfig;)V
/* .line 1585 */
v0 = this.mBrightnessDataProcessor;
v1 = this.mBrightnessMapper;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).setBrightnessMapper ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setBrightnessMapper(Lcom/android/server/display/BrightnessMappingStrategy;)V
/* .line 1587 */
} // :cond_0
v0 = this.mCbmController;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1588 */
v1 = this.mBrightnessMapper;
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).setAutoBrightnessComponent ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setAutoBrightnessComponent(Lcom/android/server/display/BrightnessMappingStrategy;)V
/* .line 1590 */
} // :cond_1
v0 = this.mDisplayDeviceConfig;
/* .line 1591 */
(( com.android.server.display.DisplayDeviceConfig ) v0 ).getHighBrightnessModeData ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayDeviceConfig;->getHighBrightnessModeData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;
/* .line 1592 */
/* .local v0, "highBrightnessModeData":Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1593 */
/* const/high16 v1, 0x3f000000 # 0.5f */
/* invoke-direct {p0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->recalculationForBCBC(F)V */
/* .line 1594 */
v1 = this.mHbmController;
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_HDR_HBM_BRIGHTEN:Z */
(( com.android.server.display.HighBrightnessModeController ) v1 ).supportHdrBrightenHbm ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/HighBrightnessModeController;->supportHdrBrightenHbm(Z)V
/* .line 1595 */
v1 = this.mHbmController;
v2 = this.mHdrStateListener;
(( com.android.server.display.HighBrightnessModeController ) v1 ).registerListener ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/HighBrightnessModeController;->registerListener(Lcom/android/server/display/HighBrightnessModeController$HdrStateListener;)V
/* .line 1597 */
} // :cond_2
v1 = this.mBrightnessABTester;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1598 */
v2 = this.mAutomaticBrightnessControllerImpl;
(( com.android.server.display.BrightnessABTester ) v1 ).setAutomaticBrightnessControllerImpl ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/BrightnessABTester;->setAutomaticBrightnessControllerImpl(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V
/* .line 1600 */
} // :cond_3
v1 = this.mBrightnessMapper;
if ( v1 != null) { // if-eqz v1, :cond_4
/* const/high16 v3, -0x40800000 # -1.0f */
/* cmpl-float v3, v2, v3 */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1602 */
v1 = (( com.android.server.display.BrightnessMappingStrategy ) v1 ).convertToBrightness ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/BrightnessMappingStrategy;->convertToBrightness(F)F
/* iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxPowerSaveModeBrightness:F */
/* .line 1605 */
} // .end local v0 # "highBrightnessModeData":Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;
} // :cond_4
/* invoke-direct {p0, p4}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateAmbientLightSensor(Landroid/hardware/Sensor;)V */
/* .line 1606 */
return;
} // .end method
public void settingBrightnessStartChange ( Float p0 ) {
/* .locals 1 */
/* .param p1, "brightness" # F */
/* .line 1407 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mShowOutdoorHighTempToast:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1408 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mShowOutdoorHighTempToast:Z */
/* .line 1409 */
/* const/high16 v0, 0x7fc00000 # Float.NaN */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastTemporaryBrightness:F */
/* .line 1411 */
} // :cond_0
return;
} // .end method
public void showTouchCoverProtectionRect ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isShow" # Z */
/* .line 2025 */
v0 = this.mAutomaticBrightnessControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2026 */
(( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).showTouchCoverProtectionRect ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->showTouchCoverProtectionRect(Z)V
/* .line 2028 */
} // :cond_0
return;
} // .end method
protected void startCbmStatsJob ( ) {
/* .locals 1 */
/* .line 2390 */
v0 = this.mCbmController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2391 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).startCbmStatsJob ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->startCbmStatsJob()V
/* .line 2393 */
} // :cond_0
return;
} // .end method
protected void startDetailThermalUsageStatsOnThermalChanged ( Integer p0, Float p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "conditionId" # I */
/* .param p2, "temperature" # F */
/* .param p3, "brightnessChanged" # Z */
/* .line 2257 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;IFZ)V */
(( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->post(Ljava/lang/Runnable;)Z
/* .line 2266 */
return;
} // .end method
public void stop ( ) {
/* .locals 2 */
/* .line 2015 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).unregisterContentObserver ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
/* .line 2016 */
v0 = this.mBatteryReceiver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2017 */
v1 = this.mContext;
(( android.content.Context ) v1 ).unregisterReceiver ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 2019 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V */
(( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->post(Ljava/lang/Runnable;)Z
/* .line 2021 */
return;
} // .end method
public void temporaryBrightnessStartChange ( Float p0 ) {
/* .locals 1 */
/* .param p1, "brightness" # F */
/* .line 1348 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isOverrideBrightnessPolicyEnable()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedScreenBrightnessOverride:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1349 */
com.android.server.wm.WindowManagerServiceStub .get ( );
/* .line 1350 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedScreenBrightnessOverride:Z */
/* .line 1353 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessControlAvailable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayId:I */
/* if-nez v0, :cond_1 */
/* .line 1355 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isInOutdoorCriticalTemperature()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mShowOutdoorHighTempToast:Z */
/* if-nez v0, :cond_1 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastTemporaryBrightness:F */
/* cmpl-float v0, p1, v0 */
/* if-lez v0, :cond_1 */
/* .line 1358 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mShowOutdoorHighTempToast:Z */
/* .line 1359 */
v0 = this.mInjector;
(( com.android.server.display.DisplayPowerControllerImpl$Injector ) v0 ).showOutdoorHighTempToast ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl$Injector;->showOutdoorHighTempToast()V
/* .line 1361 */
} // :cond_1
/* iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastTemporaryBrightness:F */
/* .line 1362 */
return;
} // .end method
public void updateAutoBrightness ( ) {
/* .locals 1 */
/* .line 2331 */
v0 = this.mAutomaticBrightnessControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2332 */
(( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).update ( ); // invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->update()V
/* .line 2334 */
} // :cond_0
return;
} // .end method
public void updateBCBCStateIfNeeded ( ) {
/* .locals 4 */
/* .line 1733 */
/* sget-boolean v0, Lcom/android/server/display/DisplayPowerControllerImpl;->BCBC_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mMiuiDisplayCloudController;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1736 */
/* nop */
/* .line 1734 */
(( com.android.server.display.MiuiDisplayCloudController ) v0 ).getBCBCAppList ( ); // invoke-virtual {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->getBCBCAppList()Ljava/util/List;
v0 = v1 = this.mForegroundAppPackageName;
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1735 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrScene()Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsScreenOn:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1736 */
/* move v0, v1 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1737 */
/* .local v0, "state":I */
} // :goto_0
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBCBCState:I */
final String v3 = "DisplayPowerControllerImpl"; // const-string v3, "DisplayPowerControllerImpl"
/* if-eq v0, v2, :cond_2 */
/* .line 1738 */
/* iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBCBCState:I */
/* .line 1739 */
v2 = this.mDisplayFeatureManagerServicImpl;
(( com.android.server.display.DisplayFeatureManagerServiceImpl ) v2 ).updateBCBCState ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->updateBCBCState(I)V
/* .line 1740 */
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1741 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* if-ne v0, v1, :cond_1 */
final String v1 = "Enter "; // const-string v1, "Enter "
} // :cond_1
final String v1 = "Exit "; // const-string v1, "Exit "
} // :goto_1
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "BCBC State, mForegroundAppPackageName = "; // const-string v2, "BCBC State, mForegroundAppPackageName = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mForegroundAppPackageName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", mAutoBrightnessEnable = "; // const-string v2, ", mAutoBrightnessEnable = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v1 );
/* .line 1745 */
} // :cond_2
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1746 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Skip BCBC State, mBCBCState = "; // const-string v2, "Skip BCBC State, mBCBCState = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBCBCState:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v1 );
/* .line 1749 */
} // .end local v0 # "state":I
} // :cond_3
} // :goto_2
return;
} // .end method
public void updateBrightnessAnimInfoIfNeeded ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 2065 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mUpdateBrightnessAnimInfoEnable:Z */
/* if-eq p1, v0, :cond_0 */
v0 = this.mBrightnessDataProcessor;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2066 */
/* iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mUpdateBrightnessAnimInfoEnable:Z */
/* .line 2067 */
/* if-nez p1, :cond_0 */
/* .line 2068 */
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).notifyResetBrightnessAnimInfo ( ); // invoke-virtual {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->notifyResetBrightnessAnimInfo()V
/* .line 2071 */
} // :cond_0
return;
} // .end method
public void updateBrightnessChangeStatus ( Boolean p0, Integer p1, Boolean p2, Boolean p3, Boolean p4, Float p5, Float p6, Float p7, Float p8, Integer p9, com.android.server.display.brightness.BrightnessReason p10, com.android.server.display.brightness.BrightnessReason p11 ) {
/* .locals 18 */
/* .param p1, "animating" # Z */
/* .param p2, "displayState" # I */
/* .param p3, "slowChange" # Z */
/* .param p4, "appliedDimming" # Z */
/* .param p5, "appliedLowPower" # Z */
/* .param p6, "currentBrightness" # F */
/* .param p7, "currentSdrBrightness" # F */
/* .param p8, "targetBrightness" # F */
/* .param p9, "targetSdrBrightness" # F */
/* .param p10, "displayPolicy" # I */
/* .param p11, "reason" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .param p12, "reasonTemp" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .line 1068 */
/* move-object/from16 v0, p0 */
/* move/from16 v14, p1 */
/* move/from16 v15, p2 */
/* move/from16 v13, p3 */
/* move/from16 v12, p4 */
/* move/from16 v11, p5 */
/* move/from16 v10, p6 */
/* move/from16 v9, p7 */
/* move/from16 v8, p8 */
/* move/from16 v7, p9 */
/* move/from16 v6, p10 */
/* iget v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentSdrBrightness:F */
/* cmpl-float v1, v9, v1 */
/* if-nez v1, :cond_0 */
/* iget v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mTargetBrightness:F */
/* cmpl-float v1, v8, v1 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1070 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "updateBrightnessChangeStatus: animating: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v14 ); // invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", displayState: "; // const-string v2, ", displayState: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v15 ); // invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", slowChange: "; // const-string v2, ", slowChange: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v13 ); // invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", appliedDimming: "; // const-string v2, ", appliedDimming: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", appliedLowPower: "; // const-string v2, ", appliedLowPower: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", currentBrightness: "; // const-string v2, ", currentBrightness: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", currentSdrBrightness: "; // const-string v2, ", currentSdrBrightness: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", targetBrightness: "; // const-string v2, ", targetBrightness: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", targetSdrBrightness: "; // const-string v2, ", targetSdrBrightness: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", previousDisplayPolicy: "; // const-string v2, ", previousDisplayPolicy: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentDisplayPolicy:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", currentDisplayPolicy: "; // const-string v2, ", currentDisplayPolicy: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "DisplayPowerControllerImpl"; // const-string v2, "DisplayPowerControllerImpl"
android.util.Slog .d ( v2,v1 );
/* .line 1083 */
} // :cond_1
v1 = this.mRampRateController;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1084 */
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isAnimating()Z */
/* iget v5, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentDisplayPolicy:I */
/* iget v4, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastDisplayState:I */
/* move/from16 v3, p1 */
/* move/from16 v16, v4 */
/* move/from16 v4, p3 */
/* move/from16 v17, v5 */
/* move/from16 v5, p6 */
/* move/from16 v6, p7 */
/* move/from16 v7, p8 */
/* move/from16 v8, p9 */
/* move/from16 v9, p10 */
/* move/from16 v10, v17 */
/* move/from16 v11, v16 */
/* move-object/from16 v12, p12 */
/* move-object/from16 v13, p11 */
/* invoke-virtual/range {v1 ..v13}, Lcom/android/server/display/RampRateController;->updateBrightnessState(ZZZFFFFIIILcom/android/server/display/brightness/BrightnessReason;Lcom/android/server/display/brightness/BrightnessReason;)V */
/* .line 1090 */
} // :cond_2
/* iput-boolean v14, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastAnimating:Z */
/* .line 1091 */
/* iput v15, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastDisplayState:I */
/* .line 1092 */
/* move/from16 v7, p6 */
/* iput v7, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentBrightness:F */
/* .line 1093 */
/* move/from16 v8, p7 */
/* iput v8, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentSdrBrightness:F */
/* .line 1094 */
/* move/from16 v9, p3 */
/* iput-boolean v9, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSlowChange:Z */
/* .line 1095 */
/* move/from16 v10, p4 */
/* iput-boolean v10, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedDimming:Z */
/* .line 1096 */
/* move/from16 v11, p5 */
/* iput-boolean v11, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedLowPower:Z */
/* .line 1097 */
/* move/from16 v12, p8 */
/* iput v12, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mTargetBrightness:F */
/* .line 1098 */
/* move/from16 v13, p9 */
/* iput v13, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mTargetSdrBrightness:F */
/* .line 1099 */
/* iget v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentDisplayPolicy:I */
/* iput v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPreviousDisplayPolicy:I */
/* .line 1100 */
/* move/from16 v6, p10 */
/* iput v6, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentDisplayPolicy:I */
/* .line 1102 */
v1 = this.mAutomaticBrightnessControllerImpl;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1103 */
/* move/from16 v2, p3 */
/* move/from16 v3, p4 */
/* move/from16 v4, p5 */
/* move/from16 v5, p6 */
/* move v7, v6 */
/* move/from16 v6, p7 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->updateSlowChangeStatus(ZZZFF)V */
/* .line 1102 */
} // :cond_3
/* move v7, v6 */
/* .line 1106 */
} // :goto_0
v1 = this.mThermalBrightnessController;
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1107 */
(( com.android.server.display.ThermalBrightnessController ) v1 ).updateScreenState ( v15, v7 ); // invoke-virtual {v1, v15, v7}, Lcom/android/server/display/ThermalBrightnessController;->updateScreenState(II)V
/* .line 1109 */
} // :cond_4
return;
} // .end method
protected void updateCbmState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "autoBrightnessEnabled" # Z */
/* .line 2444 */
v0 = this.mCbmController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2445 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).updateCbmState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCbmState(Z)V
/* .line 2447 */
} // :cond_0
return;
} // .end method
public void updateCurrentGrayScale ( android.os.Bundle p0 ) {
/* .locals 2 */
/* .param p1, "bundle" # Landroid/os/Bundle; */
/* .line 1475 */
v0 = this.mHandler;
int v1 = 6; // const/4 v1, 0x6
(( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->removeMessages(I)V
/* .line 1476 */
v0 = this.mHandler;
(( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 1477 */
/* .local v0, "msg":Landroid/os/Message; */
(( android.os.Message ) v0 ).setData ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 1478 */
v1 = this.mHandler;
(( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 1479 */
return;
} // .end method
protected void updateCustomSceneState ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 2438 */
v0 = this.mCbmController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2439 */
/* iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOrientation:I */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).updateCustomSceneState ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCustomSceneState(Ljava/lang/String;I)V
/* .line 2441 */
} // :cond_0
return;
} // .end method
public void updateDolbyPreviewStateLocked ( Boolean p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .param p2, "dolbyPreviewBoostRatio" # F */
/* .line 1216 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostAvailable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewEnable:Z */
/* if-ne v0, p1, :cond_0 */
/* iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostRatio:F */
/* cmpl-float v0, v0, p2 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1219 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewEnable:Z */
/* .line 1220 */
/* iput p2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostRatio:F */
/* .line 1221 */
v0 = this.mDisplayPowerController;
(( com.android.server.display.DisplayPowerController ) v0 ).updateBrightness ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V
/* .line 1223 */
} // :cond_1
return;
} // .end method
public void updateFastRateStatus ( Float p0 ) {
/* .locals 1 */
/* .param p1, "brightness" # F */
/* .line 1799 */
v0 = this.mAutomaticBrightnessControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1800 */
(( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).updateFastRateStatus ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->updateFastRateStatus(F)V
/* .line 1802 */
} // :cond_0
return;
} // .end method
public void updateGalleryHdrState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 1246 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsGalleryHdrEnable:Z */
/* if-eq v0, p1, :cond_0 */
/* .line 1247 */
/* iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsGalleryHdrEnable:Z */
/* .line 1248 */
v0 = this.mDisplayPowerController;
(( com.android.server.display.DisplayPowerController ) v0 ).updateBrightness ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V
/* .line 1250 */
} // :cond_0
return;
} // .end method
public void updateGalleryHdrThermalThrottler ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "throttled" # Z */
/* .line 1257 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGalleryHdrThrottled:Z */
/* if-eq v0, p1, :cond_0 */
/* .line 1258 */
/* iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGalleryHdrThrottled:Z */
/* .line 1259 */
v0 = this.mDisplayPowerController;
(( com.android.server.display.DisplayPowerController ) v0 ).updateBrightness ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V
/* .line 1261 */
} // :cond_0
return;
} // .end method
public void updateGrayScale ( android.os.Bundle p0 ) {
/* .locals 2 */
/* .param p1, "bundle" # Landroid/os/Bundle; */
/* .line 1463 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 1464 */
/* .local v0, "msg":Landroid/os/Message; */
(( android.os.Message ) v0 ).setData ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 1465 */
v1 = this.mHandler;
(( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 1466 */
return;
} // .end method
public void updateOutdoorThermalAppCategoryList ( java.util.List p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1806 */
/* .local p1, "outdoorThermalAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;Ljava/util/List;)V */
/* const-wide/16 v2, 0xfa0 */
(( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 1811 */
return;
} // .end method
public Float updateRampRate ( java.lang.String p0, Float p1, Float p2, Float p3 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "currentBrightness" # F */
/* .param p3, "targetBrightness" # F */
/* .param p4, "rate" # F */
/* .line 2505 */
v0 = this.mRampRateController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2506 */
v0 = (( com.android.server.display.RampRateController ) v0 ).updateBrightnessRate ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/display/RampRateController;->updateBrightnessRate(Ljava/lang/String;FFF)F
/* .line 2509 */
} // :cond_0
} // .end method
public void updateScreenGrayscaleStateIfNeeded ( ) {
/* .locals 7 */
/* .line 1755 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) p0 ).isSupportPeakBrightness ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isSupportPeakBrightness()Z
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrVideo()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
/* .line 1756 */
/* .local v0, "peakState":Z */
} // :goto_0
/* iget-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprBrightnessControlAvailable:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1757 */
v3 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrScene()Z */
/* if-nez v3, :cond_1 */
v3 = this.mAutomaticBrightnessControllerImpl;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1759 */
v3 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v3 ).getCurrentAmbientLux ( ); // invoke-virtual {v3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getCurrentAmbientLux()F
/* iget v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprAmbientLuxThreshold:I */
/* int-to-float v4, v4 */
/* cmpl-float v3, v3, v4 */
/* if-lez v3, :cond_1 */
/* move v3, v1 */
} // :cond_1
/* move v3, v2 */
/* .line 1761 */
/* .local v3, "oprState":Z */
} // :goto_1
/* iget-boolean v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z */
if ( v4 != null) { // if-eqz v4, :cond_3
/* iget-boolean v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsScreenOn:Z */
if ( v4 != null) { // if-eqz v4, :cond_3
/* if-nez v0, :cond_2 */
if ( v3 != null) { // if-eqz v3, :cond_3
} // :cond_2
/* move v4, v1 */
} // :cond_3
/* move v4, v2 */
/* .line 1762 */
/* .local v4, "state":Z */
} // :goto_2
/* iget-boolean v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenGrayscaleState:Z */
/* if-eq v4, v5, :cond_6 */
/* .line 1763 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
if ( v4 != null) { // if-eqz v4, :cond_4
final String v6 = "Starting"; // const-string v6, "Starting"
} // :cond_4
final String v6 = "Ending"; // const-string v6, "Ending"
} // :goto_3
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " update screen gray scale."; // const-string v6, " update screen gray scale."
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "DisplayPowerControllerImpl"; // const-string v6, "DisplayPowerControllerImpl"
android.util.Slog .d ( v6,v5 );
/* .line 1764 */
/* iput-boolean v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenGrayscaleState:Z */
/* .line 1765 */
v5 = this.mDisplayFeatureManagerServicImpl;
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 1766 */
} // :cond_5
/* move v1, v2 */
/* .line 1765 */
} // :goto_4
(( com.android.server.display.DisplayFeatureManagerServiceImpl ) v5 ).updateScreenGrayscaleState ( v1 ); // invoke-virtual {v5, v1}, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->updateScreenGrayscaleState(I)V
/* .line 1768 */
} // :cond_6
return;
} // .end method
public void updateScreenState ( Float p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "brightnessState" # F */
/* .param p2, "policy" # I */
/* .line 2198 */
int v0 = 0; // const/4 v0, 0x0
/* cmpl-float v0, p1, v0 */
/* if-lez v0, :cond_1 */
v0 = this.mDisplayPowerController;
/* .line 2199 */
(( com.android.server.display.DisplayPowerController ) v0 ).getDisplayPowerState ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->getDisplayPowerState()Lcom/android/server/display/DisplayPowerState;
v0 = (( com.android.server.display.DisplayPowerState ) v0 ).getScreenState ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerState;->getScreenState()I
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_1 */
int v0 = 3; // const/4 v0, 0x3
/* if-eq p2, v0, :cond_0 */
/* if-ne p2, v1, :cond_1 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 2201 */
/* .local v0, "state":Z */
} // :goto_0
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsScreenOn:Z */
/* if-eq v1, v0, :cond_2 */
/* .line 2202 */
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsScreenOn:Z */
/* .line 2203 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->startUpdateThermalStats(FZ)V */
/* .line 2204 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetScreenGrayscaleState()V */
/* .line 2205 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetBCBCState()V */
/* .line 2207 */
} // :cond_2
return;
} // .end method
