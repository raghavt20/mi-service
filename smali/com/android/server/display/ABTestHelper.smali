.class public Lcom/android/server/display/ABTestHelper;
.super Ljava/lang/Object;
.source "ABTestHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/ABTestHelper$AbTestHandler;
    }
.end annotation


# static fields
.field private static final DELAY_TIME:J = 0x1388L

.field private static final PATH:Ljava/lang/String; = "/ExpLayer/ExpDomain/"

.field private static final TAG:Ljava/lang/String; = "ABTestHelper"

.field private static final UPDATE_EXPERIMENTS:I = 0x0

.field private static final UPDATE_EXPERIMENTS_DELAY:J = 0x927c0L


# instance fields
.field private mABTest:Lcom/xiaomi/abtest/ABTest;

.field private mAppName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mEndExperiment:Ljava/util/function/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/function/Consumer<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mExpCondition:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mExpId:I

.field private mHandler:Landroid/os/Handler;

.field private mLayerName:Ljava/lang/String;

.field private mStartExperiment:Ljava/util/function/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/function/Consumer<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public static synthetic $r8$lambda$iPdUacJN3I7AdaYXuIJUW2un4Sk(Lcom/android/server/display/ABTestHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/ABTestHelper;->lambda$new$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateABTest(Lcom/android/server/display/ABTestHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/ABTestHelper;->updateABTest()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/function/Consumer;Ljava/util/function/Consumer;J)V
    .locals 2
    .param p1, "looper"    # Landroid/os/Looper;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "appName"    # Ljava/lang/String;
    .param p4, "layerName"    # Ljava/lang/String;
    .param p8, "delay"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Looper;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/function/Consumer<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/util/function/Consumer<",
            "Ljava/lang/Void;",
            ">;J)V"
        }
    .end annotation

    .line 49
    .local p5, "expCondition":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p6, "startExperiment":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .local p7, "endExperiment":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Ljava/lang/Void;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/display/ABTestHelper;->mExpId:I

    .line 50
    new-instance v0, Lcom/android/server/display/ABTestHelper$AbTestHandler;

    invoke-direct {v0, p0, p1}, Lcom/android/server/display/ABTestHelper$AbTestHandler;-><init>(Lcom/android/server/display/ABTestHelper;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/ABTestHelper;->mHandler:Landroid/os/Handler;

    .line 51
    iput-object p2, p0, Lcom/android/server/display/ABTestHelper;->mContext:Landroid/content/Context;

    .line 52
    iput-object p3, p0, Lcom/android/server/display/ABTestHelper;->mAppName:Ljava/lang/String;

    .line 53
    iput-object p4, p0, Lcom/android/server/display/ABTestHelper;->mLayerName:Ljava/lang/String;

    .line 54
    iput-object p5, p0, Lcom/android/server/display/ABTestHelper;->mExpCondition:Ljava/util/Map;

    .line 55
    iput-object p6, p0, Lcom/android/server/display/ABTestHelper;->mStartExperiment:Ljava/util/function/Consumer;

    .line 56
    iput-object p7, p0, Lcom/android/server/display/ABTestHelper;->mEndExperiment:Ljava/util/function/Consumer;

    .line 57
    const-string v0, "phone"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/server/display/ABTestHelper;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 58
    iget-object v0, p0, Lcom/android/server/display/ABTestHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/ABTestHelper$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/display/ABTestHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/ABTestHelper;)V

    invoke-virtual {v0, v1, p8, p9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 61
    return-void
.end method

.method private getImei()Ljava/lang/String;
    .locals 2

    .line 110
    const/4 v0, 0x0

    .line 111
    .local v0, "imei":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/display/ABTestHelper;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_0

    .line 112
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 114
    :cond_0
    return-object v0
.end method

.method private init()V
    .locals 7

    .line 64
    invoke-direct {p0}, Lcom/android/server/display/ABTestHelper;->getImei()Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "imei":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 66
    const-string v1, "ABTestHelper"

    const-string v2, "failed to init, imei = null !!"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    return-void

    .line 69
    :cond_0
    new-instance v1, Lcom/xiaomi/abtest/ABTestConfig$Builder;

    invoke-direct {v1}, Lcom/xiaomi/abtest/ABTestConfig$Builder;-><init>()V

    .line 70
    .local v1, "builder":Lcom/xiaomi/abtest/ABTestConfig$Builder;
    iget-object v2, p0, Lcom/android/server/display/ABTestHelper;->mAppName:Ljava/lang/String;

    .line 71
    invoke-virtual {v1, v2}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->setAppName(Ljava/lang/String;)Lcom/xiaomi/abtest/ABTestConfig$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/display/ABTestHelper;->mLayerName:Ljava/lang/String;

    .line 72
    invoke-virtual {v2, v3}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->setLayerName(Ljava/lang/String;)Lcom/xiaomi/abtest/ABTestConfig$Builder;

    move-result-object v2

    .line 73
    invoke-virtual {v2, v0}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->setDeviceId(Ljava/lang/String;)Lcom/xiaomi/abtest/ABTestConfig$Builder;

    move-result-object v2

    .line 74
    invoke-virtual {v2, v0}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->setUserId(Ljava/lang/String;)Lcom/xiaomi/abtest/ABTestConfig$Builder;

    move-result-object v2

    .line 75
    invoke-virtual {v2}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->build()Lcom/xiaomi/abtest/ABTestConfig;

    move-result-object v2

    .line 76
    .local v2, "conf":Lcom/xiaomi/abtest/ABTestConfig;
    iget-object v3, p0, Lcom/android/server/display/ABTestHelper;->mContext:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/xiaomi/abtest/ABTest;->abTestWithConfig(Landroid/content/Context;Lcom/xiaomi/abtest/ABTestConfig;)Lcom/xiaomi/abtest/ABTest;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/display/ABTestHelper;->mABTest:Lcom/xiaomi/abtest/ABTest;

    .line 77
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/xiaomi/abtest/ABTest;->setIsLoadConfigWhenBackground(Z)V

    .line 78
    iget-object v3, p0, Lcom/android/server/display/ABTestHelper;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    const-wide/16 v5, 0x1388

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 79
    return-void
.end method

.method private synthetic lambda$new$0()V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/android/server/display/ABTestHelper;->init()V

    .line 60
    return-void
.end method

.method private updateABTest()V
    .locals 10

    .line 82
    iget-object v0, p0, Lcom/android/server/display/ABTestHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 83
    iget-object v0, p0, Lcom/android/server/display/ABTestHelper;->mABTest:Lcom/xiaomi/abtest/ABTest;

    iget-object v2, p0, Lcom/android/server/display/ABTestHelper;->mExpCondition:Ljava/util/Map;

    invoke-virtual {v0, v2}, Lcom/xiaomi/abtest/ABTest;->getExperiments(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 84
    .local v0, "experiments":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/xiaomi/abtest/ExperimentInfo;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "experiments: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ABTestHelper"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const-string v2, "/ExpLayer/ExpDomain/"

    const-string v4, "/"

    if-eqz v0, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/display/ABTestHelper;->mAppName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/display/ABTestHelper;->mLayerName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 86
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/display/ABTestHelper;->mAppName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/display/ABTestHelper;->mLayerName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/xiaomi/abtest/ExperimentInfo;

    .line 87
    .local v5, "experimentInfo":Lcom/xiaomi/abtest/ExperimentInfo;
    invoke-virtual {v5}, Lcom/xiaomi/abtest/ExperimentInfo;->getParams()Ljava/util/Map;

    move-result-object v6

    .line 88
    .local v6, "expParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v5}, Lcom/xiaomi/abtest/ExperimentInfo;->getExpId()I

    move-result v7

    .line 89
    .local v7, "expId":I
    iput v7, p0, Lcom/android/server/display/ABTestHelper;->mExpId:I

    .line 90
    if-eqz v6, :cond_0

    .line 91
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "start experiment mExpId: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/server/display/ABTestHelper;->mExpId:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mAppName: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/display/ABTestHelper;->mAppName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mLayerName: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/display/ABTestHelper;->mLayerName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", expParams: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v8, p0, Lcom/android/server/display/ABTestHelper;->mStartExperiment:Ljava/util/function/Consumer;

    invoke-interface {v8, v6}, Ljava/util/function/Consumer;->accept(Ljava/lang/Object;)V

    .line 96
    .end local v5    # "experimentInfo":Lcom/xiaomi/abtest/ExperimentInfo;
    .end local v6    # "expParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "expId":I
    :cond_0
    iget v5, p0, Lcom/android/server/display/ABTestHelper;->mExpId:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    if-eqz v0, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/display/ABTestHelper;->mAppName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/display/ABTestHelper;->mLayerName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    .line 98
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "end experiment mExpId: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/android/server/display/ABTestHelper;->mExpId:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iput v6, p0, Lcom/android/server/display/ABTestHelper;->mExpId:I

    .line 100
    iget-object v2, p0, Lcom/android/server/display/ABTestHelper;->mEndExperiment:Ljava/util/function/Consumer;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/function/Consumer;->accept(Ljava/lang/Object;)V

    .line 102
    :cond_2
    iget-object v2, p0, Lcom/android/server/display/ABTestHelper;->mHandler:Landroid/os/Handler;

    const-wide/32 v3, 0x927c0

    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 103
    return-void
.end method


# virtual methods
.method public getExpId()I
    .locals 1

    .line 106
    iget v0, p0, Lcom/android/server/display/ABTestHelper;->mExpId:I

    return v0
.end method
