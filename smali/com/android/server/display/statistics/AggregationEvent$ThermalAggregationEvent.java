public class com.android.server.display.statistics.AggregationEvent$ThermalAggregationEvent extends com.android.server.display.statistics.AggregationEvent {
	 /* .source "AggregationEvent.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/statistics/AggregationEvent; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "ThermalAggregationEvent" */
} // .end annotation
/* # static fields */
public static final java.lang.String EVENT_THERMAL_AVERAGE_TEMPERATURE;
public static final java.lang.String EVENT_THERMAL_BRIGHTNESS_RESTRICTED_ADJUST_HIGH_TIMES;
public static final java.lang.String EVENT_THERMAL_BRIGHTNESS_RESTRICTED_USAGE;
public static final java.lang.String EVENT_THERMAL_DETAIL_RESTRICTED_USAGE;
public static final java.lang.String EVENT_THERMAL_DETAIL_UNRESTRICTED_USAGE;
public static final java.lang.String EVENT_THERMAL_OUTDOOR_USAGE;
public static final java.lang.String EVENT_THERMAL_USAGE;
public static final java.lang.String KEY_AVERAGE_VALUE;
public static final java.lang.String KEY_RESTRICTED_USAGE_VALUE;
public static final java.lang.String KEY_UNRESTRICTED_USAGE_VALUE;
/* # instance fields */
private java.util.Map mThermalCacheDataMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Float;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mThermalQuotaEvents;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.display.statistics.AggregationEvent$ThermalAggregationEvent ( ) {
/* .locals 1 */
/* .line 112 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/AggregationEvent;-><init>()V */
/* .line 134 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mThermalQuotaEvents = v0;
/* .line 135 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mThermalCacheDataMap = v0;
return;
} // .end method
/* # virtual methods */
public java.util.Map getCacheDataMap ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Float;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 149 */
v0 = this.mThermalCacheDataMap;
} // .end method
public java.util.Map getQuotaEvents ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 144 */
v0 = this.mThermalQuotaEvents;
} // .end method
public java.lang.String toString ( ) {
/* .locals 1 */
/* .line 139 */
v0 = this.mThermalQuotaEvents;
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
