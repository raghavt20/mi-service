.class public Lcom/android/server/display/statistics/SwitchStatsHelper;
.super Ljava/lang/Object;
.source "SwitchStatsHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;
    }
.end annotation


# static fields
.field private static final AI_DISPLAY_MODE:Ljava/lang/String; = "ai_display_mode"

.field private static final BACKGROUND_BLUR_ENABLE:Ljava/lang/String; = "background_blur_enable"

.field private static final BACKGROUND_BLUR_SUPPORTED:Z

.field private static final COLOR_GAMUT_MODE:Ljava/lang/String; = "color_gamut_mode"

.field public static final DC_BACK_LIGHT_SWITCH:Ljava/lang/String; = "dc_back_light"

.field private static final EXPERT_DATA:Ljava/lang/String; = "expert_data"

.field private static final IS_SMART_FPS:Ljava/lang/String; = "is_smart_fps"

.field private static final MIUI_SCREEN_COMPAT:Ljava/lang/String; = "miui_screen_compat"

.field private static final SCREEN_ENHANCE_ENGINE_GALLERY_AI_MODE_STATUS:Ljava/lang/String; = "screen_enhance_engine_gallery_ai_mode_status"

.field private static final SUPPORT_RESOLUTION_SWITCH:Z

.field private static final SUPPORT_SMART_FPS:Z

.field public static final USER_REFRESH_RATE:Ljava/lang/String; = "user_refresh_rate"

.field private static mInstance:Lcom/android/server/display/statistics/SwitchStatsHelper;

.field private static mScreenResolutionSupported:[I


# instance fields
.field private mAdaptiveSleepEnable:Z

.field private mAiDisplayModeEnable:Z

.field private mAutoBrightnessSettingsEnable:Z

.field private mBackgroundBlurEnable:Z

.field private mBgHandler:Landroid/os/Handler;

.field private mColorGamutMode:I

.field private mContext:Landroid/content/Context;

.field private mDarkModeSettingsEnable:Z

.field private mDcBacklightSettingsEnable:Z

.field private mDozeAlwaysOn:Z

.field private mReadModeSettingsEnable:Z

.field private mRefreshRateFromDeviceFeature:I

.field private mResolver:Landroid/content/ContentResolver;

.field private mScreenColorLevel:I

.field private mScreenCompat:Z

.field private mScreenOptimizeSettingsMode:I

.field private mScreenTrueToneEnable:Z

.field private mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

.field private mSmartRefreshRateEnable:Z

.field private mSunlightSettingsEnable:Z

.field private mSupportAdaptiveSleep:Z

.field private mSupportAiDisplayMode:Z

.field private mSupportExpertMode:Z

.field private mSwitchStats:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mUserRefreshRate:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmBgHandler(Lcom/android/server/display/statistics/SwitchStatsHelper;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mBgHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleSettingsChangeEvent(Lcom/android/server/display/statistics/SwitchStatsHelper;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/SwitchStatsHelper;->handleSettingsChangeEvent(Landroid/net/Uri;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 35
    nop

    .line 36
    const-string/jumbo v0, "support_smart_fps"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/statistics/SwitchStatsHelper;->SUPPORT_SMART_FPS:Z

    .line 38
    nop

    .line 39
    const-string/jumbo v0, "screen_resolution_supported"

    invoke-static {v0}, Lmiui/util/FeatureParser;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    sput-object v0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenResolutionSupported:[I

    .line 40
    if-eqz v0, :cond_0

    array-length v0, v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    sput-boolean v2, Lcom/android/server/display/statistics/SwitchStatsHelper;->SUPPORT_RESOLUTION_SWITCH:Z

    .line 49
    const-string v0, "persist.sys.background_blur_supported"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/statistics/SwitchStatsHelper;->BACKGROUND_BLUR_SUPPORTED:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    .line 77
    iput-object p1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mContext:Landroid/content/Context;

    .line 78
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mBgHandler:Landroid/os/Handler;

    .line 79
    new-instance v0, Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mBgHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;-><init>(Lcom/android/server/display/statistics/SwitchStatsHelper;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    .line 80
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    .line 81
    invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->readConfigFromDeviceFeature()V

    .line 82
    invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->loadSmartSwitches()V

    .line 83
    invoke-virtual {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->registerSettingsObserver()V

    .line 84
    return-void
.end method

.method private getColorLevelCode(I)I
    .locals 5
    .param p1, "mScreenColorLevel"    # I

    .line 455
    const v0, 0xffffff

    and-int/2addr v0, p1

    .line 456
    .local v0, "validRGB":I
    shr-int/lit8 v1, v0, 0x10

    .line 457
    .local v1, "value_R":I
    shr-int/lit8 v2, v0, 0x8

    and-int/lit16 v2, v2, 0xff

    .line 458
    .local v2, "value_G":I
    and-int/lit16 v3, v0, 0xff

    .line 459
    .local v3, "value_B":I
    const/4 v4, -0x1

    if-eq p1, v4, :cond_3

    if-ne v1, v2, :cond_0

    if-ne v2, v3, :cond_0

    goto :goto_1

    .line 462
    :cond_0
    const/4 v4, 0x3

    if-le v1, v2, :cond_1

    if-le v1, v3, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    .line 463
    :cond_1
    if-ge v3, v2, :cond_2

    const/4 v4, 0x2

    .line 462
    :cond_2
    :goto_0
    return v4

    .line 460
    :cond_3
    :goto_1
    const/4 v4, 0x0

    return v4
.end method

.method private getCurrentCloseLidDisplaySetting()I
    .locals 3

    .line 436
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "close_lid_display_setting"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getCurrentRefreshRate()I
    .locals 3

    .line 416
    sget-boolean v0, Lcom/android/server/display/statistics/SwitchStatsHelper;->SUPPORT_SMART_FPS:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSmartRefreshRateEnable:Z

    if-eqz v0, :cond_0

    .line 418
    const/16 v0, -0x78

    return v0

    .line 420
    :cond_0
    const-string v0, "ro.vendor.fps.switch.default"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 421
    .local v0, "defaultFps":Z
    const/4 v1, -0x1

    if-eqz v0, :cond_2

    .line 422
    iget v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mUserRefreshRate:I

    if-eq v2, v1, :cond_1

    goto :goto_0

    :cond_1
    iget v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mRefreshRateFromDeviceFeature:I

    :goto_0
    return v2

    .line 424
    :cond_2
    const-string v2, "persist.vendor.dfps.level"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private getCurrentScreenCompat()Z
    .locals 6

    .line 442
    const/4 v0, 0x0

    .line 443
    .local v0, "isQhd":Z
    sget-boolean v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->SUPPORT_RESOLUTION_SWITCH:Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    .line 444
    invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getCurrentScreenResolution()I

    move-result v1

    sget-object v4, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenResolutionSupported:[I

    aget v5, v4, v3

    aget v4, v4, v2

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    if-ne v1, v4, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    move v0, v1

    .line 447
    :cond_1
    if-eqz v0, :cond_2

    iget-boolean v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenCompat:Z

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    move v2, v3

    :goto_1
    return v2
.end method

.method private getCurrentScreenResolution()I
    .locals 3

    .line 428
    const-string v0, "persist.sys.miui_resolution"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 429
    .local v0, "screenResolution":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 430
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1

    .line 432
    :cond_0
    const/16 v1, 0x5a0

    return v1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/display/statistics/SwitchStatsHelper;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 87
    sget-object v0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mInstance:Lcom/android/server/display/statistics/SwitchStatsHelper;

    if-nez v0, :cond_0

    .line 88
    new-instance v0, Lcom/android/server/display/statistics/SwitchStatsHelper;

    invoke-direct {v0, p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mInstance:Lcom/android/server/display/statistics/SwitchStatsHelper;

    .line 90
    :cond_0
    sget-object v0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mInstance:Lcom/android/server/display/statistics/SwitchStatsHelper;

    return-object v0
.end method

.method private handleSettingsChangeEvent(Landroid/net/Uri;)V
    .locals 18
    .param p1, "uri"    # Landroid/net/Uri;

    .line 218
    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const-string/jumbo v4, "screen_paper_mode_enabled"

    const-string v5, "screen_optimize_mode"

    const-string v6, "is_smart_fps"

    const-string/jumbo v7, "ui_night_mode"

    const-string v8, "background_blur_enable"

    const-string v9, "screen_color_level"

    const-string v10, "dc_back_light"

    const-string v11, "doze_always_on"

    const-string v12, "screen_brightness_mode"

    const-string v13, "screen_enhance_engine_gallery_ai_mode_status"

    const-string v14, "miui_screen_compat"

    const-string/jumbo v15, "screen_true_tone"

    const-string/jumbo v3, "user_refresh_rate"

    const-string v0, "adaptive_sleep"

    move-object/from16 v16, v0

    const-string/jumbo v0, "sunlight_mode"

    move-object/from16 v17, v0

    sparse-switch v2, :sswitch_data_0

    move-object/from16 v2, v16

    move-object/from16 v0, v17

    goto/16 :goto_3

    :sswitch_0
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    goto/16 :goto_0

    :sswitch_1
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    goto/16 :goto_0

    :sswitch_2
    const-string v2, "expert_data"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xd

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    :sswitch_4
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v2, v16

    move-object/from16 v0, v17

    const/4 v1, 0x1

    goto/16 :goto_4

    :sswitch_5
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xf

    goto :goto_0

    :sswitch_6
    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v2, v16

    move-object/from16 v0, v17

    const/4 v1, 0x0

    goto :goto_4

    :sswitch_8
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x9

    goto :goto_0

    :sswitch_9
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_a
    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xe

    goto :goto_0

    :sswitch_b
    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xb

    goto :goto_0

    :sswitch_c
    invoke-virtual {v1, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xa

    goto :goto_0

    :sswitch_d
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x7

    :goto_0
    move-object/from16 v2, v16

    goto :goto_1

    :cond_0
    move-object/from16 v2, v16

    goto :goto_2

    :sswitch_e
    move-object/from16 v2, v16

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0xc

    :goto_1
    move-object/from16 v0, v17

    goto :goto_4

    :cond_1
    :goto_2
    move-object/from16 v0, v17

    goto :goto_3

    :sswitch_f
    move-object/from16 v2, v16

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    goto :goto_4

    :cond_2
    :goto_3
    const/4 v1, -0x1

    :goto_4
    move-object/from16 v17, v2

    const/4 v2, -0x2

    packed-switch v1, :pswitch_data_0

    move-object/from16 v1, p0

    goto/16 :goto_10

    .line 290
    :pswitch_0
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    invoke-static {v0, v8, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    goto :goto_5

    :cond_3
    const/4 v0, 0x0

    :goto_5
    iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mBackgroundBlurEnable:Z

    .line 292
    goto/16 :goto_10

    .line 286
    :pswitch_1
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v0, v13, v2}, Landroid/provider/Settings$Global;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "true"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAiDisplayModeEnable:Z

    .line 288
    goto/16 :goto_10

    .line 283
    :pswitch_2
    move-object/from16 v1, p0

    invoke-direct/range {p0 .. p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->updateColorGamutMode()V

    .line 284
    goto/16 :goto_10

    .line 279
    :pswitch_3
    move-object/from16 v1, p0

    move-object/from16 v0, v17

    iget-object v3, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, -0x1

    invoke-static {v3, v0, v4, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    const/4 v0, 0x1

    goto :goto_6

    :cond_4
    const/4 v0, 0x0

    :goto_6
    iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAdaptiveSleepEnable:Z

    .line 281
    goto/16 :goto_10

    .line 275
    :pswitch_4
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, -0x1

    invoke-static {v0, v14, v3, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_5

    const/4 v0, 0x1

    goto :goto_7

    :cond_5
    const/4 v0, 0x0

    :goto_7
    iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenCompat:Z

    .line 277
    goto/16 :goto_10

    .line 270
    :pswitch_5
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    invoke-static {v0, v15, v3, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_6

    const/4 v0, 0x1

    goto :goto_8

    :cond_6
    const/4 v0, 0x0

    :goto_8
    iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenTrueToneEnable:Z

    .line 273
    goto/16 :goto_10

    .line 266
    :pswitch_6
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, -0x1

    invoke-static {v0, v11, v3, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    const/4 v0, 0x1

    goto :goto_9

    :cond_7
    const/4 v0, 0x0

    :goto_9
    iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDozeAlwaysOn:Z

    .line 268
    goto/16 :goto_10

    .line 262
    :pswitch_7
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, -0x1

    invoke-static {v0, v6, v3, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_8

    const/4 v0, 0x1

    goto :goto_a

    :cond_8
    const/4 v0, 0x0

    :goto_a
    iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSmartRefreshRateEnable:Z

    .line 264
    goto/16 :goto_10

    .line 258
    :pswitch_8
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, -0x1

    invoke-static {v0, v3, v4, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mUserRefreshRate:I

    .line 260
    goto/16 :goto_10

    .line 253
    :pswitch_9
    const/4 v4, -0x1

    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v0, v9, v4, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenColorLevel:I

    .line 256
    goto/16 :goto_10

    .line 247
    :pswitch_a
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    invoke-static {v0, v4, v3, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    goto :goto_b

    :cond_9
    const/4 v0, 0x0

    :goto_b
    iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mReadModeSettingsEnable:Z

    .line 251
    goto :goto_10

    .line 240
    :pswitch_b
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    sget v3, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_SCREEN_OPTIMIZE_MODE:I

    invoke-static {v0, v5, v3, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenOptimizeSettingsMode:I

    .line 244
    invoke-direct/range {p0 .. p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->updateColorGamutMode()V

    .line 245
    goto :goto_10

    .line 234
    :pswitch_c
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    invoke-static {v0, v12, v3, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_a

    const/4 v0, 0x1

    goto :goto_c

    :cond_a
    const/4 v0, 0x0

    :goto_c
    iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAutoBrightnessSettingsEnable:Z

    .line 238
    goto :goto_10

    .line 228
    :pswitch_d
    move-object/from16 v1, p0

    iget-object v3, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    invoke-static {v3, v0, v4, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_b

    const/4 v0, 0x1

    goto :goto_d

    :cond_b
    move v0, v4

    :goto_d
    iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSunlightSettingsEnable:Z

    .line 232
    goto :goto_10

    .line 224
    :pswitch_e
    const/4 v4, 0x0

    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, -0x1

    invoke-static {v0, v7, v3, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_c

    const/4 v0, 0x1

    goto :goto_e

    :cond_c
    move v0, v4

    :goto_e
    iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDarkModeSettingsEnable:Z

    .line 226
    goto :goto_10

    .line 220
    :pswitch_f
    const/4 v4, 0x0

    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, -0x1

    invoke-static {v0, v10, v3, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_d

    move v0, v2

    goto :goto_f

    :cond_d
    move v0, v4

    :goto_f
    iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDcBacklightSettingsEnable:Z

    .line 222
    nop

    .line 296
    :goto_10
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x69203588 -> :sswitch_f
        -0x46cdc832 -> :sswitch_e
        -0x45a4bc48 -> :sswitch_d
        -0x4241e690 -> :sswitch_c
        -0x346f7e1a -> :sswitch_b
        -0x3086abc3 -> :sswitch_a
        -0x294f7102 -> :sswitch_9
        -0x611a9fa -> :sswitch_8
        0x859e77e -> :sswitch_7
        0x2807b455 -> :sswitch_6
        0x2c525eca -> :sswitch_5
        0x46be7ff5 -> :sswitch_4
        0x5bcc609e -> :sswitch_3
        0x7451049f -> :sswitch_2
        0x74fb4732 -> :sswitch_1
        0x7e544b2b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private loadSettings()V
    .locals 7

    .line 155
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "dc_back_light"

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x0

    const/4 v4, 0x1

    if-ne v0, v4, :cond_0

    move v0, v4

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDcBacklightSettingsEnable:Z

    .line 157
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v5, "ui_night_mode"

    invoke-static {v0, v5, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v5, 0x2

    if-ne v0, v5, :cond_1

    move v0, v4

    goto :goto_1

    :cond_1
    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDarkModeSettingsEnable:Z

    .line 159
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v5, "sunlight_mode"

    invoke-static {v0, v5, v1, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v4, :cond_2

    move v0, v4

    goto :goto_2

    :cond_2
    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSunlightSettingsEnable:Z

    .line 163
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "screen_brightness_mode"

    invoke-static {v0, v5, v1, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v4, :cond_3

    move v0, v4

    goto :goto_3

    :cond_3
    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAutoBrightnessSettingsEnable:Z

    .line 167
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "screen_optimize_mode"

    sget v6, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_SCREEN_OPTIMIZE_MODE:I

    invoke-static {v0, v5, v6, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenOptimizeSettingsMode:I

    .line 171
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v5, "screen_paper_mode_enabled"

    invoke-static {v0, v5, v1, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v4

    goto :goto_4

    :cond_4
    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mReadModeSettingsEnable:Z

    .line 175
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "screen_color_level"

    invoke-static {v0, v5, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenColorLevel:I

    .line 177
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v5, "user_refresh_rate"

    invoke-static {v0, v5, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mUserRefreshRate:I

    .line 179
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "is_smart_fps"

    invoke-static {v0, v5, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v4, :cond_5

    move v0, v4

    goto :goto_5

    :cond_5
    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSmartRefreshRateEnable:Z

    .line 181
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "doze_always_on"

    invoke-static {v0, v5, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v4, :cond_6

    move v0, v4

    goto :goto_6

    :cond_6
    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDozeAlwaysOn:Z

    .line 183
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v5, "screen_true_tone"

    invoke-static {v0, v5, v1, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v4, :cond_7

    move v0, v4

    goto :goto_7

    :cond_7
    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenTrueToneEnable:Z

    .line 186
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "miui_screen_compat"

    invoke-static {v0, v5, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v4, :cond_8

    move v0, v4

    goto :goto_8

    :cond_8
    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenCompat:Z

    .line 188
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "adaptive_sleep"

    invoke-static {v0, v5, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v4, :cond_9

    move v0, v4

    goto :goto_9

    :cond_9
    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAdaptiveSleepEnable:Z

    .line 190
    invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->updateColorGamutMode()V

    .line 191
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "screen_enhance_engine_gallery_ai_mode_status"

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$Global;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "true"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAiDisplayModeEnable:Z

    .line 193
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "background_blur_enable"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_a

    move v1, v4

    :cond_a
    iput-boolean v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mBackgroundBlurEnable:Z

    .line 194
    return-void
.end method

.method private loadSmartSwitches()V
    .locals 6

    .line 94
    sget-boolean v0, Lcom/android/server/display/statistics/SwitchStatsHelper;->SUPPORT_SMART_FPS:Z

    const/4 v1, -0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, -0x1

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "is_smart_fps"

    invoke-static {v0, v5, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_0

    move v0, v3

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSmartRefreshRateEnable:Z

    .line 96
    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v0, v5, v3, v1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 100
    :cond_1
    sget-boolean v0, Lcom/android/server/display/statistics/SwitchStatsHelper;->SUPPORT_RESOLUTION_SWITCH:Z

    if-eqz v0, :cond_3

    .line 101
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "miui_screen_compat"

    invoke-static {v0, v5, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_2

    move v2, v3

    :cond_2
    iput-boolean v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenCompat:Z

    .line 102
    if-eqz v2, :cond_3

    .line 103
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v0, v5, v3, v1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 107
    :cond_3
    return-void
.end method

.method private readConfigFromDeviceFeature()V
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 210
    const v1, 0x1110013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSupportAdaptiveSleep:Z

    .line 211
    const-string v0, "defaultFps"

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mRefreshRateFromDeviceFeature:I

    .line 212
    const-string/jumbo v0, "support_AI_display"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSupportAiDisplayMode:Z

    .line 213
    const-string/jumbo v0, "support_display_expert_mode"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSupportExpertMode:Z

    .line 215
    return-void
.end method

.method private updateColorGamutMode()V
    .locals 2

    .line 197
    iget v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenOptimizeSettingsMode:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 198
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/display/expertmode/ExpertData;->getFromDatabase(Landroid/content/Context;)Lcom/android/server/display/expertmode/ExpertData;

    move-result-object v0

    .line 199
    .local v0, "data":Lcom/android/server/display/expertmode/ExpertData;
    if-nez v0, :cond_0

    .line 200
    invoke-static {}, Lcom/android/server/display/expertmode/ExpertData;->getDefaultValue()Lcom/android/server/display/expertmode/ExpertData;

    move-result-object v0

    .line 202
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/display/expertmode/ExpertData;->getByCookie(I)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mColorGamutMode:I

    .line 203
    .end local v0    # "data":Lcom/android/server/display/expertmode/ExpertData;
    goto :goto_0

    .line 204
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mColorGamutMode:I

    .line 206
    :goto_0
    return-void
.end method


# virtual methods
.method public getAllSwitchStats()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;",
            ">;"
        }
    .end annotation

    .line 323
    invoke-virtual {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->updateSwitchStatsValue()V

    .line 324
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getScreenOptimizeSettingsMode()I
    .locals 1

    .line 319
    iget v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenOptimizeSettingsMode:I

    return v0
.end method

.method public isAutoBrightnessSettingsEnable()Z
    .locals 1

    .line 311
    iget-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAutoBrightnessSettingsEnable:Z

    return v0
.end method

.method public isDarkModeSettingsEnable()Z
    .locals 1

    .line 307
    iget-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDarkModeSettingsEnable:Z

    return v0
.end method

.method public isDcBacklightSettingsEnable()Z
    .locals 1

    .line 299
    iget-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDcBacklightSettingsEnable:Z

    return v0
.end method

.method public isReadModeSettingsEnable()Z
    .locals 1

    .line 315
    iget-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mReadModeSettingsEnable:Z

    return v0
.end method

.method public isSunlightSettingsEnable()Z
    .locals 1

    .line 303
    iget-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSunlightSettingsEnable:Z

    return v0
.end method

.method protected registerSettingsObserver()V
    .locals 5

    .line 111
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_brightness_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 114
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "sunlight_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 117
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_optimize_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 120
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "dc_back_light"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 122
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "ui_night_mode"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 124
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_color_level"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 127
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "screen_paper_mode_enabled"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 130
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "user_refresh_rate"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 132
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "is_smart_fps"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 134
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "doze_always_on"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 136
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "screen_true_tone"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 139
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "miui_screen_compat"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 141
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "adaptive_sleep"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 143
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "expert_data"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 145
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    .line 146
    const-string v1, "screen_enhance_engine_gallery_ai_mode_status"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    .line 145
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 148
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mResolver:Landroid/content/ContentResolver;

    .line 149
    const-string v1, "background_blur_enable"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSettingsObserver:Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;

    .line 148
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 151
    invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->loadSettings()V

    .line 152
    return-void
.end method

.method public updateSwitchStatsValue()V
    .locals 6

    .line 328
    iget-object v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 330
    new-instance v0, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    const-string v1, "screen_brightness_mode"

    iget-boolean v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAutoBrightnessSettingsEnable:Z

    const/4 v3, 0x0

    invoke-direct {v0, v3, v1, v2}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V

    .line 332
    .local v0, "statEvent":Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    const-string v2, "dc_back_light"

    iget-boolean v4, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDcBacklightSettingsEnable:Z

    invoke-direct {v1, v3, v2, v4}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V

    move-object v0, v1

    .line 336
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    const-string/jumbo v2, "ui_night_mode"

    iget-boolean v4, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDarkModeSettingsEnable:Z

    invoke-direct {v1, v3, v2, v4}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V

    move-object v0, v1

    .line 340
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 342
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    iget-boolean v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSunlightSettingsEnable:Z

    const/4 v4, 0x1

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAutoBrightnessSettingsEnable:Z

    if-nez v2, :cond_0

    move v2, v4

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    const-string/jumbo v5, "sunlight_mode"

    invoke-direct {v1, v3, v5, v2}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V

    move-object v0, v1

    .line 345
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 347
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    const-string v2, "screen_optimize_mode"

    iget v5, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenOptimizeSettingsMode:I

    invoke-direct {v1, v4, v2, v5}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;I)V

    move-object v0, v1

    .line 349
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    const-string/jumbo v2, "screen_paper_mode_enabled"

    iget-boolean v5, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mReadModeSettingsEnable:Z

    invoke-direct {v1, v3, v2, v5}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V

    move-object v0, v1

    .line 353
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    iget v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenColorLevel:I

    .line 356
    invoke-direct {p0, v2}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getColorLevelCode(I)I

    move-result v2

    const-string v5, "screen_color_level"

    invoke-direct {v1, v4, v5, v2}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;I)V

    move-object v0, v1

    .line 357
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    .line 361
    invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getCurrentRefreshRate()I

    move-result v2

    const-string v5, "dynamic_refresh_rate"

    invoke-direct {v1, v4, v5, v2}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;I)V

    move-object v0, v1

    .line 362
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    const-string v2, "doze_always_on"

    iget-boolean v5, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDozeAlwaysOn:Z

    invoke-direct {v1, v3, v2, v5}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V

    move-object v0, v1

    .line 366
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 368
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    const-string/jumbo v2, "screen_true_tone"

    iget-boolean v5, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenTrueToneEnable:Z

    invoke-direct {v1, v3, v2, v5}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V

    move-object v0, v1

    .line 370
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 372
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    .line 373
    invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getCurrentScreenResolution()I

    move-result v2

    const-string/jumbo v5, "screen_resolution"

    invoke-direct {v1, v4, v5, v2}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;I)V

    move-object v0, v1

    .line 374
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 376
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    .line 377
    invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getCurrentScreenCompat()Z

    move-result v2

    const-string v5, "miui_screen_compat"

    invoke-direct {v1, v3, v5, v2}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V

    move-object v0, v1

    .line 378
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    iget-boolean v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSupportAdaptiveSleep:Z

    if-eqz v1, :cond_1

    .line 381
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    const-string v2, "adaptive_sleep"

    iget-boolean v5, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAdaptiveSleepEnable:Z

    invoke-direct {v1, v3, v2, v5}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V

    move-object v0, v1

    .line 383
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 386
    :cond_1
    const-string v1, "persist.sys.muiltdisplay_type"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 388
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    .line 390
    invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getCurrentCloseLidDisplaySetting()I

    move-result v2

    const-string v5, "close_lid_display_setting"

    invoke-direct {v1, v4, v5, v2}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;I)V

    move-object v0, v1

    .line 391
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 395
    :cond_2
    iget-boolean v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSupportExpertMode:Z

    if-eqz v1, :cond_3

    .line 396
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    const-string v2, "color_gamut_mode"

    iget v5, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mColorGamutMode:I

    invoke-direct {v1, v4, v2, v5}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;I)V

    move-object v0, v1

    .line 398
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 401
    :cond_3
    iget-boolean v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSupportAiDisplayMode:Z

    if-eqz v1, :cond_4

    .line 402
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    const-string v2, "ai_display_mode"

    iget-boolean v4, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAiDisplayModeEnable:Z

    invoke-direct {v1, v3, v2, v4}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V

    move-object v0, v1

    .line 404
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    :cond_4
    sget-boolean v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->BACKGROUND_BLUR_SUPPORTED:Z

    if-eqz v1, :cond_5

    .line 409
    new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;

    const-string v2, "background_blur_enable"

    iget-boolean v4, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mBackgroundBlurEnable:Z

    invoke-direct {v1, v3, v2, v4}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V

    move-object v0, v1

    .line 411
    iget-object v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSwitchStats:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 413
    :cond_5
    return-void
.end method
