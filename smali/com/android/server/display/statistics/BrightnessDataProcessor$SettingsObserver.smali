.class Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "BrightnessDataProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/statistics/BrightnessDataProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;


# direct methods
.method public constructor <init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 1487
    iput-object p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    .line 1488
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 1489
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 6
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 1493
    if-eqz p1, :cond_0

    .line 1494
    return-void

    .line 1496
    :cond_0
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const-string v2, "disable_security_by_mishow"

    const-string v3, "screen_brightness_mode"

    const/4 v4, 0x0

    const/4 v5, 0x1

    sparse-switch v1, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v5

    goto :goto_1

    :sswitch_1
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v4

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 1512
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$fgetmContentResolver(Lcom/android/server/display/statistics/BrightnessDataProcessor;)Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v5, :cond_2

    move v4, v5

    :cond_2
    invoke-static {v0, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$fputmIsMiShow(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V

    .line 1513
    goto :goto_2

    .line 1498
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$fgetmContentResolver(Lcom/android/server/display/statistics/BrightnessDataProcessor;)Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, -0x2

    invoke-static {v1, v3, v4, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-ne v1, v5, :cond_3

    move v4, v5

    :cond_3
    invoke-static {v0, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$fputmAutoBrightnessEnable(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V

    .line 1502
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$fputmAutoBrightnessModeChanged(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V

    .line 1505
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$fgetmReportBrightnessEventsEnable(Lcom/android/server/display/statistics/BrightnessDataProcessor;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1506
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mreportDisabledAutoBrightnessEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    .line 1509
    :cond_4
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mupdateUserResetAutoBrightnessModeTimes(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    .line 1510
    nop

    .line 1517
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x294f7102 -> :sswitch_1
        0x5da4f7f9 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
