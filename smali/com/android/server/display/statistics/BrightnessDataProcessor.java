public class com.android.server.display.statistics.BrightnessDataProcessor implements com.android.server.display.MiuiDisplayCloudController$CloudListener implements com.android.server.display.ThermalBrightnessController$ThermalListener {
	 /* .source "BrightnessDataProcessor.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;, */
	 /* Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;, */
	 /* Lcom/android/server/display/statistics/BrightnessDataProcessor$TaskStackListenerImpl;, */
	 /* Lcom/android/server/display/statistics/BrightnessDataProcessor$RotationWatcher;, */
	 /* Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;, */
	 /* Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;, */
	 /* Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;, */
	 /* Lcom/android/server/display/statistics/BrightnessDataProcessor$ModelEventCallback;, */
	 /* Lcom/android/server/display/statistics/BrightnessDataProcessor$ScreenStateReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long AON_FLARE_EFFECTIVE_TIME;
private static final Integer DEBOUNCE_REPORT_BRIGHTNESS;
private static final Long DEBOUNCE_REPORT_DISABLE_AUTO_BRIGHTNESS_EVENT;
private static final Boolean DEBUG;
private static final Long DEBUG_REPORT_TIME_DURATION;
private static final java.lang.String DISABLE_SECURITY_BY_MI_SHOW;
private static final Integer FACTOR_DARK_MODE;
private static final Integer FACTOR_DC_MODE;
private static final Integer FACTOR_READING_MODE;
private static final Integer LUX_SPLIT_HIGH;
private static final Integer LUX_SPLIT_LOW;
private static final Integer LUX_SPLIT_MEDIUM;
private static final Integer LUX_SPLIT_SUPREME_FIVE;
private static final Integer LUX_SPLIT_SUPREME_FOUR;
private static final Integer LUX_SPLIT_SUPREME_ONE;
private static final Integer LUX_SPLIT_SUPREME_THREE;
private static final Integer LUX_SPLIT_SUPREME_TWO;
private static final Integer MAX_NIT_SPAN;
private static final Integer MAX_SPAN_INDEX;
private static final Integer MSG_BRIGHTNESS_REPORT_DEBOUNCE;
private static final Integer MSG_HANDLE_EVENT_CHANGED;
private static final Integer MSG_REPORT_DISABLE_AUTO_BRIGHTNESS_EVENT;
private static final Integer MSG_RESET_BRIGHTNESS_ANIMATION_INFO;
private static final Integer MSG_RESET_FLARE_NOT_SUPPRESS_DARKEN_STATUS;
private static final Integer MSG_RESET_FLARE_SUPPRESS_DARKEN_STATUS;
private static final Integer MSG_ROTATION_CHANGED;
private static final Integer MSG_UPDATE_BRIGHTNESS_ANIMATION_DURATION;
private static final Integer MSG_UPDATE_BRIGHTNESS_ANIMATION_INFO;
private static final Integer MSG_UPDATE_BRIGHTNESS_ANIMATION_TARGET;
private static final Integer MSG_UPDATE_BRIGHTNESS_STATISTICS_DATA;
private static final Integer MSG_UPDATE_FOREGROUND_APP;
private static final Integer MSG_UPDATE_MOTION_EVENT;
private static final Integer MSG_UPDATE_SCREEN_STATE;
private static final Integer MSG_UPDATE_TEMPORARY_BRIGHTNESS_TIME_STAMP;
private static final java.lang.String REASON_UPDATE_BRIGHTNESS_USAGE_REPORT;
private static final java.lang.String REASON_UPDATE_BRIGHTNESS_USAGE_SCREEN_OFF;
private static final java.lang.String REASON_UPDATE_BRIGHTNESS_USAGE_SPAN_CHANGE;
private static final Float SCREEN_NIT_SPAN_FIVE;
private static final Float SCREEN_NIT_SPAN_FOUR;
private static final Float SCREEN_NIT_SPAN_ONE;
private static final Float SCREEN_NIT_SPAN_SIX;
private static final Float SCREEN_NIT_SPAN_THREE;
private static final Float SCREEN_NIT_SPAN_TWO;
private static final Integer SENSOR_TYPE_LIGHT_FOV;
private static final Integer SPAN_LUX_STEP_HIGH;
private static final Integer SPAN_LUX_STEP_LOW;
private static final Integer SPAN_LUX_STEP_MEDIUM;
private static final Integer SPAN_LUX_STEP_SUPREME;
private static final Float SPAN_SCREEN_NIT_STEP_FOUR;
private static final Float SPAN_SCREEN_NIT_STEP_ONE;
private static final Float SPAN_SCREEN_NIT_STEP_THREE;
private static final Float SPAN_SCREEN_NIT_STEP_TWO;
protected static final java.lang.String TAG;
private static final Integer THERMAL_STATUS_INVALID_THERMAL_UNRESTRICTED_BRIGHTNESS;
private static final Integer THERMAL_STATUS_VALID_THERMAL_RESTRICTED_BRIGHTNESS;
private static final Integer THERMAL_STATUS_VALID_THERMAL_UNRESTRICTED_BRIGHTNESS;
public static final Integer TYPE_IN_FLARE_SCENE;
public static final Integer TYPE_NOT_IN_FLARE_SCENE;
public static final Integer TYPE_NOT_REPORT_IN_TIME;
/* # instance fields */
private android.hardware.Sensor mAccSensor;
private Boolean mAccSensorEnabled;
private final android.app.IActivityTaskManager mActivityTaskManager;
private Float mActualNit;
private java.util.Map mAdvancedBrightnessEventsMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/display/statistics/AggregationEvent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.app.AlarmManager mAlarmManager;
private java.util.Map mAonFlareEventsMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/display/statistics/AggregationEvent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final com.android.server.display.aiautobrt.AppClassifier mAppClassifier;
private Float mAutoBrightnessDuration;
private Boolean mAutoBrightnessEnable;
private Float mAutoBrightnessIntegral;
private Boolean mAutoBrightnessModeChanged;
private final android.os.Handler mBackgroundHandler;
private android.util.Spline mBrighteningSceneSpline;
private Boolean mBrightnessAnimStart;
private Long mBrightnessAnimStartTime;
private Integer mBrightnessChangedState;
private java.util.Map mBrightnessEventsMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/display/statistics/AggregationEvent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.display.BrightnessMappingStrategy mBrightnessMapper;
private final Float mBrightnessMin;
private java.util.Map mCbmEventsMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/display/statistics/AggregationEvent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.display.AutomaticBrightnessControllerImpl$CloudControllerListener mCloudControllerListener;
private android.content.ContentResolver mContentResolver;
private final android.content.Context mContext;
private Float mCurrentBrightnessAnimValue;
private Integer mCurrentUserId;
private android.util.Spline mDarkeningSceneSpline;
private android.util.Spline mDefaultSceneSpline;
private Float mDefaultSplineError;
private final android.util.SparseArray mDetailThermalRestrictedUsage;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Float;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private final android.util.SparseArray mDetailThermalUnrestrictedUsage;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Float;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.display.DisplayDeviceConfig mDisplayDeviceConfig;
private Integer mExpId;
private Boolean mForcedReportTrainDataEnabled;
private java.lang.String mForegroundAppPackageName;
private final miui.process.IForegroundWindowListener mForegroundWindowListener;
private Float mGrayScale;
private Boolean mHaveValidMotionForWindowBrightness;
private Boolean mHaveValidWindowBrightness;
private final Float mHbmMinLux;
private java.lang.String mHdrAppPackageName;
public final com.android.server.display.aiautobrt.IndividualEventNormalizer mIndividualEventNormalizer;
private Boolean mIsAonSuppressDarken;
private Boolean mIsHdrLayer;
private Boolean mIsMiShow;
private Boolean mIsNotAonSuppressDarken;
private Boolean mIsTemporaryBrightnessAdjustment;
private Boolean mIsValidResetAutoBrightnessMode;
private Float mLastAmbientLux;
private Float mLastAssistAmbientLux;
private Float mLastAutoBrightness;
private Boolean mLastAutoBrightnessEnable;
private com.android.server.display.statistics.BrightnessDataProcessor$BrightnessChangeItem mLastBrightnessChangeItem;
private Float mLastBrightnessOverrideFromWindow;
private Float mLastBrightnessRestrictedTimeStamp;
private Integer mLastConditionId;
private Long mLastDetailThermalUsageTimeStamp;
private Long mLastHdrEnableTimeStamp;
private Float mLastMainAmbientLux;
private Float mLastManualBrightness;
private Long mLastOutDoorHighTemTimeStamp;
private Boolean mLastOutDoorHighTempState;
private Long mLastResetBrightnessModeTime;
private Float mLastRestrictedBrightness;
private Float mLastScreenBrightness;
private Long mLastScreenOnTimeStamp;
private Float mLastStoreBrightness;
private Float mLastTemperature;
private Long mLastThermalStatusTimeStamp;
private Float mLastUnrestrictedBrightness;
private Long mLatestDraggingChangedTime;
private android.hardware.Sensor mLightFovSensor;
private Boolean mLightFovSensorEnabled;
private android.hardware.Sensor mLightSensor;
private Boolean mLightSensorEnabled;
private Float mLongTermModelSplineError;
private Float mManualBrightnessDuration;
private Float mManualBrightnessIntegral;
private Float mMinOutDoorHighTempNit;
private com.android.server.display.statistics.BrightnessDataProcessor$ModelEventCallback mModelEventCallback;
private java.util.List mModelTrainIndicatorsList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private final Float mNormalBrightnessMax;
private final android.app.AlarmManager$OnAlarmListener mOnAlarmListener;
private android.util.Spline mOriSpline;
private Integer mOrientation;
private Float mOriginalNit;
private Boolean mPendingAnimationStart;
private com.android.server.display.statistics.BrightnessDataProcessor$BrightnessChangeItem mPendingBrightnessChangeItem;
private Integer mPendingBrightnessChangedState;
private Float mPendingTargetBrightnessAnimValue;
private com.android.server.display.statistics.BrightnessDataProcessor$WindowOverlayEventListener mPointerEventListener;
private Boolean mPointerEventListenerEnabled;
private final com.android.server.policy.WindowManagerPolicy mPolicy;
private final android.os.PowerManager mPowerManager;
private volatile Boolean mReportBrightnessEventsEnable;
private Boolean mRotationListenerEnabled;
private com.android.server.display.statistics.BrightnessDataProcessor$RotationWatcher mRotationWatcher;
private Boolean mScreenOn;
private com.android.server.display.statistics.BrightnessDataProcessor$SensorListener mSensorListener;
private final android.hardware.SensorManager mSensorManager;
private com.android.server.display.statistics.BrightnessDataProcessor$SettingsObserver mSettingsObserver;
private Long mStartTimeStamp;
private com.android.server.display.statistics.SwitchStatsHelper mSwitchStatsHelper;
private Float mTargetBrightnessAnimValue;
private com.android.server.display.statistics.BrightnessDataProcessor$TaskStackListenerImpl mTaskStackListener;
private final java.util.List mTemperatureSpan;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long mTemporaryBrightnessTimeStamp;
private final android.util.SparseArray mThermalBrightnessRestrictedUsage;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Float;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mThermalEventsMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/display/statistics/AggregationEvent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mThermalStatus;
private Boolean mUserDragging;
private Boolean mWindowOverrideBrightnessChanging;
private final com.android.server.wm.WindowManagerService mWms;
/* # direct methods */
public static void $r8$lambda$5qMT5d1PNKjIZv8OdxG-2nMjYoU ( com.android.server.display.statistics.BrightnessDataProcessor p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$updateExpId$2(I)V */
return;
} // .end method
public static void $r8$lambda$6IecPUjGHBE-Bx5Ck3Z9tIP6DQs ( com.android.server.display.statistics.BrightnessDataProcessor p0, Boolean p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$startHdyUsageStats$7(Z)V */
return;
} // .end method
public static void $r8$lambda$9tgdtxpUQ4JCbkGhntloDez1X98 ( com.android.server.display.statistics.BrightnessDataProcessor p0, Integer p1, Float p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$noteDetailThermalUsage$4(IF)V */
return;
} // .end method
public static void $r8$lambda$ArOf22yAnGXEP5nt7SDdbucjKug ( com.android.server.display.statistics.BrightnessDataProcessor p0, Float p1, Float p2, Boolean p3, Integer p4, Float p5 ) { //synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$noteFullSceneThermalUsageStats$3(FFZIF)V */
return;
} // .end method
public static void $r8$lambda$CCallUpQ1PZW0X3Go7tGakSov8o ( com.android.server.display.statistics.BrightnessDataProcessor p0, Float p1, Float p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$updateScreenNits$8(FF)V */
return;
} // .end method
public static void $r8$lambda$Ew8O1G3Sf0HBJrqhJGVO1Xt5zB8 ( com.android.server.display.statistics.BrightnessDataProcessor p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$aggregateModelPredictTimeoutTimes$11()V */
return;
} // .end method
public static void $r8$lambda$KRcmTewZisTtEhpXZHsnRmfToT0 ( com.android.server.display.statistics.BrightnessDataProcessor p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->start()V */
return;
} // .end method
public static void $r8$lambda$MbGu4gM5oITybd38d9xshBxdr8k ( com.android.server.display.statistics.BrightnessDataProcessor p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$new$0()V */
return;
} // .end method
public static void $r8$lambda$NRI-aGGfaZ40wW_kJpQOEtpVUfE ( com.android.server.display.statistics.BrightnessDataProcessor p0, Boolean p1, Float p2, Boolean p3, Float p4 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$updateThermalStats$5(ZFZF)V */
return;
} // .end method
public static void $r8$lambda$SGfyDWAR1awWsSLqiXuehLDDuoU ( com.android.server.display.statistics.BrightnessDataProcessor p0, Boolean p1, Float p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$noteAverageTemperature$6(ZF)V */
return;
} // .end method
public static void $r8$lambda$e-Z4CwLvvcyzQGcBwTh_-p9ZpYI ( com.android.server.display.statistics.BrightnessDataProcessor p0, Boolean p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$aggregateIndividualModelTrainTimes$10(Z)V */
return;
} // .end method
public static void $r8$lambda$jlp0xvT8gc6txC5MHiOr44fwxBY ( com.android.server.display.statistics.BrightnessDataProcessor p0, Boolean p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$forceReportTrainDataEnabled$9(Z)V */
return;
} // .end method
public static void $r8$lambda$oQzhtpP4993nLvClxlXuPEeHl9c ( com.android.server.display.statistics.BrightnessDataProcessor p0, Float p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$updateGrayScale$1(F)V */
return;
} // .end method
static android.os.Handler -$$Nest$fgetmBackgroundHandler ( com.android.server.display.statistics.BrightnessDataProcessor p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBackgroundHandler;
} // .end method
static android.content.ContentResolver -$$Nest$fgetmContentResolver ( com.android.server.display.statistics.BrightnessDataProcessor p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContentResolver;
} // .end method
static Boolean -$$Nest$fgetmReportBrightnessEventsEnable ( com.android.server.display.statistics.BrightnessDataProcessor p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mReportBrightnessEventsEnable:Z */
} // .end method
static Boolean -$$Nest$fgetmUserDragging ( com.android.server.display.statistics.BrightnessDataProcessor p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mUserDragging:Z */
} // .end method
static void -$$Nest$fputmAutoBrightnessEnable ( com.android.server.display.statistics.BrightnessDataProcessor p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z */
return;
} // .end method
static void -$$Nest$fputmAutoBrightnessModeChanged ( com.android.server.display.statistics.BrightnessDataProcessor p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessModeChanged:Z */
return;
} // .end method
static void -$$Nest$fputmIsAonSuppressDarken ( com.android.server.display.statistics.BrightnessDataProcessor p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z */
return;
} // .end method
static void -$$Nest$fputmIsMiShow ( com.android.server.display.statistics.BrightnessDataProcessor p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z */
return;
} // .end method
static void -$$Nest$fputmIsNotAonSuppressDarken ( com.android.server.display.statistics.BrightnessDataProcessor p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z */
return;
} // .end method
static void -$$Nest$fputmOrientation ( com.android.server.display.statistics.BrightnessDataProcessor p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOrientation:I */
return;
} // .end method
static void -$$Nest$fputmTargetBrightnessAnimValue ( com.android.server.display.statistics.BrightnessDataProcessor p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTargetBrightnessAnimValue:F */
return;
} // .end method
static void -$$Nest$fputmTemporaryBrightnessTimeStamp ( com.android.server.display.statistics.BrightnessDataProcessor p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemporaryBrightnessTimeStamp:J */
return;
} // .end method
static void -$$Nest$mbrightnessChangedTriggerAggregation ( com.android.server.display.statistics.BrightnessDataProcessor p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->brightnessChangedTriggerAggregation()V */
return;
} // .end method
static void -$$Nest$mhandleAccSensor ( com.android.server.display.statistics.BrightnessDataProcessor p0, android.hardware.SensorEvent p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->handleAccSensor(Landroid/hardware/SensorEvent;)V */
return;
} // .end method
static void -$$Nest$mhandleBrightnessChangeEvent ( com.android.server.display.statistics.BrightnessDataProcessor p0, com.android.server.display.statistics.BrightnessDataProcessor$BrightnessChangeItem p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->handleBrightnessChangeEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;)V */
return;
} // .end method
static void -$$Nest$mhandleLightFovSensor ( com.android.server.display.statistics.BrightnessDataProcessor p0, android.hardware.SensorEvent p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->handleLightFovSensor(Landroid/hardware/SensorEvent;)V */
return;
} // .end method
static void -$$Nest$mhandleLightSensor ( com.android.server.display.statistics.BrightnessDataProcessor p0, android.hardware.SensorEvent p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->handleLightSensor(Landroid/hardware/SensorEvent;)V */
return;
} // .end method
static void -$$Nest$mreadyToReportEvent ( com.android.server.display.statistics.BrightnessDataProcessor p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->readyToReportEvent()V */
return;
} // .end method
static void -$$Nest$mreportDisabledAutoBrightnessEvent ( com.android.server.display.statistics.BrightnessDataProcessor p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportDisabledAutoBrightnessEvent()V */
return;
} // .end method
static void -$$Nest$mreportEventToServer ( com.android.server.display.statistics.BrightnessDataProcessor p0, com.android.server.display.statistics.BrightnessEvent p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportEventToServer(Lcom/android/server/display/statistics/BrightnessEvent;)V */
return;
} // .end method
static void -$$Nest$mresetBrightnessAnimInfo ( com.android.server.display.statistics.BrightnessDataProcessor p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->resetBrightnessAnimInfo()V */
return;
} // .end method
static void -$$Nest$mresetPendingParams ( com.android.server.display.statistics.BrightnessDataProcessor p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->resetPendingParams()V */
return;
} // .end method
static void -$$Nest$mupdateBrightnessAnimInfo ( com.android.server.display.statistics.BrightnessDataProcessor p0, Float p1, Float p2, Boolean p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateBrightnessAnimInfo(FFZ)V */
return;
} // .end method
static void -$$Nest$mupdateBrightnessStatisticsData ( com.android.server.display.statistics.BrightnessDataProcessor p0, Boolean p1, Float p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateBrightnessStatisticsData(ZF)V */
return;
} // .end method
static void -$$Nest$mupdateForegroundApps ( com.android.server.display.statistics.BrightnessDataProcessor p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateForegroundApps()V */
return;
} // .end method
static void -$$Nest$mupdateInterruptBrightnessAnimDuration ( com.android.server.display.statistics.BrightnessDataProcessor p0, Integer p1, Float p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateInterruptBrightnessAnimDuration(IF)V */
return;
} // .end method
static void -$$Nest$mupdatePointerEventMotionState ( com.android.server.display.statistics.BrightnessDataProcessor p0, Boolean p1, Integer p2, Integer p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updatePointerEventMotionState(ZII)V */
return;
} // .end method
static void -$$Nest$mupdateScreenStateChanged ( com.android.server.display.statistics.BrightnessDataProcessor p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateScreenStateChanged(Z)V */
return;
} // .end method
static void -$$Nest$mupdateUserResetAutoBrightnessModeTimes ( com.android.server.display.statistics.BrightnessDataProcessor p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateUserResetAutoBrightnessModeTimes()V */
return;
} // .end method
static Boolean -$$Nest$sfgetDEBUG ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
} // .end method
static com.android.server.display.statistics.BrightnessDataProcessor ( ) {
/* .locals 2 */
/* .line 103 */
/* nop */
/* .line 104 */
final String v0 = "debug.miui.display.dgb"; // const-string v0, "debug.miui.display.dgb"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
com.android.server.display.statistics.BrightnessDataProcessor.DEBUG = (v1!= 0);
/* .line 103 */
return;
} // .end method
public com.android.server.display.statistics.BrightnessDataProcessor ( ) {
/* .locals 17 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "displayDeviceConfig" # Lcom/android/server/display/DisplayDeviceConfig; */
/* .param p3, "min" # F */
/* .param p4, "normalMax" # F */
/* .param p5, "hbmMinLux" # F */
/* .line 383 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move/from16 v2, p3 */
/* move/from16 v3, p4 */
/* move/from16 v4, p5 */
/* invoke-direct/range {p0 ..p0}, Ljava/lang/Object;-><init>()V */
/* .line 204 */
/* const/16 v5, -0x2710 */
/* iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentUserId:I */
/* .line 224 */
/* const/high16 v5, 0x7fc00000 # Float.NaN */
/* iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastStoreBrightness:F */
/* .line 225 */
/* iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightness:F */
/* .line 226 */
/* iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastManualBrightness:F */
/* .line 240 */
int v5 = -1; // const/4 v5, -0x1
/* iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I */
/* .line 243 */
/* iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangedState:I */
/* .line 245 */
/* const/high16 v5, -0x40800000 # -1.0f */
/* iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLongTermModelSplineError:F */
/* .line 247 */
/* iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDefaultSplineError:F */
/* .line 275 */
/* new-instance v6, Ljava/util/HashMap; */
/* invoke-direct {v6}, Ljava/util/HashMap;-><init>()V */
this.mBrightnessEventsMap = v6;
/* .line 277 */
/* new-instance v6, Ljava/util/HashMap; */
/* invoke-direct {v6}, Ljava/util/HashMap;-><init>()V */
this.mAdvancedBrightnessEventsMap = v6;
/* .line 283 */
/* new-instance v6, Ljava/util/HashMap; */
/* invoke-direct {v6}, Ljava/util/HashMap;-><init>()V */
this.mThermalEventsMap = v6;
/* .line 296 */
/* new-instance v6, Landroid/util/SparseArray; */
/* invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V */
this.mDetailThermalUnrestrictedUsage = v6;
/* .line 298 */
/* new-instance v6, Landroid/util/SparseArray; */
/* invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V */
this.mDetailThermalRestrictedUsage = v6;
/* .line 301 */
/* new-instance v6, Ljava/util/ArrayList; */
/* invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V */
this.mTemperatureSpan = v6;
/* .line 309 */
/* new-instance v6, Landroid/util/SparseArray; */
/* invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V */
this.mThermalBrightnessRestrictedUsage = v6;
/* .line 329 */
/* iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAmbientLux:F */
/* .line 331 */
/* iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastMainAmbientLux:F */
/* .line 333 */
/* iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAssistAmbientLux:F */
/* .line 360 */
/* new-instance v5, Lcom/android/server/display/statistics/BrightnessDataProcessor$1; */
/* invoke-direct {v5, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$1;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V */
this.mForegroundWindowListener = v5;
/* .line 368 */
/* new-instance v5, Ljava/util/HashMap; */
/* invoke-direct {v5}, Ljava/util/HashMap;-><init>()V */
this.mCbmEventsMap = v5;
/* .line 370 */
/* new-instance v5, Ljava/util/ArrayList; */
/* invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V */
this.mModelTrainIndicatorsList = v5;
/* .line 378 */
/* new-instance v5, Ljava/util/HashMap; */
/* invoke-direct {v5}, Ljava/util/HashMap;-><init>()V */
this.mAonFlareEventsMap = v5;
/* .line 441 */
/* new-instance v5, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda5; */
/* invoke-direct {v5, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V */
this.mOnAlarmListener = v5;
/* .line 384 */
this.mContext = v1;
/* .line 385 */
/* move-object/from16 v5, p2 */
this.mDisplayDeviceConfig = v5;
/* .line 386 */
/* iput v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessMin:F */
/* .line 387 */
/* iput v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mNormalBrightnessMax:F */
/* .line 388 */
/* iput v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHbmMinLux:F */
/* .line 389 */
/* const-class v6, Lcom/android/server/policy/WindowManagerPolicy; */
com.android.server.LocalServices .getService ( v6 );
/* check-cast v6, Lcom/android/server/policy/WindowManagerPolicy; */
this.mPolicy = v6;
/* .line 390 */
/* new-instance v6, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler; */
com.android.internal.os.BackgroundThread .getHandler ( );
(( android.os.Handler ) v7 ).getLooper ( ); // invoke-virtual {v7}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v6, v0, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Landroid/os/Looper;)V */
this.mBackgroundHandler = v6;
/* .line 391 */
android.app.ActivityTaskManager .getService ( );
this.mActivityTaskManager = v7;
/* .line 392 */
/* const-string/jumbo v7, "window" */
android.os.ServiceManager .getService ( v7 );
/* check-cast v7, Lcom/android/server/wm/WindowManagerService; */
this.mWms = v7;
/* .line 393 */
final String v7 = "power"; // const-string v7, "power"
(( android.content.Context ) v1 ).getSystemService ( v7 ); // invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v7, Landroid/os/PowerManager; */
this.mPowerManager = v7;
/* .line 394 */
final String v7 = "alarm"; // const-string v7, "alarm"
(( android.content.Context ) v1 ).getSystemService ( v7 ); // invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v7, Landroid/app/AlarmManager; */
this.mAlarmManager = v7;
/* .line 395 */
/* const-string/jumbo v7, "sensor" */
(( android.content.Context ) v1 ).getSystemService ( v7 ); // invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v7, Landroid/hardware/SensorManager; */
this.mSensorManager = v7;
/* .line 396 */
com.android.server.display.aiautobrt.AppClassifier .getInstance ( );
this.mAppClassifier = v7;
/* .line 397 */
/* new-instance v7, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer; */
v8 = this.mDisplayDeviceConfig;
/* .line 398 */
v9 = (( com.android.server.display.DisplayDeviceConfig ) v8 ).getNitsFromBacklight ( v2 ); // invoke-virtual {v8, v2}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
v8 = this.mDisplayDeviceConfig;
/* .line 399 */
v10 = (( com.android.server.display.DisplayDeviceConfig ) v8 ).getNitsFromBacklight ( v3 ); // invoke-virtual {v8, v3}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
int v11 = 0; // const/4 v11, 0x0
/* const/high16 v8, 0x3f800000 # 1.0f */
/* sub-float v12, v4, v8 */
int v13 = 0; // const/4 v13, 0x0
/* .line 401 */
v14 = /* invoke-direct {v0, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I */
/* int-to-float v14, v14 */
int v15 = 0; // const/4 v15, 0x0
/* sub-float v8, v4, v8 */
/* .line 402 */
v8 = /* invoke-direct {v0, v8}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getAmbientLuxSpanIndex(F)I */
/* int-to-float v8, v8 */
/* move/from16 v16, v8 */
/* move-object v8, v7 */
/* invoke-direct/range {v8 ..v16}, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;-><init>(FFFFFFFF)V */
this.mIndividualEventNormalizer = v7;
/* .line 403 */
/* new-instance v7, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda6; */
/* invoke-direct {v7, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V */
(( android.os.Handler ) v6 ).post ( v7 ); // invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 404 */
return;
} // .end method
private void aggregateAdvancedBrightnessEventsSum ( java.lang.String p0, java.lang.Object p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "keySubEvent" # Ljava/lang/String; */
/* .param p2, "key" # Ljava/lang/Object; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .line 1889 */
v0 = this.mAdvancedBrightnessEventsMap;
/* check-cast v0, Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 1890 */
/* .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent; */
/* if-nez v0, :cond_0 */
/* .line 1891 */
/* new-instance v1, Lcom/android/server/display/statistics/AggregationEvent$AdvancedAggregationEvent; */
/* invoke-direct {v1}, Lcom/android/server/display/statistics/AggregationEvent$AdvancedAggregationEvent;-><init>()V */
/* move-object v0, v1 */
/* .line 1893 */
} // :cond_0
(( com.android.server.display.statistics.AggregationEvent ) v0 ).getQuotaEvents ( ); // invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;
/* invoke-direct {p0, v1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsSummary(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1894 */
v1 = this.mAdvancedBrightnessEventsMap;
/* .line 1895 */
return;
} // .end method
private void aggregateAonFlareEventsSum ( java.lang.String p0, java.lang.Object p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "keySubEvent" # Ljava/lang/String; */
/* .param p2, "key" # Ljava/lang/Object; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .line 3127 */
v0 = this.mAonFlareEventsMap;
/* check-cast v0, Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 3128 */
/* .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent; */
/* if-nez v0, :cond_0 */
/* .line 3129 */
/* new-instance v1, Lcom/android/server/display/statistics/AggregationEvent$AonFlareAggregationEvent; */
/* invoke-direct {v1}, Lcom/android/server/display/statistics/AggregationEvent$AonFlareAggregationEvent;-><init>()V */
/* move-object v0, v1 */
/* .line 3131 */
} // :cond_0
(( com.android.server.display.statistics.AggregationEvent ) v0 ).getQuotaEvents ( ); // invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;
/* invoke-direct {p0, v1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsSummary(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 3132 */
v1 = this.mAonFlareEventsMap;
/* .line 3133 */
return;
} // .end method
private void aggregateBrightnessEventsAvg ( java.lang.String p0, java.lang.Object p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "keySubEvent" # Ljava/lang/String; */
/* .param p2, "key" # Ljava/lang/Object; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .line 1865 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 1866 */
/* .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent; */
/* move-object v1, p3 */
/* check-cast v1, Ljava/lang/Float; */
/* invoke-direct {p0, v0, p2, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsAverageValues(Lcom/android/server/display/statistics/AggregationEvent;Ljava/lang/Object;Ljava/lang/Float;)V */
/* .line 1867 */
v1 = this.mBrightnessEventsMap;
/* .line 1868 */
return;
} // .end method
private void aggregateBrightnessEventsSum ( java.lang.String p0, java.lang.Object p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "keySubEvent" # Ljava/lang/String; */
/* .param p2, "key" # Ljava/lang/Object; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .line 1853 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 1854 */
/* .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent; */
(( com.android.server.display.statistics.AggregationEvent ) v0 ).getQuotaEvents ( ); // invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;
/* invoke-direct {p0, v1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsSummary(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1855 */
v1 = this.mBrightnessEventsMap;
/* .line 1856 */
return;
} // .end method
private void aggregateBrightnessEventsSync ( java.lang.String p0, java.lang.Object p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "keySubEvent" # Ljava/lang/String; */
/* .param p2, "key" # Ljava/lang/Object; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .line 1877 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 1878 */
/* .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent; */
(( com.android.server.display.statistics.AggregationEvent ) v0 ).getQuotaEvents ( ); // invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;
/* .line 1879 */
v1 = this.mBrightnessEventsMap;
/* .line 1880 */
return;
} // .end method
private void aggregateBrightnessUsageDuration ( ) {
/* .locals 3 */
/* .line 774 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "report"; // const-string v2, "report"
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateBrightnessUsage(IZLjava/lang/String;)V */
/* .line 776 */
return;
} // .end method
private void aggregateCbmEventsAvg ( java.lang.String p0, java.lang.Object p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "keySubEvent" # Ljava/lang/String; */
/* .param p2, "key" # Ljava/lang/Object; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .line 2955 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getCbmAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 2956 */
/* .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent; */
/* move-object v1, p3 */
/* check-cast v1, Ljava/lang/Float; */
/* invoke-direct {p0, v0, p2, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsAverageValues(Lcom/android/server/display/statistics/AggregationEvent;Ljava/lang/Object;Ljava/lang/Float;)V */
/* .line 2957 */
v1 = this.mCbmEventsMap;
/* .line 2958 */
return;
} // .end method
private void aggregateCbmEventsSum ( java.lang.String p0, java.lang.Object p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "keySubEvent" # Ljava/lang/String; */
/* .param p2, "key" # Ljava/lang/Object; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .line 2943 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getCbmAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 2944 */
/* .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent; */
(( com.android.server.display.statistics.AggregationEvent ) v0 ).getQuotaEvents ( ); // invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;
/* invoke-direct {p0, v1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsSummary(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2945 */
v1 = this.mCbmEventsMap;
/* .line 2946 */
return;
} // .end method
private void aggregateCbmEventsSync ( java.lang.String p0, java.lang.Object p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "keySubEvent" # Ljava/lang/String; */
/* .param p2, "key" # Ljava/lang/Object; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .line 2931 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getCbmAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 2932 */
/* .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent; */
(( com.android.server.display.statistics.AggregationEvent ) v0 ).getQuotaEvents ( ); // invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;
/* .line 2933 */
v1 = this.mCbmEventsMap;
/* .line 2934 */
return;
} // .end method
private void aggregateSwitchEvents ( ) {
/* .locals 3 */
/* .line 1970 */
v0 = this.mSwitchStatsHelper;
(( com.android.server.display.statistics.SwitchStatsHelper ) v0 ).getAllSwitchStats ( ); // invoke-virtual {v0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getAllSwitchStats()Ljava/util/List;
/* .line 1971 */
/* .local v0, "allSwitchStats":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;>;" */
/* sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1972 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "aggregateSwitchEvents: allSwitchStats:"; // const-string v2, "aggregateSwitchEvents: allSwitchStats:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BrightnessDataProcessor"; // const-string v2, "BrightnessDataProcessor"
android.util.Slog .d ( v2,v1 );
/* .line 1974 */
} // :cond_0
/* const-string/jumbo v1, "switch_stats" */
/* const-string/jumbo v2, "switch_stats_details" */
/* invoke-direct {p0, v1, v2, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1975 */
return;
} // .end method
private void aggregateThermalEventsAvg ( java.lang.String p0, java.lang.Object p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "keySubEvent" # Ljava/lang/String; */
/* .param p2, "key" # Ljava/lang/Object; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .line 2062 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getThermalAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 2063 */
/* .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent; */
/* move-object v1, p3 */
/* check-cast v1, Ljava/lang/Float; */
/* invoke-direct {p0, v0, p2, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsAverageValues(Lcom/android/server/display/statistics/AggregationEvent;Ljava/lang/Object;Ljava/lang/Float;)V */
/* .line 2064 */
v1 = this.mThermalEventsMap;
/* .line 2065 */
return;
} // .end method
private void aggregateThermalEventsSum ( java.lang.String p0, java.lang.Object p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "keySubEvent" # Ljava/lang/String; */
/* .param p2, "key" # Ljava/lang/Object; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .line 2050 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getThermalAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 2051 */
/* .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent; */
(( com.android.server.display.statistics.AggregationEvent ) v0 ).getQuotaEvents ( ); // invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;
/* invoke-direct {p0, v1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsSummary(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2052 */
v1 = this.mThermalEventsMap;
/* .line 2053 */
return;
} // .end method
private void aggregateThermalEventsSync ( java.lang.String p0, java.lang.Object p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "keySubEvent" # Ljava/lang/String; */
/* .param p2, "key" # Ljava/lang/Object; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .line 2038 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getThermalAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 2039 */
/* .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent; */
(( com.android.server.display.statistics.AggregationEvent ) v0 ).getQuotaEvents ( ); // invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;
/* .line 2040 */
v1 = this.mThermalEventsMap;
/* .line 2041 */
return;
} // .end method
private void brightnessChangedTriggerAggregation ( ) {
/* .locals 12 */
/* .line 1797 */
v0 = this.mPendingBrightnessChangeItem;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1798 */
/* iget v0, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
/* .line 1799 */
/* .local v0, "type":I */
v1 = this.mPendingBrightnessChangeItem;
v1 = this.packageName;
/* .line 1800 */
/* .local v1, "pkg":Ljava/lang/String; */
v2 = /* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->convergeAffectFactors()I */
/* .line 1801 */
/* .local v2, "factor":I */
v3 = this.mPendingBrightnessChangeItem;
/* iget v3, v3, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F */
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getAmbientLuxSpanIndex(F)I */
/* .line 1802 */
/* .local v3, "luxSpan":I */
/* iget v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F */
v4 = /* invoke-direct {p0, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I */
/* .line 1803 */
/* .local v4, "preBrightnessSpan":I */
v5 = this.mPendingBrightnessChangeItem;
/* iget v5, v5, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F */
v5 = /* invoke-direct {p0, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I */
/* .line 1804 */
/* .local v5, "curBrightnessSpan":I */
v6 = this.mPendingBrightnessChangeItem;
/* iget v6, v6, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F */
/* .line 1805 */
/* .local v6, "brightness":F */
v7 = this.mDisplayDeviceConfig;
v7 = (( com.android.server.display.DisplayDeviceConfig ) v7 ).getNitsFromBacklight ( v6 ); // invoke-virtual {v7, v6}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
v7 = java.lang.Math .round ( v7 );
/* int-to-float v7, v7 */
/* .line 1807 */
/* .local v7, "curNit":F */
java.lang.Integer .valueOf ( v0 );
int v9 = 1; // const/4 v9, 0x1
java.lang.Integer .valueOf ( v9 );
final String v11 = "brightness_adj_times"; // const-string v11, "brightness_adj_times"
/* invoke-direct {p0, v11, v8, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1808 */
int v8 = 3; // const/4 v8, 0x3
/* if-ne v0, v8, :cond_0 */
/* .line 1810 */
final String v8 = "override_adj_app_ranking"; // const-string v8, "override_adj_app_ranking"
java.lang.Integer .valueOf ( v9 );
/* invoke-direct {p0, v8, v1, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1811 */
} // :cond_0
int v8 = 2; // const/4 v8, 0x2
/* if-ne v0, v8, :cond_2 */
/* .line 1813 */
final String v8 = "auto_manual_adj_app_ranking"; // const-string v8, "auto_manual_adj_app_ranking"
java.lang.Integer .valueOf ( v9 );
/* invoke-direct {p0, v8, v1, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1815 */
java.lang.Integer .valueOf ( v3 );
java.lang.Float .valueOf ( v7 );
final String v11 = "auto_manual_adj_avg_nits_lux_span"; // const-string v11, "auto_manual_adj_avg_nits_lux_span"
/* invoke-direct {p0, v11, v8, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsAvg(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1817 */
java.lang.Integer .valueOf ( v2 );
java.lang.Integer .valueOf ( v9 );
final String v11 = "auto_manual_adj_display_mode"; // const-string v11, "auto_manual_adj_display_mode"
/* invoke-direct {p0, v11, v8, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1819 */
java.lang.Integer .valueOf ( v3 );
java.lang.Integer .valueOf ( v9 );
final String v11 = "auto_manual_adj_lux_span"; // const-string v11, "auto_manual_adj_lux_span"
/* invoke-direct {p0, v11, v8, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1821 */
/* if-ge v5, v4, :cond_1 */
/* .line 1822 */
java.lang.Integer .valueOf ( v3 );
java.lang.Integer .valueOf ( v9 );
final String v11 = "auto_manual_adj_low_lux_span"; // const-string v11, "auto_manual_adj_low_lux_span"
/* invoke-direct {p0, v11, v8, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1823 */
int v8 = 0; // const/4 v8, 0x0
/* invoke-direct {p0, v8}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->flareStatisticalManualAdjustTimes(Z)V */
/* .line 1826 */
} // :cond_1
/* if-le v5, v4, :cond_2 */
/* .line 1827 */
java.lang.Integer .valueOf ( v3 );
java.lang.Integer .valueOf ( v9 );
final String v11 = "auto_manual_adj_high_lux_span"; // const-string v11, "auto_manual_adj_high_lux_span"
/* invoke-direct {p0, v11, v8, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1828 */
/* invoke-direct {p0, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->flareStatisticalManualAdjustTimes(Z)V */
/* .line 1832 */
} // :cond_2
} // :goto_0
/* invoke-direct {p0, v0, v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteAdjHighTimesOnBrightnessRestricted(IF)V */
/* .line 1833 */
/* sget-boolean v8, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 1834 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "brightnessChangedTriggerAggregation: type: "; // const-string v9, "brightnessChangedTriggerAggregation: type: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ", mLastScreenBrightness: "; // const-string v9, ", mLastScreenBrightness: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v9 = ", brightness: "; // const-string v9, ", brightness: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v9 = ", curNit: "; // const-string v9, ", curNit: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v9 = ", preBrightnessSpan: "; // const-string v9, ", preBrightnessSpan: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ", curBrightnessSpan: "; // const-string v9, ", curBrightnessSpan: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ", mBrightnessEventsMap: "; // const-string v9, ", mBrightnessEventsMap: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.mBrightnessEventsMap;
/* .line 1841 */
(( java.lang.Object ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1834 */
final String v9 = "BrightnessDataProcessor"; // const-string v9, "BrightnessDataProcessor"
android.util.Slog .d ( v9,v8 );
/* .line 1844 */
} // .end local v0 # "type":I
} // .end local v1 # "pkg":Ljava/lang/String;
} // .end local v2 # "factor":I
} // .end local v3 # "luxSpan":I
} // .end local v4 # "preBrightnessSpan":I
} // .end local v5 # "curBrightnessSpan":I
} // .end local v6 # "brightness":F
} // .end local v7 # "curNit":F
} // :cond_3
return;
} // .end method
private Boolean checkIsValidMotionForWindowBrightness ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "distanceX" # I */
/* .param p2, "distanceY" # I */
/* .line 889 */
int v0 = 1; // const/4 v0, 0x1
/* .line 890 */
/* .local v0, "isValid":Z */
/* const/16 v1, 0xa */
/* if-le p2, v1, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* div-int v1, p2, p1 */
int v2 = 2; // const/4 v2, 0x2
/* if-ge v1, v2, :cond_1 */
/* .line 892 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 893 */
/* sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 894 */
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
final String v2 = "checkIsValidMotionForWindowBrightness: invalid and return."; // const-string v2, "checkIsValidMotionForWindowBrightness: invalid and return."
android.util.Slog .d ( v1,v2 );
/* .line 897 */
} // :cond_1
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHaveValidMotionForWindowBrightness:Z */
/* .line 898 */
} // .end method
private Float clampBrightness ( Float p0 ) {
/* .locals 1 */
/* .param p1, "brightness" # F */
/* .line 2892 */
v0 = java.lang.Float .isNaN ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2893 */
/* const/high16 v0, -0x40800000 # -1.0f */
/* .line 2895 */
} // :cond_0
} // .end method
private void computeAverageBrightnessIfNeeded ( Float p0 ) {
/* .locals 6 */
/* .param p1, "brightness" # F */
/* .line 1420 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastStoreBrightness:F */
/* cmpl-float v0, v0, p1 */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessModeChanged:Z */
/* if-nez v0, :cond_0 */
/* .line 1421 */
return;
/* .line 1423 */
} // :cond_0
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastStoreBrightness:F */
/* .line 1424 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 1425 */
/* .local v0, "now":J */
/* iget-wide v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenOnTimeStamp:J */
/* const-wide/16 v4, 0x0 */
/* cmp-long v2, v2, v4 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1426 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->computeLastAverageBrightness(J)V */
/* .line 1428 */
} // :cond_1
/* const/high16 v2, 0x7fc00000 # Float.NaN */
/* iput v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightness:F */
/* .line 1429 */
/* iput v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastManualBrightness:F */
/* .line 1430 */
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1431 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightness:F */
/* .line 1433 */
} // :cond_2
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastManualBrightness:F */
/* .line 1435 */
} // :goto_0
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenOnTimeStamp:J */
/* .line 1436 */
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessModeChanged:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1437 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessModeChanged:Z */
/* .line 1439 */
} // :cond_3
/* sget-boolean v2, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1440 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "computeAverageBrightnessIfNeeded: current brightness: "; // const-string v3, "computeAverageBrightnessIfNeeded: current brightness: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = ", mAutoBrightnessEnable: "; // const-string v3, ", mAutoBrightnessEnable: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ", time: "; // const-string v3, ", time: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenOnTimeStamp:J */
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "BrightnessDataProcessor"; // const-string v3, "BrightnessDataProcessor"
android.util.Slog .d ( v3,v2 );
/* .line 1444 */
} // :cond_4
return;
} // .end method
private void computeLastAverageBrightness ( Long p0 ) {
/* .locals 7 */
/* .param p1, "now" # J */
/* .line 1451 */
/* iget-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenOnTimeStamp:J */
/* sub-long v0, p1, v0 */
/* long-to-float v0, v0 */
/* const v1, 0x3a83126f # 0.001f */
/* mul-float/2addr v0, v1 */
/* .line 1453 */
/* .local v0, "timeDuration":F */
int v1 = 0; // const/4 v1, 0x0
/* cmpl-float v1, v0, v1 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1454 */
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightness:F */
v1 = java.lang.Float .isNaN ( v1 );
final String v2 = ", avgNits: "; // const-string v2, ", avgNits: "
final String v3 = "BrightnessDataProcessor"; // const-string v3, "BrightnessDataProcessor"
final String v4 = "average_brightness"; // const-string v4, "average_brightness"
/* if-nez v1, :cond_0 */
/* .line 1455 */
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessDuration:F */
/* add-float/2addr v1, v0 */
/* iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessDuration:F */
/* .line 1456 */
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessIntegral:F */
v5 = this.mDisplayDeviceConfig;
/* iget v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightness:F */
/* .line 1457 */
v5 = (( com.android.server.display.DisplayDeviceConfig ) v5 ).getNitsFromBacklight ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
/* mul-float/2addr v5, v0 */
/* add-float/2addr v1, v5 */
/* iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessIntegral:F */
/* .line 1458 */
/* iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessDuration:F */
/* div-float/2addr v1, v5 */
/* .line 1459 */
/* .local v1, "avgNits":F */
/* nop */
/* .line 1460 */
int v5 = 1; // const/4 v5, 0x1
java.lang.Integer .valueOf ( v5 );
java.lang.Float .valueOf ( v1 );
/* .line 1459 */
/* invoke-direct {p0, v4, v5, v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1461 */
/* sget-boolean v4, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1462 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "computeLastAverageBrightness: compute last auto average brightness, timeDuration: "; // const-string v5, "computeLastAverageBrightness: compute last auto average brightness, timeDuration: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = "s, mAutoBrightnessDuration: "; // const-string v5, "s, mAutoBrightnessDuration: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessDuration:F */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = "s, mAutoBrightnessIntegral: "; // const-string v5, "s, mAutoBrightnessIntegral: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessIntegral:F */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 1469 */
} // .end local v1 # "avgNits":F
} // :cond_0
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessDuration:F */
/* add-float/2addr v1, v0 */
/* iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessDuration:F */
/* .line 1470 */
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessIntegral:F */
v5 = this.mDisplayDeviceConfig;
/* iget v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastManualBrightness:F */
/* float-to-int v6, v6 */
/* .line 1471 */
v6 = com.android.internal.display.BrightnessSynchronizer .brightnessIntToFloat ( v6 );
/* .line 1470 */
v5 = (( com.android.server.display.DisplayDeviceConfig ) v5 ).getNitsFromBacklight ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
/* mul-float/2addr v5, v0 */
/* add-float/2addr v1, v5 */
/* iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessIntegral:F */
/* .line 1472 */
/* iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessDuration:F */
/* div-float/2addr v1, v5 */
/* .line 1473 */
/* .restart local v1 # "avgNits":F */
/* nop */
/* .line 1474 */
int v5 = 0; // const/4 v5, 0x0
java.lang.Integer .valueOf ( v5 );
java.lang.Float .valueOf ( v1 );
/* .line 1473 */
/* invoke-direct {p0, v4, v5, v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1475 */
/* sget-boolean v4, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1476 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "computeLastAverageBrightness: compute last manual average brightness, timeDuration: "; // const-string v5, "computeLastAverageBrightness: compute last manual average brightness, timeDuration: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = "s, mManualBrightnessDuration: "; // const-string v5, "s, mManualBrightnessDuration: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessDuration:F */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = "s, mManualBrightnessIntegral: "; // const-string v5, "s, mManualBrightnessIntegral: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessIntegral:F */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 1484 */
} // .end local v1 # "avgNits":F
} // :cond_1
} // :goto_0
return;
} // .end method
private Integer convergeAffectFactors ( ) {
/* .locals 2 */
/* .line 1041 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1042 */
/* .local v0, "factor":I */
v1 = this.mSwitchStatsHelper;
v1 = (( com.android.server.display.statistics.SwitchStatsHelper ) v1 ).isReadModeSettingsEnable ( ); // invoke-virtual {v1}, Lcom/android/server/display/statistics/SwitchStatsHelper;->isReadModeSettingsEnable()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1043 */
/* or-int/lit8 v0, v0, 0x1 */
/* .line 1045 */
} // :cond_0
v1 = this.mSwitchStatsHelper;
v1 = (( com.android.server.display.statistics.SwitchStatsHelper ) v1 ).isDcBacklightSettingsEnable ( ); // invoke-virtual {v1}, Lcom/android/server/display/statistics/SwitchStatsHelper;->isDcBacklightSettingsEnable()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1046 */
/* or-int/lit8 v0, v0, 0x2 */
/* .line 1048 */
} // :cond_1
v1 = this.mSwitchStatsHelper;
v1 = (( com.android.server.display.statistics.SwitchStatsHelper ) v1 ).isDarkModeSettingsEnable ( ); // invoke-virtual {v1}, Lcom/android/server/display/statistics/SwitchStatsHelper;->isDarkModeSettingsEnable()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1049 */
/* or-int/lit8 v0, v0, 0x4 */
/* .line 1051 */
} // :cond_2
} // .end method
private void countThermalBrightnessRestrictedUsage ( Float p0, android.util.SparseArray p1, Integer p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "duration" # F */
/* .param p3, "nit" # I */
/* .param p4, "brightnessSpan" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(F", */
/* "Landroid/util/SparseArray<", */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Float;", */
/* ">;>;II)V" */
/* } */
} // .end annotation
/* .line 2468 */
/* .local p2, "usage":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/SparseArray<Ljava/lang/Float;>;>;" */
(( android.util.SparseArray ) p2 ).get ( p3 ); // invoke-virtual {p2, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Landroid/util/SparseArray; */
/* .line 2469 */
/* .local v0, "sparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Float;>;" */
/* if-nez v0, :cond_0 */
/* .line 2470 */
/* new-instance v1, Landroid/util/SparseArray; */
/* invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V */
/* move-object v0, v1 */
/* .line 2472 */
} // :cond_0
v1 = (( android.util.SparseArray ) v0 ).contains ( p4 ); // invoke-virtual {v0, p4}, Landroid/util/SparseArray;->contains(I)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2473 */
(( android.util.SparseArray ) v0 ).get ( p4 ); // invoke-virtual {v0, p4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* add-float/2addr p1, v1 */
/* .line 2475 */
} // :cond_1
java.lang.Float .valueOf ( p1 );
(( android.util.SparseArray ) v0 ).put ( p4, v1 ); // invoke-virtual {v0, p4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 2476 */
(( android.util.SparseArray ) p2 ).put ( p3, v0 ); // invoke-virtual {p2, p3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 2477 */
/* sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 2478 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "countThermalBrightnessRestrictedUsage: nit: "; // const-string v2, "countThermalBrightnessRestrictedUsage: nit: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", brightnessSpan: "; // const-string v2, ", brightnessSpan: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p4 ); // invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", duration: "; // const-string v2, ", duration: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BrightnessDataProcessor"; // const-string v2, "BrightnessDataProcessor"
android.util.Slog .d ( v2,v1 );
/* .line 2482 */
} // :cond_2
return;
} // .end method
private void countThermalUsage ( Float p0, android.util.SparseArray p1, Integer p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "duration" # F */
/* .param p3, "conditionId" # I */
/* .param p4, "temperatureSpan" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(F", */
/* "Landroid/util/SparseArray<", */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Float;", */
/* ">;>;II)V" */
/* } */
} // .end annotation
/* .line 2443 */
/* .local p2, "usage":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/SparseArray<Ljava/lang/Float;>;>;" */
(( android.util.SparseArray ) p2 ).get ( p3 ); // invoke-virtual {p2, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Landroid/util/SparseArray; */
/* .line 2444 */
/* .local v0, "sparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Float;>;" */
/* if-nez v0, :cond_0 */
/* .line 2445 */
/* new-instance v1, Landroid/util/SparseArray; */
/* invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V */
/* move-object v0, v1 */
/* .line 2447 */
} // :cond_0
v1 = (( android.util.SparseArray ) v0 ).contains ( p4 ); // invoke-virtual {v0, p4}, Landroid/util/SparseArray;->contains(I)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2448 */
(( android.util.SparseArray ) v0 ).get ( p4 ); // invoke-virtual {v0, p4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* add-float/2addr p1, v1 */
/* .line 2450 */
} // :cond_1
java.lang.Float .valueOf ( p1 );
(( android.util.SparseArray ) v0 ).put ( p4, v1 ); // invoke-virtual {v0, p4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 2451 */
(( android.util.SparseArray ) p2 ).put ( p3, v0 ); // invoke-virtual {p2, p3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 2452 */
/* sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 2453 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "countThermalUsage: conditionId: "; // const-string v2, "countThermalUsage: conditionId: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", temperatureSpan: "; // const-string v2, ", temperatureSpan: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p4 ); // invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", duration: "; // const-string v2, ", duration: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BrightnessDataProcessor"; // const-string v2, "BrightnessDataProcessor"
android.util.Slog .d ( v2,v1 );
/* .line 2457 */
} // :cond_2
return;
} // .end method
private com.xiaomi.aiautobrt.IndividualModelEvent createModelEvent ( com.android.server.display.statistics.BrightnessDataProcessor$BrightnessChangeItem p0 ) {
/* .locals 6 */
/* .param p1, "item" # Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem; */
/* .line 708 */
/* iget v1, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F */
v2 = this.packageName;
/* iget v3, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F */
/* iget v4, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->orientation:I */
/* iget v5, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->sceneState:I */
/* move-object v0, p0 */
/* invoke-virtual/range {v0 ..v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->createModelEvent(FLjava/lang/String;FII)Lcom/xiaomi/aiautobrt/IndividualModelEvent; */
} // .end method
private void debounceBrightnessEvent ( Long p0 ) {
/* .locals 2 */
/* .param p1, "debounceTime" # J */
/* .line 547 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mForcedReportTrainDataEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* const-wide/16 v0, 0x0 */
} // :cond_0
/* move-wide v0, p1 */
} // :goto_0
/* move-wide p1, v0 */
/* .line 548 */
v0 = this.mBackgroundHandler;
int v1 = 3; // const/4 v1, 0x3
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 549 */
v0 = this.mBackgroundHandler;
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 550 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mBackgroundHandler;
(( android.os.Handler ) v1 ).sendMessageDelayed ( v0, p1, p2 ); // invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 551 */
return;
} // .end method
private void flareStatisticalManualAdjustTimes ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "isIncrease" # Z */
/* .line 3163 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 3164 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "manual adjust brightness: mIsAonSuppressDarken: "; // const-string v1, "manual adjust brightness: mIsAonSuppressDarken: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", mIsNotAonSuppressDarken: "; // const-string v1, ", mIsNotAonSuppressDarken: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", isIncrease:"; // const-string v1, ", isIncrease:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .i ( v1,v0 );
/* .line 3168 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z */
int v1 = 1; // const/4 v1, 0x1
final String v2 = "flare_manual_adjust_times"; // const-string v2, "flare_manual_adjust_times"
int v3 = 0; // const/4 v3, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 3169 */
/* iput-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z */
/* .line 3170 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 3171 */
final String v0 = "2"; // const-string v0, "2"
/* .line 3172 */
} // :cond_1
final String v0 = "1"; // const-string v0, "1"
} // :goto_0
java.lang.Integer .valueOf ( v1 );
/* .line 3170 */
/* invoke-direct {p0, v2, v0, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 3174 */
} // :cond_2
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 3175 */
/* iput-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z */
/* .line 3176 */
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 3177 */
final String v0 = "4"; // const-string v0, "4"
/* .line 3178 */
} // :cond_3
final String v0 = "3"; // const-string v0, "3"
} // :goto_1
java.lang.Integer .valueOf ( v1 );
/* .line 3176 */
/* invoke-direct {p0, v2, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 3180 */
} // :cond_4
return;
} // .end method
private void flareStatisticalResetBrightnessModeTimes ( ) {
/* .locals 5 */
/* .line 3186 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 3187 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "reset brightness mode: mIsAonSuppressDarken: "; // const-string v1, "reset brightness mode: mIsAonSuppressDarken: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", mIsNotAonSuppressDarken: "; // const-string v1, ", mIsNotAonSuppressDarken: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .i ( v1,v0 );
/* .line 3191 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z */
int v1 = 1; // const/4 v1, 0x1
final String v2 = "flare_user_reset_brightness_mode_times"; // const-string v2, "flare_user_reset_brightness_mode_times"
int v3 = 0; // const/4 v3, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 3192 */
/* iput-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z */
/* .line 3193 */
/* nop */
/* .line 3194 */
java.lang.Integer .valueOf ( v1 );
/* .line 3193 */
final String v4 = "1"; // const-string v4, "1"
/* invoke-direct {p0, v2, v4, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 3196 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 3197 */
/* iput-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z */
/* .line 3198 */
/* nop */
/* .line 3199 */
java.lang.Integer .valueOf ( v1 );
/* .line 3198 */
final String v1 = "2"; // const-string v1, "2"
/* invoke-direct {p0, v2, v1, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 3201 */
} // :cond_2
return;
} // .end method
private Integer getAmbientLuxSpanIndex ( Float p0 ) {
/* .locals 4 */
/* .param p1, "lux" # F */
/* .line 986 */
int v0 = 0; // const/4 v0, 0x0
/* .line 987 */
/* .local v0, "index":I */
/* const/high16 v1, 0x41f00000 # 30.0f */
/* cmpg-float v1, p1, v1 */
/* if-gez v1, :cond_0 */
/* .line 988 */
/* const/high16 v1, 0x40a00000 # 5.0f */
/* div-float v1, p1, v1 */
/* float-to-int v0, v1 */
/* .line 989 */
} // :cond_0
/* const/high16 v1, 0x44610000 # 900.0f */
/* cmpg-float v2, p1, v1 */
/* if-gez v2, :cond_1 */
/* .line 990 */
/* const/high16 v1, 0x42c80000 # 100.0f */
/* div-float v1, p1, v1 */
/* const/high16 v2, 0x40c00000 # 6.0f */
/* add-float/2addr v1, v2 */
/* float-to-int v0, v1 */
/* .line 991 */
} // :cond_1
/* const v2, 0x455ac000 # 3500.0f */
/* cmpg-float v3, p1, v2 */
/* if-gez v3, :cond_2 */
/* .line 993 */
/* sub-float v1, p1, v1 */
/* const/high16 v2, 0x43480000 # 200.0f */
/* div-float/2addr v1, v2 */
/* const/high16 v2, 0x41700000 # 15.0f */
/* add-float/2addr v1, v2 */
/* float-to-int v0, v1 */
/* .line 994 */
} // :cond_2
/* const v1, 0x461c4000 # 10000.0f */
/* cmpg-float v1, p1, v1 */
/* if-gez v1, :cond_3 */
/* .line 996 */
/* sub-float v1, p1, v2 */
/* const/high16 v2, 0x43fa0000 # 500.0f */
/* div-float/2addr v1, v2 */
/* const/high16 v2, 0x41e00000 # 28.0f */
/* add-float/2addr v1, v2 */
/* float-to-int v0, v1 */
/* .line 997 */
} // :cond_3
/* const v1, 0x466a6000 # 15000.0f */
/* cmpg-float v1, p1, v1 */
/* if-gez v1, :cond_4 */
/* .line 998 */
/* const/16 v0, 0x29 */
/* .line 999 */
} // :cond_4
/* const v1, 0x4708b800 # 35000.0f */
/* cmpg-float v1, p1, v1 */
/* if-gez v1, :cond_5 */
/* .line 1000 */
/* const/16 v0, 0x2a */
/* .line 1001 */
} // :cond_5
/* const v1, 0x477de800 # 65000.0f */
/* cmpg-float v1, p1, v1 */
/* if-gez v1, :cond_6 */
/* .line 1002 */
/* const/16 v0, 0x2b */
/* .line 1003 */
} // :cond_6
/* const v1, 0x47c35000 # 100000.0f */
/* cmpg-float v1, p1, v1 */
/* if-gez v1, :cond_7 */
/* .line 1004 */
/* const/16 v0, 0x2c */
/* .line 1006 */
} // :cond_7
/* const/16 v0, 0x2c */
/* .line 1008 */
} // :goto_0
/* sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 1009 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "lux = "; // const-string v2, "lux = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", index = "; // const-string v2, ", index = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BrightnessDataProcessor"; // const-string v2, "BrightnessDataProcessor"
android.util.Slog .d ( v2,v1 );
/* .line 1011 */
} // :cond_8
} // .end method
private com.android.server.display.statistics.AggregationEvent getBrightnessAggregationEvent ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "keySubEvent" # Ljava/lang/String; */
/* .line 1903 */
v0 = this.mBrightnessEventsMap;
/* check-cast v0, Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 1904 */
/* .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent; */
/* if-nez v0, :cond_0 */
/* .line 1905 */
/* new-instance v1, Lcom/android/server/display/statistics/AggregationEvent$BrightnessAggregationEvent; */
/* invoke-direct {v1}, Lcom/android/server/display/statistics/AggregationEvent$BrightnessAggregationEvent;-><init>()V */
/* move-object v0, v1 */
/* .line 1907 */
} // :cond_0
} // .end method
private Integer getBrightnessChangedState ( Float p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "currentValue" # F */
/* .param p2, "targetValue" # F */
/* .line 1720 */
int v0 = 0; // const/4 v0, 0x0
/* cmpl-float v1, p1, v0 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* cmpl-float v0, p2, v0 */
/* if-nez v0, :cond_0 */
/* .line 1723 */
} // :cond_0
/* cmpl-float v0, p2, p1 */
/* if-lez v0, :cond_1 */
/* .line 1724 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1725 */
} // :cond_1
/* cmpg-float v0, p2, p1 */
/* if-gez v0, :cond_2 */
/* .line 1726 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1728 */
} // :cond_2
int v0 = 2; // const/4 v0, 0x2
/* .line 1721 */
} // :cond_3
} // :goto_0
int v0 = 3; // const/4 v0, 0x3
} // .end method
private Integer getBrightnessSpanByNit ( Float p0 ) {
/* .locals 6 */
/* .param p1, "brightness" # F */
/* .line 1015 */
v0 = this.mDisplayDeviceConfig;
v0 = (( com.android.server.display.DisplayDeviceConfig ) v0 ).getNitsFromBacklight ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
v0 = java.lang.Math .round ( v0 );
/* int-to-float v0, v0 */
/* .line 1017 */
/* .local v0, "screenNit":F */
int v1 = 0; // const/4 v1, 0x0
/* .line 1018 */
/* .local v1, "index":I */
/* const/high16 v2, 0x40600000 # 3.5f */
/* cmpl-float v2, v0, v2 */
/* const/high16 v3, 0x41000000 # 8.0f */
/* if-ltz v2, :cond_0 */
/* cmpg-float v2, v0, v3 */
/* if-gez v2, :cond_0 */
/* .line 1019 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1020 */
} // :cond_0
/* cmpl-float v2, v0, v3 */
/* const/high16 v4, 0x42480000 # 50.0f */
/* if-ltz v2, :cond_1 */
/* cmpg-float v2, v0, v4 */
/* if-gez v2, :cond_1 */
/* .line 1021 */
/* sub-float v2, v0, v3 */
/* const/high16 v3, 0x40e00000 # 7.0f */
/* div-float/2addr v2, v3 */
/* float-to-int v2, v2 */
/* add-int/lit8 v1, v2, 0x2 */
/* .line 1022 */
} // :cond_1
/* cmpl-float v2, v0, v4 */
/* const/high16 v3, 0x42a00000 # 80.0f */
/* if-ltz v2, :cond_2 */
/* cmpg-float v2, v0, v3 */
/* if-gez v2, :cond_2 */
/* .line 1023 */
/* sub-float v2, v0, v4 */
/* const/high16 v3, 0x41200000 # 10.0f */
/* div-float/2addr v2, v3 */
/* float-to-int v2, v2 */
/* add-int/lit8 v2, v2, 0x2 */
/* add-int/lit8 v1, v2, 0x6 */
/* .line 1024 */
} // :cond_2
/* cmpl-float v2, v0, v3 */
/* const/high16 v5, 0x43fa0000 # 500.0f */
/* if-ltz v2, :cond_3 */
/* cmpg-float v2, v0, v5 */
/* if-gez v2, :cond_3 */
/* .line 1025 */
/* sub-float v2, v0, v3 */
/* const/high16 v3, 0x41a00000 # 20.0f */
/* div-float/2addr v2, v3 */
/* float-to-int v2, v2 */
/* add-int/lit8 v2, v2, 0x2 */
/* add-int/lit8 v1, v2, 0x9 */
/* .line 1026 */
} // :cond_3
/* cmpl-float v2, v0, v5 */
/* const/high16 v3, 0x447a0000 # 1000.0f */
/* if-ltz v2, :cond_4 */
/* cmpg-float v2, v0, v3 */
/* if-gez v2, :cond_4 */
/* .line 1027 */
/* sub-float v2, v0, v5 */
/* div-float/2addr v2, v4 */
/* float-to-int v2, v2 */
/* add-int/lit8 v2, v2, 0x2 */
/* add-int/lit8 v1, v2, 0x1e */
/* .line 1028 */
} // :cond_4
/* cmpl-float v2, v0, v3 */
/* if-lez v2, :cond_5 */
/* .line 1029 */
/* const/16 v1, 0x2a */
/* .line 1031 */
} // :cond_5
} // :goto_0
/* sget-boolean v2, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 1032 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "brightness = "; // const-string v3, "brightness = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = ", screenNit = "; // const-string v3, ", screenNit = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = ", index = "; // const-string v3, ", index = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "BrightnessDataProcessor"; // const-string v3, "BrightnessDataProcessor"
android.util.Slog .d ( v3,v2 );
/* .line 1034 */
} // :cond_6
} // .end method
private Integer getBrightnessType ( Boolean p0, Boolean p1, Float p2, Boolean p3 ) {
/* .locals 2 */
/* .param p1, "userInitiatedChange" # Z */
/* .param p2, "useAutoBrightness" # Z */
/* .param p3, "brightnessOverrideFromWindow" # F */
/* .param p4, "sunlightActive" # Z */
/* .line 963 */
int v0 = 0; // const/4 v0, 0x0
/* .line 965 */
/* .local v0, "type":I */
v1 = java.lang.Float .isNaN ( p3 );
/* if-nez v1, :cond_0 */
/* .line 966 */
int v0 = 3; // const/4 v0, 0x3
/* .line 967 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 968 */
int v0 = 2; // const/4 v0, 0x2
/* .line 969 */
} // :cond_1
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 970 */
int v0 = 1; // const/4 v0, 0x1
/* .line 971 */
} // :cond_2
if ( p4 != null) { // if-eqz p4, :cond_3
/* .line 972 */
int v0 = 4; // const/4 v0, 0x4
/* .line 974 */
} // :cond_3
} // :goto_0
} // .end method
private com.android.server.display.statistics.AggregationEvent getCbmAggregationEvent ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "keySubEvent" # Ljava/lang/String; */
/* .line 2917 */
v0 = this.mCbmEventsMap;
/* check-cast v0, Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 2918 */
/* .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent; */
/* if-nez v0, :cond_0 */
/* .line 2919 */
/* new-instance v1, Lcom/android/server/display/statistics/AggregationEvent$CbmAggregationEvent; */
/* invoke-direct {v1}, Lcom/android/server/display/statistics/AggregationEvent$CbmAggregationEvent;-><init>()V */
/* move-object v0, v1 */
/* .line 2921 */
} // :cond_0
} // .end method
private Float getConfigBrightness ( Float p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "lux" # F */
/* .param p2, "sceneState" # I */
/* .line 753 */
int v0 = -1; // const/4 v0, -0x1
/* if-ne p2, v0, :cond_0 */
v0 = this.mOriSpline;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 754 */
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
/* .line 755 */
} // :cond_0
/* if-nez p2, :cond_1 */
v0 = this.mDefaultSceneSpline;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 756 */
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
/* .line 757 */
} // :cond_1
int v0 = 2; // const/4 v0, 0x2
/* if-ne p2, v0, :cond_2 */
v0 = this.mDarkeningSceneSpline;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 758 */
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
/* .line 759 */
} // :cond_2
int v0 = 1; // const/4 v0, 0x1
/* if-ne p2, v0, :cond_3 */
v0 = this.mBrighteningSceneSpline;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 760 */
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
/* .line 762 */
} // :cond_3
/* const/high16 v0, 0x7fc00000 # Float.NaN */
} // .end method
private Integer getHourFromTimestamp ( Long p0 ) {
/* .locals 2 */
/* .param p1, "timestamp" # J */
/* .line 3154 */
java.util.Calendar .getInstance ( );
/* .line 3155 */
/* .local v0, "calendar":Ljava/util/Calendar; */
(( java.util.Calendar ) v0 ).setTimeInMillis ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V
/* .line 3156 */
/* const/16 v1, 0xb */
v1 = (( java.util.Calendar ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I
} // .end method
private android.util.Spline getSpline ( android.hardware.display.BrightnessConfiguration p0 ) {
/* .locals 4 */
/* .param p1, "config" # Landroid/hardware/display/BrightnessConfiguration; */
/* .line 1774 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1775 */
/* .local v0, "spline":Landroid/util/Spline; */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1776 */
(( android.hardware.display.BrightnessConfiguration ) p1 ).getCurve ( ); // invoke-virtual {p1}, Landroid/hardware/display/BrightnessConfiguration;->getCurve()Landroid/util/Pair;
/* .line 1777 */
/* .local v1, "defaultCurve":Landroid/util/Pair;, "Landroid/util/Pair<[F[F>;" */
v2 = this.first;
/* check-cast v2, [F */
v3 = this.second;
/* check-cast v3, [F */
android.util.Spline .createSpline ( v2,v3 );
/* .line 1779 */
} // .end local v1 # "defaultCurve":Landroid/util/Pair;, "Landroid/util/Pair<[F[F>;"
} // :cond_0
} // .end method
private java.lang.Object getSumValues ( java.lang.Object p0, java.lang.Object p1 ) {
/* .locals 4 */
/* .param p1, "value" # Ljava/lang/Object; */
/* .param p2, "increment" # Ljava/lang/Object; */
/* .line 1931 */
/* instance-of v0, p1, Ljava/lang/Integer; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* instance-of v0, p2, Ljava/lang/Integer; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1932 */
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* move-object v1, p2 */
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* add-int/2addr v0, v1 */
java.lang.Integer .valueOf ( v0 );
/* .line 1934 */
} // :cond_0
/* instance-of v0, p1, Ljava/lang/Long; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* instance-of v0, p2, Ljava/lang/Long; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1935 */
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/Long; */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v0 */
/* move-object v2, p2 */
/* check-cast v2, Ljava/lang/Long; */
(( java.lang.Long ) v2 ).longValue ( ); // invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
/* move-result-wide v2 */
/* add-long/2addr v0, v2 */
java.lang.Long .valueOf ( v0,v1 );
/* .line 1937 */
} // :cond_1
/* instance-of v0, p1, Ljava/lang/Float; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* instance-of v0, p2, Ljava/lang/Float; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1938 */
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* move-object v1, p2 */
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* add-float/2addr v0, v1 */
java.lang.Float .valueOf ( v0 );
/* .line 1940 */
} // :cond_2
} // .end method
private Integer getTemperatureSpan ( Float p0 ) {
/* .locals 4 */
/* .param p1, "temperature" # F */
/* .line 1093 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1094 */
/* .local v0, "temperatureSpan":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 1095 */
/* .local v1, "index":I */
v2 = java.lang.Float .isNaN ( p1 );
/* if-nez v2, :cond_3 */
/* .line 1096 */
} // :goto_0
v2 = v2 = this.mTemperatureSpan;
/* if-ge v1, v2, :cond_3 */
/* .line 1097 */
v2 = v2 = this.mTemperatureSpan;
/* add-int/lit8 v2, v2, -0x1 */
/* if-ne v1, v2, :cond_0 */
v2 = this.mTemperatureSpan;
/* .line 1098 */
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* cmpl-float v2, p1, v2 */
/* if-nez v2, :cond_0 */
/* .line 1099 */
/* move v0, v1 */
/* .line 1100 */
/* .line 1101 */
} // :cond_0
v2 = v2 = this.mTemperatureSpan;
/* add-int/lit8 v2, v2, -0x1 */
/* if-ne v1, v2, :cond_1 */
v2 = this.mTemperatureSpan;
/* .line 1102 */
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* cmpl-float v2, p1, v2 */
/* if-lez v2, :cond_1 */
/* .line 1103 */
/* add-int/lit8 v0, v1, 0x1 */
/* .line 1104 */
/* .line 1105 */
} // :cond_1
v2 = this.mTemperatureSpan;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* cmpg-float v2, v2, p1 */
/* if-gtz v2, :cond_2 */
v2 = this.mTemperatureSpan;
/* add-int/lit8 v3, v1, 0x1 */
/* .line 1106 */
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* cmpg-float v2, p1, v2 */
/* if-gez v2, :cond_2 */
/* .line 1107 */
/* move v0, v1 */
/* .line 1108 */
/* .line 1110 */
} // :cond_2
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1113 */
} // :cond_3
} // :goto_1
} // .end method
private com.android.server.display.statistics.AggregationEvent getThermalAggregationEvent ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "keySubEvent" # Ljava/lang/String; */
/* .line 2024 */
v0 = this.mThermalEventsMap;
/* check-cast v0, Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 2025 */
/* .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent; */
/* if-nez v0, :cond_0 */
/* .line 2026 */
/* new-instance v1, Lcom/android/server/display/statistics/AggregationEvent$ThermalAggregationEvent; */
/* invoke-direct {v1}, Lcom/android/server/display/statistics/AggregationEvent$ThermalAggregationEvent;-><init>()V */
/* move-object v0, v1 */
/* .line 2028 */
} // :cond_0
} // .end method
private void handleAccSensor ( android.hardware.SensorEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 2180 */
v0 = this.mPendingBrightnessChangeItem;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
v0 = this.values;
/* array-length v0, v0 */
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_0 */
/* .line 2183 */
v0 = this.mPendingBrightnessChangeItem;
v1 = this.values;
this.accValues = v1;
/* .line 2184 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerAccSensor(Z)V */
/* .line 2186 */
} // :cond_0
return;
} // .end method
private void handleBrightnessChangeEvent ( com.android.server.display.statistics.BrightnessDataProcessor$BrightnessChangeItem p0 ) {
/* .locals 10 */
/* .param p1, "item" # Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem; */
/* .line 511 */
/* iget v0, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->brightnessOverrideFromWindow:F */
v0 = java.lang.Float .isNaN ( v0 );
int v1 = 1; // const/4 v1, 0x1
/* xor-int/2addr v0, v1 */
/* .line 512 */
/* .local v0, "windowOverrideApplying":Z */
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v3, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->brightnessOverrideFromWindow:F */
/* iget v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessOverrideFromWindow:F */
/* cmpl-float v3, v3, v4 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* move v3, v1 */
} // :cond_0
/* move v3, v2 */
/* .line 514 */
/* .local v3, "windowOverrideChanging":Z */
} // :goto_0
/* iget-boolean v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mWindowOverrideBrightnessChanging:Z */
/* if-eq v3, v4, :cond_1 */
/* .line 515 */
/* iput-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mWindowOverrideBrightnessChanging:Z */
/* .line 518 */
} // :cond_1
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 519 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v4 */
/* .line 521 */
/* .local v4, "now":J */
/* iget-boolean v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mUserDragging:Z */
/* if-nez v6, :cond_2 */
/* iget-boolean v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHaveValidMotionForWindowBrightness:Z */
if ( v6 != null) { // if-eqz v6, :cond_3
/* iget-wide v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLatestDraggingChangedTime:J */
/* sub-long v6, v4, v6 */
/* const-wide/16 v8, 0x32 */
/* cmp-long v6, v6, v8 */
/* if-gez v6, :cond_3 */
/* .line 523 */
} // :cond_2
/* iget v6, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F */
/* iput v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessOverrideFromWindow:F */
/* .line 524 */
/* iput-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHaveValidWindowBrightness:Z */
/* .line 525 */
/* sget-boolean v6, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 526 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Brightness from window is changing: "; // const-string v7, "Brightness from window is changing: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v7 = "BrightnessDataProcessor"; // const-string v7, "BrightnessDataProcessor"
android.util.Slog .d ( v7,v6 );
/* .line 530 */
} // .end local v4 # "now":J
} // :cond_3
if ( v0 != null) { // if-eqz v0, :cond_4
/* iget-boolean v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHaveValidWindowBrightness:Z */
/* if-nez v4, :cond_4 */
/* .line 531 */
return;
/* .line 534 */
} // :cond_4
v4 = this.mPendingBrightnessChangeItem;
if ( v4 != null) { // if-eqz v4, :cond_5
/* iget v4, v4, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
/* iget v5, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
/* if-eq v4, v5, :cond_5 */
/* .line 535 */
v4 = this.mPendingBrightnessChangeItem;
/* iget v4, v4, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
/* invoke-direct {p0, v4, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerSensorByBrightnessType(IZ)V */
/* .line 537 */
} // :cond_5
this.mPendingBrightnessChangeItem = p1;
/* .line 539 */
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mReportBrightnessEventsEnable:Z */
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 540 */
/* iget v2, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
/* invoke-direct {p0, v2, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerSensorByBrightnessType(IZ)V */
/* .line 543 */
} // :cond_6
/* const-wide/16 v1, 0xbb8 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->debounceBrightnessEvent(J)V */
/* .line 544 */
return;
} // .end method
private void handleLightFovSensor ( android.hardware.SensorEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 2193 */
v0 = this.mPendingBrightnessChangeItem;
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v0, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_1 */
/* .line 2195 */
v0 = this.mPendingBrightnessChangeItem;
v1 = this.values;
int v2 = 1; // const/4 v2, 0x1
/* aget v1, v1, v2 */
/* float-to-int v1, v1 */
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
} // :cond_0
/* move v2, v3 */
} // :goto_0
/* iput-boolean v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->isUseLightFovOptimization:Z */
/* .line 2196 */
/* invoke-direct {p0, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerLightFovSensor(Z)V */
/* .line 2198 */
} // :cond_1
return;
} // .end method
private void handleLightSensor ( android.hardware.SensorEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 2166 */
v0 = this.mPendingBrightnessChangeItem;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2167 */
/* iget v0, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
/* .line 2168 */
/* .local v0, "type":I */
/* if-nez v0, :cond_0 */
/* .line 2169 */
v1 = this.mPendingBrightnessChangeItem;
v2 = this.values;
int v3 = 0; // const/4 v3, 0x0
/* aget v2, v2, v3 */
/* iput v2, v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F */
/* .line 2170 */
/* invoke-direct {p0, v0, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerSensorByBrightnessType(IZ)V */
/* .line 2173 */
} // .end local v0 # "type":I
} // :cond_0
return;
} // .end method
private Boolean isValidStartTimeStamp ( ) {
/* .locals 4 */
/* .line 979 */
/* iget-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mStartTimeStamp:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private void lambda$aggregateIndividualModelTrainTimes$10 ( Boolean p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "isSuccessful" # Z */
/* .line 3004 */
if ( p1 != null) { // if-eqz p1, :cond_0
final String v0 = "model_validation_success_count"; // const-string v0, "model_validation_success_count"
/* .line 3005 */
} // :cond_0
final String v0 = "model_validation_fail_count"; // const-string v0, "model_validation_fail_count"
} // :goto_0
/* nop */
/* .line 3006 */
/* .local v0, "name":Ljava/lang/String; */
int v1 = 1; // const/4 v1, 0x1
java.lang.Integer .valueOf ( v1 );
final String v2 = "individual_model_train"; // const-string v2, "individual_model_train"
/* invoke-direct {p0, v2, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 3007 */
/* sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 3008 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "aggregateIndividualModelTrainTimes: isSuccessful: "; // const-string v2, "aggregateIndividualModelTrainTimes: isSuccessful: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", mCbmEventsMap: "; // const-string v2, ", mCbmEventsMap: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mCbmEventsMap;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BrightnessDataProcessor"; // const-string v2, "BrightnessDataProcessor"
android.util.Slog .d ( v2,v1 );
/* .line 3011 */
} // :cond_1
return;
} // .end method
private void lambda$aggregateModelPredictTimeoutTimes$11 ( ) { //synthethic
/* .locals 3 */
/* .line 3019 */
/* nop */
/* .line 3020 */
int v0 = 1; // const/4 v0, 0x1
java.lang.Integer .valueOf ( v0 );
/* .line 3019 */
final String v1 = "individual_model_predict"; // const-string v1, "individual_model_predict"
final String v2 = "model_predict_timeout_count"; // const-string v2, "model_predict_timeout_count"
/* invoke-direct {p0, v1, v2, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 3021 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 3022 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "aggregateModelPredictTimeoutTimes: mCbmEventsMap: "; // const-string v1, "aggregateModelPredictTimeoutTimes: mCbmEventsMap: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCbmEventsMap;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 3024 */
} // :cond_0
return;
} // .end method
private void lambda$forceReportTrainDataEnabled$9 ( Boolean p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 2908 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mForcedReportTrainDataEnabled:Z */
return;
} // .end method
private void lambda$new$0 ( ) { //synthethic
/* .locals 1 */
/* .line 442 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportScheduleEvent()V */
/* .line 443 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setReportScheduleEventAlarm(Z)V */
/* .line 444 */
return;
} // .end method
private void lambda$noteAverageTemperature$6 ( Boolean p0, Float p1 ) { //synthethic
/* .locals 3 */
/* .param p1, "needComputed" # Z */
/* .param p2, "temperature" # F */
/* .line 2574 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mScreenOn:Z */
/* if-nez v0, :cond_0 */
/* .line 2575 */
return;
/* .line 2577 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 2578 */
final String v0 = "average"; // const-string v0, "average"
java.lang.Float .valueOf ( p2 );
/* const-string/jumbo v2, "thermal_average_temperature" */
/* invoke-direct {p0, v2, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsAvg(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2579 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2580 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "noteAverageTemperature: mThermalEventsMap: "; // const-string v1, "noteAverageTemperature: mThermalEventsMap: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mThermalEventsMap;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 2583 */
} // :cond_1
return;
} // .end method
private void lambda$noteDetailThermalUsage$4 ( Integer p0, Float p1 ) { //synthethic
/* .locals 13 */
/* .param p1, "conditionId" # I */
/* .param p2, "temperature" # F */
/* .line 2331 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mScreenOn:Z */
/* if-nez v0, :cond_0 */
/* .line 2332 */
return;
/* .line 2334 */
} // :cond_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 2336 */
/* .local v0, "now":J */
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
final String v3 = "restricted_usage"; // const-string v3, "restricted_usage"
/* const-string/jumbo v4, "thermal_detail_restricted_usage" */
int v5 = 1; // const/4 v5, 0x1
/* const-string/jumbo v6, "unrestricted_usage" */
/* const-string/jumbo v7, "thermal_detail_unrestricted_usage" */
int v8 = 3; // const/4 v8, 0x3
int v9 = 2; // const/4 v9, 0x2
/* const/high16 v10, 0x447a0000 # 1000.0f */
/* if-eq v2, p1, :cond_3 */
/* .line 2337 */
/* iget v11, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
/* if-ne v11, v9, :cond_1 */
/* .line 2338 */
/* iget-wide v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* sub-long v3, v0, v3 */
/* long-to-float v3, v3 */
/* div-float/2addr v3, v10 */
/* .line 2339 */
/* .local v3, "duration":F */
v4 = this.mDetailThermalUnrestrictedUsage;
/* iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* .line 2340 */
v5 = /* invoke-direct {p0, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I */
/* .line 2339 */
/* invoke-direct {p0, v3, v4, v2, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V */
/* .line 2341 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
/* .line 2342 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2343 */
v2 = this.mDetailThermalUnrestrictedUsage;
/* invoke-direct {p0, v7, v6, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2345 */
} // .end local v3 # "duration":F
} // :cond_1
/* if-ne v11, v8, :cond_2 */
/* .line 2346 */
/* iget-wide v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* sub-long v5, v0, v5 */
/* long-to-float v5, v5 */
/* div-float/2addr v5, v10 */
/* .line 2347 */
/* .local v5, "duration":F */
v6 = this.mDetailThermalRestrictedUsage;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* .line 2348 */
v7 = /* invoke-direct {p0, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I */
/* .line 2347 */
/* invoke-direct {p0, v5, v6, v2, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V */
/* .line 2349 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
/* .line 2350 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2351 */
v2 = this.mDetailThermalRestrictedUsage;
/* invoke-direct {p0, v4, v3, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2353 */
} // .end local v5 # "duration":F
} // :cond_2
/* if-ne v11, v5, :cond_6 */
/* .line 2354 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
/* .line 2355 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2357 */
} // :cond_3
/* iget v11, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* cmpl-float v12, v11, p2 */
if ( v12 != null) { // if-eqz v12, :cond_6
/* .line 2358 */
/* iget v12, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
/* if-ne v12, v9, :cond_4 */
/* .line 2359 */
/* iget-wide v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* sub-long v3, v0, v3 */
/* long-to-float v3, v3 */
/* div-float/2addr v3, v10 */
/* .line 2360 */
/* .restart local v3 # "duration":F */
v4 = this.mDetailThermalUnrestrictedUsage;
/* .line 2361 */
v5 = /* invoke-direct {p0, v11}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I */
/* .line 2360 */
/* invoke-direct {p0, v3, v4, v2, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V */
/* .line 2362 */
/* iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* .line 2363 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2364 */
v2 = this.mDetailThermalUnrestrictedUsage;
/* invoke-direct {p0, v7, v6, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2366 */
} // .end local v3 # "duration":F
} // :cond_4
/* if-ne v12, v8, :cond_5 */
/* .line 2367 */
/* iget-wide v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* sub-long v5, v0, v5 */
/* long-to-float v5, v5 */
/* div-float/2addr v5, v10 */
/* .line 2368 */
/* .restart local v5 # "duration":F */
v6 = this.mDetailThermalRestrictedUsage;
/* .line 2369 */
v7 = /* invoke-direct {p0, v11}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I */
/* .line 2368 */
/* invoke-direct {p0, v5, v6, v2, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V */
/* .line 2370 */
/* iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* .line 2371 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2372 */
v2 = this.mDetailThermalRestrictedUsage;
/* invoke-direct {p0, v4, v3, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2374 */
} // .end local v5 # "duration":F
} // :cond_5
/* if-ne v12, v5, :cond_6 */
/* .line 2375 */
/* iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* .line 2376 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2379 */
} // :cond_6
} // :goto_0
/* sget-boolean v2, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 2380 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "noteDetailThermalUsage: thermal state changed, mThermalStatus: "; // const-string v3, "noteDetailThermalUsage: thermal state changed, mThermalStatus: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", conditionId: "; // const-string v3, ", conditionId: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", temperature: "; // const-string v3, ", temperature: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = ", mLastTemperature: "; // const-string v3, ", mLastTemperature: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = ", mThermalEventsMap: "; // const-string v3, ", mThermalEventsMap: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mThermalEventsMap;
/* .line 2385 */
(( java.lang.Object ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2380 */
final String v3 = "BrightnessDataProcessor"; // const-string v3, "BrightnessDataProcessor"
android.util.Slog .d ( v3,v2 );
/* .line 2387 */
} // :cond_7
return;
} // .end method
private void lambda$noteFullSceneThermalUsageStats$3 ( Float p0, Float p1, Boolean p2, Integer p3, Float p4 ) { //synthethic
/* .locals 10 */
/* .param p1, "thermalBrightness" # F */
/* .param p2, "brightness" # F */
/* .param p3, "outdoorHighTemState" # Z */
/* .param p4, "conditionId" # I */
/* .param p5, "temperature" # F */
/* .line 2079 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v7 */
/* .line 2080 */
/* .local v7, "now":J */
int v6 = 0; // const/4 v6, 0x0
/* move-object v0, p0 */
/* move v1, p1 */
/* move v2, p2 */
/* move-wide v3, v7 */
/* move v5, p3 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteOutDoorHighTempUsage(FFJZZ)V */
/* .line 2081 */
int v5 = 1; // const/4 v5, 0x1
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalBrightnessRestrictedUsage(FFJZ)V */
/* .line 2082 */
v0 = java.lang.Float .isNaN ( p1 );
int v9 = 1; // const/4 v9, 0x1
/* if-nez v0, :cond_1 */
/* .line 2083 */
/* cmpl-float v0, p2, p1 */
/* if-lez v0, :cond_0 */
/* .line 2084 */
int v1 = 3; // const/4 v1, 0x3
int v6 = 1; // const/4 v6, 0x1
/* move-object v0, p0 */
/* move v2, p4 */
/* move v3, p5 */
/* move-wide v4, v7 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteDetailThermalUsage(IIFJZ)V */
/* .line 2086 */
int v0 = 3; // const/4 v0, 0x3
/* invoke-direct {p0, v0, v7, v8, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalUsage(IJZ)V */
/* .line 2088 */
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
int v6 = 1; // const/4 v6, 0x1
/* move-object v0, p0 */
/* move v2, p4 */
/* move v3, p5 */
/* move-wide v4, v7 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteDetailThermalUsage(IIFJZ)V */
/* .line 2090 */
int v0 = 2; // const/4 v0, 0x2
/* invoke-direct {p0, v0, v7, v8, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalUsage(IJZ)V */
/* .line 2093 */
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
int v6 = 1; // const/4 v6, 0x1
/* move-object v0, p0 */
/* move v2, p4 */
/* move v3, p5 */
/* move-wide v4, v7 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteDetailThermalUsage(IIFJZ)V */
/* .line 2095 */
/* invoke-direct {p0, v9, v7, v8, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalUsage(IJZ)V */
/* .line 2097 */
} // :goto_0
return;
} // .end method
private void lambda$startHdyUsageStats$7 ( Boolean p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "isHdrLayer" # Z */
/* .line 2606 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteHdrUsage(Z)V */
/* .line 2607 */
return;
} // .end method
private void lambda$updateExpId$2 ( Integer p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "expId" # I */
/* .line 1790 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mExpId:I */
return;
} // .end method
private void lambda$updateGrayScale$1 ( Float p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "grayScale" # F */
/* .line 766 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mGrayScale:F */
return;
} // .end method
private void lambda$updateScreenNits$8 ( Float p0, Float p1 ) { //synthethic
/* .locals 2 */
/* .param p1, "originalNit" # F */
/* .param p2, "actualNit" # F */
/* .line 2877 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOriginalNit:F */
/* .line 2878 */
/* iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mActualNit:F */
/* .line 2879 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2880 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateScreenNits: mOriginalNit: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOriginalNit:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", mActualNit: "; // const-string v1, ", mActualNit: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mActualNit:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 2883 */
} // :cond_0
return;
} // .end method
private void lambda$updateThermalStats$5 ( Boolean p0, Float p1, Boolean p2, Float p3 ) { //synthethic
/* .locals 10 */
/* .param p1, "isScreenOn" # Z */
/* .param p2, "temperature" # F */
/* .param p3, "needComputed" # Z */
/* .param p4, "brightnessState" # F */
/* .line 2548 */
/* iget-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2549 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v8 */
/* .line 2550 */
/* .local v8, "now":J */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 2551 */
/* iput-wide v8, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J */
/* .line 2552 */
/* iput-wide v8, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2553 */
/* long-to-float v0, v8 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessRestrictedTimeStamp:F */
/* .line 2554 */
/* iput-wide v8, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J */
/* .line 2555 */
/* invoke-direct {p0, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteAverageTemperatureScreenOn(FZ)V */
/* .line 2557 */
} // :cond_0
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F */
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* move-object v1, p0 */
/* move v3, p4 */
/* move-wide v4, v8 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteOutDoorHighTempUsage(FFJZZ)V */
/* .line 2558 */
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F */
/* iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalBrightnessRestrictedUsage(FFJZ)V */
/* .line 2559 */
int v2 = 3; // const/4 v2, 0x3
/* iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
/* iget v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* move-wide v5, v8 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteDetailThermalUsage(IIFJZ)V */
/* .line 2561 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v0, v8, v9, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalUsage(IJZ)V */
/* .line 2564 */
} // .end local v8 # "now":J
} // :cond_1
} // :goto_0
return;
} // .end method
private void noteAdjHighTimesOnBrightnessRestricted ( Integer p0, Float p1 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .param p2, "brightness" # F */
/* .line 2679 */
int v0 = 1; // const/4 v0, 0x1
int v1 = 3; // const/4 v1, 0x3
if ( p1 != null) { // if-eqz p1, :cond_0
/* if-eq p1, v0, :cond_0 */
int v2 = 2; // const/4 v2, 0x2
/* if-eq p1, v2, :cond_0 */
/* if-eq p1, v1, :cond_0 */
int v2 = 4; // const/4 v2, 0x4
/* if-ne p1, v2, :cond_1 */
} // :cond_0
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
/* if-ne v2, v1, :cond_1 */
/* .line 2685 */
v1 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( p2 );
/* int-to-float v1, v1 */
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F */
/* cmpl-float v1, v1, v2 */
/* if-lez v1, :cond_1 */
/* .line 2686 */
java.lang.Integer .valueOf ( p1 );
java.lang.Integer .valueOf ( v0 );
/* const-string/jumbo v2, "thermal_brightness_restricted_adj_high_times" */
/* invoke-direct {p0, v2, v1, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2688 */
} // :cond_1
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 2689 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "noteAdjHighTimesOnBrightnessRestricted: type: "; // const-string v1, "noteAdjHighTimesOnBrightnessRestricted: type: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", brightness: "; // const-string v1, ", brightness: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 2690 */
v1 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( p2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mLastScreenBrightness: "; // const-string v1, ", mLastScreenBrightness: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", mThermalEventsMap: "; // const-string v1, ", mThermalEventsMap: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mThermalEventsMap;
/* .line 2692 */
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2689 */
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 2694 */
} // :cond_2
return;
} // .end method
private void noteAverageTemperatureScreenOn ( Float p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "temperature" # F */
/* .param p2, "needComputed" # Z */
/* .line 2592 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 2593 */
final String v0 = "average"; // const-string v0, "average"
java.lang.Float .valueOf ( p1 );
/* const-string/jumbo v2, "thermal_average_temperature" */
/* invoke-direct {p0, v2, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsAvg(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2594 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2595 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "noteAverageTemperature: mThermalEventsMap: "; // const-string v1, "noteAverageTemperature: mThermalEventsMap: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mThermalEventsMap;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 2598 */
} // :cond_0
return;
} // .end method
private void noteDetailThermalUsage ( Integer p0, Integer p1, Float p2, Long p3, Boolean p4 ) {
/* .locals 17 */
/* .param p1, "status" # I */
/* .param p2, "conditionId" # I */
/* .param p3, "temperature" # F */
/* .param p4, "now" # J */
/* .param p6, "isScreenOn" # Z */
/* .line 2225 */
/* move-object/from16 v0, p0 */
/* move/from16 v1, p1 */
/* move/from16 v2, p2 */
/* move/from16 v3, p3 */
/* move-wide/from16 v4, p4 */
/* iget-wide v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* const-wide/16 v8, 0x0 */
/* cmp-long v6, v6, v8 */
/* if-nez v6, :cond_0 */
/* .line 2226 */
/* iput v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
/* .line 2227 */
/* iput v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* .line 2228 */
/* iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2230 */
} // :cond_0
final String v6 = "restricted_usage"; // const-string v6, "restricted_usage"
/* const-string/jumbo v7, "thermal_detail_restricted_usage" */
/* const-string/jumbo v8, "unrestricted_usage" */
/* const-string/jumbo v9, "thermal_detail_unrestricted_usage" */
int v10 = 3; // const/4 v10, 0x3
int v11 = 2; // const/4 v11, 0x2
/* const/high16 v12, 0x447a0000 # 1000.0f */
if ( p6 != null) { // if-eqz p6, :cond_a
/* .line 2231 */
/* iget v13, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
int v14 = 1; // const/4 v14, 0x1
/* if-eq v13, v2, :cond_3 */
/* .line 2232 */
/* iget v15, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
/* if-ne v15, v11, :cond_1 */
/* .line 2233 */
/* iget-wide v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* sub-long v6, v4, v6 */
/* long-to-float v6, v6 */
/* div-float/2addr v6, v12 */
/* .line 2234 */
/* .local v6, "duration":F */
v7 = this.mDetailThermalUnrestrictedUsage;
/* iget v10, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* .line 2235 */
v10 = /* invoke-direct {v0, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I */
/* .line 2234 */
/* invoke-direct {v0, v6, v7, v13, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V */
/* .line 2236 */
/* iput v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
/* .line 2237 */
/* iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2238 */
v7 = this.mDetailThermalUnrestrictedUsage;
/* invoke-direct {v0, v9, v8, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* goto/16 :goto_0 */
/* .line 2240 */
} // .end local v6 # "duration":F
} // :cond_1
/* if-ne v15, v10, :cond_2 */
/* .line 2241 */
/* iget-wide v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* sub-long v8, v4, v8 */
/* long-to-float v8, v8 */
/* div-float/2addr v8, v12 */
/* .line 2242 */
/* .local v8, "duration":F */
v9 = this.mDetailThermalRestrictedUsage;
/* iget v10, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* .line 2243 */
v10 = /* invoke-direct {v0, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I */
/* .line 2242 */
/* invoke-direct {v0, v8, v9, v13, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V */
/* .line 2244 */
/* iput v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
/* .line 2245 */
/* iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2246 */
v9 = this.mDetailThermalRestrictedUsage;
/* invoke-direct {v0, v7, v6, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* goto/16 :goto_0 */
/* .line 2248 */
} // .end local v8 # "duration":F
} // :cond_2
/* if-ne v15, v14, :cond_c */
/* .line 2249 */
/* iput v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
/* .line 2250 */
/* iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* goto/16 :goto_0 */
/* .line 2252 */
} // :cond_3
/* iget v15, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* cmpl-float v16, v15, v3 */
if ( v16 != null) { // if-eqz v16, :cond_6
/* .line 2253 */
/* iget v14, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
/* if-ne v14, v11, :cond_4 */
/* .line 2254 */
/* iget-wide v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* sub-long v6, v4, v6 */
/* long-to-float v6, v6 */
/* div-float/2addr v6, v12 */
/* .line 2255 */
/* .restart local v6 # "duration":F */
v7 = this.mDetailThermalUnrestrictedUsage;
/* .line 2256 */
v10 = /* invoke-direct {v0, v15}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I */
/* .line 2255 */
/* invoke-direct {v0, v6, v7, v13, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V */
/* .line 2257 */
/* iput v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* .line 2258 */
/* iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2259 */
v7 = this.mDetailThermalUnrestrictedUsage;
/* invoke-direct {v0, v9, v8, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* goto/16 :goto_0 */
/* .line 2261 */
} // .end local v6 # "duration":F
} // :cond_4
/* if-ne v14, v10, :cond_5 */
/* .line 2262 */
/* iget-wide v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* sub-long v8, v4, v8 */
/* long-to-float v8, v8 */
/* div-float/2addr v8, v12 */
/* .line 2263 */
/* .restart local v8 # "duration":F */
v9 = this.mDetailThermalRestrictedUsage;
/* .line 2264 */
v10 = /* invoke-direct {v0, v15}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I */
/* .line 2263 */
/* invoke-direct {v0, v8, v9, v13, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V */
/* .line 2265 */
/* iput v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* .line 2266 */
/* iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2267 */
v9 = this.mDetailThermalRestrictedUsage;
/* invoke-direct {v0, v7, v6, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* goto/16 :goto_0 */
/* .line 2269 */
} // .end local v8 # "duration":F
} // :cond_5
int v6 = 1; // const/4 v6, 0x1
/* if-ne v14, v6, :cond_c */
/* .line 2270 */
/* iput v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* .line 2271 */
/* iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* goto/16 :goto_0 */
/* .line 2273 */
} // :cond_6
/* iget v14, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
/* if-eq v14, v1, :cond_9 */
/* .line 2274 */
/* if-ne v14, v11, :cond_7 */
/* .line 2275 */
/* iget-wide v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* sub-long v6, v4, v6 */
/* long-to-float v6, v6 */
/* div-float/2addr v6, v12 */
/* .line 2276 */
/* .restart local v6 # "duration":F */
v7 = this.mDetailThermalUnrestrictedUsage;
/* .line 2277 */
v10 = /* invoke-direct {v0, v15}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I */
/* .line 2276 */
/* invoke-direct {v0, v6, v7, v13, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V */
/* .line 2278 */
/* iput v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
/* .line 2279 */
/* iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2280 */
v7 = this.mDetailThermalUnrestrictedUsage;
/* invoke-direct {v0, v9, v8, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2282 */
} // .end local v6 # "duration":F
} // :cond_7
/* if-ne v14, v10, :cond_8 */
/* .line 2283 */
/* iget-wide v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* sub-long v8, v4, v8 */
/* long-to-float v8, v8 */
/* div-float/2addr v8, v12 */
/* .line 2284 */
/* .restart local v8 # "duration":F */
v9 = this.mDetailThermalRestrictedUsage;
/* .line 2285 */
v10 = /* invoke-direct {v0, v15}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I */
/* .line 2284 */
/* invoke-direct {v0, v8, v9, v13, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V */
/* .line 2286 */
/* iput v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
/* .line 2287 */
/* iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2288 */
v9 = this.mDetailThermalRestrictedUsage;
/* invoke-direct {v0, v7, v6, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2290 */
} // .end local v8 # "duration":F
} // :cond_8
int v6 = 1; // const/4 v6, 0x1
/* if-ne v14, v6, :cond_c */
/* .line 2291 */
/* iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2293 */
} // :cond_9
int v6 = 1; // const/4 v6, 0x1
/* if-ne v14, v6, :cond_c */
/* .line 2294 */
/* iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2297 */
} // :cond_a
/* iget v13, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
/* if-ne v13, v11, :cond_b */
/* .line 2298 */
/* iget-wide v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* sub-long v6, v4, v6 */
/* long-to-float v6, v6 */
/* div-float/2addr v6, v12 */
/* .line 2299 */
/* .restart local v6 # "duration":F */
v7 = this.mDetailThermalUnrestrictedUsage;
/* iget v10, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
/* iget v11, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* .line 2300 */
v11 = /* invoke-direct {v0, v11}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I */
/* .line 2299 */
/* invoke-direct {v0, v6, v7, v10, v11}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V */
/* .line 2301 */
/* iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2302 */
v7 = this.mDetailThermalUnrestrictedUsage;
/* invoke-direct {v0, v9, v8, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2304 */
} // .end local v6 # "duration":F
} // :cond_b
/* if-ne v13, v10, :cond_c */
/* .line 2305 */
/* iget-wide v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* sub-long v8, v4, v8 */
/* long-to-float v8, v8 */
/* div-float/2addr v8, v12 */
/* .line 2306 */
/* .restart local v8 # "duration":F */
v9 = this.mDetailThermalRestrictedUsage;
/* iget v10, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
/* iget v11, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
/* .line 2307 */
v11 = /* invoke-direct {v0, v11}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I */
/* .line 2306 */
/* invoke-direct {v0, v8, v9, v10, v11}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V */
/* .line 2308 */
/* iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J */
/* .line 2309 */
v9 = this.mDetailThermalRestrictedUsage;
/* invoke-direct {v0, v7, v6, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2313 */
} // .end local v8 # "duration":F
} // :cond_c
} // :goto_0
/* sget-boolean v6, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v6 != null) { // if-eqz v6, :cond_d
/* .line 2314 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "noteDetailThermalUsage: status: "; // const-string v7, "noteDetailThermalUsage: status: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ", mThermalStatus: "; // const-string v7, ", mThermalStatus: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ", conditionId: "; // const-string v7, ", conditionId: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ", temperature: "; // const-string v7, ", temperature: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v7 = ", mLastTemperature: "; // const-string v7, ", mLastTemperature: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v7 = ", mThermalEventsMap: "; // const-string v7, ", mThermalEventsMap: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mThermalEventsMap;
/* .line 2319 */
(( java.lang.Object ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2314 */
final String v7 = "BrightnessDataProcessor"; // const-string v7, "BrightnessDataProcessor"
android.util.Slog .d ( v7,v6 );
/* .line 2321 */
} // :cond_d
return;
} // .end method
private void noteHbmUsage ( Float p0, Float p1 ) {
/* .locals 6 */
/* .param p1, "spanIndex" # F */
/* .param p2, "duration" # F */
/* .line 2650 */
/* const/high16 v0, 0x7fc00000 # Float.NaN */
/* .line 2651 */
/* .local v0, "transitionBrightness":F */
v1 = this.mDisplayDeviceConfig;
(( com.android.server.display.DisplayDeviceConfig ) v1 ).getBrightness ( ); // invoke-virtual {v1}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightness()[F
/* .line 2652 */
/* .local v1, "brightness":[F */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2653 */
/* array-length v2, v1 */
int v3 = 2; // const/4 v3, 0x2
/* if-le v2, v3, :cond_0 */
/* .line 2654 */
/* array-length v2, v1 */
/* sub-int/2addr v2, v3 */
/* aget v0, v1, v2 */
/* .line 2656 */
} // :cond_0
/* array-length v2, v1 */
/* add-int/lit8 v2, v2, -0x1 */
/* aget v0, v1, v2 */
/* .line 2659 */
} // :cond_1
} // :goto_0
v2 = java.lang.Float .isNaN ( v0 );
/* if-nez v2, :cond_2 */
/* .line 2660 */
v2 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( v0 );
/* int-to-float v2, v2 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I */
/* int-to-float v2, v2 */
/* .line 2662 */
/* .local v2, "spanTransition":F */
/* cmpl-float v3, p1, v2 */
/* if-lez v3, :cond_2 */
/* .line 2663 */
java.lang.Float .valueOf ( p1 );
java.lang.Float .valueOf ( p2 );
final String v5 = "hbm_usage"; // const-string v5, "hbm_usage"
/* invoke-direct {p0, v5, v3, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2664 */
/* sget-boolean v3, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 2665 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "noteHbmUsage: spanIndex: "; // const-string v4, "noteHbmUsage: spanIndex: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", spanTransition: "; // const-string v4, ", spanTransition: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", mBrightnessEventsMap: "; // const-string v4, ", mBrightnessEventsMap: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mBrightnessEventsMap;
/* .line 2667 */
(( java.lang.Object ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2665 */
final String v4 = "BrightnessDataProcessor"; // const-string v4, "BrightnessDataProcessor"
android.util.Slog .d ( v4,v3 );
/* .line 2671 */
} // .end local v2 # "spanTransition":F
} // :cond_2
return;
} // .end method
private void noteHdrAppUsage ( Float p0 ) {
/* .locals 3 */
/* .param p1, "duration" # F */
/* .line 2639 */
v0 = this.mHdrAppPackageName;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2640 */
final String v1 = "hdr_usage_app_usage"; // const-string v1, "hdr_usage_app_usage"
java.lang.Float .valueOf ( p1 );
/* invoke-direct {p0, v1, v0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2642 */
} // :cond_0
return;
} // .end method
private void noteHdrUsage ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "isHdrLayer" # Z */
/* .line 2615 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsHdrLayer:Z */
/* if-eq v0, p1, :cond_2 */
/* .line 2616 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsHdrLayer:Z */
/* .line 2617 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 2618 */
/* .local v0, "now":J */
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsHdrLayer:Z */
/* if-nez v2, :cond_1 */
v2 = this.mHdrAppPackageName;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 2619 */
/* iget-wide v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastHdrEnableTimeStamp:J */
/* sub-long v2, v0, v2 */
/* long-to-float v2, v2 */
/* const/high16 v3, 0x447a0000 # 1000.0f */
/* div-float/2addr v2, v3 */
/* .line 2620 */
/* .local v2, "duration":F */
/* const-string/jumbo v3, "usage_value" */
java.lang.Float .valueOf ( v2 );
final String v5 = "hdr_usage"; // const-string v5, "hdr_usage"
/* invoke-direct {p0, v5, v3, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2621 */
/* invoke-direct {p0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteHdrAppUsage(F)V */
/* .line 2622 */
int v3 = 0; // const/4 v3, 0x0
this.mHdrAppPackageName = v3;
/* .line 2623 */
/* const-wide/16 v3, 0x0 */
/* iput-wide v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastHdrEnableTimeStamp:J */
/* .line 2624 */
/* sget-boolean v3, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 2625 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "noteHdrUsage: mBrightnessEventsMap: "; // const-string v4, "noteHdrUsage: mBrightnessEventsMap: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mBrightnessEventsMap;
(( java.lang.Object ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "BrightnessDataProcessor"; // const-string v4, "BrightnessDataProcessor"
android.util.Slog .d ( v4,v3 );
/* .line 2627 */
} // .end local v2 # "duration":F
} // :cond_0
/* .line 2628 */
} // :cond_1
v2 = this.mForegroundAppPackageName;
this.mHdrAppPackageName = v2;
/* .line 2629 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastHdrEnableTimeStamp:J */
/* .line 2632 */
} // .end local v0 # "now":J
} // :cond_2
} // :goto_0
return;
} // .end method
private void noteHdrUsageBeforeReported ( ) {
/* .locals 6 */
/* .line 2747 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 2748 */
/* .local v0, "now":J */
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsHdrLayer:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mHdrAppPackageName;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 2749 */
/* iget-wide v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastHdrEnableTimeStamp:J */
/* sub-long v2, v0, v2 */
/* long-to-float v2, v2 */
/* const/high16 v3, 0x447a0000 # 1000.0f */
/* div-float/2addr v2, v3 */
/* .line 2750 */
/* .local v2, "duration":F */
/* const-string/jumbo v3, "usage_value" */
java.lang.Float .valueOf ( v2 );
final String v5 = "hdr_usage"; // const-string v5, "hdr_usage"
/* invoke-direct {p0, v5, v3, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2751 */
/* invoke-direct {p0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteHdrAppUsage(F)V */
/* .line 2752 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastHdrEnableTimeStamp:J */
/* .line 2753 */
/* sget-boolean v3, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 2754 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "noteHdrUsageBeforeReported: mBrightnessEventsMap: "; // const-string v4, "noteHdrUsageBeforeReported: mBrightnessEventsMap: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mBrightnessEventsMap;
(( java.lang.Object ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "BrightnessDataProcessor"; // const-string v4, "BrightnessDataProcessor"
android.util.Slog .d ( v4,v3 );
/* .line 2757 */
} // .end local v2 # "duration":F
} // :cond_0
return;
} // .end method
private void noteOutDoorHighTempUsage ( Float p0, Float p1, Long p2, Boolean p3, Boolean p4 ) {
/* .locals 15 */
/* .param p1, "restrictedBrightness" # F */
/* .param p2, "brightness" # F */
/* .param p3, "now" # J */
/* .param p5, "outDoorHighTempState" # Z */
/* .param p6, "isPendingReported" # Z */
/* .line 2495 */
/* move-object v0, p0 */
/* move/from16 v1, p2 */
/* move-wide/from16 v2, p3 */
/* move/from16 v4, p5 */
/* iget-wide v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J */
/* const-wide/16 v7, 0x0 */
/* cmp-long v5, v5, v7 */
/* if-nez v5, :cond_0 */
/* .line 2496 */
/* iput-boolean v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTempState:Z */
/* .line 2497 */
/* iput-wide v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J */
/* .line 2499 */
} // :cond_0
/* iget v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F */
v5 = java.lang.Float .isNaN ( v5 );
int v6 = 0; // const/4 v6, 0x0
if ( v5 != null) { // if-eqz v5, :cond_1
/* move v5, v6 */
/* .line 2500 */
} // :cond_1
v5 = this.mDisplayDeviceConfig;
/* iget v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F */
v5 = (( com.android.server.display.DisplayDeviceConfig ) v5 ).getNitsFromBacklight ( v7 ); // invoke-virtual {v5, v7}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
v5 = java.lang.Math .round ( v5 );
/* int-to-float v5, v5 */
} // :goto_0
/* nop */
/* .line 2501 */
/* .local v5, "lastUnrestrictedNit":F */
/* iget v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F */
v7 = java.lang.Float .isNaN ( v7 );
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 2502 */
} // :cond_2
v6 = this.mDisplayDeviceConfig;
/* iget v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F */
v6 = (( com.android.server.display.DisplayDeviceConfig ) v6 ).getNitsFromBacklight ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
v6 = java.lang.Math .round ( v6 );
/* int-to-float v6, v6 */
} // :goto_1
/* nop */
/* .line 2503 */
/* .local v6, "lastRestrictedNit":F */
v7 = android.util.MathUtils .min ( v5,v6 );
/* .line 2505 */
/* .local v7, "lastRestrictedNitByThermal":F */
/* iget-boolean v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTempState:Z */
/* const-string/jumbo v9, "unrestricted_usage" */
final String v10 = "restricted_usage"; // const-string v10, "restricted_usage"
/* const-string/jumbo v11, "thermal_outdoor_usage" */
/* const/high16 v12, 0x447a0000 # 1000.0f */
/* if-eq v8, v4, :cond_6 */
/* .line 2506 */
if ( v8 != null) { // if-eqz v8, :cond_3
/* iget v13, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mMinOutDoorHighTempNit:F */
/* cmpl-float v13, v7, v13 */
/* if-nez v13, :cond_3 */
/* .line 2507 */
/* iget-wide v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J */
/* sub-long v8, v2, v8 */
/* long-to-float v8, v8 */
/* div-float/2addr v8, v12 */
/* .line 2508 */
/* .local v8, "duration":F */
java.lang.Float .valueOf ( v8 );
/* invoke-direct {p0, v11, v10, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2509 */
} // .end local v8 # "duration":F
} // :cond_3
if ( v8 != null) { // if-eqz v8, :cond_4
/* iget v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mMinOutDoorHighTempNit:F */
/* cmpg-float v10, v7, v8 */
/* if-gez v10, :cond_4 */
/* cmpl-float v8, v6, v8 */
/* if-nez v8, :cond_4 */
/* .line 2511 */
/* iget-wide v13, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J */
/* sub-long v13, v2, v13 */
/* long-to-float v8, v13 */
/* div-float/2addr v8, v12 */
/* .line 2512 */
/* .restart local v8 # "duration":F */
java.lang.Float .valueOf ( v8 );
/* invoke-direct {p0, v11, v9, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2514 */
} // .end local v8 # "duration":F
} // :cond_4
} // :goto_2
if ( p6 != null) { // if-eqz p6, :cond_5
/* iget-boolean v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTempState:Z */
} // :cond_5
/* move v8, v4 */
} // :goto_3
/* iput-boolean v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTempState:Z */
/* .line 2515 */
/* iput-wide v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J */
/* .line 2516 */
} // :cond_6
if ( v8 != null) { // if-eqz v8, :cond_a
/* iget v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F */
/* cmpl-float v8, v1, v8 */
/* if-nez v8, :cond_7 */
/* iget v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F */
/* cmpl-float v8, p1, v8 */
if ( v8 != null) { // if-eqz v8, :cond_a
/* .line 2518 */
} // :cond_7
/* iget v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mMinOutDoorHighTempNit:F */
/* cmpl-float v13, v7, v8 */
/* if-nez v13, :cond_8 */
/* .line 2519 */
/* iget-wide v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J */
/* sub-long v8, v2, v8 */
/* long-to-float v8, v8 */
/* div-float/2addr v8, v12 */
/* .line 2520 */
/* .restart local v8 # "duration":F */
java.lang.Float .valueOf ( v8 );
/* invoke-direct {p0, v11, v10, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2521 */
} // .end local v8 # "duration":F
} // :cond_8
/* cmpg-float v10, v7, v8 */
/* if-gez v10, :cond_9 */
/* cmpl-float v8, v6, v8 */
/* if-nez v8, :cond_9 */
/* .line 2523 */
/* long-to-float v8, v2 */
/* iget v10, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessRestrictedTimeStamp:F */
/* sub-float/2addr v8, v10 */
/* div-float/2addr v8, v12 */
/* .line 2524 */
/* .restart local v8 # "duration":F */
java.lang.Float .valueOf ( v8 );
/* invoke-direct {p0, v11, v9, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2526 */
} // .end local v8 # "duration":F
} // :cond_9
} // :goto_4
/* iput-wide v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J */
/* .line 2528 */
} // :cond_a
} // :goto_5
/* sget-boolean v8, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v8 != null) { // if-eqz v8, :cond_b
/* .line 2529 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "noteOutDoorHighTempUsage: brightness: "; // const-string v9, "noteOutDoorHighTempUsage: brightness: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v1 ); // invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v9 = ", lastRestrictedNit: "; // const-string v9, ", lastRestrictedNit: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v9 = ", mLastUnrestrictedBrightness: "; // const-string v9, ", mLastUnrestrictedBrightness: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v9 = ", lastRestrictedNitByThermal: "; // const-string v9, ", lastRestrictedNitByThermal: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v9 = ", outDoorHighTempState: "; // const-string v9, ", outDoorHighTempState: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v9 = ", mLastOutDoorHighTempState: "; // const-string v9, ", mLastOutDoorHighTempState: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v9, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTempState:Z */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v9 = ", mThermalEventsMap: "; // const-string v9, ", mThermalEventsMap: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.mThermalEventsMap;
/* .line 2535 */
(( java.lang.Object ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2529 */
final String v9 = "BrightnessDataProcessor"; // const-string v9, "BrightnessDataProcessor"
android.util.Slog .d ( v9,v8 );
/* .line 2537 */
} // :cond_b
return;
} // .end method
private void noteThermalBrightnessRestrictedUsage ( Float p0, Float p1, Long p2, Boolean p3 ) {
/* .locals 6 */
/* .param p1, "restrictedBrightness" # F */
/* .param p2, "unrestrictedBrightness" # F */
/* .param p3, "now" # J */
/* .param p5, "isScreenOn" # Z */
/* .line 2400 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessRestrictedTimeStamp:F */
int v1 = 0; // const/4 v1, 0x0
/* cmpl-float v0, v0, v1 */
/* if-nez v0, :cond_0 */
/* .line 2401 */
/* iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F */
/* .line 2402 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F */
/* .line 2403 */
/* long-to-float v0, p3 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessRestrictedTimeStamp:F */
/* .line 2406 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F */
/* cmpl-float v1, v0, p1 */
/* if-nez v1, :cond_1 */
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F */
/* cmpl-float v1, v1, p2 */
/* if-nez v1, :cond_1 */
/* if-nez p5, :cond_4 */
/* .line 2409 */
} // :cond_1
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
int v2 = 3; // const/4 v2, 0x3
/* if-ne v1, v2, :cond_2 */
/* .line 2410 */
/* long-to-float v1, p3 */
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessRestrictedTimeStamp:F */
/* sub-float/2addr v1, v2 */
/* const/high16 v2, 0x447a0000 # 1000.0f */
/* div-float/2addr v1, v2 */
/* .line 2411 */
/* .local v1, "duration":F */
v2 = this.mDisplayDeviceConfig;
v0 = (( com.android.server.display.DisplayDeviceConfig ) v2 ).getNitsFromBacklight ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
v0 = java.lang.Math .round ( v0 );
/* .line 2412 */
/* .local v0, "nit":I */
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F */
v2 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( v2 );
/* int-to-float v2, v2 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I */
/* .line 2413 */
/* .local v2, "brightnessSpan":I */
v3 = this.mThermalBrightnessRestrictedUsage;
/* invoke-direct {p0, v1, v3, v0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalBrightnessRestrictedUsage(FLandroid/util/SparseArray;II)V */
/* .line 2414 */
final String v3 = "restricted_usage"; // const-string v3, "restricted_usage"
v4 = this.mThermalBrightnessRestrictedUsage;
/* const-string/jumbo v5, "thermal_brightness_restricted_usage" */
/* invoke-direct {p0, v5, v3, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2416 */
/* iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F */
/* .line 2417 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F */
/* .line 2418 */
/* long-to-float v3, p3 */
/* iput v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessRestrictedTimeStamp:F */
/* .line 2419 */
} // .end local v0 # "nit":I
} // .end local v2 # "brightnessSpan":I
} // .end local v1 # "duration":F
} // :cond_2
int v0 = 2; // const/4 v0, 0x2
/* if-eq v1, v0, :cond_3 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne v1, v0, :cond_4 */
/* .line 2421 */
} // :cond_3
/* iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F */
/* .line 2422 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F */
/* .line 2423 */
/* long-to-float v0, p3 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessRestrictedTimeStamp:F */
/* .line 2426 */
} // :cond_4
} // :goto_0
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 2427 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "noteThermalBrightnessRestrictedUsage: restrictedBrightness: "; // const-string v1, "noteThermalBrightnessRestrictedUsage: restrictedBrightness: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", unrestrictedBrightness: "; // const-string v1, ", unrestrictedBrightness: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", mThermalEventsMap: "; // const-string v1, ", mThermalEventsMap: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mThermalEventsMap;
/* .line 2430 */
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2427 */
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 2432 */
} // :cond_5
return;
} // .end method
private void noteThermalUsage ( Integer p0, Long p1, Boolean p2 ) {
/* .locals 9 */
/* .param p1, "status" # I */
/* .param p2, "now" # J */
/* .param p4, "isScreenOn" # Z */
/* .line 2109 */
/* iget-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-nez v0, :cond_0 */
/* .line 2110 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
/* .line 2111 */
/* iput-wide p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J */
/* .line 2113 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
final String v1 = ", mThermalEventsMap: "; // const-string v1, ", mThermalEventsMap: "
final String v2 = ", duration: "; // const-string v2, ", duration: "
final String v3 = "noteThermalUsage: status: "; // const-string v3, "noteThermalUsage: status: "
final String v4 = "BrightnessDataProcessor"; // const-string v4, "BrightnessDataProcessor"
/* const-string/jumbo v5, "thermal_usage" */
/* const/high16 v6, 0x447a0000 # 1000.0f */
/* if-eq v0, p1, :cond_1 */
if ( p4 != null) { // if-eqz p4, :cond_1
/* .line 2114 */
/* iget-wide v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J */
/* sub-long v7, p2, v7 */
/* long-to-float v7, v7 */
/* div-float/2addr v7, v6 */
/* .line 2115 */
/* .local v7, "duration":F */
java.lang.Integer .valueOf ( v0 );
java.lang.Float .valueOf ( v7 );
/* invoke-direct {p0, v5, v0, v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2116 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
/* .line 2117 */
/* iput-wide p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J */
/* .line 2118 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 2119 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mThermalEventsMap;
/* .line 2121 */
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2119 */
android.util.Slog .d ( v4,v0 );
/* .line 2123 */
} // .end local v7 # "duration":F
} // :cond_1
/* if-nez p4, :cond_2 */
/* .line 2124 */
/* iget-wide v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J */
/* sub-long v7, p2, v7 */
/* long-to-float v7, v7 */
/* div-float/2addr v7, v6 */
/* .line 2125 */
/* .restart local v7 # "duration":F */
java.lang.Integer .valueOf ( v0 );
java.lang.Float .valueOf ( v7 );
/* invoke-direct {p0, v5, v0, v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2126 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
/* .line 2127 */
/* iput-wide p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J */
/* .line 2128 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 2129 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mThermalEventsMap;
/* .line 2131 */
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2129 */
android.util.Slog .d ( v4,v0 );
/* .line 2134 */
} // .end local v7 # "duration":F
} // :cond_2
} // :goto_0
return;
} // .end method
private void readyToReportEvent ( ) {
/* .locals 2 */
/* .line 599 */
v0 = this.mPendingBrightnessChangeItem;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 601 */
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHaveValidWindowBrightness:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mUserDragging:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 602 */
return;
/* .line 604 */
} // :cond_0
/* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportBrightnessEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;)V */
/* .line 607 */
v0 = this.mPendingBrightnessChangeItem;
/* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportIndividualModelEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;)V */
/* .line 609 */
} // :cond_1
return;
} // .end method
private void registerAccSensor ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "enable" # Z */
/* .line 2785 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAccSensorEnabled:Z */
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
/* if-nez v0, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 2786 */
v0 = this.mSensorManager;
v2 = this.mSensorListener;
v3 = this.mAccSensor;
int v4 = 3; // const/4 v4, 0x3
v5 = this.mBackgroundHandler;
(( android.hardware.SensorManager ) v0 ).registerListener ( v2, v3, v4, v5 ); // invoke-virtual {v0, v2, v3, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z
/* .line 2788 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAccSensorEnabled:Z */
/* .line 2789 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2790 */
final String v0 = "registerAccSensor: register acc sensor."; // const-string v0, "registerAccSensor: register acc sensor."
android.util.Slog .d ( v1,v0 );
/* .line 2792 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez p1, :cond_1 */
/* .line 2793 */
v0 = this.mSensorManager;
v2 = this.mSensorListener;
v3 = this.mAccSensor;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V
/* .line 2794 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAccSensorEnabled:Z */
/* .line 2795 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2796 */
final String v0 = "registerAccSensor: unregister acc sensor."; // const-string v0, "registerAccSensor: unregister acc sensor."
android.util.Slog .d ( v1,v0 );
/* .line 2799 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void registerForegroundAppUpdater ( ) {
/* .locals 3 */
/* .line 824 */
try { // :try_start_0
v0 = this.mActivityTaskManager;
v1 = this.mTaskStackListener;
/* .line 825 */
v0 = this.mForegroundWindowListener;
miui.process.ProcessManager .registerForegroundWindowListener ( v0 );
/* .line 828 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateForegroundApps()V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 834 */
/* .line 829 */
/* :catch_0 */
/* move-exception v0 */
/* .line 830 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 831 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to register foreground app updater: "; // const-string v2, "Failed to register foreground app updater: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BrightnessDataProcessor"; // const-string v2, "BrightnessDataProcessor"
android.util.Slog .e ( v2,v1 );
/* .line 835 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
return;
} // .end method
private void registerLightFovSensor ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "enable" # Z */
/* .line 2806 */
v0 = this.mLightFovSensor;
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightFovSensorEnabled:Z */
/* if-nez v2, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 2807 */
v2 = this.mSensorManager;
v3 = this.mSensorListener;
int v4 = 3; // const/4 v4, 0x3
v5 = this.mBackgroundHandler;
(( android.hardware.SensorManager ) v2 ).registerListener ( v3, v0, v4, v5 ); // invoke-virtual {v2, v3, v0, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z
/* .line 2809 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightFovSensorEnabled:Z */
/* .line 2810 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2811 */
final String v0 = "registerLightFovSensor: register light fov sensor."; // const-string v0, "registerLightFovSensor: register light fov sensor."
android.util.Slog .d ( v1,v0 );
/* .line 2813 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightFovSensorEnabled:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* if-nez p1, :cond_1 */
/* .line 2814 */
v2 = this.mSensorManager;
v3 = this.mSensorListener;
(( android.hardware.SensorManager ) v2 ).unregisterListener ( v3, v0 ); // invoke-virtual {v2, v3, v0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V
/* .line 2815 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightFovSensorEnabled:Z */
/* .line 2816 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2817 */
final String v0 = "registerLightFovSensor: unregister light fov sensor."; // const-string v0, "registerLightFovSensor: unregister light fov sensor."
android.util.Slog .d ( v1,v0 );
/* .line 2820 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void registerLightSensor ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "enable" # Z */
/* .line 2764 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightSensorEnabled:Z */
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
/* if-nez v0, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 2765 */
v0 = this.mSensorManager;
v2 = this.mSensorListener;
v3 = this.mLightSensor;
int v4 = 3; // const/4 v4, 0x3
v5 = this.mBackgroundHandler;
(( android.hardware.SensorManager ) v0 ).registerListener ( v2, v3, v4, v5 ); // invoke-virtual {v0, v2, v3, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z
/* .line 2767 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightSensorEnabled:Z */
/* .line 2768 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2769 */
final String v0 = "registerLightSensor: register light sensor."; // const-string v0, "registerLightSensor: register light sensor."
android.util.Slog .d ( v1,v0 );
/* .line 2771 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez p1, :cond_1 */
/* .line 2772 */
v0 = this.mSensorManager;
v2 = this.mSensorListener;
v3 = this.mLightSensor;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V
/* .line 2773 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightSensorEnabled:Z */
/* .line 2774 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2775 */
final String v0 = "registerLightSensor: unregister light sensor."; // const-string v0, "registerLightSensor: unregister light sensor."
android.util.Slog .d ( v1,v0 );
/* .line 2778 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void registerScreenStateReceiver ( ) {
/* .locals 3 */
/* .line 866 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 867 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 868 */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 869 */
/* const/16 v1, 0x3e8 */
(( android.content.IntentFilter ) v0 ).setPriority ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V
/* .line 870 */
v1 = this.mContext;
/* new-instance v2, Lcom/android/server/display/statistics/BrightnessDataProcessor$ScreenStateReceiver; */
/* invoke-direct {v2, p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$ScreenStateReceiver;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V */
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 871 */
return;
} // .end method
private void registerSensorByBrightnessType ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "enable" # Z */
/* .line 2206 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_0 */
/* .line 2207 */
/* invoke-direct {p0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerAccSensor(Z)V */
/* .line 2208 */
/* invoke-direct {p0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerLightFovSensor(Z)V */
/* .line 2209 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 2210 */
/* invoke-direct {p0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerLightSensor(Z)V */
/* .line 2212 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void registerSettingsObserver ( ) {
/* .locals 7 */
/* .line 1547 */
v0 = this.mContentResolver;
/* .line 1548 */
final String v1 = "screen_brightness_mode"; // const-string v1, "screen_brightness_mode"
android.provider.Settings$System .getUriFor ( v1 );
v3 = this.mSettingsObserver;
/* .line 1547 */
int v4 = 0; // const/4 v4, 0x0
int v5 = -1; // const/4 v5, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v0, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1551 */
v0 = this.mContentResolver;
int v2 = -2; // const/4 v2, -0x2
v0 = android.provider.Settings$System .getIntForUser ( v0,v1,v4,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v0, v1 */
} // :cond_0
/* move v0, v4 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z */
/* .line 1554 */
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightnessEnable:Z */
/* .line 1555 */
v0 = this.mContentResolver;
/* .line 1556 */
final String v2 = "disable_security_by_mishow"; // const-string v2, "disable_security_by_mishow"
android.provider.Settings$System .getUriFor ( v2 );
v6 = this.mSettingsObserver;
/* .line 1555 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v3, v4, v6, v5 ); // invoke-virtual {v0, v3, v4, v6, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1559 */
v0 = this.mContentResolver;
v0 = android.provider.Settings$System .getInt ( v0,v2,v4 );
/* if-ne v0, v1, :cond_1 */
/* move v4, v1 */
} // :cond_1
/* iput-boolean v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z */
/* .line 1560 */
return;
} // .end method
private void reportAdvancedBrightnessEvents ( ) {
/* .locals 4 */
/* .line 2007 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z */
/* if-nez v0, :cond_0 */
/* .line 2008 */
v0 = this.mContext;
v1 = this.mAdvancedBrightnessEventsMap;
final String v2 = "advanced_brightness_aggregation"; // const-string v2, "advanced_brightness_aggregation"
/* iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mExpId:I */
com.android.server.display.statistics.OneTrackUploaderHelper .reportAggregatedEventsToServer ( v0,v1,v2,v3 );
/* .line 2010 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2011 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "reportAdvancedBrightnessEvents: mAdvancedBrightnessEventsMap: "; // const-string v1, "reportAdvancedBrightnessEvents: mAdvancedBrightnessEventsMap: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mAdvancedBrightnessEventsMap;
/* .line 2012 */
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2011 */
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 2015 */
} // :cond_0
v0 = this.mAdvancedBrightnessEventsMap;
/* .line 2016 */
return;
} // .end method
private void reportAggregatedBrightnessEvents ( ) {
/* .locals 4 */
/* .line 1981 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateSwitchEvents()V */
/* .line 1982 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessUsageDuration()V */
/* .line 1983 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteHdrUsageBeforeReported()V */
/* .line 1984 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z */
/* if-nez v0, :cond_0 */
/* .line 1985 */
v0 = this.mContext;
v1 = this.mBrightnessEventsMap;
final String v2 = "brightness_quota_aggregation"; // const-string v2, "brightness_quota_aggregation"
/* iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mExpId:I */
com.android.server.display.statistics.OneTrackUploaderHelper .reportAggregatedEventsToServer ( v0,v1,v2,v3 );
/* .line 1987 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1988 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "reportAggregatedBrightnessEvents: mBrightnessEventsMap: "; // const-string v1, "reportAggregatedBrightnessEvents: mBrightnessEventsMap: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mBrightnessEventsMap;
/* .line 1989 */
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1988 */
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 1992 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->resetStatsCache()V */
/* .line 1993 */
return;
} // .end method
private void reportAonFlareEvents ( ) {
/* .locals 4 */
/* .line 3139 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z */
/* if-nez v0, :cond_0 */
/* .line 3140 */
v0 = this.mContext;
v1 = this.mAonFlareEventsMap;
final String v2 = "aon_flare_aggregation"; // const-string v2, "aon_flare_aggregation"
/* iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mExpId:I */
com.android.server.display.statistics.OneTrackUploaderHelper .reportAggregatedEventsToServer ( v0,v1,v2,v3 );
/* .line 3142 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 3143 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "reportAonFlareEvents: mAonFlareEventsMap: "; // const-string v1, "reportAonFlareEvents: mAonFlareEventsMap: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mAonFlareEventsMap;
/* .line 3144 */
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 3143 */
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 3147 */
} // :cond_0
v0 = this.mAonFlareEventsMap;
/* .line 3148 */
return;
} // .end method
private void reportBrightnessEvent ( com.android.server.display.statistics.BrightnessDataProcessor$BrightnessChangeItem p0 ) {
/* .locals 8 */
/* .param p1, "item" # Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem; */
/* .line 615 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 616 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "brightness changed, let\'s make a recode: "; // const-string v1, "brightness changed, let\'s make a recode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.display.statistics.BrightnessDataProcessor$BrightnessChangeItem ) p1 ).toString ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", foregroundApps: "; // const-string v1, ", foregroundApps: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mForegroundAppPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 620 */
} // :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 621 */
/* .local v0, "now":J */
/* iget v2, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I */
/* .line 622 */
/* .local v2, "curSpanIndex":I */
/* iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F */
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I */
/* .line 625 */
/* .local v3, "preSpanIndex":I */
/* iget v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
int v5 = 3; // const/4 v5, 0x3
int v6 = 1; // const/4 v6, 0x1
/* if-ne v4, v5, :cond_1 */
/* move v4, v6 */
} // :cond_1
int v4 = 0; // const/4 v4, 0x0
/* .line 627 */
/* .local v4, "brightnessRestricted":Z */
} // :goto_0
/* iget v5, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
/* if-ne v5, v6, :cond_2 */
v5 = this.mCloudControllerListener;
v5 = /* .line 628 */
/* if-nez v5, :cond_3 */
} // :cond_2
/* iget v5, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
int v7 = 2; // const/4 v7, 0x2
/* if-ne v5, v7, :cond_4 */
/* iget-boolean v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mReportBrightnessEventsEnable:Z */
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 631 */
} // :cond_3
/* new-instance v5, Lcom/android/server/display/statistics/BrightnessEvent; */
/* invoke-direct {v5}, Lcom/android/server/display/statistics/BrightnessEvent;-><init>()V */
/* .line 632 */
/* .local v5, "event":Lcom/android/server/display/statistics/BrightnessEvent; */
/* iget v6, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
(( com.android.server.display.statistics.BrightnessEvent ) v5 ).setEventType ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/display/statistics/BrightnessEvent;->setEventType(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 633 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setCurBrightnessSpanIndex ( v2 ); // invoke-virtual {v6, v2}, Lcom/android/server/display/statistics/BrightnessEvent;->setCurBrightnessSpanIndex(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 634 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setPreBrightnessSpanIndex ( v3 ); // invoke-virtual {v6, v3}, Lcom/android/server/display/statistics/BrightnessEvent;->setPreBrightnessSpanIndex(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 635 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setTimeStamp ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Lcom/android/server/display/statistics/BrightnessEvent;->setTimeStamp(J)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F */
/* .line 636 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setScreenBrightness ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setScreenBrightness(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F */
/* .line 637 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setPreviousBrightness ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setPreviousBrightness(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F */
/* .line 638 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setAmbientLux ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->userDataPoint:F */
/* .line 639 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setUserDataPoint ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setUserDataPoint(F)Lcom/android/server/display/statistics/BrightnessEvent;
v7 = this.packageName;
/* .line 640 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setForegroundPackage ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setForegroundPackage(Ljava/lang/String;)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget-boolean v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->lowPowerMode:Z */
/* .line 641 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setLowPowerModeFlag ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLowPowerModeFlag(Z)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget-boolean v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->defaultConfig:Z */
/* .line 642 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setIsDefaultConfig ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setIsDefaultConfig(Z)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 643 */
v7 = /* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->convergeAffectFactors()I */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setAffectFactorFlag ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setAffectFactorFlag(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F */
/* .line 644 */
v7 = /* invoke-direct {p0, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getAmbientLuxSpanIndex(F)I */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setLuxSpanIndex ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLuxSpanIndex(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOrientation:I */
/* .line 645 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setOrientation ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setOrientation(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mGrayScale:F */
/* .line 646 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setDisplayGrayScale ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setDisplayGrayScale(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentUserId:I */
/* .line 647 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setUserId ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setUserId(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalNit:F */
/* .line 648 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setOriginalNit ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setOriginalNit(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->actualNit:F */
/* .line 649 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setActualNit ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setActualNit(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget-boolean v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsHdrLayer:Z */
/* .line 650 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setHdrLayerEnable ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setHdrLayerEnable(Z)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 651 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setBrightnessRestrictedEnable ( v4 ); // invoke-virtual {v6, v4}, Lcom/android/server/display/statistics/BrightnessEvent;->setBrightnessRestrictedEnable(Z)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->mainLux:F */
/* .line 652 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setMainAmbientLux ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setMainAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->assistLux:F */
/* .line 653 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setAssistAmbientLux ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setAssistAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastMainAmbientLux:F */
/* .line 654 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setLastMainAmbientLux ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLastMainAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAssistAmbientLux:F */
/* .line 655 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setLastAssistAmbientLux ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLastAssistAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;
v7 = this.accValues;
/* .line 656 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setAccValues ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setAccValues([F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget-boolean v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->isUseLightFovOptimization:Z */
/* .line 657 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setIsUseLightFovOptimization ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setIsUseLightFovOptimization(Z)Lcom/android/server/display/statistics/BrightnessEvent;
v7 = this.mSwitchStatsHelper;
/* .line 658 */
(( com.android.server.display.statistics.SwitchStatsHelper ) v7 ).getAllSwitchStats ( ); // invoke-virtual {v7}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getAllSwitchStats()Ljava/util/List;
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setSwitchStats ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setSwitchStats(Ljava/util/List;)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 659 */
/* invoke-direct {p0, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportEventToServer(Lcom/android/server/display/statistics/BrightnessEvent;)V */
} // .end local v5 # "event":Lcom/android/server/display/statistics/BrightnessEvent;
/* goto/16 :goto_1 */
/* .line 660 */
} // :cond_4
/* iget v5, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
/* if-eq v5, v6, :cond_6 */
/* iget-boolean v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mReportBrightnessEventsEnable:Z */
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 662 */
/* new-instance v5, Lcom/android/server/display/statistics/BrightnessEvent; */
/* invoke-direct {v5}, Lcom/android/server/display/statistics/BrightnessEvent;-><init>()V */
/* .line 663 */
/* .restart local v5 # "event":Lcom/android/server/display/statistics/BrightnessEvent; */
/* iget v6, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
(( com.android.server.display.statistics.BrightnessEvent ) v5 ).setEventType ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/display/statistics/BrightnessEvent;->setEventType(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 664 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setCurBrightnessSpanIndex ( v2 ); // invoke-virtual {v6, v2}, Lcom/android/server/display/statistics/BrightnessEvent;->setCurBrightnessSpanIndex(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 665 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setPreBrightnessSpanIndex ( v3 ); // invoke-virtual {v6, v3}, Lcom/android/server/display/statistics/BrightnessEvent;->setPreBrightnessSpanIndex(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 666 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setTimeStamp ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Lcom/android/server/display/statistics/BrightnessEvent;->setTimeStamp(J)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F */
/* .line 667 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setPreviousBrightness ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setPreviousBrightness(F)Lcom/android/server/display/statistics/BrightnessEvent;
v7 = this.packageName;
/* .line 668 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setForegroundPackage ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setForegroundPackage(Ljava/lang/String;)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget-boolean v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->lowPowerMode:Z */
/* .line 669 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setLowPowerModeFlag ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLowPowerModeFlag(Z)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 670 */
v7 = /* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->convergeAffectFactors()I */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setAffectFactorFlag ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setAffectFactorFlag(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOrientation:I */
/* .line 671 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setOrientation ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setOrientation(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentUserId:I */
/* .line 672 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setUserId ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setUserId(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalNit:F */
/* .line 673 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setOriginalNit ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setOriginalNit(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->actualNit:F */
/* .line 674 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setActualNit ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setActualNit(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget-boolean v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsHdrLayer:Z */
/* .line 675 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setHdrLayerEnable ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setHdrLayerEnable(Z)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 676 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setBrightnessRestrictedEnable ( v4 ); // invoke-virtual {v6, v4}, Lcom/android/server/display/statistics/BrightnessEvent;->setBrightnessRestrictedEnable(Z)Lcom/android/server/display/statistics/BrightnessEvent;
v7 = this.mSwitchStatsHelper;
/* .line 677 */
(( com.android.server.display.statistics.SwitchStatsHelper ) v7 ).getAllSwitchStats ( ); // invoke-virtual {v7}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getAllSwitchStats()Ljava/util/List;
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setSwitchStats ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setSwitchStats(Ljava/util/List;)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 678 */
/* iget v6, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
/* if-nez v6, :cond_5 */
/* .line 679 */
/* iget v6, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F */
(( com.android.server.display.statistics.BrightnessEvent ) v5 ).setAmbientLux ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/display/statistics/BrightnessEvent;->setAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F */
/* .line 680 */
v7 = /* invoke-direct {p0, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getAmbientLuxSpanIndex(F)I */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setLuxSpanIndex ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLuxSpanIndex(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 682 */
} // :cond_5
/* invoke-direct {p0, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportEventToServer(Lcom/android/server/display/statistics/BrightnessEvent;)V */
/* .line 660 */
} // .end local v5 # "event":Lcom/android/server/display/statistics/BrightnessEvent;
} // :cond_6
} // :goto_1
/* nop */
/* .line 684 */
} // :goto_2
/* invoke-direct {p0, v2, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateBrightnessUsageIfNeeded(II)V */
/* .line 685 */
return;
} // .end method
private void reportCustomBrightnessEvents ( ) {
/* .locals 4 */
/* .line 3066 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z */
/* if-nez v0, :cond_0 */
/* .line 3067 */
v0 = this.mContext;
v1 = this.mCbmEventsMap;
final String v2 = "custom_brightness_aggregation"; // const-string v2, "custom_brightness_aggregation"
/* iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mExpId:I */
com.android.server.display.statistics.OneTrackUploaderHelper .reportAggregatedEventsToServer ( v0,v1,v2,v3 );
/* .line 3069 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 3070 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "reportCustomBrightnessEvents: mCbmEventsMap: "; // const-string v1, "reportCustomBrightnessEvents: mCbmEventsMap: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCbmEventsMap;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 3073 */
} // :cond_0
v0 = this.mCbmEventsMap;
/* .line 3074 */
v0 = this.mModelTrainIndicatorsList;
/* .line 3075 */
return;
} // .end method
private void reportDisabledAutoBrightnessEvent ( ) {
/* .locals 9 */
/* .line 2826 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mScreenOn:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 2827 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightnessEnable:Z */
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
/* const/16 v2, 0xd */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z */
/* if-nez v3, :cond_1 */
/* .line 2828 */
v0 = this.mBackgroundHandler;
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 2829 */
/* new-instance v0, Lcom/android/server/display/statistics/BrightnessEvent; */
/* invoke-direct {v0}, Lcom/android/server/display/statistics/BrightnessEvent;-><init>()V */
/* .line 2830 */
/* .local v0, "event":Lcom/android/server/display/statistics/BrightnessEvent; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* .line 2831 */
/* .local v3, "now":J */
/* iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
int v6 = 3; // const/4 v6, 0x3
/* if-ne v5, v6, :cond_0 */
int v5 = 1; // const/4 v5, 0x1
} // :cond_0
int v5 = 0; // const/4 v5, 0x0
/* .line 2833 */
/* .local v5, "brightnessRestricted":Z */
} // :goto_0
/* const/16 v6, 0x8 */
(( com.android.server.display.statistics.BrightnessEvent ) v0 ).setEventType ( v6 ); // invoke-virtual {v0, v6}, Lcom/android/server/display/statistics/BrightnessEvent;->setEventType(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 2834 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setTimeStamp ( v3, v4 ); // invoke-virtual {v6, v3, v4}, Lcom/android/server/display/statistics/BrightnessEvent;->setTimeStamp(J)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mActualNit:F */
/* .line 2835 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setActualNit ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setActualNit(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOriginalNit:F */
/* .line 2836 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setOriginalNit ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setOriginalNit(F)Lcom/android/server/display/statistics/BrightnessEvent;
v7 = this.mForegroundAppPackageName;
/* .line 2837 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setForegroundPackage ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setForegroundPackage(Ljava/lang/String;)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 2838 */
v7 = /* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->convergeAffectFactors()I */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setAffectFactorFlag ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setAffectFactorFlag(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOrientation:I */
/* .line 2839 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setOrientation ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setOrientation(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentUserId:I */
/* .line 2840 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setUserId ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setUserId(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget-boolean v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsHdrLayer:Z */
/* .line 2841 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setHdrLayerEnable ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setHdrLayerEnable(Z)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 2842 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setBrightnessRestrictedEnable ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/display/statistics/BrightnessEvent;->setBrightnessRestrictedEnable(Z)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAmbientLux:F */
/* .line 2843 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setAmbientLux ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAmbientLux:F */
/* .line 2844 */
v7 = /* invoke-direct {p0, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getAmbientLuxSpanIndex(F)I */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setLuxSpanIndex ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLuxSpanIndex(I)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastMainAmbientLux:F */
/* .line 2845 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setLastMainAmbientLux ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLastMainAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;
/* iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAssistAmbientLux:F */
/* .line 2846 */
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setLastAssistAmbientLux ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLastAssistAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;
v7 = this.mSwitchStatsHelper;
/* .line 2847 */
(( com.android.server.display.statistics.SwitchStatsHelper ) v7 ).getAllSwitchStats ( ); // invoke-virtual {v7}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getAllSwitchStats()Ljava/util/List;
(( com.android.server.display.statistics.BrightnessEvent ) v6 ).setSwitchStats ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setSwitchStats(Ljava/util/List;)Lcom/android/server/display/statistics/BrightnessEvent;
/* .line 2848 */
v6 = this.mBackgroundHandler;
(( android.os.Handler ) v6 ).obtainMessage ( v2 ); // invoke-virtual {v6, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 2849 */
/* .local v2, "msg":Landroid/os/Message; */
this.obj = v0;
/* .line 2850 */
v6 = this.mBackgroundHandler;
/* const-wide/32 v7, 0x493e0 */
(( android.os.Handler ) v6 ).sendMessageDelayed ( v2, v7, v8 ); // invoke-virtual {v6, v2, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 2851 */
/* sget-boolean v6, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 2852 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "reportDisabledAutoBrightnessEvent: event: "; // const-string v7, "reportDisabledAutoBrightnessEvent: event: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.display.statistics.BrightnessEvent ) v0 ).toString ( ); // invoke-virtual {v0}, Lcom/android/server/display/statistics/BrightnessEvent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v6 );
/* .line 2854 */
} // .end local v0 # "event":Lcom/android/server/display/statistics/BrightnessEvent;
} // .end local v2 # "msg":Landroid/os/Message;
} // .end local v3 # "now":J
} // .end local v5 # "brightnessRestricted":Z
} // :cond_1
/* if-nez v0, :cond_2 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 2855 */
v0 = this.mBackgroundHandler;
v0 = (( android.os.Handler ) v0 ).hasMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 2856 */
v0 = this.mBackgroundHandler;
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 2857 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 2858 */
final String v0 = "reportDisabledAutoBrightnessEvent: remove message due to enabled auto brightness."; // const-string v0, "reportDisabledAutoBrightnessEvent: remove message due to enabled auto brightness."
android.util.Slog .d ( v1,v0 );
/* .line 2854 */
} // :cond_2
} // :goto_1
/* nop */
/* .line 2864 */
} // :cond_3
} // :goto_2
return;
} // .end method
private void reportEventToServer ( com.android.server.display.statistics.AdvancedEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Lcom/android/server/display/statistics/AdvancedEvent; */
/* .line 790 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z */
/* if-nez v0, :cond_0 */
/* .line 791 */
v0 = this.mContext;
com.android.server.display.statistics.OneTrackUploaderHelper .reportToOneTrack ( v0,p1 );
/* .line 792 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 793 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 794 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
(( com.android.server.display.statistics.AdvancedEvent ) p1 ).convertToString ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->convertToString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 795 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "reportAdvancedEventToServer:, event:"; // const-string v2, "reportAdvancedEventToServer:, event:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BrightnessDataProcessor"; // const-string v2, "BrightnessDataProcessor"
android.util.Slog .d ( v2,v1 );
/* .line 798 */
} // .end local v0 # "sb":Ljava/lang/StringBuilder;
} // :cond_0
return;
} // .end method
private void reportEventToServer ( com.android.server.display.statistics.BrightnessEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Lcom/android/server/display/statistics/BrightnessEvent; */
/* .line 779 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z */
/* if-nez v0, :cond_0 */
/* .line 780 */
v0 = this.mContext;
com.android.server.display.statistics.OneTrackUploaderHelper .reportToOneTrack ( v0,p1 );
/* .line 781 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 782 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 783 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
(( com.android.server.display.statistics.BrightnessEvent ) p1 ).toString ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 784 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "reportBrightnessEventToServer: , event:"; // const-string v2, "reportBrightnessEventToServer: , event:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BrightnessDataProcessor"; // const-string v2, "BrightnessDataProcessor"
android.util.Slog .d ( v2,v1 );
/* .line 787 */
} // .end local v0 # "sb":Ljava/lang/StringBuilder;
} // :cond_0
return;
} // .end method
private void reportIndividualModelEvent ( com.android.server.display.statistics.BrightnessDataProcessor$BrightnessChangeItem p0 ) {
/* .locals 2 */
/* .param p1, "item" # Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem; */
/* .line 691 */
/* iget v0, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, v1, :cond_0 */
/* iget v0, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_3 */
} // :cond_0
/* iget v0, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F */
/* .line 693 */
v0 = java.lang.Float .isNaN ( v0 );
/* if-nez v0, :cond_3 */
v0 = this.mModelEventCallback;
/* if-nez v0, :cond_1 */
/* .line 700 */
} // :cond_1
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->createModelEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;)Lcom/xiaomi/aiautobrt/IndividualModelEvent; */
/* .line 702 */
/* .local v0, "event":Lcom/xiaomi/aiautobrt/IndividualModelEvent; */
v1 = (( com.xiaomi.aiautobrt.IndividualModelEvent ) v0 ).isValidRawEvent ( ); // invoke-virtual {v0}, Lcom/xiaomi/aiautobrt/IndividualModelEvent;->isValidRawEvent()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 703 */
v1 = this.mModelEventCallback;
/* .line 705 */
} // :cond_2
return;
/* .line 697 */
} // .end local v0 # "event":Lcom/xiaomi/aiautobrt/IndividualModelEvent;
} // :cond_3
} // :goto_0
return;
} // .end method
private void reportScheduleEvent ( ) {
/* .locals 0 */
/* .line 447 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportAggregatedBrightnessEvents()V */
/* .line 448 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportAdvancedBrightnessEvents()V */
/* .line 449 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportThermalEvents()V */
/* .line 450 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportCustomBrightnessEvents()V */
/* .line 451 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportAonFlareEvents()V */
/* .line 452 */
return;
} // .end method
private void reportThermalEvents ( ) {
/* .locals 4 */
/* .line 2722 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateThermalStatsBeforeReported()V */
/* .line 2723 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z */
/* if-nez v0, :cond_0 */
/* .line 2724 */
v0 = this.mContext;
v1 = this.mThermalEventsMap;
/* const-string/jumbo v2, "thermal_aggregation" */
/* iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mExpId:I */
com.android.server.display.statistics.OneTrackUploaderHelper .reportAggregatedEventsToServer ( v0,v1,v2,v3 );
/* .line 2726 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2727 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "reportThermalEvents: mThermalEventsMap: "; // const-string v1, "reportThermalEvents: mThermalEventsMap: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mThermalEventsMap;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 2730 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->resetThermalEventsCache()V */
/* .line 2731 */
return;
} // .end method
private void resetAverageBrightnessInfo ( ) {
/* .locals 1 */
/* .line 1566 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessDuration:F */
/* .line 1567 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessIntegral:F */
/* .line 1568 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessDuration:F */
/* .line 1569 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessIntegral:F */
/* .line 1570 */
return;
} // .end method
private void resetBrightnessAnimInfo ( ) {
/* .locals 3 */
/* .line 1736 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentBrightnessAnimValue:F */
/* .line 1737 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTargetBrightnessAnimValue:F */
/* .line 1738 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStart:Z */
/* .line 1739 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStartTime:J */
/* .line 1740 */
int v2 = -1; // const/4 v2, -0x1
/* iput v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I */
/* .line 1741 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemporaryBrightnessTimeStamp:J */
/* .line 1742 */
return;
} // .end method
private void resetPendingParams ( ) {
/* .locals 2 */
/* .line 802 */
v0 = this.mPendingBrightnessChangeItem;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 803 */
/* iget v0, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I */
/* .line 804 */
/* .local v0, "type":I */
v1 = this.mPendingBrightnessChangeItem;
/* iget v1, v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F */
/* iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F */
/* .line 805 */
v1 = this.mPendingBrightnessChangeItem;
/* iget v1, v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F */
/* iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAmbientLux:F */
/* .line 806 */
v1 = this.mPendingBrightnessChangeItem;
/* iget v1, v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->mainLux:F */
/* iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastMainAmbientLux:F */
/* .line 807 */
v1 = this.mPendingBrightnessChangeItem;
/* iget v1, v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->assistLux:F */
/* iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAssistAmbientLux:F */
/* .line 809 */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerSensorByBrightnessType(IZ)V */
/* .line 810 */
int v1 = 0; // const/4 v1, 0x0
this.mPendingBrightnessChangeItem = v1;
/* .line 813 */
} // .end local v0 # "type":I
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->resetWindowOverrideParams()V */
/* .line 814 */
return;
} // .end method
private void resetStatsCache ( ) {
/* .locals 1 */
/* .line 1999 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->resetAverageBrightnessInfo()V */
/* .line 2000 */
v0 = this.mBrightnessEventsMap;
/* .line 2001 */
return;
} // .end method
private void resetThermalEventsCache ( ) {
/* .locals 1 */
/* .line 2737 */
v0 = this.mThermalEventsMap;
/* .line 2738 */
v0 = this.mDetailThermalUnrestrictedUsage;
(( android.util.SparseArray ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
/* .line 2739 */
v0 = this.mDetailThermalRestrictedUsage;
(( android.util.SparseArray ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
/* .line 2740 */
v0 = this.mThermalBrightnessRestrictedUsage;
(( android.util.SparseArray ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
/* .line 2741 */
return;
} // .end method
private void resetWindowOverrideParams ( ) {
/* .locals 2 */
/* .line 817 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHaveValidWindowBrightness:Z */
/* .line 818 */
/* const/high16 v1, -0x40800000 # -1.0f */
/* iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessOverrideFromWindow:F */
/* .line 819 */
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mWindowOverrideBrightnessChanging:Z */
/* .line 820 */
return;
} // .end method
private void scheduleUpdateBrightnessStatisticsData ( Boolean p0, Float p1 ) {
/* .locals 3 */
/* .param p1, "screenOn" # Z */
/* .param p2, "brightness" # F */
/* .line 1383 */
com.android.internal.os.SomeArgs .obtain ( );
/* .line 1384 */
/* .local v0, "args":Lcom/android/internal/os/SomeArgs; */
java.lang.Boolean .valueOf ( p1 );
this.arg1 = v1;
/* .line 1385 */
java.lang.Float .valueOf ( p2 );
this.arg2 = v1;
/* .line 1386 */
v1 = this.mBackgroundHandler;
int v2 = 7; // const/4 v2, 0x7
(( android.os.Handler ) v1 ).obtainMessage ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 1387 */
return;
} // .end method
private void setPointerEventListener ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "enable" # Z */
/* .line 554 */
v0 = this.mWms;
/* if-nez v0, :cond_0 */
/* .line 555 */
return;
/* .line 557 */
} // :cond_0
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
int v2 = 0; // const/4 v2, 0x0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 558 */
/* iget-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPointerEventListenerEnabled:Z */
/* if-nez v3, :cond_2 */
/* .line 559 */
v3 = this.mPointerEventListener;
(( com.android.server.wm.WindowManagerService ) v0 ).registerPointerEventListener ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Lcom/android/server/wm/WindowManagerService;->registerPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V
/* .line 561 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPointerEventListenerEnabled:Z */
/* .line 562 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 563 */
final String v0 = "register pointer event listener."; // const-string v0, "register pointer event listener."
android.util.Slog .d ( v1,v0 );
/* .line 567 */
} // :cond_1
/* iget-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPointerEventListenerEnabled:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 568 */
v3 = this.mPointerEventListener;
(( com.android.server.wm.WindowManagerService ) v0 ).unregisterPointerEventListener ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Lcom/android/server/wm/WindowManagerService;->unregisterPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V
/* .line 570 */
/* iput-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPointerEventListenerEnabled:Z */
/* .line 571 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 572 */
/* const-string/jumbo v0, "unregister pointer event listener." */
android.util.Slog .d ( v1,v0 );
/* .line 576 */
} // :cond_2
} // :goto_0
return;
} // .end method
private void setReportScheduleEventAlarm ( Boolean p0 ) {
/* .locals 14 */
/* .param p1, "init" # Z */
/* .line 429 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 430 */
/* .local v0, "now":J */
/* sget-boolean v2, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* const-wide/32 v3, 0x1d4c0 */
/* .line 431 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* const-wide/32 v3, 0x2932e00 */
} // :cond_1
/* const-wide/32 v3, 0x5265c00 */
} // :goto_0
/* nop */
/* .line 432 */
/* .local v3, "duration":J */
/* add-long v12, v0, v3 */
/* .line 433 */
/* .local v12, "nextTime":J */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 434 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "setReportSwitchStatAlarm: next time: " */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 435 */
android.util.TimeUtils .formatDuration ( v3,v4 );
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 434 */
final String v5 = "BrightnessDataProcessor"; // const-string v5, "BrightnessDataProcessor"
android.util.Slog .d ( v5,v2 );
/* .line 437 */
} // :cond_2
v5 = this.mAlarmManager;
int v6 = 2; // const/4 v6, 0x2
final String v9 = "report_switch_stats"; // const-string v9, "report_switch_stats"
v10 = this.mOnAlarmListener;
v11 = this.mBackgroundHandler;
/* move-wide v7, v12 */
/* invoke-virtual/range {v5 ..v11}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V */
/* .line 439 */
return;
} // .end method
private void setRotationListener ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "enable" # Z */
/* .line 579 */
final String v0 = "BrightnessDataProcessor"; // const-string v0, "BrightnessDataProcessor"
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 580 */
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mRotationListenerEnabled:Z */
/* if-nez v2, :cond_1 */
/* .line 581 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mRotationListenerEnabled:Z */
/* .line 582 */
v2 = this.mWms;
v3 = this.mRotationWatcher;
(( com.android.server.wm.WindowManagerService ) v2 ).watchRotation ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Lcom/android/server/wm/WindowManagerService;->watchRotation(Landroid/view/IRotationWatcher;I)I
/* .line 583 */
/* sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 584 */
final String v1 = "register rotation listener."; // const-string v1, "register rotation listener."
android.util.Slog .d ( v0,v1 );
/* .line 588 */
} // :cond_0
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mRotationListenerEnabled:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 589 */
/* iput-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mRotationListenerEnabled:Z */
/* .line 590 */
v1 = this.mWms;
v2 = this.mRotationWatcher;
(( com.android.server.wm.WindowManagerService ) v1 ).removeRotationWatcher ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowManagerService;->removeRotationWatcher(Landroid/view/IRotationWatcher;)V
/* .line 591 */
/* sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 592 */
/* const-string/jumbo v1, "unregister rotation listener." */
android.util.Slog .d ( v0,v1 );
/* .line 596 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void start ( ) {
/* .locals 3 */
/* .line 407 */
/* new-instance v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V */
this.mPointerEventListener = v0;
/* .line 408 */
v0 = this.mContext;
com.android.server.display.statistics.SwitchStatsHelper .getInstance ( v0 );
this.mSwitchStatsHelper = v0;
/* .line 409 */
/* new-instance v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$TaskStackListenerImpl; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$TaskStackListenerImpl;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V */
this.mTaskStackListener = v0;
/* .line 410 */
/* new-instance v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$RotationWatcher; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$RotationWatcher;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V */
this.mRotationWatcher = v0;
/* .line 411 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 412 */
/* const v1, 0x11070035 */
v0 = (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mMinOutDoorHighTempNit:F */
/* .line 413 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mStartTimeStamp:J */
/* .line 414 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setRotationListener(Z)V */
/* .line 415 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setPointerEventListener(Z)V */
/* .line 416 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerForegroundAppUpdater()V */
/* .line 417 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerScreenStateReceiver()V */
/* .line 418 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setReportScheduleEventAlarm(Z)V */
/* .line 419 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver; */
v2 = this.mBackgroundHandler;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Landroid/os/Handler;)V */
this.mSettingsObserver = v1;
/* .line 420 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mContentResolver = v1;
/* .line 421 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerSettingsObserver()V */
/* .line 422 */
v1 = this.mSensorManager;
int v2 = 5; // const/4 v2, 0x5
(( android.hardware.SensorManager ) v1 ).getDefaultSensor ( v2 ); // invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mLightSensor = v1;
/* .line 423 */
v1 = this.mSensorManager;
(( android.hardware.SensorManager ) v1 ).getDefaultSensor ( v0 ); // invoke-virtual {v1, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mAccSensor = v0;
/* .line 424 */
v0 = this.mSensorManager;
/* const v1, 0x1fa2a8f */
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mLightFovSensor = v0;
/* .line 425 */
/* new-instance v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener-IA;)V */
this.mSensorListener = v0;
/* .line 426 */
return;
} // .end method
private void startHdyUsageStats ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isHdrLayer" # Z */
/* .line 2605 */
v0 = this.mBackgroundHandler;
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda9; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda9;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2608 */
return;
} // .end method
private void statsAverageValues ( com.android.server.display.statistics.AggregationEvent p0, java.lang.Object p1, java.lang.Float p2 ) {
/* .locals 6 */
/* .param p1, "event" # Lcom/android/server/display/statistics/AggregationEvent; */
/* .param p2, "key" # Ljava/lang/Object; */
/* .param p3, "value" # Ljava/lang/Float; */
/* .line 1951 */
(( com.android.server.display.statistics.AggregationEvent ) p1 ).getQuotaEvents ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;
/* .line 1952 */
/* .local v0, "quotaEvents":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;" */
(( com.android.server.display.statistics.AggregationEvent ) p1 ).getCacheDataMap ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/AggregationEvent;->getCacheDataMap()Ljava/util/Map;
/* .line 1953 */
/* .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/util/List<Ljava/lang/Float;>;>;" */
/* check-cast v2, Ljava/util/List; */
/* .line 1954 */
/* .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;" */
int v3 = 0; // const/4 v3, 0x0
/* .line 1955 */
/* .local v3, "sum":F */
/* if-nez v2, :cond_0 */
/* .line 1956 */
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
/* move-object v2, v4 */
/* .line 1958 */
} // :cond_0
/* .line 1959 */
/* .line 1960 */
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_1
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
/* .line 1961 */
/* .local v5, "v":F */
/* add-float/2addr v3, v5 */
/* .line 1962 */
} // .end local v5 # "v":F
/* .line 1963 */
v4 = } // :cond_1
/* int-to-float v4, v4 */
/* div-float v4, v3, v4 */
java.lang.Float .valueOf ( v4 );
/* .line 1964 */
return;
} // .end method
private void statsSummary ( java.util.Map p0, java.lang.Object p1, java.lang.Object p2 ) {
/* .locals 1 */
/* .param p2, "key" # Ljava/lang/Object; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* ">;", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 1917 */
/* .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;" */
/* .line 1918 */
/* .local v0, "totalValues":Ljava/lang/Object; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1919 */
/* invoke-direct {p0, v0, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getSumValues(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; */
/* .line 1921 */
} // :cond_0
/* .line 1922 */
return;
} // .end method
private void updateBrightnessAnimInfo ( Float p0, Float p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "currentBrightnessAnim" # F */
/* .param p2, "targetBrightnessAnim" # F */
/* .param p3, "begin" # Z */
/* .line 1636 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1637 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentBrightnessAnimValue:F */
/* .line 1638 */
/* iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTargetBrightnessAnimValue:F */
/* .line 1639 */
/* iput-boolean p3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStart:Z */
/* .line 1640 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStartTime:J */
/* .line 1641 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessChangedState(FF)I */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I */
/* .line 1643 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1644 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateBrightnessAnimInfo: mCurrentAnimateValue:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentBrightnessAnimValue:F */
/* .line 1645 */
v1 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mTargetAnimateValue:"; // const-string v1, ", mTargetAnimateValue:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTargetBrightnessAnimValue:F */
/* .line 1647 */
v1 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mAnimationStart:"; // const-string v1, ", mAnimationStart:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStart:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", mAnimationStartTime:"; // const-string v1, ", mAnimationStartTime:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStartTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", mBrightnessChangedState = "; // const-string v1, ", mBrightnessChangedState = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1644 */
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 1653 */
} // :cond_0
return;
} // .end method
private void updateBrightnessStatisticsData ( Boolean p0, Float p1 ) {
/* .locals 4 */
/* .param p1, "screenOn" # Z */
/* .param p2, "brightness" # F */
/* .line 1395 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 1396 */
/* .local v0, "now":J */
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mScreenOn:Z */
/* if-eq v2, p1, :cond_1 */
/* .line 1397 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mScreenOn:Z */
/* .line 1398 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1399 */
/* const-wide/16 v2, 0x0 */
/* iput-wide v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenOnTimeStamp:J */
/* .line 1401 */
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F */
int v3 = 0; // const/4 v3, 0x0
/* cmpl-float v2, v2, v3 */
/* if-nez v2, :cond_1 */
/* .line 1402 */
/* iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F */
/* .line 1405 */
} // :cond_0
/* const/high16 v2, 0x7fc00000 # Float.NaN */
/* iput v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastStoreBrightness:F */
/* .line 1406 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->computeLastAverageBrightness(J)V */
/* .line 1409 */
} // :cond_1
} // :goto_0
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 1410 */
/* invoke-direct {p0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->computeAverageBrightnessIfNeeded(F)V */
/* .line 1412 */
} // :cond_2
return;
} // .end method
private void updateBrightnessUsage ( Integer p0, Boolean p1, java.lang.String p2 ) {
/* .locals 6 */
/* .param p1, "spanIndex" # I */
/* .param p2, "dueToScreenOff" # Z */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 920 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->isValidStartTimeStamp()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 921 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 922 */
/* .local v0, "now":J */
/* iget-wide v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mStartTimeStamp:J */
/* sub-long v2, v0, v2 */
/* long-to-float v2, v2 */
/* const/high16 v3, 0x447a0000 # 1000.0f */
/* div-float/2addr v2, v3 */
/* .line 923 */
/* .local v2, "duration":F */
java.lang.Integer .valueOf ( p1 );
java.lang.Float .valueOf ( v2 );
final String v5 = "brightness_usage"; // const-string v5, "brightness_usage"
/* invoke-direct {p0, v5, v3, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 925 */
/* int-to-float v3, p1 */
/* invoke-direct {p0, v3, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteHbmUsage(FF)V */
/* .line 926 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 927 */
/* const-wide/16 v3, 0x0 */
/* iput-wide v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mStartTimeStamp:J */
/* .line 929 */
} // :cond_0
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mStartTimeStamp:J */
/* .line 931 */
} // :goto_0
/* sget-boolean v3, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 932 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "updateBrightnessUsage: reason: " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", span: "; // const-string v4, ", span: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", append duration: "; // const-string v4, ", append duration: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = "s"; // const-string v4, "s"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "BrightnessDataProcessor"; // const-string v4, "BrightnessDataProcessor"
android.util.Slog .d ( v4,v3 );
/* .line 937 */
} // .end local v0 # "now":J
} // .end local v2 # "duration":F
} // :cond_1
return;
} // .end method
private void updateBrightnessUsageIfNeeded ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "curSpanIndex" # I */
/* .param p2, "preSpanIndex" # I */
/* .line 903 */
/* if-eq p1, p2, :cond_0 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->isValidStartTimeStamp()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 904 */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "brightness span change"; // const-string v1, "brightness span change"
/* invoke-direct {p0, p2, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateBrightnessUsage(IZLjava/lang/String;)V */
/* .line 907 */
} // :cond_0
return;
} // .end method
private void updateForegroundApps ( ) {
/* .locals 3 */
/* .line 840 */
try { // :try_start_0
v0 = this.mActivityTaskManager;
/* .line 841 */
/* .local v0, "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
if ( v0 != null) { // if-eqz v0, :cond_4
v1 = this.topActivity;
/* if-nez v1, :cond_0 */
/* .line 845 */
} // :cond_0
v1 = (( android.app.ActivityTaskManager$RootTaskInfo ) v0 ).getWindowingMode ( ); // invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I
int v2 = 5; // const/4 v2, 0x5
/* if-eq v1, v2, :cond_3 */
/* .line 846 */
v1 = (( android.app.ActivityTaskManager$RootTaskInfo ) v0 ).getWindowingMode ( ); // invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I
int v2 = 6; // const/4 v2, 0x6
/* if-eq v1, v2, :cond_3 */
v1 = this.mActivityTaskManager;
v1 = /* .line 847 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 851 */
} // :cond_1
v1 = this.topActivity;
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 854 */
/* .local v1, "packageName":Ljava/lang/String; */
v2 = this.mForegroundAppPackageName;
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 855 */
v2 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 856 */
return;
/* .line 858 */
} // :cond_2
/* iget v2, v0, Landroid/app/ActivityTaskManager$RootTaskInfo;->userId:I */
/* iput v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentUserId:I */
/* .line 859 */
this.mForegroundAppPackageName = v1;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 862 */
} // .end local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
} // .end local v1 # "packageName":Ljava/lang/String;
/* .line 848 */
/* .restart local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
} // :cond_3
} // :goto_0
return;
/* .line 842 */
} // :cond_4
} // :goto_1
return;
/* .line 860 */
} // .end local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
/* :catch_0 */
/* move-exception v0 */
/* .line 863 */
} // :goto_2
return;
} // .end method
private void updateInterruptBrightnessAnimDuration ( Integer p0, Float p1 ) {
/* .locals 13 */
/* .param p1, "type" # I */
/* .param p2, "brightness" # F */
/* .line 1672 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* if-nez v0, :cond_0 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentBrightnessAnimValue:F */
/* .line 1674 */
v0 = /* invoke-direct {p0, v0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessChangedState(FF)I */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I */
/* if-ne v0, v2, :cond_2 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentBrightnessAnimValue:F */
/* .line 1677 */
v0 = /* invoke-direct {p0, v0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessChangedState(FF)I */
/* if-ne v0, v2, :cond_2 */
} // :cond_1
/* move v0, v2 */
} // :cond_2
/* move v0, v1 */
/* .line 1681 */
/* .local v0, "isSameAdjustment":Z */
} // :goto_0
int v3 = 2; // const/4 v3, 0x2
/* if-ne p1, v3, :cond_5 */
/* .line 1682 */
/* iget-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStart:Z */
/* const-wide/16 v4, 0x0 */
if ( v3 != null) { // if-eqz v3, :cond_4
if ( v0 != null) { // if-eqz v0, :cond_4
/* iget-wide v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemporaryBrightnessTimeStamp:J */
/* cmp-long v3, v6, v4 */
if ( v3 != null) { // if-eqz v3, :cond_4
/* iget-wide v8, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStartTime:J */
/* cmp-long v3, v8, v4 */
if ( v3 != null) { // if-eqz v3, :cond_4
/* cmp-long v3, v6, v8 */
/* if-lez v3, :cond_4 */
/* .line 1685 */
/* sub-long/2addr v6, v8 */
/* .line 1686 */
/* .local v6, "duration":J */
/* nop */
/* .line 1687 */
java.lang.Integer .valueOf ( v2 );
/* .line 1686 */
final String v8 = "interrupt_animation_times"; // const-string v8, "interrupt_animation_times"
final String v9 = "interrupt_times"; // const-string v9, "interrupt_times"
/* invoke-direct {p0, v8, v9, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAdvancedBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1688 */
/* iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
int v8 = 3; // const/4 v8, 0x3
/* if-ne v3, v8, :cond_3 */
/* move v3, v2 */
} // :cond_3
/* move v3, v1 */
/* .line 1690 */
/* .local v3, "brightnessRestricted":Z */
} // :goto_1
/* new-instance v8, Lcom/android/server/display/statistics/AdvancedEvent; */
/* invoke-direct {v8}, Lcom/android/server/display/statistics/AdvancedEvent;-><init>()V */
/* .line 1691 */
/* .local v8, "event":Lcom/android/server/display/statistics/AdvancedEvent; */
(( com.android.server.display.statistics.AdvancedEvent ) v8 ).setEventType ( v2 ); // invoke-virtual {v8, v2}, Lcom/android/server/display/statistics/AdvancedEvent;->setEventType(I)Lcom/android/server/display/statistics/AdvancedEvent;
/* long-to-float v9, v6 */
/* const v10, 0x3a83126f # 0.001f */
/* mul-float/2addr v9, v10 */
/* .line 1692 */
(( com.android.server.display.statistics.AdvancedEvent ) v2 ).setAutoBrightnessAnimationDuration ( v9 ); // invoke-virtual {v2, v9}, Lcom/android/server/display/statistics/AdvancedEvent;->setAutoBrightnessAnimationDuration(F)Lcom/android/server/display/statistics/AdvancedEvent;
/* iget v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentBrightnessAnimValue:F */
/* .line 1693 */
v9 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( v9 );
(( com.android.server.display.statistics.AdvancedEvent ) v2 ).setCurrentAnimateValue ( v9 ); // invoke-virtual {v2, v9}, Lcom/android/server/display/statistics/AdvancedEvent;->setCurrentAnimateValue(I)Lcom/android/server/display/statistics/AdvancedEvent;
/* iget v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTargetBrightnessAnimValue:F */
/* .line 1694 */
v9 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( v9 );
(( com.android.server.display.statistics.AdvancedEvent ) v2 ).setTargetAnimateValue ( v9 ); // invoke-virtual {v2, v9}, Lcom/android/server/display/statistics/AdvancedEvent;->setTargetAnimateValue(I)Lcom/android/server/display/statistics/AdvancedEvent;
/* .line 1695 */
v9 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( p2 );
(( com.android.server.display.statistics.AdvancedEvent ) v2 ).setUserBrightness ( v9 ); // invoke-virtual {v2, v9}, Lcom/android/server/display/statistics/AdvancedEvent;->setUserBrightness(I)Lcom/android/server/display/statistics/AdvancedEvent;
/* iget v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I */
/* .line 1696 */
(( com.android.server.display.statistics.AdvancedEvent ) v2 ).setBrightnessChangedState ( v9 ); // invoke-virtual {v2, v9}, Lcom/android/server/display/statistics/AdvancedEvent;->setBrightnessChangedState(I)Lcom/android/server/display/statistics/AdvancedEvent;
/* .line 1697 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v11 */
(( com.android.server.display.statistics.AdvancedEvent ) v2 ).setTimeStamp ( v11, v12 ); // invoke-virtual {v2, v11, v12}, Lcom/android/server/display/statistics/AdvancedEvent;->setTimeStamp(J)Lcom/android/server/display/statistics/AdvancedEvent;
/* .line 1698 */
(( com.android.server.display.statistics.AdvancedEvent ) v2 ).setBrightnessRestrictedEnable ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/display/statistics/AdvancedEvent;->setBrightnessRestrictedEnable(Z)Lcom/android/server/display/statistics/AdvancedEvent;
/* .line 1699 */
/* invoke-direct {p0, v8}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportEventToServer(Lcom/android/server/display/statistics/AdvancedEvent;)V */
/* .line 1700 */
/* sget-boolean v2, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1701 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "updateInterruptBrightnessAnimDuration: duration:" */
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* long-to-float v9, v6 */
/* mul-float/2addr v9, v10 */
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v9 = ", currentAnimateValue:"; // const-string v9, ", currentAnimateValue:"
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentBrightnessAnimValue:F */
/* .line 1702 */
v9 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( v9 );
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ", targetAnimateValue:"; // const-string v9, ", targetAnimateValue:"
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTargetBrightnessAnimValue:F */
/* .line 1703 */
v9 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( v9 );
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ", userBrightness:"; // const-string v9, ", userBrightness:"
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1704 */
v9 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( p2 );
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ", mBrightnessChangedState:"; // const-string v9, ", mBrightnessChangedState:"
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I */
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1701 */
final String v9 = "BrightnessDataProcessor"; // const-string v9, "BrightnessDataProcessor"
android.util.Slog .d ( v9,v2 );
/* .line 1708 */
} // .end local v3 # "brightnessRestricted":Z
} // .end local v6 # "duration":J
} // .end local v8 # "event":Lcom/android/server/display/statistics/AdvancedEvent;
} // :cond_4
/* iput-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStart:Z */
/* .line 1709 */
/* iput-wide v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemporaryBrightnessTimeStamp:J */
/* .line 1711 */
} // :cond_5
return;
} // .end method
private void updateInterruptBrightnessAnimDurationIfNeeded ( Integer p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .param p2, "brightness" # F */
/* .line 1656 */
v0 = this.mBackgroundHandler;
/* const/16 v1, 0xa */
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 1657 */
/* .local v0, "msg":Landroid/os/Message; */
/* iput p1, v0, Landroid/os/Message;->arg1:I */
/* .line 1658 */
java.lang.Float .valueOf ( p2 );
this.obj = v1;
/* .line 1659 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 1660 */
return;
} // .end method
private void updatePointerEventMotionState ( Boolean p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "dragging" # Z */
/* .param p2, "distanceX" # I */
/* .param p3, "distanceY" # I */
/* .line 874 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mUserDragging:Z */
/* if-eq v0, p1, :cond_0 */
/* .line 875 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mUserDragging:Z */
/* .line 876 */
/* if-nez p1, :cond_0 */
v0 = /* invoke-direct {p0, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->checkIsValidMotionForWindowBrightness(II)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 877 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLatestDraggingChangedTime:J */
/* .line 878 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHaveValidWindowBrightness:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 879 */
/* const-wide/16 v0, 0xbb8 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->debounceBrightnessEvent(J)V */
/* .line 883 */
} // :cond_0
return;
} // .end method
private void updateScreenNits ( Float p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "originalNit" # F */
/* .param p2, "actualNit" # F */
/* .line 2876 */
v0 = this.mBackgroundHandler;
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda12; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda12;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;FF)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2884 */
return;
} // .end method
private void updateScreenStateChanged ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "screenOn" # Z */
/* .line 940 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateStartTimeStamp(Z)V */
/* .line 941 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setPointerEventListener(Z)V */
/* .line 942 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setRotationListener(Z)V */
/* .line 943 */
return;
} // .end method
private void updateStartTimeStamp ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "screenOn" # Z */
/* .line 946 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 947 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateStartTimeStamp: screenOn: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 949 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->isValidStartTimeStamp()Z */
/* if-nez v0, :cond_1 */
/* .line 950 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mStartTimeStamp:J */
/* .line 951 */
} // :cond_1
/* if-nez p1, :cond_2 */
/* .line 952 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I */
int v1 = 1; // const/4 v1, 0x1
final String v2 = "screen off"; // const-string v2, "screen off"
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateBrightnessUsage(IZLjava/lang/String;)V */
/* .line 955 */
} // :cond_2
} // :goto_0
return;
} // .end method
private void updateThermalStatsBeforeReported ( ) {
/* .locals 10 */
/* .line 2700 */
/* iget-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2701 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v8 */
/* .line 2702 */
/* .local v8, "now":J */
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F */
/* const/high16 v3, -0x40800000 # -1.0f */
/* iget-boolean v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTempState:Z */
int v7 = 1; // const/4 v7, 0x1
/* move-object v1, p0 */
/* move-wide v4, v8 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteOutDoorHighTempUsage(FFJZZ)V */
/* .line 2703 */
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F */
/* iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F */
int v6 = 0; // const/4 v6, 0x0
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalBrightnessRestrictedUsage(FFJZ)V */
/* .line 2704 */
int v2 = 1; // const/4 v2, 0x1
/* iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I */
/* iget v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
int v7 = 0; // const/4 v7, 0x0
/* move-wide v5, v8 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteDetailThermalUsage(IIFJZ)V */
/* .line 2706 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v0, v8, v9, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalUsage(IJZ)V */
/* .line 2708 */
v0 = this.mThermalEventsMap;
v0 = /* const-string/jumbo v1, "thermal_average_temperature" */
/* if-nez v0, :cond_0 */
/* .line 2709 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F */
java.lang.Float .valueOf ( v0 );
final String v2 = "average"; // const-string v2, "average"
/* invoke-direct {p0, v1, v2, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsAvg(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2711 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2712 */
final String v0 = "BrightnessDataProcessor"; // const-string v0, "BrightnessDataProcessor"
/* const-string/jumbo v1, "updateThermalStatsBeforeReported: update stats before reporting stats." */
android.util.Slog .d ( v0,v1 );
/* .line 2715 */
} // .end local v8 # "now":J
} // :cond_1
return;
} // .end method
private void updateUserResetAutoBrightnessModeTimes ( ) {
/* .locals 8 */
/* .line 1525 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z */
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightnessEnable:Z */
/* if-ne v0, v1, :cond_0 */
/* .line 1526 */
return;
/* .line 1528 */
} // :cond_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 1529 */
/* .local v0, "now":J */
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z */
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_1
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsValidResetAutoBrightnessMode:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* iget-wide v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastResetBrightnessModeTime:J */
/* const-wide/16 v6, 0x0 */
/* cmp-long v2, v4, v6 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* sub-long v4, v0, v4 */
/* const-wide/16 v6, 0xbb8 */
/* cmp-long v2, v4, v6 */
/* if-gtz v2, :cond_1 */
/* .line 1531 */
final String v2 = "reset_times"; // const-string v2, "reset_times"
java.lang.Integer .valueOf ( v3 );
final String v5 = "reset_brightness_mode_times"; // const-string v5, "reset_brightness_mode_times"
/* invoke-direct {p0, v5, v2, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAdvancedBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1532 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->flareStatisticalResetBrightnessModeTimes()V */
/* .line 1536 */
} // :cond_1
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightnessEnable:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z */
/* if-nez v2, :cond_2 */
} // :cond_2
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* iput-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsValidResetAutoBrightnessMode:Z */
/* .line 1537 */
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z */
/* iput-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightnessEnable:Z */
/* .line 1538 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastResetBrightnessModeTime:J */
/* .line 1539 */
/* sget-boolean v2, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1540 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateUserResetAutoBrightnessModeTimes: mAdvancedBrightnessEventsMap: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mAdvancedBrightnessEventsMap;
/* .line 1541 */
(( java.lang.Object ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1540 */
final String v3 = "BrightnessDataProcessor"; // const-string v3, "BrightnessDataProcessor"
android.util.Slog .d ( v3,v2 );
/* .line 1543 */
} // :cond_3
return;
} // .end method
/* # virtual methods */
public void aggregateCbmBrightnessAdjustTimes ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "cbmState" # I */
/* .param p2, "isManuallySet" # Z */
/* .line 2966 */
com.android.server.display.statistics.AggregationEvent$CbmAggregationEvent .getCbmAutoAdjustQuotaName ( p1,p2 );
/* .line 2967 */
/* .local v0, "name":Ljava/lang/String; */
int v1 = 1; // const/4 v1, 0x1
java.lang.Integer .valueOf ( v1 );
final String v2 = "custom_brightness_adj"; // const-string v2, "custom_brightness_adj"
/* invoke-direct {p0, v2, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2968 */
/* sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2969 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "aggregateCbmBrightnessAdjustTimes: mCbmEventsMap: "; // const-string v2, "aggregateCbmBrightnessAdjustTimes: mCbmEventsMap: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mCbmEventsMap;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BrightnessDataProcessor"; // const-string v2, "BrightnessDataProcessor"
android.util.Slog .d ( v2,v1 );
/* .line 2971 */
} // :cond_0
return;
} // .end method
public void aggregateCbmBrightnessUsageDuration ( Float p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "duration" # F */
/* .param p2, "cbmSate" # I */
/* .line 2979 */
/* const/high16 v0, 0x447a0000 # 1000.0f */
/* div-float/2addr p1, v0 */
/* .line 2980 */
com.android.server.display.statistics.AggregationEvent$CbmAggregationEvent .getCbmBrtUsageQuotaName ( p2 );
/* .line 2981 */
/* .local v0, "name":Ljava/lang/String; */
final String v1 = "custom_brightness_usage"; // const-string v1, "custom_brightness_usage"
java.lang.Float .valueOf ( p1 );
/* invoke-direct {p0, v1, v0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2982 */
/* sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2983 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "aggregateCbmBrightnessUsageDuration: duration: "; // const-string v2, "aggregateCbmBrightnessUsageDuration: duration: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", mCbmEventsMap: "; // const-string v2, ", mCbmEventsMap: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mCbmEventsMap;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BrightnessDataProcessor"; // const-string v2, "BrightnessDataProcessor"
android.util.Slog .d ( v2,v1 );
/* .line 2986 */
} // :cond_0
return;
} // .end method
public void aggregateIndividualModelTrainTimes ( ) {
/* .locals 3 */
/* .line 2992 */
int v0 = 1; // const/4 v0, 0x1
java.lang.Integer .valueOf ( v0 );
final String v1 = "individual_model_train"; // const-string v1, "individual_model_train"
final String v2 = "model_train_count"; // const-string v2, "model_train_count"
/* invoke-direct {p0, v1, v2, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 2993 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2994 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "aggregateIndividualModelTrainTimes: mCbmEventsMap: "; // const-string v1, "aggregateIndividualModelTrainTimes: mCbmEventsMap: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCbmEventsMap;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 2996 */
} // :cond_0
return;
} // .end method
public void aggregateIndividualModelTrainTimes ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isSuccessful" # Z */
/* .line 3003 */
v0 = this.mBackgroundHandler;
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 3012 */
return;
} // .end method
public void aggregateModelAvgPredictDuration ( Float p0 ) {
/* .locals 3 */
/* .param p1, "duration" # F */
/* .line 3032 */
final String v0 = "model_predict_average_duration"; // const-string v0, "model_predict_average_duration"
java.lang.Float .valueOf ( p1 );
final String v2 = "individual_model_predict"; // const-string v2, "individual_model_predict"
/* invoke-direct {p0, v2, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmEventsAvg(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 3033 */
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 3034 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "aggregateModelAvgPredictDuration: duration: "; // const-string v1, "aggregateModelAvgPredictDuration: duration: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", mCbmEventsMap: "; // const-string v1, ", mCbmEventsMap: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCbmEventsMap;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 3037 */
} // :cond_0
return;
} // .end method
public void aggregateModelPredictTimeoutTimes ( ) {
/* .locals 2 */
/* .line 3018 */
v0 = this.mBackgroundHandler;
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 3025 */
return;
} // .end method
public void aggregateModelTrainIndicators ( com.xiaomi.aiautobrt.IndividualTrainEvent p0 ) {
/* .locals 6 */
/* .param p1, "event" # Lcom/xiaomi/aiautobrt/IndividualTrainEvent; */
/* .line 3045 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 3046 */
/* .local v0, "indicatorsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;" */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
/* .line 3048 */
/* .local v1, "now":J */
v3 = (( com.xiaomi.aiautobrt.IndividualTrainEvent ) p1 ).getTrainSampleNum ( ); // invoke-virtual {p1}, Lcom/xiaomi/aiautobrt/IndividualTrainEvent;->getTrainSampleNum()I
java.lang.Integer .valueOf ( v3 );
final String v4 = "sample_num"; // const-string v4, "sample_num"
/* .line 3049 */
v3 = (( com.xiaomi.aiautobrt.IndividualTrainEvent ) p1 ).getModelTrainLoss ( ); // invoke-virtual {p1}, Lcom/xiaomi/aiautobrt/IndividualTrainEvent;->getModelTrainLoss()F
java.lang.Float .valueOf ( v3 );
/* const-string/jumbo v4, "train_loss" */
/* .line 3050 */
v3 = (( com.xiaomi.aiautobrt.IndividualTrainEvent ) p1 ).getPreModelMAE ( ); // invoke-virtual {p1}, Lcom/xiaomi/aiautobrt/IndividualTrainEvent;->getPreModelMAE()F
java.lang.Float .valueOf ( v3 );
final String v4 = "pre_train_mae"; // const-string v4, "pre_train_mae"
/* .line 3051 */
v3 = (( com.xiaomi.aiautobrt.IndividualTrainEvent ) p1 ).getCurModelMAE ( ); // invoke-virtual {p1}, Lcom/xiaomi/aiautobrt/IndividualTrainEvent;->getCurModelMAE()F
java.lang.Float .valueOf ( v3 );
final String v4 = "cur_train_mae"; // const-string v4, "cur_train_mae"
/* .line 3052 */
v3 = (( com.xiaomi.aiautobrt.IndividualTrainEvent ) p1 ).getPreModelMAPE ( ); // invoke-virtual {p1}, Lcom/xiaomi/aiautobrt/IndividualTrainEvent;->getPreModelMAPE()F
java.lang.Float .valueOf ( v3 );
final String v4 = "pre_train_mape"; // const-string v4, "pre_train_mape"
/* .line 3053 */
v3 = (( com.xiaomi.aiautobrt.IndividualTrainEvent ) p1 ).getCurModelMAPE ( ); // invoke-virtual {p1}, Lcom/xiaomi/aiautobrt/IndividualTrainEvent;->getCurModelMAPE()F
java.lang.Float .valueOf ( v3 );
final String v4 = "cur_train_mape"; // const-string v4, "cur_train_mape"
/* .line 3054 */
/* const-string/jumbo v3, "timestamp" */
com.android.server.display.statistics.BrightnessEvent .timestamp2String ( v1,v2 );
/* .line 3055 */
v3 = this.mModelTrainIndicatorsList;
/* .line 3056 */
final String v3 = "model_train_indicators"; // const-string v3, "model_train_indicators"
v4 = this.mModelTrainIndicatorsList;
final String v5 = "individual_model_train"; // const-string v5, "individual_model_train"
/* invoke-direct {p0, v5, v3, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 3057 */
/* sget-boolean v3, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 3058 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "aggregateModelTrainIndicators: mCbmEventsMap: "; // const-string v4, "aggregateModelTrainIndicators: mCbmEventsMap: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mCbmEventsMap;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "BrightnessDataProcessor"; // const-string v4, "BrightnessDataProcessor"
android.util.Slog .d ( v4,v3 );
/* .line 3060 */
} // :cond_0
return;
} // .end method
public com.xiaomi.aiautobrt.IndividualModelEvent createModelEvent ( Float p0, Integer p1, Float p2, Integer p3, Integer p4 ) {
/* .locals 9 */
/* .param p1, "lux" # F */
/* .param p2, "categoryId" # I */
/* .param p3, "newBrightness" # F */
/* .param p4, "orientation" # I */
/* .param p5, "sceneState" # I */
/* .line 721 */
v0 = this.mDisplayDeviceConfig;
v0 = (( com.android.server.display.DisplayDeviceConfig ) v0 ).getNitsFromBacklight ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
/* .line 723 */
/* .local v0, "currentNit":F */
v1 = /* invoke-direct {p0, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I */
/* int-to-float v1, v1 */
/* .line 725 */
/* .local v1, "currentNitSpan":F */
v2 = /* invoke-direct {p0, p1, p5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getConfigBrightness(FI)F */
/* .line 727 */
/* .local v2, "defaultConfigNit":F */
v3 = /* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getAmbientLuxSpanIndex(F)I */
/* int-to-float v3, v3 */
/* .line 729 */
/* .local v3, "currentLuxSpan":F */
v4 = this.mIndividualEventNormalizer;
v4 = (( com.android.server.display.aiautobrt.IndividualEventNormalizer ) v4 ).getMixedOrientApp ( p2, p4 ); // invoke-virtual {v4, p2, p4}, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->getMixedOrientApp(II)F
/* .line 731 */
/* .local v4, "mixedOrientationApp":F */
/* new-instance v5, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder; */
/* invoke-direct {v5}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;-><init>()V */
/* .line 732 */
/* .local v5, "builder":Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder; */
(( com.xiaomi.aiautobrt.IndividualModelEvent$Builder ) v5 ).setCurrentBrightness ( v0 ); // invoke-virtual {v5, v0}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setCurrentBrightness(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;
/* .line 733 */
(( com.xiaomi.aiautobrt.IndividualModelEvent$Builder ) v6 ).setCurrentBrightnessSpan ( v1 ); // invoke-virtual {v6, v1}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setCurrentBrightnessSpan(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;
/* .line 734 */
(( com.xiaomi.aiautobrt.IndividualModelEvent$Builder ) v6 ).setAmbientLux ( p1 ); // invoke-virtual {v6, p1}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setAmbientLux(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;
/* .line 735 */
(( com.xiaomi.aiautobrt.IndividualModelEvent$Builder ) v6 ).setAmbientLuxSpan ( v3 ); // invoke-virtual {v6, v3}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setAmbientLuxSpan(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;
/* .line 736 */
(( com.xiaomi.aiautobrt.IndividualModelEvent$Builder ) v6 ).setDefaultConfigBrightness ( v2 ); // invoke-virtual {v6, v2}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setDefaultConfigBrightness(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;
/* int-to-float v7, p2 */
/* .line 737 */
(( com.xiaomi.aiautobrt.IndividualModelEvent$Builder ) v6 ).setAppCategoryId ( v7 ); // invoke-virtual {v6, v7}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setAppCategoryId(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;
/* int-to-float v7, p4 */
/* .line 738 */
(( com.xiaomi.aiautobrt.IndividualModelEvent$Builder ) v6 ).setOrientation ( v7 ); // invoke-virtual {v6, v7}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setOrientation(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;
/* .line 739 */
(( com.xiaomi.aiautobrt.IndividualModelEvent$Builder ) v6 ).setMixedOrientationAppId ( v4 ); // invoke-virtual {v6, v4}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setMixedOrientationAppId(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;
/* .line 740 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v7 */
(( com.xiaomi.aiautobrt.IndividualModelEvent$Builder ) v6 ).setTimeStamp ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setTimeStamp(J)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;
/* .line 741 */
(( com.xiaomi.aiautobrt.IndividualModelEvent$Builder ) v5 ).build ( ); // invoke-virtual {v5}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->build()Lcom/xiaomi/aiautobrt/IndividualModelEvent;
} // .end method
public com.xiaomi.aiautobrt.IndividualModelEvent createModelEvent ( Float p0, java.lang.String p1, Float p2, Integer p3, Integer p4 ) {
/* .locals 7 */
/* .param p1, "lux" # F */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "newBrightness" # F */
/* .param p4, "orientation" # I */
/* .param p5, "sceneState" # I */
/* .line 714 */
v0 = this.mAppClassifier;
v0 = (( com.android.server.display.aiautobrt.AppClassifier ) v0 ).getAppCategoryId ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/display/aiautobrt/AppClassifier;->getAppCategoryId(Ljava/lang/String;)I
/* .line 715 */
/* .local v0, "categoryId":I */
/* move-object v1, p0 */
/* move v2, p1 */
/* move v3, v0 */
/* move v4, p3 */
/* move v5, p4 */
/* move v6, p5 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->createModelEvent(FIFII)Lcom/xiaomi/aiautobrt/IndividualModelEvent; */
} // .end method
public void forceReportTrainDataEnabled ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 2908 */
v0 = this.mBackgroundHandler;
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda7; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2909 */
return;
} // .end method
public void noteAverageTemperature ( Float p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "temperature" # F */
/* .param p2, "needComputed" # Z */
/* .line 2573 */
v0 = this.mBackgroundHandler;
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0, p2, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;ZF)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2584 */
return;
} // .end method
public void noteDetailThermalUsage ( Integer p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "conditionId" # I */
/* .param p2, "temperature" # F */
/* .line 2330 */
v0 = this.mBackgroundHandler;
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda10; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda10;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;IF)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2388 */
return;
} // .end method
public void noteFullSceneThermalUsageStats ( Float p0, Float p1, Integer p2, Float p3, Boolean p4 ) {
/* .locals 9 */
/* .param p1, "brightness" # F */
/* .param p2, "thermalBrightness" # F */
/* .param p3, "conditionId" # I */
/* .param p4, "temperature" # F */
/* .param p5, "outdoorHighTemState" # Z */
/* .line 2078 */
v0 = this.mBackgroundHandler;
/* new-instance v8, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda8; */
/* move-object v1, v8 */
/* move-object v2, p0 */
/* move v3, p2 */
/* move v4, p1 */
/* move v5, p5 */
/* move v6, p3 */
/* move v7, p4 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda8;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;FFZIF)V */
(( android.os.Handler ) v0 ).post ( v8 ); // invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2098 */
return;
} // .end method
public void notifyAonFlareEvents ( Integer p0, Float p1 ) {
/* .locals 7 */
/* .param p1, "type" # I */
/* .param p2, "preLux" # F */
/* .line 3078 */
/* const-wide/32 v0, 0xea60 */
final String v2 = "flare_scene_check_times"; // const-string v2, "flare_scene_check_times"
/* const/16 v3, 0xf */
int v4 = 1; // const/4 v4, 0x1
/* .line 3096 */
java.lang.Integer .valueOf ( v4 );
/* .line 3078 */
/* packed-switch p1, :pswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 3103 */
/* :pswitch_0 */
/* nop */
/* .line 3104 */
/* nop */
/* .line 3103 */
final String v6 = "3"; // const-string v6, "3"
/* invoke-direct {p0, v2, v6, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 3105 */
/* iput-boolean v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z */
/* .line 3106 */
v2 = this.mBackgroundHandler;
(( android.os.Handler ) v2 ).removeMessages ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V
/* .line 3107 */
v2 = this.mBackgroundHandler;
(( android.os.Handler ) v2 ).sendEmptyMessageDelayed ( v3, v0, v1 ); // invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 3109 */
/* .line 3095 */
/* :pswitch_1 */
/* nop */
/* .line 3096 */
/* nop */
/* .line 3095 */
final String v6 = "2"; // const-string v6, "2"
/* invoke-direct {p0, v2, v6, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 3097 */
/* iput-boolean v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z */
/* .line 3098 */
v2 = this.mBackgroundHandler;
(( android.os.Handler ) v2 ).removeMessages ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V
/* .line 3099 */
v2 = this.mBackgroundHandler;
(( android.os.Handler ) v2 ).sendEmptyMessageDelayed ( v3, v0, v1 ); // invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 3101 */
/* .line 3080 */
/* :pswitch_2 */
/* sget-boolean v3, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 3081 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "notifyAonFlareEvents: preLux: "; // const-string v6, "notifyAonFlareEvents: preLux: "
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "BrightnessDataProcessor"; // const-string v6, "BrightnessDataProcessor"
android.util.Slog .i ( v6,v3 );
/* .line 3083 */
} // :cond_0
/* nop */
/* .line 3084 */
/* nop */
/* .line 3083 */
final String v3 = "1"; // const-string v3, "1"
/* invoke-direct {p0, v2, v3, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 3085 */
/* nop */
/* .line 3086 */
v2 = /* invoke-direct {p0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getAmbientLuxSpanIndex(F)I */
java.lang.Integer .valueOf ( v2 );
/* .line 3085 */
final String v3 = "flare_suppress_darken_lux_span"; // const-string v3, "flare_suppress_darken_lux_span"
/* invoke-direct {p0, v3, v2, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 3087 */
/* nop */
/* .line 3088 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
v2 = /* invoke-direct {p0, v2, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getHourFromTimestamp(J)I */
java.lang.Integer .valueOf ( v2 );
/* .line 3087 */
final String v3 = "flare_suppress_darken_hour"; // const-string v3, "flare_suppress_darken_hour"
/* invoke-direct {p0, v3, v2, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 3089 */
/* iput-boolean v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z */
/* .line 3090 */
v2 = this.mBackgroundHandler;
/* const/16 v3, 0xe */
(( android.os.Handler ) v2 ).removeMessages ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V
/* .line 3091 */
v2 = this.mBackgroundHandler;
(( android.os.Handler ) v2 ).sendEmptyMessageDelayed ( v3, v0, v1 ); // invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 3093 */
/* nop */
/* .line 3113 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void notifyBrightnessEventIfNeeded ( Boolean p0, Float p1, Float p2, Boolean p3, Boolean p4, Float p5, Boolean p6, Float p7, Float p8, Boolean p9, Boolean p10, Boolean p11, Boolean p12, Float p13, Float p14, Integer p15 ) {
/* .locals 26 */
/* .param p1, "screenOn" # Z */
/* .param p2, "originalBrightness" # F */
/* .param p3, "actualBrightness" # F */
/* .param p4, "userInitiatedChange" # Z */
/* .param p5, "useAutoBrightness" # Z */
/* .param p6, "brightnessOverrideFromWindow" # F */
/* .param p7, "lowPowerMode" # Z */
/* .param p8, "ambientLux" # F */
/* .param p9, "userDataPoint" # F */
/* .param p10, "defaultConfig" # Z */
/* .param p11, "sunlightActive" # Z */
/* .param p12, "isHdrLayer" # Z */
/* .param p13, "isDimmingChanged" # Z */
/* .param p14, "mainFastAmbientLux" # F */
/* .param p15, "assistFastAmbientLux" # F */
/* .param p16, "sceneState" # I */
/* .line 461 */
/* move-object/from16 v0, p0 */
/* move/from16 v1, p1 */
/* move/from16 v15, p2 */
/* move/from16 v13, p3 */
/* move/from16 v12, p4 */
/* move/from16 v11, p5 */
/* move/from16 v10, p6 */
/* move/from16 v9, p11 */
v8 = /* invoke-direct {v0, v12, v11, v10, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessType(ZZFZ)I */
/* .line 464 */
/* .local v8, "type":I */
int v7 = 1; // const/4 v7, 0x1
int v2 = 2; // const/4 v2, 0x2
/* if-eq v8, v2, :cond_0 */
/* if-eq v8, v7, :cond_0 */
/* .line 466 */
/* const/high16 v3, 0x7fc00000 # Float.NaN */
/* move/from16 v21, v3 */
} // .end local p8 # "ambientLux":F
/* .local v3, "ambientLux":F */
/* .line 469 */
} // .end local v3 # "ambientLux":F
/* .restart local p8 # "ambientLux":F */
} // :cond_0
/* move/from16 v21, p8 */
} // .end local p8 # "ambientLux":F
/* .local v21, "ambientLux":F */
} // :goto_0
v3 = this.mDisplayDeviceConfig;
/* .line 470 */
v4 = /* invoke-direct {v0, v15}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->clampBrightness(F)F */
v3 = (( com.android.server.display.DisplayDeviceConfig ) v3 ).getNitsFromBacklight ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
/* .line 471 */
/* .local v3, "originalNit":F */
/* float-to-double v4, v3 */
java.math.BigDecimal .valueOf ( v4,v5 );
v5 = java.math.RoundingMode.HALF_UP;
/* .line 472 */
(( java.math.BigDecimal ) v4 ).setScale ( v2, v5 ); // invoke-virtual {v4, v2, v5}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;
v6 = (( java.math.BigDecimal ) v4 ).floatValue ( ); // invoke-virtual {v4}, Ljava/math/BigDecimal;->floatValue()F
/* .line 474 */
} // .end local v3 # "originalNit":F
/* .local v6, "originalNit":F */
v3 = this.mDisplayDeviceConfig;
/* .line 475 */
v4 = /* invoke-direct {v0, v13}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->clampBrightness(F)F */
v3 = (( com.android.server.display.DisplayDeviceConfig ) v3 ).getNitsFromBacklight ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
/* .line 476 */
/* .local v3, "actualNit":F */
/* float-to-double v4, v3 */
java.math.BigDecimal .valueOf ( v4,v5 );
v5 = java.math.RoundingMode.HALF_UP;
/* .line 477 */
(( java.math.BigDecimal ) v4 ).setScale ( v2, v5 ); // invoke-virtual {v4, v2, v5}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;
v5 = (( java.math.BigDecimal ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/math/BigDecimal;->floatValue()F
/* .line 479 */
} // .end local v3 # "actualNit":F
/* .local v5, "actualNit":F */
/* invoke-direct {v0, v1, v13}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->scheduleUpdateBrightnessStatisticsData(ZF)V */
/* .line 481 */
/* move/from16 v4, p12 */
/* invoke-direct {v0, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->startHdyUsageStats(Z)V */
/* .line 483 */
/* invoke-direct {v0, v6, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateScreenNits(FF)V */
/* .line 485 */
v2 = this.mLastBrightnessChangeItem;
if ( v2 != null) { // if-eqz v2, :cond_2
/* iget v2, v2, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F */
/* .line 487 */
v2 = com.android.internal.display.BrightnessSynchronizer .floatEquals ( v2,v15 );
/* if-nez v2, :cond_1 */
} // :cond_1
/* move/from16 v23, v5 */
/* move/from16 v24, v6 */
/* move/from16 v25, v8 */
/* goto/16 :goto_2 */
} // :cond_2
} // :goto_1
/* if-nez p13, :cond_6 */
v2 = this.mPolicy;
v2 = /* .line 489 */
/* if-nez v2, :cond_5 */
v2 = this.mPolicy;
v2 = /* .line 490 */
/* if-nez v2, :cond_4 */
/* if-nez v1, :cond_3 */
/* move/from16 v23, v5 */
/* move/from16 v24, v6 */
/* move/from16 v25, v8 */
/* .line 495 */
} // :cond_3
/* invoke-direct {v0, v8, v13}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateInterruptBrightnessAnimDurationIfNeeded(IF)V */
/* .line 497 */
/* new-instance v22, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem; */
/* move-object/from16 v2, v22 */
v14 = this.mForegroundAppPackageName;
/* iget v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOrientation:I */
/* move/from16 v19, v3 */
/* move/from16 v3, p2 */
/* move/from16 v4, p3 */
/* move/from16 v23, v5 */
} // .end local v5 # "actualNit":F
/* .local v23, "actualNit":F */
/* move/from16 v5, p4 */
/* move/from16 v24, v6 */
} // .end local v6 # "originalNit":F
/* .local v24, "originalNit":F */
/* move/from16 v6, p5 */
/* move v1, v7 */
/* move/from16 v7, p6 */
/* move/from16 v25, v8 */
} // .end local v8 # "type":I
/* .local v25, "type":I */
/* move/from16 v8, p7 */
/* move/from16 v9, p11 */
/* move/from16 v10, v21 */
/* move/from16 v11, p9 */
/* move/from16 v12, p10 */
/* move/from16 v13, v25 */
/* move/from16 v15, p14 */
/* move/from16 v16, p15 */
/* move/from16 v17, v24 */
/* move/from16 v18, v23 */
/* move/from16 v20, p16 */
/* invoke-direct/range {v2 ..v20}, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;-><init>(FFZZFZZFFZILjava/lang/String;FFFFII)V */
/* .line 503 */
/* .local v2, "item":Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem; */
this.mLastBrightnessChangeItem = v2;
/* .line 504 */
v3 = this.mBackgroundHandler;
android.os.Message .obtain ( v3,v1,v2 );
/* .line 505 */
/* .local v1, "msg":Landroid/os/Message; */
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 506 */
return;
/* .line 490 */
} // .end local v1 # "msg":Landroid/os/Message;
} // .end local v2 # "item":Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;
} // .end local v23 # "actualNit":F
} // .end local v24 # "originalNit":F
} // .end local v25 # "type":I
/* .restart local v5 # "actualNit":F */
/* .restart local v6 # "originalNit":F */
/* .restart local v8 # "type":I */
} // :cond_4
/* move/from16 v23, v5 */
/* move/from16 v24, v6 */
/* move/from16 v25, v8 */
} // .end local v5 # "actualNit":F
} // .end local v6 # "originalNit":F
} // .end local v8 # "type":I
/* .restart local v23 # "actualNit":F */
/* .restart local v24 # "originalNit":F */
/* .restart local v25 # "type":I */
/* .line 489 */
} // .end local v23 # "actualNit":F
} // .end local v24 # "originalNit":F
} // .end local v25 # "type":I
/* .restart local v5 # "actualNit":F */
/* .restart local v6 # "originalNit":F */
/* .restart local v8 # "type":I */
} // :cond_5
/* move/from16 v23, v5 */
/* move/from16 v24, v6 */
/* move/from16 v25, v8 */
} // .end local v5 # "actualNit":F
} // .end local v6 # "originalNit":F
} // .end local v8 # "type":I
/* .restart local v23 # "actualNit":F */
/* .restart local v24 # "originalNit":F */
/* .restart local v25 # "type":I */
/* .line 487 */
} // .end local v23 # "actualNit":F
} // .end local v24 # "originalNit":F
} // .end local v25 # "type":I
/* .restart local v5 # "actualNit":F */
/* .restart local v6 # "originalNit":F */
/* .restart local v8 # "type":I */
} // :cond_6
/* move/from16 v23, v5 */
/* move/from16 v24, v6 */
/* move/from16 v25, v8 */
/* .line 492 */
} // .end local v5 # "actualNit":F
} // .end local v6 # "originalNit":F
} // .end local v8 # "type":I
/* .restart local v23 # "actualNit":F */
/* .restart local v24 # "originalNit":F */
/* .restart local v25 # "type":I */
} // :goto_2
return;
} // .end method
public void notifyResetBrightnessAnimInfo ( ) {
/* .locals 2 */
/* .line 1749 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingAnimationStart:Z */
/* .line 1750 */
v0 = this.mBackgroundHandler;
/* const/16 v1, 0xc */
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 1751 */
return;
} // .end method
public void notifyUpdateBrightness ( ) {
/* .locals 1 */
/* .line 3116 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z */
/* .line 3117 */
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z */
/* .line 3118 */
return;
} // .end method
public void notifyUpdateBrightnessAnimInfo ( Float p0, Float p1, Float p2 ) {
/* .locals 5 */
/* .param p1, "currentBrightnessAnim" # F */
/* .param p2, "brightnessAnim" # F */
/* .param p3, "targetBrightnessAnim" # F */
/* .line 1594 */
/* cmpl-float v0, p2, p3 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1595 */
/* .local v0, "begin":Z */
} // :goto_0
v1 = /* invoke-direct {p0, p1, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessChangedState(FF)I */
/* .line 1599 */
/* .local v1, "state":I */
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingAnimationStart:Z */
/* if-ne v0, v2, :cond_2 */
int v2 = 3; // const/4 v2, 0x3
int v3 = 2; // const/4 v3, 0x2
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangedState:I */
/* if-eq v4, v3, :cond_1 */
/* if-eq v4, v2, :cond_1 */
/* if-eq v1, v3, :cond_1 */
/* if-eq v1, v2, :cond_1 */
/* if-eq v1, v4, :cond_1 */
/* .line 1615 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangedState:I */
/* if-eq v4, v3, :cond_3 */
/* if-eq v4, v2, :cond_3 */
/* if-eq v1, v3, :cond_3 */
/* if-eq v1, v2, :cond_3 */
/* if-ne v1, v4, :cond_3 */
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingTargetBrightnessAnimValue:F */
/* cmpl-float v2, p3, v2 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1621 */
/* iput p3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingTargetBrightnessAnimValue:F */
/* .line 1622 */
v2 = this.mBackgroundHandler;
/* const/16 v3, 0x9 */
(( android.os.Handler ) v2 ).obtainMessage ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 1623 */
/* .local v2, "msg":Landroid/os/Message; */
java.lang.Float .valueOf ( p3 );
this.obj = v3;
/* .line 1624 */
(( android.os.Message ) v2 ).sendToTarget ( ); // invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V
/* .line 1605 */
} // .end local v2 # "msg":Landroid/os/Message;
} // :cond_2
} // :goto_1
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingAnimationStart:Z */
/* .line 1606 */
/* iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangedState:I */
/* .line 1607 */
/* iput p3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingTargetBrightnessAnimValue:F */
/* .line 1608 */
com.android.internal.os.SomeArgs .obtain ( );
/* .line 1609 */
/* .local v2, "args":Lcom/android/internal/os/SomeArgs; */
java.lang.Float .valueOf ( p1 );
this.arg1 = v3;
/* .line 1610 */
java.lang.Float .valueOf ( p3 );
this.arg2 = v3;
/* .line 1611 */
java.lang.Boolean .valueOf ( v0 );
this.arg3 = v3;
/* .line 1612 */
v3 = this.mBackgroundHandler;
/* const/16 v4, 0x8 */
(( android.os.Handler ) v3 ).obtainMessage ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v3 ).sendToTarget ( ); // invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V
/* .line 1615 */
} // .end local v2 # "args":Lcom/android/internal/os/SomeArgs;
} // :cond_3
/* nop */
/* .line 1626 */
} // :goto_2
return;
} // .end method
public void notifyUpdateTempBrightnessTimeStampIfNeeded ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 1577 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsTemporaryBrightnessAdjustment:Z */
/* if-eq p1, v0, :cond_0 */
/* .line 1578 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsTemporaryBrightnessAdjustment:Z */
/* .line 1579 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1580 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingAnimationStart:Z */
/* .line 1581 */
v0 = this.mBackgroundHandler;
/* const/16 v1, 0xb */
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 1584 */
} // :cond_0
return;
} // .end method
public void onCloudUpdated ( Long p0, java.util.Map p1 ) {
/* .locals 4 */
/* .param p1, "summary" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(J", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1056 */
/* .local p3, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;" */
/* const-wide/16 v0, 0x1 */
/* and-long/2addr v0, p1 */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mReportBrightnessEventsEnable:Z */
/* .line 1058 */
return;
} // .end method
public void setBrightnessConfiguration ( android.hardware.display.BrightnessConfiguration p0, android.hardware.display.BrightnessConfiguration p1, android.hardware.display.BrightnessConfiguration p2 ) {
/* .locals 1 */
/* .param p1, "defaultSceneConfig" # Landroid/hardware/display/BrightnessConfiguration; */
/* .param p2, "darkeningSceneConfig" # Landroid/hardware/display/BrightnessConfiguration; */
/* .param p3, "brighteningSceneConfig" # Landroid/hardware/display/BrightnessConfiguration; */
/* .line 1784 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getSpline(Landroid/hardware/display/BrightnessConfiguration;)Landroid/util/Spline; */
this.mDefaultSceneSpline = v0;
/* .line 1785 */
/* invoke-direct {p0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getSpline(Landroid/hardware/display/BrightnessConfiguration;)Landroid/util/Spline; */
this.mDarkeningSceneSpline = v0;
/* .line 1786 */
/* invoke-direct {p0, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getSpline(Landroid/hardware/display/BrightnessConfiguration;)Landroid/util/Spline; */
this.mBrighteningSceneSpline = v0;
/* .line 1787 */
return;
} // .end method
public void setBrightnessMapper ( com.android.server.display.BrightnessMappingStrategy p0 ) {
/* .locals 2 */
/* .param p1, "brightnessMapper" # Lcom/android/server/display/BrightnessMappingStrategy; */
/* .line 1766 */
this.mBrightnessMapper = p1;
/* .line 1767 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1768 */
(( com.android.server.display.BrightnessMappingStrategy ) p1 ).getDefaultConfig ( ); // invoke-virtual {p1}, Lcom/android/server/display/BrightnessMappingStrategy;->getDefaultConfig()Landroid/hardware/display/BrightnessConfiguration;
/* .line 1769 */
/* .local v0, "config":Landroid/hardware/display/BrightnessConfiguration; */
/* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getSpline(Landroid/hardware/display/BrightnessConfiguration;)Landroid/util/Spline; */
this.mOriSpline = v1;
/* .line 1771 */
} // .end local v0 # "config":Landroid/hardware/display/BrightnessConfiguration;
} // :cond_0
return;
} // .end method
public void setDisplayDeviceConfig ( com.android.server.display.DisplayDeviceConfig p0 ) {
/* .locals 0 */
/* .param p1, "config" # Lcom/android/server/display/DisplayDeviceConfig; */
/* .line 1762 */
this.mDisplayDeviceConfig = p1;
/* .line 1763 */
return;
} // .end method
public void setExpId ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "id" # I */
/* .line 2867 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mExpId:I */
/* .line 2868 */
return;
} // .end method
public void setModelEventCallback ( com.android.server.display.statistics.BrightnessDataProcessor$ModelEventCallback p0 ) {
/* .locals 0 */
/* .param p1, "callback" # Lcom/android/server/display/statistics/BrightnessDataProcessor$ModelEventCallback; */
/* .line 745 */
this.mModelEventCallback = p1;
/* .line 746 */
return;
} // .end method
public void setUpCloudControllerListener ( com.android.server.display.AutomaticBrightnessControllerImpl$CloudControllerListener p0 ) {
/* .locals 0 */
/* .param p1, "listener" # Lcom/android/server/display/AutomaticBrightnessControllerImpl$CloudControllerListener; */
/* .line 1758 */
this.mCloudControllerListener = p1;
/* .line 1759 */
return;
} // .end method
public void thermalConfigChanged ( java.util.List p0 ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1066 */
/* .local p1, "item":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
/* .line 1067 */
/* .local v1, "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v1 ).getTemperatureBrightnessPair ( ); // invoke-virtual {v1}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;
/* .line 1068 */
/* .local v2, "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;" */
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
/* .line 1069 */
/* .local v4, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
v5 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v4 ).getMinInclusive ( ); // invoke-virtual {v4}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMinInclusive()F
/* .line 1070 */
/* .local v5, "minTemp":F */
v6 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v4 ).getMaxExclusive ( ); // invoke-virtual {v4}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMaxExclusive()F
/* .line 1071 */
/* .local v6, "maxTemp":F */
v7 = this.mTemperatureSpan;
v7 = java.lang.Float .valueOf ( v5 );
/* if-nez v7, :cond_0 */
/* .line 1072 */
v7 = this.mTemperatureSpan;
java.lang.Float .valueOf ( v5 );
/* .line 1074 */
} // :cond_0
v7 = this.mTemperatureSpan;
v7 = java.lang.Float .valueOf ( v6 );
/* if-nez v7, :cond_1 */
/* .line 1075 */
v7 = this.mTemperatureSpan;
java.lang.Float .valueOf ( v6 );
/* .line 1077 */
} // .end local v4 # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
} // .end local v5 # "minTemp":F
} // .end local v6 # "maxTemp":F
} // :cond_1
/* .line 1078 */
} // .end local v1 # "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
} // .end local v2 # "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;"
} // :cond_2
/* .line 1079 */
} // :cond_3
v0 = v0 = this.mTemperatureSpan;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1080 */
v0 = this.mTemperatureSpan;
java.util.Collections .sort ( v0 );
/* .line 1082 */
} // :cond_4
/* sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 1083 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "thermalConfigChanged: mTemperatureSpan: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mTemperatureSpan;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BrightnessDataProcessor"; // const-string v1, "BrightnessDataProcessor"
android.util.Slog .d ( v1,v0 );
/* .line 1085 */
} // :cond_5
return;
} // .end method
public void updateExpId ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "expId" # I */
/* .line 1790 */
v0 = this.mBackgroundHandler;
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda11; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda11;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1791 */
return;
} // .end method
public void updateGrayScale ( Float p0 ) {
/* .locals 2 */
/* .param p1, "grayScale" # F */
/* .line 766 */
v0 = this.mBackgroundHandler;
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;F)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 767 */
return;
} // .end method
public void updateThermalStats ( Float p0, Boolean p1, Float p2, Boolean p3 ) {
/* .locals 8 */
/* .param p1, "brightnessState" # F */
/* .param p2, "isScreenOn" # Z */
/* .param p3, "temperature" # F */
/* .param p4, "needComputed" # Z */
/* .line 2547 */
v0 = this.mBackgroundHandler;
/* new-instance v7, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda2; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move v3, p2 */
/* move v4, p3 */
/* move v5, p4 */
/* move v6, p1 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;ZFZF)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2565 */
return;
} // .end method
