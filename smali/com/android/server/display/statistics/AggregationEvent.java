public abstract class com.android.server.display.statistics.AggregationEvent {
	 /* .source "AggregationEvent.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/statistics/AggregationEvent$AonFlareAggregationEvent;, */
	 /* Lcom/android/server/display/statistics/AggregationEvent$CbmAggregationEvent;, */
	 /* Lcom/android/server/display/statistics/AggregationEvent$ThermalAggregationEvent;, */
	 /* Lcom/android/server/display/statistics/AggregationEvent$AdvancedAggregationEvent;, */
	 /* Lcom/android/server/display/statistics/AggregationEvent$BrightnessAggregationEvent; */
	 /* } */
} // .end annotation
/* # direct methods */
public com.android.server.display.statistics.AggregationEvent ( ) {
	 /* .locals 0 */
	 /* .line 15 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
/* # virtual methods */
public abstract java.util.Map getCacheDataMap ( ) {
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "()", */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/Object;", */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/Float;", */
	 /* ">;>;" */
	 /* } */
} // .end annotation
} // .end method
public abstract java.util.Map getQuotaEvents ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
} // .end method
public abstract java.lang.String toString ( ) {
} // .end method
