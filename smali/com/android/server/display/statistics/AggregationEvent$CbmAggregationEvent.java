public class com.android.server.display.statistics.AggregationEvent$CbmAggregationEvent extends com.android.server.display.statistics.AggregationEvent {
	 /* .source "AggregationEvent.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/statistics/AggregationEvent; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "CbmAggregationEvent" */
} // .end annotation
/* # static fields */
protected static final java.lang.String EVENT_CUSTOM_BRIGHTNESS_ADJUST;
protected static final java.lang.String EVENT_CUSTOM_BRIGHTNESS_USAGE;
protected static final java.lang.String EVENT_INDIVIDUAL_MODEL_PREDICT;
protected static final java.lang.String EVENT_INDIVIDUAL_MODEL_TRAIN;
protected static final java.lang.String KEY_CURRENT_INDIVIDUAL_MODEL_MAE;
protected static final java.lang.String KEY_CURRENT_INDIVIDUAL_MODEL_MAPE;
private static final java.lang.String KEY_CUSTOM_CURVE_AUTO_BRIGHTNESS_ADJUST;
private static final java.lang.String KEY_CUSTOM_CURVE_AUTO_BRIGHTNESS_MANUAL_ADJUST;
private static final java.lang.String KEY_CUSTOM_CURVE_DURATION;
private static final java.lang.String KEY_DEFAULT_AUTO_BRIGHTNESS_ADJUST;
private static final java.lang.String KEY_DEFAULT_AUTO_BRIGHTNESS_MANUAL_ADJUST;
private static final java.lang.String KEY_DEFAULT_AUTO_DURATION;
private static final java.lang.String KEY_INDIVIDUAL_MODEL_AUTO_BRIGHTNESS_ADJUST;
private static final java.lang.String KEY_INDIVIDUAL_MODEL_AUTO_BRIGHTNESS_MANUAL_ADJUST;
private static final java.lang.String KEY_INDIVIDUAL_MODEL_DURATION;
protected static final java.lang.String KEY_INDIVIDUAL_MODEL_PREDICT_AVERAGE_DURATION;
protected static final java.lang.String KEY_INDIVIDUAL_MODEL_PREDICT_TIMEOUT_COUNT;
protected static final java.lang.String KEY_INDIVIDUAL_MODEL_TRAIN_COUNT;
protected static final java.lang.String KEY_INDIVIDUAL_MODEL_TRAIN_INDICATORS;
protected static final java.lang.String KEY_INDIVIDUAL_MODEL_TRAIN_LOSS;
protected static final java.lang.String KEY_INDIVIDUAL_MODEL_TRAIN_SAMPLE_NUM;
protected static final java.lang.String KEY_INDIVIDUAL_MODEL_TRAIN_TIMESTAMP;
protected static final java.lang.String KEY_INDIVIDUAL_MODEL_VALIDATION_FAIL_COUNT;
protected static final java.lang.String KEY_INDIVIDUAL_MODEL_VALIDATION_SUCCESS_COUNT;
protected static final java.lang.String KEY_PREVIOUS_INDIVIDUAL_MODEL_MAE;
protected static final java.lang.String KEY_PREVIOUS_INDIVIDUAL_MODEL_MAPE;
/* # instance fields */
private final java.util.Map mCbmCacheDataMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Float;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Map mCbmQuotaEvents;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.display.statistics.AggregationEvent$CbmAggregationEvent ( ) {
/* .locals 1 */
/* .line 157 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/AggregationEvent;-><init>()V */
/* .line 214 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCbmQuotaEvents = v0;
/* .line 216 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCbmCacheDataMap = v0;
return;
} // .end method
public static java.lang.String getCbmAutoAdjustQuotaName ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p0, "cbmState" # I */
/* .param p1, "isManuallySet" # Z */
/* .line 235 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 246 */
if ( p1 != null) { // if-eqz p1, :cond_2
final String v0 = "default_auto_brt_manual_adj"; // const-string v0, "default_auto_brt_manual_adj"
/* .line 241 */
/* :pswitch_0 */
if ( p1 != null) { // if-eqz p1, :cond_0
final String v0 = "model_auto_brt_manual_adj"; // const-string v0, "model_auto_brt_manual_adj"
/* .line 242 */
} // :cond_0
final String v0 = "model_auto_brt_adj"; // const-string v0, "model_auto_brt_adj"
} // :goto_0
/* nop */
/* .line 243 */
/* .local v0, "name":Ljava/lang/String; */
/* .line 237 */
} // .end local v0 # "name":Ljava/lang/String;
/* :pswitch_1 */
if ( p1 != null) { // if-eqz p1, :cond_1
final String v0 = "custom_curve_auto_brt_manual_adj"; // const-string v0, "custom_curve_auto_brt_manual_adj"
/* .line 238 */
} // :cond_1
final String v0 = "custom_curve_auto_brt_adj"; // const-string v0, "custom_curve_auto_brt_adj"
} // :goto_1
/* nop */
/* .line 239 */
/* .restart local v0 # "name":Ljava/lang/String; */
/* .line 247 */
} // .end local v0 # "name":Ljava/lang/String;
} // :cond_2
final String v0 = "default_auto_brt_adj"; // const-string v0, "default_auto_brt_adj"
} // :goto_2
/* nop */
/* .line 250 */
/* .restart local v0 # "name":Ljava/lang/String; */
} // :goto_3
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public static java.lang.String getCbmBrtUsageQuotaName ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "cbmState" # I */
/* .line 255 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 264 */
final String v0 = "default_auto_duration"; // const-string v0, "default_auto_duration"
/* .local v0, "name":Ljava/lang/String; */
/* .line 260 */
} // .end local v0 # "name":Ljava/lang/String;
/* :pswitch_0 */
final String v0 = "model_duration"; // const-string v0, "model_duration"
/* .line 261 */
/* .restart local v0 # "name":Ljava/lang/String; */
/* .line 257 */
} // .end local v0 # "name":Ljava/lang/String;
/* :pswitch_1 */
final String v0 = "custom_curve_duration"; // const-string v0, "custom_curve_duration"
/* .line 258 */
/* .restart local v0 # "name":Ljava/lang/String; */
/* nop */
/* .line 267 */
} // :goto_0
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public java.util.Map getCacheDataMap ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Float;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 230 */
v0 = this.mCbmCacheDataMap;
} // .end method
public java.util.Map getQuotaEvents ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 225 */
v0 = this.mCbmQuotaEvents;
} // .end method
public java.lang.String toString ( ) {
/* .locals 1 */
/* .line 220 */
v0 = this.mCbmQuotaEvents;
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
