public class com.android.server.display.statistics.BrightnessEvent$SwitchStatEntry {
	 /* .source "BrightnessEvent.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/statistics/BrightnessEvent; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "SwitchStatEntry" */
} // .end annotation
/* # static fields */
public static final Integer TYPE_BOOLEAN;
public static final Integer TYPE_INTEGER;
/* # instance fields */
public Boolean b_value;
public Integer i_value;
public java.lang.String key;
public Integer type;
/* # direct methods */
public com.android.server.display.statistics.BrightnessEvent$SwitchStatEntry ( ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .param p2, "key" # Ljava/lang/String; */
/* .param p3, "i_value" # I */
/* .line 378 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 379 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 383 */
	 /* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;->type:I */
	 /* .line 384 */
	 this.key = p2;
	 /* .line 385 */
	 /* iput p3, p0, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;->i_value:I */
	 /* .line 386 */
	 return;
	 /* .line 380 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "Type and value are incompatible,the expected type is TYPE_INTEGER."; // const-string v1, "Type and value are incompatible,the expected type is TYPE_INTEGER."
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent$SwitchStatEntry ( ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .param p2, "key" # Ljava/lang/String; */
/* .param p3, "b_value" # Z */
/* .line 388 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 389 */
int v0 = 1; // const/4 v0, 0x1
/* if-eq p1, v0, :cond_0 */
/* .line 393 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;->type:I */
/* .line 394 */
this.key = p2;
/* .line 395 */
/* iput-boolean p3, p0, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;->b_value:Z */
/* .line 396 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;->i_value:I */
/* .line 397 */
return;
/* .line 390 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* const-string/jumbo v1, "type and value are incompatible,the expected type is TYPE_BOOLEAN." */
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 401 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "{key:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.key;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", value:"; // const-string v1, ", value:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;->type:I */
/* if-nez v1, :cond_0 */
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;->b_value:Z */
java.lang.Boolean .valueOf ( v1 );
} // :cond_0
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;->i_value:I */
java.lang.Integer .valueOf ( v1 );
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
