public class com.android.server.display.statistics.AggregationEvent$BrightnessAggregationEvent extends com.android.server.display.statistics.AggregationEvent {
	 /* .source "AggregationEvent.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/statistics/AggregationEvent; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "BrightnessAggregationEvent" */
} // .end annotation
/* # static fields */
public static final java.lang.String EVENT_AUTO_ADJUST_TIMES;
public static final java.lang.String EVENT_AUTO_MANUAL_ADJUST_APP_RANKING;
public static final java.lang.String EVENT_AUTO_MANUAL_ADJUST_AVG_NITS_LUX_SPAN;
public static final java.lang.String EVENT_AUTO_MANUAL_ADJUST_DISPLAY_MODE;
public static final java.lang.String EVENT_AUTO_MANUAL_ADJUST_HIGH_LUX_SPAN;
public static final java.lang.String EVENT_AUTO_MANUAL_ADJUST_LOW_LUX_SPAN;
public static final java.lang.String EVENT_AUTO_MANUAL_ADJUST_LUX_SPAN;
public static final java.lang.String EVENT_AUTO_MANUAL_ADJUST_TIMES;
public static final java.lang.String EVENT_AVERAGE_BRIGHTNESS;
public static final java.lang.String EVENT_BRIGHTNESS_ADJUST_TIMES;
public static final java.lang.String EVENT_BRIGHTNESS_USAGE;
public static final java.lang.String EVENT_HBM_USAGE;
public static final java.lang.String EVENT_HDR_USAGE;
public static final java.lang.String EVENT_HDR_USAGE_APP_USAGE;
public static final java.lang.String EVENT_MANUAL_ADJUST_TIMES;
public static final java.lang.String EVENT_OVERRIDE_ADJUST_APP_RANKING;
public static final java.lang.String EVENT_SUNLIGHT_ADJUST_TIMES;
public static final java.lang.String EVENT_SWITCH_STATS;
public static final java.lang.String EVENT_WINDOW_ADJUST_TIMES;
public static final java.lang.String KEY_SWITCH_STATS_DETAILS;
public static final java.lang.String KEY_USAGE_VALUE;
/* # instance fields */
private java.util.Map mBrightnessQuotaEvents;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mCacheDataMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Float;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.display.statistics.AggregationEvent$BrightnessAggregationEvent ( ) {
/* .locals 1 */
/* .line 23 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/AggregationEvent;-><init>()V */
/* .line 64 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mBrightnessQuotaEvents = v0;
/* .line 66 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCacheDataMap = v0;
return;
} // .end method
/* # virtual methods */
public java.util.Map getCacheDataMap ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Float;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 79 */
v0 = this.mCacheDataMap;
} // .end method
public java.util.Map getQuotaEvents ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 75 */
v0 = this.mBrightnessQuotaEvents;
} // .end method
public java.lang.String toString ( ) {
/* .locals 1 */
/* .line 70 */
v0 = this.mBrightnessQuotaEvents;
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
