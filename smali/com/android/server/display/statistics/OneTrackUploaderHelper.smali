.class public Lcom/android/server/display/statistics/OneTrackUploaderHelper;
.super Ljava/lang/Object;
.source "OneTrackUploaderHelper.java"


# static fields
.field public static final ADVANCED_BRIGHTNESS_AGGREGATION_EVENT_NAME:Ljava/lang/String; = "advanced_brightness_aggregation"

.field public static final ADVANCED_EVENT_NAME:Ljava/lang/String; = "advanced_brightness"

.field public static final AON_FLARE_AGGREGATION_EVENT_NAME:Ljava/lang/String; = "aon_flare_aggregation"

.field private static final BRIGHTNESS_EVENT_APP_ID:Ljava/lang/String; = "31000000084"

.field public static final BRIGHTNESS_EVENT_NAME:Ljava/lang/String; = "brightness"

.field public static final BRIGHTNESS_QUOTA_AGGREGATION_EVENT_NAME:Ljava/lang/String; = "brightness_quota_aggregation"

.field public static final CUSTOM_BRIGHTNESS_AGGREGATION_EVENT_NAME:Ljava/lang/String; = "custom_brightness_aggregation"

.field private static final DEVICE_REGION:Ljava/lang/String;

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN:I = 0x1

.field public static final IS_INTERNATIONAL_BUILD:Z

.field private static final KEY_ACC_VALUES:Ljava/lang/String; = "acc_values"

.field private static final KEY_ACTUAL_NIT:Ljava/lang/String; = "actual_nit"

.field private static final KEY_AFFECT_FACTOR_FLAG:Ljava/lang/String; = "affect_factor_flag"

.field private static final KEY_ALL_STATS_ENTRIES:Ljava/lang/String; = "all_stats_entries"

.field private static final KEY_AMBIENT_LUX:Ljava/lang/String; = "ambient_lux"

.field private static final KEY_AMBIENT_LUX_SPAN:Ljava/lang/String; = "ambient_lux_span"

.field private static final KEY_ASSIST_AMBIENT_LUX:Ljava/lang/String; = "assist_ambient_lux"

.field private static final KEY_AUTO_BRIGHTNESS_ANIMATION_DURATION:Ljava/lang/String; = "auto_brightness_animation_duration"

.field private static final KEY_BRIGHTNESS_CHANGED_STATUS:Ljava/lang/String; = "brightness_changed_state"

.field private static final KEY_BRIGHTNESS_RESTRICTED_ENABLE:Ljava/lang/String; = "brightness_restricted_enable"

.field private static final KEY_CURRENT_ANIMATE_VALUE:Ljava/lang/String; = "current_animate_value"

.field private static final KEY_CURRENT_USER_ID:Ljava/lang/String; = "current_user_id"

.field private static final KEY_DEFAULT_SPLINE_ERROR:Ljava/lang/String; = "default_spline_error"

.field private static final KEY_DISPLAY_GRAY_SCALE:Ljava/lang/String; = "display_gray_scale"

.field private static final KEY_EVENT_TYPE:Ljava/lang/String; = "type"

.field private static final KEY_EXP_ID:Ljava/lang/String; = "exp_id"

.field private static final KEY_EXTRA:Ljava/lang/String; = "extra"

.field private static final KEY_HDR_LAYER_ENABLE:Ljava/lang/String; = "hdr_layer_enable"

.field private static final KEY_IS_DEFAULT_CONFIG:Ljava/lang/String; = "is_default_config"

.field private static final KEY_IS_USE_LIGHT_FOV_OPTIMIZATION:Ljava/lang/String; = "is_use_light_fov_optimization"

.field private static final KEY_LAST_ASSIST_AMBIENT_LUX:Ljava/lang/String; = "last_assist_ambient_lux"

.field private static final KEY_LAST_MAIN_AMBIENT_LUX:Ljava/lang/String; = "last_main_ambient_lux"

.field private static final KEY_LONG_TERM_MODEL_SPLINE_ERROR:Ljava/lang/String; = "long_term_model_spline_error"

.field private static final KEY_LOW_POWER_MODE_FLAG:Ljava/lang/String; = "low_power_mode_flag"

.field private static final KEY_MAIN_AMBIENT_LUX:Ljava/lang/String; = "main_ambient_lux"

.field private static final KEY_ORIENTATION:Ljava/lang/String; = "orientation"

.field private static final KEY_ORIGINAL_NIT:Ljava/lang/String; = "original_nit"

.field private static final KEY_PREVIOUS_BRIGHTNESS:Ljava/lang/String; = "previous_brightness"

.field private static final KEY_PREVIOUS_BRIGHTNESS_SPAN:Ljava/lang/String; = "previous_brightness_span"

.field private static final KEY_SCREEN_BRIGHTNESS:Ljava/lang/String; = "screen_brightness"

.field private static final KEY_SCREEN_BRIGHTNESS_SPAN:Ljava/lang/String; = "screen_brightness_span"

.field private static final KEY_TARGET_ANIMATE_VALUE:Ljava/lang/String; = "target_animate_value"

.field private static final KEY_TIME_STAMP:Ljava/lang/String; = "time_stamp"

.field private static final KEY_TOP_PACKAGE:Ljava/lang/String; = "top_package"

.field private static final KEY_USER_BRIGHTNESS:Ljava/lang/String; = "user_brightness"

.field private static final KEY_USER_DATA_POINT:Ljava/lang/String; = "user_data_point"

.field private static final ONE_TRACK_ACTION:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field public static final THERMAL_AGGREGATION_EVENT_NAME:Ljava/lang/String; = "thermal_aggregation"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 65
    const-string v0, "ro.miui.region"

    const-string v1, "CN"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->DEVICE_REGION:Ljava/lang/String;

    .line 68
    nop

    .line 69
    const-string v0, "ro.product.mod_device"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "_global"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->IS_INTERNATIONAL_BUILD:Z

    .line 68
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getBrightnessEventIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 226
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onetrack.action.TRACK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 227
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 228
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PACKAGE"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 229
    const-string v2, "APP_ID"

    const-string v3, "31000000084"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 230
    return-object v0
.end method

.method private static getEventName(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 2
    .param p0, "key"    # Ljava/lang/Integer;

    .line 197
    const/4 v0, 0x0

    .line 198
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 212
    :pswitch_0
    const-string/jumbo v0, "sunlight_adj_times"

    .line 213
    goto :goto_0

    .line 209
    :pswitch_1
    const-string/jumbo v0, "window_adj_times"

    .line 210
    goto :goto_0

    .line 206
    :pswitch_2
    const-string v0, "auto_manual_adj_times"

    .line 207
    goto :goto_0

    .line 203
    :pswitch_3
    const-string v0, "auto_adj_times"

    .line 204
    goto :goto_0

    .line 200
    :pswitch_4
    const-string v0, "manual_adj_times"

    .line 201
    nop

    .line 217
    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic lambda$reportAggregatedEventsToServer$0(Landroid/content/Intent;Ljava/lang/String;Lcom/android/server/display/statistics/AggregationEvent;)V
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Lcom/android/server/display/statistics/AggregationEvent;

    .line 86
    invoke-virtual {p2}, Lcom/android/server/display/statistics/AggregationEvent;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    return-void
.end method

.method static synthetic lambda$reportAggregatedEventsToServer$1(Landroid/content/Intent;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/Object;

    .line 93
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 94
    move-object v0, p1

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->getEventName(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v0

    move-object v1, p2

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 96
    :cond_0
    return-void
.end method

.method public static reportAggregatedEventsToServer(Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "eventName"    # Ljava/lang/String;
    .param p3, "expId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/display/statistics/AggregationEvent;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .line 79
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/display/statistics/AggregationEvent;>;"
    sget-object v0, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->DEVICE_REGION:Ljava/lang/String;

    const-string v1, "IN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 82
    :cond_0
    invoke-static {p0}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->getBrightnessEventIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 83
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EVENT_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    const-string v1, "exp_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 85
    new-instance v1, Lcom/android/server/display/statistics/OneTrackUploaderHelper$$ExternalSyntheticLambda0;

    invoke-direct {v1, v0}, Lcom/android/server/display/statistics/OneTrackUploaderHelper$$ExternalSyntheticLambda0;-><init>(Landroid/content/Intent;)V

    invoke-interface {p1, v1}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 88
    const-string v1, "brightness_quota_aggregation"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89
    const-string v1, "brightness_adj_times"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/display/statistics/AggregationEvent;

    .line 90
    .local v1, "event":Lcom/android/server/display/statistics/AggregationEvent;
    if-eqz v1, :cond_1

    .line 91
    invoke-virtual {v1}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;

    move-result-object v2

    .line 92
    .local v2, "quotaEvents":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;"
    new-instance v3, Lcom/android/server/display/statistics/OneTrackUploaderHelper$$ExternalSyntheticLambda1;

    invoke-direct {v3, v0}, Lcom/android/server/display/statistics/OneTrackUploaderHelper$$ExternalSyntheticLambda1;-><init>(Landroid/content/Intent;)V

    invoke-interface {v2, v3}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 99
    .end local v1    # "event":Lcom/android/server/display/statistics/AggregationEvent;
    .end local v2    # "quotaEvents":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;"
    :cond_1
    invoke-static {p0, v0}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->startService(Landroid/content/Context;Landroid/content/Intent;)V

    .line 100
    return-void

    .line 80
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    :goto_0
    return-void
.end method

.method public static reportToOneTrack(Landroid/content/Context;Lcom/android/server/display/statistics/AdvancedEvent;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "event"    # Lcom/android/server/display/statistics/AdvancedEvent;

    .line 112
    sget-object v0, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->DEVICE_REGION:Ljava/lang/String;

    const-string v1, "IN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    return-void

    .line 115
    :cond_0
    invoke-static {p0}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->getBrightnessEventIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 116
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {v0, p1}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->updateAdvancedEventIntent(Landroid/content/Intent;Lcom/android/server/display/statistics/AdvancedEvent;)V

    .line 117
    invoke-static {p0, v0}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->startService(Landroid/content/Context;Landroid/content/Intent;)V

    .line 118
    return-void
.end method

.method public static reportToOneTrack(Landroid/content/Context;Lcom/android/server/display/statistics/BrightnessEvent;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "event"    # Lcom/android/server/display/statistics/BrightnessEvent;

    .line 103
    sget-object v0, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->DEVICE_REGION:Ljava/lang/String;

    const-string v1, "IN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    return-void

    .line 106
    :cond_0
    invoke-static {p0}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->getBrightnessEventIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 107
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {v0, p1}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->updateBrightnessEventIntent(Landroid/content/Intent;Lcom/android/server/display/statistics/BrightnessEvent;)V

    .line 108
    invoke-static {p0, v0}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->startService(Landroid/content/Context;Landroid/content/Intent;)V

    .line 109
    return-void
.end method

.method private static startService(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .line 127
    sget-boolean v0, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    .line 128
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 131
    :cond_0
    :try_start_0
    sget-object v0, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    goto :goto_0

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to upload brightness event! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 134
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 133
    const-string v2, "BrightnessDataProcessor"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public static updateAdvancedEventIntent(Landroid/content/Intent;Lcom/android/server/display/statistics/AdvancedEvent;)V
    .locals 4
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "event"    # Lcom/android/server/display/statistics/AdvancedEvent;

    .line 182
    const-string v0, "EVENT_NAME"

    const-string v1, "advanced_brightness"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 183
    invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getEventType()I

    move-result v1

    const-string/jumbo v2, "type"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 184
    invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getAutoBrightnessAnimationDuration()F

    move-result v1

    const-string v2, "auto_brightness_animation_duration"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    .line 185
    invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getCurrentAnimateValue()I

    move-result v1

    const-string v2, "current_animate_value"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 186
    invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getTargetAnimateValue()I

    move-result v1

    const-string/jumbo v2, "target_animate_value"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 187
    invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getUserBrightness()I

    move-result v1

    const-string/jumbo v2, "user_brightness"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 188
    invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getLongTermModelSplineError()F

    move-result v1

    const-string v2, "long_term_model_spline_error"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    .line 189
    invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getDefaultSplineError()F

    move-result v1

    const-string v2, "default_spline_error"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    .line 190
    invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getBrightnessChangedState()I

    move-result v1

    const-string v2, "brightness_changed_state"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 191
    invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getExtra()Ljava/lang/String;

    move-result-object v1

    const-string v2, "extra"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 192
    invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getTimeStamp()J

    move-result-wide v1

    const-string/jumbo v3, "time_stamp"

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 193
    invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getBrightnessRestrictedEnable()Z

    move-result v1

    const-string v2, "brightness_restricted_enable"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 194
    return-void
.end method

.method public static updateBrightnessEventIntent(Landroid/content/Intent;Lcom/android/server/display/statistics/BrightnessEvent;)V
    .locals 4
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "event"    # Lcom/android/server/display/statistics/BrightnessEvent;

    .line 144
    const-string v0, "EVENT_NAME"

    const-string v1, "brightness"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 145
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getEventType()I

    move-result v1

    const-string/jumbo v2, "type"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 146
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getAmbientLux()F

    move-result v1

    const-string v2, "ambient_lux"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    .line 147
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getScreenBrightness()F

    move-result v1

    const-string v2, "screen_brightness"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    .line 148
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getOrientation()I

    move-result v1

    const-string v2, "orientation"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 149
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getForegroundPackage()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "top_package"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 150
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getTimeStamp()J

    move-result-wide v1

    const-string/jumbo v3, "time_stamp"

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 151
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getExtra()Ljava/lang/String;

    move-result-object v1

    const-string v2, "extra"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 152
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getPreviousBrightness()F

    move-result v1

    const-string v2, "previous_brightness"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    .line 153
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->isDefaultConfig()Z

    move-result v1

    const-string v2, "is_default_config"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 154
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getUserDataPoint()F

    move-result v1

    const-string/jumbo v2, "user_data_point"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    .line 155
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getLowPowerModeFlag()Z

    move-result v1

    const-string v2, "low_power_mode_flag"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 156
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getUserId()I

    move-result v1

    const-string v2, "current_user_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 157
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getSwitchStats()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "all_stats_entries"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 158
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getAffectFactorFlag()I

    move-result v1

    const-string v2, "affect_factor_flag"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 159
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getCurBrightnessSpanIndex()I

    move-result v1

    const-string v2, "screen_brightness_span"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 160
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getPreBrightnessSpanIndex()I

    move-result v1

    const-string v2, "previous_brightness_span"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 161
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getLuxSpanIndex()I

    move-result v1

    const-string v2, "ambient_lux_span"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 162
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getDisplayGrayScale()F

    move-result v1

    const-string v2, "display_gray_scale"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    .line 163
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getActualNit()F

    move-result v1

    const-string v2, "actual_nit"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    .line 164
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getOriginalNit()F

    move-result v1

    const-string v2, "original_nit"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    .line 165
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getHdrLayerEnable()Z

    move-result v1

    const-string v2, "hdr_layer_enable"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 166
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getBrightnessRestrictedEnable()Z

    move-result v1

    const-string v2, "brightness_restricted_enable"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 167
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getMainAmbientLux()F

    move-result v1

    const-string v2, "main_ambient_lux"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    .line 168
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getAssistAmbientLux()F

    move-result v1

    const-string v2, "assist_ambient_lux"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    .line 169
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getLastMainAmbientLux()F

    move-result v1

    const-string v2, "last_main_ambient_lux"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    .line 170
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getLastAssistAmbientLux()F

    move-result v1

    const-string v2, "last_assist_ambient_lux"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v0

    .line 171
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getAccValues()[F

    move-result-object v1

    const-string v2, "acc_values"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[F)Landroid/content/Intent;

    move-result-object v0

    .line 172
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getIsUseLightFovOptimization()Z

    move-result v1

    const-string v2, "is_use_light_fov_optimization"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 173
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getExpId()I

    move-result v1

    const-string v2, "exp_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 174
    return-void
.end method
