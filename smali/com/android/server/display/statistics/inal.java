class inal extends android.database.ContentObserver {
	 /* .source "SwitchStatsHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/statistics/SwitchStatsHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x12 */
/* name = "SettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.display.statistics.SwitchStatsHelper this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$zMjECXgid0WTixQKc2obx998wa8 ( com.android.server.display.statistics.SwitchStatsHelper$SettingsObserver p0, android.net.Uri p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;->lambda$onChange$0(Landroid/net/Uri;)V */
return;
} // .end method
public inal ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 468 */
this.this$0 = p1;
/* .line 469 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 470 */
return;
} // .end method
private void lambda$onChange$0 ( android.net.Uri p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "uri" # Landroid/net/Uri; */
/* .line 474 */
v0 = this.this$0;
com.android.server.display.statistics.SwitchStatsHelper .-$$Nest$mhandleSettingsChangeEvent ( v0,p1 );
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 474 */
v0 = this.this$0;
com.android.server.display.statistics.SwitchStatsHelper .-$$Nest$fgetmBgHandler ( v0 );
/* new-instance v1, Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p2}, Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;Landroid/net/Uri;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 475 */
return;
} // .end method
