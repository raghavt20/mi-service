.class public Lcom/android/server/display/statistics/AggregationEvent$CbmAggregationEvent;
.super Lcom/android/server/display/statistics/AggregationEvent;
.source "AggregationEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/statistics/AggregationEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CbmAggregationEvent"
.end annotation


# static fields
.field protected static final EVENT_CUSTOM_BRIGHTNESS_ADJUST:Ljava/lang/String; = "custom_brightness_adj"

.field protected static final EVENT_CUSTOM_BRIGHTNESS_USAGE:Ljava/lang/String; = "custom_brightness_usage"

.field protected static final EVENT_INDIVIDUAL_MODEL_PREDICT:Ljava/lang/String; = "individual_model_predict"

.field protected static final EVENT_INDIVIDUAL_MODEL_TRAIN:Ljava/lang/String; = "individual_model_train"

.field protected static final KEY_CURRENT_INDIVIDUAL_MODEL_MAE:Ljava/lang/String; = "cur_train_mae"

.field protected static final KEY_CURRENT_INDIVIDUAL_MODEL_MAPE:Ljava/lang/String; = "cur_train_mape"

.field private static final KEY_CUSTOM_CURVE_AUTO_BRIGHTNESS_ADJUST:Ljava/lang/String; = "custom_curve_auto_brt_adj"

.field private static final KEY_CUSTOM_CURVE_AUTO_BRIGHTNESS_MANUAL_ADJUST:Ljava/lang/String; = "custom_curve_auto_brt_manual_adj"

.field private static final KEY_CUSTOM_CURVE_DURATION:Ljava/lang/String; = "custom_curve_duration"

.field private static final KEY_DEFAULT_AUTO_BRIGHTNESS_ADJUST:Ljava/lang/String; = "default_auto_brt_adj"

.field private static final KEY_DEFAULT_AUTO_BRIGHTNESS_MANUAL_ADJUST:Ljava/lang/String; = "default_auto_brt_manual_adj"

.field private static final KEY_DEFAULT_AUTO_DURATION:Ljava/lang/String; = "default_auto_duration"

.field private static final KEY_INDIVIDUAL_MODEL_AUTO_BRIGHTNESS_ADJUST:Ljava/lang/String; = "model_auto_brt_adj"

.field private static final KEY_INDIVIDUAL_MODEL_AUTO_BRIGHTNESS_MANUAL_ADJUST:Ljava/lang/String; = "model_auto_brt_manual_adj"

.field private static final KEY_INDIVIDUAL_MODEL_DURATION:Ljava/lang/String; = "model_duration"

.field protected static final KEY_INDIVIDUAL_MODEL_PREDICT_AVERAGE_DURATION:Ljava/lang/String; = "model_predict_average_duration"

.field protected static final KEY_INDIVIDUAL_MODEL_PREDICT_TIMEOUT_COUNT:Ljava/lang/String; = "model_predict_timeout_count"

.field protected static final KEY_INDIVIDUAL_MODEL_TRAIN_COUNT:Ljava/lang/String; = "model_train_count"

.field protected static final KEY_INDIVIDUAL_MODEL_TRAIN_INDICATORS:Ljava/lang/String; = "model_train_indicators"

.field protected static final KEY_INDIVIDUAL_MODEL_TRAIN_LOSS:Ljava/lang/String; = "train_loss"

.field protected static final KEY_INDIVIDUAL_MODEL_TRAIN_SAMPLE_NUM:Ljava/lang/String; = "sample_num"

.field protected static final KEY_INDIVIDUAL_MODEL_TRAIN_TIMESTAMP:Ljava/lang/String; = "timestamp"

.field protected static final KEY_INDIVIDUAL_MODEL_VALIDATION_FAIL_COUNT:Ljava/lang/String; = "model_validation_fail_count"

.field protected static final KEY_INDIVIDUAL_MODEL_VALIDATION_SUCCESS_COUNT:Ljava/lang/String; = "model_validation_success_count"

.field protected static final KEY_PREVIOUS_INDIVIDUAL_MODEL_MAE:Ljava/lang/String; = "pre_train_mae"

.field protected static final KEY_PREVIOUS_INDIVIDUAL_MODEL_MAPE:Ljava/lang/String; = "pre_train_mape"


# instance fields
.field private final mCbmCacheDataMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mCbmQuotaEvents:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 157
    invoke-direct {p0}, Lcom/android/server/display/statistics/AggregationEvent;-><init>()V

    .line 214
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/statistics/AggregationEvent$CbmAggregationEvent;->mCbmQuotaEvents:Ljava/util/Map;

    .line 216
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/statistics/AggregationEvent$CbmAggregationEvent;->mCbmCacheDataMap:Ljava/util/Map;

    return-void
.end method

.method public static getCbmAutoAdjustQuotaName(IZ)Ljava/lang/String;
    .locals 1
    .param p0, "cbmState"    # I
    .param p1, "isManuallySet"    # Z

    .line 235
    packed-switch p0, :pswitch_data_0

    .line 246
    if-eqz p1, :cond_2

    const-string v0, "default_auto_brt_manual_adj"

    goto :goto_2

    .line 241
    :pswitch_0
    if-eqz p1, :cond_0

    const-string v0, "model_auto_brt_manual_adj"

    goto :goto_0

    .line 242
    :cond_0
    const-string v0, "model_auto_brt_adj"

    :goto_0
    nop

    .line 243
    .local v0, "name":Ljava/lang/String;
    goto :goto_3

    .line 237
    .end local v0    # "name":Ljava/lang/String;
    :pswitch_1
    if-eqz p1, :cond_1

    const-string v0, "custom_curve_auto_brt_manual_adj"

    goto :goto_1

    .line 238
    :cond_1
    const-string v0, "custom_curve_auto_brt_adj"

    :goto_1
    nop

    .line 239
    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_3

    .line 247
    .end local v0    # "name":Ljava/lang/String;
    :cond_2
    const-string v0, "default_auto_brt_adj"

    :goto_2
    nop

    .line 250
    .restart local v0    # "name":Ljava/lang/String;
    :goto_3
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getCbmBrtUsageQuotaName(I)Ljava/lang/String;
    .locals 1
    .param p0, "cbmState"    # I

    .line 255
    packed-switch p0, :pswitch_data_0

    .line 264
    const-string v0, "default_auto_duration"

    .local v0, "name":Ljava/lang/String;
    goto :goto_0

    .line 260
    .end local v0    # "name":Ljava/lang/String;
    :pswitch_0
    const-string v0, "model_duration"

    .line 261
    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0

    .line 257
    .end local v0    # "name":Ljava/lang/String;
    :pswitch_1
    const-string v0, "custom_curve_duration"

    .line 258
    .restart local v0    # "name":Ljava/lang/String;
    nop

    .line 267
    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getCacheDataMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation

    .line 230
    iget-object v0, p0, Lcom/android/server/display/statistics/AggregationEvent$CbmAggregationEvent;->mCbmCacheDataMap:Ljava/util/Map;

    return-object v0
.end method

.method public getQuotaEvents()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 225
    iget-object v0, p0, Lcom/android/server/display/statistics/AggregationEvent$CbmAggregationEvent;->mCbmQuotaEvents:Ljava/util/Map;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/android/server/display/statistics/AggregationEvent$CbmAggregationEvent;->mCbmQuotaEvents:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
