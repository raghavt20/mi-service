public class com.android.server.display.statistics.BrightnessEvent {
	 /* .source "BrightnessEvent.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer EVENT_AUTO_CHANGED_BRIGHTNESS;
public static final Integer EVENT_AUTO_MANUAL_CHANGED_BRIGHTNESS;
public static final Integer EVENT_BRIGHTNESS_UNDEFINED;
public static final Integer EVENT_DISABLE_AUTO_BRIGHTNESS;
public static final Integer EVENT_MANUAL_CHANGED_BRIGHTNESS;
public static final Integer EVENT_SUNLIGHT_CHANGED_BRIGHTNESS;
public static final Integer EVENT_WINDOW_CHANGED_BRIGHTNESS;
/* # instance fields */
private acc_values;
private Float actual_nit;
private Integer affect_factor_flag;
private java.util.List all_stats_entries;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Float ambient_lux;
private Integer ambient_lux_span;
private Float assist_ambient_lux;
private Boolean brightness_restricted_enable;
private java.util.Map brightness_usage_map;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer current_user_id;
private Float display_gray_scale;
private Integer expId;
private java.lang.String extra;
private Boolean hdr_layer_enable;
private Boolean is_default_config;
private Boolean is_use_light_fov_optimization;
private Float last_assist_ambient_lux;
private Float last_main_ambient_lux;
private Boolean low_power_mode_flag;
private Float main_ambient_lux;
private Integer orientation;
private Float original_nit;
private Float previous_brightness;
private Integer previous_brightness_span;
private Float screen_brightness;
private Integer screen_brightness_span;
private Long time_stamp;
private java.lang.String top_package;
private Integer type;
private Float user_data_point;
/* # direct methods */
public com.android.server.display.statistics.BrightnessEvent ( ) {
/* .locals 2 */
/* .line 53 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 22 */
/* const/high16 v0, -0x40800000 # -1.0f */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->ambient_lux:F */
/* .line 25 */
final String v1 = ""; // const-string v1, ""
this.top_package = v1;
/* .line 27 */
this.extra = v1;
/* .line 30 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->is_default_config:Z */
/* .line 31 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->user_data_point:F */
/* .line 34 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.all_stats_entries = v1;
/* .line 35 */
int v1 = -1; // const/4 v1, -0x1
/* iput v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->affect_factor_flag:I */
/* .line 36 */
/* iput v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->screen_brightness_span:I */
/* .line 37 */
/* iput v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->previous_brightness_span:I */
/* .line 38 */
/* iput v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->ambient_lux_span:I */
/* .line 39 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
this.brightness_usage_map = v1;
/* .line 40 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->display_gray_scale:F */
/* .line 41 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->original_nit:F */
/* .line 42 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->actual_nit:F */
/* .line 45 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->main_ambient_lux:F */
/* .line 46 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->assist_ambient_lux:F */
/* .line 47 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->last_main_ambient_lux:F */
/* .line 48 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->last_assist_ambient_lux:F */
/* .line 49 */
int v0 = 3; // const/4 v0, 0x3
/* new-array v0, v0, [F */
this.acc_values = v0;
/* .line 55 */
return;
} // .end method
public static java.lang.String timestamp2String ( Long p0 ) {
/* .locals 3 */
/* .param p0, "time" # J */
/* .line 121 */
/* new-instance v0, Ljava/util/Date; */
/* invoke-direct {v0, p0, p1}, Ljava/util/Date;-><init>(J)V */
/* .line 122 */
/* .local v0, "d":Ljava/util/Date; */
/* new-instance v1, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v2, "yyyy-MM-dd HH:mm:ss:sss" */
/* invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 123 */
/* .local v1, "sf":Ljava/text/SimpleDateFormat; */
(( java.text.SimpleDateFormat ) v1 ).format ( v0 ); // invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
} // .end method
/* # virtual methods */
public getAccValues ( ) {
/* .locals 1 */
/* .line 303 */
v0 = this.acc_values;
} // .end method
public Float getActualNit ( ) {
/* .locals 1 */
/* .line 240 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->actual_nit:F */
} // .end method
public Integer getAffectFactorFlag ( ) {
/* .locals 1 */
/* .line 186 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->affect_factor_flag:I */
} // .end method
public Float getAmbientLux ( ) {
/* .locals 1 */
/* .line 72 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->ambient_lux:F */
} // .end method
public Float getAssistAmbientLux ( ) {
/* .locals 1 */
/* .line 276 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->assist_ambient_lux:F */
} // .end method
public Boolean getBrightnessRestrictedEnable ( ) {
/* .locals 1 */
/* .line 258 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->brightness_restricted_enable:Z */
} // .end method
public java.util.Map getBrightnessUsageMap ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 222 */
v0 = this.brightness_usage_map;
} // .end method
public Integer getCurBrightnessSpanIndex ( ) {
/* .locals 1 */
/* .line 195 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->screen_brightness_span:I */
} // .end method
public Float getDisplayGrayScale ( ) {
/* .locals 1 */
/* .line 231 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->display_gray_scale:F */
} // .end method
public Integer getEventType ( ) {
/* .locals 1 */
/* .line 63 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->type:I */
} // .end method
public Integer getExpId ( ) {
/* .locals 1 */
/* .line 325 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->expId:I */
} // .end method
public java.lang.String getExtra ( ) {
/* .locals 1 */
/* .line 117 */
v0 = this.extra;
} // .end method
public java.lang.String getForegroundPackage ( ) {
/* .locals 1 */
/* .line 99 */
v0 = this.top_package;
} // .end method
public Boolean getHdrLayerEnable ( ) {
/* .locals 1 */
/* .line 249 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->hdr_layer_enable:Z */
} // .end method
public Boolean getIsUseLightFovOptimization ( ) {
/* .locals 1 */
/* .line 312 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->is_use_light_fov_optimization:Z */
} // .end method
public Float getLastAssistAmbientLux ( ) {
/* .locals 1 */
/* .line 294 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->last_assist_ambient_lux:F */
} // .end method
public Float getLastMainAmbientLux ( ) {
/* .locals 1 */
/* .line 285 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->last_main_ambient_lux:F */
} // .end method
public Boolean getLowPowerModeFlag ( ) {
/* .locals 1 */
/* .line 159 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->low_power_mode_flag:Z */
} // .end method
public Integer getLuxSpanIndex ( ) {
/* .locals 1 */
/* .line 213 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->ambient_lux_span:I */
} // .end method
public Float getMainAmbientLux ( ) {
/* .locals 1 */
/* .line 267 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->main_ambient_lux:F */
} // .end method
public Integer getOrientation ( ) {
/* .locals 1 */
/* .line 90 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->orientation:I */
} // .end method
public Float getOriginalNit ( ) {
/* .locals 1 */
/* .line 321 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->original_nit:F */
} // .end method
public Integer getPreBrightnessSpanIndex ( ) {
/* .locals 1 */
/* .line 204 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->previous_brightness_span:I */
} // .end method
public Float getPreviousBrightness ( ) {
/* .locals 1 */
/* .line 132 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->previous_brightness:F */
} // .end method
public Float getScreenBrightness ( ) {
/* .locals 1 */
/* .line 81 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->screen_brightness:F */
} // .end method
public java.util.List getSwitchStats ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 177 */
v0 = this.all_stats_entries;
} // .end method
public Long getTimeStamp ( ) {
/* .locals 2 */
/* .line 108 */
/* iget-wide v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->time_stamp:J */
/* return-wide v0 */
} // .end method
public Float getUserDataPoint ( ) {
/* .locals 1 */
/* .line 141 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->user_data_point:F */
} // .end method
public Integer getUserId ( ) {
/* .locals 1 */
/* .line 168 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->current_user_id:I */
} // .end method
public Boolean isDefaultConfig ( ) {
/* .locals 1 */
/* .line 150 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessEvent;->is_default_config:Z */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setAccValues ( Float[] p0 ) {
/* .locals 0 */
/* .param p1, "accValues" # [F */
/* .line 298 */
this.acc_values = p1;
/* .line 299 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setActualNit ( Float p0 ) {
/* .locals 0 */
/* .param p1, "nit" # F */
/* .line 235 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->actual_nit:F */
/* .line 236 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setAffectFactorFlag ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "flag" # I */
/* .line 181 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->affect_factor_flag:I */
/* .line 182 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setAmbientLux ( Float p0 ) {
/* .locals 0 */
/* .param p1, "ambientLux" # F */
/* .line 67 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->ambient_lux:F */
/* .line 68 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setAssistAmbientLux ( Float p0 ) {
/* .locals 0 */
/* .param p1, "lux" # F */
/* .line 271 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->assist_ambient_lux:F */
/* .line 272 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setBrightnessRestrictedEnable ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 253 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->brightness_restricted_enable:Z */
/* .line 254 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setBrightnessUsageMap ( java.util.Map p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Long;", */
/* ">;)", */
/* "Lcom/android/server/display/statistics/BrightnessEvent;" */
/* } */
} // .end annotation
/* .line 217 */
/* .local p1, "brightness_usage_map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Long;>;" */
this.brightness_usage_map = p1;
/* .line 218 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setCurBrightnessSpanIndex ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "span" # I */
/* .line 190 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->screen_brightness_span:I */
/* .line 191 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setDisplayGrayScale ( Float p0 ) {
/* .locals 0 */
/* .param p1, "gray" # F */
/* .line 226 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->display_gray_scale:F */
/* .line 227 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setEventType ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "type" # I */
/* .line 58 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->type:I */
/* .line 59 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setExtra ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "string" # Ljava/lang/String; */
/* .line 112 */
this.extra = p1;
/* .line 113 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setForegroundPackage ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "top_package" # Ljava/lang/String; */
/* .line 94 */
this.top_package = p1;
/* .line 95 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setHdrLayerEnable ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 244 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->hdr_layer_enable:Z */
/* .line 245 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setIsDefaultConfig ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "defaultConfig" # Z */
/* .line 145 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->is_default_config:Z */
/* .line 146 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setIsUseLightFovOptimization ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isUseLightFovOptimization" # Z */
/* .line 307 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->is_use_light_fov_optimization:Z */
/* .line 308 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setLastAssistAmbientLux ( Float p0 ) {
/* .locals 0 */
/* .param p1, "lux" # F */
/* .line 289 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->last_assist_ambient_lux:F */
/* .line 290 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setLastMainAmbientLux ( Float p0 ) {
/* .locals 0 */
/* .param p1, "lux" # F */
/* .line 280 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->last_main_ambient_lux:F */
/* .line 281 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setLowPowerModeFlag ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 154 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->low_power_mode_flag:Z */
/* .line 155 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setLuxSpanIndex ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "span" # I */
/* .line 208 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->ambient_lux_span:I */
/* .line 209 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setMainAmbientLux ( Float p0 ) {
/* .locals 0 */
/* .param p1, "lux" # F */
/* .line 262 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->main_ambient_lux:F */
/* .line 263 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setOrientation ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "orientation" # I */
/* .line 85 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->orientation:I */
/* .line 86 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setOriginalNit ( Float p0 ) {
/* .locals 0 */
/* .param p1, "nit" # F */
/* .line 316 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->original_nit:F */
/* .line 317 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setPreBrightnessSpanIndex ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "span" # I */
/* .line 199 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->previous_brightness_span:I */
/* .line 200 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setPreviousBrightness ( Float p0 ) {
/* .locals 0 */
/* .param p1, "previousBrightness" # F */
/* .line 127 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->previous_brightness:F */
/* .line 128 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setScreenBrightness ( Float p0 ) {
/* .locals 0 */
/* .param p1, "screenBrightness" # F */
/* .line 76 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->screen_brightness:F */
/* .line 77 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setSwitchStats ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;", */
/* ">;)", */
/* "Lcom/android/server/display/statistics/BrightnessEvent;" */
/* } */
} // .end annotation
/* .line 172 */
/* .local p1, "all_stats_events":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;>;" */
this.all_stats_entries = p1;
/* .line 173 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setTimeStamp ( Long p0 ) {
/* .locals 0 */
/* .param p1, "timeStamp" # J */
/* .line 103 */
/* iput-wide p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->time_stamp:J */
/* .line 104 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setUserDataPoint ( Float p0 ) {
/* .locals 0 */
/* .param p1, "userDataPoint" # F */
/* .line 136 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->user_data_point:F */
/* .line 137 */
} // .end method
public com.android.server.display.statistics.BrightnessEvent setUserId ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "userId" # I */
/* .line 163 */
/* iput p1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->current_user_id:I */
/* .line 164 */
} // .end method
public java.lang.String toSimpleString ( ) {
/* .locals 3 */
/* .line 329 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->type:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ","; // const-string v1, ","
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessEvent;->ambient_lux:F */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessEvent;->screen_brightness:F */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessEvent;->orientation:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.top_package;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.extra;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", "; // const-string v2, ", "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessEvent;->previous_brightness:F */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessEvent;->is_default_config:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessEvent;->user_data_point:F */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessEvent;->low_power_mode_flag:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->current_user_id:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 339 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "{type:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->type:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",orientation:"; // const-string v1, ",orientation:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->orientation:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",top_package:"; // const-string v1, ",top_package:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.top_package;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",screen_brightness:"; // const-string v1, ",screen_brightness:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->screen_brightness:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ",previous_brightness:"; // const-string v1, ",previous_brightness:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->previous_brightness:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ",ambient_lux:"; // const-string v1, ",ambient_lux:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->ambient_lux:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ",user_data_point:"; // const-string v1, ",user_data_point:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->user_data_point:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ",is_default_config:"; // const-string v1, ",is_default_config:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->is_default_config:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ",screen_brightness_span:"; // const-string v1, ",screen_brightness_span:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->screen_brightness_span:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",previous_brightness_span:"; // const-string v1, ",previous_brightness_span:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->previous_brightness_span:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",ambient_lux_span:"; // const-string v1, ",ambient_lux_span:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->ambient_lux_span:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",all_stats_entries:"; // const-string v1, ",all_stats_entries:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.all_stats_entries;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ",affect_factor_flag:"; // const-string v1, ",affect_factor_flag:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->affect_factor_flag:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",display_gray_scale:"; // const-string v1, ",display_gray_scale:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->display_gray_scale:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ",low_power_mode_flag:"; // const-string v1, ",low_power_mode_flag:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->low_power_mode_flag:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ",current_user_id:"; // const-string v1, ",current_user_id:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->current_user_id:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",original_nit:"; // const-string v1, ",original_nit:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->original_nit:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ",actual_nit:"; // const-string v1, ",actual_nit:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->actual_nit:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ",hdr_layer_enable:"; // const-string v1, ",hdr_layer_enable:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->hdr_layer_enable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ",brightness_restricted_enable:"; // const-string v1, ",brightness_restricted_enable:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->brightness_restricted_enable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ",extra:"; // const-string v1, ",extra:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.extra;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",time_stamp:"; // const-string v1, ",time_stamp:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->time_stamp:J */
/* .line 358 */
com.android.server.display.statistics.BrightnessEvent .timestamp2String ( v1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",main_ambient_lux:"; // const-string v1, ",main_ambient_lux:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->main_ambient_lux:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ",assist_ambient_lux:"; // const-string v1, ",assist_ambient_lux:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->assist_ambient_lux:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ",last_main_ambient_lux:"; // const-string v1, ",last_main_ambient_lux:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->last_main_ambient_lux:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ",last_assist_ambient_lux:"; // const-string v1, ",last_assist_ambient_lux:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->last_assist_ambient_lux:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ",acc_values:"; // const-string v1, ",acc_values:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.acc_values;
/* .line 363 */
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",expId:"; // const-string v1, ",expId:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/BrightnessEvent;->expId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 339 */
} // .end method
