public class com.android.server.display.statistics.SwitchStatsHelper {
	 /* .source "SwitchStatsHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String AI_DISPLAY_MODE;
private static final java.lang.String BACKGROUND_BLUR_ENABLE;
private static final Boolean BACKGROUND_BLUR_SUPPORTED;
private static final java.lang.String COLOR_GAMUT_MODE;
public static final java.lang.String DC_BACK_LIGHT_SWITCH;
private static final java.lang.String EXPERT_DATA;
private static final java.lang.String IS_SMART_FPS;
private static final java.lang.String MIUI_SCREEN_COMPAT;
private static final java.lang.String SCREEN_ENHANCE_ENGINE_GALLERY_AI_MODE_STATUS;
private static final Boolean SUPPORT_RESOLUTION_SWITCH;
private static final Boolean SUPPORT_SMART_FPS;
public static final java.lang.String USER_REFRESH_RATE;
private static com.android.server.display.statistics.SwitchStatsHelper mInstance;
private static mScreenResolutionSupported;
/* # instance fields */
private Boolean mAdaptiveSleepEnable;
private Boolean mAiDisplayModeEnable;
private Boolean mAutoBrightnessSettingsEnable;
private Boolean mBackgroundBlurEnable;
private android.os.Handler mBgHandler;
private Integer mColorGamutMode;
private android.content.Context mContext;
private Boolean mDarkModeSettingsEnable;
private Boolean mDcBacklightSettingsEnable;
private Boolean mDozeAlwaysOn;
private Boolean mReadModeSettingsEnable;
private Integer mRefreshRateFromDeviceFeature;
private android.content.ContentResolver mResolver;
private Integer mScreenColorLevel;
private Boolean mScreenCompat;
private Integer mScreenOptimizeSettingsMode;
private Boolean mScreenTrueToneEnable;
private com.android.server.display.statistics.SwitchStatsHelper$SettingsObserver mSettingsObserver;
private Boolean mSmartRefreshRateEnable;
private Boolean mSunlightSettingsEnable;
private Boolean mSupportAdaptiveSleep;
private Boolean mSupportAiDisplayMode;
private Boolean mSupportExpertMode;
private java.util.ArrayList mSwitchStats;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mUserRefreshRate;
/* # direct methods */
static android.os.Handler -$$Nest$fgetmBgHandler ( com.android.server.display.statistics.SwitchStatsHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBgHandler;
} // .end method
static void -$$Nest$mhandleSettingsChangeEvent ( com.android.server.display.statistics.SwitchStatsHelper p0, android.net.Uri p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/SwitchStatsHelper;->handleSettingsChangeEvent(Landroid/net/Uri;)V */
return;
} // .end method
static com.android.server.display.statistics.SwitchStatsHelper ( ) {
/* .locals 3 */
/* .line 35 */
/* nop */
/* .line 36 */
/* const-string/jumbo v0, "support_smart_fps" */
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
com.android.server.display.statistics.SwitchStatsHelper.SUPPORT_SMART_FPS = (v0!= 0);
/* .line 38 */
/* nop */
/* .line 39 */
/* const-string/jumbo v0, "screen_resolution_supported" */
miui.util.FeatureParser .getIntArray ( v0 );
/* .line 40 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* array-length v0, v0 */
int v2 = 1; // const/4 v2, 0x1
/* if-le v0, v2, :cond_0 */
} // :cond_0
/* move v2, v1 */
} // :goto_0
com.android.server.display.statistics.SwitchStatsHelper.SUPPORT_RESOLUTION_SWITCH = (v2!= 0);
/* .line 49 */
final String v0 = "persist.sys.background_blur_supported"; // const-string v0, "persist.sys.background_blur_supported"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.display.statistics.SwitchStatsHelper.BACKGROUND_BLUR_SUPPORTED = (v0!= 0);
return;
} // .end method
public com.android.server.display.statistics.SwitchStatsHelper ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 76 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 73 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mSwitchStats = v0;
/* .line 77 */
this.mContext = p1;
/* .line 78 */
/* new-instance v0, Landroid/os/Handler; */
com.android.internal.os.BackgroundThread .getHandler ( );
(( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mBgHandler = v0;
/* .line 79 */
/* new-instance v0, Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver; */
v1 = this.mBgHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/display/statistics/SwitchStatsHelper$SettingsObserver;-><init>(Lcom/android/server/display/statistics/SwitchStatsHelper;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 80 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mResolver = v0;
/* .line 81 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->readConfigFromDeviceFeature()V */
/* .line 82 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->loadSmartSwitches()V */
/* .line 83 */
(( com.android.server.display.statistics.SwitchStatsHelper ) p0 ).registerSettingsObserver ( ); // invoke-virtual {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->registerSettingsObserver()V
/* .line 84 */
return;
} // .end method
private Integer getColorLevelCode ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "mScreenColorLevel" # I */
/* .line 455 */
/* const v0, 0xffffff */
/* and-int/2addr v0, p1 */
/* .line 456 */
/* .local v0, "validRGB":I */
/* shr-int/lit8 v1, v0, 0x10 */
/* .line 457 */
/* .local v1, "value_R":I */
/* shr-int/lit8 v2, v0, 0x8 */
/* and-int/lit16 v2, v2, 0xff */
/* .line 458 */
/* .local v2, "value_G":I */
/* and-int/lit16 v3, v0, 0xff */
/* .line 459 */
/* .local v3, "value_B":I */
int v4 = -1; // const/4 v4, -0x1
/* if-eq p1, v4, :cond_3 */
/* if-ne v1, v2, :cond_0 */
/* if-ne v2, v3, :cond_0 */
/* .line 462 */
} // :cond_0
int v4 = 3; // const/4 v4, 0x3
/* if-le v1, v2, :cond_1 */
/* if-le v1, v3, :cond_2 */
int v4 = 1; // const/4 v4, 0x1
/* .line 463 */
} // :cond_1
/* if-ge v3, v2, :cond_2 */
int v4 = 2; // const/4 v4, 0x2
/* .line 462 */
} // :cond_2
} // :goto_0
/* .line 460 */
} // :cond_3
} // :goto_1
int v4 = 0; // const/4 v4, 0x0
} // .end method
private Integer getCurrentCloseLidDisplaySetting ( ) {
/* .locals 3 */
/* .line 436 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "close_lid_display_setting"; // const-string v1, "close_lid_display_setting"
int v2 = 1; // const/4 v2, 0x1
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
} // .end method
private Integer getCurrentRefreshRate ( ) {
/* .locals 3 */
/* .line 416 */
/* sget-boolean v0, Lcom/android/server/display/statistics/SwitchStatsHelper;->SUPPORT_SMART_FPS:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSmartRefreshRateEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 418 */
/* const/16 v0, -0x78 */
/* .line 420 */
} // :cond_0
final String v0 = "ro.vendor.fps.switch.default"; // const-string v0, "ro.vendor.fps.switch.default"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* .line 421 */
/* .local v0, "defaultFps":Z */
int v1 = -1; // const/4 v1, -0x1
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 422 */
/* iget v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mUserRefreshRate:I */
/* if-eq v2, v1, :cond_1 */
} // :cond_1
/* iget v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mRefreshRateFromDeviceFeature:I */
} // :goto_0
/* .line 424 */
} // :cond_2
final String v2 = "persist.vendor.dfps.level"; // const-string v2, "persist.vendor.dfps.level"
v1 = android.os.SystemProperties .getInt ( v2,v1 );
} // .end method
private Boolean getCurrentScreenCompat ( ) {
/* .locals 6 */
/* .line 442 */
int v0 = 0; // const/4 v0, 0x0
/* .line 443 */
/* .local v0, "isQhd":Z */
/* sget-boolean v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->SUPPORT_RESOLUTION_SWITCH:Z */
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 444 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getCurrentScreenResolution()I */
v4 = com.android.server.display.statistics.SwitchStatsHelper.mScreenResolutionSupported;
/* aget v5, v4, v3 */
/* aget v4, v4, v2 */
v4 = java.lang.Math .max ( v5,v4 );
/* if-ne v1, v4, :cond_0 */
/* move v1, v2 */
} // :cond_0
/* move v1, v3 */
} // :goto_0
/* move v0, v1 */
/* .line 447 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenCompat:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
} // :cond_2
/* move v2, v3 */
} // :goto_1
} // .end method
private Integer getCurrentScreenResolution ( ) {
/* .locals 3 */
/* .line 428 */
final String v0 = "persist.sys.miui_resolution"; // const-string v0, "persist.sys.miui_resolution"
int v1 = 0; // const/4 v1, 0x0
android.os.SystemProperties .get ( v0,v1 );
/* .line 429 */
/* .local v0, "screenResolution":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 430 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
int v2 = 0; // const/4 v2, 0x0
/* aget-object v1, v1, v2 */
java.lang.Integer .valueOf ( v1 );
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 432 */
} // :cond_0
/* const/16 v1, 0x5a0 */
} // .end method
public static com.android.server.display.statistics.SwitchStatsHelper getInstance ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 87 */
v0 = com.android.server.display.statistics.SwitchStatsHelper.mInstance;
/* if-nez v0, :cond_0 */
/* .line 88 */
/* new-instance v0, Lcom/android/server/display/statistics/SwitchStatsHelper; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;-><init>(Landroid/content/Context;)V */
/* .line 90 */
} // :cond_0
v0 = com.android.server.display.statistics.SwitchStatsHelper.mInstance;
} // .end method
private void handleSettingsChangeEvent ( android.net.Uri p0 ) {
/* .locals 18 */
/* .param p1, "uri" # Landroid/net/Uri; */
/* .line 218 */
/* move-object/from16 v0, p0 */
/* invoke-virtual/range {p1 ..p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String; */
v2 = (( java.lang.String ) v1 ).hashCode ( ); // invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
/* const-string/jumbo v4, "screen_paper_mode_enabled" */
final String v5 = "screen_optimize_mode"; // const-string v5, "screen_optimize_mode"
final String v6 = "is_smart_fps"; // const-string v6, "is_smart_fps"
/* const-string/jumbo v7, "ui_night_mode" */
final String v8 = "background_blur_enable"; // const-string v8, "background_blur_enable"
final String v9 = "screen_color_level"; // const-string v9, "screen_color_level"
final String v10 = "dc_back_light"; // const-string v10, "dc_back_light"
final String v11 = "doze_always_on"; // const-string v11, "doze_always_on"
final String v12 = "screen_brightness_mode"; // const-string v12, "screen_brightness_mode"
final String v13 = "screen_enhance_engine_gallery_ai_mode_status"; // const-string v13, "screen_enhance_engine_gallery_ai_mode_status"
final String v14 = "miui_screen_compat"; // const-string v14, "miui_screen_compat"
/* const-string/jumbo v15, "screen_true_tone" */
/* const-string/jumbo v3, "user_refresh_rate" */
final String v0 = "adaptive_sleep"; // const-string v0, "adaptive_sleep"
/* move-object/from16 v16, v0 */
/* const-string/jumbo v0, "sunlight_mode" */
/* move-object/from16 v17, v0 */
/* sparse-switch v2, :sswitch_data_0 */
/* move-object/from16 v2, v16 */
/* move-object/from16 v0, v17 */
/* goto/16 :goto_3 */
/* :sswitch_0 */
v1 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 5; // const/4 v1, 0x5
/* goto/16 :goto_0 */
/* :sswitch_1 */
v1 = (( java.lang.String ) v1 ).equals ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 4; // const/4 v1, 0x4
/* goto/16 :goto_0 */
/* :sswitch_2 */
final String v2 = "expert_data"; // const-string v2, "expert_data"
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0xd */
/* goto/16 :goto_0 */
/* :sswitch_3 */
v1 = (( java.lang.String ) v1 ).equals ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0x8 */
/* :sswitch_4 */
v1 = (( java.lang.String ) v1 ).equals ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move-object/from16 v2, v16 */
/* move-object/from16 v0, v17 */
int v1 = 1; // const/4 v1, 0x1
/* goto/16 :goto_4 */
/* :sswitch_5 */
v1 = (( java.lang.String ) v1 ).equals ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0xf */
/* :sswitch_6 */
v1 = (( java.lang.String ) v1 ).equals ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 6; // const/4 v1, 0x6
/* :sswitch_7 */
v1 = (( java.lang.String ) v1 ).equals ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move-object/from16 v2, v16 */
/* move-object/from16 v0, v17 */
int v1 = 0; // const/4 v1, 0x0
/* :sswitch_8 */
v1 = (( java.lang.String ) v1 ).equals ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0x9 */
/* :sswitch_9 */
v1 = (( java.lang.String ) v1 ).equals ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 3; // const/4 v1, 0x3
/* :sswitch_a */
v1 = (( java.lang.String ) v1 ).equals ( v13 ); // invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0xe */
/* :sswitch_b */
v1 = (( java.lang.String ) v1 ).equals ( v14 ); // invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0xb */
/* :sswitch_c */
v1 = (( java.lang.String ) v1 ).equals ( v15 ); // invoke-virtual {v1, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0xa */
/* :sswitch_d */
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 int v1 = 7; // const/4 v1, 0x7
} // :goto_0
/* move-object/from16 v2, v16 */
} // :cond_0
/* move-object/from16 v2, v16 */
/* :sswitch_e */
/* move-object/from16 v2, v16 */
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* const/16 v1, 0xc */
} // :goto_1
/* move-object/from16 v0, v17 */
} // :cond_1
} // :goto_2
/* move-object/from16 v0, v17 */
/* :sswitch_f */
/* move-object/from16 v2, v16 */
/* move-object/from16 v0, v17 */
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
int v1 = 2; // const/4 v1, 0x2
} // :cond_2
} // :goto_3
int v1 = -1; // const/4 v1, -0x1
} // :goto_4
/* move-object/from16 v17, v2 */
int v2 = -2; // const/4 v2, -0x2
/* packed-switch v1, :pswitch_data_0 */
/* move-object/from16 v1, p0 */
/* goto/16 :goto_10 */
/* .line 290 */
/* :pswitch_0 */
/* move-object/from16 v1, p0 */
v0 = this.mResolver;
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Secure .getInt ( v0,v8,v2 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_3 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
} // :goto_5
/* iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mBackgroundBlurEnable:Z */
/* .line 292 */
/* goto/16 :goto_10 */
/* .line 286 */
/* :pswitch_1 */
/* move-object/from16 v1, p0 */
v0 = this.mResolver;
android.provider.Settings$Global .getStringForUser ( v0,v13,v2 );
/* const-string/jumbo v2, "true" */
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAiDisplayModeEnable:Z */
/* .line 288 */
/* goto/16 :goto_10 */
/* .line 283 */
/* :pswitch_2 */
/* move-object/from16 v1, p0 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->updateColorGamutMode()V */
/* .line 284 */
/* goto/16 :goto_10 */
/* .line 279 */
/* :pswitch_3 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v0, v17 */
v3 = this.mResolver;
int v4 = -1; // const/4 v4, -0x1
v0 = android.provider.Settings$Secure .getIntForUser ( v3,v0,v4,v2 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_4 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_4
int v0 = 0; // const/4 v0, 0x0
} // :goto_6
/* iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAdaptiveSleepEnable:Z */
/* .line 281 */
/* goto/16 :goto_10 */
/* .line 275 */
/* :pswitch_4 */
/* move-object/from16 v1, p0 */
v0 = this.mResolver;
int v3 = -1; // const/4 v3, -0x1
v0 = android.provider.Settings$System .getIntForUser ( v0,v14,v3,v2 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_5 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_5
int v0 = 0; // const/4 v0, 0x0
} // :goto_7
/* iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenCompat:Z */
/* .line 277 */
/* goto/16 :goto_10 */
/* .line 270 */
/* :pswitch_5 */
/* move-object/from16 v1, p0 */
v0 = this.mResolver;
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v15,v3,v2 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_6 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_6
int v0 = 0; // const/4 v0, 0x0
} // :goto_8
/* iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenTrueToneEnable:Z */
/* .line 273 */
/* goto/16 :goto_10 */
/* .line 266 */
/* :pswitch_6 */
/* move-object/from16 v1, p0 */
v0 = this.mResolver;
int v3 = -1; // const/4 v3, -0x1
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v11,v3,v2 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_7 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_7
int v0 = 0; // const/4 v0, 0x0
} // :goto_9
/* iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDozeAlwaysOn:Z */
/* .line 268 */
/* goto/16 :goto_10 */
/* .line 262 */
/* :pswitch_7 */
/* move-object/from16 v1, p0 */
v0 = this.mResolver;
int v3 = -1; // const/4 v3, -0x1
v0 = android.provider.Settings$System .getIntForUser ( v0,v6,v3,v2 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_8 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_8
int v0 = 0; // const/4 v0, 0x0
} // :goto_a
/* iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSmartRefreshRateEnable:Z */
/* .line 264 */
/* goto/16 :goto_10 */
/* .line 258 */
/* :pswitch_8 */
/* move-object/from16 v1, p0 */
v0 = this.mResolver;
int v4 = -1; // const/4 v4, -0x1
v0 = android.provider.Settings$System .getIntForUser ( v0,v3,v4,v2 );
/* iput v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mUserRefreshRate:I */
/* .line 260 */
/* goto/16 :goto_10 */
/* .line 253 */
/* :pswitch_9 */
int v4 = -1; // const/4 v4, -0x1
/* move-object/from16 v1, p0 */
v0 = this.mResolver;
v0 = android.provider.Settings$System .getIntForUser ( v0,v9,v4,v2 );
/* iput v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenColorLevel:I */
/* .line 256 */
/* goto/16 :goto_10 */
/* .line 247 */
/* :pswitch_a */
/* move-object/from16 v1, p0 */
v0 = this.mResolver;
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v4,v3,v2 );
if ( v0 != null) { // if-eqz v0, :cond_9
int v0 = 1; // const/4 v0, 0x1
} // :cond_9
int v0 = 0; // const/4 v0, 0x0
} // :goto_b
/* iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mReadModeSettingsEnable:Z */
/* .line 251 */
/* .line 240 */
/* :pswitch_b */
/* move-object/from16 v1, p0 */
v0 = this.mResolver;
v0 = android.provider.Settings$System .getIntForUser ( v0,v5,v3,v2 );
/* iput v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenOptimizeSettingsMode:I */
/* .line 244 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->updateColorGamutMode()V */
/* .line 245 */
/* .line 234 */
/* :pswitch_c */
/* move-object/from16 v1, p0 */
v0 = this.mResolver;
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v12,v3,v2 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_a */
int v0 = 1; // const/4 v0, 0x1
} // :cond_a
int v0 = 0; // const/4 v0, 0x0
} // :goto_c
/* iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAutoBrightnessSettingsEnable:Z */
/* .line 238 */
/* .line 228 */
/* :pswitch_d */
/* move-object/from16 v1, p0 */
v3 = this.mResolver;
int v4 = 0; // const/4 v4, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v3,v0,v4,v2 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_b */
int v0 = 1; // const/4 v0, 0x1
} // :cond_b
/* move v0, v4 */
} // :goto_d
/* iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSunlightSettingsEnable:Z */
/* .line 232 */
/* .line 224 */
/* :pswitch_e */
int v4 = 0; // const/4 v4, 0x0
/* move-object/from16 v1, p0 */
v0 = this.mResolver;
int v3 = -1; // const/4 v3, -0x1
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v7,v3,v2 );
int v2 = 2; // const/4 v2, 0x2
/* if-ne v0, v2, :cond_c */
int v0 = 1; // const/4 v0, 0x1
} // :cond_c
/* move v0, v4 */
} // :goto_e
/* iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDarkModeSettingsEnable:Z */
/* .line 226 */
/* .line 220 */
/* :pswitch_f */
int v4 = 0; // const/4 v4, 0x0
/* move-object/from16 v1, p0 */
v0 = this.mResolver;
int v3 = -1; // const/4 v3, -0x1
v0 = android.provider.Settings$System .getIntForUser ( v0,v10,v3,v2 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_d */
/* move v0, v2 */
} // :cond_d
/* move v0, v4 */
} // :goto_f
/* iput-boolean v0, v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDcBacklightSettingsEnable:Z */
/* .line 222 */
/* nop */
/* .line 296 */
} // :goto_10
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x69203588 -> :sswitch_f */
/* -0x46cdc832 -> :sswitch_e */
/* -0x45a4bc48 -> :sswitch_d */
/* -0x4241e690 -> :sswitch_c */
/* -0x346f7e1a -> :sswitch_b */
/* -0x3086abc3 -> :sswitch_a */
/* -0x294f7102 -> :sswitch_9 */
/* -0x611a9fa -> :sswitch_8 */
/* 0x859e77e -> :sswitch_7 */
/* 0x2807b455 -> :sswitch_6 */
/* 0x2c525eca -> :sswitch_5 */
/* 0x46be7ff5 -> :sswitch_4 */
/* 0x5bcc609e -> :sswitch_3 */
/* 0x7451049f -> :sswitch_2 */
/* 0x74fb4732 -> :sswitch_1 */
/* 0x7e544b2b -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void loadSettings ( ) {
/* .locals 7 */
/* .line 155 */
v0 = this.mResolver;
final String v1 = "dc_back_light"; // const-string v1, "dc_back_light"
int v2 = -1; // const/4 v2, -0x1
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$System .getIntForUser ( v0,v1,v2,v3 );
int v1 = 0; // const/4 v1, 0x0
int v4 = 1; // const/4 v4, 0x1
/* if-ne v0, v4, :cond_0 */
/* move v0, v4 */
} // :cond_0
/* move v0, v1 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDcBacklightSettingsEnable:Z */
/* .line 157 */
v0 = this.mResolver;
/* const-string/jumbo v5, "ui_night_mode" */
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v5,v2,v3 );
int v5 = 2; // const/4 v5, 0x2
/* if-ne v0, v5, :cond_1 */
/* move v0, v4 */
} // :cond_1
/* move v0, v1 */
} // :goto_1
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDarkModeSettingsEnable:Z */
/* .line 159 */
v0 = this.mResolver;
/* const-string/jumbo v5, "sunlight_mode" */
v0 = android.provider.Settings$System .getIntForUser ( v0,v5,v1,v3 );
/* if-ne v0, v4, :cond_2 */
/* move v0, v4 */
} // :cond_2
/* move v0, v1 */
} // :goto_2
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSunlightSettingsEnable:Z */
/* .line 163 */
v0 = this.mResolver;
final String v5 = "screen_brightness_mode"; // const-string v5, "screen_brightness_mode"
v0 = android.provider.Settings$System .getIntForUser ( v0,v5,v1,v3 );
/* if-ne v0, v4, :cond_3 */
/* move v0, v4 */
} // :cond_3
/* move v0, v1 */
} // :goto_3
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAutoBrightnessSettingsEnable:Z */
/* .line 167 */
v0 = this.mResolver;
final String v5 = "screen_optimize_mode"; // const-string v5, "screen_optimize_mode"
v0 = android.provider.Settings$System .getIntForUser ( v0,v5,v6,v3 );
/* iput v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenOptimizeSettingsMode:I */
/* .line 171 */
v0 = this.mResolver;
/* const-string/jumbo v5, "screen_paper_mode_enabled" */
v0 = android.provider.Settings$System .getIntForUser ( v0,v5,v1,v3 );
if ( v0 != null) { // if-eqz v0, :cond_4
/* move v0, v4 */
} // :cond_4
/* move v0, v1 */
} // :goto_4
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mReadModeSettingsEnable:Z */
/* .line 175 */
v0 = this.mResolver;
final String v5 = "screen_color_level"; // const-string v5, "screen_color_level"
v0 = android.provider.Settings$System .getIntForUser ( v0,v5,v2,v3 );
/* iput v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenColorLevel:I */
/* .line 177 */
v0 = this.mResolver;
/* const-string/jumbo v5, "user_refresh_rate" */
v0 = android.provider.Settings$System .getIntForUser ( v0,v5,v2,v3 );
/* iput v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mUserRefreshRate:I */
/* .line 179 */
v0 = this.mResolver;
final String v5 = "is_smart_fps"; // const-string v5, "is_smart_fps"
v0 = android.provider.Settings$System .getIntForUser ( v0,v5,v2,v3 );
/* if-ne v0, v4, :cond_5 */
/* move v0, v4 */
} // :cond_5
/* move v0, v1 */
} // :goto_5
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSmartRefreshRateEnable:Z */
/* .line 181 */
v0 = this.mResolver;
final String v5 = "doze_always_on"; // const-string v5, "doze_always_on"
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v5,v2,v3 );
/* if-ne v0, v4, :cond_6 */
/* move v0, v4 */
} // :cond_6
/* move v0, v1 */
} // :goto_6
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDozeAlwaysOn:Z */
/* .line 183 */
v0 = this.mResolver;
/* const-string/jumbo v5, "screen_true_tone" */
v0 = android.provider.Settings$System .getIntForUser ( v0,v5,v1,v3 );
/* if-ne v0, v4, :cond_7 */
/* move v0, v4 */
} // :cond_7
/* move v0, v1 */
} // :goto_7
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenTrueToneEnable:Z */
/* .line 186 */
v0 = this.mResolver;
final String v5 = "miui_screen_compat"; // const-string v5, "miui_screen_compat"
v0 = android.provider.Settings$System .getIntForUser ( v0,v5,v2,v3 );
/* if-ne v0, v4, :cond_8 */
/* move v0, v4 */
} // :cond_8
/* move v0, v1 */
} // :goto_8
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenCompat:Z */
/* .line 188 */
v0 = this.mResolver;
final String v5 = "adaptive_sleep"; // const-string v5, "adaptive_sleep"
v0 = android.provider.Settings$System .getIntForUser ( v0,v5,v2,v3 );
/* if-ne v0, v4, :cond_9 */
/* move v0, v4 */
} // :cond_9
/* move v0, v1 */
} // :goto_9
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAdaptiveSleepEnable:Z */
/* .line 190 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->updateColorGamutMode()V */
/* .line 191 */
v0 = this.mResolver;
final String v2 = "screen_enhance_engine_gallery_ai_mode_status"; // const-string v2, "screen_enhance_engine_gallery_ai_mode_status"
android.provider.Settings$Global .getStringForUser ( v0,v2,v3 );
/* const-string/jumbo v2, "true" */
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAiDisplayModeEnable:Z */
/* .line 193 */
v0 = this.mResolver;
final String v2 = "background_blur_enable"; // const-string v2, "background_blur_enable"
v0 = android.provider.Settings$Secure .getInt ( v0,v2,v1 );
/* if-ne v0, v4, :cond_a */
/* move v1, v4 */
} // :cond_a
/* iput-boolean v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mBackgroundBlurEnable:Z */
/* .line 194 */
return;
} // .end method
private void loadSmartSwitches ( ) {
/* .locals 6 */
/* .line 94 */
/* sget-boolean v0, Lcom/android/server/display/statistics/SwitchStatsHelper;->SUPPORT_SMART_FPS:Z */
int v1 = -2; // const/4 v1, -0x2
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
int v4 = -1; // const/4 v4, -0x1
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 95 */
v0 = this.mResolver;
final String v5 = "is_smart_fps"; // const-string v5, "is_smart_fps"
v0 = android.provider.Settings$System .getInt ( v0,v5,v4 );
/* if-ne v0, v4, :cond_0 */
/* move v0, v3 */
} // :cond_0
/* move v0, v2 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSmartRefreshRateEnable:Z */
/* .line 96 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 97 */
v0 = this.mResolver;
android.provider.Settings$System .putIntForUser ( v0,v5,v3,v1 );
/* .line 100 */
} // :cond_1
/* sget-boolean v0, Lcom/android/server/display/statistics/SwitchStatsHelper;->SUPPORT_RESOLUTION_SWITCH:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 101 */
v0 = this.mResolver;
final String v5 = "miui_screen_compat"; // const-string v5, "miui_screen_compat"
v0 = android.provider.Settings$System .getInt ( v0,v5,v4 );
/* if-ne v0, v4, :cond_2 */
/* move v2, v3 */
} // :cond_2
/* iput-boolean v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenCompat:Z */
/* .line 102 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 103 */
v0 = this.mResolver;
android.provider.Settings$System .putIntForUser ( v0,v5,v3,v1 );
/* .line 107 */
} // :cond_3
return;
} // .end method
private void readConfigFromDeviceFeature ( ) {
/* .locals 2 */
/* .line 209 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 210 */
/* const v1, 0x1110013 */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSupportAdaptiveSleep:Z */
/* .line 211 */
final String v0 = "defaultFps"; // const-string v0, "defaultFps"
int v1 = -1; // const/4 v1, -0x1
v0 = miui.util.FeatureParser .getInteger ( v0,v1 );
/* iput v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mRefreshRateFromDeviceFeature:I */
/* .line 212 */
/* const-string/jumbo v0, "support_AI_display" */
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSupportAiDisplayMode:Z */
/* .line 213 */
/* const-string/jumbo v0, "support_display_expert_mode" */
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSupportExpertMode:Z */
/* .line 215 */
return;
} // .end method
private void updateColorGamutMode ( ) {
/* .locals 2 */
/* .line 197 */
/* iget v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenOptimizeSettingsMode:I */
int v1 = 4; // const/4 v1, 0x4
/* if-ne v0, v1, :cond_1 */
/* .line 198 */
v0 = this.mContext;
com.android.server.display.expertmode.ExpertData .getFromDatabase ( v0 );
/* .line 199 */
/* .local v0, "data":Lcom/android/server/display/expertmode/ExpertData; */
/* if-nez v0, :cond_0 */
/* .line 200 */
com.android.server.display.expertmode.ExpertData .getDefaultValue ( );
/* .line 202 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
v1 = (( com.android.server.display.expertmode.ExpertData ) v0 ).getByCookie ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/expertmode/ExpertData;->getByCookie(I)I
/* iput v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mColorGamutMode:I */
/* .line 203 */
} // .end local v0 # "data":Lcom/android/server/display/expertmode/ExpertData;
/* .line 204 */
} // :cond_1
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mColorGamutMode:I */
/* .line 206 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
public java.util.List getAllSwitchStats ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 323 */
(( com.android.server.display.statistics.SwitchStatsHelper ) p0 ).updateSwitchStatsValue ( ); // invoke-virtual {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->updateSwitchStatsValue()V
/* .line 324 */
v0 = this.mSwitchStats;
} // .end method
public Integer getScreenOptimizeSettingsMode ( ) {
/* .locals 1 */
/* .line 319 */
/* iget v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenOptimizeSettingsMode:I */
} // .end method
public Boolean isAutoBrightnessSettingsEnable ( ) {
/* .locals 1 */
/* .line 311 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAutoBrightnessSettingsEnable:Z */
} // .end method
public Boolean isDarkModeSettingsEnable ( ) {
/* .locals 1 */
/* .line 307 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDarkModeSettingsEnable:Z */
} // .end method
public Boolean isDcBacklightSettingsEnable ( ) {
/* .locals 1 */
/* .line 299 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDcBacklightSettingsEnable:Z */
} // .end method
public Boolean isReadModeSettingsEnable ( ) {
/* .locals 1 */
/* .line 315 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mReadModeSettingsEnable:Z */
} // .end method
public Boolean isSunlightSettingsEnable ( ) {
/* .locals 1 */
/* .line 303 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSunlightSettingsEnable:Z */
} // .end method
protected void registerSettingsObserver ( ) {
/* .locals 5 */
/* .line 111 */
v0 = this.mResolver;
final String v1 = "screen_brightness_mode"; // const-string v1, "screen_brightness_mode"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 114 */
v0 = this.mResolver;
/* const-string/jumbo v1, "sunlight_mode" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 117 */
v0 = this.mResolver;
final String v1 = "screen_optimize_mode"; // const-string v1, "screen_optimize_mode"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 120 */
v0 = this.mResolver;
final String v1 = "dc_back_light"; // const-string v1, "dc_back_light"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 122 */
v0 = this.mResolver;
/* const-string/jumbo v1, "ui_night_mode" */
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 124 */
v0 = this.mResolver;
final String v1 = "screen_color_level"; // const-string v1, "screen_color_level"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 127 */
v0 = this.mResolver;
/* const-string/jumbo v1, "screen_paper_mode_enabled" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 130 */
v0 = this.mResolver;
/* const-string/jumbo v1, "user_refresh_rate" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 132 */
v0 = this.mResolver;
final String v1 = "is_smart_fps"; // const-string v1, "is_smart_fps"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 134 */
v0 = this.mResolver;
final String v1 = "doze_always_on"; // const-string v1, "doze_always_on"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 136 */
v0 = this.mResolver;
/* const-string/jumbo v1, "screen_true_tone" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 139 */
v0 = this.mResolver;
final String v1 = "miui_screen_compat"; // const-string v1, "miui_screen_compat"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 141 */
v0 = this.mResolver;
final String v1 = "adaptive_sleep"; // const-string v1, "adaptive_sleep"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 143 */
v0 = this.mResolver;
final String v1 = "expert_data"; // const-string v1, "expert_data"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 145 */
v0 = this.mResolver;
/* .line 146 */
final String v1 = "screen_enhance_engine_gallery_ai_mode_status"; // const-string v1, "screen_enhance_engine_gallery_ai_mode_status"
android.provider.Settings$Global .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 145 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 148 */
v0 = this.mResolver;
/* .line 149 */
final String v1 = "background_blur_enable"; // const-string v1, "background_blur_enable"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 148 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 151 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->loadSettings()V */
/* .line 152 */
return;
} // .end method
public void updateSwitchStatsValue ( ) {
/* .locals 6 */
/* .line 328 */
v0 = this.mSwitchStats;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 330 */
/* new-instance v0, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
final String v1 = "screen_brightness_mode"; // const-string v1, "screen_brightness_mode"
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAutoBrightnessSettingsEnable:Z */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v0, v3, v1, v2}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V */
/* .line 332 */
/* .local v0, "statEvent":Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 334 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
final String v2 = "dc_back_light"; // const-string v2, "dc_back_light"
/* iget-boolean v4, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDcBacklightSettingsEnable:Z */
/* invoke-direct {v1, v3, v2, v4}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V */
/* move-object v0, v1 */
/* .line 336 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 338 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
/* const-string/jumbo v2, "ui_night_mode" */
/* iget-boolean v4, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDarkModeSettingsEnable:Z */
/* invoke-direct {v1, v3, v2, v4}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V */
/* move-object v0, v1 */
/* .line 340 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 342 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSunlightSettingsEnable:Z */
int v4 = 1; // const/4 v4, 0x1
if ( v2 != null) { // if-eqz v2, :cond_0
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAutoBrightnessSettingsEnable:Z */
/* if-nez v2, :cond_0 */
/* move v2, v4 */
} // :cond_0
/* move v2, v3 */
} // :goto_0
/* const-string/jumbo v5, "sunlight_mode" */
/* invoke-direct {v1, v3, v5, v2}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V */
/* move-object v0, v1 */
/* .line 345 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 347 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
final String v2 = "screen_optimize_mode"; // const-string v2, "screen_optimize_mode"
/* iget v5, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenOptimizeSettingsMode:I */
/* invoke-direct {v1, v4, v2, v5}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 349 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 351 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
/* const-string/jumbo v2, "screen_paper_mode_enabled" */
/* iget-boolean v5, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mReadModeSettingsEnable:Z */
/* invoke-direct {v1, v3, v2, v5}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V */
/* move-object v0, v1 */
/* .line 353 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 355 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
/* iget v2, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenColorLevel:I */
/* .line 356 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getColorLevelCode(I)I */
final String v5 = "screen_color_level"; // const-string v5, "screen_color_level"
/* invoke-direct {v1, v4, v5, v2}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 357 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 360 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
/* .line 361 */
v2 = /* invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getCurrentRefreshRate()I */
final String v5 = "dynamic_refresh_rate"; // const-string v5, "dynamic_refresh_rate"
/* invoke-direct {v1, v4, v5, v2}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 362 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 364 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
final String v2 = "doze_always_on"; // const-string v2, "doze_always_on"
/* iget-boolean v5, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mDozeAlwaysOn:Z */
/* invoke-direct {v1, v3, v2, v5}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V */
/* move-object v0, v1 */
/* .line 366 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 368 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
/* const-string/jumbo v2, "screen_true_tone" */
/* iget-boolean v5, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mScreenTrueToneEnable:Z */
/* invoke-direct {v1, v3, v2, v5}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V */
/* move-object v0, v1 */
/* .line 370 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 372 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
/* .line 373 */
v2 = /* invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getCurrentScreenResolution()I */
/* const-string/jumbo v5, "screen_resolution" */
/* invoke-direct {v1, v4, v5, v2}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 374 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 376 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
/* .line 377 */
v2 = /* invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getCurrentScreenCompat()Z */
final String v5 = "miui_screen_compat"; // const-string v5, "miui_screen_compat"
/* invoke-direct {v1, v3, v5, v2}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V */
/* move-object v0, v1 */
/* .line 378 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 380 */
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSupportAdaptiveSleep:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 381 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
final String v2 = "adaptive_sleep"; // const-string v2, "adaptive_sleep"
/* iget-boolean v5, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAdaptiveSleepEnable:Z */
/* invoke-direct {v1, v3, v2, v5}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V */
/* move-object v0, v1 */
/* .line 383 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 386 */
} // :cond_1
final String v1 = "persist.sys.muiltdisplay_type"; // const-string v1, "persist.sys.muiltdisplay_type"
v1 = android.os.SystemProperties .getInt ( v1,v3 );
int v2 = 2; // const/4 v2, 0x2
/* if-ne v1, v2, :cond_2 */
/* .line 388 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
/* .line 390 */
v2 = /* invoke-direct {p0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getCurrentCloseLidDisplaySetting()I */
final String v5 = "close_lid_display_setting"; // const-string v5, "close_lid_display_setting"
/* invoke-direct {v1, v4, v5, v2}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 391 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 395 */
} // :cond_2
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSupportExpertMode:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 396 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
final String v2 = "color_gamut_mode"; // const-string v2, "color_gamut_mode"
/* iget v5, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mColorGamutMode:I */
/* invoke-direct {v1, v4, v2, v5}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 398 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 401 */
} // :cond_3
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mSupportAiDisplayMode:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 402 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
final String v2 = "ai_display_mode"; // const-string v2, "ai_display_mode"
/* iget-boolean v4, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mAiDisplayModeEnable:Z */
/* invoke-direct {v1, v3, v2, v4}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V */
/* move-object v0, v1 */
/* .line 404 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 408 */
} // :cond_4
/* sget-boolean v1, Lcom/android/server/display/statistics/SwitchStatsHelper;->BACKGROUND_BLUR_SUPPORTED:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 409 */
/* new-instance v1, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry; */
final String v2 = "background_blur_enable"; // const-string v2, "background_blur_enable"
/* iget-boolean v4, p0, Lcom/android/server/display/statistics/SwitchStatsHelper;->mBackgroundBlurEnable:Z */
/* invoke-direct {v1, v3, v2, v4}, Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;-><init>(ILjava/lang/String;Z)V */
/* move-object v0, v1 */
/* .line 411 */
v1 = this.mSwitchStats;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 413 */
} // :cond_5
return;
} // .end method
