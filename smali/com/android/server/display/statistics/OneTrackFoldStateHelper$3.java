class com.android.server.display.statistics.OneTrackFoldStateHelper$3 extends miui.process.IForegroundWindowListener$Stub {
	 /* .source "OneTrackFoldStateHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/statistics/OneTrackFoldStateHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.statistics.OneTrackFoldStateHelper this$0; //synthetic
/* # direct methods */
 com.android.server.display.statistics.OneTrackFoldStateHelper$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/statistics/OneTrackFoldStateHelper; */
/* .line 230 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/process/IForegroundWindowListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundWindowChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 2 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .line 233 */
v0 = com.android.server.display.statistics.OneTrackFoldStateHelper .-$$Nest$sfgetDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 234 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "onForegroundWindowChanged: pkgName: "; // const-string v1, "onForegroundWindowChanged: pkgName: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.mForegroundPackageName;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "OneTrackFoldStateHelper"; // const-string v1, "OneTrackFoldStateHelper"
	 android.util.Slog .d ( v1,v0 );
	 /* .line 237 */
} // :cond_0
v0 = this.this$0;
com.android.server.display.statistics.OneTrackFoldStateHelper .-$$Nest$fgetmHandler ( v0 );
int v1 = 2; // const/4 v1, 0x2
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 238 */
v0 = this.this$0;
com.android.server.display.statistics.OneTrackFoldStateHelper .-$$Nest$fgetmHandler ( v0 );
(( android.os.Handler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 239 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.this$0;
com.android.server.display.statistics.OneTrackFoldStateHelper .-$$Nest$fgetmHandler ( v1 );
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 240 */
return;
} // .end method
