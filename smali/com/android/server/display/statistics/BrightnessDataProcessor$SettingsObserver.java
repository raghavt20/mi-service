class com.android.server.display.statistics.BrightnessDataProcessor$SettingsObserver extends android.database.ContentObserver {
	 /* .source "BrightnessDataProcessor.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/statistics/BrightnessDataProcessor; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "SettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.display.statistics.BrightnessDataProcessor this$0; //synthetic
/* # direct methods */
public com.android.server.display.statistics.BrightnessDataProcessor$SettingsObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 1487 */
this.this$0 = p1;
/* .line 1488 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 1489 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 1493 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 1494 */
	 return;
	 /* .line 1496 */
} // :cond_0
(( android.net.Uri ) p2 ).getLastPathSegment ( ); // invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
final String v2 = "disable_security_by_mishow"; // const-string v2, "disable_security_by_mishow"
final String v3 = "screen_brightness_mode"; // const-string v3, "screen_brightness_mode"
int v4 = 0; // const/4 v4, 0x0
int v5 = 1; // const/4 v5, 0x1
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v0, v5 */
/* :sswitch_1 */
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* move v0, v4 */
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 1512 */
/* :pswitch_0 */
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fgetmContentResolver ( v0 );
v1 = android.provider.Settings$System .getInt ( v1,v2,v4 );
/* if-ne v1, v5, :cond_2 */
/* move v4, v5 */
} // :cond_2
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fputmIsMiShow ( v0,v4 );
/* .line 1513 */
/* .line 1498 */
/* :pswitch_1 */
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fgetmContentResolver ( v0 );
int v2 = -2; // const/4 v2, -0x2
v1 = android.provider.Settings$System .getIntForUser ( v1,v3,v4,v2 );
/* if-ne v1, v5, :cond_3 */
/* move v4, v5 */
} // :cond_3
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fputmAutoBrightnessEnable ( v0,v4 );
/* .line 1502 */
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fputmAutoBrightnessModeChanged ( v0,v5 );
/* .line 1505 */
v0 = this.this$0;
v0 = com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fgetmReportBrightnessEventsEnable ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1506 */
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$mreportDisabledAutoBrightnessEvent ( v0 );
/* .line 1509 */
} // :cond_4
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$mupdateUserResetAutoBrightnessModeTimes ( v0 );
/* .line 1510 */
/* nop */
/* .line 1517 */
} // :goto_2
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x294f7102 -> :sswitch_1 */
/* 0x5da4f7f9 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
