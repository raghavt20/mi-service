.class public Lcom/android/server/display/statistics/AggregationEvent$BrightnessAggregationEvent;
.super Lcom/android/server/display/statistics/AggregationEvent;
.source "AggregationEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/statistics/AggregationEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BrightnessAggregationEvent"
.end annotation


# static fields
.field public static final EVENT_AUTO_ADJUST_TIMES:Ljava/lang/String; = "auto_adj_times"

.field public static final EVENT_AUTO_MANUAL_ADJUST_APP_RANKING:Ljava/lang/String; = "auto_manual_adj_app_ranking"

.field public static final EVENT_AUTO_MANUAL_ADJUST_AVG_NITS_LUX_SPAN:Ljava/lang/String; = "auto_manual_adj_avg_nits_lux_span"

.field public static final EVENT_AUTO_MANUAL_ADJUST_DISPLAY_MODE:Ljava/lang/String; = "auto_manual_adj_display_mode"

.field public static final EVENT_AUTO_MANUAL_ADJUST_HIGH_LUX_SPAN:Ljava/lang/String; = "auto_manual_adj_high_lux_span"

.field public static final EVENT_AUTO_MANUAL_ADJUST_LOW_LUX_SPAN:Ljava/lang/String; = "auto_manual_adj_low_lux_span"

.field public static final EVENT_AUTO_MANUAL_ADJUST_LUX_SPAN:Ljava/lang/String; = "auto_manual_adj_lux_span"

.field public static final EVENT_AUTO_MANUAL_ADJUST_TIMES:Ljava/lang/String; = "auto_manual_adj_times"

.field public static final EVENT_AVERAGE_BRIGHTNESS:Ljava/lang/String; = "average_brightness"

.field public static final EVENT_BRIGHTNESS_ADJUST_TIMES:Ljava/lang/String; = "brightness_adj_times"

.field public static final EVENT_BRIGHTNESS_USAGE:Ljava/lang/String; = "brightness_usage"

.field public static final EVENT_HBM_USAGE:Ljava/lang/String; = "hbm_usage"

.field public static final EVENT_HDR_USAGE:Ljava/lang/String; = "hdr_usage"

.field public static final EVENT_HDR_USAGE_APP_USAGE:Ljava/lang/String; = "hdr_usage_app_usage"

.field public static final EVENT_MANUAL_ADJUST_TIMES:Ljava/lang/String; = "manual_adj_times"

.field public static final EVENT_OVERRIDE_ADJUST_APP_RANKING:Ljava/lang/String; = "override_adj_app_ranking"

.field public static final EVENT_SUNLIGHT_ADJUST_TIMES:Ljava/lang/String; = "sunlight_adj_times"

.field public static final EVENT_SWITCH_STATS:Ljava/lang/String; = "switch_stats"

.field public static final EVENT_WINDOW_ADJUST_TIMES:Ljava/lang/String; = "window_adj_times"

.field public static final KEY_SWITCH_STATS_DETAILS:Ljava/lang/String; = "switch_stats_details"

.field public static final KEY_USAGE_VALUE:Ljava/lang/String; = "usage_value"


# instance fields
.field private mBrightnessQuotaEvents:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mCacheDataMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 23
    invoke-direct {p0}, Lcom/android/server/display/statistics/AggregationEvent;-><init>()V

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/statistics/AggregationEvent$BrightnessAggregationEvent;->mBrightnessQuotaEvents:Ljava/util/Map;

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/statistics/AggregationEvent$BrightnessAggregationEvent;->mCacheDataMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public getCacheDataMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/android/server/display/statistics/AggregationEvent$BrightnessAggregationEvent;->mCacheDataMap:Ljava/util/Map;

    return-object v0
.end method

.method public getQuotaEvents()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/android/server/display/statistics/AggregationEvent$BrightnessAggregationEvent;->mBrightnessQuotaEvents:Ljava/util/Map;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/android/server/display/statistics/AggregationEvent$BrightnessAggregationEvent;->mBrightnessQuotaEvents:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
