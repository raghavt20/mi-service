public class com.android.server.display.statistics.AggregationEvent$AdvancedAggregationEvent extends com.android.server.display.statistics.AggregationEvent {
	 /* .source "AggregationEvent.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/statistics/AggregationEvent; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "AdvancedAggregationEvent" */
} // .end annotation
/* # static fields */
public static final java.lang.String EVENT_INTERRUPT_ANIMATION_TIMES;
public static final java.lang.String EVENT_RESET_BRIGHTNESS_MODE_TIMES;
public static final java.lang.String KEY_INTERRUPT_TIMES;
public static final java.lang.String KEY_RESET_TIMES;
/* # instance fields */
private java.util.Map mAdvancedQuotaEvents;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.display.statistics.AggregationEvent$AdvancedAggregationEvent ( ) {
/* .locals 1 */
/* .line 83 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/AggregationEvent;-><init>()V */
/* .line 94 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mAdvancedQuotaEvents = v0;
return;
} // .end method
/* # virtual methods */
public java.util.Map getCacheDataMap ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Float;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 108 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.util.Map getQuotaEvents ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 103 */
v0 = this.mAdvancedQuotaEvents;
} // .end method
public java.lang.String toString ( ) {
/* .locals 1 */
/* .line 98 */
v0 = this.mAdvancedQuotaEvents;
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
