.class final Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler;
.super Landroid/os/Handler;
.source "OneTrackFoldStateHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/statistics/OneTrackFoldStateHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "OneTrackFoldStateHelperHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;


# direct methods
.method public constructor <init>(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 450
    iput-object p1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler;->this$0:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    .line 451
    const/4 p1, 0x0

    invoke-direct {p0, p2, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 452
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 456
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 479
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler;->this$0:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->-$$Nest$mhandleFocusedWindowChanged(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;Ljava/lang/String;)V

    .line 480
    goto :goto_0

    .line 476
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler;->this$0:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->-$$Nest$mhandleDisplaySwapFinished(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;J)V

    .line 477
    goto :goto_0

    .line 473
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler;->this$0:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->-$$Nest$mhandleDisplaySwapFinished(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;J)V

    .line 474
    goto :goto_0

    .line 470
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler;->this$0:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->-$$Nest$mhandleInteractiveChanged(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;Z)V

    .line 471
    goto :goto_0

    .line 467
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler;->this$0:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->-$$Nest$mdebounceDeviceState(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;J)V

    .line 468
    goto :goto_0

    .line 464
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler;->this$0:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->-$$Nest$mhandleDeviceStateChanged(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;IJ)V

    .line 465
    goto :goto_0

    .line 461
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler;->this$0:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lmiui/process/ForegroundInfo;

    invoke-static {v0, v1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->-$$Nest$mhandleWindowChanged(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;Lmiui/process/ForegroundInfo;)V

    .line 462
    goto :goto_0

    .line 458
    :pswitch_7
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler;->this$0:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->-$$Nest$mhandleFoldChanged(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;Z)V

    .line 459
    nop

    .line 484
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
