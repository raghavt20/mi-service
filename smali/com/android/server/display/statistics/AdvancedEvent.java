public class com.android.server.display.statistics.AdvancedEvent {
	 /* .source "AdvancedEvent.java" */
	 /* # static fields */
	 public static final Integer BRIGHTNESS_CHANGE_STATE_DECREASE;
	 public static final Integer BRIGHTNESS_CHANGE_STATE_EQUAL;
	 public static final Integer BRIGHTNESS_CHANGE_STATE_INCREASE;
	 public static final Integer BRIGHTNESS_CHANGE_STATE_RESET;
	 public static final Integer EVENT_AUTO_BRIGHTNESS_ANIMATION_INFO;
	 public static final Integer EVENT_SCHEDULE_ADVANCED_EVENT;
	 /* # instance fields */
	 private Float auto_brightness_animation_duration;
	 private Integer brightness_changed_state;
	 private Boolean brightness_restricted_enable;
	 private Integer current_animate_value;
	 private Float default_spline_error;
	 private java.lang.String extra;
	 private Integer interrupt_brightness_animation_times;
	 private Float long_term_model_spline_error;
	 private Integer target_animate_value;
	 private Long time_stamp;
	 private Integer type;
	 private Integer user_brightness;
	 private Integer user_reset_brightness_mode_times;
	 /* # direct methods */
	 public com.android.server.display.statistics.AdvancedEvent ( ) {
		 /* .locals 2 */
		 /* .line 30 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 19 */
		 int v0 = -1; // const/4 v0, -0x1
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->current_animate_value:I */
		 /* .line 20 */
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->target_animate_value:I */
		 /* .line 21 */
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->user_brightness:I */
		 /* .line 22 */
		 /* const/high16 v1, -0x40800000 # -1.0f */
		 /* iput v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->auto_brightness_animation_duration:F */
		 /* .line 23 */
		 /* iput v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->long_term_model_spline_error:F */
		 /* .line 24 */
		 /* iput v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->default_spline_error:F */
		 /* .line 25 */
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->brightness_changed_state:I */
		 /* .line 26 */
		 final String v0 = ""; // const-string v0, ""
		 this.extra = v0;
		 /* .line 31 */
		 return;
	 } // .end method
	 private com.android.server.display.statistics.AdvancedEvent ( ) {
		 /* .locals 2 */
		 /* .param p1, "in" # Landroid/os/Parcel; */
		 /* .line 33 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 19 */
		 int v0 = -1; // const/4 v0, -0x1
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->current_animate_value:I */
		 /* .line 20 */
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->target_animate_value:I */
		 /* .line 21 */
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->user_brightness:I */
		 /* .line 22 */
		 /* const/high16 v1, -0x40800000 # -1.0f */
		 /* iput v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->auto_brightness_animation_duration:F */
		 /* .line 23 */
		 /* iput v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->long_term_model_spline_error:F */
		 /* .line 24 */
		 /* iput v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->default_spline_error:F */
		 /* .line 25 */
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->brightness_changed_state:I */
		 /* .line 26 */
		 final String v0 = ""; // const-string v0, ""
		 this.extra = v0;
		 /* .line 34 */
		 v0 = 		 (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->type:I */
		 /* .line 35 */
		 v0 = 		 (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->interrupt_brightness_animation_times:I */
		 /* .line 36 */
		 v0 = 		 (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->user_reset_brightness_mode_times:I */
		 /* .line 37 */
		 v0 = 		 (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->current_animate_value:I */
		 /* .line 38 */
		 v0 = 		 (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->target_animate_value:I */
		 /* .line 39 */
		 v0 = 		 (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->user_brightness:I */
		 /* .line 40 */
		 v0 = 		 (( android.os.Parcel ) p1 ).readFloat ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->auto_brightness_animation_duration:F */
		 /* .line 41 */
		 v0 = 		 (( android.os.Parcel ) p1 ).readFloat ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->long_term_model_spline_error:F */
		 /* .line 42 */
		 v0 = 		 (( android.os.Parcel ) p1 ).readFloat ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->default_spline_error:F */
		 /* .line 43 */
		 v0 = 		 (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
		 /* iput v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->brightness_changed_state:I */
		 /* .line 44 */
		 (( android.os.Parcel ) p1 ).readString ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
		 this.extra = v0;
		 /* .line 45 */
		 (( android.os.Parcel ) p1 ).readLong ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J
		 /* move-result-wide v0 */
		 /* iput-wide v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->time_stamp:J */
		 /* .line 46 */
		 v0 = 		 (( android.os.Parcel ) p1 ).readBoolean ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z
		 /* iput-boolean v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->brightness_restricted_enable:Z */
		 /* .line 47 */
		 return;
	 } // .end method
	 private java.lang.String getBrightnessChangedState ( Integer p0 ) {
		 /* .locals 1 */
		 /* .param p1, "state" # I */
		 /* .line 187 */
		 /* packed-switch p1, :pswitch_data_0 */
		 /* .line 195 */
		 final String v0 = "brightness reset"; // const-string v0, "brightness reset"
		 /* .line 193 */
		 /* :pswitch_0 */
		 final String v0 = "brightness equal"; // const-string v0, "brightness equal"
		 /* .line 191 */
		 /* :pswitch_1 */
		 final String v0 = "brightness decrease"; // const-string v0, "brightness decrease"
		 /* .line 189 */
		 /* :pswitch_2 */
		 final String v0 = "brightness increase"; // const-string v0, "brightness increase"
		 /* nop */
		 /* :pswitch_data_0 */
		 /* .packed-switch 0x0 */
		 /* :pswitch_2 */
		 /* :pswitch_1 */
		 /* :pswitch_0 */
	 } // .end packed-switch
} // .end method
public static java.lang.String timestampToString ( Long p0 ) {
	 /* .locals 3 */
	 /* .param p0, "time" # J */
	 /* .line 167 */
	 /* new-instance v0, Ljava/util/Date; */
	 /* invoke-direct {v0, p0, p1}, Ljava/util/Date;-><init>(J)V */
	 /* .line 168 */
	 /* .local v0, "d":Ljava/util/Date; */
	 /* new-instance v1, Ljava/text/SimpleDateFormat; */
	 /* const-string/jumbo v2, "yyyy-MM-dd HH:mm:ss:sss" */
	 /* invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
	 /* .line 169 */
	 /* .local v1, "sf":Ljava/text/SimpleDateFormat; */
	 (( java.text.SimpleDateFormat ) v1 ).format ( v0 ); // invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
} // .end method
/* # virtual methods */
public java.lang.String convertToString ( ) {
	 /* .locals 3 */
	 /* .line 173 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v1, "type:" */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->type:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v1 = ", auto_brightness_animation_duration:"; // const-string v1, ", auto_brightness_animation_duration:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->auto_brightness_animation_duration:F */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
	 final String v1 = ", current_animate_value:"; // const-string v1, ", current_animate_value:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->current_animate_value:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v1 = ", target_animate_value:"; // const-string v1, ", target_animate_value:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->target_animate_value:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v1 = ", user_brightness:"; // const-string v1, ", user_brightness:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->user_brightness:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v1 = ", long_term_model_spline_error:"; // const-string v1, ", long_term_model_spline_error:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->long_term_model_spline_error:F */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
	 final String v1 = ", default_spline_error:"; // const-string v1, ", default_spline_error:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->default_spline_error:F */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
	 final String v1 = ", brightness_changed_state:"; // const-string v1, ", brightness_changed_state:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->brightness_changed_state:I */
	 /* .line 180 */
	 /* invoke-direct {p0, v1}, Lcom/android/server/display/statistics/AdvancedEvent;->getBrightnessChangedState(I)Ljava/lang/String; */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v1 = ", extra:"; // const-string v1, ", extra:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.extra;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v1 = ", time_stamp:"; // const-string v1, ", time_stamp:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget-wide v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->time_stamp:J */
	 /* .line 182 */
	 com.android.server.display.statistics.AdvancedEvent .timestampToString ( v1,v2 );
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v1 = ", brightness_restricted_enable:"; // const-string v1, ", brightness_restricted_enable:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget-boolean v1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->brightness_restricted_enable:Z */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 173 */
} // .end method
public Float getAutoBrightnessAnimationDuration ( ) {
	 /* .locals 1 */
	 /* .line 109 */
	 /* iget v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->auto_brightness_animation_duration:F */
} // .end method
public Integer getBrightnessChangedState ( ) {
	 /* .locals 1 */
	 /* .line 136 */
	 /* iget v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->brightness_changed_state:I */
} // .end method
public Boolean getBrightnessRestrictedEnable ( ) {
	 /* .locals 1 */
	 /* .line 163 */
	 /* iget-boolean v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->brightness_restricted_enable:Z */
} // .end method
public Integer getCurrentAnimateValue ( ) {
	 /* .locals 1 */
	 /* .line 82 */
	 /* iget v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->current_animate_value:I */
} // .end method
public Float getDefaultSplineError ( ) {
	 /* .locals 1 */
	 /* .line 127 */
	 /* iget v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->default_spline_error:F */
} // .end method
public Integer getEventType ( ) {
	 /* .locals 1 */
	 /* .line 55 */
	 /* iget v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->type:I */
} // .end method
public java.lang.String getExtra ( ) {
	 /* .locals 1 */
	 /* .line 145 */
	 v0 = this.extra;
} // .end method
public Integer getInterruptBrightnessAnimationTimes ( ) {
	 /* .locals 1 */
	 /* .line 64 */
	 /* iget v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->interrupt_brightness_animation_times:I */
} // .end method
public Float getLongTermModelSplineError ( ) {
	 /* .locals 1 */
	 /* .line 118 */
	 /* iget v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->long_term_model_spline_error:F */
} // .end method
public Integer getTargetAnimateValue ( ) {
	 /* .locals 1 */
	 /* .line 91 */
	 /* iget v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->target_animate_value:I */
} // .end method
public Long getTimeStamp ( ) {
	 /* .locals 2 */
	 /* .line 154 */
	 /* iget-wide v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->time_stamp:J */
	 /* return-wide v0 */
} // .end method
public Integer getUserBrightness ( ) {
	 /* .locals 1 */
	 /* .line 100 */
	 /* iget v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->user_brightness:I */
} // .end method
public Integer getUserResetBrightnessModeTimes ( ) {
	 /* .locals 1 */
	 /* .line 73 */
	 /* iget v0, p0, Lcom/android/server/display/statistics/AdvancedEvent;->user_reset_brightness_mode_times:I */
} // .end method
public com.android.server.display.statistics.AdvancedEvent setAutoBrightnessAnimationDuration ( Float p0 ) {
	 /* .locals 0 */
	 /* .param p1, "duration" # F */
	 /* .line 104 */
	 /* iput p1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->auto_brightness_animation_duration:F */
	 /* .line 105 */
} // .end method
public com.android.server.display.statistics.AdvancedEvent setBrightnessChangedState ( Integer p0 ) {
	 /* .locals 0 */
	 /* .param p1, "state" # I */
	 /* .line 131 */
	 /* iput p1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->brightness_changed_state:I */
	 /* .line 132 */
} // .end method
public com.android.server.display.statistics.AdvancedEvent setBrightnessRestrictedEnable ( Boolean p0 ) {
	 /* .locals 0 */
	 /* .param p1, "enable" # Z */
	 /* .line 158 */
	 /* iput-boolean p1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->brightness_restricted_enable:Z */
	 /* .line 159 */
} // .end method
public com.android.server.display.statistics.AdvancedEvent setCurrentAnimateValue ( Integer p0 ) {
	 /* .locals 0 */
	 /* .param p1, "value" # I */
	 /* .line 77 */
	 /* iput p1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->current_animate_value:I */
	 /* .line 78 */
} // .end method
public com.android.server.display.statistics.AdvancedEvent setDefaultSplineError ( Float p0 ) {
	 /* .locals 0 */
	 /* .param p1, "error" # F */
	 /* .line 122 */
	 /* iput p1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->default_spline_error:F */
	 /* .line 123 */
} // .end method
public com.android.server.display.statistics.AdvancedEvent setEventType ( Integer p0 ) {
	 /* .locals 0 */
	 /* .param p1, "type" # I */
	 /* .line 50 */
	 /* iput p1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->type:I */
	 /* .line 51 */
} // .end method
public com.android.server.display.statistics.AdvancedEvent setExtra ( java.lang.String p0 ) {
	 /* .locals 0 */
	 /* .param p1, "extra" # Ljava/lang/String; */
	 /* .line 140 */
	 this.extra = p1;
	 /* .line 141 */
} // .end method
public com.android.server.display.statistics.AdvancedEvent setInterruptBrightnessAnimationTimes ( Integer p0 ) {
	 /* .locals 0 */
	 /* .param p1, "times" # I */
	 /* .line 59 */
	 /* iput p1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->interrupt_brightness_animation_times:I */
	 /* .line 60 */
} // .end method
public com.android.server.display.statistics.AdvancedEvent setLongTermModelSplineError ( Float p0 ) {
	 /* .locals 0 */
	 /* .param p1, "error" # F */
	 /* .line 113 */
	 /* iput p1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->long_term_model_spline_error:F */
	 /* .line 114 */
} // .end method
public com.android.server.display.statistics.AdvancedEvent setTargetAnimateValue ( Integer p0 ) {
	 /* .locals 0 */
	 /* .param p1, "value" # I */
	 /* .line 86 */
	 /* iput p1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->target_animate_value:I */
	 /* .line 87 */
} // .end method
public com.android.server.display.statistics.AdvancedEvent setTimeStamp ( Long p0 ) {
	 /* .locals 0 */
	 /* .param p1, "timeStamp" # J */
	 /* .line 149 */
	 /* iput-wide p1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->time_stamp:J */
	 /* .line 150 */
} // .end method
public com.android.server.display.statistics.AdvancedEvent setUserBrightness ( Integer p0 ) {
	 /* .locals 0 */
	 /* .param p1, "value" # I */
	 /* .line 95 */
	 /* iput p1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->user_brightness:I */
	 /* .line 96 */
} // .end method
public com.android.server.display.statistics.AdvancedEvent setUserResetBrightnessModeTimes ( Integer p0 ) {
	 /* .locals 0 */
	 /* .param p1, "times" # I */
	 /* .line 68 */
	 /* iput p1, p0, Lcom/android/server/display/statistics/AdvancedEvent;->user_reset_brightness_mode_times:I */
	 /* .line 69 */
} // .end method
