.class public Lcom/android/server/display/statistics/OneTrackFoldStateHelper;
.super Ljava/lang/Object;
.source "OneTrackFoldStateHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler;
    }
.end annotation


# static fields
.field private static final APP_FOLDED_UNFOLDED_AVG_TIME:Ljava/lang/String; = "app_folded_unfolded_avg_time"

.field private static final APP_FOLD_SCREEN_AVG_TIME:I = 0x1

.field private static final APP_ID:Ljava/lang/String; = "31000401594"

.field private static final APP_UNFOLD_SCREEN_AVG_TIME:I = 0x0

.field private static final APP_USE_SCREEN_TIME:Ljava/lang/String; = "app_use_screen_time"

.field private static final DEBUG:Z

.field private static final DEBUG_REPORT_TIME_DURATION:J = 0x1d4c0L

.field private static final DEVICE_REGION:Ljava/lang/String;

.field private static final DEVICE_STATE_CLOSE:I = 0x0

.field private static final DEVICE_STATE_DEBOUNCE_TIME:J = 0x3e8L

.field private static final DEVICE_STATE_HALF_OPEN:I = 0x2

.field private static final DEVICE_STATE_INVALID:I = -0x1

.field private static final DEVICE_STATE_OPEN:I = 0x3

.field private static final DEVICE_STATE_OPEN_PRESENTATION:I = 0x5

.field private static final DEVICE_STATE_OPEN_REVERSE:I = 0x4

.field private static final DEVICE_STATE_OPEN_REVERSE_PRESENTATION:I = 0x6

.field private static final DEVICE_STATE_TENT:I = 0x1

.field private static final DEVICE_STATE_TIME:Ljava/lang/String; = "device_state_time"

.field private static final EVENT_NAME:Ljava/lang/String; = "fold_statistic"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN:I = 0x1

.field private static final FOLDED_AVG_TIME:Ljava/lang/String; = "folded_avg_time"

.field private static final FOLD_TIMES:Ljava/lang/String; = "fold_times"

.field private static final IS_FOLDABLE_DEVICE:Z

.field private static final IS_INTERNATIONAL_BUILD:Z

.field private static final MSG_DEVICE_STATE_CHANGED:I = 0x3

.field private static final MSG_DEVICE_STATE_DEBOUNCE:I = 0x4

.field private static final MSG_DISPLAY_SWAP_FINISHED:I = 0x6

.field private static final MSG_INTERACTIVE_CHANGED:I = 0x5

.field private static final MSG_ON_FOCUS_PACKAGE_CHANGED:I = 0x8

.field private static final MSG_ON_FOLD_CHANGED:I = 0x1

.field private static final MSG_ON_FOREGROUND_APP_CHANGED:I = 0x2

.field private static final MSG_RESET_PENDING_DISPLAY_SWAP:I = 0x7

.field private static final ONE_SECOND:F = 1000.0f

.field private static final ONE_TRACK_ACTION:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final ONE_TRACK_PACKAGE_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static final PACKAGE:Ljava/lang/String; = "android"

.field private static final RECORD_REASON_DEVICE_STATE:I = 0x0

.field private static final RECORD_REASON_FOREGROUND_APP:I = 0x2

.field private static final RECORD_REASON_INTERACTIVE:I = 0x1

.field private static final RECORD_REASON_REPORT_DATA:I = 0x3

.field private static final SCREEN_TYPE_INVALID:I = -0x1

.field private static final SCREEN_TYPE_LARGE:I = 0x0

.field private static final SCREEN_TYPE_SMALL:I = 0x1

.field private static final TAG:Ljava/lang/String; = "OneTrackFoldStateHelper"

.field private static final TIMEOUT_PENDING_DISPLAY_SWAP_MILLIS:J = 0x1388L

.field private static final UNFOLDED_AVG_TIME:Ljava/lang/String; = "unfolded_avg_time"

.field private static volatile sInstance:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;


# instance fields
.field private mActivityManager:Landroid/app/ActivityManager;

.field private mAlarmManager:Landroid/app/AlarmManager;

.field private final mAppFoldAvgTimeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private mApplicationContext:Landroid/content/Context;

.field private mCurrentDeviceState:I

.field private final mDeviceStateUsageDetail:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private mFocusPackageName:Ljava/lang/String;

.field private mFoldCountSum:I

.field private mFoldTimeSum:F

.field private mFoldTimes:I

.field private mFolded:Z

.field private final mFoldedStateList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mForegroundAppName:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mInteractive:Z

.field private mLastEventReason:I

.field private mLastEventTime:J

.field private mLastFocusPackageName:Ljava/lang/String;

.field private mLastFoldChangeTime:J

.field private final mOnAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

.field private mPendingDeviceState:I

.field private mPendingDeviceStateDebounceTime:J

.field private mPendingWaitForDisplaySwapFinished:Z

.field private mPreInteractive:Z

.field private mPreviousDeviceState:I

.field private mUnfoldCountSum:I

.field private mUnfoldTimeSum:F

.field private final mUnfoldedStateList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mWindowListener:Lmiui/process/IForegroundWindowListener;


# direct methods
.method public static synthetic $r8$lambda$8rCWTTbE3NSM0NONciFj9mdfhhc(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->lambda$new$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$IyyHt-6k500ctpciPAlYHlhrU_8(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->lambda$new$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mdebounceDeviceState(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->debounceDeviceState(J)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleDeviceStateChanged(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;IJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->handleDeviceStateChanged(IJ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleDisplaySwapFinished(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->handleDisplaySwapFinished(J)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleFocusedWindowChanged(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->handleFocusedWindowChanged(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleFoldChanged(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->handleFoldChanged(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleInteractiveChanged(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->handleInteractiveChanged(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleWindowChanged(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;Lmiui/process/ForegroundInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->handleWindowChanged(Lmiui/process/ForegroundInfo;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetDEBUG()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 37
    nop

    .line 38
    const-string v0, "debug.miui.power.fold.dgb"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z

    .line 58
    nop

    .line 59
    const-string v0, "persist.sys.muiltdisplay_type"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v1, v2

    :cond_1
    sput-boolean v1, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->IS_FOLDABLE_DEVICE:Z

    .line 85
    nop

    .line 86
    const-string v0, "ro.product.mod_device"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "_global"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->IS_INTERNATIONAL_BUILD:Z

    .line 88
    const-string v0, "ro.miui.region"

    const-string v1, "CN"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEVICE_REGION:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mDeviceStateUsageDetail:Ljava/util/Map;

    .line 122
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mAppFoldAvgTimeMap:Ljava/util/Map;

    .line 124
    new-instance v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$1;

    invoke-direct {v0, p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$1;-><init>(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;)V

    iput-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldedStateList:Ljava/util/List;

    .line 133
    new-instance v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$2;

    invoke-direct {v0, p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$2;-><init>(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;)V

    iput-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldedStateList:Ljava/util/List;

    .line 141
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimes:I

    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mInteractive:Z

    .line 152
    iput-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPreInteractive:Z

    .line 153
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I

    .line 154
    iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPreviousDeviceState:I

    .line 155
    iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceState:I

    .line 158
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceStateDebounceTime:J

    .line 210
    new-instance v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;)V

    iput-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mOnAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    .line 230
    new-instance v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$3;

    invoke-direct {v0, p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$3;-><init>(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;)V

    iput-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mWindowListener:Lmiui/process/IForegroundWindowListener;

    .line 168
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getApplication()Landroid/app/Application;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mApplicationContext:Landroid/content/Context;

    .line 169
    new-instance v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler;

    invoke-static {}, Lcom/android/server/MiuiBgThread;->get()Lcom/android/server/MiuiBgThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler;-><init>(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    .line 170
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mApplicationContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mActivityManager:Landroid/app/ActivityManager;

    .line 172
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mApplicationContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mAlarmManager:Landroid/app/AlarmManager;

    .line 174
    sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->IS_FOLDABLE_DEVICE:Z

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 180
    :cond_0
    return-void
.end method

.method private addAppFoldInfoToIntent(Landroid/content/Intent;)V
    .locals 13
    .param p1, "intent"    # Landroid/content/Intent;

    .line 352
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mAppFoldAvgTimeMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 354
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 355
    .local v0, "appFoldAvgTimeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Float;>;>;"
    iget-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mAppFoldAvgTimeMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 356
    .local v2, "appName":Ljava/lang/String;
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 358
    .local v3, "tempMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Float;>;"
    iget-object v4, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mAppFoldAvgTimeMap:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 359
    .local v5, "foldEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/Long;>;>;"
    const/4 v6, 0x0

    .line 361
    .local v6, "totalTime":F
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 363
    .local v7, "foldType":Ljava/lang/Integer;
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    .line 365
    .local v8, "timeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 366
    .local v10, "time":J
    long-to-float v12, v10

    add-float/2addr v6, v12

    .line 367
    .end local v10    # "time":J
    goto :goto_2

    .line 368
    :cond_0
    const/4 v9, 0x0

    .line 370
    .local v9, "avgTime":F
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v10

    if-eqz v10, :cond_1

    .line 371
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v10

    int-to-float v10, v10

    div-float v9, v6, v10

    .line 373
    :cond_1
    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v3, v7, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    .end local v5    # "foldEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/Long;>;>;"
    .end local v6    # "totalTime":F
    .end local v7    # "foldType":Ljava/lang/Integer;
    .end local v8    # "timeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v9    # "avgTime":F
    goto :goto_1

    .line 375
    :cond_2
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    .end local v2    # "appName":Ljava/lang/String;
    .end local v3    # "tempMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Float;>;"
    goto :goto_0

    .line 378
    :cond_3
    const-string v1, "app_folded_unfolded_avg_time"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 380
    .end local v0    # "appFoldAvgTimeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Float;>;>;"
    :cond_4
    iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimeSum:F

    const/4 v1, 0x0

    cmpl-float v2, v0, v1

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldCountSum:I

    if-eqz v2, :cond_5

    .line 381
    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 383
    .local v0, "foldAvgTime":F
    const-string v2, "folded_avg_time"

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 385
    .end local v0    # "foldAvgTime":F
    :cond_5
    iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldTimeSum:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_6

    iget v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldCountSum:I

    if-eqz v1, :cond_6

    .line 386
    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 388
    .local v0, "unfoldAvgTime":F
    const-string/jumbo v1, "unfolded_avg_time"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 391
    .end local v0    # "unfoldAvgTime":F
    :cond_6
    const-string v0, "fold_times"

    iget v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimes:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 392
    return-void
.end method

.method private addAppUsageTimeInScreenToIntent(Landroid/content/Intent;)V
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;

    .line 399
    const/4 v0, -0x1

    .line 400
    .local v0, "stateType":I
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 401
    .local v1, "deviceStateTimeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Float;>;>;"
    iget-object v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mDeviceStateUsageDetail:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 403
    .local v3, "deviceState":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldedStateList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 404
    const/4 v0, 0x1

    goto :goto_1

    .line 405
    :cond_0
    iget-object v4, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldedStateList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 406
    const/4 v0, 0x0

    .line 410
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mDeviceStateUsageDetail:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 411
    .local v5, "appUsageEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 412
    .local v6, "appName":Ljava/lang/String;
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 413
    .local v7, "appUseTime":J
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 414
    invoke-interface {v1, v6, v9}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map;

    .line 415
    .local v9, "appUseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Float;>;"
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    .line 416
    .local v10, "duration":F
    long-to-float v11, v7

    add-float/2addr v11, v10

    const/high16 v12, 0x447a0000    # 1000.0f

    div-float/2addr v11, v12

    .line 417
    .local v11, "newDuration":F
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-interface {v9, v12, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    invoke-interface {v1, v6, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 419
    .end local v5    # "appUsageEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v6    # "appName":Ljava/lang/String;
    .end local v7    # "appUseTime":J
    .end local v9    # "appUseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Float;>;"
    .end local v10    # "duration":F
    .end local v11    # "newDuration":F
    goto :goto_2

    .line 420
    .end local v3    # "deviceState":Ljava/lang/Integer;
    :cond_2
    goto :goto_0

    .line 421
    :cond_3
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 422
    const-string v2, "app_use_screen_time"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 424
    :cond_4
    return-void
.end method

.method private addDeviceStateUsageTimeToIntent(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .line 331
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 332
    .local v0, "deviceStateUsageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Float;>;"
    iget-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mDeviceStateUsageDetail:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 334
    .local v2, "deviceState":Ljava/lang/Integer;
    const-wide/16 v3, 0x0

    .line 335
    .local v3, "stateSumTime":J
    iget-object v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mDeviceStateUsageDetail:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 336
    .local v6, "usageTime":J
    add-long/2addr v3, v6

    .line 337
    .end local v6    # "usageTime":J
    goto :goto_1

    .line 338
    :cond_0
    long-to-float v5, v3

    const/high16 v6, 0x447a0000    # 1000.0f

    div-float/2addr v5, v6

    .line 339
    .local v5, "deviceStateTime":F
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v0, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    .end local v2    # "deviceState":Ljava/lang/Integer;
    .end local v3    # "stateSumTime":J
    .end local v5    # "deviceStateTime":F
    goto :goto_0

    .line 341
    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 342
    const-string v1, "device_state_time"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    :cond_2
    return-void
.end method

.method private debounceDeviceState(J)V
    .locals 6
    .param p1, "eventTime"    # J

    .line 580
    iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceState:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-wide v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceStateDebounceTime:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_2

    .line 581
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 583
    .local v2, "now":J
    iget-wide v4, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceStateDebounceTime:J

    cmp-long v0, v4, v2

    if-gtz v0, :cond_1

    .line 584
    iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I

    iget v4, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceState:I

    if-eq v0, v4, :cond_0

    .line 586
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->recordDeviceStateDetails(I)V

    .line 587
    iput-wide v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastEventTime:J

    .line 588
    iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I

    iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPreviousDeviceState:I

    .line 589
    iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceState:I

    iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I

    .line 591
    :cond_0
    iput v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceState:I

    .line 592
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceStateDebounceTime:J

    goto :goto_0

    .line 595
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 596
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 597
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    iget-wide v4, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceStateDebounceTime:J

    invoke-virtual {v1, v0, v4, v5}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    .line 600
    .end local v0    # "msg":Landroid/os/Message;
    .end local v2    # "now":J
    :cond_2
    :goto_0
    return-void
.end method

.method public static getInstance()Lcom/android/server/display/statistics/OneTrackFoldStateHelper;
    .locals 2

    .line 183
    sget-object v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->sInstance:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    if-nez v0, :cond_1

    .line 184
    const-class v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    monitor-enter v0

    .line 185
    :try_start_0
    sget-object v1, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->sInstance:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    if-nez v1, :cond_0

    .line 186
    new-instance v1, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    invoke-direct {v1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;-><init>()V

    sput-object v1, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->sInstance:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    .line 188
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 190
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->sInstance:Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    return-object v0
.end method

.method private getTopAppName()Ljava/lang/String;
    .locals 6

    .line 432
    const-string v0, "getTopAppName: exception:"

    const-string v1, "OneTrackFoldStateHelper"

    const-string v2, ""

    :try_start_0
    iget-object v3, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mActivityManager:Landroid/app/ActivityManager;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mActivityManager:Landroid/app/ActivityManager;

    .line 433
    invoke-virtual {v3, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 434
    iget-object v3, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mActivityManager:Landroid/app/ActivityManager;

    invoke-virtual {v3, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 435
    .local v3, "cn":Landroid/content/ComponentName;
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 436
    .local v0, "packageName":Ljava/lang/String;
    return-object v0

    .line 438
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v3    # "cn":Landroid/content/ComponentName;
    :cond_0
    return-object v2

    .line 443
    :catch_0
    move-exception v3

    .line 444
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    return-object v2

    .line 440
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 441
    .local v3, "e":Ljava/lang/IndexOutOfBoundsException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/IndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    return-object v2
.end method

.method private handleDeviceStateChanged(IJ)V
    .locals 4
    .param p1, "toState"    # I
    .param p2, "eventTime"    # J

    .line 566
    sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 567
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleDeviceStateChanged: toState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OneTrackFoldStateHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    :cond_0
    iput p1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceState:I

    .line 570
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 571
    .local v0, "now":J
    const-wide/16 v2, 0x3e8

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceStateDebounceTime:J

    .line 572
    invoke-direct {p0, p2, p3}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->debounceDeviceState(J)V

    .line 573
    return-void
.end method

.method private handleDisplaySwapFinished(J)V
    .locals 7
    .param p1, "time"    # J

    .line 510
    iget-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingWaitForDisplaySwapFinished:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastFocusPackageName:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 511
    iget-wide v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastFoldChangeTime:J

    sub-long v0, p1, v0

    .line 512
    .local v0, "duration":J
    sget-boolean v2, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 513
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleDisplaySwapFinished: duration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OneTrackFoldStateHelper"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    :cond_0
    iget-boolean v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFolded:Z

    xor-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 517
    .local v2, "foldType":Ljava/lang/Integer;
    iget-object v3, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mAppFoldAvgTimeMap:Ljava/util/Map;

    iget-object v4, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastFocusPackageName:Ljava/lang/String;

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 518
    invoke-interface {v3, v4, v5}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 519
    .local v3, "appFoldTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/Long;>;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, v2, v4}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 521
    .local v4, "appTimeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 522
    invoke-virtual {v3, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 524
    iget-boolean v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFolded:Z

    if-eqz v5, :cond_1

    .line 525
    iget v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimeSum:F

    long-to-float v6, v0

    add-float/2addr v5, v6

    iput v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimeSum:F

    .line 526
    iget v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldCountSum:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldCountSum:I

    goto :goto_0

    .line 528
    :cond_1
    iget v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldTimeSum:F

    long-to-float v6, v0

    add-float/2addr v5, v6

    iput v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldTimeSum:F

    .line 529
    iget v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldCountSum:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldCountSum:I

    .line 531
    :goto_0
    iget-object v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mAppFoldAvgTimeMap:Ljava/util/Map;

    iget-object v6, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastFocusPackageName:Ljava/lang/String;

    invoke-interface {v5, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingWaitForDisplaySwapFinished:Z

    .line 535
    .end local v0    # "duration":J
    .end local v2    # "foldType":Ljava/lang/Integer;
    .end local v3    # "appFoldTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/Long;>;>;"
    .end local v4    # "appTimeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :cond_2
    return-void
.end method

.method private handleDisplaySwapStarted()V
    .locals 4

    .line 491
    sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 492
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleDisplaySwapStarted: mInteractive: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mInteractive:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OneTrackFoldStateHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mInteractive:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPreInteractive:Z

    if-eqz v0, :cond_1

    .line 495
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastFoldChangeTime:J

    .line 496
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFocusPackageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastFocusPackageName:Ljava/lang/String;

    .line 497
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingWaitForDisplaySwapFinished:Z

    .line 498
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 499
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 500
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 502
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mInteractive:Z

    iput-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPreInteractive:Z

    .line 503
    return-void
.end method

.method private handleFocusedWindowChanged(Ljava/lang/String;)V
    .locals 2
    .param p1, "focusPackageName"    # Ljava/lang/String;

    .line 542
    sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 543
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleFocusedWindowChanged: focusPackageName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OneTrackFoldStateHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFocusPackageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 546
    iput-object p1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFocusPackageName:Ljava/lang/String;

    .line 548
    :cond_1
    return-void
.end method

.method private handleFoldChanged(Z)V
    .locals 3
    .param p1, "folded"    # Z

    .line 257
    iget-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFolded:Z

    const-string v1, "OneTrackFoldStateHelper"

    if-ne v0, p1, :cond_1

    .line 258
    sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleFoldChanged: no change mFolded: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFolded:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    :cond_0
    return-void

    .line 263
    :cond_1
    iput-boolean p1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFolded:Z

    .line 264
    invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->handleDisplaySwapStarted()V

    .line 266
    iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimes:I

    .line 267
    sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 268
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleFoldChanged: mFoldTimes: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimes:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    :cond_2
    return-void
.end method

.method private handleInteractiveChanged(Z)V
    .locals 3
    .param p1, "interactive"    # Z

    .line 607
    sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 608
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleInteractiveChanged: interactive: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OneTrackFoldStateHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 610
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mInteractive:Z

    if-eq v0, p1, :cond_3

    .line 611
    iput-boolean p1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mInteractive:Z

    .line 612
    const/4 v0, 0x1

    if-eqz p1, :cond_1

    .line 613
    invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->getTopAppName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mForegroundAppName:Ljava/lang/String;

    .line 615
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-direct {p0, v1, v2, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->updateLastEventInfo(JI)V

    goto :goto_0

    .line 618
    :cond_1
    iget-boolean v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingWaitForDisplaySwapFinished:Z

    if-eqz v1, :cond_2

    .line 619
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingWaitForDisplaySwapFinished:Z

    .line 620
    iget-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 623
    :cond_2
    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->recordDeviceStateDetails(I)V

    .line 626
    :cond_3
    :goto_0
    return-void
.end method

.method private handleWindowChanged(Lmiui/process/ForegroundInfo;)V
    .locals 4
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;

    .line 273
    iget-object v0, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    .line 274
    .local v0, "packageName":Ljava/lang/String;
    sget-boolean v1, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 275
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleWindowChanged: pkgName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mForegroundAppName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mForegroundAppName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "OneTrackFoldStateHelper"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mForegroundAppName:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 279
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->recordDeviceStateDetails(I)V

    .line 280
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-direct {p0, v2, v3, v1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->updateLastEventInfo(JI)V

    .line 281
    iput-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mForegroundAppName:Ljava/lang/String;

    .line 283
    :cond_1
    return-void
.end method

.method private synthetic lambda$new$0()V
    .locals 0

    .line 176
    invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->registerForegroundWindowListener()V

    .line 177
    invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->setReportScheduleEventAlarm()V

    .line 178
    return-void
.end method

.method private synthetic lambda$new$1()V
    .locals 0

    .line 211
    invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->reportScheduleEvents()V

    .line 212
    invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->setReportScheduleEventAlarm()V

    .line 213
    return-void
.end method

.method private recordDeviceStateDetails(I)V
    .locals 9
    .param p1, "reason"    # I

    .line 641
    iget-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mInteractive:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 644
    :cond_1
    return-void

    .line 646
    :cond_2
    sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z

    if-eqz v0, :cond_3

    .line 647
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "recordDeviceStateDetails: start tracking device state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reason: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OneTrackFoldStateHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 650
    :cond_3
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mDeviceStateUsageDetail:Ljava/util/Map;

    iget v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I

    .line 651
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 652
    .local v0, "deviceStateMaps":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    iget-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mForegroundAppName:Ljava/lang/String;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 653
    .local v1, "duration":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    .line 654
    .local v3, "now":J
    iget-wide v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastEventTime:J

    sub-long v5, v3, v5

    add-long/2addr v5, v1

    .line 655
    .local v5, "newDuration":J
    iget-object v7, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mForegroundAppName:Ljava/lang/String;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 656
    iget-object v7, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mDeviceStateUsageDetail:Ljava/util/Map;

    iget v8, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 657
    return-void
.end method

.method private registerForegroundWindowListener()V
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mWindowListener:Lmiui/process/IForegroundWindowListener;

    invoke-static {v0}, Lmiui/process/ProcessManager;->registerForegroundWindowListener(Lmiui/process/IForegroundWindowListener;)V

    .line 254
    return-void
.end method

.method private reportFoldInfoToOneTrack()V
    .locals 4

    .line 290
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onetrack.action.TRACK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 291
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 292
    const-string v2, "APP_ID"

    const-string v3, "31000401594"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 293
    const-string v2, "PACKAGE"

    const-string v3, "android"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 294
    const-string v2, "EVENT_NAME"

    const-string v3, "fold_statistic"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 296
    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->addDeviceStateUsageTimeToIntent(Landroid/content/Intent;)V

    .line 298
    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->addAppFoldInfoToIntent(Landroid/content/Intent;)V

    .line 300
    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->addAppUsageTimeInScreenToIntent(Landroid/content/Intent;)V

    .line 302
    sget-boolean v1, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->IS_INTERNATIONAL_BUILD:Z

    if-nez v1, :cond_0

    .line 303
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 305
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reportOneTrack: data: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "OneTrackFoldStateHelper"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    :try_start_0
    iget-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mApplicationContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v3}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    goto :goto_0

    .line 308
    :catch_0
    move-exception v1

    .line 309
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "reportOneTrack Failed to upload!"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private reportScheduleEvents()V
    .locals 3

    .line 216
    sget-object v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEVICE_REGION:Ljava/lang/String;

    const-string v1, "IN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    return-void

    .line 219
    :cond_0
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->recordDeviceStateDetails(I)V

    .line 220
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-direct {p0, v1, v2, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->updateLastEventInfo(JI)V

    .line 222
    invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->reportFoldInfoToOneTrack()V

    .line 224
    invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->resetFoldInfoAfterReported()V

    .line 225
    return-void
.end method

.method private resetFoldInfoAfterReported()V
    .locals 2

    .line 317
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimes:I

    .line 318
    iget-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mDeviceStateUsageDetail:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 319
    iget-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mAppFoldAvgTimeMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 320
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimeSum:F

    .line 321
    iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldCountSum:I

    .line 322
    iput v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldTimeSum:F

    .line 323
    iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldCountSum:I

    .line 324
    return-void
.end method

.method private setReportScheduleEventAlarm()V
    .locals 14

    .line 197
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 198
    .local v0, "now":J
    sget-boolean v2, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z

    if-eqz v2, :cond_0

    const-wide/32 v3, 0x1d4c0

    goto :goto_0

    :cond_0
    const-wide/32 v3, 0x5265c00

    .line 199
    .local v3, "duration":J
    :goto_0
    add-long v12, v0, v3

    .line 200
    .local v12, "nextTime":J
    if-eqz v2, :cond_1

    .line 201
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setReportSwitchStatAlarm: next time: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 202
    invoke-static {v3, v4}, Landroid/util/TimeUtils;->formatDuration(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 201
    const-string v5, "OneTrackFoldStateHelper"

    invoke-static {v5, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    :cond_1
    iget-object v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mAlarmManager:Landroid/app/AlarmManager;

    if-eqz v5, :cond_2

    .line 205
    const/4 v6, 0x2

    const-string v9, "OneTrackFoldStateHelper"

    iget-object v10, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mOnAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    iget-object v11, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    move-wide v7, v12

    invoke-virtual/range {v5 .. v11}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V

    .line 208
    :cond_2
    return-void
.end method

.method private updateLastEventInfo(JI)V
    .locals 0
    .param p1, "eventTime"    # J
    .param p3, "reason"    # I

    .line 629
    iput-wide p1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastEventTime:J

    .line 630
    iput p3, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastEventReason:I

    .line 631
    return-void
.end method


# virtual methods
.method public notifyDeviceStateChanged(I)V
    .locals 4
    .param p1, "newState"    # I

    .line 660
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 661
    .local v0, "now":J
    iget-object v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 662
    iget-object v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 663
    .local v2, "msg":Landroid/os/Message;
    iput p1, v2, Landroid/os/Message;->arg1:I

    .line 664
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 665
    iget-object v3, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 666
    return-void
.end method

.method public notifyDisplaySwapFinished()V
    .locals 5

    .line 669
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 670
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 671
    .local v2, "now":J
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 672
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 673
    return-void
.end method

.method public notifyFocusedWindowChanged(Ljava/lang/String;)V
    .locals 2
    .param p1, "focusedPackageName"    # Ljava/lang/String;

    .line 676
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 677
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 678
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 679
    iget-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 680
    return-void
.end method

.method public onEarlyInteractivityChange(Z)V
    .locals 3
    .param p1, "interactive"    # Z

    .line 555
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 556
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 557
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 558
    return-void
.end method

.method public oneTrackFoldState(Z)V
    .locals 3
    .param p1, "folded"    # Z

    .line 244
    sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "oneTrackFoldState: folded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OneTrackFoldStateHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 248
    iget-object v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 249
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 250
    return-void
.end method
