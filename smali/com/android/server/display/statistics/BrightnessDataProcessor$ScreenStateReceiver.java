class com.android.server.display.statistics.BrightnessDataProcessor$ScreenStateReceiver extends android.content.BroadcastReceiver {
	 /* .source "BrightnessDataProcessor.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/statistics/BrightnessDataProcessor; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ScreenStateReceiver" */
} // .end annotation
/* # instance fields */
final com.android.server.display.statistics.BrightnessDataProcessor this$0; //synthetic
/* # direct methods */
 com.android.server.display.statistics.BrightnessDataProcessor$ScreenStateReceiver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/statistics/BrightnessDataProcessor; */
/* .line 1299 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 1302 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1303 */
/* .local v0, "screenOn":Z */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v2 = (( java.lang.String ) v1 ).hashCode ( ); // invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v2, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v2 = "android.intent.action.SCREEN_ON"; // const-string v2, "android.intent.action.SCREEN_ON"
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 0; // const/4 v1, 0x0
/* :sswitch_1 */
final String v2 = "android.intent.action.SCREEN_OFF"; // const-string v2, "android.intent.action.SCREEN_OFF"
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 int v1 = 1; // const/4 v1, 0x1
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
} // :goto_1
/* packed-switch v1, :pswitch_data_0 */
/* .line 1305 */
/* :pswitch_0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1306 */
/* nop */
/* .line 1311 */
} // :goto_2
v1 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fgetmBackgroundHandler ( v1 );
int v2 = 4; // const/4 v2, 0x4
java.lang.Boolean .valueOf ( v0 );
android.os.Message .obtain ( v1,v2,v3 );
/* .line 1312 */
/* .local v1, "msg":Landroid/os/Message; */
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 1313 */
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x7ed8ea7f -> :sswitch_1 */
/* -0x56ac2893 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
