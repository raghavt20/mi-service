.class Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;
.super Ljava/lang/Object;
.source "BrightnessDataProcessor.java"

# interfaces
.implements Landroid/view/WindowManagerPolicyConstants$PointerEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/statistics/BrightnessDataProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WindowOverlayEventListener"
.end annotation


# instance fields
.field distance_x:I

.field distance_y:I

.field eventXDown:F

.field eventXUp:F

.field eventYDown:F

.field eventYUp:F

.field final synthetic this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

.field userDragging:Z


# direct methods
.method constructor <init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/display/statistics/BrightnessDataProcessor;

    .line 1333
    iput-object p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1335
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXDown:F

    .line 1336
    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYDown:F

    .line 1337
    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXUp:F

    .line 1338
    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYUp:F

    return-void
.end method


# virtual methods
.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 1345
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1351
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->userDragging:Z

    .line 1352
    goto :goto_0

    .line 1354
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXUp:F

    .line 1355
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYUp:F

    .line 1356
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->userDragging:Z

    .line 1357
    goto :goto_0

    .line 1347
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXDown:F

    .line 1348
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYDown:F

    .line 1349
    nop

    .line 1362
    :goto_0
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->userDragging:Z

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$fgetmUserDragging(Lcom/android/server/display/statistics/BrightnessDataProcessor;)Z

    move-result v1

    if-eq v0, v1, :cond_1

    .line 1363
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$fgetmBackgroundHandler(Lcom/android/server/display/statistics/BrightnessDataProcessor;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1364
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->userDragging:Z

    if-nez v0, :cond_0

    .line 1365
    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXUp:F

    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXDown:F

    sub-float/2addr v0, v2

    invoke-static {v0}, Landroid/util/MathUtils;->abs(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->distance_x:I

    .line 1366
    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYUp:F

    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYDown:F

    sub-float/2addr v0, v2

    invoke-static {v0}, Landroid/util/MathUtils;->abs(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->distance_y:I

    .line 1367
    invoke-static {}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$sfgetDEBUG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1368
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPointerEvent: x_down: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXDown:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", y_down: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYDown:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", x_up: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXUp:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", y_up: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYUp:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", distance_x: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->distance_x:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", distance_y: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->distance_y:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "BrightnessDataProcessor"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1375
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$fgetmBackgroundHandler(Lcom/android/server/display/statistics/BrightnessDataProcessor;)Landroid/os/Handler;

    move-result-object v0

    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->distance_x:I

    iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->distance_y:I

    iget-boolean v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->userDragging:Z

    .line 1376
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 1375
    invoke-static {v0, v1, v2, v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1377
    .local v0, "message":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1379
    .end local v0    # "message":Landroid/os/Message;
    :cond_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
