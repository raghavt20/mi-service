.class Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;
.super Landroid/os/Handler;
.source "BrightnessDataProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/statistics/BrightnessDataProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BrightnessChangeHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;


# direct methods
.method public constructor <init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 1118
    iput-object p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    .line 1119
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1120
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .line 1124
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 1174
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$fputmIsNotAonSuppressDarken(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V

    .line 1175
    goto/16 :goto_0

    .line 1171
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$fputmIsAonSuppressDarken(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V

    .line 1172
    goto/16 :goto_0

    .line 1167
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/display/statistics/BrightnessEvent;

    .line 1168
    .local v0, "event":Lcom/android/server/display/statistics/BrightnessEvent;
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v1, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mreportEventToServer(Lcom/android/server/display/statistics/BrightnessDataProcessor;Lcom/android/server/display/statistics/BrightnessEvent;)V

    .line 1169
    goto/16 :goto_0

    .line 1164
    .end local v0    # "event":Lcom/android/server/display/statistics/BrightnessEvent;
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mresetBrightnessAnimInfo(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    .line 1165
    goto/16 :goto_0

    .line 1161
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$fputmTemporaryBrightnessTimeStamp(Lcom/android/server/display/statistics/BrightnessDataProcessor;J)V

    .line 1162
    goto/16 :goto_0

    .line 1158
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mupdateInterruptBrightnessAnimDuration(Lcom/android/server/display/statistics/BrightnessDataProcessor;IF)V

    .line 1159
    goto/16 :goto_0

    .line 1155
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$fputmTargetBrightnessAnimValue(Lcom/android/server/display/statistics/BrightnessDataProcessor;F)V

    .line 1156
    goto/16 :goto_0

    .line 1151
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/internal/os/SomeArgs;

    .line 1152
    .local v0, "someArgs":Lcom/android/internal/os/SomeArgs;
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget-object v2, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v3, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iget-object v4, v0, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mupdateBrightnessAnimInfo(Lcom/android/server/display/statistics/BrightnessDataProcessor;FFZ)V

    .line 1153
    goto :goto_0

    .line 1147
    .end local v0    # "someArgs":Lcom/android/internal/os/SomeArgs;
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/internal/os/SomeArgs;

    .line 1148
    .local v0, "args":Lcom/android/internal/os/SomeArgs;
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget-object v2, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v3, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mupdateBrightnessStatisticsData(Lcom/android/server/display/statistics/BrightnessDataProcessor;ZF)V

    .line 1149
    goto :goto_0

    .line 1144
    .end local v0    # "args":Lcom/android/internal/os/SomeArgs;
    :pswitch_9
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$fputmOrientation(Lcom/android/server/display/statistics/BrightnessDataProcessor;I)V

    .line 1145
    goto :goto_0

    .line 1140
    :pswitch_a
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mupdateForegroundApps(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    .line 1141
    goto :goto_0

    .line 1137
    :pswitch_b
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mupdateScreenStateChanged(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V

    .line 1138
    goto :goto_0

    .line 1132
    :pswitch_c
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mreadyToReportEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    .line 1133
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mbrightnessChangedTriggerAggregation(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    .line 1134
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mresetPendingParams(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    .line 1135
    goto :goto_0

    .line 1129
    :pswitch_d
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mupdatePointerEventMotionState(Lcom/android/server/display/statistics/BrightnessDataProcessor;ZII)V

    .line 1130
    goto :goto_0

    .line 1126
    :pswitch_e
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    invoke-static {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mhandleBrightnessChangeEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor;Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;)V

    .line 1127
    nop

    .line 1179
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
