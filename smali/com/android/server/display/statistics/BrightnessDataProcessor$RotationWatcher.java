class com.android.server.display.statistics.BrightnessDataProcessor$RotationWatcher extends android.view.IRotationWatcher$Stub {
	 /* .source "BrightnessDataProcessor.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/statistics/BrightnessDataProcessor; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "RotationWatcher" */
} // .end annotation
/* # instance fields */
final com.android.server.display.statistics.BrightnessDataProcessor this$0; //synthetic
/* # direct methods */
 com.android.server.display.statistics.BrightnessDataProcessor$RotationWatcher ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/statistics/BrightnessDataProcessor; */
/* .line 1324 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/view/IRotationWatcher$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onRotationChanged ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "rotation" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1328 */
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fgetmBackgroundHandler ( v0 );
int v1 = 6; // const/4 v1, 0x6
java.lang.Integer .valueOf ( p1 );
android.os.Message .obtain ( v0,v1,v2 );
/* .line 1329 */
/* .local v0, "msg":Landroid/os/Message; */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 1330 */
return;
} // .end method
