public class com.android.server.display.statistics.OneTrackFoldStateHelper {
	 /* .source "OneTrackFoldStateHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String APP_FOLDED_UNFOLDED_AVG_TIME;
private static final Integer APP_FOLD_SCREEN_AVG_TIME;
private static final java.lang.String APP_ID;
private static final Integer APP_UNFOLD_SCREEN_AVG_TIME;
private static final java.lang.String APP_USE_SCREEN_TIME;
private static final Boolean DEBUG;
private static final Long DEBUG_REPORT_TIME_DURATION;
private static final java.lang.String DEVICE_REGION;
private static final Integer DEVICE_STATE_CLOSE;
private static final Long DEVICE_STATE_DEBOUNCE_TIME;
private static final Integer DEVICE_STATE_HALF_OPEN;
private static final Integer DEVICE_STATE_INVALID;
private static final Integer DEVICE_STATE_OPEN;
private static final Integer DEVICE_STATE_OPEN_PRESENTATION;
private static final Integer DEVICE_STATE_OPEN_REVERSE;
private static final Integer DEVICE_STATE_OPEN_REVERSE_PRESENTATION;
private static final Integer DEVICE_STATE_TENT;
private static final java.lang.String DEVICE_STATE_TIME;
private static final java.lang.String EVENT_NAME;
private static final Integer FLAG_NON_ANONYMOUS;
private static final Integer FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN;
private static final java.lang.String FOLDED_AVG_TIME;
private static final java.lang.String FOLD_TIMES;
private static final Boolean IS_FOLDABLE_DEVICE;
private static final Boolean IS_INTERNATIONAL_BUILD;
private static final Integer MSG_DEVICE_STATE_CHANGED;
private static final Integer MSG_DEVICE_STATE_DEBOUNCE;
private static final Integer MSG_DISPLAY_SWAP_FINISHED;
private static final Integer MSG_INTERACTIVE_CHANGED;
private static final Integer MSG_ON_FOCUS_PACKAGE_CHANGED;
private static final Integer MSG_ON_FOLD_CHANGED;
private static final Integer MSG_ON_FOREGROUND_APP_CHANGED;
private static final Integer MSG_RESET_PENDING_DISPLAY_SWAP;
private static final Float ONE_SECOND;
private static final java.lang.String ONE_TRACK_ACTION;
private static final java.lang.String ONE_TRACK_PACKAGE_NAME;
private static final java.lang.String PACKAGE;
private static final Integer RECORD_REASON_DEVICE_STATE;
private static final Integer RECORD_REASON_FOREGROUND_APP;
private static final Integer RECORD_REASON_INTERACTIVE;
private static final Integer RECORD_REASON_REPORT_DATA;
private static final Integer SCREEN_TYPE_INVALID;
private static final Integer SCREEN_TYPE_LARGE;
private static final Integer SCREEN_TYPE_SMALL;
private static final java.lang.String TAG;
private static final Long TIMEOUT_PENDING_DISPLAY_SWAP_MILLIS;
private static final java.lang.String UNFOLDED_AVG_TIME;
private static volatile com.android.server.display.statistics.OneTrackFoldStateHelper sInstance;
/* # instance fields */
private android.app.ActivityManager mActivityManager;
private android.app.AlarmManager mAlarmManager;
private final java.util.Map mAppFoldAvgTimeMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Long;", */
/* ">;>;>;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mApplicationContext;
private Integer mCurrentDeviceState;
private final java.util.Map mDeviceStateUsageDetail;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String mFocusPackageName;
private Integer mFoldCountSum;
private Float mFoldTimeSum;
private Integer mFoldTimes;
private Boolean mFolded;
private final java.util.List mFoldedStateList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String mForegroundAppName;
private android.os.Handler mHandler;
private Boolean mInteractive;
private Integer mLastEventReason;
private Long mLastEventTime;
private java.lang.String mLastFocusPackageName;
private Long mLastFoldChangeTime;
private final android.app.AlarmManager$OnAlarmListener mOnAlarmListener;
private Integer mPendingDeviceState;
private Long mPendingDeviceStateDebounceTime;
private Boolean mPendingWaitForDisplaySwapFinished;
private Boolean mPreInteractive;
private Integer mPreviousDeviceState;
private Integer mUnfoldCountSum;
private Float mUnfoldTimeSum;
private final java.util.List mUnfoldedStateList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final miui.process.IForegroundWindowListener mWindowListener;
/* # direct methods */
public static void $r8$lambda$8rCWTTbE3NSM0NONciFj9mdfhhc ( com.android.server.display.statistics.OneTrackFoldStateHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->lambda$new$1()V */
return;
} // .end method
public static void $r8$lambda$IyyHt-6k500ctpciPAlYHlhrU_8 ( com.android.server.display.statistics.OneTrackFoldStateHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->lambda$new$0()V */
return;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.display.statistics.OneTrackFoldStateHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static void -$$Nest$mdebounceDeviceState ( com.android.server.display.statistics.OneTrackFoldStateHelper p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->debounceDeviceState(J)V */
return;
} // .end method
static void -$$Nest$mhandleDeviceStateChanged ( com.android.server.display.statistics.OneTrackFoldStateHelper p0, Integer p1, Long p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->handleDeviceStateChanged(IJ)V */
return;
} // .end method
static void -$$Nest$mhandleDisplaySwapFinished ( com.android.server.display.statistics.OneTrackFoldStateHelper p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->handleDisplaySwapFinished(J)V */
return;
} // .end method
static void -$$Nest$mhandleFocusedWindowChanged ( com.android.server.display.statistics.OneTrackFoldStateHelper p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->handleFocusedWindowChanged(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mhandleFoldChanged ( com.android.server.display.statistics.OneTrackFoldStateHelper p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->handleFoldChanged(Z)V */
return;
} // .end method
static void -$$Nest$mhandleInteractiveChanged ( com.android.server.display.statistics.OneTrackFoldStateHelper p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->handleInteractiveChanged(Z)V */
return;
} // .end method
static void -$$Nest$mhandleWindowChanged ( com.android.server.display.statistics.OneTrackFoldStateHelper p0, miui.process.ForegroundInfo p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->handleWindowChanged(Lmiui/process/ForegroundInfo;)V */
return;
} // .end method
static Boolean -$$Nest$sfgetDEBUG ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z */
} // .end method
static com.android.server.display.statistics.OneTrackFoldStateHelper ( ) {
/* .locals 4 */
/* .line 37 */
/* nop */
/* .line 38 */
final String v0 = "debug.miui.power.fold.dgb"; // const-string v0, "debug.miui.power.fold.dgb"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v2 */
} // :cond_0
/* move v0, v1 */
} // :goto_0
com.android.server.display.statistics.OneTrackFoldStateHelper.DEBUG = (v0!= 0);
/* .line 58 */
/* nop */
/* .line 59 */
final String v0 = "persist.sys.muiltdisplay_type"; // const-string v0, "persist.sys.muiltdisplay_type"
v0 = android.os.SystemProperties .getInt ( v0,v1 );
int v3 = 2; // const/4 v3, 0x2
/* if-ne v0, v3, :cond_1 */
/* move v1, v2 */
} // :cond_1
com.android.server.display.statistics.OneTrackFoldStateHelper.IS_FOLDABLE_DEVICE = (v1!= 0);
/* .line 85 */
/* nop */
/* .line 86 */
final String v0 = "ro.product.mod_device"; // const-string v0, "ro.product.mod_device"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
final String v1 = "_global"; // const-string v1, "_global"
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
com.android.server.display.statistics.OneTrackFoldStateHelper.IS_INTERNATIONAL_BUILD = (v0!= 0);
/* .line 88 */
final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
final String v1 = "CN"; // const-string v1, "CN"
android.os.SystemProperties .get ( v0,v1 );
return;
} // .end method
private com.android.server.display.statistics.OneTrackFoldStateHelper ( ) {
/* .locals 2 */
/* .line 167 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 120 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mDeviceStateUsageDetail = v0;
/* .line 122 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mAppFoldAvgTimeMap = v0;
/* .line 124 */
/* new-instance v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$1;-><init>(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;)V */
this.mFoldedStateList = v0;
/* .line 133 */
/* new-instance v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$2;-><init>(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;)V */
this.mUnfoldedStateList = v0;
/* .line 141 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimes:I */
/* .line 151 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mInteractive:Z */
/* .line 152 */
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPreInteractive:Z */
/* .line 153 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I */
/* .line 154 */
/* iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPreviousDeviceState:I */
/* .line 155 */
/* iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceState:I */
/* .line 158 */
/* const-wide/16 v0, -0x1 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceStateDebounceTime:J */
/* .line 210 */
/* new-instance v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;)V */
this.mOnAlarmListener = v0;
/* .line 230 */
/* new-instance v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$3; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$3;-><init>(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;)V */
this.mWindowListener = v0;
/* .line 168 */
android.app.ActivityThread .currentActivityThread ( );
(( android.app.ActivityThread ) v0 ).getApplication ( ); // invoke-virtual {v0}, Landroid/app/ActivityThread;->getApplication()Landroid/app/Application;
this.mApplicationContext = v0;
/* .line 169 */
/* new-instance v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler; */
com.android.server.MiuiBgThread .get ( );
(( com.android.server.MiuiBgThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$OneTrackFoldStateHelperHandler;-><init>(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 170 */
v0 = this.mApplicationContext;
final String v1 = "activity"; // const-string v1, "activity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
this.mActivityManager = v0;
/* .line 172 */
v0 = this.mApplicationContext;
final String v1 = "alarm"; // const-string v1, "alarm"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AlarmManager; */
this.mAlarmManager = v0;
/* .line 174 */
/* sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->IS_FOLDABLE_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 175 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/statistics/OneTrackFoldStateHelper;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 180 */
} // :cond_0
return;
} // .end method
private void addAppFoldInfoToIntent ( android.content.Intent p0 ) {
/* .locals 13 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 352 */
v0 = v0 = this.mAppFoldAvgTimeMap;
/* if-nez v0, :cond_4 */
/* .line 354 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 355 */
/* .local v0, "appFoldAvgTimeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Float;>;>;" */
v1 = this.mAppFoldAvgTimeMap;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Ljava/lang/String; */
/* .line 356 */
/* .local v2, "appName":Ljava/lang/String; */
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
/* .line 358 */
/* .local v3, "tempMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Float;>;" */
v4 = this.mAppFoldAvgTimeMap;
/* check-cast v4, Ljava/util/HashMap; */
(( java.util.HashMap ) v4 ).entrySet ( ); // invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v5 = } // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Ljava/util/Map$Entry; */
/* .line 359 */
/* .local v5, "foldEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/Long;>;>;" */
int v6 = 0; // const/4 v6, 0x0
/* .line 361 */
/* .local v6, "totalTime":F */
/* check-cast v7, Ljava/lang/Integer; */
/* .line 363 */
/* .local v7, "foldType":Ljava/lang/Integer; */
/* check-cast v8, Ljava/util/List; */
/* .line 365 */
/* .local v8, "timeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;" */
v10 = } // :goto_2
if ( v10 != null) { // if-eqz v10, :cond_0
/* check-cast v10, Ljava/lang/Long; */
(( java.lang.Long ) v10 ).longValue ( ); // invoke-virtual {v10}, Ljava/lang/Long;->longValue()J
/* move-result-wide v10 */
/* .line 366 */
/* .local v10, "time":J */
/* long-to-float v12, v10 */
/* add-float/2addr v6, v12 */
/* .line 367 */
} // .end local v10 # "time":J
/* .line 368 */
} // :cond_0
int v9 = 0; // const/4 v9, 0x0
/* .line 370 */
v10 = /* .local v9, "avgTime":F */
if ( v10 != null) { // if-eqz v10, :cond_1
v10 = /* .line 371 */
/* int-to-float v10, v10 */
/* div-float v9, v6, v10 */
/* .line 373 */
} // :cond_1
java.lang.Float .valueOf ( v9 );
(( java.util.HashMap ) v3 ).put ( v7, v10 ); // invoke-virtual {v3, v7, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 374 */
} // .end local v5 # "foldEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/Long;>;>;"
} // .end local v6 # "totalTime":F
} // .end local v7 # "foldType":Ljava/lang/Integer;
} // .end local v8 # "timeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
} // .end local v9 # "avgTime":F
/* .line 375 */
} // :cond_2
/* .line 376 */
} // .end local v2 # "appName":Ljava/lang/String;
} // .end local v3 # "tempMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Float;>;"
/* .line 378 */
} // :cond_3
final String v1 = "app_folded_unfolded_avg_time"; // const-string v1, "app_folded_unfolded_avg_time"
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( android.content.Intent ) p1 ).putExtra ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 380 */
} // .end local v0 # "appFoldAvgTimeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Float;>;>;"
} // :cond_4
/* iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimeSum:F */
int v1 = 0; // const/4 v1, 0x0
/* cmpl-float v2, v0, v1 */
if ( v2 != null) { // if-eqz v2, :cond_5
/* iget v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldCountSum:I */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 381 */
/* int-to-float v2, v2 */
/* div-float/2addr v0, v2 */
/* .line 383 */
/* .local v0, "foldAvgTime":F */
final String v2 = "folded_avg_time"; // const-string v2, "folded_avg_time"
(( android.content.Intent ) p1 ).putExtra ( v2, v0 ); // invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 385 */
} // .end local v0 # "foldAvgTime":F
} // :cond_5
/* iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldTimeSum:F */
/* cmpl-float v1, v0, v1 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* iget v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldCountSum:I */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 386 */
/* int-to-float v1, v1 */
/* div-float/2addr v0, v1 */
/* .line 388 */
/* .local v0, "unfoldAvgTime":F */
/* const-string/jumbo v1, "unfolded_avg_time" */
(( android.content.Intent ) p1 ).putExtra ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 391 */
} // .end local v0 # "unfoldAvgTime":F
} // :cond_6
final String v0 = "fold_times"; // const-string v0, "fold_times"
/* iget v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimes:I */
(( android.content.Intent ) p1 ).putExtra ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 392 */
return;
} // .end method
private void addAppUsageTimeInScreenToIntent ( android.content.Intent p0 ) {
/* .locals 14 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 399 */
int v0 = -1; // const/4 v0, -0x1
/* .line 400 */
/* .local v0, "stateType":I */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* .line 401 */
/* .local v1, "deviceStateTimeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Float;>;>;" */
v2 = this.mDeviceStateUsageDetail;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Ljava/lang/Integer; */
/* .line 403 */
/* .local v3, "deviceState":Ljava/lang/Integer; */
v4 = v4 = this.mFoldedStateList;
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 404 */
int v0 = 1; // const/4 v0, 0x1
/* .line 405 */
} // :cond_0
v4 = v4 = this.mUnfoldedStateList;
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 406 */
int v0 = 0; // const/4 v0, 0x0
/* .line 410 */
} // :cond_1
} // :goto_1
v4 = this.mDeviceStateUsageDetail;
/* check-cast v4, Ljava/util/HashMap; */
(( java.util.HashMap ) v4 ).entrySet ( ); // invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v5 = } // :goto_2
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Ljava/util/Map$Entry; */
/* .line 411 */
/* .local v5, "appUsageEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;" */
/* check-cast v6, Ljava/lang/String; */
/* .line 412 */
/* .local v6, "appName":Ljava/lang/String; */
/* check-cast v7, Ljava/lang/Long; */
(( java.lang.Long ) v7 ).longValue ( ); // invoke-virtual {v7}, Ljava/lang/Long;->longValue()J
/* move-result-wide v7 */
/* .line 413 */
/* .local v7, "appUseTime":J */
/* new-instance v9, Ljava/util/HashMap; */
/* invoke-direct {v9}, Ljava/util/HashMap;-><init>()V */
/* .line 414 */
/* check-cast v9, Ljava/util/Map; */
/* .line 415 */
/* .local v9, "appUseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Float;>;" */
java.lang.Integer .valueOf ( v0 );
int v11 = 0; // const/4 v11, 0x0
java.lang.Float .valueOf ( v11 );
/* check-cast v10, Ljava/lang/Float; */
v10 = (( java.lang.Float ) v10 ).floatValue ( ); // invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F
/* .line 416 */
/* .local v10, "duration":F */
/* long-to-float v11, v7 */
/* add-float/2addr v11, v10 */
/* const/high16 v12, 0x447a0000 # 1000.0f */
/* div-float/2addr v11, v12 */
/* .line 417 */
/* .local v11, "newDuration":F */
java.lang.Integer .valueOf ( v0 );
java.lang.Float .valueOf ( v11 );
/* .line 418 */
/* .line 419 */
} // .end local v5 # "appUsageEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
} // .end local v6 # "appName":Ljava/lang/String;
} // .end local v7 # "appUseTime":J
} // .end local v9 # "appUseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Float;>;"
} // .end local v10 # "duration":F
} // .end local v11 # "newDuration":F
/* .line 420 */
} // .end local v3 # "deviceState":Ljava/lang/Integer;
} // :cond_2
/* .line 421 */
v2 = } // :cond_3
/* if-nez v2, :cond_4 */
/* .line 422 */
final String v2 = "app_use_screen_time"; // const-string v2, "app_use_screen_time"
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( android.content.Intent ) p1 ).putExtra ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 424 */
} // :cond_4
return;
} // .end method
private void addDeviceStateUsageTimeToIntent ( android.content.Intent p0 ) {
/* .locals 8 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 331 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 332 */
/* .local v0, "deviceStateUsageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Float;>;" */
v1 = this.mDeviceStateUsageDetail;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/lang/Integer; */
/* .line 334 */
/* .local v2, "deviceState":Ljava/lang/Integer; */
/* const-wide/16 v3, 0x0 */
/* .line 335 */
/* .local v3, "stateSumTime":J */
v5 = this.mDeviceStateUsageDetail;
/* check-cast v5, Ljava/util/HashMap; */
(( java.util.HashMap ) v5 ).values ( ); // invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_0
/* check-cast v6, Ljava/lang/Long; */
(( java.lang.Long ) v6 ).longValue ( ); // invoke-virtual {v6}, Ljava/lang/Long;->longValue()J
/* move-result-wide v6 */
/* .line 336 */
/* .local v6, "usageTime":J */
/* add-long/2addr v3, v6 */
/* .line 337 */
} // .end local v6 # "usageTime":J
/* .line 338 */
} // :cond_0
/* long-to-float v5, v3 */
/* const/high16 v6, 0x447a0000 # 1000.0f */
/* div-float/2addr v5, v6 */
/* .line 339 */
/* .local v5, "deviceStateTime":F */
java.lang.Float .valueOf ( v5 );
/* .line 340 */
} // .end local v2 # "deviceState":Ljava/lang/Integer;
} // .end local v3 # "stateSumTime":J
} // .end local v5 # "deviceStateTime":F
/* .line 341 */
v1 = } // :cond_1
/* if-nez v1, :cond_2 */
/* .line 342 */
final String v1 = "device_state_time"; // const-string v1, "device_state_time"
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( android.content.Intent ) p1 ).putExtra ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 344 */
} // :cond_2
return;
} // .end method
private void debounceDeviceState ( Long p0 ) {
/* .locals 6 */
/* .param p1, "eventTime" # J */
/* .line 580 */
/* iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceState:I */
int v1 = -1; // const/4 v1, -0x1
/* if-eq v0, v1, :cond_2 */
/* iget-wide v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceStateDebounceTime:J */
/* const-wide/16 v4, 0x0 */
/* cmp-long v0, v2, v4 */
/* if-ltz v0, :cond_2 */
/* .line 581 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* .line 583 */
/* .local v2, "now":J */
/* iget-wide v4, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceStateDebounceTime:J */
/* cmp-long v0, v4, v2 */
/* if-gtz v0, :cond_1 */
/* .line 584 */
/* iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I */
/* iget v4, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceState:I */
/* if-eq v0, v4, :cond_0 */
/* .line 586 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->recordDeviceStateDetails(I)V */
/* .line 587 */
/* iput-wide v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastEventTime:J */
/* .line 588 */
/* iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I */
/* iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPreviousDeviceState:I */
/* .line 589 */
/* iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceState:I */
/* iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I */
/* .line 591 */
} // :cond_0
/* iput v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceState:I */
/* .line 592 */
/* const-wide/16 v0, -0x1 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceStateDebounceTime:J */
/* .line 595 */
} // :cond_1
v0 = this.mHandler;
int v1 = 4; // const/4 v1, 0x4
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 596 */
v0 = this.mHandler;
java.lang.Long .valueOf ( p1,p2 );
(( android.os.Handler ) v0 ).obtainMessage ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 597 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
/* iget-wide v4, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceStateDebounceTime:J */
(( android.os.Handler ) v1 ).sendMessageAtTime ( v0, v4, v5 ); // invoke-virtual {v1, v0, v4, v5}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z
/* .line 600 */
} // .end local v0 # "msg":Landroid/os/Message;
} // .end local v2 # "now":J
} // :cond_2
} // :goto_0
return;
} // .end method
public static com.android.server.display.statistics.OneTrackFoldStateHelper getInstance ( ) {
/* .locals 2 */
/* .line 183 */
v0 = com.android.server.display.statistics.OneTrackFoldStateHelper.sInstance;
/* if-nez v0, :cond_1 */
/* .line 184 */
/* const-class v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper; */
/* monitor-enter v0 */
/* .line 185 */
try { // :try_start_0
v1 = com.android.server.display.statistics.OneTrackFoldStateHelper.sInstance;
/* if-nez v1, :cond_0 */
/* .line 186 */
/* new-instance v1, Lcom/android/server/display/statistics/OneTrackFoldStateHelper; */
/* invoke-direct {v1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;-><init>()V */
/* .line 188 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 190 */
} // :cond_1
} // :goto_0
v0 = com.android.server.display.statistics.OneTrackFoldStateHelper.sInstance;
} // .end method
private java.lang.String getTopAppName ( ) {
/* .locals 6 */
/* .line 432 */
final String v0 = "getTopAppName: exception:"; // const-string v0, "getTopAppName: exception:"
final String v1 = "OneTrackFoldStateHelper"; // const-string v1, "OneTrackFoldStateHelper"
final String v2 = ""; // const-string v2, ""
try { // :try_start_0
v3 = this.mActivityManager;
int v4 = 1; // const/4 v4, 0x1
(( android.app.ActivityManager ) v3 ).getRunningTasks ( v4 ); // invoke-virtual {v3, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = this.mActivityManager;
/* .line 433 */
(( android.app.ActivityManager ) v3 ).getRunningTasks ( v4 ); // invoke-virtual {v3, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;
int v5 = 0; // const/4 v5, 0x0
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 434 */
v3 = this.mActivityManager;
(( android.app.ActivityManager ) v3 ).getRunningTasks ( v4 ); // invoke-virtual {v3, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;
/* check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo; */
v3 = this.topActivity;
/* .line 435 */
/* .local v3, "cn":Landroid/content/ComponentName; */
(( android.content.ComponentName ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 436 */
/* .local v0, "packageName":Ljava/lang/String; */
/* .line 438 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // .end local v3 # "cn":Landroid/content/ComponentName;
} // :cond_0
/* .line 443 */
/* :catch_0 */
/* move-exception v3 */
/* .line 444 */
/* .local v3, "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 445 */
/* .line 440 */
} // .end local v3 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v3 */
/* .line 441 */
/* .local v3, "e":Ljava/lang/IndexOutOfBoundsException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.IndexOutOfBoundsException ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/IndexOutOfBoundsException;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 442 */
} // .end method
private void handleDeviceStateChanged ( Integer p0, Long p1 ) {
/* .locals 4 */
/* .param p1, "toState" # I */
/* .param p2, "eventTime" # J */
/* .line 566 */
/* sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 567 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handleDeviceStateChanged: toState: "; // const-string v1, "handleDeviceStateChanged: toState: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "OneTrackFoldStateHelper"; // const-string v1, "OneTrackFoldStateHelper"
android.util.Slog .d ( v1,v0 );
/* .line 569 */
} // :cond_0
/* iput p1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceState:I */
/* .line 570 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 571 */
/* .local v0, "now":J */
/* const-wide/16 v2, 0x3e8 */
/* add-long/2addr v2, v0 */
/* iput-wide v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingDeviceStateDebounceTime:J */
/* .line 572 */
/* invoke-direct {p0, p2, p3}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->debounceDeviceState(J)V */
/* .line 573 */
return;
} // .end method
private void handleDisplaySwapFinished ( Long p0 ) {
/* .locals 7 */
/* .param p1, "time" # J */
/* .line 510 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingWaitForDisplaySwapFinished:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mLastFocusPackageName;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 511 */
/* iget-wide v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastFoldChangeTime:J */
/* sub-long v0, p1, v0 */
/* .line 512 */
/* .local v0, "duration":J */
/* sget-boolean v2, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 513 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "handleDisplaySwapFinished: duration: "; // const-string v3, "handleDisplaySwapFinished: duration: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "OneTrackFoldStateHelper"; // const-string v3, "OneTrackFoldStateHelper"
android.util.Slog .i ( v3,v2 );
/* .line 516 */
} // :cond_0
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFolded:Z */
/* xor-int/lit8 v2, v2, 0x1 */
java.lang.Integer .valueOf ( v2 );
/* .line 517 */
/* .local v2, "foldType":Ljava/lang/Integer; */
v3 = this.mAppFoldAvgTimeMap;
v4 = this.mLastFocusPackageName;
/* new-instance v5, Ljava/util/HashMap; */
/* invoke-direct {v5}, Ljava/util/HashMap;-><init>()V */
/* .line 518 */
/* check-cast v3, Ljava/util/HashMap; */
/* .line 519 */
/* .local v3, "appFoldTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/Long;>;>;" */
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
(( java.util.HashMap ) v3 ).getOrDefault ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Ljava/util/List; */
/* .line 521 */
/* .local v4, "appTimeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;" */
java.lang.Long .valueOf ( v0,v1 );
/* .line 522 */
(( java.util.HashMap ) v3 ).put ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 524 */
/* iget-boolean v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFolded:Z */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 525 */
/* iget v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimeSum:F */
/* long-to-float v6, v0 */
/* add-float/2addr v5, v6 */
/* iput v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimeSum:F */
/* .line 526 */
/* iget v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldCountSum:I */
/* add-int/lit8 v5, v5, 0x1 */
/* iput v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldCountSum:I */
/* .line 528 */
} // :cond_1
/* iget v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldTimeSum:F */
/* long-to-float v6, v0 */
/* add-float/2addr v5, v6 */
/* iput v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldTimeSum:F */
/* .line 529 */
/* iget v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldCountSum:I */
/* add-int/lit8 v5, v5, 0x1 */
/* iput v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldCountSum:I */
/* .line 531 */
} // :goto_0
v5 = this.mAppFoldAvgTimeMap;
v6 = this.mLastFocusPackageName;
/* .line 533 */
int v5 = 0; // const/4 v5, 0x0
/* iput-boolean v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingWaitForDisplaySwapFinished:Z */
/* .line 535 */
} // .end local v0 # "duration":J
} // .end local v2 # "foldType":Ljava/lang/Integer;
} // .end local v3 # "appFoldTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/Long;>;>;"
} // .end local v4 # "appTimeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
} // :cond_2
return;
} // .end method
private void handleDisplaySwapStarted ( ) {
/* .locals 4 */
/* .line 491 */
/* sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 492 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handleDisplaySwapStarted: mInteractive: "; // const-string v1, "handleDisplaySwapStarted: mInteractive: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mInteractive:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "OneTrackFoldStateHelper"; // const-string v1, "OneTrackFoldStateHelper"
android.util.Slog .d ( v1,v0 );
/* .line 494 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mInteractive:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPreInteractive:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 495 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastFoldChangeTime:J */
/* .line 496 */
v0 = this.mFocusPackageName;
this.mLastFocusPackageName = v0;
/* .line 497 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingWaitForDisplaySwapFinished:Z */
/* .line 498 */
v0 = this.mHandler;
int v1 = 7; // const/4 v1, 0x7
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 499 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 500 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
/* const-wide/16 v2, 0x1388 */
(( android.os.Handler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 502 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mInteractive:Z */
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPreInteractive:Z */
/* .line 503 */
return;
} // .end method
private void handleFocusedWindowChanged ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "focusPackageName" # Ljava/lang/String; */
/* .line 542 */
/* sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 543 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handleFocusedWindowChanged: focusPackageName: "; // const-string v1, "handleFocusedWindowChanged: focusPackageName: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "OneTrackFoldStateHelper"; // const-string v1, "OneTrackFoldStateHelper"
android.util.Slog .d ( v1,v0 );
/* .line 545 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = this.mFocusPackageName;
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 546 */
this.mFocusPackageName = p1;
/* .line 548 */
} // :cond_1
return;
} // .end method
private void handleFoldChanged ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "folded" # Z */
/* .line 257 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFolded:Z */
final String v1 = "OneTrackFoldStateHelper"; // const-string v1, "OneTrackFoldStateHelper"
/* if-ne v0, p1, :cond_1 */
/* .line 258 */
/* sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 259 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "handleFoldChanged: no change mFolded: "; // const-string v2, "handleFoldChanged: no change mFolded: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFolded:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 261 */
} // :cond_0
return;
/* .line 263 */
} // :cond_1
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFolded:Z */
/* .line 264 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->handleDisplaySwapStarted()V */
/* .line 266 */
/* iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimes:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimes:I */
/* .line 267 */
/* sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 268 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "handleFoldChanged: mFoldTimes: "; // const-string v2, "handleFoldChanged: mFoldTimes: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimes:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 270 */
} // :cond_2
return;
} // .end method
private void handleInteractiveChanged ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "interactive" # Z */
/* .line 607 */
/* sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 608 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handleInteractiveChanged: interactive: "; // const-string v1, "handleInteractiveChanged: interactive: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "OneTrackFoldStateHelper"; // const-string v1, "OneTrackFoldStateHelper"
android.util.Slog .d ( v1,v0 );
/* .line 610 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mInteractive:Z */
/* if-eq v0, p1, :cond_3 */
/* .line 611 */
/* iput-boolean p1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mInteractive:Z */
/* .line 612 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 613 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->getTopAppName()Ljava/lang/String; */
this.mForegroundAppName = v1;
/* .line 615 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* invoke-direct {p0, v1, v2, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->updateLastEventInfo(JI)V */
/* .line 618 */
} // :cond_1
/* iget-boolean v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingWaitForDisplaySwapFinished:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 619 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mPendingWaitForDisplaySwapFinished:Z */
/* .line 620 */
v1 = this.mHandler;
int v2 = 7; // const/4 v2, 0x7
(( android.os.Handler ) v1 ).removeMessages ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 623 */
} // :cond_2
/* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->recordDeviceStateDetails(I)V */
/* .line 626 */
} // :cond_3
} // :goto_0
return;
} // .end method
private void handleWindowChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 4 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .line 273 */
v0 = this.mForegroundPackageName;
/* .line 274 */
/* .local v0, "packageName":Ljava/lang/String; */
/* sget-boolean v1, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 275 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "handleWindowChanged: pkgName: "; // const-string v2, "handleWindowChanged: pkgName: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", mForegroundAppName: "; // const-string v2, ", mForegroundAppName: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mForegroundAppName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "OneTrackFoldStateHelper"; // const-string v2, "OneTrackFoldStateHelper"
android.util.Slog .d ( v2,v1 );
/* .line 278 */
} // :cond_0
v1 = this.mForegroundAppName;
v1 = android.text.TextUtils .equals ( v1,v0 );
/* if-nez v1, :cond_1 */
/* .line 279 */
int v1 = 2; // const/4 v1, 0x2
/* invoke-direct {p0, v1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->recordDeviceStateDetails(I)V */
/* .line 280 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* invoke-direct {p0, v2, v3, v1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->updateLastEventInfo(JI)V */
/* .line 281 */
this.mForegroundAppName = v0;
/* .line 283 */
} // :cond_1
return;
} // .end method
private void lambda$new$0 ( ) { //synthethic
/* .locals 0 */
/* .line 176 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->registerForegroundWindowListener()V */
/* .line 177 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->setReportScheduleEventAlarm()V */
/* .line 178 */
return;
} // .end method
private void lambda$new$1 ( ) { //synthethic
/* .locals 0 */
/* .line 211 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->reportScheduleEvents()V */
/* .line 212 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->setReportScheduleEventAlarm()V */
/* .line 213 */
return;
} // .end method
private void recordDeviceStateDetails ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "reason" # I */
/* .line 641 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mInteractive:Z */
/* if-nez v0, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_1
int v0 = 3; // const/4 v0, 0x3
/* if-eq p1, v0, :cond_1 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_2 */
/* .line 644 */
} // :cond_1
return;
/* .line 646 */
} // :cond_2
/* sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 647 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "recordDeviceStateDetails: start tracking device state: "; // const-string v1, "recordDeviceStateDetails: start tracking device state: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", reason: "; // const-string v1, ", reason: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "OneTrackFoldStateHelper"; // const-string v1, "OneTrackFoldStateHelper"
android.util.Slog .i ( v1,v0 );
/* .line 650 */
} // :cond_3
v0 = this.mDeviceStateUsageDetail;
/* iget v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I */
/* .line 651 */
java.lang.Integer .valueOf ( v1 );
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
/* check-cast v0, Ljava/util/HashMap; */
/* .line 652 */
/* .local v0, "deviceStateMaps":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;" */
v1 = this.mForegroundAppName;
/* const-wide/16 v2, 0x0 */
java.lang.Long .valueOf ( v2,v3 );
(( java.util.HashMap ) v0 ).getOrDefault ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Long; */
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v1 */
/* .line 653 */
/* .local v1, "duration":J */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* .line 654 */
/* .local v3, "now":J */
/* iget-wide v5, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastEventTime:J */
/* sub-long v5, v3, v5 */
/* add-long/2addr v5, v1 */
/* .line 655 */
/* .local v5, "newDuration":J */
v7 = this.mForegroundAppName;
java.lang.Long .valueOf ( v5,v6 );
(( java.util.HashMap ) v0 ).put ( v7, v8 ); // invoke-virtual {v0, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 656 */
v7 = this.mDeviceStateUsageDetail;
/* iget v8, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mCurrentDeviceState:I */
java.lang.Integer .valueOf ( v8 );
/* .line 657 */
return;
} // .end method
private void registerForegroundWindowListener ( ) {
/* .locals 1 */
/* .line 253 */
v0 = this.mWindowListener;
miui.process.ProcessManager .registerForegroundWindowListener ( v0 );
/* .line 254 */
return;
} // .end method
private void reportFoldInfoToOneTrack ( ) {
/* .locals 4 */
/* .line 290 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "onetrack.action.TRACK_EVENT"; // const-string v1, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 291 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 292 */
final String v2 = "APP_ID"; // const-string v2, "APP_ID"
final String v3 = "31000401594"; // const-string v3, "31000401594"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 293 */
final String v2 = "PACKAGE"; // const-string v2, "PACKAGE"
final String v3 = "android"; // const-string v3, "android"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 294 */
final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
final String v3 = "fold_statistic"; // const-string v3, "fold_statistic"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 296 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->addDeviceStateUsageTimeToIntent(Landroid/content/Intent;)V */
/* .line 298 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->addAppFoldInfoToIntent(Landroid/content/Intent;)V */
/* .line 300 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->addAppUsageTimeInScreenToIntent(Landroid/content/Intent;)V */
/* .line 302 */
/* sget-boolean v1, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v1, :cond_0 */
/* .line 303 */
int v1 = 3; // const/4 v1, 0x3
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 305 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "reportOneTrack: data: "; // const-string v2, "reportOneTrack: data: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.Intent ) v0 ).getExtras ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "OneTrackFoldStateHelper"; // const-string v2, "OneTrackFoldStateHelper"
android.util.Slog .d ( v2,v1 );
/* .line 307 */
try { // :try_start_0
v1 = this.mApplicationContext;
v3 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).startServiceAsUser ( v0, v3 ); // invoke-virtual {v1, v0, v3}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 310 */
/* .line 308 */
/* :catch_0 */
/* move-exception v1 */
/* .line 309 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v3 = "reportOneTrack Failed to upload!"; // const-string v3, "reportOneTrack Failed to upload!"
android.util.Slog .e ( v2,v3 );
/* .line 311 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void reportScheduleEvents ( ) {
/* .locals 3 */
/* .line 216 */
v0 = com.android.server.display.statistics.OneTrackFoldStateHelper.DEVICE_REGION;
final String v1 = "IN"; // const-string v1, "IN"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 217 */
return;
/* .line 219 */
} // :cond_0
int v0 = 3; // const/4 v0, 0x3
/* invoke-direct {p0, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->recordDeviceStateDetails(I)V */
/* .line 220 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* invoke-direct {p0, v1, v2, v0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->updateLastEventInfo(JI)V */
/* .line 222 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->reportFoldInfoToOneTrack()V */
/* .line 224 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->resetFoldInfoAfterReported()V */
/* .line 225 */
return;
} // .end method
private void resetFoldInfoAfterReported ( ) {
/* .locals 2 */
/* .line 317 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimes:I */
/* .line 318 */
v1 = this.mDeviceStateUsageDetail;
/* .line 319 */
v1 = this.mAppFoldAvgTimeMap;
/* .line 320 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldTimeSum:F */
/* .line 321 */
/* iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mFoldCountSum:I */
/* .line 322 */
/* iput v1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldTimeSum:F */
/* .line 323 */
/* iput v0, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mUnfoldCountSum:I */
/* .line 324 */
return;
} // .end method
private void setReportScheduleEventAlarm ( ) {
/* .locals 14 */
/* .line 197 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 198 */
/* .local v0, "now":J */
/* sget-boolean v2, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* const-wide/32 v3, 0x1d4c0 */
} // :cond_0
/* const-wide/32 v3, 0x5265c00 */
/* .line 199 */
/* .local v3, "duration":J */
} // :goto_0
/* add-long v12, v0, v3 */
/* .line 200 */
/* .local v12, "nextTime":J */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 201 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "setReportSwitchStatAlarm: next time: " */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 202 */
android.util.TimeUtils .formatDuration ( v3,v4 );
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 201 */
final String v5 = "OneTrackFoldStateHelper"; // const-string v5, "OneTrackFoldStateHelper"
android.util.Slog .d ( v5,v2 );
/* .line 204 */
} // :cond_1
v5 = this.mAlarmManager;
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 205 */
int v6 = 2; // const/4 v6, 0x2
final String v9 = "OneTrackFoldStateHelper"; // const-string v9, "OneTrackFoldStateHelper"
v10 = this.mOnAlarmListener;
v11 = this.mHandler;
/* move-wide v7, v12 */
/* invoke-virtual/range {v5 ..v11}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V */
/* .line 208 */
} // :cond_2
return;
} // .end method
private void updateLastEventInfo ( Long p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "eventTime" # J */
/* .param p3, "reason" # I */
/* .line 629 */
/* iput-wide p1, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastEventTime:J */
/* .line 630 */
/* iput p3, p0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->mLastEventReason:I */
/* .line 631 */
return;
} // .end method
/* # virtual methods */
public void notifyDeviceStateChanged ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "newState" # I */
/* .line 660 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 661 */
/* .local v0, "now":J */
v2 = this.mHandler;
int v3 = 3; // const/4 v3, 0x3
(( android.os.Handler ) v2 ).removeMessages ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V
/* .line 662 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).obtainMessage ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 663 */
/* .local v2, "msg":Landroid/os/Message; */
/* iput p1, v2, Landroid/os/Message;->arg1:I */
/* .line 664 */
java.lang.Long .valueOf ( v0,v1 );
this.obj = v3;
/* .line 665 */
v3 = this.mHandler;
(( android.os.Handler ) v3 ).sendMessage ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 666 */
return;
} // .end method
public void notifyDisplaySwapFinished ( ) {
/* .locals 5 */
/* .line 669 */
v0 = this.mHandler;
int v1 = 6; // const/4 v1, 0x6
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 670 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* .line 671 */
/* .local v2, "now":J */
v0 = this.mHandler;
java.lang.Long .valueOf ( v2,v3 );
(( android.os.Handler ) v0 ).obtainMessage ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 672 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 673 */
return;
} // .end method
public void notifyFocusedWindowChanged ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "focusedPackageName" # Ljava/lang/String; */
/* .line 676 */
v0 = this.mHandler;
/* const/16 v1, 0x8 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 677 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 678 */
/* .local v0, "msg":Landroid/os/Message; */
this.obj = p1;
/* .line 679 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 680 */
return;
} // .end method
public void onEarlyInteractivityChange ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "interactive" # Z */
/* .line 555 */
v0 = this.mHandler;
int v1 = 5; // const/4 v1, 0x5
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 556 */
v0 = this.mHandler;
java.lang.Boolean .valueOf ( p1 );
(( android.os.Handler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 557 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 558 */
return;
} // .end method
public void oneTrackFoldState ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "folded" # Z */
/* .line 244 */
/* sget-boolean v0, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 245 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "oneTrackFoldState: folded: "; // const-string v1, "oneTrackFoldState: folded: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "OneTrackFoldStateHelper"; // const-string v1, "OneTrackFoldStateHelper"
android.util.Slog .d ( v1,v0 );
/* .line 247 */
} // :cond_0
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 248 */
v0 = this.mHandler;
java.lang.Boolean .valueOf ( p1 );
(( android.os.Handler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 249 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 250 */
return;
} // .end method
