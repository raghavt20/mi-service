class com.android.server.display.statistics.BrightnessDataProcessor$BrightnessChangeHandler extends android.os.Handler {
	 /* .source "BrightnessDataProcessor.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/statistics/BrightnessDataProcessor; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BrightnessChangeHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.display.statistics.BrightnessDataProcessor this$0; //synthetic
/* # direct methods */
public com.android.server.display.statistics.BrightnessDataProcessor$BrightnessChangeHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 1118 */
this.this$0 = p1;
/* .line 1119 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 1120 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 5 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1124 */
/* iget v0, p1, Landroid/os/Message;->what:I */
int v1 = 0; // const/4 v1, 0x0
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 1174 */
/* :pswitch_0 */
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fputmIsNotAonSuppressDarken ( v0,v1 );
/* .line 1175 */
/* goto/16 :goto_0 */
/* .line 1171 */
/* :pswitch_1 */
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fputmIsAonSuppressDarken ( v0,v1 );
/* .line 1172 */
/* goto/16 :goto_0 */
/* .line 1167 */
/* :pswitch_2 */
v0 = this.obj;
/* check-cast v0, Lcom/android/server/display/statistics/BrightnessEvent; */
/* .line 1168 */
/* .local v0, "event":Lcom/android/server/display/statistics/BrightnessEvent; */
v1 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$mreportEventToServer ( v1,v0 );
/* .line 1169 */
/* goto/16 :goto_0 */
/* .line 1164 */
} // .end local v0 # "event":Lcom/android/server/display/statistics/BrightnessEvent;
/* :pswitch_3 */
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$mresetBrightnessAnimInfo ( v0 );
/* .line 1165 */
/* goto/16 :goto_0 */
/* .line 1161 */
/* :pswitch_4 */
v0 = this.this$0;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fputmTemporaryBrightnessTimeStamp ( v0,v1,v2 );
/* .line 1162 */
/* goto/16 :goto_0 */
/* .line 1158 */
/* :pswitch_5 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
v2 = this.obj;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$mupdateInterruptBrightnessAnimDuration ( v0,v1,v2 );
/* .line 1159 */
/* goto/16 :goto_0 */
/* .line 1155 */
/* :pswitch_6 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fputmTargetBrightnessAnimValue ( v0,v1 );
/* .line 1156 */
/* goto/16 :goto_0 */
/* .line 1151 */
/* :pswitch_7 */
v0 = this.obj;
/* check-cast v0, Lcom/android/internal/os/SomeArgs; */
/* .line 1152 */
/* .local v0, "someArgs":Lcom/android/internal/os/SomeArgs; */
v1 = this.this$0;
v2 = this.arg1;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
v3 = this.arg2;
/* check-cast v3, Ljava/lang/Float; */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
v4 = this.arg3;
/* check-cast v4, Ljava/lang/Boolean; */
v4 = (( java.lang.Boolean ) v4 ).booleanValue ( ); // invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$mupdateBrightnessAnimInfo ( v1,v2,v3,v4 );
/* .line 1153 */
/* .line 1147 */
} // .end local v0 # "someArgs":Lcom/android/internal/os/SomeArgs;
/* :pswitch_8 */
v0 = this.obj;
/* check-cast v0, Lcom/android/internal/os/SomeArgs; */
/* .line 1148 */
/* .local v0, "args":Lcom/android/internal/os/SomeArgs; */
v1 = this.this$0;
v2 = this.arg1;
/* check-cast v2, Ljava/lang/Boolean; */
v2 = (( java.lang.Boolean ) v2 ).booleanValue ( ); // invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
v3 = this.arg2;
/* check-cast v3, Ljava/lang/Float; */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$mupdateBrightnessStatisticsData ( v1,v2,v3 );
/* .line 1149 */
/* .line 1144 */
} // .end local v0 # "args":Lcom/android/internal/os/SomeArgs;
/* :pswitch_9 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fputmOrientation ( v0,v1 );
/* .line 1145 */
/* .line 1140 */
/* :pswitch_a */
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$mupdateForegroundApps ( v0 );
/* .line 1141 */
/* .line 1137 */
/* :pswitch_b */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$mupdateScreenStateChanged ( v0,v1 );
/* .line 1138 */
/* .line 1132 */
/* :pswitch_c */
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$mreadyToReportEvent ( v0 );
/* .line 1133 */
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$mbrightnessChangedTriggerAggregation ( v0 );
/* .line 1134 */
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$mresetPendingParams ( v0 );
/* .line 1135 */
/* .line 1129 */
/* :pswitch_d */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
/* iget v2, p1, Landroid/os/Message;->arg1:I */
/* iget v3, p1, Landroid/os/Message;->arg2:I */
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$mupdatePointerEventMotionState ( v0,v1,v2,v3 );
/* .line 1130 */
/* .line 1126 */
/* :pswitch_e */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem; */
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$mhandleBrightnessChangeEvent ( v0,v1 );
/* .line 1127 */
/* nop */
/* .line 1179 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
