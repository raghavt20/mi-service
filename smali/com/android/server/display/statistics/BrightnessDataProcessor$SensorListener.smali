.class final Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;
.super Ljava/lang/Object;
.source "BrightnessDataProcessor.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/statistics/BrightnessDataProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SensorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;


# direct methods
.method private constructor <init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V
    .locals 0

    .line 2136
    iput-object p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .line 2158
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 2140
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 2148
    :sswitch_0
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mhandleLightFovSensor(Lcom/android/server/display/statistics/BrightnessDataProcessor;Landroid/hardware/SensorEvent;)V

    .line 2149
    goto :goto_0

    .line 2142
    :sswitch_1
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mhandleLightSensor(Lcom/android/server/display/statistics/BrightnessDataProcessor;Landroid/hardware/SensorEvent;)V

    .line 2143
    goto :goto_0

    .line 2145
    :sswitch_2
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;->this$0:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-static {v0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->-$$Nest$mhandleAccSensor(Lcom/android/server/display/statistics/BrightnessDataProcessor;Landroid/hardware/SensorEvent;)V

    .line 2146
    nop

    .line 2153
    :goto_0
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x5 -> :sswitch_1
        0x1fa2a8f -> :sswitch_0
    .end sparse-switch
.end method
