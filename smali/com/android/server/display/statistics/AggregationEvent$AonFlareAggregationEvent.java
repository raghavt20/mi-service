public class com.android.server.display.statistics.AggregationEvent$AonFlareAggregationEvent extends com.android.server.display.statistics.AggregationEvent {
	 /* .source "AggregationEvent.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/statistics/AggregationEvent; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "AonFlareAggregationEvent" */
} // .end annotation
/* # static fields */
public static final java.lang.String EVENT_FLARE_MANUAL_ADJUST_TIMES;
public static final java.lang.String EVENT_FLARE_SCENE_CHECK_TIMES;
public static final java.lang.String EVENT_FLARE_SUPPRESS_DARKEN_HOUR;
public static final java.lang.String EVENT_FLARE_SUPPRESS_DARKEN_LUX_SPAN;
public static final java.lang.String EVENT_FLARE_USER_RESET_BRIGHTNESS_MODE_TIMES;
public static final java.lang.String KEY_FLARE_NOT_SUPPRESS_DARKEN_MANUAL_ADJUST_DECREASE_TIMES;
public static final java.lang.String KEY_FLARE_NOT_SUPPRESS_DARKEN_MANUAL_ADJUST_INCREASE_TIMES;
public static final java.lang.String KEY_FLARE_NOT_SUPPRESS_DARKEN_USER_RESET_BRIGHTNESS_MODE_TIMES;
public static final java.lang.String KEY_FLARE_SUPPRESS_DARKEN_MANUAL_ADJUST_DECREASE_TIMES;
public static final java.lang.String KEY_FLARE_SUPPRESS_DARKEN_MANUAL_ADJUST_INCREASE_TIMES;
public static final java.lang.String KEY_FLARE_SUPPRESS_DARKEN_USER_RESET_BRIGHTNESS_MODE_TIMES;
public static final java.lang.String KEY_IN_FLARE_SCENE_TIMES;
public static final java.lang.String KEY_NOT_IN_FLARE_SCENE_TIMES;
public static final java.lang.String KEY_NOT_REPORT_IN_TIME_TIMES;
/* # instance fields */
private java.util.Map mAonFlareQuotaEvents;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.display.statistics.AggregationEvent$AonFlareAggregationEvent ( ) {
/* .locals 1 */
/* .line 274 */
/* invoke-direct {p0}, Lcom/android/server/display/statistics/AggregationEvent;-><init>()V */
/* .line 303 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mAonFlareQuotaEvents = v0;
return;
} // .end method
/* # virtual methods */
public java.util.Map getCacheDataMap ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Float;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 317 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.util.Map getQuotaEvents ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 312 */
v0 = this.mAonFlareQuotaEvents;
} // .end method
public java.lang.String toString ( ) {
/* .locals 1 */
/* .line 307 */
v0 = this.mAonFlareQuotaEvents;
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
