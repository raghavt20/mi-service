public class com.android.server.display.statistics.OneTrackUploaderHelper {
	 /* .source "OneTrackUploaderHelper.java" */
	 /* # static fields */
	 public static final java.lang.String ADVANCED_BRIGHTNESS_AGGREGATION_EVENT_NAME;
	 public static final java.lang.String ADVANCED_EVENT_NAME;
	 public static final java.lang.String AON_FLARE_AGGREGATION_EVENT_NAME;
	 private static final java.lang.String BRIGHTNESS_EVENT_APP_ID;
	 public static final java.lang.String BRIGHTNESS_EVENT_NAME;
	 public static final java.lang.String BRIGHTNESS_QUOTA_AGGREGATION_EVENT_NAME;
	 public static final java.lang.String CUSTOM_BRIGHTNESS_AGGREGATION_EVENT_NAME;
	 private static final java.lang.String DEVICE_REGION;
	 private static final Integer FLAG_NON_ANONYMOUS;
	 private static final Integer FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN;
	 public static final Boolean IS_INTERNATIONAL_BUILD;
	 private static final java.lang.String KEY_ACC_VALUES;
	 private static final java.lang.String KEY_ACTUAL_NIT;
	 private static final java.lang.String KEY_AFFECT_FACTOR_FLAG;
	 private static final java.lang.String KEY_ALL_STATS_ENTRIES;
	 private static final java.lang.String KEY_AMBIENT_LUX;
	 private static final java.lang.String KEY_AMBIENT_LUX_SPAN;
	 private static final java.lang.String KEY_ASSIST_AMBIENT_LUX;
	 private static final java.lang.String KEY_AUTO_BRIGHTNESS_ANIMATION_DURATION;
	 private static final java.lang.String KEY_BRIGHTNESS_CHANGED_STATUS;
	 private static final java.lang.String KEY_BRIGHTNESS_RESTRICTED_ENABLE;
	 private static final java.lang.String KEY_CURRENT_ANIMATE_VALUE;
	 private static final java.lang.String KEY_CURRENT_USER_ID;
	 private static final java.lang.String KEY_DEFAULT_SPLINE_ERROR;
	 private static final java.lang.String KEY_DISPLAY_GRAY_SCALE;
	 private static final java.lang.String KEY_EVENT_TYPE;
	 private static final java.lang.String KEY_EXP_ID;
	 private static final java.lang.String KEY_EXTRA;
	 private static final java.lang.String KEY_HDR_LAYER_ENABLE;
	 private static final java.lang.String KEY_IS_DEFAULT_CONFIG;
	 private static final java.lang.String KEY_IS_USE_LIGHT_FOV_OPTIMIZATION;
	 private static final java.lang.String KEY_LAST_ASSIST_AMBIENT_LUX;
	 private static final java.lang.String KEY_LAST_MAIN_AMBIENT_LUX;
	 private static final java.lang.String KEY_LONG_TERM_MODEL_SPLINE_ERROR;
	 private static final java.lang.String KEY_LOW_POWER_MODE_FLAG;
	 private static final java.lang.String KEY_MAIN_AMBIENT_LUX;
	 private static final java.lang.String KEY_ORIENTATION;
	 private static final java.lang.String KEY_ORIGINAL_NIT;
	 private static final java.lang.String KEY_PREVIOUS_BRIGHTNESS;
	 private static final java.lang.String KEY_PREVIOUS_BRIGHTNESS_SPAN;
	 private static final java.lang.String KEY_SCREEN_BRIGHTNESS;
	 private static final java.lang.String KEY_SCREEN_BRIGHTNESS_SPAN;
	 private static final java.lang.String KEY_TARGET_ANIMATE_VALUE;
	 private static final java.lang.String KEY_TIME_STAMP;
	 private static final java.lang.String KEY_TOP_PACKAGE;
	 private static final java.lang.String KEY_USER_BRIGHTNESS;
	 private static final java.lang.String KEY_USER_DATA_POINT;
	 private static final java.lang.String ONE_TRACK_ACTION;
	 public static final java.lang.String THERMAL_AGGREGATION_EVENT_NAME;
	 /* # direct methods */
	 static com.android.server.display.statistics.OneTrackUploaderHelper ( ) {
		 /* .locals 2 */
		 /* .line 65 */
		 final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
		 final String v1 = "CN"; // const-string v1, "CN"
		 android.os.SystemProperties .get ( v0,v1 );
		 /* .line 68 */
		 /* nop */
		 /* .line 69 */
		 final String v0 = "ro.product.mod_device"; // const-string v0, "ro.product.mod_device"
		 final String v1 = ""; // const-string v1, ""
		 android.os.SystemProperties .get ( v0,v1 );
		 final String v1 = "_global"; // const-string v1, "_global"
		 v0 = 		 (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
		 com.android.server.display.statistics.OneTrackUploaderHelper.IS_INTERNATIONAL_BUILD = (v0!= 0);
		 /* .line 68 */
		 return;
	 } // .end method
	 public com.android.server.display.statistics.OneTrackUploaderHelper ( ) {
		 /* .locals 0 */
		 /* .line 14 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static android.content.Intent getBrightnessEventIntent ( android.content.Context p0 ) {
		 /* .locals 4 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .line 226 */
		 /* new-instance v0, Landroid/content/Intent; */
		 final String v1 = "onetrack.action.TRACK_EVENT"; // const-string v1, "onetrack.action.TRACK_EVENT"
		 /* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
		 /* .line 227 */
		 /* .local v0, "intent":Landroid/content/Intent; */
		 final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
		 (( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 228 */
		 (( android.content.Context ) p0 ).getPackageName ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
		 final String v3 = "PACKAGE"; // const-string v3, "PACKAGE"
		 (( android.content.Intent ) v1 ).putExtra ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 229 */
		 final String v2 = "APP_ID"; // const-string v2, "APP_ID"
		 final String v3 = "31000000084"; // const-string v3, "31000000084"
		 (( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 230 */
	 } // .end method
	 private static java.lang.String getEventName ( java.lang.Integer p0 ) {
		 /* .locals 2 */
		 /* .param p0, "key" # Ljava/lang/Integer; */
		 /* .line 197 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 198 */
		 /* .local v0, "name":Ljava/lang/String; */
		 v1 = 		 (( java.lang.Integer ) p0 ).intValue ( ); // invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I
		 /* packed-switch v1, :pswitch_data_0 */
		 /* .line 212 */
		 /* :pswitch_0 */
		 /* const-string/jumbo v0, "sunlight_adj_times" */
		 /* .line 213 */
		 /* .line 209 */
		 /* :pswitch_1 */
		 /* const-string/jumbo v0, "window_adj_times" */
		 /* .line 210 */
		 /* .line 206 */
		 /* :pswitch_2 */
		 final String v0 = "auto_manual_adj_times"; // const-string v0, "auto_manual_adj_times"
		 /* .line 207 */
		 /* .line 203 */
		 /* :pswitch_3 */
		 final String v0 = "auto_adj_times"; // const-string v0, "auto_adj_times"
		 /* .line 204 */
		 /* .line 200 */
		 /* :pswitch_4 */
		 final String v0 = "manual_adj_times"; // const-string v0, "manual_adj_times"
		 /* .line 201 */
		 /* nop */
		 /* .line 217 */
	 } // :goto_0
	 /* nop */
	 /* :pswitch_data_0 */
	 /* .packed-switch 0x0 */
	 /* :pswitch_4 */
	 /* :pswitch_3 */
	 /* :pswitch_2 */
	 /* :pswitch_1 */
	 /* :pswitch_0 */
} // .end packed-switch
} // .end method
static void lambda$reportAggregatedEventsToServer$0 ( android.content.Intent p0, java.lang.String p1, com.android.server.display.statistics.AggregationEvent p2 ) { //synthethic
/* .locals 1 */
/* .param p0, "intent" # Landroid/content/Intent; */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 86 */
(( com.android.server.display.statistics.AggregationEvent ) p2 ).toString ( ); // invoke-virtual {p2}, Lcom/android/server/display/statistics/AggregationEvent;->toString()Ljava/lang/String;
(( android.content.Intent ) p0 ).putExtra ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 87 */
return;
} // .end method
static void lambda$reportAggregatedEventsToServer$1 ( android.content.Intent p0, java.lang.Object p1, java.lang.Object p2 ) { //synthethic
/* .locals 2 */
/* .param p0, "intent" # Landroid/content/Intent; */
/* .param p1, "key" # Ljava/lang/Object; */
/* .param p2, "value" # Ljava/lang/Object; */
/* .line 93 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 if ( p2 != null) { // if-eqz p2, :cond_0
		 /* .line 94 */
		 /* move-object v0, p1 */
		 /* check-cast v0, Ljava/lang/Integer; */
		 com.android.server.display.statistics.OneTrackUploaderHelper .getEventName ( v0 );
		 /* move-object v1, p2 */
		 /* check-cast v1, Ljava/lang/Integer; */
		 (( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;
		 /* .line 96 */
	 } // :cond_0
	 return;
} // .end method
public static void reportAggregatedEventsToServer ( android.content.Context p0, java.util.Map p1, java.lang.String p2, Integer p3 ) {
	 /* .locals 4 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .param p2, "eventName" # Ljava/lang/String; */
	 /* .param p3, "expId" # I */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Landroid/content/Context;", */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Lcom/android/server/display/statistics/AggregationEvent;", */
	 /* ">;", */
	 /* "Ljava/lang/String;", */
	 /* "I)V" */
	 /* } */
} // .end annotation
/* .line 79 */
/* .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/display/statistics/AggregationEvent;>;" */
v0 = com.android.server.display.statistics.OneTrackUploaderHelper.DEVICE_REGION;
final String v1 = "IN"; // const-string v1, "IN"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
v0 = /* if-nez v0, :cond_2 */
/* if-nez v0, :cond_0 */
/* .line 82 */
} // :cond_0
com.android.server.display.statistics.OneTrackUploaderHelper .getBrightnessEventIntent ( p0 );
/* .line 83 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 84 */
final String v1 = "exp_id"; // const-string v1, "exp_id"
(( android.content.Intent ) v0 ).putExtra ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 85 */
/* new-instance v1, Lcom/android/server/display/statistics/OneTrackUploaderHelper$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, v0}, Lcom/android/server/display/statistics/OneTrackUploaderHelper$$ExternalSyntheticLambda0;-><init>(Landroid/content/Intent;)V */
/* .line 88 */
final String v1 = "brightness_quota_aggregation"; // const-string v1, "brightness_quota_aggregation"
v1 = (( java.lang.String ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 89 */
final String v1 = "brightness_adj_times"; // const-string v1, "brightness_adj_times"
/* check-cast v1, Lcom/android/server/display/statistics/AggregationEvent; */
/* .line 90 */
/* .local v1, "event":Lcom/android/server/display/statistics/AggregationEvent; */
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 91 */
	 (( com.android.server.display.statistics.AggregationEvent ) v1 ).getQuotaEvents ( ); // invoke-virtual {v1}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;
	 /* .line 92 */
	 /* .local v2, "quotaEvents":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;" */
	 /* new-instance v3, Lcom/android/server/display/statistics/OneTrackUploaderHelper$$ExternalSyntheticLambda1; */
	 /* invoke-direct {v3, v0}, Lcom/android/server/display/statistics/OneTrackUploaderHelper$$ExternalSyntheticLambda1;-><init>(Landroid/content/Intent;)V */
	 /* .line 99 */
} // .end local v1 # "event":Lcom/android/server/display/statistics/AggregationEvent;
} // .end local v2 # "quotaEvents":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;"
} // :cond_1
com.android.server.display.statistics.OneTrackUploaderHelper .startService ( p0,v0 );
/* .line 100 */
return;
/* .line 80 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_2
} // :goto_0
return;
} // .end method
public static void reportToOneTrack ( android.content.Context p0, com.android.server.display.statistics.AdvancedEvent p1 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "event" # Lcom/android/server/display/statistics/AdvancedEvent; */
/* .line 112 */
v0 = com.android.server.display.statistics.OneTrackUploaderHelper.DEVICE_REGION;
final String v1 = "IN"; // const-string v1, "IN"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 113 */
return;
/* .line 115 */
} // :cond_0
com.android.server.display.statistics.OneTrackUploaderHelper .getBrightnessEventIntent ( p0 );
/* .line 116 */
/* .local v0, "intent":Landroid/content/Intent; */
com.android.server.display.statistics.OneTrackUploaderHelper .updateAdvancedEventIntent ( v0,p1 );
/* .line 117 */
com.android.server.display.statistics.OneTrackUploaderHelper .startService ( p0,v0 );
/* .line 118 */
return;
} // .end method
public static void reportToOneTrack ( android.content.Context p0, com.android.server.display.statistics.BrightnessEvent p1 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "event" # Lcom/android/server/display/statistics/BrightnessEvent; */
/* .line 103 */
v0 = com.android.server.display.statistics.OneTrackUploaderHelper.DEVICE_REGION;
final String v1 = "IN"; // const-string v1, "IN"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 104 */
return;
/* .line 106 */
} // :cond_0
com.android.server.display.statistics.OneTrackUploaderHelper .getBrightnessEventIntent ( p0 );
/* .line 107 */
/* .local v0, "intent":Landroid/content/Intent; */
com.android.server.display.statistics.OneTrackUploaderHelper .updateBrightnessEventIntent ( v0,p1 );
/* .line 108 */
com.android.server.display.statistics.OneTrackUploaderHelper .startService ( p0,v0 );
/* .line 109 */
return;
} // .end method
private static void startService ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 127 */
/* sget-boolean v0, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
/* .line 128 */
int v0 = 3; // const/4 v0, 0x3
(( android.content.Intent ) p1 ).setFlags ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 131 */
} // :cond_0
try { // :try_start_0
v0 = android.os.UserHandle.CURRENT;
(( android.content.Context ) p0 ).startServiceAsUser ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 135 */
/* .line 132 */
/* :catch_0 */
/* move-exception v0 */
/* .line 133 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to upload brightness event! "; // const-string v2, "Failed to upload brightness event! "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 134 */
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 133 */
final String v2 = "BrightnessDataProcessor"; // const-string v2, "BrightnessDataProcessor"
android.util.Slog .w ( v2,v1 );
/* .line 136 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public static void updateAdvancedEventIntent ( android.content.Intent p0, com.android.server.display.statistics.AdvancedEvent p1 ) {
/* .locals 4 */
/* .param p0, "intent" # Landroid/content/Intent; */
/* .param p1, "event" # Lcom/android/server/display/statistics/AdvancedEvent; */
/* .line 182 */
final String v0 = "EVENT_NAME"; // const-string v0, "EVENT_NAME"
final String v1 = "advanced_brightness"; // const-string v1, "advanced_brightness"
(( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 183 */
v1 = (( com.android.server.display.statistics.AdvancedEvent ) p1 ).getEventType ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getEventType()I
/* const-string/jumbo v2, "type" */
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 184 */
v1 = (( com.android.server.display.statistics.AdvancedEvent ) p1 ).getAutoBrightnessAnimationDuration ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getAutoBrightnessAnimationDuration()F
final String v2 = "auto_brightness_animation_duration"; // const-string v2, "auto_brightness_animation_duration"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 185 */
v1 = (( com.android.server.display.statistics.AdvancedEvent ) p1 ).getCurrentAnimateValue ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getCurrentAnimateValue()I
final String v2 = "current_animate_value"; // const-string v2, "current_animate_value"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 186 */
v1 = (( com.android.server.display.statistics.AdvancedEvent ) p1 ).getTargetAnimateValue ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getTargetAnimateValue()I
/* const-string/jumbo v2, "target_animate_value" */
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 187 */
v1 = (( com.android.server.display.statistics.AdvancedEvent ) p1 ).getUserBrightness ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getUserBrightness()I
/* const-string/jumbo v2, "user_brightness" */
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 188 */
v1 = (( com.android.server.display.statistics.AdvancedEvent ) p1 ).getLongTermModelSplineError ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getLongTermModelSplineError()F
final String v2 = "long_term_model_spline_error"; // const-string v2, "long_term_model_spline_error"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 189 */
v1 = (( com.android.server.display.statistics.AdvancedEvent ) p1 ).getDefaultSplineError ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getDefaultSplineError()F
final String v2 = "default_spline_error"; // const-string v2, "default_spline_error"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 190 */
v1 = (( com.android.server.display.statistics.AdvancedEvent ) p1 ).getBrightnessChangedState ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getBrightnessChangedState()I
final String v2 = "brightness_changed_state"; // const-string v2, "brightness_changed_state"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 191 */
(( com.android.server.display.statistics.AdvancedEvent ) p1 ).getExtra ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getExtra()Ljava/lang/String;
final String v2 = "extra"; // const-string v2, "extra"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 192 */
(( com.android.server.display.statistics.AdvancedEvent ) p1 ).getTimeStamp ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getTimeStamp()J
/* move-result-wide v1 */
/* const-string/jumbo v3, "time_stamp" */
(( android.content.Intent ) v0 ).putExtra ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 193 */
v1 = (( com.android.server.display.statistics.AdvancedEvent ) p1 ).getBrightnessRestrictedEnable ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->getBrightnessRestrictedEnable()Z
final String v2 = "brightness_restricted_enable"; // const-string v2, "brightness_restricted_enable"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 194 */
return;
} // .end method
public static void updateBrightnessEventIntent ( android.content.Intent p0, com.android.server.display.statistics.BrightnessEvent p1 ) {
/* .locals 4 */
/* .param p0, "intent" # Landroid/content/Intent; */
/* .param p1, "event" # Lcom/android/server/display/statistics/BrightnessEvent; */
/* .line 144 */
final String v0 = "EVENT_NAME"; // const-string v0, "EVENT_NAME"
final String v1 = "brightness"; // const-string v1, "brightness"
(( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 145 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getEventType ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getEventType()I
/* const-string/jumbo v2, "type" */
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 146 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getAmbientLux ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getAmbientLux()F
final String v2 = "ambient_lux"; // const-string v2, "ambient_lux"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 147 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getScreenBrightness ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getScreenBrightness()F
final String v2 = "screen_brightness"; // const-string v2, "screen_brightness"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 148 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getOrientation ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getOrientation()I
final String v2 = "orientation"; // const-string v2, "orientation"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 149 */
(( com.android.server.display.statistics.BrightnessEvent ) p1 ).getForegroundPackage ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getForegroundPackage()Ljava/lang/String;
/* const-string/jumbo v2, "top_package" */
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 150 */
(( com.android.server.display.statistics.BrightnessEvent ) p1 ).getTimeStamp ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getTimeStamp()J
/* move-result-wide v1 */
/* const-string/jumbo v3, "time_stamp" */
(( android.content.Intent ) v0 ).putExtra ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 151 */
(( com.android.server.display.statistics.BrightnessEvent ) p1 ).getExtra ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getExtra()Ljava/lang/String;
final String v2 = "extra"; // const-string v2, "extra"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 152 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getPreviousBrightness ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getPreviousBrightness()F
final String v2 = "previous_brightness"; // const-string v2, "previous_brightness"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 153 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).isDefaultConfig ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->isDefaultConfig()Z
final String v2 = "is_default_config"; // const-string v2, "is_default_config"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 154 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getUserDataPoint ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getUserDataPoint()F
/* const-string/jumbo v2, "user_data_point" */
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 155 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getLowPowerModeFlag ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getLowPowerModeFlag()Z
final String v2 = "low_power_mode_flag"; // const-string v2, "low_power_mode_flag"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 156 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getUserId ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getUserId()I
final String v2 = "current_user_id"; // const-string v2, "current_user_id"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 157 */
(( com.android.server.display.statistics.BrightnessEvent ) p1 ).getSwitchStats ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getSwitchStats()Ljava/util/List;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
final String v2 = "all_stats_entries"; // const-string v2, "all_stats_entries"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 158 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getAffectFactorFlag ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getAffectFactorFlag()I
final String v2 = "affect_factor_flag"; // const-string v2, "affect_factor_flag"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 159 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getCurBrightnessSpanIndex ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getCurBrightnessSpanIndex()I
final String v2 = "screen_brightness_span"; // const-string v2, "screen_brightness_span"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 160 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getPreBrightnessSpanIndex ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getPreBrightnessSpanIndex()I
final String v2 = "previous_brightness_span"; // const-string v2, "previous_brightness_span"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 161 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getLuxSpanIndex ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getLuxSpanIndex()I
final String v2 = "ambient_lux_span"; // const-string v2, "ambient_lux_span"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 162 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getDisplayGrayScale ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getDisplayGrayScale()F
final String v2 = "display_gray_scale"; // const-string v2, "display_gray_scale"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 163 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getActualNit ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getActualNit()F
final String v2 = "actual_nit"; // const-string v2, "actual_nit"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 164 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getOriginalNit ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getOriginalNit()F
final String v2 = "original_nit"; // const-string v2, "original_nit"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 165 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getHdrLayerEnable ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getHdrLayerEnable()Z
final String v2 = "hdr_layer_enable"; // const-string v2, "hdr_layer_enable"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 166 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getBrightnessRestrictedEnable ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getBrightnessRestrictedEnable()Z
final String v2 = "brightness_restricted_enable"; // const-string v2, "brightness_restricted_enable"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 167 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getMainAmbientLux ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getMainAmbientLux()F
final String v2 = "main_ambient_lux"; // const-string v2, "main_ambient_lux"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 168 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getAssistAmbientLux ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getAssistAmbientLux()F
final String v2 = "assist_ambient_lux"; // const-string v2, "assist_ambient_lux"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 169 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getLastMainAmbientLux ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getLastMainAmbientLux()F
final String v2 = "last_main_ambient_lux"; // const-string v2, "last_main_ambient_lux"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 170 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getLastAssistAmbientLux ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getLastAssistAmbientLux()F
final String v2 = "last_assist_ambient_lux"; // const-string v2, "last_assist_ambient_lux"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 171 */
(( com.android.server.display.statistics.BrightnessEvent ) p1 ).getAccValues ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getAccValues()[F
final String v2 = "acc_values"; // const-string v2, "acc_values"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[F)Landroid/content/Intent;
/* .line 172 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getIsUseLightFovOptimization ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getIsUseLightFovOptimization()Z
final String v2 = "is_use_light_fov_optimization"; // const-string v2, "is_use_light_fov_optimization"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 173 */
v1 = (( com.android.server.display.statistics.BrightnessEvent ) p1 ).getExpId ( ); // invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->getExpId()I
final String v2 = "exp_id"; // const-string v2, "exp_id"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 174 */
return;
} // .end method
