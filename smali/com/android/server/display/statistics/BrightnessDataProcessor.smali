.class public Lcom/android/server/display/statistics/BrightnessDataProcessor;
.super Ljava/lang/Object;
.source "BrightnessDataProcessor.java"

# interfaces
.implements Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;
.implements Lcom/android/server/display/ThermalBrightnessController$ThermalListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;,
        Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;,
        Lcom/android/server/display/statistics/BrightnessDataProcessor$TaskStackListenerImpl;,
        Lcom/android/server/display/statistics/BrightnessDataProcessor$RotationWatcher;,
        Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;,
        Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;,
        Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;,
        Lcom/android/server/display/statistics/BrightnessDataProcessor$ModelEventCallback;,
        Lcom/android/server/display/statistics/BrightnessDataProcessor$ScreenStateReceiver;
    }
.end annotation


# static fields
.field private static final AON_FLARE_EFFECTIVE_TIME:J = 0xea60L

.field private static final DEBOUNCE_REPORT_BRIGHTNESS:I = 0xbb8

.field private static final DEBOUNCE_REPORT_DISABLE_AUTO_BRIGHTNESS_EVENT:J = 0x493e0L

.field private static final DEBUG:Z

.field private static final DEBUG_REPORT_TIME_DURATION:J = 0x1d4c0L

.field private static final DISABLE_SECURITY_BY_MI_SHOW:Ljava/lang/String; = "disable_security_by_mishow"

.field private static final FACTOR_DARK_MODE:I = 0x4

.field private static final FACTOR_DC_MODE:I = 0x2

.field private static final FACTOR_READING_MODE:I = 0x1

.field private static final LUX_SPLIT_HIGH:I = 0xdac

.field private static final LUX_SPLIT_LOW:I = 0x1e

.field private static final LUX_SPLIT_MEDIUM:I = 0x384

.field private static final LUX_SPLIT_SUPREME_FIVE:I = 0x186a0

.field private static final LUX_SPLIT_SUPREME_FOUR:I = 0xfde8

.field private static final LUX_SPLIT_SUPREME_ONE:I = 0x2710

.field private static final LUX_SPLIT_SUPREME_THREE:I = 0x88b8

.field private static final LUX_SPLIT_SUPREME_TWO:I = 0x3a98

.field private static final MAX_NIT_SPAN:I = 0x2a

.field private static final MAX_SPAN_INDEX:I = 0x2c

.field private static final MSG_BRIGHTNESS_REPORT_DEBOUNCE:I = 0x3

.field private static final MSG_HANDLE_EVENT_CHANGED:I = 0x1

.field private static final MSG_REPORT_DISABLE_AUTO_BRIGHTNESS_EVENT:I = 0xd

.field private static final MSG_RESET_BRIGHTNESS_ANIMATION_INFO:I = 0xc

.field private static final MSG_RESET_FLARE_NOT_SUPPRESS_DARKEN_STATUS:I = 0xf

.field private static final MSG_RESET_FLARE_SUPPRESS_DARKEN_STATUS:I = 0xe

.field private static final MSG_ROTATION_CHANGED:I = 0x6

.field private static final MSG_UPDATE_BRIGHTNESS_ANIMATION_DURATION:I = 0xa

.field private static final MSG_UPDATE_BRIGHTNESS_ANIMATION_INFO:I = 0x8

.field private static final MSG_UPDATE_BRIGHTNESS_ANIMATION_TARGET:I = 0x9

.field private static final MSG_UPDATE_BRIGHTNESS_STATISTICS_DATA:I = 0x7

.field private static final MSG_UPDATE_FOREGROUND_APP:I = 0x5

.field private static final MSG_UPDATE_MOTION_EVENT:I = 0x2

.field private static final MSG_UPDATE_SCREEN_STATE:I = 0x4

.field private static final MSG_UPDATE_TEMPORARY_BRIGHTNESS_TIME_STAMP:I = 0xb

.field private static final REASON_UPDATE_BRIGHTNESS_USAGE_REPORT:Ljava/lang/String; = "report"

.field private static final REASON_UPDATE_BRIGHTNESS_USAGE_SCREEN_OFF:Ljava/lang/String; = "screen off"

.field private static final REASON_UPDATE_BRIGHTNESS_USAGE_SPAN_CHANGE:Ljava/lang/String; = "brightness span change"

.field private static final SCREEN_NIT_SPAN_FIVE:F = 500.0f

.field private static final SCREEN_NIT_SPAN_FOUR:F = 80.0f

.field private static final SCREEN_NIT_SPAN_ONE:F = 3.5f

.field private static final SCREEN_NIT_SPAN_SIX:F = 1000.0f

.field private static final SCREEN_NIT_SPAN_THREE:F = 50.0f

.field private static final SCREEN_NIT_SPAN_TWO:F = 8.0f

.field private static final SENSOR_TYPE_LIGHT_FOV:I = 0x1fa2a8f

.field private static final SPAN_LUX_STEP_HIGH:I = 0xc8

.field private static final SPAN_LUX_STEP_LOW:I = 0x5

.field private static final SPAN_LUX_STEP_MEDIUM:I = 0x64

.field private static final SPAN_LUX_STEP_SUPREME:I = 0x1f4

.field private static final SPAN_SCREEN_NIT_STEP_FOUR:F = 50.0f

.field private static final SPAN_SCREEN_NIT_STEP_ONE:F = 7.0f

.field private static final SPAN_SCREEN_NIT_STEP_THREE:F = 20.0f

.field private static final SPAN_SCREEN_NIT_STEP_TWO:F = 10.0f

.field protected static final TAG:Ljava/lang/String; = "BrightnessDataProcessor"

.field private static final THERMAL_STATUS_INVALID_THERMAL_UNRESTRICTED_BRIGHTNESS:I = 0x1

.field private static final THERMAL_STATUS_VALID_THERMAL_RESTRICTED_BRIGHTNESS:I = 0x3

.field private static final THERMAL_STATUS_VALID_THERMAL_UNRESTRICTED_BRIGHTNESS:I = 0x2

.field public static final TYPE_IN_FLARE_SCENE:I = 0x1

.field public static final TYPE_NOT_IN_FLARE_SCENE:I = 0x2

.field public static final TYPE_NOT_REPORT_IN_TIME:I = 0x3


# instance fields
.field private mAccSensor:Landroid/hardware/Sensor;

.field private mAccSensorEnabled:Z

.field private final mActivityTaskManager:Landroid/app/IActivityTaskManager;

.field private mActualNit:F

.field private mAdvancedBrightnessEventsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/display/statistics/AggregationEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mAlarmManager:Landroid/app/AlarmManager;

.field private mAonFlareEventsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/display/statistics/AggregationEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mAppClassifier:Lcom/android/server/display/aiautobrt/AppClassifier;

.field private mAutoBrightnessDuration:F

.field private mAutoBrightnessEnable:Z

.field private mAutoBrightnessIntegral:F

.field private mAutoBrightnessModeChanged:Z

.field private final mBackgroundHandler:Landroid/os/Handler;

.field private mBrighteningSceneSpline:Landroid/util/Spline;

.field private mBrightnessAnimStart:Z

.field private mBrightnessAnimStartTime:J

.field private mBrightnessChangedState:I

.field private mBrightnessEventsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/display/statistics/AggregationEvent;",
            ">;"
        }
    .end annotation
.end field

.field private mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

.field private final mBrightnessMin:F

.field private mCbmEventsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/display/statistics/AggregationEvent;",
            ">;"
        }
    .end annotation
.end field

.field private mCloudControllerListener:Lcom/android/server/display/AutomaticBrightnessControllerImpl$CloudControllerListener;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private mCurrentBrightnessAnimValue:F

.field private mCurrentUserId:I

.field private mDarkeningSceneSpline:Landroid/util/Spline;

.field private mDefaultSceneSpline:Landroid/util/Spline;

.field private mDefaultSplineError:F

.field private final mDetailThermalRestrictedUsage:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/util/SparseArray<",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/util/SparseArray<",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation
.end field

.field private mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

.field private mExpId:I

.field private mForcedReportTrainDataEnabled:Z

.field private mForegroundAppPackageName:Ljava/lang/String;

.field private final mForegroundWindowListener:Lmiui/process/IForegroundWindowListener;

.field private mGrayScale:F

.field private mHaveValidMotionForWindowBrightness:Z

.field private mHaveValidWindowBrightness:Z

.field private final mHbmMinLux:F

.field private mHdrAppPackageName:Ljava/lang/String;

.field public final mIndividualEventNormalizer:Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;

.field private mIsAonSuppressDarken:Z

.field private mIsHdrLayer:Z

.field private mIsMiShow:Z

.field private mIsNotAonSuppressDarken:Z

.field private mIsTemporaryBrightnessAdjustment:Z

.field private mIsValidResetAutoBrightnessMode:Z

.field private mLastAmbientLux:F

.field private mLastAssistAmbientLux:F

.field private mLastAutoBrightness:F

.field private mLastAutoBrightnessEnable:Z

.field private mLastBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

.field private mLastBrightnessOverrideFromWindow:F

.field private mLastBrightnessRestrictedTimeStamp:F

.field private mLastConditionId:I

.field private mLastDetailThermalUsageTimeStamp:J

.field private mLastHdrEnableTimeStamp:J

.field private mLastMainAmbientLux:F

.field private mLastManualBrightness:F

.field private mLastOutDoorHighTemTimeStamp:J

.field private mLastOutDoorHighTempState:Z

.field private mLastResetBrightnessModeTime:J

.field private mLastRestrictedBrightness:F

.field private mLastScreenBrightness:F

.field private mLastScreenOnTimeStamp:J

.field private mLastStoreBrightness:F

.field private mLastTemperature:F

.field private mLastThermalStatusTimeStamp:J

.field private mLastUnrestrictedBrightness:F

.field private mLatestDraggingChangedTime:J

.field private mLightFovSensor:Landroid/hardware/Sensor;

.field private mLightFovSensorEnabled:Z

.field private mLightSensor:Landroid/hardware/Sensor;

.field private mLightSensorEnabled:Z

.field private mLongTermModelSplineError:F

.field private mManualBrightnessDuration:F

.field private mManualBrightnessIntegral:F

.field private mMinOutDoorHighTempNit:F

.field private mModelEventCallback:Lcom/android/server/display/statistics/BrightnessDataProcessor$ModelEventCallback;

.field private mModelTrainIndicatorsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mNormalBrightnessMax:F

.field private final mOnAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

.field private mOriSpline:Landroid/util/Spline;

.field private mOrientation:I

.field private mOriginalNit:F

.field private mPendingAnimationStart:Z

.field private mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

.field private mPendingBrightnessChangedState:I

.field private mPendingTargetBrightnessAnimValue:F

.field private mPointerEventListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;

.field private mPointerEventListenerEnabled:Z

.field private final mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

.field private final mPowerManager:Landroid/os/PowerManager;

.field private volatile mReportBrightnessEventsEnable:Z

.field private mRotationListenerEnabled:Z

.field private mRotationWatcher:Lcom/android/server/display/statistics/BrightnessDataProcessor$RotationWatcher;

.field private mScreenOn:Z

.field private mSensorListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;

.field private final mSensorManager:Landroid/hardware/SensorManager;

.field private mSettingsObserver:Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;

.field private mStartTimeStamp:J

.field private mSwitchStatsHelper:Lcom/android/server/display/statistics/SwitchStatsHelper;

.field private mTargetBrightnessAnimValue:F

.field private mTaskStackListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$TaskStackListenerImpl;

.field private final mTemperatureSpan:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mTemporaryBrightnessTimeStamp:J

.field private final mThermalBrightnessRestrictedUsage:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/util/SparseArray<",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation
.end field

.field private mThermalEventsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/display/statistics/AggregationEvent;",
            ">;"
        }
    .end annotation
.end field

.field private mThermalStatus:I

.field private mUserDragging:Z

.field private mWindowOverrideBrightnessChanging:Z

.field private final mWms:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method public static synthetic $r8$lambda$5qMT5d1PNKjIZv8OdxG-2nMjYoU(Lcom/android/server/display/statistics/BrightnessDataProcessor;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$updateExpId$2(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$6IecPUjGHBE-Bx5Ck3Z9tIP6DQs(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$startHdyUsageStats$7(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$9tgdtxpUQ4JCbkGhntloDez1X98(Lcom/android/server/display/statistics/BrightnessDataProcessor;IF)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$noteDetailThermalUsage$4(IF)V

    return-void
.end method

.method public static synthetic $r8$lambda$ArOf22yAnGXEP5nt7SDdbucjKug(Lcom/android/server/display/statistics/BrightnessDataProcessor;FFZIF)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$noteFullSceneThermalUsageStats$3(FFZIF)V

    return-void
.end method

.method public static synthetic $r8$lambda$CCallUpQ1PZW0X3Go7tGakSov8o(Lcom/android/server/display/statistics/BrightnessDataProcessor;FF)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$updateScreenNits$8(FF)V

    return-void
.end method

.method public static synthetic $r8$lambda$Ew8O1G3Sf0HBJrqhJGVO1Xt5zB8(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$aggregateModelPredictTimeoutTimes$11()V

    return-void
.end method

.method public static synthetic $r8$lambda$KRcmTewZisTtEhpXZHsnRmfToT0(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->start()V

    return-void
.end method

.method public static synthetic $r8$lambda$MbGu4gM5oITybd38d9xshBxdr8k(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$new$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$NRI-aGGfaZ40wW_kJpQOEtpVUfE(Lcom/android/server/display/statistics/BrightnessDataProcessor;ZFZF)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$updateThermalStats$5(ZFZF)V

    return-void
.end method

.method public static synthetic $r8$lambda$SGfyDWAR1awWsSLqiXuehLDDuoU(Lcom/android/server/display/statistics/BrightnessDataProcessor;ZF)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$noteAverageTemperature$6(ZF)V

    return-void
.end method

.method public static synthetic $r8$lambda$e-Z4CwLvvcyzQGcBwTh_-p9ZpYI(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$aggregateIndividualModelTrainTimes$10(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$jlp0xvT8gc6txC5MHiOr44fwxBY(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$forceReportTrainDataEnabled$9(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$oQzhtpP4993nLvClxlXuPEeHl9c(Lcom/android/server/display/statistics/BrightnessDataProcessor;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->lambda$updateGrayScale$1(F)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmBackgroundHandler(Lcom/android/server/display/statistics/BrightnessDataProcessor;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContentResolver(Lcom/android/server/display/statistics/BrightnessDataProcessor;)Landroid/content/ContentResolver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContentResolver:Landroid/content/ContentResolver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmReportBrightnessEventsEnable(Lcom/android/server/display/statistics/BrightnessDataProcessor;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mReportBrightnessEventsEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUserDragging(Lcom/android/server/display/statistics/BrightnessDataProcessor;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mUserDragging:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmAutoBrightnessEnable(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmAutoBrightnessModeChanged(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessModeChanged:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsAonSuppressDarken(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsMiShow(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsNotAonSuppressDarken(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmOrientation(Lcom/android/server/display/statistics/BrightnessDataProcessor;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOrientation:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTargetBrightnessAnimValue(Lcom/android/server/display/statistics/BrightnessDataProcessor;F)V
    .locals 0

    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTargetBrightnessAnimValue:F

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTemporaryBrightnessTimeStamp(Lcom/android/server/display/statistics/BrightnessDataProcessor;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemporaryBrightnessTimeStamp:J

    return-void
.end method

.method static bridge synthetic -$$Nest$mbrightnessChangedTriggerAggregation(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->brightnessChangedTriggerAggregation()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleAccSensor(Lcom/android/server/display/statistics/BrightnessDataProcessor;Landroid/hardware/SensorEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->handleAccSensor(Landroid/hardware/SensorEvent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleBrightnessChangeEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor;Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->handleBrightnessChangeEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleLightFovSensor(Lcom/android/server/display/statistics/BrightnessDataProcessor;Landroid/hardware/SensorEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->handleLightFovSensor(Landroid/hardware/SensorEvent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleLightSensor(Lcom/android/server/display/statistics/BrightnessDataProcessor;Landroid/hardware/SensorEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->handleLightSensor(Landroid/hardware/SensorEvent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreadyToReportEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->readyToReportEvent()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreportDisabledAutoBrightnessEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportDisabledAutoBrightnessEvent()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreportEventToServer(Lcom/android/server/display/statistics/BrightnessDataProcessor;Lcom/android/server/display/statistics/BrightnessEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportEventToServer(Lcom/android/server/display/statistics/BrightnessEvent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mresetBrightnessAnimInfo(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->resetBrightnessAnimInfo()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mresetPendingParams(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->resetPendingParams()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateBrightnessAnimInfo(Lcom/android/server/display/statistics/BrightnessDataProcessor;FFZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateBrightnessAnimInfo(FFZ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateBrightnessStatisticsData(Lcom/android/server/display/statistics/BrightnessDataProcessor;ZF)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateBrightnessStatisticsData(ZF)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateForegroundApps(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateForegroundApps()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateInterruptBrightnessAnimDuration(Lcom/android/server/display/statistics/BrightnessDataProcessor;IF)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateInterruptBrightnessAnimDuration(IF)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdatePointerEventMotionState(Lcom/android/server/display/statistics/BrightnessDataProcessor;ZII)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updatePointerEventMotionState(ZII)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateScreenStateChanged(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateScreenStateChanged(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateUserResetAutoBrightnessModeTimes(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateUserResetAutoBrightnessModeTimes()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetDEBUG()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 103
    nop

    .line 104
    const-string v0, "debug.miui.display.dgb"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    sput-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/display/DisplayDeviceConfig;FFF)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "displayDeviceConfig"    # Lcom/android/server/display/DisplayDeviceConfig;
    .param p3, "min"    # F
    .param p4, "normalMax"    # F
    .param p5, "hbmMinLux"    # F

    .line 383
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 204
    const/16 v5, -0x2710

    iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentUserId:I

    .line 224
    const/high16 v5, 0x7fc00000    # Float.NaN

    iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastStoreBrightness:F

    .line 225
    iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightness:F

    .line 226
    iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastManualBrightness:F

    .line 240
    const/4 v5, -0x1

    iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I

    .line 243
    iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangedState:I

    .line 245
    const/high16 v5, -0x40800000    # -1.0f

    iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLongTermModelSplineError:F

    .line 247
    iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDefaultSplineError:F

    .line 275
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessEventsMap:Ljava/util/Map;

    .line 277
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAdvancedBrightnessEventsMap:Ljava/util/Map;

    .line 283
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    .line 296
    new-instance v6, Landroid/util/SparseArray;

    invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V

    iput-object v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;

    .line 298
    new-instance v6, Landroid/util/SparseArray;

    invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V

    iput-object v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalRestrictedUsage:Landroid/util/SparseArray;

    .line 301
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    .line 309
    new-instance v6, Landroid/util/SparseArray;

    invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V

    iput-object v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalBrightnessRestrictedUsage:Landroid/util/SparseArray;

    .line 329
    iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAmbientLux:F

    .line 331
    iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastMainAmbientLux:F

    .line 333
    iput v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAssistAmbientLux:F

    .line 360
    new-instance v5, Lcom/android/server/display/statistics/BrightnessDataProcessor$1;

    invoke-direct {v5, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$1;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    iput-object v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mForegroundWindowListener:Lmiui/process/IForegroundWindowListener;

    .line 368
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    .line 370
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mModelTrainIndicatorsList:Ljava/util/List;

    .line 378
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAonFlareEventsMap:Ljava/util/Map;

    .line 441
    new-instance v5, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda5;

    invoke-direct {v5, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    iput-object v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOnAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    .line 384
    iput-object v1, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContext:Landroid/content/Context;

    .line 385
    move-object/from16 v5, p2

    iput-object v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    .line 386
    iput v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessMin:F

    .line 387
    iput v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mNormalBrightnessMax:F

    .line 388
    iput v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHbmMinLux:F

    .line 389
    const-class v6, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v6}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 390
    new-instance v6, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-direct {v6, v0, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeHandler;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Landroid/os/Looper;)V

    iput-object v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    .line 391
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v7

    iput-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    .line 392
    const-string/jumbo v7, "window"

    invoke-static {v7}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v7

    check-cast v7, Lcom/android/server/wm/WindowManagerService;

    iput-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mWms:Lcom/android/server/wm/WindowManagerService;

    .line 393
    const-string v7, "power"

    invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/PowerManager;

    iput-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPowerManager:Landroid/os/PowerManager;

    .line 394
    const-string v7, "alarm"

    invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/AlarmManager;

    iput-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAlarmManager:Landroid/app/AlarmManager;

    .line 395
    const-string/jumbo v7, "sensor"

    invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/hardware/SensorManager;

    iput-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorManager:Landroid/hardware/SensorManager;

    .line 396
    invoke-static {}, Lcom/android/server/display/aiautobrt/AppClassifier;->getInstance()Lcom/android/server/display/aiautobrt/AppClassifier;

    move-result-object v7

    iput-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAppClassifier:Lcom/android/server/display/aiautobrt/AppClassifier;

    .line 397
    new-instance v7, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;

    iget-object v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    .line 398
    invoke-virtual {v8, v2}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v9

    iget-object v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    .line 399
    invoke-virtual {v8, v3}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v10

    const/4 v11, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float v12, v4, v8

    const/4 v13, 0x0

    .line 401
    invoke-direct {v0, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I

    move-result v14

    int-to-float v14, v14

    const/4 v15, 0x0

    sub-float v8, v4, v8

    .line 402
    invoke-direct {v0, v8}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getAmbientLuxSpanIndex(F)I

    move-result v8

    int-to-float v8, v8

    move/from16 v16, v8

    move-object v8, v7

    invoke-direct/range {v8 .. v16}, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;-><init>(FFFFFFFF)V

    iput-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIndividualEventNormalizer:Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;

    .line 403
    new-instance v7, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda6;

    invoke-direct {v7, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 404
    return-void
.end method

.method private aggregateAdvancedBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "keySubEvent"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .line 1889
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAdvancedBrightnessEventsMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/display/statistics/AggregationEvent;

    .line 1890
    .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent;
    if-nez v0, :cond_0

    .line 1891
    new-instance v1, Lcom/android/server/display/statistics/AggregationEvent$AdvancedAggregationEvent;

    invoke-direct {v1}, Lcom/android/server/display/statistics/AggregationEvent$AdvancedAggregationEvent;-><init>()V

    move-object v0, v1

    .line 1893
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsSummary(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1894
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAdvancedBrightnessEventsMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895
    return-void
.end method

.method private aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "keySubEvent"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .line 3127
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAonFlareEventsMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/display/statistics/AggregationEvent;

    .line 3128
    .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent;
    if-nez v0, :cond_0

    .line 3129
    new-instance v1, Lcom/android/server/display/statistics/AggregationEvent$AonFlareAggregationEvent;

    invoke-direct {v1}, Lcom/android/server/display/statistics/AggregationEvent$AonFlareAggregationEvent;-><init>()V

    move-object v0, v1

    .line 3131
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsSummary(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3132
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAonFlareEventsMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3133
    return-void
.end method

.method private aggregateBrightnessEventsAvg(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "keySubEvent"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .line 1865
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent;

    move-result-object v0

    .line 1866
    .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent;
    move-object v1, p3

    check-cast v1, Ljava/lang/Float;

    invoke-direct {p0, v0, p2, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsAverageValues(Lcom/android/server/display/statistics/AggregationEvent;Ljava/lang/Object;Ljava/lang/Float;)V

    .line 1867
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessEventsMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1868
    return-void
.end method

.method private aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "keySubEvent"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .line 1853
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent;

    move-result-object v0

    .line 1854
    .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent;
    invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsSummary(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1855
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessEventsMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1856
    return-void
.end method

.method private aggregateBrightnessEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "keySubEvent"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .line 1877
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent;

    move-result-object v0

    .line 1878
    .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent;
    invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1879
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessEventsMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1880
    return-void
.end method

.method private aggregateBrightnessUsageDuration()V
    .locals 3

    .line 774
    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F

    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I

    move-result v0

    const/4 v1, 0x0

    const-string v2, "report"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateBrightnessUsage(IZLjava/lang/String;)V

    .line 776
    return-void
.end method

.method private aggregateCbmEventsAvg(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "keySubEvent"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .line 2955
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getCbmAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent;

    move-result-object v0

    .line 2956
    .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent;
    move-object v1, p3

    check-cast v1, Ljava/lang/Float;

    invoke-direct {p0, v0, p2, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsAverageValues(Lcom/android/server/display/statistics/AggregationEvent;Ljava/lang/Object;Ljava/lang/Float;)V

    .line 2957
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2958
    return-void
.end method

.method private aggregateCbmEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "keySubEvent"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .line 2943
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getCbmAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent;

    move-result-object v0

    .line 2944
    .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent;
    invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsSummary(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2945
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2946
    return-void
.end method

.method private aggregateCbmEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "keySubEvent"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .line 2931
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getCbmAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent;

    move-result-object v0

    .line 2932
    .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent;
    invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2933
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2934
    return-void
.end method

.method private aggregateSwitchEvents()V
    .locals 3

    .line 1970
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSwitchStatsHelper:Lcom/android/server/display/statistics/SwitchStatsHelper;

    invoke-virtual {v0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getAllSwitchStats()Ljava/util/List;

    move-result-object v0

    .line 1971
    .local v0, "allSwitchStats":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/statistics/BrightnessEvent$SwitchStatEntry;>;"
    sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 1972
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "aggregateSwitchEvents: allSwitchStats:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BrightnessDataProcessor"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1974
    :cond_0
    const-string/jumbo v1, "switch_stats"

    const-string/jumbo v2, "switch_stats_details"

    invoke-direct {p0, v1, v2, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1975
    return-void
.end method

.method private aggregateThermalEventsAvg(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "keySubEvent"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .line 2062
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getThermalAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent;

    move-result-object v0

    .line 2063
    .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent;
    move-object v1, p3

    check-cast v1, Ljava/lang/Float;

    invoke-direct {p0, v0, p2, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsAverageValues(Lcom/android/server/display/statistics/AggregationEvent;Ljava/lang/Object;Ljava/lang/Float;)V

    .line 2064
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2065
    return-void
.end method

.method private aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "keySubEvent"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .line 2050
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getThermalAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent;

    move-result-object v0

    .line 2051
    .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent;
    invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->statsSummary(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2052
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2053
    return-void
.end method

.method private aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "keySubEvent"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .line 2038
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getThermalAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent;

    move-result-object v0

    .line 2039
    .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent;
    invoke-virtual {v0}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2040
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2041
    return-void
.end method

.method private brightnessChangedTriggerAggregation()V
    .locals 12

    .line 1797
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    if-eqz v0, :cond_3

    .line 1798
    iget v0, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    .line 1799
    .local v0, "type":I
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    iget-object v1, v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->packageName:Ljava/lang/String;

    .line 1800
    .local v1, "pkg":Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->convergeAffectFactors()I

    move-result v2

    .line 1801
    .local v2, "factor":I
    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    iget v3, v3, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F

    invoke-direct {p0, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getAmbientLuxSpanIndex(F)I

    move-result v3

    .line 1802
    .local v3, "luxSpan":I
    iget v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F

    invoke-direct {p0, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I

    move-result v4

    .line 1803
    .local v4, "preBrightnessSpan":I
    iget-object v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    iget v5, v5, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F

    invoke-direct {p0, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I

    move-result v5

    .line 1804
    .local v5, "curBrightnessSpan":I
    iget-object v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    iget v6, v6, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F

    .line 1805
    .local v6, "brightness":F
    iget-object v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v7, v6}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    int-to-float v7, v7

    .line 1807
    .local v7, "curNit":F
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const-string v11, "brightness_adj_times"

    invoke-direct {p0, v11, v8, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1808
    const/4 v8, 0x3

    if-ne v0, v8, :cond_0

    .line 1810
    const-string v8, "override_adj_app_ranking"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {p0, v8, v1, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 1811
    :cond_0
    const/4 v8, 0x2

    if-ne v0, v8, :cond_2

    .line 1813
    const-string v8, "auto_manual_adj_app_ranking"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-direct {p0, v8, v1, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1815
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    const-string v11, "auto_manual_adj_avg_nits_lux_span"

    invoke-direct {p0, v11, v8, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsAvg(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1817
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const-string v11, "auto_manual_adj_display_mode"

    invoke-direct {p0, v11, v8, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1819
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const-string v11, "auto_manual_adj_lux_span"

    invoke-direct {p0, v11, v8, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1821
    if-ge v5, v4, :cond_1

    .line 1822
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const-string v11, "auto_manual_adj_low_lux_span"

    invoke-direct {p0, v11, v8, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1823
    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->flareStatisticalManualAdjustTimes(Z)V

    .line 1826
    :cond_1
    if-le v5, v4, :cond_2

    .line 1827
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const-string v11, "auto_manual_adj_high_lux_span"

    invoke-direct {p0, v11, v8, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1828
    invoke-direct {p0, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->flareStatisticalManualAdjustTimes(Z)V

    .line 1832
    :cond_2
    :goto_0
    invoke-direct {p0, v0, v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteAdjHighTimesOnBrightnessRestricted(IF)V

    .line 1833
    sget-boolean v8, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v8, :cond_3

    .line 1834
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "brightnessChangedTriggerAggregation: type: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mLastScreenBrightness: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", brightness: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", curNit: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", preBrightnessSpan: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", curBrightnessSpan: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mBrightnessEventsMap: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessEventsMap:Ljava/util/Map;

    .line 1841
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1834
    const-string v9, "BrightnessDataProcessor"

    invoke-static {v9, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1844
    .end local v0    # "type":I
    .end local v1    # "pkg":Ljava/lang/String;
    .end local v2    # "factor":I
    .end local v3    # "luxSpan":I
    .end local v4    # "preBrightnessSpan":I
    .end local v5    # "curBrightnessSpan":I
    .end local v6    # "brightness":F
    .end local v7    # "curNit":F
    :cond_3
    return-void
.end method

.method private checkIsValidMotionForWindowBrightness(II)Z
    .locals 3
    .param p1, "distanceX"    # I
    .param p2, "distanceY"    # I

    .line 889
    const/4 v0, 0x1

    .line 890
    .local v0, "isValid":Z
    const/16 v1, 0xa

    if-le p2, v1, :cond_0

    if-eqz p1, :cond_1

    div-int v1, p2, p1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    .line 892
    :cond_0
    const/4 v0, 0x0

    .line 893
    sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 894
    const-string v1, "BrightnessDataProcessor"

    const-string v2, "checkIsValidMotionForWindowBrightness: invalid and return."

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    :cond_1
    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHaveValidMotionForWindowBrightness:Z

    .line 898
    return v0
.end method

.method private clampBrightness(F)F
    .locals 1
    .param p1, "brightness"    # F

    .line 2892
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2893
    const/high16 v0, -0x40800000    # -1.0f

    return v0

    .line 2895
    :cond_0
    return p1
.end method

.method private computeAverageBrightnessIfNeeded(F)V
    .locals 6
    .param p1, "brightness"    # F

    .line 1420
    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastStoreBrightness:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessModeChanged:Z

    if-nez v0, :cond_0

    .line 1421
    return-void

    .line 1423
    :cond_0
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastStoreBrightness:F

    .line 1424
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 1425
    .local v0, "now":J
    iget-wide v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenOnTimeStamp:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1426
    invoke-direct {p0, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->computeLastAverageBrightness(J)V

    .line 1428
    :cond_1
    const/high16 v2, 0x7fc00000    # Float.NaN

    iput v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightness:F

    .line 1429
    iput v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastManualBrightness:F

    .line 1430
    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z

    if-eqz v2, :cond_2

    .line 1431
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightness:F

    goto :goto_0

    .line 1433
    :cond_2
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastManualBrightness:F

    .line 1435
    :goto_0
    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenOnTimeStamp:J

    .line 1436
    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessModeChanged:Z

    if-eqz v2, :cond_3

    .line 1437
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessModeChanged:Z

    .line 1439
    :cond_3
    sget-boolean v2, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v2, :cond_4

    .line 1440
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "computeAverageBrightnessIfNeeded: current brightness: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mAutoBrightnessEnable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", time: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenOnTimeStamp:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "BrightnessDataProcessor"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1444
    :cond_4
    return-void
.end method

.method private computeLastAverageBrightness(J)V
    .locals 7
    .param p1, "now"    # J

    .line 1451
    iget-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenOnTimeStamp:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    const v1, 0x3a83126f    # 0.001f

    mul-float/2addr v0, v1

    .line 1453
    .local v0, "timeDuration":F
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_1

    .line 1454
    iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightness:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    const-string v2, ", avgNits: "

    const-string v3, "BrightnessDataProcessor"

    const-string v4, "average_brightness"

    if-nez v1, :cond_0

    .line 1455
    iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessDuration:F

    add-float/2addr v1, v0

    iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessDuration:F

    .line 1456
    iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessIntegral:F

    iget-object v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    iget v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightness:F

    .line 1457
    invoke-virtual {v5, v6}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v5

    mul-float/2addr v5, v0

    add-float/2addr v1, v5

    iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessIntegral:F

    .line 1458
    iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessDuration:F

    div-float/2addr v1, v5

    .line 1459
    .local v1, "avgNits":F
    nop

    .line 1460
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    .line 1459
    invoke-direct {p0, v4, v5, v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1461
    sget-boolean v4, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 1462
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "computeLastAverageBrightness: compute last auto average brightness, timeDuration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "s, mAutoBrightnessDuration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessDuration:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "s, mAutoBrightnessIntegral: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessIntegral:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1469
    .end local v1    # "avgNits":F
    :cond_0
    iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessDuration:F

    add-float/2addr v1, v0

    iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessDuration:F

    .line 1470
    iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessIntegral:F

    iget-object v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    iget v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastManualBrightness:F

    float-to-int v6, v6

    .line 1471
    invoke-static {v6}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessIntToFloat(I)F

    move-result v6

    .line 1470
    invoke-virtual {v5, v6}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v5

    mul-float/2addr v5, v0

    add-float/2addr v1, v5

    iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessIntegral:F

    .line 1472
    iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessDuration:F

    div-float/2addr v1, v5

    .line 1473
    .restart local v1    # "avgNits":F
    nop

    .line 1474
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    .line 1473
    invoke-direct {p0, v4, v5, v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1475
    sget-boolean v4, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 1476
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "computeLastAverageBrightness: compute last manual average brightness, timeDuration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "s, mManualBrightnessDuration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessDuration:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "s, mManualBrightnessIntegral: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessIntegral:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1484
    .end local v1    # "avgNits":F
    :cond_1
    :goto_0
    return-void
.end method

.method private convergeAffectFactors()I
    .locals 2

    .line 1041
    const/4 v0, 0x0

    .line 1042
    .local v0, "factor":I
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSwitchStatsHelper:Lcom/android/server/display/statistics/SwitchStatsHelper;

    invoke-virtual {v1}, Lcom/android/server/display/statistics/SwitchStatsHelper;->isReadModeSettingsEnable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1043
    or-int/lit8 v0, v0, 0x1

    .line 1045
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSwitchStatsHelper:Lcom/android/server/display/statistics/SwitchStatsHelper;

    invoke-virtual {v1}, Lcom/android/server/display/statistics/SwitchStatsHelper;->isDcBacklightSettingsEnable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1046
    or-int/lit8 v0, v0, 0x2

    .line 1048
    :cond_1
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSwitchStatsHelper:Lcom/android/server/display/statistics/SwitchStatsHelper;

    invoke-virtual {v1}, Lcom/android/server/display/statistics/SwitchStatsHelper;->isDarkModeSettingsEnable()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1049
    or-int/lit8 v0, v0, 0x4

    .line 1051
    :cond_2
    return v0
.end method

.method private countThermalBrightnessRestrictedUsage(FLandroid/util/SparseArray;II)V
    .locals 3
    .param p1, "duration"    # F
    .param p3, "nit"    # I
    .param p4, "brightnessSpan"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Landroid/util/SparseArray<",
            "Landroid/util/SparseArray<",
            "Ljava/lang/Float;",
            ">;>;II)V"
        }
    .end annotation

    .line 2468
    .local p2, "usage":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/SparseArray<Ljava/lang/Float;>;>;"
    invoke-virtual {p2, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 2469
    .local v0, "sparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Float;>;"
    if-nez v0, :cond_0

    .line 2470
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    move-object v0, v1

    .line 2472
    :cond_0
    invoke-virtual {v0, p4}, Landroid/util/SparseArray;->contains(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2473
    invoke-virtual {v0, p4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    add-float/2addr p1, v1

    .line 2475
    :cond_1
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, p4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2476
    invoke-virtual {p2, p3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2477
    sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v1, :cond_2

    .line 2478
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "countThermalBrightnessRestrictedUsage: nit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", brightnessSpan: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", duration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BrightnessDataProcessor"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2482
    :cond_2
    return-void
.end method

.method private countThermalUsage(FLandroid/util/SparseArray;II)V
    .locals 3
    .param p1, "duration"    # F
    .param p3, "conditionId"    # I
    .param p4, "temperatureSpan"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Landroid/util/SparseArray<",
            "Landroid/util/SparseArray<",
            "Ljava/lang/Float;",
            ">;>;II)V"
        }
    .end annotation

    .line 2443
    .local p2, "usage":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/SparseArray<Ljava/lang/Float;>;>;"
    invoke-virtual {p2, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 2444
    .local v0, "sparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Float;>;"
    if-nez v0, :cond_0

    .line 2445
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    move-object v0, v1

    .line 2447
    :cond_0
    invoke-virtual {v0, p4}, Landroid/util/SparseArray;->contains(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2448
    invoke-virtual {v0, p4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    add-float/2addr p1, v1

    .line 2450
    :cond_1
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, p4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2451
    invoke-virtual {p2, p3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2452
    sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v1, :cond_2

    .line 2453
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "countThermalUsage: conditionId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", temperatureSpan: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", duration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BrightnessDataProcessor"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2457
    :cond_2
    return-void
.end method

.method private createModelEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;)Lcom/xiaomi/aiautobrt/IndividualModelEvent;
    .locals 6
    .param p1, "item"    # Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    .line 708
    iget v1, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F

    iget-object v2, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->packageName:Ljava/lang/String;

    iget v3, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F

    iget v4, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->orientation:I

    iget v5, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->sceneState:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->createModelEvent(FLjava/lang/String;FII)Lcom/xiaomi/aiautobrt/IndividualModelEvent;

    move-result-object v0

    return-object v0
.end method

.method private debounceBrightnessEvent(J)V
    .locals 2
    .param p1, "debounceTime"    # J

    .line 547
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mForcedReportTrainDataEnabled:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    move-wide v0, p1

    :goto_0
    move-wide p1, v0

    .line 548
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 549
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 550
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 551
    return-void
.end method

.method private flareStatisticalManualAdjustTimes(Z)V
    .locals 5
    .param p1, "isIncrease"    # Z

    .line 3163
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 3164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "manual adjust brightness: mIsAonSuppressDarken: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsNotAonSuppressDarken: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isIncrease:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3168
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z

    const/4 v1, 0x1

    const-string v2, "flare_manual_adjust_times"

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    .line 3169
    iput-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z

    .line 3170
    if-eqz p1, :cond_1

    .line 3171
    const-string v0, "2"

    goto :goto_0

    .line 3172
    :cond_1
    const-string v0, "1"

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 3170
    invoke-direct {p0, v2, v0, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3174
    :cond_2
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z

    if-eqz v0, :cond_4

    .line 3175
    iput-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z

    .line 3176
    if-eqz p1, :cond_3

    .line 3177
    const-string v0, "4"

    goto :goto_1

    .line 3178
    :cond_3
    const-string v0, "3"

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 3176
    invoke-direct {p0, v2, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3180
    :cond_4
    return-void
.end method

.method private flareStatisticalResetBrightnessModeTimes()V
    .locals 5

    .line 3186
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 3187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reset brightness mode: mIsAonSuppressDarken: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsNotAonSuppressDarken: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3191
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z

    const/4 v1, 0x1

    const-string v2, "flare_user_reset_brightness_mode_times"

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    .line 3192
    iput-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z

    .line 3193
    nop

    .line 3194
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 3193
    const-string v4, "1"

    invoke-direct {p0, v2, v4, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3196
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z

    if-eqz v0, :cond_2

    .line 3197
    iput-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z

    .line 3198
    nop

    .line 3199
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 3198
    const-string v1, "2"

    invoke-direct {p0, v2, v1, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3201
    :cond_2
    return-void
.end method

.method private getAmbientLuxSpanIndex(F)I
    .locals 4
    .param p1, "lux"    # F

    .line 986
    const/4 v0, 0x0

    .line 987
    .local v0, "index":I
    const/high16 v1, 0x41f00000    # 30.0f

    cmpg-float v1, p1, v1

    if-gez v1, :cond_0

    .line 988
    const/high16 v1, 0x40a00000    # 5.0f

    div-float v1, p1, v1

    float-to-int v0, v1

    goto :goto_0

    .line 989
    :cond_0
    const/high16 v1, 0x44610000    # 900.0f

    cmpg-float v2, p1, v1

    if-gez v2, :cond_1

    .line 990
    const/high16 v1, 0x42c80000    # 100.0f

    div-float v1, p1, v1

    const/high16 v2, 0x40c00000    # 6.0f

    add-float/2addr v1, v2

    float-to-int v0, v1

    goto :goto_0

    .line 991
    :cond_1
    const v2, 0x455ac000    # 3500.0f

    cmpg-float v3, p1, v2

    if-gez v3, :cond_2

    .line 993
    sub-float v1, p1, v1

    const/high16 v2, 0x43480000    # 200.0f

    div-float/2addr v1, v2

    const/high16 v2, 0x41700000    # 15.0f

    add-float/2addr v1, v2

    float-to-int v0, v1

    goto :goto_0

    .line 994
    :cond_2
    const v1, 0x461c4000    # 10000.0f

    cmpg-float v1, p1, v1

    if-gez v1, :cond_3

    .line 996
    sub-float v1, p1, v2

    const/high16 v2, 0x43fa0000    # 500.0f

    div-float/2addr v1, v2

    const/high16 v2, 0x41e00000    # 28.0f

    add-float/2addr v1, v2

    float-to-int v0, v1

    goto :goto_0

    .line 997
    :cond_3
    const v1, 0x466a6000    # 15000.0f

    cmpg-float v1, p1, v1

    if-gez v1, :cond_4

    .line 998
    const/16 v0, 0x29

    goto :goto_0

    .line 999
    :cond_4
    const v1, 0x4708b800    # 35000.0f

    cmpg-float v1, p1, v1

    if-gez v1, :cond_5

    .line 1000
    const/16 v0, 0x2a

    goto :goto_0

    .line 1001
    :cond_5
    const v1, 0x477de800    # 65000.0f

    cmpg-float v1, p1, v1

    if-gez v1, :cond_6

    .line 1002
    const/16 v0, 0x2b

    goto :goto_0

    .line 1003
    :cond_6
    const v1, 0x47c35000    # 100000.0f

    cmpg-float v1, p1, v1

    if-gez v1, :cond_7

    .line 1004
    const/16 v0, 0x2c

    goto :goto_0

    .line 1006
    :cond_7
    const/16 v0, 0x2c

    .line 1008
    :goto_0
    sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v1, :cond_8

    .line 1009
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lux = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BrightnessDataProcessor"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1011
    :cond_8
    return v0
.end method

.method private getBrightnessAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent;
    .locals 2
    .param p1, "keySubEvent"    # Ljava/lang/String;

    .line 1903
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessEventsMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/display/statistics/AggregationEvent;

    .line 1904
    .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent;
    if-nez v0, :cond_0

    .line 1905
    new-instance v1, Lcom/android/server/display/statistics/AggregationEvent$BrightnessAggregationEvent;

    invoke-direct {v1}, Lcom/android/server/display/statistics/AggregationEvent$BrightnessAggregationEvent;-><init>()V

    move-object v0, v1

    .line 1907
    :cond_0
    return-object v0
.end method

.method private getBrightnessChangedState(FF)I
    .locals 2
    .param p1, "currentValue"    # F
    .param p2, "targetValue"    # F

    .line 1720
    const/4 v0, 0x0

    cmpl-float v1, p1, v0

    if-eqz v1, :cond_3

    cmpl-float v0, p2, v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1723
    :cond_0
    cmpl-float v0, p2, p1

    if-lez v0, :cond_1

    .line 1724
    const/4 v0, 0x0

    return v0

    .line 1725
    :cond_1
    cmpg-float v0, p2, p1

    if-gez v0, :cond_2

    .line 1726
    const/4 v0, 0x1

    return v0

    .line 1728
    :cond_2
    const/4 v0, 0x2

    return v0

    .line 1721
    :cond_3
    :goto_0
    const/4 v0, 0x3

    return v0
.end method

.method private getBrightnessSpanByNit(F)I
    .locals 6
    .param p1, "brightness"    # F

    .line 1015
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 1017
    .local v0, "screenNit":F
    const/4 v1, 0x0

    .line 1018
    .local v1, "index":I
    const/high16 v2, 0x40600000    # 3.5f

    cmpl-float v2, v0, v2

    const/high16 v3, 0x41000000    # 8.0f

    if-ltz v2, :cond_0

    cmpg-float v2, v0, v3

    if-gez v2, :cond_0

    .line 1019
    const/4 v1, 0x1

    goto :goto_0

    .line 1020
    :cond_0
    cmpl-float v2, v0, v3

    const/high16 v4, 0x42480000    # 50.0f

    if-ltz v2, :cond_1

    cmpg-float v2, v0, v4

    if-gez v2, :cond_1

    .line 1021
    sub-float v2, v0, v3

    const/high16 v3, 0x40e00000    # 7.0f

    div-float/2addr v2, v3

    float-to-int v2, v2

    add-int/lit8 v1, v2, 0x2

    goto :goto_0

    .line 1022
    :cond_1
    cmpl-float v2, v0, v4

    const/high16 v3, 0x42a00000    # 80.0f

    if-ltz v2, :cond_2

    cmpg-float v2, v0, v3

    if-gez v2, :cond_2

    .line 1023
    sub-float v2, v0, v4

    const/high16 v3, 0x41200000    # 10.0f

    div-float/2addr v2, v3

    float-to-int v2, v2

    add-int/lit8 v2, v2, 0x2

    add-int/lit8 v1, v2, 0x6

    goto :goto_0

    .line 1024
    :cond_2
    cmpl-float v2, v0, v3

    const/high16 v5, 0x43fa0000    # 500.0f

    if-ltz v2, :cond_3

    cmpg-float v2, v0, v5

    if-gez v2, :cond_3

    .line 1025
    sub-float v2, v0, v3

    const/high16 v3, 0x41a00000    # 20.0f

    div-float/2addr v2, v3

    float-to-int v2, v2

    add-int/lit8 v2, v2, 0x2

    add-int/lit8 v1, v2, 0x9

    goto :goto_0

    .line 1026
    :cond_3
    cmpl-float v2, v0, v5

    const/high16 v3, 0x447a0000    # 1000.0f

    if-ltz v2, :cond_4

    cmpg-float v2, v0, v3

    if-gez v2, :cond_4

    .line 1027
    sub-float v2, v0, v5

    div-float/2addr v2, v4

    float-to-int v2, v2

    add-int/lit8 v2, v2, 0x2

    add-int/lit8 v1, v2, 0x1e

    goto :goto_0

    .line 1028
    :cond_4
    cmpl-float v2, v0, v3

    if-lez v2, :cond_5

    .line 1029
    const/16 v1, 0x2a

    .line 1031
    :cond_5
    :goto_0
    sget-boolean v2, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v2, :cond_6

    .line 1032
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "brightness = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", screenNit = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "BrightnessDataProcessor"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1034
    :cond_6
    return v1
.end method

.method private getBrightnessType(ZZFZ)I
    .locals 2
    .param p1, "userInitiatedChange"    # Z
    .param p2, "useAutoBrightness"    # Z
    .param p3, "brightnessOverrideFromWindow"    # F
    .param p4, "sunlightActive"    # Z

    .line 963
    const/4 v0, 0x0

    .line 965
    .local v0, "type":I
    invoke-static {p3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    .line 966
    const/4 v0, 0x3

    goto :goto_0

    .line 967
    :cond_0
    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    .line 968
    const/4 v0, 0x2

    goto :goto_0

    .line 969
    :cond_1
    if-eqz p2, :cond_2

    .line 970
    const/4 v0, 0x1

    goto :goto_0

    .line 971
    :cond_2
    if-eqz p4, :cond_3

    .line 972
    const/4 v0, 0x4

    .line 974
    :cond_3
    :goto_0
    return v0
.end method

.method private getCbmAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent;
    .locals 2
    .param p1, "keySubEvent"    # Ljava/lang/String;

    .line 2917
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/display/statistics/AggregationEvent;

    .line 2918
    .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent;
    if-nez v0, :cond_0

    .line 2919
    new-instance v1, Lcom/android/server/display/statistics/AggregationEvent$CbmAggregationEvent;

    invoke-direct {v1}, Lcom/android/server/display/statistics/AggregationEvent$CbmAggregationEvent;-><init>()V

    move-object v0, v1

    .line 2921
    :cond_0
    return-object v0
.end method

.method private getConfigBrightness(FI)F
    .locals 1
    .param p1, "lux"    # F
    .param p2, "sceneState"    # I

    .line 753
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOriSpline:Landroid/util/Spline;

    if-eqz v0, :cond_0

    .line 754
    invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F

    move-result v0

    return v0

    .line 755
    :cond_0
    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDefaultSceneSpline:Landroid/util/Spline;

    if-eqz v0, :cond_1

    .line 756
    invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F

    move-result v0

    return v0

    .line 757
    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDarkeningSceneSpline:Landroid/util/Spline;

    if-eqz v0, :cond_2

    .line 758
    invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F

    move-result v0

    return v0

    .line 759
    :cond_2
    const/4 v0, 0x1

    if-ne p2, v0, :cond_3

    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrighteningSceneSpline:Landroid/util/Spline;

    if-eqz v0, :cond_3

    .line 760
    invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F

    move-result v0

    return v0

    .line 762
    :cond_3
    const/high16 v0, 0x7fc00000    # Float.NaN

    return v0
.end method

.method private getHourFromTimestamp(J)I
    .locals 2
    .param p1, "timestamp"    # J

    .line 3154
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 3155
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 3156
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    return v1
.end method

.method private getSpline(Landroid/hardware/display/BrightnessConfiguration;)Landroid/util/Spline;
    .locals 4
    .param p1, "config"    # Landroid/hardware/display/BrightnessConfiguration;

    .line 1774
    const/4 v0, 0x0

    .line 1775
    .local v0, "spline":Landroid/util/Spline;
    if-eqz p1, :cond_0

    .line 1776
    invoke-virtual {p1}, Landroid/hardware/display/BrightnessConfiguration;->getCurve()Landroid/util/Pair;

    move-result-object v1

    .line 1777
    .local v1, "defaultCurve":Landroid/util/Pair;, "Landroid/util/Pair<[F[F>;"
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, [F

    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, [F

    invoke-static {v2, v3}, Landroid/util/Spline;->createSpline([F[F)Landroid/util/Spline;

    move-result-object v0

    .line 1779
    .end local v1    # "defaultCurve":Landroid/util/Pair;, "Landroid/util/Pair<[F[F>;"
    :cond_0
    return-object v0
.end method

.method private getSumValues(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "value"    # Ljava/lang/Object;
    .param p2, "increment"    # Ljava/lang/Object;

    .line 1931
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    instance-of v0, p2, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1932
    move-object v0, p1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move-object v1, p2

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 1934
    :cond_0
    instance-of v0, p1, Ljava/lang/Long;

    if-eqz v0, :cond_1

    instance-of v0, p2, Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1935
    move-object v0, p1

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    move-object v2, p2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0

    .line 1937
    :cond_1
    instance-of v0, p1, Ljava/lang/Float;

    if-eqz v0, :cond_2

    instance-of v0, p2, Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 1938
    move-object v0, p1

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    move-object v1, p2

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0

    .line 1940
    :cond_2
    return-object p2
.end method

.method private getTemperatureSpan(F)I
    .locals 4
    .param p1, "temperature"    # F

    .line 1093
    const/4 v0, 0x0

    .line 1094
    .local v0, "temperatureSpan":I
    const/4 v1, 0x0

    .line 1095
    .local v1, "index":I
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1096
    :goto_0
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 1097
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_0

    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    .line 1098
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpl-float v2, p1, v2

    if-nez v2, :cond_0

    .line 1099
    move v0, v1

    .line 1100
    goto :goto_1

    .line 1101
    :cond_0
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_1

    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    .line 1102
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpl-float v2, p1, v2

    if-lez v2, :cond_1

    .line 1103
    add-int/lit8 v0, v1, 0x1

    .line 1104
    goto :goto_1

    .line 1105
    :cond_1
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpg-float v2, v2, p1

    if-gtz v2, :cond_2

    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    add-int/lit8 v3, v1, 0x1

    .line 1106
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpg-float v2, p1, v2

    if-gez v2, :cond_2

    .line 1107
    move v0, v1

    .line 1108
    goto :goto_1

    .line 1110
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1113
    :cond_3
    :goto_1
    return v0
.end method

.method private getThermalAggregationEvent(Ljava/lang/String;)Lcom/android/server/display/statistics/AggregationEvent;
    .locals 2
    .param p1, "keySubEvent"    # Ljava/lang/String;

    .line 2024
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/display/statistics/AggregationEvent;

    .line 2025
    .local v0, "event":Lcom/android/server/display/statistics/AggregationEvent;
    if-nez v0, :cond_0

    .line 2026
    new-instance v1, Lcom/android/server/display/statistics/AggregationEvent$ThermalAggregationEvent;

    invoke-direct {v1}, Lcom/android/server/display/statistics/AggregationEvent$ThermalAggregationEvent;-><init>()V

    move-object v0, v1

    .line 2028
    :cond_0
    return-object v0
.end method

.method private handleAccSensor(Landroid/hardware/SensorEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 2180
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    if-eqz v0, :cond_0

    iget v0, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v0, v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 2183
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    iput-object v1, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->accValues:[F

    .line 2184
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerAccSensor(Z)V

    .line 2186
    :cond_0
    return-void
.end method

.method private handleBrightnessChangeEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;)V
    .locals 10
    .param p1, "item"    # Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    .line 511
    iget v0, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->brightnessOverrideFromWindow:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    .line 512
    .local v0, "windowOverrideApplying":Z
    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget v3, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->brightnessOverrideFromWindow:F

    iget v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessOverrideFromWindow:F

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    move v3, v1

    goto :goto_0

    :cond_0
    move v3, v2

    .line 514
    .local v3, "windowOverrideChanging":Z
    :goto_0
    iget-boolean v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mWindowOverrideBrightnessChanging:Z

    if-eq v3, v4, :cond_1

    .line 515
    iput-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mWindowOverrideBrightnessChanging:Z

    .line 518
    :cond_1
    if-eqz v3, :cond_3

    .line 519
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 521
    .local v4, "now":J
    iget-boolean v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mUserDragging:Z

    if-nez v6, :cond_2

    iget-boolean v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHaveValidMotionForWindowBrightness:Z

    if-eqz v6, :cond_3

    iget-wide v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLatestDraggingChangedTime:J

    sub-long v6, v4, v6

    const-wide/16 v8, 0x32

    cmp-long v6, v6, v8

    if-gez v6, :cond_3

    .line 523
    :cond_2
    iget v6, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F

    iput v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessOverrideFromWindow:F

    .line 524
    iput-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHaveValidWindowBrightness:Z

    .line 525
    sget-boolean v6, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v6, :cond_3

    .line 526
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Brightness from window is changing: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "BrightnessDataProcessor"

    invoke-static {v7, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    .end local v4    # "now":J
    :cond_3
    if-eqz v0, :cond_4

    iget-boolean v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHaveValidWindowBrightness:Z

    if-nez v4, :cond_4

    .line 531
    return-void

    .line 534
    :cond_4
    iget-object v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    if-eqz v4, :cond_5

    iget v4, v4, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    iget v5, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    if-eq v4, v5, :cond_5

    .line 535
    iget-object v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    iget v4, v4, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    invoke-direct {p0, v4, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerSensorByBrightnessType(IZ)V

    .line 537
    :cond_5
    iput-object p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    .line 539
    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mReportBrightnessEventsEnable:Z

    if-eqz v2, :cond_6

    .line 540
    iget v2, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    invoke-direct {p0, v2, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerSensorByBrightnessType(IZ)V

    .line 543
    :cond_6
    const-wide/16 v1, 0xbb8

    invoke-direct {p0, v1, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->debounceBrightnessEvent(J)V

    .line 544
    return-void
.end method

.method private handleLightFovSensor(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 2193
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    if-eqz v0, :cond_1

    iget v0, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2195
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    float-to-int v1, v1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    iput-boolean v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->isUseLightFovOptimization:Z

    .line 2196
    invoke-direct {p0, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerLightFovSensor(Z)V

    .line 2198
    :cond_1
    return-void
.end method

.method private handleLightSensor(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 2166
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    if-eqz v0, :cond_0

    .line 2167
    iget v0, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    .line 2168
    .local v0, "type":I
    if-nez v0, :cond_0

    .line 2169
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    iput v2, v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F

    .line 2170
    invoke-direct {p0, v0, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerSensorByBrightnessType(IZ)V

    .line 2173
    .end local v0    # "type":I
    :cond_0
    return-void
.end method

.method private isValidStartTimeStamp()Z
    .locals 4

    .line 979
    iget-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mStartTimeStamp:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private synthetic lambda$aggregateIndividualModelTrainTimes$10(Z)V
    .locals 3
    .param p1, "isSuccessful"    # Z

    .line 3004
    if-eqz p1, :cond_0

    const-string v0, "model_validation_success_count"

    goto :goto_0

    .line 3005
    :cond_0
    const-string v0, "model_validation_fail_count"

    :goto_0
    nop

    .line 3006
    .local v0, "name":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "individual_model_train"

    invoke-direct {p0, v2, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3007
    sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 3008
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "aggregateIndividualModelTrainTimes: isSuccessful: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCbmEventsMap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BrightnessDataProcessor"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3011
    :cond_1
    return-void
.end method

.method private synthetic lambda$aggregateModelPredictTimeoutTimes$11()V
    .locals 3

    .line 3019
    nop

    .line 3020
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 3019
    const-string v1, "individual_model_predict"

    const-string v2, "model_predict_timeout_count"

    invoke-direct {p0, v1, v2, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3021
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 3022
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "aggregateModelPredictTimeoutTimes: mCbmEventsMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3024
    :cond_0
    return-void
.end method

.method private synthetic lambda$forceReportTrainDataEnabled$9(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 2908
    iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mForcedReportTrainDataEnabled:Z

    return-void
.end method

.method private synthetic lambda$new$0()V
    .locals 1

    .line 442
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportScheduleEvent()V

    .line 443
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setReportScheduleEventAlarm(Z)V

    .line 444
    return-void
.end method

.method private synthetic lambda$noteAverageTemperature$6(ZF)V
    .locals 3
    .param p1, "needComputed"    # Z
    .param p2, "temperature"    # F

    .line 2574
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mScreenOn:Z

    if-nez v0, :cond_0

    .line 2575
    return-void

    .line 2577
    :cond_0
    if-eqz p1, :cond_1

    .line 2578
    const-string v0, "average"

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const-string/jumbo v2, "thermal_average_temperature"

    invoke-direct {p0, v2, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsAvg(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2579
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 2580
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "noteAverageTemperature: mThermalEventsMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2583
    :cond_1
    return-void
.end method

.method private synthetic lambda$noteDetailThermalUsage$4(IF)V
    .locals 13
    .param p1, "conditionId"    # I
    .param p2, "temperature"    # F

    .line 2331
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mScreenOn:Z

    if-nez v0, :cond_0

    .line 2332
    return-void

    .line 2334
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 2336
    .local v0, "now":J
    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    const-string v3, "restricted_usage"

    const-string/jumbo v4, "thermal_detail_restricted_usage"

    const/4 v5, 0x1

    const-string/jumbo v6, "unrestricted_usage"

    const-string/jumbo v7, "thermal_detail_unrestricted_usage"

    const/4 v8, 0x3

    const/4 v9, 0x2

    const/high16 v10, 0x447a0000    # 1000.0f

    if-eq v2, p1, :cond_3

    .line 2337
    iget v11, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    if-ne v11, v9, :cond_1

    .line 2338
    iget-wide v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    sub-long v3, v0, v3

    long-to-float v3, v3

    div-float/2addr v3, v10

    .line 2339
    .local v3, "duration":F
    iget-object v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;

    iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    .line 2340
    invoke-direct {p0, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I

    move-result v5

    .line 2339
    invoke-direct {p0, v3, v4, v2, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V

    .line 2341
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    .line 2342
    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2343
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;

    invoke-direct {p0, v7, v6, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 2345
    .end local v3    # "duration":F
    :cond_1
    if-ne v11, v8, :cond_2

    .line 2346
    iget-wide v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    sub-long v5, v0, v5

    long-to-float v5, v5

    div-float/2addr v5, v10

    .line 2347
    .local v5, "duration":F
    iget-object v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalRestrictedUsage:Landroid/util/SparseArray;

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    .line 2348
    invoke-direct {p0, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I

    move-result v7

    .line 2347
    invoke-direct {p0, v5, v6, v2, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V

    .line 2349
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    .line 2350
    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2351
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalRestrictedUsage:Landroid/util/SparseArray;

    invoke-direct {p0, v4, v3, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 2353
    .end local v5    # "duration":F
    :cond_2
    if-ne v11, v5, :cond_6

    .line 2354
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    .line 2355
    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    goto :goto_0

    .line 2357
    :cond_3
    iget v11, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    cmpl-float v12, v11, p2

    if-eqz v12, :cond_6

    .line 2358
    iget v12, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    if-ne v12, v9, :cond_4

    .line 2359
    iget-wide v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    sub-long v3, v0, v3

    long-to-float v3, v3

    div-float/2addr v3, v10

    .line 2360
    .restart local v3    # "duration":F
    iget-object v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;

    .line 2361
    invoke-direct {p0, v11}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I

    move-result v5

    .line 2360
    invoke-direct {p0, v3, v4, v2, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V

    .line 2362
    iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    .line 2363
    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2364
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;

    invoke-direct {p0, v7, v6, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 2366
    .end local v3    # "duration":F
    :cond_4
    if-ne v12, v8, :cond_5

    .line 2367
    iget-wide v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    sub-long v5, v0, v5

    long-to-float v5, v5

    div-float/2addr v5, v10

    .line 2368
    .restart local v5    # "duration":F
    iget-object v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalRestrictedUsage:Landroid/util/SparseArray;

    .line 2369
    invoke-direct {p0, v11}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I

    move-result v7

    .line 2368
    invoke-direct {p0, v5, v6, v2, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V

    .line 2370
    iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    .line 2371
    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2372
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalRestrictedUsage:Landroid/util/SparseArray;

    invoke-direct {p0, v4, v3, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 2374
    .end local v5    # "duration":F
    :cond_5
    if-ne v12, v5, :cond_6

    .line 2375
    iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    .line 2376
    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2379
    :cond_6
    :goto_0
    sget-boolean v2, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v2, :cond_7

    .line 2380
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "noteDetailThermalUsage: thermal state changed, mThermalStatus: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", conditionId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", temperature: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mLastTemperature: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mThermalEventsMap: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    .line 2385
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2380
    const-string v3, "BrightnessDataProcessor"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2387
    :cond_7
    return-void
.end method

.method private synthetic lambda$noteFullSceneThermalUsageStats$3(FFZIF)V
    .locals 10
    .param p1, "thermalBrightness"    # F
    .param p2, "brightness"    # F
    .param p3, "outdoorHighTemState"    # Z
    .param p4, "conditionId"    # I
    .param p5, "temperature"    # F

    .line 2079
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    .line 2080
    .local v7, "now":J
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-wide v3, v7

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteOutDoorHighTempUsage(FFJZZ)V

    .line 2081
    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalBrightnessRestrictedUsage(FFJZ)V

    .line 2082
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    const/4 v9, 0x1

    if-nez v0, :cond_1

    .line 2083
    cmpl-float v0, p2, p1

    if-lez v0, :cond_0

    .line 2084
    const/4 v1, 0x3

    const/4 v6, 0x1

    move-object v0, p0

    move v2, p4

    move v3, p5

    move-wide v4, v7

    invoke-direct/range {v0 .. v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteDetailThermalUsage(IIFJZ)V

    .line 2086
    const/4 v0, 0x3

    invoke-direct {p0, v0, v7, v8, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalUsage(IJZ)V

    goto :goto_0

    .line 2088
    :cond_0
    const/4 v1, 0x2

    const/4 v6, 0x1

    move-object v0, p0

    move v2, p4

    move v3, p5

    move-wide v4, v7

    invoke-direct/range {v0 .. v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteDetailThermalUsage(IIFJZ)V

    .line 2090
    const/4 v0, 0x2

    invoke-direct {p0, v0, v7, v8, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalUsage(IJZ)V

    goto :goto_0

    .line 2093
    :cond_1
    const/4 v1, 0x1

    const/4 v6, 0x1

    move-object v0, p0

    move v2, p4

    move v3, p5

    move-wide v4, v7

    invoke-direct/range {v0 .. v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteDetailThermalUsage(IIFJZ)V

    .line 2095
    invoke-direct {p0, v9, v7, v8, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalUsage(IJZ)V

    .line 2097
    :goto_0
    return-void
.end method

.method private synthetic lambda$startHdyUsageStats$7(Z)V
    .locals 0
    .param p1, "isHdrLayer"    # Z

    .line 2606
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteHdrUsage(Z)V

    .line 2607
    return-void
.end method

.method private synthetic lambda$updateExpId$2(I)V
    .locals 0
    .param p1, "expId"    # I

    .line 1790
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mExpId:I

    return-void
.end method

.method private synthetic lambda$updateGrayScale$1(F)V
    .locals 0
    .param p1, "grayScale"    # F

    .line 766
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mGrayScale:F

    return-void
.end method

.method private synthetic lambda$updateScreenNits$8(FF)V
    .locals 2
    .param p1, "originalNit"    # F
    .param p2, "actualNit"    # F

    .line 2877
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOriginalNit:F

    .line 2878
    iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mActualNit:F

    .line 2879
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 2880
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateScreenNits: mOriginalNit: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOriginalNit:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mActualNit: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mActualNit:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2883
    :cond_0
    return-void
.end method

.method private synthetic lambda$updateThermalStats$5(ZFZF)V
    .locals 10
    .param p1, "isScreenOn"    # Z
    .param p2, "temperature"    # F
    .param p3, "needComputed"    # Z
    .param p4, "brightnessState"    # F

    .line 2548
    iget-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 2549
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 2550
    .local v8, "now":J
    if-eqz p1, :cond_0

    .line 2551
    iput-wide v8, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J

    .line 2552
    iput-wide v8, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2553
    long-to-float v0, v8

    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessRestrictedTimeStamp:F

    .line 2554
    iput-wide v8, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J

    .line 2555
    invoke-direct {p0, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteAverageTemperatureScreenOn(FZ)V

    goto :goto_0

    .line 2557
    :cond_0
    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move v3, p4

    move-wide v4, v8

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteOutDoorHighTempUsage(FFJZZ)V

    .line 2558
    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F

    iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F

    invoke-direct/range {v1 .. v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalBrightnessRestrictedUsage(FFJZ)V

    .line 2559
    const/4 v2, 0x3

    iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    iget v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    move-wide v5, v8

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteDetailThermalUsage(IIFJZ)V

    .line 2561
    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v8, v9, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalUsage(IJZ)V

    .line 2564
    .end local v8    # "now":J
    :cond_1
    :goto_0
    return-void
.end method

.method private noteAdjHighTimesOnBrightnessRestricted(IF)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "brightness"    # F

    .line 2679
    const/4 v0, 0x1

    const/4 v1, 0x3

    if-eqz p1, :cond_0

    if-eq p1, v0, :cond_0

    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    if-eq p1, v1, :cond_0

    const/4 v2, 0x4

    if-ne p1, v2, :cond_1

    :cond_0
    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    if-ne v2, v1, :cond_1

    .line 2685
    invoke-static {p2}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 2686
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string/jumbo v2, "thermal_brightness_restricted_adj_high_times"

    invoke-direct {p0, v2, v1, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2688
    :cond_1
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 2689
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "noteAdjHighTimesOnBrightnessRestricted: type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", brightness: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2690
    invoke-static {p2}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLastScreenBrightness: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mThermalEventsMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    .line 2692
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2689
    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2694
    :cond_2
    return-void
.end method

.method private noteAverageTemperatureScreenOn(FZ)V
    .locals 3
    .param p1, "temperature"    # F
    .param p2, "needComputed"    # Z

    .line 2592
    if-eqz p2, :cond_0

    .line 2593
    const-string v0, "average"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const-string/jumbo v2, "thermal_average_temperature"

    invoke-direct {p0, v2, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsAvg(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2594
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 2595
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "noteAverageTemperature: mThermalEventsMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2598
    :cond_0
    return-void
.end method

.method private noteDetailThermalUsage(IIFJZ)V
    .locals 17
    .param p1, "status"    # I
    .param p2, "conditionId"    # I
    .param p3, "temperature"    # F
    .param p4, "now"    # J
    .param p6, "isScreenOn"    # Z

    .line 2225
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    iget-wide v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    .line 2226
    iput v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    .line 2227
    iput v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    .line 2228
    iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2230
    :cond_0
    const-string v6, "restricted_usage"

    const-string/jumbo v7, "thermal_detail_restricted_usage"

    const-string/jumbo v8, "unrestricted_usage"

    const-string/jumbo v9, "thermal_detail_unrestricted_usage"

    const/4 v10, 0x3

    const/4 v11, 0x2

    const/high16 v12, 0x447a0000    # 1000.0f

    if-eqz p6, :cond_a

    .line 2231
    iget v13, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    const/4 v14, 0x1

    if-eq v13, v2, :cond_3

    .line 2232
    iget v15, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    if-ne v15, v11, :cond_1

    .line 2233
    iget-wide v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    sub-long v6, v4, v6

    long-to-float v6, v6

    div-float/2addr v6, v12

    .line 2234
    .local v6, "duration":F
    iget-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;

    iget v10, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    .line 2235
    invoke-direct {v0, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I

    move-result v10

    .line 2234
    invoke-direct {v0, v6, v7, v13, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V

    .line 2236
    iput v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    .line 2237
    iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2238
    iget-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;

    invoke-direct {v0, v9, v8, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 2240
    .end local v6    # "duration":F
    :cond_1
    if-ne v15, v10, :cond_2

    .line 2241
    iget-wide v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    sub-long v8, v4, v8

    long-to-float v8, v8

    div-float/2addr v8, v12

    .line 2242
    .local v8, "duration":F
    iget-object v9, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalRestrictedUsage:Landroid/util/SparseArray;

    iget v10, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    .line 2243
    invoke-direct {v0, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I

    move-result v10

    .line 2242
    invoke-direct {v0, v8, v9, v13, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V

    .line 2244
    iput v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    .line 2245
    iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2246
    iget-object v9, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalRestrictedUsage:Landroid/util/SparseArray;

    invoke-direct {v0, v7, v6, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 2248
    .end local v8    # "duration":F
    :cond_2
    if-ne v15, v14, :cond_c

    .line 2249
    iput v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    .line 2250
    iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    goto/16 :goto_0

    .line 2252
    :cond_3
    iget v15, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    cmpl-float v16, v15, v3

    if-eqz v16, :cond_6

    .line 2253
    iget v14, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    if-ne v14, v11, :cond_4

    .line 2254
    iget-wide v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    sub-long v6, v4, v6

    long-to-float v6, v6

    div-float/2addr v6, v12

    .line 2255
    .restart local v6    # "duration":F
    iget-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;

    .line 2256
    invoke-direct {v0, v15}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I

    move-result v10

    .line 2255
    invoke-direct {v0, v6, v7, v13, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V

    .line 2257
    iput v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    .line 2258
    iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2259
    iget-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;

    invoke-direct {v0, v9, v8, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 2261
    .end local v6    # "duration":F
    :cond_4
    if-ne v14, v10, :cond_5

    .line 2262
    iget-wide v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    sub-long v8, v4, v8

    long-to-float v8, v8

    div-float/2addr v8, v12

    .line 2263
    .restart local v8    # "duration":F
    iget-object v9, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalRestrictedUsage:Landroid/util/SparseArray;

    .line 2264
    invoke-direct {v0, v15}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I

    move-result v10

    .line 2263
    invoke-direct {v0, v8, v9, v13, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V

    .line 2265
    iput v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    .line 2266
    iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2267
    iget-object v9, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalRestrictedUsage:Landroid/util/SparseArray;

    invoke-direct {v0, v7, v6, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 2269
    .end local v8    # "duration":F
    :cond_5
    const/4 v6, 0x1

    if-ne v14, v6, :cond_c

    .line 2270
    iput v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    .line 2271
    iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    goto/16 :goto_0

    .line 2273
    :cond_6
    iget v14, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    if-eq v14, v1, :cond_9

    .line 2274
    if-ne v14, v11, :cond_7

    .line 2275
    iget-wide v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    sub-long v6, v4, v6

    long-to-float v6, v6

    div-float/2addr v6, v12

    .line 2276
    .restart local v6    # "duration":F
    iget-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;

    .line 2277
    invoke-direct {v0, v15}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I

    move-result v10

    .line 2276
    invoke-direct {v0, v6, v7, v13, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V

    .line 2278
    iput v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    .line 2279
    iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2280
    iget-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;

    invoke-direct {v0, v9, v8, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 2282
    .end local v6    # "duration":F
    :cond_7
    if-ne v14, v10, :cond_8

    .line 2283
    iget-wide v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    sub-long v8, v4, v8

    long-to-float v8, v8

    div-float/2addr v8, v12

    .line 2284
    .restart local v8    # "duration":F
    iget-object v9, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalRestrictedUsage:Landroid/util/SparseArray;

    .line 2285
    invoke-direct {v0, v15}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I

    move-result v10

    .line 2284
    invoke-direct {v0, v8, v9, v13, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V

    .line 2286
    iput v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    .line 2287
    iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2288
    iget-object v9, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalRestrictedUsage:Landroid/util/SparseArray;

    invoke-direct {v0, v7, v6, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 2290
    .end local v8    # "duration":F
    :cond_8
    const/4 v6, 0x1

    if-ne v14, v6, :cond_c

    .line 2291
    iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    goto :goto_0

    .line 2293
    :cond_9
    const/4 v6, 0x1

    if-ne v14, v6, :cond_c

    .line 2294
    iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    goto :goto_0

    .line 2297
    :cond_a
    iget v13, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    if-ne v13, v11, :cond_b

    .line 2298
    iget-wide v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    sub-long v6, v4, v6

    long-to-float v6, v6

    div-float/2addr v6, v12

    .line 2299
    .restart local v6    # "duration":F
    iget-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;

    iget v10, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    iget v11, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    .line 2300
    invoke-direct {v0, v11}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I

    move-result v11

    .line 2299
    invoke-direct {v0, v6, v7, v10, v11}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V

    .line 2301
    iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2302
    iget-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;

    invoke-direct {v0, v9, v8, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 2304
    .end local v6    # "duration":F
    :cond_b
    if-ne v13, v10, :cond_c

    .line 2305
    iget-wide v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    sub-long v8, v4, v8

    long-to-float v8, v8

    div-float/2addr v8, v12

    .line 2306
    .restart local v8    # "duration":F
    iget-object v9, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalRestrictedUsage:Landroid/util/SparseArray;

    iget v10, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    iget v11, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    .line 2307
    invoke-direct {v0, v11}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getTemperatureSpan(F)I

    move-result v11

    .line 2306
    invoke-direct {v0, v8, v9, v10, v11}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalUsage(FLandroid/util/SparseArray;II)V

    .line 2308
    iput-wide v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastDetailThermalUsageTimeStamp:J

    .line 2309
    iget-object v9, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalRestrictedUsage:Landroid/util/SparseArray;

    invoke-direct {v0, v7, v6, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2313
    .end local v8    # "duration":F
    :cond_c
    :goto_0
    sget-boolean v6, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v6, :cond_d

    .line 2314
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "noteDetailThermalUsage: status: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mThermalStatus: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", conditionId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", temperature: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mLastTemperature: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mThermalEventsMap: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    .line 2319
    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2314
    const-string v7, "BrightnessDataProcessor"

    invoke-static {v7, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2321
    :cond_d
    return-void
.end method

.method private noteHbmUsage(FF)V
    .locals 6
    .param p1, "spanIndex"    # F
    .param p2, "duration"    # F

    .line 2650
    const/high16 v0, 0x7fc00000    # Float.NaN

    .line 2651
    .local v0, "transitionBrightness":F
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v1}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightness()[F

    move-result-object v1

    .line 2652
    .local v1, "brightness":[F
    if-eqz v1, :cond_1

    .line 2653
    array-length v2, v1

    const/4 v3, 0x2

    if-le v2, v3, :cond_0

    .line 2654
    array-length v2, v1

    sub-int/2addr v2, v3

    aget v0, v1, v2

    goto :goto_0

    .line 2656
    :cond_0
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget v0, v1, v2

    .line 2659
    :cond_1
    :goto_0
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2660
    invoke-static {v0}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v2

    int-to-float v2, v2

    invoke-direct {p0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I

    move-result v2

    int-to-float v2, v2

    .line 2662
    .local v2, "spanTransition":F
    cmpl-float v3, p1, v2

    if-lez v3, :cond_2

    .line 2663
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const-string v5, "hbm_usage"

    invoke-direct {p0, v5, v3, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2664
    sget-boolean v3, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v3, :cond_2

    .line 2665
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "noteHbmUsage: spanIndex: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", spanTransition: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mBrightnessEventsMap: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessEventsMap:Ljava/util/Map;

    .line 2667
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2665
    const-string v4, "BrightnessDataProcessor"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2671
    .end local v2    # "spanTransition":F
    :cond_2
    return-void
.end method

.method private noteHdrAppUsage(F)V
    .locals 3
    .param p1, "duration"    # F

    .line 2639
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHdrAppPackageName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2640
    const-string v1, "hdr_usage_app_usage"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2642
    :cond_0
    return-void
.end method

.method private noteHdrUsage(Z)V
    .locals 6
    .param p1, "isHdrLayer"    # Z

    .line 2615
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsHdrLayer:Z

    if-eq v0, p1, :cond_2

    .line 2616
    iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsHdrLayer:Z

    .line 2617
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 2618
    .local v0, "now":J
    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsHdrLayer:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHdrAppPackageName:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 2619
    iget-wide v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastHdrEnableTimeStamp:J

    sub-long v2, v0, v2

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    .line 2620
    .local v2, "duration":F
    const-string/jumbo v3, "usage_value"

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const-string v5, "hdr_usage"

    invoke-direct {p0, v5, v3, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2621
    invoke-direct {p0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteHdrAppUsage(F)V

    .line 2622
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHdrAppPackageName:Ljava/lang/String;

    .line 2623
    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastHdrEnableTimeStamp:J

    .line 2624
    sget-boolean v3, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 2625
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "noteHdrUsage: mBrightnessEventsMap: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessEventsMap:Ljava/util/Map;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "BrightnessDataProcessor"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2627
    .end local v2    # "duration":F
    :cond_0
    goto :goto_0

    .line 2628
    :cond_1
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mForegroundAppPackageName:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHdrAppPackageName:Ljava/lang/String;

    .line 2629
    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastHdrEnableTimeStamp:J

    .line 2632
    .end local v0    # "now":J
    :cond_2
    :goto_0
    return-void
.end method

.method private noteHdrUsageBeforeReported()V
    .locals 6

    .line 2747
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 2748
    .local v0, "now":J
    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsHdrLayer:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHdrAppPackageName:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 2749
    iget-wide v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastHdrEnableTimeStamp:J

    sub-long v2, v0, v2

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    .line 2750
    .local v2, "duration":F
    const-string/jumbo v3, "usage_value"

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const-string v5, "hdr_usage"

    invoke-direct {p0, v5, v3, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2751
    invoke-direct {p0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteHdrAppUsage(F)V

    .line 2752
    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastHdrEnableTimeStamp:J

    .line 2753
    sget-boolean v3, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 2754
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "noteHdrUsageBeforeReported: mBrightnessEventsMap: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessEventsMap:Ljava/util/Map;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "BrightnessDataProcessor"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2757
    .end local v2    # "duration":F
    :cond_0
    return-void
.end method

.method private noteOutDoorHighTempUsage(FFJZZ)V
    .locals 15
    .param p1, "restrictedBrightness"    # F
    .param p2, "brightness"    # F
    .param p3, "now"    # J
    .param p5, "outDoorHighTempState"    # Z
    .param p6, "isPendingReported"    # Z

    .line 2495
    move-object v0, p0

    move/from16 v1, p2

    move-wide/from16 v2, p3

    move/from16 v4, p5

    iget-wide v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_0

    .line 2496
    iput-boolean v4, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTempState:Z

    .line 2497
    iput-wide v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J

    .line 2499
    :cond_0
    iget v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    const/4 v6, 0x0

    if-eqz v5, :cond_1

    move v5, v6

    goto :goto_0

    .line 2500
    :cond_1
    iget-object v5, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    iget v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F

    invoke-virtual {v5, v7}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-float v5, v5

    :goto_0
    nop

    .line 2501
    .local v5, "lastUnrestrictedNit":F
    iget v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F

    invoke-static {v7}, Ljava/lang/Float;->isNaN(F)Z

    move-result v7

    if-eqz v7, :cond_2

    goto :goto_1

    .line 2502
    :cond_2
    iget-object v6, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    iget v7, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F

    invoke-virtual {v6, v7}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    int-to-float v6, v6

    :goto_1
    nop

    .line 2503
    .local v6, "lastRestrictedNit":F
    invoke-static {v5, v6}, Landroid/util/MathUtils;->min(FF)F

    move-result v7

    .line 2505
    .local v7, "lastRestrictedNitByThermal":F
    iget-boolean v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTempState:Z

    const-string/jumbo v9, "unrestricted_usage"

    const-string v10, "restricted_usage"

    const-string/jumbo v11, "thermal_outdoor_usage"

    const/high16 v12, 0x447a0000    # 1000.0f

    if-eq v8, v4, :cond_6

    .line 2506
    if-eqz v8, :cond_3

    iget v13, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mMinOutDoorHighTempNit:F

    cmpl-float v13, v7, v13

    if-nez v13, :cond_3

    .line 2507
    iget-wide v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J

    sub-long v8, v2, v8

    long-to-float v8, v8

    div-float/2addr v8, v12

    .line 2508
    .local v8, "duration":F
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-direct {p0, v11, v10, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    .line 2509
    .end local v8    # "duration":F
    :cond_3
    if-eqz v8, :cond_4

    iget v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mMinOutDoorHighTempNit:F

    cmpg-float v10, v7, v8

    if-gez v10, :cond_4

    cmpl-float v8, v6, v8

    if-nez v8, :cond_4

    .line 2511
    iget-wide v13, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J

    sub-long v13, v2, v13

    long-to-float v8, v13

    div-float/2addr v8, v12

    .line 2512
    .restart local v8    # "duration":F
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-direct {p0, v11, v9, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2514
    .end local v8    # "duration":F
    :cond_4
    :goto_2
    if-eqz p6, :cond_5

    iget-boolean v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTempState:Z

    goto :goto_3

    :cond_5
    move v8, v4

    :goto_3
    iput-boolean v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTempState:Z

    .line 2515
    iput-wide v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J

    goto :goto_5

    .line 2516
    :cond_6
    if-eqz v8, :cond_a

    iget v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F

    cmpl-float v8, v1, v8

    if-nez v8, :cond_7

    iget v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F

    cmpl-float v8, p1, v8

    if-eqz v8, :cond_a

    .line 2518
    :cond_7
    iget v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mMinOutDoorHighTempNit:F

    cmpl-float v13, v7, v8

    if-nez v13, :cond_8

    .line 2519
    iget-wide v8, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J

    sub-long v8, v2, v8

    long-to-float v8, v8

    div-float/2addr v8, v12

    .line 2520
    .restart local v8    # "duration":F
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-direct {p0, v11, v10, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_4

    .line 2521
    .end local v8    # "duration":F
    :cond_8
    cmpg-float v10, v7, v8

    if-gez v10, :cond_9

    cmpl-float v8, v6, v8

    if-nez v8, :cond_9

    .line 2523
    long-to-float v8, v2

    iget v10, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessRestrictedTimeStamp:F

    sub-float/2addr v8, v10

    div-float/2addr v8, v12

    .line 2524
    .restart local v8    # "duration":F
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-direct {p0, v11, v9, v10}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2526
    .end local v8    # "duration":F
    :cond_9
    :goto_4
    iput-wide v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTemTimeStamp:J

    .line 2528
    :cond_a
    :goto_5
    sget-boolean v8, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v8, :cond_b

    .line 2529
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "noteOutDoorHighTempUsage: brightness: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", lastRestrictedNit: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mLastUnrestrictedBrightness: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", lastRestrictedNitByThermal: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", outDoorHighTempState: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mLastOutDoorHighTempState: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTempState:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mThermalEventsMap: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    .line 2535
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2529
    const-string v9, "BrightnessDataProcessor"

    invoke-static {v9, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2537
    :cond_b
    return-void
.end method

.method private noteThermalBrightnessRestrictedUsage(FFJZ)V
    .locals 6
    .param p1, "restrictedBrightness"    # F
    .param p2, "unrestrictedBrightness"    # F
    .param p3, "now"    # J
    .param p5, "isScreenOn"    # Z

    .line 2400
    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessRestrictedTimeStamp:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 2401
    iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F

    .line 2402
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F

    .line 2403
    long-to-float v0, p3

    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessRestrictedTimeStamp:F

    .line 2406
    :cond_0
    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F

    cmpl-float v1, v0, p1

    if-nez v1, :cond_1

    iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F

    cmpl-float v1, v1, p2

    if-nez v1, :cond_1

    if-nez p5, :cond_4

    .line 2409
    :cond_1
    iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 2410
    long-to-float v1, p3

    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessRestrictedTimeStamp:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    .line 2411
    .local v1, "duration":F
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v2, v0}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 2412
    .local v0, "nit":I
    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F

    invoke-static {v2}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v2

    int-to-float v2, v2

    invoke-direct {p0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I

    move-result v2

    .line 2413
    .local v2, "brightnessSpan":I
    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalBrightnessRestrictedUsage:Landroid/util/SparseArray;

    invoke-direct {p0, v1, v3, v0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->countThermalBrightnessRestrictedUsage(FLandroid/util/SparseArray;II)V

    .line 2414
    const-string v3, "restricted_usage"

    iget-object v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalBrightnessRestrictedUsage:Landroid/util/SparseArray;

    const-string/jumbo v5, "thermal_brightness_restricted_usage"

    invoke-direct {p0, v5, v3, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2416
    iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F

    .line 2417
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F

    .line 2418
    long-to-float v3, p3

    iput v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessRestrictedTimeStamp:F

    .line 2419
    .end local v0    # "nit":I
    .end local v2    # "brightnessSpan":I
    goto :goto_0

    .end local v1    # "duration":F
    :cond_2
    const/4 v0, 0x2

    if-eq v1, v0, :cond_3

    const/4 v0, 0x1

    if-ne v1, v0, :cond_4

    .line 2421
    :cond_3
    iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F

    .line 2422
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F

    .line 2423
    long-to-float v0, p3

    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessRestrictedTimeStamp:F

    .line 2426
    :cond_4
    :goto_0
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_5

    .line 2427
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "noteThermalBrightnessRestrictedUsage: restrictedBrightness: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", unrestrictedBrightness: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mThermalEventsMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    .line 2430
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2427
    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2432
    :cond_5
    return-void
.end method

.method private noteThermalUsage(IJZ)V
    .locals 9
    .param p1, "status"    # I
    .param p2, "now"    # J
    .param p4, "isScreenOn"    # Z

    .line 2109
    iget-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2110
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    .line 2111
    iput-wide p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J

    .line 2113
    :cond_0
    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    const-string v1, ", mThermalEventsMap: "

    const-string v2, ", duration: "

    const-string v3, "noteThermalUsage: status: "

    const-string v4, "BrightnessDataProcessor"

    const-string/jumbo v5, "thermal_usage"

    const/high16 v6, 0x447a0000    # 1000.0f

    if-eq v0, p1, :cond_1

    if-eqz p4, :cond_1

    .line 2114
    iget-wide v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J

    sub-long v7, p2, v7

    long-to-float v7, v7

    div-float/2addr v7, v6

    .line 2115
    .local v7, "duration":F
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {p0, v5, v0, v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2116
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    .line 2117
    iput-wide p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J

    .line 2118
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 2119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    .line 2121
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2119
    invoke-static {v4, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2123
    .end local v7    # "duration":F
    :cond_1
    if-nez p4, :cond_2

    .line 2124
    iget-wide v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J

    sub-long v7, p2, v7

    long-to-float v7, v7

    div-float/2addr v7, v6

    .line 2125
    .restart local v7    # "duration":F
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {p0, v5, v0, v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2126
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    .line 2127
    iput-wide p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J

    .line 2128
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 2129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    .line 2131
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2129
    invoke-static {v4, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2134
    .end local v7    # "duration":F
    :cond_2
    :goto_0
    return-void
.end method

.method private readyToReportEvent()V
    .locals 2

    .line 599
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    if-eqz v0, :cond_1

    .line 601
    iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHaveValidWindowBrightness:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mUserDragging:Z

    if-eqz v1, :cond_0

    .line 602
    return-void

    .line 604
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportBrightnessEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;)V

    .line 607
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportIndividualModelEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;)V

    .line 609
    :cond_1
    return-void
.end method

.method private registerAccSensor(Z)V
    .locals 6
    .param p1, "enable"    # Z

    .line 2785
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAccSensorEnabled:Z

    const-string v1, "BrightnessDataProcessor"

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 2786
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;

    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAccSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 2788
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAccSensorEnabled:Z

    .line 2789
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 2790
    const-string v0, "registerAccSensor: register acc sensor."

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2792
    :cond_0
    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 2793
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;

    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAccSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v2, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 2794
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAccSensorEnabled:Z

    .line 2795
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 2796
    const-string v0, "registerAccSensor: unregister acc sensor."

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2799
    :cond_1
    :goto_0
    return-void
.end method

.method private registerForegroundAppUpdater()V
    .locals 3

    .line 824
    :try_start_0
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTaskStackListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$TaskStackListenerImpl;

    invoke-interface {v0, v1}, Landroid/app/IActivityTaskManager;->registerTaskStackListener(Landroid/app/ITaskStackListener;)V

    .line 825
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mForegroundWindowListener:Lmiui/process/IForegroundWindowListener;

    invoke-static {v0}, Lmiui/process/ProcessManager;->registerForegroundWindowListener(Lmiui/process/IForegroundWindowListener;)V

    .line 828
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateForegroundApps()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 834
    goto :goto_0

    .line 829
    :catch_0
    move-exception v0

    .line 830
    .local v0, "e":Landroid/os/RemoteException;
    sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 831
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to register foreground app updater: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BrightnessDataProcessor"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 835
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void
.end method

.method private registerLightFovSensor(Z)V
    .locals 6
    .param p1, "enable"    # Z

    .line 2806
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightFovSensor:Landroid/hardware/Sensor;

    const-string v1, "BrightnessDataProcessor"

    if-eqz v0, :cond_0

    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightFovSensorEnabled:Z

    if-nez v2, :cond_0

    if-eqz p1, :cond_0

    .line 2807
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 2809
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightFovSensorEnabled:Z

    .line 2810
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 2811
    const-string v0, "registerLightFovSensor: register light fov sensor."

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2813
    :cond_0
    if-eqz v0, :cond_1

    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightFovSensorEnabled:Z

    if-eqz v2, :cond_1

    if-nez p1, :cond_1

    .line 2814
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;

    invoke-virtual {v2, v3, v0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 2815
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightFovSensorEnabled:Z

    .line 2816
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 2817
    const-string v0, "registerLightFovSensor: unregister light fov sensor."

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2820
    :cond_1
    :goto_0
    return-void
.end method

.method private registerLightSensor(Z)V
    .locals 6
    .param p1, "enable"    # Z

    .line 2764
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightSensorEnabled:Z

    const-string v1, "BrightnessDataProcessor"

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 2765
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;

    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 2767
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightSensorEnabled:Z

    .line 2768
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 2769
    const-string v0, "registerLightSensor: register light sensor."

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2771
    :cond_0
    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 2772
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;

    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v2, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 2773
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightSensorEnabled:Z

    .line 2774
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 2775
    const-string v0, "registerLightSensor: unregister light sensor."

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2778
    :cond_1
    :goto_0
    return-void
.end method

.method private registerScreenStateReceiver()V
    .locals 3

    .line 866
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 867
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 868
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 869
    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 870
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/server/display/statistics/BrightnessDataProcessor$ScreenStateReceiver;

    invoke-direct {v2, p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$ScreenStateReceiver;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 871
    return-void
.end method

.method private registerSensorByBrightnessType(IZ)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "enable"    # Z

    .line 2206
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 2207
    invoke-direct {p0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerAccSensor(Z)V

    .line 2208
    invoke-direct {p0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerLightFovSensor(Z)V

    goto :goto_0

    .line 2209
    :cond_0
    if-nez p1, :cond_1

    .line 2210
    invoke-direct {p0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerLightSensor(Z)V

    .line 2212
    :cond_1
    :goto_0
    return-void
.end method

.method private registerSettingsObserver()V
    .locals 7

    .line 1547
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContentResolver:Landroid/content/ContentResolver;

    .line 1548
    const-string v1, "screen_brightness_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSettingsObserver:Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;

    .line 1547
    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {v0, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1551
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v2, -0x2

    invoke-static {v0, v1, v4, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v4

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z

    .line 1554
    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightnessEnable:Z

    .line 1555
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContentResolver:Landroid/content/ContentResolver;

    .line 1556
    const-string v2, "disable_security_by_mishow"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSettingsObserver:Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;

    .line 1555
    invoke-virtual {v0, v3, v4, v6, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1559
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {v0, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_1

    move v4, v1

    :cond_1
    iput-boolean v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z

    .line 1560
    return-void
.end method

.method private reportAdvancedBrightnessEvents()V
    .locals 4

    .line 2007
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z

    if-nez v0, :cond_0

    .line 2008
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAdvancedBrightnessEventsMap:Ljava/util/Map;

    const-string v2, "advanced_brightness_aggregation"

    iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mExpId:I

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->reportAggregatedEventsToServer(Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;I)V

    .line 2010
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 2011
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reportAdvancedBrightnessEvents: mAdvancedBrightnessEventsMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAdvancedBrightnessEventsMap:Ljava/util/Map;

    .line 2012
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2011
    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2015
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAdvancedBrightnessEventsMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2016
    return-void
.end method

.method private reportAggregatedBrightnessEvents()V
    .locals 4

    .line 1981
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateSwitchEvents()V

    .line 1982
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessUsageDuration()V

    .line 1983
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteHdrUsageBeforeReported()V

    .line 1984
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z

    if-nez v0, :cond_0

    .line 1985
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessEventsMap:Ljava/util/Map;

    const-string v2, "brightness_quota_aggregation"

    iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mExpId:I

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->reportAggregatedEventsToServer(Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;I)V

    .line 1987
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 1988
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reportAggregatedBrightnessEvents: mBrightnessEventsMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessEventsMap:Ljava/util/Map;

    .line 1989
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1988
    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1992
    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->resetStatsCache()V

    .line 1993
    return-void
.end method

.method private reportAonFlareEvents()V
    .locals 4

    .line 3139
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z

    if-nez v0, :cond_0

    .line 3140
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAonFlareEventsMap:Ljava/util/Map;

    const-string v2, "aon_flare_aggregation"

    iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mExpId:I

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->reportAggregatedEventsToServer(Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;I)V

    .line 3142
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 3143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reportAonFlareEvents: mAonFlareEventsMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAonFlareEventsMap:Ljava/util/Map;

    .line 3144
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3143
    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3147
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAonFlareEventsMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 3148
    return-void
.end method

.method private reportBrightnessEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;)V
    .locals 8
    .param p1, "item"    # Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    .line 615
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 616
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "brightness changed, let\'s make a recode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", foregroundApps: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mForegroundAppPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 621
    .local v0, "now":J
    iget v2, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F

    invoke-direct {p0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I

    move-result v2

    .line 622
    .local v2, "curSpanIndex":I
    iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F

    invoke-direct {p0, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I

    move-result v3

    .line 625
    .local v3, "preSpanIndex":I
    iget v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    const/4 v5, 0x3

    const/4 v6, 0x1

    if-ne v4, v5, :cond_1

    move v4, v6

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    .line 627
    .local v4, "brightnessRestricted":Z
    :goto_0
    iget v5, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    if-ne v5, v6, :cond_2

    iget-object v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCloudControllerListener:Lcom/android/server/display/AutomaticBrightnessControllerImpl$CloudControllerListener;

    .line 628
    invoke-interface {v5}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$CloudControllerListener;->isAutoBrightnessStatisticsEventEnable()Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    iget v5, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    const/4 v7, 0x2

    if-ne v5, v7, :cond_4

    iget-boolean v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mReportBrightnessEventsEnable:Z

    if-eqz v5, :cond_4

    .line 631
    :cond_3
    new-instance v5, Lcom/android/server/display/statistics/BrightnessEvent;

    invoke-direct {v5}, Lcom/android/server/display/statistics/BrightnessEvent;-><init>()V

    .line 632
    .local v5, "event":Lcom/android/server/display/statistics/BrightnessEvent;
    iget v6, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    invoke-virtual {v5, v6}, Lcom/android/server/display/statistics/BrightnessEvent;->setEventType(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    .line 633
    invoke-virtual {v6, v2}, Lcom/android/server/display/statistics/BrightnessEvent;->setCurBrightnessSpanIndex(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    .line 634
    invoke-virtual {v6, v3}, Lcom/android/server/display/statistics/BrightnessEvent;->setPreBrightnessSpanIndex(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    .line 635
    invoke-virtual {v6, v0, v1}, Lcom/android/server/display/statistics/BrightnessEvent;->setTimeStamp(J)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F

    .line 636
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setScreenBrightness(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F

    .line 637
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setPreviousBrightness(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F

    .line 638
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->userDataPoint:F

    .line 639
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setUserDataPoint(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget-object v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->packageName:Ljava/lang/String;

    .line 640
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setForegroundPackage(Ljava/lang/String;)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget-boolean v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->lowPowerMode:Z

    .line 641
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLowPowerModeFlag(Z)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget-boolean v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->defaultConfig:Z

    .line 642
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setIsDefaultConfig(Z)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    .line 643
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->convergeAffectFactors()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setAffectFactorFlag(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F

    .line 644
    invoke-direct {p0, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getAmbientLuxSpanIndex(F)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLuxSpanIndex(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOrientation:I

    .line 645
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setOrientation(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mGrayScale:F

    .line 646
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setDisplayGrayScale(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentUserId:I

    .line 647
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setUserId(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalNit:F

    .line 648
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setOriginalNit(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->actualNit:F

    .line 649
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setActualNit(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget-boolean v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsHdrLayer:Z

    .line 650
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setHdrLayerEnable(Z)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    .line 651
    invoke-virtual {v6, v4}, Lcom/android/server/display/statistics/BrightnessEvent;->setBrightnessRestrictedEnable(Z)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->mainLux:F

    .line 652
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setMainAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->assistLux:F

    .line 653
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setAssistAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastMainAmbientLux:F

    .line 654
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLastMainAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAssistAmbientLux:F

    .line 655
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLastAssistAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget-object v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->accValues:[F

    .line 656
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setAccValues([F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget-boolean v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->isUseLightFovOptimization:Z

    .line 657
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setIsUseLightFovOptimization(Z)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSwitchStatsHelper:Lcom/android/server/display/statistics/SwitchStatsHelper;

    .line 658
    invoke-virtual {v7}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getAllSwitchStats()Ljava/util/List;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setSwitchStats(Ljava/util/List;)Lcom/android/server/display/statistics/BrightnessEvent;

    .line 659
    invoke-direct {p0, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportEventToServer(Lcom/android/server/display/statistics/BrightnessEvent;)V

    .end local v5    # "event":Lcom/android/server/display/statistics/BrightnessEvent;
    goto/16 :goto_1

    .line 660
    :cond_4
    iget v5, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    if-eq v5, v6, :cond_6

    iget-boolean v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mReportBrightnessEventsEnable:Z

    if-eqz v5, :cond_6

    .line 662
    new-instance v5, Lcom/android/server/display/statistics/BrightnessEvent;

    invoke-direct {v5}, Lcom/android/server/display/statistics/BrightnessEvent;-><init>()V

    .line 663
    .restart local v5    # "event":Lcom/android/server/display/statistics/BrightnessEvent;
    iget v6, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    invoke-virtual {v5, v6}, Lcom/android/server/display/statistics/BrightnessEvent;->setEventType(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    .line 664
    invoke-virtual {v6, v2}, Lcom/android/server/display/statistics/BrightnessEvent;->setCurBrightnessSpanIndex(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    .line 665
    invoke-virtual {v6, v3}, Lcom/android/server/display/statistics/BrightnessEvent;->setPreBrightnessSpanIndex(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    .line 666
    invoke-virtual {v6, v0, v1}, Lcom/android/server/display/statistics/BrightnessEvent;->setTimeStamp(J)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F

    .line 667
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setPreviousBrightness(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget-object v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->packageName:Ljava/lang/String;

    .line 668
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setForegroundPackage(Ljava/lang/String;)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget-boolean v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->lowPowerMode:Z

    .line 669
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLowPowerModeFlag(Z)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    .line 670
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->convergeAffectFactors()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setAffectFactorFlag(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOrientation:I

    .line 671
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setOrientation(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentUserId:I

    .line 672
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setUserId(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalNit:F

    .line 673
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setOriginalNit(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->actualNit:F

    .line 674
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setActualNit(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget-boolean v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsHdrLayer:Z

    .line 675
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setHdrLayerEnable(Z)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    .line 676
    invoke-virtual {v6, v4}, Lcom/android/server/display/statistics/BrightnessEvent;->setBrightnessRestrictedEnable(Z)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSwitchStatsHelper:Lcom/android/server/display/statistics/SwitchStatsHelper;

    .line 677
    invoke-virtual {v7}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getAllSwitchStats()Ljava/util/List;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setSwitchStats(Ljava/util/List;)Lcom/android/server/display/statistics/BrightnessEvent;

    .line 678
    iget v6, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    if-nez v6, :cond_5

    .line 679
    iget v6, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F

    invoke-virtual {v5, v6}, Lcom/android/server/display/statistics/BrightnessEvent;->setAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F

    .line 680
    invoke-direct {p0, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getAmbientLuxSpanIndex(F)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLuxSpanIndex(I)Lcom/android/server/display/statistics/BrightnessEvent;

    .line 682
    :cond_5
    invoke-direct {p0, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportEventToServer(Lcom/android/server/display/statistics/BrightnessEvent;)V

    goto :goto_2

    .line 660
    .end local v5    # "event":Lcom/android/server/display/statistics/BrightnessEvent;
    :cond_6
    :goto_1
    nop

    .line 684
    :goto_2
    invoke-direct {p0, v2, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateBrightnessUsageIfNeeded(II)V

    .line 685
    return-void
.end method

.method private reportCustomBrightnessEvents()V
    .locals 4

    .line 3066
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z

    if-nez v0, :cond_0

    .line 3067
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    const-string v2, "custom_brightness_aggregation"

    iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mExpId:I

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->reportAggregatedEventsToServer(Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;I)V

    .line 3069
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 3070
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reportCustomBrightnessEvents: mCbmEventsMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3073
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 3074
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mModelTrainIndicatorsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 3075
    return-void
.end method

.method private reportDisabledAutoBrightnessEvent()V
    .locals 9

    .line 2826
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mScreenOn:Z

    if-eqz v0, :cond_3

    .line 2827
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightnessEnable:Z

    const-string v1, "BrightnessDataProcessor"

    const/16 v2, 0xd

    if-eqz v0, :cond_1

    iget-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z

    if-nez v3, :cond_1

    .line 2828
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2829
    new-instance v0, Lcom/android/server/display/statistics/BrightnessEvent;

    invoke-direct {v0}, Lcom/android/server/display/statistics/BrightnessEvent;-><init>()V

    .line 2830
    .local v0, "event":Lcom/android/server/display/statistics/BrightnessEvent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 2831
    .local v3, "now":J
    iget v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    .line 2833
    .local v5, "brightnessRestricted":Z
    :goto_0
    const/16 v6, 0x8

    invoke-virtual {v0, v6}, Lcom/android/server/display/statistics/BrightnessEvent;->setEventType(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    .line 2834
    invoke-virtual {v6, v3, v4}, Lcom/android/server/display/statistics/BrightnessEvent;->setTimeStamp(J)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mActualNit:F

    .line 2835
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setActualNit(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOriginalNit:F

    .line 2836
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setOriginalNit(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mForegroundAppPackageName:Ljava/lang/String;

    .line 2837
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setForegroundPackage(Ljava/lang/String;)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    .line 2838
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->convergeAffectFactors()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setAffectFactorFlag(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOrientation:I

    .line 2839
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setOrientation(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentUserId:I

    .line 2840
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setUserId(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget-boolean v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsHdrLayer:Z

    .line 2841
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setHdrLayerEnable(Z)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    .line 2842
    invoke-virtual {v6, v5}, Lcom/android/server/display/statistics/BrightnessEvent;->setBrightnessRestrictedEnable(Z)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAmbientLux:F

    .line 2843
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAmbientLux:F

    .line 2844
    invoke-direct {p0, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getAmbientLuxSpanIndex(F)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLuxSpanIndex(I)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastMainAmbientLux:F

    .line 2845
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLastMainAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAssistAmbientLux:F

    .line 2846
    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setLastAssistAmbientLux(F)Lcom/android/server/display/statistics/BrightnessEvent;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSwitchStatsHelper:Lcom/android/server/display/statistics/SwitchStatsHelper;

    .line 2847
    invoke-virtual {v7}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getAllSwitchStats()Ljava/util/List;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/server/display/statistics/BrightnessEvent;->setSwitchStats(Ljava/util/List;)Lcom/android/server/display/statistics/BrightnessEvent;

    .line 2848
    iget-object v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v6, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 2849
    .local v2, "msg":Landroid/os/Message;
    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2850
    iget-object v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    const-wide/32 v7, 0x493e0

    invoke-virtual {v6, v2, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 2851
    sget-boolean v6, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v6, :cond_2

    .line 2852
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "reportDisabledAutoBrightnessEvent: event: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/android/server/display/statistics/BrightnessEvent;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2854
    .end local v0    # "event":Lcom/android/server/display/statistics/BrightnessEvent;
    .end local v2    # "msg":Landroid/os/Message;
    .end local v3    # "now":J
    .end local v5    # "brightnessRestricted":Z
    :cond_1
    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z

    if-eqz v0, :cond_2

    .line 2855
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2856
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2857
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_3

    .line 2858
    const-string v0, "reportDisabledAutoBrightnessEvent: remove message due to enabled auto brightness."

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 2854
    :cond_2
    :goto_1
    nop

    .line 2864
    :cond_3
    :goto_2
    return-void
.end method

.method private reportEventToServer(Lcom/android/server/display/statistics/AdvancedEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/android/server/display/statistics/AdvancedEvent;

    .line 790
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z

    if-nez v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->reportToOneTrack(Landroid/content/Context;Lcom/android/server/display/statistics/AdvancedEvent;)V

    .line 792
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 793
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 794
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/android/server/display/statistics/AdvancedEvent;->convertToString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 795
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reportAdvancedEventToServer:, event:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BrightnessDataProcessor"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    return-void
.end method

.method private reportEventToServer(Lcom/android/server/display/statistics/BrightnessEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/android/server/display/statistics/BrightnessEvent;

    .line 779
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z

    if-nez v0, :cond_0

    .line 780
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->reportToOneTrack(Landroid/content/Context;Lcom/android/server/display/statistics/BrightnessEvent;)V

    .line 781
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 782
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 783
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/android/server/display/statistics/BrightnessEvent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 784
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reportBrightnessEventToServer: , event:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BrightnessDataProcessor"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    return-void
.end method

.method private reportIndividualModelEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;)V
    .locals 2
    .param p1, "item"    # Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    .line 691
    iget v0, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_0
    iget v0, p1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F

    .line 693
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mModelEventCallback:Lcom/android/server/display/statistics/BrightnessDataProcessor$ModelEventCallback;

    if-nez v0, :cond_1

    goto :goto_0

    .line 700
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->createModelEvent(Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;)Lcom/xiaomi/aiautobrt/IndividualModelEvent;

    move-result-object v0

    .line 702
    .local v0, "event":Lcom/xiaomi/aiautobrt/IndividualModelEvent;
    invoke-virtual {v0}, Lcom/xiaomi/aiautobrt/IndividualModelEvent;->isValidRawEvent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 703
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mModelEventCallback:Lcom/android/server/display/statistics/BrightnessDataProcessor$ModelEventCallback;

    invoke-interface {v1, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$ModelEventCallback;->onBrightnessModelEvent(Lcom/xiaomi/aiautobrt/IndividualModelEvent;)V

    .line 705
    :cond_2
    return-void

    .line 697
    .end local v0    # "event":Lcom/xiaomi/aiautobrt/IndividualModelEvent;
    :cond_3
    :goto_0
    return-void
.end method

.method private reportScheduleEvent()V
    .locals 0

    .line 447
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportAggregatedBrightnessEvents()V

    .line 448
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportAdvancedBrightnessEvents()V

    .line 449
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportThermalEvents()V

    .line 450
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportCustomBrightnessEvents()V

    .line 451
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportAonFlareEvents()V

    .line 452
    return-void
.end method

.method private reportThermalEvents()V
    .locals 4

    .line 2722
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateThermalStatsBeforeReported()V

    .line 2723
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsMiShow:Z

    if-nez v0, :cond_0

    .line 2724
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    const-string/jumbo v2, "thermal_aggregation"

    iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mExpId:I

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/display/statistics/OneTrackUploaderHelper;->reportAggregatedEventsToServer(Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;I)V

    .line 2726
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 2727
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reportThermalEvents: mThermalEventsMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2730
    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->resetThermalEventsCache()V

    .line 2731
    return-void
.end method

.method private resetAverageBrightnessInfo()V
    .locals 1

    .line 1566
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessDuration:F

    .line 1567
    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessIntegral:F

    .line 1568
    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessDuration:F

    .line 1569
    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mManualBrightnessIntegral:F

    .line 1570
    return-void
.end method

.method private resetBrightnessAnimInfo()V
    .locals 3

    .line 1736
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentBrightnessAnimValue:F

    .line 1737
    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTargetBrightnessAnimValue:F

    .line 1738
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStart:Z

    .line 1739
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStartTime:J

    .line 1740
    const/4 v2, -0x1

    iput v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I

    .line 1741
    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemporaryBrightnessTimeStamp:J

    .line 1742
    return-void
.end method

.method private resetPendingParams()V
    .locals 2

    .line 802
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    if-eqz v0, :cond_0

    .line 803
    iget v0, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->type:I

    .line 804
    .local v0, "type":I
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    iget v1, v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F

    iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F

    .line 805
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    iget v1, v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->ambientLux:F

    iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAmbientLux:F

    .line 806
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    iget v1, v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->mainLux:F

    iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastMainAmbientLux:F

    .line 807
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    iget v1, v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->assistLux:F

    iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAssistAmbientLux:F

    .line 809
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerSensorByBrightnessType(IZ)V

    .line 810
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    .line 813
    .end local v0    # "type":I
    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->resetWindowOverrideParams()V

    .line 814
    return-void
.end method

.method private resetStatsCache()V
    .locals 1

    .line 1999
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->resetAverageBrightnessInfo()V

    .line 2000
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessEventsMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2001
    return-void
.end method

.method private resetThermalEventsCache()V
    .locals 1

    .line 2737
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2738
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalUnrestrictedUsage:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 2739
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDetailThermalRestrictedUsage:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 2740
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalBrightnessRestrictedUsage:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 2741
    return-void
.end method

.method private resetWindowOverrideParams()V
    .locals 2

    .line 817
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHaveValidWindowBrightness:Z

    .line 818
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessOverrideFromWindow:F

    .line 819
    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mWindowOverrideBrightnessChanging:Z

    .line 820
    return-void
.end method

.method private scheduleUpdateBrightnessStatisticsData(ZF)V
    .locals 3
    .param p1, "screenOn"    # Z
    .param p2, "brightness"    # F

    .line 1383
    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    move-result-object v0

    .line 1384
    .local v0, "args":Lcom/android/internal/os/SomeArgs;
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    .line 1385
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    .line 1386
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 1387
    return-void
.end method

.method private setPointerEventListener(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .line 554
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mWms:Lcom/android/server/wm/WindowManagerService;

    if-nez v0, :cond_0

    .line 555
    return-void

    .line 557
    :cond_0
    const-string v1, "BrightnessDataProcessor"

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    .line 558
    iget-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPointerEventListenerEnabled:Z

    if-nez v3, :cond_2

    .line 559
    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPointerEventListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;

    invoke-virtual {v0, v3, v2}, Lcom/android/server/wm/WindowManagerService;->registerPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V

    .line 561
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPointerEventListenerEnabled:Z

    .line 562
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 563
    const-string v0, "register pointer event listener."

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 567
    :cond_1
    iget-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPointerEventListenerEnabled:Z

    if-eqz v3, :cond_2

    .line 568
    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPointerEventListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;

    invoke-virtual {v0, v3, v2}, Lcom/android/server/wm/WindowManagerService;->unregisterPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V

    .line 570
    iput-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPointerEventListenerEnabled:Z

    .line 571
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 572
    const-string/jumbo v0, "unregister pointer event listener."

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    :cond_2
    :goto_0
    return-void
.end method

.method private setReportScheduleEventAlarm(Z)V
    .locals 14
    .param p1, "init"    # Z

    .line 429
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 430
    .local v0, "now":J
    sget-boolean v2, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v2, :cond_0

    const-wide/32 v3, 0x1d4c0

    goto :goto_0

    .line 431
    :cond_0
    if-eqz p1, :cond_1

    const-wide/32 v3, 0x2932e00

    goto :goto_0

    :cond_1
    const-wide/32 v3, 0x5265c00

    :goto_0
    nop

    .line 432
    .local v3, "duration":J
    add-long v12, v0, v3

    .line 433
    .local v12, "nextTime":J
    if-eqz v2, :cond_2

    .line 434
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setReportSwitchStatAlarm: next time: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 435
    invoke-static {v3, v4}, Landroid/util/TimeUtils;->formatDuration(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 434
    const-string v5, "BrightnessDataProcessor"

    invoke-static {v5, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    :cond_2
    iget-object v5, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAlarmManager:Landroid/app/AlarmManager;

    const/4 v6, 0x2

    const-string v9, "report_switch_stats"

    iget-object v10, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOnAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    iget-object v11, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    move-wide v7, v12

    invoke-virtual/range {v5 .. v11}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V

    .line 439
    return-void
.end method

.method private setRotationListener(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .line 579
    const-string v0, "BrightnessDataProcessor"

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 580
    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mRotationListenerEnabled:Z

    if-nez v2, :cond_1

    .line 581
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mRotationListenerEnabled:Z

    .line 582
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mRotationWatcher:Lcom/android/server/display/statistics/BrightnessDataProcessor$RotationWatcher;

    invoke-virtual {v2, v3, v1}, Lcom/android/server/wm/WindowManagerService;->watchRotation(Landroid/view/IRotationWatcher;I)I

    .line 583
    sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 584
    const-string v1, "register rotation listener."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 588
    :cond_0
    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mRotationListenerEnabled:Z

    if-eqz v2, :cond_1

    .line 589
    iput-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mRotationListenerEnabled:Z

    .line 590
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mRotationWatcher:Lcom/android/server/display/statistics/BrightnessDataProcessor$RotationWatcher;

    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowManagerService;->removeRotationWatcher(Landroid/view/IRotationWatcher;)V

    .line 591
    sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 592
    const-string/jumbo v1, "unregister rotation listener."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    :cond_1
    :goto_0
    return-void
.end method

.method private start()V
    .locals 3

    .line 407
    new-instance v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;

    invoke-direct {v0, p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    iput-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPointerEventListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;

    .line 408
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/display/statistics/SwitchStatsHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/display/statistics/SwitchStatsHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSwitchStatsHelper:Lcom/android/server/display/statistics/SwitchStatsHelper;

    .line 409
    new-instance v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$TaskStackListenerImpl;

    invoke-direct {v0, p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$TaskStackListenerImpl;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    iput-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTaskStackListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$TaskStackListenerImpl;

    .line 410
    new-instance v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$RotationWatcher;

    invoke-direct {v0, p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$RotationWatcher;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    iput-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mRotationWatcher:Lcom/android/server/display/statistics/BrightnessDataProcessor$RotationWatcher;

    .line 411
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 412
    const v1, 0x11070035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mMinOutDoorHighTempNit:F

    .line 413
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mStartTimeStamp:J

    .line 414
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setRotationListener(Z)V

    .line 415
    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setPointerEventListener(Z)V

    .line 416
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerForegroundAppUpdater()V

    .line 417
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerScreenStateReceiver()V

    .line 418
    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setReportScheduleEventAlarm(Z)V

    .line 419
    new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;

    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSettingsObserver:Lcom/android/server/display/statistics/BrightnessDataProcessor$SettingsObserver;

    .line 420
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mContentResolver:Landroid/content/ContentResolver;

    .line 421
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->registerSettingsObserver()V

    .line 422
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightSensor:Landroid/hardware/Sensor;

    .line 423
    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v1, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAccSensor:Landroid/hardware/Sensor;

    .line 424
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1fa2a8f

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLightFovSensor:Landroid/hardware/Sensor;

    .line 425
    new-instance v0, Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener-IA;)V

    iput-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mSensorListener:Lcom/android/server/display/statistics/BrightnessDataProcessor$SensorListener;

    .line 426
    return-void
.end method

.method private startHdyUsageStats(Z)V
    .locals 2
    .param p1, "isHdrLayer"    # Z

    .line 2605
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda9;

    invoke-direct {v1, p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda9;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2608
    return-void
.end method

.method private statsAverageValues(Lcom/android/server/display/statistics/AggregationEvent;Ljava/lang/Object;Ljava/lang/Float;)V
    .locals 6
    .param p1, "event"    # Lcom/android/server/display/statistics/AggregationEvent;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Float;

    .line 1951
    invoke-virtual {p1}, Lcom/android/server/display/statistics/AggregationEvent;->getQuotaEvents()Ljava/util/Map;

    move-result-object v0

    .line 1952
    .local v0, "quotaEvents":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;"
    invoke-virtual {p1}, Lcom/android/server/display/statistics/AggregationEvent;->getCacheDataMap()Ljava/util/Map;

    move-result-object v1

    .line 1953
    .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/util/List<Ljava/lang/Float;>;>;"
    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 1954
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    const/4 v3, 0x0

    .line 1955
    .local v3, "sum":F
    if-nez v2, :cond_0

    .line 1956
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object v2, v4

    .line 1958
    :cond_0
    invoke-interface {v2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1959
    invoke-interface {v1, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1960
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 1961
    .local v5, "v":F
    add-float/2addr v3, v5

    .line 1962
    .end local v5    # "v":F
    goto :goto_0

    .line 1963
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    int-to-float v4, v4

    div-float v4, v3, v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v0, p2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964
    return-void
.end method

.method private statsSummary(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .line 1917
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;"
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1918
    .local v0, "totalValues":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 1919
    invoke-direct {p0, v0, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getSumValues(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    .line 1921
    :cond_0
    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1922
    return-void
.end method

.method private updateBrightnessAnimInfo(FFZ)V
    .locals 3
    .param p1, "currentBrightnessAnim"    # F
    .param p2, "targetBrightnessAnim"    # F
    .param p3, "begin"    # Z

    .line 1636
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z

    if-eqz v0, :cond_0

    .line 1637
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentBrightnessAnimValue:F

    .line 1638
    iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTargetBrightnessAnimValue:F

    .line 1639
    iput-boolean p3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStart:Z

    .line 1640
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStartTime:J

    .line 1641
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessChangedState(FF)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I

    .line 1643
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 1644
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateBrightnessAnimInfo: mCurrentAnimateValue:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentBrightnessAnimValue:F

    .line 1645
    invoke-static {v1}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTargetAnimateValue:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTargetBrightnessAnimValue:F

    .line 1647
    invoke-static {v1}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAnimationStart:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStart:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAnimationStartTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStartTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mBrightnessChangedState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1644
    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1653
    :cond_0
    return-void
.end method

.method private updateBrightnessStatisticsData(ZF)V
    .locals 4
    .param p1, "screenOn"    # Z
    .param p2, "brightness"    # F

    .line 1395
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 1396
    .local v0, "now":J
    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mScreenOn:Z

    if-eq v2, p1, :cond_1

    .line 1397
    iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mScreenOn:Z

    .line 1398
    if-eqz p1, :cond_0

    .line 1399
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenOnTimeStamp:J

    .line 1401
    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    .line 1402
    iput p2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F

    goto :goto_0

    .line 1405
    :cond_0
    const/high16 v2, 0x7fc00000    # Float.NaN

    iput v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastStoreBrightness:F

    .line 1406
    invoke-direct {p0, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->computeLastAverageBrightness(J)V

    .line 1409
    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    .line 1410
    invoke-direct {p0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->computeAverageBrightnessIfNeeded(F)V

    .line 1412
    :cond_2
    return-void
.end method

.method private updateBrightnessUsage(IZLjava/lang/String;)V
    .locals 6
    .param p1, "spanIndex"    # I
    .param p2, "dueToScreenOff"    # Z
    .param p3, "reason"    # Ljava/lang/String;

    .line 920
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->isValidStartTimeStamp()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 921
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 922
    .local v0, "now":J
    iget-wide v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mStartTimeStamp:J

    sub-long v2, v0, v2

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    .line 923
    .local v2, "duration":F
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const-string v5, "brightness_usage"

    invoke-direct {p0, v5, v3, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 925
    int-to-float v3, p1

    invoke-direct {p0, v3, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteHbmUsage(FF)V

    .line 926
    if-eqz p2, :cond_0

    .line 927
    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mStartTimeStamp:J

    goto :goto_0

    .line 929
    :cond_0
    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mStartTimeStamp:J

    .line 931
    :goto_0
    sget-boolean v3, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v3, :cond_1

    .line 932
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateBrightnessUsage: reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", span: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", append duration: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "BrightnessDataProcessor"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 937
    .end local v0    # "now":J
    .end local v2    # "duration":F
    :cond_1
    return-void
.end method

.method private updateBrightnessUsageIfNeeded(II)V
    .locals 2
    .param p1, "curSpanIndex"    # I
    .param p2, "preSpanIndex"    # I

    .line 903
    if-eq p1, p2, :cond_0

    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->isValidStartTimeStamp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 904
    const/4 v0, 0x0

    const-string v1, "brightness span change"

    invoke-direct {p0, p2, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateBrightnessUsage(IZLjava/lang/String;)V

    .line 907
    :cond_0
    return-void
.end method

.method private updateForegroundApps()V
    .locals 3

    .line 840
    :try_start_0
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    invoke-interface {v0}, Landroid/app/IActivityTaskManager;->getFocusedRootTaskInfo()Landroid/app/ActivityTaskManager$RootTaskInfo;

    move-result-object v0

    .line 841
    .local v0, "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    if-eqz v0, :cond_4

    iget-object v1, v0, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-nez v1, :cond_0

    goto :goto_1

    .line 845
    :cond_0
    invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_3

    .line 846
    invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I

    move-result v1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    .line 847
    invoke-interface {v1}, Landroid/app/IActivityTaskManager;->isInSplitScreenWindowingMode()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 851
    :cond_1
    iget-object v1, v0, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 854
    .local v1, "packageName":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mForegroundAppPackageName:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 855
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 856
    return-void

    .line 858
    :cond_2
    iget v2, v0, Landroid/app/ActivityTaskManager$RootTaskInfo;->userId:I

    iput v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentUserId:I

    .line 859
    iput-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mForegroundAppPackageName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 862
    .end local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    .end local v1    # "packageName":Ljava/lang/String;
    goto :goto_2

    .line 848
    .restart local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :cond_3
    :goto_0
    return-void

    .line 842
    :cond_4
    :goto_1
    return-void

    .line 860
    .end local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :catch_0
    move-exception v0

    .line 863
    :goto_2
    return-void
.end method

.method private updateInterruptBrightnessAnimDuration(IF)V
    .locals 13
    .param p1, "type"    # I
    .param p2, "brightness"    # F

    .line 1672
    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentBrightnessAnimValue:F

    .line 1674
    invoke-direct {p0, v0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessChangedState(FF)I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I

    if-ne v0, v2, :cond_2

    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentBrightnessAnimValue:F

    .line 1677
    invoke-direct {p0, v0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessChangedState(FF)I

    move-result v0

    if-ne v0, v2, :cond_2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1681
    .local v0, "isSameAdjustment":Z
    :goto_0
    const/4 v3, 0x2

    if-ne p1, v3, :cond_5

    .line 1682
    iget-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStart:Z

    const-wide/16 v4, 0x0

    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    iget-wide v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemporaryBrightnessTimeStamp:J

    cmp-long v3, v6, v4

    if-eqz v3, :cond_4

    iget-wide v8, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStartTime:J

    cmp-long v3, v8, v4

    if-eqz v3, :cond_4

    cmp-long v3, v6, v8

    if-lez v3, :cond_4

    .line 1685
    sub-long/2addr v6, v8

    .line 1686
    .local v6, "duration":J
    nop

    .line 1687
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 1686
    const-string v8, "interrupt_animation_times"

    const-string v9, "interrupt_times"

    invoke-direct {p0, v8, v9, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAdvancedBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1688
    iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    const/4 v8, 0x3

    if-ne v3, v8, :cond_3

    move v3, v2

    goto :goto_1

    :cond_3
    move v3, v1

    .line 1690
    .local v3, "brightnessRestricted":Z
    :goto_1
    new-instance v8, Lcom/android/server/display/statistics/AdvancedEvent;

    invoke-direct {v8}, Lcom/android/server/display/statistics/AdvancedEvent;-><init>()V

    .line 1691
    .local v8, "event":Lcom/android/server/display/statistics/AdvancedEvent;
    invoke-virtual {v8, v2}, Lcom/android/server/display/statistics/AdvancedEvent;->setEventType(I)Lcom/android/server/display/statistics/AdvancedEvent;

    move-result-object v2

    long-to-float v9, v6

    const v10, 0x3a83126f    # 0.001f

    mul-float/2addr v9, v10

    .line 1692
    invoke-virtual {v2, v9}, Lcom/android/server/display/statistics/AdvancedEvent;->setAutoBrightnessAnimationDuration(F)Lcom/android/server/display/statistics/AdvancedEvent;

    move-result-object v2

    iget v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentBrightnessAnimValue:F

    .line 1693
    invoke-static {v9}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v9

    invoke-virtual {v2, v9}, Lcom/android/server/display/statistics/AdvancedEvent;->setCurrentAnimateValue(I)Lcom/android/server/display/statistics/AdvancedEvent;

    move-result-object v2

    iget v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTargetBrightnessAnimValue:F

    .line 1694
    invoke-static {v9}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v9

    invoke-virtual {v2, v9}, Lcom/android/server/display/statistics/AdvancedEvent;->setTargetAnimateValue(I)Lcom/android/server/display/statistics/AdvancedEvent;

    move-result-object v2

    .line 1695
    invoke-static {p2}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v9

    invoke-virtual {v2, v9}, Lcom/android/server/display/statistics/AdvancedEvent;->setUserBrightness(I)Lcom/android/server/display/statistics/AdvancedEvent;

    move-result-object v2

    iget v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I

    .line 1696
    invoke-virtual {v2, v9}, Lcom/android/server/display/statistics/AdvancedEvent;->setBrightnessChangedState(I)Lcom/android/server/display/statistics/AdvancedEvent;

    move-result-object v2

    .line 1697
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-virtual {v2, v11, v12}, Lcom/android/server/display/statistics/AdvancedEvent;->setTimeStamp(J)Lcom/android/server/display/statistics/AdvancedEvent;

    move-result-object v2

    .line 1698
    invoke-virtual {v2, v3}, Lcom/android/server/display/statistics/AdvancedEvent;->setBrightnessRestrictedEnable(Z)Lcom/android/server/display/statistics/AdvancedEvent;

    .line 1699
    invoke-direct {p0, v8}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->reportEventToServer(Lcom/android/server/display/statistics/AdvancedEvent;)V

    .line 1700
    sget-boolean v2, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v2, :cond_4

    .line 1701
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "updateInterruptBrightnessAnimDuration: duration:"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    long-to-float v9, v6

    mul-float/2addr v9, v10

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ", currentAnimateValue:"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCurrentBrightnessAnimValue:F

    .line 1702
    invoke-static {v9}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ", targetAnimateValue:"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTargetBrightnessAnimValue:F

    .line 1703
    invoke-static {v9}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ", userBrightness:"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1704
    invoke-static {p2}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ", mBrightnessChangedState:"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v9, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessChangedState:I

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1701
    const-string v9, "BrightnessDataProcessor"

    invoke-static {v9, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1708
    .end local v3    # "brightnessRestricted":Z
    .end local v6    # "duration":J
    .end local v8    # "event":Lcom/android/server/display/statistics/AdvancedEvent;
    :cond_4
    iput-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessAnimStart:Z

    .line 1709
    iput-wide v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemporaryBrightnessTimeStamp:J

    .line 1711
    :cond_5
    return-void
.end method

.method private updateInterruptBrightnessAnimDurationIfNeeded(IF)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "brightness"    # F

    .line 1656
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1657
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 1658
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1659
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1660
    return-void
.end method

.method private updatePointerEventMotionState(ZII)V
    .locals 2
    .param p1, "dragging"    # Z
    .param p2, "distanceX"    # I
    .param p3, "distanceY"    # I

    .line 874
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mUserDragging:Z

    if-eq v0, p1, :cond_0

    .line 875
    iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mUserDragging:Z

    .line 876
    if-nez p1, :cond_0

    invoke-direct {p0, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->checkIsValidMotionForWindowBrightness(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 877
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLatestDraggingChangedTime:J

    .line 878
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mHaveValidWindowBrightness:Z

    if-eqz v0, :cond_0

    .line 879
    const-wide/16 v0, 0xbb8

    invoke-direct {p0, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->debounceBrightnessEvent(J)V

    .line 883
    :cond_0
    return-void
.end method

.method private updateScreenNits(FF)V
    .locals 2
    .param p1, "originalNit"    # F
    .param p2, "actualNit"    # F

    .line 2876
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda12;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda12;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;FF)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2884
    return-void
.end method

.method private updateScreenStateChanged(Z)V
    .locals 0
    .param p1, "screenOn"    # Z

    .line 940
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateStartTimeStamp(Z)V

    .line 941
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setPointerEventListener(Z)V

    .line 942
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setRotationListener(Z)V

    .line 943
    return-void
.end method

.method private updateStartTimeStamp(Z)V
    .locals 3
    .param p1, "screenOn"    # Z

    .line 946
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 947
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateStartTimeStamp: screenOn: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 949
    :cond_0
    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->isValidStartTimeStamp()Z

    move-result v0

    if-nez v0, :cond_1

    .line 950
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mStartTimeStamp:J

    goto :goto_0

    .line 951
    :cond_1
    if-nez p1, :cond_2

    .line 952
    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastScreenBrightness:F

    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I

    move-result v0

    const/4 v1, 0x1

    const-string v2, "screen off"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateBrightnessUsage(IZLjava/lang/String;)V

    .line 955
    :cond_2
    :goto_0
    return-void
.end method

.method private updateThermalStatsBeforeReported()V
    .locals 10

    .line 2700
    iget-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastThermalStatusTimeStamp:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 2701
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 2702
    .local v8, "now":J
    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F

    const/high16 v3, -0x40800000    # -1.0f

    iget-boolean v6, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastOutDoorHighTempState:Z

    const/4 v7, 0x1

    move-object v1, p0

    move-wide v4, v8

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteOutDoorHighTempUsage(FFJZZ)V

    .line 2703
    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastRestrictedBrightness:F

    iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastUnrestrictedBrightness:F

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalBrightnessRestrictedUsage(FFJZ)V

    .line 2704
    const/4 v2, 0x1

    iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastConditionId:I

    iget v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    const/4 v7, 0x0

    move-wide v5, v8

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteDetailThermalUsage(IIFJZ)V

    .line 2706
    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalStatus:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v8, v9, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteThermalUsage(IJZ)V

    .line 2708
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mThermalEventsMap:Ljava/util/Map;

    const-string/jumbo v1, "thermal_average_temperature"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2709
    iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastTemperature:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    const-string v2, "average"

    invoke-direct {p0, v1, v2, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateThermalEventsAvg(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2711
    :cond_0
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 2712
    const-string v0, "BrightnessDataProcessor"

    const-string/jumbo v1, "updateThermalStatsBeforeReported: update stats before reporting stats."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2715
    .end local v8    # "now":J
    :cond_1
    return-void
.end method

.method private updateUserResetAutoBrightnessModeTimes()V
    .locals 8

    .line 1525
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z

    iget-boolean v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightnessEnable:Z

    if-ne v0, v1, :cond_0

    .line 1526
    return-void

    .line 1528
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 1529
    .local v0, "now":J
    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsValidResetAutoBrightnessMode:Z

    if-eqz v2, :cond_1

    iget-wide v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastResetBrightnessModeTime:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-eqz v2, :cond_1

    sub-long v4, v0, v4

    const-wide/16 v6, 0xbb8

    cmp-long v2, v4, v6

    if-gtz v2, :cond_1

    .line 1531
    const-string v2, "reset_times"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "reset_brightness_mode_times"

    invoke-direct {p0, v5, v2, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAdvancedBrightnessEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1532
    invoke-direct {p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->flareStatisticalResetBrightnessModeTimes()V

    .line 1536
    :cond_1
    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightnessEnable:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    :goto_0
    iput-boolean v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsValidResetAutoBrightnessMode:Z

    .line 1537
    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAutoBrightnessEnable:Z

    iput-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastAutoBrightnessEnable:Z

    .line 1538
    iput-wide v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastResetBrightnessModeTime:J

    .line 1539
    sget-boolean v2, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v2, :cond_3

    .line 1540
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateUserResetAutoBrightnessModeTimes: mAdvancedBrightnessEventsMap: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAdvancedBrightnessEventsMap:Ljava/util/Map;

    .line 1541
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1540
    const-string v3, "BrightnessDataProcessor"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1543
    :cond_3
    return-void
.end method


# virtual methods
.method public aggregateCbmBrightnessAdjustTimes(IZ)V
    .locals 3
    .param p1, "cbmState"    # I
    .param p2, "isManuallySet"    # Z

    .line 2966
    invoke-static {p1, p2}, Lcom/android/server/display/statistics/AggregationEvent$CbmAggregationEvent;->getCbmAutoAdjustQuotaName(IZ)Ljava/lang/String;

    move-result-object v0

    .line 2967
    .local v0, "name":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "custom_brightness_adj"

    invoke-direct {p0, v2, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2968
    sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 2969
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "aggregateCbmBrightnessAdjustTimes: mCbmEventsMap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BrightnessDataProcessor"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2971
    :cond_0
    return-void
.end method

.method public aggregateCbmBrightnessUsageDuration(FI)V
    .locals 3
    .param p1, "duration"    # F
    .param p2, "cbmSate"    # I

    .line 2979
    const/high16 v0, 0x447a0000    # 1000.0f

    div-float/2addr p1, v0

    .line 2980
    invoke-static {p2}, Lcom/android/server/display/statistics/AggregationEvent$CbmAggregationEvent;->getCbmBrtUsageQuotaName(I)Ljava/lang/String;

    move-result-object v0

    .line 2981
    .local v0, "name":Ljava/lang/String;
    const-string v1, "custom_brightness_usage"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2982
    sget-boolean v1, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 2983
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "aggregateCbmBrightnessUsageDuration: duration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCbmEventsMap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BrightnessDataProcessor"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2986
    :cond_0
    return-void
.end method

.method public aggregateIndividualModelTrainTimes()V
    .locals 3

    .line 2992
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "individual_model_train"

    const-string v2, "model_train_count"

    invoke-direct {p0, v1, v2, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2993
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 2994
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "aggregateIndividualModelTrainTimes: mCbmEventsMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2996
    :cond_0
    return-void
.end method

.method public aggregateIndividualModelTrainTimes(Z)V
    .locals 2
    .param p1, "isSuccessful"    # Z

    .line 3003
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3012
    return-void
.end method

.method public aggregateModelAvgPredictDuration(F)V
    .locals 3
    .param p1, "duration"    # F

    .line 3032
    const-string v0, "model_predict_average_duration"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const-string v2, "individual_model_predict"

    invoke-direct {p0, v2, v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmEventsAvg(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3033
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 3034
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "aggregateModelAvgPredictDuration: duration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCbmEventsMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3037
    :cond_0
    return-void
.end method

.method public aggregateModelPredictTimeoutTimes()V
    .locals 2

    .line 3018
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3025
    return-void
.end method

.method public aggregateModelTrainIndicators(Lcom/xiaomi/aiautobrt/IndividualTrainEvent;)V
    .locals 6
    .param p1, "event"    # Lcom/xiaomi/aiautobrt/IndividualTrainEvent;

    .line 3045
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 3046
    .local v0, "indicatorsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 3048
    .local v1, "now":J
    invoke-virtual {p1}, Lcom/xiaomi/aiautobrt/IndividualTrainEvent;->getTrainSampleNum()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "sample_num"

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3049
    invoke-virtual {p1}, Lcom/xiaomi/aiautobrt/IndividualTrainEvent;->getModelTrainLoss()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    const-string/jumbo v4, "train_loss"

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3050
    invoke-virtual {p1}, Lcom/xiaomi/aiautobrt/IndividualTrainEvent;->getPreModelMAE()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    const-string v4, "pre_train_mae"

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3051
    invoke-virtual {p1}, Lcom/xiaomi/aiautobrt/IndividualTrainEvent;->getCurModelMAE()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    const-string v4, "cur_train_mae"

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3052
    invoke-virtual {p1}, Lcom/xiaomi/aiautobrt/IndividualTrainEvent;->getPreModelMAPE()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    const-string v4, "pre_train_mape"

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3053
    invoke-virtual {p1}, Lcom/xiaomi/aiautobrt/IndividualTrainEvent;->getCurModelMAPE()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    const-string v4, "cur_train_mape"

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3054
    const-string/jumbo v3, "timestamp"

    invoke-static {v1, v2}, Lcom/android/server/display/statistics/BrightnessEvent;->timestamp2String(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3055
    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mModelTrainIndicatorsList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3056
    const-string v3, "model_train_indicators"

    iget-object v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mModelTrainIndicatorsList:Ljava/util/List;

    const-string v5, "individual_model_train"

    invoke-direct {p0, v5, v3, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmEventsSync(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3057
    sget-boolean v3, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 3058
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "aggregateModelTrainIndicators: mCbmEventsMap: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCbmEventsMap:Ljava/util/Map;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "BrightnessDataProcessor"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3060
    :cond_0
    return-void
.end method

.method public createModelEvent(FIFII)Lcom/xiaomi/aiautobrt/IndividualModelEvent;
    .locals 9
    .param p1, "lux"    # F
    .param p2, "categoryId"    # I
    .param p3, "newBrightness"    # F
    .param p4, "orientation"    # I
    .param p5, "sceneState"    # I

    .line 721
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v0, p3}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v0

    .line 723
    .local v0, "currentNit":F
    invoke-direct {p0, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessSpanByNit(F)I

    move-result v1

    int-to-float v1, v1

    .line 725
    .local v1, "currentNitSpan":F
    invoke-direct {p0, p1, p5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getConfigBrightness(FI)F

    move-result v2

    .line 727
    .local v2, "defaultConfigNit":F
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getAmbientLuxSpanIndex(F)I

    move-result v3

    int-to-float v3, v3

    .line 729
    .local v3, "currentLuxSpan":F
    iget-object v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIndividualEventNormalizer:Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;

    invoke-virtual {v4, p2, p4}, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->getMixedOrientApp(II)F

    move-result v4

    .line 731
    .local v4, "mixedOrientationApp":F
    new-instance v5, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;

    invoke-direct {v5}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;-><init>()V

    .line 732
    .local v5, "builder":Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;
    invoke-virtual {v5, v0}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setCurrentBrightness(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;

    move-result-object v6

    .line 733
    invoke-virtual {v6, v1}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setCurrentBrightnessSpan(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;

    move-result-object v6

    .line 734
    invoke-virtual {v6, p1}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setAmbientLux(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;

    move-result-object v6

    .line 735
    invoke-virtual {v6, v3}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setAmbientLuxSpan(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;

    move-result-object v6

    .line 736
    invoke-virtual {v6, v2}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setDefaultConfigBrightness(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;

    move-result-object v6

    int-to-float v7, p2

    .line 737
    invoke-virtual {v6, v7}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setAppCategoryId(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;

    move-result-object v6

    int-to-float v7, p4

    .line 738
    invoke-virtual {v6, v7}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setOrientation(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;

    move-result-object v6

    .line 739
    invoke-virtual {v6, v4}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setMixedOrientationAppId(F)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;

    move-result-object v6

    .line 740
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->setTimeStamp(J)Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;

    .line 741
    invoke-virtual {v5}, Lcom/xiaomi/aiautobrt/IndividualModelEvent$Builder;->build()Lcom/xiaomi/aiautobrt/IndividualModelEvent;

    move-result-object v6

    return-object v6
.end method

.method public createModelEvent(FLjava/lang/String;FII)Lcom/xiaomi/aiautobrt/IndividualModelEvent;
    .locals 7
    .param p1, "lux"    # F
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "newBrightness"    # F
    .param p4, "orientation"    # I
    .param p5, "sceneState"    # I

    .line 714
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mAppClassifier:Lcom/android/server/display/aiautobrt/AppClassifier;

    invoke-virtual {v0, p2}, Lcom/android/server/display/aiautobrt/AppClassifier;->getAppCategoryId(Ljava/lang/String;)I

    move-result v0

    .line 715
    .local v0, "categoryId":I
    move-object v1, p0

    move v2, p1

    move v3, v0

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->createModelEvent(FIFII)Lcom/xiaomi/aiautobrt/IndividualModelEvent;

    move-result-object v1

    return-object v1
.end method

.method public forceReportTrainDataEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 2908
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda7;

    invoke-direct {v1, p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2909
    return-void
.end method

.method public noteAverageTemperature(FZ)V
    .locals 2
    .param p1, "temperature"    # F
    .param p2, "needComputed"    # Z

    .line 2573
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p2, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;ZF)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2584
    return-void
.end method

.method public noteDetailThermalUsage(IF)V
    .locals 2
    .param p1, "conditionId"    # I
    .param p2, "temperature"    # F

    .line 2330
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda10;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda10;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;IF)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2388
    return-void
.end method

.method public noteFullSceneThermalUsageStats(FFIFZ)V
    .locals 9
    .param p1, "brightness"    # F
    .param p2, "thermalBrightness"    # F
    .param p3, "conditionId"    # I
    .param p4, "temperature"    # F
    .param p5, "outdoorHighTemState"    # Z

    .line 2078
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v8, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda8;

    move-object v1, v8

    move-object v2, p0

    move v3, p2

    move v4, p1

    move v5, p5

    move v6, p3

    move v7, p4

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda8;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;FFZIF)V

    invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2098
    return-void
.end method

.method public notifyAonFlareEvents(IF)V
    .locals 7
    .param p1, "type"    # I
    .param p2, "preLux"    # F

    .line 3078
    const-wide/32 v0, 0xea60

    const-string v2, "flare_scene_check_times"

    const/16 v3, 0xf

    const/4 v4, 0x1

    .line 3096
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 3078
    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 3103
    :pswitch_0
    nop

    .line 3104
    nop

    .line 3103
    const-string v6, "3"

    invoke-direct {p0, v2, v6, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3105
    iput-boolean v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z

    .line 3106
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 3107
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 3109
    goto :goto_0

    .line 3095
    :pswitch_1
    nop

    .line 3096
    nop

    .line 3095
    const-string v6, "2"

    invoke-direct {p0, v2, v6, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3097
    iput-boolean v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z

    .line 3098
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 3099
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 3101
    goto :goto_0

    .line 3080
    :pswitch_2
    sget-boolean v3, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 3081
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "notifyAonFlareEvents: preLux: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v6, "BrightnessDataProcessor"

    invoke-static {v6, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3083
    :cond_0
    nop

    .line 3084
    nop

    .line 3083
    const-string v3, "1"

    invoke-direct {p0, v2, v3, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3085
    nop

    .line 3086
    invoke-direct {p0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getAmbientLuxSpanIndex(F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 3085
    const-string v3, "flare_suppress_darken_lux_span"

    invoke-direct {p0, v3, v2, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3087
    nop

    .line 3088
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getHourFromTimestamp(J)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 3087
    const-string v3, "flare_suppress_darken_hour"

    invoke-direct {p0, v3, v2, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateAonFlareEventsSum(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3089
    iput-boolean v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z

    .line 3090
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    const/16 v3, 0xe

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 3091
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 3093
    nop

    .line 3113
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public notifyBrightnessEventIfNeeded(ZFFZZFZFFZZZZFFI)V
    .locals 26
    .param p1, "screenOn"    # Z
    .param p2, "originalBrightness"    # F
    .param p3, "actualBrightness"    # F
    .param p4, "userInitiatedChange"    # Z
    .param p5, "useAutoBrightness"    # Z
    .param p6, "brightnessOverrideFromWindow"    # F
    .param p7, "lowPowerMode"    # Z
    .param p8, "ambientLux"    # F
    .param p9, "userDataPoint"    # F
    .param p10, "defaultConfig"    # Z
    .param p11, "sunlightActive"    # Z
    .param p12, "isHdrLayer"    # Z
    .param p13, "isDimmingChanged"    # Z
    .param p14, "mainFastAmbientLux"    # F
    .param p15, "assistFastAmbientLux"    # F
    .param p16, "sceneState"    # I

    .line 461
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v15, p2

    move/from16 v13, p3

    move/from16 v12, p4

    move/from16 v11, p5

    move/from16 v10, p6

    move/from16 v9, p11

    invoke-direct {v0, v12, v11, v10, v9}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessType(ZZFZ)I

    move-result v8

    .line 464
    .local v8, "type":I
    const/4 v7, 0x1

    const/4 v2, 0x2

    if-eq v8, v2, :cond_0

    if-eq v8, v7, :cond_0

    .line 466
    const/high16 v3, 0x7fc00000    # Float.NaN

    move/from16 v21, v3

    .end local p8    # "ambientLux":F
    .local v3, "ambientLux":F
    goto :goto_0

    .line 469
    .end local v3    # "ambientLux":F
    .restart local p8    # "ambientLux":F
    :cond_0
    move/from16 v21, p8

    .end local p8    # "ambientLux":F
    .local v21, "ambientLux":F
    :goto_0
    iget-object v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    .line 470
    invoke-direct {v0, v15}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->clampBrightness(F)F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v3

    .line 471
    .local v3, "originalNit":F
    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v4

    sget-object v5, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    .line 472
    invoke-virtual {v4, v2, v5}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigDecimal;->floatValue()F

    move-result v6

    .line 474
    .end local v3    # "originalNit":F
    .local v6, "originalNit":F
    iget-object v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    .line 475
    invoke-direct {v0, v13}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->clampBrightness(F)F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v3

    .line 476
    .local v3, "actualNit":F
    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v4

    sget-object v5, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    .line 477
    invoke-virtual {v4, v2, v5}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->floatValue()F

    move-result v5

    .line 479
    .end local v3    # "actualNit":F
    .local v5, "actualNit":F
    invoke-direct {v0, v1, v13}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->scheduleUpdateBrightnessStatisticsData(ZF)V

    .line 481
    move/from16 v4, p12

    invoke-direct {v0, v4}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->startHdyUsageStats(Z)V

    .line 483
    invoke-direct {v0, v6, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateScreenNits(FF)V

    .line 485
    iget-object v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    if-eqz v2, :cond_2

    iget v2, v2, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;->originalBrightness:F

    .line 487
    invoke-static {v2, v15}, Lcom/android/internal/display/BrightnessSynchronizer;->floatEquals(FF)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    move/from16 v23, v5

    move/from16 v24, v6

    move/from16 v25, v8

    goto/16 :goto_2

    :cond_2
    :goto_1
    if-nez p13, :cond_6

    iget-object v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 489
    invoke-interface {v2}, Lcom/android/server/policy/WindowManagerPolicy;->isKeyguardShowingAndNotOccluded()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 490
    invoke-interface {v2}, Lcom/android/server/policy/WindowManagerPolicy;->isKeyguardShowing()Z

    move-result v2

    if-nez v2, :cond_4

    if-nez v1, :cond_3

    move/from16 v23, v5

    move/from16 v24, v6

    move/from16 v25, v8

    goto :goto_2

    .line 495
    :cond_3
    invoke-direct {v0, v8, v13}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateInterruptBrightnessAnimDurationIfNeeded(IF)V

    .line 497
    new-instance v22, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    move-object/from16 v2, v22

    iget-object v14, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mForegroundAppPackageName:Ljava/lang/String;

    iget v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOrientation:I

    move/from16 v19, v3

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v23, v5

    .end local v5    # "actualNit":F
    .local v23, "actualNit":F
    move/from16 v5, p4

    move/from16 v24, v6

    .end local v6    # "originalNit":F
    .local v24, "originalNit":F
    move/from16 v6, p5

    move v1, v7

    move/from16 v7, p6

    move/from16 v25, v8

    .end local v8    # "type":I
    .local v25, "type":I
    move/from16 v8, p7

    move/from16 v9, p11

    move/from16 v10, v21

    move/from16 v11, p9

    move/from16 v12, p10

    move/from16 v13, v25

    move/from16 v15, p14

    move/from16 v16, p15

    move/from16 v17, v24

    move/from16 v18, v23

    move/from16 v20, p16

    invoke-direct/range {v2 .. v20}, Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;-><init>(FFZZFZZFFZILjava/lang/String;FFFFII)V

    .line 503
    .local v2, "item":Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;
    iput-object v2, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mLastBrightnessChangeItem:Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;

    .line 504
    iget-object v3, v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    invoke-static {v3, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 505
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 506
    return-void

    .line 490
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "item":Lcom/android/server/display/statistics/BrightnessDataProcessor$BrightnessChangeItem;
    .end local v23    # "actualNit":F
    .end local v24    # "originalNit":F
    .end local v25    # "type":I
    .restart local v5    # "actualNit":F
    .restart local v6    # "originalNit":F
    .restart local v8    # "type":I
    :cond_4
    move/from16 v23, v5

    move/from16 v24, v6

    move/from16 v25, v8

    .end local v5    # "actualNit":F
    .end local v6    # "originalNit":F
    .end local v8    # "type":I
    .restart local v23    # "actualNit":F
    .restart local v24    # "originalNit":F
    .restart local v25    # "type":I
    goto :goto_2

    .line 489
    .end local v23    # "actualNit":F
    .end local v24    # "originalNit":F
    .end local v25    # "type":I
    .restart local v5    # "actualNit":F
    .restart local v6    # "originalNit":F
    .restart local v8    # "type":I
    :cond_5
    move/from16 v23, v5

    move/from16 v24, v6

    move/from16 v25, v8

    .end local v5    # "actualNit":F
    .end local v6    # "originalNit":F
    .end local v8    # "type":I
    .restart local v23    # "actualNit":F
    .restart local v24    # "originalNit":F
    .restart local v25    # "type":I
    goto :goto_2

    .line 487
    .end local v23    # "actualNit":F
    .end local v24    # "originalNit":F
    .end local v25    # "type":I
    .restart local v5    # "actualNit":F
    .restart local v6    # "originalNit":F
    .restart local v8    # "type":I
    :cond_6
    move/from16 v23, v5

    move/from16 v24, v6

    move/from16 v25, v8

    .line 492
    .end local v5    # "actualNit":F
    .end local v6    # "originalNit":F
    .end local v8    # "type":I
    .restart local v23    # "actualNit":F
    .restart local v24    # "originalNit":F
    .restart local v25    # "type":I
    :goto_2
    return-void
.end method

.method public notifyResetBrightnessAnimInfo()V
    .locals 2

    .line 1749
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingAnimationStart:Z

    .line 1750
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1751
    return-void
.end method

.method public notifyUpdateBrightness()V
    .locals 1

    .line 3116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsAonSuppressDarken:Z

    .line 3117
    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsNotAonSuppressDarken:Z

    .line 3118
    return-void
.end method

.method public notifyUpdateBrightnessAnimInfo(FFF)V
    .locals 5
    .param p1, "currentBrightnessAnim"    # F
    .param p2, "brightnessAnim"    # F
    .param p3, "targetBrightnessAnim"    # F

    .line 1594
    cmpl-float v0, p2, p3

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1595
    .local v0, "begin":Z
    :goto_0
    invoke-direct {p0, p1, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getBrightnessChangedState(FF)I

    move-result v1

    .line 1599
    .local v1, "state":I
    iget-boolean v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingAnimationStart:Z

    if-ne v0, v2, :cond_2

    const/4 v2, 0x3

    const/4 v3, 0x2

    if-eqz v0, :cond_1

    iget v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangedState:I

    if-eq v4, v3, :cond_1

    if-eq v4, v2, :cond_1

    if-eq v1, v3, :cond_1

    if-eq v1, v2, :cond_1

    if-eq v1, v4, :cond_1

    goto :goto_1

    .line 1615
    :cond_1
    if-eqz v0, :cond_3

    iget v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangedState:I

    if-eq v4, v3, :cond_3

    if-eq v4, v2, :cond_3

    if-eq v1, v3, :cond_3

    if-eq v1, v2, :cond_3

    if-ne v1, v4, :cond_3

    iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingTargetBrightnessAnimValue:F

    cmpl-float v2, p3, v2

    if-eqz v2, :cond_3

    .line 1621
    iput p3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingTargetBrightnessAnimValue:F

    .line 1622
    iget-object v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 1623
    .local v2, "msg":Landroid/os/Message;
    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iput-object v3, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1624
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_2

    .line 1605
    .end local v2    # "msg":Landroid/os/Message;
    :cond_2
    :goto_1
    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingAnimationStart:Z

    .line 1606
    iput v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingBrightnessChangedState:I

    .line 1607
    iput p3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingTargetBrightnessAnimValue:F

    .line 1608
    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    move-result-object v2

    .line 1609
    .local v2, "args":Lcom/android/internal/os/SomeArgs;
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iput-object v3, v2, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    .line 1610
    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iput-object v3, v2, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    .line 1611
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    .line 1612
    iget-object v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    const/16 v4, 0x8

    invoke-virtual {v3, v4, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    .line 1615
    .end local v2    # "args":Lcom/android/internal/os/SomeArgs;
    :cond_3
    nop

    .line 1626
    :goto_2
    return-void
.end method

.method public notifyUpdateTempBrightnessTimeStampIfNeeded(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 1577
    iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsTemporaryBrightnessAdjustment:Z

    if-eq p1, v0, :cond_0

    .line 1578
    iput-boolean p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIsTemporaryBrightnessAdjustment:Z

    .line 1579
    if-eqz p1, :cond_0

    .line 1580
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mPendingAnimationStart:Z

    .line 1581
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1584
    :cond_0
    return-void
.end method

.method public onCloudUpdated(JLjava/util/Map;)V
    .locals 4
    .param p1, "summary"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1056
    .local p3, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-wide/16 v0, 0x1

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mReportBrightnessEventsEnable:Z

    .line 1058
    return-void
.end method

.method public setBrightnessConfiguration(Landroid/hardware/display/BrightnessConfiguration;Landroid/hardware/display/BrightnessConfiguration;Landroid/hardware/display/BrightnessConfiguration;)V
    .locals 1
    .param p1, "defaultSceneConfig"    # Landroid/hardware/display/BrightnessConfiguration;
    .param p2, "darkeningSceneConfig"    # Landroid/hardware/display/BrightnessConfiguration;
    .param p3, "brighteningSceneConfig"    # Landroid/hardware/display/BrightnessConfiguration;

    .line 1784
    invoke-direct {p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getSpline(Landroid/hardware/display/BrightnessConfiguration;)Landroid/util/Spline;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDefaultSceneSpline:Landroid/util/Spline;

    .line 1785
    invoke-direct {p0, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getSpline(Landroid/hardware/display/BrightnessConfiguration;)Landroid/util/Spline;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDarkeningSceneSpline:Landroid/util/Spline;

    .line 1786
    invoke-direct {p0, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getSpline(Landroid/hardware/display/BrightnessConfiguration;)Landroid/util/Spline;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrighteningSceneSpline:Landroid/util/Spline;

    .line 1787
    return-void
.end method

.method public setBrightnessMapper(Lcom/android/server/display/BrightnessMappingStrategy;)V
    .locals 2
    .param p1, "brightnessMapper"    # Lcom/android/server/display/BrightnessMappingStrategy;

    .line 1766
    iput-object p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    .line 1767
    if-eqz p1, :cond_0

    .line 1768
    invoke-virtual {p1}, Lcom/android/server/display/BrightnessMappingStrategy;->getDefaultConfig()Landroid/hardware/display/BrightnessConfiguration;

    move-result-object v0

    .line 1769
    .local v0, "config":Landroid/hardware/display/BrightnessConfiguration;
    invoke-direct {p0, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->getSpline(Landroid/hardware/display/BrightnessConfiguration;)Landroid/util/Spline;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mOriSpline:Landroid/util/Spline;

    .line 1771
    .end local v0    # "config":Landroid/hardware/display/BrightnessConfiguration;
    :cond_0
    return-void
.end method

.method public setDisplayDeviceConfig(Lcom/android/server/display/DisplayDeviceConfig;)V
    .locals 0
    .param p1, "config"    # Lcom/android/server/display/DisplayDeviceConfig;

    .line 1762
    iput-object p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    .line 1763
    return-void
.end method

.method public setExpId(I)V
    .locals 0
    .param p1, "id"    # I

    .line 2867
    iput p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mExpId:I

    .line 2868
    return-void
.end method

.method public setModelEventCallback(Lcom/android/server/display/statistics/BrightnessDataProcessor$ModelEventCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/android/server/display/statistics/BrightnessDataProcessor$ModelEventCallback;

    .line 745
    iput-object p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mModelEventCallback:Lcom/android/server/display/statistics/BrightnessDataProcessor$ModelEventCallback;

    .line 746
    return-void
.end method

.method public setUpCloudControllerListener(Lcom/android/server/display/AutomaticBrightnessControllerImpl$CloudControllerListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/server/display/AutomaticBrightnessControllerImpl$CloudControllerListener;

    .line 1758
    iput-object p1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mCloudControllerListener:Lcom/android/server/display/AutomaticBrightnessControllerImpl$CloudControllerListener;

    .line 1759
    return-void
.end method

.method public thermalConfigChanged(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;",
            ">;)V"
        }
    .end annotation

    .line 1066
    .local p1, "item":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    .line 1067
    .local v1, "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
    invoke-virtual {v1}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;

    move-result-object v2

    .line 1068
    .local v2, "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;

    .line 1069
    .local v4, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    invoke-virtual {v4}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMinInclusive()F

    move-result v5

    .line 1070
    .local v5, "minTemp":F
    invoke-virtual {v4}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMaxExclusive()F

    move-result v6

    .line 1071
    .local v6, "maxTemp":F
    iget-object v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1072
    iget-object v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1074
    :cond_0
    iget-object v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1075
    iget-object v7, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1077
    .end local v4    # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    .end local v5    # "minTemp":F
    .end local v6    # "maxTemp":F
    :cond_1
    goto :goto_1

    .line 1078
    .end local v1    # "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
    .end local v2    # "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;"
    :cond_2
    goto :goto_0

    .line 1079
    :cond_3
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_4

    .line 1080
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1082
    :cond_4
    sget-boolean v0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->DEBUG:Z

    if-eqz v0, :cond_5

    .line 1083
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "thermalConfigChanged: mTemperatureSpan: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mTemperatureSpan:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrightnessDataProcessor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1085
    :cond_5
    return-void
.end method

.method public updateExpId(I)V
    .locals 2
    .param p1, "expId"    # I

    .line 1790
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda11;

    invoke-direct {v1, p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda11;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1791
    return-void
.end method

.method public updateGrayScale(F)V
    .locals 2
    .param p1, "grayScale"    # F

    .line 766
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;F)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 767
    return-void
.end method

.method public updateThermalStats(FZFZ)V
    .locals 8
    .param p1, "brightnessState"    # F
    .param p2, "isScreenOn"    # Z
    .param p3, "temperature"    # F
    .param p4, "needComputed"    # Z

    .line 2547
    iget-object v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda2;

    move-object v1, v7

    move-object v2, p0

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/android/server/display/statistics/BrightnessDataProcessor$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/statistics/BrightnessDataProcessor;ZFZF)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2565
    return-void
.end method
