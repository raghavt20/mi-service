class com.android.server.display.statistics.BrightnessDataProcessor$WindowOverlayEventListener implements android.view.WindowManagerPolicyConstants$PointerEventListener {
	 /* .source "BrightnessDataProcessor.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/statistics/BrightnessDataProcessor; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "WindowOverlayEventListener" */
} // .end annotation
/* # instance fields */
Integer distance_x;
Integer distance_y;
Float eventXDown;
Float eventXUp;
Float eventYDown;
Float eventYUp;
final com.android.server.display.statistics.BrightnessDataProcessor this$0; //synthetic
Boolean userDragging;
/* # direct methods */
 com.android.server.display.statistics.BrightnessDataProcessor$WindowOverlayEventListener ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/display/statistics/BrightnessDataProcessor; */
/* .line 1333 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1335 */
/* const/high16 v0, -0x40800000 # -1.0f */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXDown:F */
/* .line 1336 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYDown:F */
/* .line 1337 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXUp:F */
/* .line 1338 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYUp:F */
return;
} // .end method
/* # virtual methods */
public void onPointerEvent ( android.view.MotionEvent p0 ) {
/* .locals 5 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 1345 */
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
/* packed-switch v0, :pswitch_data_0 */
/* .line 1351 */
/* :pswitch_0 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->userDragging:Z */
/* .line 1352 */
/* .line 1354 */
/* :pswitch_1 */
v0 = (( android.view.MotionEvent ) p1 ).getX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXUp:F */
/* .line 1355 */
v0 = (( android.view.MotionEvent ) p1 ).getY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYUp:F */
/* .line 1356 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->userDragging:Z */
/* .line 1357 */
/* .line 1347 */
/* :pswitch_2 */
v0 = (( android.view.MotionEvent ) p1 ).getX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXDown:F */
/* .line 1348 */
v0 = (( android.view.MotionEvent ) p1 ).getY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYDown:F */
/* .line 1349 */
/* nop */
/* .line 1362 */
} // :goto_0
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->userDragging:Z */
v1 = this.this$0;
v1 = com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fgetmUserDragging ( v1 );
/* if-eq v0, v1, :cond_1 */
/* .line 1363 */
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fgetmBackgroundHandler ( v0 );
int v1 = 2; // const/4 v1, 0x2
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1364 */
/* iget-boolean v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->userDragging:Z */
/* if-nez v0, :cond_0 */
/* .line 1365 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXUp:F */
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXDown:F */
/* sub-float/2addr v0, v2 */
v0 = android.util.MathUtils .abs ( v0 );
/* float-to-int v0, v0 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->distance_x:I */
/* .line 1366 */
/* iget v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYUp:F */
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYDown:F */
/* sub-float/2addr v0, v2 */
v0 = android.util.MathUtils .abs ( v0 );
/* float-to-int v0, v0 */
/* iput v0, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->distance_y:I */
/* .line 1367 */
v0 = com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$sfgetDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1368 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onPointerEvent: x_down: "; // const-string v2, "onPointerEvent: x_down: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXDown:F */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", y_down: "; // const-string v2, ", y_down: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYDown:F */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", x_up: "; // const-string v2, ", x_up: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventXUp:F */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", y_up: "; // const-string v2, ", y_up: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->eventYUp:F */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", distance_x: "; // const-string v2, ", distance_x: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->distance_x:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", distance_y: "; // const-string v2, ", distance_y: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->distance_y:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BrightnessDataProcessor"; // const-string v2, "BrightnessDataProcessor"
android.util.Slog .d ( v2,v0 );
/* .line 1375 */
} // :cond_0
v0 = this.this$0;
com.android.server.display.statistics.BrightnessDataProcessor .-$$Nest$fgetmBackgroundHandler ( v0 );
/* iget v2, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->distance_x:I */
/* iget v3, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->distance_y:I */
/* iget-boolean v4, p0, Lcom/android/server/display/statistics/BrightnessDataProcessor$WindowOverlayEventListener;->userDragging:Z */
/* .line 1376 */
java.lang.Boolean .valueOf ( v4 );
/* .line 1375 */
android.os.Message .obtain ( v0,v1,v2,v3,v4 );
/* .line 1377 */
/* .local v0, "message":Landroid/os/Message; */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 1379 */
} // .end local v0 # "message":Landroid/os/Message;
} // :cond_1
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
