class com.android.server.display.DisplayPowerControllerImpl$Injector {
	 /* .source "DisplayPowerControllerImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/DisplayPowerControllerImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "Injector" */
} // .end annotation
/* # instance fields */
private android.widget.Toast mOutdoorHighTempToast;
/* # direct methods */
public com.android.server.display.DisplayPowerControllerImpl$Injector ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 2185 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 2186 */
/* const v0, 0x110f0232 */
(( android.content.Context ) p1 ).getString ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;
int v1 = 0; // const/4 v1, 0x0
android.widget.Toast .makeText ( p1,v0,v1 );
this.mOutdoorHighTempToast = v0;
/* .line 2188 */
return;
} // .end method
/* # virtual methods */
public void showOutdoorHighTempToast ( ) {
/* .locals 1 */
/* .line 2191 */
v0 = this.mOutdoorHighTempToast;
(( android.widget.Toast ) v0 ).cancel ( ); // invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V
/* .line 2192 */
v0 = this.mOutdoorHighTempToast;
(( android.widget.Toast ) v0 ).show ( ); // invoke-virtual {v0}, Landroid/widget/Toast;->show()V
/* .line 2193 */
return;
} // .end method
