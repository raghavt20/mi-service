public class com.android.server.display.ThermalBrightnessController implements com.android.server.display.MiuiDisplayCloudController$CloudListener {
	 /* .source "ThermalBrightnessController.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/ThermalBrightnessController$Callback;, */
	 /* Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;, */
	 /* Lcom/android/server/display/ThermalBrightnessController$ThermalListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CLOUD_BACKUP_CONFIG_FILE;
private static final java.lang.String CLOUD_BACKUP_FILE_NAME;
private static final java.lang.String CLOUD_BAKUP_FILE_TEMP_GAP_VALUE;
private static final Integer CONDITION_ID_FOR_NTC_THERMAL;
protected static Boolean DEBUG;
private static final java.lang.String DEFAULT_CONFIG_FILE;
private static final java.lang.String DOLBY_OVERRIDE_DESC;
private static final java.lang.String ETC_DIR;
private static final Integer EVENT_CONDITION_CHANGE;
private static final Integer EVENT_NTC_TEMPERATURE_CHANGE;
private static final Integer EVENT_OUTDOOR_CHANGE;
private static final Integer EVENT_SAFETY_BRIGHTNESS_CHANGE;
private static final Integer EVENT_TEMPERATURE_CHANGE;
private static final java.lang.String FILE_NTC_TEMPERATURE;
private static final java.lang.String FILE_SAFETY_BRIGHTNESS;
private static final java.lang.String FILE_SKIN_TEMPERATURE;
private static final java.lang.String FILE_THERMAL_CONDITION_ID;
private static final java.lang.String MAX_BRIGHTNESS_FILE;
private static final Integer NTC_TEMPERATURE_GAP;
private static final Integer OUTDOOR_THERMAL_ID;
private static final java.lang.String TAG;
private static final java.lang.String THERMAL_BRIGHTNESS_CONFIG_DIR;
private static final java.lang.String THERMAL_CONFIG_DIR;
/* # instance fields */
private android.os.Handler mBackgroundHandler;
private android.util.SparseArrayMap mCacheInfo;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArrayMap<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.display.ThermalBrightnessController$Callback mCallback;
private android.util.SparseArray mConditionIdMaps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private volatile Integer mCurrentActualThermalConditionId;
private volatile Float mCurrentAppliedNtcTemperature;
private volatile Float mCurrentAppliedTemperature;
private volatile Integer mCurrentAppliedThermalConditionId;
private com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem mCurrentCondition;
private Float mCurrentMaxThermalBrightness;
private volatile Float mCurrentNtcTemperature;
private volatile Float mCurrentTemperature;
private com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem mDefaultCondition;
private com.android.server.display.DisplayPowerControllerImpl mDisplayPowerControllerImpl;
private java.lang.String mForegroundAppPackageName;
private volatile Boolean mIsDolbyEnabled;
private volatile Boolean mIsHdrLayerPresent;
private volatile Boolean mIsInOutdoorHighTempState;
private Boolean mIsThermalBrightnessBoostEnable;
private final java.lang.Runnable mLoadFileRunnable;
private java.lang.String mLoadedFileFrom;
private final Float mMinBrightnessInOutdoorHighTemperature;
private android.util.SparseArray mMiniTemperatureThresholds;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private volatile Boolean mNeedOverrideCondition;
private Boolean mNeedThermalBrightnessBoost;
private com.android.server.display.OutdoorDetector mOutdoorDetector;
private java.util.List mOutdoorThermalAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem mOverrideCondition;
private volatile Boolean mScreenOn;
private final com.android.server.display.ThermalBrightnessController$SettingsObserver mSettingsObserver;
private Boolean mSunlightModeActive;
private Float mTemperatureGap;
private final Float mTemperatureLevelCritical;
private final Float mTemperatureLevelEmergency;
private final Boolean mThermalBrightnessControlNtcAvailable;
protected java.util.List mThermalConditions;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.ArrayList mThermalListener;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/display/ThermalBrightnessController$ThermalListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Float mThermalSafetyBrightness;
private final Float mThresholdLuxEnterOutdoor;
private final java.lang.Runnable mUpdateConditionRunnable;
private final java.lang.Runnable mUpdateNtcTemperatureRunnable;
private final java.lang.Runnable mUpdateOutdoorRunnable;
private final java.lang.Runnable mUpdateOverrideConditionRunnable;
private final java.lang.Runnable mUpdateSafetyBrightnessRunnable;
private final java.lang.Runnable mUpdateTemperatureRunnable;
private Boolean mUseAutoBrightness;
/* # direct methods */
public static void $r8$lambda$3iYN90ot-OIokKpGlYHa4LZho9k ( com.android.server.display.ThermalBrightnessController p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->lambda$new$1()V */
return;
} // .end method
public static void $r8$lambda$FtypDuq4GmSt8ostCcfZC1q7pN8 ( com.android.server.display.ThermalBrightnessController p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->lambda$new$2()V */
return;
} // .end method
public static void $r8$lambda$O01v36ukOCBTPjYfLnM7YyQ0qQg ( com.android.server.display.ThermalBrightnessController p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateOverrideCondition()V */
return;
} // .end method
public static void $r8$lambda$SZExmViQJCLUesVQIetdi6Kw-s4 ( com.android.server.display.ThermalBrightnessController p0, com.android.server.display.ThermalBrightnessController$ThermalListener p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/ThermalBrightnessController;->lambda$loadThermalConfig$0(Lcom/android/server/display/ThermalBrightnessController$ThermalListener;)V */
return;
} // .end method
public static void $r8$lambda$fn6uzVMbXafkmh4Vuc_e_CzYnX4 ( com.android.server.display.ThermalBrightnessController p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->lambda$new$4()V */
return;
} // .end method
public static void $r8$lambda$nhkyG9-ejBfhl-OiobIMzG7eovg ( com.android.server.display.ThermalBrightnessController p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->lambda$new$3()V */
return;
} // .end method
public static void $r8$lambda$uy36Q3EV5YGq7Y7Ale1g419nQG8 ( com.android.server.display.ThermalBrightnessController p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->loadThermalConfig()V */
return;
} // .end method
static android.os.Handler -$$Nest$fgetmBackgroundHandler ( com.android.server.display.ThermalBrightnessController p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBackgroundHandler;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.display.ThermalBrightnessController p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static java.lang.Runnable -$$Nest$fgetmUpdateOutdoorRunnable ( com.android.server.display.ThermalBrightnessController p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mUpdateOutdoorRunnable;
} // .end method
static Boolean -$$Nest$fgetmUseAutoBrightness ( com.android.server.display.ThermalBrightnessController p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/display/ThermalBrightnessController;->mUseAutoBrightness:Z */
} // .end method
static void -$$Nest$fputmUseAutoBrightness ( com.android.server.display.ThermalBrightnessController p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUseAutoBrightness:Z */
return;
} // .end method
public com.android.server.display.ThermalBrightnessController ( ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .param p3, "callback" # Lcom/android/server/display/ThermalBrightnessController$Callback; */
/* .param p4, "displayPowerControllerImpl" # Lcom/android/server/display/DisplayPowerControllerImpl; */
/* .line 175 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 83 */
final String v0 = "null"; // const-string v0, "null"
this.mLoadedFileFrom = v0;
/* .line 101 */
/* const/high16 v0, 0x7fc00000 # Float.NaN */
/* iput v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentMaxThermalBrightness:F */
/* .line 117 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mThermalConditions = v1;
/* .line 119 */
/* new-instance v1, Landroid/util/SparseArrayMap; */
/* invoke-direct {v1}, Landroid/util/SparseArrayMap;-><init>()V */
this.mCacheInfo = v1;
/* .line 121 */
/* new-instance v1, Landroid/util/SparseArray; */
/* invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V */
this.mMiniTemperatureThresholds = v1;
/* .line 123 */
/* new-instance v1, Landroid/util/SparseArray; */
/* invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V */
this.mConditionIdMaps = v1;
/* .line 149 */
/* iput v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalSafetyBrightness:F */
/* .line 155 */
/* const/high16 v0, 0x3f000000 # 0.5f */
/* iput v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureGap:F */
/* .line 156 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mThermalListener = v0;
/* .line 462 */
/* new-instance v0, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V */
this.mLoadFileRunnable = v0;
/* .line 464 */
/* new-instance v0, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda1; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V */
this.mUpdateOverrideConditionRunnable = v0;
/* .line 466 */
/* new-instance v0, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda2; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V */
this.mUpdateOutdoorRunnable = v0;
/* .line 467 */
/* new-instance v0, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda3; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V */
this.mUpdateConditionRunnable = v0;
/* .line 468 */
/* new-instance v0, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda4; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V */
this.mUpdateTemperatureRunnable = v0;
/* .line 469 */
/* new-instance v0, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda5; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V */
this.mUpdateSafetyBrightnessRunnable = v0;
/* .line 470 */
/* new-instance v0, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda6; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V */
this.mUpdateNtcTemperatureRunnable = v0;
/* .line 176 */
this.mContext = p1;
/* .line 177 */
/* new-instance v0, Landroid/os/Handler; */
com.android.internal.os.BackgroundThread .get ( );
(( com.android.internal.os.BackgroundThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mBackgroundHandler = v0;
/* .line 178 */
this.mCallback = p3;
/* .line 179 */
this.mDisplayPowerControllerImpl = p4;
/* .line 180 */
/* new-instance v0, Lcom/android/server/display/ThermalBrightnessController$SettingsObserver; */
v1 = this.mBackgroundHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;-><init>(Lcom/android/server/display/ThermalBrightnessController;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 182 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 183 */
/* const v2, 0x11070038 */
v1 = (( android.content.res.Resources ) v1 ).getFloat ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelEmergency:F */
/* .line 184 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 185 */
/* const v2, 0x11070037 */
v1 = (( android.content.res.Resources ) v1 ).getFloat ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelCritical:F */
/* .line 186 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 187 */
/* const v2, 0x11070035 */
v1 = (( android.content.res.Resources ) v1 ).getFloat ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mMinBrightnessInOutdoorHighTemperature:F */
/* .line 188 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 189 */
/* const v2, 0x1107003a */
v1 = (( android.content.res.Resources ) v1 ).getFloat ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThresholdLuxEnterOutdoor:F */
/* .line 190 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 191 */
/* const v3, 0x11050088 */
v2 = (( android.content.res.Resources ) v2 ).getBoolean ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalBrightnessControlNtcAvailable:Z */
/* .line 192 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 193 */
/* const v3, 0x11050086 */
v2 = (( android.content.res.Resources ) v2 ).getBoolean ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsThermalBrightnessBoostEnable:Z */
/* .line 195 */
/* new-instance v2, Lcom/android/server/display/OutdoorDetector; */
/* invoke-direct {v2, p1, p0, v1}, Lcom/android/server/display/OutdoorDetector;-><init>(Landroid/content/Context;Lcom/android/server/display/ThermalBrightnessController;F)V */
this.mOutdoorDetector = v2;
/* .line 196 */
v1 = this.mContext;
/* .line 197 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 196 */
final String v2 = "screen_brightness_mode"; // const-string v2, "screen_brightness_mode"
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
v1 = android.provider.Settings$System .getIntForUser ( v1,v2,v3,v4 );
/* .line 199 */
/* .local v1, "screenBrightnessModeSetting":I */
int v4 = 1; // const/4 v4, 0x1
/* if-ne v1, v4, :cond_0 */
} // :cond_0
/* move v4, v3 */
} // :goto_0
/* iput-boolean v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mUseAutoBrightness:Z */
/* .line 202 */
v4 = this.mBackgroundHandler;
/* new-instance v5, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda0; */
/* invoke-direct {v5, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V */
(( android.os.Handler ) v4 ).post ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 203 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 204 */
android.provider.Settings$System .getUriFor ( v2 );
/* .line 203 */
int v5 = -1; // const/4 v5, -0x1
(( android.content.ContentResolver ) v4 ).registerContentObserver ( v2, v3, v0, v5 ); // invoke-virtual {v4, v2, v3, v0, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 207 */
/* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->nativeInit()V */
/* .line 208 */
return;
} // .end method
private void formatDumpTemperatureBrightnessPair ( java.util.List p0, java.io.PrintWriter p1 ) {
/* .locals 13 */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;", */
/* ">;", */
/* "Ljava/io/PrintWriter;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 839 */
/* .local p1, "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;" */
final String v0 = " temperature-brightness pair: "; // const-string v0, " temperature-brightness pair: "
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 840 */
int v0 = 0; // const/4 v0, 0x0
/* .line 841 */
/* .local v0, "sbTemperature":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .line 842 */
/* .local v1, "sbNit":Ljava/lang/StringBuilder; */
int v2 = 1; // const/4 v2, 0x1
/* .line 843 */
/* .local v2, "needsHeaders":Z */
final String v3 = ""; // const-string v3, ""
/* .line 844 */
v4 = /* .local v3, "separator":Ljava/lang/String; */
/* .line 845 */
/* .local v4, "size":I */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
v6 = } // :goto_0
/* if-ge v5, v6, :cond_3 */
/* .line 846 */
/* check-cast v6, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
/* .line 847 */
/* .local v6, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 848 */
/* new-instance v7, Ljava/lang/StringBuilder; */
final String v8 = " temperature: "; // const-string v8, " temperature: "
/* invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* move-object v0, v7 */
/* .line 849 */
/* new-instance v7, Ljava/lang/StringBuilder; */
final String v8 = " nit: "; // const-string v8, " nit: "
/* invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* move-object v1, v7 */
/* .line 850 */
int v2 = 0; // const/4 v2, 0x0
/* .line 853 */
} // :cond_0
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 854 */
final String v8 = "["; // const-string v8, "["
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 855 */
v8 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v6 ).getMinInclusive ( ); // invoke-virtual {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMinInclusive()F
com.android.server.display.ThermalBrightnessController .toStrFloatForDump ( v8 );
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 856 */
final String v8 = ","; // const-string v8, ","
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 857 */
v8 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v6 ).getMaxExclusive ( ); // invoke-virtual {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMaxExclusive()F
com.android.server.display.ThermalBrightnessController .toStrFloatForDump ( v8 );
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 858 */
final String v8 = ")"; // const-string v8, ")"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 859 */
/* .local v7, "strTemperature":Ljava/lang/String; */
v8 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v6 ).getNit ( ); // invoke-virtual {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getNit()F
com.android.server.display.ThermalBrightnessController .toStrFloatForDump ( v8 );
/* .line 860 */
/* .local v8, "strNit":Ljava/lang/String; */
v9 = (( java.lang.String ) v7 ).length ( ); // invoke-virtual {v7}, Ljava/lang/String;->length()I
v10 = (( java.lang.String ) v8 ).length ( ); // invoke-virtual {v8}, Ljava/lang/String;->length()I
v9 = java.lang.Math .max ( v9,v10 );
/* .line 861 */
/* .local v9, "maxLen":I */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v3 ); // invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = "%"; // const-string v11, "%"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v11 = "s"; // const-string v11, "s"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 862 */
/* .local v10, "format":Ljava/lang/String; */
final String v3 = ", "; // const-string v3, ", "
/* .line 863 */
/* filled-new-array {v7}, [Ljava/lang/Object; */
android.text.TextUtils .formatSimple ( v10,v11 );
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 864 */
/* filled-new-array {v8}, [Ljava/lang/Object; */
android.text.TextUtils .formatSimple ( v10,v11 );
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 866 */
v11 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* const/16 v12, 0x50 */
/* if-gt v11, v12, :cond_1 */
/* add-int/lit8 v11, v4, -0x1 */
/* if-ne v5, v11, :cond_2 */
/* .line 867 */
} // :cond_1
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 868 */
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 869 */
int v2 = 1; // const/4 v2, 0x1
/* .line 870 */
final String v3 = ""; // const-string v3, ""
/* .line 845 */
} // .end local v6 # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
} // .end local v7 # "strTemperature":Ljava/lang/String;
} // .end local v8 # "strNit":Ljava/lang/String;
} // .end local v9 # "maxLen":I
} // .end local v10 # "format":Ljava/lang/String;
} // :cond_2
/* add-int/lit8 v5, v5, 0x1 */
/* goto/16 :goto_0 */
/* .line 873 */
} // .end local v5 # "i":I
} // :cond_3
return;
} // .end method
private void lambda$loadThermalConfig$0 ( com.android.server.display.ThermalBrightnessController$ThermalListener p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "l" # Lcom/android/server/display/ThermalBrightnessController$ThermalListener; */
/* .line 222 */
v0 = this.mThermalConditions;
/* .line 223 */
return;
} // .end method
private void lambda$new$1 ( ) { //synthethic
/* .locals 1 */
/* .line 467 */
int v0 = 2; // const/4 v0, 0x2
/* invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V */
return;
} // .end method
private void lambda$new$2 ( ) { //synthethic
/* .locals 1 */
/* .line 468 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V */
return;
} // .end method
private void lambda$new$3 ( ) { //synthethic
/* .locals 1 */
/* .line 469 */
/* const/16 v0, 0x8 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V */
return;
} // .end method
private void lambda$new$4 ( ) { //synthethic
/* .locals 1 */
/* .line 470 */
/* const/16 v0, 0x10 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V */
return;
} // .end method
private com.android.server.display.thermalbrightnesscondition.config.ThermalBrightnessConfig loadConfigFromFile ( ) {
/* .locals 4 */
/* .line 666 */
android.os.Environment .getProductDirectory ( );
/* const-string/jumbo v1, "thermal_brightness_control.xml" */
final String v2 = "etc"; // const-string v2, "etc"
final String v3 = "displayconfig"; // const-string v3, "displayconfig"
/* filled-new-array {v2, v3, v1}, [Ljava/lang/String; */
/* .line 665 */
android.os.Environment .buildPath ( v0,v1 );
/* .line 670 */
/* .local v0, "defaultFile":Ljava/io/File; */
android.os.Environment .getDataSystemDirectory ( );
final String v2 = "cloud_thermal_brightness_control.xml"; // const-string v2, "cloud_thermal_brightness_control.xml"
/* filled-new-array {v3, v2}, [Ljava/lang/String; */
/* .line 669 */
android.os.Environment .buildPath ( v1,v2 );
/* .line 672 */
/* .local v1, "cloudFile":Ljava/io/File; */
/* invoke-direct {p0, v1}, Lcom/android/server/display/ThermalBrightnessController;->parseConfig(Ljava/io/File;)Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig; */
/* .line 673 */
/* .local v2, "config":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 674 */
final String v3 = "cloud_file"; // const-string v3, "cloud_file"
this.mLoadedFileFrom = v3;
/* .line 675 */
/* .line 678 */
} // :cond_0
/* invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->parseConfig(Ljava/io/File;)Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig; */
/* .line 679 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 680 */
/* const-string/jumbo v3, "static_file" */
this.mLoadedFileFrom = v3;
/* .line 681 */
/* .line 683 */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
} // .end method
private void loadThermalConfig ( ) {
/* .locals 11 */
/* .line 211 */
v0 = this.mThermalConditions;
/* .line 212 */
/* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->loadConfigFromFile()Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig; */
/* .line 213 */
/* .local v0, "config":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig; */
final String v1 = "ThermalBrightnessController"; // const-string v1, "ThermalBrightnessController"
/* if-nez v0, :cond_0 */
/* .line 214 */
final String v2 = "config file was not found!"; // const-string v2, "config file was not found!"
android.util.Slog .w ( v1,v2 );
/* .line 215 */
return;
/* .line 217 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "load thermal config from: "; // const-string v3, "load thermal config from: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mLoadedFileFrom;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 219 */
v1 = this.mThermalConditions;
(( com.android.server.display.thermalbrightnesscondition.config.ThermalBrightnessConfig ) v0 ).getThermalConditionItem ( ); // invoke-virtual {v0}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;->getThermalConditionItem()Ljava/util/List;
/* .line 221 */
v1 = this.mThermalListener;
/* new-instance v2, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda7; */
/* invoke-direct {v2, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V */
(( java.util.ArrayList ) v1 ).forEach ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->forEach(Ljava/util/function/Consumer;)V
/* .line 224 */
v1 = this.mThermalConditions;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_6
/* check-cast v2, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
/* .line 226 */
/* .local v2, "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
v3 = (( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v2 ).getIdentifier ( ); // invoke-virtual {v2}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I
/* .line 227 */
/* .local v3, "id":I */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v2 ).getDescription ( ); // invoke-virtual {v2}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getDescription()Ljava/lang/String;
/* .line 229 */
/* .local v4, "description":Ljava/lang/String; */
/* if-nez v3, :cond_1 */
/* .line 230 */
this.mDefaultCondition = v2;
/* .line 234 */
} // :cond_1
final String v5 = "DOLBY-VISION"; // const-string v5, "DOLBY-VISION"
v5 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 235 */
this.mOverrideCondition = v2;
/* .line 238 */
} // :cond_2
v5 = this.mConditionIdMaps;
(( android.util.SparseArray ) v5 ).append ( v3, v2 ); // invoke-virtual {v5, v3, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
/* .line 240 */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v2 ).getTemperatureBrightnessPair ( ); // invoke-virtual {v2}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;
/* .line 243 */
/* .local v5, "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;" */
/* const/high16 v6, 0x7fc00000 # Float.NaN */
/* .line 244 */
/* .local v6, "miniTemperature":F */
v8 = } // :goto_1
if ( v8 != null) { // if-eqz v8, :cond_5
/* check-cast v8, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
/* .line 245 */
/* .local v8, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
v9 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v8 ).getMinInclusive ( ); // invoke-virtual {v8}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMinInclusive()F
/* .line 246 */
/* .local v9, "minTemp":F */
v10 = java.lang.Float .isNaN ( v6 );
/* if-nez v10, :cond_3 */
/* cmpg-float v10, v9, v6 */
/* if-gez v10, :cond_4 */
/* .line 247 */
} // :cond_3
/* move v6, v9 */
/* .line 249 */
} // .end local v8 # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
} // .end local v9 # "minTemp":F
} // :cond_4
/* .line 250 */
} // :cond_5
v7 = this.mMiniTemperatureThresholds;
java.lang.Float .valueOf ( v6 );
(( android.util.SparseArray ) v7 ).append ( v3, v8 ); // invoke-virtual {v7, v3, v8}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
/* .line 251 */
} // .end local v2 # "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
} // .end local v3 # "id":I
} // .end local v4 # "description":Ljava/lang/String;
} // .end local v5 # "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;"
} // .end local v6 # "miniTemperature":F
/* .line 252 */
} // :cond_6
int v1 = 2; // const/4 v1, 0x2
/* invoke-direct {p0, v1}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V */
/* .line 253 */
return;
} // .end method
private native void nativeInit ( ) {
} // .end method
private com.android.server.display.thermalbrightnesscondition.config.ThermalBrightnessConfig parseConfig ( java.io.File p0 ) {
/* .locals 4 */
/* .param p1, "configFile" # Ljava/io/File; */
/* .line 690 */
v0 = (( java.io.File ) p1 ).exists ( ); // invoke-virtual {p1}, Ljava/io/File;->exists()Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 691 */
/* .line 693 */
} // :cond_0
try { // :try_start_0
/* new-instance v0, Ljava/io/BufferedInputStream; */
/* new-instance v2, Ljava/io/FileInputStream; */
/* invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* invoke-direct {v0, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljavax/xml/datatype/DatatypeConfigurationException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 694 */
/* .local v0, "in":Ljava/io/InputStream; */
try { // :try_start_1
com.android.server.display.thermalbrightnesscondition.config.XmlParser .read ( v0 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 695 */
try { // :try_start_2
(( java.io.InputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/InputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catch Ljavax/xml/datatype/DatatypeConfigurationException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 694 */
/* .line 693 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.InputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/InputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/android/server/display/ThermalBrightnessController;
} // .end local p1 # "configFile":Ljava/io/File;
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catch Ljavax/xml/datatype/DatatypeConfigurationException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 695 */
} // .end local v0 # "in":Ljava/io/InputStream;
/* .restart local p0 # "this":Lcom/android/server/display/ThermalBrightnessController; */
/* .restart local p1 # "configFile":Ljava/io/File; */
/* :catch_0 */
/* move-exception v0 */
/* .line 696 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 698 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end method
private java.lang.String readThermalConfigFromNode ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "file" # Ljava/lang/String; */
/* .line 633 */
int v0 = 0; // const/4 v0, 0x0
/* .line 634 */
/* .local v0, "reader":Ljava/io/BufferedReader; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 637 */
/* .local v1, "builder":Ljava/lang/StringBuilder; */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/FileReader; */
/* new-instance v4, Ljava/io/File; */
final String v5 = "/sys/class/thermal/thermal_message"; // const-string v5, "/sys/class/thermal/thermal_message"
/* invoke-direct {v4, v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v0, v2 */
/* .line 638 */
} // :goto_0
(( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v3, v2 */
/* .local v3, "info":Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 639 */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 641 */
} // :cond_0
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 648 */
/* nop */
/* .line 649 */
try { // :try_start_1
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 653 */
/* .line 651 */
/* :catch_0 */
/* move-exception v4 */
/* .line 652 */
/* .local v4, "e":Ljava/io/IOException; */
(( java.io.IOException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
/* .line 641 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :goto_1
/* .line 647 */
} // .end local v3 # "info":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v2 */
/* .line 644 */
/* :catch_1 */
/* move-exception v2 */
/* .line 645 */
/* .local v2, "e":Ljava/io/IOException; */
try { // :try_start_2
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 648 */
} // .end local v2 # "e":Ljava/io/IOException;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 649 */
try { // :try_start_3
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 651 */
/* :catch_2 */
/* move-exception v2 */
/* .line 652 */
/* .restart local v2 # "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 654 */
} // .end local v2 # "e":Ljava/io/IOException;
/* .line 653 */
} // :cond_1
} // :goto_2
/* .line 642 */
/* :catch_3 */
/* move-exception v2 */
/* .line 643 */
/* .local v2, "e":Ljava/io/FileNotFoundException; */
try { // :try_start_4
(( java.io.FileNotFoundException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 648 */
} // .end local v2 # "e":Ljava/io/FileNotFoundException;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 649 */
try { // :try_start_5
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_2 */
/* .line 655 */
} // :goto_3
int v2 = 0; // const/4 v2, 0x0
/* .line 648 */
} // :goto_4
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 649 */
try { // :try_start_6
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_4 */
/* .line 651 */
/* :catch_4 */
/* move-exception v3 */
/* .line 652 */
/* .local v3, "e":Ljava/io/IOException; */
(( java.io.IOException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
/* .line 653 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :cond_2
} // :goto_5
/* nop */
/* .line 654 */
} // :goto_6
/* throw v2 */
} // .end method
private static java.lang.String toStrFloatForDump ( Float p0 ) {
/* .locals 3 */
/* .param p0, "value" # F */
/* .line 889 */
int v0 = 0; // const/4 v0, 0x0
/* cmpl-float v0, p0, v0 */
/* if-nez v0, :cond_0 */
/* .line 890 */
final String v0 = "0"; // const-string v0, "0"
/* .line 891 */
} // :cond_0
/* const v0, 0x3dcccccd # 0.1f */
/* cmpg-float v0, p0, v0 */
/* if-gez v0, :cond_1 */
/* .line 892 */
v0 = java.util.Locale.US;
java.lang.Float .valueOf ( p0 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
final String v2 = "%.3f"; // const-string v2, "%.3f"
java.lang.String .format ( v0,v2,v1 );
/* .line 893 */
} // :cond_1
/* const/high16 v0, 0x3f800000 # 1.0f */
/* cmpg-float v0, p0, v0 */
/* if-gez v0, :cond_2 */
/* .line 894 */
v0 = java.util.Locale.US;
java.lang.Float .valueOf ( p0 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
final String v2 = "%.2f"; // const-string v2, "%.2f"
java.lang.String .format ( v0,v2,v1 );
/* .line 895 */
} // :cond_2
/* const/high16 v0, 0x41200000 # 10.0f */
/* cmpg-float v0, p0, v0 */
/* if-gez v0, :cond_3 */
/* .line 896 */
v0 = java.util.Locale.US;
java.lang.Float .valueOf ( p0 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
final String v2 = "%.1f"; // const-string v2, "%.1f"
java.lang.String .format ( v0,v2,v1 );
/* .line 898 */
} // :cond_3
v0 = java.lang.Math .round ( p0 );
java.lang.Integer .valueOf ( v0 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
final String v1 = "%d"; // const-string v1, "%d"
android.text.TextUtils .formatSimple ( v1,v0 );
} // .end method
private static java.lang.String toStringCondition ( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem p0 ) {
/* .locals 3 */
/* .param p0, "condition" # Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
/* .line 876 */
/* if-nez p0, :cond_0 */
/* .line 877 */
final String v0 = "null"; // const-string v0, "null"
/* .line 879 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 880 */
/* .local v0, "builder":Ljava/lang/StringBuilder; */
/* const-string/jumbo v1, "{identifier: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 881 */
v2 = (( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) p0 ).getIdentifier ( ); // invoke-virtual {p0}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 882 */
final String v2 = ", description: "; // const-string v2, ", description: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 883 */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) p0 ).getDescription ( ); // invoke-virtual {p0}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getDescription()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 884 */
/* const-string/jumbo v2, "}" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 885 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private void updateConditionState ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "event" # I */
/* .line 337 */
int v0 = 0; // const/4 v0, 0x0
/* .line 338 */
/* .local v0, "changed":Z */
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_0 */
/* .line 339 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateSkinTemperature()Z */
/* .line 340 */
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
/* if-ne p1, v1, :cond_1 */
/* .line 341 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateThermalCondition()Z */
/* or-int/2addr v0, v1 */
/* .line 342 */
} // :cond_1
int v1 = 4; // const/4 v1, 0x4
/* if-ne p1, v1, :cond_2 */
/* .line 343 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateOutdoorState()Z */
/* or-int/2addr v0, v1 */
/* .line 344 */
v1 = this.mDisplayPowerControllerImpl;
/* iget-boolean v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z */
(( com.android.server.display.DisplayPowerControllerImpl ) v1 ).notifyOutDoorHighTempState ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->notifyOutDoorHighTempState(Z)V
/* .line 345 */
} // :cond_2
/* const/16 v1, 0x8 */
/* if-ne p1, v1, :cond_3 */
/* .line 346 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateSafetyBrightness()Z */
/* or-int/2addr v0, v1 */
/* .line 347 */
} // :cond_3
/* const/16 v1, 0x10 */
/* if-ne p1, v1, :cond_4 */
/* .line 348 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateNtcTemperature()Z */
/* or-int/2addr v0, v1 */
/* .line 350 */
} // :cond_4
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 351 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateThermalBrightnessIfNeeded()Z */
/* .line 353 */
/* .local v1, "thermalBrightnessChanged":Z */
v2 = this.mDisplayPowerControllerImpl;
/* iget v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedThermalConditionId:I */
/* iget v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F */
(( com.android.server.display.DisplayPowerControllerImpl ) v2 ).startDetailThermalUsageStatsOnThermalChanged ( v3, v4, v1 ); // invoke-virtual {v2, v3, v4, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->startDetailThermalUsageStatsOnThermalChanged(IFZ)V
/* .line 358 */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 359 */
v2 = this.mCallback;
/* iget v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentMaxThermalBrightness:F */
/* .line 362 */
} // .end local v1 # "thermalBrightnessChanged":Z
} // :cond_5
return;
} // .end method
private Float updateMaxNtcTempBrightness ( ) {
/* .locals 9 */
/* .line 610 */
v0 = this.mConditionIdMaps;
int v1 = -3; // const/4 v1, -0x3
(( android.util.SparseArray ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
/* .line 611 */
/* .local v0, "ntcTempCondition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
/* const/high16 v1, 0x7fc00000 # Float.NaN */
/* if-nez v0, :cond_0 */
/* .line 612 */
/* .line 614 */
} // :cond_0
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v0 ).getTemperatureBrightnessPair ( ); // invoke-virtual {v0}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;
/* .line 615 */
/* .local v2, "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;" */
int v3 = 0; // const/4 v3, 0x0
/* .line 616 */
/* .local v3, "target":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
/* .line 617 */
/* .local v5, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
v6 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v5 ).getMinInclusive ( ); // invoke-virtual {v5}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMinInclusive()F
/* .line 618 */
/* .local v6, "minTemp":F */
v7 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v5 ).getMaxExclusive ( ); // invoke-virtual {v5}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMaxExclusive()F
/* .line 619 */
/* .local v7, "maxTemp":F */
/* iget v8, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedNtcTemperature:F */
/* cmpl-float v8, v8, v6 */
/* if-ltz v8, :cond_1 */
/* iget v8, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedNtcTemperature:F */
/* cmpg-float v8, v8, v7 */
/* if-gez v8, :cond_1 */
/* .line 620 */
/* move-object v3, v5 */
/* .line 621 */
/* .line 623 */
} // .end local v5 # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
} // .end local v6 # "minTemp":F
} // .end local v7 # "maxTemp":F
} // :cond_1
/* .line 624 */
} // :cond_2
} // :goto_1
/* if-nez v3, :cond_3 */
/* .line 625 */
/* .line 627 */
} // :cond_3
v1 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v3 ).getNit ( ); // invoke-virtual {v3}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getNit()F
/* .line 628 */
/* .local v1, "value":F */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "updateMaxNtcTempBrightness: get brightness threshold: " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "ThermalBrightnessController"; // const-string v5, "ThermalBrightnessController"
android.util.Slog .d ( v5,v4 );
/* .line 629 */
} // .end method
private Float updateMaxThermalBrightness ( ) {
/* .locals 12 */
/* .line 556 */
v0 = this.mCurrentCondition;
/* const/high16 v1, 0x7fc00000 # Float.NaN */
final String v2 = "ThermalBrightnessController"; // const-string v2, "ThermalBrightnessController"
/* if-nez v0, :cond_1 */
/* .line 557 */
v0 = this.mDefaultCondition;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 558 */
this.mCurrentCondition = v0;
/* .line 559 */
/* const-string/jumbo v0, "updateMaxThermalBrightness: mCurrentCondition is null, initialized with default condition." */
android.util.Slog .w ( v2,v0 );
/* .line 562 */
} // :cond_0
/* const-string/jumbo v0, "updateMaxThermalBrightness: no valid conditions!" */
android.util.Slog .w ( v2,v0 );
/* .line 563 */
/* .line 566 */
} // :cond_1
} // :goto_0
v0 = this.mCurrentCondition;
v0 = (( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v0 ).getIdentifier ( ); // invoke-virtual {v0}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I
/* .line 567 */
/* .local v0, "conditionId":I */
v3 = this.mMiniTemperatureThresholds;
(( android.util.SparseArray ) v3 ).get ( v0 ); // invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Float; */
/* .line 568 */
/* .local v3, "miniTemperature":Ljava/lang/Float; */
if ( v3 != null) { // if-eqz v3, :cond_3
/* iget v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F */
v5 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* cmpg-float v4, v4, v5 */
/* if-gez v4, :cond_3 */
/* .line 569 */
/* sget-boolean v4, Lcom/android/server/display/ThermalBrightnessController;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 570 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "updateMaxThermalBrightness: Current skin temperature is less than the minimum temperature threshold: " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v4 );
/* .line 572 */
} // :cond_2
/* .line 575 */
} // :cond_3
v4 = this.mCacheInfo;
/* iget v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F */
/* float-to-int v5, v5 */
java.lang.Integer .valueOf ( v5 );
(( android.util.SparseArrayMap ) v4 ).get ( v0, v5 ); // invoke-virtual {v4, v0, v5}, Landroid/util/SparseArrayMap;->get(ILjava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/Float; */
/* .line 576 */
/* .local v4, "cachedNit":Ljava/lang/Float; */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 577 */
/* sget-boolean v1, Lcom/android/server/display/ThermalBrightnessController;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 578 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "updateMaxThermalBrightness: Max thermal brightness already cached: " */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 580 */
} // :cond_4
v1 = (( java.lang.Float ) v4 ).floatValue ( ); // invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F
/* .line 583 */
} // :cond_5
v5 = this.mCurrentCondition;
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v5 ).getTemperatureBrightnessPair ( ); // invoke-virtual {v5}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;
/* .line 584 */
/* .local v5, "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;" */
int v6 = 0; // const/4 v6, 0x0
/* .line 585 */
/* .local v6, "target":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
v8 = } // :goto_1
if ( v8 != null) { // if-eqz v8, :cond_7
/* check-cast v8, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
/* .line 586 */
/* .local v8, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
v9 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v8 ).getMinInclusive ( ); // invoke-virtual {v8}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMinInclusive()F
/* .line 587 */
/* .local v9, "minTemp":F */
v10 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v8 ).getMaxExclusive ( ); // invoke-virtual {v8}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMaxExclusive()F
/* .line 588 */
/* .local v10, "maxTemp":F */
/* iget v11, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F */
/* cmpl-float v11, v11, v9 */
/* if-ltz v11, :cond_6 */
/* iget v11, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F */
/* cmpg-float v11, v11, v10 */
/* if-gez v11, :cond_6 */
/* .line 589 */
/* move-object v6, v8 */
/* .line 590 */
/* .line 592 */
} // .end local v8 # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
} // .end local v9 # "minTemp":F
} // .end local v10 # "maxTemp":F
} // :cond_6
/* .line 594 */
} // :cond_7
} // :goto_2
/* if-nez v6, :cond_8 */
/* .line 595 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Current temperature "; // const-string v8, "Current temperature "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v8, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v8 = " does not match in config: "; // const-string v8, " does not match in config: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v7 );
/* .line 596 */
/* .line 599 */
} // :cond_8
v1 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v6 ).getNit ( ); // invoke-virtual {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getNit()F
/* .line 600 */
/* .local v1, "value":F */
v7 = this.mCacheInfo;
/* iget v8, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F */
/* float-to-int v8, v8 */
java.lang.Integer .valueOf ( v8 );
java.lang.Float .valueOf ( v1 );
(( android.util.SparseArrayMap ) v7 ).add ( v0, v8, v9 ); // invoke-virtual {v7, v0, v8, v9}, Landroid/util/SparseArrayMap;->add(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 601 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "updateMaxThermalBrightness: get brightness threshold: " */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v7 );
/* .line 602 */
} // .end method
private Boolean updateNtcTemperature ( ) {
/* .locals 6 */
/* .line 430 */
final String v0 = "display_therm_temp"; // const-string v0, "display_therm_temp"
/* invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->readThermalConfigFromNode(Ljava/lang/String;)Ljava/lang/String; */
/* .line 432 */
/* .local v0, "value":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 433 */
try { // :try_start_0
v1 = java.lang.Float .parseFloat ( v0 );
/* const/high16 v2, 0x447a0000 # 1000.0f */
/* div-float/2addr v1, v2 */
/* .line 434 */
/* .local v1, "temperature":F */
/* sget-boolean v2, Lcom/android/server/display/ThermalBrightnessController;->DEBUG:Z */
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
final String v3 = "ThermalBrightnessController"; // const-string v3, "ThermalBrightnessController"
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 435 */
try { // :try_start_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "updateNtcTemperature: temperature: " */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 437 */
} // :cond_0
/* iput v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentNtcTemperature:F */
/* .line 438 */
/* iget v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedNtcTemperature:F */
int v4 = 1; // const/4 v4, 0x1
v2 = (( com.android.server.display.ThermalBrightnessController ) p0 ).asSameTemperature ( v1, v2, v4 ); // invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/display/ThermalBrightnessController;->asSameTemperature(FFZ)Z
/* if-nez v2, :cond_1 */
/* .line 439 */
/* iput v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedNtcTemperature:F */
/* .line 440 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "updateNtcTemperature: actual temperature: " */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentNtcTemperature:F */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = ", applied temperature: "; // const-string v5, ", applied temperature: "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedNtcTemperature:F */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* :try_end_1 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 442 */
/* .line 445 */
} // .end local v1 # "temperature":F
/* :catch_0 */
/* move-exception v1 */
/* .line 446 */
/* .local v1, "e":Ljava/lang/NumberFormatException; */
(( java.lang.NumberFormatException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V
/* .line 447 */
} // .end local v1 # "e":Ljava/lang/NumberFormatException;
} // :cond_1
/* nop */
/* .line 448 */
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private Boolean updateOutdoorState ( ) {
/* .locals 2 */
/* .line 477 */
v0 = this.mOutdoorDetector;
v0 = (( com.android.server.display.OutdoorDetector ) v0 ).isOutdoorState ( ); // invoke-virtual {v0}, Lcom/android/server/display/OutdoorDetector;->isOutdoorState()Z
/* .line 478 */
/* .local v0, "state":Z */
/* iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z */
/* if-eq v0, v1, :cond_0 */
/* .line 479 */
/* iput-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z */
/* .line 480 */
int v1 = 1; // const/4 v1, 0x1
/* .line 482 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private void updateOverrideCondition ( ) {
/* .locals 2 */
/* .line 744 */
/* iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsHdrLayerPresent:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsDolbyEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 745 */
/* .local v0, "needOverride":Z */
} // :goto_1
/* iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedOverrideCondition:Z */
/* if-eq v1, v0, :cond_2 */
/* .line 746 */
/* iput-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedOverrideCondition:Z */
/* .line 747 */
int v1 = 2; // const/4 v1, 0x2
/* invoke-direct {p0, v1}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V */
/* .line 749 */
} // :cond_2
return;
} // .end method
private Boolean updateSafetyBrightness ( ) {
/* .locals 3 */
/* .line 415 */
/* const-string/jumbo v0, "thermal_max_brightness" */
/* invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->readThermalConfigFromNode(Ljava/lang/String;)Ljava/lang/String; */
/* .line 416 */
/* .local v0, "value":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 417 */
v1 = java.lang.Float .parseFloat ( v0 );
/* .line 418 */
/* .local v1, "nit":F */
/* iget v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalSafetyBrightness:F */
/* cmpl-float v2, v2, v1 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 419 */
int v2 = 0; // const/4 v2, 0x0
/* cmpl-float v2, v1, v2 */
/* if-nez v2, :cond_0 */
/* const/high16 v2, 0x7fc00000 # Float.NaN */
} // :cond_0
/* move v2, v1 */
} // :goto_0
/* iput v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalSafetyBrightness:F */
/* .line 420 */
int v2 = 1; // const/4 v2, 0x1
/* .line 423 */
} // .end local v1 # "nit":F
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
private Boolean updateSkinTemperature ( ) {
/* .locals 6 */
/* .line 308 */
final String v0 = "board_sensor_temp"; // const-string v0, "board_sensor_temp"
/* invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->readThermalConfigFromNode(Ljava/lang/String;)Ljava/lang/String; */
/* .line 310 */
/* .local v0, "value":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 311 */
try { // :try_start_0
v2 = java.lang.Float .parseFloat ( v0 );
/* const/high16 v3, 0x447a0000 # 1000.0f */
/* div-float/2addr v2, v3 */
/* .line 312 */
/* .local v2, "temperature":F */
/* sget-boolean v3, Lcom/android/server/display/ThermalBrightnessController;->DEBUG:Z */
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
final String v4 = "ThermalBrightnessController"; // const-string v4, "ThermalBrightnessController"
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 313 */
try { // :try_start_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "updateSkinTemperature: temperature: " */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v3 );
/* .line 315 */
} // :cond_0
/* iput v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F */
/* .line 316 */
/* iget v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F */
v3 = (( com.android.server.display.ThermalBrightnessController ) p0 ).asSameTemperature ( v2, v3, v1 ); // invoke-virtual {p0, v2, v3, v1}, Lcom/android/server/display/ThermalBrightnessController;->asSameTemperature(FFZ)Z
/* if-nez v3, :cond_1 */
/* .line 317 */
/* iput v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F */
/* .line 319 */
(( com.android.server.display.ThermalBrightnessController ) p0 ).checkOutdoorState ( ); // invoke-virtual {p0}, Lcom/android/server/display/ThermalBrightnessController;->checkOutdoorState()V
/* .line 320 */
/* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateThermalBrightnessBoostCondition()V */
/* .line 321 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "updateSkinTemperature: actual temperature: " */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = ", applied temperature: "; // const-string v5, ", applied temperature: "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v3 );
/* :try_end_1 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 323 */
int v1 = 1; // const/4 v1, 0x1
/* .line 326 */
} // .end local v2 # "temperature":F
/* :catch_0 */
/* move-exception v2 */
/* .line 327 */
/* .local v2, "e":Ljava/lang/NumberFormatException; */
(( java.lang.NumberFormatException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/NumberFormatException;->printStackTrace()V
/* .line 328 */
} // .end local v2 # "e":Ljava/lang/NumberFormatException;
} // :cond_1
/* nop */
/* .line 329 */
} // :goto_0
} // .end method
private void updateThermalBrightnessBoostCondition ( ) {
/* .locals 2 */
/* .line 775 */
v0 = this.mOutdoorThermalAppList;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mOutdoorThermalAppList;
v1 = this.mForegroundAppPackageName;
v0 = /* .line 776 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 777 */
/* .local v0, "needThermalBrightnessBoost":Z */
} // :goto_0
/* iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedThermalBrightnessBoost:Z */
/* if-eq v1, v0, :cond_1 */
/* .line 778 */
/* iput-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedThermalBrightnessBoost:Z */
/* .line 779 */
int v1 = 2; // const/4 v1, 0x2
/* invoke-direct {p0, v1}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V */
/* .line 781 */
} // :cond_1
return;
} // .end method
private Boolean updateThermalBrightnessIfNeeded ( ) {
/* .locals 4 */
/* .line 515 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateMaxThermalBrightness()F */
/* .line 516 */
/* .local v0, "restrictedBrightness":F */
/* iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalBrightnessControlNtcAvailable:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 517 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateMaxNtcTempBrightness()F */
/* .line 518 */
/* .local v1, "restrictedNtcBrightness":F */
v2 = java.lang.Float .isNaN ( v1 );
/* if-nez v2, :cond_1 */
/* .line 519 */
v2 = java.lang.Float .isNaN ( v0 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* move v2, v1 */
/* .line 520 */
} // :cond_0
v2 = java.lang.Math .min ( v0,v1 );
} // :goto_0
/* move v0, v2 */
/* .line 523 */
} // .end local v1 # "restrictedNtcBrightness":F
} // :cond_1
v1 = java.lang.Float .isNaN ( v0 );
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentMaxThermalBrightness:F */
v1 = java.lang.Float .isNaN ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 524 */
/* .line 529 */
} // :cond_2
/* iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F */
/* iget v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelCritical:F */
/* cmpl-float v1, v1, v3 */
/* if-ltz v1, :cond_3 */
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F */
/* iget v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelEmergency:F */
/* cmpg-float v1, v1, v3 */
/* if-gez v1, :cond_3 */
/* .line 531 */
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mMinBrightnessInOutdoorHighTemperature:F */
v0 = java.lang.Math .max ( v0,v1 );
/* .line 535 */
} // :cond_3
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalSafetyBrightness:F */
/* const/high16 v3, -0x40800000 # -1.0f */
/* cmpl-float v3, v1, v3 */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 536 */
v1 = java.lang.Float .isNaN ( v1 );
/* if-nez v1, :cond_4 */
/* .line 537 */
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalSafetyBrightness:F */
v0 = java.lang.Math .min ( v0,v1 );
/* .line 539 */
} // :cond_4
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentMaxThermalBrightness:F */
/* cmpl-float v1, v0, v1 */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 540 */
/* iput v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentMaxThermalBrightness:F */
/* .line 541 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "updateThermalBrightnessState: condition id: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedThermalConditionId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", temperature: "; // const-string v2, ", temperature: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", maximum thermal brightness: "; // const-string v2, ", maximum thermal brightness: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentMaxThermalBrightness:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", outdoor: "; // const-string v2, ", outdoor: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", safety thermal brightness: "; // const-string v2, ", safety thermal brightness: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalSafetyBrightness:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "ThermalBrightnessController"; // const-string v2, "ThermalBrightnessController"
android.util.Slog .d ( v2,v1 );
/* .line 546 */
int v1 = 1; // const/4 v1, 0x1
/* .line 548 */
} // :cond_5
} // .end method
private Boolean updateThermalCondition ( ) {
/* .locals 7 */
/* .line 368 */
final String v0 = ", name="; // const-string v0, ", name="
final String v1 = "ThermalBrightnessController"; // const-string v1, "ThermalBrightnessController"
final String v2 = "sconfig"; // const-string v2, "sconfig"
/* invoke-direct {p0, v2}, Lcom/android/server/display/ThermalBrightnessController;->readThermalConfigFromNode(Ljava/lang/String;)Ljava/lang/String; */
/* .line 370 */
/* .local v2, "value":Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 371 */
try { // :try_start_0
v3 = java.lang.Integer .parseInt ( v2 );
/* .line 372 */
/* .local v3, "id":I */
/* iget v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedThermalConditionId:I */
/* if-ne v4, v3, :cond_0 */
/* iget-boolean v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedOverrideCondition:Z */
/* if-nez v4, :cond_0 */
/* iget-boolean v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedThermalBrightnessBoost:Z */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 373 */
} // :cond_0
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "updateThermalCondition: condition changed: " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v4 );
/* .line 374 */
/* iput v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentActualThermalConditionId:I */
/* .line 377 */
/* iget-boolean v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedThermalBrightnessBoost:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 378 */
int v3 = -4; // const/4 v3, -0x4
/* .line 381 */
} // :cond_1
v4 = this.mConditionIdMaps;
(( android.util.SparseArray ) v4 ).get ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
/* .line 383 */
/* .local v4, "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
/* iget-boolean v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedOverrideCondition:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 384 */
v5 = this.mOverrideCondition;
/* move-object v4, v5 */
/* .line 387 */
} // :cond_2
/* if-nez v4, :cond_3 */
/* .line 388 */
v5 = this.mDefaultCondition;
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 389 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Thermal condition (id="; // const-string v6, "Thermal condition (id="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentActualThermalConditionId:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ") is not configured in file, apply default condition!"; // const-string v6, ") is not configured in file, apply default condition!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v5 );
/* .line 392 */
v5 = this.mDefaultCondition;
/* move-object v4, v5 */
/* .line 395 */
} // :cond_3
v5 = this.mCurrentCondition;
/* if-eq v4, v5, :cond_5 */
/* .line 396 */
if ( v5 != null) { // if-eqz v5, :cond_4
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 397 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "condition changed from: [id="; // const-string v6, "condition changed from: [id="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mCurrentCondition;
v6 = (( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v6 ).getIdentifier ( ); // invoke-virtual {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mCurrentCondition;
/* .line 398 */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v6 ).getDescription ( ); // invoke-virtual {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getDescription()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "] to [id="; // const-string v6, "] to [id="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 399 */
v6 = (( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v4 ).getIdentifier ( ); // invoke-virtual {v4}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 400 */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v4 ).getDescription ( ); // invoke-virtual {v4}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getDescription()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "]"; // const-string v5, "]"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 397 */
android.util.Slog .d ( v1,v0 );
/* .line 402 */
} // :cond_4
this.mCurrentCondition = v4;
/* .line 403 */
v0 = (( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v4 ).getIdentifier ( ); // invoke-virtual {v4}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I
/* iput v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedThermalConditionId:I */
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 404 */
int v0 = 1; // const/4 v0, 0x1
/* .line 408 */
} // .end local v3 # "id":I
} // .end local v4 # "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
/* :catch_0 */
/* move-exception v0 */
/* .line 409 */
/* .local v0, "e":Ljava/lang/NumberFormatException; */
(( java.lang.NumberFormatException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V
/* .line 410 */
} // .end local v0 # "e":Ljava/lang/NumberFormatException;
} // :cond_5
/* nop */
/* .line 411 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
/* # virtual methods */
public void addThermalListener ( com.android.server.display.ThermalBrightnessController$ThermalListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lcom/android/server/display/ThermalBrightnessController$ThermalListener; */
/* .line 903 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 904 */
v0 = this.mThermalListener;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 906 */
} // :cond_0
return;
} // .end method
public Boolean asSameTemperature ( Float p0, Float p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "a" # F */
/* .param p2, "b" # F */
/* .param p3, "isNtcTemperatureGap" # Z */
/* .line 702 */
/* cmpl-float v0, p1, p2 */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
/* .line 703 */
/* .line 704 */
} // :cond_0
v0 = java.lang.Float .isNaN ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = java.lang.Float .isNaN ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 705 */
/* .line 706 */
} // :cond_1
/* float-to-int v0, p1 */
/* float-to-int v2, p2 */
/* sub-int/2addr v0, v2 */
v0 = java.lang.Math .abs ( v0 );
/* if-ge v0, v1, :cond_2 */
/* .line 707 */
/* .line 709 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
if ( p3 != null) { // if-eqz p3, :cond_4
/* .line 710 */
/* sub-float v2, p1, p2 */
v2 = java.lang.Math .abs ( v2 );
/* const/high16 v3, 0x3f800000 # 1.0f */
/* cmpg-float v2, v2, v3 */
/* if-gez v2, :cond_3 */
} // :cond_3
/* move v1, v0 */
} // :goto_0
/* .line 712 */
} // :cond_4
/* sub-float v2, p1, p2 */
v2 = java.lang.Math .abs ( v2 );
/* iget v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureGap:F */
/* cmpg-float v2, v2, v3 */
/* if-gez v2, :cond_5 */
} // :cond_5
/* move v1, v0 */
} // :goto_1
} // .end method
public void checkOutdoorState ( ) {
/* .locals 2 */
/* .line 494 */
/* iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mScreenOn:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsThermalBrightnessBoostEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mUseAutoBrightness:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mSunlightModeActive:Z */
/* if-nez v0, :cond_1 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F */
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelCritical:F */
/* cmpl-float v0, v0, v1 */
/* if-ltz v0, :cond_2 */
/* iget v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F */
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelEmergency:F */
/* cmpg-float v0, v0, v1 */
/* if-gez v0, :cond_2 */
/* .line 498 */
} // :cond_1
v0 = this.mOutdoorDetector;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.display.OutdoorDetector ) v0 ).detect ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/OutdoorDetector;->detect(Z)Z
/* .line 500 */
} // :cond_2
v0 = this.mOutdoorDetector;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.display.OutdoorDetector ) v0 ).detect ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/OutdoorDetector;->detect(Z)Z
/* .line 502 */
} // :goto_0
return;
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 800 */
/* sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_DPC:Z */
com.android.server.display.ThermalBrightnessController.DEBUG = (v0!= 0);
/* .line 801 */
final String v0 = ""; // const-string v0, ""
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 802 */
final String v0 = "Thermal Brightness Controller: "; // const-string v0, "Thermal Brightness Controller: "
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 803 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCurrentCondition="; // const-string v1, " mCurrentCondition="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCurrentCondition;
com.android.server.display.ThermalBrightnessController .toStringCondition ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 804 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mDefaultCondition="; // const-string v1, " mDefaultCondition="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mDefaultCondition;
com.android.server.display.ThermalBrightnessController .toStringCondition ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 805 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mOverrideCondition="; // const-string v1, " mOverrideCondition="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mOverrideCondition;
com.android.server.display.ThermalBrightnessController .toStringCondition ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 806 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCurrentAppliedThermalConditionId="; // const-string v1, " mCurrentAppliedThermalConditionId="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedThermalConditionId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 807 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCurrentActualThermalConditionId="; // const-string v1, " mCurrentActualThermalConditionId="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentActualThermalConditionId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 808 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCurrentAppliedTemperature="; // const-string v1, " mCurrentAppliedTemperature="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 809 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCurrentTemperature="; // const-string v1, " mCurrentTemperature="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 810 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCurrentThermalMaxBrightness="; // const-string v1, " mCurrentThermalMaxBrightness="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentMaxThermalBrightness:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 811 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mMiniTemperatureThresholds="; // const-string v1, " mMiniTemperatureThresholds="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mMiniTemperatureThresholds;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 812 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mIsInOutdoorHighTempState="; // const-string v1, " mIsInOutdoorHighTempState="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 813 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mIsThermalBrightnessBoostEnable="; // const-string v1, " mIsThermalBrightnessBoostEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsThermalBrightnessBoostEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 814 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mNeedThermalBrightnessBoost="; // const-string v1, " mNeedThermalBrightnessBoost="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedThermalBrightnessBoost:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 815 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mScreenOn="; // const-string v1, " mScreenOn="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mScreenOn:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 816 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mNeedOverrideCondition="; // const-string v1, " mNeedOverrideCondition="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedOverrideCondition:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 817 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTemperatureLevelEmergency="; // const-string v1, " mTemperatureLevelEmergency="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelEmergency:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 818 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTemperatureLevelCritical="; // const-string v1, " mTemperatureLevelCritical="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelCritical:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 819 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mMinBrightnessInOutdoorHighTemperature="; // const-string v1, " mMinBrightnessInOutdoorHighTemperature="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mMinBrightnessInOutdoorHighTemperature:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 820 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mThresholdLuxEnterOutdoor="; // const-string v1, " mThresholdLuxEnterOutdoor="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThresholdLuxEnterOutdoor:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 821 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mThermalSafetyBrightness="; // const-string v1, " mThermalSafetyBrightness="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalSafetyBrightness:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 822 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTemperatureGap="; // const-string v1, " mTemperatureGap="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureGap:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 823 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mThermalBrightnessControlNtcAvailable="; // const-string v1, " mThermalBrightnessControlNtcAvailable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalBrightnessControlNtcAvailable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 824 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCurrentNtcTemperature="; // const-string v1, " mCurrentNtcTemperature="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentNtcTemperature:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 825 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCurrentAppliedNtcTemperature="; // const-string v1, " mCurrentAppliedNtcTemperature="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedNtcTemperature:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 827 */
final String v0 = " Thermal brightness config:"; // const-string v0, " Thermal brightness config:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 828 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mLoadedFileFrom:"; // const-string v1, " mLoadedFileFrom:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mLoadedFileFrom;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 829 */
v0 = this.mThermalConditions;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
/* .line 830 */
/* .local v1, "conditionItem":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " identifier: "; // const-string v3, " identifier: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v1 ).getIdentifier ( ); // invoke-virtual {v1}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 831 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " description: "; // const-string v3, " description: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v1 ).getDescription ( ); // invoke-virtual {v1}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getDescription()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 832 */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v1 ).getTemperatureBrightnessPair ( ); // invoke-virtual {v1}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;
/* invoke-direct {p0, v2, p1}, Lcom/android/server/display/ThermalBrightnessController;->formatDumpTemperatureBrightnessPair(Ljava/util/List;Ljava/io/PrintWriter;)V */
/* .line 833 */
} // .end local v1 # "conditionItem":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
/* .line 836 */
} // :cond_0
return;
} // .end method
protected Boolean isInOutdoorCriticalTemperature ( ) {
/* .locals 2 */
/* .line 509 */
/* iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F */
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelCritical:F */
/* cmpl-float v0, v0, v1 */
/* if-ltz v0, :cond_0 */
/* iget v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F */
/* iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelEmergency:F */
/* cmpg-float v0, v0, v1 */
/* if-gez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void onCloudUpdated ( Long p0, java.util.Map p1 ) {
/* .locals 4 */
/* .param p1, "summary" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(J", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 453 */
/* .local p3, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;" */
/* const-wide/16 v0, 0x2 */
/* and-long/2addr v0, p1 */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 454 */
v0 = this.mBackgroundHandler;
v1 = this.mLoadFileRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 455 */
v0 = this.mBackgroundHandler;
v1 = this.mLoadFileRunnable;
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 457 */
} // :cond_0
/* const-wide/16 v0, 0x4 */
/* and-long/2addr v0, p1 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 458 */
final String v0 = "TemperatureGap"; // const-string v0, "TemperatureGap"
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* iput v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureGap:F */
/* .line 460 */
} // :cond_1
return;
} // .end method
protected void onConditionEvent ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "event" # I */
/* .line 277 */
/* and-int/lit8 v0, p1, 0x2 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 278 */
v0 = this.mBackgroundHandler;
v1 = this.mUpdateConditionRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 279 */
v0 = this.mBackgroundHandler;
v1 = this.mUpdateConditionRunnable;
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 281 */
} // :cond_0
return;
} // .end method
protected void onNtcTemperatureEvent ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "event" # I */
/* .line 297 */
/* and-int/lit8 v0, p1, 0x2 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 298 */
v0 = this.mBackgroundHandler;
v1 = this.mUpdateNtcTemperatureRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 299 */
v0 = this.mBackgroundHandler;
v1 = this.mUpdateNtcTemperatureRunnable;
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 301 */
} // :cond_0
return;
} // .end method
protected void onSafetyBrightnessEvent ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "event" # I */
/* .line 287 */
/* and-int/lit8 v0, p1, 0x2 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 288 */
v0 = this.mBackgroundHandler;
v1 = this.mUpdateSafetyBrightnessRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 289 */
v0 = this.mBackgroundHandler;
v1 = this.mUpdateSafetyBrightnessRunnable;
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 291 */
} // :cond_0
return;
} // .end method
protected void onTemperatureEvent ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "event" # I */
/* .line 266 */
/* and-int/lit8 v0, p1, 0x2 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 267 */
v0 = this.mBackgroundHandler;
v1 = this.mUpdateTemperatureRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 268 */
v0 = this.mBackgroundHandler;
v1 = this.mUpdateTemperatureRunnable;
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 271 */
} // :cond_0
return;
} // .end method
public void outDoorStateChanged ( ) {
/* .locals 1 */
/* .line 473 */
int v0 = 4; // const/4 v0, 0x4
/* invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V */
/* .line 474 */
return;
} // .end method
public void setDolbyEnabled ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 736 */
/* iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsDolbyEnabled:Z */
/* if-eq v0, p1, :cond_0 */
/* .line 737 */
/* iput-boolean p1, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsDolbyEnabled:Z */
/* .line 738 */
v0 = this.mBackgroundHandler;
v1 = this.mUpdateOverrideConditionRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 739 */
v0 = this.mBackgroundHandler;
v1 = this.mUpdateOverrideConditionRunnable;
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 741 */
} // :cond_0
return;
} // .end method
public void setHdrLayerPresent ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isHdrLayerPresent" # Z */
/* .line 728 */
/* iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsHdrLayerPresent:Z */
/* if-eq v0, p1, :cond_0 */
/* .line 729 */
/* iput-boolean p1, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsHdrLayerPresent:Z */
/* .line 730 */
v0 = this.mBackgroundHandler;
v1 = this.mUpdateOverrideConditionRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 731 */
v0 = this.mBackgroundHandler;
v1 = this.mUpdateOverrideConditionRunnable;
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 733 */
} // :cond_0
return;
} // .end method
public void setSunlightState ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "active" # Z */
/* .line 788 */
/* iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mSunlightModeActive:Z */
/* if-eq p1, v0, :cond_0 */
/* .line 789 */
/* iput-boolean p1, p0, Lcom/android/server/display/ThermalBrightnessController;->mSunlightModeActive:Z */
/* .line 790 */
v0 = this.mBackgroundHandler;
v1 = this.mUpdateOutdoorRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 791 */
v0 = this.mBackgroundHandler;
v1 = this.mUpdateOutdoorRunnable;
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 793 */
} // :cond_0
return;
} // .end method
public void updateForegroundApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "foregroundAppPackageName" # Ljava/lang/String; */
/* .line 764 */
/* iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsThermalBrightnessBoostEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 765 */
this.mForegroundAppPackageName = p1;
/* .line 766 */
/* invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateThermalBrightnessBoostCondition()V */
/* .line 768 */
} // :cond_0
return;
} // .end method
public void updateOutdoorThermalAppCategoryList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 756 */
/* .local p1, "outdoorThermalAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
this.mOutdoorThermalAppList = p1;
/* .line 757 */
return;
} // .end method
protected void updateScreenState ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "displayState" # I */
/* .param p2, "displayPolicy" # I */
/* .line 717 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_1 */
int v1 = 3; // const/4 v1, 0x3
/* if-eq p2, v1, :cond_0 */
/* if-ne p2, v0, :cond_1 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 720 */
/* .local v0, "screenOn":Z */
} // :goto_0
/* iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mScreenOn:Z */
/* if-eq v0, v1, :cond_2 */
/* .line 721 */
/* iput-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mScreenOn:Z */
/* .line 722 */
v1 = this.mBackgroundHandler;
v2 = this.mUpdateOutdoorRunnable;
(( android.os.Handler ) v1 ).removeCallbacks ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 723 */
v1 = this.mBackgroundHandler;
v2 = this.mUpdateOutdoorRunnable;
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 725 */
} // :cond_2
return;
} // .end method
