.class Lcom/android/server/display/DaemonSensorPolicy$2;
.super Landroid/content/BroadcastReceiver;
.source "DaemonSensorPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/DaemonSensorPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/DaemonSensorPolicy;


# direct methods
.method constructor <init>(Lcom/android/server/display/DaemonSensorPolicy;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/DaemonSensorPolicy;

    .line 156
    iput-object p1, p0, Lcom/android/server/display/DaemonSensorPolicy$2;->this$0:Lcom/android/server/display/DaemonSensorPolicy;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 159
    invoke-static {}, Lcom/android/server/display/DaemonSensorPolicy;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device idle changed, mDeviceIdle: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DaemonSensorPolicy$2;->this$0:Lcom/android/server/display/DaemonSensorPolicy;

    invoke-static {v2}, Lcom/android/server/display/DaemonSensorPolicy;->-$$Nest$fgetmIsDeviceIdleMode(Lcom/android/server/display/DaemonSensorPolicy;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    iget-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy$2;->this$0:Lcom/android/server/display/DaemonSensorPolicy;

    invoke-static {v0}, Lcom/android/server/display/DaemonSensorPolicy;->-$$Nest$fgetmHandler(Lcom/android/server/display/DaemonSensorPolicy;)Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;->sendEmptyMessage(I)Z

    .line 161
    return-void
.end method
