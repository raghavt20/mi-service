.class final Lcom/android/server/display/SwipeUpWindow$SwipeUpWindowHandler;
.super Landroid/os/Handler;
.source "SwipeUpWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/SwipeUpWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SwipeUpWindowHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/SwipeUpWindow;


# direct methods
.method public constructor <init>(Lcom/android/server/display/SwipeUpWindow;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 497
    iput-object p1, p0, Lcom/android/server/display/SwipeUpWindow$SwipeUpWindowHandler;->this$0:Lcom/android/server/display/SwipeUpWindow;

    .line 498
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 499
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .line 503
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 514
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow$SwipeUpWindowHandler;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow;->releaseSwipeWindow()V

    .line 515
    goto :goto_0

    .line 517
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow$SwipeUpWindowHandler;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v0}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$mhandleScreenOff(Lcom/android/server/display/SwipeUpWindow;)V

    .line 518
    goto :goto_0

    .line 511
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow$SwipeUpWindowHandler;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v0}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$mplayIconAndTipHideAnimation(Lcom/android/server/display/SwipeUpWindow;)V

    .line 512
    goto :goto_0

    .line 508
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow$SwipeUpWindowHandler;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v0}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$mplayIconAnimation(Lcom/android/server/display/SwipeUpWindow;)V

    .line 509
    goto :goto_0

    .line 505
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow$SwipeUpWindowHandler;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v0}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$msetLockStateWithLongAnimation(Lcom/android/server/display/SwipeUpWindow;)V

    .line 506
    nop

    .line 522
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
