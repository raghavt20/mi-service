class com.android.server.display.SwipeUpWindow$7 implements android.view.View$OnTouchListener {
	 /* .source "SwipeUpWindow.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/SwipeUpWindow; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
private Float startTouchY;
final com.android.server.display.SwipeUpWindow this$0; //synthetic
/* # direct methods */
 com.android.server.display.SwipeUpWindow$7 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/SwipeUpWindow; */
/* .line 578 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Boolean onTouch ( android.view.View p0, android.view.MotionEvent p1 ) {
/* .locals 23 */
/* .param p1, "v" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .line 583 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p2 */
v2 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$minitVelocityTrackerIfNotExists ( v2 );
/* .line 584 */
v2 = /* invoke-virtual/range {p2 ..p2}, Landroid/view/MotionEvent;->getAction()I */
/* packed-switch v2, :pswitch_data_0 */
int v3 = 1; // const/4 v3, 0x1
/* goto/16 :goto_1 */
/* .line 601 */
/* :pswitch_0 */
v2 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmHandler ( v2 );
/* const/16 v4, 0x65 */
(( android.os.Handler ) v2 ).removeMessages ( v4 ); // invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V
/* .line 603 */
v2 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmVelocityTracker ( v2 );
(( android.view.VelocityTracker ) v2 ).addMovement ( v1 ); // invoke-virtual {v2, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V
/* .line 605 */
v2 = /* invoke-virtual/range {p2 ..p2}, Landroid/view/MotionEvent;->getRawY()F */
/* iget v4, v0, Lcom/android/server/display/SwipeUpWindow$7;->startTouchY:F */
/* sub-float/2addr v2, v4 */
/* .line 606 */
/* .local v2, "offsetY":F */
v4 = this.this$0;
v4 = com.android.server.display.SwipeUpWindow .-$$Nest$fgetmStartPer ( v4 );
v5 = this.this$0;
v5 = com.android.server.display.SwipeUpWindow .-$$Nest$fgetmScreenHeight ( v5 );
/* div-int/lit8 v5, v5, 0x3 */
/* int-to-float v5, v5 */
/* div-float v5, v2, v5 */
/* add-float/2addr v4, v5 */
/* .line 607 */
/* .local v4, "per":F */
v5 = this.this$0;
v6 = com.android.server.display.SwipeUpWindow .-$$Nest$fgetmScreenHeight ( v5 );
/* int-to-float v6, v6 */
/* div-float v6, v2, v6 */
/* .line 608 */
/* const/high16 v7, 0x3f800000 # 1.0f */
v5 = (( com.android.server.display.SwipeUpWindow ) v5 ).afterFrictionValue ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Lcom/android/server/display/SwipeUpWindow;->afterFrictionValue(FF)F
/* .line 607 */
int v6 = 0; // const/4 v6, 0x0
v5 = android.util.MathUtils .min ( v6,v5 );
/* .line 609 */
/* .local v5, "frictionY":F */
/* const/high16 v6, 0x40400000 # 3.0f */
/* mul-float/2addr v6, v4 */
/* add-float/2addr v6, v7 */
v6 = android.util.MathUtils .min ( v7,v6 );
/* .line 611 */
/* .local v6, "alpha":F */
v7 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmTipView ( v7 );
v7 = (( android.widget.TextView ) v7 ).getTop ( ); // invoke-virtual {v7}, Landroid/widget/TextView;->getTop()I
/* int-to-float v7, v7 */
/* const/high16 v8, 0x43160000 # 150.0f */
/* mul-float/2addr v8, v5 */
/* add-float/2addr v7, v8 */
/* .line 612 */
/* .local v7, "tipY":F */
/* const v8, 0x3ecccccd # 0.4f */
/* mul-float v15, v6, v8 */
/* .line 613 */
/* .local v15, "tipAlpha":F */
v8 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmIconView ( v8 );
v8 = (( android.widget.ImageView ) v8 ).getTop ( ); // invoke-virtual {v8}, Landroid/widget/ImageView;->getTop()I
/* int-to-float v8, v8 */
/* const/high16 v9, 0x42480000 # 50.0f */
/* mul-float/2addr v9, v5 */
/* add-float v14, v8, v9 */
/* .line 615 */
/* .local v14, "iconY":F */
v8 = this.this$0;
v8 = com.android.server.display.SwipeUpWindow .-$$Nest$fgetmScrollAnimationNeedInit ( v8 );
if ( v8 != null) { // if-eqz v8, :cond_0
	 /* .line 616 */
	 v8 = this.this$0;
	 int v9 = 0; // const/4 v9, 0x0
	 com.android.server.display.SwipeUpWindow .-$$Nest$fputmScrollAnimationNeedInit ( v8,v9 );
	 /* .line 618 */
	 v8 = this.this$0;
	 /* const v9, 0x4476bd78 */
	 /* const v10, 0x3f666666 # 0.9f */
	 com.android.server.display.SwipeUpWindow .-$$Nest$mstartGradientShadowAnimation ( v8,v9,v10,v4 );
	 /* .line 620 */
	 v8 = this.this$0;
	 com.android.server.display.SwipeUpWindow .-$$Nest$fgetmIconSpringAnimation ( v8 );
	 (( com.android.server.display.SwipeUpWindow$DualSpringAnimation ) v8 ).cancel ( ); // invoke-virtual {v8}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->cancel()V
	 /* .line 621 */
	 v8 = this.this$0;
	 com.android.server.display.SwipeUpWindow .-$$Nest$fgetmTipSpringAnimation ( v8 );
	 (( com.android.server.display.SwipeUpWindow$DualSpringAnimation ) v8 ).cancel ( ); // invoke-virtual {v8}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->cancel()V
	 /* .line 622 */
	 v13 = this.this$0;
	 /* new-instance v12, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation; */
	 com.android.server.display.SwipeUpWindow .-$$Nest$fgetmIconView ( v13 );
	 v18 = com.android.server.display.animation.SpringAnimation.Y;
	 /* const v19, 0x4476bd78 */
	 /* const v20, 0x3f666666 # 0.9f */
	 /* .line 623 */
	 /* move-object/from16 v16, v13 */
	 /* move/from16 v21, v14 */
	 /* invoke-static/range {v16 ..v21}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$mcreatSpringAnimation(Lcom/android/server/display/SwipeUpWindow;Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation; */
	 v8 = this.this$0;
	 com.android.server.display.SwipeUpWindow .-$$Nest$fgetmIconView ( v8 );
	 v10 = com.android.server.display.animation.SpringAnimation.ALPHA;
	 /* const v16, 0x4476bd78 */
	 /* const v17, 0x3f666666 # 0.9f */
	 /* .line 625 */
	 /* move-object v3, v11 */
	 /* move/from16 v11, v16 */
	 /* move-object/from16 v22, v12 */
	 /* move/from16 v12, v17 */
	 /* move/from16 v16, v2 */
	 /* move-object v2, v13 */
} // .end local v2 # "offsetY":F
/* .local v16, "offsetY":F */
/* move v13, v6 */
/* invoke-static/range {v8 ..v13}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$mcreatSpringAnimation(Lcom/android/server/display/SwipeUpWindow;Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation; */
/* move-object/from16 v9, v22 */
/* invoke-direct {v9, v2, v3, v8}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;-><init>(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/animation/SpringAnimation;Lcom/android/server/display/animation/SpringAnimation;)V */
com.android.server.display.SwipeUpWindow .-$$Nest$fputmIconSpringAnimation ( v2,v9 );
/* .line 628 */
v2 = this.this$0;
/* new-instance v3, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation; */
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmTipView ( v2 );
v11 = com.android.server.display.animation.SpringAnimation.Y;
/* const v12, 0x4476bd78 */
/* const v13, 0x3f666666 # 0.9f */
/* .line 629 */
/* move-object v9, v2 */
/* move v8, v14 */
} // .end local v14 # "iconY":F
/* .local v8, "iconY":F */
/* move v14, v7 */
/* invoke-static/range {v9 ..v14}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$mcreatSpringAnimation(Lcom/android/server/display/SwipeUpWindow;Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation; */
v9 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmTipView ( v9 );
v11 = com.android.server.display.animation.SpringAnimation.ALPHA;
/* .line 631 */
/* move/from16 v17, v5 */
/* move-object v5, v14 */
} // .end local v5 # "frictionY":F
/* .local v17, "frictionY":F */
/* move v14, v15 */
/* invoke-static/range {v9 ..v14}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$mcreatSpringAnimation(Lcom/android/server/display/SwipeUpWindow;Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation; */
/* invoke-direct {v3, v2, v5, v9}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;-><init>(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/animation/SpringAnimation;Lcom/android/server/display/animation/SpringAnimation;)V */
com.android.server.display.SwipeUpWindow .-$$Nest$fputmTipSpringAnimation ( v2,v3 );
/* .line 634 */
v2 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmIconSpringAnimation ( v2 );
(( com.android.server.display.SwipeUpWindow$DualSpringAnimation ) v2 ).start ( ); // invoke-virtual {v2}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->start()V
/* .line 635 */
v2 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmTipSpringAnimation ( v2 );
(( com.android.server.display.SwipeUpWindow$DualSpringAnimation ) v2 ).start ( ); // invoke-virtual {v2}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->start()V
int v3 = 1; // const/4 v3, 0x1
/* goto/16 :goto_1 */
/* .line 637 */
} // .end local v8 # "iconY":F
} // .end local v16 # "offsetY":F
} // .end local v17 # "frictionY":F
/* .restart local v2 # "offsetY":F */
/* .restart local v5 # "frictionY":F */
/* .restart local v14 # "iconY":F */
} // :cond_0
/* move/from16 v16, v2 */
/* move/from16 v17, v5 */
/* move v8, v14 */
} // .end local v2 # "offsetY":F
} // .end local v5 # "frictionY":F
} // .end local v14 # "iconY":F
/* .restart local v8 # "iconY":F */
/* .restart local v16 # "offsetY":F */
/* .restart local v17 # "frictionY":F */
v2 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmGradientShadowSpringAnimation ( v2 );
(( com.android.server.display.animation.SpringAnimation ) v2 ).animateToFinalPosition ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/server/display/animation/SpringAnimation;->animateToFinalPosition(F)V
/* .line 638 */
v2 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmIconSpringAnimation ( v2 );
(( com.android.server.display.SwipeUpWindow$DualSpringAnimation ) v2 ).animateToFinalPosition ( v8, v6 ); // invoke-virtual {v2, v8, v6}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->animateToFinalPosition(FF)V
/* .line 639 */
v2 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmTipSpringAnimation ( v2 );
(( com.android.server.display.SwipeUpWindow$DualSpringAnimation ) v2 ).animateToFinalPosition ( v7, v15 ); // invoke-virtual {v2, v7, v15}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->animateToFinalPosition(FF)V
/* .line 641 */
int v3 = 1; // const/4 v3, 0x1
/* goto/16 :goto_1 */
/* .line 644 */
} // .end local v4 # "per":F
} // .end local v6 # "alpha":F
} // .end local v7 # "tipY":F
} // .end local v8 # "iconY":F
} // .end local v15 # "tipAlpha":F
} // .end local v16 # "offsetY":F
} // .end local v17 # "frictionY":F
/* :pswitch_1 */
v2 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmVelocityTracker ( v2 );
/* .line 645 */
/* .local v2, "velocityTracker":Landroid/view/VelocityTracker; */
/* const/16 v3, 0x3e8 */
(( android.view.VelocityTracker ) v2 ).computeCurrentVelocity ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V
/* .line 646 */
v3 = (( android.view.VelocityTracker ) v2 ).getYVelocity ( ); // invoke-virtual {v2}, Landroid/view/VelocityTracker;->getYVelocity()F
/* .line 647 */
/* .local v3, "curVelocitY":F */
v4 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$mrecycleVelocityTracker ( v4 );
/* .line 649 */
v4 = this.this$0;
v4 = com.android.server.display.SwipeUpWindow .-$$Nest$fgetmStartPer ( v4 );
v5 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmAnimationState ( v5 );
v5 = (( com.android.server.display.SwipeUpWindow$AnimationState ) v5 ).getPerState ( ); // invoke-virtual {v5}, Lcom/android/server/display/SwipeUpWindow$AnimationState;->getPerState()F
/* cmpl-float v4, v4, v5 */
/* if-nez v4, :cond_1 */
/* .line 650 */
int v4 = 1; // const/4 v4, 0x1
/* .line 653 */
} // :cond_1
v4 = /* invoke-virtual/range {p2 ..p2}, Landroid/view/MotionEvent;->getRawY()F */
/* iget v5, v0, Lcom/android/server/display/SwipeUpWindow$7;->startTouchY:F */
/* sub-float/2addr v4, v5 */
/* .line 655 */
/* .local v4, "distance":F */
/* const/high16 v5, -0x3b860000 # -1000.0f */
/* cmpg-float v5, v3, v5 */
/* const/high16 v6, 0x43480000 # 200.0f */
/* if-gez v5, :cond_2 */
/* neg-float v5, v4 */
/* cmpl-float v5, v5, v6 */
/* if-ltz v5, :cond_2 */
/* .line 656 */
v5 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$msetUnlockState ( v5 );
int v3 = 1; // const/4 v3, 0x1
/* goto/16 :goto_1 */
/* .line 657 */
} // :cond_2
/* const/high16 v5, 0x447a0000 # 1000.0f */
/* cmpl-float v5, v3, v5 */
/* if-lez v5, :cond_3 */
/* cmpl-float v5, v4, v6 */
/* if-ltz v5, :cond_3 */
/* .line 658 */
v5 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$msetLockStateWithShortAnimation ( v5 );
int v3 = 1; // const/4 v3, 0x1
/* .line 660 */
} // :cond_3
/* neg-float v5, v4 */
v6 = this.this$0;
v6 = com.android.server.display.SwipeUpWindow .-$$Nest$fgetmUnlockDistance ( v6 );
/* cmpl-float v5, v5, v6 */
/* if-lez v5, :cond_4 */
/* .line 661 */
v5 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$msetUnlockState ( v5 );
int v3 = 1; // const/4 v3, 0x1
/* .line 662 */
} // :cond_4
v5 = this.this$0;
v5 = com.android.server.display.SwipeUpWindow .-$$Nest$fgetmUnlockDistance ( v5 );
/* cmpl-float v5, v4, v5 */
/* if-lez v5, :cond_5 */
/* .line 663 */
v5 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$msetLockStateWithShortAnimation ( v5 );
int v3 = 1; // const/4 v3, 0x1
/* .line 665 */
} // :cond_5
v5 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$msetWakeState ( v5 );
/* .line 668 */
int v3 = 1; // const/4 v3, 0x1
/* .line 586 */
} // .end local v2 # "velocityTracker":Landroid/view/VelocityTracker;
} // .end local v3 # "curVelocitY":F
} // .end local v4 # "distance":F
/* :pswitch_2 */
v2 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmVelocityTracker ( v2 );
/* if-nez v2, :cond_6 */
/* .line 587 */
v2 = this.this$0;
android.view.VelocityTracker .obtain ( );
com.android.server.display.SwipeUpWindow .-$$Nest$fputmVelocityTracker ( v2,v3 );
/* .line 590 */
} // :cond_6
v2 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmVelocityTracker ( v2 );
(( android.view.VelocityTracker ) v2 ).clear ( ); // invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V
/* .line 592 */
} // :goto_0
v2 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmVelocityTracker ( v2 );
(( android.view.VelocityTracker ) v2 ).addMovement ( v1 ); // invoke-virtual {v2, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V
/* .line 594 */
v2 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$fgetmAnimationState ( v2 );
v3 = (( com.android.server.display.SwipeUpWindow$AnimationState ) v3 ).getCurrentState ( ); // invoke-virtual {v3}, Lcom/android/server/display/SwipeUpWindow$AnimationState;->getCurrentState()F
com.android.server.display.SwipeUpWindow .-$$Nest$fputmStartPer ( v2,v3 );
/* .line 595 */
v2 = /* invoke-virtual/range {p2 ..p2}, Landroid/view/MotionEvent;->getRawY()F */
/* iput v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->startTouchY:F */
/* .line 596 */
v2 = this.this$0;
com.android.server.display.SwipeUpWindow .-$$Nest$mresetIconAnimation ( v2 );
/* .line 597 */
v2 = this.this$0;
int v3 = 1; // const/4 v3, 0x1
com.android.server.display.SwipeUpWindow .-$$Nest$fputmScrollAnimationNeedInit ( v2,v3 );
/* .line 598 */
/* nop */
/* .line 672 */
} // :goto_1
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
