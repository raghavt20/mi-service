.class Lcom/android/server/display/SwipeUpWindow$7;
.super Ljava/lang/Object;
.source "SwipeUpWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/SwipeUpWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private startTouchY:F

.field final synthetic this$0:Lcom/android/server/display/SwipeUpWindow;


# direct methods
.method constructor <init>(Lcom/android/server/display/SwipeUpWindow;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/SwipeUpWindow;

    .line 578
    iput-object p1, p0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 23
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .line 583
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$minitVelocityTrackerIfNotExists(Lcom/android/server/display/SwipeUpWindow;)V

    .line 584
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    const/4 v3, 0x1

    goto/16 :goto_1

    .line 601
    :pswitch_0
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmHandler(Lcom/android/server/display/SwipeUpWindow;)Landroid/os/Handler;

    move-result-object v2

    const/16 v4, 0x65

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 603
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmVelocityTracker(Lcom/android/server/display/SwipeUpWindow;)Landroid/view/VelocityTracker;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 605
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget v4, v0, Lcom/android/server/display/SwipeUpWindow$7;->startTouchY:F

    sub-float/2addr v2, v4

    .line 606
    .local v2, "offsetY":F
    iget-object v4, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v4}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmStartPer(Lcom/android/server/display/SwipeUpWindow;)F

    move-result v4

    iget-object v5, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v5}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmScreenHeight(Lcom/android/server/display/SwipeUpWindow;)I

    move-result v5

    div-int/lit8 v5, v5, 0x3

    int-to-float v5, v5

    div-float v5, v2, v5

    add-float/2addr v4, v5

    .line 607
    .local v4, "per":F
    iget-object v5, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v5}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmScreenHeight(Lcom/android/server/display/SwipeUpWindow;)I

    move-result v6

    int-to-float v6, v6

    div-float v6, v2, v6

    .line 608
    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6, v7}, Lcom/android/server/display/SwipeUpWindow;->afterFrictionValue(FF)F

    move-result v5

    .line 607
    const/4 v6, 0x0

    invoke-static {v6, v5}, Landroid/util/MathUtils;->min(FF)F

    move-result v5

    .line 609
    .local v5, "frictionY":F
    const/high16 v6, 0x40400000    # 3.0f

    mul-float/2addr v6, v4

    add-float/2addr v6, v7

    invoke-static {v7, v6}, Landroid/util/MathUtils;->min(FF)F

    move-result v6

    .line 611
    .local v6, "alpha":F
    iget-object v7, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v7}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmTipView(Lcom/android/server/display/SwipeUpWindow;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/TextView;->getTop()I

    move-result v7

    int-to-float v7, v7

    const/high16 v8, 0x43160000    # 150.0f

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    .line 612
    .local v7, "tipY":F
    const v8, 0x3ecccccd    # 0.4f

    mul-float v15, v6, v8

    .line 613
    .local v15, "tipAlpha":F
    iget-object v8, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v8}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmIconView(Lcom/android/server/display/SwipeUpWindow;)Landroid/widget/ImageView;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/ImageView;->getTop()I

    move-result v8

    int-to-float v8, v8

    const/high16 v9, 0x42480000    # 50.0f

    mul-float/2addr v9, v5

    add-float v14, v8, v9

    .line 615
    .local v14, "iconY":F
    iget-object v8, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v8}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmScrollAnimationNeedInit(Lcom/android/server/display/SwipeUpWindow;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 616
    iget-object v8, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fputmScrollAnimationNeedInit(Lcom/android/server/display/SwipeUpWindow;Z)V

    .line 618
    iget-object v8, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    const v9, 0x4476bd78

    const v10, 0x3f666666    # 0.9f

    invoke-static {v8, v9, v10, v4}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$mstartGradientShadowAnimation(Lcom/android/server/display/SwipeUpWindow;FFF)V

    .line 620
    iget-object v8, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v8}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmIconSpringAnimation(Lcom/android/server/display/SwipeUpWindow;)Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->cancel()V

    .line 621
    iget-object v8, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v8}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmTipSpringAnimation(Lcom/android/server/display/SwipeUpWindow;)Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->cancel()V

    .line 622
    iget-object v13, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    new-instance v12, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    invoke-static {v13}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmIconView(Lcom/android/server/display/SwipeUpWindow;)Landroid/widget/ImageView;

    move-result-object v17

    sget-object v18, Lcom/android/server/display/animation/SpringAnimation;->Y:Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;

    const v19, 0x4476bd78

    const v20, 0x3f666666    # 0.9f

    .line 623
    move-object/from16 v16, v13

    move/from16 v21, v14

    invoke-static/range {v16 .. v21}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$mcreatSpringAnimation(Lcom/android/server/display/SwipeUpWindow;Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object v11

    iget-object v8, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v8}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmIconView(Lcom/android/server/display/SwipeUpWindow;)Landroid/widget/ImageView;

    move-result-object v9

    sget-object v10, Lcom/android/server/display/animation/SpringAnimation;->ALPHA:Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;

    const v16, 0x4476bd78

    const v17, 0x3f666666    # 0.9f

    .line 625
    move-object v3, v11

    move/from16 v11, v16

    move-object/from16 v22, v12

    move/from16 v12, v17

    move/from16 v16, v2

    move-object v2, v13

    .end local v2    # "offsetY":F
    .local v16, "offsetY":F
    move v13, v6

    invoke-static/range {v8 .. v13}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$mcreatSpringAnimation(Lcom/android/server/display/SwipeUpWindow;Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object v8

    move-object/from16 v9, v22

    invoke-direct {v9, v2, v3, v8}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;-><init>(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/animation/SpringAnimation;Lcom/android/server/display/animation/SpringAnimation;)V

    invoke-static {v2, v9}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fputmIconSpringAnimation(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;)V

    .line 628
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    new-instance v3, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmTipView(Lcom/android/server/display/SwipeUpWindow;)Landroid/widget/TextView;

    move-result-object v10

    sget-object v11, Lcom/android/server/display/animation/SpringAnimation;->Y:Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;

    const v12, 0x4476bd78

    const v13, 0x3f666666    # 0.9f

    .line 629
    move-object v9, v2

    move v8, v14

    .end local v14    # "iconY":F
    .local v8, "iconY":F
    move v14, v7

    invoke-static/range {v9 .. v14}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$mcreatSpringAnimation(Lcom/android/server/display/SwipeUpWindow;Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object v14

    iget-object v9, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v9}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmTipView(Lcom/android/server/display/SwipeUpWindow;)Landroid/widget/TextView;

    move-result-object v10

    sget-object v11, Lcom/android/server/display/animation/SpringAnimation;->ALPHA:Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;

    .line 631
    move/from16 v17, v5

    move-object v5, v14

    .end local v5    # "frictionY":F
    .local v17, "frictionY":F
    move v14, v15

    invoke-static/range {v9 .. v14}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$mcreatSpringAnimation(Lcom/android/server/display/SwipeUpWindow;Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object v9

    invoke-direct {v3, v2, v5, v9}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;-><init>(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/animation/SpringAnimation;Lcom/android/server/display/animation/SpringAnimation;)V

    invoke-static {v2, v3}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fputmTipSpringAnimation(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;)V

    .line 634
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmIconSpringAnimation(Lcom/android/server/display/SwipeUpWindow;)Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->start()V

    .line 635
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmTipSpringAnimation(Lcom/android/server/display/SwipeUpWindow;)Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->start()V

    const/4 v3, 0x1

    goto/16 :goto_1

    .line 637
    .end local v8    # "iconY":F
    .end local v16    # "offsetY":F
    .end local v17    # "frictionY":F
    .restart local v2    # "offsetY":F
    .restart local v5    # "frictionY":F
    .restart local v14    # "iconY":F
    :cond_0
    move/from16 v16, v2

    move/from16 v17, v5

    move v8, v14

    .end local v2    # "offsetY":F
    .end local v5    # "frictionY":F
    .end local v14    # "iconY":F
    .restart local v8    # "iconY":F
    .restart local v16    # "offsetY":F
    .restart local v17    # "frictionY":F
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmGradientShadowSpringAnimation(Lcom/android/server/display/SwipeUpWindow;)Lcom/android/server/display/animation/SpringAnimation;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/android/server/display/animation/SpringAnimation;->animateToFinalPosition(F)V

    .line 638
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmIconSpringAnimation(Lcom/android/server/display/SwipeUpWindow;)Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    move-result-object v2

    invoke-virtual {v2, v8, v6}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->animateToFinalPosition(FF)V

    .line 639
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmTipSpringAnimation(Lcom/android/server/display/SwipeUpWindow;)Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;

    move-result-object v2

    invoke-virtual {v2, v7, v15}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->animateToFinalPosition(FF)V

    .line 641
    const/4 v3, 0x1

    goto/16 :goto_1

    .line 644
    .end local v4    # "per":F
    .end local v6    # "alpha":F
    .end local v7    # "tipY":F
    .end local v8    # "iconY":F
    .end local v15    # "tipAlpha":F
    .end local v16    # "offsetY":F
    .end local v17    # "frictionY":F
    :pswitch_1
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmVelocityTracker(Lcom/android/server/display/SwipeUpWindow;)Landroid/view/VelocityTracker;

    move-result-object v2

    .line 645
    .local v2, "velocityTracker":Landroid/view/VelocityTracker;
    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 646
    invoke-virtual {v2}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v3

    .line 647
    .local v3, "curVelocitY":F
    iget-object v4, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v4}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$mrecycleVelocityTracker(Lcom/android/server/display/SwipeUpWindow;)V

    .line 649
    iget-object v4, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v4}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmStartPer(Lcom/android/server/display/SwipeUpWindow;)F

    move-result v4

    iget-object v5, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v5}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmAnimationState(Lcom/android/server/display/SwipeUpWindow;)Lcom/android/server/display/SwipeUpWindow$AnimationState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/display/SwipeUpWindow$AnimationState;->getPerState()F

    move-result v5

    cmpl-float v4, v4, v5

    if-nez v4, :cond_1

    .line 650
    const/4 v4, 0x1

    return v4

    .line 653
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    iget v5, v0, Lcom/android/server/display/SwipeUpWindow$7;->startTouchY:F

    sub-float/2addr v4, v5

    .line 655
    .local v4, "distance":F
    const/high16 v5, -0x3b860000    # -1000.0f

    cmpg-float v5, v3, v5

    const/high16 v6, 0x43480000    # 200.0f

    if-gez v5, :cond_2

    neg-float v5, v4

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_2

    .line 656
    iget-object v5, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v5}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$msetUnlockState(Lcom/android/server/display/SwipeUpWindow;)V

    const/4 v3, 0x1

    goto/16 :goto_1

    .line 657
    :cond_2
    const/high16 v5, 0x447a0000    # 1000.0f

    cmpl-float v5, v3, v5

    if-lez v5, :cond_3

    cmpl-float v5, v4, v6

    if-ltz v5, :cond_3

    .line 658
    iget-object v5, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v5}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$msetLockStateWithShortAnimation(Lcom/android/server/display/SwipeUpWindow;)V

    const/4 v3, 0x1

    goto :goto_1

    .line 660
    :cond_3
    neg-float v5, v4

    iget-object v6, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v6}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmUnlockDistance(Lcom/android/server/display/SwipeUpWindow;)F

    move-result v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_4

    .line 661
    iget-object v5, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v5}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$msetUnlockState(Lcom/android/server/display/SwipeUpWindow;)V

    const/4 v3, 0x1

    goto :goto_1

    .line 662
    :cond_4
    iget-object v5, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v5}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmUnlockDistance(Lcom/android/server/display/SwipeUpWindow;)F

    move-result v5

    cmpl-float v5, v4, v5

    if-lez v5, :cond_5

    .line 663
    iget-object v5, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v5}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$msetLockStateWithShortAnimation(Lcom/android/server/display/SwipeUpWindow;)V

    const/4 v3, 0x1

    goto :goto_1

    .line 665
    :cond_5
    iget-object v5, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v5}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$msetWakeState(Lcom/android/server/display/SwipeUpWindow;)V

    .line 668
    const/4 v3, 0x1

    goto :goto_1

    .line 586
    .end local v2    # "velocityTracker":Landroid/view/VelocityTracker;
    .end local v3    # "curVelocitY":F
    .end local v4    # "distance":F
    :pswitch_2
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmVelocityTracker(Lcom/android/server/display/SwipeUpWindow;)Landroid/view/VelocityTracker;

    move-result-object v2

    if-nez v2, :cond_6

    .line 587
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fputmVelocityTracker(Lcom/android/server/display/SwipeUpWindow;Landroid/view/VelocityTracker;)V

    goto :goto_0

    .line 590
    :cond_6
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmVelocityTracker(Lcom/android/server/display/SwipeUpWindow;)Landroid/view/VelocityTracker;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    .line 592
    :goto_0
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmVelocityTracker(Lcom/android/server/display/SwipeUpWindow;)Landroid/view/VelocityTracker;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 594
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fgetmAnimationState(Lcom/android/server/display/SwipeUpWindow;)Lcom/android/server/display/SwipeUpWindow$AnimationState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/display/SwipeUpWindow$AnimationState;->getCurrentState()F

    move-result v3

    invoke-static {v2, v3}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fputmStartPer(Lcom/android/server/display/SwipeUpWindow;F)V

    .line 595
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iput v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->startTouchY:F

    .line 596
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-static {v2}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$mresetIconAnimation(Lcom/android/server/display/SwipeUpWindow;)V

    .line 597
    iget-object v2, v0, Lcom/android/server/display/SwipeUpWindow$7;->this$0:Lcom/android/server/display/SwipeUpWindow;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/android/server/display/SwipeUpWindow;->-$$Nest$fputmScrollAnimationNeedInit(Lcom/android/server/display/SwipeUpWindow;Z)V

    .line 598
    nop

    .line 672
    :goto_1
    return v3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
