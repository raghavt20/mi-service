.class final Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "DisplayPowerControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/DisplayPowerControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/DisplayPowerControllerImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/display/DisplayPowerControllerImpl;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 1422
    iput-object p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    .line 1423
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 1424
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 1428
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "is_dynamic_lockscreen_shown"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_1
    const-string v1, "curtain_anim_enabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_2
    const-string v1, "accessibility_display_inversion_enabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_3
    const-string v1, "screen_brightness_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 1437
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$mupdateCurtainAnimationEnabled(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    .line 1438
    goto :goto_2

    .line 1433
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$mupdateAutoBrightnessMode(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    .line 1434
    goto :goto_2

    .line 1430
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$mupdateColorInversionEnabled(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    .line 1431
    nop

    .line 1442
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x294f7102 -> :sswitch_3
        -0x20db1ad9 -> :sswitch_2
        -0xff61c60 -> :sswitch_1
        0x4211759e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
