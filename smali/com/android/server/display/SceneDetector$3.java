class com.android.server.display.SceneDetector$3 extends com.xiaomi.aon.IMiAONListener$Stub {
	 /* .source "SceneDetector.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/SceneDetector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.SceneDetector this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$Lv8E7MnLzpc2AXx-6uW6Za-0UrY ( com.android.server.display.SceneDetector$3 p0, Integer[] p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/SceneDetector$3;->lambda$onCallbackListener$0([I)V */
return;
} // .end method
 com.android.server.display.SceneDetector$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/SceneDetector; */
/* .line 258 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/xiaomi/aon/IMiAONListener$Stub;-><init>()V */
return;
} // .end method
private void lambda$onCallbackListener$0 ( Integer[] p0 ) { //synthethic
/* .locals 4 */
/* .param p1, "data" # [I */
/* .line 264 */
v0 = this.this$0;
v0 = com.android.server.display.SceneDetector .-$$Nest$fgetmAonState ( v0 );
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_2 */
/* .line 265 */
int v0 = 0; // const/4 v0, 0x0
/* aget v0, p1, v0 */
final String v2 = "SceneDetector"; // const-string v2, "SceneDetector"
int v3 = 1; // const/4 v3, 0x1
/* if-ne v0, v3, :cond_0 */
/* .line 266 */
v0 = this.this$0;
com.android.server.display.SceneDetector .-$$Nest$fgetmBrightnessControllerImpl ( v0 );
v1 = this.this$0;
v1 = com.android.server.display.SceneDetector .-$$Nest$fgetmPreAmbientLux ( v1 );
(( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).notifyAonFlareEvents ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->notifyAonFlareEvents(IF)V
/* .line 268 */
final String v0 = "aon flare algo suppress this darken event!"; // const-string v0, "aon flare algo suppress this darken event!"
android.util.Slog .i ( v2,v0 );
/* .line 270 */
} // :cond_0
v0 = this.this$0;
com.android.server.display.SceneDetector .-$$Nest$fgetmBrightnessControllerImpl ( v0 );
v3 = this.this$0;
v3 = com.android.server.display.SceneDetector .-$$Nest$fgetmPreAmbientLux ( v3 );
(( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).notifyAonFlareEvents ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->notifyAonFlareEvents(IF)V
/* .line 272 */
v0 = this.this$0;
v0 = com.android.server.display.SceneDetector .-$$Nest$fgetmIsMainDarkenEvent ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 273 */
v0 = this.this$0;
com.android.server.display.SceneDetector .-$$Nest$mupdateAutoBrightness ( v0 );
/* .line 274 */
final String v0 = "aon flare algo not suppress this darken event!"; // const-string v0, "aon flare algo not suppress this darken event!"
android.util.Slog .i ( v2,v0 );
/* .line 277 */
} // :cond_1
} // :goto_0
v0 = this.this$0;
com.android.server.display.SceneDetector .-$$Nest$munregisterAonFlareListener ( v0 );
/* .line 279 */
} // :cond_2
return;
} // .end method
/* # virtual methods */
public void onCallbackListener ( Integer p0, Integer[] p1 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .param p2, "data" # [I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 261 */
int v0 = 5; // const/4 v0, 0x5
/* if-ne p1, v0, :cond_0 */
/* .line 263 */
v0 = this.this$0;
com.android.server.display.SceneDetector .-$$Nest$fgetmSceneDetectorHandler ( v0 );
/* new-instance v1, Lcom/android/server/display/SceneDetector$3$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p2}, Lcom/android/server/display/SceneDetector$3$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/SceneDetector$3;[I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 281 */
} // :cond_0
return;
} // .end method
public void onImageAvailiable ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "frameId" # I */
/* .line 286 */
return;
} // .end method
