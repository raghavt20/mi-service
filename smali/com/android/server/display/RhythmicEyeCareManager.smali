.class public Lcom/android/server/display/RhythmicEyeCareManager;
.super Ljava/lang/Object;
.source "RhythmicEyeCareManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener;
    }
.end annotation


# static fields
.field private static final ALARM_TAG:Ljava/lang/String; = "rhythmic_eyecare_alarm"

.field private static final RHYTHMIC_APP_CATEGORY_TYPE_DEFAULT:I = 0x0

.field private static final RHYTHMIC_APP_CATEGORY_TYPE_IMAGE:I = 0x1

.field private static final RHYTHMIC_APP_CATEGORY_TYPE_READ:I = 0x2

.field private static final TAG:Ljava/lang/String; = "RhythmicEyeCareManager"


# instance fields
.field private mActivityTaskManager:Landroid/app/IActivityTaskManager;

.field private mAlarmIndex:I

.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mAlarmTimePoints:[I

.field private mAppCategoryMapper:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mAppChanged:Z

.field private mAppType:I

.field private mComponentName:Landroid/content/ComponentName;

.field private mDisplayState:I

.field private mHandler:Landroid/os/Handler;

.field private mIsDeskTopMode:Z

.field private mIsEnabled:Z

.field private final mKeyguardLockedStateListener:Lcom/android/internal/policy/IKeyguardLockedStateListener;

.field private final mOnAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

.field private mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

.field private final mProcessObserver:Landroid/app/IProcessObserver$Stub;

.field private final mRhythmicEyeCareAlarmReceiver:Landroid/content/BroadcastReceiver;

.field private mRhythmicEyeCareListener:Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener;

.field private final mTaskStackListener:Landroid/app/TaskStackListener;

.field private mTime:I

.field private mWindowManagerService:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method public static synthetic $r8$lambda$UB0zXyitIKMRgNy5P9lMtDeZ-eQ(Lcom/android/server/display/RhythmicEyeCareManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->lambda$enableRhythmicEyeCareMode$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$n6HRyEgeSe6p429zaw5YoPwjbFY(Lcom/android/server/display/RhythmicEyeCareManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->lambda$notifyScreenStateChanged$1()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmAlarmIndex(Lcom/android/server/display/RhythmicEyeCareManager;)I
    .locals 0

    iget p0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmIndex:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmAlarmManager(Lcom/android/server/display/RhythmicEyeCareManager;)Landroid/app/AlarmManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmManager:Landroid/app/AlarmManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAlarmTimePoints(Lcom/android/server/display/RhythmicEyeCareManager;)[I
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmTimePoints:[I

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/display/RhythmicEyeCareManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsEnabled(Lcom/android/server/display/RhythmicEyeCareManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmOnAlarmListener(Lcom/android/server/display/RhythmicEyeCareManager;)Landroid/app/AlarmManager$OnAlarmListener;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mOnAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmAlarmIndex(Lcom/android/server/display/RhythmicEyeCareManager;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmIndex:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mscheduleRhythmicAlarm(Lcom/android/server/display/RhythmicEyeCareManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->scheduleRhythmicAlarm()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetAlarm(Lcom/android/server/display/RhythmicEyeCareManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->setAlarm()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateState(Lcom/android/server/display/RhythmicEyeCareManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/RhythmicEyeCareManager;->updateState(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppCategoryMapper:Landroid/util/SparseArray;

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppType:I

    .line 54
    iput v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mDisplayState:I

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppChanged:Z

    .line 61
    new-instance v0, Lcom/android/server/display/RhythmicEyeCareManager$1;

    invoke-direct {v0, p0}, Lcom/android/server/display/RhythmicEyeCareManager$1;-><init>(Lcom/android/server/display/RhythmicEyeCareManager;)V

    iput-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mOnAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    .line 70
    new-instance v0, Lcom/android/server/display/RhythmicEyeCareManager$2;

    invoke-direct {v0, p0}, Lcom/android/server/display/RhythmicEyeCareManager$2;-><init>(Lcom/android/server/display/RhythmicEyeCareManager;)V

    iput-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mRhythmicEyeCareAlarmReceiver:Landroid/content/BroadcastReceiver;

    .line 88
    new-instance v0, Lcom/android/server/display/RhythmicEyeCareManager$3;

    invoke-direct {v0, p0}, Lcom/android/server/display/RhythmicEyeCareManager$3;-><init>(Lcom/android/server/display/RhythmicEyeCareManager;)V

    iput-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mTaskStackListener:Landroid/app/TaskStackListener;

    .line 95
    new-instance v0, Lcom/android/server/display/RhythmicEyeCareManager$4;

    invoke-direct {v0, p0}, Lcom/android/server/display/RhythmicEyeCareManager$4;-><init>(Lcom/android/server/display/RhythmicEyeCareManager;)V

    iput-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mKeyguardLockedStateListener:Lcom/android/internal/policy/IKeyguardLockedStateListener;

    .line 105
    new-instance v0, Lcom/android/server/display/RhythmicEyeCareManager$5;

    invoke-direct {v0, p0}, Lcom/android/server/display/RhythmicEyeCareManager$5;-><init>(Lcom/android/server/display/RhythmicEyeCareManager;)V

    iput-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mProcessObserver:Landroid/app/IProcessObserver$Stub;

    .line 126
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    .line 127
    const v1, 0x1103004d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmTimePoints:[I

    .line 128
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mHandler:Landroid/os/Handler;

    .line 129
    const-class v0, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 130
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    iput-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    .line 131
    invoke-direct {p0, p1}, Lcom/android/server/display/RhythmicEyeCareManager;->registerReceiver(Landroid/content/Context;)V

    .line 132
    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmManager:Landroid/app/AlarmManager;

    .line 133
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    .line 134
    return-void
.end method

.method private disableRhythmicEyeCareMode()V
    .locals 3

    .line 158
    iget-boolean v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsEnabled:Z

    if-nez v0, :cond_0

    .line 159
    return-void

    .line 162
    :cond_0
    const-string v0, "RhythmicEyeCareManager"

    const-string v1, "disableRhythmicEyeCareMode"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mRhythmicEyeCareListener:Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener;

    iget v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppType:I

    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->getMinuteOfDay()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener;->onRhythmicEyeCareChange(II)V

    .line 164
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppType:I

    .line 165
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppChanged:Z

    .line 166
    iput-boolean v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsEnabled:Z

    .line 167
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mComponentName:Landroid/content/ComponentName;

    .line 168
    iput v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmIndex:I

    .line 169
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mOnAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    if-eqz v0, :cond_1

    .line 170
    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-virtual {v1, v0}, Landroid/app/AlarmManager;->cancel(Landroid/app/AlarmManager$OnAlarmListener;)V

    .line 172
    :cond_1
    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->unregisterForegroundAppObserver()V

    .line 173
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mKeyguardLockedStateListener:Lcom/android/internal/policy/IKeyguardLockedStateListener;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService;->removeKeyguardLockedStateListener(Lcom/android/internal/policy/IKeyguardLockedStateListener;)V

    .line 174
    return-void
.end method

.method private enableRhythmicEyeCareMode()V
    .locals 2

    .line 145
    iget-boolean v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsEnabled:Z

    if-eqz v0, :cond_0

    .line 146
    return-void

    .line 149
    :cond_0
    const-string v0, "RhythmicEyeCareManager"

    const-string v1, "enableRhythmicEyeCareMode"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsEnabled:Z

    .line 151
    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->registerForegroundAppObserver()V

    .line 152
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mKeyguardLockedStateListener:Lcom/android/internal/policy/IKeyguardLockedStateListener;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService;->addKeyguardLockedStateListener(Lcom/android/internal/policy/IKeyguardLockedStateListener;)V

    .line 153
    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->scheduleRhythmicAlarm()V

    .line 154
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/RhythmicEyeCareManager$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/display/RhythmicEyeCareManager$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/RhythmicEyeCareManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 155
    return-void
.end method

.method private getAlarmDateString(J)Ljava/lang/String;
    .locals 8
    .param p1, "mills"    # J

    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 262
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "next alarm:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 264
    .local v1, "calendar":Ljava/util/Calendar;
    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 265
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 266
    .local v2, "month":I
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 268
    .local v3, "day":I
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    const/16 v5, 0xb

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 270
    .local v5, "hour":I
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    const/16 v6, 0xc

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 272
    .local v6, "minute":I
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    const/16 v4, 0xd

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 274
    .local v4, "second":I
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 275
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method private getAlarmInMills(I)J
    .locals 4
    .param p1, "time"    # I

    .line 246
    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->getMinuteOfDay()I

    move-result v0

    .line 247
    .local v0, "currentTime":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 248
    .local v1, "calendar":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 249
    if-lt v0, p1, :cond_0

    .line 250
    const/4 v2, 0x6

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 252
    :cond_0
    div-int/lit8 v2, p1, 0x3c

    const/16 v3, 0xb

    invoke-virtual {v1, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 253
    const/16 v2, 0xc

    rem-int/lit8 v3, p1, 0x3c

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 254
    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 255
    const/16 v2, 0xe

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 256
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/android/server/display/RhythmicEyeCareManager;->getAlarmDateString(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "RhythmicEyeCareManager"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method private getAppTypeByPkgName(Ljava/lang/String;)I
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 232
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppCategoryMapper:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 233
    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppCategoryMapper:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 234
    .local v1, "appType":I
    iget-object v2, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppCategoryMapper:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 235
    .local v2, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v2, :cond_0

    .line 236
    goto :goto_1

    .line 238
    :cond_0
    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 239
    return v1

    .line 232
    .end local v1    # "appType":I
    .end local v2    # "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 242
    .end local v0    # "i":I
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private getMinuteOfDay()I
    .locals 4

    .line 212
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 213
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 214
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 215
    .local v1, "hour":I
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 216
    .local v2, "minute":I
    mul-int/lit8 v3, v1, 0x3c

    add-int/2addr v3, v2

    return v3
.end method

.method private getNextAlarmTimeIndex()I
    .locals 4

    .line 220
    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->getMinuteOfDay()I

    move-result v0

    .line 221
    .local v0, "currentTime":I
    const/4 v1, 0x0

    .line 222
    .local v1, "index":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmTimePoints:[I

    array-length v3, v2

    if-ge v1, v3, :cond_0

    aget v3, v2, v1

    if-gt v3, v0, :cond_0

    .line 223
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 225
    :cond_0
    array-length v2, v2

    if-ne v1, v2, :cond_1

    .line 226
    const/4 v1, 0x0

    .line 228
    :cond_1
    return v1
.end method

.method private synthetic lambda$enableRhythmicEyeCareMode$0()V
    .locals 1

    .line 154
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/server/display/RhythmicEyeCareManager;->updateState(Z)V

    return-void
.end method

.method private synthetic lambda$notifyScreenStateChanged$1()V
    .locals 1

    .line 370
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/server/display/RhythmicEyeCareManager;->updateState(Z)V

    return-void
.end method

.method private registerForegroundAppObserver()V
    .locals 3

    .line 195
    :try_start_0
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mTaskStackListener:Landroid/app/TaskStackListener;

    invoke-interface {v0, v1}, Landroid/app/IActivityTaskManager;->registerTaskStackListener(Landroid/app/ITaskStackListener;)V

    .line 196
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mProcessObserver:Landroid/app/IProcessObserver$Stub;

    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->registerProcessObserver(Landroid/app/IProcessObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    goto :goto_0

    .line 197
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "RhythmicEyeCareManager"

    const-string v2, "registerForegroundAppObserver failed."

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 200
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method private registerReceiver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 137
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 138
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 139
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 140
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 141
    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mRhythmicEyeCareAlarmReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 142
    return-void
.end method

.method private scheduleRhythmicAlarm()V
    .locals 2

    .line 279
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "scheduleRhythmicAlarm: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmTimePoints:[I

    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RhythmicEyeCareManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmTimePoints:[I

    array-length v0, v0

    if-nez v0, :cond_0

    .line 281
    const-string v0, "no alarm exists"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    return-void

    .line 284
    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->getNextAlarmTimeIndex()I

    move-result v0

    iput v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmIndex:I

    .line 285
    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->setAlarm()V

    .line 286
    return-void
.end method

.method private setAlarm()V
    .locals 10

    .line 289
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmTimePoints:[I

    iget v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmIndex:I

    aget v0, v0, v1

    invoke-direct {p0, v0}, Lcom/android/server/display/RhythmicEyeCareManager;->getAlarmInMills(I)J

    move-result-wide v8

    .line 290
    .local v8, "startTime":J
    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmManager:Landroid/app/AlarmManager;

    const/4 v2, 0x0

    const-string v5, "rhythmic_eyecare_alarm"

    iget-object v6, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mOnAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    iget-object v7, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mHandler:Landroid/os/Handler;

    move-wide v3, v8

    invoke-virtual/range {v1 .. v7}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V

    .line 292
    return-void
.end method

.method private unregisterForegroundAppObserver()V
    .locals 3

    .line 204
    :try_start_0
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mTaskStackListener:Landroid/app/TaskStackListener;

    invoke-interface {v0, v1}, Landroid/app/IActivityTaskManager;->unregisterTaskStackListener(Landroid/app/ITaskStackListener;)V

    .line 205
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mProcessObserver:Landroid/app/IProcessObserver$Stub;

    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->unregisterProcessObserver(Landroid/app/IProcessObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    goto :goto_0

    .line 206
    :catch_0
    move-exception v0

    .line 207
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "RhythmicEyeCareManager"

    const-string/jumbo v2, "unregisterForegroundAppObserver failed."

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 209
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method private updateAppType(I)Z
    .locals 1
    .param p1, "appType"    # I

    .line 343
    iget v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppType:I

    if-eq v0, p1, :cond_0

    .line 344
    iput p1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppType:I

    .line 345
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppChanged:Z

    .line 346
    return v0

    .line 348
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private updateForegroundAppType()Z
    .locals 10

    .line 296
    const/4 v0, 0x0

    .line 297
    .local v0, "topActivity":Landroid/content/ComponentName;
    const/4 v1, 0x1

    .line 298
    .local v1, "topActivityFullScreenOrNotOccluded":Z
    const/4 v2, 0x0

    .line 300
    .local v2, "visibleTaskNum":I
    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    invoke-interface {v4}, Landroid/app/IActivityTaskManager;->getAllRootTaskInfos()Ljava/util/List;

    move-result-object v4

    .line 301
    .local v4, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    const/4 v7, 0x1

    if-eqz v6, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityTaskManager$RootTaskInfo;

    .line 302
    .local v6, "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    iget v8, v6, Landroid/app/ActivityTaskManager$RootTaskInfo;->displayId:I

    if-nez v8, :cond_0

    iget-boolean v8, v6, Landroid/app/ActivityTaskManager$RootTaskInfo;->visible:Z

    if-eqz v8, :cond_0

    iget-object v8, v6, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 303
    invoke-static {v8}, Ljava/util/Objects;->isNull(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 304
    goto :goto_0

    .line 306
    :cond_1
    invoke-virtual {v6}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I

    move-result v8

    .line 307
    .local v8, "windoingMode":I
    iget-boolean v9, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsDeskTopMode:Z

    if-nez v9, :cond_4

    .line 308
    if-nez v2, :cond_3

    .line 309
    iget-object v9, v6, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object v0, v9

    .line 310
    if-ne v8, v7, :cond_2

    goto :goto_1

    :cond_2
    move v7, v3

    :goto_1
    move v1, v7

    .line 312
    :cond_3
    iget-object v7, v6, Landroid/app/ActivityTaskManager$RootTaskInfo;->childTaskIds:[I

    array-length v7, v7

    add-int/2addr v2, v7

    goto :goto_3

    .line 317
    :cond_4
    if-ne v8, v7, :cond_5

    .line 318
    iget-object v5, v6, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object v0, v5

    .line 319
    goto :goto_4

    .line 321
    :cond_5
    if-eqz v1, :cond_7

    .line 323
    const/4 v9, 0x5

    if-ne v8, v9, :cond_6

    goto :goto_2

    :cond_6
    move v7, v3

    :goto_2
    move v1, v7

    .line 326
    .end local v6    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    .end local v8    # "windoingMode":I
    :cond_7
    :goto_3
    goto :goto_0

    .line 328
    :cond_8
    :goto_4
    if-gt v2, v7, :cond_b

    if-eqz v1, :cond_b

    .line 329
    invoke-static {v0}, Ljava/util/Objects;->isNull(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    iget-object v5, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v0, v5}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    goto :goto_6

    .line 332
    :cond_9
    iput-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mComponentName:Landroid/content/ComponentName;

    .line 333
    iget-object v5, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v5}, Lcom/android/server/policy/WindowManagerPolicy;->isKeyguardShowingAndNotOccluded()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 334
    move v5, v3

    goto :goto_5

    :cond_a
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/server/display/RhythmicEyeCareManager;->getAppTypeByPkgName(Ljava/lang/String;)I

    move-result v5

    .line 335
    .local v5, "appType":I
    :goto_5
    invoke-direct {p0, v5}, Lcom/android/server/display/RhythmicEyeCareManager;->updateAppType(I)Z

    move-result v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v3

    .line 330
    .end local v5    # "appType":I
    :cond_b
    :goto_6
    return v3

    .line 336
    .end local v0    # "topActivity":Landroid/content/ComponentName;
    .end local v1    # "topActivityFullScreenOrNotOccluded":Z
    .end local v2    # "visibleTaskNum":I
    .end local v4    # "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;"
    :catch_0
    move-exception v0

    .line 337
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "RhythmicEyeCareManager"

    const-string/jumbo v2, "updateForegroundAppType failed."

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 339
    .end local v0    # "e":Landroid/os/RemoteException;
    return v3
.end method

.method private updateState(Z)V
    .locals 3
    .param p1, "forceApply"    # Z

    .line 352
    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->updateForegroundAppType()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_2

    :cond_0
    iget v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mDisplayState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 354
    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->getMinuteOfDay()I

    move-result v0

    .line 355
    .local v0, "minute":I
    iget-boolean v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppChanged:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mTime:I

    if-eq v1, v0, :cond_2

    .line 356
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppChanged:Z

    .line 357
    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->getMinuteOfDay()I

    move-result v1

    iput v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mTime:I

    .line 358
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "applyChange, appType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", minute="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "RhythmicEyeCareManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mRhythmicEyeCareListener:Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener;

    iget v2, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppType:I

    invoke-interface {v1, v2, v0}, Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener;->onRhythmicEyeCareChange(II)V

    .line 364
    .end local v0    # "minute":I
    :cond_2
    return-void
.end method


# virtual methods
.method public notifyScreenStateChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .line 367
    iget v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mDisplayState:I

    if-eq v0, p1, :cond_0

    .line 368
    iput p1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mDisplayState:I

    .line 369
    iget-boolean v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/RhythmicEyeCareManager$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/display/RhythmicEyeCareManager$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/RhythmicEyeCareManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 373
    :cond_0
    return-void
.end method

.method public setModeEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 186
    if-eqz p1, :cond_0

    .line 187
    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->enableRhythmicEyeCareMode()V

    goto :goto_0

    .line 189
    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->disableRhythmicEyeCareMode()V

    .line 191
    :goto_0
    return-void
.end method

.method public setRhythmicEyeCareListener(Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener;

    .line 177
    iput-object p1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mRhythmicEyeCareListener:Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener;

    .line 178
    return-void
.end method

.method public updateDeskTopMode(Z)V
    .locals 0
    .param p1, "deskTopModeEnabled"    # Z

    .line 376
    iput-boolean p1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsDeskTopMode:Z

    .line 377
    return-void
.end method

.method public updateRhythmicAppCategoryList(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 181
    .local p1, "imageAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "readAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppCategoryMapper:Landroid/util/SparseArray;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 182
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppCategoryMapper:Landroid/util/SparseArray;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 183
    return-void
.end method
