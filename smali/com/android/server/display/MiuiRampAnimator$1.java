class com.android.server.display.MiuiRampAnimator$1 implements java.lang.Runnable {
	 /* .source "MiuiRampAnimator.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/MiuiRampAnimator; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.MiuiRampAnimator this$0; //synthetic
/* # direct methods */
 com.android.server.display.MiuiRampAnimator$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/MiuiRampAnimator; */
/* .line 131 */
/* .local p0, "this":Lcom/android/server/display/MiuiRampAnimator$1;, "Lcom/android/server/display/MiuiRampAnimator$1;" */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 8 */
/* .line 134 */
/* .local p0, "this":Lcom/android/server/display/MiuiRampAnimator$1;, "Lcom/android/server/display/MiuiRampAnimator$1;" */
v0 = this.this$0;
com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmChoreographer ( v0 );
(( android.view.Choreographer ) v0 ).getFrameTimeNanos ( ); // invoke-virtual {v0}, Landroid/view/Choreographer;->getFrameTimeNanos()J
/* move-result-wide v0 */
/* .line 135 */
/* .local v0, "frameTimeNanos":J */
v2 = this.this$0;
com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmLastFrameTimeNanos ( v2 );
/* move-result-wide v2 */
/* sub-long v2, v0, v2 */
/* long-to-float v2, v2 */
/* const v3, 0x3089705f # 1.0E-9f */
/* mul-float/2addr v2, v3 */
/* .line 137 */
/* .local v2, "timeDelta":F */
v3 = this.this$0;
com.android.server.display.MiuiRampAnimator .-$$Nest$fputmLastFrameTimeNanos ( v3,v0,v1 );
/* .line 143 */
v3 = android.animation.ValueAnimator .getDurationScale ( );
/* .line 144 */
/* .local v3, "scale":F */
int v4 = 0; // const/4 v4, 0x0
/* cmpl-float v4, v3, v4 */
/* if-nez v4, :cond_0 */
/* .line 146 */
v4 = this.this$0;
v5 = com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmTargetValue ( v4 );
/* int-to-float v5, v5 */
com.android.server.display.MiuiRampAnimator .-$$Nest$fputmAnimatedValue ( v4,v5 );
/* .line 148 */
} // :cond_0
v4 = this.this$0;
v4 = com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmRate ( v4 );
/* int-to-float v4, v4 */
/* mul-float/2addr v4, v2 */
/* div-float/2addr v4, v3 */
/* .line 149 */
/* .local v4, "amount":F */
v5 = this.this$0;
v5 = com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmTargetValue ( v5 );
v6 = this.this$0;
v6 = com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmCurrentValue ( v6 );
/* if-le v5, v6, :cond_1 */
/* .line 150 */
v5 = this.this$0;
v6 = com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmAnimatedValue ( v5 );
/* add-float/2addr v6, v4 */
v7 = this.this$0;
v7 = com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmTargetValue ( v7 );
/* int-to-float v7, v7 */
v6 = java.lang.Math .min ( v6,v7 );
com.android.server.display.MiuiRampAnimator .-$$Nest$fputmAnimatedValue ( v5,v6 );
/* .line 152 */
} // :cond_1
v5 = this.this$0;
v6 = com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmAnimatedValue ( v5 );
/* sub-float/2addr v6, v4 */
v7 = this.this$0;
v7 = com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmTargetValue ( v7 );
/* int-to-float v7, v7 */
v6 = java.lang.Math .max ( v6,v7 );
com.android.server.display.MiuiRampAnimator .-$$Nest$fputmAnimatedValue ( v5,v6 );
/* .line 155 */
} // .end local v4 # "amount":F
} // :goto_0
v4 = this.this$0;
v4 = com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmCurrentValue ( v4 );
/* .line 156 */
/* .local v4, "oldCurrentValue":I */
v5 = this.this$0;
v6 = com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmAnimatedValue ( v5 );
v6 = java.lang.Math .round ( v6 );
com.android.server.display.MiuiRampAnimator .-$$Nest$fputmCurrentValue ( v5,v6 );
/* .line 158 */
v5 = this.this$0;
v5 = com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmCurrentValue ( v5 );
/* if-eq v4, v5, :cond_2 */
/* .line 159 */
v5 = this.this$0;
com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmProperty ( v5 );
v6 = this.this$0;
com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmObject ( v6 );
v7 = this.this$0;
v7 = com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmCurrentValue ( v7 );
(( android.util.IntProperty ) v5 ).setValue ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Landroid/util/IntProperty;->setValue(Ljava/lang/Object;I)V
/* .line 162 */
} // :cond_2
v5 = this.this$0;
v5 = com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmTargetValue ( v5 );
v6 = this.this$0;
v6 = com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmCurrentValue ( v6 );
/* if-eq v5, v6, :cond_3 */
/* .line 163 */
v5 = this.this$0;
com.android.server.display.MiuiRampAnimator .-$$Nest$mpostAnimationCallback ( v5 );
/* .line 165 */
} // :cond_3
v5 = this.this$0;
int v6 = 0; // const/4 v6, 0x0
com.android.server.display.MiuiRampAnimator .-$$Nest$fputmAnimating ( v5,v6 );
/* .line 166 */
v5 = this.this$0;
com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmListener ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 167 */
v5 = this.this$0;
com.android.server.display.MiuiRampAnimator .-$$Nest$fgetmListener ( v5 );
/* .line 170 */
} // :cond_4
} // :goto_1
return;
} // .end method
