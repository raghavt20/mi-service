.class public Lcom/android/server/display/MiuiFoldPolicy;
.super Ljava/lang/Object;
.source "MiuiFoldPolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/MiuiFoldPolicy$MiuiFoldPolicyHandler;,
        Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;,
        Lcom/android/server/display/MiuiFoldPolicy$UserSwitchReceiver;
    }
.end annotation


# static fields
.field private static final CLOSE_LID_DISPLAY_SETTING:Ljava/lang/String; = "close_lid_display_setting"

.field private static final DEBUG:Ljava/lang/Boolean;

.field private static final FOLD_GESTURE_ANGLE_THRESHOLD:I = 0x52

.field private static final MIUI_OPTIMIZATION:Ljava/lang/String; = "miui_optimization"

.field private static final MSG_RELEASE_WINDOW_BY_SCREEN_OFF:I = 0x2

.field private static final MSG_SCREEN_TURNING_OFF:I = 0x6

.field private static final MSG_SCREEN_TURNING_ON:I = 0x5

.field private static final MSG_SHOW_OR_RELEASE_SWIPE_UP_WINDOW:I = 0x1

.field private static final MSG_UPDATE_DEVICE_STATE:I = 0x3

.field private static final MSG_USER_SWITCH:I = 0x4

.field private static final SETTING_EVENT_INVALID:I = -0x1

.field private static final SETTING_EVENT_KEEP_ON:I = 0x2

.field private static final SETTING_EVENT_SCREEN_OFF:I = 0x0

.field private static final SETTING_EVENT_SMART:I = 0x3

.field private static final SETTING_EVENT_SWIPE_UP:I = 0x1

.field public static final TAG:Ljava/lang/String; = "MiuiFoldPolicy"

.field public static final TYPE_HINGE_STATE:I = 0x1fa268f

.field private static final VIRTUAL_CAMERA_BOUNDARY:I = 0x64


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private final mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

.field private mCameraManager:Landroid/hardware/camera2/CameraManager;

.field private mContext:Landroid/content/Context;

.field private mFoldGestureAngleThreshold:I

.field private mHandler:Landroid/os/Handler;

.field private mIsCtsMode:Z

.field private mIsDeviceProvisioned:Z

.field private mMiHingeAngleSensor:Landroid/hardware/Sensor;

.field private mMiHingeAngleSensorEnabled:Z

.field private final mMiHingeAngleSensorListener:Landroid/hardware/SensorEventListener;

.field private mNeedOffDueToFoldGesture:Z

.field private mNeedReleaseByScreenTurningOn:Z

.field private mNeedReleaseSwipeUpWindow:Z

.field private mNeedShowSwipeUpWindow:Z

.field private final mOpeningCameraID:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPreState:I

.field private mScreenStateAfterFold:I

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSettingsObserver:Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;

.field private mState:I

.field private final mStrictFoldedDeviceStates:[I

.field private mSwipeUpWindow:Lcom/android/server/display/SwipeUpWindow;

.field private mTelecomManager:Landroid/telecom/TelecomManager;

.field private final mTentDeviceStates:[I

.field private mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;


# direct methods
.method static bridge synthetic -$$Nest$fgetmFoldGestureAngleThreshold(Lcom/android/server/display/MiuiFoldPolicy;)I
    .locals 0

    iget p0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mFoldGestureAngleThreshold:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/display/MiuiFoldPolicy;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmOpeningCameraID(Lcom/android/server/display/MiuiFoldPolicy;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mOpeningCameraID:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmNeedOffDueToFoldGesture(Lcom/android/server/display/MiuiFoldPolicy;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedOffDueToFoldGesture:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mreleaseSwipeUpWindow(Lcom/android/server/display/MiuiFoldPolicy;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/MiuiFoldPolicy;->releaseSwipeUpWindow(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshowOrReleaseSwipeUpWindow(Lcom/android/server/display/MiuiFoldPolicy;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->showOrReleaseSwipeUpWindow()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCtsMode(Lcom/android/server/display/MiuiFoldPolicy;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateCtsMode()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateDeviceProVisioned(Lcom/android/server/display/MiuiFoldPolicy;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateDeviceProVisioned()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateFoldGestureAngleThreshold(Lcom/android/server/display/MiuiFoldPolicy;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateFoldGestureAngleThreshold()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateScreenStateAfterFold(Lcom/android/server/display/MiuiFoldPolicy;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateScreenStateAfterFold()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateSettings(Lcom/android/server/display/MiuiFoldPolicy;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateSettings()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 42
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/android/server/display/MiuiFoldPolicy;->DEBUG:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mPreState:I

    .line 67
    iput v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mState:I

    .line 68
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mOpeningCameraID:Ljava/util/Set;

    .line 85
    const/16 v0, 0x52

    iput v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mFoldGestureAngleThreshold:I

    .line 338
    new-instance v0, Lcom/android/server/display/MiuiFoldPolicy$1;

    invoke-direct {v0, p0}, Lcom/android/server/display/MiuiFoldPolicy$1;-><init>(Lcom/android/server/display/MiuiFoldPolicy;)V

    iput-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

    .line 482
    new-instance v0, Lcom/android/server/display/MiuiFoldPolicy$2;

    invoke-direct {v0, p0}, Lcom/android/server/display/MiuiFoldPolicy$2;-><init>(Lcom/android/server/display/MiuiFoldPolicy;)V

    iput-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mMiHingeAngleSensorListener:Landroid/hardware/SensorEventListener;

    .line 93
    iput-object p1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    .line 94
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10700bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mStrictFoldedDeviceStates:[I

    .line 96
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11030056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mTentDeviceStates:[I

    .line 98
    return-void
.end method

.method private isCtsScene()Z
    .locals 3

    .line 447
    iget-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mIsCtsMode:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    :cond_0
    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    move v0, v1

    .line 449
    .local v0, "isCtsScene":Z
    if-eqz v0, :cond_2

    .line 450
    const-string v1, "MiuiFoldPolicy"

    const-string v2, "running cts, skip fold policy."

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    :cond_2
    return v0
.end method

.method private isFolded(I)Z
    .locals 1
    .param p1, "state"    # I

    .line 206
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mStrictFoldedDeviceStates:[I

    invoke-static {v0, p1}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v0

    return v0
.end method

.method private isFoldedOrTent(I)Z
    .locals 1
    .param p1, "state"    # I

    .line 214
    invoke-direct {p0, p1}, Lcom/android/server/display/MiuiFoldPolicy;->isFolded(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/server/display/MiuiFoldPolicy;->isTent(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isHoldScreenOn()Z
    .locals 4

    .line 313
    const-string v0, ""

    .line 314
    .local v0, "reason":Ljava/lang/String;
    const/4 v1, 0x0

    .line 317
    .local v1, "isHoldScreenOn":Z
    iget-object v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v2}, Landroid/telecom/TelecomManager;->isInCall()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 318
    const/4 v1, 0x1

    .line 319
    const-string v0, "in call"

    goto :goto_0

    .line 321
    :cond_0
    iget-object v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mOpeningCameraID:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 322
    const/4 v1, 0x1

    .line 323
    const-string v0, "camera using"

    goto :goto_0

    .line 325
    :cond_1
    iget-object v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->getMode()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mAudioManager:Landroid/media/AudioManager;

    .line 326
    invoke-virtual {v2}, Landroid/media/AudioManager;->getMode()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    .line 327
    :cond_2
    const/4 v1, 0x1

    .line 328
    const-string v0, "audio using"

    .line 331
    :cond_3
    :goto_0
    if-eqz v1, :cond_4

    .line 332
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hold screen on reason : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiFoldPolicy"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    :cond_4
    return v1
.end method

.method private isKeepScreenOnAfterFolded()Z
    .locals 2

    .line 187
    const/4 v0, 0x0

    .line 188
    .local v0, "isKeepScreenOn":Z
    iget v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I

    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 190
    :pswitch_0
    const/4 v0, 0x1

    .line 191
    goto :goto_2

    .line 196
    :pswitch_1
    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->isHoldScreenOn()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 197
    invoke-interface {v1}, Lcom/android/server/policy/WindowManagerPolicy;->isKeyguardShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 198
    goto :goto_2

    .line 193
    :pswitch_2
    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->isHoldScreenOn()Z

    move-result v0

    .line 194
    nop

    .line 202
    :goto_2
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private isTent(I)Z
    .locals 1
    .param p1, "state"    # I

    .line 210
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mTentDeviceStates:[I

    invoke-static {v0, p1}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v0

    return v0
.end method

.method private registerContentObserver()V
    .locals 5

    .line 396
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 397
    const-string v1, "close_lid_display_setting"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mSettingsObserver:Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;

    .line 396
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 400
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 401
    const-string v1, "fold_gesture_angle_threshold"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mSettingsObserver:Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;

    .line 400
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 404
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 405
    const-string v1, "miui_optimization"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mSettingsObserver:Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;

    .line 404
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 408
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 409
    const-string v1, "device_provisioned"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mSettingsObserver:Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;

    .line 408
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 412
    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateSettings()V

    .line 413
    return-void
.end method

.method private registerMiHingeAngleSensorListener(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .line 468
    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 469
    iget-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mMiHingeAngleSensorEnabled:Z

    if-nez v1, :cond_1

    .line 470
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mMiHingeAngleSensorEnabled:Z

    .line 471
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mMiHingeAngleSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v3, p0, Lcom/android/server/display/MiuiFoldPolicy;->mMiHingeAngleSensor:Landroid/hardware/Sensor;

    invoke-virtual {v1, v2, v3, v0}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0

    .line 475
    :cond_0
    iget-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mMiHingeAngleSensorEnabled:Z

    if-eqz v1, :cond_1

    .line 476
    iput-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mMiHingeAngleSensorEnabled:Z

    .line 477
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mMiHingeAngleSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 480
    :cond_1
    :goto_0
    return-void
.end method

.method private releaseSwipeUpWindow(Ljava/lang/String;)V
    .locals 1
    .param p1, "reason"    # Ljava/lang/String;

    .line 239
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mSwipeUpWindow:Lcom/android/server/display/SwipeUpWindow;

    invoke-virtual {v0, p1}, Lcom/android/server/display/SwipeUpWindow;->releaseSwipeWindow(Ljava/lang/String;)V

    .line 240
    return-void
.end method

.method private screenTurnOff()V
    .locals 5

    .line 307
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 308
    .local v0, "powerManager":Landroid/os/PowerManager;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/PowerManager;->goToSleep(JII)V

    .line 310
    return-void
.end method

.method private showOrReleaseSwipeUpWindow()V
    .locals 2

    .line 218
    sget-object v0, Lcom/android/server/display/MiuiFoldPolicy;->DEBUG:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "showOrReleaseSwipeUpWindow: fold?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mState:I

    invoke-direct {p0, v1}, Lcom/android/server/display/MiuiFoldPolicy;->isFoldedOrTent(I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mNeedShowSwipeUpWindow:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedShowSwipeUpWindow:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiFoldPolicy"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseByScreenTurningOn:Z

    .line 224
    iget-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseSwipeUpWindow:Z

    if-nez v1, :cond_3

    iget v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mState:I

    invoke-direct {p0, v1}, Lcom/android/server/display/MiuiFoldPolicy;->isFoldedOrTent(I)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 231
    :cond_1
    iget-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedShowSwipeUpWindow:Z

    if-eqz v1, :cond_2

    .line 232
    iput-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedShowSwipeUpWindow:Z

    .line 233
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mSwipeUpWindow:Lcom/android/server/display/SwipeUpWindow;

    invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow;->showSwipeUpWindow()V

    .line 235
    :cond_2
    return-void

    .line 225
    :cond_3
    :goto_0
    iput-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseSwipeUpWindow:Z

    .line 226
    const-string v0, "device state"

    invoke-direct {p0, v0}, Lcom/android/server/display/MiuiFoldPolicy;->releaseSwipeUpWindow(Ljava/lang/String;)V

    .line 227
    return-void
.end method

.method private updateCtsMode()V
    .locals 2

    .line 442
    nop

    .line 443
    const-string v0, "ro.miui.cts"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 442
    xor-int/lit8 v0, v0, 0x1

    const-string v1, "persist.sys.miui_optimization"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mIsCtsMode:Z

    .line 444
    return-void
.end method

.method private updateDeviceProVisioned()V
    .locals 3

    .line 456
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "device_provisioned"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iput-boolean v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mIsDeviceProvisioned:Z

    .line 458
    return-void
.end method

.method private updateFoldGestureAngleThreshold()V
    .locals 4

    .line 416
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/16 v1, 0x52

    const/4 v2, -0x2

    const-string v3, "fold_gesture_angle_threshold"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mFoldGestureAngleThreshold:I

    .line 419
    return-void
.end method

.method private updateScreenStateAfterFold()V
    .locals 5

    .line 422
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "close_lid_display_setting"

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I

    .line 425
    const/4 v4, 0x1

    if-ne v0, v2, :cond_1

    .line 427
    const-string v0, "cetus"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 428
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I

    goto :goto_0

    .line 430
    :cond_0
    iput v4, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I

    .line 433
    :goto_0
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 438
    :cond_1
    iget v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    invoke-direct {p0, v4}, Lcom/android/server/display/MiuiFoldPolicy;->registerMiHingeAngleSensorListener(Z)V

    .line 439
    return-void
.end method

.method private updateSettings()V
    .locals 0

    .line 389
    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateScreenStateAfterFold()V

    .line 390
    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateFoldGestureAngleThreshold()V

    .line 391
    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateCtsMode()V

    .line 392
    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateDeviceProVisioned()V

    .line 393
    return-void
.end method


# virtual methods
.method public dealDisplayTransition()V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 131
    return-void

    .line 134
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 135
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 136
    return-void
.end method

.method public handleDeviceStateChanged(I)V
    .locals 5
    .param p1, "state"    # I

    .line 154
    iget v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mState:I

    if-ne p1, v0, :cond_0

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "the new state is equal the old("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") skip"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiFoldPolicy"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    return-void

    .line 159
    :cond_0
    iput v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mPreState:I

    .line 160
    iput p1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mState:I

    .line 162
    invoke-direct {p0, p1}, Lcom/android/server/display/MiuiFoldPolicy;->isFoldedOrTent(I)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->isKeepScreenOnAfterFolded()Z

    move-result v0

    if-nez v0, :cond_5

    .line 163
    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->isCtsScene()Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mIsDeviceProvisioned:Z

    if-nez v0, :cond_1

    goto :goto_1

    .line 171
    :cond_1
    iget v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mState:I

    invoke-direct {p0, v0}, Lcom/android/server/display/MiuiFoldPolicy;->isFolded(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mPreState:I

    invoke-direct {p0, v0}, Lcom/android/server/display/MiuiFoldPolicy;->isTent(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 172
    iget v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I

    if-nez v0, :cond_2

    iget v3, p0, Lcom/android/server/display/MiuiFoldPolicy;->mPreState:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 174
    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->screenTurnOff()V

    goto :goto_0

    .line 175
    :cond_2
    const/4 v3, 0x3

    if-ne v0, v3, :cond_3

    .line 176
    iget-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedOffDueToFoldGesture:Z

    if-eqz v0, :cond_4

    .line 177
    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->screenTurnOff()V

    goto :goto_0

    .line 179
    :cond_3
    if-ne v0, v2, :cond_4

    .line 180
    iput-boolean v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedShowSwipeUpWindow:Z

    .line 181
    iput-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseSwipeUpWindow:Z

    .line 184
    :cond_4
    :goto_0
    return-void

    .line 164
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mSwipeUpWindow:Lcom/android/server/display/SwipeUpWindow;

    invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow;->cancelScreenOffDelay()V

    .line 165
    iput-boolean v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseSwipeUpWindow:Z

    .line 166
    iput-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedShowSwipeUpWindow:Z

    .line 167
    return-void
.end method

.method public handleScreenTurningOff()V
    .locals 1

    .line 303
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseByScreenTurningOn:Z

    .line 304
    return-void
.end method

.method public handleScreenTurningOn()V
    .locals 1

    .line 296
    iget-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseByScreenTurningOn:Z

    if-eqz v0, :cond_0

    .line 297
    const-string v0, "screen on"

    invoke-direct {p0, v0}, Lcom/android/server/display/MiuiFoldPolicy;->releaseSwipeUpWindow(Ljava/lang/String;)V

    .line 298
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseByScreenTurningOn:Z

    .line 300
    :cond_0
    return-void
.end method

.method public initMiuiFoldPolicy()V
    .locals 5

    .line 101
    new-instance v0, Lcom/android/server/ServiceThread;

    const/4 v1, -0x4

    const/4 v2, 0x0

    const-string v3, "MiuiFoldPolicy"

    invoke-direct {v0, v3, v1, v2}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V

    .line 102
    .local v0, "handlerThread":Lcom/android/server/ServiceThread;
    invoke-virtual {v0}, Lcom/android/server/ServiceThread;->start()V

    .line 103
    new-instance v1, Lcom/android/server/display/MiuiFoldPolicy$MiuiFoldPolicyHandler;

    invoke-virtual {v0}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/server/display/MiuiFoldPolicy$MiuiFoldPolicyHandler;-><init>(Lcom/android/server/display/MiuiFoldPolicy;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mHandler:Landroid/os/Handler;

    .line 104
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "telecom"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/TelecomManager;

    iput-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mTelecomManager:Landroid/telecom/TelecomManager;

    .line 105
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mAudioManager:Landroid/media/AudioManager;

    .line 106
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    const-string v2, "camera"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/camera2/CameraManager;

    iput-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    .line 107
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "sensor"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mSensorManager:Landroid/hardware/SensorManager;

    .line 108
    const-class v1, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 110
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    iget-object v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

    iget-object v3, p0, Lcom/android/server/display/MiuiFoldPolicy;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, v3}, Landroid/hardware/camera2/CameraManager;->registerAvailabilityCallback(Landroid/hardware/camera2/CameraManager$AvailabilityCallback;Landroid/os/Handler;)V

    .line 112
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mSensorManager:Landroid/hardware/SensorManager;

    const v2, 0x1fa268f

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mMiHingeAngleSensor:Landroid/hardware/Sensor;

    .line 114
    new-instance v1, Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;

    iget-object v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;-><init>(Lcom/android/server/display/MiuiFoldPolicy;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mSettingsObserver:Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;

    .line 115
    new-instance v1, Lcom/android/server/display/SwipeUpWindow;

    iget-object v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/android/server/display/SwipeUpWindow;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mSwipeUpWindow:Lcom/android/server/display/SwipeUpWindow;

    .line 117
    invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->registerContentObserver()V

    .line 119
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/server/display/MiuiFoldPolicy$UserSwitchReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/server/display/MiuiFoldPolicy$UserSwitchReceiver;-><init>(Lcom/android/server/display/MiuiFoldPolicy;Lcom/android/server/display/MiuiFoldPolicy$UserSwitchReceiver-IA;)V

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.USER_SWITCHED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 121
    return-void
.end method

.method public notifyFinishedGoingToSleep()V
    .locals 2

    .line 289
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 290
    return-void

    .line 292
    :cond_0
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 293
    return-void
.end method

.method public screenTurningOff()V
    .locals 2

    .line 282
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 283
    return-void

    .line 285
    :cond_0
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 286
    return-void
.end method

.method public screenTurningOn()V
    .locals 2

    .line 275
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 276
    return-void

    .line 278
    :cond_0
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 279
    return-void
.end method

.method public setDeviceStateLocked(I)V
    .locals 3
    .param p1, "state"    # I

    .line 139
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 140
    return-void

    .line 143
    :cond_0
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 144
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 145
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 147
    sget-object v1, Lcom/android/server/display/MiuiFoldPolicy;->DEBUG:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 148
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setDeviceStateLocked: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiFoldPolicy"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    :cond_1
    return-void
.end method
