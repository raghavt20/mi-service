class com.android.server.display.DisplayFeatureManagerService$LocalService extends com.android.server.display.DisplayFeatureManagerInternal {
	 /* .source "DisplayFeatureManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/DisplayFeatureManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "LocalService" */
} // .end annotation
/* # instance fields */
final com.android.server.display.DisplayFeatureManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.display.DisplayFeatureManagerService$LocalService ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/DisplayFeatureManagerService; */
/* .line 1490 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerInternal;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void setVideoInformation ( Integer p0, Boolean p1, Float p2, Integer p3, Integer p4, Float p5, android.os.IBinder p6 ) {
/* .locals 8 */
/* .param p1, "pid" # I */
/* .param p2, "bulletChatStatus" # Z */
/* .param p3, "frameRate" # F */
/* .param p4, "width" # I */
/* .param p5, "height" # I */
/* .param p6, "compressionRatio" # F */
/* .param p7, "token" # Landroid/os/IBinder; */
/* .line 1517 */
v0 = this.this$0;
/* move v1, p1 */
/* move v2, p2 */
/* move v3, p3 */
/* move v4, p4 */
/* move v5, p5 */
/* move v6, p6 */
/* move-object v7, p7 */
/* invoke-static/range {v0 ..v7}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mupdateVideoInformationIfNeeded(Lcom/android/server/display/DisplayFeatureManagerService;IZFIIFLandroid/os/IBinder;)V */
/* .line 1519 */
return;
} // .end method
public void updateBCBCState ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "state" # I */
/* .line 1511 */
v0 = this.this$0;
/* const/16 v1, 0x12 */
com.android.server.display.DisplayFeatureManagerService .-$$Nest$msetScreenEffect ( v0,v1,p1 );
/* .line 1512 */
return;
} // .end method
public void updateDozeBrightness ( Long p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "physicalDisplayId" # J */
/* .param p3, "brightness" # I */
/* .line 1506 */
v0 = this.this$0;
(( com.android.server.display.DisplayFeatureManagerService ) v0 ).setDozeBrightness ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/DisplayFeatureManagerService;->setDozeBrightness(JI)V
/* .line 1507 */
return;
} // .end method
public void updateRhythmicAppCategoryList ( java.util.List p0, java.util.List p1 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1523 */
/* .local p1, "imageAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p2, "readAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmRhythmicEyeCareManager ( v0 );
(( com.android.server.display.RhythmicEyeCareManager ) v0 ).updateRhythmicAppCategoryList ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/RhythmicEyeCareManager;->updateRhythmicAppCategoryList(Ljava/util/List;Ljava/util/List;)V
/* .line 1524 */
return;
} // .end method
public void updateScreenEffect ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "state" # I */
/* .line 1493 */
v0 = this.this$0;
v0 = com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmDisplayState ( v0 );
/* .line 1494 */
/* .local v0, "oldState":I */
v1 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fputmDisplayState ( v1,p1 );
/* .line 1495 */
/* sget-boolean v1, Lmiui/os/DeviceFeature;->PERSIST_SCREEN_EFFECT:Z */
/* if-nez v1, :cond_1 */
/* .line 1496 */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* if-eq p1, v0, :cond_0 */
/* .line 1498 */
v1 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
com.android.server.display.DisplayFeatureManagerService .-$$Nest$msetScreenEffectColor ( v1,v2 );
/* .line 1500 */
} // :cond_0
v1 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmRhythmicEyeCareManager ( v1 );
(( com.android.server.display.RhythmicEyeCareManager ) v1 ).notifyScreenStateChanged ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/display/RhythmicEyeCareManager;->notifyScreenStateChanged(I)V
/* .line 1502 */
} // :cond_1
return;
} // .end method
public void updateScreenGrayscaleState ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "state" # I */
/* .line 1528 */
v0 = this.this$0;
/* const/16 v1, 0x38 */
com.android.server.display.DisplayFeatureManagerService .-$$Nest$msetScreenEffect ( v0,v1,p1 );
/* .line 1529 */
return;
} // .end method
