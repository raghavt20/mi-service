.class Lcom/android/server/display/MiuiFoldPolicy$1;
.super Landroid/hardware/camera2/CameraManager$AvailabilityCallback;
.source "MiuiFoldPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/MiuiFoldPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/MiuiFoldPolicy;


# direct methods
.method constructor <init>(Lcom/android/server/display/MiuiFoldPolicy;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/MiuiFoldPolicy;

    .line 339
    iput-object p1, p0, Lcom/android/server/display/MiuiFoldPolicy$1;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onCameraAvailable(Ljava/lang/String;)V
    .locals 3
    .param p1, "cameraId"    # Ljava/lang/String;

    .line 342
    invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraAvailable(Ljava/lang/String;)V

    .line 343
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 344
    .local v0, "id":I
    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 345
    return-void

    .line 347
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy$1;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    invoke-static {v1}, Lcom/android/server/display/MiuiFoldPolicy;->-$$Nest$fgetmOpeningCameraID(Lcom/android/server/display/MiuiFoldPolicy;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 348
    return-void
.end method

.method public onCameraUnavailable(Ljava/lang/String;)V
    .locals 3
    .param p1, "cameraId"    # Ljava/lang/String;

    .line 352
    invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraUnavailable(Ljava/lang/String;)V

    .line 353
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 354
    .local v0, "id":I
    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 355
    return-void

    .line 357
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy$1;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    invoke-static {v1}, Lcom/android/server/display/MiuiFoldPolicy;->-$$Nest$fgetmOpeningCameraID(Lcom/android/server/display/MiuiFoldPolicy;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 358
    return-void
.end method
