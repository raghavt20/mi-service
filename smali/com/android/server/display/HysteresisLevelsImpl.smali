.class public Lcom/android/server/display/HysteresisLevelsImpl;
.super Lcom/android/server/display/HysteresisLevelsStub;
.source "HysteresisLevelsImpl.java"


# static fields
.field private static final HBM_MINIMUM_LUX:F = 6000.0f

.field private static final TAG:Ljava/lang/String; = "HysteresisLevelsImpl"


# instance fields
.field private mAmbientBrightnessThresholds:Lcom/android/server/display/HysteresisLevels;

.field private mHbmController:Lcom/android/server/display/HighBrightnessModeController;

.field private mHysteresisBrightSpline:Landroid/util/Spline;

.field private mHysteresisDarkSpline:Landroid/util/Spline;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/android/server/display/HysteresisLevelsStub;-><init>()V

    return-void
.end method


# virtual methods
.method public createHysteresisThresholdSpline([F[F[F[F)V
    .locals 2
    .param p1, "ambientBrighteningLux"    # [F
    .param p2, "ambientBrighteningThresholds"    # [F
    .param p3, "ambientDarkeningLux"    # [F
    .param p4, "ambientDarkeningThresholds"    # [F

    .line 43
    array-length v0, p1

    array-length v1, p2

    if-ne v0, v1, :cond_2

    array-length v0, p3

    array-length v1, p4

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 48
    :cond_0
    array-length v0, p1

    if-lez v0, :cond_1

    array-length v0, p2

    if-lez v0, :cond_1

    array-length v0, p3

    if-lez v0, :cond_1

    array-length v0, p4

    if-lez v0, :cond_1

    .line 50
    invoke-static {p1, p2}, Landroid/util/Spline;->createLinearSpline([F[F)Landroid/util/Spline;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/HysteresisLevelsImpl;->mHysteresisBrightSpline:Landroid/util/Spline;

    .line 51
    invoke-static {p3, p4}, Landroid/util/Spline;->createLinearSpline([F[F)Landroid/util/Spline;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/HysteresisLevelsImpl;->mHysteresisDarkSpline:Landroid/util/Spline;

    .line 53
    :cond_1
    return-void

    .line 45
    :cond_2
    :goto_0
    const-string v0, "HysteresisLevelsImpl"

    const-string v1, "Mismatch between hysteresis array lengths."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    return-void
.end method

.method dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 78
    const-string v0, "MiuiHysteresisLevels:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mHysteresisBrightSpline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/HysteresisLevelsImpl;->mHysteresisBrightSpline:Landroid/util/Spline;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mHysteresisDarkSpline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/HysteresisLevelsImpl;->mHysteresisDarkSpline:Landroid/util/Spline;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method public getBrighteningThreshold(F)F
    .locals 3
    .param p1, "value"    # F

    .line 57
    iget-object v0, p0, Lcom/android/server/display/HysteresisLevelsImpl;->mHysteresisBrightSpline:Landroid/util/Spline;

    if-eqz v0, :cond_2

    .line 58
    invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F

    move-result v0

    .line 59
    .local v0, "brighteningThreshold":F
    const v1, 0x45bb8000    # 6000.0f

    .line 60
    .local v1, "hbmMinValue":F
    iget-object v2, p0, Lcom/android/server/display/HysteresisLevelsImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/android/server/display/HighBrightnessModeController;->deviceSupportsHbm()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/android/server/display/HysteresisLevelsImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    invoke-virtual {v2}, Lcom/android/server/display/HighBrightnessModeController;->getHbmData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;

    move-result-object v2

    iget v1, v2, Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;->minimumLux:F

    .line 64
    :cond_0
    cmpl-float v2, p1, v1

    if-lez v2, :cond_1

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v2, p1

    goto :goto_0

    :cond_1
    move v2, v0

    :goto_0
    return v2

    .line 66
    .end local v0    # "brighteningThreshold":F
    .end local v1    # "hbmMinValue":F
    :cond_2
    iget-object v0, p0, Lcom/android/server/display/HysteresisLevelsImpl;->mAmbientBrightnessThresholds:Lcom/android/server/display/HysteresisLevels;

    invoke-virtual {v0, p1}, Lcom/android/server/display/HysteresisLevels;->getBrighteningThreshold(F)F

    move-result v0

    return v0
.end method

.method public getDarkeningThreshold(F)F
    .locals 1
    .param p1, "value"    # F

    .line 71
    iget-object v0, p0, Lcom/android/server/display/HysteresisLevelsImpl;->mHysteresisDarkSpline:Landroid/util/Spline;

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F

    move-result v0

    return v0

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/HysteresisLevelsImpl;->mAmbientBrightnessThresholds:Lcom/android/server/display/HysteresisLevels;

    invoke-virtual {v0, p1}, Lcom/android/server/display/HysteresisLevels;->getDarkeningThreshold(F)F

    move-result v0

    return v0
.end method

.method public initialize(Lcom/android/server/display/HighBrightnessModeController;Lcom/android/server/display/HysteresisLevels;)V
    .locals 5
    .param p1, "hbmController"    # Lcom/android/server/display/HighBrightnessModeController;
    .param p2, "ambientBrightnessThresholds"    # Lcom/android/server/display/HysteresisLevels;

    .line 22
    iput-object p1, p0, Lcom/android/server/display/HysteresisLevelsImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    .line 23
    iput-object p2, p0, Lcom/android/server/display/HysteresisLevelsImpl;->mAmbientBrightnessThresholds:Lcom/android/server/display/HysteresisLevels;

    .line 25
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1103001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 24
    invoke-static {v0}, Lcom/android/server/display/BrightnessMappingStrategy;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v0

    .line 28
    .local v0, "ambientBrighteningLux":[F
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x11030023

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 27
    invoke-static {v1}, Lcom/android/server/display/BrightnessMappingStrategy;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v1

    .line 31
    .local v1, "ambientBrighteningThresholds":[F
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x11030028

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 30
    invoke-static {v2}, Lcom/android/server/display/BrightnessMappingStrategy;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v2

    .line 34
    .local v2, "ambientDarkeningLux":[F
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x1103002d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 33
    invoke-static {v3}, Lcom/android/server/display/BrightnessMappingStrategy;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v3

    .line 36
    .local v3, "ambientDarkeningThresholds":[F
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/server/display/HysteresisLevelsImpl;->createHysteresisThresholdSpline([F[F[F[F)V

    .line 38
    return-void
.end method
