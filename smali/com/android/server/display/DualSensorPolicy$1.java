class com.android.server.display.DualSensorPolicy$1 implements android.hardware.SensorEventListener {
	 /* .source "DualSensorPolicy.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/DualSensorPolicy; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.DualSensorPolicy this$0; //synthetic
/* # direct methods */
 com.android.server.display.DualSensorPolicy$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/DualSensorPolicy; */
/* .line 91 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAccuracyChanged ( android.hardware.Sensor p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .param p2, "accuracy" # I */
/* .line 102 */
return;
} // .end method
public void onSensorChanged ( android.hardware.SensorEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 94 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 95 */
/* .local v0, "time":J */
v2 = this.values;
int v3 = 0; // const/4 v3, 0x0
/* aget v2, v2, v3 */
/* .line 96 */
/* .local v2, "lux":F */
v3 = this.this$0;
com.android.server.display.DualSensorPolicy .-$$Nest$mhandleAssistLightSensorEvent ( v3,v0,v1,v2 );
/* .line 97 */
return;
} // .end method
