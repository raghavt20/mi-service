public abstract class com.android.server.display.DisplayFeatureManagerInternal {
	 /* .source "DisplayFeatureManagerInternal.java" */
	 /* # direct methods */
	 public com.android.server.display.DisplayFeatureManagerInternal ( ) {
		 /* .locals 0 */
		 /* .line 5 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public abstract void setVideoInformation ( Integer p0, Boolean p1, Float p2, Integer p3, Integer p4, Float p5, android.os.IBinder p6 ) {
	 } // .end method
	 public abstract void updateBCBCState ( Integer p0 ) {
	 } // .end method
	 public abstract void updateDozeBrightness ( Long p0, Integer p1 ) {
	 } // .end method
	 public abstract void updateRhythmicAppCategoryList ( java.util.List p0, java.util.List p1 ) {
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/List<", */
		 /* "Ljava/lang/String;", */
		 /* ">;", */
		 /* "Ljava/util/List<", */
		 /* "Ljava/lang/String;", */
		 /* ">;)V" */
		 /* } */
	 } // .end annotation
} // .end method
public abstract void updateScreenEffect ( Integer p0 ) {
} // .end method
public abstract void updateScreenGrayscaleState ( Integer p0 ) {
} // .end method
