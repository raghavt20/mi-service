public class com.android.server.display.RhythmicEyeCareManager {
	 /* .source "RhythmicEyeCareManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ALARM_TAG;
private static final Integer RHYTHMIC_APP_CATEGORY_TYPE_DEFAULT;
private static final Integer RHYTHMIC_APP_CATEGORY_TYPE_IMAGE;
private static final Integer RHYTHMIC_APP_CATEGORY_TYPE_READ;
private static final java.lang.String TAG;
/* # instance fields */
private android.app.IActivityTaskManager mActivityTaskManager;
private Integer mAlarmIndex;
private android.app.AlarmManager mAlarmManager;
private mAlarmTimePoints;
private android.util.SparseArray mAppCategoryMapper;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private Boolean mAppChanged;
private Integer mAppType;
private android.content.ComponentName mComponentName;
private Integer mDisplayState;
private android.os.Handler mHandler;
private Boolean mIsDeskTopMode;
private Boolean mIsEnabled;
private final com.android.internal.policy.IKeyguardLockedStateListener mKeyguardLockedStateListener;
private final android.app.AlarmManager$OnAlarmListener mOnAlarmListener;
private com.android.server.policy.WindowManagerPolicy mPolicy;
private final android.app.IProcessObserver$Stub mProcessObserver;
private final android.content.BroadcastReceiver mRhythmicEyeCareAlarmReceiver;
private com.android.server.display.RhythmicEyeCareManager$RhythmicEyeCareListener mRhythmicEyeCareListener;
private final android.app.TaskStackListener mTaskStackListener;
private Integer mTime;
private com.android.server.wm.WindowManagerService mWindowManagerService;
/* # direct methods */
public static void $r8$lambda$UB0zXyitIKMRgNy5P9lMtDeZ-eQ ( com.android.server.display.RhythmicEyeCareManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->lambda$enableRhythmicEyeCareMode$0()V */
return;
} // .end method
public static void $r8$lambda$n6HRyEgeSe6p429zaw5YoPwjbFY ( com.android.server.display.RhythmicEyeCareManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->lambda$notifyScreenStateChanged$1()V */
return;
} // .end method
static Integer -$$Nest$fgetmAlarmIndex ( com.android.server.display.RhythmicEyeCareManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmIndex:I */
} // .end method
static android.app.AlarmManager -$$Nest$fgetmAlarmManager ( com.android.server.display.RhythmicEyeCareManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAlarmManager;
} // .end method
static -$$Nest$fgetmAlarmTimePoints ( com.android.server.display.RhythmicEyeCareManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAlarmTimePoints;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.display.RhythmicEyeCareManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmIsEnabled ( com.android.server.display.RhythmicEyeCareManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsEnabled:Z */
} // .end method
static android.app.AlarmManager$OnAlarmListener -$$Nest$fgetmOnAlarmListener ( com.android.server.display.RhythmicEyeCareManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mOnAlarmListener;
} // .end method
static void -$$Nest$fputmAlarmIndex ( com.android.server.display.RhythmicEyeCareManager p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmIndex:I */
return;
} // .end method
static void -$$Nest$mscheduleRhythmicAlarm ( com.android.server.display.RhythmicEyeCareManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->scheduleRhythmicAlarm()V */
return;
} // .end method
static void -$$Nest$msetAlarm ( com.android.server.display.RhythmicEyeCareManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->setAlarm()V */
return;
} // .end method
static void -$$Nest$mupdateState ( com.android.server.display.RhythmicEyeCareManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/RhythmicEyeCareManager;->updateState(Z)V */
return;
} // .end method
public com.android.server.display.RhythmicEyeCareManager ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 125 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 47 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mAppCategoryMapper = v0;
/* .line 53 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppType:I */
/* .line 54 */
/* iput v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mDisplayState:I */
/* .line 57 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppChanged:Z */
/* .line 61 */
/* new-instance v0, Lcom/android/server/display/RhythmicEyeCareManager$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/RhythmicEyeCareManager$1;-><init>(Lcom/android/server/display/RhythmicEyeCareManager;)V */
this.mOnAlarmListener = v0;
/* .line 70 */
/* new-instance v0, Lcom/android/server/display/RhythmicEyeCareManager$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/RhythmicEyeCareManager$2;-><init>(Lcom/android/server/display/RhythmicEyeCareManager;)V */
this.mRhythmicEyeCareAlarmReceiver = v0;
/* .line 88 */
/* new-instance v0, Lcom/android/server/display/RhythmicEyeCareManager$3; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/RhythmicEyeCareManager$3;-><init>(Lcom/android/server/display/RhythmicEyeCareManager;)V */
this.mTaskStackListener = v0;
/* .line 95 */
/* new-instance v0, Lcom/android/server/display/RhythmicEyeCareManager$4; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/RhythmicEyeCareManager$4;-><init>(Lcom/android/server/display/RhythmicEyeCareManager;)V */
this.mKeyguardLockedStateListener = v0;
/* .line 105 */
/* new-instance v0, Lcom/android/server/display/RhythmicEyeCareManager$5; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/RhythmicEyeCareManager$5;-><init>(Lcom/android/server/display/RhythmicEyeCareManager;)V */
this.mProcessObserver = v0;
/* .line 126 */
android.content.res.Resources .getSystem ( );
/* .line 127 */
/* const v1, 0x1103004d */
(( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
this.mAlarmTimePoints = v0;
/* .line 128 */
/* new-instance v0, Landroid/os/Handler; */
/* invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 129 */
/* const-class v0, Lcom/android/server/policy/WindowManagerPolicy; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/policy/WindowManagerPolicy; */
this.mPolicy = v0;
/* .line 130 */
/* const-string/jumbo v0, "window" */
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
this.mWindowManagerService = v0;
/* .line 131 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/RhythmicEyeCareManager;->registerReceiver(Landroid/content/Context;)V */
/* .line 132 */
final String v0 = "alarm"; // const-string v0, "alarm"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AlarmManager; */
this.mAlarmManager = v0;
/* .line 133 */
android.app.ActivityTaskManager .getService ( );
this.mActivityTaskManager = v0;
/* .line 134 */
return;
} // .end method
private void disableRhythmicEyeCareMode ( ) {
/* .locals 3 */
/* .line 158 */
/* iget-boolean v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsEnabled:Z */
/* if-nez v0, :cond_0 */
/* .line 159 */
return;
/* .line 162 */
} // :cond_0
final String v0 = "RhythmicEyeCareManager"; // const-string v0, "RhythmicEyeCareManager"
final String v1 = "disableRhythmicEyeCareMode"; // const-string v1, "disableRhythmicEyeCareMode"
android.util.Slog .i ( v0,v1 );
/* .line 163 */
v0 = this.mRhythmicEyeCareListener;
/* iget v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppType:I */
v2 = /* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->getMinuteOfDay()I */
/* .line 164 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppType:I */
/* .line 165 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppChanged:Z */
/* .line 166 */
/* iput-boolean v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsEnabled:Z */
/* .line 167 */
int v1 = 0; // const/4 v1, 0x0
this.mComponentName = v1;
/* .line 168 */
/* iput v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmIndex:I */
/* .line 169 */
v0 = this.mOnAlarmListener;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 170 */
v1 = this.mAlarmManager;
(( android.app.AlarmManager ) v1 ).cancel ( v0 ); // invoke-virtual {v1, v0}, Landroid/app/AlarmManager;->cancel(Landroid/app/AlarmManager$OnAlarmListener;)V
/* .line 172 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->unregisterForegroundAppObserver()V */
/* .line 173 */
v0 = this.mWindowManagerService;
v1 = this.mKeyguardLockedStateListener;
(( com.android.server.wm.WindowManagerService ) v0 ).removeKeyguardLockedStateListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService;->removeKeyguardLockedStateListener(Lcom/android/internal/policy/IKeyguardLockedStateListener;)V
/* .line 174 */
return;
} // .end method
private void enableRhythmicEyeCareMode ( ) {
/* .locals 2 */
/* .line 145 */
/* iget-boolean v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 146 */
return;
/* .line 149 */
} // :cond_0
final String v0 = "RhythmicEyeCareManager"; // const-string v0, "RhythmicEyeCareManager"
final String v1 = "enableRhythmicEyeCareMode"; // const-string v1, "enableRhythmicEyeCareMode"
android.util.Slog .i ( v0,v1 );
/* .line 150 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsEnabled:Z */
/* .line 151 */
/* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->registerForegroundAppObserver()V */
/* .line 152 */
v0 = this.mWindowManagerService;
v1 = this.mKeyguardLockedStateListener;
(( com.android.server.wm.WindowManagerService ) v0 ).addKeyguardLockedStateListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService;->addKeyguardLockedStateListener(Lcom/android/internal/policy/IKeyguardLockedStateListener;)V
/* .line 153 */
/* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->scheduleRhythmicAlarm()V */
/* .line 154 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/display/RhythmicEyeCareManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/RhythmicEyeCareManager$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/RhythmicEyeCareManager;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 155 */
return;
} // .end method
private java.lang.String getAlarmDateString ( Long p0 ) {
/* .locals 8 */
/* .param p1, "mills" # J */
/* .line 261 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 262 */
/* .local v0, "builder":Ljava/lang/StringBuilder; */
final String v1 = "next alarm:"; // const-string v1, "next alarm:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 263 */
java.util.Calendar .getInstance ( );
/* .line 264 */
/* .local v1, "calendar":Ljava/util/Calendar; */
(( java.util.Calendar ) v1 ).setTimeInMillis ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V
/* .line 265 */
int v2 = 2; // const/4 v2, 0x2
v2 = (( java.util.Calendar ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I
/* add-int/lit8 v2, v2, 0x1 */
/* .line 266 */
/* .local v2, "month":I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "-"; // const-string v4, "-"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 267 */
int v3 = 5; // const/4 v3, 0x5
v3 = (( java.util.Calendar ) v1 ).get ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I
/* .line 268 */
/* .local v3, "day":I */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 269 */
/* const/16 v5, 0xb */
v5 = (( java.util.Calendar ) v1 ).get ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I
/* .line 270 */
/* .local v5, "hour":I */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 271 */
/* const/16 v6, 0xc */
v6 = (( java.util.Calendar ) v1 ).get ( v6 ); // invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I
/* .line 272 */
/* .local v6, "minute":I */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 273 */
/* const/16 v4, 0xd */
v4 = (( java.util.Calendar ) v1 ).get ( v4 ); // invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I
/* .line 274 */
/* .local v4, "second":I */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 275 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private Long getAlarmInMills ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "time" # I */
/* .line 246 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->getMinuteOfDay()I */
/* .line 247 */
/* .local v0, "currentTime":I */
java.util.Calendar .getInstance ( );
/* .line 248 */
/* .local v1, "calendar":Ljava/util/Calendar; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
(( java.util.Calendar ) v1 ).setTimeInMillis ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V
/* .line 249 */
/* if-lt v0, p1, :cond_0 */
/* .line 250 */
int v2 = 6; // const/4 v2, 0x6
int v3 = 1; // const/4 v3, 0x1
(( java.util.Calendar ) v1 ).add ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->add(II)V
/* .line 252 */
} // :cond_0
/* div-int/lit8 v2, p1, 0x3c */
/* const/16 v3, 0xb */
(( java.util.Calendar ) v1 ).set ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Ljava/util/Calendar;->set(II)V
/* .line 253 */
/* const/16 v2, 0xc */
/* rem-int/lit8 v3, p1, 0x3c */
(( java.util.Calendar ) v1 ).set ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V
/* .line 254 */
/* const/16 v2, 0xd */
int v3 = 0; // const/4 v3, 0x0
(( java.util.Calendar ) v1 ).set ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V
/* .line 255 */
/* const/16 v2, 0xe */
(( java.util.Calendar ) v1 ).set ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V
/* .line 256 */
(( java.util.Calendar ) v1 ).getTimeInMillis ( ); // invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v2 */
/* invoke-direct {p0, v2, v3}, Lcom/android/server/display/RhythmicEyeCareManager;->getAlarmDateString(J)Ljava/lang/String; */
final String v3 = "RhythmicEyeCareManager"; // const-string v3, "RhythmicEyeCareManager"
android.util.Slog .d ( v3,v2 );
/* .line 257 */
(( java.util.Calendar ) v1 ).getTimeInMillis ( ); // invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v2 */
/* return-wide v2 */
} // .end method
private Integer getAppTypeByPkgName ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 232 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mAppCategoryMapper;
v1 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* if-ge v0, v1, :cond_2 */
/* .line 233 */
v1 = this.mAppCategoryMapper;
v1 = (( android.util.SparseArray ) v1 ).keyAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 234 */
/* .local v1, "appType":I */
v2 = this.mAppCategoryMapper;
(( android.util.SparseArray ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v2, Ljava/util/List; */
/* .line 235 */
/* .local v2, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez v2, :cond_0 */
/* .line 236 */
/* .line 238 */
v3 = } // :cond_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 239 */
/* .line 232 */
} // .end local v1 # "appType":I
} // .end local v2 # "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_1
} // :goto_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 242 */
} // .end local v0 # "i":I
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Integer getMinuteOfDay ( ) {
/* .locals 4 */
/* .line 212 */
java.util.Calendar .getInstance ( );
/* .line 213 */
/* .local v0, "calendar":Ljava/util/Calendar; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
(( java.util.Calendar ) v0 ).setTimeInMillis ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V
/* .line 214 */
/* const/16 v1, 0xb */
v1 = (( java.util.Calendar ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I
/* .line 215 */
/* .local v1, "hour":I */
/* const/16 v2, 0xc */
v2 = (( java.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I
/* .line 216 */
/* .local v2, "minute":I */
/* mul-int/lit8 v3, v1, 0x3c */
/* add-int/2addr v3, v2 */
} // .end method
private Integer getNextAlarmTimeIndex ( ) {
/* .locals 4 */
/* .line 220 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->getMinuteOfDay()I */
/* .line 221 */
/* .local v0, "currentTime":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 222 */
/* .local v1, "index":I */
} // :goto_0
v2 = this.mAlarmTimePoints;
/* array-length v3, v2 */
/* if-ge v1, v3, :cond_0 */
/* aget v3, v2, v1 */
/* if-gt v3, v0, :cond_0 */
/* .line 223 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 225 */
} // :cond_0
/* array-length v2, v2 */
/* if-ne v1, v2, :cond_1 */
/* .line 226 */
int v1 = 0; // const/4 v1, 0x0
/* .line 228 */
} // :cond_1
} // .end method
private void lambda$enableRhythmicEyeCareMode$0 ( ) { //synthethic
/* .locals 1 */
/* .line 154 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/android/server/display/RhythmicEyeCareManager;->updateState(Z)V */
return;
} // .end method
private void lambda$notifyScreenStateChanged$1 ( ) { //synthethic
/* .locals 1 */
/* .line 370 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/android/server/display/RhythmicEyeCareManager;->updateState(Z)V */
return;
} // .end method
private void registerForegroundAppObserver ( ) {
/* .locals 3 */
/* .line 195 */
try { // :try_start_0
v0 = this.mActivityTaskManager;
v1 = this.mTaskStackListener;
/* .line 196 */
android.app.ActivityManager .getService ( );
v1 = this.mProcessObserver;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 199 */
/* .line 197 */
/* :catch_0 */
/* move-exception v0 */
/* .line 198 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "RhythmicEyeCareManager"; // const-string v1, "RhythmicEyeCareManager"
final String v2 = "registerForegroundAppObserver failed."; // const-string v2, "registerForegroundAppObserver failed."
android.util.Slog .e ( v1,v2,v0 );
/* .line 200 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
private void registerReceiver ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 137 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 138 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.DATE_CHANGED"; // const-string v1, "android.intent.action.DATE_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 139 */
final String v1 = "android.intent.action.TIME_SET"; // const-string v1, "android.intent.action.TIME_SET"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 140 */
final String v1 = "android.intent.action.TIMEZONE_CHANGED"; // const-string v1, "android.intent.action.TIMEZONE_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 141 */
v1 = this.mRhythmicEyeCareAlarmReceiver;
(( android.content.Context ) p1 ).registerReceiver ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 142 */
return;
} // .end method
private void scheduleRhythmicAlarm ( ) {
/* .locals 2 */
/* .line 279 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "scheduleRhythmicAlarm: "; // const-string v1, "scheduleRhythmicAlarm: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mAlarmTimePoints;
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "RhythmicEyeCareManager"; // const-string v1, "RhythmicEyeCareManager"
android.util.Slog .d ( v1,v0 );
/* .line 280 */
v0 = this.mAlarmTimePoints;
/* array-length v0, v0 */
/* if-nez v0, :cond_0 */
/* .line 281 */
final String v0 = "no alarm exists"; // const-string v0, "no alarm exists"
android.util.Slog .w ( v1,v0 );
/* .line 282 */
return;
/* .line 284 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->getNextAlarmTimeIndex()I */
/* iput v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmIndex:I */
/* .line 285 */
/* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->setAlarm()V */
/* .line 286 */
return;
} // .end method
private void setAlarm ( ) {
/* .locals 10 */
/* .line 289 */
v0 = this.mAlarmTimePoints;
/* iget v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAlarmIndex:I */
/* aget v0, v0, v1 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/RhythmicEyeCareManager;->getAlarmInMills(I)J */
/* move-result-wide v8 */
/* .line 290 */
/* .local v8, "startTime":J */
v1 = this.mAlarmManager;
int v2 = 0; // const/4 v2, 0x0
final String v5 = "rhythmic_eyecare_alarm"; // const-string v5, "rhythmic_eyecare_alarm"
v6 = this.mOnAlarmListener;
v7 = this.mHandler;
/* move-wide v3, v8 */
/* invoke-virtual/range {v1 ..v7}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V */
/* .line 292 */
return;
} // .end method
private void unregisterForegroundAppObserver ( ) {
/* .locals 3 */
/* .line 204 */
try { // :try_start_0
v0 = this.mActivityTaskManager;
v1 = this.mTaskStackListener;
/* .line 205 */
android.app.ActivityManager .getService ( );
v1 = this.mProcessObserver;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 208 */
/* .line 206 */
/* :catch_0 */
/* move-exception v0 */
/* .line 207 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "RhythmicEyeCareManager"; // const-string v1, "RhythmicEyeCareManager"
/* const-string/jumbo v2, "unregisterForegroundAppObserver failed." */
android.util.Slog .e ( v1,v2,v0 );
/* .line 209 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
private Boolean updateAppType ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "appType" # I */
/* .line 343 */
/* iget v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppType:I */
/* if-eq v0, p1, :cond_0 */
/* .line 344 */
/* iput p1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppType:I */
/* .line 345 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppChanged:Z */
/* .line 346 */
/* .line 348 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean updateForegroundAppType ( ) {
/* .locals 10 */
/* .line 296 */
int v0 = 0; // const/4 v0, 0x0
/* .line 297 */
/* .local v0, "topActivity":Landroid/content/ComponentName; */
int v1 = 1; // const/4 v1, 0x1
/* .line 298 */
/* .local v1, "topActivityFullScreenOrNotOccluded":Z */
int v2 = 0; // const/4 v2, 0x0
/* .line 300 */
/* .local v2, "visibleTaskNum":I */
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
v4 = this.mActivityTaskManager;
/* .line 301 */
/* .local v4, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;" */
} // :cond_0
v6 = } // :goto_0
int v7 = 1; // const/4 v7, 0x1
if ( v6 != null) { // if-eqz v6, :cond_8
/* check-cast v6, Landroid/app/ActivityTaskManager$RootTaskInfo; */
/* .line 302 */
/* .local v6, "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
/* iget v8, v6, Landroid/app/ActivityTaskManager$RootTaskInfo;->displayId:I */
/* if-nez v8, :cond_0 */
/* iget-boolean v8, v6, Landroid/app/ActivityTaskManager$RootTaskInfo;->visible:Z */
if ( v8 != null) { // if-eqz v8, :cond_0
v8 = this.topActivity;
/* .line 303 */
v8 = java.util.Objects .isNull ( v8 );
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 304 */
/* .line 306 */
} // :cond_1
v8 = (( android.app.ActivityTaskManager$RootTaskInfo ) v6 ).getWindowingMode ( ); // invoke-virtual {v6}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I
/* .line 307 */
/* .local v8, "windoingMode":I */
/* iget-boolean v9, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsDeskTopMode:Z */
/* if-nez v9, :cond_4 */
/* .line 308 */
/* if-nez v2, :cond_3 */
/* .line 309 */
v9 = this.topActivity;
/* move-object v0, v9 */
/* .line 310 */
/* if-ne v8, v7, :cond_2 */
} // :cond_2
/* move v7, v3 */
} // :goto_1
/* move v1, v7 */
/* .line 312 */
} // :cond_3
v7 = this.childTaskIds;
/* array-length v7, v7 */
/* add-int/2addr v2, v7 */
/* .line 317 */
} // :cond_4
/* if-ne v8, v7, :cond_5 */
/* .line 318 */
v5 = this.topActivity;
/* move-object v0, v5 */
/* .line 319 */
/* .line 321 */
} // :cond_5
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 323 */
int v9 = 5; // const/4 v9, 0x5
/* if-ne v8, v9, :cond_6 */
} // :cond_6
/* move v7, v3 */
} // :goto_2
/* move v1, v7 */
/* .line 326 */
} // .end local v6 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
} // .end local v8 # "windoingMode":I
} // :cond_7
} // :goto_3
/* .line 328 */
} // :cond_8
} // :goto_4
/* if-gt v2, v7, :cond_b */
if ( v1 != null) { // if-eqz v1, :cond_b
/* .line 329 */
v5 = java.util.Objects .isNull ( v0 );
/* if-nez v5, :cond_b */
v5 = this.mComponentName;
v5 = (( android.content.ComponentName ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_9
/* .line 332 */
} // :cond_9
this.mComponentName = v0;
/* .line 333 */
v5 = v5 = this.mPolicy;
if ( v5 != null) { // if-eqz v5, :cond_a
/* .line 334 */
/* move v5, v3 */
} // :cond_a
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v5 = /* invoke-direct {p0, v5}, Lcom/android/server/display/RhythmicEyeCareManager;->getAppTypeByPkgName(Ljava/lang/String;)I */
/* .line 335 */
/* .local v5, "appType":I */
} // :goto_5
v3 = /* invoke-direct {p0, v5}, Lcom/android/server/display/RhythmicEyeCareManager;->updateAppType(I)Z */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 330 */
} // .end local v5 # "appType":I
} // :cond_b
} // :goto_6
/* .line 336 */
} // .end local v0 # "topActivity":Landroid/content/ComponentName;
} // .end local v1 # "topActivityFullScreenOrNotOccluded":Z
} // .end local v2 # "visibleTaskNum":I
} // .end local v4 # "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;"
/* :catch_0 */
/* move-exception v0 */
/* .line 337 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "RhythmicEyeCareManager"; // const-string v1, "RhythmicEyeCareManager"
/* const-string/jumbo v2, "updateForegroundAppType failed." */
android.util.Slog .e ( v1,v2,v0 );
/* .line 339 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // .end method
private void updateState ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "forceApply" # Z */
/* .line 352 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->updateForegroundAppType()Z */
/* if-nez v0, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_2
} // :cond_0
/* iget v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mDisplayState:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_2 */
/* .line 354 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->getMinuteOfDay()I */
/* .line 355 */
/* .local v0, "minute":I */
/* iget-boolean v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppChanged:Z */
/* if-nez v1, :cond_1 */
/* iget v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mTime:I */
/* if-eq v1, v0, :cond_2 */
/* .line 356 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppChanged:Z */
/* .line 357 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->getMinuteOfDay()I */
/* iput v1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mTime:I */
/* .line 358 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "applyChange, appType="; // const-string v2, "applyChange, appType="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppType:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", minute="; // const-string v2, ", minute="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "RhythmicEyeCareManager"; // const-string v2, "RhythmicEyeCareManager"
android.util.Slog .i ( v2,v1 );
/* .line 361 */
v1 = this.mRhythmicEyeCareListener;
/* iget v2, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mAppType:I */
/* .line 364 */
} // .end local v0 # "minute":I
} // :cond_2
return;
} // .end method
/* # virtual methods */
public void notifyScreenStateChanged ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "state" # I */
/* .line 367 */
/* iget v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mDisplayState:I */
/* if-eq v0, p1, :cond_0 */
/* .line 368 */
/* iput p1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mDisplayState:I */
/* .line 369 */
/* iget-boolean v0, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_0 */
/* .line 370 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/display/RhythmicEyeCareManager$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/RhythmicEyeCareManager$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/RhythmicEyeCareManager;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 373 */
} // :cond_0
return;
} // .end method
public void setModeEnable ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 186 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 187 */
/* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->enableRhythmicEyeCareMode()V */
/* .line 189 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager;->disableRhythmicEyeCareMode()V */
/* .line 191 */
} // :goto_0
return;
} // .end method
public void setRhythmicEyeCareListener ( com.android.server.display.RhythmicEyeCareManager$RhythmicEyeCareListener p0 ) {
/* .locals 0 */
/* .param p1, "listener" # Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener; */
/* .line 177 */
this.mRhythmicEyeCareListener = p1;
/* .line 178 */
return;
} // .end method
public void updateDeskTopMode ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "deskTopModeEnabled" # Z */
/* .line 376 */
/* iput-boolean p1, p0, Lcom/android/server/display/RhythmicEyeCareManager;->mIsDeskTopMode:Z */
/* .line 377 */
return;
} // .end method
public void updateRhythmicAppCategoryList ( java.util.List p0, java.util.List p1 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 181 */
/* .local p1, "imageAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p2, "readAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mAppCategoryMapper;
int v1 = 1; // const/4 v1, 0x1
(( android.util.SparseArray ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 182 */
v0 = this.mAppCategoryMapper;
int v1 = 2; // const/4 v1, 0x2
(( android.util.SparseArray ) v0 ).put ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 183 */
return;
} // .end method
