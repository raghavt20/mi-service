public class com.android.server.display.BrightnessMappingStrategyImpl implements com.android.server.display.BrightnessMappingStrategyStub {
	 /* .source "BrightnessMappingStrategyImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Boolean DEBUG;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final Boolean IS_INTERNAL_BUILD;
	 private final Boolean IS_SUPPORT_AUTOBRIGHTNESS_BY_APPLICATION_CATEGORY;
	 private com.android.server.display.BrightnessCurve mBrightnessCurve;
	 private Float sUnadjustedBrightness;
	 /* # direct methods */
	 public com.android.server.display.BrightnessMappingStrategyImpl ( ) {
		 /* .locals 2 */
		 /* .line 16 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 21 */
		 /* const/high16 v0, -0x40800000 # -1.0f */
		 /* iput v0, p0, Lcom/android/server/display/BrightnessMappingStrategyImpl;->sUnadjustedBrightness:F */
		 /* .line 23 */
		 /* nop */
		 /* .line 24 */
		 /* const-string/jumbo v0, "support_autobrightness_by_application_category" */
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 miui.util.FeatureParser .getBoolean ( v0,v1 );
		 /* iput-boolean v0, p0, Lcom/android/server/display/BrightnessMappingStrategyImpl;->IS_SUPPORT_AUTOBRIGHTNESS_BY_APPLICATION_CATEGORY:Z */
		 /* .line 25 */
		 /* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
		 /* iput-boolean v0, p0, Lcom/android/server/display/BrightnessMappingStrategyImpl;->IS_INTERNAL_BUILD:Z */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void dump ( java.io.PrintWriter p0 ) {
		 /* .locals 1 */
		 /* .param p1, "pw" # Ljava/io/PrintWriter; */
		 /* .line 101 */
		 v0 = this.mBrightnessCurve;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 102 */
			 (( com.android.server.display.BrightnessCurve ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/BrightnessCurve;->dump(Ljava/io/PrintWriter;)V
			 /* .line 104 */
		 } // :cond_0
		 return;
	 } // .end method
	 public Float getMaxScreenNit ( ) {
		 /* .locals 2 */
		 /* .line 31 */
		 /* nop */
		 /* .line 32 */
		 final String v0 = "persist.vendor.max.brightness"; // const-string v0, "persist.vendor.max.brightness"
		 final String v1 = "0"; // const-string v1, "0"
		 android.os.SystemProperties .get ( v0,v1 );
		 /* .line 31 */
		 v0 = 		 java.lang.Float .parseFloat ( v0 );
	 } // .end method
	 public com.android.server.display.BrightnessMappingStrategy getMiuiMapperInstance ( android.hardware.display.BrightnessConfiguration p0, Float[] p1, Float[] p2, Float p3 ) {
		 /* .locals 1 */
		 /* .param p1, "build" # Landroid/hardware/display/BrightnessConfiguration; */
		 /* .param p2, "nitsRange" # [F */
		 /* .param p3, "brightnessRange" # [F */
		 /* .param p4, "autoBrightnessAdjustmentMaxGamma" # F */
		 /* .line 72 */
		 /* new-instance v0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy; */
		 /* invoke-direct {v0, p1, p2, p3, p4}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;-><init>(Landroid/hardware/display/BrightnessConfiguration;[F[FF)V */
	 } // .end method
	 public void initMiuiCurve ( android.hardware.display.BrightnessConfiguration p0, android.util.Spline p1, android.util.Spline p2 ) {
		 /* .locals 1 */
		 /* .param p1, "defaultConfig" # Landroid/hardware/display/BrightnessConfiguration; */
		 /* .param p2, "nitsToBrightnessSpline" # Landroid/util/Spline; */
		 /* .param p3, "brightnessToNitsSpline" # Landroid/util/Spline; */
		 /* .line 89 */
		 /* new-instance v0, Lcom/android/server/display/BrightnessCurve; */
		 /* invoke-direct {v0, p1, p2, p3}, Lcom/android/server/display/BrightnessCurve;-><init>(Landroid/hardware/display/BrightnessConfiguration;Landroid/util/Spline;Landroid/util/Spline;)V */
		 this.mBrightnessCurve = v0;
		 /* .line 91 */
		 return;
	 } // .end method
	 public Boolean isSupportAutobrightnessByApplicationCategory ( ) {
		 /* .locals 1 */
		 /* .line 65 */
		 /* iget-boolean v0, p0, Lcom/android/server/display/BrightnessMappingStrategyImpl;->IS_SUPPORT_AUTOBRIGHTNESS_BY_APPLICATION_CATEGORY:Z */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* iget-boolean v0, p0, Lcom/android/server/display/BrightnessMappingStrategyImpl;->IS_INTERNAL_BUILD:Z */
			 /* if-nez v0, :cond_0 */
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :goto_0
} // .end method
public Boolean smoothNewCurve ( Float[] p0, Float[] p1, Integer p2 ) {
	 /* .locals 5 */
	 /* .param p1, "lux" # [F */
	 /* .param p2, "brightness" # [F */
	 /* .param p3, "idx" # I */
	 /* .line 50 */
	 /* add-int/lit8 v0, p3, 0x1 */
	 /* .local v0, "i":I */
} // :goto_0
/* array-length v1, p1 */
/* if-ge v0, v1, :cond_0 */
/* .line 51 */
/* aget v1, p2, v0 */
/* iget v2, p0, Lcom/android/server/display/BrightnessMappingStrategyImpl;->sUnadjustedBrightness:F */
/* aget v3, p2, p3 */
/* sub-float/2addr v2, v3 */
/* sub-float/2addr v1, v2 */
/* aput v1, p2, v0 */
/* .line 52 */
/* aget v1, p2, v0 */
/* add-int/lit8 v2, v0, -0x1 */
/* aget v2, p2, v2 */
v1 = android.util.MathUtils .max ( v1,v2 );
/* aput v1, p2, v0 */
/* .line 50 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 54 */
} // .end local v0 # "i":I
} // :cond_0
/* add-int/lit8 v0, p3, -0x1 */
/* .restart local v0 # "i":I */
} // :goto_1
/* if-ltz v0, :cond_1 */
/* .line 55 */
/* aget v1, p2, v0 */
/* iget v2, p0, Lcom/android/server/display/BrightnessMappingStrategyImpl;->sUnadjustedBrightness:F */
/* aget v3, p2, p3 */
/* sub-float v3, v2, v3 */
/* aget v4, p2, v0 */
/* mul-float/2addr v3, v4 */
/* div-float/2addr v3, v2 */
/* sub-float/2addr v1, v3 */
/* aput v1, p2, v0 */
/* .line 57 */
/* aget v1, p2, v0 */
/* add-int/lit8 v2, v0, 0x1 */
/* aget v2, p2, v2 */
v1 = android.util.MathUtils .min ( v1,v2 );
/* aput v1, p2, v0 */
/* .line 54 */
/* add-int/lit8 v0, v0, -0x1 */
/* .line 60 */
} // .end local v0 # "i":I
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // .end method
public android.util.Pair smoothNewCurveV2 ( Float[] p0, Float[] p1, Integer p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "lux" # [F */
/* .param p2, "brightness" # [F */
/* .param p3, "idx" # I */
/* .param p4, "curveV2Enable" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([F[FIZ)", */
/* "Landroid/util/Pair<", */
/* "[F[F>;" */
/* } */
} // .end annotation
/* .line 78 */
if ( p4 != null) { // if-eqz p4, :cond_0
v0 = this.mBrightnessCurve;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.display.BrightnessCurve ) v0 ).isEnable ( ); // invoke-virtual {v0}, Lcom/android/server/display/BrightnessCurve;->isEnable()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 79 */
v0 = this.mBrightnessCurve;
(( com.android.server.display.BrightnessCurve ) v0 ).smoothNewCurveV2 ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/BrightnessCurve;->smoothNewCurveV2([F[FI)Landroid/util/Pair;
/* .line 81 */
} // :cond_0
(( com.android.server.display.BrightnessMappingStrategyImpl ) p0 ).smoothNewCurve ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/display/BrightnessMappingStrategyImpl;->smoothNewCurve([F[FI)Z
/* .line 82 */
android.util.Pair .create ( p1,p2 );
} // .end method
public void updateSplineConfig ( android.hardware.display.BrightnessConfiguration p0, android.util.Spline p1, android.util.Spline p2 ) {
/* .locals 1 */
/* .param p1, "config" # Landroid/hardware/display/BrightnessConfiguration; */
/* .param p2, "nitsToBrightnessSpline" # Landroid/util/Spline; */
/* .param p3, "brightnessToNitsSpline" # Landroid/util/Spline; */
/* .line 96 */
v0 = this.mBrightnessCurve;
(( com.android.server.display.BrightnessCurve ) v0 ).updateSplineConfig ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/BrightnessCurve;->updateSplineConfig(Landroid/hardware/display/BrightnessConfiguration;Landroid/util/Spline;Landroid/util/Spline;)V
/* .line 97 */
return;
} // .end method
public void updateUnadjustedBrightness ( Float p0, Float p1, Float p2 ) {
/* .locals 0 */
/* .param p1, "lux" # F */
/* .param p2, "brightness" # F */
/* .param p3, "unadjustedbrightness" # F */
/* .line 41 */
/* iput p3, p0, Lcom/android/server/display/BrightnessMappingStrategyImpl;->sUnadjustedBrightness:F */
/* .line 42 */
return;
} // .end method
