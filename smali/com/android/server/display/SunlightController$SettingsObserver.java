class com.android.server.display.SunlightController$SettingsObserver extends android.database.ContentObserver {
	 /* .source "SunlightController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/SunlightController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "SettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.display.SunlightController this$0; //synthetic
/* # direct methods */
public com.android.server.display.SunlightController$SettingsObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 517 */
this.this$0 = p1;
/* .line 518 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 519 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 523 */
(( android.net.Uri ) p2 ).getLastPathSegment ( ); // invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;
/* .line 524 */
/* .local v0, "lastPathSegment":Ljava/lang/String; */
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v1 = "screen_brightness"; // const-string v1, "screen_brightness"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 2; // const/4 v1, 0x2
/* :sswitch_1 */
final String v1 = "screen_brightness_mode"; // const-string v1, "screen_brightness_mode"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 int v1 = 1; // const/4 v1, 0x1
	 /* :sswitch_2 */
	 final String v1 = "low_power_level_state"; // const-string v1, "low_power_level_state"
	 v1 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 int v1 = 3; // const/4 v1, 0x3
		 /* :sswitch_3 */
		 /* const-string/jumbo v1, "sunlight_mode" */
		 v1 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 int v1 = 0; // const/4 v1, 0x0
		 } // :goto_0
		 int v1 = -1; // const/4 v1, -0x1
	 } // :goto_1
	 /* packed-switch v1, :pswitch_data_0 */
	 /* .line 531 */
	 /* :pswitch_0 */
	 v1 = this.this$0;
	 com.android.server.display.SunlightController .-$$Nest$mupdateLowPowerState ( v1 );
	 /* .line 532 */
	 /* .line 528 */
	 /* :pswitch_1 */
	 v1 = this.this$0;
	 com.android.server.display.SunlightController .-$$Nest$mupdateSunlightModeSettings ( v1 );
	 /* .line 529 */
	 /* nop */
	 /* .line 537 */
} // :goto_2
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x69203588 -> :sswitch_3 */
/* -0x2a02884f -> :sswitch_2 */
/* -0x294f7102 -> :sswitch_1 */
/* 0x67748604 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
