class com.android.server.display.DisplayPowerControllerImpl$RotationWatcher extends android.view.IRotationWatcher$Stub {
	 /* .source "DisplayPowerControllerImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/DisplayPowerControllerImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "RotationWatcher" */
} // .end annotation
/* # instance fields */
final com.android.server.display.DisplayPowerControllerImpl this$0; //synthetic
/* # direct methods */
private com.android.server.display.DisplayPowerControllerImpl$RotationWatcher ( ) {
/* .locals 0 */
/* .line 2302 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/view/IRotationWatcher$Stub;-><init>()V */
return;
} // .end method
 com.android.server.display.DisplayPowerControllerImpl$RotationWatcher ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl$RotationWatcher;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V */
return;
} // .end method
/* # virtual methods */
public void onRotationChanged ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "rotation" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 2306 */
v0 = this.this$0;
com.android.server.display.DisplayPowerControllerImpl .-$$Nest$fgetmHandler ( v0 );
int v1 = 7; // const/4 v1, 0x7
java.lang.Integer .valueOf ( p1 );
android.os.Message .obtain ( v0,v1,v2 );
/* .line 2307 */
/* .local v0, "msg":Landroid/os/Message; */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 2308 */
return;
} // .end method
