public class com.android.server.display.TouchCoverProtectionHelper {
	 /* .source "TouchCoverProtectionHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/TouchCoverProtectionHelper$Injector;, */
	 /* Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker; */
	 /* } */
} // .end annotation
/* # static fields */
private static Boolean DEBUG;
private static final Integer GAME_TOUCH_EVENT_INTERVAL;
private static final java.lang.String TAG;
/* # instance fields */
private android.graphics.Rect mBorderRect;
private android.content.Context mContext;
private android.graphics.Rect mCurrentBorderRect;
private android.hardware.devicestate.DeviceStateManager mDeviceStateManager;
private final android.hardware.display.DisplayManager$DisplayListener mDisplayListener;
private Boolean mDisplayListenerEnabled;
private android.hardware.display.DisplayManager mDisplayManager;
private com.android.server.display.DisplayManagerServiceImpl mDisplayManagerServiceImpl;
private android.hardware.devicestate.DeviceStateManager$FoldStateListener mFoldStateListener;
private android.os.Handler mHandler;
private com.android.server.display.TouchCoverProtectionHelper$Injector mInjector;
private com.android.server.display.LogicalDisplay mLogicalDisplay;
private Integer mLogicalHeight;
private Integer mLogicalWidth;
private Integer mRotation;
private android.graphics.Rect mSecondDisplayBorderRect;
private java.lang.Object mSyncRoot;
private Boolean mTouchAreaEnabled;
private final android.graphics.Rect mTouchCoverProtectionRect;
private Integer mTouchEventDebounce;
private com.android.server.display.TouchCoverProtectionHelper$TouchPositionTracker mTouchPositionTracker;
private Boolean mTouchTrackingEnabled;
private com.android.server.wm.WindowManagerService mWindowManagerService;
/* # direct methods */
public static void $r8$lambda$d80CvqLEszP8mTzTNFJKHuIMvKw ( com.android.server.display.TouchCoverProtectionHelper p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/TouchCoverProtectionHelper;->lambda$showTouchCoverProtectionRect$1()V */
	 return;
} // .end method
public static void $r8$lambda$gyDFvXjtaGxAXT4-76TKF2NnMWc ( com.android.server.display.TouchCoverProtectionHelper p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/TouchCoverProtectionHelper;->lambda$showTouchCoverProtectionRect$2()V */
	 return;
} // .end method
public static void $r8$lambda$t9WMKeqeoE0goZnzEVv7-txU8KE ( com.android.server.display.TouchCoverProtectionHelper p0, java.lang.Boolean p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper;->lambda$new$0(Ljava/lang/Boolean;)V */
	 return;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.display.TouchCoverProtectionHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static com.android.server.display.LogicalDisplay -$$Nest$fgetmLogicalDisplay ( com.android.server.display.TouchCoverProtectionHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mLogicalDisplay;
} // .end method
static android.graphics.Rect -$$Nest$fgetmTouchCoverProtectionRect ( com.android.server.display.TouchCoverProtectionHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mTouchCoverProtectionRect;
} // .end method
static void -$$Nest$mupdateTouchCoverProtectionRect ( com.android.server.display.TouchCoverProtectionHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/TouchCoverProtectionHelper;->updateTouchCoverProtectionRect()V */
	 return;
} // .end method
static Boolean -$$Nest$sfgetDEBUG ( ) { //bridge//synthethic
	 /* .locals 1 */
	 /* sget-boolean v0, Lcom/android/server/display/TouchCoverProtectionHelper;->DEBUG:Z */
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
	 /* .locals 1 */
	 v0 = com.android.server.display.TouchCoverProtectionHelper.TAG;
} // .end method
static com.android.server.display.TouchCoverProtectionHelper ( ) {
	 /* .locals 1 */
	 /* .line 39 */
	 /* const-class v0, Lcom/android/server/display/TouchCoverProtectionHelper; */
	 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 return;
} // .end method
public com.android.server.display.TouchCoverProtectionHelper ( ) {
	 /* .locals 4 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "looper" # Landroid/os/Looper; */
	 /* .line 75 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 67 */
	 /* new-instance v0, Landroid/graphics/Rect; */
	 /* invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V */
	 this.mTouchCoverProtectionRect = v0;
	 /* .line 282 */
	 /* new-instance v0, Lcom/android/server/display/TouchCoverProtectionHelper$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/TouchCoverProtectionHelper$1;-><init>(Lcom/android/server/display/TouchCoverProtectionHelper;)V */
	 this.mDisplayListener = v0;
	 /* .line 76 */
	 this.mContext = p1;
	 /* .line 77 */
	 /* new-instance v0, Landroid/os/Handler; */
	 /* invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 78 */
	 /* new-instance v0, Lcom/android/server/display/TouchCoverProtectionHelper$Injector; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/TouchCoverProtectionHelper$Injector;-><init>(Lcom/android/server/display/TouchCoverProtectionHelper;)V */
	 this.mInjector = v0;
	 /* .line 79 */
	 /* const-string/jumbo v0, "window" */
	 android.os.ServiceManager .getService ( v0 );
	 /* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
	 this.mWindowManagerService = v0;
	 /* .line 80 */
	 v0 = this.mContext;
	 final String v1 = "display"; // const-string v1, "display"
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/hardware/display/DisplayManager; */
	 this.mDisplayManager = v0;
	 /* .line 81 */
	 com.android.server.display.DisplayManagerServiceStub .getInstance ( );
	 /* check-cast v0, Lcom/android/server/display/DisplayManagerServiceImpl; */
	 this.mDisplayManagerServiceImpl = v0;
	 /* .line 82 */
	 (( com.android.server.display.DisplayManagerServiceImpl ) v0 ).getSyncRoot ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayManagerServiceImpl;->getSyncRoot()Ljava/lang/Object;
	 this.mSyncRoot = v0;
	 /* .line 83 */
	 /* new-instance v0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;-><init>(Lcom/android/server/display/TouchCoverProtectionHelper;Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker-IA;)V */
	 this.mTouchPositionTracker = v0;
	 /* .line 84 */
	 (( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
	 /* .line 85 */
	 /* .local v0, "resources":Landroid/content/res/Resources; */
	 /* const v1, 0x11050083 */
	 v1 = 	 (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
	 /* iput-boolean v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchAreaEnabled:Z */
	 /* .line 88 */
	 /* const v1, 0x11030058 */
	 (( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
	 /* invoke-direct {p0, v1}, Lcom/android/server/display/TouchCoverProtectionHelper;->setBorderRect([I)Landroid/graphics/Rect; */
	 this.mBorderRect = v1;
	 /* .line 90 */
	 /* const v1, 0x11030050 */
	 (( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
	 /* invoke-direct {p0, v1}, Lcom/android/server/display/TouchCoverProtectionHelper;->setBorderRect([I)Landroid/graphics/Rect; */
	 this.mSecondDisplayBorderRect = v1;
	 /* .line 92 */
	 /* const v1, 0x110b0010 */
	 v1 = 	 (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
	 /* iput v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchEventDebounce:I */
	 /* .line 94 */
	 v1 = this.mBorderRect;
	 this.mCurrentBorderRect = v1;
	 /* .line 96 */
	 /* new-instance v1, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener; */
	 /* new-instance v2, Lcom/android/server/display/TouchCoverProtectionHelper$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v2, p0}, Lcom/android/server/display/TouchCoverProtectionHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/TouchCoverProtectionHelper;)V */
	 /* invoke-direct {v1, p1, v2}, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;-><init>(Landroid/content/Context;Ljava/util/function/Consumer;)V */
	 this.mFoldStateListener = v1;
	 /* .line 97 */
	 /* const-class v1, Landroid/hardware/devicestate/DeviceStateManager; */
	 (( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
	 /* check-cast v1, Landroid/hardware/devicestate/DeviceStateManager; */
	 this.mDeviceStateManager = v1;
	 /* .line 98 */
	 /* new-instance v2, Landroid/os/HandlerExecutor; */
	 v3 = this.mHandler;
	 /* invoke-direct {v2, v3}, Landroid/os/HandlerExecutor;-><init>(Landroid/os/Handler;)V */
	 v3 = this.mFoldStateListener;
	 (( android.hardware.devicestate.DeviceStateManager ) v1 ).registerCallback ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/hardware/devicestate/DeviceStateManager;->registerCallback(Ljava/util/concurrent/Executor;Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;)V
	 /* .line 99 */
	 return;
} // .end method
private void lambda$new$0 ( java.lang.Boolean p0 ) { //synthethic
	 /* .locals 0 */
	 /* .param p1, "folded" # Ljava/lang/Boolean; */
	 /* .line 96 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper;->updateBorderRect(Ljava/lang/Boolean;)V */
	 return;
} // .end method
private void lambda$showTouchCoverProtectionRect$1 ( ) { //synthethic
	 /* .locals 1 */
	 /* .line 310 */
	 v0 = this.mInjector;
	 com.android.server.display.TouchCoverProtectionHelper$Injector .-$$Nest$mshow ( v0 );
	 return;
} // .end method
private void lambda$showTouchCoverProtectionRect$2 ( ) { //synthethic
	 /* .locals 1 */
	 /* .line 312 */
	 v0 = this.mInjector;
	 com.android.server.display.TouchCoverProtectionHelper$Injector .-$$Nest$mhide ( v0 );
	 return;
} // .end method
private android.graphics.Rect setBorderRect ( Integer[] p0 ) {
	 /* .locals 5 */
	 /* .param p1, "configArray" # [I */
	 /* .line 262 */
	 /* array-length v0, p1 */
	 int v1 = 4; // const/4 v1, 0x4
	 int v2 = 0; // const/4 v2, 0x0
	 /* if-eq v0, v1, :cond_0 */
	 /* .line 263 */
	 v0 = com.android.server.display.TouchCoverProtectionHelper.TAG;
	 final String v1 = "The touch cover array configuration must be four."; // const-string v1, "The touch cover array configuration must be four."
	 android.util.Slog .w ( v0,v1 );
	 /* .line 264 */
	 /* new-instance v0, Landroid/graphics/Rect; */
	 /* invoke-direct {v0, v2, v2, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V */
	 /* .line 266 */
} // :cond_0
/* new-instance v0, Landroid/graphics/Rect; */
/* aget v1, p1, v2 */
int v2 = 1; // const/4 v2, 0x1
/* aget v2, p1, v2 */
int v3 = 2; // const/4 v3, 0x2
/* aget v3, p1, v3 */
int v4 = 3; // const/4 v4, 0x3
/* aget v4, p1, v4 */
/* invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V */
} // .end method
private void setDisplayListenerEnabled ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 250 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 251 */
	 /* iget-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayListenerEnabled:Z */
	 /* if-nez v0, :cond_1 */
	 /* .line 252 */
	 v0 = this.mDisplayManager;
	 v1 = this.mDisplayListener;
	 v2 = this.mHandler;
	 (( android.hardware.display.DisplayManager ) v0 ).registerDisplayListener ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V
	 /* .line 253 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayListenerEnabled:Z */
	 /* .line 255 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayListenerEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 256 */
	 v0 = this.mDisplayManager;
	 v1 = this.mDisplayListener;
	 (( android.hardware.display.DisplayManager ) v0 ).unregisterDisplayListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V
	 /* .line 257 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayListenerEnabled:Z */
	 /* .line 259 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void setTouchTrackingEnabled ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 270 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 271 */
/* iget-boolean v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchTrackingEnabled:Z */
/* if-nez v1, :cond_1 */
/* .line 272 */
v1 = this.mWindowManagerService;
v2 = this.mTouchPositionTracker;
(( com.android.server.wm.WindowManagerService ) v1 ).registerPointerEventListener ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/android/server/wm/WindowManagerService;->registerPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V
/* .line 273 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchTrackingEnabled:Z */
/* .line 275 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchTrackingEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 277 */
v1 = this.mWindowManagerService;
v2 = this.mTouchPositionTracker;
(( com.android.server.wm.WindowManagerService ) v1 ).unregisterPointerEventListener ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/android/server/wm/WindowManagerService;->unregisterPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V
/* .line 278 */
/* iput-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchTrackingEnabled:Z */
/* .line 280 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void updateBorderRect ( java.lang.Boolean p0 ) {
/* .locals 1 */
/* .param p1, "folded" # Ljava/lang/Boolean; */
/* .line 114 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = (( java.lang.Boolean ) p1 ).booleanValue ( ); // invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 115 */
v0 = this.mSecondDisplayBorderRect;
this.mCurrentBorderRect = v0;
/* .line 117 */
} // :cond_0
v0 = this.mBorderRect;
this.mCurrentBorderRect = v0;
/* .line 119 */
} // :goto_0
return;
} // .end method
private void updateTouchCoverProtectionRect ( ) {
/* .locals 15 */
/* .line 137 */
/* iget-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchAreaEnabled:Z */
/* if-nez v0, :cond_0 */
/* .line 139 */
return;
/* .line 141 */
} // :cond_0
v0 = this.mLogicalDisplay;
/* if-nez v0, :cond_1 */
/* .line 143 */
return;
/* .line 146 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 147 */
/* .local v0, "physicalWidth":F */
int v1 = 0; // const/4 v1, 0x0
/* .line 149 */
/* .local v1, "physicalHeight":F */
int v2 = 0; // const/4 v2, 0x0
/* .line 150 */
/* .local v2, "logicalWidth":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 151 */
/* .local v3, "logicalHeight":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 154 */
/* .local v4, "rotation":I */
v5 = this.mSyncRoot;
/* monitor-enter v5 */
/* .line 155 */
try { // :try_start_0
v6 = this.mLogicalDisplay;
(( com.android.server.display.LogicalDisplay ) v6 ).getPrimaryDisplayDeviceLocked ( ); // invoke-virtual {v6}, Lcom/android/server/display/LogicalDisplay;->getPrimaryDisplayDeviceLocked()Lcom/android/server/display/DisplayDevice;
/* .line 156 */
/* .local v6, "displayDevice":Lcom/android/server/display/DisplayDevice; */
/* if-nez v6, :cond_2 */
/* .line 157 */
v7 = com.android.server.display.TouchCoverProtectionHelper.TAG;
final String v8 = "current display device is not exist"; // const-string v8, "current display device is not exist"
android.util.Slog .w ( v7,v8 );
/* .line 158 */
/* monitor-exit v5 */
return;
/* .line 161 */
} // :cond_2
(( com.android.server.display.DisplayDevice ) v6 ).getDisplayDeviceInfoLocked ( ); // invoke-virtual {v6}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;
/* .line 162 */
/* .local v7, "displayDeviceInfo":Lcom/android/server/display/DisplayDeviceInfo; */
/* iget v8, v7, Lcom/android/server/display/DisplayDeviceInfo;->width:I */
/* int-to-float v0, v8 */
/* .line 163 */
/* iget v8, v7, Lcom/android/server/display/DisplayDeviceInfo;->height:I */
/* int-to-float v1, v8 */
/* .line 166 */
v8 = this.mLogicalDisplay;
(( com.android.server.display.LogicalDisplay ) v8 ).getDisplayInfoLocked ( ); // invoke-virtual {v8}, Lcom/android/server/display/LogicalDisplay;->getDisplayInfoLocked()Landroid/view/DisplayInfo;
/* .line 167 */
/* .local v8, "displayInfo":Landroid/view/DisplayInfo; */
/* iget v9, v8, Landroid/view/DisplayInfo;->logicalWidth:I */
/* move v2, v9 */
/* .line 168 */
/* iget v9, v8, Landroid/view/DisplayInfo;->logicalHeight:I */
/* move v3, v9 */
/* .line 169 */
/* iget v9, v8, Landroid/view/DisplayInfo;->rotation:I */
/* move v4, v9 */
/* .line 170 */
} // .end local v6 # "displayDevice":Lcom/android/server/display/DisplayDevice;
} // .end local v7 # "displayDeviceInfo":Lcom/android/server/display/DisplayDeviceInfo;
} // .end local v8 # "displayInfo":Landroid/view/DisplayInfo;
/* monitor-exit v5 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 172 */
/* iget v5, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalWidth:I */
/* if-ne v5, v2, :cond_3 */
/* iget v5, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalHeight:I */
/* if-ne v5, v3, :cond_3 */
/* iget v5, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mRotation:I */
/* if-ne v4, v5, :cond_3 */
/* .line 174 */
return;
/* .line 177 */
} // :cond_3
/* iput v2, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalWidth:I */
/* .line 178 */
/* iput v3, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalHeight:I */
/* .line 179 */
/* iput v4, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mRotation:I */
/* .line 182 */
int v5 = 0; // const/4 v5, 0x0
/* .line 183 */
/* .local v5, "left":I */
int v6 = 0; // const/4 v6, 0x0
/* .line 184 */
/* .local v6, "right":I */
int v7 = 0; // const/4 v7, 0x0
/* .line 185 */
/* .local v7, "top":I */
int v8 = 0; // const/4 v8, 0x0
/* .line 186 */
/* .local v8, "bottom":I */
int v9 = 3; // const/4 v9, 0x3
int v10 = 1; // const/4 v10, 0x1
/* if-ne v4, v10, :cond_4 */
/* .line 187 */
v11 = this.mCurrentBorderRect;
/* iget v5, v11, Landroid/graphics/Rect;->top:I */
/* .line 188 */
v11 = this.mCurrentBorderRect;
/* iget v6, v11, Landroid/graphics/Rect;->bottom:I */
/* .line 189 */
v11 = this.mCurrentBorderRect;
/* iget v11, v11, Landroid/graphics/Rect;->right:I */
/* int-to-float v11, v11 */
/* sub-float v11, v0, v11 */
/* float-to-int v7, v11 */
/* .line 190 */
v11 = this.mCurrentBorderRect;
/* iget v11, v11, Landroid/graphics/Rect;->left:I */
/* int-to-float v11, v11 */
/* sub-float v11, v0, v11 */
/* float-to-int v8, v11 */
/* .line 191 */
} // :cond_4
/* if-ne v4, v9, :cond_5 */
/* .line 192 */
v11 = this.mCurrentBorderRect;
/* iget v11, v11, Landroid/graphics/Rect;->bottom:I */
/* int-to-float v11, v11 */
/* sub-float v11, v1, v11 */
/* float-to-int v5, v11 */
/* .line 193 */
v11 = this.mCurrentBorderRect;
/* iget v11, v11, Landroid/graphics/Rect;->top:I */
/* int-to-float v11, v11 */
/* sub-float v11, v1, v11 */
/* float-to-int v6, v11 */
/* .line 194 */
v11 = this.mCurrentBorderRect;
/* iget v7, v11, Landroid/graphics/Rect;->left:I */
/* .line 195 */
v11 = this.mCurrentBorderRect;
/* iget v8, v11, Landroid/graphics/Rect;->right:I */
/* .line 197 */
} // :cond_5
} // :goto_0
v11 = this.mTouchCoverProtectionRect;
(( android.graphics.Rect ) v11 ).set ( v5, v7, v6, v8 ); // invoke-virtual {v11, v5, v7, v6, v8}, Landroid/graphics/Rect;->set(IIII)V
/* .line 200 */
/* if-eq v4, v10, :cond_7 */
/* if-ne v4, v9, :cond_6 */
} // :cond_6
int v10 = 0; // const/4 v10, 0x0
} // :cond_7
} // :goto_1
/* move v9, v10 */
/* .line 201 */
/* .local v9, "rotated":Z */
if ( v9 != null) { // if-eqz v9, :cond_8
/* move v10, v3 */
} // :cond_8
/* move v10, v2 */
} // :goto_2
/* int-to-float v10, v10 */
/* div-float/2addr v10, v0 */
/* .line 202 */
/* .local v10, "scale":F */
/* float-to-double v11, v10 */
/* const-wide/high16 v13, 0x3ff0000000000000L # 1.0 */
/* cmpl-double v11, v11, v13 */
if ( v11 != null) { // if-eqz v11, :cond_9
/* .line 203 */
v11 = this.mTouchCoverProtectionRect;
(( android.graphics.Rect ) v11 ).scale ( v10 ); // invoke-virtual {v11, v10}, Landroid/graphics/Rect;->scale(F)V
/* .line 206 */
} // :cond_9
v11 = com.android.server.display.TouchCoverProtectionHelper.TAG;
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v13, "updateTouchCoverProtectionRect, mLogicalWidth: " */
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v13, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalWidth:I */
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v13 = ", mLogicalHeight: "; // const-string v13, ", mLogicalHeight: "
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v13, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalHeight:I */
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v13 = ", physicalWidth: "; // const-string v13, ", physicalWidth: "
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v0 ); // invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v13 = ", physicalHeight: "; // const-string v13, ", physicalHeight: "
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v1 ); // invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v13 = ", scale: "; // const-string v13, ", scale: "
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v10 ); // invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v11,v12 );
/* .line 211 */
return;
/* .line 170 */
} // .end local v5 # "left":I
} // .end local v6 # "right":I
} // .end local v7 # "top":I
} // .end local v8 # "bottom":I
} // .end local v9 # "rotated":Z
} // .end local v10 # "scale":F
/* :catchall_0 */
/* move-exception v6 */
try { // :try_start_1
/* monitor-exit v5 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v6 */
} // .end method
/* # virtual methods */
protected void configure ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 102 */
/* iget-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchAreaEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 103 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper;->setTouchTrackingEnabled(Z)V */
/* .line 104 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper;->setDisplayListenerEnabled(Z)V */
/* .line 106 */
} // :cond_0
return;
} // .end method
protected void dump ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 317 */
/* sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_ABC:Z */
com.android.server.display.TouchCoverProtectionHelper.DEBUG = (v0!= 0);
/* .line 318 */
final String v0 = "Touch cover protection state:"; // const-string v0, "Touch cover protection state:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 319 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTouchAreaEnabled="; // const-string v1, " mTouchAreaEnabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchAreaEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 320 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " isTouchCoverProtectionActive="; // const-string v1, " isTouchCoverProtectionActive="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mTouchPositionTracker;
v1 = (( com.android.server.display.TouchCoverProtectionHelper$TouchPositionTracker ) v1 ).isTouchingInArea ( ); // invoke-virtual {v1}, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->isTouchingInArea()Z
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 321 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mBorderLeft="; // const-string v1, " mBorderLeft="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCurrentBorderRect;
/* iget v1, v1, Landroid/graphics/Rect;->left:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 322 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mBorderTop="; // const-string v1, " mBorderTop="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCurrentBorderRect;
/* iget v1, v1, Landroid/graphics/Rect;->top:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 323 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mBorderRight="; // const-string v1, " mBorderRight="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCurrentBorderRect;
/* iget v1, v1, Landroid/graphics/Rect;->right:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 324 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mBorderBottom="; // const-string v1, " mBorderBottom="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCurrentBorderRect;
/* iget v1, v1, Landroid/graphics/Rect;->bottom:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 325 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTouchCoverProtectionRect="; // const-string v1, " mTouchCoverProtectionRect="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mTouchCoverProtectionRect;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 326 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTouchEventDebounce="; // const-string v1, " mTouchEventDebounce="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchEventDebounce:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 327 */
return;
} // .end method
protected Boolean isGameSceneWithinTouchTime ( ) {
/* .locals 6 */
/* .line 236 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 237 */
/* .local v0, "now":J */
v2 = this.mTouchPositionTracker;
com.android.server.display.TouchCoverProtectionHelper$TouchPositionTracker .-$$Nest$fgetmLastObservedTouchTime ( v2 );
/* move-result-wide v2 */
/* sub-long v2, v0, v2 */
/* const-wide/32 v4, 0x2bf20 */
/* cmp-long v2, v2, v4 */
/* if-gtz v2, :cond_1 */
/* .line 238 */
/* sget-boolean v2, Lcom/android/server/display/TouchCoverProtectionHelper;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 239 */
v2 = com.android.server.display.TouchCoverProtectionHelper.TAG;
final String v3 = "Time of light sensor event is within given time of touch event in game scene."; // const-string v3, "Time of light sensor event is within given time of touch event in game scene."
android.util.Slog .d ( v2,v3 );
/* .line 241 */
} // :cond_0
int v2 = 1; // const/4 v2, 0x1
/* .line 243 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // .end method
protected Boolean isTouchCoverProtectionActive ( ) {
/* .locals 8 */
/* .line 218 */
v0 = this.mTouchPositionTracker;
v0 = (( com.android.server.display.TouchCoverProtectionHelper$TouchPositionTracker ) v0 ).isTouchingInArea ( ); // invoke-virtual {v0}, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->isTouchingInArea()Z
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 219 */
/* .line 221 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* .line 222 */
/* .local v2, "now":J */
v0 = this.mTouchPositionTracker;
com.android.server.display.TouchCoverProtectionHelper$TouchPositionTracker .-$$Nest$fgetmLastObservedTouchTime ( v0 );
/* move-result-wide v4 */
/* sub-long v4, v2, v4 */
/* iget v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchEventDebounce:I */
/* mul-int/lit16 v0, v0, 0x3e8 */
/* int-to-long v6, v0 */
/* cmp-long v0, v4, v6 */
/* if-gez v0, :cond_2 */
/* .line 223 */
/* sget-boolean v0, Lcom/android/server/display/TouchCoverProtectionHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 224 */
v0 = com.android.server.display.TouchCoverProtectionHelper.TAG;
final String v4 = "Time of light sensor event is within given time of touch event."; // const-string v4, "Time of light sensor event is within given time of touch event."
android.util.Slog .d ( v0,v4 );
/* .line 226 */
} // :cond_1
/* .line 228 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void setUpLogicalDisplay ( com.android.server.display.LogicalDisplay p0 ) {
/* .locals 0 */
/* .param p1, "logicalDisplay" # Lcom/android/server/display/LogicalDisplay; */
/* .line 109 */
this.mLogicalDisplay = p1;
/* .line 110 */
/* invoke-direct {p0}, Lcom/android/server/display/TouchCoverProtectionHelper;->updateTouchCoverProtectionRect()V */
/* .line 111 */
return;
} // .end method
protected void showTouchCoverProtectionRect ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isShow" # Z */
/* .line 309 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 310 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/display/TouchCoverProtectionHelper$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/TouchCoverProtectionHelper$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/TouchCoverProtectionHelper;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 312 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/display/TouchCoverProtectionHelper$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/TouchCoverProtectionHelper$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/TouchCoverProtectionHelper;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 314 */
} // :goto_0
return;
} // .end method
protected void stop ( ) {
/* .locals 2 */
/* .line 125 */
/* iget-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchAreaEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 126 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/display/TouchCoverProtectionHelper;->setTouchTrackingEnabled(Z)V */
/* .line 127 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/TouchCoverProtectionHelper;->setDisplayListenerEnabled(Z)V */
/* .line 128 */
(( com.android.server.display.TouchCoverProtectionHelper ) p0 ).showTouchCoverProtectionRect ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/display/TouchCoverProtectionHelper;->showTouchCoverProtectionRect(Z)V
/* .line 130 */
} // :cond_0
v0 = this.mDeviceStateManager;
v1 = this.mFoldStateListener;
(( android.hardware.devicestate.DeviceStateManager ) v0 ).unregisterCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/devicestate/DeviceStateManager;->unregisterCallback(Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;)V
/* .line 131 */
return;
} // .end method
