.class Lcom/android/server/display/OutdoorDetector$MyHandler;
.super Landroid/os/Handler;
.source "OutdoorDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/OutdoorDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/OutdoorDetector;


# direct methods
.method public constructor <init>(Lcom/android/server/display/OutdoorDetector;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 164
    iput-object p1, p0, Lcom/android/server/display/OutdoorDetector$MyHandler;->this$0:Lcom/android/server/display/OutdoorDetector;

    .line 165
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 166
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .line 170
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 172
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/display/OutdoorDetector$MyHandler;->this$0:Lcom/android/server/display/OutdoorDetector;

    invoke-static {v0}, Lcom/android/server/display/OutdoorDetector;->-$$Nest$mupdateAmbientLux(Lcom/android/server/display/OutdoorDetector;)V

    .line 173
    nop

    .line 177
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
