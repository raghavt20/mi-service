.class public final Lcom/android/server/display/expertmode/ExpertData$Cookie;
.super Ljava/lang/Object;
.source "ExpertData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/expertmode/ExpertData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Cookie"
.end annotation


# static fields
.field public static final COLOR_B_COOKIE:I = 0x3

.field public static final COLOR_GAMUT_COOKIE:I = 0x0

.field public static final COLOR_G_COOKIE:I = 0x2

.field public static final COLOR_HUE_COOKIE:I = 0x4

.field public static final COLOR_R_COOKIE:I = 0x1

.field public static final COLOR_SATURATION_COOKIE:I = 0x5

.field public static final COLOR_VALUE_COOKIE:I = 0x6

.field public static final CONTRAST_RATIO_COOKIE:I = 0x7

.field public static final COOKIE_RESTORE:I = 0x9

.field public static final COOKIE_SET_LAST:I = 0xa

.field public static final GAMMA_COOKIE:I = 0x8


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
