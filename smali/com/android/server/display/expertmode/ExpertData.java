public class com.android.server.display.expertmode.ExpertData {
	 /* .source "ExpertData.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/expertmode/ExpertData$Cookie; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String COLOR_B;
public static final java.lang.String COLOR_G;
public static final java.lang.String COLOR_GAMUT;
public static final java.lang.String COLOR_HUE;
public static final java.lang.String COLOR_R;
public static final java.lang.String COLOR_SATURATION;
public static final java.lang.String COLOR_VALUE;
public static final java.lang.String CONTRAST_RATIO;
public static final Integer COOKIE_SET_NUM;
public static final Integer DEFAULT_EXPERT_COLOR_CONTRAST;
public static final Integer DEFAULT_EXPERT_COLOR_GAMMA;
public static final Integer DEFAULT_EXPERT_COLOR_GAMUT;
public static final Integer DEFAULT_EXPERT_COLOR_HUE;
public static final Integer DEFAULT_EXPERT_COLOR_RGB;
public static final Integer DEFAULT_EXPERT_COLOR_SATURATION;
public static final Integer DEFAULT_EXPERT_COLOR_VALUE;
public static final java.lang.String GAMMA;
public static final java.lang.String PROVIDER_KEY;
public static final Boolean SUPPORT_DISPLAY_EXPERT_MODE;
private static final java.lang.String TAG;
/* # instance fields */
private Integer colorB;
private Integer colorG;
private Integer colorGamut;
private Integer colorHue;
private Integer colorR;
private Integer colorSaturation;
private Integer colorValue;
private Integer contrastRatio;
private Integer gamma;
/* # direct methods */
static com.android.server.display.expertmode.ExpertData ( ) {
	 /* .locals 3 */
	 /* .line 27 */
	 /* const-string/jumbo v0, "support_display_expert_mode" */
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 miui.util.FeatureParser .getBoolean ( v0,v1 );
	 com.android.server.display.expertmode.ExpertData.SUPPORT_DISPLAY_EXPERT_MODE = (v0!= 0);
	 /* .line 31 */
	 final String v0 = "expert_gamut_default"; // const-string v0, "expert_gamut_default"
	 v0 = 	 miui.util.FeatureParser .getInteger ( v0,v1 );
	 /* .line 35 */
	 final String v0 = "expert_RGB_default"; // const-string v0, "expert_RGB_default"
	 /* const/16 v2, 0xff */
	 v0 = 	 miui.util.FeatureParser .getInteger ( v0,v2 );
	 /* .line 39 */
	 final String v0 = "expert_hue_default"; // const-string v0, "expert_hue_default"
	 v0 = 	 miui.util.FeatureParser .getInteger ( v0,v1 );
	 /* .line 43 */
	 final String v0 = "expert_saturation_default"; // const-string v0, "expert_saturation_default"
	 v0 = 	 miui.util.FeatureParser .getInteger ( v0,v1 );
	 /* .line 47 */
	 final String v0 = "expert_value_default"; // const-string v0, "expert_value_default"
	 v0 = 	 miui.util.FeatureParser .getInteger ( v0,v1 );
	 /* .line 51 */
	 final String v0 = "expert_contrast_default"; // const-string v0, "expert_contrast_default"
	 v0 = 	 miui.util.FeatureParser .getInteger ( v0,v1 );
	 /* .line 55 */
	 final String v0 = "expert_gamma_default"; // const-string v0, "expert_gamma_default"
	 /* const/16 v1, 0xdc */
	 v0 = 	 miui.util.FeatureParser .getInteger ( v0,v1 );
	 return;
} // .end method
public com.android.server.display.expertmode.ExpertData ( ) {
	 /* .locals 0 */
	 /* .param p1, "colorGamut" # I */
	 /* .param p2, "colorR" # I */
	 /* .param p3, "colorG" # I */
	 /* .param p4, "colorB" # I */
	 /* .param p5, "colorHue" # I */
	 /* .param p6, "colorSaturation" # I */
	 /* .param p7, "colorValue" # I */
	 /* .param p8, "contrastRatio" # I */
	 /* .param p9, "gamma" # I */
	 /* .line 160 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 161 */
	 /* iput p1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorGamut:I */
	 /* .line 162 */
	 /* iput p2, p0, Lcom/android/server/display/expertmode/ExpertData;->colorR:I */
	 /* .line 163 */
	 /* iput p3, p0, Lcom/android/server/display/expertmode/ExpertData;->colorG:I */
	 /* .line 164 */
	 /* iput p4, p0, Lcom/android/server/display/expertmode/ExpertData;->colorB:I */
	 /* .line 165 */
	 /* iput p5, p0, Lcom/android/server/display/expertmode/ExpertData;->colorHue:I */
	 /* .line 166 */
	 /* iput p6, p0, Lcom/android/server/display/expertmode/ExpertData;->colorSaturation:I */
	 /* .line 167 */
	 /* iput p7, p0, Lcom/android/server/display/expertmode/ExpertData;->colorValue:I */
	 /* .line 168 */
	 /* iput p8, p0, Lcom/android/server/display/expertmode/ExpertData;->contrastRatio:I */
	 /* .line 169 */
	 /* iput p9, p0, Lcom/android/server/display/expertmode/ExpertData;->gamma:I */
	 /* .line 170 */
	 return;
} // .end method
public static com.android.server.display.expertmode.ExpertData createFromJson ( org.json.JSONObject p0 ) {
	 /* .locals 11 */
	 /* .param p0, "data" # Lorg/json/JSONObject; */
	 /* .line 213 */
	 try { // :try_start_0
		 /* new-instance v10, Lcom/android/server/display/expertmode/ExpertData; */
		 final String v0 = "color_gamut"; // const-string v0, "color_gamut"
		 v1 = 		 (( org.json.JSONObject ) p0 ).getInt ( v0 ); // invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
		 final String v0 = "color_r"; // const-string v0, "color_r"
		 v2 = 		 (( org.json.JSONObject ) p0 ).getInt ( v0 ); // invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
		 final String v0 = "color_g"; // const-string v0, "color_g"
		 v3 = 		 (( org.json.JSONObject ) p0 ).getInt ( v0 ); // invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
		 final String v0 = "color_b"; // const-string v0, "color_b"
		 /* .line 214 */
		 v4 = 		 (( org.json.JSONObject ) p0 ).getInt ( v0 ); // invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
		 final String v0 = "color_hue"; // const-string v0, "color_hue"
		 v5 = 		 (( org.json.JSONObject ) p0 ).getInt ( v0 ); // invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
		 final String v0 = "color_saturation"; // const-string v0, "color_saturation"
		 v6 = 		 (( org.json.JSONObject ) p0 ).getInt ( v0 ); // invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
		 final String v0 = "color_value"; // const-string v0, "color_value"
		 /* .line 215 */
		 v7 = 		 (( org.json.JSONObject ) p0 ).getInt ( v0 ); // invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
		 final String v0 = "contrast_ratio"; // const-string v0, "contrast_ratio"
		 v8 = 		 (( org.json.JSONObject ) p0 ).getInt ( v0 ); // invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
		 final String v0 = "gamma"; // const-string v0, "gamma"
		 v9 = 		 (( org.json.JSONObject ) p0 ).getInt ( v0 ); // invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
		 /* move-object v0, v10 */
		 /* invoke-direct/range {v0 ..v9}, Lcom/android/server/display/expertmode/ExpertData;-><init>(IIIIIIIII)V */
		 /* :try_end_0 */
		 /* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 213 */
		 /* .line 216 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 217 */
		 /* .local v0, "e":Lorg/json/JSONException; */
		 final String v1 = "ExpertData"; // const-string v1, "ExpertData"
		 final String v2 = "createFromJson failed"; // const-string v2, "createFromJson failed"
		 android.util.Log .e ( v1,v2 );
		 /* .line 218 */
		 (( org.json.JSONException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
		 /* .line 220 */
	 } // .end local v0 # "e":Lorg/json/JSONException;
	 int v0 = 0; // const/4 v0, 0x0
} // .end method
public static com.android.server.display.expertmode.ExpertData getDefaultValue ( ) {
	 /* .locals 11 */
	 /* .line 59 */
	 /* new-instance v10, Lcom/android/server/display/expertmode/ExpertData; */
	 /* move-object v0, v10 */
	 /* move v2, v4 */
	 /* move v3, v4 */
	 /* invoke-direct/range {v0 ..v9}, Lcom/android/server/display/expertmode/ExpertData;-><init>(IIIIIIIII)V */
} // .end method
public static com.android.server.display.expertmode.ExpertData getFromDatabase ( android.content.Context p0 ) {
	 /* .locals 5 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .line 195 */
	 (( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 final String v1 = "expert_data"; // const-string v1, "expert_data"
	 int v2 = 0; // const/4 v2, 0x0
	 android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
	 /* .line 197 */
	 /* .local v0, "data":Ljava/lang/String; */
	 v1 = 	 android.text.TextUtils .isEmpty ( v0 );
	 int v2 = 0; // const/4 v2, 0x0
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 198 */
		 /* .line 202 */
	 } // :cond_0
	 try { // :try_start_0
		 /* new-instance v1, Lorg/json/JSONObject; */
		 /* invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
		 com.android.server.display.expertmode.ExpertData .createFromJson ( v1 );
		 /* :try_end_0 */
		 /* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 203 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 204 */
		 /* .local v1, "e":Lorg/json/JSONException; */
		 final String v3 = "ExpertData"; // const-string v3, "ExpertData"
		 final String v4 = "getFromDatabase failed"; // const-string v4, "getFromDatabase failed"
		 android.util.Log .e ( v3,v4 );
		 /* .line 205 */
		 (( org.json.JSONException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
		 /* .line 207 */
	 } // .end local v1 # "e":Lorg/json/JSONException;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
	 /* .locals 3 */
	 /* .param p1, "obj" # Ljava/lang/Object; */
	 /* .line 128 */
	 /* move-object v0, p1 */
	 /* check-cast v0, Lcom/android/server/display/expertmode/ExpertData; */
	 /* .line 129 */
	 /* .local v0, "data":Lcom/android/server/display/expertmode/ExpertData; */
	 /* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorGamut:I */
	 /* iget v2, v0, Lcom/android/server/display/expertmode/ExpertData;->colorGamut:I */
	 /* if-ne v1, v2, :cond_0 */
	 /* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorR:I */
	 /* iget v2, v0, Lcom/android/server/display/expertmode/ExpertData;->colorR:I */
	 /* if-ne v1, v2, :cond_0 */
	 /* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorG:I */
	 /* iget v2, v0, Lcom/android/server/display/expertmode/ExpertData;->colorG:I */
	 /* if-ne v1, v2, :cond_0 */
	 /* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorB:I */
	 /* iget v2, v0, Lcom/android/server/display/expertmode/ExpertData;->colorB:I */
	 /* if-ne v1, v2, :cond_0 */
	 /* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorHue:I */
	 /* iget v2, v0, Lcom/android/server/display/expertmode/ExpertData;->colorHue:I */
	 /* if-ne v1, v2, :cond_0 */
	 /* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorSaturation:I */
	 /* iget v2, v0, Lcom/android/server/display/expertmode/ExpertData;->colorSaturation:I */
	 /* if-ne v1, v2, :cond_0 */
	 /* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorValue:I */
	 /* iget v2, v0, Lcom/android/server/display/expertmode/ExpertData;->colorValue:I */
	 /* if-ne v1, v2, :cond_0 */
	 /* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->contrastRatio:I */
	 /* iget v2, v0, Lcom/android/server/display/expertmode/ExpertData;->contrastRatio:I */
	 /* if-ne v1, v2, :cond_0 */
	 /* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->gamma:I */
	 /* iget v2, v0, Lcom/android/server/display/expertmode/ExpertData;->gamma:I */
	 /* if-ne v1, v2, :cond_0 */
	 int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public Integer getByCookie ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "cookie" # I */
/* .line 100 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 120 */
final String v0 = "ExpertData"; // const-string v0, "ExpertData"
final String v1 = "getByCookie cookie illegal"; // const-string v1, "getByCookie cookie illegal"
android.util.Log .e ( v0,v1 );
/* .line 123 */
int v0 = 0; // const/4 v0, 0x0
/* .line 118 */
/* :pswitch_0 */
/* iget v0, p0, Lcom/android/server/display/expertmode/ExpertData;->gamma:I */
/* .line 116 */
/* :pswitch_1 */
/* iget v0, p0, Lcom/android/server/display/expertmode/ExpertData;->contrastRatio:I */
/* .line 114 */
/* :pswitch_2 */
/* iget v0, p0, Lcom/android/server/display/expertmode/ExpertData;->colorValue:I */
/* .line 112 */
/* :pswitch_3 */
/* iget v0, p0, Lcom/android/server/display/expertmode/ExpertData;->colorSaturation:I */
/* .line 110 */
/* :pswitch_4 */
/* iget v0, p0, Lcom/android/server/display/expertmode/ExpertData;->colorHue:I */
/* .line 108 */
/* :pswitch_5 */
/* iget v0, p0, Lcom/android/server/display/expertmode/ExpertData;->colorB:I */
/* .line 106 */
/* :pswitch_6 */
/* iget v0, p0, Lcom/android/server/display/expertmode/ExpertData;->colorG:I */
/* .line 104 */
/* :pswitch_7 */
/* iget v0, p0, Lcom/android/server/display/expertmode/ExpertData;->colorR:I */
/* .line 102 */
/* :pswitch_8 */
/* iget v0, p0, Lcom/android/server/display/expertmode/ExpertData;->colorGamut:I */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public org.json.JSONObject toJson ( ) {
/* .locals 3 */
/* .line 173 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 175 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
final String v1 = "color_gamut"; // const-string v1, "color_gamut"
/* iget v2, p0, Lcom/android/server/display/expertmode/ExpertData;->colorGamut:I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 177 */
final String v1 = "color_r"; // const-string v1, "color_r"
/* iget v2, p0, Lcom/android/server/display/expertmode/ExpertData;->colorR:I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 178 */
final String v1 = "color_g"; // const-string v1, "color_g"
/* iget v2, p0, Lcom/android/server/display/expertmode/ExpertData;->colorG:I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 179 */
final String v1 = "color_b"; // const-string v1, "color_b"
/* iget v2, p0, Lcom/android/server/display/expertmode/ExpertData;->colorB:I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 181 */
final String v1 = "color_hue"; // const-string v1, "color_hue"
/* iget v2, p0, Lcom/android/server/display/expertmode/ExpertData;->colorHue:I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 182 */
final String v1 = "color_saturation"; // const-string v1, "color_saturation"
/* iget v2, p0, Lcom/android/server/display/expertmode/ExpertData;->colorSaturation:I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 183 */
final String v1 = "color_value"; // const-string v1, "color_value"
/* iget v2, p0, Lcom/android/server/display/expertmode/ExpertData;->colorValue:I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 185 */
final String v1 = "contrast_ratio"; // const-string v1, "contrast_ratio"
/* iget v2, p0, Lcom/android/server/display/expertmode/ExpertData;->contrastRatio:I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 186 */
final String v1 = "gamma"; // const-string v1, "gamma"
/* iget v2, p0, Lcom/android/server/display/expertmode/ExpertData;->gamma:I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 189 */
/* .line 187 */
/* :catch_0 */
/* move-exception v1 */
/* .line 188 */
/* .local v1, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
/* .line 190 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_0
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 225 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ExpertData{colorGamut="; // const-string v1, "ExpertData{colorGamut="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorGamut:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", colorR="; // const-string v1, ", colorR="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorR:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", colorG="; // const-string v1, ", colorG="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorG:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", colorB="; // const-string v1, ", colorB="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorB:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", colorHue="; // const-string v1, ", colorHue="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorHue:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", colorSaturation="; // const-string v1, ", colorSaturation="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorSaturation:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", colorValue="; // const-string v1, ", colorValue="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->colorValue:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", contrastRatio="; // const-string v1, ", contrastRatio="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->contrastRatio:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", gamma="; // const-string v1, ", gamma="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/expertmode/ExpertData;->gamma:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
