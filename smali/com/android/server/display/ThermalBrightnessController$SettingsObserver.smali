.class final Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "ThermalBrightnessController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/ThermalBrightnessController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/ThermalBrightnessController;


# direct methods
.method constructor <init>(Lcom/android/server/display/ThermalBrightnessController;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 913
    iput-object p1, p0, Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;->this$0:Lcom/android/server/display/ThermalBrightnessController;

    .line 914
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 915
    return-void
.end method

.method private updateScreenBrightnessMode()V
    .locals 4

    .line 930
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;->this$0:Lcom/android/server/display/ThermalBrightnessController;

    invoke-static {v0}, Lcom/android/server/display/ThermalBrightnessController;->-$$Nest$fgetmContext(Lcom/android/server/display/ThermalBrightnessController;)Landroid/content/Context;

    move-result-object v0

    .line 931
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 930
    const-string v1, "screen_brightness_mode"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 933
    .local v0, "screenBrightnessModeSetting":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    move v1, v2

    .line 935
    .local v1, "useAutoBrightness":Z
    iget-object v2, p0, Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;->this$0:Lcom/android/server/display/ThermalBrightnessController;

    invoke-static {v2}, Lcom/android/server/display/ThermalBrightnessController;->-$$Nest$fgetmUseAutoBrightness(Lcom/android/server/display/ThermalBrightnessController;)Z

    move-result v2

    if-eq v1, v2, :cond_1

    .line 936
    iget-object v2, p0, Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;->this$0:Lcom/android/server/display/ThermalBrightnessController;

    invoke-static {v2, v1}, Lcom/android/server/display/ThermalBrightnessController;->-$$Nest$fputmUseAutoBrightness(Lcom/android/server/display/ThermalBrightnessController;Z)V

    .line 937
    iget-object v2, p0, Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;->this$0:Lcom/android/server/display/ThermalBrightnessController;

    invoke-static {v2}, Lcom/android/server/display/ThermalBrightnessController;->-$$Nest$fgetmBackgroundHandler(Lcom/android/server/display/ThermalBrightnessController;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;->this$0:Lcom/android/server/display/ThermalBrightnessController;

    invoke-static {v3}, Lcom/android/server/display/ThermalBrightnessController;->-$$Nest$fgetmUpdateOutdoorRunnable(Lcom/android/server/display/ThermalBrightnessController;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 938
    iget-object v2, p0, Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;->this$0:Lcom/android/server/display/ThermalBrightnessController;

    invoke-static {v2}, Lcom/android/server/display/ThermalBrightnessController;->-$$Nest$fgetmBackgroundHandler(Lcom/android/server/display/ThermalBrightnessController;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;->this$0:Lcom/android/server/display/ThermalBrightnessController;

    invoke-static {v3}, Lcom/android/server/display/ThermalBrightnessController;->-$$Nest$fgetmUpdateOutdoorRunnable(Lcom/android/server/display/ThermalBrightnessController;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 940
    :cond_1
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 919
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 920
    .local v0, "lastPathSegment":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    goto :goto_0

    :pswitch_0
    const-string v1, "screen_brightness_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_1

    goto :goto_2

    .line 922
    :pswitch_1
    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;->updateScreenBrightnessMode()V

    .line 923
    nop

    .line 927
    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch -0x294f7102
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
