.class final Lcom/android/server/display/SwipeUpWindow$AnimationState;
.super Ljava/lang/Object;
.source "SwipeUpWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/SwipeUpWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AnimationState"
.end annotation


# static fields
.field public static final STATE_LOCK:F = 1.0f

.field public static final STATE_UNLOCK:F = -1.0f

.field public static final STATE_WAKE:F


# instance fields
.field private perState:F

.field final synthetic this$0:Lcom/android/server/display/SwipeUpWindow;


# direct methods
.method public constructor <init>(Lcom/android/server/display/SwipeUpWindow;)V
    .locals 0

    .line 818
    iput-object p1, p0, Lcom/android/server/display/SwipeUpWindow$AnimationState;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 819
    const/high16 p1, 0x3f800000    # 1.0f

    iput p1, p0, Lcom/android/server/display/SwipeUpWindow$AnimationState;->perState:F

    .line 820
    return-void
.end method

.method public constructor <init>(Lcom/android/server/display/SwipeUpWindow;F)V
    .locals 0
    .param p2, "curState"    # F

    .line 822
    iput-object p1, p0, Lcom/android/server/display/SwipeUpWindow$AnimationState;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 823
    iput p2, p0, Lcom/android/server/display/SwipeUpWindow$AnimationState;->perState:F

    .line 824
    return-void
.end method


# virtual methods
.method public getCurrentState()F
    .locals 3

    .line 835
    const/4 v0, 0x0

    .line 836
    .local v0, "state":F
    iget v1, p0, Lcom/android/server/display/SwipeUpWindow$AnimationState;->perState:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_0

    .line 837
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 838
    :cond_0
    const/high16 v2, -0x40800000    # -1.0f

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_1

    .line 839
    const/high16 v0, -0x40800000    # -1.0f

    .line 841
    :cond_1
    :goto_0
    return v0
.end method

.method public getPerState()F
    .locals 1

    .line 827
    iget v0, p0, Lcom/android/server/display/SwipeUpWindow$AnimationState;->perState:F

    return v0
.end method

.method public setPerState(F)V
    .locals 0
    .param p1, "perState"    # F

    .line 831
    iput p1, p0, Lcom/android/server/display/SwipeUpWindow$AnimationState;->perState:F

    .line 832
    return-void
.end method
