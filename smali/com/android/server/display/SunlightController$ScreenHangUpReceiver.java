class com.android.server.display.SunlightController$ScreenHangUpReceiver extends android.content.BroadcastReceiver {
	 /* .source "SunlightController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/SunlightController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "ScreenHangUpReceiver" */
} // .end annotation
/* # instance fields */
final com.android.server.display.SunlightController this$0; //synthetic
/* # direct methods */
private com.android.server.display.SunlightController$ScreenHangUpReceiver ( ) {
/* .locals 0 */
/* .line 504 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
 com.android.server.display.SunlightController$ScreenHangUpReceiver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/SunlightController$ScreenHangUpReceiver;-><init>(Lcom/android/server/display/SunlightController;)V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 507 */
final String v0 = "hang_up_enable"; // const-string v0, "hang_up_enable"
int v1 = 0; // const/4 v1, 0x0
v0 = (( android.content.Intent ) p2 ).getBooleanExtra ( v0, v1 ); // invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
/* .line 508 */
/* .local v0, "screenIsHangUp":Z */
v1 = com.android.server.display.SunlightController .-$$Nest$sfgetDEBUG ( );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 509 */
	 final String v1 = "SunlightController"; // const-string v1, "SunlightController"
	 final String v2 = "Receive screen hang on broadcast."; // const-string v2, "Receive screen hang on broadcast."
	 android.util.Slog .d ( v1,v2 );
	 /* .line 511 */
} // :cond_0
v1 = this.this$0;
com.android.server.display.SunlightController .-$$Nest$fgetmHandler ( v1 );
int v2 = 3; // const/4 v2, 0x3
java.lang.Boolean .valueOf ( v0 );
(( com.android.server.display.SunlightController$SunlightModeHandler ) v1 ).obtainMessage ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/display/SunlightController$SunlightModeHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 512 */
/* .local v1, "msg":Landroid/os/Message; */
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 513 */
return;
} // .end method
