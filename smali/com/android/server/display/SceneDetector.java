public class com.android.server.display.SceneDetector {
	 /* .source "SceneDetector.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/SceneDetector$SceneDetectorHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_CLIENT_SERVICE;
private static final Integer AON_WAIT_DATA_STATE;
private static final Integer AON_WAIT_REGISTER_STATE;
private static final Float DEFAULT_FRAMES_PER_SECOND;
private static final Integer DEFAULT_MAX_DELAY_TIME;
private static final Integer MSG_CHECK_AON_DATA_RETURN;
private static final Integer MSG_UPDATE_BIND_AON_STATUS;
private static final Integer MSG_UPDATE_UNBIND_AON_STATUS;
private static final Integer SUPPRESS_DARKEN_EVENT;
private static final java.lang.String TAG;
private static final Integer TYPE_AON_FLARE;
private static Boolean sDEBUG;
/* # instance fields */
private Float mAmbientLux;
private Integer mAonFlareMaxDelayTime;
private com.xiaomi.aon.IAONFlareService mAonFlareService;
private Integer mAonState;
private Boolean mAutomaticBrightnessEnable;
private com.android.server.display.AutomaticBrightnessControllerImpl mBrightnessControllerImpl;
private android.content.Context mContext;
private android.os.IBinder$DeathRecipient mDeathRecipient;
private com.android.server.display.AutomaticBrightnessControllerStub$DualSensorPolicyListener mDualSensorPolicyListener;
private Boolean mIsMainDarkenEvent;
private android.os.Handler mMainHandler;
private Float mMaxAonFlareEnableLux;
private com.xiaomi.aon.IMiAONListener mMiAONListener;
private Float mMinAonFlareEnableLux;
private Float mPreAmbientLux;
private android.os.Handler mSceneDetectorHandler;
private final android.content.ComponentName mServiceComponent;
private Boolean mServiceConnected;
private android.content.ServiceConnection mServiceConnection;
/* # direct methods */
public static void $r8$lambda$h3y05PBjFEj-uL3MWDfuyyhV-og ( com.android.server.display.SceneDetector p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SceneDetector;->lambda$updateAutoBrightness$2()V */
	 return;
} // .end method
public static void $r8$lambda$inSCfs7rsK1izZETiUec96Nmay0 ( com.android.server.display.SceneDetector p0, Boolean p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/display/SceneDetector;->lambda$updateAmbientLux$0(Z)V */
	 return;
} // .end method
public static void $r8$lambda$yK3nBKl5psr65swE9Q3PcvYCogE ( com.android.server.display.SceneDetector p0, Float p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/display/SceneDetector;->lambda$updateAmbientLux$1(F)V */
	 return;
} // .end method
static Integer -$$Nest$fgetmAonState ( com.android.server.display.SceneDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/SceneDetector;->mAonState:I */
} // .end method
static com.android.server.display.AutomaticBrightnessControllerImpl -$$Nest$fgetmBrightnessControllerImpl ( com.android.server.display.SceneDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mBrightnessControllerImpl;
} // .end method
static android.os.IBinder$DeathRecipient -$$Nest$fgetmDeathRecipient ( com.android.server.display.SceneDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mDeathRecipient;
} // .end method
static Boolean -$$Nest$fgetmIsMainDarkenEvent ( com.android.server.display.SceneDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/display/SceneDetector;->mIsMainDarkenEvent:Z */
} // .end method
static Float -$$Nest$fgetmPreAmbientLux ( com.android.server.display.SceneDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/SceneDetector;->mPreAmbientLux:F */
} // .end method
static android.os.Handler -$$Nest$fgetmSceneDetectorHandler ( com.android.server.display.SceneDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mSceneDetectorHandler;
} // .end method
static void -$$Nest$fputmAonFlareService ( com.android.server.display.SceneDetector p0, com.xiaomi.aon.IAONFlareService p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mAonFlareService = p1;
	 return;
} // .end method
static void -$$Nest$fputmAonState ( com.android.server.display.SceneDetector p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/display/SceneDetector;->mAonState:I */
	 return;
} // .end method
static void -$$Nest$fputmIsMainDarkenEvent ( com.android.server.display.SceneDetector p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/display/SceneDetector;->mIsMainDarkenEvent:Z */
	 return;
} // .end method
static void -$$Nest$fputmServiceConnected ( com.android.server.display.SceneDetector p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/display/SceneDetector;->mServiceConnected:Z */
	 return;
} // .end method
static void -$$Nest$mresetServiceConnectedStatus ( com.android.server.display.SceneDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SceneDetector;->resetServiceConnectedStatus()V */
	 return;
} // .end method
static void -$$Nest$mtryToBindAonFlareService ( com.android.server.display.SceneDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SceneDetector;->tryToBindAonFlareService()V */
	 return;
} // .end method
static void -$$Nest$munregisterAonFlareListener ( com.android.server.display.SceneDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SceneDetector;->unregisterAonFlareListener()V */
	 return;
} // .end method
static void -$$Nest$mupdateAutoBrightness ( com.android.server.display.SceneDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SceneDetector;->updateAutoBrightness()V */
	 return;
} // .end method
static com.android.server.display.SceneDetector ( ) {
	 /* .locals 1 */
	 /* .line 45 */
	 int v0 = 0; // const/4 v0, 0x0
	 com.android.server.display.SceneDetector.sDEBUG = (v0!= 0);
	 return;
} // .end method
public com.android.server.display.SceneDetector ( ) {
	 /* .locals 3 */
	 /* .param p1, "listener" # Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener; */
	 /* .param p2, "brightnessControllerImpl" # Lcom/android/server/display/AutomaticBrightnessControllerImpl; */
	 /* .param p3, "handler" # Landroid/os/Handler; */
	 /* .param p4, "context" # Landroid/content/Context; */
	 /* .line 65 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 54 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput v0, p0, Lcom/android/server/display/SceneDetector;->mAonState:I */
	 /* .line 208 */
	 /* new-instance v0, Lcom/android/server/display/SceneDetector$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/SceneDetector$1;-><init>(Lcom/android/server/display/SceneDetector;)V */
	 this.mDeathRecipient = v0;
	 /* .line 230 */
	 /* new-instance v0, Lcom/android/server/display/SceneDetector$2; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/SceneDetector$2;-><init>(Lcom/android/server/display/SceneDetector;)V */
	 this.mServiceConnection = v0;
	 /* .line 258 */
	 /* new-instance v0, Lcom/android/server/display/SceneDetector$3; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/SceneDetector$3;-><init>(Lcom/android/server/display/SceneDetector;)V */
	 this.mMiAONListener = v0;
	 /* .line 66 */
	 this.mMainHandler = p3;
	 /* .line 67 */
	 this.mContext = p4;
	 /* .line 68 */
	 this.mDualSensorPolicyListener = p1;
	 /* .line 69 */
	 /* new-instance v0, Lcom/android/server/display/SceneDetector$SceneDetectorHandler; */
	 com.android.internal.os.BackgroundThread .get ( );
	 (( com.android.internal.os.BackgroundThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;-><init>(Lcom/android/server/display/SceneDetector;Landroid/os/Looper;)V */
	 this.mSceneDetectorHandler = v0;
	 /* .line 70 */
	 this.mBrightnessControllerImpl = p2;
	 /* .line 71 */
	 (( android.content.Context ) p4 ).getResources ( ); // invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
	 /* .line 72 */
	 /* .local v0, "resources":Landroid/content/res/Resources; */
	 /* const v1, 0x1107002d */
	 v1 = 	 (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
	 /* iput v1, p0, Lcom/android/server/display/SceneDetector;->mMaxAonFlareEnableLux:F */
	 /* .line 74 */
	 /* const v1, 0x11070031 */
	 v1 = 	 (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
	 /* iput v1, p0, Lcom/android/server/display/SceneDetector;->mMinAonFlareEnableLux:F */
	 /* .line 76 */
	 /* const v1, 0x110b000f */
	 v1 = 	 (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
	 /* iput v1, p0, Lcom/android/server/display/SceneDetector;->mAonFlareMaxDelayTime:I */
	 /* .line 78 */
	 /* const v1, 0x110f00a2 */
	 (( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
	 /* .line 80 */
	 /* .local v1, "componentName":Ljava/lang/String; */
	 /* invoke-direct {p0, v1}, Lcom/android/server/display/SceneDetector;->getAonFlareServiceComponent(Ljava/lang/String;)Landroid/content/ComponentName; */
	 this.mServiceComponent = v2;
	 /* .line 81 */
	 return;
} // .end method
private void checkAonFlareStatus ( Float p0 ) {
	 /* .locals 2 */
	 /* .param p1, "preLux" # F */
	 /* .line 150 */
	 /* sget-boolean v0, Lcom/android/server/display/SceneDetector;->sDEBUG:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 151 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "checkAonFlareStatus mIsMainDarkenEvent:"; // const-string v1, "checkAonFlareStatus mIsMainDarkenEvent:"
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-boolean v1, p0, Lcom/android/server/display/SceneDetector;->mIsMainDarkenEvent:Z */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 final String v1 = "mAonState: "; // const-string v1, "mAonState: "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/android/server/display/SceneDetector;->mAonState:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v1 = "SceneDetector"; // const-string v1, "SceneDetector"
		 android.util.Slog .i ( v1,v0 );
		 /* .line 154 */
	 } // :cond_0
	 /* iget v0, p0, Lcom/android/server/display/SceneDetector;->mAonState:I */
	 int v1 = 1; // const/4 v1, 0x1
	 /* if-ne v0, v1, :cond_2 */
	 /* .line 155 */
	 /* iput p1, p0, Lcom/android/server/display/SceneDetector;->mPreAmbientLux:F */
	 /* .line 156 */
	 /* iget-boolean v0, p0, Lcom/android/server/display/SceneDetector;->mServiceConnected:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 v0 = this.mAonFlareService;
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 157 */
			 /* invoke-direct {p0}, Lcom/android/server/display/SceneDetector;->registerAonFlareListener()V */
			 /* .line 159 */
		 } // :cond_1
		 /* invoke-direct {p0}, Lcom/android/server/display/SceneDetector;->updateAutoBrightness()V */
		 /* .line 162 */
	 } // :cond_2
} // :goto_0
return;
} // .end method
private android.content.ComponentName getAonFlareServiceComponent ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 134 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 135 */
	 android.content.ComponentName .unflattenFromString ( p1 );
	 /* .line 137 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void lambda$updateAmbientLux$0 ( Boolean p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "isMainDarkenEvent" # Z */
/* .line 94 */
v0 = this.mBrightnessControllerImpl;
(( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).notifyUpdateBrightness ( ); // invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->notifyUpdateBrightness()V
/* .line 95 */
/* iput-boolean p1, p0, Lcom/android/server/display/SceneDetector;->mIsMainDarkenEvent:Z */
/* .line 96 */
return;
} // .end method
private void lambda$updateAmbientLux$1 ( Float p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "preLux" # F */
/* .line 101 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/SceneDetector;->checkAonFlareStatus(F)V */
/* .line 102 */
return;
} // .end method
private void lambda$updateAutoBrightness$2 ( ) { //synthethic
/* .locals 4 */
/* .line 143 */
v0 = this.mDualSensorPolicyListener;
/* iget v2, p0, Lcom/android/server/display/SceneDetector;->mAmbientLux:F */
int v3 = 1; // const/4 v3, 0x1
/* .line 145 */
return;
} // .end method
private void registerAonFlareListener ( ) {
/* .locals 6 */
/* .line 167 */
final String v0 = "SceneDetector"; // const-string v0, "SceneDetector"
try { // :try_start_0
	 v1 = this.mAonFlareService;
	 v2 = this.mMiAONListener;
	 int v3 = 5; // const/4 v3, 0x5
	 /* const/high16 v4, 0x3f800000 # 1.0f */
	 /* const/16 v5, 0x1388 */
	 /* .line 169 */
	 v1 = this.mSceneDetectorHandler;
	 /* iget v2, p0, Lcom/android/server/display/SceneDetector;->mAonFlareMaxDelayTime:I */
	 /* int-to-long v2, v2 */
	 int v4 = 1; // const/4 v4, 0x1
	 (( android.os.Handler ) v1 ).sendEmptyMessageDelayed ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
	 /* .line 171 */
	 int v1 = 2; // const/4 v1, 0x2
	 /* iput v1, p0, Lcom/android/server/display/SceneDetector;->mAonState:I */
	 /* .line 172 */
	 final String v1 = "registerAonFlareListener: register aon flare listener success."; // const-string v1, "registerAonFlareListener: register aon flare listener success."
	 android.util.Slog .i ( v0,v1 );
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 175 */
	 /* .line 173 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 174 */
	 /* .local v1, "e":Landroid/os/RemoteException; */
	 final String v2 = "registerAonFlareListener: register aon flare listener failed."; // const-string v2, "registerAonFlareListener: register aon flare listener failed."
	 android.util.Slog .e ( v0,v2 );
	 /* .line 176 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
private void resetServiceConnectedStatus ( ) {
/* .locals 3 */
/* .line 222 */
v0 = this.mAonFlareService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v1, p0, Lcom/android/server/display/SceneDetector;->mServiceConnected:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 223 */
	 v1 = this.mDeathRecipient;
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 224 */
	 v0 = this.mContext;
	 v1 = this.mServiceConnection;
	 (( android.content.Context ) v0 ).unbindService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
	 /* .line 225 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mAonFlareService = v0;
	 /* .line 226 */
	 /* iput-boolean v2, p0, Lcom/android/server/display/SceneDetector;->mServiceConnected:Z */
	 /* .line 228 */
} // :cond_0
return;
} // .end method
private void tryToBindAonFlareService ( ) {
/* .locals 6 */
/* .line 109 */
v0 = this.mServiceComponent;
/* if-nez v0, :cond_0 */
/* .line 111 */
return;
/* .line 113 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/SceneDetector;->mServiceConnected:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mAonFlareService;
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 115 */
	 return;
	 /* .line 117 */
} // :cond_1
v0 = android.app.ActivityManager .getCurrentUser ( );
/* .line 118 */
/* .local v0, "userId":I */
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "com.xiaomi.aon.AonFlareService"; // const-string v2, "com.xiaomi.aon.AonFlareService"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 119 */
/* .local v1, "intent":Landroid/content/Intent; */
v2 = this.mServiceComponent;
(( android.content.Intent ) v1 ).setComponent ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 120 */
/* const/high16 v2, 0x800000 */
(( android.content.Intent ) v1 ).addFlags ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 121 */
v2 = this.mContext;
v3 = this.mServiceConnection;
/* new-instance v4, Landroid/os/UserHandle; */
/* invoke-direct {v4, v0}, Landroid/os/UserHandle;-><init>(I)V */
int v5 = 1; // const/4 v5, 0x1
v2 = (( android.content.Context ) v2 ).bindServiceAsUser ( v1, v3, v5, v4 ); // invoke-virtual {v2, v1, v3, v5, v4}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z
/* if-nez v2, :cond_2 */
/* .line 123 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "unable to bind service: bindService failed " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "SceneDetector"; // const-string v3, "SceneDetector"
android.util.Slog .e ( v3,v2 );
/* .line 125 */
} // :cond_2
v2 = this.mBrightnessControllerImpl;
(( com.android.server.display.AutomaticBrightnessControllerImpl ) v2 ).notifyUpdateBrightness ( ); // invoke-virtual {v2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->notifyUpdateBrightness()V
/* .line 126 */
return;
} // .end method
private void unregisterAonFlareListener ( ) {
/* .locals 4 */
/* .line 199 */
final String v0 = "SceneDetector"; // const-string v0, "SceneDetector"
try { // :try_start_0
v1 = this.mAonFlareService;
v2 = this.mMiAONListener;
int v3 = 5; // const/4 v3, 0x5
/* .line 200 */
v1 = this.mSceneDetectorHandler;
int v2 = 1; // const/4 v2, 0x1
(( android.os.Handler ) v1 ).removeMessages ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 201 */
/* iput v2, p0, Lcom/android/server/display/SceneDetector;->mAonState:I */
/* .line 202 */
/* const-string/jumbo v1, "unregisterAonFlareListener: unregister aon flare listener success." */
android.util.Slog .i ( v0,v1 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 205 */
/* .line 203 */
/* :catch_0 */
/* move-exception v1 */
/* .line 204 */
/* .local v1, "e":Landroid/os/RemoteException; */
/* const-string/jumbo v2, "unregisterAonFlareListener: unregister aon flare listener failed." */
android.util.Slog .e ( v0,v2 );
/* .line 206 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
private void updateAutoBrightness ( ) {
/* .locals 2 */
/* .line 142 */
v0 = this.mMainHandler;
/* new-instance v1, Lcom/android/server/display/SceneDetector$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/SceneDetector$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/SceneDetector;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 146 */
return;
} // .end method
/* # virtual methods */
public void configure ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 179 */
int v0 = 3; // const/4 v0, 0x3
int v1 = 2; // const/4 v1, 0x2
/* if-nez p1, :cond_0 */
/* iget-boolean v2, p0, Lcom/android/server/display/SceneDetector;->mAutomaticBrightnessEnable:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 180 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/display/SceneDetector;->mAutomaticBrightnessEnable:Z */
/* .line 182 */
v2 = this.mSceneDetectorHandler;
(( android.os.Handler ) v2 ).removeMessages ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 183 */
v1 = this.mSceneDetectorHandler;
(( android.os.Handler ) v1 ).removeMessages ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V
/* .line 184 */
v1 = this.mSceneDetectorHandler;
(( android.os.Handler ) v1 ).obtainMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 185 */
/* .local v0, "msg":Landroid/os/Message; */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
} // .end local v0 # "msg":Landroid/os/Message;
/* .line 186 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* iget-boolean v2, p0, Lcom/android/server/display/SceneDetector;->mAutomaticBrightnessEnable:Z */
/* if-nez v2, :cond_1 */
/* .line 187 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/display/SceneDetector;->mAutomaticBrightnessEnable:Z */
/* .line 189 */
v2 = this.mSceneDetectorHandler;
(( android.os.Handler ) v2 ).removeMessages ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 190 */
v2 = this.mSceneDetectorHandler;
(( android.os.Handler ) v2 ).removeMessages ( v0 ); // invoke-virtual {v2, v0}, Landroid/os/Handler;->removeMessages(I)V
/* .line 191 */
v0 = this.mSceneDetectorHandler;
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 192 */
/* .restart local v0 # "msg":Landroid/os/Message; */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 186 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_1
} // :goto_0
/* nop */
/* .line 194 */
} // :goto_1
return;
} // .end method
protected void dump ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 324 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 325 */
final String v0 = "Scene Detector State:"; // const-string v0, "Scene Detector State:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 326 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mMaxAonFlareEnableLux="; // const-string v1, " mMaxAonFlareEnableLux="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/SceneDetector;->mMaxAonFlareEnableLux:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 327 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mMinAonFlareEnableLux="; // const-string v1, " mMinAonFlareEnableLux="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/SceneDetector;->mMinAonFlareEnableLux:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 328 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAonFlareMaxDelayTime="; // const-string v1, " mAonFlareMaxDelayTime="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/SceneDetector;->mAonFlareMaxDelayTime:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 329 */
/* sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_ABC:Z */
com.android.server.display.SceneDetector.sDEBUG = (v0!= 0);
/* .line 330 */
return;
} // .end method
public void updateAmbientLux ( Integer p0, Float p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "event" # I */
/* .param p2, "lux" # F */
/* .param p3, "isMainDarkenEvent" # Z */
/* .line 90 */
v0 = v0 = this.mDualSensorPolicyListener;
/* .line 91 */
/* .local v0, "preLux":F */
/* iput p2, p0, Lcom/android/server/display/SceneDetector;->mAmbientLux:F */
/* .line 93 */
v1 = this.mSceneDetectorHandler;
/* new-instance v2, Lcom/android/server/display/SceneDetector$$ExternalSyntheticLambda1; */
/* invoke-direct {v2, p0, p3}, Lcom/android/server/display/SceneDetector$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/SceneDetector;Z)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 97 */
int v1 = 1; // const/4 v1, 0x1
if ( p3 != null) { // if-eqz p3, :cond_0
/* iget v2, p0, Lcom/android/server/display/SceneDetector;->mMaxAonFlareEnableLux:F */
/* cmpg-float v2, p2, v2 */
/* if-gtz v2, :cond_0 */
/* iget v2, p0, Lcom/android/server/display/SceneDetector;->mMinAonFlareEnableLux:F */
/* cmpl-float v2, p2, v2 */
/* if-ltz v2, :cond_0 */
/* .line 98 */
v2 = this.mDualSensorPolicyListener;
int v3 = 0; // const/4 v3, 0x0
/* .line 100 */
v1 = this.mSceneDetectorHandler;
/* new-instance v2, Lcom/android/server/display/SceneDetector$$ExternalSyntheticLambda2; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/display/SceneDetector$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/SceneDetector;F)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 104 */
} // :cond_0
v2 = this.mDualSensorPolicyListener;
/* .line 106 */
} // :goto_0
return;
} // .end method
