.class Lcom/android/server/display/DisplayFeatureManagerService$LocalService;
.super Lcom/android/server/display/DisplayFeatureManagerInternal;
.source "DisplayFeatureManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/DisplayFeatureManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/DisplayFeatureManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/DisplayFeatureManagerService;

    .line 1490
    iput-object p1, p0, Lcom/android/server/display/DisplayFeatureManagerService$LocalService;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerInternal;-><init>()V

    return-void
.end method


# virtual methods
.method public setVideoInformation(IZFIIFLandroid/os/IBinder;)V
    .locals 8
    .param p1, "pid"    # I
    .param p2, "bulletChatStatus"    # Z
    .param p3, "frameRate"    # F
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "compressionRatio"    # F
    .param p7, "token"    # Landroid/os/IBinder;

    .line 1517
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$LocalService;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object v7, p7

    invoke-static/range {v0 .. v7}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mupdateVideoInformationIfNeeded(Lcom/android/server/display/DisplayFeatureManagerService;IZFIIFLandroid/os/IBinder;)V

    .line 1519
    return-void
.end method

.method public updateBCBCState(I)V
    .locals 2
    .param p1, "state"    # I

    .line 1511
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$LocalService;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    const/16 v1, 0x12

    invoke-static {v0, v1, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetScreenEffect(Lcom/android/server/display/DisplayFeatureManagerService;II)V

    .line 1512
    return-void
.end method

.method public updateDozeBrightness(JI)V
    .locals 1
    .param p1, "physicalDisplayId"    # J
    .param p3, "brightness"    # I

    .line 1506
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$LocalService;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/DisplayFeatureManagerService;->setDozeBrightness(JI)V

    .line 1507
    return-void
.end method

.method public updateRhythmicAppCategoryList(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1523
    .local p1, "imageAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "readAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$LocalService;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmRhythmicEyeCareManager(Lcom/android/server/display/DisplayFeatureManagerService;)Lcom/android/server/display/RhythmicEyeCareManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/RhythmicEyeCareManager;->updateRhythmicAppCategoryList(Ljava/util/List;Ljava/util/List;)V

    .line 1524
    return-void
.end method

.method public updateScreenEffect(I)V
    .locals 3
    .param p1, "state"    # I

    .line 1493
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$LocalService;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmDisplayState(Lcom/android/server/display/DisplayFeatureManagerService;)I

    move-result v0

    .line 1494
    .local v0, "oldState":I
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$LocalService;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fputmDisplayState(Lcom/android/server/display/DisplayFeatureManagerService;I)V

    .line 1495
    sget-boolean v1, Lmiui/os/DeviceFeature;->PERSIST_SCREEN_EFFECT:Z

    if-nez v1, :cond_1

    .line 1496
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    if-eq p1, v0, :cond_0

    .line 1498
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$LocalService;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetScreenEffectColor(Lcom/android/server/display/DisplayFeatureManagerService;Z)V

    .line 1500
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$LocalService;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmRhythmicEyeCareManager(Lcom/android/server/display/DisplayFeatureManagerService;)Lcom/android/server/display/RhythmicEyeCareManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/server/display/RhythmicEyeCareManager;->notifyScreenStateChanged(I)V

    .line 1502
    :cond_1
    return-void
.end method

.method public updateScreenGrayscaleState(I)V
    .locals 2
    .param p1, "state"    # I

    .line 1528
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$LocalService;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    const/16 v1, 0x38

    invoke-static {v0, v1, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetScreenEffect(Lcom/android/server/display/DisplayFeatureManagerService;II)V

    .line 1529
    return-void
.end method
