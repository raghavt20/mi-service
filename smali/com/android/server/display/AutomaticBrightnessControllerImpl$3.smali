.class Lcom/android/server/display/AutomaticBrightnessControllerImpl$3;
.super Landroid/util/SparseArray;
.source "AutomaticBrightnessControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/display/AutomaticBrightnessControllerImpl;->fillInLuxFromDaemonSensor()Landroid/util/SparseArray;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/SparseArray<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;


# direct methods
.method constructor <init>(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    .line 606
    iput-object p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$3;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-direct {p0}, Landroid/util/SparseArray;-><init>()V

    .line 608
    sget v0, Lcom/android/server/display/AutomaticBrightnessControllerStub;->HANDLE_MAIN_LUX_EVENT:I

    const/high16 v1, 0x7fc00000    # Float.NaN

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$3;->put(ILjava/lang/Object;)V

    .line 609
    sget v0, Lcom/android/server/display/AutomaticBrightnessControllerStub;->HANDLE_ASSIST_LUX_EVENT:I

    invoke-virtual {p0, v0, v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$3;->put(ILjava/lang/Object;)V

    .line 610
    return-void
.end method
