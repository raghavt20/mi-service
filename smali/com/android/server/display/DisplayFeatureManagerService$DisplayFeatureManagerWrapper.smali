.class Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;
.super Ljava/lang/Object;
.source "DisplayFeatureManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/DisplayFeatureManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DisplayFeatureManagerWrapper"
.end annotation


# static fields
.field private static final AIDL_TRANSACTION_registerCallback:I = 0x2

.field private static final AIDL_TRANSACTION_setFeature:I = 0x7

.field private static final HIDL_TRANSACTION_interfaceDescriptor:I = 0xf445343

.field private static final HIDL_TRANSACTION_registerCallback:I = 0x2

.field private static final HIDL_TRANSACTION_setFeature:I = 0x1

.field private static final HWBINDER_BASE_INTERFACE_DESCRIPTOR:Ljava/lang/String; = "android.hidl.base@1.0::IBase"

.field private static final HWBINDER_INTERFACE_DESCRIPTOR:Ljava/lang/String; = "vendor.xiaomi.hardware.displayfeature@1.0::IDisplayFeature"

.field private static final INTERFACE_DESCRIPTOR:Ljava/lang/String; = "vendor.xiaomi.hardware.displayfeature_aidl.IDisplayFeature"


# instance fields
.field private mDescriptor:Ljava/lang/String;

.field private mHwService:Landroid/os/IHwBinder;

.field private mService:Landroid/os/IBinder;

.field final synthetic this$0:Lcom/android/server/display/DisplayFeatureManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/display/DisplayFeatureManagerService;Ljava/lang/Object;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/display/DisplayFeatureManagerService;
    .param p2, "service"    # Ljava/lang/Object;

    .line 1620
    iput-object p1, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1621
    instance-of v0, p2, Landroid/os/IBinder;

    if-eqz v0, :cond_0

    .line 1622
    move-object v0, p2

    check-cast v0, Landroid/os/IBinder;

    iput-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->mService:Landroid/os/IBinder;

    .line 1624
    :try_start_0
    invoke-interface {v0}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->mDescriptor:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1626
    goto :goto_0

    .line 1625
    :catch_0
    move-exception v0

    .line 1627
    :goto_0
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->mDescriptor:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1628
    const-string/jumbo v0, "vendor.xiaomi.hardware.displayfeature_aidl.IDisplayFeature"

    iput-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->mDescriptor:Ljava/lang/String;

    goto :goto_2

    .line 1630
    :cond_0
    instance-of v0, p2, Landroid/os/IHwBinder;

    if-eqz v0, :cond_1

    .line 1631
    move-object v0, p2

    check-cast v0, Landroid/os/IHwBinder;

    iput-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->mHwService:Landroid/os/IHwBinder;

    .line 1633
    :try_start_1
    invoke-virtual {p0}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->interfaceDescriptor()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->mDescriptor:Ljava/lang/String;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1635
    goto :goto_1

    .line 1634
    :catch_1
    move-exception v0

    .line 1636
    :goto_1
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->mDescriptor:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1637
    const-string/jumbo v0, "vendor.xiaomi.hardware.displayfeature@1.0::IDisplayFeature"

    iput-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->mDescriptor:Ljava/lang/String;

    .line 1640
    :cond_1
    :goto_2
    return-void
.end method

.method private varargs callBinderTransact(II[Ljava/lang/Object;)V
    .locals 6
    .param p1, "transactId"    # I
    .param p2, "flag"    # I
    .param p3, "params"    # [Ljava/lang/Object;

    .line 1678
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 1679
    .local v0, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 1681
    .local v1, "reply":Landroid/os/Parcel;
    :try_start_0
    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->mDescriptor:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 1682
    array-length v2, p3

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, p3, v3

    .line 1683
    .local v4, "param":Ljava/lang/Object;
    instance-of v5, v4, Ljava/lang/Integer;

    if-eqz v5, :cond_0

    .line 1684
    move-object v5, v4

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 1685
    :cond_0
    instance-of v5, v4, Landroid/os/IInterface;

    if-eqz v5, :cond_1

    .line 1686
    move-object v5, v4

    check-cast v5, Landroid/os/IInterface;

    invoke-interface {v5}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 1682
    .end local v4    # "param":Ljava/lang/Object;
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1689
    :cond_2
    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->mService:Landroid/os/IBinder;

    if-eqz v2, :cond_3

    .line 1690
    invoke-interface {v2, p1, v0, v1, p2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1695
    :cond_3
    nop

    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1696
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 1697
    goto :goto_3

    .line 1695
    :catchall_0
    move-exception v2

    goto :goto_4

    .line 1692
    :catch_0
    move-exception v2

    .line 1693
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v3, "DisplayFeatureManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "callBinderTransact transact fail. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1695
    nop

    .end local v2    # "e":Landroid/os/RemoteException;
    goto :goto_2

    .line 1698
    :goto_3
    return-void

    .line 1695
    :goto_4
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1696
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 1697
    throw v2
.end method

.method private varargs callHwBinderTransact(II[Ljava/lang/Object;)V
    .locals 6
    .param p1, "_hidl_code"    # I
    .param p2, "flag"    # I
    .param p3, "params"    # [Ljava/lang/Object;

    .line 1701
    new-instance v0, Landroid/os/HwParcel;

    invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V

    .line 1703
    .local v0, "hidl_reply":Landroid/os/HwParcel;
    :try_start_0
    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    .line 1704
    .local v1, "hidl_request":Landroid/os/HwParcel;
    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->mDescriptor:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 1705
    array-length v2, p3

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, p3, v3

    .line 1706
    .local v4, "param":Ljava/lang/Object;
    instance-of v5, v4, Ljava/lang/Integer;

    if-eqz v5, :cond_0

    .line 1707
    move-object v5, v4

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/os/HwParcel;->writeInt32(I)V

    goto :goto_1

    .line 1708
    :cond_0
    instance-of v5, v4, Landroid/os/IHwInterface;

    if-eqz v5, :cond_1

    .line 1709
    move-object v5, v4

    check-cast v5, Landroid/os/IHwInterface;

    invoke-interface {v5}, Landroid/os/IHwInterface;->asBinder()Landroid/os/IHwBinder;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/os/HwParcel;->writeStrongBinder(Landroid/os/IHwBinder;)V

    .line 1705
    .end local v4    # "param":Ljava/lang/Object;
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1712
    :cond_2
    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->mHwService:Landroid/os/IHwBinder;

    invoke-interface {v2, p1, v1, v0, p2}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    .line 1713
    invoke-virtual {v0}, Landroid/os/HwParcel;->verifySuccess()V

    .line 1714
    invoke-virtual {v1}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1718
    .end local v1    # "hidl_request":Landroid/os/HwParcel;
    goto :goto_2

    :catchall_0
    move-exception v1

    goto :goto_3

    .line 1715
    :catch_0
    move-exception v1

    .line 1716
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v2, "DisplayFeatureManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "callHwBinderTransact transact fail. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1718
    nop

    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_2
    invoke-virtual {v0}, Landroid/os/HwParcel;->release()V

    .line 1719
    nop

    .line 1720
    return-void

    .line 1718
    :goto_3
    invoke-virtual {v0}, Landroid/os/HwParcel;->release()V

    .line 1719
    throw v1
.end method


# virtual methods
.method public interfaceDescriptor()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1660
    new-instance v0, Landroid/os/HwParcel;

    invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V

    .line 1661
    .local v0, "_hidl_request":Landroid/os/HwParcel;
    const-string v1, "android.hidl.base@1.0::IBase"

    invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 1663
    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    .line 1665
    .local v1, "_hidl_reply":Landroid/os/HwParcel;
    :try_start_0
    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->mHwService:Landroid/os/IHwBinder;

    const v3, 0xf445343

    const/4 v4, 0x0

    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    .line 1667
    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    .line 1668
    invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    .line 1670
    invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1671
    .local v2, "_hidl_out_descriptor":Ljava/lang/String;
    nop

    .line 1673
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 1671
    return-object v2

    .line 1673
    .end local v2    # "_hidl_out_descriptor":Ljava/lang/String;
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 1674
    throw v2
.end method

.method registerCallback(ILjava/lang/Object;)V
    .locals 3
    .param p1, "displayId"    # I
    .param p2, "callback"    # Ljava/lang/Object;

    .line 1651
    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_DISPLAYFEATURE_HIDL:Z

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-eqz v0, :cond_0

    .line 1652
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    filled-new-array {v0, p2}, [Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v2, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->callHwBinderTransact(II[Ljava/lang/Object;)V

    goto :goto_0

    .line 1654
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    filled-new-array {v0, p2}, [Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v2, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->callBinderTransact(II[Ljava/lang/Object;)V

    .line 1656
    :goto_0
    return-void
.end method

.method setFeature(IIII)V
    .locals 5
    .param p1, "displayId"    # I
    .param p2, "mode"    # I
    .param p3, "value"    # I
    .param p4, "cookie"    # I

    .line 1643
    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_DISPLAYFEATURE_HIDL:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 1644
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    filled-new-array {v0, v2, v3, v4}, [Ljava/lang/Object;

    move-result-object v0

    const/4 v2, 0x1

    invoke-direct {p0, v2, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->callHwBinderTransact(II[Ljava/lang/Object;)V

    goto :goto_0

    .line 1646
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    filled-new-array {v0, v2, v3, v4}, [Ljava/lang/Object;

    move-result-object v0

    const/4 v2, 0x7

    invoke-direct {p0, v2, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->callBinderTransact(II[Ljava/lang/Object;)V

    .line 1648
    :goto_0
    return-void
.end method
