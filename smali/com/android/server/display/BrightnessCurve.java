public class com.android.server.display.BrightnessCurve {
	 /* .source "BrightnessCurve.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/BrightnessCurve$Curve;, */
	 /* Lcom/android/server/display/BrightnessCurve$DarkLightCurve;, */
	 /* Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;, */
	 /* Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;, */
	 /* Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;, */
	 /* Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer DARK_LIGHT_CURVE_HEAD_LUX_INDEX;
private static final Float DARK_LIGHT_CURVE_RADIO;
private static final Integer DARK_LIGHT_CURVE_TAIL_LUX_INDEX;
private static final Integer FIRST_OUTDOOR_LIGHT_CURVE_TAIL_INDEX;
private static final Integer INDOOR_LIGHT_CURVE_TAIL_LUX_INDEX;
private static final Integer SECOND_OUTDOOR_LIGHT_CURVE_TAIL_INDEX;
private static final java.lang.String TAG;
/* # instance fields */
private mBrightness;
private final Boolean mBrightnessCurveAlgoAvailable;
private final Float mBrightnessMinTan;
private android.util.Spline mBrightnessToNit;
private android.hardware.display.BrightnessConfiguration mConfig;
private mCurrentCurveInterval;
private final Float mCurveAnchorPointLux;
private final mCustomBrighteningCurveInterval;
private final mCustomDarkeningCurveInterval;
private final Float mDarkLightCurvePullUpLimitNit;
private final Float mDarkLightCurvePullUpMaxTan;
private android.hardware.display.BrightnessConfiguration mDefault;
private final mDefaultCurveInterval;
private final Float mIndoorLightCurveTan;
private mLux;
private android.util.Spline mLuxToNit;
private android.util.Spline mLuxToNitsDefault;
private Float mMaxLux;
private Float mMinNit;
private android.util.Spline mNitToBrightness;
private final Float mSecondOutdoorCurveBrightenMinTan;
private final Float mThirdOutdoorCurveBrightenMinTan;
/* # direct methods */
static Float -$$Nest$fgetmBrightnessMinTan ( com.android.server.display.BrightnessCurve p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mBrightnessMinTan:F */
} // .end method
static -$$Nest$fgetmCurrentCurveInterval ( com.android.server.display.BrightnessCurve p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mCurrentCurveInterval;
} // .end method
static Float -$$Nest$fgetmCurveAnchorPointLux ( com.android.server.display.BrightnessCurve p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mCurveAnchorPointLux:F */
} // .end method
static Float -$$Nest$fgetmDarkLightCurvePullUpLimitNit ( com.android.server.display.BrightnessCurve p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mDarkLightCurvePullUpLimitNit:F */
} // .end method
static Float -$$Nest$fgetmDarkLightCurvePullUpMaxTan ( com.android.server.display.BrightnessCurve p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mDarkLightCurvePullUpMaxTan:F */
} // .end method
static Float -$$Nest$fgetmIndoorLightCurveTan ( com.android.server.display.BrightnessCurve p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mIndoorLightCurveTan:F */
} // .end method
static android.util.Spline -$$Nest$fgetmLuxToNitsDefault ( com.android.server.display.BrightnessCurve p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mLuxToNitsDefault;
} // .end method
static Float -$$Nest$fgetmMaxLux ( com.android.server.display.BrightnessCurve p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mMaxLux:F */
} // .end method
static Float -$$Nest$fgetmSecondOutdoorCurveBrightenMinTan ( com.android.server.display.BrightnessCurve p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mSecondOutdoorCurveBrightenMinTan:F */
} // .end method
static Float -$$Nest$fgetmThirdOutdoorCurveBrightenMinTan ( com.android.server.display.BrightnessCurve p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mThirdOutdoorCurveBrightenMinTan:F */
} // .end method
static void -$$Nest$mcopyToDefaultSpline ( com.android.server.display.BrightnessCurve p0, Float p1, Float p2, java.util.List p3, Float p4, Float p5 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct/range {p0 ..p5}, Lcom/android/server/display/BrightnessCurve;->copyToDefaultSpline(FFLjava/util/List;FF)V */
	 return;
} // .end method
static void -$$Nest$mpullDownCurveCreate ( com.android.server.display.BrightnessCurve p0, Float p1, Float p2, java.util.List p3, Float p4, Float p5, Float p6, Float p7 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct/range {p0 ..p7}, Lcom/android/server/display/BrightnessCurve;->pullDownCurveCreate(FFLjava/util/List;FFFF)V */
	 return;
} // .end method
static void -$$Nest$mpullUpConnectTail ( com.android.server.display.BrightnessCurve p0, android.util.Pair p1, Float p2, Float p3, java.util.List p4 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/display/BrightnessCurve;->pullUpConnectTail(Landroid/util/Pair;FFLjava/util/List;)V */
	 return;
} // .end method
static void -$$Nest$mpullUpCurveCreate ( com.android.server.display.BrightnessCurve p0, Float p1, Float p2, java.util.List p3, Float p4, Float p5, Float p6, Float p7 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct/range {p0 ..p7}, Lcom/android/server/display/BrightnessCurve;->pullUpCurveCreate(FFLjava/util/List;FFFF)V */
	 return;
} // .end method
public com.android.server.display.BrightnessCurve ( ) {
	 /* .locals 3 */
	 /* .param p1, "defaultConfig" # Landroid/hardware/display/BrightnessConfiguration; */
	 /* .param p2, "nitsToBrightnessSpline" # Landroid/util/Spline; */
	 /* .param p3, "brightnessToNitsSpline" # Landroid/util/Spline; */
	 /* .line 54 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 55 */
	 this.mConfig = p1;
	 /* .line 56 */
	 this.mDefault = p1;
	 /* .line 57 */
	 this.mNitToBrightness = p2;
	 /* .line 58 */
	 this.mBrightnessToNit = p3;
	 /* .line 59 */
	 android.content.res.Resources .getSystem ( );
	 /* .line 60 */
	 /* .local v0, "resources":Landroid/content/res/Resources; */
	 /* nop */
	 /* .line 61 */
	 /* const v1, 0x1103003c */
	 (( android.content.res.Resources ) v0 ).obtainTypedArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
	 /* .line 60 */
	 com.android.server.display.BrightnessMappingStrategy .getFloatArray ( v1 );
	 this.mDefaultCurveInterval = v1;
	 /* .line 62 */
	 /* nop */
	 /* .line 63 */
	 /* const v2, 0x11030037 */
	 (( android.content.res.Resources ) v0 ).obtainTypedArray ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
	 /* .line 62 */
	 com.android.server.display.BrightnessMappingStrategy .getFloatArray ( v2 );
	 this.mCustomBrighteningCurveInterval = v2;
	 /* .line 64 */
	 /* nop */
	 /* .line 65 */
	 /* const v2, 0x11030038 */
	 (( android.content.res.Resources ) v0 ).obtainTypedArray ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
	 /* .line 64 */
	 com.android.server.display.BrightnessMappingStrategy .getFloatArray ( v2 );
	 this.mCustomDarkeningCurveInterval = v2;
	 /* .line 66 */
	 /* const v2, 0x11070026 */
	 v2 = 	 (( android.content.res.Resources ) v0 ).getFloat ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F
	 /* iput v2, p0, Lcom/android/server/display/BrightnessCurve;->mBrightnessMinTan:F */
	 /* .line 67 */
	 /* const v2, 0x11070033 */
	 v2 = 	 (( android.content.res.Resources ) v0 ).getFloat ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F
	 /* iput v2, p0, Lcom/android/server/display/BrightnessCurve;->mSecondOutdoorCurveBrightenMinTan:F */
	 /* .line 69 */
	 /* const v2, 0x11070039 */
	 v2 = 	 (( android.content.res.Resources ) v0 ).getFloat ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F
	 /* iput v2, p0, Lcom/android/server/display/BrightnessCurve;->mThirdOutdoorCurveBrightenMinTan:F */
	 /* .line 71 */
	 /* const v2, 0x11070027 */
	 v2 = 	 (( android.content.res.Resources ) v0 ).getFloat ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F
	 /* iput v2, p0, Lcom/android/server/display/BrightnessCurve;->mCurveAnchorPointLux:F */
	 /* .line 73 */
	 /* const v2, 0x11070029 */
	 v2 = 	 (( android.content.res.Resources ) v0 ).getFloat ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F
	 /* iput v2, p0, Lcom/android/server/display/BrightnessCurve;->mDarkLightCurvePullUpMaxTan:F */
	 /* .line 75 */
	 /* const v2, 0x11070028 */
	 v2 = 	 (( android.content.res.Resources ) v0 ).getFloat ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F
	 /* iput v2, p0, Lcom/android/server/display/BrightnessCurve;->mDarkLightCurvePullUpLimitNit:F */
	 /* .line 77 */
	 /* const v2, 0x1105000e */
	 v2 = 	 (( android.content.res.Resources ) v0 ).getBoolean ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z
	 /* iput-boolean v2, p0, Lcom/android/server/display/BrightnessCurve;->mBrightnessCurveAlgoAvailable:Z */
	 /* .line 79 */
	 /* const v2, 0x1107002c */
	 v2 = 	 (( android.content.res.Resources ) v0 ).getFloat ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F
	 /* iput v2, p0, Lcom/android/server/display/BrightnessCurve;->mIndoorLightCurveTan:F */
	 /* .line 81 */
	 this.mCurrentCurveInterval = v1;
	 /* .line 82 */
	 return;
} // .end method
private java.util.List buildCurve ( Float p0, Float p1 ) {
	 /* .locals 9 */
	 /* .param p1, "lux" # F */
	 /* .param p2, "changeNit" # F */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(FF)", */
	 /* "Ljava/util/List<", */
	 /* "Lcom/android/server/display/BrightnessCurve$Curve;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* .line 162 */
/* new-instance v0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;-><init>(Lcom/android/server/display/BrightnessCurve;)V */
/* .line 163 */
/* .local v0, "darkLightCurve":Lcom/android/server/display/BrightnessCurve$Curve; */
/* new-instance v1, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;-><init>(Lcom/android/server/display/BrightnessCurve;)V */
/* .line 164 */
/* .local v1, "indoorLightCurve":Lcom/android/server/display/BrightnessCurve$Curve; */
/* new-instance v2, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve; */
/* invoke-direct {v2, p0}, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;-><init>(Lcom/android/server/display/BrightnessCurve;)V */
/* .line 165 */
/* .local v2, "firstOutdoorLightCurve":Lcom/android/server/display/BrightnessCurve$Curve; */
/* new-instance v3, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve; */
/* invoke-direct {v3, p0}, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;-><init>(Lcom/android/server/display/BrightnessCurve;)V */
/* .line 166 */
/* .local v3, "secondOutdoorLightCurve":Lcom/android/server/display/BrightnessCurve$Curve; */
/* new-instance v4, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve; */
/* invoke-direct {v4, p0}, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;-><init>(Lcom/android/server/display/BrightnessCurve;)V */
/* .line 167 */
/* .local v4, "thirdOutdoorLightCurve":Lcom/android/server/display/BrightnessCurve$Curve; */
/* new-instance v5, Ljava/util/ArrayList; */
/* invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V */
/* .line 168 */
/* .local v5, "curveList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/BrightnessCurve$Curve;>;" */
v6 = this.mCurrentCurveInterval;
int v7 = 0; // const/4 v7, 0x0
/* aget v7, v6, v7 */
/* cmpl-float v7, p1, v7 */
int v8 = 1; // const/4 v8, 0x1
/* if-ltz v7, :cond_0 */
/* aget v7, v6, v8 */
/* cmpg-float v7, p1, v7 */
/* if-gez v7, :cond_0 */
/* .line 170 */
(( com.android.server.display.BrightnessCurve$Curve ) v0 ).create ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/BrightnessCurve$Curve;->create(FF)V
/* .line 171 */
(( com.android.server.display.BrightnessCurve$Curve ) v1 ).connectLeft ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 172 */
(( com.android.server.display.BrightnessCurve$Curve ) v2 ).connectLeft ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 173 */
(( com.android.server.display.BrightnessCurve$Curve ) v3 ).connectLeft ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 174 */
(( com.android.server.display.BrightnessCurve$Curve ) v4 ).connectLeft ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 175 */
} // :cond_0
/* aget v7, v6, v8 */
/* cmpl-float v7, p1, v7 */
int v8 = 2; // const/4 v8, 0x2
/* if-ltz v7, :cond_1 */
/* aget v7, v6, v8 */
/* cmpg-float v7, p1, v7 */
/* if-gez v7, :cond_1 */
/* .line 177 */
(( com.android.server.display.BrightnessCurve$Curve ) v1 ).create ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/android/server/display/BrightnessCurve$Curve;->create(FF)V
/* .line 178 */
(( com.android.server.display.BrightnessCurve$Curve ) v0 ).connectRight ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 179 */
(( com.android.server.display.BrightnessCurve$Curve ) v2 ).connectLeft ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 180 */
(( com.android.server.display.BrightnessCurve$Curve ) v3 ).connectLeft ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 181 */
(( com.android.server.display.BrightnessCurve$Curve ) v4 ).connectLeft ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 182 */
} // :cond_1
/* aget v7, v6, v8 */
/* cmpl-float v7, p1, v7 */
int v8 = 3; // const/4 v8, 0x3
/* if-ltz v7, :cond_2 */
/* aget v7, v6, v8 */
/* cmpg-float v7, p1, v7 */
/* if-gez v7, :cond_2 */
/* .line 184 */
(( com.android.server.display.BrightnessCurve$Curve ) v2 ).create ( p1, p2 ); // invoke-virtual {v2, p1, p2}, Lcom/android/server/display/BrightnessCurve$Curve;->create(FF)V
/* .line 185 */
(( com.android.server.display.BrightnessCurve$Curve ) v1 ).connectRight ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 186 */
(( com.android.server.display.BrightnessCurve$Curve ) v0 ).connectRight ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 187 */
(( com.android.server.display.BrightnessCurve$Curve ) v3 ).connectLeft ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 188 */
(( com.android.server.display.BrightnessCurve$Curve ) v4 ).connectLeft ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 189 */
} // :cond_2
/* aget v7, v6, v8 */
/* cmpl-float v7, p1, v7 */
int v8 = 4; // const/4 v8, 0x4
/* if-ltz v7, :cond_3 */
/* aget v7, v6, v8 */
/* cmpg-float v7, p1, v7 */
/* if-gez v7, :cond_3 */
/* .line 191 */
(( com.android.server.display.BrightnessCurve$Curve ) v3 ).create ( p1, p2 ); // invoke-virtual {v3, p1, p2}, Lcom/android/server/display/BrightnessCurve$Curve;->create(FF)V
/* .line 192 */
(( com.android.server.display.BrightnessCurve$Curve ) v2 ).connectRight ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 193 */
(( com.android.server.display.BrightnessCurve$Curve ) v1 ).connectRight ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 194 */
(( com.android.server.display.BrightnessCurve$Curve ) v0 ).connectRight ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 195 */
(( com.android.server.display.BrightnessCurve$Curve ) v4 ).connectLeft ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 196 */
} // :cond_3
/* aget v6, v6, v8 */
/* cmpl-float v6, p1, v6 */
/* if-ltz v6, :cond_4 */
/* .line 197 */
(( com.android.server.display.BrightnessCurve$Curve ) v4 ).create ( p1, p2 ); // invoke-virtual {v4, p1, p2}, Lcom/android/server/display/BrightnessCurve$Curve;->create(FF)V
/* .line 198 */
(( com.android.server.display.BrightnessCurve$Curve ) v3 ).connectRight ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 199 */
(( com.android.server.display.BrightnessCurve$Curve ) v2 ).connectRight ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 200 */
(( com.android.server.display.BrightnessCurve$Curve ) v1 ).connectRight ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 201 */
(( com.android.server.display.BrightnessCurve$Curve ) v0 ).connectRight ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V
/* .line 203 */
} // :cond_4
} // :goto_0
/* .line 204 */
/* .line 205 */
/* .line 206 */
/* .line 207 */
/* .line 208 */
} // .end method
private void computeBrightness ( ) {
/* .locals 5 */
/* .line 149 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mLux;
/* array-length v2, v1 */
/* if-ge v0, v2, :cond_2 */
v2 = this.mBrightness;
/* array-length v2, v2 */
/* if-ge v0, v2, :cond_2 */
/* .line 150 */
v2 = this.mLuxToNit;
/* aget v1, v1, v0 */
v1 = (( android.util.Spline ) v2 ).interpolate ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/Spline;->interpolate(F)F
/* .line 151 */
/* .local v1, "nit":F */
/* iget v2, p0, Lcom/android/server/display/BrightnessCurve;->mMinNit:F */
/* cmpg-float v2, v1, v2 */
/* if-gez v2, :cond_0 */
/* .line 152 */
/* iget v1, p0, Lcom/android/server/display/BrightnessCurve;->mMinNit:F */
/* .line 154 */
} // :cond_0
v2 = this.mBrightness;
v3 = this.mNitToBrightness;
v3 = (( android.util.Spline ) v3 ).interpolate ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/Spline;->interpolate(F)F
/* aput v3, v2, v0 */
/* .line 155 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 156 */
v2 = this.mBrightness;
/* aget v3, v2, v0 */
/* add-int/lit8 v4, v0, -0x1 */
/* aget v4, v2, v4 */
v3 = android.util.MathUtils .max ( v3,v4 );
/* aput v3, v2, v0 */
/* .line 149 */
} // .end local v1 # "nit":F
} // :cond_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 159 */
} // .end local v0 # "i":I
} // :cond_2
return;
} // .end method
private void copyToDefaultSpline ( Float p0, Float p1, java.util.List p2, Float p3, Float p4 ) {
/* .locals 6 */
/* .param p1, "startLux" # F */
/* .param p2, "endLux" # F */
/* .param p4, "diffNit" # F */
/* .param p5, "ratio" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(FF", */
/* "Ljava/util/List<", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Float;", */
/* "Ljava/lang/Float;", */
/* ">;>;FF)V" */
/* } */
} // .end annotation
/* .line 213 */
/* .local p3, "mPointList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;>;" */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mLux;
/* array-length v2, v1 */
/* if-ge v0, v2, :cond_1 */
/* aget v1, v1, v0 */
/* cmpg-float v2, v1, p2 */
/* if-gtz v2, :cond_1 */
/* .line 214 */
/* cmpg-float v2, v1, p1 */
/* if-gez v2, :cond_0 */
/* .line 215 */
/* .line 217 */
} // :cond_0
/* sub-float v1, p2, v1 */
/* mul-float/2addr v1, p5 */
/* sub-float v2, p2, p1 */
/* div-float/2addr v1, v2 */
/* const/high16 v2, 0x3f800000 # 1.0f */
/* sub-float/2addr v2, v1 */
/* .line 218 */
/* .local v2, "coefficient":F */
/* new-instance v1, Landroid/util/Pair; */
v3 = this.mLux;
/* aget v3, v3, v0 */
java.lang.Float .valueOf ( v3 );
v4 = this.mLuxToNitsDefault;
v5 = this.mLux;
/* aget v5, v5, v0 */
v4 = (( android.util.Spline ) v4 ).interpolate ( v5 ); // invoke-virtual {v4, v5}, Landroid/util/Spline;->interpolate(F)F
/* mul-float v5, p4, v2 */
/* sub-float/2addr v4, v5 */
java.lang.Float .valueOf ( v4 );
/* invoke-direct {v1, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 220 */
/* .local v1, "point":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
/* .line 213 */
} // .end local v1 # "point":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
} // .end local v2 # "coefficient":F
} // :goto_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 222 */
} // .end local v0 # "i":I
} // :cond_1
return;
} // .end method
private void createSpline ( java.util.List p0 ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/BrightnessCurve$Curve;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 124 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/BrightnessCurve$Curve;>;" */
/* invoke-direct {p0, p1}, Lcom/android/server/display/BrightnessCurve;->getSplinePointList(Ljava/util/List;)Ljava/util/List; */
/* .line 125 */
v1 = /* .local v0, "splinePointList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;>;" */
/* new-array v1, v1, [F */
/* .line 126 */
v2 = /* .local v1, "lux":[F */
/* new-array v2, v2, [F */
/* .line 127 */
/* .local v2, "nit":[F */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
v4 = } // :goto_0
/* if-ge v3, v4, :cond_0 */
/* .line 128 */
/* check-cast v4, Landroid/util/Pair; */
/* .line 129 */
/* .local v4, "point":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v5 = this.first;
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
/* aput v5, v1, v3 */
/* .line 130 */
v5 = this.second;
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
/* aput v5, v2, v3 */
/* .line 127 */
} // .end local v4 # "point":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
/* add-int/lit8 v3, v3, 0x1 */
/* .line 132 */
} // .end local v3 # "i":I
} // :cond_0
android.util.Spline .createLinearSpline ( v1,v2 );
this.mLuxToNit = v3;
/* .line 133 */
return;
} // .end method
private java.util.List getSplinePointList ( java.util.List p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/BrightnessCurve$Curve;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Float;", */
/* "Ljava/lang/Float;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 136 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/BrightnessCurve$Curve;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 137 */
v1 = /* .local v0, "splinePointList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;>;" */
/* if-lez v1, :cond_1 */
int v1 = 0; // const/4 v1, 0x0
/* check-cast v2, Lcom/android/server/display/BrightnessCurve$Curve; */
v2 = v2 = this.mPointList;
/* if-lez v2, :cond_1 */
/* .line 138 */
/* check-cast v2, Lcom/android/server/display/BrightnessCurve$Curve; */
v2 = this.mPointList;
/* check-cast v1, Landroid/util/Pair; */
/* .line 139 */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/display/BrightnessCurve$Curve; */
/* .line 140 */
/* .local v2, "curve":Lcom/android/server/display/BrightnessCurve$Curve; */
int v3 = 1; // const/4 v3, 0x1
/* .local v3, "i":I */
} // :goto_1
v4 = v4 = this.mPointList;
/* if-ge v3, v4, :cond_0 */
/* .line 141 */
v4 = this.mPointList;
/* check-cast v4, Landroid/util/Pair; */
/* .line 140 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 143 */
} // .end local v2 # "curve":Lcom/android/server/display/BrightnessCurve$Curve;
} // .end local v3 # "i":I
} // :cond_0
/* .line 145 */
} // :cond_1
} // .end method
private void pullDownCurveCreate ( Float p0, Float p1, java.util.List p2, Float p3, Float p4, Float p5, Float p6 ) {
/* .locals 13 */
/* .param p1, "lux" # F */
/* .param p2, "changeNit" # F */
/* .param p4, "headLux" # F */
/* .param p5, "tailLux" # F */
/* .param p6, "minTanToHead" # F */
/* .param p7, "minTanToTail" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(FF", */
/* "Ljava/util/List<", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Float;", */
/* "Ljava/lang/Float;", */
/* ">;>;FFFF)V" */
/* } */
} // .end annotation
/* .line 226 */
/* .local p3, "pointList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;>;" */
/* move-object v0, p0 */
/* move-object/from16 v1, p3 */
/* move/from16 v2, p4 */
/* move/from16 v3, p5 */
v4 = this.mLuxToNitsDefault;
v4 = (( android.util.Spline ) v4 ).interpolate ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/Spline;->interpolate(F)F
v5 = this.mLuxToNitsDefault;
int v6 = 0; // const/4 v6, 0x0
v5 = (( android.util.Spline ) v5 ).interpolate ( v6 ); // invoke-virtual {v5, v6}, Landroid/util/Spline;->interpolate(F)F
/* sub-float/2addr v4, v5 */
/* div-float/2addr v4, v3 */
/* .line 228 */
/* .local v4, "tan0LuxToTailLux":F */
/* move/from16 v5, p6 */
/* .line 229 */
/* .local v5, "tanToHeadLux":F */
/* cmpl-float v7, p1, v2 */
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 230 */
v7 = this.mLuxToNitsDefault;
v7 = (( android.util.Spline ) v7 ).interpolate ( v2 ); // invoke-virtual {v7, v2}, Landroid/util/Spline;->interpolate(F)F
/* sub-float v7, p2, v7 */
/* sub-float v8, p1, v2 */
/* div-float v5, v7, v8 */
/* .line 232 */
} // :cond_0
/* cmpg-float v7, v5, p6 */
/* if-gez v7, :cond_1 */
/* .line 233 */
/* move/from16 v5, p6 */
/* .line 235 */
} // :cond_1
/* sub-float v7, p1, v2 */
/* mul-float/2addr v7, v5 */
/* sub-float v7, p2, v7 */
/* .line 236 */
/* .local v7, "headPointNit":F */
/* new-instance v8, Landroid/util/Pair; */
/* invoke-static/range {p4 ..p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float; */
java.lang.Float .valueOf ( v7 );
/* invoke-direct {v8, v9, v10}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 237 */
/* .local v8, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
/* .line 238 */
/* cmpl-float v9, p1, v2 */
if ( v9 != null) { // if-eqz v9, :cond_2
/* .line 239 */
/* new-instance v9, Landroid/util/Pair; */
java.lang.Float .valueOf ( p1 );
java.lang.Float .valueOf ( p2 );
/* invoke-direct {v9, v10, v11}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 240 */
/* .local v9, "changePoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
/* .line 242 */
} // .end local v9 # "changePoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
} // :cond_2
v9 = this.mLuxToNitsDefault;
v6 = (( android.util.Spline ) v9 ).interpolate ( v6 ); // invoke-virtual {v9, v6}, Landroid/util/Spline;->interpolate(F)F
/* sub-float v6, p2, v6 */
/* div-float/2addr v6, p1 */
/* .line 243 */
/* .local v6, "tanToStartPoint":F */
/* cmpl-float v9, v6, v4 */
/* if-lez v9, :cond_3 */
/* .line 244 */
/* new-instance v9, Landroid/util/Pair; */
/* invoke-static/range {p5 ..p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float; */
v11 = this.mLuxToNitsDefault;
v11 = (( android.util.Spline ) v11 ).interpolate ( v3 ); // invoke-virtual {v11, v3}, Landroid/util/Spline;->interpolate(F)F
java.lang.Float .valueOf ( v11 );
/* invoke-direct {v9, v10, v11}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 245 */
/* .local v9, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
/* .line 246 */
return;
/* .line 248 */
} // .end local v9 # "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
} // :cond_3
/* cmpg-float v9, v6, p7 */
/* if-gez v9, :cond_4 */
/* .line 249 */
/* move/from16 v6, p7 */
/* .line 251 */
} // :cond_4
/* sub-float v9, v3, p1 */
/* mul-float/2addr v9, v6 */
/* add-float/2addr v9, p2 */
/* .line 252 */
/* .local v9, "tailPointNit":F */
/* new-instance v10, Landroid/util/Pair; */
/* invoke-static/range {p5 ..p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float; */
java.lang.Float .valueOf ( v9 );
/* invoke-direct {v10, v11, v12}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 253 */
/* .local v10, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
/* .line 254 */
return;
} // .end method
private void pullUpConnectTail ( android.util.Pair p0, Float p1, Float p2, java.util.List p3 ) {
/* .locals 7 */
/* .param p2, "tailLux" # F */
/* .param p3, "minTan" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Float;", */
/* "Ljava/lang/Float;", */
/* ">;FF", */
/* "Ljava/util/List<", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Float;", */
/* "Ljava/lang/Float;", */
/* ">;>;)V" */
/* } */
} // .end annotation
/* .line 269 */
/* .local p1, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
/* .local p4, "pointList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;>;" */
/* .line 270 */
v0 = this.first;
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 271 */
/* .local v0, "lux":F */
v1 = this.second;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* .line 272 */
/* .local v1, "changeNit":F */
/* move v2, p3 */
/* .line 273 */
/* .local v2, "tanToTailPoint":F */
/* cmpl-float v3, p2, v0 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 274 */
v3 = this.mLuxToNitsDefault;
v3 = (( android.util.Spline ) v3 ).interpolate ( p2 ); // invoke-virtual {v3, p2}, Landroid/util/Spline;->interpolate(F)F
/* sub-float/2addr v3, v1 */
/* sub-float v4, p2, v0 */
/* div-float v2, v3, v4 */
/* .line 276 */
} // :cond_0
/* cmpg-float v3, v2, p3 */
/* if-gez v3, :cond_1 */
/* .line 277 */
/* move v2, p3 */
/* .line 279 */
} // :cond_1
/* sub-float v3, p2, v0 */
/* mul-float/2addr v3, v2 */
/* add-float/2addr v3, v1 */
/* .line 280 */
/* .local v3, "tailPointNit":F */
/* new-instance v4, Landroid/util/Pair; */
java.lang.Float .valueOf ( p2 );
java.lang.Float .valueOf ( v3 );
/* invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 281 */
/* .local v4, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
/* .line 282 */
return;
} // .end method
private void pullUpCurveCreate ( Float p0, Float p1, java.util.List p2, Float p3, Float p4, Float p5, Float p6 ) {
/* .locals 7 */
/* .param p1, "lux" # F */
/* .param p2, "changeNit" # F */
/* .param p4, "headLux" # F */
/* .param p5, "tailLux" # F */
/* .param p6, "anchorPointLux" # F */
/* .param p7, "anchorPointNit" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(FF", */
/* "Ljava/util/List<", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Float;", */
/* "Ljava/lang/Float;", */
/* ">;>;FFFF)V" */
/* } */
} // .end annotation
/* .line 258 */
/* .local p3, "pointList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;>;" */
/* sub-float v0, p2, p7 */
/* sub-float v1, p1, p6 */
/* div-float/2addr v0, v1 */
/* .line 259 */
/* .local v0, "tanToAnchor":F */
/* sub-float v1, p1, p4 */
/* mul-float/2addr v1, v0 */
/* sub-float v1, p2, v1 */
/* .line 260 */
/* .local v1, "headPointNit":F */
/* new-instance v2, Landroid/util/Pair; */
java.lang.Float .valueOf ( p4 );
java.lang.Float .valueOf ( v1 );
/* invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 261 */
/* .local v2, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
/* .line 262 */
/* sub-float v3, p5, p1 */
/* mul-float/2addr v3, v0 */
/* add-float/2addr v3, p2 */
/* .line 263 */
/* .local v3, "tailPointNit":F */
/* new-instance v4, Landroid/util/Pair; */
java.lang.Float .valueOf ( p5 );
java.lang.Float .valueOf ( v3 );
/* invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 264 */
/* .local v4, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
/* .line 265 */
return;
} // .end method
private void setCurveInterval ( android.hardware.display.BrightnessConfiguration p0 ) {
/* .locals 2 */
/* .param p1, "config" # Landroid/hardware/display/BrightnessConfiguration; */
/* .line 102 */
(( android.hardware.display.BrightnessConfiguration ) p1 ).getDescription ( ); // invoke-virtual {p1}, Landroid/hardware/display/BrightnessConfiguration;->getDescription()Ljava/lang/String;
/* .line 103 */
/* .local v0, "description":Ljava/lang/String; */
final String v1 = "custom_brightness_curve_brightening"; // const-string v1, "custom_brightness_curve_brightening"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 104 */
v1 = this.mCustomBrighteningCurveInterval;
this.mCurrentCurveInterval = v1;
/* .line 105 */
} // :cond_0
final String v1 = "custom_brightness_curve_darkening"; // const-string v1, "custom_brightness_curve_darkening"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 106 */
v1 = this.mCustomDarkeningCurveInterval;
this.mCurrentCurveInterval = v1;
/* .line 108 */
} // :cond_1
v1 = this.mDefaultCurveInterval;
this.mCurrentCurveInterval = v1;
/* .line 110 */
} // :goto_0
return;
} // .end method
private void updateData ( Float[] p0, Float[] p1 ) {
/* .locals 3 */
/* .param p1, "lux" # [F */
/* .param p2, "brightness" # [F */
/* .line 92 */
v0 = this.mConfig;
/* invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessCurve;->setCurveInterval(Landroid/hardware/display/BrightnessConfiguration;)V */
/* .line 93 */
v0 = this.mConfig;
(( android.hardware.display.BrightnessConfiguration ) v0 ).getCurve ( ); // invoke-virtual {v0}, Landroid/hardware/display/BrightnessConfiguration;->getCurve()Landroid/util/Pair;
/* .line 94 */
/* .local v0, "defaultCurve":Landroid/util/Pair;, "Landroid/util/Pair<[F[F>;" */
v1 = this.first;
/* check-cast v1, [F */
v2 = this.second;
/* check-cast v2, [F */
android.util.Spline .createLinearSpline ( v1,v2 );
this.mLuxToNitsDefault = v1;
/* .line 95 */
v1 = this.mBrightnessToNit;
int v2 = 0; // const/4 v2, 0x0
v1 = (( android.util.Spline ) v1 ).interpolate ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F
/* iput v1, p0, Lcom/android/server/display/BrightnessCurve;->mMinNit:F */
/* .line 96 */
v1 = this.first;
/* check-cast v1, [F */
v2 = this.first;
/* check-cast v2, [F */
/* array-length v2, v2 */
/* add-int/lit8 v2, v2, -0x1 */
/* aget v1, v1, v2 */
/* iput v1, p0, Lcom/android/server/display/BrightnessCurve;->mMaxLux:F */
/* .line 97 */
this.mLux = p1;
/* .line 98 */
this.mBrightness = p2;
/* .line 99 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 640 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 641 */
final String v0 = "BrightnessCurve Configuration:"; // const-string v0, "BrightnessCurve Configuration:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 642 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mBrightnessCurveAlgoAvailable="; // const-string v1, " mBrightnessCurveAlgoAvailable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/BrightnessCurve;->mBrightnessCurveAlgoAvailable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 643 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mConfig="; // const-string v1, " mConfig="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mConfig;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 644 */
return;
} // .end method
public Boolean isEnable ( ) {
/* .locals 3 */
/* .line 629 */
v0 = this.mConfig;
(( android.hardware.display.BrightnessConfiguration ) v0 ).getDescription ( ); // invoke-virtual {v0}, Landroid/hardware/display/BrightnessConfiguration;->getDescription()Ljava/lang/String;
/* .line 630 */
/* .local v0, "description":Ljava/lang/String; */
/* iget-boolean v1, p0, Lcom/android/server/display/BrightnessCurve;->mBrightnessCurveAlgoAvailable:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
final String v1 = "custom_brightness_curve_default"; // const-string v1, "custom_brightness_curve_default"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 631 */
final String v1 = "custom_brightness_curve_brightening"; // const-string v1, "custom_brightness_curve_brightening"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 632 */
final String v1 = "custom_brightness_curve_darkening"; // const-string v1, "custom_brightness_curve_darkening"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
v1 = this.mDefault;
v2 = this.mConfig;
/* .line 633 */
v1 = (( android.hardware.display.BrightnessConfiguration ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Landroid/hardware/display/BrightnessConfiguration;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 634 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* .line 636 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
public android.util.Pair smoothNewCurveV2 ( Float[] p0, Float[] p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "lux" # [F */
/* .param p2, "brightness" # [F */
/* .param p3, "idx" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([F[FI)", */
/* "Landroid/util/Pair<", */
/* "[F[F>;" */
/* } */
} // .end annotation
/* .line 113 */
/* aget v0, p1, p3 */
/* .line 114 */
/* .local v0, "changeLux":F */
v1 = this.mBrightnessToNit;
/* aget v2, p2, p3 */
v1 = (( android.util.Spline ) v1 ).interpolate ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F
/* .line 115 */
/* .local v1, "changeNit":F */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "smoothNewCurveV2: changeLux: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = ", changeNit: "; // const-string v3, ", changeNit: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "BrightnessCurve"; // const-string v3, "BrightnessCurve"
android.util.Slog .d ( v3,v2 );
/* .line 116 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/BrightnessCurve;->updateData([F[F)V */
/* .line 117 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/display/BrightnessCurve;->buildCurve(FF)Ljava/util/List; */
/* .line 118 */
/* .local v2, "listPoint":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/BrightnessCurve$Curve;>;" */
/* invoke-direct {p0, v2}, Lcom/android/server/display/BrightnessCurve;->createSpline(Ljava/util/List;)V */
/* .line 119 */
/* invoke-direct {p0}, Lcom/android/server/display/BrightnessCurve;->computeBrightness()V */
/* .line 120 */
v3 = this.mLux;
v4 = this.mBrightness;
android.util.Pair .create ( v3,v4 );
} // .end method
public void updateSplineConfig ( android.hardware.display.BrightnessConfiguration p0, android.util.Spline p1, android.util.Spline p2 ) {
/* .locals 0 */
/* .param p1, "config" # Landroid/hardware/display/BrightnessConfiguration; */
/* .param p2, "nitsToBrightnessSpline" # Landroid/util/Spline; */
/* .param p3, "brightnessToNitsSpline" # Landroid/util/Spline; */
/* .line 86 */
this.mConfig = p1;
/* .line 87 */
this.mNitToBrightness = p2;
/* .line 88 */
this.mBrightnessToNit = p3;
/* .line 89 */
return;
} // .end method
