.class final Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;
.super Ljava/lang/Object;
.source "SwipeUpWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/SwipeUpWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DualSpringAnimation"
.end annotation


# instance fields
.field private mSpringAnimationAlpha:Lcom/android/server/display/animation/SpringAnimation;

.field private mSpringAnimationY:Lcom/android/server/display/animation/SpringAnimation;

.field final synthetic this$0:Lcom/android/server/display/SwipeUpWindow;


# direct methods
.method public constructor <init>(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/animation/SpringAnimation;Lcom/android/server/display/animation/SpringAnimation;)V
    .locals 0
    .param p2, "springAnimationY"    # Lcom/android/server/display/animation/SpringAnimation;
    .param p3, "springAnimationAlpha"    # Lcom/android/server/display/animation/SpringAnimation;

    .line 849
    iput-object p1, p0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->this$0:Lcom/android/server/display/SwipeUpWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 850
    iput-object p2, p0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->mSpringAnimationY:Lcom/android/server/display/animation/SpringAnimation;

    .line 851
    iput-object p3, p0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->mSpringAnimationAlpha:Lcom/android/server/display/animation/SpringAnimation;

    .line 852
    return-void
.end method


# virtual methods
.method public animateToFinalPosition(FF)V
    .locals 1
    .param p1, "y"    # F
    .param p2, "alpha"    # F

    .line 870
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->mSpringAnimationY:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v0, p1}, Lcom/android/server/display/animation/SpringAnimation;->animateToFinalPosition(F)V

    .line 871
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->mSpringAnimationAlpha:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v0, p2}, Lcom/android/server/display/animation/SpringAnimation;->animateToFinalPosition(F)V

    .line 872
    return-void
.end method

.method public cancel()V
    .locals 1

    .line 860
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->mSpringAnimationY:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v0}, Lcom/android/server/display/animation/SpringAnimation;->cancel()V

    .line 861
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->mSpringAnimationAlpha:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v0}, Lcom/android/server/display/animation/SpringAnimation;->cancel()V

    .line 862
    return-void
.end method

.method public skipToEnd()V
    .locals 1

    .line 865
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->mSpringAnimationY:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v0}, Lcom/android/server/display/animation/SpringAnimation;->skipToEnd()V

    .line 866
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->mSpringAnimationAlpha:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v0}, Lcom/android/server/display/animation/SpringAnimation;->skipToEnd()V

    .line 867
    return-void
.end method

.method public start()V
    .locals 1

    .line 855
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->mSpringAnimationY:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v0}, Lcom/android/server/display/animation/SpringAnimation;->start()V

    .line 856
    iget-object v0, p0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->mSpringAnimationAlpha:Lcom/android/server/display/animation/SpringAnimation;

    invoke-virtual {v0}, Lcom/android/server/display/animation/SpringAnimation;->start()V

    .line 857
    return-void
.end method
